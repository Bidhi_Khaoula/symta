tableextension 50025 "Average Cost Calc. Overview" extends "Average Cost Calc. Overview"//5847
{
    var
        ValueEntrySymta: Record "Value Entry";

    procedure CalculateAverageCostSymta() AverageCost: Decimal
    begin
        AverageCost := 0;
        if Type = Type::"Closing Entry" then begin
            SetItemFiltersSymta();
            ValueEntrySymta.SumCostsTillValuationDate(ValueEntrySymta);
            if ValueEntrySymta."Item Ledger Entry Quantity" = 0 then
                exit(AverageCost);
            AverageCost :=
              (ValueEntrySymta."Cost Amount (Actual)" + ValueEntrySymta."Cost Amount (Expected)") /
              ValueEntrySymta."Item Ledger Entry Quantity";
            exit(AverageCost);
        end;
        if Quantity = 0 then
            exit(AverageCost);
        AverageCost := ("Cost Amount (Actual)" + "Cost Amount (Expected)") / Quantity;
        exit(AverageCost);
    end;


    procedure SetItemFiltersSymta()
    begin
        ValueEntrySymta."Item No." := "Item No.";
        ValueEntrySymta."Valuation Date" := "Valuation Date";
        ValueEntrySymta."Location Code" := "Location Code";
        ValueEntrySymta."Variant Code" := "Variant Code";
    end;

}
