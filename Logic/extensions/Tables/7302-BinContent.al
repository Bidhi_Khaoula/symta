tableextension 50417 "Bin Content Logic" extends "Bin Content" //7302
{

    procedure SetCurrFieldNo(NewCurrFieldNo: Integer)
    begin
        // ** Cette fonction permet d'initialiser CurrFieldNo afin de passer dans les validates depuis du code **//
        CurrFieldNo := NewCurrFieldNo;
    end;

    procedure ShowBinContents2(LocationCode: Code[10]; ItemNo: Code[20]; VariantCode: Code[10]; BinCode: Code[20])
    var
        BinContent: Record "Bin Content";
        BinContentLookup: Page "Bin Contents";
    begin
        IF BinCode <> '' THEN
            BinContent.SETRANGE("Bin Code", BinCode)
        ELSE
            BinContent.SETCURRENTKEY("Location Code", "Item No.", "Variant Code");
        BinContent.SETRANGE("Item No.", ItemNo);
        BinContent.SETRANGE("Variant Code", VariantCode);
        BinContent.SETRANGE("Location Code", LocationCode);
        BinContentLookup.SETTABLEVIEW(BinContent);

        BinContentLookup.RUNMODAL();
        CLEAR(BinContentLookup);
    end;

    procedure "GetCommentaireInfoEntrée"(_pType: Code[10]): Text[40]
    var
        WhseLine: Record "Warehouse Entry";
    begin
        WhseLine.RESET();
        WhseLine.SETRANGE("Location Code", "Location Code");
        WhseLine.SETRANGE("Bin Code", "Bin Code");
        WhseLine.SETRANGE("Item No.", "Item No.");
        WhseLine.SETRANGE("Variant Code", "Variant Code");
        WhseLine.SETRANGE("Unit of Measure Code", "Unit of Measure Code");
        IF NOT WhseLine.FINDLAST() THEN EXIT('');

        CASE _pType OF
            'COMMENT':
                BEGIN
                    // CFR le 11/05/2022 => R‚gie : info commentaire reclassement QUE sur le casier de destinatation
                    IF (WhseLine."Reference Document" = WhseLine."Reference Document"::"Item Journal") AND
                       (WhseLine."Entry Type" = WhseLine."Entry Type"::Movement) THEN
                        IF (WhseLine.Quantity > 0) THEN
                            EXIT(WhseLine.Commentaire)
                        ELSE
                            EXIT('');

                    // FIN CFR le 11/05/2022

                    EXIT(WhseLine.Commentaire);
                END;
            'DATE':
                EXIT(FORMAT(WhseLine."Registering Date"));
        END;
    end;

    procedure GetDesignation(): Text[100]
    var
        LItem: Record Item;
    begin
        IF LItem.GET("Item No.") THEN
            EXIT(LItem.Description);
    end;
}

