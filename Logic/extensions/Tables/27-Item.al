tableextension 50242 "Item Logic" extends Item //27
{
    DataCaptionFields = "No. 2", Description;
    fields
    {
        modify("Sales multiple")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 08-06-2017 => Régie
                IF "Sales multiple" <> xRec."Sales multiple" THEN BEGIN
                    "date maj multiple" := WORKDATE();
                    "user maj multiple" := COPYSTR(USERID, 1, MAXSTRLEN("user maj multiple"));
                END;
                // FIN MCO Le 08-06-22017
            end;
        }
        modify("Classe Poids")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement
                CreateShelfNo();
            end;
        }
        modify("Allée")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement
                CreateShelfNo();
            end;
        }
        modify("N° Emplacement")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement
                CreateShelfNo();
            end;
        }

        modify("Process Blocked")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 26-08-2009 => Gestion avancé du bloque
                VALIDATE(Blocked, "Process Blocked" = "Process Blocked"::"Ventes & Achats");
                // FIN AD Le 26-08-2009
            end;
        }
        modify("Sans Mouvements de Stock")
        {
            trigger OnAfterValidate()
            var
                TransLine: Record "Transfer Line";
                ItemStock: Record Item;
                ReservEntry: Record "Reservation Entry";
                SalesOrderLine: Record "Sales Line";
                PurchOrderLine: Record "Purchase Line";
            begin
                // AD Le 17-09-2009 => Non stocké

                ItemStock.GET("No.");
                ItemStock.CALCFIELDS(Inventory, "Net Invoiced Qty.");
                IF (ItemStock.Inventory <> 0) OR (ItemStock."Net Invoiced Qty." <> 0) THEN
                    ERROR(Text50010);


                SalesOrderLine.SETCURRENTKEY("Document Type", Type, "No.");
                SalesOrderLine.SETFILTER("Document Type", '%1|%2',
                SalesOrderLine."Document Type"::Order, SalesOrderLine."Document Type"::"Return Order");
                SalesOrderLine.SETRANGE(Type, SalesOrderLine.Type::Item);
                SalesOrderLine.SETRANGE("No.", "No.");
                IF SalesOrderLine."Document Type" = SalesOrderLine."Document Type"::Order THEN
                    SalesOrderLine.SETFILTER("Qty. Shipped Not Invoiced", '<>0');
                IF SalesOrderLine."Document Type" = SalesOrderLine."Document Type"::"Return Order" THEN
                    SalesOrderLine.SETFILTER("Return Qty. Rcd. Not Invd.", '<>0');
                IF Not SalesOrderLine.IsEmpty() THEN
                    ERROR(Text50011);

                PurchOrderLine.SETCURRENTKEY("Document Type", Type, "No.");
                PurchOrderLine.SETFILTER("Document Type", '%1|%2',
                  PurchOrderLine."Document Type"::Order, PurchOrderLine."Document Type"::"Return Order");
                PurchOrderLine.SETRANGE(Type, PurchOrderLine.Type::Item);
                PurchOrderLine.SETRANGE("No.", "No.");
                IF PurchOrderLine."Document Type" = PurchOrderLine."Document Type"::Order THEN
                    PurchOrderLine.SETFILTER("Qty. Rcd. Not Invoiced", '<>0');
                IF PurchOrderLine."Document Type" = PurchOrderLine."Document Type"::"Return Order" THEN
                    PurchOrderLine.SETFILTER(PurchOrderLine."Return Qty. Shipped Not Invd.", '<>0');
                IF Not PurchOrderLine.IsEmpty() THEN
                    ERROR(Text50011);

                TransLine.SETCURRENTKEY("Item No.");
                TransLine.SETRANGE("Item No.", "No.");
                IF Not TransLine.IsEmpty() THEN
                    ERROR(Text50011);

                ReservEntry.SETCURRENTKEY("Reservation Status", "Item No.");
                ReservEntry.SETRANGE("Reservation Status", ReservEntry."Reservation Status"::Reservation);
                ReservEntry.SETRANGE("Item No.", "No.");
                ReservEntry.CALCSUMS("Quantity (Base)");
                IF ReservEntry."Quantity (Base)" <> 0 THEN
                    ERROR(Text50011);

                IF "Sans Mouvements de Stock" = TRUE THEN
                    TESTFIELD("Costing Method", "Costing Method"::Standard);
                // FIN AD Le 17-09-2009
            end;
        }

        modify("Stock Mort")
        {
            trigger OnAfterValidate()
            begin

                IF "Stock Mort" <> xRec."Stock Mort" THEN BEGIN
                    "date maj stock mort" := WORKDATE();
                    Evaluate("user maj stock mort", USERID());
                END;
            end;
        }
        modify("Stocké")
        {
            trigger OnAfterValidate()
            begin

                IF Stocké <> xRec.Stocké THEN BEGIN
                    "date maj stocké" := WORKDATE();
                    Evaluate("user maj stocké", USERID());
                END;
            end;
        }
        modify("Recommended Shipping Agent")
        {
            trigger OnAfterValidate()
            BEGIN
                IF ("Recommended Shipping Agent" = '') THEN
                    Rec."Mandatory Shipping Agent" := FALSE;
            END;
        }
        modify("Mandatory Shipping Agent")
        {
            Trigger OnBeforeValidate()
            BEGIN
                IF ("Recommended Shipping Agent" = '') THEN
                    ERROR('Saisir d''abord le Transporteur préconisé');
            END;
        }

        modify("user maj stock mort")
        {
            trigger OnAfterValidate()

            begin
                CduFunctions.ValidateUserID("user maj stock mort");
            end;
        }

        modify("user maj stocké")
        {
            trigger OnAfterValidate()
            begin
                CduFunctions.ValidateUserID("user maj stocké");
            end;
        }

        modify("user maj multiple")
        {
            trigger OnAfterValidate()
            begin
                //UserMgt.ValidateUserID("user maj multiple");
            end;
        }

        modify(Publiable)
        {
            trigger OnAfterValidate()
            begin
                // AD Le 28-04-2019 => FE20190425
                IF Rec.Publiable <> xRec.Publiable THEN
                    VALIDATE("Date Modif. Publiable Web", WORKDATE());
            end;
        }

    }

    trigger OnInsert()
    begin
        rec."Automatic Ext. Texts" := true;
    end;



    procedure CreationGencod(): Code[13]
    var
        CompanyInfo: Record "Company Information";
        Item: Record Item;
        Gencod: Code[13];
        i: Integer;
    begin
        // AD Le 17-07-2009 => Calcul du gencode
        // PAS CHEZ SYMTA
        EXIT;

        //*******************************Code Non utiliser*********************************************
        // CompanyInfo.GET();

        // CompanyInfo.TESTFIELD("CNUF Code");

        // Gencod := '';
        // IF (STRLEN(CompanyInfo."CNUF Code") + STRLEN("No.")) < 12 THEN
        //     FOR i := 1 TO (12 - (STRLEN(CompanyInfo."CNUF Code") + STRLEN("No."))) DO
        //         Gencod := Gencod + '0';

        // Gencod := CompanyInfo."CNUF Code" + Gencod + "No.";

        // Gencod := Gencod + FORMAT(STRCHECKSUM(Gencod, '131313131313', 10));

        // Item.RESET();
        // Item.SETRANGE("Gencod EAN13", Gencod);
        // IF Item.FINDFIRST() THEN
        //     ERROR(Text50001, Gencod, Item."No.");


        // VALIDATE("Gencod EAN13", Gencod);
        //*******************************Code Non utiliser*********************************************
    end;

    procedure GetHorsNorme(Qty: Decimal): Boolean
    begin
        // AD Le 21-10-2009 => On passe sue 1/3 de la conso normative
        //EXIT ( Qty > "Check Qty Hors Norme" );
        IF "Conso. Normative" = 0 THEN
            EXIT(FALSE);

        EXIT(Qty > ("Conso. Normative" / 3));
    end;

    procedure CreateShelfNo()
    begin
        // AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement
        VALIDATE("Shelf No.", STRSUBSTNO('%1-%2%3', "Classe Poids", Allée, "N° Emplacement"));
    end;

    procedure GetSalesPriceAllCustomer(): Decimal
    var
        SalesPrice: Record "Sales Price";
    begin
        SalesPrice.SETRANGE("Item No.", "No.");
        SalesPrice.SETRANGE("Sales Type", SalesPrice."Sales Type"::"All Customers");
        SalesPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
        SalesPrice.SETFILTER("Currency Code", '%1', '');
        SalesPrice.SETFILTER("Unit of Measure Code", '%1|%2', "Base Unit of Measure", '');
        SalesPrice.SETRANGE("Starting Date", 0D, WORKDATE());
        IF SalesPrice.FINDLAST() THEN
            EXIT(SalesPrice."Unit Price")
        ELSE
            EXIT(0);
    end;

    procedure CalcAvailability(): Decimal
    var
        Item: Record Item;
        AvailableToPromise: Codeunit "Available to Promise";
        LookaheadDateformula: DateFormula;
        GrossRequirement: Decimal;
        ScheduledReceipt: Decimal;
        PeriodType: Option Day,Week,Month,Quarter,Year;
        AvailabilityDate: Date;
    begin
        AvailabilityDate := WORKDATE();

        Item.GET("No.");

        Item.RESET();
        Item.SETRANGE("Date Filter", 0D, AvailabilityDate);
        Item.SETRANGE("Drop Shipment Filter", FALSE);

        EXIT(
          AvailableToPromise.CalcQtyAvailableToPromise(
            Item,
            GrossRequirement,
            ScheduledReceipt,
            AvailabilityDate,
            "Analysis Period Type".FromInteger(PeriodType),
            LookaheadDateformula));
    end;

    procedure OpenLinkDocument(Type: Code[10])
    var
        OpenLink: Page "Liste Fichiers / Répertoires";
    begin
        // Ouvre des document associés
        CASE Type OF
            'PHOTO':
                OpenLink.InitialisationPhoto(Rec);
            'QUALITE':
                OpenLink.InitialisationQuanlité(Rec);
            'FICHE':
                OpenLink.InitialisationFicheProduit(Rec);
            'VUEECLAT':
                OpenLink.InitialisationVuesEclatées(Rec);
            'ATWORKS':
                OpenLink.InitialisationArtworks(Rec);
            'PALETTE':
                OpenLink.InitialisationPalette(Rec);
        END;
        OpenLink.Initialisation();
        OpenLink.EDITABLE := FALSE;
        OpenLink.RUNMODAL();
    end;

    procedure CalcAttenduReserve(var "_Reservé": Decimal; var _Attendu: Decimal)
    begin

        CALCFIELDS("Qty. on Component Lines", "Planning Issues (Qty.)",
          "Qty. on Sales Order", "Qty. on Service Order",
          "Trans. Ord. Shipment (Qty.)",
          Inventory,
          "Scheduled Receipt (Qty.)", "Qty. on Purch. Order",
          "Trans. Ord. Receipt (Qty.)", "Qty. in Transit",
          "Qty. on Blanket Sales Order",
           // MC Le 08-02-2013 => Prise en compte des retours
           "Qty. on Purchase Return"
          );

        _Reservé := "Qty. on Component Lines" + "Planning Issues (Qty.)" +
          "Qty. on Sales Order" + "Qty. on Service Order" +
          "Trans. Ord. Shipment (Qty.)" + "Qty. on Blanket Sales Order"
          // MC Le 08-02-2013 => Prise en compte des retours
          + "Qty. on Purchase Return";


        //Attendu := LItem."Scheduled Receipt (Qty.)" + LItem."Purch. Req. Receipt (Qty.)" +
        _Attendu := "Scheduled Receipt (Qty.)" + // LItem."Purch. Req. Receipt (Qty.)" +
          "Qty. on Purch. Order" + "Trans. Ord. Receipt (Qty.)" +
          "Qty. in Transit";


        CALCFIELDS("Qty. on Component Lines", "Qty. on Prod. Order");
        _Reservé -= "Qty. on Component Lines";
        _Attendu -= "Qty. on Prod. Order";
    end;

    procedure DateDerniereSortie(): Date
    var
        ItemLedgerEntry: Record "Item Ledger Entry";
    begin
        ItemLedgerEntry.SETCURRENTKEY("Item No.", "Posting Date");
        ItemLedgerEntry.SETRANGE("Item No.", "No.");
        ItemLedgerEntry.SETFILTER("Entry Type", '%1|%2|%3', ItemLedgerEntry."Entry Type"::Sale,
               ItemLedgerEntry."Entry Type"::"Negative Adjmt.",
                                  ItemLedgerEntry."Entry Type"::Consumption);
        ItemLedgerEntry.SETFILTER(Quantity, '<>%1', 0);
        IF ItemLedgerEntry.FINDLAST() THEN
            EXIT(ItemLedgerEntry."Posting Date")
        ELSE
            EXIT(0D);
    end;

    procedure FileExist(): Boolean
    var
        Lien: Record "Record Link";
    begin
        Lien.SETRANGE(Company, COMPANYNAME);
        Lien.SETFILTER("Record ID", TABLENAME + ':' + "No.");
        EXIT(Not Lien.IsEmpty());
    end;

    procedure GestionPlusFourni(_pVariantCode: Code[10]; _pVendorNo: Code[20])
    var
        LItemVendor: Record "Item Vendor";
        LFrnNewHolloand: Code[10];
        LPlusFourni: Boolean;
    begin
        LPlusFourni := TRUE;
        LItemVendor.RESET();
        LItemVendor.SETRANGE("Item No.", "No.");
        LItemVendor.SETRANGE("Variant Code", _pVariantCode);
        IF _pVendorNo <> '' THEN
            LItemVendor.SETRANGE("Vendor No.", _pVendorNo);

        IF LItemVendor.FindSet() THEN
            REPEAT
                IF NOT ((LItemVendor."Statut Approvisionnement" IN [LItemVendor."Statut Approvisionnement"::Bloqué,
                      LItemVendor."Statut Approvisionnement"::"Plus Fourni"]) OR
                      (LItemVendor."Statut Qualité" IN [LItemVendor."Statut Qualité"::"Non Conforme",
                                                       LItemVendor."Statut Qualité"::"A contrôler",
                                                       LItemVendor."Statut Qualité"::"Contrôle en cours"])) THEN
                    LPlusFourni := FALSE;
            UNTIL LItemVendor.NEXT() = 0;

        IF LPlusFourni THEN
            MESSAGE(ESK001, "No. 2");

        IF NOT Stocké THEN
            MESSAGE(ESK002, "No. 2");


        // AD Le 21-10-2011 => SYMTA -> En dur, je sais mais pas trop le choix
        IF "Manufacturer Code" <> 'LAV' THEN BEGIN
            LFrnNewHolloand := '616050-fac';
            LItemVendor.RESET();
            LItemVendor.SETRANGE("Vendor No.", LFrnNewHolloand);
            LItemVendor.SETRANGE("Item No.", "No.");
            LItemVendor.SETRANGE("Variant Code", _pVariantCode);
            IF LItemVendor.FINDFIRST() THEN
                // MC Le 08-11-2011 => Ne pas afficher le statut s'il est non Bloqu‚
                IF LItemVendor."Statut Approvisionnement" <> LItemVendor."Statut Approvisionnement"::"Non Bloqué" THEN
                    // FIN MC Le 08-11-11
                    // CFR le 22/03/2023 => R‚gie : affichage conditionn‚ au code marque NH et TRA
                    IF Rec."Manufacturer Code" IN ['NH', 'TRA'] THEN
                        // FIN CFR le 22/03/2023
                        MESSAGE(ESK003, LItemVendor."Statut Approvisionnement");
        END
    end;

    PROCEDURE GetRecommendedColor(): Text[50];
    VAR
        lShippingAgent: Record "Shipping Agent";
    BEGIN
        //CFR le 06/04/2022 => R‚gie : 50030 [Couleur transporteur pr‚conis‚] pour coloration ligne vente
        IF ("Recommended Shipping Agent" <> '') AND (lShippingAgent.GET("Recommended Shipping Agent")) THEN
            CASE lShippingAgent."Recomended Color" OF
                lShippingAgent."Recomended Color"::" ":
                    EXIT('None');
                lShippingAgent."Recomended Color"::Bleu:
                    EXIT('AttentionAccent');
                lShippingAgent."Recomended Color"::Gris:
                    EXIT('Subordinate');
                lShippingAgent."Recomended Color"::Orange:
                    EXIT('Ambiguous');
                lShippingAgent."Recomended Color"::Rouge:
                    EXIT('Attention');
                lShippingAgent."Recomended Color"::Vert:
                    EXIT('Favorable');
                lShippingAgent."Recomended Color"::"Gris":
                    EXIT('StrongAccent');
                ELSE
                    EXIT('');
            END;

        EXIT('');
    END;

    procedure "OnInsert.Eskape"()
    begin

        // ESKAPE => Traçabilité des enregistrements
        Evaluate("Create User ID", COPYSTR(USERID, MAXSTRLEN("Create User ID")));
        "Create Date" := TODAY;
        "Create Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    procedure "OnModify.Eskape"()
    begin
        // ESKAPE => Traçabilité des enregistrements

        Evaluate("Modify User ID", USERID());
        "Modify Date" := TODAY;
        "Modify Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    procedure CloturerAncienPrix(NvPrix: Record "Sales Price")
    var
        AncPrix: Record "Sales Price";
    begin

        //    MESSAGE('Item=' + NvPrix."Item No."  + ' SalCode=' + NvPrix."Sales Code" + ' StDate=' + FORMAT( NvPrix."Starting Date"));

        AncPrix.RESET();
        AncPrix.SETRANGE("Item No.", NvPrix."Item No.");
        AncPrix.SETRANGE("Sales Type", NvPrix."Sales Type");
        AncPrix.SETRANGE("Type de commande", NvPrix."Type de commande");
        AncPrix.SETRANGE("Sales Code", NvPrix."Sales Code");
        AncPrix.SETRANGE("Starting Date", 0D, CALCDATE('<-1J>', NvPrix."Starting Date"));
        AncPrix.SETRANGE("Currency Code", NvPrix."Currency Code");
        AncPrix.SETRANGE("Variant Code", NvPrix."Variant Code");
        // AD Le 26-01-2012 => Pour cloturer les prix sans unité si il  y en a
        // AncPrix.SETRANGE("Unit of Measure Code", NvPrix."Unit of Measure Code");
        AncPrix.SETFILTER("Unit of Measure Code", '%1|%2', NvPrix."Unit of Measure Code", '');
        // FIN AD Le 26-01-2012
        AncPrix.SETRANGE("Minimum Quantity", NvPrix."Minimum Quantity");
        AncPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, NvPrix."Starting Date");

        AncPrix.MODIFYALL("Ending Date", CALCDATE('<-1J>', NvPrix."Starting Date"));
    end;

    procedure ImplementerPrixAchat()
    var
        LItemVendor: Record "Item Vendor";
        LGestionTabTar: Codeunit "Gestion des tarifs Fourn. Temp";
    begin
        // Implémente les tarifs stocké dans tab_tar
        LGestionTabTar.ImplémenterTarif("No. 2", '', FALSE);

        LItemVendor.RESET();
        LItemVendor.SETCURRENTKEY("Item No.", "Variant Code", "Vendor No."); // AD Le 09-03-2016
        LItemVendor.SETRANGE("Item No.", "No.");
        LItemVendor.SETFILTER("Vendor Item No.", '<>%1', '');
        IF LItemVendor.FindSet() THEN
            REPEAT
                LGestionTabTar.ImplémenterTarif(LItemVendor."Vendor Item No.", LItemVendor."Vendor No.", FALSE);
            UNTIL LItemVendor.NEXT() = 0;
    end;

    procedure ImplementerPrixVente()
    begin
    end;

    procedure GetStockPrepa() dec_rtn: Decimal
    var
        rec_WhseShipment: Record "Warehouse Shipment Line";
        qteprepa: Decimal;
    begin
        // Fonction renvoyant le stock - la quantité sur bon de préparation.
        Rec.CALCFIELDS(Inventory);
        qteprepa := 0;
        // Calcul la qté sur bon de prépa.
        rec_WhseShipment.RESET();
        rec_WhseShipment.SETRANGE("Item No.", "No.");
        IF rec_WhseShipment.FINDSET() THEN
            REPEAT
                qteprepa += rec_WhseShipment."Qty. to Ship (Base)";
            UNTIL rec_WhseShipment.NEXT() = 0;

        dec_rtn := Rec.Inventory - qteprepa;
    end;

    procedure ReInitCriteria(_pCodeVariante: Code[10])
    var
    // Rec_ItemCriteria: Record "Article Critere";
    // Rec_FamilyCrit: Record "Famille Critere";
    // Rec_ItemCriteriaCopy: Record "Article Critere";
    // LItemVariante: Record "Item Variant";
    // "LCopiéVariante": Boolean;
    // LVarianteCopy: Code[10];
    begin
        ERROR('Fonction ne doit plus être utilisée, veuillez contacter Eskape');
        //*****************************************Code Non utiliser***********************************************
        // IF Confirm('Ré-initialisation des critères ? \ [ATTENTION] Suppression des valeurs existantes !') THEN
        //     EXIT;


        // // AD => Suppression des anciens critères -> ! Si demande de sauvegarde des valeurs, faire une propale ou me voir car chez CLEDE13 !
        // Rec_ItemCriteria.RESET();
        // Rec_ItemCriteria.SETRANGE("Code Article", "No.");
        // // AD Le 04-07-2011
        // Rec_ItemCriteria.SETRANGE("Variant Code", _pCodeVariante);
        // // FIN AD Le 04-07-2011
        // Rec_ItemCriteria.DELETEALL();

        // // AD Le 23-09-2011 =>
        // LVarianteCopy := '';
        // Rec_ItemCriteriaCopy.RESET();
        // Rec_ItemCriteriaCopy.SETRANGE("Code Article", "No.");
        // Rec_ItemCriteriaCopy.SETFILTER("Variant Code", '<>%1', _pCodeVariante);
        // IF Rec_ItemCriteriaCopy.FINDFIRST() THEN BEGIN
        //     LItemVariante.RESET();
        //     LItemVariante.SETRANGE("Item No.", "No.");
        //     LItemVariante.SETFILTER(Code, '<>%1', _pCodeVariante);
        //     IF LItemVariante.FINDLAST() THEN
        //         LVarianteCopy := LItemVariante.Code
        //     ELSE
        //         LVarianteCopy := '[VIDE]';
        //     IF LVarianteCopy <> '' THEN
        //         LCopiéVariante := CONFIRM('Vouler vous copier les valeur de la variante %1 ?', TRUE, LVarianteCopy);
        //     IF LVarianteCopy = '[VIDE]' THEN LVarianteCopy := '';
        // END;

        // // FIN AD Le 23-09-2011


        // Rec_FamilyCrit.RESET();
        // Rec_FamilyCrit.SETRANGE("Super Famille", "Super Famille Marketing");
        // Rec_FamilyCrit.SETRANGE(Famille, "Famille Marketing");
        // Rec_FamilyCrit.SETRANGE("Sous Famille", "Sous Famille Marketing");
        // IF Rec_FamilyCrit.ISEMPTY THEN
        //     Rec_FamilyCrit.SETRANGE("Sous Famille");


        // IF Rec_FamilyCrit.FindSet() THEN
        //     REPEAT
        //         Rec_ItemCriteria.INIT();

        //         IF LCopiéVariante THEN
        //             IF Rec_ItemCriteriaCopy.GET("No.", Rec_FamilyCrit."Code Critere", LVarianteCopy) THEN
        //                 Rec_ItemCriteria := Rec_ItemCriteriaCopy;

        //         Rec_ItemCriteria.VALIDATE("Code Article", "No.");
        //         Rec_ItemCriteria.VALIDATE("Code Critere", Rec_FamilyCrit."Code Critere");
        //         // AD Le 04-07-2011
        //         Rec_ItemCriteria.VALIDATE("Variant Code", _pCodeVariante);
        //         // FIN AD Le 04-07-2011

        //         Rec_ItemCriteria.VALIDATE(Priorité, Rec_FamilyCrit.Priorité);
        //         Rec_ItemCriteria.INSERT();

        //     UNTIL Rec_FamilyCrit.NEXT() = 0
        //*****************************************Code Non utiliser***********************************************
    end;

    procedure PrixTarif(_pDateTarif: Date; _pQuantity: Decimal; _pTypeCdeVente: Integer): Decimal
    var
        TempSalesPrice: Record "Sales Price" temporary;
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        LRemise: Decimal;
        LPrix: Decimal;
        LCoefficient: Decimal;
    begin
        IF _pDateTarif = 0D THEN _pDateTarif := WORKDATE();
        IF NOT LCodeunitsFunctions.GetUnitPrice(
                  TempSalesPrice, '', '', '',
                  '', "No.", '', "Base Unit of Measure", '', _pDateTarif, FALSE, _pQuantity, LRemise, LPrix,
                  '', '', _pTypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
        THEN
            LPrix := "Unit Price"

        ELSE BEGIN
            LPrix := TempSalesPrice."Unit Price";
            LCoefficient := TempSalesPrice.Coefficient;
        END;
        // FIN MC Le 25-10-2011.

        EXIT(LPrix);
    end;

    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        FrmQuid.InitArticle("No.");
        //FrmQuid.InitClient("Sell-to Customer No.");
        //FrmQuid.RUNMODAL();
        FrmQuid.RUN();
    end;

    procedure ReInitCriteriaByHierarchy(_pCodeVariante: Code[10]; bln_JustUpdate: Boolean)
    var
        Rec_ItemCriteria: Record "Article Critere";
        Rec_FamilyCrit: Record "Famille Critere";
        Rec_ItemCriteriaCopy: Record "Article Critere";
        rec_ItemHierarchy: Record "Item Hierarchies";
        LItemVariante: Record "Item Variant";
        rec_ItemHierarchy2: Record "Item Hierarchies";
        "LCopiéVariante": Boolean;
        LVarianteCopy: Code[10];
        txt_TabHierarchies: array[100] of Text;
        j: Integer;
        i: Integer;
        LOptions: Text;
        txt_TabIdHierarchies: array[100] of Text;
        bln_ItemHierachyFilter: Boolean;
        ESKCHOICE: Label 'Veuillez choisir une hierarchie';
    begin
        // Permet de ré-affecter les critères de la famille
        IF NOT bln_JustUpdate THEN
            IF NOT CONFIRM('Ré-initialisation des critères ? \ [ATTENTION] Suppression des valeurs existantes !') THEN
                EXIT;

        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie
        rec_ItemHierarchy.RESET();
        rec_ItemHierarchy.SETCURRENTKEY("By Default");
        rec_ItemHierarchy.ASCENDING(FALSE);
        rec_ItemHierarchy.SETRANGE("Code Article", "No.");
        IF rec_ItemHierarchy.ISEMPTY THEN ERROR('Aucune hierarchie pour l''article');
        IF rec_ItemHierarchy.COUNT > 1 THEN BEGIN
            // On initialise la première ligne avec l'option "Toutes"
            txt_TabHierarchies[1] := 'Toutes les hiérarchies';
            txt_TabIdHierarchies[1] := '';
            j := 1;
            rec_ItemHierarchy.FINDSET();
            REPEAT
                j += 1;
                txt_TabHierarchies[j] := STRSUBSTNO(ESK006, rec_ItemHierarchy."Super Famille Marketing", rec_ItemHierarchy."Famille Marketing", rec_ItemHierarchy."Sous Famille Marketing");
                rec_ItemHierarchy2 := rec_ItemHierarchy;
                rec_ItemHierarchy2.SETRECFILTER();
                txt_TabIdHierarchies[j] := rec_ItemHierarchy2.GETVIEW();
            UNTIL rec_ItemHierarchy.NEXT() = 0;

            // COnstruction du stroptions
            IF j > 1 THEN BEGIN
                FOR i := 1 TO j DO
                    IF LOptions = '' THEN
                        LOptions := txt_TabHierarchies[i]
                    ELSE
                        LOptions := LOptions + ',' + txt_TabHierarchies[i];

                // L'utilisateur choisit la commande
                MESSAGE(ESKCHOICE);
                i := STRMENU(LOptions, 1);
                IF i = 0 THEN
                    ERROR('Traitement annulé');

                // Si i est supérieur à 1 alors une hierarchie a été choisit
                IF i > 1 THEN BEGIN
                    bln_ItemHierachyFilter := TRUE;
                    rec_ItemHierarchy.SETVIEW(txt_TabIdHierarchies[i]);
                    rec_ItemHierarchy.FINDFIRST();
                END;
            END;
        END;

        // AD => Suppression des anciens critères -> ! Si demande de sauvegarde des valeurs, faire une propale ou me voir car chez CLEDE13 !
        Rec_ItemCriteria.RESET();
        Rec_ItemCriteria.SETRANGE("Code Article", "No.");
        // AD Le 04-07-2011
        Rec_ItemCriteria.SETRANGE("Variant Code", _pCodeVariante);
        // FIN AD Le 04-07-2011

        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie
        IF bln_ItemHierachyFilter THEN BEGIN
            Rec_ItemCriteria.SETRANGE("Super Famille Marketing", rec_ItemHierarchy."Super Famille Marketing");
            Rec_ItemCriteria.SETRANGE("Famille Marketing", rec_ItemHierarchy."Famille Marketing");
            Rec_ItemCriteria.SETRANGE("Sous Famille Marketing", rec_ItemHierarchy."Sous Famille Marketing");
        END;
        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie

        IF NOT bln_JustUpdate THEN
            Rec_ItemCriteria.DELETEALL();

        // AD Le 23-09-2011 =>
        LVarianteCopy := '';
        Rec_ItemCriteriaCopy.RESET();
        Rec_ItemCriteriaCopy.SETRANGE("Code Article", "No.");
        Rec_ItemCriteriaCopy.SETFILTER("Variant Code", '<>%1', _pCodeVariante);
        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie
        IF bln_ItemHierachyFilter THEN BEGIN
            Rec_ItemCriteriaCopy.SETRANGE("Super Famille Marketing", rec_ItemHierarchy."Super Famille Marketing");
            Rec_ItemCriteriaCopy.SETRANGE("Famille Marketing", rec_ItemHierarchy."Famille Marketing");
            Rec_ItemCriteriaCopy.SETRANGE("Sous Famille Marketing", rec_ItemHierarchy."Sous Famille Marketing");
        END;
        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie
        IF Rec_ItemCriteriaCopy.FINDFIRST() THEN BEGIN
            LItemVariante.RESET();
            LItemVariante.SETRANGE("Item No.", "No.");
            LItemVariante.SETFILTER(Code, '<>%1', _pCodeVariante);
            IF LItemVariante.FINDLAST() THEN
                LVarianteCopy := LItemVariante.Code
            ELSE
                LVarianteCopy := '[VIDE]';
            IF LVarianteCopy <> '' THEN
                LCopiéVariante := CONFIRM('Vouler vous copier les valeur de la variante %1 ?', TRUE, LVarianteCopy);
            IF LVarianteCopy = '[VIDE]' THEN LVarianteCopy := '';
        END;

        // FIN AD Le 23-09-2011


        // MCO Le 16-01-2018 => Mise en place de hierarchies multiples : on demande si l'utilisateur veut tout réinitialiser ou qu'une seule hiérarchie
        //MESSAGE('%1',rec_ItemHierarchy.COUNT);
        //ERROR(rec_ItemHierarchy.GETFILTERS);

        // Je ne sais pas pourquoi les filtres sont réinitialisés
        IF NOT bln_ItemHierachyFilter THEN BEGIN
            rec_ItemHierarchy.RESET();
            rec_ItemHierarchy.SETCURRENTKEY("By Default");
            rec_ItemHierarchy.ASCENDING(FALSE);
            rec_ItemHierarchy.SETRANGE("Code Article", "No.");
        END;


        rec_ItemHierarchy.FINDSET();
        REPEAT
            Rec_FamilyCrit.RESET();
            Rec_FamilyCrit.SETRANGE("Super Famille", rec_ItemHierarchy."Super Famille Marketing");
            Rec_FamilyCrit.SETRANGE(Famille, rec_ItemHierarchy."Famille Marketing");
            Rec_FamilyCrit.SETRANGE("Sous Famille", rec_ItemHierarchy."Sous Famille Marketing");
            IF Rec_FamilyCrit.ISEMPTY THEN
                Rec_FamilyCrit.SETRANGE("Sous Famille");


            IF Rec_FamilyCrit.FINDSET() THEN
                REPEAT
                    Rec_ItemCriteria.INIT();

                    IF LCopiéVariante THEN
                        IF Rec_ItemCriteriaCopy.GET("No.", Rec_FamilyCrit."Code Critere", LVarianteCopy) THEN
                            Rec_ItemCriteria := Rec_ItemCriteriaCopy;

                    Rec_ItemCriteria.VALIDATE("Code Article", "No.");
                    Rec_ItemCriteria.VALIDATE("Code Critere", Rec_FamilyCrit."Code Critere");
                    // AD Le 04-07-2011
                    Rec_ItemCriteria.VALIDATE("Variant Code", _pCodeVariante);
                    // FIN AD Le 04-07-2011

                    Rec_ItemCriteria.VALIDATE(Priorité, Rec_FamilyCrit.Priorité);
                    // MCO Le 16-01-2018 => MultiHierarchie
                    Rec_ItemCriteria."Super Famille Marketing" := Rec_FamilyCrit."Super Famille";
                    Rec_ItemCriteria."Famille Marketing" := Rec_FamilyCrit.Famille;
                    Rec_ItemCriteria."Sous Famille Marketing" := Rec_FamilyCrit."Sous Famille";

                    IF Rec_ItemCriteria.INSERT() THEN;
                // FIN MCO Le 16-01-2018

                UNTIL Rec_FamilyCrit.NEXT() = 0
        UNTIL rec_ItemHierarchy.NEXT() = 0;
    end;

    procedure GetCodeFamille(): Code[20]
    var
        lItemCategory: Record "Item Category";
    begin

        if lItemCategory.Get(Rec."Item Category Code") then
            if lItemCategory."Parent Category" <> '' then
                exit(lItemCategory."Parent Category");

        exit('');
    end;

    var

        CduFunctions: Codeunit Functions;
        // Text50001: Label 'IMPOSSIBLE ! Le Gencode %1 est affecté à l''article %2.';
        Text50010: Label 'You can''t change this field if %1 or %2 are zero.';
        Text50011: Label 'Impossible car il existe des documents contenant cet article !';
        ESK001: Label 'L''article %1 n''est fourni par aucun fournisseur !';
        ESK002: Label 'L''article %1 n''est pas stocké !';
        ESK003: Label 'Status appovisionnement NEW HOLLAND : %1 !';
        ESK006: Label '%1 - %2 - %3';
}

