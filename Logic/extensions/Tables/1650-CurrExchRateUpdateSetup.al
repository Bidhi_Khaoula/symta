tableextension 50327 "Curr. Exch. Rate Update Setup" extends "Curr. Exch. Rate Update Setup" //1650
{
    procedure ShowHttpError()
    var
        WebRequestHelper: Codeunit "Web Request Helper";
        WebException: DotNet WebException;
        ErrorText: Text;
        ServiceURL: Text;
        WebServiceURL: Text;
        XmlStructureIsNotSupportedErr: Label ' The provided url does not contain a supported structure.';
    begin
        WebServiceURL := Rec.GetWebServiceURL(ServiceURL);
        ErrorText := WebRequestHelper.GetWebResponseError(WebException, WebServiceURL);


        if IsNull(WebException.Response) then
            exit;

        ErrorText := XmlStructureIsNotSupportedErr;
        Message(ErrorText);
    end;


}