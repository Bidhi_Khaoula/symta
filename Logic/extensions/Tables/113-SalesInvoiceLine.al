tableextension 50310 "Sales Invoice Line Logic" extends "Sales Invoice Line" //113
{
    fields
    {
        modify("RV Defective Location")
        {
            Trigger OnAfterValidate()
            BEGIN
                IF (Type <> Type::Item) THEN
                    ERROR('Saisie possible uniquement sur les lignes articles');
            END;
        }
    }

    procedure "Calcule Marge"(): Decimal
    begin
        IF "Net Unit Price" <> 0 THEN
            EXIT(
              ROUND(
                100 * (1 - "Unit Cost" / "Net Unit Price"), 0.01))
        ELSE
            EXIT(0);
    end;

    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        IF Type <> Type::Item THEN EXIT;

        FrmQuid.InitArticle("No.");
        FrmQuid.InitClient("Sell-to Customer No.");
        // AD Le 23-03-2016
        FrmQuid.RUN(); //FrmQuid.RUNMODAL();
    end;
}

