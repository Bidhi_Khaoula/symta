tableextension 50645 "Comment Line Logic" extends "Comment Line" //97
{
    fields
    {
        modify("Afficher Multiconsultation")
        {
            trigger OnBeforeValidate()
            begin
                TESTFIELD("Table Name", "Table Name"::Item);
            end;
        }
        modify("Afficher Appro")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
        modify("No. 2")
        {
            trigger OnAfterValidate()
            begin
            end;
        }
        modify("Print Wharehouse Shipment")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
        modify("Display Sales Order")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
        modify("Display Purch. Order")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
        modify("Print Purch. Order")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
        modify("Display Purch. Receipt")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
                IF "Table Name" = "Table Name"::"VAT Business Posting Group" THEN
                    FIELDERROR("Table Name");
                // FIN MC Le 21-01-2014
            end;
        }
    }

}

