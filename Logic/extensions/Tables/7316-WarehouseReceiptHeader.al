tableextension 50421 "Warehouse Receipt Header Logic" extends "Warehouse Receipt Header" //7316
{
    fields
    {

        modify("Date Arrivage Marchandise")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 30-01-2020 => REGIE
                IF "Date Arrivage Marchandise" <> 0D THEN
                    VALIDATE(Position, Position::"En magasin");
            end;
        }
        modify("N° Fusion Réception")
        {
            trigger OnAfterValidate()
            begin
                WhseRcptLine.RESET();
                WhseRcptLine.SETRANGE("No.", "No.");
                IF WhseRcptLine.FINDFIRST() THEN
                    REPEAT
                        WhseRcptLine.VALIDATE("N° Fusion Réception", "N° Fusion Réception");
                        WhseRcptLine.MODIFY(TRUE);
                    UNTIL WhseRcptLine.NEXT() = 0;

                // Mise à jour du booléen indiquant que c'est une fusion, sauf sur la fusion principale.
                IF "N° Fusion Réception" = '' THEN
                    Fusion := FALSE
                ELSE
                    IF ("N° Fusion Réception" <> "No.") THEN
                        Fusion := TRUE
                    ELSE
                        Fusion := FALSE;
            end;
        }
    }
    procedure GetOrderCurency(): Code[10]
    var
        WarehouseReceiptLine: Record "Warehouse Receipt Line";
        PurchHeader: Record "Purchase Header";
    begin
        // Retourne na devise de la première les ligne de commande
        WarehouseReceiptLine.RESET();
        WarehouseReceiptLine.SETRANGE("No.", "No.");
        WarehouseReceiptLine.SETRANGE("Source Type", 39);
        WarehouseReceiptLine.SETRANGE("Source Subtype", 1);
        IF WarehouseReceiptLine.FINDFIRST() THEN BEGIN
            PurchHeader.GET(PurchHeader."Document Type"::Order, WarehouseReceiptLine."Source No.");
            EXIT(PurchHeader."Currency Code");


        END;
    end;

    procedure GetVendorNo(): Code[20]
    var
        WarehouseReceiptLine: Record "Warehouse Receipt Line";
        PurchHeader: Record "Purchase Header";
    begin
        // Retourne le fournisseur de la première les ligne de commande

        WarehouseReceiptLine.RESET();
        WarehouseReceiptLine.SETRANGE("No.", "No.");
        WarehouseReceiptLine.SETRANGE("Source Type", 39);
        WarehouseReceiptLine.SETRANGE("Source Subtype", 1);
        IF WarehouseReceiptLine.FINDFIRST() THEN BEGIN
            PurchHeader.GET(PurchHeader."Document Type"::Order, WarehouseReceiptLine."Source No.");
            EXIT(PurchHeader."Buy-from Vendor No.");
        END;
    end;

    procedure GetVendorName(): Code[100]
    var
        WarehouseReceiptLine: Record "Warehouse Receipt Line";
        PurchHeader: Record "Purchase Header";
    begin
        // Retourne le fournisseur de la première les ligne de commande

        WarehouseReceiptLine.RESET();
        WarehouseReceiptLine.SETRANGE("No.", "No.");
        WarehouseReceiptLine.SETRANGE("Source Type", 39);
        WarehouseReceiptLine.SETRANGE("Source Subtype", 1);
        IF WarehouseReceiptLine.FINDFIRST() THEN BEGIN
            PurchHeader.GET(PurchHeader."Document Type"::Order, WarehouseReceiptLine."Source No.");
            EXIT(PurchHeader."Buy-from Vendor Name");
        END;
    end;

    procedure GetAmount() dec_amount: Decimal
    var
        rec_WhseRcptLine: Record "Warehouse Receipt Line";
    // rec_PurchLine: Record "Purchase Line";
    begin
        //** Cette fonction calcule le montant de la réception à partir des lignes de commande achat **//

        // Initialisation des variables de retour
        dec_amount := 0;

        rec_WhseRcptLine.RESET();
        rec_WhseRcptLine.SETRANGE("No.", "No.");
        IF rec_WhseRcptLine.FINDSET() THEN
            REPEAT
                // CFR le 22/03/2023 - R‚gie : homog‚n‚isation des montants entre lignes et en-tˆte
                /*
                                rec_PurchLine.RESET();
                                rec_PurchLine.SETRANGE("Document Type", rec_PurchLine."Document Type"::Order);
                                rec_PurchLine.SETRANGE("Document No.", rec_WhseRcptLine."Source No.");
                                rec_PurchLine.SETRANGE("Line No.", rec_WhseRcptLine."Source Line No.");
                                IF rec_PurchLine.FINDFIRST () THEN
                                    dec_amount += (rec_PurchLine."Line Amount" / rec_PurchLine.Quantity) * rec_WhseRcptLine."Qty. to Receive";
                           */
                dec_amount += rec_WhseRcptLine.GetInfoLigne('TOTALNET2');
            // FIN CFR le 22/03/2023
            UNTIL rec_WhseRcptLine.NEXT() = 0;
    end;

    procedure DeleteInfoOnFacture()
    var
        RecLWhseRcptLine: Record "Warehouse Receipt Line";
        FactureFour: Record "Import facture tempo. fourn.";
        FactureFourHeader: Record "Import facture tempo. fourn.";
    begin
        // Appelé lors de la suppresion de l'entete
        WITH RecLWhseRcptLine DO BEGIN
            RESET();
            SETRANGE("No.", Rec."No.");
            IF FINDSET() THEN
                REPEAT
                    IF "N° sequence facture fourn." <> 0 THEN BEGIN
                        //IF FactureFour.GET("N° sequence facture fourn.",FactureFour."Type ligne"::Ligne) THEN
                        // BEGIN
                        // Vu que maintenant deux lignes de factures fourn peuvent être egale à une ligne de réception il faut chercher
                        FactureFour.RESET();
                        FactureFour.SETRANGE("No de reception", "No.");
                        FactureFour.SETRANGE("No ligne reception", "Line No.");
                        IF FactureFour.FINDSET() THEN
                            REPEAT
                                FactureFour."date intégration" := 0D;
                                FactureFour."heure intégration" := 0T;
                                FactureFour."No de reception" := '';
                                FactureFour."No ligne reception" := 0;
                                FactureFour.MODIFY();
                            UNTIL FactureFour.NEXT() = 0;


                        FactureFourHeader.RESET();
                        FactureFourHeader.SETRANGE("Code fournisseur", FactureFour."Code fournisseur");
                        FactureFourHeader.SETRANGE("Type ligne", FactureFour."Type ligne"::Entete);
                        FactureFourHeader.SETRANGE("No Facture", FactureFour."No Facture");
                        IF FactureFourHeader.FINDFIRST() THEN BEGIN
                            FactureFourHeader."date intégration" := 0D;
                            FactureFourHeader."heure intégration" := 0T;
                            FactureFourHeader."No de reception" := '';
                            FactureFourHeader."No ligne reception" := 0;
                            FactureFourHeader.MODIFY();
                        END;
                    END;

                UNTIL NEXT() = 0;
        END;
    end;

    var
        WhseRcptLine: Record "Warehouse Receipt Line";
}

