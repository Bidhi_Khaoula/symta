tableextension 50424 "Posted Whse.Receipt Line Logic" extends "Posted Whse. Receipt Line" //7319
{
    PROCEDURE SetPartner();
    VAR
        lPurchRcptHeader: Record "Purch. Rcpt. Header";
        lReturnReceiptHeader: Record "Return Receipt Header";
    BEGIN
        //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [50060] et [50061]
        Rec."Source Partner No." := '';
        Rec."Source Partner Name" := '';

        IF Rec."Posted Source No." = '' THEN EXIT;

        CASE "Source Type" OF
            37:

                IF lReturnReceiptHeader.GET(Rec."Posted Source No.") THEN BEGIN
                    Rec."Source Partner No." := lReturnReceiptHeader."Sell-to Customer No.";
                    Rec."Source Partner Name" := lReturnReceiptHeader."Sell-to Customer Name";
                END;
            39:
                IF lPurchRcptHeader.GET(Rec."Posted Source No.") THEN BEGIN
                    Rec."Source Partner No." := lPurchRcptHeader."Buy-from Vendor No.";
                    Rec."Source Partner Name" := lPurchRcptHeader."Buy-from Vendor Name";
                END;
        END;
    END;

    PROCEDURE GetInfoLigneTexte(_pTypeInfo: Code[20]): Text;
    VAR
        LligneAchat: Record "Purchase Line";
    BEGIN
        // MCO Le 24-03-2016 => R‚gie
        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT('');

        CASE _pTypeInfo OF
            'DESC':
                EXIT(LligneAchat.Description);
        END;
        // MCO Le 24-03-2016 => R‚gie
    END;
}

