tableextension 50067 "Job Queue Log Entry Logic" extends "Job Queue Log Entry" //474
{

    procedure SendEmail()
    var
        LCompanyInfo: Record "Company Information";
        LEmail: Codeunit Mail;
        LSujet: Text[1024];
    begin
        IF Status <> Status::Error THEN
            EXIT;

        LCompanyInfo.GET();

        Evaluate(LSujet, 'Objet : ' + FORMAT("Object Type to Run") + ' ' + FORMAT("Object ID to Run") + '-' + "Object Caption to Run" + '<BR>' +
          "Error Message" + '<BR>');
        // +"Error Message 2" + '<BR>' +
        //"Error Message 3" + '<BR>' +
        //"Error Message 4" + '<BR>';

        LEmail.CreateMessage(LCompanyInfo."Email Admin File Projet", 'adagois@eskape.fr', 'erp-support@eskape.fr',
            'Echec exécution file projet' + LCompanyInfo.Name, LSujet, True, TRUE);
        LEmail.Send();
    end;
}

