tableextension 50307 "Sales Shipment Header Logic" extends "Sales Shipment Header" //110
{

    fields
    {
        modify("Invoice Customer No.")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 07-09-2009 => FARGROUP -> Client Facturé
            end;
        }
        modify("En attente de facturation")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 22/09/2021 => Régie - Suivre l'utilisateur qui a mis en attente
                IF "En attente de facturation" THEN
                    VALIDATE("User En Attente", USERID)
                ELSE
                    VALIDATE("User En Attente", '');
            end;
        }

    }

    procedure "RécupérerFacturationCentral"()
    var
        Cust: Record Customer;
        ligneExp: Record "Sales Shipment Line";
    begin

        ligneExp.RESET();
        ligneExp.SETRANGE("Document No.", "No.");
        ligneExp.SETRANGE("Type", ligneExp."Type"::Item);
        ligneExp.SETFILTER(Quantity, '>0');
        ligneExp.SETRANGE("Gerer par groupement", TRUE);
        IF ligneExp.ISEMPTY THEN EXIT;




        // MC LE 04-05-2011 => On identifie le BL correspondant au regroupement centrale.
        "Regroupement Centrale" := TRUE;

        Cust.GET("Centrale Active");
        "Payment Terms Code" := Cust."Payment Terms Code";

        MODIFY();

        IF NOT "Groupement payeur" THEN EXIT;

        // SI le groupement est payeur, récupération des infos pour la facturation

        "Bill-to Customer No." := Cust."No.";
        "Bill-to Name" := Cust.Name;
        "Bill-to Name 2" := Cust."Name 2";
        "Bill-to Address" := Cust.Address;
        "Bill-to Address 2" := Cust."Address 2";
        "Bill-to City" := Cust.City;
        "Bill-to Post Code" := Cust."Post Code";
        "Bill-to County" := Cust.County;
        "Bill-to Country/Region Code" := Cust."Country/Region Code";
        "VAT Country/Region Code" := Cust."Country/Region Code";

        // AD Le 26-01-2012 => Pour un grpt payeur le mode de règlement est celui de la centrale
        "Payment Method Code" := Cust."Payment Method Code";
        // FIN AD Le 26-01-2012

        // AD Le 19-12-2011 =>
        IF "Payment Terms Code 2" <> '' THEN
            "Payment Method Code" := Cust."Payment Method Code";
        // FIN AD Le 19-12-2011
        MODIFY();

        ligneExp.RESET();
        ligneExp.SETRANGE("Document No.", "No.");
        IF ligneExp.FINDFIRST() THEN
            ligneExp.MODIFYALL("Bill-to Customer No.", "Centrale Active");
    end;

    procedure "MajEntierementFacturé"()
    begin
        CALCFIELDS("Somme Qté livrée non facturée");
        "Entierement facturé" := ("Somme Qté livrée non facturée" = 0);
        MODIFY();
    end;

    procedure GetAmount() dec_rtn: Decimal
    var
        _recSalesSipmentLine: Record "Sales Shipment Line";
    begin
        //** Cette fonction retourne le montant total du BL **//
        dec_rtn := 0;
        CLEAR(_recSalesSipmentLine);
        _recSalesSipmentLine.SETRANGE("Document No.", "No.");
        IF _recSalesSipmentLine.FindSet() THEN
            REPEAT
                dec_rtn += (_recSalesSipmentLine."Net Unit Price" * _recSalesSipmentLine.Quantity);
            UNTIL _recSalesSipmentLine.NEXT() = 0;
    end;

    procedure GetPortAmount() dec_rtn: Decimal
    var
        _recSalesSipmentLine: Record "Sales Shipment Line";
        "cust post group": Record "Customer Posting Group";
    begin
        //** Cette fonction retourne le montant total des lignes de port du BL **//
        dec_rtn := 0;
        IF "cust post group".GET("Customer Posting Group") THEN BEGIN
            CLEAR(_recSalesSipmentLine);
            _recSalesSipmentLine.SETRANGE("Document No.", "No.");
            _recSalesSipmentLine.SETRANGE("No.", "cust post group"."Shipment Account");
            // AD Le 05-12
            _recSalesSipmentLine.SETRANGE("Internal Line Type", _recSalesSipmentLine."Internal Line Type"::Shipment);
            IF _recSalesSipmentLine.FindSet() THEN
                REPEAT
                    dec_rtn += (_recSalesSipmentLine."Net Unit Price" * _recSalesSipmentLine.Quantity);
                UNTIL _recSalesSipmentLine.NEXT() = 0;
        END;
    end;

    procedure IsReliquat() bln_rtn: Boolean
    var
        rec_SalesLines: Record "Sales Line";
        _recSalesSipmentLine: Record "Sales Shipment Line";
    begin
        //** Cette fonction retourne un boolean indiquant si le BL est un reliquat ou non **//
        CLEAR(_recSalesSipmentLine);
        _recSalesSipmentLine.SETRANGE("Document No.", "No.");
        IF _recSalesSipmentLine.FindSet() THEN
            REPEAT
                IF rec_SalesLines.GET(rec_SalesLines."Document Type"::Order, _recSalesSipmentLine."Order No.",
                                     _recSalesSipmentLine."Order Line No.") THEN
                    bln_rtn := rec_SalesLines.Quantity <> _recSalesSipmentLine.Quantity;


            UNTIL (_recSalesSipmentLine.NEXT() = 0) OR (bln_rtn);
    end;

    procedure GetTheoWeight() dec_rtn: Decimal
    var
        _recSalesSipmentLine: Record "Sales Shipment Line";
    begin
        //** Cette fonction retourne le poids théorique du BL **//
        dec_rtn := 0;
        CLEAR(_recSalesSipmentLine);
        _recSalesSipmentLine.SETRANGE("Document No.", "No.");
        IF _recSalesSipmentLine.FindSet() THEN
            REPEAT
                dec_rtn += _recSalesSipmentLine."Net Weight";
            UNTIL _recSalesSipmentLine.NEXT() = 0;
    end;

    procedure SendRecords(ShowRequestForm: Boolean; SendAsEmail: Boolean)
    var
        ReportSelection: Record "Report Selections";
    begin

        WITH SalesShptHeader DO BEGIN
            COPY(Rec);
            ReportSelection.SETRANGE(Usage, ReportSelection.Usage::"S.Shipment");
            ReportSelection.SETFILTER("Report ID", '<>0');
            ReportSelection.FIND('-');
            REPEAT
                IF NOT SendAsEmail THEN
                    REPORT.RUNMODAL(ReportSelection."Report ID", ShowRequestForm, FALSE, SalesShptHeader)
                ELSE
                    SendReport(ReportSelection."Report ID", SalesShptHeader)

            UNTIL ReportSelection.NEXT() = 0;
        END;
    end;

    local procedure SendReport(ReportId: Integer; var PSalesShptHeader: Record "Sales Shipment Header")
    var
        FileManagement: Codeunit "File Management";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        ServerAttachmentFilePath: Text[250];
    begin
        ServerAttachmentFilePath := COPYSTR(FileManagement.ServerTempFileName('pdf'), 1, 250);
        REPORT.SAVEASPDF(ReportId, ServerAttachmentFilePath, PSalesShptHeader);
        COMMIT();
        LCodeunitsFunctions.EmailFileFromSalesShipmentHeader(PSalesShptHeader, ServerAttachmentFilePath);
    end;

    procedure CreerIncidentTransport()
    var
        LGestionLitige: Codeunit "Gestion des litiges";
    begin
        LGestionLitige.CréerIncidentTransport('', "No.");
    end;

    procedure ShowOrder()
    var
        SalesHeader: Record "Sales Header";
        SalesHeaderArchive: Record "Sales Header Archive";
    begin
        // MCO Le 21-11-2018 => Régie
        IF SalesHeader.GET(SalesHeader."Document Type"::Order, "Order No.") THEN
            PAGE.RUN(PAGE::"Sales Order", SalesHeader)
        ELSE BEGIN
            SalesHeaderArchive.RESET();
            SalesHeaderArchive.SETRANGE("Document Type", SalesHeaderArchive."Document Type"::Order);
            SalesHeaderArchive.SETRANGE("No.", "Order No.");
            IF SalesHeaderArchive.FINDLAST() THEN
                PAGE.RUN(PAGE::"Sales Order Archive", SalesHeaderArchive);
        END;
    end;

    PROCEDURE GetPreparateurName(): Text[50];
    VAR
        lGeneralsParameters: Record "Generals Parameters";
    BEGIN
        // CFR le 05/04/2023 - R‚gie : Affichage des champs nom preparateur, heure pr‚paration
        IF lGeneralsParameters.GET('MAG_EMPLOYES', Rec.Preparateur) THEN
            EXIT(lGeneralsParameters.LongDescription)
        ELSE
            EXIT('');
    END;

    var
        SalesShptHeader: Record "Sales Shipment Header";
}

