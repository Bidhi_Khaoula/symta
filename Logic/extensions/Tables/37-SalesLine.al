tableextension 50358 "Sales Line Logic" extends "Sales Line" //37
{
    fields
    {
        modify("Unit Cost")
        {
            trigger OnBeforeValidate()
            BEGIN
                // CFR le 04/10/2023 - Régie ajout du calcul de marge
                Rec.CalcMarge();
            END;

        }
        modify("Initial Quantity")
        {
            trigger OnAfterValidate()
            begin
            end;
        }

        modify("Exclure RFA")
        {
            Caption = 'Exclure RFA';
            Description = 'MC Le 17-05-2010 =>Analyse complémentaire FARGROUP';

            trigger OnBeforeValidate()
            begin
                IF Type = Type::Item THEN
                    TESTFIELD("No.");
            end;
        }

        modify("Line type")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 21-10-2009 => FARGROUP Demande de Pierre -> Ligne SAV et réparation Tjs Sans Mvt de stock
                IF Type = Type::Item THEN
                    // AD Le 01-04-2015 => Pas chez SYMTA
                    //IF "Line type" IN ["Line type"::SAV, "Line type"::Réparation] THEN
                    //  VALIDATE("Sans mouvement de stock", TRUE);
                    // FIN AD Le 01-04-2015
                    IF "Line type" IN ["Line type"::Gratuit, "Line type"::Echange] THEN
                        VALIDATE("Unit Price", 0);
                // FIN AD Le 21-10-2009
            end;
        }
        modify("Campaign No.")
        {
            trigger OnAfterValidate()
            begin
            end;
        }
        modify("Sans mouvement de stock")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 17-09-2009 => Non Stocké
                TESTFIELD("Quantity Shipped", 0);

                // AD Le 21-10-2009 => FARGROUP Demande de Pierre -> Si sans Mvt de stock, cout à 0
                IF "Sans mouvement de stock" THEN
                    VALIDATE("Unit Cost (LCY)", 0)
                ELSE
                    GetUnitCost();
                // FIN AD Le 21-10-2009
            end;
        }
        modify(Urgent)
        {
            trigger OnAfterValidate()
            var
            // lRemiseOrder: Decimal;
            // lRemiseExpress: Decimal;
            // lCoefficient: Decimal;
            begin
                /*
                  // CFR le 18/11/2020 : Régie > remises express
                  ExpressDiscount(lRemiseExpress, lRemiseOrder, lCoefficient, TRUE);
                  IF (lRemiseExpress = 0) OR (lRemiseExpress = lRemiseOrder) THEN BEGIN
                    IF Rec.Urgent THEN
                      Rec.Urgent := FALSE;
                  END
                  ELSE BEGIN
                    IF (Rec.Urgent) THEN BEGIN
                      Rec.VALIDATE("Discount1 %", lRemiseExpress);
                      Rec.VALIDATE("Requested Delivery Date", CALCDATE('3J', Rec."Requested Delivery Date"));
                    END
                    ELSE BEGIN
                      Rec.VALIDATE("Discount1 %", lRemiseOrder);
                      Rec.VALIDATE("Requested Delivery Date", CALCDATE('-3J', Rec."Requested Delivery Date"));
                    END;
                  END;
                  // FIN CFR le 18/11/2020
                */

            end;
        }

        modify("Recherche référence")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
                LItemCrossRef: Record "Item Reference";
                NoRef: Code[30];
                txt_RefSaisie: Code[40];
            begin
                // MC Le 31-01-2013 => Statut doit être ouvert
                TestStatusOpen();
                // FIN MC Le 31-01-2013

                // MC Le 06-06-2011 => SYMTA -> Stocke la référence saisie.
                txt_RefSaisie := "Recherche référence";
                // FIN MC Le 06-06-2011

                // AD Le 11-12-2009 => GDI -> Recherche de la référence
                IF Type <> Type::Item THEN
                    VALIDATE("No.", "Recherche référence")
                ELSE BEGIN
                    NoRef := GestionMultiReference.RechercheMultiReference("Recherche référence");
                    IF NoRef <> 'RIENTROUVE' THEN
                        VALIDATE("No.", NoRef)
                    ELSE
                        IF STRPOS("Recherche référence", '$') = 0 THEN BEGIN
                            // AD Le 29-01-2020
                            // => CODE AVANT
                            LItem.RESET();
                            LItem.SETCURRENTKEY("No. 2");
                            LItem.SETFILTER("No. 2", "Recherche référence" + '*');
                            IF PAGE.RUNMODAL(PAGE::"Item List", LItem) = ACTION::LookupOK THEN BEGIN
                                CLEAR(Description);
                                CLEAR("No.");
                                "Recherche référence" := LItem."No. 2";
                                VALIDATE("No.", LItem."No.");
                                // MC Le 06-06-2011 => SYMTA -> Stocke la référence saisie.
                                "Référence saisie" := LItem."No. 2";
                                // FIN MC Le 06-06-2011

                            END
                            ELSE BEGIN
                                CLEAR(Description);
                                CLEAR("No.");
                                "Recherche référence" := '';
                            END;
                            // FIN AD Le 29-01-2020
                            //<== CODE AVANT
                        END ELSE BEGIN
                            CLEAR(LItemCrossRef);
                            "Recherche référence" := CONVERTSTR("Recherche référence", '$', '*');
                            LItemCrossRef.SETFILTER("Reference No.", "Recherche référence");
                            IF PAGE.RUNMODAL(PAGE::"Item Reference List", LItemCrossRef) = ACTION::LookupOK THEN BEGIN
                                CLEAR(Description);
                                CLEAR("No.");
                                LItem.GET(LItemCrossRef."Item No.");
                                "Recherche référence" := LItem."No. 2";
                                VALIDATE("No.", LItemCrossRef."Item No.");
                                // MC Le 06-06-2011 => SYMTA -> Stocke la référence saisie.
                                "Référence saisie" := LItemCrossRef."Reference No.";
                                // FIN MC Le 06-06-2011

                            END
                            ELSE BEGIN
                                CLEAR(Description);
                                CLEAR("No.");
                                "Recherche référence" := '';
                            END;


                        END;
                END;
                // AD Le 11-12-2009

                // AD Le 21-03-2013 => Pour ne pas avoir de reference vide
                TESTFIELD("Recherche référence");
                // FIN AD Le 21-03-2013

                // MC Le 06-06-2011 => SYMTA -> Stocke la référence saisie.
                IF "Référence saisie" = '' THEN
                    "Référence saisie" := txt_RefSaisie;
                // FIN MC Le 06-06-2011
            end;
        }
        modify("RV Defective Location")
        {
            Trigger OnAfterValidate()
            BEGIN
                IF (Type <> Type::Item) THEN
                    ERROR('Saisie possible uniquement sur les lignes articles');
            END;
        }

        modify("Discount1 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Discount2 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Net Unit Price")
        {
            trigger OnAfterValidate()
            var
                Currency: Record Currency;
            begin
                Currency.Get(rec."Currency Code");
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                IF "Line Discount %" <> 0 THEN BEGIN
                    "Net Unit Price" := "Unit Price" - ("Unit Price" * "Line Discount %") / 100;
                    "Net Unit Price" := ROUND("Net Unit Price", Currency."Amount Rounding Precision")
                END
                ELSE
                    "Net Unit Price" := ROUND("Unit Price", Currency."Amount Rounding Precision");
            end;
        }
        modify("Opération ADV")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 24-09-2009 => FARGROUP -> Calcul des comissions
                GetComission();
                // FIN AD Le 24-09-2009
            end;
        }
        modify("Header Salesperson Code")
        {
            trigger OnAfterValidate()
            var
            // ApprovalEntry: Record "Approval Entry";
            begin
            end;
        }

        modify("Qty To Receive Loans")
        {
            trigger OnAfterValidate()
            var
                Text50003Err: Label 'Vous ne pouvez retourner plus que la quantité prêtée non facturée.';
            begin
                //PRET  Contrôle sur la saisie de la quantité à recevoir
                IF "Qty To Receive Loans" > "Qty. Shipped Not Invoiced" THEN
                    ERROR(Text50003Err);
                "Qty To Receive Loans(Base)" := "Qty. to Ship" * "Qty. per Unit of Measure";
                VALIDATE("Qty. to Invoice", "Qty. Shipped Not Invoiced" - "Qty To Receive Loans");
            end;
        }
    }

    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
        Disc1 := 1 - ("Discount1 %" / 100);
        Disc2 := 1 - ("Discount2 %" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;

    procedure "ValidateNo.Eskape"()
    var
        Item: Record Item;
        SalesHeader: Record "Sales Header";
        SalesSetup: Record "Sales & Receivables Setup";
        lIUOM: Record "Item Unit of Measure";
        StdTxt: Record "Standard Text";
        CommentLine: Record "Comment Line";
        "Item Substitution": Record "Item Substitution";
        FrmCommentLine: Page "Comment Sheet";
        TxtSubstitution: Text[1024];
    begin
        CASE Type OF
            Type::" ":
                BEGIN
                    StdTxt.GET("No.");

                    // AD Le 09-10-2008 => COMMENTAIRE -> Pour choisir ou s'intégre le code texte standard
                    "Print Wharehouse Shipment" := StdTxt."Print Wharehouse Shipment";
                    "Print Shipment" := StdTxt."Print Shipment";
                    "Print Invoice" := StdTxt."Print Invoice";
                    "Print Order" := StdTxt."Print Order";
                    "Print Quote" := StdTxt."Print Quote";
                    // FIN AD Le 09-10-2008
                END;
            Type::Item:
                BEGIN
                    GetItem(Item);
                    // AD Le 26-08-2009 => Gestion avancé du bloque
                    IF Item."Process Blocked" IN [Item."Process Blocked"::Ventes, Item."Process Blocked"::"Ventes & Achats"] THEN BEGIN
                        Item.CALCFIELDS("No. of Substitutes");
                        IF Item."No. of Substitutes" <> 0 THEN BEGIN
                            "Item Substitution".RESET();
                            "Item Substitution".SETRANGE(Type, "Item Substitution".Type::Item);
                            "Item Substitution".SETRANGE("No.", Item."No.");
                            TxtSubstitution := '';
                            IF "Item Substitution".FINDSET() THEN
                                REPEAT
                                    TxtSubstitution += "Item Substitution"."Substitute No." + '-';
                                UNTIL "Item Substitution".NEXT() = 0;
                            ERROR(ESK50113, Item."No.", TxtSubstitution);
                        END
                        ELSE
                            ERROR(ESK50110, Item."No.");
                    END;
                    // FIN AD Le 26-08-2009
                    SalesHeader.get(Rec."Document Type", rec."Document No.");
                    //WF le 03-05-2010 => FarGroup Analyse complémentaire Avril 2010 §2.3
                    IF NOT HideCheckInterdiction THEN
                        IF CheckInterdiction() THEN
                            IF NOT CONFIRM(STRSUBSTNO(ESK50114, "No.", SalesHeader."Sell-to Customer No.")) THEN
                                ERROR(ESK50115);
                    //FIN WF le 03-05-2010

                    // AD Le 24-09-2009 => FARGROUP -> Calcul des comissions
                    GetComission();
                    // FIN AD Le 24-09-2009

                    // AD Le 13-09-2011 => SYMTA
                    IF (NOT HideValidationDialog) THEN
                        Item.GestionPlusFourni("Variant Code", '');
                    // FIN AD Le 13-09-2011

                    IF (NOT HideValidationDialog) AND (Item."Stock Mort" <> Item."Stock Mort"::Non) THEN
                        MESSAGE(ESK50116, Item."Stock Mort");


                    // MC Le 27-10-2011 => SYMTA -> Suivi des commentaires article.
                    // MCO Le 14-02-2018 => Régie : Ajout des retours
                    IF (NOT HideValidationDialog) AND ("Document Type" IN ["Document Type"::Order, "Document Type"::Quote, "Document Type"::"Return Order"])
                     // FIN MCO Le 14-02-2018
                     AND GUIALLOWED THEN BEGIN
                        CommentLine.RESET();
                        CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
                        CommentLine.SETRANGE("No.", "No.");
                        CommentLine.SETRANGE("Display Sales Order", TRUE);
                        IF CommentLine.COUNT > 0 THEN BEGIN
                            CLEAR(FrmCommentLine);
                            FrmCommentLine.SETTABLEVIEW(CommentLine);
                            FrmCommentLine.EDITABLE(FALSE);
                            FrmCommentLine.RUN();
                        END;
                    END;
                    // FIN MC Le 27-10-2011 => SYMTA

                    // AD Le 24-03-2016
                    SalesSetup.GET();
                    // CFR le 12/05/2022 => R‚gie : homog‚n‚isation des unites (27 / 5404)
                    IF (Item.Longueur / 100 >= SalesSetup."Longueur Alerte Dimension") THEN
                        "Dimension Hors Norme" := TRUE;

                    // CFR le 12/04/2022 => R‚gie : Articles hors norme (unit‚)
                    IF lIUOM.GET(Item."No.", Item."Base Unit of Measure") THEN
                        IF (lIUOM.Length / 100 >= SalesSetup."Longueur Alerte Dimension") THEN
                            "Dimension Hors Norme" := TRUE;

                    // FIN CFR le 12/04/2022

                    IF (NOT HideValidationDialog) AND ("Document Type" IN ["Document Type"::Order, "Document Type"::Quote])
                     AND GUIALLOWED AND "Dimension Hors Norme" THEN
                        MESSAGE(ESK001);
                    // FIN AD Le 24-03-2016

                END;
        END;
    end;

    procedure "ValidateQte.Eskape"()
    var
        Item: Record Item;
    begin
        CASE Type OF
            Type::Item:
                BEGIN
                    GetItem(Item);
                    IF NOT HideValidationDialog THEN BEGIN
                        // Verif d'une qte maximum
                        IF Item.GetHorsNorme(Quantity) THEN
                            //MESSAGE(ESK50111, Quantity, Item."Hors Norme Sales (Qty.)");
                            MESSAGE(ESK50111, Quantity, FORMAT((Item."Conso. Normative" / 3), 0, '<Precision,0:2><Standard Format,0>'));

                        // Qte Restante sur commande ouverte
                        IF "Document Type" = "Document Type"::"Blanket Order" THEN
                            VALIDATE("Blanket Order Quantity Outstan", "Blanket Order Quantity Outstan" + (Quantity - xRec.Quantity));

                        // AD Le 24-09-2009 => FARGROUP -> Calcul des comissions
                        GetComission();
                        // FIN AD Le 24-09-2009

                        // AD Le 12-10-2011 => Gestion Prepa SYMTA
                        //IF USERID = 'eskape' THEN
                        /*
                        IF "Document Type"="Document Type"::"Return Order" THEN
                         BEGIN
                          VALIDATE("Return Qty. to Receive",Quantity)
                         END
                         ELSE
                         BEGIN
                            IF "Document Type" IN ["Document Type"::Order, "Document Type"::Quo] THEN
                              VALIDATE("Qty. to Ship", CalcQteLivrableSYMPA(15));
                         END;
                        */
                        VALIDATE("Qty. to Ship", CalcQteLivrableSYMPA(15));
                    END;
                END;
        END;

    end;

    procedure "Insert.Eskape"()
    var
        //     test: Integer;
        //     Cust: Record Customer;
        //     "--- ESKAPE ---": Integer;
        //     Param: Record "Generals Parameters";
        //     "Transfer Item Charge": Codeunit "Transfer Item Charge";
        SalesHeader: Record "Sales Header";
    begin
        /*
        // AD Le 23-05-2012 => Pas utile chez SYMTA et peut être provoque les lock
        // AD Le 21-10-2009 => On force le type de ligne en fct du type de document (integer 1 de la table)
        Param.RESET();
        Param.SETFILTER(Type, 'VTE-DOC*');
        Param.SETRANGE(Code, SalesHeader."Source Document Type");
        IF Param.FINDFIRST () THEN
          IF Param.integer1 <> 0 THEN
            BEGIN
              VALIDATE("Line type", Param.integer1);
              "Transfer Item Charge".UpdateLineType(Rec);
            END;
        // FIN AD Le 21-10-2009
        */
        SalesHeader.get(Rec."Document Type", Rec."Document No.");
        //WF le 30-04-2010 => FaRGroup Analyse complémentaire Avril 2010 §2.11
        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::"Credit Memo") THEN
            VALIDATE("Sans mouvement de stock", SalesHeader."Sans mouvement de stock");
        // FIN WF le 30-04-2010

        // MC Le 17-05-2010 => Fargroup Analyse complémentaire => On fait suivre le champ Exclure Contrat RFA de l'en-tête
        VALIDATE("Exclure RFA", SalesHeader."Exclure RFA");
        // FIN MC

        SuiviCommentaireArticle(); // AD Le 09-03-2016 => Déplace dans une fonction

    end;

    procedure InsertPortLine(Type: Option Port,FraisFacturation,RemiseGlobale)
    var
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        FraisLine: Record "Sales Line";
        "Cust Post Group": Record "Customer Posting Group";
        LineSpacing: Integer;
        NextLineNo: Integer;
        Text000Err: Label 'You cannot delete the order line because it is associated with purchase order %1 line %2.', Comment = '%1,%2';

    begin
        // AD Le 02-10-2007 => Insertion d'une ligne de port

        SalesHeader.GET("Document Type", "Document No.");
        "Cust Post Group".GET(SalesHeader."Customer Posting Group");

        // AD Le 15-12-2008 => Modification pour insertion uniquement du compte de frais
        // sans prise en compte des paramètres spéciaux de NAVISION


        CASE Type OF
            Type::Port:
                // BEGIN
                "Cust Post Group".TESTFIELD("Shipment Account");
            // AD Le 31-08-2009 => FARGROUP Décablé pour le moment
            // AD Le 22-03-2008 => Ajout de la gestion du franco de port,
            //IF SalesHeader.GetFrancoOrder THEN
            //  IF NOT CONFIRM(Text50006, FALSE) THEN
            //    EXIT;
            // FIN AD Le 31-08-2009
            // END;
            Type::FraisFacturation:
                "Cust Post Group".TESTFIELD("Service Charge Acc.");
            Type::RemiseGlobale:
                "Cust Post Group".TESTFIELD("Discount Account");
        END;

        FraisLine.RESET();
        FraisLine.SETRANGE("Document Type", "Document Type");
        FraisLine.SETRANGE("Document No.", "Document No.");
        FraisLine := Rec;
        IF FraisLine.FIND('>') THEN BEGIN
            LineSpacing :=
              (FraisLine."Line No." - Rec."Line No.") DIV 2;
            IF LineSpacing = 0 THEN
                ERROR(Text000Err);
        END ELSE
            LineSpacing := 10000;

        NextLineNo := Rec."Line No." + LineSpacing;

        FraisLine.INIT();
        FraisLine.VALIDATE("Document Type", "Document Type");
        FraisLine.VALIDATE("Document No.", "Document No.");
        FraisLine.VALIDATE(Type, FraisLine.Type::"G/L Account");
        FraisLine."Line No." := NextLineNo;

        CASE Type OF
            Type::Port:
                BEGIN
                    FraisLine.VALIDATE("No.", "Cust Post Group"."Shipment Account");
                    FraisLine.VALIDATE("Internal Line Type", "Internal Line Type"::Shipment);
                END;
            Type::FraisFacturation:

                FraisLine.VALIDATE("No.", "Cust Post Group"."Service Charge Acc.");

            Type::RemiseGlobale:
                BEGIN
                    FraisLine.VALIDATE("No.", "Cust Post Group"."Discount Account");
                    FraisLine.VALIDATE("Internal Line Type", "Internal Line Type"::"Discount Header");
                END;


        END;

        FraisLine.VALIDATE(Quantity, 1);
        "Allow Invoice Disc." := TRUE;
        FraisLine.INSERT();
    end;

    procedure ShortClose()
    var
        QtyToRcve: Decimal;
    begin
        // AD Le 28-08-2009 => Possibilité de solder la ligne d'origine

        CASE "Document Type" OF
            "Document Type"::"Return Order":
                BEGIN
                    QtyToRcve := "Return Qty. to Receive";
                    ShortCloseLine := TRUE;
                    SuspendStatusCheck(TRUE);
                    VALIDATE("Initial Quantity", Quantity);
                    VALIDATE(Quantity, "Return Qty. to Receive" + "Return Qty. Received");
                    VALIDATE("Return Qty. to Receive", QtyToRcve);
                    // MCO KLe 10-02-2017
                    InitOutstanding();
                    // FIN MCO Le 10-02-2017


                    SuspendStatusCheck(FALSE);
                END;
            "Document Type"::Order:
                ShortCloseLine := TRUE;

        END;
    end;

    procedure MajDelivry(RecDelivrySale: Record "Sales Line")
    var
        SalesL: Record "Sales Line";
        SalesH: Record "Sales Header";
        Item: Record Item;
        SKU: Record "Stockkeeping Unit";
        physique: Decimal;
        physiqueRestant: Decimal;
        alreadySelect: Decimal;
    begin
        // ------------------------------------------------------------------------------------------------
        //                  Permet de connaitre la qte livrable d'une ligne de commande
        //                        => Utilisé pour la préparation (ecran des lignes)
        // ------------------------------------------------------------------------------------------------

        // AD Le 19-09-2009 => ATTENTION G MODIFIE LA FONCTION LE 19-09-2009 => CAR POUR MOI C ETAIT PAS LOGIQUE

        // AD Le 07-10-2005 => Prise en compte dans le A traiter des cdes selectionnées.
        alreadySelect := 0;
        SalesL.SETCURRENTKEY("Document Type", Type, "No.");
        SalesL.SETRANGE("Document Type", "Document Type");
        SalesL.SETRANGE(Type, Rec.Type);
        SalesL.SETRANGE("No.", "No.");
        SalesL.SETFILTER("Outstanding Quantity", '<>%1', 0); // AD Le 20-01-2014
        IF SalesL.FINDSET() THEN
            REPEAT
                IF SalesH.GET(SalesL."Document Type", SalesL."Document No.") THEN
                    IF (SalesH."Select Order") AND (SalesH."No." <> "Document No.") THEN
                        alreadySelect += SalesL."A Traiter en préparation";
            UNTIL SalesL.NEXT() = 0;
        // FIN AD Le 07-10-2005

        IF "Qty. to Assemble to Order" = 0 THEN BEGIN
            IF SKU.GET("Location Code", "No.", "Variant Code") THEN BEGIN
                SKU.CALCFIELDS(SKU.Inventory);
                physiqueRestant := SKU.Inventory + -alreadySelect;
                physique := SKU.Inventory;
            END
            ELSE
                IF Item.GET(SalesL."No.") THEN BEGIN
                    // AD Le 11-06-2010 => Stock que du magasin de la ligne
                    Item.SETRANGE("Location Filter", SalesL."Location Code");
                    // FIN AD Le 11-06-2010
                    Item.CALCFIELDS(Inventory);
                    physiqueRestant := Item.Inventory + -alreadySelect;
                    physique := Item.Inventory;
                END
                ELSE
                    physiqueRestant := 0;
        END
        ELSE
            ERROR('KIT TODO MIG 2015'); //physiqueRestant := kitManagement.GetQtéLivrable(Rec) + - alreadySelect;

        // AD Le 07-10-2005 => Qte a traiter en fonction du restant
        IF physiqueRestant > "Outstanding Quantity" THEN
            "A Traiter en préparation" := RecDelivrySale."Outstanding Quantity"
        ELSE
            IF physiqueRestant <= 0 THEN
                "A Traiter en préparation" := 0
            ELSE
                "A Traiter en préparation" := physiqueRestant;

        IF Item.GET("No.") THEN
            IF Item."Sans Mouvements de Stock" THEN
                "A Traiter en préparation" := "Outstanding Quantity";

        MODIFY();
    end;

    procedure GetComission()
    var
        SalesPers: Record "Salesperson/Purchaser";
        Cust: Record Customer;
        Campaign: Record Campaign;
        ExcluCom: Record "Exclusions Comissions";
        TempExcluCom: Record "Exclusions Comissions" temporary;
        Item: Record Item;
        salesHeader: Record "Sales Header";
        Currency: Record Currency;
    begin
        // =====================================================
        // AD Le 24-09-2009 => FARGROUP -> Calcul des comissions
        // =====================================================


        IF Type <> Type::Item THEN
            EXIT;

        // ------------------------
        // Chargement des variables
        // ------------------------
        GetSalesHeader(salesHeader, Currency);
        GetItem(Item);
        Cust.GET("Sell-to Customer No.");
        SalesPers.GET(SalesHeader."Salesperson Code");
        IF "Campaign No." <> '' THEN
            Campaign.GET("Campaign No.")
        ELSE
            Campaign."Rate Commission" := 1;

        // ----------------------
        // Calcul du taux de base
        // ----------------------
        "Commission % Calculate" := SalesPers."Commission %" * Cust."Rate Commission" * Item."Rate Commission" * Campaign."Rate Commission";

        // ------------------------
        // Recherche des exceptions
        // ------------------------
        TempExcluCom.RESET();
        TempExcluCom.DELETEALL();
        WITH ExcluCom DO
            // Boucle pour les type de vendeur (tous ou reprsentant)
            FOR "Salesperson Type" := "Salesperson Type"::Tous TO ExcluCom."Salesperson Type"::Vendeur DO BEGIN
                SETRANGE("Salesperson Type", "Salesperson Type");
                CASE "Salesperson Type" OF
                    "Salesperson Type"::Tous:
                        SETRANGE("Salesperson Code", '');
                    ExcluCom."Salesperson Type"::Vendeur:
                        SETRANGE("Salesperson Code", SalesPers.Code);
                END;
                // Boucle pour les type de ventes
                FOR "Sales Type" := "Sales Type"::Tous TO "Sales Type"::Client DO BEGIN
                    SETRANGE("Sales Type", "Sales Type");
                    CASE "Sales Type" OF
                        "Sales Type"::Tous:
                            SETRANGE("Sales Code", '');
                        "Sales Type"::Centrale:
                            SETRANGE("Sales Code", '');
                        "Sales Type"::"Famille 1":
                            SETRANGE("Sales Code", Cust."Family Code 1");
                        "Sales Type"::"Famille 2":
                            SETRANGE("Sales Code", Cust."Family Code 2");
                        "Sales Type"::"Famille 3":
                            SETRANGE("Sales Code", Cust."Family Code 3");
                        "Sales Type"::Client:
                            SETRANGE("Sales Code", Cust."No.");
                    END;

                    // Boucle pour les type article (famille ...)
                    FOR Type := Type::Tous TO Type::Article DO BEGIN
                        SETRANGE(Type, Type);
                        CASE Type OF
                            Type::Tous:
                                SETRANGE(Code, '');
                            Type::Marque:
                                SETRANGE(Code, Item."Manufacturer Code");
                            Type::Famille:
                                SETRANGE(Code, Item."Item Category Code");
                            /*//TODOType::"Sous Famille":
                                SETRANGE(Code, Item."Product Group Code");*/
                            Type::Article:
                                SETRANGE(Code, "No.");
                        END;

                        IF FINDSET() THEN
                            REPEAT
                                TempExcluCom := ExcluCom;
                                TempExcluCom.INSERT();
                            UNTIL NEXT() = 0;

                    END;
                END;

            END;

        // -------------------------
        // Traitement des exceptions
        // -------------------------
        TempExcluCom.SETCURRENTKEY("Comission %");
        TempExcluCom.ASCENDING(TRUE);
        IF TempExcluCom.FINDFIRST() THEN
            "Commission % Calculate" := TempExcluCom."Comission %";


        // --------------------------------
        // Aplication de la defalcation SAV
        // --------------------------------
        IF "Opération ADV" THEN
            "Commission % Calculate" -= SalesPers."Rate ADV";

        // ----------------------
        // Calcul du taux final
        // ----------------------
        IF "Commission % Calculate" < 0 THEN
            "Commission % Calculate" := 0;

        "Commission %" := "Commission % Calculate";
    end;

    procedure "---- Interdiction Vente ----"()
    begin
        //WF le 03-05-2010 => FarGroup Analyse Complémentaire Avril 2010 §2.3
    end;

    procedure CheckInterdiction() blnReturn: Boolean
    var
        InterdictionVente: Record "Interdiction Vente";
    begin
        //Initialise le retour
        blnReturn := FALSE;

        //On enlève tous les marques de la table
        InterdictionVente.CLEARMARKS();
        //On marque tous les enregistrements concernant le produit et le client
        MarkInterdiction_Item(InterdictionVente);

        //On récupère tous les enregistrements marqués
        InterdictionVente.MARKEDONLY(TRUE);

        //Prend la dernière ligne et vérifie son statut
        IF InterdictionVente.FINDLAST() THEN
            //On Regarde le type d'autorisation/Interdiction
            blnReturn := (InterdictionVente.Statut = InterdictionVente.Statut::Interdiction);
    end;

    procedure MarkInterdiction_Item(var pInterdictionVente: Record "Interdiction Vente")
    var
        Item: Record Item;
        Customer: Record Customer;
        SalesHeader: Record "Sales Header";
    begin
        //Récupère L'article saisi dans la ligne vente
        Item.RESET();
        Item.GET("No.");
        SalesHeader.get(Rec."Document Type", Rec."Document No.");
        //Récupère le client de la vente
        Customer.RESET();
        Customer.GET(SalesHeader."Sell-to Customer No.");

        //Pause les filtres sur la date, puis on travaillera avec ses données
        pInterdictionVente.RESET();
        pInterdictionVente.SETFILTER(pInterdictionVente."Date Debut", '<=%1', SalesHeader."Order Date");
        pInterdictionVente.SETFILTER(pInterdictionVente."Date Fin", '>=%1', SalesHeader."Order Date");

        //Filtre tous les enregistrements concernant l'article (à partir de son numéro)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article", pInterdictionVente."Type Article"::Article);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Article", Item."No.");
        //On ajoute les marques pour le client
        MarkInterdiction_Customer(Customer, pInterdictionVente);

        //Filtre tous les enregistrements concernant l'article (à partir de la sous famille)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article", pInterdictionVente."Type Article"::"Sous Famille");
        //TODOpInterdictionVente.SETRANGE(pInterdictionVente."Code Article", Item."Product Group Code");
        //On ajoute les marques pour le client
        MarkInterdiction_Customer(Customer, pInterdictionVente);

        //Filtre tous les enregistrements concernant l'article (à partir de la famille)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article", pInterdictionVente."Type Article"::Famille);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Article", Item."Item Category Code");
        //On ajoute les marques pour le client
        MarkInterdiction_Customer(Customer, pInterdictionVente);

        //Filtre tous les enregistrements concernant l'article (à partir de la marque)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article", pInterdictionVente."Type Article"::Marque);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Article", Item."Manufacturer Code");
        //On ajoute les marques pour le client
        MarkInterdiction_Customer(Customer, pInterdictionVente);

        //Filtre tous les enregistrements concernant l'article (Tous)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article", pInterdictionVente."Type Article"::Tous);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Article");
        //On ajoute les marques pour le client
        MarkInterdiction_Customer(Customer, pInterdictionVente);

        //Enlève les filtres
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Article");
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Article");

        pInterdictionVente.SETRANGE(pInterdictionVente."Date Debut");
        pInterdictionVente.SETRANGE(pInterdictionVente."Date Fin");
    end;

    procedure MarkInterdiction_Customer(pCustomer: Record Customer; var pInterdictionVente: Record "Interdiction Vente")
    begin
        //Filtre tous les enregistrements concernant le Client (à partir de son numéro)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente", pInterdictionVente."Type Vente"::Client);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente", pCustomer."No.");
        //Marque les champs pour les récupérer plus tard
        Mark_Field(pInterdictionVente);

        //Filtre tous les enregistrements concernant le Client (à partir de la Famille 3)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente", pInterdictionVente."Type Vente"::"Famille 3");
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente", pCustomer."Family Code 3");
        //Marque les champs pour les récupérer plus tard
        Mark_Field(pInterdictionVente);

        //Filtre tous les enregistrements concernant le Client (à partir de la famille 2)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente", pInterdictionVente."Type Vente"::"Famille 2");
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente", pCustomer."Family Code 2");
        //Marque les champs pour les récupérer plus tard
        Mark_Field(pInterdictionVente);

        //Filtre tous les enregistrements concernant le Client (à partir de la famille 1)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente", pInterdictionVente."Type Vente"::"Famille 1");
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente", pCustomer."Family Code 1");
        //Marque les champs pour les récupérer plus tard
        Mark_Field(pInterdictionVente);

        //Filtre tous les enregistrements concernant le Client (Tous)
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente", pInterdictionVente."Type Vente"::Tous);
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente");
        //Marque les champs pour les récupérer plus tard
        Mark_Field(pInterdictionVente);

        //Enlève les filtres
        pInterdictionVente.SETRANGE(pInterdictionVente."Type Vente");
        pInterdictionVente.SETRANGE(pInterdictionVente."Code Vente");
    end;

    procedure Mark_Field(var pInterdictionVente: Record "Interdiction Vente")
    begin
        // On marque tous les champs présent dans le jeu d'enregistrement
        IF pInterdictionVente.FINDFIRST() THEN
            REPEAT
                pInterdictionVente.MARK(TRUE);
            UNTIL pInterdictionVente.NEXT() = 0;
    END;

    procedure SetHideCheckInterdiction(NewHideCheckInterdiction: Boolean)
    begin
        //Cette fonction permet de passer outre l'interdiction sinon BUG dans l'EDI
        NewHideCheckInterdiction := HideCheckInterdiction;
    end;

    procedure "-- Fin Interdiction --"()
    begin
    end;

    procedure CalcQteOuverteSurCde(): Decimal
    var
        SalesLine: Record "Sales Line";
        Qty: Decimal;
    begin
        // AD Le 07-06-2010 => FARGROUP -> Permet de retourner la qte sur commande normale

        Qty := 0;
        SalesLine.RESET();
        SalesLine.SETCURRENTKEY("Document Type", "Blanket Order No.", "Blanket Order Line No.");
        SalesLine.SETRANGE("Document Type", SalesLine."Document Type"::Order);
        SalesLine.SETRANGE("Blanket Order No.", "Document No.");
        SalesLine.SETRANGE("Blanket Order Line No.", "Line No.");
        SalesLine.SETFILTER("Outstanding Quantity", '>%1', 0);
        REPEAT
            Qty += SalesLine."Outstanding Quantity";
        UNTIL SalesLine.NEXT() = 0;

        EXIT(Qty);
    end;

    procedure VerifItemOnOrder()
    var
        Rec_SalesLine: Record "Sales Line";
        Rec_Company: Record "Company Information";
        Rec_SalesHeader: Record "Sales Header";
        Text901: Label 'L''article : %1 est déjà saisit sur la commande : %2 datant du %3';
        DateMini: Date;
        DateMaxi: Date;
    begin
        // ----------------------------------------------------------------------------------------------- //
        // CETTE FONCTION VERIFIE SI L'ARTICLE EST DEJA PRESENT SUR UNE COMMANDE DANS LES x DERNIERS JOURS //
        // ----------------------------------------------------------------------------------------------- //


        // On récupère le nombre de jours stocké dans la table [Company Info] pour lequel on veut chercher les commandes
        Rec_Company.GET();
        // AD Le 25-11-2010 =>
        // DateMini := CALCDATE('-'+ Rec_Company."Nb Jour Verif Commande", WORKDATE()); => CIDE D'ORIGINE
        DateMini := CALCDATE('-' + Rec_Company."Nb Jour Verif Commande Article", WORKDATE());
        // FIN AD Le 25-11-2010

        DateMaxi := WORKDATE();

        // On recherche les en-têtes de commandes dans la période définit
        CLEAR(Rec_SalesHeader);
        Rec_SalesHeader.SETCURRENTKEY("Document Type", "Order Date", "Sell-to Customer No.", "No.");
        Rec_SalesHeader.SETRANGE("Document Type", Rec_SalesHeader."Document Type"::Order);
        Rec_SalesHeader.SETRANGE(Rec_SalesHeader."Order Date", DateMini, DateMaxi);
        Rec_SalesHeader.SETRANGE(Rec_SalesHeader."Sell-to Customer No.", "Sell-to Customer No.");
        Rec_SalesHeader.SETFILTER("No.", '<>%1', "Document No.");
        IF Rec_SalesHeader.FINDSET() THEN
            REPEAT
                Rec_SalesLine.SETCURRENTKEY("Document No.", Type, "No.");
                Rec_SalesLine.SETRANGE(Rec_SalesLine."Document No.", Rec_SalesHeader."No.");
                Rec_SalesLine.SETRANGE(Rec_SalesLine.Type, Type);
                Rec_SalesLine.SETRANGE(Rec_SalesLine."No.", "No.");
                IF Rec_SalesLine.FINDFIRST() THEN BEGIN
                    MESSAGE(Text901, Rec_SalesLine."No.", Rec_SalesHeader."No.", Rec_SalesHeader."Order Date");
                    EXIT;
                END;
            UNTIL Rec_SalesHeader.NEXT() = 0;
    end;

    procedure SetCompLine(LComponentLine: Boolean)
    begin
        // ----------------------------------------------------------------------------------------- //
        // CETTE FONCTION PERMET DE NE PAS FAIRE CERTAINS TESTS QUAND ON EST SUR UN COMPOSANT DE KIT //
        // ----------------------------------------------------------------------------------------- //

        ComponentLine := LComponentLine;
    end;

    procedure ShowItemSubEskape()
    var
        // PanelMngt: Codeunit "Sales Info-Pane Management";
        LItem: Record Item;
    begin
        TestStatusOpen();


        IF Type <> Type::Item THEN
            EXIT;

        LItem.GET("No.");
        LItem.CALCFIELDS(Inventory);

        IF (LItem.Inventory <> 0) AND (LItem."Assembly Policy" <> LItem."Assembly Policy"::"Assemble-to-Order") THEN
            EXIT;

        IF LItem."Assembly Policy" = LItem."Assembly Policy"::"Assemble-to-Order" THEN
            EXIT;
        ////  IF PanelMngt.CalcDispKit(LItem."No.", TRUE,"Location Code") <> 0 THEN
        ////    EXIT;


        ItemSubstitutionMgt.ItemSubstGet(Rec);
        IF TransferExtendedText.SalesCheckIfAnyExtText(Rec, TRUE) THEN
            TransferExtendedText.InsertSalesExtText(Rec);
    end;

    procedure CloseSalesLine()
    var
        assocLine: Record "Sales Line";
        SalesHeader: Record "Sales Header";
        // CU80: Codeunit "Sales-Post";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin

        SuspendStatusCheck(TRUE);
        ShortCloseLine := TRUE;
        SetHideValidationDialog(TRUE);

        "Initial Quantity" := Quantity;
        VALIDATE(Quantity, "Quantity Shipped");
        VALIDATE("Qty. to Ship", 0);

        IF Quantity = 0 THEN BEGIN
            VALIDATE("Unit Price", 0);
            VALIDATE(Amount, 0);
        END;

        "Completely Shipped" := TRUE;
        SuspendStatusCheck(FALSE);
        SetHideValidationDialog(FALSE);
        MODIFY();

        // AD Le 11-12-2006 => Abandon de reliquat des frais associés.
        assocLine.RESET();
        assocLine.SETRANGE("Document Type", "Document Type");
        assocLine.SETRANGE("Document No.", "Document No.");
        assocLine.SETRANGE("Attached to Line No.", "Line No.");
        assocLine.SETFILTER(Type, '%1|%2', assocLine.Type::"Charge (Item)", assocLine.Type::Resource);

        IF assocLine.FIND('-') THEN
            REPEAT
                assocLine.SuspendStatusCheck(TRUE);
                assocLine.ShortClose();
                assocLine."Initial Quantity" := assocLine.Quantity;
                assocLine.VALIDATE(Quantity, Quantity);
                assocLine.VALIDATE("Quantity (Base)", "Quantity (Base)");
                assocLine.VALIDATE("Qty. to Ship", 0);
                IF Quantity = 0 THEN BEGIN
                    assocLine.VALIDATE("Unit Price", 0);
                    assocLine.VALIDATE(Amount, 0);
                END;
                assocLine."Completely Shipped" := TRUE;
                assocLine.SuspendStatusCheck(FALSE);
                assocLine.MODIFY();

            UNTIL assocLine.NEXT() = 0;
        // FIN AD Le 11-12-2006

        SalesHeader.GET("Document Type", "Document No.");
        CduLFunctions.UpdateOrderShipmentStatus(SalesHeader);
    end;

    procedure CalcQteLivrableSYMPA(_pCallByFieldNo: Integer): Decimal
    var
        SKU: Record "Stockkeeping Unit";
        Item: Record Item;
        physique: Decimal;
        physiqueRestant: Decimal;
        Livrable: Decimal;
        LQteReserve: Decimal;
        LQteAttendu: Decimal;
    begin
        // ------------------------------------------------------------------------------------------------
        //                  Permet de connaitre la qte livrable d'une ligne de commande
        //                        => Utilisé pour la saisie des commandes chez SYMPTA
        // ------------------------------------------------------------------------------------------------

        // Demande de Symta
        // Si devis -> Disponible à la date du jour.
        // Si commande -> Si livraison demandée > à la date du jour : 0
        //                Sinon Disponible à la date du jour.
        // Pour les retours -> Quantité retournée
        // Pour les avoirs/facture -> 0;

        IF Type <> Type::Item THEN EXIT(0);

        CASE "Document Type" OF
            // "Document Type"::"Return Order" : EXIT(Quantity);
            "Document Type"::"Return Order":
                EXIT(0);
            "Document Type"::"Credit Memo":
                EXIT(0);
            "Document Type"::Invoice:
                EXIT(0);
        END;

        IF "Requested Delivery Date" > WORKDATE() THEN
            EXIT(0);



        IF Quantity < 0 THEN
            EXIT(Quantity);

        GetItem();
        Item.SETRANGE("Date Filter", 0D, WORKDATE());
        Item.CalcAttenduReserve(LQteReserve, LQteAttendu);

        // AD Le 05-02-2015 => Si on est en devis, l'ancienne qte n'est pas reservée donc on la prend pas en commpte
        // IF _pCallByFieldNo = 15 THEN
        IF (_pCallByFieldNo = 15) AND ("Document Type" <> "Document Type"::Quote) THEN
            // FIN AD Le 05-02-2015
            LQteReserve := LQteReserve - xRec.Quantity;

        // Ré-écrit pour les kit version 2015
        IF "Qty. to Assemble to Order" = 0 THEN BEGIN
            IF SKU.GET("Location Code", "No.", "Variant Code") THEN BEGIN
                SKU.CALCFIELDS(SKU.Inventory);
                physiqueRestant := SKU.Inventory - LQteReserve;
                physique := SKU.Inventory;
            END
            ELSE
                IF Item.GET("No.") THEN BEGIN
                    // AD Le 11-06-2010 => Stock que du magasin de la ligne
                    Item.SETRANGE("Location Filter", "Location Code");
                    // FIN AD Le 11-06-2010
                    Item.CALCFIELDS(Inventory);
                    physiqueRestant := Item.Inventory - LQteReserve;
                    physique := Item.Inventory;
                END
                ELSE
                    physiqueRestant := 0;
        END
        ELSE
            ERROR('physiqueRestant := kitManagement.GetQtéLivrable(Rec)');
        ;

        // AD Le 07-10-2005 => Qte a traiter en fonction du restant
        IF physiqueRestant > "Outstanding Quantity" THEN
            Livrable := "Outstanding Quantity"
        ELSE
            IF physiqueRestant <= 0 THEN
                Livrable := 0
            ELSE
                Livrable := physiqueRestant;

        IF Item.GET("No.") THEN
            IF Item."Sans Mouvements de Stock" THEN
                Livrable := "Outstanding Quantity";

        EXIT(Livrable);
    end;

    procedure SetHideQuoteToOrder(NewHideQuoteToOrder: Boolean)
    begin
        // MCO Le 01-04-2015
        HideQuoteToOrder := NewHideQuoteToOrder;
        // FIN MCO Le 01-04-2015
    end;

    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        IF Type <> Type::Item THEN EXIT;

        FrmQuid.InitArticle("No.");
        FrmQuid.InitClient("Sell-to Customer No.");
        //FrmQuid.RUNMODAL();
        FrmQuid.RUN();
    end;

    procedure GetInfoEntete(Type: Integer): Text[100]
    var
        SalesHeader: Record "Sales Header";
    begin
        // AD Le 25-11-2009 => Retourne des infos de l'entete

        SalesHeader.GET("Document Type", "Document No.");
        CASE Type OF
            1:
                EXIT(FORMAT(SalesHeader."Order Date"));  // Date de commande
            2:
                EXIT(SalesHeader."Sell-to Customer Name");
        END;
    end;

    procedure GetSalesMultiple() dec_Return: Decimal
    var
        LItem: Record Item;
    begin
        IF Type <> Type::Item THEN EXIT(0);
        IF LItem.GET("No.") THEN
            EXIT(LItem."Sales multiple");
    end;

    procedure SuiviCommentaireArticle()
    var
        CommentLine: Record "Comment Line";
        SalesCommentLine: Record "Sales Comment Line";
    // Item: Record Item;
    begin
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        IF bln_DoNotCreateComment THEN EXIT;
        // FIN MCO Le 09-02-2017

        // AD Le 26-04-2010 => GDI -> On fait suivre les commentaires
        IF Type = Type::Item THEN BEGIN
            CommentLine.RESET();
            CommentLine.SETCURRENTKEY("Table Name", "No.", "Line No.");
            CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
            CommentLine.SETRANGE("No.", "No.");
            IF CommentLine.FINDSET() THEN BEGIN
                SalesCommentLine.RESET();
                SalesCommentLine.SETRANGE("Document Type", "Document Type");
                SalesCommentLine.SETRANGE("No.", "Document No.");
                SalesCommentLine.SETRANGE("Document Line No.", "Line No.");
                IF NOT SalesCommentLine.ISEMPTY THEN
                    SalesCommentLine.DELETEALL();

                REPEAT
                    SalesCommentLine."Document Type" := "Document Type";
                    SalesCommentLine."No." := "Document No.";
                    SalesCommentLine."Line No." := CommentLine."Line No.";
                    SalesCommentLine."Document Line No." := "Line No.";
                    SalesCommentLine.Comment := CommentLine.Comment;
                    SalesCommentLine."Print Wharehouse Shipment" := CommentLine."Print Wharehouse Shipment";
                    SalesCommentLine."Print Shipment" := CommentLine."Print Shipment";
                    SalesCommentLine."Print Invoice" := CommentLine."Print Invoice";
                    SalesCommentLine."Display Order" := CommentLine."Display Sales Order";
                    SalesCommentLine."Print Order" := CommentLine."Print Order";
                    SalesCommentLine."Print Quote" := CommentLine."Print Quote";
                    SalesCommentLine.INSERT();
                UNTIL CommentLine.NEXT() = 0;
            END;
        END;
        // FIN AD LE 26-04-2010
    end;

    procedure SetNotCreateComment(New_BlnNotCreateComment: Boolean)
    begin
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        bln_DoNotCreateComment := New_BlnNotCreateComment;
        // FIN MCO Le 09-02-2017
    end;

    //****************************Fct Non utiliser*****************************************
    // local procedure SearchByDescription(): Boolean
    // var
    //     SalesSetup: Record "Sales & Receivables Setup";
    // begin
    //     // MCO Le 14-02-2018 => Régie
    //     SalesSetup.GET();
    //     EXIT(NOT SalesSetup."Do not search description");
    //     // FIN MCO Le 14-02-2018
    // end;
    //******************************************************************************************

    procedure ExpressDiscount(var pRemiseOrder: Decimal; var pRemiseExpress: Decimal; var pCoefficient: Decimal; pVerbose: Boolean): Boolean
    var
        lSalesLineDiscountTemp: Record "Sales Line Discount" temporary;
        lSalesPriceTemp: Record "Sales Price" temporary;
        lItem: Record Item;
        lSalesHeader: Record "Sales Header";
        lCustomerOrder: Record Customer;
        lCustomerExpress: Record Customer;
        lSalesSetup: Record "Sales & Receivables Setup";
        lItemCategory: Record "Item Category";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        // lCalcDate: Text;
        lRemise: Decimal;
        lPrix: Decimal;
    begin

        // CFR le 18/11/2020 : Régie > remises express
        pRemiseExpress := 0;
        pRemiseOrder := 0;
        pCoefficient := 0;

        lSalesSetup.GET();
        IF lSalesSetup."Code Client Reprise Express" = '' THEN
            EXIT(FALSE);

        // Uniquement sur les articles des commandes et des devis
        IF (Rec.Type <> Rec.Type::Item) THEN
            EXIT(FALSE);
        IF (Rec."Document Type" <> Rec."Document Type"::Quote) AND (Rec."Document Type" <> Rec."Document Type"::Order) THEN
            EXIT(FALSE);

        IF (lSalesHeader.GET(Rec."Document Type", Rec."Document No.")) AND
            (lItem.GET(Rec."No.")) THEN BEGIN
            // CFR le 06/04/2023 - R‚gie : Gestion du champ [Exclude Express Discount] de la table Cat‚gorie Article
            IF (lItemCategory.GET(lItem."Item Category Code")) AND (lItemCategory."Exclude Express Discount") THEN
                EXIT(FALSE);
            // FIN CFR le 06/04/2023
            lCustomerExpress.GET(lSalesSetup."Code Client Reprise Express");
            pRemiseExpress := LCodeunitsFunctions.GetSalesDisc(lSalesLineDiscountTemp, lCustomerExpress."No.", '', lCustomerExpress."Customer Disc. Group", '',
                                lItem."No.", '', lItem."Base Unit of Measure", '', lSalesHeader."Order Date", FALSE, Rec.Quantity, 1,
                                lCustomerExpress."Sous Groupe Tarifs", lCustomerExpress."Centrale Active", lSalesHeader."Type de commande");

            lCustomerOrder.GET(lSalesHeader."Bill-to Customer No.");
            pRemiseOrder := LCodeunitsFunctions.GetSalesDisc(lSalesLineDiscountTemp, lCustomerOrder."No.", '', lCustomerOrder."Customer Disc. Group", '',
                                lItem."No.", '', lItem."Base Unit of Measure", '', lSalesHeader."Order Date", FALSE, Rec.Quantity, 1,
                                lCustomerOrder."Sous Groupe Tarifs", lCustomerOrder."Centrale Active", lSalesHeader."Type de commande");

            IF LCodeunitsFunctions.GetUnitPrice(lSalesPriceTemp, lCustomerOrder."No.", '', lCustomerOrder."Customer Price Group", '',
                                lItem."No.", '', lItem."Base Unit of Measure", '', lSalesHeader."Order Date", FALSE, Rec.Quantity, lRemise, lPrix,
                                lCustomerOrder."Sous Groupe Tarifs", lCustomerOrder."Centrale Active", lSalesHeader."Type de commande") THEN
                pCoefficient := lSalesPriceTemp.Coefficient;

            IF (pVerbose) THEN
                MESSAGE('Express : %1 - Commande : %2 - Coefficient : %3', pRemiseExpress, pRemiseOrder, pCoefficient);

            IF (pRemiseExpress <> 0) AND (pRemiseExpress <> pRemiseOrder) THEN
                EXIT(TRUE);

        END;

        EXIT(FALSE);
        // FIN CFR le 18/11/2020
    end;

    PROCEDURE CalcMarge();
    BEGIN
        // CFR le 04/10/2023 - R‚gie ajout du calcul de marge
        IF NOT (Rec."Document Type" IN [Rec."Document Type"::Quote, Rec."Document Type"::Order]) THEN EXIT;
        IF NOT (Rec.Type IN [Rec.Type::Item]) THEN EXIT;

        Rec."Marge %" := 0;
        IF (Rec."Net Unit Price" <> 0) THEN
            Rec."Marge %" := (Rec."Net Unit Price" - Rec."Unit Cost") / Rec."Net Unit Price" * 100;
    END;

    PROCEDURE HasItemLink(): Boolean;
    VAR
        lItem: Record Item;
    BEGIN
        // CFR le 04/10/2023 - R‚gie gestion des liens articles
        IF (Rec.Type = Rec.Type::Item) AND (lItem.GET(Rec."No.")) THEN
            EXIT(lItem.HASLINKS())
        ELSE
            EXIT(FALSE);
    END;

    procedure GetShortCloseLine(): Boolean
    begin
        exit(ShortCloseLine);
    end;


    var
        GestionMultiReference: Codeunit "Gestion Multi-référence";
        ItemSubstitutionMgt: Codeunit "Item Subst.";
        TransferExtendedText: Codeunit "Transfer Extended Text";
        ESK50110: Label 'L''artilce %1 est bloqué en ventes !';
        ShortCloseLine: Boolean;
        ESK50111: Label 'La quantité saisie (%1) est suppérieur à la quantité Hors Norme de l''article (%2)!';
        ESK50113: Label 'L''artilce %1 est bloqué en ventes !\ Mais il existe des article de subsitution : %2';
        ESK50114: Label 'L''article %1 pour le client %2 est bloqué ! (Module Interdiction vente) Voulez vous l''autoriser quand même ?';
        HideCheckInterdiction: Boolean;
        ESK50115: Label 'L''article ne peut donc être ajouté à la commande.';
        ComponentLine: Boolean;
        ESK50116: Label 'Stock mort = %1 - Prix Spécial !';
        HideQuoteToOrder: Boolean;
        ESK001: Label 'ATTENTION : Dimensions spéciales. Prévoir du port !';
        bln_DoNotCreateComment: Boolean;


}

