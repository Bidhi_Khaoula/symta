tableextension 50374 "Sales Header Archive Logic" extends "Sales Header Archive" //5107
{
    fields
    {

        modify("Invoice Customer No.")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 07-09-2009 => FARGROUP -> Client Facturé
                VALIDATE("Bill-to Customer No.");
            end;
        }
    }

    procedure GetCentraleName(): Text[60]
    var
        LCust: Record Customer;
    begin
        IF "Centrale Active" = '' THEN
            EXIT('');

        LCust.GET("Centrale Active");
        EXIT(Format(LCust."No." + ' ' + LCust.Name));
    end;

    PROCEDURE GetWorkDescription(): Text;
    VAR
        TempBlob: Record "Upgrade Blob Storage" TEMPORARY;
        CduLFunctions: Codeunit Functions;
        CR: Text[1];


    BEGIN
        // CFR le 06/04/2022 => R‚gie : Affichage du champ "Work Description"
        CALCFIELDS("Work Description");
        IF NOT "Work Description".HASVALUE THEN
            EXIT('');
        CR[1] := 10;
        TempBlob.Blob := "Work Description";
        EXIT(CduLFunctions.ReadAsText(CR, TEXTENCODING::Windows));
    END;
}

