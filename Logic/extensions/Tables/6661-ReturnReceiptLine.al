tableextension 50409 "Return Receipt Line Logic" extends "Return Receipt Line" //6661
{
    fields
    {
        modify("Exclure RFA")
        {
            trigger OnAfterValidate()
            begin
                IF Type = Type::Item THEN
                    TESTFIELD("No.");
            end;
        }

    }


    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        IF Type <> Type::Item THEN EXIT;

        FrmQuid.InitArticle("No.");
        FrmQuid.InitClient("Sell-to Customer No.");
        FrmQuid.RUNMODAL();
    end;


}

