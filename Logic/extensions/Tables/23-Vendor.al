tableextension 50212 "Vendor Logic" extends Vendor //23
{
    fields
    {

        modify("Code Appro")
        {
            trigger OnAfterValidate()
            begin
                // ANI Le 23-06-2016 FE20160617 -> annulation du traitement car calculé par une tâche planifiée
                /*
                // ANI Le 16-06-2016 FE20150424
                IF xRec."Code Appro" <> "Code Appro" THEN
                  IF CONFIRM(STRSUBSTNO(ESK001, "Code Appro")) THEN
                    UpdateItemCodeAppro();
                */
                // ANI Le 23-06-2016 FE20160617

            end;
        }
    }


    procedure FileExist(): Boolean
    var
        Lien: Record "Record Link";
    begin
        Lien.SETRANGE(Company, COMPANYNAME);
        Lien.SETFILTER("Record ID", TABLENAME + ':' + "No.");
        EXIT(Not Lien.IsEmpty());
    end;

    procedure UpdateItemCodeAppro()
    var
        lItem: Record Item;
    begin
        // ANI Le 16-06-2016 FE20150424
        lItem.RESET();
        lItem.SETRANGE("Vendor No.", "No.");
        lItem.MODIFYALL("Code Appro", "Code Appro");
    end;

    procedure UpdateLeadTime(prec_Vendor: Record Vendor)
    var
        lrec_Item: Record Item;
        lrec_ItemVendor: Record "Item Vendor";
    begin

        CLEAR(lrec_Item);
        lrec_Item.SETRANGE("Vendor No.", prec_Vendor."No.");
        lrec_Item.SETCURRENTKEY("Vendor No.");
        IF lrec_Item.FINDSET() THEN
            REPEAT
                lrec_Item.VALIDATE("Lead Time Calculation", prec_Vendor."Lead Time Calculation");
                lrec_Item.MODIFY();
            UNTIL lrec_Item.NEXT() = 0;


        CLEAR(lrec_ItemVendor);
        lrec_ItemVendor.SETCURRENTKEY("Vendor No.", "Item No.", "Variant Code");
        lrec_ItemVendor.SETRANGE("Vendor No.", prec_Vendor."No.");
        IF lrec_ItemVendor.FINDSET() THEN
            REPEAT
                lrec_ItemVendor.VALIDATE("Lead Time Calculation", prec_Vendor."Lead Time Calculation");
                lrec_ItemVendor.MODIFY();
            UNTIL lrec_ItemVendor.NEXT() = 0;
    END;


    var

    // ESK001: Label 'Souhaitez-vous actualiser les articles avec le Code Appro "%1" ?';
}

