tableextension 50393 "Manufacturer Logic" extends Manufacturer //5720
{

    procedure EnvoyerMiniload()
    var
        LMarque: Record Manufacturer;
        cu_MiniLoad: Codeunit "Gestion MINILOAD";
    begin
        LMarque := Rec;
        LMarque.SETRECFILTER();
        CLEAR(cu_MiniLoad);
        cu_MiniLoad.TypeArticle_MsgTPR(LMarque);
    end;
}

