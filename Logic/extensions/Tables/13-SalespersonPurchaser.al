tableextension 50203 "Salesperson/Purchaser Logic" extends "Salesperson/Purchaser" //13
{
    fields
    {
        modify(City)
        {
            trigger OnAfterValidate()
            begin
                PostCode.ValidateCity(City, "Post Code", County, "Country/Region Code", ((CurrFieldNo <> 0) AND GUIALLOWED));
            end;
        }

        modify("Post Code")
        {
            trigger OnAfterValidate()
            begin
                PostCode.ValidatePostCode(City, "Post Code", County, "Country/Region Code", ((CurrFieldNo <> 0) AND GUIALLOWED));
            end;
        }

    }

    var
        PostCode: Record "Post Code";
}

