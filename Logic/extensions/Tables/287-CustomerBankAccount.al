tableextension 50046 "Customer Bank Account Logic" extends "Customer Bank Account" //287
{
    procedure "CheckRIB&IBAN"()
    var
        txt_Chaine: Text;
        Text000Qst: Label 'Le RIB ne correspond pas aux champs Etablissement, agence et N° de compte bancaire. Voulez-vous continuer ?';
        Text001Err: Label 'Veuillez corriger les informations !';
    begin
        // MCO Le 21-11-2018
        txt_Chaine := "Bank Branch No." + "Agency Code" + "Bank Account No.";
        IF STRPOS(IBAN, txt_Chaine) = 0 THEN
            IF NOT CONFIRM(Text000Qst) THEN ERROR(Text001Err);
    end;
}

