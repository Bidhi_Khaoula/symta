tableextension 50551 "Sales Receivables Setup Logic" extends "Sales & Receivables Setup" //311
{

    procedure CalculPourcentEch3(): Decimal
    begin
        IF ((100 - ("Pourcentage échéance 1" + "Pourcentage échéance 2")) < 0) OR ((100 - ("Pourcentage échéance 1" + "Pourcentage échéance 2")) > 100)
         THEN
            ERROR('Valeur incohérente');

        EXIT(100 - ("Pourcentage échéance 1" + "Pourcentage échéance 2"));
    end;
}

