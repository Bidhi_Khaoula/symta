tableextension 50132 "Report Selections Logic" extends "Report Selections" //77
{
    procedure GetEmailAddressFromVariant(RecVariant: Variant): Text[250]
    var
        EskapeCommunication: Codeunit "Eskape Communication";
        DataTypeManagement: Codeunit "Data Type Management";
        RecRef: RecordRef;
        ToAddress: Text;
        optPrinter: Option ptrEmail,ptrFax;
    begin
        // on récupére un RecordRef depuis un Variant
        DataTypeManagement.GetRecordRef(RecVariant, RecRef);
        // on appelle la fenetre de selection de contact
        ToAddress := EskapeCommunication.GetRecipient(optPrinter::ptrEmail, RecRef);

        EXIT(Format(ToAddress));
    end;
}

