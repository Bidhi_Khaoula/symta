tableextension 50206 "Customer Logic" extends Customer //18
{
    fields
    {
        modify("Code Assurance Credit")
        {

            trigger OnAfterValidate()
            begin
                // AD Le 28-08-2009 => CREDIT -> Historisation des modifications sur le credit
                rec.SaveCreditUpdate();
            end;
        }
        modify("Identif. Assurance Credit")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 28-08-2009 => CREDIT -> Historisation des modifications sur le credit
                SaveCreditUpdate();
            end;
        }
        modify("Envoi Facturation")
        {
            trigger OnBeforeValidate()
            begin
                IF "Envoi Facturation" IN ["Envoi Facturation"::"E-Mail", "Envoi Facturation"::"Papier + E-Mail"] THEN
                    TESTFIELD("E-Mail Facturation");
            end;
        }
        modify("Ville Expédition")
        {
            trigger OnAfterValidate()
            begin
                PostCode.ValidateCity("Ville Expédition", "Code postal Expédition", "County Expédition", "Country/Region Code Expédition", (CurrFieldNo <> 0) AND GUIALLOWED);
            end;
        }
        modify("Code postal Expédition")
        {
            trigger OnAfterValidate()
            begin
                PostCode.ValidatePostCode("Ville Expédition", "Code postal Expédition", "County Expédition", "Country/Region Code Expédition", (CurrFieldNo <> 0) AND GUIALLOWED);
            end;
        }

    }

    trigger OnBeforeInsert()
    begin
        Rec."Print Statements" := true;
    end;


    procedure SaveCreditUpdate()
    begin

        /*
        // AD Le 28-08-2009 => CREDIT -> Historisation des modifications sur le credit
        // --------------------------------------------------------------------------
        IF ("Credit Limit (LCY)" <> xRec."Credit Limit (LCY)") OR
           ("Code Assurance Credit" <> xRec."Code Assurance Credit") OR
           ("Identif. Assurance Credit" <> xRec."Identif. Assurance Credit") THEN
        
        HistoCred.RESET();
        HistoCred.SETRANGE("Customer No.", "No.");
        IF NOT HistoCred.FINDLAST THEN
          HistoCred."Entry No." := 1
        ELSE
          HistoCred."Entry No." += 1;
        
        HistoCred.VALIDATE("Customer No.", "No.");
        HistoCred.VALIDATE("Update Date", TODAY);
        HistoCred.VALIDATE("Update Time", TIME);
        HistoCred.VALIDATE("Update User", USERID);
        
        HistoCred.VALIDATE("Credit Limit (LCY)", "Credit Limit (LCY)");
        HistoCred.VALIDATE("Code Assurance Credit", "Code Assurance Credit");
        HistoCred.VALIDATE("Identif. Assurance Credit", "Identif. Assurance Credit");
        
        HistoCred.VALIDATE("Old Credit Limit (LCY)", xRec."Credit Limit (LCY)");
        HistoCred.VALIDATE("Old Code Assurance Credit", xRec."Code Assurance Credit");
        HistoCred.VALIDATE("Old Identif. Assurance Credit", xRec."Identif. Assurance Credit");
        
        HistoCred.Insert();
        */

    end;

    procedure FileExist(): Boolean
    var
        Lien: Record "Record Link";
    begin
        Lien.SETRANGE(Company, COMPANYNAME);
        Lien.SETFILTER("Record ID", TABLENAME + ':' + "No.");
        EXIT(Not Lien.IsEmpty());
    end;

    //Unsupported feature: Property Modification (Fields) on "DropDown(FieldGroup 1)".


    var
        PostCode: record "post code";
}

