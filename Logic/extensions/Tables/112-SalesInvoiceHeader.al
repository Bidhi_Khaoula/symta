tableextension 50309 "Sales Invoice Header Logic" extends "Sales Invoice Header" //112
{
    procedure CalcInvoiceCost(Type: Integer): Decimal
    var
        SalesInvLine: Record "Sales Invoice Line";
        Cost: Decimal;
        CostCalcMgt: Codeunit "Cost Calculation Management";
    begin
        // AD Le 18-11-2009 => Fonction qui retourne le cou global d'une facture

        // Type > 1 = Mt Marge Initial   2 = %Marge Initial  3 = Mt Marge Ajustée   4 = %Marge Ajustée

        CALCFIELDS(Amount);
        SalesInvLine.RESET();
        SalesInvLine.SETRANGE("Document No.", "No.");
        IF SalesInvLine.FINDFIRST() THEN
            REPEAT
                IF Type IN [1, 2] THEN
                    Cost += (SalesInvLine.Quantity * SalesInvLine."Unit Cost (LCY)")
                ELSE
                    Cost += CostCalcMgt.CalcSalesInvLineCostLCY(SalesInvLine);

            UNTIL SalesInvLine.NEXT() = 0;

        CASE TRUE OF
            Type IN [1, 3]:
                EXIT(Amount - Cost);
            Type IN [2, 4]:
                IF Amount <> 0 THEN
                    EXIT(ROUND(100 * (Amount - Cost) / Amount, 0.1))
                ELSE
                    EXIT(0);
            ELSE
                ERROR('Type non géré');
        END;
    end;

}

