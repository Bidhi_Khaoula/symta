tableextension 50348 "Shipping Agent Logic" extends "Shipping Agent" //291
{
    procedure PrestationParDefaut(): Code[10]
    var
        ShipAgentServive: Record "Shipping Agent Services";
    begin
        ShipAgentServive.RESET();
        ShipAgentServive.SETRANGE("Shipping Agent Code", Code);
        ShipAgentServive.SETRANGE(Default, TRUE);

        IF ShipAgentServive.FINDFIRST() THEN
            EXIT(ShipAgentServive.Code)
        ELSE
            EXIT('');
    end;

    PROCEDURE ShowComment(pCountryCode: Code[10]; pPostCode: Code[20]; pConfirm: Boolean): Boolean;
    VAR
        lShippingAgentCommentLine: Record "Shipping Agent Comment Line";
        lMessageTxtMsg: Label 'Transporteur %1:%2', Comment = '%1 %2';
        lConfirmTxtQst: Label 'Transporteur %1:%2\\Voulez vous continuer ?', Comment = '%1 code %2 comment';
        lComment: Text;
        lPostCode: Code[2];
        l_CRLFTAB: Text[2];
    BEGIN
        //CFR le 01/12/2022 => R‚gie : Commentaire transporteur
        IF (NOT GUIALLOWED()) THEN
            EXIT(TRUE);

        // pas d'alerte sur les pays ‚trangers
        IF (pCountryCode <> '') THEN
            IF (pCountryCode <> 'FR') THEN
                EXIT(TRUE);

        // Code postal sur 2 digits
        lPostCode := '';
        IF STRLEN(pPostCode) = 4 THEN lPostCode := '0' + COPYSTR(pPostCode, 1, 1);
        IF STRLEN(pPostCode) = 5 THEN lPostCode := COPYSTR(pPostCode, 1, 2);
        l_CRLFTAB[1] := 13; // CR Cariage Return
        l_CRLFTAB[2] := 10; // LF Line Feed

        lComment := '';
        lShippingAgentCommentLine.SETRANGE("Shipping Agent Code", Rec.Code);
        lShippingAgentCommentLine.SETFILTER("Beginning Date", '%1|..%2', 0D, TODAY());
        lShippingAgentCommentLine.SETFILTER("Ending Date", '%1|%2..', 0D, TODAY());
        lShippingAgentCommentLine.SETFILTER(Department, '%1|%2', '', lPostCode);
        lShippingAgentCommentLine.SETFILTER(Comment, '<>%1', '');
        IF lShippingAgentCommentLine.FINDSET() THEN
            REPEAT
                lComment := lComment + l_CRLFTAB + ' - ';
                IF (lShippingAgentCommentLine.Department <> '') THEN
                    lComment := lComment + '[' + lShippingAgentCommentLine.Department + '] ';
                lComment := lComment + lShippingAgentCommentLine.Comment;
            UNTIL lShippingAgentCommentLine.NEXT() = 0;

        IF (lComment <> '') THEN
            IF pConfirm THEN BEGIN
                IF CONFIRM(STRSUBSTNO(lConfirmTxtQst, Rec.Code, lComment)) THEN
                    EXIT(TRUE)
                ELSE
                    EXIT(FALSE);
            END
            ELSE BEGIN
                MESSAGE(lMessageTxtMsg, Rec.Code, lComment);
                EXIT(TRUE);
            END;


        EXIT(TRUE);
    END;
}

