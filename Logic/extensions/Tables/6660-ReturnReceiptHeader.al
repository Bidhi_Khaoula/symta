tableextension 50408 "Return Receipt Header Logic" extends "Return Receipt Header" //6660
{
    fields
    {
        modify("Invoice Customer No.")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 07-09-2009 => FARGROUP -> Client Facturé
                VALIDATE("Bill-to Customer No.");
            end;
        }
        modify("Material Information")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 17/05/2024 - Suite R‚gie : 50150 info mat‚riel (tous documents) >> UPERCASE
                Rec."Material Information" := UPPERCASE(Rec."Material Information");
            end;
        }

        modify("En attente de facturation")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 22/09/2021 => Régie - Suivre l'utilisateur qui a mis en attente
                IF "En attente de facturation" THEN
                    VALIDATE("User En Attente", USERID)
                ELSE
                    VALIDATE("User En Attente", '');
                MODIFY(FALSE);
            end;
        }
    }
    procedure "RécupérerFacturationCentral"()
    var
        Cust: Record Customer;
        ligneExp: Record "Return Receipt Line";
    begin

        ligneExp.RESET();
        ligneExp.SETRANGE("Document No.", "No.");
        ligneExp.SETRANGE(Type, ligneExp.Type::Item);
        ligneExp.SETFILTER(Quantity, '>0');
        ligneExp.SETRANGE("Gerer par groupement", TRUE);
        IF ligneExp.ISEMPTY THEN EXIT;


        // MC LE 04-05-2011 => On identifie le BL correspondant au regroupement centrale.
        "Regroupement Centrale" := TRUE;


        // MC Le 05-12-2013 => Ticket N° 029-201-3556 : On met à jour comme sur le BL
        Cust.GET("Centrale Active");
        IF Cust."Payment Terms Code" <> '' THEN // AD Le 14-02-2020 => Ticket 51107
            "Payment Terms Code" := Cust."Payment Terms Code";
        // FIN MC Le 05-12-2013


        MODIFY();

        IF NOT "Groupement payeur" THEN EXIT;

        // SI le groupement est payeur, récupération des infos pour la facturation
        Cust.GET("Centrale Active");
        "Bill-to Customer No." := Cust."No.";
        "Bill-to Name" := Cust.Name;
        "Bill-to Name 2" := Cust."Name 2";
        "Bill-to Address" := Cust.Address;
        "Bill-to Address 2" := Cust."Address 2";
        "Bill-to City" := Cust.City;
        "Bill-to Post Code" := Cust."Post Code";
        "Bill-to County" := Cust.County;
        "Bill-to Country/Region Code" := Cust."Country/Region Code";
        "VAT Country/Region Code" := Cust."Country/Region Code";
        "Payment Terms Code" := Cust."Payment Terms Code";

        // AD Le 26-01-2012 => Pour un grpt payeur le mode de règlement est celui de la centrale
        "Payment Method Code" := Cust."Payment Method Code";
        // FIN AD Le 26-01-2012

        MODIFY();

        ligneExp.RESET();
        ligneExp.SETRANGE("Document No.", "No.");
        IF ligneExp.FINDFIRST() THEN
            ligneExp.MODIFYALL("Bill-to Customer No.", "Centrale Active");
    end;

    procedure "MajEntierementFacturé"()
    begin

        CALCFIELDS("Somme Qté livrée non facturée");
        "Entierement facturé" := ("Somme Qté livrée non facturée" = 0);
        MODIFY();
    end;

    procedure GetAmount() dec_rtn: Decimal
    var
        _recReturnLine: Record "Return Receipt Line";
    begin
        //** Cette fonction retourne le montant total du BL **//
        dec_rtn := 0;
        CLEAR(_recReturnLine);
        _recReturnLine.SETRANGE("Document No.", "No.");
        IF _recReturnLine.FindSet() THEN
            REPEAT
                dec_rtn += (_recReturnLine."Net Unit Price" * _recReturnLine.Quantity);
            UNTIL _recReturnLine.NEXT() = 0;
    end;


    procedure SendRecords(ShowRequestForm: Boolean; SendAsEmail: Boolean)
    var
        ReportSelection: Record "Report Selections";
    begin
        ReturnRcptHeader.Copy(Rec);
        WITH ReturnRcptHeader DO BEGIN
            COPY(Rec);
            ReportSelection.SETRANGE(Usage, ReportSelection.Usage::"S.Ret.Rcpt.");
            ReportSelection.SETFILTER("Report ID", '<>0');
            ReportSelection.FIND('-');
            REPEAT
                IF NOT SendAsEmail THEN
                    REPORT.RUNMODAL(ReportSelection."Report ID", ShowRequestForm, FALSE, ReturnRcptHeader)
                ELSE
                    SendReport(ReportSelection."Report ID", ReturnRcptHeader);

            UNTIL ReportSelection.NEXT() = 0;
        END;
    end;

    local procedure SendReport(ReportId: Integer; var PReturnRcptHeader: Record "Return Receipt Header")
    var
        FileManagement: Codeunit "File Management";
        CduLFunctions: Codeunit "Codeunits Functions";
        ServerAttachmentFilePath: Text[250];
    begin
        ServerAttachmentFilePath := COPYSTR(FileManagement.ServerTempFileName('pdf'), 1, 250);
        REPORT.SAVEASPDF(ReportId, ServerAttachmentFilePath, PReturnRcptHeader);
        COMMIT();
        CduLFunctions.EmailFileFromReturnReceiptHeader(PReturnRcptHeader, ServerAttachmentFilePath);
    end;

    procedure ShowOrder()
    var
        SalesHeader: Record "Sales Header";
        SalesHeaderArchive: Record "Sales Header Archive";
    begin
        // MCO Le 21-11-2018 => Régie
        IF SalesHeader.GET(SalesHeader."Document Type"::"Return Order", "Return Order No.") THEN
            PAGE.RUN(PAGE::"Sales Return Order", SalesHeader)
        ELSE BEGIN
            SalesHeaderArchive.RESET();
            SalesHeaderArchive.SETRANGE("Document Type", SalesHeaderArchive."Document Type"::"Return Order");
            SalesHeaderArchive.SETRANGE("No.", "Return Order No.");
            IF SalesHeaderArchive.FINDLAST() THEN
                PAGE.RUN(PAGE::"Sales Return Order Archive", SalesHeaderArchive);
        END;
    end;

    var
        ReturnRcptHeader: Record "Return Receipt Header";
}

