tableextension 50411 "Sales Price Logic" extends "Sales Price" //7002
{
    fields
    {

        modify("Prix catalogue")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs.
                // ANI Le 16-12-2015 => ticket 6734 : pas d'arrondi
                "Unit Price" := ROUND("Prix catalogue" * Coefficient, 0.01);
                // FIN MC Le 19-04-2011
            end;
        }
        modify(Coefficient)
        {
            trigger OnAfterValidate()
            begin
                // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs.
                // ANI Le 16-12-2015 => ticket 6734 : pas d'arrondi
                "Unit Price" := ROUND("Prix catalogue" * Coefficient, 0.01);
                // FIN MC Le 19-04-2011
            end;
        }

    }
}

