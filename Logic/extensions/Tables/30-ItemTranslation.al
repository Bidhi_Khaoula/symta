tableextension 50049 "Item Translation Logic" extends "Item Translation" //30
{
    procedure CopyToExtendText()
    var
        ExtendTextHeader: Record "Extended Text Header";
    begin
        //-------------Cette fonction permet de copier les traductions-----------------//
        //-------------------dans les textes etendus-----------------------------------//
        //WF le 04-05-2010 => FarGroup Analyse Complémentaire Avril 2010 §1.4

        //Vérifie que le texte étendu n'existe pas pour cette traduction
        ExtendTextHeader.RESET();
        ExtendTextHeader.SETRANGE(ExtendTextHeader."Table Name", ExtendTextHeader."Table Name"::Item);
        ExtendTextHeader.SETRANGE(ExtendTextHeader."No.", "Item No.");
        ExtendTextHeader.SETRANGE(ExtendTextHeader."Language Code", "Language Code");
        IF ExtendTextHeader.FINDFIRST() THEN ERROR(Text001Lbl, "Language Code", "Item No.");

        //Ajout le texte étendu pour la traduction
        //Ajout de l'entete
        ExtendTextHeader.RESET();
        ExtendTextHeader.INIT();
        ExtendTextHeader.VALIDATE("Table Name", ExtendTextHeader."Table Name"::Item);
        ExtendTextHeader.VALIDATE("No.", "Item No.");
        ExtendTextHeader.VALIDATE("Language Code", "Language Code");
        ExtendTextHeader.VALIDATE("Text No.", 10000);
        ExtendTextHeader.INSERT(TRUE);


        //Ajout des lignes
        IF Description <> '' THEN
            AddExtendTextLine(ExtendTextHeader, Description);


        IF "Description 2" <> '' THEN
            AddExtendTextLine(ExtendTextHeader, "Description 2");
        //Fin le 04-05-2010
        IF NOT HideValidationDialog THEN
            MESSAGE(Text002Lbl, "Language Code", "Item No.");
    end;

    procedure AddExtendTextLine(Rec_Entete: Record "Extended Text Header"; Commentaire: Text[100])
    var
        ExtendTextLine: Record "Extended Text Line";
        NumLigne: Integer;
    begin
        //--------------Cette fonction permet d'ajouter une ligne pour un entete dans la table 280----------//
        ExtendTextLine.SETRANGE("Table Name", Rec_Entete."Table Name");
        ExtendTextLine.SETRANGE("No.", Rec_Entete."No.");
        ExtendTextLine.SETRANGE("Language Code", Rec_Entete."Language Code");
        ExtendTextLine.SETRANGE("Text No.", Rec_Entete."Text No.");

        IF ExtendTextLine.FINDLAST() THEN
            NumLigne := ExtendTextLine."Line No." + 10000
        ELSE
            NumLigne := 10000;

        ExtendTextLine.RESET();
        ExtendTextLine.INIT();
        ExtendTextLine.VALIDATE("Table Name", Rec_Entete."Table Name");
        ExtendTextLine.VALIDATE("No.", Rec_Entete."No.");
        ExtendTextLine.VALIDATE("Language Code", Rec_Entete."Language Code");
        ExtendTextLine.VALIDATE("Text No.", Rec_Entete."Text No.");
        ExtendTextLine.VALIDATE("Line No.", NumLigne);
        ExtendTextLine.VALIDATE(Text, Commentaire);
        ExtendTextLine.INSERT(TRUE);
    end;

    procedure SetHideValidationDialog(NewHideValidationDialog: Boolean)
    begin
        HideValidationDialog := NewHideValidationDialog;
    end;

    var
        Text001Lbl: Label 'La description %1 pour l''article %2 est déjà étendue', comment = '%1 description , %2 Item No.';
        Text002Lbl: Label 'La traduction %1 pour l''article %2 est maintenant étendue', comment = '%1  "Language Code",%2 "Item No."';
        HideValidationDialog: Boolean;
}

