tableextension 50311 "Sales Cr.Memo Header Logic" extends "Sales Cr.Memo Header" //114
{


    procedure CalcCredMemoCost(Type: Integer): Decimal
    var
        SalesCreditLine: Record "Sales Cr.Memo Line";
        CostCalcMgt: Codeunit "Cost Calculation Management";
        Cost: Decimal;
    begin
        // AD Le 18-11-2009 => Fonction qui retourne le cou global d'une facture

        // Type > 1 = Mt Marge Initial   2 = %Marge Initial  3 = Mt Marge Ajustée   4 = %Marge Ajustée

        CALCFIELDS(Amount);
        SalesCreditLine.RESET();
        SalesCreditLine.SETRANGE("Document No.", "No.");
        IF SalesCreditLine.FINDFIRST() THEN
            REPEAT
                IF Type IN [1, 2] THEN
                    Cost += (SalesCreditLine.Quantity * SalesCreditLine."Unit Cost (LCY)")
                ELSE
                    Cost += CostCalcMgt.CalcSalesCrMemoLineCostLCY(SalesCreditLine);
            UNTIL SalesCreditLine.NEXT() = 0;

        CASE TRUE OF
            Type IN [1, 3]:
                EXIT(Amount - Cost);
            Type IN [2, 4]:
                IF Amount <> 0 THEN
                    EXIT(ROUND(100 * (Amount - Cost) / Amount, 0.1))
                ELSE
                    EXIT(0);
            ELSE
                ERROR('Type non géré');
        END;
    end;

}

