tableextension 50314 "Purch. Rcpt. Line Logic" extends "Purch. Rcpt. Line" //121
{
    fields
    {
        modify("Manufacturer Code")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 26-04-2011 => Mis en commentaire pour les imports de données
                //IF "Manufacturer Code" <> xRec."Manufacturer Code" THEN
                //  IF NOT CONFIRM(Text50013, FALSE) THEN
                //    "Manufacturer Code" := xRec."Manufacturer Code";
            end;
        }
    }
}

