tableextension 50545 "Bank Acc.Line Logic" extends "Bank Acc. Reconciliation Line" //274
{
    fields
    {
        modify(Select)
        {
            trigger OnAfterValidate()
            begin
                UpdateSelection();
            end;
        }
    }

    procedure UpdateSelection()
    var
        RecLine: Record "Bank Acc. Reconciliation Line";
    begin
        IF Select THEN BEGIN
            RecLine.SETRANGE("Bank Account No.", "Bank Account No.");
            RecLine.SETRANGE("Statement No.", "Statement No.");
            RecLine.SETFILTER("Statement Line No.", '<>%1', "Statement Line No.");
            RecLine.SETRANGE(Select, TRUE);
            IF NOT RecLine.ISEMPTY THEN
                RecLine.MODIFYALL(Select, FALSE);
        END;
    end;
}

