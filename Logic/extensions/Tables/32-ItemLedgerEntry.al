tableextension 50301 "Item Ledger Entry Logic" extends "Item Ledger Entry" //32
{
    procedure GetNameSource(): Text
    var
        RecCust: Record Customer;
        RecVend: Record Vendor;
    begin
        CASE "Source Type" OF
            "Source Type"::Vendor:
                IF RecVend.GET("Source No.") THEN
                    EXIT(RecVend.Name);


            "Source Type"::Customer:
                IF RecCust.GET("Source No.") THEN
                    EXIT(RecCust.Name);
        END;

        EXIT('');
    end;

    PROCEDURE GetEnseigne(pNumber: Integer): Code[20];
    VAR
        lCustomer: Record Customer;
    BEGIN
        // CFR le 04/10/2023 - R‚gie : affichage des enseignes
        CASE "Source Type" OF
            "Source Type"::Customer:
                BEGIN
                    IF lCustomer.GET("Source No.") THEN
                        IF (pNumber = 1) THEN EXIT(lCustomer."Code Enseigne 1");
                    IF (pNumber = 2) THEN EXIT(lCustomer."Code Enseigne 2");
                END;
        END;
        EXIT('');
    END;
}

