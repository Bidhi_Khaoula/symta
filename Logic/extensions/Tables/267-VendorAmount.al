tableextension 50040 "Vendor Amount Logic" extends "Vendor Amount" //267
{
    procedure GetVendorName() rtn_txt: Text[100]
    var
        rec_vendor: Record Vendor;
    begin
        // Renvoi le nom du fournisseur.
        rtn_txt := '';
        IF rec_vendor.GET("Vendor No.") THEN
            rtn_txt := rec_vendor.Name;
    end;
}

