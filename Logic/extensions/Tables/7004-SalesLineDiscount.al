tableextension 50412 "Sales Line Discount Logic" extends "Sales Line Discount" //7004
{
    fields
    {

        modify("Line Discount 1 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Line Discount 2 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }

    }

    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // AD Le 08-12-2008 => Gestion d'une remise 1 et remise 2
        Disc1 := 1 - ("Line Discount 1 %" / 100);
        Disc2 := 1 - ("Line Discount 2 %" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;
}

