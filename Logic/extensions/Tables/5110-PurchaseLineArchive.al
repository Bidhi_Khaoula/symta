tableextension 50377 "Purchase Line Archive Logic" extends "Purchase Line Archive" //5110
{
    fields
    {
        modify("Manufacturer Code")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 26-04-2011 => Mis en commentaire pour les imports de données
                //IF "Manufacturer Code" <> xRec."Manufacturer Code" THEN
                //  IF NOT CONFIRM(Text50013, FALSE) THEN
                //    "Manufacturer Code" := xRec."Manufacturer Code";
            end;
        }
    }
}

