tableextension 50422 "Warehouse Receipt Line Logic" extends "Warehouse Receipt Line" //7317
{
    fields
    {
        modify("Direct Unit Cost Fac")
        {
            trigger OnAfterValidate()
            begin
                VALIDATE("Line Discount % Fac");
            end;
        }

        modify("Discount1 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount % Fac", UpdateTotalDiscount());
            end;
        }
        modify("Discount2 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount % Fac", UpdateTotalDiscount());
            end;
        }
        modify("Discount1 % Fac")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount % Fac", UpdateTotalDiscount());
            end;
        }
        modify("Discount2 % Fac")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount % Fac", UpdateTotalDiscount());
            end;
        }
        modify("Qte Reçue théorique")
        {
            trigger OnAfterValidate()
            begin
                IF "Qte Reçue théorique" > "Qty. Outstanding" THEN
                    ERROR(Text002Err, "Qty. Outstanding", "Item No.");
            end;
        }

    }

    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
        Disc1 := 1 - ("Discount1 % Fac" / 100);
        Disc2 := 1 - ("Discount2 % Fac" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;

    procedure GetItemDesignation(): Text[100]
    var
        LItem: Record Item;
    begin
        IF NOT LItem.GET("Item No.") THEN
            EXIT('')
        ELSE
            EXIT(LItem.Description);
    end;

    procedure GetOrderItemVendorRef(): Text[50]
    var
        LPurchLine: Record "Purchase Line";
    begin

        IF "Source Type" = 39 THEN
            IF LPurchLine.GET("Source Subtype", "Source No.", "Source Line No.") THEN
                EXIT(LPurchLine."Item Reference No.");
    end;

    procedure GetInfoLigne(_pTypeInfo: Code[20]): Decimal
    var
        LligneAchat: Record "Purchase Line";
    begin
        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT(0);

        CASE _pTypeInfo OF
            'PXBRUT':
                EXIT(LligneAchat."Direct Unit Cost");
            'REMISE1':
                EXIT(LligneAchat."Discount1 %");
            'NET1':
                EXIT(GetInfoLigne('PXBRUT') * (1 - GetInfoLigne('REMISE1') / 100));
            'TOTALNET1':
                EXIT(GetInfoLigne('NET1') * "Qty. to Receive");
            'REMISE2':
                EXIT(LligneAchat."Discount2 %");
            'NET2':
                EXIT(LligneAchat."Net Unit Cost");
            // MCO Le 24-03-2016 => Problème avec les arrondis
            //'TOTALNET2' :  EXIT(GetInfoLigne('NET2') * "Qty. to Receive");
            'TOTALNET2':
                EXIT((GetInfoLigne('PXBRUT') * (1 - GetInfoLigne('REMISE') / 100)) * "Qty. to Receive");
            'REMISE':
                EXIT(LligneAchat."Line Discount %");
        // FIN MCO Le 24-03-2016

        END;
    end;

    procedure GetInfoLigneCode(_pTypeInfo: Code[20]): Code[50]
    var
        LligneAchat: Record "Purchase Line";
    begin
        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT('');

        CASE _pTypeInfo OF
            'REFFOURN':
                EXIT(LligneAchat."Vendor Item No.");
            // MCO Le 24-03-2016 => Régie
            'FOURN':
                EXIT(LligneAchat."Buy-from Vendor No.");
        // MCO Le 24-03-2016 => Régie
        END;
    end;

    procedure GetInfoLigneTexte(_pTypeInfo: Code[20]): Text
    var
        LligneAchat: Record "Purchase Line";
    begin
        // MCO Le 24-03-2016 => Régie
        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT('');

        CASE _pTypeInfo OF
            'DESC':
                EXIT(LligneAchat.Description);
        END;
        // MCO Le 24-03-2016 => Régie
    end;

    procedure SetInfoLigne(_pTypeInfo: Code[20]; _pdecValue: Decimal)
    var
        LligneAchat: Record "Purchase Line";
        rec_PurchHeader: Record "Purchase Header";
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        cu_ReleasePurchDoc: Codeunit "Release Purchase Document";
    begin
        // MC Le 30-01-2013 => Possibilité de mettre à jour les informations de la ligne de commande d'achat

        // Demande de confirmation
        IF NOT HideValidationDialog THEN
            IF NOT CONFIRM(ESK50002Qst) THEN EXIT;

        // Message si la ligne d'achat est sur d'autres réceptions
        IF NOT HideValidationDialog THEN BEGIN
            CLEAR(rec_WhseReceiptLine);
            rec_WhseReceiptLine.SETCURRENTKEY("Source Type", "Source No.", "Source Line No.");
            rec_WhseReceiptLine.SETRANGE("Source Type", 39);
            rec_WhseReceiptLine.SETRANGE("Source No.", "Source No.");
            rec_WhseReceiptLine.SETRANGE("Source Line No.", "Source Line No.");
            IF rec_WhseReceiptLine.COUNT > 1 THEN
                MESSAGE(ESK50004Msg);
        END;

        // Récupère l'entete de commande pour rouvrir la commande
        IF NOT rec_PurchHeader.GET(rec_PurchHeader."Document Type"::Order, "Source No.") THEN EXIT;
        // Rouvre la commande
        CLEAR(cu_ReleasePurchDoc);
        cu_ReleasePurchDoc.Reopen(rec_PurchHeader);
        //COMMIT;


        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT;

        // Ne pas écraser les info d'origines
        LligneAchat.HideValidationFromFac(TRUE);
        CASE _pTypeInfo OF
            'PXBRUT':
                LligneAchat.VALIDATE("Direct Unit Cost", _pdecValue);
            'REMISE1':
                LligneAchat.VALIDATE("Discount1 %", _pdecValue);
            'REMISE2':
                LligneAchat.VALIDATE("Discount2 %", _pdecValue);
        END;
        LligneAchat.MODIFY();

        // Lance la commande
        cu_ReleasePurchDoc.RUN(rec_PurchHeader);
        IF NOT HideValidationDialog THEN
            MESSAGE(ESK50003Msg);
    end;

    procedure SetInfoLigneText(_pTypeInfo: Code[20]; _pTxtValue: Text)
    var
        LligneAchat: Record "Purchase Line";
        rec_PurchHeader: Record "Purchase Header";
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        cu_ReleasePurchDoc: Codeunit "Release Purchase Document";
    begin
        // MC Le 30-01-2013 => Possibilité de mettre à jour les informations de la ligne de commande d'achat

        // Demande de confirmation
        IF NOT HideValidationDialog THEN
            IF NOT CONFIRM(ESK50002Qst) THEN EXIT;

        // Message si la ligne d'achat est sur d'autres réceptions
        IF NOT HideValidationDialog THEN BEGIN
            CLEAR(rec_WhseReceiptLine);
            rec_WhseReceiptLine.SETCURRENTKEY("Source Type", "Source No.", "Source Line No.");
            rec_WhseReceiptLine.SETRANGE("Source Type", 39);
            rec_WhseReceiptLine.SETRANGE("Source No.", "Source No.");
            rec_WhseReceiptLine.SETRANGE("Source Line No.", "Source Line No.");
            IF rec_WhseReceiptLine.COUNT > 1 THEN
                MESSAGE(ESK50004Msg);
        END;

        // Récupère l'entete de commande pour rouvrir la commande
        IF NOT rec_PurchHeader.GET(rec_PurchHeader."Document Type"::Order, "Source No.") THEN EXIT;
        // Rouvre la commande
        CLEAR(cu_ReleasePurchDoc);
        cu_ReleasePurchDoc.Reopen(rec_PurchHeader);
        //COMMIT;


        IF NOT LligneAchat.GET(LligneAchat."Document Type"::Order, "Source No.", "Source Line No.") THEN
            EXIT;

        // Ne pas écraser les info d'origines
        LligneAchat.HideValidationFromFac(TRUE);
        CASE _pTypeInfo OF
            'DESC':
                LligneAchat.VALIDATE(Description, _pTxtValue);
        END;
        LligneAchat.MODIFY();

        // Lance la commande
        cu_ReleasePurchDoc.RUN(rec_PurchHeader);
        IF NOT HideValidationDialog THEN
            MESSAGE(ESK50003Msg);
    end;

    procedure OuvrirMultiConsultation()
    var
        LPurchHeader: Record "Purchase Header";
        FrmQuid: Page "Multi -consultation";
    begin

        FrmQuid.InitArticle("Item No.");
        IF LPurchHeader.GET("Source Subtype", "Source No.") THEN
            FrmQuid.InitFournisseur(LPurchHeader."Buy-from Vendor No.");
        FrmQuid.RUNMODAL();
    end;

    procedure GetVendorOrder(prec_WarehouseReceiptLine: Record "Warehouse Receipt Line"): Code[35]
    var
        lrec_PurchaseHeader: Record "Purchase Header";
    begin

        CLEAR(lrec_PurchaseHeader);
        lrec_PurchaseHeader.SETRANGE("No.", prec_WarehouseReceiptLine."Source No.");
        IF prec_WarehouseReceiptLine."Source Document" = prec_WarehouseReceiptLine."Source Document"::"Purchase Order" THEN
            IF lrec_PurchaseHeader.FINDFIRST() THEN
                prec_WarehouseReceiptLine."Vendor Order No." := lrec_PurchaseHeader."Vendor Order No.";

        EXIT(prec_WarehouseReceiptLine."Vendor Order No.");
    end;


    var
        Text002Err: Label 'Vous ne pouvez pas traiter plus que les %1 unités restantes, article %2.', Comment = '%1 unités , %2 article';

        ESK50002Qst: Label 'Etes vous certain de vouloir mettre à jour la ligne de commande correspondante ?';
        ESK50003Msg: Label 'Mise à jour effectuée.';
        ESK50004Msg: Label 'La ligne de commande est sur d''autres réceptions.';
        HideValidationDialog: Boolean;
}

