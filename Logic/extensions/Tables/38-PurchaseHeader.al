tableextension 50360 "Purchase Header Logic" extends "Purchase Header" //38
{
    fields
    {
        modify("Date de dépat estimée (ETD)")
        {
            trigger OnAfterValidate()
            var
                vend: record vendor;
                PurchSetup: Record "Purchases & Payables Setup";
            begin
                // AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE
                vend.get("Buy-from Vendor No.");
                IF FORMAT(Vend."Délai ETD -> ETA") <> '' THEN
                    VALIDATE("Date d'arrivée estimée (ETA)", CALCDATE(Vend."Délai ETD -> ETA", "Date de dépat estimée (ETD)"))
                ELSE BEGIN
                    PurchSetup.GET();
                    VALIDATE("Date d'arrivée estimée (ETA)", CALCDATE(PurchSetup."Délai ETD -> ETA", "Date de dépat estimée (ETD)"));
                END;
                // FIN AD Le 31-08-2009
            end;
        }
        modify("Date d'arrivée estimée (ETA)")
        {
            trigger OnAfterValidate()
            var
                PurchSetup: Record "Purchases & Payables Setup";
            begin
                // AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE
                PurchSetup.GET();
                VALIDATE("Requested Receipt Date", CALCDATE(PurchSetup."Délai ETA -> Réception", "Date d'arrivée estimée (ETA)"));
                // FIN AD Le 31-08-2009

                //WF le 03-05-2010 => Analyse Complémentaire Avril 2010 $3.4
                VALIDATE("Payment Terms Code");
                // FIN WF le 03-05-2010
            end;
        }

        modify("Type de commande")
        {
            trigger OnAfterValidate()
            begin
                RecreatePurchLines(Format(FIELDCAPTION("Type de commande")));
            end;
        }
    }
    Var
        CduGFunctions: Codeunit Functions;

    procedure InsertComment()
    var
        "Comment Line": Record "Comment Line";
        "Purch. Comment Line": Record "Purch. Comment Line";
    // NumLine: Integer;
    begin
        IF "Buy-from Vendor No." = '' THEN
            EXIT;

        "Comment Line".RESET();
        "Comment Line".SETRANGE("Table Name", "Comment Line"."Table Name"::Vendor);
        "Comment Line".SETRANGE("No.", "Buy-from Vendor No.");
        IF "Comment Line".FIND('-') THEN
            REPEAT
                "Purch. Comment Line".INIT();
                "Purch. Comment Line"."Document Type" := "Document Type";
                "Purch. Comment Line"."No." := "No.";
                "Purch. Comment Line"."Line No." := "Comment Line"."Line No.";
                "Purch. Comment Line".Date := "Comment Line".Date;
                "Purch. Comment Line".Comment := "Comment Line".Comment;
                "Purch. Comment Line".Code := "Comment Line".Code;
                "Purch. Comment Line"."Display Order" := "Comment Line"."Display Purch. Order";
                "Purch. Comment Line"."Print Order" := "Comment Line"."Print Order";
                "Purch. Comment Line".Insert();
            UNTIL "Comment Line".NEXT() = 0;
        // NumLine := "Comment Line"."Line No.";
        COMMIT();
    end;

    procedure DeleteComment()
    var
        "Purch. Comment Line": Record "Purch. Comment Line";
    begin
        "Purch. Comment Line".RESET();
        "Purch. Comment Line".SETRANGE("Document Type", "Document Type");
        "Purch. Comment Line".SETRANGE("No.", "No.");
        "Purch. Comment Line".DELETEALL();
    end;

    procedure ValidateBuyVendorNo()
    var
        "Purch. Comment Line": Record "Purch. Comment Line";
    begin
        // AD Le 04-03-2009 => MIG V5

        DeleteComment();
        InsertComment();

        //MIGV5
        IF ("Buy-from Vendor No." <> '') AND (NOT HideValidationDialog) THEN BEGIN
            "Purch. Comment Line".RESET();
            "Purch. Comment Line".SETRANGE("Document Type", "Document Type");
            "Purch. Comment Line".SETRANGE("No.", "No.");
            "Purch. Comment Line".SETRANGE("Display Order", TRUE);
            IF "Purch. Comment Line".COUNT > 0 THEN
                PAGE.RUN(PAGE::"Purch. Comment Sheet", "Purch. Comment Line");
        END
    end;

    procedure ExporterEDI()
    var
        ExportEDI: Codeunit "Gestion Cde Fou EDI";
    begin
        ExportEDI.ExportCommande("No.");
    end;

    procedure VerifNoFactureFournisseur()
    var
        rec_purch_hea: Record "Purchase Header";
        rec_Purch_Invoice: Record "Purch. Inv. Header";
    begin
        IF "Document Type" <> "Document Type"::Invoice THEN EXIT;
        IF "Vendor Invoice No." = '' THEN EXIT;

        rec_purch_hea.RESET();
        // MCO Le 01-04-2015 => Régie
        rec_purch_hea.SETRANGE("Buy-from Vendor No.", "Buy-from Vendor No.");
        // MCO Le 01-04-2015 => Régie
        rec_purch_hea.SETRANGE("Vendor Invoice No.", "Vendor Invoice No.");

        // MCO Le 01-04-2015 => Régie
        rec_Purch_Invoice.RESET();
        rec_Purch_Invoice.SETRANGE("Buy-from Vendor No.", "Buy-from Vendor No.");
        rec_Purch_Invoice.SETRANGE("Vendor Invoice No.", "Vendor Invoice No.");
        // MCO Le 01-04-2015 => Régie


        IF (rec_purch_hea.FINDFIRST()) OR (rec_Purch_Invoice.FINDFIRST()) THEN
            MESSAGE(ESK001, "Document Type", "Vendor Invoice No.", rec_purch_hea."No." + '-' + rec_Purch_Invoice."No.");
    end;

    procedure VerifNoAvoirFournisseur()
    var
        rec_purch_hea: Record "Purchase Header";
        rec_Purch_Credit: Record "Purch. Cr. Memo Hdr.";
    begin
        IF "Document Type" <> "Document Type"::"Credit Memo" THEN EXIT;
        IF "Vendor Cr. Memo No." = '' THEN EXIT;

        rec_purch_hea.RESET();
        // MCO Le 01-04-2015 => Régie
        rec_purch_hea.SETRANGE("Buy-from Vendor No.", "Buy-from Vendor No.");
        // MCO Le 01-04-2015 => Régie
        rec_purch_hea.SETRANGE("Vendor Cr. Memo No.", "Vendor Cr. Memo No.");

        // MCO Le 01-04-2015 => Régie
        rec_Purch_Credit.RESET();
        rec_Purch_Credit.SETRANGE("Buy-from Vendor No.", "Buy-from Vendor No.");
        rec_Purch_Credit.SETRANGE("Vendor Cr. Memo No.", "Vendor Cr. Memo No.");
        // MCO Le 01-04-2015 => Régie


        IF (rec_purch_hea.FINDFIRST()) OR (rec_Purch_Credit.FINDFIRST()) THEN
            MESSAGE(ESK001, "Document Type", "Vendor Cr. Memo No.", rec_purch_hea."No." + '-' + rec_Purch_Credit."No.");
    end;

    procedure CheckDelete()
    var
        PurchLine: Record "Purchase Line";
    begin
        // MCO Le 27-09-2016 => Régie : Ne pas pouvoir supprimer retour si au moins une ligne est réceptionnée
        IF "Document Type" = "Document Type"::"Return Order" THEN BEGIN
            PurchLine.SETRANGE("Document Type", "Document Type");
            PurchLine.SETRANGE("Document No.", "No.");
            PurchLine.SETFILTER("Return Qty. Shipped", '<>%1', 0);
            IF NOT PurchLine.ISEMPTY THEN
                ERROR(ESK005Err);
        END;
    end;

    procedure SetWorkDescription(NewWorkDescription: Text)
    var
        TempBlob: Record "Upgrade Blob Storage" temporary;
    begin
        //CFR le 16/12/2020 - Régie : Description du travail
        CLEAR("Work Description");
        IF NewWorkDescription = '' THEN
            EXIT;
        TempBlob.Blob := "Work Description";
        CduGFunctions.WriteAsText(NewWorkDescription, TEXTENCODING::Windows);
        "Work Description" := TempBlob.Blob;
        MODIFY();
    end;

    procedure GetWorkDescription(): Text
    var
        TempBlob: Record "Upgrade Blob Storage" temporary;
        CR: Text[1];
    begin
        //CFR le 16/12/2020 - Régie : Description du travail
        CALCFIELDS("Work Description");
        IF NOT "Work Description".HASVALUE THEN
            EXIT('');
        CR[1] := 10;
        TempBlob.Blob := "Work Description";
        EXIT(CduGFunctions.ReadAsText(CR, TEXTENCODING::Windows));
    end;

    procedure ShowVendorComment()
    var
        "Purch. Comment Line": Record "Purch. Comment Line";
    begin
        // CFR le 14/09/2021 => Régie : affichage des commentaires à l'ouverture de la commande d'achat

        IF ("Buy-from Vendor No." <> '') AND (NOT HideValidationDialog) THEN BEGIN
            "Purch. Comment Line".RESET();
            "Purch. Comment Line".SETRANGE("Document Type", "Document Type");
            "Purch. Comment Line".SETRANGE("No.", "No.");
            "Purch. Comment Line".SETRANGE("Display Order", TRUE);
            "Purch. Comment Line".SETRANGE("Document Line No.", 0);
            IF "Purch. Comment Line".COUNT > 0 THEN
                PAGE.RUNMODAL(PAGE::"Purch. Comment Sheet", "Purch. Comment Line");
        END
    end;

    PROCEDURE UpdateLineDirectUnitCost();
    VAR
        lWarehouseReceiptLine: Record "Warehouse Receipt Line";
        CONFIRM001Qst: label 'Souhaitez vous mettre à jour les coûts des lignes ouvertes en fonction de la date de commande ?';
        ERROR001: label 'Traitement annulé.\Il n''est pas possible de mettre à jour le tarif de l''article %1 (ligne %2) \car il existe des quantités re‡ues (%3) non facturées (%4).';
        lMAJPossible: Boolean;
        ERROR002: label 'Traitement annulé.\Il n''est pas possible de mettre à jour le tarif de l''article %1 (ligne %2) \car il existe une réception en cours (%3).';
        INFO001Msg: label 'Il n''y a pas de ligne à mettre à jour.';
        INFO002: label 'La mise à jour n''a pas trouvé de tarif pour les articles suivants :\%1';
        lINFO002Txt: Text;
    BEGIN
        // CFR le 10/05/2022 => Régie : UpdateLineDirectUnitCost()

        // Pas de MAJ de tarif si ce n'est pas une commande ou un devis
        IF NOT ("Document Type" IN ["Document Type"::Quote, "Document Type"::Order]) THEN
            EXIT;

        IF Rec.PurchLinesExist() THEN BEGIN

            // Confirmation...
            IF NOT CONFIRM(CONFIRM001Qst, FALSE) THEN
                EXIT;

            // Pas de MAJ de tarif si la commande n'est pas ouverte
            TESTFIELD(Status, Status::Open);

            PurchLine.RESET();
            PurchLine.SETRANGE("Document Type", Rec."Document Type");
            PurchLine.SETRANGE("Document No.", Rec."No.");
            PurchLine.SETFILTER("Outstanding Quantity", '>%1', 0);
            IF PurchLine.FINDSET() THEN BEGIN
                lMAJPossible := TRUE;
                REPEAT
                    // Pas de MAJ de tarif si la ligne n'est pas facturée (On ne passe pas par un error car le standard permet de modifier la date de commande)
                    IF (PurchLine."Quantity Received" > PurchLine."Quantity Invoiced") THEN BEGIN
                        lMAJPossible := FALSE;
                        MESSAGE(ERROR001, PurchLine."No.", PurchLine."Line No.", PurchLine."Quantity Received", PurchLine."Quantity Invoiced");
                    END;
                    // Pas de MAJ de tarif si la ligne est en cours de réception
                    lWarehouseReceiptLine.SETRANGE("Source Type", 39);
                    lWarehouseReceiptLine.SETRANGE("Source Subtype", 1);
                    lWarehouseReceiptLine.SETRANGE("Source No.", PurchLine."Document No.");
                    lWarehouseReceiptLine.SETRANGE("Source Line No.", PurchLine."Line No.");
                    IF lWarehouseReceiptLine.FINDFIRST() THEN BEGIN
                        lMAJPossible := FALSE;
                        MESSAGE(ERROR002, PurchLine."No.", PurchLine."Line No.", lWarehouseReceiptLine."No.");
                    END;
                UNTIL (PurchLine.NEXT() = 0) OR (NOT lMAJPossible);
            END
            ELSE BEGIN
                // Pas de ligne à mettre à jour
                MESSAGE(INFO001Msg);
                EXIT;
            END;

            // MAJ
            IF lMAJPossible THEN BEGIN
                lINFO002Txt := '';
                PurchLine.RESET();
                PurchLine.SETRANGE("Document Type", Rec."Document Type");
                PurchLine.SETRANGE("Document No.", Rec."No.");
                PurchLine.SETFILTER("Outstanding Quantity", '>%1', 0);
                IF PurchLine.FINDSET() THEN
                    REPEAT
                        PurchLine.UpdateDirectUnitCost(PurchLine.FIELDNO("Order Date"));
                        PurchLine.MODIFY();
                        IF (PurchLine."Direct Unit Cost" = 0) THEN
                            lINFO002Txt := lINFO002Txt + ' ' + PurchLine."No.";
                    UNTIL PurchLine.NEXT() = 0;
                // Info prix à 0
                IF (lINFO002Txt <> '') THEN
                    MESSAGE(INFO002, lINFO002Txt);
            END;
        END;
    END;

    PROCEDURE GetOutstandingAmount() rAmount: Decimal;
    VAR
        lPurchaseLine: Record "Purchase Line";
        lCurrency: Record Currency;
    BEGIN
        // CFR le 19/10/2022 => Régie : GetOutstandingAmount();
        IF Rec."Currency Code" = '' THEN
            lCurrency.InitRoundingPrecision()
        ELSE
            lCurrency.GET("Currency Code");

        rAmount := 0;
        lPurchaseLine.SETRANGE("Document Type", Rec."Document Type");
        lPurchaseLine.SETRANGE("Document No.", Rec."No.");
        IF lPurchaseLine.FINDSET() THEN
            REPEAT
                rAmount += ROUND(lPurchaseLine."Outstanding Quantity" * lPurchaseLine."Net Unit Cost ctrl fac", lCurrency."Amount Rounding Precision");
            //MESSAGE('Qté restante %1 - Cout %2 - Total %3', lPurchaseLine."Outstanding Quantity", lPurchaseLine."Net Unit Cost ctrl fac", rAmount);
            UNTIL lPurchaseLine.NEXT() = 0;

        EXIT(rAmount);
    END;

    var
        ESK001: Label '%1 %2 existe déjà (%3) !';
        ESK005Err: Label 'Impossible de supprimer le retour. Il y a eu de la réception.';
}

