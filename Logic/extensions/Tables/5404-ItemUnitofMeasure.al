tableextension 50387 "Item Unit of Measure Logic" extends "Item Unit of Measure" //5404
{
    procedure UpdateCaracteristiqueArticle()
    var
        Item: Record Item;
    begin
        // AD Le 27-10-2009 => FARGROUP -> Mise a jour des infos de la fiche articles
        Item.GET("Item No.");
        IF Code = Item."Base Unit of Measure" THEN BEGIN
            Item."Gross Weight" := Weight;
            Item."Net Weight" := Weight;
            Item."Unit Volume" := Cubage;
            // CFR Le 11/05/2022 => R‚gie : Synchro caract‚ristiques entre [Item] et [Item Unit Of Measure]
            // Attention, pas de validate car on fait la mˆme chose cot‚ Item... (donc ‚vitons la boucle infinie)
            Item.Longueur := Length;
            Item.Largeur := Width;
            // FIN CFR Le 11/05/2022
            Item.MODIFY();
        END;
        // FIN AD Le 27-10-2009
    end;
}

