tableextension 50425 "Warehouse Shpt Header Logic" extends "Warehouse Shipment Header" //7320
{
    fields
    {
        modify("Zone Code")
        {
            trigger OnBeforeValidate()
            var
                Location: Record Location;
            begin
                if "Zone Code" <> xRec."Zone Code" then begin
                    TestField(Status, Status::Open);
                    if "Zone Code" <> '' then begin
                        Location := GetLocation("Location Code");
                        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
                        IF NOT Location."Autoriser emplacement" THEN
                            // FIN MC Le 27-04-2011
                            Location.TestField("Directed Put-away and Pick");
                    end;
                end;
            end;
        }
        modify(Poids)
        {
            trigger OnAfterValidate()
            begin
                // MC Le 02-05-2011 => SYMTA -> Gestion de la préparation.
                VerifPoids();
                // FIN MC Le 02-05-2011

                // MCO Le 04-10-2018 => Régie
                //"CheckWeigth&Box"();
                // FIN MCO Le 04-10-2018
            end;
        }
        modify("Nb Of Box")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 04-10-2018 => Régie
                //"CheckWeigth&Box"();
                // FIN MCO Le 04-10-2018
            end;
        }
    }

    procedure GetSourceDoc(Rec_WhseShipmentHeader: Record "Warehouse Shipment Header"; var VSourceType: Integer; var VSourceNo: Code[20])
    var
        Rec_WhseShipmentLine: Record "Warehouse Shipment Line";
    begin
        Rec_WhseShipmentLine.RESET();
        Rec_WhseShipmentLine.SETRANGE("No.", Rec_WhseShipmentHeader."No.");
        IF Rec_WhseShipmentLine.FINDFIRST() THEN BEGIN
            VSourceType := Rec_WhseShipmentLine."Source Type";
            VSourceNo := Rec_WhseShipmentLine."Source No.";
        END ELSE BEGIN
            VSourceType := 0;
            VSourceNo := '';
        END;
    end;

    procedure VerifPoids()
    var
        WrhShipLine: Record "Warehouse Shipment Line";
        ShipPack: Record "Shipement Packaging";
        paramMagasin: Record "Warehouse Setup";
        l_WhEmployee: Record "Warehouse Employee";
        totalPoidNet: Decimal;
        PoidsMin: Decimal;
        PoidsMax: Decimal;
        // txt001: Label 'Le poids devrait être compris entre %1 et %2 !';
        txt002Qst: Label 'Attention le poids attendu devrait être compris entre %1 et %2, validez-vous le poids saisi ?', Comment = '%1 %2';
        txt003Err: Label 'Le poids saisi n''a pas été validé';
    begin

        // CFR Le 24/06/2019 => FE20190529 Limitation du contrôle du poids à quelques utilisateurs
        IF l_WhEmployee.GET(USERID, "Location Code") THEN
            IF (l_WhEmployee."Validation Logistique Direct") AND (l_WhEmployee."Pas de Contrôle Poids") THEN
                EXIT;

        // fin CFR

        // Somme le poids des articles expédiés
        WrhShipLine.SETRANGE(WrhShipLine."No.", "No.");
        IF WrhShipLine.FIND('-') THEN
            REPEAT
                totalPoidNet := totalPoidNet + ROUND(WrhShipLine."Qty. to Ship" * WrhShipLine.Weight, 0.00001);
            UNTIL WrhShipLine.NEXT() = 0;

        // Somme le poids des emballages
        ShipPack.SETRANGE(ShipPack."Sales Shipement no", "Shipping No.");
        IF ShipPack.FindFirst() THEN
            REPEAT
                totalPoidNet := totalPoidNet + ROUND(ShipPack.Qte * ShipPack.weight, 0.00001);
            UNTIL WrhShipLine.NEXT() = 0;

        paramMagasin.GET();

        PoidsMin := totalPoidNet - paramMagasin."Différence poids autorisée";
        PoidsMax := totalPoidNet + paramMagasin."Différence poids autorisée";

        IF (Poids < PoidsMin) OR (Poids > PoidsMax) THEN
            // FBO le 007-04-2017 => FE20170406
            //    MESSAGE(STRSUBSTNO(txt001,PoidsMin,PoidsMax));
            IF NOT (CONFIRM(STRSUBSTNO(txt002Qst, PoidsMin, PoidsMax))) THEN
                ERROR(txt003Err);
        // FIN FBO le 007-04-2017
    end;

    procedure CalcPoids(): Decimal
    var
        WrhShipLine: Record "Warehouse Shipment Line";
    begin
        // Somme le poids des articles expédiés
        //Poids := 0;
        WrhShipLine.RESET();

        WrhShipLine.SETRANGE(WrhShipLine."No.", "No.");
        IF WrhShipLine.FIND('-') THEN
            REPEAT
                Poids += ROUND(WrhShipLine."Qty. to Ship" * WrhShipLine.Weight, 0.00001);
            UNTIL WrhShipLine.NEXT() = 0;

        EXIT(Poids);
    end;

    procedure Get_IsDeliverySaturday(): Boolean
    begin
        //** Cette fonction permet de renvoyer si la commande vente est livrable le samedi **//
        IF "Source Document" = "Source Document"::"Sales Order" THEN BEGIN
            IF rec_SalesOrder.GET(rec_SalesOrder."Document Type"::Order, "Source No.") THEN;
            EXIT(rec_SalesOrder."Saturday Delivery");
        END;
    end;

    procedure Get_IsInsurance(): Boolean
    begin
        //** Cette fonction permet de renvoyer si la commande vente est assurée **//
        IF "Source Document" = "Source Document"::"Sales Order" THEN BEGIN
            IF rec_SalesOrder.GET(rec_SalesOrder."Document Type"::Order, "Source No.") THEN;
            EXIT(rec_SalesOrder."Insurance of Delivery");
        END;
    end;

    procedure GetDestinationName(): Text[100]
    var
        LCust: Record Customer;
    begin
        IF "Destination Type" = "Destination Type"::Customer THEN BEGIN
            LCust.GET("Destination No.");
            EXIT(Format(LCust.Name + '-' + LCust.City));
        END;
        EXIT('');
    end;

    procedure GetDestinationCountry(): Text[100]
    var
        LCust: Record Customer;
    begin
        //CFR le 15/09/2021 => Régie : ajout Code pays
        IF "Destination Type" = "Destination Type"::Customer THEN BEGIN
            LCust.GET("Destination No.");
            EXIT(LCust."Country/Region Code");
        END;
        EXIT('');
    end;

    procedure "CheckWeigth&Box"()
    var
        lShippingAgent: Record "Shipping Agent";
        WeightPerBox: Decimal;
        ESK000Err: Label 'Le transporteur %1 n''autorise pas les colis ayant un poids supérieur à %2.', Comment = '%1 %2';
        ESK001Qst: Label 'Attention, votre expédition dépasse %1 kg, elle peut être affrétée par le service transport. souhaitez vous tout de même la remettre à GE (ME)  ?', comment = '%1';
    begin
        // MCO Le 04-10-2018 => Régie
        IF lShippingAgent.GET("Shipping Agent Code") THEN
            IF "Nb Of Box" <> 0 THEN BEGIN
                WeightPerBox := Poids / "Nb Of Box";
                IF (WeightPerBox >= lShippingAgent."Poids colis maximum") AND (lShippingAgent."Poids colis maximum" <> 0) THEN
                    ERROR(Format(STRSUBSTNO(ESK000Err, lShippingAgent.Code, lShippingAgent."Poids colis maximum")));
                // CFR le 11/05/2022 => R‚gie : [Alert Package Weight]
                // CFR le 06/04/2023 => R‚gie : [Alert Package Weight] bas‚e sur le poids total et non le poids moyen
                //IF (WeightPerBox >= lShippingAgent."Alert Package Weight") AND (lShippingAgent."Alert Package Weight" <> 0) THEN
                IF (Poids >= lShippingAgent."Alert Package Weight") AND (lShippingAgent."Alert Package Weight" <> 0) THEN
                    // FIN CFR le 06/04/2023
                    IF NOT CONFIRM(STRSUBSTNO(ESK001Qst, lShippingAgent."Alert Package Weight"), FALSE)
               THEN
                        ERROR('Traitement annulé');
                // FIN CFR le 11/05/2022
            END;
    END;

    var
        rec_SalesOrder: Record "Sales Header";

}

