tableextension 50413 "Purchase Price Logic" extends "Purchase Price" //7012
{
    procedure EnregPrixNetCoture()
    var
        TempToPurchDisc: Record "Purchase Line Discount" temporary;
        // "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        "L%Rem": Decimal;
    begin
        // AD Le 06-02-2015 => Pour enregistrer les prix net au moment de la cloture du tarif
        IF "Ending Date" = 0D THEN BEGIN
            "Prix net achat" := 0;
            EXIT;
        END;

        "L%Rem" := LCodeunitsFunctions.GetPurchDisc(TempToPurchDisc, "Vendor No.", "Item No.",
            "Variant Code", "Unit of Measure Code", '',
            "Ending Date", FALSE, "Minimum Quantity", 3, '', 0);


        "Prix net achat" := "Direct Unit Cost" * (1 - "L%Rem" / 100);
    end;

    procedure GetVendorName(): Text[50]
    var
        LVendor: Record Vendor;
    begin
        IF LVendor.GET("Vendor No.") THEN
            EXIT(LVendor.Name);
    end;

    procedure GetStatutQualite(): Text[100]
    var
        lItemVendor: Record "Item Vendor";
    begin
        IF lItemVendor.GET("Vendor No.", "Item No.", "Variant Code") THEN
            EXIT(FORMAT(lItemVendor."Statut Qualité"))
        ELSE
            EXIT('');
    end;

    procedure GetStatutApprovisionnement(): Text[100]
    var
        lItemVendor: Record "Item Vendor";
    begin
        IF lItemVendor.GET("Vendor No.", "Item No.", "Variant Code") THEN
            EXIT(FORMAT(lItemVendor."Statut Approvisionnement"))
        ELSE
            EXIT('');
    end;
}

