tableextension 50414 "Purchase Line Discount Logic" extends "Purchase Line Discount" //7014
{
    fields
    {
        modify("Line Discount 1 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion d'une remise 1 et remise 2
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Line Discount 2 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion d'une remise 1 et remise 2
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
    }


    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // MC Le 09-05-2011 => Gestion d'une remise 1 et remise 2
        Disc1 := 1 - ("Line Discount 1 %" / 100);
        Disc2 := 1 - ("Line Discount 2 %" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;
}

