tableextension 50426 "Warehouse Shpt Line Logic" extends "Warehouse Shipment Line" //7321
{
    fields
    {
        modify("WIIO Préparation")
        {
            trigger OnAfterValidate()
            var
            // LItem: Record Item;
            begin
                /*
                IF "Term Missing" THEN BEGIN
                  LItem.GET("Item No.");
                  LItem.SETRANGE("Location Filter", "Location Code");
                  LItem.CALCFIELDS(Inventory);
                  VALIDATE("Stock on validation", LItem.Inventory);
                END ELSE
                  VALIDATE("Stock on validation", 0);
                */

            end;
        }
    }
    procedure OuvrirMultiConsultation()
    var
        LSalesheader: Record "Sales Header";
        FrmQuid: Page "Multi -consultation";
    begin

        FrmQuid.InitArticle("Item No.");
        IF LSalesheader.GET("Source Subtype", "Source No.") THEN
            FrmQuid.InitClient(LSalesheader."Sell-to Customer No.");
        FrmQuid.RUNMODAL();
    end;

    procedure getCommentLigneKit() r_Commentaire: Text
    var
        lSalesCommentLine: Record "Sales Comment Line";
    begin
        // CFR le 16/12/2020 : ajout commentaire ligne (kit)
        r_Commentaire := '';
        lSalesCommentLine.SETCURRENTKEY("Document Type", "No.", "Document Line No.", "Line No.");
        lSalesCommentLine.SETRANGE("Document Type", Rec."Source Subtype");
        lSalesCommentLine.SETRANGE("No.", Rec."Source No.");
        lSalesCommentLine.SETRANGE("Document Line No.", Rec."Source Line No.");
        lSalesCommentLine.SETRANGE("Print Wharehouse Shipment", TRUE);
        IF lSalesCommentLine.FINDSET() THEN
            REPEAT
                r_Commentaire := r_Commentaire + ' ' + lSalesCommentLine.Comment;
            UNTIL lSalesCommentLine.NEXT() = 0;

        EXIT(r_Commentaire);
    end;
}

