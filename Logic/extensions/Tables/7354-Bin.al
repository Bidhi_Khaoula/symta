tableextension 50430 "Bin Logic" extends Bin //7354
{
    //******************************Non utiliser***********************************
    // local procedure _CheckEmptyBinPourZone(ErrorText: Text[250])
    // var
    //     // WarehouseEntry: Record "Warehouse Entry";
    //     WarehouseJnl: Record "Warehouse Journal Line";
    //     WhseActivLine: Record "Warehouse Activity Line";
    //     WhseRcptLine: Record "Warehouse Receipt Line";
    //     WhseShptLine: Record "Warehouse Shipment Line";
    // begin
    //     // ANI Le 18-05-2016 FE20160427
    //     /*
    //     WarehouseEntry.SETCURRENTKEY("Bin Code","Location Code");
    //     WarehouseEntry.SETRANGE("Bin Code",Code);
    //     WarehouseEntry.SETRANGE("Location Code","Location Code");
    //     WarehouseEntry.CALCSUMS("Qty. (Base)");
    //     IF WarehouseEntry."Qty. (Base)" <> 0 THEN
    //       ERROR(
    //         Text000,
    //         ErrorText,TABLECAPTION,FIELDCAPTION("Location Code"),
    //         "Location Code",FIELDCAPTION(Code),Code);
    //     */
    //     WhseActivLine.SETCURRENTKEY("Bin Code", "Location Code");
    //     WhseActivLine.SETRANGE("Bin Code", Code);
    //     WhseActivLine.SETRANGE("Location Code", "Location Code");
    //     WhseActivLine.SETRANGE("Activity Type", WhseActivLine."Activity Type"::Movement);
    //     IF WhseActivLine.FINDFIRST() THEN
    //         ERROR(
    //           Text001Err,
    //           ErrorText, TABLECAPTION, FIELDCAPTION("Location Code"), "Location Code",
    //           FIELDCAPTION(Code), Code, WhseActivLine.TABLECAPTION);

    //     WarehouseJnl.SETRANGE("Location Code", "Location Code");
    //     WarehouseJnl.SETRANGE("From Bin Code", Code);
    //     IF WarehouseJnl.FINDFIRST() THEN
    //         ERROR(
    //           Text001Err,
    //           ErrorText, TABLECAPTION, FIELDCAPTION("Location Code"), "Location Code",
    //           FIELDCAPTION(Code), Code, WarehouseJnl.TABLECAPTION);

    //     WarehouseJnl.RESET();
    //     WarehouseJnl.SETCURRENTKEY("To Bin Code", "Location Code");
    //     WarehouseJnl.SETRANGE("To Bin Code", Code);
    //     WarehouseJnl.SETRANGE("Location Code", "Location Code");
    //     IF WarehouseJnl.FINDFIRST() THEN
    //         ERROR(
    //           Text001Err,
    //           ErrorText, TABLECAPTION, FIELDCAPTION("Location Code"), "Location Code",
    //           FIELDCAPTION(Code), Code, WarehouseJnl.TABLECAPTION);

    //     WhseRcptLine.SETCURRENTKEY("Bin Code", "Location Code");
    //     WhseRcptLine.SETRANGE("Bin Code", Code);
    //     WhseRcptLine.SETRANGE("Location Code", "Location Code");
    //     IF WhseRcptLine.FINDFIRST() THEN
    //         ERROR(
    //           Text001Err,
    //           ErrorText, TABLECAPTION, FIELDCAPTION("Location Code"), "Location Code",
    //           FIELDCAPTION(Code), Code, WhseRcptLine.TABLECAPTION);

    //     WhseShptLine.SETCURRENTKEY("Bin Code", "Location Code");
    //     WhseShptLine.SETRANGE("Bin Code", Code);
    //     WhseShptLine.SETRANGE("Location Code", "Location Code");
    //     IF WhseShptLine.FINDFIRST() THEN
    //         ERROR(
    //           Text001Err,
    //           ErrorText, TABLECAPTION, FIELDCAPTION("Location Code"), "Location Code",
    //           FIELDCAPTION(Code), Code, WhseShptLine.TABLECAPTION);

    // end;
    //******************************Non utiliser***********************************
    var
    // Text001Err: Label 'You cannot %1 the %2 with %3 = %4, %5 = %6, because one or more %7 exists for this %2.', Comment = 'You cannot %1 the %2 with %3 = %4, %5 = %6, because one or more %7 exists for this %2.';
}

