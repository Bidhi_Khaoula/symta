tableextension 50059 "Excel Buffer Logic" extends "Excel Buffer" //370
{

    procedure OnlyCreateBook(SheetName: Text[250]; ReportHeader: Text[80]; CompanyName: Text[30]; UserID2: Text; BookCreated: Boolean)
    begin
        //>> SFD20201005

        ExcelBookCreated := BookCreated;
        NewSheetName := SheetName;

        IF NOT BookCreated THEN
            CreateBook('', SheetName);

        Rec.SetCurrent(0, 0);

        WriteSheet(ReportHeader, CompanyName, UserID2);
        //<< SFD20201005
    end;

    procedure OnlyOpenExcel()
    begin
        //>> SFD20201005
        CloseBook();
        OpenExcel();
        // GiveUserControl;
        //<< SFD20201005
    end;

    var
        ExcelBookCreated: Boolean;
        NewSheetName: Text;
}

