tableextension 50392 "Item Reference Logic" extends "Item Reference" //5777
{

    procedure VerifRefExiste(Type: Code[1])
    var
        ItemCrossRef2: Record "Item Reference";
        Item: Record Item;
        RRef1: RecordRef;
        RRef2: RecordRef;
    begin
        // -------------------------------------------------------------------------------------
        // Cette fonction permet de vérifier si il existe déjà cette référence Origine ou active
        // -------------------------------------------------------------------------------------


        // AD Le 13-10-2010 => Franck -> Ne pas créer une ref avec un gencode
        IF ("Reference Type" <> "Reference Type"::Dbx) AND ("Reference No." <> '') THEN
            IF STRLEN("Reference No.") <= 13 THEN BEGIN
                Item.RESET();
                Item.SETRANGE("Gencod EAN13", "Reference No.");
                IF Item.FINDFIRST() THEN
                    ERROR(ESK5002Err, Item."No. 2", "Reference No.");
            END;
        // FIN AD Le 13-10-2010

        ItemCrossRef2.RESET();

        /* AD Le 01-04-2015 => Demande de Nath.
        IF ("Reference Type" IN ["Reference Type"::Vendor, "Reference Type"::Achat]) THEN
          ItemCrossRef2.SETFILTER("Reference Type", '<>%1&<>%2&<>%3', "Reference Type"::Vendor, "Reference Type"::Achat,
                "Reference Type"::Dbx);
        */

        //ItemCrossRef2.SETFILTER("Reference Type", '%1|%2',
        //      ItemCrossRef2."Reference Type"::Origine, ItemCrossRef2."Reference Type"::Active);
        ItemCrossRef2.SETRANGE("Reference No.", "Reference No.");
        IF ItemCrossRef2.FINDFIRST() THEN BEGIN
            RRef1.GETTABLE(ItemCrossRef2);
            RRef2.GETTABLE(Rec);
            ERROR(ESK5000Err, "Reference No.", ItemCrossRef2."Item No.");
        END;

    end;

    procedure "OnInsert.Eskape"()
    begin

        // ESKAPE => Traçabilité des enregistrements
        IF STRLEN(USERID) <= MAXSTRLEN("Create User ID") THEN
            Evaluate("Create User ID", USERID);
        "Create Date" := TODAY;
        "Create Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    procedure "OnModify.Eskape"()
    begin
        // ESKAPE => Traçabilité des enregistrements
        IF STRLEN(USERID) <= MAXSTRLEN("Create User ID") THEN
            Evaluate("Modify User ID", USERID);
        "Modify Date" := TODAY;
        "Modify Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    procedure SetHideControl(NewHideValidationDialog: Boolean)
    begin
        HideValidationDialog := NewHideValidationDialog;
    end;

    procedure GETHideControl(): Boolean
    begin
        exit(HideValidationDialog);
    end;

    protected var
        ESK5000Err: Label 'La référence %1 existe déjà pour l''article %2.', Comment = 'La référence %1 article %2';
        ESK5002Err: Label 'Cette référence [%2] correspond au gencod de l''article %1.', Comment = 'La référence %1 article %2';
        HideValidationDialog: Boolean;
}

