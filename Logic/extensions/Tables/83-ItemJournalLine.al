tableextension 50335 "Item Journal Line Logic" extends "Item Journal Line" //83
{
    fields
    {
        modify("Recherche référence")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
                GestionMultiReference: Codeunit "Gestion Multi-référence";
                NoRef: Code[30];
            begin
                // AD Le 11-12-2009 => GDI -> Recherche de la référence
                NoRef := GestionMultiReference.RechercheMultiReference("Recherche référence");
                IF NoRef <> 'RIENTROUVE' THEN
                    VALIDATE("Item No.", NoRef)
                ELSE BEGIN
                    LItem.RESET();
                    LItem.SETCURRENTKEY("No. 2");
                    LItem.SETFILTER("No. 2", "Recherche référence" + '*');
                    IF PAGE.RUNMODAL(PAGE::"Item List", LItem) = ACTION::LookupOK THEN BEGIN
                        CLEAR(Description);
                        CLEAR("No.");
                        "Recherche référence" := LItem."No. 2";
                        VALIDATE("No.", LItem."No.");
                    END
                    ELSE BEGIN
                        CLEAR(Description);
                        CLEAR("No.");
                        "Recherche référence" := '';
                    END;
                END;

                // AD Le 11-12-2009
            end;
        }
    }

    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        FrmQuid.InitArticle("Item No.");
        FrmQuid.RUN();
    end;
}

