tableextension 50427 "Posted Whse. Shpt Hdr Logic" extends "Posted Whse. Shipment Header" //7322
{

    procedure VerifPoids(pdec_Poids: Decimal)
    var
        WrhShipLine: Record "Posted Whse. Shipment Line";
        paramMagasin: Record "Warehouse Setup";
        totalPoidNet: Decimal;
        PoidsMin: Decimal;
        PoidsMax: Decimal;
        txt001Msg: Label 'Le poids devrait être compris entre %1 et %2 !', Comment = '%1 %2';
    begin

        // Somme le poids des articles expédiés
        WrhShipLine.SETRANGE(WrhShipLine."No.", "No.");
        IF WrhShipLine.FIND('-') THEN
            REPEAT
                totalPoidNet := totalPoidNet + ROUND(WrhShipLine.Quantity * WrhShipLine.Weight, 0.00001);
            UNTIL WrhShipLine.NEXT() = 0;

        paramMagasin.GET();

        PoidsMin := totalPoidNet - paramMagasin."Différence poids autorisée";
        PoidsMax := totalPoidNet + paramMagasin."Différence poids autorisée";

        IF (pdec_Poids < PoidsMin) OR (pdec_Poids > PoidsMax) THEN
            MESSAGE(STRSUBSTNO(txt001Msg, PoidsMin, PoidsMax));
    end;

    procedure CalcPoids() rtn_dec: Decimal
    var
        WrhShipLine: Record "Posted Whse. Shipment Line";
    begin
        // Somme le poids des articles expédiés
        rtn_dec := 0;
        WrhShipLine.RESET();

        WrhShipLine.SETRANGE(WrhShipLine."No.", "No.");
        IF WrhShipLine.FIND('-') THEN
            REPEAT
                rtn_dec += ROUND(WrhShipLine.Quantity * WrhShipLine.Weight, 0.00001);
            UNTIL WrhShipLine.NEXT() = 0;
    end;
}

