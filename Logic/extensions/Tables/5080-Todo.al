tableextension 50373 "To-do Logic" extends "To-do" //5080
{
    fields
    {
        modify("Durée Naviway")
        {
            trigger OnAfterValidate()
            begin

                IF "Durée Naviway" < 0 THEN
                    ERROR(Text005Err);

                IF ("Durée Naviway" * 60 * 1000) < (60 * 1000) THEN
                    ERROR(Text007Err);

                IF ("Durée Naviway" * 60 * 1000) > (CREATEDATETIME(TODAY + 3650, 0T) - CREATEDATETIME(TODAY, 0T)) THEN
                    ERROR(Text008Err);

                IF "Durée Naviway" <> xRec."Durée Naviway" THEN
                    VALIDATE(Duration, ("Durée Naviway" * 60 * 1000));
            end;
        }
    }
    var
        Text005Err: Label 'Information that you have entered in this field will cause the duration to be negative which is not allowed. Please modify the ending date/time value.';
        Text007Err: Label 'Information that you have entered in this field will cause the duration to be less than 1 minute, which is not allowed. Please modify the ending date/time value.';
        Text008Err: Label 'Information that you have entered in this field will cause the duration to be more than 10 years, which is not allowed. Please modify the ending date/time value.';

}

