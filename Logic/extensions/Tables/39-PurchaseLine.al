tableextension 50362 "Purchase Line Logic" extends "Purchase Line" //39
{
    fields
    {
        modify("Initial Quantity")
        {
            trigger OnAfterValidate()
            begin
            end;
        }

        modify("% Facture HK")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                "Calc%CoutIndirect"();
            end;
        }
        modify("% Assurance")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                "Calc%CoutIndirect"();
            end;
        }
        modify("% Coef Réception")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                "Calc%CoutIndirect"();
            end;
        }
        modify("% Cout Indirect Article")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                "Calc%CoutIndirect"();
            end;
        }
        modify("Frais Généraux Article")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                CalcMtFraisGénéraux();
            end;
        }
        modify("Frais LCY")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                CalcMtFraisGénéraux();
            end;
        }
        modify("Frais FCY")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 22-09-2009 => FARGROUP -> Cout
                CalcMtFraisGénéraux();
            end;
        }
        modify("Recherche référence")
        {

            trigger OnAfterValidate()
            begin
                // AD Le 11-12-2009 => GDI -> Recherche de la référence
                IF Type <> Type::Item THEN
                    VALIDATE("No.", "Recherche référence")
                ELSE
                    VALIDATE("No.", GestionMultiReference.RechercheMultiReference("Recherche référence"));
                // AD Le 11-12-2009
            end;
        }
        modify("Discount1 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Discount2 %")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }

        modify("Net Unit Cost")
        {
            trigger OnAfterValidate()
            var
                Currency: record Currency;
            begin
                Currency.get(rec."Currency Code");
                // MC Le 15-09-2011 => Gestion des prix/Remise ESKAPE
                IF "Line Discount %" <> 0 THEN BEGIN
                    "Net Unit Cost" := "Direct Unit Cost" - ("Direct Unit Cost" * "Line Discount %") / 100;
                    "Net Unit Cost" := ROUND("Net Unit Cost", Currency."Amount Rounding Precision")
                END
                ELSE
                    "Net Unit Cost" := ROUND("Direct Unit Cost", Currency."Amount Rounding Precision");

                IF NOT HideValidationctrlfac THEN
                    "Net Unit Cost ctrl fac" := "Net Unit Cost";
            end;
        }
        modify("Manufacturer Code")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 26-04-2011 => Mis en commentaire pour les imports de données
                //IF "Manufacturer Code" <> xRec."Manufacturer Code" THEN
                //  IF NOT CONFIRM(Text50013, FALSE) THEN
                //    "Manufacturer Code" := xRec."Manufacturer Code";
            end;
        }

        modify("Qty to attach")
        {
            trigger OnAfterValidate()
            begin
                // MC Le 14-09-2011 => Test le dépassement de quantité.
                CALCFIELDS("Qty On Fac Receipt", "Qty On Receipt Order Manual");
                IF "Qty to attach" > "Outstanding Quantity" - "Qty On Fac Receipt" - "Qty On Receipt Order Manual" THEN
                    ERROR(Format(STRSUBSTNO(ESK20116Err, "Outstanding Quantity", "Qty On Fac Receipt" + "Qty On Receipt Order Manual")));
            end;
        }
        modify("Demande retour")
        {
            Trigger OnAfterValidate()
            BEGIN
                // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
                SetWarehouseEntryComment();
            END;
        }
    }



    procedure "Insert.Eskape"()
    begin
        InsertCommentaire()// AD Le 18-03-2016
    end;

    procedure "ValidateNo.Eskape"()
    var
        CommentLine: Record "Comment Line";
        ItemVendor: Record "Item Vendor";
        PurchParam: Record "Purchases & Payables Setup";
        RecBin: Record Bin;
        REcBinContent: Record "Bin Content";
        recL_Item: Record Item;
        Item: Record Item;
        PurchHeader: Record "Purchase Header";
        FrmCommentLine: Page "Comment Sheet";
        BinCode: Code[20];
    begin
        CASE Type OF
            Type::Item:
                BEGIN
                    Item := GetItem();


                    // AD Le 26-08-2009 => Gestion avancé du bloque

                    IF Item."Process Blocked" IN [Item."Process Blocked"::Achats, Item."Process Blocked"::"Ventes & Achats"] THEN
                        ERROR(ESK50110Err, Item."No.");
                    // FIN AD Le 26-08-2009

                    // AD Le 22-09-2009 => FARGROUP -> Cout
                    "Frais Généraux Article" := Item."Overhead Rate";
                    "% Cout Indirect Article" := Item."Indirect Cost %";
                    // FIN AD Le 22-09-2009

                    // AD Le 13-09-2011 => SYMTA
                    // MCO Le 01-04-2015 => Régie
                    // Demande de nathalie on cherche pour tous les fournisseurs
                    // Item.GestionPlusFourni("Variant Code", "Buy-from Vendor No.");
                    Item.GestionPlusFourni("Variant Code", '');
                    // MCO Le 01-04-2015 => Régie
                    // FIN AD Le 13-09-2011

                    // AD Le 22-11-2011 =>
                    IF "Recherche référence" = '' THEN
                        "Recherche référence" := Item."No. 2";
                    // FIN AD Le 22-11-2011 =>

                    InsertCommentaire();// AD Le 18-03-2016

                    IF ("Document Type" = "Document Type"::Order) THEN BEGIN
                        // AD Le 10-06-2010 => GDI -> Commentaire article
                        CommentLine.RESET();
                        CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
                        CommentLine.SETRANGE("No.", "No.");
                        CommentLine.SETRANGE("Display Purch. Order", TRUE);
                        IF CommentLine.COUNT > 0 THEN BEGIN
                            CLEAR(FrmCommentLine);
                            FrmCommentLine.SETTABLEVIEW(CommentLine);
                            FrmCommentLine.EDITABLE(FALSE);
                            // AD Le 13-01-2011
                            //FrmCommentLine.RUNMODAL(); => CODE D'ORIGINE
                            FrmCommentLine.RUN();
                            // FIN AD Le 13-01-2011
                        END;

                        // AD Le 13-09-2011 => SYMTA
                        IF NOT ItemVendor.GET("Buy-from Vendor No.", "No.", "Variant Code") THEN
                            MESSAGE(ESK001Msg, "No.", "Buy-from Vendor No.")
                        ELSE
                            ItemVendor.MessageAlerte();
                        // FIN AD Le 13-09-2011

                    END;
                    // FIN AD Le 10-06-2010


                    // GB Le 18/09/2017 => Reprise des developpements perdu NAv 2009->2015  FR20150608
                    IF "Document Type" = "Document Type"::"Return Order" THEN BEGIN
                        CLEAR(PurchParam);
                        PurchHeader.get(rec."Document Type", rec."Document No.");
                        PurchParam.GET();
                        Evaluate(BinCode, 'RET-' + PurchHeader."Buy-from Vendor No."); // MCO Le 21-11-2017
                        CLEAR(RecBin);
                        IF NOT RecBin.GET(PurchParam."Magasin Retour", BinCode) THEN begin
                            CLEAR(RecBin);
                            WITH RecBin DO BEGIN
                                INIT();
                                VALIDATE("Location Code", PurchParam."Magasin Retour");
                                VALIDATE(Code, BinCode);
                                // MCO Le 21-11-2017
                                // CFR le 16/04/2024 - R‚gie : Gestion du champ 7354.50020 "Return Vendor Name"
                                VALIDATE("Return Vendor Name", PurchHeader."Buy-from Vendor Name");
                                INSERT(TRUE);
                                COMMIT();
                            END;
                        END;

                        CLEAR(REcBinContent);
                        REcBinContent.SETRANGE("Location Code", PurchParam."Magasin Retour");
                        REcBinContent.SETRANGE("Bin Code", BinCode);// MCO Le 21-11-2017
                        REcBinContent.SETRANGE("Item No.", "No.");
                        REcBinContent.SETRANGE("Variant Code", '');
                        // MCO Le 13-02-2018 => Régie
                        // MCO Le 16-07-2018 => FE20180612_B - SYMTA – Gestion des retours achats
                        //REcBinContent.SETRANGE("Unit of Measure Code", "Unit of Measure Code");
                        recL_Item.GET("No.");
                        REcBinContent.SETRANGE("Unit of Measure Code", recL_Item."Base Unit of Measure");
                        // FIN MCO Le 16-07-2018 => FE20180612_B - SYMTA – Gestion des retours achats
                        // MCO Le 13-02-2018 => Régie
                        IF REcBinContent.ISEMPTY THEN
                            WITH REcBinContent DO BEGIN
                                CLEAR(REcBinContent);
                                INIT();
                                VALIDATE("Location Code", PurchParam."Magasin Retour");
                                VALIDATE("Bin Code", BinCode);// MCO Le 21-11-2017
                                VALIDATE("Item No.", "No.");
                                // MCO Le 13-02-2018 => Régie
                                // MCO Le 16-07-2018 => FE20180612_B - SYMTA – Gestion des retours achats
                                // VALIDATE("Unit of Measure Code", "Unit of Measure Code");
                                recL_Item.GET("No.");
                                VALIDATE("Unit of Measure Code", recL_Item."Base Unit of Measure");
                                // FIN MCO Le 16-07-2018 => FE20180612_B - SYMTA – Gestion des retours achats
                                // MCO Le 13-02-2018 => Régie
                                VALIDATE("Variant Code", '');
                                VALIDATE(Fixed, TRUE); // MCO Le 21-11-2017 => Demande de SYMTA
                                INSERT(TRUE);
                                COMMIT();
                            END;

                        VALIDATE("Location Code Temp", PurchParam."Magasin Retour");
                        VALIDATE("Bin Code Temp", BinCode);// MCO Le 21-11-2017

                    END;
                    //FIN GB Le 18/09/2017

                END;
        END;
    end;

    procedure FctGetArrondirQteValue(): Boolean
    begin
        exit(gFromArrondirQte);
    end;

    procedure GetShortCloseLine(): boolean
    begin
        Exit(ShortCloseLine);
    end;

    procedure ShortClose()
    var
        SauvRecPourPrix: Record "Purchase Line";
        QtyToRcve: Decimal;
    begin
        // AD Le 28-08-2009 => Possibilité de solder la ligne d'origine
        QtyToRcve := "Qty. to Receive";
        ShortCloseLine := TRUE;
        SuspendStatusCheck(TRUE);
        //fbrun le 26/03/20 ticket 50940
        SauvRecPourPrix := Rec;

        VALIDATE("Initial Quantity", Quantity);
        VALIDATE(Quantity, "Qty. to Receive" + "Quantity Received");
        //fbrun le 26/03/20 ticket 50940
        VALIDATE("Direct Unit Cost", SauvRecPourPrix."Direct Unit Cost");
        VALIDATE("Discount1 %", SauvRecPourPrix."Discount1 %");
        VALIDATE("Discount2 %", SauvRecPourPrix."Discount2 %");
        //fin frun

        VALIDATE("Qty. to Receive", QtyToRcve);
        // MCO KLe 10-02-2017
        InitOutstanding();
        // FIN MCO Le 10-02-2017
        SuspendStatusCheck(FALSE);
    end;

    procedure "Calc%CoutIndirect"()
    begin
        // AD Le 22-09-2009 => FARGROUP -> Cout
        VALIDATE("Indirect Cost %", "% Facture HK" + "% Assurance" + "% Coef Réception" + "% Cout Indirect Article");
    end;

    procedure "CalcMtFraisGénéraux"()
    var
        PurchHeader: Record "Purchase Header";
        currency: Record currency;
        GLSetup: Record "General Ledger Setup";
        CurrExchRate: Record "Currency Exchange Rate";
    begin
        GetPurchHeader(PurchHeader, currency);
        //PurchHeader.TESTFIELD("Currency Factor");
        GLSetup.Get();

        IF PurchHeader."Currency Factor" <> 0 THEN BEGIN
            VALIDATE("Overhead Rate",
                           "Frais Généraux Article" + // Standard Nav
                           "Frais LCY" + // Frais en EURO

                  // FRAIS EN DEVIS CONVERSION EN EUROS
                  ROUND(
                    CurrExchRate.ExchangeAmtFCYToLCY(
                      GetDate(), "Currency Code",
                      "Frais FCY", PurchHeader."Currency Factor"),
                      GLSetup."Unit-Amount Rounding Precision"));
            // FIN

            // AD Le 23-11
            "Frais FCY -> LCY" :=
                  ROUND(
                    CurrExchRate.ExchangeAmtFCYToLCY(
                      GetDate(), "Currency Code",
                      "Frais FCY", PurchHeader."Currency Factor"),
                      GLSetup."Unit-Amount Rounding Precision");

        END
        ELSE
            VALIDATE("Overhead Rate",
                           "Frais Généraux Article" + // Standard Nav
                           "Frais LCY" + // Frais en EURO
                           "Frais FCY");              // FRAIS EN DEVIS CONVERSION EN EUROS

        //IF UPPERCASE(USERID) = 'ESKAPE' THEN ERROR('ad');
    end;

    procedure GetStatutMarchandise(): Text[30]
    var
        PurchHeader: Record "Purchase Header";
    begin
        // Retourne le statut de la marchandise
        PurchHeader.GET("Document Type", "Document No.");
        EXIT(FORMAT(PurchHeader."Etat Marchandise"));
    end;

    procedure GetDateHeader(type: Integer): Date
    var
        PurchHeader: Record "Purchase Header";
    begin
        // Retourne les dates de l'entete sans faire suivre dans les lignes
        PurchHeader.GET("Document Type", "Document No.");
        CASE type OF
            1:
                EXIT(PurchHeader."Date de dépat estimée (ETD)"); // DATE ETD
            2:
                EXIT(PurchHeader."Date d'arrivée estimée (ETA)"); // DATE ETA
                                                                  // CFR le 10/05/2022 => R‚gie : GetDateHeader : ajout champ 50002
            3:
                EXIT(PurchHeader."Order Dispatch Date");
        END;
    end;

    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
        Disc1 := 1 - ("Discount1 %" / 100);
        Disc2 := 1 - ("Discount2 %" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;

    procedure HideValidationFromFac(NewHideValidationctrlfac: Boolean)
    begin
        HideValidationctrlfac := NewHideValidationctrlfac;
    end;

    procedure GetHideValidationFromFac(): Boolean
    begin
        exit(HideValidationctrlfac);
    end;

    procedure "--- TRACABILITE ---"()
    begin
        //** Ensemble de fonction permettant de connaitre ou se trouve les quantité dans le processus de réception **//
    end;

    procedure GetQtyOnDepart() dec_rtn: Decimal
    var
        rec_WhseReceipt: Record "Warehouse Receipt Line";
    begin
        //** Cette fonction renvoit la totalité de la quantité en préparation si la ligne se trouve sur une pré-réception **//

        // Initialisation de la valeur de retour.
        dec_rtn := 0;

        // Parcours les lignes de réceptions magasins.
        WITH rec_WhseReceipt DO BEGIN
            SETRANGE("Source No.", Rec."Document No.");
            SETRANGE("Source Line No.", Rec."Line No.");
            SETRANGE("Source Document", "Source Document"::"Purchase Order");
            IF FINDSET() THEN
                REPEAT
                    dec_rtn += "Qty. to Receive";
                UNTIL NEXT() = 0;
        END;
    end;

    procedure GetQtyOnMagasin() dec_rtn: Decimal
    var
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        rec_WhseReceiptHeader: Record "Warehouse Receipt Header";
    begin
        //** Cette fonction renvoit la totalité de la quantité en préparation si la ligne se trouve sur une pré-réception  **//
        //                  *** Et que le champ [Positon] de la pré-réception dispose de la valeur 'M'                   ***//
        // Initialisation de la valeur de retour.
        dec_rtn := 0;

        // Parcours les lignes de réceptions magasins.
        WITH rec_WhseReceiptLine DO BEGIN
            SETRANGE("Source No.", Rec."Document No.");
            SETRANGE("Source Line No.", Rec."Line No.");
            SETRANGE("Source Document", "Source Document"::"Purchase Order");
            IF FINDSET() THEN
                REPEAT
                    // Récupère l'en-tête
                    IF rec_WhseReceiptHeader.GET("No.") THEN
                        IF rec_WhseReceiptHeader.Position = rec_WhseReceiptHeader.Position::"En magasin" THEN
                            dec_rtn += "Qty. to Receive";
                UNTIL NEXT() = 0;
        END;
    end;

    procedure GetQtyOnReceipt() dec_rtn: Decimal
    var
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        rec_WhseReceiptHeader: Record "Warehouse Receipt Header";
    begin
        //** Cette fonction renvoit la quantité totale si la récéption est en pré-pointage **//

        // Initialisation de la valeur de retour.
        dec_rtn := 0;

        // Parcours les lignes de réceptions magasins.
        WITH rec_WhseReceiptLine DO BEGIN
            SETRANGE("Source No.", Rec."Document No.");
            SETRANGE("Source Line No.", Rec."Line No.");
            SETRANGE("Source Document", "Source Document"::"Purchase Order");
            IF FINDSET() THEN
                REPEAT
                    // Récupère l'en-tête
                    // Gestion des fusions
                    IF "N° Fusion Réception" <> '' THEN BEGIN
                        IF NOT rec_WhseReceiptHeader.GET("N° Fusion Réception") THEN
                            rec_WhseReceiptHeader.GET("No.");
                    END
                    ELSE
                        rec_WhseReceiptHeader.GET("No.");

                    IF rec_WhseReceiptHeader."En réception" THEN
                        dec_rtn += "Qty. to Receive";
                UNTIL NEXT() = 0;
        END;
    end;

    procedure GetIfPointage() bln_rtn: Boolean
    var
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        rec_PreReceipt: Record "Saisis Réception Magasin";
    begin
        //** Cette fonction renvoit la quantité pointé si la ligne se trouve sur une pré-réception pour laquelle il y'a des pointages  **//

        // Initialisation de la valeur de retour.
        bln_rtn := FALSE;

        // Parcours les lignes de réceptions magasins.
        WITH rec_WhseReceiptLine DO BEGIN
            SETRANGE("Source No.", Rec."Document No.");
            SETRANGE("Source Line No.", Rec."Line No.");
            SETRANGE("Source Document", "Source Document"::"Purchase Order");
            IF FINDSET() THEN
                REPEAT
                    // Récupère la ligne de pré-réception
                    // Si c'est une fusion on cherche la fusion sinon on stocke le numéro de réception.
                    IF rec_WhseReceiptLine."N° Fusion Réception" <> '' THEN
                        rec_PreReceipt.SETRANGE("No.", rec_WhseReceiptLine."N° Fusion Réception")
                    ELSE
                        rec_PreReceipt.SETRANGE("No.", "No.");

                    rec_PreReceipt.SETRANGE("Item No.", "Item No.");
                    IF rec_PreReceipt.FINDSET() THEN
                        REPEAT
                            bln_rtn := TRUE;
                        UNTIL rec_PreReceipt.NEXT() = 0;
                UNTIL NEXT() = 0;
        END;
    end;

    procedure GetQuantityPointage() dec_return: Decimal
    var
        rec_WhseReceiptLine: Record "Warehouse Receipt Line";
        rec_PreReceipt: Record "Saisis Réception Magasin";
    begin
        // CFR le 16/12/2020 - Régie : Afficher la quantité pointée en saisie réception

        //** Cette fonction renvoit la quantité pointé si la ligne se trouve sur une pré-réception pour laquelle il y a des pointages  **//
        // Initialisation de la valeur de retour.
        dec_return := 0;

        // Parcours les lignes de réceptions magasins.
        WITH rec_WhseReceiptLine DO BEGIN
            SETRANGE("Source No.", Rec."Document No.");
            SETRANGE("Source Line No.", Rec."Line No.");
            SETRANGE("Source Document", "Source Document"::"Purchase Order");
            IF FINDSET() THEN
                REPEAT
                    // Récupère la ligne de pré-réception
                    // Si c'est une fusion on cherche la fusion sinon on stocke le numéro de réception.
                    IF rec_WhseReceiptLine."N° Fusion Réception" <> '' THEN
                        rec_PreReceipt.SETRANGE("No.", rec_WhseReceiptLine."N° Fusion Réception")
                    ELSE
                        rec_PreReceipt.SETRANGE("No.", "No.");

                    rec_PreReceipt.SETRANGE("Item No.", "Item No.");
                    IF rec_PreReceipt.FINDSET() THEN
                        REPEAT
                            dec_return += rec_PreReceipt.Quantity;
                        UNTIL rec_PreReceipt.NEXT() = 0;
                UNTIL NEXT() = 0;
        END;
    end;

    procedure "-- FIN TRACABILITE --"()
    begin
    end;

    procedure ShowSubstitutions()
    var
        ItemSubstitution: Record "Item Substitution";
        ItemSubstitutionSheet: Page "Item Substitution Entry";
    begin
        // MC Le 08-11-2011 => Permet l'ouverture du formulaire des articles de substitutions
        TESTFIELD("Document No.");
        TESTFIELD("Line No.");
        ItemSubstitution.SETRANGE(Type, ItemSubstitution.Type::Item);
        ItemSubstitution.SETRANGE("No.", "No.");
        ItemSubstitutionSheet.SETTABLEVIEW(ItemSubstitution);
        ItemSubstitutionSheet.RUNMODAL();
    end;

    procedure GetVendorName() txt_rtn: Text[100]
    var
        vendor: Record Vendor;
    begin
        // MC Le 14-11-2011 => Cette fonction renvoit le nom du fournisseur.
        txt_rtn := '';
        IF vendor.GET("Buy-from Vendor No.") THEN
            txt_rtn := vendor.Name;
    end;

    procedure ArrondirQte(ByHowMany: Decimal)
    var
        RecLItemVendor: Record "Item Vendor";
        LItemUOM: Record "Item Unit of Measure";
        "LQuantité": Decimal;
        LCodeUnite: Code[10];
    begin
        // MCO Le 04-10-2018 => Régie : Réappro AJout du paramètre

        IF Type <> Type::Item THEN EXIT;

        LCodeUnite := "Unit of Measure Code";
        LQuantité := "Quantity (Base)";

        // Lecture ItemVendor
        RecLItemVendor.GET("Buy-from Vendor No.", "No.", "Variant Code");

        // Arrondi au mini de commande
        // MCO Le 04-10-2018 => Régie
        IF ByHowMany <> 0 THEN BEGIN
            IF "Quantity (Base)" < ByHowMany THEN
                LQuantité := ByHowMany;
        END
        ELSE
            // FIN MCO Le 04-10-2018 => Régie
            IF "Quantity (Base)" < RecLItemVendor."Minimum Order Quantity" THEN
                LQuantité := RecLItemVendor."Minimum Order Quantity";
        // Arrondi au par combien
        IF (RecLItemVendor."Purch. Multiple" <> 0) AND (LQuantité <> 0) THEN
            IF (LQuantité MOD RecLItemVendor."Purch. Multiple") <> 0 THEN
                LQuantité := ROUND(LQuantité, RecLItemVendor."Purch. Multiple", '>');

        // Passage en unité d'achat
        IF (RecLItemVendor."Purch. Unit of Measure" <> '') AND (RecLItemVendor."Purch. Unit of Measure" <> "Unit of Measure Code") THEN BEGIN
            LItemUOM.GET("No.", RecLItemVendor."Purch. Unit of Measure");
            LQuantité := LQuantité / LItemUOM."Qty. per Unit of Measure";
            LCodeUnite := RecLItemVendor."Purch. Unit of Measure";
        END;

        //IF LCodeUnite <> "Unit of Measure Code" THEN
        // CFR le 18/01/2024 - R‚gie : masquer message [...multiple d'achat...] inutile
        gFromArrondirQte := TRUE;
        Quantity := LQuantité;
        VALIDATE("Unit of Measure Code", LCodeUnite);
        // CFR le 18/01/2024 - R‚gie : masquer message [...multiple d'achat...] inutile
        gFromArrondirQte := FALSE;
        UpdateDirectUnitCost(CurrFieldNo);
    end;

    procedure GetPamp(): Decimal
    var
        recL_Item: Record Item;
    begin
        IF Type = Type::Item THEN
            IF "No." <> '' THEN
                recL_Item.GET("No.");

        EXIT(recL_Item."Unit Cost");
    end;

    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        IF Type <> Type::Item THEN EXIT;

        FrmQuid.InitArticle("No.");
        FrmQuid.InitFournisseur("Buy-from Vendor No.");
        // AD Le 23-03-2016
        FrmQuid.RUN(); //FrmQuid.RUNMODAL();
    end;

    procedure InsertCommentaire()
    var
        PurchCommentLine: Record "Purch. Comment Line";
        CommentLine: Record "Comment Line";
    begin
        // CFR le 14/09/2021 => Régie : bug sur affichage des commentaires en-tête
        IF ("Line No.") = 0 THEN EXIT;
        // AD Le 26-04-2010 => GDI -> On fait suivre les commentaires
        PurchCommentLine.RESET();
        PurchCommentLine.SETRANGE("Document Type", "Document Type");
        PurchCommentLine.SETRANGE("No.", "Document No.");
        PurchCommentLine.SETRANGE("Document Line No.", "Line No.");
        IF NOT PurchCommentLine.ISEMPTY THEN
            PurchCommentLine.DELETEALL();

        IF Type = Type::Item THEN BEGIN
            CommentLine.RESET();
            CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
            CommentLine.SETRANGE("No.", "No.");
            IF CommentLine.FindSet() THEN
                REPEAT
                    PurchCommentLine."Document Type" := "Document Type";
                    PurchCommentLine."No." := "Document No.";
                    PurchCommentLine."Line No." := CommentLine."Line No.";
                    PurchCommentLine."Document Line No." := "Line No.";
                    PurchCommentLine.Comment := CommentLine.Comment;
                    PurchCommentLine."Display Order" := CommentLine."Display Sales Order";
                    PurchCommentLine."Print Order" := CommentLine."Print Purch. Order";
                    // MCO Le 18-01-2016 => Symta -> Suivi de l'affichage réception
                    PurchCommentLine."Display Receipt" := CommentLine."Display Purch. Receipt";
                    // FIN MCO Le 18-01-2016
                    PurchCommentLine.Insert();
                UNTIL CommentLine.NEXT() = 0;
        END;
    end;

    procedure CheckUpdateQtyOnReturn()
    begin
        // MCO Le 15-01-2018 => Migration 2017 finalisation du développement sur les retours
        IF "Document Type" = "Document Type"::"Return Order" THEN
            IF "Bin Code Orig" <> '' THEN
                ERROR(ESK50118Err);
        // FIN MCO Le 15-01-2018
    end;

    procedure CancelMvtEmplReturn(_PurchLine: Record "Purchase Line")
    var
        LBinDest: Record Bin;
        LItemJnlLine: Record "Item Journal Line";
        ItemJnlPost: Codeunit "Item Jnl.-Post";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        // GB Le 18/09/2017 => Reprise des developpements perdu NAv 2009->2015  FR20150608
        IF (_PurchLine."Outstanding Quantity" <> 0) AND (_PurchLine."Location Code Orig" <> '') AND (_PurchLine."Bin Code Orig" <> '')
          // MCO Le 23-07-2018 => Ticket : 39200
          AND (_PurchLine."Document Type" = _PurchLine."Document Type"::"Return Order") THEN BEGIN
            // FIN MCO Le 23-07-2018
            WITH LItemJnlLine DO BEGIN
                CLEAR(LItemJnlLine);
                INIT();
                VALIDATE("Journal Template Name", 'RECLASS');
                VALIDATE("Entry Type", "Entry Type"::Transfer);
                VALIDATE("Journal Batch Name", 'DEFAUT');

                VALIDATE("Posting Date", WORKDATE());
                VALIDATE("Document No.", _PurchLine."Document No.");
                VALIDATE("A Valider", TRUE);
                VALIDATE("Item No.", _PurchLine."No.");

                // emplacement source
                VALIDATE("Location Code", _PurchLine."Location Code");
                VALIDATE("Bin Code", _PurchLine."Bin Code");
                // emplacement de destination
                VALIDATE("New Location Code", _PurchLine."Location Code Orig");
                VALIDATE("New Bin Code", _PurchLine."Bin Code Orig");

                // AD Le 30-01-2020 => REGIE -> On veut mette dans le nv emplacement par defaut si il a changé
                CduLFunctions.ESK_GetDefaultBin(_PurchLine."No.", _PurchLine."Variant Code", _PurchLine."Location Code", "New Bin Code", "New Zone Code");


                LBinDest.GET("Location Code", "Bin Code");

                VALIDATE(Quantity, _PurchLine."Outstanding Quantity");
                VALIDATE(Commentaire, 'Mvt pour gestion ret/ann achat');
                INSERT(TRUE);
            END;
            CduLFunctions.SetHideValidationDialog(TRUE);
            ItemJnlPost.RUN(LItemJnlLine);
        END;
    end;

    procedure ShowItemVendorCard()
    var
        recL_ItemVendor: Record "Item Vendor";
        pgeL_ItemVendor: Page "Item Vendor Card";
    begin
        // MCO Le 13-02-2018 => Régie
        recL_ItemVendor.GET("Buy-from Vendor No.", "No.", "Variant Code");
        recL_ItemVendor.SETRECFILTER();
        pgeL_ItemVendor.SETTABLEVIEW(recL_ItemVendor);
        pgeL_ItemVendor.RUN();
        // FIN MCO Le 13-02-2018 => Régie
    end;

    procedure GetItemVendorIndirectCost(): Decimal
    var
        lItemVendor: Record "Item Vendor";
    begin
        // MCO Le 04-10-2018 => Régie
        IF lItemVendor.GET("Buy-from Vendor No.", "No.") THEN
            EXIT(lItemVendor."Indirect Cost")
        ELSE
            EXIT(0);
    end;

    PROCEDURE SetWarehouseEntryComment();
    VAR
        lWarehouseEntry: Record "Warehouse Entry";
        CommentMsg: Label 'Mvt gestion retour achat : %1', comment = '%1 reour achat';
    BEGIN
        // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
        // Attention : permission en modification sur table 7312
        lWarehouseEntry.RESET();
        lWarehouseEntry.SETRANGE("Journal Template Name", 'RECLASS');
        lWarehouseEntry.SETRANGE("Journal Batch Name", 'DEFAUT');
        lWarehouseEntry.SETRANGE("Source No.", Rec."Document No.");
        lWarehouseEntry.SETRANGE("Source Line No.", Rec."Line No.");
        lWarehouseEntry.SETRANGE("Location Code", Rec."Location Code");
        lWarehouseEntry.SETRANGE("Bin Code", Rec."Bin Code");
        lWarehouseEntry.SETRANGE("Item No.", Rec."No.");
        lWarehouseEntry.SETRANGE("Variant Code", Rec."Variant Code");
        lWarehouseEntry.SETRANGE("Unit of Measure Code", Rec."Unit of Measure Code");
        IF lWarehouseEntry.FINDFIRST() THEN BEGIN
            lWarehouseEntry.Commentaire := Format(STRSUBSTNO(CommentMsg, FORMAT(Rec."Demande retour")));
            lWarehouseEntry.MODIFY();
        END;
    END;

    var

    var
        GestionMultiReference: Codeunit "Gestion Multi-référence";
        gFromArrondirQte: Boolean;
        ESK50110Err: Label 'L''article %1 est bloqué en achats !', comment = '%1 article';
        ShortCloseLine: Boolean;
        ESK001Msg: Label 'L''article %1 na pas de fiche fournisseur pour %2 !', comment = '%1 article,%2 fournisseur';
        ESK20116Err: Label 'Vous ne pouvez pas attacher plus que la quantité de ligne : %1 en tenant compte des quantités qui sont déjà en réception : %2', Comment = ' %1,%2';
        HideValidationctrlfac: Boolean;
        ESK50118Err: Label 'Un mouvement de casier à déjà eu lieue. Vous pouvez soit supprimer la ligne et la refaire soit créer une nouvelle ligne.';
}

