tableextension 50357 "Sales Header Logic" extends "Sales Header" //36
{
    fields
    {
        modify("Invoice Customer No.")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 07-09-2009 => FARGROUP -> Client Facturé
                VALIDATE("Bill-to Customer No.");
            end;
        }
        modify("Avoid Active Central")
        {
            Trigger OnAfterValidate()
            BEGIN
                // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
                SetCentraleActiveGroupementPayeur();
                // FIN CFR le 24/09/2021
            END;
        }
        modify("Quote Validity Date")
        {
            Trigger OnAfterValidate()
            VAR
                lDateTemp: Date;
            BEGIN
                // CFR le 04/10/2023 - Régie : Date de validité des devis champ 50090
                SalesSetup.GET();
                IF (FORMAT(SalesSetup."Quote Max Validity Period") <> '') THEN BEGIN
                    lDateTemp := CALCDATE(SalesSetup."Quote Max Validity Period", TODAY);
                    IF ("Quote Validity Date" > lDateTemp) THEN
                        ERROR('Vous ne pouvez pas saisir une date de validité de devis supérieure à %1.', lDateTemp);
                END;
                // FIN CFR le 04/10/2023
            END;
        }
        modify("Saturday Delivery")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 31-03-2015 => Régie
                IF "Saturday Delivery" THEN
                    VALIDATE("Mode d'expédition", "Mode d'expédition"::"Express Samedi");
                // MCO Le 31-03-2015 => Régie
            end;
        }

        modify("Mode d'expédition")
        {
            trigger OnAfterValidate()
            begin
                "Saturday Delivery" := "Mode d'expédition" IN ["Mode d'expédition"::"Contre Rembourssement Samedi",
                                                               "Mode d'expédition"::"Express Samedi"];
            end;
        }
        modify("Material Information")
        {
            Trigger OnAfterValidate()
            BEGIN
                // CFR le 17/05/2024 - Suite R‚gie : 50150 info mat‚riel (tous documents) >> UPERCASE
                Rec."Material Information" := UPPERCASE(Rec."Material Information");
            END;
        }
        modify("Centrale Active")
        {
            Trigger OnAfterValidate()
            BEGIN
                UpdateSalesLines(FIELDCAPTION("Centrale Active"), FALSE);
            END;
        }

        modify("Type de commande")
        {
            Trigger OnAfterValidate()
            var
                // rec_SalesLine: Record "Sales Line";
                // decQuantity: Decimal;
                rec_Customer: Record Customer;
            begin
                // MC Le 23-11-2011 => SYMTA -> Gestion des conditions de livraisons
                IF rec_Customer.GET("Sell-to Customer No.") THEN
                    "Shipment Method Code" := rec_Customer."Shipment Method Code";
                ModifShipmentMethodFromOrderTy();
                // FIN MC Le 23-11-2011


                // MC Le 09-05-2011 => SYMTA -> Lorsque l'on modifie le type de commande, on recalcule les prix. **/
                RecreateSalesLines(FIELDCAPTION("Type de commande"));

                /*
                IF "Type de commande" <> xRec."Type de commande" THEN BEGIN
                  rec_SalesLine.RESET();
                  rec_SalesLine.SETRANGE("Document Type", "Document Type");
                  rec_SalesLine.SETRANGE("Document No." , "No.");
                  IF rec_SalesLine.FINDLAST THEN
                    REPEAT
                      decQuantity := rec_SalesLine.Quantity;
                      rec_SalesLine.VALIDATE(Quantity, 0);
                      rec_SalesLine.VALIDATE(Quantity, decQuantity);
                      rec_SalesLine.MODIFY(TRUE);
                    UNTIL rec_SalesLine.NEXT() = 0;
                END;
                */

            end;
        }
        modify("Echéances fractionnées")
        {

            trigger OnAfterValidate()
            begin
                //FBRUN le 21/04/2011 =>FBRUN ECHEANCE FRACTIONNEES
                IF "Echéances fractionnées" = FALSE THEN BEGIN
                    VALIDATE("Payment Terms Code 2", '');
                    "Taux Premiere Fraction" := 0;
                END;
            end;
        }
        modify("Payment Terms Code 2")
        {
            trigger OnAfterValidate()
            var
                PaymentTerms: record "Payment Terms";
            begin
                //FBRUN le 21/04/2011 =>FBRUN ECHEANCE FRACTIONNEES
                IF ("Payment Terms Code 2" <> '') AND ("Document Date" <> 0D) THEN BEGIN
                    PaymentTerms.GET("Payment Terms Code 2");
                    IF (("Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"]) AND
                        NOT PaymentTerms."Calc. Pmt. Disc. on Cr. Memos")
                    THEN
                        VALIDATE("Due Date 2", "Document Date")
                    ELSE
                        "Due Date 2" := CALCDATE(PaymentTerms."Due Date Calculation", "Document Date");

                END ELSE
                    IF NOT "Echéances fractionnées" THEN
                        VALIDATE("Due Date 2", 0D)
                    ELSE
                        VALIDATE("Due Date 2", "Document Date");
            end;
        }
        modify("Taux Premiere Fraction")
        {
            trigger OnBeforeValidate()
            begin
                TESTFIELD("Taux Premiere Fraction");
            end;
        }

        modify("Devis Web")
        {
            trigger OnAfterValidate()
            begin
                "Source Document Type" := 'WEB';
            end;
        }


        modify("Code Abattement")
        {
            trigger OnAfterValidate()
            var
                LParam: Record "Generals Parameters";
            begin
                LParam.GET('VTE_RET_ABATEMENT', "Code Abattement");
                VALIDATE("Taux Abattement", LParam.Decimal1);
            end;
        }

    }

    procedure InsertComment()
    var
        "Comment Line": Record "Comment Line";
        "Sales comment Line": Record "Sales Comment Line";
        LCust: Record Customer;
        NumLine: Integer;
    begin
        IF "Sell-to Customer No." = '' THEN
            EXIT;

        "Comment Line".RESET();
        "Comment Line".SETRANGE("Comment Line"."Table Name", "Comment Line"."Table Name"::Customer);
        "Comment Line".SETRANGE("No.", "Sell-to Customer No.");
        IF "Comment Line".FIND('-') THEN
            REPEAT
                "Sales comment Line".INIT();
                "Sales comment Line"."Document Type" := "Document Type";
                "Sales comment Line"."No." := "No.";
                "Sales comment Line"."Line No." := "Comment Line"."Line No.";
                // AD Le 28-06-2007 => Demande de Virginie -> On met la date du commentaire d'origine
                //"Sales comment Line".Date                       := "Posting Date";
                "Sales comment Line".Date := "Comment Line".Date;
                // FIN AD Le 28-06-2007
                "Sales comment Line".Comment := "Comment Line".Comment;
                "Sales comment Line".Code := "Comment Line".Code;
                "Sales comment Line"."Print Wharehouse Shipment" := "Comment Line"."Print Wharehouse Shipment";
                "Sales comment Line"."Print Shipment" := "Comment Line"."Print Shipment";
                "Sales comment Line"."Print Invoice" := "Comment Line"."Print Invoice";
                "Sales comment Line"."Display Order" := "Comment Line"."Display Sales Order";
                "Sales comment Line"."Print Order" := "Comment Line"."Print Order";
                "Sales comment Line"."Print Quote" := "Comment Line"."Print Quote";
                "Sales comment Line".Insert();
            UNTIL "Comment Line".NEXT() = 0;
        NumLine := "Comment Line"."Line No.";

        // AD Le 29-01-2019 => ajout commentaire centrale COFAQ sur facture si client lié à une centrale
        IF Rec."Document Type" IN [Rec."Document Type"::Invoice, Rec."Document Type"::"Credit Memo"] THEN BEGIN
            LCust.GET("Sell-to Customer No.");
            // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
            LCust.SETFILTER("Centrale Active Starting DF", '..%1', "Posting Date");
            LCust.SETFILTER("Centrale Active Ending DF", '%1..', "Posting Date");
            LCust.CALCFIELDS("Centrale Active");
            // FIN CFR le 24/09/2021 - SFD20210201 historique Centrale Active
            IF LCust."Centrale Active" <> '' THEN BEGIN

                "Comment Line".RESET();
                "Comment Line".SETRANGE("Comment Line"."Table Name", "Comment Line"."Table Name"::Customer);
                "Comment Line".SETRANGE("No.", LCust."Centrale Active");
                IF "Comment Line".FIND('-') THEN
                    REPEAT
                        NumLine += 10000;
                        "Sales comment Line".INIT();
                        "Sales comment Line"."Document Type" := "Document Type";
                        "Sales comment Line"."No." := "No.";
                        "Sales comment Line"."Line No." := NumLine;
                        "Sales comment Line".Date := "Comment Line".Date;
                        "Sales comment Line".Comment := "Comment Line".Comment;
                        "Sales comment Line".Code := "Comment Line".Code;
                        "Sales comment Line"."Print Wharehouse Shipment" := "Comment Line"."Print Wharehouse Shipment";
                        "Sales comment Line"."Print Shipment" := "Comment Line"."Print Shipment";
                        "Sales comment Line"."Print Invoice" := "Comment Line"."Print Invoice";
                        "Sales comment Line"."Display Order" := "Comment Line"."Display Sales Order";
                        "Sales comment Line"."Print Order" := "Comment Line"."Print Order";
                        "Sales comment Line"."Print Quote" := "Comment Line"."Print Quote";
                        "Sales comment Line".Insert();
                    UNTIL "Comment Line".NEXT() = 0;
                NumLine := "Comment Line"."Line No.";
            END;
            // AD le 29/01/2019 - FIN


        END;


        COMMIT();
    end;

    procedure DeleteComment()
    begin
        SalesCommentLine.RESET();
        SalesCommentLine.SETRANGE("Document Type", "Document Type");
        SalesCommentLine.SETRANGE("No.", "No.");
        SalesCommentLine.SETRANGE("Document Line No.", 0); //ANI Le 13-02-2018 régie - suppression uniquement des lignes d'entête (commentaire client)
        SalesCommentLine.DELETEALL();
    end;

    procedure ControlExternalNo()
    var
    // ArchCredit: Record "Sales Cr.Memo Header";
    // ArchInvoice: Record "Sales Cr.Memo Header";
    // SalesHeader2: Record "Sales Header";
    // ArchOrder: Record "Sales Header Archive";
    begin
        // AD Le 13-12-2011 => SYMTA -> Demande de LM
        EXIT;
        // FIN AD Le 13-12-2011

        // JB Le 04-12-08 MIGV5
        // modif Sidamo par CS le 9/3/04
        // Ne pas pouvoir saisir 2 commandes avec la même référence client
        // si problème de performance créer un index et l'activer lors du test
        // **********************Code Non utiliser********************************
        // IF NOT HideValidationDialog THEN
        //     IF "External Document No." <> '' THEN BEGIN
        //         SalesHeader2.RESET();
        //         SalesHeader2.SETCURRENTKEY("Sell-to Customer No.", "External Document No.");
        //         SalesHeader2.SETRANGE("Sell-to Customer No.", "Sell-to Customer No.");
        //         SalesHeader2.SETRANGE("External Document No.", "External Document No.");
        //         SalesHeader2.SETFILTER("No.", '<>%1', "No.");
        //         IF SalesHeader2.FINDFIRST() THEN
        //             ERROR(Text50001, "External Document No.", SalesHeader2."No.");

        //         IF "Document Type" = "Document Type"::Order THEN BEGIN
        //             ArchOrder.SETRANGE("Document Type", ArchOrder."Document Type"::Order);
        //             ArchOrder.SETRANGE("Sell-to Customer No.", "Sell-to Customer No.");
        //             ArchOrder.SETRANGE("External Document No.", "External Document No.");
        //             IF ArchOrder.FINDFIRST() THEN
        //                 ERROR(Text50001, "External Document No.", ArchOrder."No.");
        //         END;

        //         IF "Document Type" = "Document Type"::"Credit Memo" THEN BEGIN
        //             ArchCredit.RESET();
        //             ArchCredit.SETRANGE("Sell-to Customer No.", "Sell-to Customer No.");
        //             ArchCredit.SETRANGE("External Document No.", "External Document No.");
        //             IF ArchCredit.FIND('-') THEN
        //                 ERROR(Text50001, "External Document No.", ArchCredit."No.");
        //         END;

        //         IF "Document Type" = "Document Type"::Invoice THEN BEGIN
        //             ArchInvoice.RESET();
        //             ArchInvoice.SETRANGE("Sell-to Customer No.", "Sell-to Customer No.");
        //             ArchInvoice.SETRANGE("External Document No.", "External Document No.");
        //             IF ArchInvoice.FIND('-') THEN
        //                 ERROR(Text50001, "External Document No.", ArchInvoice."No.");
        //         END;
        //     END;
        //**************************************************************************************
        // fin modif
    end;
    //****************************Fonction non Utiliser ***********************************************
    // procedure ConfirmeArchivage(LDemandeConfirmeArchivage: Boolean)
    // begin
    //     // AD Le 16-01-2009 => MIG V5 -> Demande si on doit archiver la commande
    //     DemandeConfirmeArchivage := LDemandeConfirmeArchivage;
    // end;
    //*************************************************************************************************
    procedure ValidateSellCustNo()
    var
        // SalesHeader2: Record "Sales Header";
        // "Ship-to Address": Record "Ship-to Address";
        Cust: Record Customer;
    begin
        Cust.get(rec."Sell-to Customer No.");
        // AD Le 04-03-2009 => MIG V5
        //--------------------------------
        // Créer un message d'alerte après la saisie du code client sur les commandes pour avertir que
        // le client a déjà passé une commande qui est toujours dans le cycle de préparation ou s'il
        // y a déjà une commande en cours pour ce client
        IF (NOT HideValidationDialog) AND ("Sell-to Customer No." <> '') THEN
            /* // AD Le 30-10-2015 => Demande de SYMTA => Pas necessaire
              SalesHeader2.RESET();
              SalesHeader2.SETRANGE("Document Type", "Document Type"::Order);
              SalesHeader2.SETFILTER("No.",'<>%1',"No.");
              SalesHeader2.SETRANGE(SalesHeader2."Sell-to Customer No.", "Sell-to Customer No.");
              IF (SalesHeader2.FINDLAST) AND ("Document Type"<>"Document Type"::Invoice)  THEN
                MESSAGE(Text50012,SalesHeader2."No.");
        
              // AD Le 05-06-2009 => Demande de Virginie -> Alerte si le client a des devis en cours en saisie de cde
              IF "Document Type" = "Document Type"::Order THEN
                BEGIN
                  SalesHeader2.RESET();
                  SalesHeader2.SETRANGE("Document Type", "Document Type"::Quote);
                  SalesHeader2.SETRANGE(SalesHeader2."Sell-to Customer No.", "Sell-to Customer No.");
                  IF (SalesHeader2.FINDLAST) AND ("Document Type"<>"Document Type"::Invoice) THEN
                    MESSAGE(Text50014, SalesHeader2."No.");
                END;
              // FIN AD Le 05-06-2009
        
              // AD Le 03-12-2009 => Demande de Catalina -> Message si plusieurs adresse de livraison
              "Ship-to Address".RESET();
              "Ship-to Address".SETRANGE("Customer No.", "Sell-to Customer No.");
              IF "Ship-to Address".COUNT <> 0 THEN
                MESSAGE(Text50015);
              // FIN AD Le 03-12-2009
            // FIN AD Le Le 30-10-2015
            */


            // MC Le 27-10-2011 => SYMTA -> Si le client est en contre-remboursement alors message
            IF Cust."Mode d'expédition" IN [
                                            (Cust."Mode d'expédition"::"Contre Rembourssement Normal"),
                                            (Cust."Mode d'expédition"::"Contre Rembourssement Express"),
                                            (Cust."Mode d'expédition"::"Contre Rembourssement Samedi")
                                           ] THEN
                MESSAGE(STRSUBSTNO(Text50006, Cust."Mode d'expédition"));

        // FIN MC Le 27-10-2011



        //fin modif

        "Abandon remainder" := Cust."Abandon remainder";
        "Invoice Type" := Cust."Invoice Type";
        Responsable := Cust.Responsable;
        "Direction Code" := Cust."Direction Code";
        "Chiffrage BL" := Cust."Figuring Sales Shipment";
        //MC Le 22-10-2010 => Suivi des champs de statistiques.
        "Family Code 1" := Cust."Family Code 1";
        "Family Code 2" := Cust."Family Code 2";
        "Family Code 3" := Cust."Family Code 3";
        //FIN MC Le 22-10-2010

        // MCO Le 23-03-2016 => Prise de l'information sur la base
        "Service Zone Code" := Cust."Service Zone Code";
        // FIN MCO Le 23-03-2016



        // Trasport;
        "Transporteur imperatif" := Cust."Transporteur imperatif";
        // FIN MC Le 19-04-2011
        DeleteComment();
        InsertComment();

        //MIGV5
        IF ("Sell-to Customer No." <> '') AND (NOT HideValidationDialog) AND GUIALLOWED THEN BEGIN
            SalesCommentLine.RESET();
            SalesCommentLine.SETFILTER("Document Type", '%1', "Document Type");
            SalesCommentLine.SETRANGE("No.", "No.");
            SalesCommentLine.SETRANGE(SalesCommentLine."Display Order", TRUE);
            IF SalesCommentLine.COUNT > 0 THEN
                PAGE.RUN(PAGE::"Sales Comment Sheet", SalesCommentLine);
        END

    end;

    procedure GetHtAmount(Type: Integer): Decimal
    var
        TotalSalesLine: array[4] of Record "Sales Line";
        TotalSalesLineLCY: array[4] of Record "Sales Line";
        TempVATAmountLine1: Record "VAT Amount Line" temporary;
        TempVATAmountLine2: Record "VAT Amount Line" temporary;
        TempVATAmountLine3: Record "VAT Amount Line" temporary;
        TempSalesLine: Record "Sales Line" temporary;
        SalesLine: Record "Sales Line";
        Header: Record "Sales Header";
        SalesPost: Codeunit "Sales-Post";
        TotalAmount1: array[4] of Decimal;
        TotalAmount2: array[4] of Decimal;
        VATAmount: array[4] of Decimal;
        VATAmountText: array[4] of Text[30];
        ProfitLCY: array[4] of Decimal;
        ProfitPct: array[4] of Decimal;
        i: Integer;
        TotalAdjCostLCY: array[4] of Decimal;
        AdjProfitLCY: array[4] of Decimal;
        AdjProfitPct: array[4] of Decimal;
        // TempVATAmountLine4: Record "VAT Amount Line" temporary;
    begin
        // AD Le 04-03-2008 => Cette fonction le montant HT d'un document
        //                  => Avec un peu d'adaptation elle retourne le TTC, TVA ...

        Header.RESET();
        Header.SETRANGE("Document Type", "Document Type");
        Header.SETRANGE("No.", "No.");
        Header.GET("Document Type", "No.");

        CLEAR(SalesLine);
        CLEAR(TotalSalesLine);
        CLEAR(TotalSalesLineLCY);

        //FOR i := 1 TO 3 DO BEGIN
        i := Type;
        TempSalesLine.DELETEALL();
        CLEAR(TempSalesLine);
        CLEAR(SalesPost);
        SalesPost.GetSalesLines(Rec, TempSalesLine, i - 1);
        CLEAR(SalesPost);
        CASE i OF
            1:
                SalesLine.CalcVATAmountLines(0, Rec, TempSalesLine, TempVATAmountLine1);
            2:
                SalesLine.CalcVATAmountLines(0, Rec, TempSalesLine, TempVATAmountLine2);
            3:
                SalesLine.CalcVATAmountLines(0, Rec, TempSalesLine, TempVATAmountLine3);
        END;

        SalesPost.SumSalesLinesTemp(
          Rec, TempSalesLine, i - 1, TotalSalesLine[i], TotalSalesLineLCY[i],
          VATAmount[i], VATAmountText[i], ProfitLCY[i], ProfitPct[i], TotalAdjCostLCY[i]);

        IF i = 3 THEN
            TotalAdjCostLCY[i] := TotalSalesLineLCY[i]."Unit Cost (LCY)";

        AdjProfitLCY[i] := TotalSalesLineLCY[i].Amount - TotalAdjCostLCY[i];
        IF TotalSalesLineLCY[i].Amount <> 0 THEN
            AdjProfitPct[i] := ROUND(AdjProfitLCY[i] / TotalSalesLineLCY[i].Amount * 100, 0.1);

        IF "Prices Including VAT" THEN BEGIN
            TotalAmount2[i] := TotalSalesLine[i].Amount;
            TotalAmount1[i] := TotalAmount2[i] + VATAmount[i];
            TotalSalesLine[i]."Line Amount" := TotalAmount1[i] + TotalSalesLine[i]."Inv. Discount Amount";
        END ELSE BEGIN
            TotalAmount1[i] := TotalSalesLine[i].Amount;
            TotalAmount2[i] := TotalSalesLine[i]."Amount Including VAT";
        END;



        EXIT(TotalSalesLine[i]."Line Amount");

        TempVATAmountLine1.MODIFYALL(Modified, FALSE);
        TempVATAmountLine2.MODIFYALL(Modified, FALSE);
        //TempVATAmountLine3.MODIFYALL(Modified,FALSE);
        //TempVATAmountLine4.MODIFYALL(Modified,FALSE);
    end;

    procedure RecalculQteSurCdeOuverte()
    var
        SalesBlancketLine: Record "Sales Line";
        SalesLine: Record "Sales Line";
        QteEnCde: Decimal;
    begin
        // Cette fonction permet de recaluler la Qté Cde Ouverte Restante / Blanket Order Quantity Outstan sur les commande ouverte
        TESTFIELD("Document Type", "Document Type"::"Blanket Order");

        // On recherche les lignes de la commande ouverte
        SalesBlancketLine.RESET();
        SalesBlancketLine.SETRANGE("Document Type", "Document Type"::"Blanket Order");
        SalesBlancketLine.SETRANGE("Document No.", "No.");
        IF SalesBlancketLine.FINDFIRST() THEN
            REPEAT
                // On recherche les qte non livrée dans les commandes normales
                QteEnCde := 0;
                SalesLine.RESET();
                SalesLine.SETRANGE("Document Type", "Document Type"::Order);
                SalesLine.SETRANGE("Blanket Order No.", SalesBlancketLine."Document No.");
                SalesLine.SETRANGE("Blanket Order Line No.", SalesBlancketLine."Line No.");
                IF SalesLine.FindSet() THEN
                    REPEAT
                        QteEnCde += SalesLine."Outstanding Quantity";
                    UNTIL SalesLine.NEXT() = 0;

                // La Qté Cde Ouverte Restante est la qte commandée - Qte en cde - qte déja livrée
                SalesBlancketLine."Blanket Order Quantity Outstan" := SalesBlancketLine.Quantity - QteEnCde -
                                          SalesBlancketLine."Quantity Shipped";

                SalesBlancketLine.MODIFY();

            UNTIL SalesBlancketLine.NEXT() = 0;
    end;

    procedure CalcCost(Type: Integer): Decimal
    var
        SalesLine: Record "Sales Line";
        Cost: Decimal;
    begin
        // AD Le 18-11-2009 => Fonction qui retourne le cou global d'une facture

        // Type > 1 = Mt Marge   2 = %Marge

        CALCFIELDS(Amount);
        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", "Document Type");
        SalesLine.SETRANGE("Document No.", "No.");
        IF SalesLine.FINDSET() THEN
            REPEAT
                Cost += (SalesLine.Quantity * SalesLine."Unit Cost (LCY)");
            UNTIL SalesLine.NEXT() = 0;

        CASE Type OF
            1:
                EXIT(Amount - Cost);
            2:
                IF Amount <> 0 THEN
                    EXIT(ROUND(100 * (Amount - Cost) / Amount, 0.1))
                ELSE
                    EXIT(0);
            ELSE
                ERROR('Type non géré');
        END;
    end;

    procedure AskArchivage() blnReturn: Boolean
    begin
        // WF Le 29-04-2010 => FarGroup Analyse complémentaire Avril 2010 §2.7
        //blnReturn := FALSE;

        //IF CONFIRM(Text50002) THEN
        //BEGIN
        ArchiveManagement.StoreSalesDocument(Rec, FALSE);
        blnReturn := TRUE;
        //END;
        // FIN WF Le 29-04-2010
    end;

    procedure CalcTaux2ndFraction(): Decimal
    begin
        EXIT(100 - "Taux Premiere Fraction");
    end;

    procedure VerifZone(pCodeZone: Code[10]) bln_rtn: Boolean
    var
        Rec_SalesLine: Record "Sales Line";
    begin
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // ** Cette fonction permet de renvoyer vrai/faux si une zone passée en paramètre existe pour la commande **//

        // On pose les filtres.
        Rec_SalesLine.SETRANGE("Document Type", "Document Type");
        Rec_SalesLine.SETRANGE("Document No.", "No.");
        Rec_SalesLine.SETRANGE("Zone Code", pCodeZone);

        // Renvoit le résultat.
        bln_rtn := (NOT Rec_SalesLine.ISEMPTY);
    end;

    procedure GetNbEquipmentSpec(pZoneCode: Code[10]) int_rtn: Integer
    var
        Rec_SalesLine: Record "Sales Line";
        rec_Item: Record Item;
    begin
        // ** Cette fonction permet de retourner le nombre d'éléments spéciaux par commandes et/ou zones. ** //

        // Initialisation de la valeur de retour.
        int_rtn := 0;

        // On pose les filtres.
        Rec_SalesLine.SETRANGE("Document Type", "Document Type");
        Rec_SalesLine.SETRANGE("Document No.", "No.");
        Rec_SalesLine.SETRANGE(Type, Rec_SalesLine.Type::Item);

        IF pZoneCode <> '' THEN
            Rec_SalesLine.SETRANGE("Zone Code", pZoneCode);

        IF Rec_SalesLine.FINDSET() THEN
            REPEAT
                rec_Item.GET(Rec_SalesLine."No.");
                IF rec_Item."Special Equipment Code" <> '' THEN
                    int_rtn += 1;
            UNTIL Rec_SalesLine.NEXT() = 0;
    end;

    procedure ModifShipmentMethodFromOrderTy()
    begin
        //** Cette fonction est appelée à plusieurs endroits, elle met à jour le champ méthode de livraison avec un paramètre
        //   Si le type de commande est égal à stock ou réappro.

        // Récupèration du paramètre.
        SalesSetup.GET();
        SalesSetup.TESTFIELD(SalesSetup."Shipment Method");

        // Vérification du champ type de commande
        IF ("Type de commande" = "Type de commande"::Stock)
           OR
              ("Type de commande" = "Type de commande"::Réappro)
           THEN
            "Shipment Method Code" := SalesSetup."Shipment Method";

    end;

    procedure GetCentraleName(): Text[60]
    var
        RecLCust: Record Customer;
    begin
        IF "Centrale Active" = '' THEN
            EXIT('');

        RecLCust.GET("Centrale Active");
        EXIT(Format(RecLCust."No." + ' ' + LCust.Name));
    end;

    procedure CalcWeightToShip(_pNet: Boolean) dec_rtn: Decimal
    var
        recL_SalesLine: Record "Sales Line";
    begin
        // MCO Le 31-03-2015 => Régie
        dec_rtn := 0;

        recL_SalesLine.RESET();
        recL_SalesLine.SETRANGE("Document Type", "Document Type");
        recL_SalesLine.SETRANGE("Document No.", "No.");
        recL_SalesLine.SETRANGE(Type, recL_SalesLine.Type::Item);
        IF recL_SalesLine.FINDSET() THEN
            REPEAT
                IF NOT _pNet THEN
                    dec_rtn := dec_rtn + (recL_SalesLine."Qty. to Ship" * recL_SalesLine."Gross Weight")
                ELSE
                    dec_rtn := dec_rtn + (recL_SalesLine."Qty. to Ship" * recL_SalesLine."Net Weight")
            UNTIL recL_SalesLine.NEXT() = 0;
    end;

    procedure SetHideQuoteToOrder(NewHideQuoteToOrder: Boolean)
    begin
        // MCO Le 01-04-2015
        HideQuoteToOrder := NewHideQuoteToOrder;
        // FIN MCO Le 01-04-2015
    end;

    procedure MailReliquat()
    var
        CduLMailReliquat: Codeunit "Mail Reliquat";
    begin
        CduLMailReliquat.MailReliquat("No.");
    end;

    procedure CreerIncidentTransport()
    var
        LGestionLitige: Codeunit "Gestion des litiges";
    begin
        LGestionLitige.CréerIncidentTransport("No.", '');
    end;

    procedure ValidationCentrale()
    var
        RecLCust: Record Customer;
        LSalesLine: Record "Sales Line";
    begin
        // AD Le 28-09-2016 => REGIE -> Bloquer la commande si controle centrale
        RecLCust.GET("Sell-to Customer No.");
        IF RecLCust."Confirmation Centrale" THEN BEGIN
            CLEAR(LSalesLine);
            LSalesLine.SETRANGE("Document Type", "Document Type");
            LSalesLine.SETRANGE("Document No.", "No.");
            LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
            LSalesLine.SETRANGE("Gerer par groupement", TRUE);
            IF NOT LSalesLine.ISEMPTY THEN
                IF CONFIRM(ESK004, TRUE, LSalesLine.COUNT) THEN
                    ERROR(ESK005);
        END;
        // FIN AD Le 28-09-2016
    end;

    procedure RecreateCommentLine(OldSalesLine: Record "Sales Line"; NewSourceRefNo: Integer; ToTemp: Boolean)
    var
        CommentLine: Record "Sales Comment Line";
    begin
        // MCO Le 09-02-2017 => Régie : On garde les commentaires
        IF ToTemp THEN BEGIN
            CommentLine.SETRANGE(CommentLine."Document Type", OldSalesLine."Document Type");
            CommentLine.SETRANGE(CommentLine."No.", OldSalesLine."Document No.");
            CommentLine.SETRANGE(CommentLine."Document Line No.", OldSalesLine."Line No.");
            IF CommentLine.FINDSET() THEN
                REPEAT
                    TempSalesCommentLine := CommentLine;
                    TempSalesCommentLine.Insert();
                UNTIL CommentLine.NEXT() = 0;
            CommentLine.DELETEALL();
        END ELSE BEGIN
            CLEAR(TempSalesCommentLine);
            TempSalesCommentLine.SETRANGE("Document Type", OldSalesLine."Document Type");
            TempSalesCommentLine.SETRANGE("No.", OldSalesLine."Document No.");
            TempSalesCommentLine.SETRANGE("Document Line No.", OldSalesLine."Line No.");

            IF TempSalesCommentLine.FINDSET() THEN
                REPEAT
                    CommentLine := TempSalesCommentLine;
                    CommentLine."Document Line No." := NewSourceRefNo;
                    CommentLine.Insert();
                UNTIL TempSalesCommentLine.NEXT() = 0;
            TempSalesCommentLine.DELETEALL();
        END;
    end;

    procedure CopyShipToAddressToSellToAddress()
    begin
        "Sell-to Customer Name" := "Ship-to Name";
        VALIDATE("Sell-to Address", "Ship-to Address");
        VALIDATE("Sell-to Address 2", "Ship-to Address 2");
        VALIDATE("Sell-to City", "Ship-to City");
        VALIDATE("Sell-to Contact", "Ship-to Contact");
        VALIDATE("Sell-to Country/Region Code", "Ship-to Country/Region Code");
        VALIDATE("Sell-to County", "Ship-to County");
        VALIDATE("Sell-to Post Code", "Ship-to Post Code");
    end;

    procedure CopyShipToAddressToBillToAddress()
    begin
        "Bill-to Name" := "Ship-to Name";
        VALIDATE("Bill-to Address", "Ship-to Address");
        VALIDATE("Bill-to Address 2", "Ship-to Address 2");
        VALIDATE("Bill-to City", "Ship-to City");
        VALIDATE("Bill-to Contact", "Ship-to Contact");
        VALIDATE("Bill-to Country/Region Code", "Ship-to Country/Region Code");
        VALIDATE("Bill-to County", "Ship-to County");
        VALIDATE("Bill-to Post Code", "Ship-to Post Code");
    end;

    procedure GetFilterContNo(): Code[20]
    begin
        if GetFilter("Sell-to Contact No.") <> '' then
            if GetRangeMin("Sell-to Contact No.") = GetRangeMax("Sell-to Contact No.") then
                exit(GetRangeMax("Sell-to Contact No."));
    end;

    procedure GetFilterCustNo(): Code[20]
    var
        MinValue: Code[20];
        MaxValue: Code[20];
    begin
        if GetFilter("Sell-to Customer No.") <> '' then
            if TryGetFilterCustNoRange(MinValue, MaxValue) then
                if MinValue = MaxValue then
                    exit(MaxValue);
    end;

    [TryFunction]
    procedure TryGetFilterCustNoRange(var MinValue: Code[20]; var MaxValue: Code[20])
    begin
        MinValue := GetRangeMin("Sell-to Customer No.");
        MaxValue := GetRangeMax("Sell-to Customer No.");
    end;

    PROCEDURE SetCentraleActiveGroupementPayeur();
    VAR
        lCustomer: Record Customer;
        lCentrale: Record Customer;
    BEGIN
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        "Centrale Active" := '';
        "Groupement payeur" := FALSE;

        IF NOT (rec."Avoid Active Central") THEN BEGIN

            IF lCustomer.GET("Invoice Customer No.") THEN BEGIN
                lCustomer.SETFILTER("Centrale Active Starting DF", '..%1', "Order Date");
                lCustomer.SETFILTER("Centrale Active Ending DF", '%1..', "Order Date");
                lCustomer.CALCFIELDS("Centrale Active", "Groupement payeur");
                "Centrale Active" := lCustomer."Centrale Active";
                "Groupement payeur" := lCustomer."Groupement payeur";
            END;
            // FIN CFR le 24/09/2021

            // AD Le 29-01-2020 => REGIE -> Si la centrale est bloquée, interdire
            IF "Centrale Active" <> '' THEN BEGIN
                lCentrale.GET("Centrale Active");
                lCentrale.CheckBlockedCustOnDocs(lCentrale, "Document Type", FALSE, FALSE);
            END;
        END;

        UpdateSalesLines(Format(FIELDCAPTION("Centrale Active")), FALSE);
    END;

    procedure GetSkipBillToContact(): Boolean
    begin
        exit(SkipBillToContact);
    end;

    procedure SetSkipBillToContact(SkipBillToContact_P: Boolean)
    begin
        SkipBillToContact := SkipBillToContact_P;
    end;

    var
        LCust: Record Customer;
        rec_salesline: Record "Sales Line";
        TempSalesCommentLine: Record "Sales Comment Line" temporary;
        SalesCommentLine: Record "Sales Comment Line";
        ArchiveManagement: Codeunit ArchiveManagement;
        // Text50001: Label 'Ce numéro de document externe existe déjà pour ce donneur d'' ordre - %1 - %2';
        // DemandeConfirmeArchivage: Boolean;
        // Text50012: Label 'La dernière commande  en cours pour ce client est la commande N° %1 !';
        // Text50014: Label 'Le dernier devis en cours pour ce client est le devis n° %1 !';
        // Text50015: Label 'Il existe des adresses destinataires pour ce client !';
        // Text50002: Label 'Voulez vous archivez le Devis?';

        Text50006: Label 'Attention ! Le client est en mode d''expédition :  %1.';
        HideQuoteToOrder: Boolean;
        ESK004: Label 'Validation centrale obligatoire pour %1 ligne(s) ! Interrompre le traitement ?';
        ESK005: Label 'Traitement Annulé !';
}

