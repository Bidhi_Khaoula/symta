tableextension 50336 "Item Journal Batch Logic" extends "Item Journal Batch" //233
{
    procedure SetMiniLoadLineNo()
    var
        ItemJnlLine: Record "Item Journal Line";
        int_NoLine: Integer;
    begin
        //** Cette fonction permet d'attribuer les numéros de ligne MiniLoad automatiquement **//

        // Initialisation
        int_NoLine := 0;

        // Mise à jour des numéros de ligne
        ItemJnlLine.RESET();
        ItemJnlLine.SETRANGE("Journal Template Name", "Journal Template Name");
        ItemJnlLine.SETRANGE("Journal Batch Name", Name);
        IF ItemJnlLine.FINDSET() THEN
            REPEAT
                int_NoLine += 1;
                ItemJnlLine."Line No for MiniLoad" := int_NoLine;
                ItemJnlLine.MODIFY();
            UNTIL ItemJnlLine.NEXT() = 0;
    end;
}

