tableextension 50347 "Item Vendor Logic" extends "Item Vendor" //99
{
    fields
    {
        modify("Statut Qualité")
        {
            Trigger OnAfterValidate()
            begin
                // AD Le 12-01-2016 => Avant dans la form maintenant dans la table -> Code de Laurent mallardeau
                IF "Statut Qualité" <> xRec."Statut Qualité" THEN BEGIN
                    "date maj statut qualité" := WORKDATE();
                    Evaluate("user maj statut qualité", USERID());
                END;
            end;
        }
        modify("Statut Approvisionnement")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 12-01-2016 => Avant dans la form maintenant dans la table -> Code de Laurent mallardeau
                IF "Statut Approvisionnement" <> xRec."Statut Approvisionnement" THEN BEGIN
                    "date maj statut appro" := WORKDATE();
                    Evaluate("user maj statut appro", USERID());
                END;
            end;
        }

    }

    procedure MessageAlerte()
    begin
        IF "Statut Qualité" <> "Statut Qualité"::Conforme THEN
            MESSAGE(ESK001Msg, FIELDCAPTION("Statut Qualité"), "Item No.", "Vendor No.", "Statut Qualité");

        IF "Statut Approvisionnement" <> "Statut Approvisionnement"::"Non Bloqué" THEN
            MESSAGE(ESK001Msg, FIELDCAPTION("Statut Approvisionnement"), "Item No.", "Vendor No.", "Statut Approvisionnement");
    end;

    procedure GetVendorName(): Text[100]
    var
        LVendor: Record Vendor;
    begin
        IF LVendor.GET("Vendor No.") THEN
            EXIT(LVendor.Name)
        ELSE
            EXIT('');
    end;

    PROCEDURE CreatePurchasePrice();
    VAR
        lPurchasePrice: Record "Purchase Price";
    BEGIN
        // CFR le 23/03/2023 => R‚gie : cr‚ation d'une ligne de tarif achat … 0 s'il n'existe pas de ligne
        lPurchasePrice.SETRANGE("Vendor No.", Rec."Vendor No.");
        lPurchasePrice.SETRANGE("Item No.", Rec."Item No.");
        lPurchasePrice.SETRANGE("Variant Code", Rec."Variant Code");
        IF NOT lPurchasePrice.FINDSET() THEN BEGIN
            CLEAR(lPurchasePrice);
            lPurchasePrice.INIT();
            lPurchasePrice.VALIDATE("Vendor No.", Rec."Vendor No.");
            lPurchasePrice.VALIDATE("Item No.", Rec."Item No.");
            lPurchasePrice.VALIDATE("Variant Code", Rec."Variant Code");
            lPurchasePrice.VALIDATE("Unit of Measure Code", '1');
            lPurchasePrice.VALIDATE("Starting Date", TODAY());
            IF lPurchasePrice.INSERT() THEN;
        END;
    END;

    PROCEDURE ModifierAchats(pDocumentType: Enum "Purchase Document Type"; pQuestion: Boolean);
    VAR
        lPurchaseLine: Record "Purchase Line";
        lConfirm01TxtQst: Label 'Voulez vous modifier les %1 lignes de %2 achat en cours ?', comment = '%1,%2';
    BEGIN

        // CFR le 16/04/2024 => R‚gie : ModifierCde(Question) >> ModifierAchats(Type, Question)
        lPurchaseLine.RESET();
        lPurchaseLine.SETRANGE("Document Type", pDocumentType);
        lPurchaseLine.SETRANGE(Type, lPurchaseLine.Type::Item);
        lPurchaseLine.SETRANGE("No.", "Item No.");
        lPurchaseLine.SETRANGE("Item Reference Type", lPurchaseLine."Item Reference Type"::Vendor);
        lPurchaseLine.SETRANGE("Item Reference Unit of Measure", "Purch. Unit of Measure");
        lPurchaseLine.SETRANGE("Item Reference Type No.", "Vendor No.");
        IF lPurchaseLine.COUNT <> 0 THEN
            IF pQuestion THEN BEGIN
                IF CONFIRM(STRSUBSTNO(lConfirm01TxtQst, lPurchaseLine.COUNT, FORMAT(pDocumentType))) THEN
                    lPurchaseLine.MODIFYALL("Item Reference No.", "Vendor Item No.")
            END
            ELSE
                lPurchaseLine.MODIFYALL("Item Reference No.", "Vendor Item No.");
    END;

    var
        ESK001Msg: Label 'Le %1 de l''article %2 chez le fournisseur %3 est %4 !', Comment = '%1 %2 %3 %4';
}

