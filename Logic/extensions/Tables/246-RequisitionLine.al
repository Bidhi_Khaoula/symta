tableextension 50337 "Requisition Line Logic" extends "Requisition Line" //246
{
    fields
    {
        modify("Type de commande")
        {
            trigger OnAfterValidate()
            begin
                VALIDATE("Vendor No.");
            end;
        }

    }


    procedure GetStockExprapole(): Decimal

    begin
        // AD Le 23-09-2009 => FARGROUP -> Stock à l'arrivé de la commande
        //MC Le 30-04-2010 -> Analyse Complémentaire => Possibilité de recalculer le stock extrapolé à la modif de la date délai
        //Calcul du nombre de jour entre la date de travail et la date délai

        IF "Due Date" = 0D THEN
            exit("Délai Calculé")
        ELSE
            exit("Due Date" - WORKDATE());
        //FIN MC
    end;

    procedure GetQteByContainer(): Decimal
    var
        Item: Record Item;
    begin
        IF Type <> Type::Item THEN
            EXIT(0);

        IF Item.GET("No.") THEN
            EXIT(Item."Quantité par container")
        ELSE
            EXIT(0);
    end;

    procedure "ValidateNo.Eskape"()
    var
        ReqLine: Record "Requisition Line";
        Item: Record Item;
        _Reserve: Decimal;
        _Attendu: Decimal;
    begin
        // AD Le 19-11-2009 => Cette focntion remplie les champs supplementaires ajoutés par ESKAPE
        IF ReapproAuto THEN
            EXIT;

        IF Type <> Type::Item THEN EXIT;

        // Recherche du delai sur la 1ere ligne de la feuille
        ReqLine.RESET();
        ReqLine.SETRANGE("Worksheet Template Name", "Worksheet Template Name");
        ReqLine.SETRANGE("Journal Batch Name", "Journal Batch Name");
        ReqLine.SETRANGE(Type, Type);
        ReqLine.FINDFIRST();
        VALIDATE("Délai Calculé", ReqLine."Délai Calculé");

        // Calcul du reserve - attendu
        // GR MIG2015 TODO RpReapproAuto.CalcAttenduReserve("No.", _Reserve, _Attendu);
        VALIDATE("Reservé Calculé", _Reserve);
        VALIDATE("Attendu Calculé", _Attendu);

        // Recupe des infos de l'article
        Item.GET("No.");
        Item.CALCFIELDS(Inventory);

        VALIDATE("Stock Calculé", Item.Inventory);
    end;

    procedure SetReapproAuto(_ReapproAuto: Boolean)
    begin
        // AD Le 19-11-2009 => Pour dire quoi est dans les réappro automatique et le pas faire le validate eskape
        ReapproAuto := _ReapproAuto;
    end;

    procedure GetCoefMultiplicateur(): Decimal
    var
        Item: Record Item;
    begin
        // MC Le 30-04-2010 -> Analyse complémentaire : Ajout du champ coef multiplicateur de la fiche article
        IF Type = Rec.Type::Item THEN BEGIN
            IF Item.GET("No.") THEN;
            EXIT(Item."Coef. multiplicateur");
        END
        ELSE
            EXIT(0);
        //FIN MC
    end;

    var
        ReapproAuto: Boolean;
}

