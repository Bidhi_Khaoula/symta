tableextension 50469 "Contact Logic" extends Contact //5050
{
    fields
    {
        modify("Old Address")
        {
            Trigger OnAfterValidate()
            BEGIN
                // MCO Le 23-03-2016 => Recalcul des coordinées
                Rec.ChangeCoordinate();
                // FIN MCO Le 23-03-2013
            END;

        }
        modify("Old Address 2")
        {
            Trigger OnAfterValidate()
            BEGIN
                // MCO Le 23-03-2016 => Recalcul des coordinées
                ChangeCoordinate();
                // FIN MCO Le 23-03-2013
            END;

        }
        modify("Old City")
        {
            Trigger OnAfterValidate()
            BEGIN
                PostCode.ValidateCity(City, "Post Code", County, "Country/Region Code", (CurrFieldNo <> 0) AND GUIALLOWED);

                // MCO Le 23-03-2016 => Recalcul des coordinées
                ChangeCoordinate();
                // FIN MCO Le 23-03-2013
            END;

        }
        modify("Old Post Code")
        {
            Trigger OnAfterValidate()
            BEGIN
                PostCode.ValidatePostCode(City, "Post Code", County, "Country/Region Code", (CurrFieldNo <> 0) AND GUIALLOWED);

                // MCO Le 23-03-2016 => Recalcul des coordinées
                ChangeCoordinate();
                // FIN MCO Le 23-03-2013
            END;

        }
        modify("Old Country/Region Code")
        {
            Trigger OnAfterValidate()
            BEGIN
                PostCode.ValidateCountryCode(City, "Post Code", County, "Country/Region Code");
            END;

        }
        modify("Result Code")
        {
            trigger OnAfterValidate()
            begin
                //----------------------------------------------------------------------------------------------------------------------------------
                // Naviway => Pour geoLocalisation
                IF "Result Code" = '200' THEN 
                    "Change Coordinate" := FALSE;
                //Fin Naviway
                //----------------------------------------------------------------------------------------------------------------------------------
            end;
        }
        modify("Change Coordinate")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es
                /* {===>>> plus besoin c'est l'utilisateur qui va le g‚rer

                // MCO Le 23-03-2016 => Recalcul des coordinées
                IF "Change Coordinate" THEN
                    "Result Code" := '';
                // FIN MCO Le 23-03-2016
                */
                IF (NOT "Change Coordinate") THEN BEGIN
                    Rec."Old Address" := '';
                    Rec."Old Address 2" := '';
                    Rec."Old Post Code" := '';
                    Rec."Old City" := '';
                    Rec."Old Country/Region Code" := '';
                    Rec."Temporary Longitude" := 0;
                    Rec."Temporary Latitude" := 0;
                END;
                // FIN CFR le 12/05/2022
            end;
        }

    }

    procedure CreatePersonContact()
    var
        New_Contact: Record Contact;
    begin
        //---------------------Cette fonction permet de créer un contact personne à partir d'un contact société---------------------//

        //On vérifie d'abord que l'on se trouve sur un contact de type société
        TESTFIELD(Type, Type::Company);

        //On initialise le record
        New_Contact.INIT();
        New_Contact.VALIDATE(Type, Type::Person);
        New_Contact.VALIDATE("Company No.", Rec."No.");
        New_Contact.INSERT(TRUE);

        //On ouvre le form sur le nouveau contact
        PAGE.RUN(PAGE::"Contact Card", New_Contact);
    end;

    procedure ChangeCoordinate()
    begin
        // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es
        IF (Rec.Address = xRec.Address) AND (Rec."Address 2" = xRec."Address 2") AND
            (Rec."Post Code" = xRec."Post Code") AND (Rec.City = xRec.City) AND
            (Rec."Country/Region Code" = xRec."Country/Region Code") THEN
            EXIT;
        // FIN CFR le 12/05/2022

        // MCO Le 23-03-2016 => Recalcul des coordinées
        IF Type = Type::Company THEN
            VALIDATE("Change Coordinate", TRUE);
        // FIN MCO Le 23-03-2016

        // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es
        IF ("Change Coordinate") THEN BEGIN
            Rec."Old Address" := xRec.Address;
            Rec."Old Address 2" := xRec."Address 2";
            Rec."Old Post Code" := xRec."Post Code";
            Rec."Old City" := xRec.City;
            Rec."Old Country/Region Code" := xRec."Country/Region Code";
            Rec."Temporary Latitude" := 123;
            Rec."Temporary Longitude" := 321;
        END;
        ;
        // FIN CFR le 12/05/2022
    end;

    PROCEDURE ValidateNewGPS(): Boolean;
    BEGIN
        // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es
        IF CONFIRM('Valider les nouvelles latitude & longitude ?') THEN BEGIN
            Rec.Latitude := Rec."Temporary Latitude";
            Rec.Longitude := Rec."Temporary Longitude";
            Rec.VALIDATE("Change Coordinate", FALSE);
            Rec.MODIFY();
            EXIT(TRUE);
        END;

        EXIT(FALSE);
    END;

    var
        PostCode: Record "Post Code";
}

