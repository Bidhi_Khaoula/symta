tableextension 50308 "Sales Shipment Line Logic" extends "Sales Shipment Line" //111
{
    procedure OuvrirMultiConsultation()
    var
        FrmQuid: Page "Multi -consultation";
    begin
        IF Type <> Type::Item THEN EXIT;

        FrmQuid.InitArticle("No.");
        FrmQuid.InitClient("Sell-to Customer No.");
        // AD Le 23-03-2016
        FrmQuid.RUN(); //FrmQuid.RUNMODAL();
    end;
}

