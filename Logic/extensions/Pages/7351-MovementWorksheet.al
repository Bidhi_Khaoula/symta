pageextension 50015 "Movement Worksheet" extends "Movement Worksheet" //7351
{
    layout
    {

    }

    actions
    {
        modify("Calculate Bin &Replenishment")
        {
            Visible = false;
        }
        addafter("Calculate Bin &Replenishment")
        {
            action("Calculate Bin &Replenishment SYM")
            {
                ApplicationArea = Warehouse;
                Caption = 'Calculate Bin &Replenishment';
                Ellipsis = true;
                Image = CalculateBinReplenishment;
                ToolTip = 'Calculate the movement of items from bulk storage bins with lower bin rankings to bins with a high bin ranking in the picking areas.';

                trigger OnAction()
                var
                    Location: Record Location;
                    BinContent: Record "Bin Content";
                    ReplenishBinContent: Report "Calculate Bin Replenish. SYM";
                begin
                    Location.Get(Rec."Location Code");
                    ReplenishBinContent.InitializeRequest(
                      Rec."Worksheet Template Name", Rec.Name, Rec."Location Code",
                      Location."Allow Breakbulk", false, false);

                    ReplenishBinContent.SetTableView(BinContent);
                    ReplenishBinContent.Run();
                    Clear(ReplenishBinContent);
                end;
            }

        }
    }
}