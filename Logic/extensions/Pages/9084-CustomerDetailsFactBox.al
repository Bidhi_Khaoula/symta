pageextension 51661 "Customer Details FactBox" extends "Customer Details FactBox" //9084
{

    layout
    {
        addafter("Phone No.")
        {
            field("Mobile Phone No."; Rec."Mobile Phone No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Mobile Phone No. field.';
            }
        }
    }
    actions
    {


    }

}

