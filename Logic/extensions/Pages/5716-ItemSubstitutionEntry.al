pageextension 50989 "Item Substitution Entry" extends "Item Substitution Entry" //5716
{
    layout
    {
        addafter(Control1)
        {
            field("No."; Rec."No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. field.';
            }
        }
        addafter("Variant Code")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        addafter("Substitute No.")
        {
            field("Substitute Ref. Active"; Rec."Substitute Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active substitut field.';
            }
        }
        addafter(Condition)
        {
            field("Affichage WEB"; Rec."Affichage WEB")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Affichage WEB field.';
            }
            field("Create User ID"; Rec."Create User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création field.';
            }
            field("Create Date"; Rec."Create Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Création field.';
            }
            field("Create Time"; Rec."Create Time")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Création field.';
            }
            field("Modify User ID"; Rec."Modify User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Modification field.';
            }
            field("Modify Date"; Rec."Modify Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Modification field.';
            }
            field("Modify Time"; Rec."Modify Time")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Modification field.';
            }
        }
    }
    actions
    {

    }

}

