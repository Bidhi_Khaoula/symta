pageextension 50366 "General Ledger Entries" extends "General Ledger Entries" //20
{
    layout
    {
        moveafter("Entry No."; "Source Type", "Source No.")
        moveafter("Source Type"; "VAT Prod. Posting Group")
    }

}