pageextension 50560 "Apply Bank Acc. Ledger Entries" extends "Apply Bank Acc. Ledger Entries" //381
{

    layout
    {
        modify(Balance)
        {
            Visible = false;
        }
        modify(CheckBalance)
        {
            Visible = false;
        }
        modify(BalanceToReconcile)
        {
            Visible = false;
        }
        addafter(Balance)
        {
            field(BalanceSym; BalanceSym)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Balance';
                Editable = false;
                ToolTip = 'Specifies the balance of the bank account since the last posting, including any amount in the Total on Outstanding Checks field.';
            }
        }
        addafter(CheckBalance)
        {
            field(CheckBalanceSym; CheckBalanceSym)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Total on Outstanding Checks';
                Editable = false;
                ToolTip = 'Specifies the part of the bank account balance that consists of posted check ledger entries. The amount in this field is a subset of the amount in the Balance field under the right pane in the Bank Acc. Reconciliation window.';

                trigger OnDrillDown()
                var
                    CheckLedgerEntry: Record "Check Ledger Entry";
                begin
                    if BankAccount."No." = '' then
                        exit;

                    CheckLedgerEntry.FilterGroup(2);
                    CheckLedgerEntry.SetRange("Bank Account No.", BankAccount."No.");
                    CheckLedgerEntry.SetRange("Entry Status", CheckLedgerEntry."Entry Status"::Posted);
                    CheckLedgerEntry.SetFilter("Statement Status", '<>%1', CheckLedgerEntry."Statement Status"::Closed);
                    CheckLedgerEntry.FilterGroup(0);
                    if not CheckLedgerEntry.IsEmpty() then
                        Page.Run(Page::"Check Ledger Entries", CheckLedgerEntry);
                end;
            }

        }
        addafter(BalanceToReconcile)
        {
            field(BalanceToReconcileSym; BalanceToReconcileSym)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Balance To Reconcile';
                Editable = false;
                ToolTip = 'Specifies the balance of the bank account since the last posting, excluding any amount in the Total on Outstanding Checks field.';
            }
        }

        addafter(Control1)
        {
            field(Select; rec.Select)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Selection field.';

                trigger OnValidate();
                begin
                    //todo   CalcBalance;
                    IF rec.Select THEN
                        SoldeSelect += rec.Amount
                    ELSE
                        SoldeSelect -= rec.Amount;
                end;
            }
        }
        addafter(Control15)
        {
            field("Total selection"; SoldeSelect)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the SoldeSelect field.';
            }
        }
    }
    PROCEDURE CalcBalance();
    BEGIN
        IF BankAccount.GET(Rec."Bank Account No.") THEN BEGIN
            BankAccount.CALCFIELDS(Balance, "Total on Checks");
            BalanceSym := BankAccount.Balance;
            CheckBalanceSym := BankAccount."Total on Checks";
            BalanceToReconcileSym := CalcBalanceToReconcile();
        END;
    END;

    local procedure CalcBalanceToReconcile(): Decimal
    var
        BankAccountLedgerEntry: Record "Bank Account Ledger Entry";
    begin
        BankAccountLedgerEntry.CopyFilters(Rec);
        BankAccountLedgerEntry.CalcSums(Amount);
        exit(BankAccountLedgerEntry.Amount);
    end;

    var
        BankAccount: Record "Bank Account";
        SoldeSelect: Decimal;
        BalanceSym: Decimal;
        CheckBalanceSym: Decimal;
        BalanceToReconcileSym: Decimal;

    procedure CalcSelect();
    var
        RecLine: Record "Bank Account Ledger Entry";
    begin
        RecLine.COPY(Rec);
        RecLine.SETRANGE(Select, TRUE);
        RecLine.CALCSUMS(Amount);
        SoldeSelect := RecLine.Amount;
    end;

    procedure GetSelectedRecordsSymta(var TempBankAccountLedgerEntry: Record "Bank Account Ledger Entry" temporary)
    var
        BankAccountLedgerEntry: Record "Bank Account Ledger Entry";

    begin
        //CurrPage.SETSELECTIONFILTER(BankAccLedgerEntry);

        BankAccountLedgerEntry.COPY(Rec);
        BankAccountLedgerEntry.SETRANGE(Select, TRUE);
        if BankAccountLedgerEntry.FindSet() then
            repeat
                TempBankAccountLedgerEntry := BankAccountLedgerEntry;
                TempBankAccountLedgerEntry.Insert();
            until BankAccountLedgerEntry.Next() = 0;
    end;

}

