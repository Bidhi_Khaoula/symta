pageextension 50218 "Posted Sales Invoices" extends "Posted Sales Invoices" //143

{
    layout
    {
        addafter("Shipment Date")
        {
            field("Centrale Active"; Rec."Centrale Active")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
            field("Payment Method Code"; Rec."Payment Method Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Payment Method Code field.';
            }
        }
    }
    actions
    {
        modify(Print)
        {
            Visible = false;
        }
        addafter(Print)
        {
            action(Print2)
            {
                ApplicationArea = Basic, Suite;
                Caption = '&Print';
                Ellipsis = true;
                Image = Print;
                ToolTip = 'Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.';
                Visible = NOT IsOfficeAddin;

                trigger OnAction()
                var
                    SalesInvHeader: Record "Sales Invoice Header";
                begin
                    SalesInvHeader := Rec;
                    CurrPage.SetSelectionFilter(SalesInvHeader);
                    // impression nomenclature douaniŠre si Export (CFT20220601)
                    IF Rec."Gen. Bus. Posting Group" = 'EXPORT' THEN
                        REPORT.RUNMODAL(REPORT::"Nomenclature Douanière", TRUE, FALSE, SalesInvHeader);
                    SalesInvHeader.PrintRecords(true);
                end;
            }
        }
    }
    trigger OnOpenPage()
    var
        OfficeMgt: Codeunit "Office Management";
    begin
        Rec.SetCurrentKey("No.");
        IsOfficeAddin := OfficeMgt.IsAvailable();
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;

    var

        IsOfficeAddin: Boolean;
}