pageextension 50003 "Countries/Regions" extends "Countries/Regions" //10
{
    layout
    {
        modify("Contact Address Format")
        {
            Visible = false;
        }
        addafter("Contact Address Format")
        {
            field("Contact Address Format Spec"; rec."Contact Address Format Spec")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Format adresse contact Spec field.';
            }
        }
    }
}