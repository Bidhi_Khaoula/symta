pageextension 50706 "Purchase Invoice" extends "Purchase Invoice" //51
{

    layout
    {

        addafter("No.")
        {
            field(Periode; Rec.Periode)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Periode field.';
            }
        }
        modify("Posting Date")
        {
            Visible = false;
        }
        moveafter("Buy-from Contact"; "Posting Description")
        modify("Buy-from Vendor No.")
        {
            Visible = false;
        }
        addafter("Buy-from Vendor No.")
        {
            field("Buy-from Vendor No. 2"; Rec."Buy-from Vendor No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Buy-from Vendor No. field.';
            }
        }
        addafter(Status)
        {
            field("Montant HT facture"; Rec."Montant HT facture")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant TTC field.';

                trigger OnValidate();
                begin
                    CurrPage.UPDATE();
                end;
            }
        }
        addafter("Area")
        {
            field("<Periode>"; Rec.Periode)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Periode field.';
            }
        }
    }
    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec.Periode := xRec.Periode;
    end;
}

