pageextension 50610 "Sales Order Subform" extends "Sales Order Subform" //46
{
    layout
    {
        modify(Type)
        {
            QuickEntry = FALSE;
        }

        modify("No.")
        {
            Visible = True;
            Editable = True;
            QuickEntry = FALSE;
        }
        modify("Substitution Available")
        {
            StyleExpr = gStyleExp_Substitution;
            Trigger OnDrillDown()
            BEGIN
                Rec.ShowItemSub();
                CurrPage.UPDATE();
            END;
        }
        modify(Description)
        {
            StyleExpr = gStyleExp_Transporteur;
        }
        modify("Bin Code")
        {
            QuickEntry = FALSE;
            Editable = false;
            Visible = true;
        }
        modify(Quantity)
        {
            Style = Unfavorable;
            StyleExpr = StyleSalesMultiple;
        }

        modify("Unit Price")
        {
            QuickEntry = FALSE;
        }


        modify("Line Amount")
        {
            QuickEntry = FALSE;
            Visible = false;
        }
        modify("Qty. to Ship")
        {
            QuickEntry = FALSE;
        }


        modify("Requested Delivery Date")
        {
            QuickEntry = FALSE;
            Visible = true;
        }

        modify("Line No.")
        {
            QuickEntry = FALSE;
            Visible = true;
        }
        modify("Reserved Quantity")
        {
            Visible = false;
        }
        modify("Unit of Measure Code")
        {
            Visible = false;
        }
        modify("Line Discount %")
        {
            Visible = false;
            Editable = false;
        }
        modify("Qty. to Invoice")
        {
            Visible = false;
        }
        modify("Quantity Invoiced")
        {
            Visible = false;
        }
        modify("Qty. to Assign")
        {
            Visible = false;
        }
        modify("Qty. Assigned")
        {
            Visible = false;
        }
        modify("Planned Delivery Date")
        {
            Visible = false;
        }
        modify("Planned Shipment Date")
        {
            Visible = false;
        }
        modify("Shipment Date")
        {
            Visible = false;
        }
        modify("Invoice Discount Amount")
        {
            visible = false;
        }
        modify("Total VAT Amount")
        {
            visible = false;
        }
        modify("Invoice Disc. Pct.")
        {
            visible = false;
        }
        modify("Total Amount Incl. VAT")
        {
            visible = false;
        }
        movefirst(Control1; "Line No.")
        // moveafter(Type; "Bin Code", Quantity, "Qty. to Ship")
        moveafter("Quantity Shipped"; Description)
        moveafter("No."; "Unit Price")
        moveafter("Unit Price"; "Line Amount", "Requested Delivery Date", SalesLineDiscExists)

        addafter("Line No.")
        {
            field("Line type"; Rec."Line type")
            {
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type ligne field.';
            }
            field(Urgent; Rec.Urgent)
            {
                Editable = gEditableUrgent;
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Urgent field.';

                trigger OnValidate();
                begin

                    // CFR le 18/11/2020 : Régie > remises express
                    // s'il est possible d'appliquer une remise express
                    IF Rec.ExpressDiscount(gRemiseOrder, gRemiseExpress, gCoefficient, FALSE) THEN BEGIN
                        // si le coefficient est OK
                        IF (gCoefficient >= 1) THEN BEGIN
                            IF (Rec.Urgent) THEN BEGIN
                                // on pose la question pour appliquer le coefficient
                                IF CONFIRM(STRSUBSTNO(gExpressConfirm001Qst, gRemiseOrder, gRemiseExpress), TRUE) THEN BEGIN
                                    Rec.VALIDATE("Discount1 %", gRemiseExpress);
                                    Rec.VALIDATE("Requested Delivery Date", CALCDATE('<3J>', Rec."Requested Delivery Date"));
                                END
                                ELSE
                                    ERROR('');
                            END
                            ELSE BEGIN
                                Rec.VALIDATE("Discount1 %", gRemiseOrder);
                                Rec.VALIDATE("Requested Delivery Date", CALCDATE('<-3J>', Rec."Requested Delivery Date"));
                            END;
                        END
                        // si le coefficient est KO
                        ELSE
                            IF (Rec.Urgent) THEN BEGIN
                                MESSAGE(gExpressInfo001Qst, gCoefficient);
                                ERROR('');
                            END;
                    END
                    // s'il n'est PAS possible d'appliquer une remise express
                    ELSE
                        ERROR('');
                end;
            }
            field("Has Comment"; Rec."Has Comment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Commentaire field.';
            }
        }
        moveafter("Has Comment"; Type)
        addafter(Type)
        {
            field(Kit; Rec.Kit)
            {
                QuickEntry = false;
                StyleExpr = gStyleExp_Kit;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Kit field.';

                trigger OnDrillDown();
                var
                    lBOMComponent: Record "BOM Component";
                    lPageAssemblyBOM: Page "Assembly BOM";
                begin

                    // CFR le 22/09/2021 => Régie - Bloquer la modification des nomenclatures d'assemblage
                    IF (Rec.Kit) THEN BEGIN
                        CLEAR(lBOMComponent);
                        lBOMComponent.SETRANGE("Parent Item No.", Rec."No.");
                        lPageAssemblyBOM.SETTABLEVIEW(lBOMComponent);
                        lPageAssemblyBOM.EDITABLE(FALSE);
                        lPageAssemblyBOM.RUNMODAL();
                    END;
                end;
            }
            field("Multiple de Vente"; Rec.GetSalesMultiple())
            {
                BlankNumbers = BlankZero;
                Caption = 'Multiple de Vente';
                DecimalPlaces = 0 : 2;
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multiple de Vente field.';
            }
        }
        addafter("Bin Code")
        {
            field("Référence saisie"; Rec."Référence saisie")
            {
                Editable = false;
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence saisie field.';
            }
            field("Recherche référence"; Rec."Recherche référence")
            {
                Caption = 'RechercheRef';
                StyleExpr = gStyleExp_NonDispo;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the RechercheRef field.';

                trigger OnValidate();
                begin
                    Rec.ShowShortcutDimCode(ShortcutDimCode);
                    RechercheRéfOnAfterValidate();
                end;
            }
        }
        addafter("Quantity Shipped")
        {
            field("Outstanding Qty. (Base)"; Rec."Outstanding Qty. (Base)")
            {
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Outstanding Qty. (Base) field.';
            }
        }
        addafter(Description)
        {
            field(GetItemRecommendedShippingAgentField; GetItemRecommendedShippingAgent())
            {
                Caption = 'Transporteur préconisé';
                StyleExpr = gStyleExp_Transporteur;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur préconisé field.';
            }
            field(GetItemMandatoryShippingAgentField; GetItemMandatoryShippingAgent())
            {
                Caption = 'Transporteur obligatoire';
                StyleExpr = gStyleExp_Transporteur;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur obligatoire field.';
            }
        }
        addafter("Unit Price")
        {
            field("Marge %"; Rec."Marge %")
            {
                StyleExpr = gStyleExp_Marge;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Marge / coût unitaire field.';
            }
            field("Discount1 %"; Rec."Discount1 %")
            {
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';

                trigger OnValidate();
                begin
                    RedistributeTotalsOnAfterValidate();
                end;
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';

                trigger OnValidate();
                begin
                    RedistributeTotalsOnAfterValidate();
                end;
            }
            field("Net Unit Price"; Rec."Net Unit Price")
            {
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix unitaire net field.';
            }
        }
        addafter("Substitution Available")
        {
            field("Additional Available"; Rec."Additional Available")
            {
                ApplicationArea = Suite;
                StyleExpr = gStyleExp_Additional;
                ToolTip = 'Spécifie que des articles complémentaires sont disponibles pour l''article sur la ligne vente.';

                trigger OnDrillDown();
                var
                    // lAdditionalItem: Record "Additional Item";
                    // lAdditionalItemList: Page "Additional Item List";
                    lSalesHeader: Record "Sales Header";
                    CduLFunctions: Codeunit "Codeunits Functions";
                begin
                    IF NOT Rec."Additional Available" THEN
                        ERROR('Il n''existe pas d''article associé pour l''article n°%1', Rec."No.");

                    // CFR le 10/03/2021 - SFD20210201 articles complémentaires
                    lSalesHeader.GET(Rec."Document Type", Rec."Document No.");
                    IF CduLFunctions.LookupAdditional(0, Rec) THEN
                        CurrPage.UPDATE();

                    /*
                    CLEAR(lAdditionalItem);
                    lAdditionalItem.SETRANGE("Item No.", Rec."No.");

                    CLEAR(lAdditionalItemList);
                    lAdditionalItemList.SETTABLEVIEW(lAdditionalItem);
                    lAdditionalItemList.LOOKUPMODE := TRUE;
                    lAdditionalItemList.EDITABLE(FALSE);
                    lAdditionalItemList.RUN();
                    */

                end;
            }
        }
        addafter("Location Code")
        {
            field("Zone Code"; Rec."Zone Code")
            {
                Editable = false;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code zone field.';
            }
        }

        addafter("Document No.")
        {
            field(CtrlHasItemLink; Rec.HasItemLink())
            {
                Caption = 'Lien article';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Lien article field.';
            }
        }
        addafter("Invoice Disc. Pct.")
        {
            field("Invoice Gross Weight"; TotalSalesLine."Gross Weight")
            {
                AutoFormatType = 1;
                Caption = 'Invoice Discount Amount';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Invoice Discount Amount field.';

                trigger OnValidate();
                var
                    SalesHeader: Record "Sales Header";
                begin
                    SalesHeader.GET(Rec."Document Type", Rec."Document No.");
                    SalesCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalSalesLine."Inv. Discount Amount", SalesHeader);
                    CurrPage.UPDATE(FALSE);
                end;
            }
            field(CtrlInvoiceGrossWeight; gTotalSalesLineToShip."Gross Weight")
            {
                AutoFormatType = 1;
                Caption = 'Invoice Discount Amount';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Invoice Discount Amount field.';
            }
        }
        addafter("Total Amount Excl. VAT")
        {
            field(CtrlTotalAmountExclVAT; gTotalSalesLineToShip.Amount)
            {
                ApplicationArea = Basic, Suite;
                AutoFormatExpression = Currency.Code;
                AutoFormatType = 1;
                Caption = 'Total Amount Excl. VAT';
                DrillDown = false;
                Editable = false;
                ToolTip = 'Specifies the sum of the value in the Line Amount Excl. VAT field on all lines in the document minus any discount amount in the Invoice Discount Amount field.';
            }
        }

    }
    actions
    {
        modify(SelectItemSubstitution)
        { Visible = false; }
        addafter(SelectItemSubstitution)
        {
            action(SelectItemSubstitution2)
            {
                AccessByPermission = TableData "Item Substitution" = R;
                ApplicationArea = Suite;
                Caption = 'Select Item Substitution';
                Image = SelectItemSubstitution;
                ToolTip = 'Select another item that has been set up to be sold instead of the original item if it is unavailable.';

                trigger OnAction()
                begin
                    CurrPage.SaveRecord();
                    Rec.ShowItemSub();
                    CurrPage.Update(true);
                    //GBO Le 29112108  => ticket 42715
                    //AutoReserve;
                    IF (Rec.Reserve = Rec.Reserve::Always) AND
                       (Rec."Outstanding Qty. (Base)" <> 0) AND
                       (Rec."Location Code" <> xRec."Location Code") THEN
                        Rec.AutoReserve();
                    //FIN GB
                    CurrPage.Update(false);
                end;
            }

        }
        addbefore("&Line")
        {
            action("Multi-Consultation")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                begin
                    MultiConsultation();
                end;
            }
            action(ExplodeBOM_Functions2)
            {
                AccessByPermission = TableData "BOM Component" = R;
                Caption = 'E&xplode BOM';
                Image = ExplodeBOM;
                ApplicationArea = All;
                ToolTip = 'Executes the E&xplode BOM action.';

                trigger OnAction();
                begin
                    ExplodeBOM();
                end;
            }
            action("Co&mmentaires")
            {
                Caption = 'Co&mments';
                Image = ViewComments;
                ApplicationArea = All;
                ToolTip = 'Executes the Co&mments action.';

                trigger OnAction();
                begin
                    Rec.ShowLineComments();
                end;
            }
        }
        modify("Roll Up &Price")
        {
            trigger OnAfterAction()
            begin
                //CFR le 04/10/2023 - R‚gie gestion des totaux … exp‚dier
                CodeunitsFunctions.CalculateSalesTotalsToShip(gTotalSalesLineToShip, gVATAmountToShip, Rec);
            end;
        }
        modify("Roll Up &Cost")
        {
            trigger OnAfterAction()
            begin
                //CFR le 04/10/2023 - R‚gie gestion des totaux … exp‚dier
                CodeunitsFunctions.CalculateSalesTotalsToShip(gTotalSalesLineToShip, gVATAmountToShip, Rec);
            end;
        }
    }


    var
        CodeunitsFunctions: Codeunit "Codeunits Functions";

    var
        gTotalSalesLineToShip: Record "Sales Line";
        Currency: Record Currency;
        SalesCalcDiscByType: Codeunit "Sales - Calc Discount By Type";
        StyleSalesMultiple: Boolean;
        gStyleExp_NonDispo: Text;
        gStyleExp_Kit: Text;
        gStyleExp_Substitution: Text;
        gStyleExp_Additional: Text;
        gRemiseExpress: Decimal;
        gRemiseOrder: Decimal;
        gCoefficient: Decimal;
        gExpressInfo001Qst: Label 'Pas de remise express par défaut car le coefficient est inférieur à 1 (%1)', Comment = '%1';
        gExpressConfirm001Qst: Label 'Souhaitez vous modifier la remise de %1 à %2 ?', Comment = '%1 %2';
        gEditableUrgent: Boolean;
        gStyleExp_Transporteur: Text;
        gStyleExp_Marge: Text;
        gVATAmountToShip: Decimal;



    trigger OnAfterGetRecord()
    var
        lItem: Record Item;
    begin
        FctSalesMultipleColor();// AD Le 29-12-2015

        // CFR le 18/11/2020 : R‚gie > Visibilit‚ stock non disponible
        IF (Rec."Document Type" = Rec."Document Type"::Order) THEN BEGIN
            // CFR le 18/04/2024 - R‚gie modification du gStyleExp_NonDispo
            Rec.CALCFIELDS("Whse. Outstanding Qty.");
            IF (Rec.Quantity <> Rec."Qty. to Ship") AND (Rec.Quantity <> Rec."Quantity Shipped") AND (Rec.Quantity <> Rec."Whse. Outstanding Qty.") THEN
                gStyleExp_NonDispo := 'Attention'
            ELSE
                gStyleExp_NonDispo := '';
        END;

        IF (Rec."Document Type" = Rec."Document Type"::Quote) OR (Rec."Document Type" = Rec."Document Type"::Order) THEN BEGIN
            IF (Rec.Kit) THEN
                gStyleExp_Kit := 'Unfavorable'
            ELSE
                gStyleExp_Kit := '';

            IF (Rec."Substitution Available") THEN
                gStyleExp_Substitution := 'Unfavorable'
            ELSE
                gStyleExp_Substitution := '';

            //CFR le 24/09/2021 => SFD20210201 articles compl‚mentaires
            IF (Rec."Additional Available") THEN
                gStyleExp_Additional := 'Unfavorable'
            ELSE
                gStyleExp_Additional := '';
        END;
        // Fin CFR le 18/11/2020

        //CFR le 06/04/2022 => R‚gie : [Couleur transporteur pr‚conis‚]
        gStyleExp_Transporteur := '';
        IF (Rec.Type = Rec.Type::Item) AND (Rec."No." <> '') AND (lItem.GET(Rec."No.")) THEN
            gStyleExp_Transporteur := lItem.GetRecommendedColor();
        //FIN CFR le 06/04/2022

        // CFR le 18/11/2020 : R‚gie > remises express
        gEditableUrgent := Rec.ExpressDiscount(gRemiseExpress, gRemiseOrder, gCoefficient, FALSE);
        // Fin CFR le 18/11/2020

        //CFR le 04/10/2023 - R‚gie ajout du calcul de marge
        gStyleExp_Marge := '';
        IF Rec."Marge %" < 0 THEN
            gStyleExp_Marge := 'Unfavorable';
        //FIN CFR le 04/10/2023

        //QuantityOnAfterValidate();
    end;

    trigger OnAfterGetCurrRecord()
    begin
        if Currency.Code <> TotalSalesHeader."Currency Code" then begin
            Clear(Currency);
            Currency.Initialize(TotalSalesHeader."Currency Code");
        end;
        //CFR le 04/10/2023 - R‚gie gestion des totaux … exp‚dier
        CodeunitsFunctions.CalculateSalesTotalsToShip(gTotalSalesLineToShip, gVATAmountToShip, Rec);

    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        // AD Le 26-01-2012 => Pour ne pas avoir des lignes avec ,5 dans les prepas
        IF Rec.Type = Rec.Type::Item THEN
            IF (Rec."Line No." MOD 10000) <> 0 THEN
                ERROR('Impossible d''inserer un ligne ici !');
        // FIN AD Le 26-01-2012
    end;

    procedure DetailLigne();
    begin
        // AD Le 12-12-2008 => Ouverture de l'écran détaillé

        IF (Rec.Type = Rec.Type::" ") THEN
            PAGE.RUNMODAL(PAGE::"Sales Line Détail Comment", Rec)
        ELSE
            PAGE.RUNMODAL(PAGE::"Sales Line Détail", Rec);
    end;

    procedure InsertItemCharge();
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Insertion des charges associées
        CurrPage.SAVERECORD();
        IF TransferItemCharge.SalesCheckIfAnyLink(Rec) THEN
            TransferItemCharge.InsertSalesLink(Rec);


        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure UpdateItemCharge();
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Modification des qtes de charges associées

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateQtySalesLink(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure UpdateLineType();
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Modification du type de ligne

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateLineType(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure InsertPort();
    var
        Type: Option Port,Emballage;
    begin
        // AD Le 31-08-2009 => Gestion des frais de port
        Rec.InsertPortLine(Type::Port);
    end;

    procedure InsertDiscount();
    var
        Type: Option Port,FraisFacturation,RemiseGlobale;
    begin
        // AD Le 31-08-2009 => Gestion des frais de port
        Rec.InsertPortLine(Type::RemiseGlobale);
    end;

    procedure CloseLine();
    begin
        Rec.CloseSalesLine();
    end;

    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;

    local procedure "RechercheRéfOnAfterValidate"();
    var
        // rec_item2: Record Item;
        // rec_param_gen: Record "Generals Parameters";
        // rec_itemunitmes: Record "Item Unit of Measure";
        SalesInfoPaneMgt: Codeunit "Sales Info-Pane Management";
    begin
        // AD Le 20-11-2015 => MIG2015
        IF SalesInfoPaneMgt.CalcNoOfSubstitutions(Rec) <> 0 THEN BEGIN
            Rec.ShowItemSubEskape();
            CurrPage.UPDATE();
        END;


        IF (Rec."Item Category Code" = 'G') AND
               (Rec."Shipping Agent Code" = 'CH') THEN
            MESSAGE('Transporteur incompatible avec article');

        /* AD Le 24-03-2016 => Décablé et mis dans le validation de la ligne
        //LM le 22-05-2013=>longeur hors norme
        rec_item2.INIT();
        IF rec_item2.GET("No.") THEN;
        IF rec_param_gen.GET('DIM_HORS_NORME','LONG') THEN;
        IF rec_itemunitmes.GET("No.",rec_item2."Base Unit of Measure") THEN;
        IF rec_itemunitmes.Length>=rec_param_gen.Decimal1 THEN MESSAGE('Longueur hors norme');
        
        */

        InsertExtendedText(FALSE);
        IF (Rec.Type = Rec.Type::"Charge (Item)") AND (Rec."No." <> xRec."No.") AND
           (xRec."No." <> '')
        THEN
            CurrPage.SAVERECORD();

        SaveAndAutoAsmToOrder();

        IF Rec.Reserve = Rec.Reserve::Always THEN BEGIN
            CurrPage.SAVERECORD();
            IF (Rec."Outstanding Qty. (Base)" <> 0) AND (Rec."No." <> xRec."No.") THEN BEGIN
                Rec.AutoReserve();
                CurrPage.UPDATE(FALSE);
            END;
        END;

        CurrPage.SAVERECORD();
        ; // AD Le 10-11-2015

    end;

    local procedure FctSalesMultipleColor();
    var
        CduLFunctions: Codeunit "Codeunits Functions";
    begin

        IF CduLFunctions.GetSalesMultiple(Rec) <> 0 THEN BEGIN
            IF (Rec.Quantity MOD CduLFunctions.GetSalesMultiple(Rec)) <> 0 THEN
                StyleSalesMultiple := TRUE
            ELSE
                StyleSalesMultiple := FALSE;
        END
        ELSE
            StyleSalesMultiple := FALSE;
    end;

    local procedure GetItemRecommendedShippingAgent(): Code[20];
    var
        lItem: Record Item;
    begin
        //CFR le 30/11/2022 => Régie : transporteur préconisé peut être obligatoire
        IF (Rec.Type = Rec.Type::Item) THEN
            IF lItem.GET(Rec."No.") THEN
                EXIT(lItem."Recommended Shipping Agent");
        EXIT('');
    end;

    local procedure GetItemMandatoryShippingAgent(): Boolean;
    var
        lItem: Record Item;
    begin
        //CFR le 30/11/2022 => Régie : transporteur préconisé peut être obligatoire
        IF (Rec.Type = Rec.Type::Item) THEN
            IF lItem.GET(Rec."No.") THEN
                EXIT(lItem."Mandatory Shipping Agent");
        EXIT(FALSE);
    end;

}

