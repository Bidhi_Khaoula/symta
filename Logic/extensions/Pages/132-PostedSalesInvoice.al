pageextension 50496 "Posted Sales Invoice" extends "Posted Sales Invoice" //132
{
    layout
    {
        addafter("No. Printed")
        {
            field("Packing List No."; Rec."Packing List No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the N° liste de colisage field.';

            }
        }
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                Editable = false;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
        }
    }

    actions
    {
        addbefore("&Invoice")
        {
            action("Multi-Consultation")
            {
                ApplicationArea = All;
                ShortCutKey = 'Ctrl+M';
                Promoted = true;
                PromotedIsBig = false;
                Image = CalculateConsumption;
                PromotedCategory = New;
                ToolTip = 'Executes the Multi-Consultation action.';
                trigger OnAction()
                VAR
                    FrmQuid: Page "Multi -consultation";
                BEGIN
                    CurrPage.SalesInvLines.PAGE.MultiConsultation();
                end;
            }
        }
        addafter(ActivityLog)
        {
            action("Nomenclature Douanière")
            {
                ApplicationArea = All;
                Image = PickLines;
                ToolTip = 'Executes the Nomenclature Douanière action.';

                trigger OnAction();
                var
                    Invoice: Record "Sales Invoice Header";
                begin
                    CurrPage.SETSELECTIONFILTER(Invoice);
                    REPORT.RUNMODAL(REPORT::"Nomenclature Douanière", TRUE, FALSE, Invoice);
                end;
            }
        }
    }
    PROCEDURE SendFaxMail(_pType: Code[10]);
    VAR
        LDoc: Record "Sales Invoice Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    BEGIN
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    END;
}