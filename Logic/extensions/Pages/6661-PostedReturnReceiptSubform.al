pageextension 51331 "Posted Return Receipt Subform" extends "Posted Return Receipt Subform" //6661
{
    layout
    {
        addafter(Control1)
        {
            field("Line No."; Rec."Line No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter("Shortcut Dimension 2 Code")
        {
            field("Unit Price"; Rec."Unit Price")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Price field.';
            }
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
            field(dec_MtLine; dec_MtLine)
            {
                Caption = 'Montant ligne';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant ligne field.';
            }
        }
    }
    actions
    {
        addafter(ItemCreditMemoLines)
        {
            action("Ouvrir multi")
            {
                ApplicationArea = All;
                Image = Open;
                ToolTip = 'Executes the Ouvrir multi action.';
                trigger OnAction();
                begin
                    MultiConsultation();
                end;
            }
        }
    }

    var
        dec_MtLine: Decimal;

    trigger OnAfterGetRecord()
    begin
        dec_MtLine := ROUND(Rec.Quantity * (Rec."Unit Price" * (1 - Rec."Line Discount %" / 100)));
    end;

    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;
}

