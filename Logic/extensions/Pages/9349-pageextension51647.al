pageextension 51647 pageextension51647 extends "Sales Order Archives" //9349
{
    layout
    {
        modify("Version No.")
        {
            Visible = false;
        }
        modify("Date Archived")
        {
            Visible = false;
        }
        modify("Time Archived")
        {
            Visible = false;
        }
        modify("Archived By")
        {
            Visible = false;
        }
        modify("Interaction Exist")
        {
            Visible = false;
        }
        addafter(Control1)
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        addafter("Sell-to Country/Region Code")
        {
            field("Bill-to Customer No."; Rec."Bill-to Customer No.")
            {
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
            }
        }
        addafter("Shipment Date")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Campaign No."; Rec."Campaign No.")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Campaign No. field.';
            }
            field(Amount; Rec.Amount)
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Amount field.';
            }
        }
    }
    actions
    {
        addafter("Co&mments")
        {
            group(Documents)
            {
                Caption = 'Documents';
                Image = Documents;
                action("E&xpéditions")
                {
                    Caption = 'S&hipments';
                    Image = Shipment;
                    RunObject = Page "Posted Sales Shipments";
                    RunPageLink = "Order No." = FIELD("No.");
                    RunPageView = SORTING("Order No.");
                    ApplicationArea = all;
                    ToolTip = 'Executes the S&hipments action.';
                }
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;
}

