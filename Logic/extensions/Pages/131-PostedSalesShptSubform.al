pageextension 50498 "Posted Sales Shpt. Subform" extends "Posted Sales Shpt. Subform" //131
{
    layout
    {
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter(Correction)
        {
            field("Unit Price"; Rec."Unit Price")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Unit Price field.';
            }
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
            field("Net Unit Price"; Rec."Net Unit Price")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Prix unitaire net field.';
            }
        }

    }

    actions
    {
        // Add changes to page actions here
    }
    trigger OnOpenPage()
    BEGIN
        // AD Le 21-10-2011 => SYMTA
        rec.SETFILTER(Quantity, '<>%1', 0);
    END;

    trigger OnAfterGetRecord()
    BEGIN
        // AD Le 21-10-2011 => SYMTA
        rec.SETFILTER(Quantity, '<>%1', 0);
    END;

    trigger OnAfterGetCurrRecord()
    BEGIN

        // AD Le 21-10-2011 => SYMTA
        rec.SETFILTER(Quantity, '<>%1', 0);
    END;

    PROCEDURE MultiConsultation();
    BEGIN
        rec.OuvrirMultiConsultation();
    END;
}