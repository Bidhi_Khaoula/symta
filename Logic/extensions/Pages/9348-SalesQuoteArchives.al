pageextension 51644 "Sales Quote Archives" extends "Sales Quote Archives" //9348
{
    layout
    {
        addafter("Shipment Date")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;


}

