pageextension 50775 "Sales Lines" extends "Sales Lines"  //516
{

    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Location Code")
        {
            Visible = false;
        }
        modify(Reserve)
        {
            Visible = false;
        }
        modify("Reserved Qty. (Base)")
        {
            Visible = false;
        }
        addafter("Unit of Measure Code")
        {
            field("Unit Price"; Rec."Unit Price")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Price field.';
            }
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
            field("Net Unit Price"; Rec."Net Unit Price")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix unitaire net field.';
            }
            field("Marge %"; Rec."Marge %")
            {
                ApplicationArea = All;
                Visible = false;
                ToolTip = 'Specifies the value of the Marge / coût unitaire field.';
            }
        }
        addafter("Line Amount")
        {
            field("Header Salesperson Code"; Rec."Header Salesperson Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Salesperson Code field.';
            }
        }
        addafter("Outstanding Quantity")
        {
            field("Requested Delivery Date"; Rec."Requested Delivery Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Requested Delivery Date field.';
            }
            field("Outstanding Qty. (Base)"; Rec."Outstanding Qty. (Base)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Outstanding Qty. (Base) field.';
            }
            field("Date Commande"; Rec.GetInfoEntete(1))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetInfoEntete(1) field.';
            }
            field("Nom Donneur d'ordre"; Rec.GetInfoEntete(2))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetInfoEntete(2) field.';
            }
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
            field("Référence saisie"; Rec."Référence saisie")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence saisie field.';
            }
            field("Description 3"; Rec."Description 3")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Informations vente field.';
            }
            field("Return Reason Code"; Rec."Return Reason Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Return Reason Code field.';
            }
            field("RV Defective Location"; Rec."RV Defective Location")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Localisation field.';
            }
        }
        moveafter("Référence saisie"; "Description 2")
    }
    actions
    {

    }
    Trigger OnOpenPage()
    BEGIN
        Rec.SETCURRENTKEY("Document Type", "Document No.", "Line No.");
        Rec.ASCENDING(FALSE);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    END;

    Trigger OnAfterGetRecord()
    var
        LItem: Record Item;
    BEGIN
        dec_reserve := 0;
        dec_attendu := 0;
        Dec_Stock := 0;
        // AD Le 02-11-2015
        IF LItem.GET(Rec."No.") THEN BEGIN
            LItem.CalcAttenduReserve(dec_reserve, dec_attendu);
            Dec_Stock := LItem.Inventory;
        END;
    END;


    var
        dec_reserve: Decimal;
        dec_attendu: Decimal;
        Dec_Stock: Decimal;



}

