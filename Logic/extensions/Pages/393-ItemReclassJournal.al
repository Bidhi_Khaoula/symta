pageextension 50569 "Item Reclass. Journal" extends "Item Reclass. Journal" //393
{
    layout
    {
        addafter(Control1)
        {
            field("A Valider"; Rec."A Valider")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the A Valider field.';
            }
        }
        addafter("Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
    }
    actions
    {
        modify(Post)
        {
            trigger OnBeforeAction()
            begin
                // AD Le 21-04-2015 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                Rec.SETRANGE("A Valider", TRUE);
                // FIN AD LE 21-04-2015 
            end;

            trigger OnAfterAction()
            begin
                // AD Le 21-04-2015 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                Rec.SETRANGE("A Valider");
                // FIN AD LE 21-04-2015
            end;
        }
        modify("Post and &Print")
        {
            trigger OnBeforeAction()
            begin
                // AD Le 21-04-2015 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                Rec.SETRANGE("A Valider", TRUE);
                // FIN AD LE 21-04-2015 
            end;

            trigger OnAfterAction()
            begin
                // AD Le 21-04-2015 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                Rec.SETRANGE("A Valider");
                // FIN AD LE 21-04-2015
            end;
        }

        addafter("Bin Contents")
        {
            action("Ouvrir multiconsultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Ouvrir multiconsultation action.';

                trigger OnAction();
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
    }

}

