pageextension 51636 "Sales Quotes" extends "Sales Quotes" //9300
{

    layout
    {
        modify("Sell-to Customer No.")
        {
            ToolTip = 'Specifies the number of the customer who will receive the products and be billed by default. When you fill this field, most of the other fields on the document are filled from the customer card.';
            StyleExpr = gStyleExp_Export;
        }
        modify("Sell-to Customer Name")
        {
            ToolTip = 'Specifies the name of the customer who will receive the products and be billed by default.';
            StyleExpr = gStyleExp_Export;
        }
        modify("Document Date")
        {
            Visible = true;
        }
        movebefore("No."; "Document Date")
        addafter("Document Date")
        {
            field("Quote Validity Date"; Rec."Quote Validity Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date de validité du devis field.';
            }
        }
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        addafter("Sell-to Customer Name")
        {
            field("Code Enseigne 1"; Rec."Code Enseigne 1")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
            }
            field("Code Enseigne 2"; Rec."Code Enseigne 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
            }
        }
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
        modify(Status)
        {
            Visible = true;
        }
        moveafter("Order Create User"; Status, Amount)
        addafter(Status)
        {
            field("Suivi Devis"; Rec."Suivi Devis")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Suivi Devis field.';
            }
        }
        modify("Ship-to Post Code")
        {
            Visible = true;
        }
        addafter("Ship-to Post Code")
        {
            field("Ship-to City"; Rec."Ship-to City")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ship-to City field.';
            }
        }
        modify("Assigned User ID")
        {
            Visible = false;
        }
        addafter("Opportunity No.")
        {
            field("Order Date"; Rec."Order Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
            field(Marge; Rec.CalcCost(1))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the CalcCost(1) field.';
            }
            field("% Marge"; Rec.CalcCost(2))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the CalcCost(2) field.';
            }
        }
        addbefore("Attached Documents")
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
        }
    }
    actions
    {
        modify("C&ontact")
        {
            Visible = false;
        }
        addafter("C&ontact")
        {
            action("C&ontact2")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'C&ontact';
                Enabled = ContactSelected;
                Image = Card;
                ToolTip = 'View or edit detailed information about the contact person at the customer.';
                Trigger OnAction()
                VAR
                    lContact: Record Contact;
                    lPageContactCard: Page "Contact Card";
                BEGIN
                    // CFR le 22/09/2021 => R‚gie - Bloquer la modification des contacts
                    CLEAR(lContact);
                    lContact.SETRANGE("No.", Rec."Sell-to Contact No.");
                    lPageContactCard.SETTABLEVIEW(lContact);
                    lPageContactCard.EDITABLE(FALSE);
                    lPageContactCard.RUNMODAL();
                END;
            }
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    FrmQuid.InitClient(Rec."Sell-to Customer No.");
                    FrmQuid.RUNMODAL();
                end;
            }
        }

    }

    var
        gStyleExp_Export: Text;

    trigger OnOpenPage()
    begin
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;

    trigger OnAfterGetRecord()
    begin
        // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
        SetStyleExp_Export();
        // FIN CFR le 05/04/2023
    end;

    local procedure SetStyleExp_Export();
    begin
        //CFR le 05/04/2023 => Régie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        IF (Rec."Salesperson Code" = '19') THEN
            gStyleExp_Export := 'Unfavorable';
        //FIN CFR le 05/04/2023
    end;



}

