pageextension 50675 "Contact Card" extends "Contact Card" //5050
{
    layout
    {

        addafter("Address 2")
        {
            field("Info Contact"; Rec."Info Contact")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Info Contact field.';
            }
        }
        modify(Address)
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
                CurrPage.UPDATE();
            end;
        }
        modify("Address 2")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
                CurrPage.UPDATE();
            end;
        }
        modify("Post Code")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
                CurrPage.UPDATE();
            end;
        }
        modify(City)
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
                CurrPage.UPDATE();
            end;
        }
        modify("Country/Region Code")
        {
            trigger OnAfterValidate()
            begin
                // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
                CurrPage.UPDATE();
            end;
        }
        addafter("VAT Registration No.")
        {
            group(NAViWay)
            {
                Caption = 'NAViWay';
                field(Latitude; Rec.Latitude)
                {
                    Enabled = NaviwayAccessOn;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Latitude field.';
                }
                field(Longitude; Rec.Longitude)
                {
                    Enabled = NaviwayAccessOn;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Longitude field.';
                }
                field("Change Coordinate"; Rec."Change Coordinate")
                {
                    Caption = 'Recalculer les coordonnées';
                    Enabled = NaviwayAccessOn;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recalculer les coordonnées field.';

                    trigger OnValidate();
                    begin
                        // CFR le 12/05/2022 => Régie : Vérification des nouvelles coordonnées : ajout Onglet "Mise à jour GPS"
                        CurrPage.UPDATE();
                    end;
                }
                field(Categorie; Rec.Categorie)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Categorie field.';
                }
            }
            group("Mise à jour GPS")
            {
                Caption = 'NAViWay';
                Visible = gIsChangeCoordinate;
                field("Old Address"; Rec."Old Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address field.';
                }
                field("Old Address 2"; Rec."Old Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address 2 field.';
                }
                field("Old City"; Rec."Old City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the City field.';
                }
                field("Old Post Code"; Rec."Old Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Post Code field.';
                }
                field("Old Country/Region Code"; Rec."Old Country/Region Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Country/Region Code field.';
                }
                field("Temporary Latitude"; Rec."Temporary Latitude")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Latitude temporaire field.';
                }
                field("Temporary Longitude"; Rec."Temporary Longitude")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Longitude  temporaire field.';
                }
                field("Result Code"; Rec."Result Code")
                {
                    Enabled = NaviwayAccessOn;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Result Code field.';
                }
            }
        }
    }
    actions
    {

        addafter("F&unctions")
        {
            action("Valider Nouvelles coordonnées")
            {
                Caption = 'Valider Nouvelles coordonnées';
                Enabled = gIsChangeCoordinate;
                Image = AlternativeAddress;
                ApplicationArea = All;
                ToolTip = 'Executes the Valider Nouvelles coordonnées action.';

                trigger OnAction();
                begin
                    // CFR le 12/05/2022 => Régie : Vérification des nouvelles coordonnées : ajout Onglet "Mise à jour GPS"
                    Rec.ValidateNewGPS();
                    CurrPage.UPDATE();
                end;
            }
        }
        modify("Launch &Web Source")
        {
            Enabled = false;
        }
    }

    var
        NaviwayAccessOn: Boolean;
        gIsChangeCoordinate: Boolean;

    trigger OnOpenPage()
    begin
        // MIG2017
        NaviwayAccessOn := TRUE;
    end;

    trigger OnAfterGetRecord()
    begin
        // CFR le 12/05/2022 => R‚gie : V‚rification des nouvelles coordonn‚es : ajout Onglet "Mise … jour GPS"
        gIsChangeCoordinate := Rec."Change Coordinate";
    end;

}

