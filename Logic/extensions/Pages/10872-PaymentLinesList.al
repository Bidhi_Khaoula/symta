pageextension 50043 "Payment Lines List" extends "Payment Lines List" //10872
{
    layout
    {
        addafter("Document No.")
        {
            field("Désignation"; Rec.Désignation)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Désignation field.';
            }
        }
        addafter("Payment in Progress")
        {
            field("RIB Checked"; Rec."RIB Checked")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the RIB Checked field.';
            }
        }
    }
    actions
    {

        addafter(Modify)
        {
            action("Calcul de la sélection")
            {
                Caption = 'Calcul de la sélection';
                Image = AddAction;
                Promoted = true;
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Calcul de la sélection action.';

                trigger OnAction();
                var
                    PaimentManagementNN: Codeunit "PaymentManagementNAViNégoce";
                begin
                    PaimentManagementNN.SelectionBordereau(Rec);
                end;
            }
        }
    }



}

