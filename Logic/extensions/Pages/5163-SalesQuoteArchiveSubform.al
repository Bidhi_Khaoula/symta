pageextension 50799 "Sales Quote Archive Subform" extends "Sales Quote Archive Subform" //5163
{
    layout
    {
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
    }

}

