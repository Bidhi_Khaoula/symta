pageextension 50007 "Purchase Orders" extends "Purchase Orders" //56
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
    }
}