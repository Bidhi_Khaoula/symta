pageextension 50970 "Value Entries" extends "Value Entries" //5802
{

    layout
    {
        addafter("Posting Date")
        {
            field("Date Réelle Ecriture"; Rec."Date Réelle Ecriture")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Réelle Ecriture field.';
            }
            field("User Entry"; Rec."User Entry")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the User ID field.';
            }
        }
    }

}

