pageextension 50217 "Posted Sales Credit Memos" extends "Posted Sales Credit Memos" //144
{
    layout
    {
        modify("Posting Date")
        {
            Visible = true;
        }
        movebefore("No."; "Posting Date")
        modify("Currency Code")
        {
            Visible = false;
        }
        modify("Bill-to Customer No.")
        {
            Visible = true;
        }
        modify("Bill-to Name")
        {
            Visible = true;
        }
        modify("Salesperson Code")
        {
            Visible = true;
        }
    }
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("No.");
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF rec.FINDFIRST() THEN;
    end;
}