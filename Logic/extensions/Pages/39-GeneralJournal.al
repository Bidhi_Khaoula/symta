pageextension 50578 "General Journal" extends "General Journal" //39
{

    layout
    {
        addafter("Account No.")
        {
            field(isBlocked; FctisBlocked())
            {
                Caption = 'Bloqué';
                ToolTip = 'Info bloqué pour type client/fournisseur';
                ApplicationArea = All;
            }
        }
        addafter("Direct Debit Mandate ID")
        {
            field("VAT Base Amount"; Rec."VAT Base Amount")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the VAT Base Amount field.';
            }

        }
        modify("Amount (LCY)")
        {
            Visible = true;
        }
        moveafter("VAT Base Amount"; "Amount (LCY)")
    }
    actions
    {
        addafter(PostAndPrint)
        {
            action(DemandeTransport)
            {
                Caption = 'Extraire depuis demande transport';
                Image = Shipment;
                ApplicationArea = All;
                ToolTip = 'Executes the Extraire depuis demande transport action.';

                trigger OnAction();
                var
                    DemandeTransport: Record "Demande transporteur";
                    DemandeTransSetup: Record "Shipping Setup";
                    lTotal: Decimal;
                begin
                    DemandeTransport.SETRANGE(Facturé, FALSE);
                    IF PAGE.RUNMODAL(PAGE::"Demande Transporteur List", DemandeTransport) = ACTION::LookupOK THEN BEGIN
                        DemandeTransSetup.GET();
                        lTotal := DemandeTransport."Cout Transport" + DemandeTransport."Frais annexe" + DemandeTransport."Frais douane" + DemandeTransport."Frais emballage";
                        IF lTotal = 0 THEN ERROR('Aucun cout renseigné pour cette demande');

                        IF DemandeTransport."Cout Transport" > 0 THEN
                            AddLineAccountDemande(DemandeTransSetup."Compte Cout de transport", DemandeTransport."Cout Transport", DemandeTransport);
                        IF DemandeTransport."Frais annexe" > 0 THEN
                            AddLineAccountDemande(DemandeTransSetup."Compte Frais annexe", DemandeTransport."Frais annexe", DemandeTransport);
                        IF DemandeTransport."Frais douane" > 0 THEN
                            AddLineAccountDemande(DemandeTransSetup."Compte Frais douane", DemandeTransport."Frais douane", DemandeTransport);
                        IF DemandeTransport."Frais emballage" > 0 THEN
                            AddLineAccountDemande(DemandeTransSetup."Compte Frais d'emballage", DemandeTransport."Frais emballage", DemandeTransport);

                        AddLineAccountDemande(DemandeTransport."Shipping Vendor No.", -lTotal, DemandeTransport);
                    END;
                    CurrPage.UPDATE();
                end;
            }
        }
    }

    local procedure FctisBlocked(): Text[50];
    var
        lCustomer: Record Customer;
        lVendor: Record Vendor;
    begin
        // CFR le 22/09/2021 => Régie : info compte bloqué
        IF (Rec."Account No." = '') THEN
            EXIT('');

        IF (Rec."Account Type" = Rec."Account Type"::Customer) THEN
            IF lCustomer.GET(Rec."Account No.") THEN
                EXIT(FORMAT(lCustomer.Blocked));

        IF (Rec."Account Type" = Rec."Account Type"::Vendor) THEN
            IF lVendor.GET(Rec."Account No.") THEN
                EXIT(FORMAT(lVendor.Blocked));

        EXIT('');
    end;

    local procedure AddLineAccountDemande(pAccountNo: Code[20]; pAmount: Decimal; pDemandeTransport: Record "Demande transporteur");
    var
        GenJnlLine: Record "Gen. Journal Line";
        DefaultDimension: Record "Default Dimension";
        GenJnlTemplate: Record "Gen. Journal Template";
        lLineNo: Integer;
    begin
        GenJnlTemplate.GET(Rec."Journal Template Name");

        GenJnlLine.SETRANGE("Journal Template Name", Rec."Journal Template Name");
        GenJnlLine.SETRANGE("Journal Batch Name", Rec."Journal Batch Name");
        IF GenJnlLine.FINDLAST() THEN lLineNo := GenJnlLine."Line No.";
        lLineNo += 1000;

        GenJnlLine.INIT();
        GenJnlLine.VALIDATE("Journal Template Name", Rec."Journal Template Name");
        GenJnlLine.VALIDATE("Journal Batch Name", Rec."Journal Batch Name");
        GenJnlLine.VALIDATE("Line No.", lLineNo);
        GenJnlLine.VALIDATE("Source Code", GenJnlTemplate."Source Code");
        IF pAmount < 0 THEN
            GenJnlLine.VALIDATE("Account Type", GenJnlLine."Account Type"::Vendor)
        ELSE
            GenJnlLine.VALIDATE("Account Type", GenJnlLine."Account Type"::"G/L Account");
        GenJnlLine.VALIDATE("Account No.", pAccountNo);
        GenJnlLine.VALIDATE("Posting Date", TODAY);
        GenJnlLine.VALIDATE(Amount, pAmount);

        GenJnlLine.VALIDATE("Document Type", GenJnlLine."Document Type"::DemandeTransporteur);
        GenJnlLine.VALIDATE("Document No.", pDemandeTransport."No.");
        IF pDemandeTransport."Customer No." <> '' THEN
            GenJnlLine.ValidateShortcutDimCode(3, pDemandeTransport."Customer No.");
        IF pDemandeTransport."Vendor No." <> '' THEN
            GenJnlLine.ValidateShortcutDimCode(3, pDemandeTransport."Vendor No.");

        DefaultDimension.SETRANGE("Table ID", DATABASE::"Shipping Agent");
        DefaultDimension.SETRANGE("Dimension Code", 'TRANSPORTEUR');
        DefaultDimension.SETRANGE("No.", pDemandeTransport."Shipping Vendor No.");
        IF DefaultDimension.FINDFIRST() THEN
            GenJnlLine.ValidateShortcutDimCode(4, DefaultDimension."Dimension Value Code");

        GenJnlLine.INSERT(TRUE);
    end;

}

