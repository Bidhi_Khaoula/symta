pageextension 51370 "Warehouse Employees" extends "Warehouse Employees" //7328
{

    layout
    {
        addafter("ADCS User")
        {
            field("Validation Logistique Direct"; Rec."Validation Logistique Direct")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Validation Logistique Direct field.';
            }
            field("Code Remettant Logiflux"; Rec."Code Remettant Logiflux")
            {
                Enabled = false;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Remettant Logiflux field.';
            }
            field("Pas de Contrôle Poids"; Rec."Pas de Contrôle Poids")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Pas de Contrôle Poids field.';
            }
        }
    }

}

