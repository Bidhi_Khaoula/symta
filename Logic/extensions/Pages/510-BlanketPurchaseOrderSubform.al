pageextension 50008 "Blanket Purchase Order Subform" extends "Blanket Purchase Order Subform" //510
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
    }
}