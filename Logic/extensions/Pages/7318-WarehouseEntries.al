pageextension 50016 "Warehouse Entries" extends "Warehouse Entries" //7318
{
    layout
    {
        addafter("Entry No.")
        {
            field(Commentaire; Rec.Commentaire)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Commentaire field.';
            }

            field(Stock; FctGetQtyBase())
            {
                ApplicationArea = all;
                Caption = 'Stock réel contenu emplacement - Quantité (base)';
                ToolTip = 'Quantité (base) du contenu emplacement pour l''article';
            }
        }
    }
    procedure FctGetQtyBase(): Decimal
    var
        gBinContent: Record "Bin Content";
    begin
        // CFR le 18/01/2024 - R‚gie : Ajout du stock r‚el pour le contenu emplacement
        IF gBinContent.GET(Rec."Location Code", Rec."Bin Code", Rec."Item No.", Rec."Variant Code", Rec."Unit of Measure Code") THEN
            gBinContent.CALCFIELDS("Quantity (Base)");
        Exit(gBinContent."Quantity (Base)");
    end;

}