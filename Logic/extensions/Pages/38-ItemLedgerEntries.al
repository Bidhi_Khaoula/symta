pageextension 50548 "Item Ledger Entries" extends "Item Ledger Entries" //38
{

    layout
    {
        addafter("Document Line No.")
        {
            field("JoGeneral Journal"; Rec."Journal Batch Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Journal Batch Name field.';
            }
        }
        addafter("Item No.")
        {
            field("Référence saisie"; Rec."Référence saisie")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence saisie field.';
            }
        }
        modify("Source Type")
        {
            Visible = true;
        }
        modify("Source No.")
        {
            Visible = true;
        }
        moveafter("Job Task No."; "Source Type", "Source No.")
        addafter("Source No.")
        {
            field("Nom Tiers"; Rec.GetNameSource())
            {
                Caption = 'Nom Tiers';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nom Tiers field.';
            }
            field("GetEnseigne(1)"; Rec.GetEnseigne(1))
            {
                Caption = 'Code enseigne 1';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code enseigne 1 field.';
            }
            field("GetEnseigne(2)"; Rec.GetEnseigne(2))
            {
                Caption = 'Code enseigne 2';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code enseigne 2 field.';
            }
            field("Ref Active"; Rec."Ref Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref Active field.';
            }
            field("Item Category Code"; Rec."Item Category Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Item Category Code field.';
            }
            //todo
            // field("Product Group Code"; Rec."Product Group Code")
            // {
            // }
            field("Warehouse Document No."; Rec."Warehouse Document No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° document magasin field.';
            }
            field("Source Document No."; Rec."Source Document No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° document origine field.';
            }
            field("Return Purchase Adjustment"; Rec."Return Purchase Adjustment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Régularisation Retour Achat field.';
            }
            field("Return Purchase Request"; Rec."Return Purchase Request")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Demande Retour Achat field.';
            }
            field("Warehouse Receipt Adjustment"; Rec."Warehouse Receipt Adjustment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Régularisation Réception Magasin field.';
            }
            field("Warehouse Receipt Request"; Rec."Warehouse Receipt Request")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Demande Réception Magasin field.';
            }
        }
    }
    Trigger OnOpenPage()
    BEGIN
        // CFR le 22/09/2021 => R‚gie : tri par [Entry No.] D‚croissant et se positionne sur la premiŠre
        IF Rec.FINDFIRST() THEN;
    END;

}

