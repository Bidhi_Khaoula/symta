pageextension 50103 "Sales & Receivables Setup" extends "Sales & Receivables Setup" //459
{
    layout
    {
        addafter("Allow Document Deletion Before")
        {
            field("Do not search description"; Rec."Do not search description")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Do not search description field.';
            }
        }
        addafter("Background Posting")
        {
            group(Autre)
            {
                group(Coef)
                {
                    field("Coef. Saisonnalité Janvier"; Rec."Coef. Saisonnalité Janvier")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Janvier field.';
                    }
                    field("Coef. Saisonnalité Février"; Rec."Coef. Saisonnalité Février")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Février field.';
                    }
                    field("Coef. Saisonnalité Mars"; Rec."Coef. Saisonnalité Mars")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Mars field.';
                    }
                    field("Coef. Saisonnalité Avril"; Rec."Coef. Saisonnalité Avril")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Avril field.';
                    }
                    field("Coef. Saisonnalité Mai"; Rec."Coef. Saisonnalité Mai")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Mai field.';
                    }
                    field("Coef. Saisonnalité Juin"; Rec."Coef. Saisonnalité Juin")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Juin field.';
                    }
                    field("Coef. Saisonnalité Juillet"; Rec."Coef. Saisonnalité Juillet")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Juillet field.';
                    }
                    field("Coef. Saisonnalité Aout"; Rec."Coef. Saisonnalité Aout")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Aout field.';
                    }
                    field("Coef. Saisonnalité Septembre"; Rec."Coef. Saisonnalité Septembre")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Septembre field.';
                    }
                    field("Coef. Saisonnalité Octobre"; Rec."Coef. Saisonnalité Octobre")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Octobre field.';
                    }
                    field("Coef. Saisonnalité Novembre"; Rec."Coef. Saisonnalité Novembre")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Novembre field.';
                    }
                    field("Coef. Saisonnalité Decembre"; Rec."Coef. Saisonnalité Decembre")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Coef. Saisonnalité Decembre field.';
                    }
                    field("Sales LCY"; Rec."Sales LCY")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Sales LCY field.';
                    }

                }
                group(Relance)
                {
                    field("Niveau de relance pour alerte"; Rec."Niveau de relance pour alerte")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Niveau de relance pour alerte field.';
                    }
                    field("Alerte sur Relance"; Rec."Alerte sur Relance")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Alerte sur Relance field.';
                    }
                    field("Delai de securité (Paiement)"; Rec."Delai de securité (Paiement)")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Delai de securité (Paiement) field.';
                    }
                    field("Longueur Alerte Dimension"; Rec."Longueur Alerte Dimension")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Longueur Alerte Dimension (en m) field.';
                    }

                }
                group(Transport)
                {
                    field("Shipment label Nos."; Rec."Shipment label Nos.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the N° Etiquette Expédition field.';
                    }
                    field("Shipment label Entry Nos."; Rec."Shipment label Entry Nos.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the N° Ecriture Etiquette Expédition field.';
                    }
                    field("Shipment Damage Nos."; Rec."Shipment Damage Nos.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Order Nos. field.';
                    }
                    field("Shipment Method"; Rec."Shipment Method")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Méthode de livraison field.';
                    }
                    field("Code Client Reprise Express"; Rec."Code Client Reprise Express")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Code Client Remise Express field.';
                    }
                    field("Teliae Export Folder"; Rec."Teliae Export Folder")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Teliae Export Folder field.';
                    }
                    field("Teliae Error Mail"; Rec."Teliae Error Mail")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Teliae Error Mail field.';
                    }
                    field("Teliae Return Folder"; Rec."Teliae Return Folder")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Teliae Return Folder field.';
                    }
                    field("Package Nos."; Rec."Package Nos.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Package Nos. field.';
                    }
                    field("Quote Default Validity Period"; Rec."Quote Default Validity Period")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Délai de validité par défaut des devis field.';
                    }
                    field("Quote Max Validity Period"; Rec."Quote Max Validity Period")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Délai de validité maximum des devis field.';
                    }
                }

            }
        }
    }

}