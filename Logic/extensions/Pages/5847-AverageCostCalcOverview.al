pageextension 51051 "Average Cost Calc. Overview" extends "Average Cost Calc. Overview" //5847
{
    layout
    {
        modify(AverageCostCntrl)
        {
            Visible = false;
        }
        addafter(AverageCostCntrl)
        {
            field(AverageCostCntrlSYM; Rec.CalculateAverageCostSymta())
            {
                ApplicationArea = Basic, Suite;
                AutoFormatType = 2;
                Caption = 'Unit Cost';
                DecimalPlaces = 0 : 5;
                Editable = false;
                StyleExpr = 'Strong';
                ToolTip = 'Specifies the average cost for this entry.';
            }
        }

    }
    actions
    {

    }
    // local procedure ToggleExpandCollapse(var AvgCostCalcOverview: Record "Average Cost Calc. Overview");
    // var
    //     CopyOfAvgCostCalcOverview: Record "Average Cost Calc. Overview";
    //     xAvgCostCalcOverview: Record "Average Cost Calc. Overview" temporary;
    // begin
    //     xAvgCostCalcOverview := Rec;
    //     CopyOfAvgCostCalcOverview.COPY(AvgCostCalcOverview);
    //     IF ActualExpansionStatus = 0 THEN BEGIN // Has children, but not expanded
    //         AvgCostCalcOverview := Rec;
    //         GetAvgCostCalcOverview.Calculate(AvgCostCalcOverview);
    //         AvgCostCalcOverview.SETFILTER("Entry No.", '<>%1', Rec."Entry No.");
    //         AvgCostCalcOverview.SETRANGE(Level, Rec.Level, Rec.Level + 1);
    //         REPEAT
    //             IF AvgCostCalcOverview.Level > xAvgCostCalcOverview.Level THEN BEGIN
    //                 Rec := AvgCostCalcOverview;
    //                 IF Rec.INSERT THEN;
    //             END;
    //         UNTIL (AvgCostCalcOverview.NEXT() = 0) OR (AvgCostCalcOverview.Level = xAvgCostCalcOverview.Level);
    //     END ELSE
    //         IF ActualExpansionStatus = 1 THEN BEGIN // Has children and is already expanded
    //             WHILE (Rec.NEXT(GetDirection) <> 0) AND (Rec.Level > xAvgCostCalcOverview.Level) DO
    //                 Rec.DELETE;
    //         END;
    //     AvgCostCalcOverview.COPY(CopyOfAvgCostCalcOverview);
    //     Rec := xAvgCostCalcOverview;
    //     CurrPage.UPDATE;
    // end;



}

