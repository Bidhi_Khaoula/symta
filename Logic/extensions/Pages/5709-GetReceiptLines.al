pageextension 50978 "Get Receipt Lines" extends "Get Receipt Lines"//5709
{
    layout
    {
        addbefore("Document No.")
        {
            field("N° Reception"; GetReceiptNo())
            {
                Caption = 'N° Reception';
                Style = Favorable;
                StyleExpr = ReceptionCherchée;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° Reception field.';
            }
            field("Whse. Receipt No."; Rec."Whse. Receipt No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Whse. Receipt No. field.';
            }
            field("Vendor Shipment No."; Rec."Vendor Shipment No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
            }
        }
        addafter("Buy-from Vendor No.")
        {
            field("Order No."; Rec."Order No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order No. field.';
            }
        }
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
            field("Unit Cost (LCY)"; Rec."Unit Cost (LCY)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Cost (LCY) field.';
            }
        }
        moveafter("Recherche référence"; "Vendor Item No.")
    }

    var
        PostedWhRecepLine: Record "Posted Whse. Receipt Line";
        no_rec_rech: Code[20];
        "ReceptionCherchée": Boolean;

    procedure GetReceiptNo(): Code[20];
    var
        PostedWhRecepLine: Record "Posted Whse. Receipt Line";
    begin
        PostedWhRecepLine.SETRANGE("Posted Source Document", PostedWhRecepLine."Posted Source Document"::"Posted Receipt");
        PostedWhRecepLine.SETRANGE("Posted Source No.", Rec."Document No.");
        IF PostedWhRecepLine.FindFirst() THEN
            EXIT(PostedWhRecepLine."Whse. Receipt No.")
        ELSE
            EXIT(' ');
    end;

    procedure GetInfoEntete(_pType: Code[10]): Code[35];
    var
        LReceiptHeader: Record "Purch. Rcpt. Header";
    begin
        IF NOT IsFirstDocLine() THEN EXIT;

        LReceiptHeader.GET(Rec."Document No.");
        CASE _pType OF
            'NOBL':
                IF LReceiptHeader."Vendor Shipment No." = '' THEN
                    EXIT('--')
                ELSE
                    EXIT(LReceiptHeader."Vendor Shipment No.");
        END;
    end;

    trigger OnOpenPage()
    var
        PagLWhseReceipt: page WhseReceipt;
    begin
        IF PagLWhseReceipt.RunModal() = action::OK then
            no_rec_rech := PagLWhseReceipt.FctGetWhseReceiptNo()
        else
            exit;
    end;

    trigger OnAfterGetRecord()
    begin
        // AD Le 10-11-2015
        ReceptionCherchée := FALSE;

        PostedWhRecepLine.SETRANGE("Posted Source Document", PostedWhRecepLine."Posted Source Document"::"Posted Receipt");
        PostedWhRecepLine.SETRANGE("Posted Source No.", Rec."Document No.");
        IF PostedWhRecepLine.FIND('-') THEN
            IF STRPOS(PostedWhRecepLine."Whse. Receipt No.", no_rec_rech) > 0 THEN
                ReceptionCherchée := TRUE;
    end;
}

