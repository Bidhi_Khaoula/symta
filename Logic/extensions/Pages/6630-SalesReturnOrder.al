pageextension 51221 "Sales Return Order" extends "Sales Return Order" //6630
{

    layout
    {
        modify("Bill-to")
        {
            Visible = false;
        }
        modify("Sell-to Customer Name")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Address")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Address 2")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Post Code")
        {
            Editable = EditableESK;
        }
        modify("Sell-to City")
        {
            Editable = EditableESK;
        }
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
            field("Transport Information"; Rec."Transport Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. transport field.';
            }
        }
        addafter("No.")
        {
            field("Type retour"; Rec."Type retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type retour field.';
            }
        }
        addafter("Type retour")
        {
            field("Sell-to Customer No. 2"; Rec."Sell-to Customer No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
            }
        }
        addafter("Sell-to Contact")
        {
            field("Code Abattement"; Rec."Code Abattement")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Abattement field.';
            }
            field("Taux Abattement"; Rec."Taux Abattement")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Taux Abattement field.';
            }
        }
        modify("Document Date")
        {
            Editable = EditableESK;
        }
        addafter("Order Date")
        {
            field("Source Document Type"; Rec."Source Document Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
        }
        addafter("Responsibility Center")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
        modify("VAT Bus. Posting Group")
        {
            Editable = EditableESK;
        }
        modify("Currency Code")
        {
            Editable = EditableESK;
        }
        modify("Prices Including VAT")
        {
            Editable = EditableESK;
        }
        modify("Transaction Type")
        {
            Editable = EditableESK;
        }
        modify("Shortcut Dimension 1 Code")
        {
            Editable = EditableESK;
        }
        modify("Shortcut Dimension 2 Code")
        {
            Editable = EditableESK;
        }
        modify("Shipment Date")
        {
            Editable = EditableESK;
        }
        modify("Applies-to Doc. Type")
        {
            Editable = EditableESK;
        }
        modify("Applies-to Doc. No.")
        {
            Editable = EditableESK;
        }
        modify("Applies-to ID")
        {
            Editable = EditableESK;
        }
        addafter(Status)
        {
            field("Return Status"; Rec."Return Status")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut Retour field.';
            }
            field("Port Charge Client"; Rec."Port Charge Client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Port Charge Client field.';
            }
            field("Enlevement Charge Client"; Rec."Enlevement Charge Client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Enlevement Charge Client field.';
            }
            group("Description du travail")
            {
                Caption = 'Work Description';
                field(WorkDescription; WorkDescription)
                {
                    Importance = Additional;
                    MultiLine = true;
                    ShowCaption = false;
                    ToolTip = 'Specifies the products or service being offered';
                    ApplicationArea = All;

                    trigger OnValidate();
                    begin
                        Rec.SetWorkDescription(WorkDescription);
                    end;
                }
            }
        }
        addafter("Applies-to ID")
        {
            field("Bill-to Customer No."; Rec."Bill-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
            }
            field(BillToName1; Rec."Bill-to Name")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Name';
                Importance = Promoted;
                ToolTip = 'Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.';

                trigger OnValidate();
                begin
                    IF Rec.GETFILTER("Bill-to Customer No.") = xRec."Bill-to Customer No." THEN
                        IF Rec."Bill-to Customer No." <> xRec."Bill-to Customer No." THEN
                            Rec.SETRANGE("Bill-to Customer No.");


                    CurrPage.UPDATE();
                end;
            }
        }
        addafter("Ship-to Contact")
        {
            group(Control1100284033)
            {
                ShowCaption = false;
                field(BillToOptions; BillToOptions)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Bill-to';
                    OptionCaption = 'Default (Customer),Another Customer';
                    ToolTip = 'Specifies the customer that the sales invoice will be sent to. Default (Customer): The same as the customer on the sales invoice. Another Customer: Any customer that you specify in the fields below.';

                    trigger OnValidate();
                    begin
                        IF BillToOptions = BillToOptions::"Default (Customer)" THEN
                            Rec.VALIDATE("Bill-to Customer No.", Rec."Sell-to Customer No.");

                        CurrPage.UPDATE();
                    end;
                }
                group(Control1100284031)
                {
                    Visible = BillToOptions = BillToOptions::"Another Customer";
                    field("Bill-to Name2"; Rec."Bill-to Name")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Name';
                        Importance = Promoted;
                        ToolTip = 'Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.';

                        trigger OnValidate();
                        begin
                            IF Rec.GETFILTER("Bill-to Customer No.") = xRec."Bill-to Customer No." THEN
                                IF Rec."Bill-to Customer No." <> xRec."Bill-to Customer No." THEN
                                    Rec.SETRANGE("Bill-to Customer No.");


                            CurrPage.UPDATE();
                        end;
                    }
                    field("Bill-to AddressN"; Rec."Bill-to Address")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Address';
                        Editable = EditableEsk;
                        Importance = Additional;
                        ToolTip = 'Specifies the address of the customer that you will send the invoice to.';
                    }
                    field("Bill-to Address 2 2"; Rec."Bill-to Address 2")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Address 2';
                        Editable = EditableEsk;
                        Importance = Additional;
                        ToolTip = 'Specifies additional address information.';
                    }
                    field("Bill-to Post Code2"; Rec."Bill-to Post Code")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Post Code';
                        Editable = EditableEsk;
                        Importance = Additional;
                        ToolTip = 'Specifies the postal code.';
                    }
                    field("Bill-to City2"; Rec."Bill-to City")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'City';
                        Editable = EditableEsk;
                        Importance = Additional;
                        ToolTip = 'Specifies the city you will send the invoice to.';
                    }
                    field("Bill-to Contact No.2"; Rec."Bill-to Contact No.")
                    {
                        Caption = 'Contact No.';
                        Editable = EditableEsk;
                        Importance = Additional;
                        ToolTip = 'Specifies the number of the contact the invoice will be sent to.';
                        ApplicationArea = All;
                    }
                    field("Bill-to Contact2"; Rec."Bill-to Contact")
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Contact';
                        Editable = EditableEsk;
                        ToolTip = 'Specifies the name of the person you should contact at the customer who you are sending the invoice to.';
                    }
                }
            }
        }
        addafter("Area")
        {
            group(Garantie)
            {
                Caption = 'Garantie';
                field("Type Matériel Garantie"; Rec."Type Matériel Garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Matériel Garantie field.';
                }
                field("No Serie Garantie"; Rec."No Serie Garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Serie Garantie field.';
                }
                field("Date Montage Garantie"; Rec."Date Montage Garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Montage Garantie field.';
                }
                field("Heures Fct Ou Kms garantie"; Rec."Heures Fct Ou Kms garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heures Fct Ou Kms garantie field.';
                }
                field("Date dépose Garantie"; Rec."Date dépose Garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dépose Garantie field.';
                }
                field("Heures 2 Fct Ou Kms garantie"; Rec."Heures 2 Fct Ou Kms garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heures 2 Fct Ou Kms garantie field.';
                }
                field("Garantie Accordée"; Rec."Garantie Accordée")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Garantie Accordée field.';
                }
                field("Comment garantie"; Rec."Comment garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
                field("Commentaire garantie"; Rec."Commentaire garantie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire garantie field.';
                }
            }
            part(Control1100284036; "Posted Return Receipts")
            {
                SubPageLink = "Return Order No." = FIELD("No.");
                ApplicationArea = All;
            }
        }
    }
    actions
    {

        addafter("Return Receipts")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin

                    CurrPage.SalesLines.PAGE.MultiConsultation();
                end;
            }
        }
        addafter("Co&mments")
        {
            action(Action1100284017)
            {
                Caption = 'Co&mments';
                Image = ViewComments;
                Promoted = false;
                ApplicationArea = all;
                RunObject = Page "Sales Comment Sheet";
                RunPageLink = "Document Type" = CONST("Retour garantie"), "No." = FIELD("No."), "Document Line No." = CONST(0);
                Visible = false;
                ToolTip = 'Executes the Co&mments action.';
            }
        }

    }
    trigger OnAfterGetRecord()
    begin
        WorkDescription := Rec.GetWorkDescription();
    end;

    var
        EditableESK: Boolean;

    var
        WorkDescription: Text;
        BillToOptions: Option "Default (Customer)","Another Customer";

    procedure sendfaxMail(_pType: Code[10]);
    var
        LDoc: Record "Sales Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    begin

        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    end;

}

