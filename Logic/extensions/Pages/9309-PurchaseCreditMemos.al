pageextension 51697 "Purchase Credit Memos" extends "Purchase Credit Memos" //9309
{
    layout
    {
        addafter("Job Queue Status")
        {
            field("Montant HT facture"; Rec."Montant HT facture")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant TTC field.';
            }
        }
        moveafter("Montant HT facture"; "Vendor Cr. Memo No.")
    }
    actions
    {

    }


}

