pageextension 50646 "Purch. Cr. Memo Subform" extends "Purch. Cr. Memo Subform" //98
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
        modify("Bin Code")
        {
            editable = false;
        }
        addbefore("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter(Type)
        {
            field("Line No."; Rec."Line No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
        }
        modify("Gen. Prod. Posting Group")
        {
            Visible = true;
        }
        Moveafter(Nonstock; "Gen. Prod. Posting Group")
        addafter("Return Reason Code")
        {
            field(GetReturnNoName; GetReturnNo())
            {
                Editable = False;
                ApplicationArea = all;
                Caption = 'N° Retour';
                ToolTip = 'Specifies the value of the N° Retour field.';
            }
        }
        addafter("Line Amount")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
        }
        movebefore(Control47; Control23)
        addafter("Invoice Disc. Pct.")
        {
            field("Diff. H.T"; DiffHT)
            {
                Editable = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the DiffHT field.';
            }
        }
    }

    actions
    {
        modify(GetReturnShipmentLines)
        {
            trigger OnAfterAction()
            begin
                PurchHeader.GET(rec."Document Type", rec."Document No.");
                IF PurchHeader."Prices Including VAT" THEN BEGIN
                    TotalAmount2 := TotalPurchaseLine.Amount;
                    TotalAmount1 := TotalAmount2 + VATAmount;
                    TotalPurchaseLine."Line Amount" := TotalAmount1 + TotalPurchaseLine."Inv. Discount Amount";
                END ELSE BEGIN
                    TotalAmount1 := TotalPurchaseLine.Amount;
                    TotalAmount2 := TotalPurchaseLine."Amount Including VAT";
                END;

                // AD Le 12-05-2016
                DiffHT := TotalPurchaseLine."Amount Including VAT" - PurchHeader."Montant HT facture";
                IF (TotalAmount1 <> 0) AND (TotalAmount2 <> 0) THEN
                    DiffHT := (PurchHeader."Montant HT facture" - TotalAmount2) / (TotalAmount2 / TotalAmount1);
                // FIN AD Le 12-05-2016      
                CurrPage.UPDATE();
            end;
        }
    }
    var
        PurchHeader: Record "Purchase Header";
        DiffHT: Decimal;
        TotalAmount2: Decimal;
        TotalAmount1: Decimal;

    trigger OnAfterGetCurrRecord()
    begin
        PurchHeader.GET(rec."Document Type", rec."Document No.");
        // AD Le 12-05-2016
        IF PurchHeader."Prices Including VAT" THEN BEGIN
            TotalAmount2 := TotalPurchaseLine.Amount;
            TotalAmount1 := TotalAmount2 + VATAmount;
            TotalPurchaseLine."Line Amount" := TotalAmount1 + TotalPurchaseLine."Inv. Discount Amount";
        END ELSE BEGIN
            TotalAmount1 := TotalPurchaseLine.Amount;
            TotalAmount2 := TotalPurchaseLine."Amount Including VAT";
        END;

        // ANI Le 02-12-2015 : erreur division par 0
        DiffHT := 0;
        IF TotalAmount1 > 0 THEN
            IF (TotalAmount2 / TotalAmount1) > 0 THEN
                DiffHT := (PurchHeader."Montant HT facture" - TotalAmount2) / (TotalAmount2 / TotalAmount1);

    end;

    PROCEDURE GetReturnNo(): Code[20];
    VAR
        lReturnShipmentLine: Record "Return Shipment Line";
    BEGIN
        //CFR le 21/09/2021 : R‚gie > Avoir achat (Ajout nø retour)
        IF (Rec.Type = Rec.Type::Item) AND (Rec."Return Shipment No." <> '') THEN BEGIN
            lReturnShipmentLine.SETRANGE("Document No.", Rec."Return Shipment No.");
            lReturnShipmentLine.SETRANGE("Line No.", Rec."Return Shipment Line No.");
            IF lReturnShipmentLine.FINDFIRST() THEN
                EXIT(lReturnShipmentLine."Return Order No.");

            //   {
            //   IF lReturnShipmentHeader.GET(Rec."Return Shipment No.") THEN
            //         EXIT(lReturnShipmentHeader."Return Order No.");
            //   }
        END
        ELSE
            EXIT('');
    END;
}