pageextension 50552 "Phys. Inventory Ledger Entries" extends "Phys. Inventory Ledger Entries" //390
{
    layout
    {
        addafter("Entry Type")
        {
            field("Journal Batch Name"; Rec."Journal Batch Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Journal Batch Name field.';
            }
        }
        addafter("Item No.")
        {
            field("No. 2"; Rec."No. 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. 2 field.';
            }
        }
        addafter("Location Code")
        {
            field("Bin Code"; Rec."Bin Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Bin Code field.';
            }
            field("Warehouse User ID"; Rec."Warehouse User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Warehouse User ID field.';
            }
        }
        addafter("Entry No.")
        {
            field("Product Type"; Rec."Product Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Product Type field.';
            }
            field("Manufacturer Code"; Rec."Manufacturer Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Manufacturer Code field.';
            }
            field("Item Category Code"; Rec."Item Category Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Item Category Code field.';
            }
        }
    }


}

