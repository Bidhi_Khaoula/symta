pageextension 50788 "Sales List Archive" extends "Sales List Archive" //5161
{
    layout
    {
        addbefore("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        modify("Sell-to Customer No.")
        {
            Visible = true;
        }
        modify("Version No.")
        {
            Visible = false;
        }
        modify("Date Archived")
        {
            Visible = false;
        }
        modify("Time Archived")
        {
            Visible = false;
        }
        modify("Archived By")
        {
            Visible = false;
        }
        modify("Interaction Exist")
        {
            Visible = false;
        }

        addafter("Currency Code")
        {
            field("Document Date"; Rec."Document Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Document Date field.';
            }
            field("Requested Delivery Date"; Rec."Requested Delivery Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Requested Delivery Date field.';
            }
            field("Payment Terms Code"; Rec."Payment Terms Code")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Payment Terms Code field.';
            }
            field("Due Date"; Rec."Due Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Due Date field.';
            }
            field("Payment Discount %"; Rec."Payment Discount %")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Payment Discount % field.';
            }
            field("Shipment Method Code"; Rec."Shipment Method Code")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Shipment Date"; Rec."Shipment Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipment Date field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Campaign No."; Rec."Campaign No.")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Campaign No. field.';
            }
            field(Amount; Rec.Amount)
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Amount field.';
            }
        }
    }
}

