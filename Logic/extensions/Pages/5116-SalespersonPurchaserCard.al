pageextension 50685 "Salesperson/Purchaser Card" extends "Salesperson/Purchaser Card" //5116
{

    layout
    {
        addafter("Next Task Date")
        {
            field("Export Salesperson"; Rec."Export Salesperson")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Export Salesperson field.';
            }
        }
        addafter("Global Dimension 2 Code")
        {
            group(NAViWay)
            {
                Caption = 'NAViWay';
                field("Windows Login"; Rec."Windows Login")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Windows Login field.';
                }
                field(AllCustomersAccess; Rec.AllCustomersAccess)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Accès tous clients field.';
                }
            }
        }
    }
    actions
    {
    }
}
