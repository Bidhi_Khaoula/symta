pageextension 50150 "Req. Worksheet" extends "Req. Worksheet" //291
{
    actions
    {
        addafter(CarryOutActionMessage)
        {
            action(InverserMessageaction)
            {
                ApplicationArea = All;
                Caption = 'Inverser Message d''action';
                Promoted = true;
                Image = ChangeStatus;
                PromotedCategory = Process;
                ToolTip = 'Executes the Inverser Message d''action action.';
                trigger OnAction()
                begin
                    Decocher();
                end;
            }
        }
    }
    PROCEDURE Decocher();
    VAR
        LLigne: Record "Requisition Line";
    BEGIN
        LLigne.SETRANGE("Worksheet Template Name", rec."Worksheet Template Name");
        LLigne.SETRANGE("Journal Batch Name", rec."Journal Batch Name");
        IF LLigne.FINDFIRST() THEN
            REPEAT
                LLigne.VALIDATE("Accept Action Message", NOT LLigne."Accept Action Message");
                LLigne.MODIFY();
            UNTIL LLigne.NEXT() = 0;

        CurrPage.UPDATE(FALSE);
    END;
}