pageextension 51316 "Get Sales Price" extends "Get Sales Price" //7007
{

    layout
    {
        addafter("Allow Line Disc.")
        {
            field("Prix catalogue"; Rec."Prix catalogue")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix catalogue field.';
            }
            field(Coefficient; Rec.Coefficient)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Coefficient field.';
            }
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
    }
}

