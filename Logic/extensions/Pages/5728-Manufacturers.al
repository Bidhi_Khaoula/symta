pageextension 50944 Manufacturers extends Manufacturers //5728
{

    layout
    {
        addafter(Name)
        {
            field("Gen. Prod. Posting Group"; Rec."Gen. Prod. Posting Group")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gen. Prod. Posting Group field.';
            }
            field("Tariff No."; Rec."Tariff No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Tariff No. field.';
            }
            field("Colonne Stats"; Rec."Colonne Stats")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Colonne Stats field.';
            }
            field("Tri Stats"; Rec."Tri Stats")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Tri Stats field.';
            }
        }
    }
    actions
    {
        addfirst(Processing)
        {
            action("Envoyer au MiniLoad")
            {
                ApplicationArea = all;
                Image = SendTo;
                ToolTip = 'Executes the Envoyer au MiniLoad action.';
                trigger OnAction();
                begin
                    Rec.EnvoyerMiniload()
                end;
            }
        }
    }

    procedure GetSelectionFilter(): Text;
    var
        Manufacturer: Record Manufacturer;
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        CurrPage.SETSELECTIONFILTER(Manufacturer);
        EXIT(CduLFunctions.GetSelectionFilterForManufacturer(Manufacturer));
    end;

}

