pageextension 50050 "Shipment Methods" extends "Shipment Methods" //11
{
    layout
    {
        addafter(Description)
        {
            field("Show Risk Transfer Address"; rec."Show Risk Transfer Address")
            {
                ToolTip = 'Précisez si vous souhaitez imprimer sur la commande d''achat la ville Incoterm, si elle a été  renseignée.';
                ApplicationArea = All;
            }
        }
    }


}

