pageextension 51325 "Purchase Line Discounts" extends "Purchase Line Discounts" //7014
{
    layout
    {
        modify("Line Discount %")
        {
            Editable = false;
        }
        addafter(Control1)
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
    }

}

