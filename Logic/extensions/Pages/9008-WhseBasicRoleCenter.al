pageextension 51576 "Whse. Basic Role Center" extends "Whse. Basic Role Center" //9008
{

    actions
    {
        modify(Vendors)
        {
            Visible = false;
        }
        modify(SalesReturnOrders)
        {
            Visible = false;
        }
        modify("Inventory Pi&ck")
        {
            Visible = false;
        }
        modify("Inventory P&ut-away")
        {
            Visible = false;
        }
        modify(InventoryMovements)
        {
            Visible = false;
        }
        modify("Internal Movements")
        {
            Visible = false;
        }
        modify(PurchaseReturnOrders)
        {
            Visible = false;
        }
        modify(TransferOrders)
        {
            Visible = false;
        }
        modify(ReleasedProductionOrders)
        {
            Visible = false;
        }
        modify("Customer &Labels")
        {
            Visible = false;
        }
        addafter("Physical &Inventory List")
        {
            action("Etat des réceptions")
            {
                Caption = 'Etat des réceptions';
                Image = PrintReport;
                // Promoted = true;
                // PromotedCategory = "Report";
                RunObject = Report "Etat des réceptions";
                ApplicationArea = All;
                ToolTip = 'Executes the Etat des réceptions action.';
            }
            action("Réappro Picking")
            {
                Caption = 'Réappro Picking';
                Image = CalculateBinReplenishment;
                // PromotedCategory = Process;
                // Promoted = true;
                // PromotedIsBig = true;
                RunObject = Report "Réappro. Picking";
                ApplicationArea = All;
                ToolTip = 'Executes the Réappro Picking action.';
            }
        }
        addafter(PhysInventoryJournals)
        {
            action("Expédition Entrepôt")
            {
                Caption = 'Expédition Entrepôt';
                RunObject = Page "Warehouse Shipment List";
                ApplicationArea = All;
                Image = Shipment;
                ToolTip = 'Executes the Expédition Entrepôt action.';
            }
            action("Réception Entrepôt")
            {
                Caption = 'Réception Entrepôt';
                RunObject = Page "Warehouse Receipts";
                ApplicationArea = All;
                Image = Receipt;
                ToolTip = 'Executes the Réception Entrepôt action.';
            }
        }
        addafter("Edit Item Reclassification &Journal")
        {
            action(Miniload)
            {
                Caption = 'Miniload';
                RunObject = Page "Item Reclass. Journal MiniLoad";
                ApplicationArea = All;
                Image = Navigate;
                ToolTip = 'Executes the Miniload action.';
            }
        }
        addafter("Item &Tracing")
        {
            separator(Separator1100284004)
            {
            }
            action("Modula & Etiquette")
            {
                Caption = 'Modula & Etiquette';
                Image = PrintCheck;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Envoi Bp Modula";
                ApplicationArea = All;
                ToolTip = 'Executes the Modula & Etiquette action.';
            }
            action("Validation Préparation")
            {
                Caption = 'Validation Préparation';
                Image = CalculateShipment;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Warehouse Shipment SYMTA";
                ShortCutKey = 'F9';
                ApplicationArea = All;
                ToolTip = 'Executes the Validation Préparation action.';
            }
            action("Expédition")
            {
                Caption = 'Expédition';
                Image = UpdateShipment;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Saisie du Pied de BL";
                ApplicationArea = All;
                ToolTip = 'Executes the Expédition action.';
            }
            action("Réédition Colis")
            {
                Caption = 'Réédition Colis';
                Image = ItemSubstitution;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Saisie Pied BL - Avec detail";
                ApplicationArea = All;
                ToolTip = 'Executes the Réédition Colis action.';
            }
            separator(Separator1100284008)
            {
            }
            action("Réception")
            {
                Caption = 'Réception';
                Image = PostedReceivableVoucher;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Saisie Réception";
                ApplicationArea = All;
                ToolTip = 'Executes the Réception action.';
            }
            separator(Separator1100284011)
            {
            }
            action("Mouvement Casier")
            {
                Caption = 'Mouvement Casier';
                Image = CopyItem;
                // Promoted = true;
                // PromotedCategory = Process;
                // PromotedIsBig = true;
                RunObject = Page "Movement Casier 2015";
                ApplicationArea = All;
                ToolTip = 'Executes the Mouvement Casier action.';
            }
            separator(Separator1100284014)
            {
            }
            action("Envoyer Article Miniload")
            {
                Caption = 'Envoyer Article Miniload';
                Image = PostedInventoryPick;
                RunObject = Codeunit "Send all item on miniload";
                ApplicationArea = All;
                ToolTip = 'Executes the Envoyer Article Miniload action.';
            }
            action("Multi-Consultation")
            {
                Caption = 'Multi-Consultation';
                Image = CalculateConsumption;
                // Promoted = true;
                // PromotedCategory = New;
                RunObject = Page "Multi -consultation";
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';
            }
        }
    }


}

