pageextension 50014 "Location Card" extends "Location Card" //5703
{
    layout
    {
        addafter("Pick According to FEFO")
        {
            Group(SYMTA)
            {
                field("Emplacement Tampon WIIO"; Rec."Emplacement Tampon WIIO")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Emplacement Tampon WIIO field.';
                }
            }
        }
    }

}