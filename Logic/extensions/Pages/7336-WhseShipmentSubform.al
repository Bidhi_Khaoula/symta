pageextension 51336 "Whse. Shipment Subform" extends "Whse. Shipment Subform" //7336
{
    layout
    {
        modify("Zone Code")
        {
            Editable = false;
        }
        modify("Bin Code")
        {
            Editable = false;
        }
        addafter("Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        modify("Qty. to Ship")
        {
            Trigger OnBeforeValidate()
            BEGIN
                CurrPage.UPDATE();
            END;
        }
        addafter("Qty. to Ship")
        {
            field("Qty. Packing List"; Rec."Qty. Packing List")
            {
                StyleExpr = gStyleExpQtePL;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qté dans Liste de colisage field.';
            }
        }
        addafter(QtyCrossDockedAllUOMBase)
        {
            field("Date Flashage"; Rec."Date Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Flashage field.';
            }
            field("Heure Flashage"; Rec."Heure Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Flashage field.';
            }
            field("Utilisateur Flashage"; Rec."Utilisateur Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Utilisateur Flashage field.';
            }
            field("WIIO Préparation"; Rec."WIIO Préparation")
            {
                Caption = 'Pointage terminal';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Pointage terminal field.';
            }
        }
    }
    var
        gWarehouseShipmentHeader: Record "Warehouse Shipment Header";
        gStyleExpQtePL: Text[20];

    trigger OnAfterGetRecord()
    begin

        // CFR 02/09/2020 > WIIO - Packing List - format des qt‚ en PL
        IF (gWarehouseShipmentHeader.GET(Rec."No.")) AND (gWarehouseShipmentHeader."Packing List No." = '') THEN
            gStyleExpQtePL := ''
        ELSE
            IF (Rec."Qty. Packing List" = 0) THEN
                gStyleExpQtePL := 'Ambiguous'
            ELSE
                IF (Rec."Qty. Packing List" = Rec."Qty. to Ship") THEN
                    gStyleExpQtePL := 'Favorable'
                ELSE
                    gStyleExpQtePL := 'Unfavorable';
        // FIN CFR 02/09/2020
    end;

    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;

}

