pageextension 50959 "Item Category Card" extends "Item Category Card" //5733
{
    layout
    {
        addafter("Parent Category")
        {
            field("Exclude Express Discount"; Rec."Exclude Express Discount")
            {
                ToolTip = 'Lorsque cette case est cochée, il n''est pas possible de sélectionner [Express] en ligne de devis/commande';
                ApplicationArea = All;
            }
        }
    }
    actions
    {
    }


}

