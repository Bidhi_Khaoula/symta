pageextension 50246 "Purchases & Payables Setup" extends "Purchases & Payables Setup" //460
{
    layout
    {
        addafter("Allow Document Deletion Before")
        {
            field("Magasin Retour"; Rec."Magasin Retour")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Return location field.';
            }
            field("Quadient Export Path"; Rec."Quadient Export Path")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Chemin export Quadient field.';
            }
        }
    }


}