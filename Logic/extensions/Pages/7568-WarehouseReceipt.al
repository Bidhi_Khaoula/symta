pageextension 51049 "Warehouse Receipt" extends "Warehouse Receipt" //5768
{

    layout
    {
        addafter("Document Status")
        {
            field(Position; Rec.Position)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Position field.';
            }
        }
        addafter("Vendor Shipment No.")
        {
            field("Vendor Type No."; Rec."Vendor Type No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type n° doc. fournisseur field.';
            }
            field(Information; Rec.Information)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Information field.';
            }
            field("Date Arrivage Marchandise"; Rec."Date Arrivage Marchandise")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Arrivage Marchandise field.';
            }
        }
        addafter("Sorting Method")
        {
            field("Montant a réceptionner"; Rec.GetAmount())
            {
                Style = Strong;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetAmount() field.';
            }
            field("Filtre Fournisseur"; Rec."Filtre Fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Filtre Fournisseur field.';
            }
            field(rech_ref_active; rech_ref_active)
            {
                Caption = 'Recherche par ref. active';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche par ref. active field.';

                trigger OnValidate();
                var
                    rec_line_recep: Record "Warehouse Receipt Line";
                    rec_item: Record Item;
                begin

                    //LM le 06-08-2012 rechere par ref active
                    rec_item.RESET();
                    rec_item.SETRANGE(rec_item."No. 2", rech_ref_active);
                    IF rec_item.FINDFIRST() THEN BEGIN
                        rec_line_recep.RESET();
                        rec_line_recep.SETRANGE(rec_line_recep."Item No.", rec_item."No.");
                        IF NOT rec_line_recep.ISEMPTY THEN
                            MESSAGE('Existe sous référence ' + rec_item."No.");


                    END
                end;
            }
        }
        modify(WhseReceiptLines)
        {
            Visible = false;
        }
        addafter(WhseReceiptLines)
        {
            part(WhseReceiptLines2; "Whse. Receipt Subform")
            {
                ApplicationArea = Warehouse;
                Editable = IsReceiptLinesEditable;
                Enabled = IsReceiptLinesEditable;
                SubPageLink = "No." = field("No.");
                SubPageView = sorting("No.", "Sorting Sequence No.");
                UpdatePropagation = Both;
            }
        }
        addafter(Control1901796907)
        {
            part("Multi Consultation Qty Factbox"; "Multi Consultation Qty Factbox")
            {
                Caption = 'Quantités';
                Provider = WhseReceiptLines;
                SubPageLink = "No." = FIELD("Item No."), "Location Filter" = FIELD("Location Code");
                Visible = true;
                ApplicationArea = all;
            }
        }
    }
    actions
    {

        modify("Post Receipt")
        {
            Visible = false;
        }
        addafter("Post Receipt")
        {
            action("Post Receipt Symta")
            {
                ApplicationArea = Warehouse;
                Caption = 'P&ost Receipt';
                Image = PostOrder;
                ShortCutKey = 'F9';
                ToolTip = 'Post the items as received. A put-away document is created automatically.';

                trigger OnAction()
                begin
                    "ImprimerListe affectation"();
                    WhsePostRcptYesNoSymta();
                    //LM le 11-10-2012 =>test si genere plusieurs RME
                    rec_receipt_line.RESET();
                    rec_receipt_line.SETRANGE(rec_receipt_line."Whse. Receipt No.", txt_No);
                    IF rec_receipt_line.FINDFIRST() THEN BEGIN
                        rme := rec_receipt_line."No.";
                        REPEAT
                            IF rec_receipt_line."No." <> rme THEN
                                MESSAGE('ATTENTION, il y a plusieurs RME pour un RM');
                        UNTIL rec_receipt_line.NEXT() = 0;
                    END;
                end;
            }
        }
        modify("Post and Print P&ut-away")
        {
            Visible = false;
        }
        addafter("Post and Print P&ut-away")
        {
            action("Post and Print P&ut-away Symta")
            {
                ApplicationArea = Warehouse;
                Caption = 'Post and Print P&ut-away';
                Image = PostPrint;
                ShortCutKey = 'Shift+Ctrl+F9';
                ToolTip = 'Post the items as received and print the put-away document.';

                trigger OnAction()
                begin
                    "ImprimerListe affectation"();
                    WhsePostRcptPrintSymta();
                end;
            }
        }
        modify("Post and &Print")
        {
            Visible = false;
        }
        addafter("Post and &Print")
        {
            action("Post and &Print Symta")
            {
                ApplicationArea = Warehouse;
                Caption = 'Post and &Print';
                Image = PostPrint;
                ShortCutKey = 'Shift+F9';
                ToolTip = 'Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.';

                trigger OnAction()
                begin
                    "ImprimerListe affectation"();
                    WhsePostRcptPrintPostedRcptSymta();
                end;
            }
        }
        addbefore("F&unctions")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = all;
                ToolTip = 'Executes the Multi-Consultation action.';
                trigger OnAction();
                begin
                    CurrPage.WhseReceiptLines.PAGE.MultiConsultation2();
                end;
            }
        }
        addafter("Get Source Documents")
        {
            action("Extraire Cde Achat")
            {
                Image = RefreshText;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'F7';
                ApplicationArea = all;
                ToolTip = 'Executes the Extraire Cde Achat action.';
                trigger OnAction();
                var
                    FrmExtraireLgAch: Page "Filtre Pour reception magasin";
                begin

                    //fbrun le 05/12/2011 pouvoir extraire une seul ligne de commande
                    CLEAR(FrmExtraireLgAch);
                    FrmExtraireLgAch.SetOneCreatedReceiptHeader(Rec);
                    FrmExtraireLgAch.FctFiltreFrn(Rec."Filtre Fournisseur");
                    FrmExtraireLgAch.RUNMODAL();

                    // MCO Le 14-02-2018 => Régie
                    CurrPage.WhseReceiptLines.PAGE.PositionEnBas();
                end;
            }
        }
        addafter(CalculateCrossDock)
        {
            separator(Separator1100284005)
            {
            }
            action("Extraire ligne facture")
            {
                ApplicationArea = all;
                ToolTip = 'Executes the Extraire ligne facture action.';
                trigger OnAction();
                begin
                    ExtraireFacture();
                end;
            }
        }
        addafter("Post and Print P&ut-away")
        {
            action("Validation litiges")
            {
                ApplicationArea = all;
                Image = BreakpointsList;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ToolTip = 'Executes the Validation litiges action.';

                trigger OnAction();
                var
                    WhseRcptLine: Record "Warehouse Receipt Line";
                    PurchRcptLine: Record "Purch. Rcpt. Line";
                    lgestionlitige: Codeunit "Gestion des litiges";
                begin

                    //LM le 31-05-2013 =>gestion des litiges
                    WhseRcptLine.RESET();
                    WhseRcptLine.SETRANGE(WhseRcptLine."No.", Rec."No.");
                    IF WhseRcptLine.FINDFIRST() THEN
                        REPEAT
                            lgestionlitige.CréerLitige(WhseRcptLine, PurchRcptLine);
                        UNTIL WhseRcptLine.NEXT() = 0;
                end;
            }
            action(Fusion)
            {
                ApplicationArea = all;
                Caption = 'Fusion réception';
                Image = CollapseDepositLines;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ToolTip = 'Executes the Fusion réception action.';

                trigger OnAction();
                var
                    lFusionReception: Page "Fusion Réception";
                begin
                    // CFR le 22/03/2023 => Régie : Ajout action [Fusion]
                    lFusionReception.InitNoReceptionPrincipale(Rec."No.");
                    lFusionReception.RUN();
                end;
            }
        }
        addafter("&Print")
        {
            action(Affectation)
            {
                ApplicationArea = all;
                Image = ExportReceipt;
                ToolTip = 'Executes the Affectation action.';

                trigger OnAction();
                begin
                    "ImprimerListe affectation"();
                end;
            }
        }
    }

    var
        rec_receipt_line: Record "Posted Whse. Receipt Line";
        rme: Text[30];
        txt_No: Code[20];
        rech_ref_active: Code[20];
        ESK001Qst: Label 'Imprimer la liste des affectations clients ?';
        IsReceiptLinesEditable: Boolean;

    trigger OnOpenPage()
    begin
        IsReceiptLinesEditable := Rec.ReceiptLinesEditable();
    end;

    procedure ExtraireFacture();
    var
        CuGestionFou: Codeunit "Réception Fournisseur";
    begin
        CuGestionFou."Extraire Ligne"(Rec);
    end;

    procedure "ImprimerListe affectation"();
    var
        LWhseheader: Record "Warehouse Receipt Header";
        LRpAffactation: Report "Affectation Réception";
    begin
        IF NOT CONFIRM(ESK001Qst, TRUE) THEN EXIT;

        LWhseheader.SETRANGE("No.", Rec."No.");
        LRpAffactation.SETTABLEVIEW(LWhseheader);
        LRpAffactation.RUNMODAL();
    end;

    local procedure WhsePostRcptYesNoSymta()
    begin
        CurrPage.WhseReceiptLines.PAGE.WhsePostRcptYesNoSymta();
    end;

    local procedure WhsePostRcptPrintSymta()
    begin
        CurrPage.WhseReceiptLines.PAGE.WhsePostRcptPrintSymta();
    end;

    local procedure WhsePostRcptPrintPostedRcptSymta()
    begin
        CurrPage.WhseReceiptLines.PAGE.WhsePostRcptPrintPostedRcptSymta();
    end;
}

