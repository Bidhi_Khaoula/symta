pageextension 50946 "Stockkeeping Unit Card" extends "Stockkeeping Unit Card" //5700
{
    layout
    {
        modify("Include Inventory")
        {
            Importance = Additional;
        }
        modify("Lot Accumulation Period")
        {
            Importance = Additional;
        }
        modify("Rescheduling Period")
        {
            Importance = Additional;
        }
        movelast("Reorder-Point Parameters"; "Include Inventory", "Lot Accumulation Period", "Rescheduling Period")
    }

}

