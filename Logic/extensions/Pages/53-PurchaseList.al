pageextension 50795 "Purchase List" extends "Purchase List" //53
{

    layout
    {
        addafter("No.")
        {
            field("Vendor Invoice No."; Rec."Vendor Invoice No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Invoice No. field.';
            }
        }
        addafter("Buy-from Contact")
        {
            field(Amount; Rec.Amount)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Amount field.';
            }
        }
    }


}

