pageextension 50034 "View/Edit Payment Line" extends "View/Edit Payment Line" //10862
{

    layout
    {
        addafter(Name)
        {
            field(Montant; Rec.Montant)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant field.';
            }
        }
    }

}

