pageextension 50601 "Sales Order Statistics" extends "Sales Order Statistics" //402
{

    layout
    {
        modify(LineAmountGeneral)
        {
            CaptionClass = GetCaptionClass(Text002Lbl, FALSE);
        }
        modify("TotalAmount1[1]")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, FALSE);
        }
        modify("TotalAmount2[1]")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, TRUE);
        }
        modify("AmountInclVAT_Invoicing")
        {
            CaptionClass = GetCaptionClass(Text002Lbl, FALSE);
        }
        modify("TotalInclVAT_Invoicing")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, FALSE);
        }
        modify("TotalExclVAT_Invoicing")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, TRUE);
        }
        modify("TotalSalesLine[3].""Line Amount""")
        {
            CaptionClass = GetCaptionClass(Text002Lbl, FALSE);
        }
        modify("TotalAmount1[3]")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, FALSE);
        }
        modify("TotalAmount2[3]")
        {
            CaptionClass = GetCaptionClass(Text001Lbl, TRUE);
        }
        modify(PrepmtTotalAmount)
        {
            CaptionClass = GetCaptionClass(Text006Lbl, FALSE);
        }
        modify(PrepmtTotalAmount2)
        {
            CaptionClass = GetCaptionClass(Text006Lbl, TRUE);
        }
        modify("TotalSalesLine[1].""Prepmt. Amt. Inv.""")
        {
            CaptionClass = GetCaptionClass(Text007Lbl, FALSE);
        }
        modify("TotalSalesLine[1].""Prepmt Amt Deducted""")
        {
            CaptionClass = GetCaptionClass(Text008Lbl, FALSE);
        }
        modify("TotalSalesLine[1].""Prepmt Amt to Deduct""")
        {
            CaptionClass = GetCaptionClass(Text009Lbl, FALSE);
        }

        addafter("TotalAmount2[1]")
        {
            field("Gross Total"; TotalAmountBrut[1])
            {
                AutoFormatExpression = 'Currency Code';
                AutoFormatType = 1;
                Caption = 'Gross Total';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gross Total field.';
            }
        }
        addafter("NoOfVATLines_General")
        {
            field("TotalAmount1[4]"; TotalAmount1[4])
            {
                AutoFormatExpression = 'Currency Code';
                AutoFormatType = 1;
                CaptionClass = GetCaptionClass(ESK001Lbl, FALSE);
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the TotalAmount1[4] field.';
            }
            field("TotalSalesLine[4].""Net Weight"""; TotalSalesLine[4]."Net Weight")
            {
                Caption = 'Net Weight';
                DecimalPlaces = 0 : 5;
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Net Weight field.';
            }
        }
    }

    //******************************NoN Utiliser***********************************************///
    // local procedure RefreshOnAfterGetRecordSymta()
    // var
    //     SalesLine: Record "Sales Line";
    //     TempSalesLine: Record "Sales Line" temporary;
    //     SalesPostPrepayments: Codeunit "Sales-Post Prepayments";
    //     SubstituteProcess: Codeunit "Substitute Process";
    //     OptionValueOutOfRange: Integer;
    //     TempVATAmountLine5: Record "VAT Amount Line" TEMPORARY;
    //     i: Integer;
    // begin
    //     // 4, so that it does calculations for  tab outstanding
    //     i := 4;
    //     TempSalesLine.DeleteAll();
    //     Clear(TempSalesLine);
    //     Clear(SalesPost);
    //     SalesPost.GetSalesLines(Rec, TempSalesLine, i - 1, false);
    //     Clear(SalesPost);
    //     // AD Le 27-09-2016 => REGIE -> Pour g‚rer la quantit‚ ouverte
    //     SalesLine.CalcVATAmountLines(3, Rec, TempSalesLine, TempVATAmountLine5);

    //     BindSubscription(SubstituteProcess);

    //     SalesPost.SumSalesLinesTemp(
    //       Rec, TempSalesLine, i - 1, TotalSalesLine[i], TotalSalesLineLCY[i],
    //       VATAmount[i], VATAmountTextSymta[i], ProfitLCY[i], ProfitPct[i], TotalAdjCostLCY[i], false);

    //     UnbindSubscription(SubstituteProcess);

    //     AdjProfitLCY[i] := TotalSalesLineLCY[i].Amount - TotalAdjCostLCY[i];
    //     if TotalSalesLineLCY[i].Amount <> 0 then
    //         AdjProfitPct[i] := Round(AdjProfitLCY[i] / TotalSalesLineLCY[i].Amount * 100, 0.1);

    //     if Rec."Prices Including VAT" then begin
    //         TotalAmount2[i] := TotalSalesLine[i].Amount;
    //         TotalAmount1[i] := TotalAmount2[i] + VATAmount[i];
    //         TotalSalesLine[i]."Line Amount" := TotalAmount1[i] + TotalSalesLine[i]."Inv. Discount Amount";
    //     end else begin
    //         TotalAmount1[i] := TotalSalesLine[i].Amount;
    //         TotalAmount2[i] := TotalSalesLine[i]."Amount Including VAT";
    //     end;
    //     // MC Le 25-10-2011 => Gestion du total Brut
    //     TotalAmountBrut[i] := TotalSalesLine[i].Amount + TotalSalesLine[i]."Line Discount Amount"
    //     // FIN MC Le 25-10-2011
    // end;

    //******************************NoN Utiliser***********************************************///
    var
        TotalAmountBrut: array[4] of Decimal;
        // VATAmountTextSymta: array[3] of Text[30];
        ESK001Lbl: Label 'Total Restant';
        Text002Lbl: Label 'Amount';
        Text001Lbl: Label 'Total';
        Text006Lbl: Label 'Prepmt. Amount';
        Text008Lbl: Label 'Prepmt. Amt. Deducted';
        Text007Lbl: Label 'Prepmt. Amt. Invoiced';
        Text009Lbl: Label 'Prepmt. Amt. to Deduct';
}

