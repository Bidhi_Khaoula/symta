pageextension 50117 "Payment Methods" extends "Payment Methods" //427
{
    layout
    {
        addlast(Control1)
        {
            field("Commentaires traites"; Rec."Commentaires traites")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Commentaires traites field.';
            }
        }
    }

}