pageextension 51573 "Purchasing Agent Role Center" extends "Purchasing Agent Role Center" //9007
{

    actions
    {
        addafter("Purchase &Return Order")
        {
            action("Page Purch. Receipt Header")
            {
                Caption = 'Purch Receipt Header';
                Image = Receipt;
                // Promoted = true;
                // PromotedCategory = Process;
                RunObject = Page "Warehouse Receipt";
                RunPageMode = Create;
                ApplicationArea = All;
                ToolTip = 'Executes the Purch Receipt Header action.';
            }
        }
        addafter("Purchase &Line Discounts")
        {
            action("&Prix vente")
            {
                Caption = 'Sales &Prices';
                Image = SalesPrices;
                RunObject = Page "Sales Prices";
                ApplicationArea = All;
                ToolTip = 'Executes the Sales &Prices action.';
            }
            action("&Remises ligne vente")
            {
                Caption = 'Sales Line &Discounts';
                Image = SalesLineDisc;
                RunObject = Page "Sales Line Discounts";
                ApplicationArea = All;
                ToolTip = 'Executes the Sales Line &Discounts action.';
            }
            action("Page Fusion")
            {
                Image = Receipt;
                RunObject = Page "Fusion Réception";
                RunPageMode = Edit;
                ApplicationArea = All;
                ToolTip = 'Executes the Page Fusion action.';
            }
            action("Appro SYMTA")
            {
                Image = Purchase;
                // Promoted = true;
                RunObject = Page "Appro. SYMTA v2";
                RunPageMode = Edit;
                ApplicationArea = All;
                ToolTip = 'Executes the Appro SYMTA action.';
            }
        }
        addafter("Navi&gate")
        {
            action("Multi-Consultation")
            {
                Caption = 'Multi-Consultation';
                Image = CalculateConsumption;
                // Promoted = true;
                // PromotedIsBig = true;
                RunObject = Page "Multi -consultation";
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';
            }
        }
    }
}

