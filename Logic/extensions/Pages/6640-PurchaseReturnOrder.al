pageextension 51278 "Purchase Return Order" extends "Purchase Return Order" //6640
{

    layout
    {
        modify("Posting Date")
        {
            Editable = false;
        }
        modify("Assigned User ID")
        {
            Importance = Additional;
        }
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Type field.';
            }
        }
        addafter("Assigned User ID")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
    }
    actions
    {

        addbefore("&Return Order")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin
                    CurrPage.PurchLines.PAGE.MultiConsultation();
                end;
            }
        }
        addafter(RemoveFromJobQueue)
        {
            group(Imprimer)
            {
                Caption = 'Print';
                Image = Print;
                action(Action1100284004)
                {
                    Caption = '&Print';
                    Ellipsis = true;
                    Image = Print;
                    Promoted = true;
                    PromotedCategory = "Report";
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Print action.';

                    trigger OnAction();
                    var
                        LSingleInstance: Codeunit SingleInstance;
                    begin
                        // MCO Le 18-01-2016
                        LSingleInstance.SetPrintPurchEmail(FALSE);
                        // FIN MCO Le 18-01-2016
                        DocPrint.PrintPurchHeader(Rec);
                    end;
                }
                action("Envoyer par mail")
                {
                    Enabled = true;
                    Promoted = true;
                    PromotedCategory = "Report";
                    PromotedIsBig = false;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Envoyer par mail action.';

                    trigger OnAction();
                    var
                        PurchaseHeader: Record "Purchase Header";
                        DocumentSendingProfile: Record "Document Sending Profile";
                        DummyReportSelections: Record "Report Selections";
                    begin
                        //DocPrint.SetPrintPurchEmail(TRUE);
                        //DocPrint.PrintPurchHeader(Rec);

                        // GR le 30/01/2018 (copier/coller commande achat)
                        PurchaseHeader := Rec;
                        CurrPage.SETSELECTIONFILTER(PurchaseHeader);
                        DocumentSendingProfile.SendVendorRecords(
                          DummyReportSelections.Usage::"P.Return".AsInteger(), PurchaseHeader, 'Retour achat', PurchaseHeader."Buy-from Vendor No.", PurchaseHeader."No.",
                          PurchaseHeader.FIELDNO("Buy-from Vendor No."), PurchaseHeader.FIELDNO("No."));
                        // fin GR
                    end;
                }
                action("Envoyer par fax")
                {
                    Promoted = true;
                    PromotedCategory = "Report";
                    ApplicationArea = All;
                    ToolTip = 'Executes the Envoyer par fax action.';

                    trigger OnAction();
                    begin
                        SendFaxMail('FAX')
                    end;
                }
                action(SendCustom)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Send';
                    Ellipsis = true;
                    Image = SendToMultiple;
                    Promoted = true;
                    PromotedCategory = "Report";
                    PromotedIsBig = true;
                    ToolTip = 'Prepare to send the document according to the vendor''s sending profile, such as attached to an email. The Send document to window opens first so you can confirm or select a sending profile.';

                    trigger OnAction();
                    var
                        PurchaseHeader: Record "Purchase Header";
                    begin
                        PurchaseHeader := Rec;
                        CurrPage.SETSELECTIONFILTER(PurchaseHeader);
                        PurchaseHeader.SendRecords();
                    end;
                }
            }
        }
    }

    var
        ESK001Msg: label 'Don''t forget to release your return order';

    trigger OnAfterGetRecord()
    begin
        // LM
        Rec."Posting Date" := WORKDATE();
        //VALIDATE("Posting Date",WORKDATE);

        // CFR le 10/05/2022 => R‚gie : Date compta … la date du jour pour les retours achat
        Rec.MODIFY(FALSE);
        COMMIT();
        // FIN CFR le 10/05/2022
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        // GB Le 18/09/2017 => Reprise des developpements perdu NAv 2009->2015  FR20150608
        IF Rec.Status = Rec.Status::Open THEN
            MESSAGE(ESK001Msg);
        //FIN GB
    end;

    procedure SendFaxMail(_ptype: Code[10]);
    var
        LDoc: Record "Purchase Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;

    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        //LM le 17-05-2013=>permet de modifier les options avant envoi
        DocPrint.PrintPurchHeader(Rec);

        CASE _ptype OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    end;

    var
        DocPrint: Codeunit "Document-Print";

}

