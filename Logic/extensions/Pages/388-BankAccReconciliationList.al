pageextension 50573 "Bank Acc. Reconciliation List" extends "Bank Acc. Reconciliation List" //388
{
    actions
    {
        addafter(PostAndPrint)
        {
            action("Justificatif solde de banque")
            {
                ApplicationArea = All;
                Caption = 'Justificatif solde de banque';
                Image = Balance;
                Promoted = true;
                RunObject = Report "Justificatif de solde bancaire";
                ToolTip = 'Executes the Justificatif solde de banque action.';
            }
        }
    }
}