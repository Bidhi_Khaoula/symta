pageextension 51639 "Purchase Order Archives" extends "Purchase Order Archives" //9347
{
    layout
    {
        addafter("Interaction Exist")
        {
            field("Vendor Order No."; Rec."Vendor Order No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Order No. field.';
            }
        }
        addafter("Order Address Code")
        {
            field("Vendor Shipment No."; Rec."Vendor Shipment No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
            }
        }
        addafter("Pay-to Post Code")
        {
            field(Amount; Rec.Amount)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Amount field.';
            }
        }
        addafter("Shipment Method Code")
        {
            field("Order Date"; Rec."Order Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
    }

}

