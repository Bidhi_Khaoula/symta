pageextension 50006 "Get Purchase Line Disc." extends "Get Purchase Line Disc." //7189
{
    layout
    {
        modify("Line Discount %")
        {
            Editable = false;
        }
    }

}