pageextension 50012 "Sales Return Orders" extends "Sales Return Orders" //6633
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Line Discount %")
        {
            Editable = false;
        }
    }

}