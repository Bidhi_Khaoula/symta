pageextension 50204 "Customer Statistics" extends "Customer Statistics" //151
{
    layout
    {
        addafter("Balance (LCY)")
        {
            group("Chiffres d'affaires")
            {
                field("Shipped Not Inv. SellCus (LCY)"; Rec."Shipped Not Inv. SellCus (LCY)")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Shipped Not Invoiced (LCY) field.';
                }
                field("Ca Client Facturé"; Rec."Ca Client Facturé")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Ca Client Facturé field.';
                }
                field("Ca Ecritures Valeurs"; Rec."Ca Ecritures Valeurs")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Ca Ecritures Valeurs field.';
                }

            }
        }
        addafter("Credit Limit (LCY)")
        {
            field("Montant risque paiement"; PaymentLine.Amount)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Amount field.';
            }
        }
        addafter(GetInvoicedPrepmtAmountLCY)
        {
            field("Gen. Bus. Posting Group"; Rec."Gen. Bus. Posting Group")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Gen. Bus. Posting Group field.';
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        LSetup: Record "Sales & Receivables Setup";
        CostCalcMgt: Codeunit "Cost Calculation Management";
        DateFilterCalc: Codeunit "DateFilter-Calc";
        Ldate_inf: Date;
        ESK001Qst: Label 'Exercice Fiscal ?';
    begin
        CurrentDate := 0D;
        if CurrentDate <> WorkDate() then begin
            CurrentDate := WorkDate();
            DateFilterCalc.CreateAccountingPeriodFilter(CustDateFilter[1], CustDateName[1], CurrentDate, 0);
            IF Not CONFIRM(ESK001Qst) THEN begin

                CustDateFilter[2] := STRSUBSTNO('%1..%2', CALCDATE('<-CY>', WORKDATE()), CALCDATE('<CY>', WORKDATE()));
                CustDateFilter[3] := STRSUBSTNO('%1..%2', CALCDATE('<-CY-1Y>', WORKDATE()), CALCDATE('<CY-1Y>', WORKDATE()));
            end
            else
                exit;
        end;

        Rec.SetRange("Date Filter", 0D, CurrentDate);
        Rec.CALCFIELDS(
                  Balance, "Balance (LCY)", "Balance Due", "Balance Due (LCY)",
                  "Outstanding Orders (LCY)", "Shipped Not Invoiced (LCY)");


        LSetup.GET();
        PaymentLine.SETCURRENTKEY("Account Type", "Account No.", "Due Date", "Payment Class");
        PaymentLine.SETRANGE("Account Type", PaymentLine."Account Type"::Customer);
        PaymentLine.SETRANGE("Account No.", Rec."No.");
        PaymentLine.SETRANGE("Payment Class", 'CLIENT EFF');
        Ldate_inf := CALCDATE(LSetup."Delai de securité (Paiement)", TODAY());
        PaymentLine.SETRANGE("Due Date", Ldate_inf, 29991231D);
        PaymentLine.SETRANGE("Payment in Progress", FALSE);
        PaymentLine.CALCSUMS(Amount);
        // FIN ESK
        for i := 1 to 4 do begin
            Rec.SetFilter("Date Filter", CustDateFilter[i]);
            Rec.CalcFields(
              "Sales (LCY)", "Profit (LCY)", "Inv. Discounts (LCY)", "Inv. Amounts (LCY)", "Pmt. Discounts (LCY)",
              "Pmt. Disc. Tolerance (LCY)", "Pmt. Tolerance (LCY)",
              "Fin. Charge Memo Amounts (LCY)", "Cr. Memo Amounts (LCY)", "Payments (LCY)",
              "Reminder Amounts (LCY)", "Refunds (LCY)", "Other Amounts (LCY)");
            CustSalesLCY[i] := Rec."Sales (LCY)";
            CustProfit[i] := Rec."Profit (LCY)";
            AdjmtCostLCY[i] :=
              CustSalesLCY[i] - CustProfit[i] + CostCalcMgt.CalcCustActualCostLCY(Rec) + CostCalcMgt.NonInvtblCostAmt(Rec);
            AdjCustProfit[i] := CustProfit[i] + AdjmtCostLCY[i];

            if Rec."Sales (LCY)" <> 0 then begin
                ProfitPct[i] := Round(100 * CustProfit[i] / Rec."Sales (LCY)", 0.1);
                AdjProfitPct[i] := Round(100 * AdjCustProfit[i] / Rec."Sales (LCY)", 0.1);
            end else begin
                ProfitPct[i] := 0;
                AdjProfitPct[i] := 0;
            end;

            InvAmountsLCY[i] := Rec."Inv. Amounts (LCY)";
            CustInvDiscAmountLCY[i] := Rec."Inv. Discounts (LCY)";
            CustPaymentDiscLCY[i] := Rec."Pmt. Discounts (LCY)";
            CustPaymentDiscTolLCY[i] := Rec."Pmt. Disc. Tolerance (LCY)";
            CustPaymentTolLCY[i] := Rec."Pmt. Tolerance (LCY)";
            CustReminderChargeAmtLCY[i] := Rec."Reminder Amounts (LCY)";
            CustFinChargeAmtLCY[i] := Rec."Fin. Charge Memo Amounts (LCY)";
            CustCrMemoAmountsLCY[i] := Rec."Cr. Memo Amounts (LCY)";
            CustPaymentsLCY[i] := Rec."Payments (LCY)";
            CustRefundsLCY[i] := Rec."Refunds (LCY)";
            CustOtherAmountsLCY[i] := Rec."Other Amounts (LCY)";
        end;
        Rec.SetRange("Date Filter", 0D, CurrentDate);

    end;

    var
        PaymentLine: Record "Payment Line";
}