pageextension 50535 "Where-Used List" extends "Where-Used List" //37
{
    layout
    {
        addafter("Parent Item No.")
        {
            field("Parent Ref. Active"; Rec."Parent Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active nomenclature field.';
            }
        }
    }
}

