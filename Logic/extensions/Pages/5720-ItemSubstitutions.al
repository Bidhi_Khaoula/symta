pageextension 50996 "Item Substitutions" extends "Item Substitutions" //5720
{

    layout
    {
        addafter("Substitute No.")
        {
            field("Substitute Ref. Active"; Rec."Substitute Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active substitut field.';
            }
        }
    }
}

