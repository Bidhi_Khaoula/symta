pageextension 50100 "General Ledger Setup" extends "General Ledger Setup" //118
{

    layout
    {

        addafter("Print VAT specification in LCY")
        {
            field("Unit-Amount Rounding Precision"; rec."Unit-Amount Rounding Precision")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit-Amount Rounding Precision field.';
            }
        }
    }


}

