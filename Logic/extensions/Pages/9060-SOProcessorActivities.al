pageextension 51595 "SO Processor Activities" extends "SO Processor Activities" //9060
{

    layout
    {
        modify(ReadyToShip)
        {
            Visible = false;

        }
        addafter(ReadyToShip)
        {
            field(ReadyToShipSymta; ReadyToShipSymta)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Ready To Ship';
                DrillDownPageID = "Sales Order List";
                ToolTip = 'Specifies the number of sales documents that are ready to ship.';
                StyleExpr = ReadyToShipStyleSymta;

                trigger OnDrillDown()
                begin
                    ShowOrdersSymta(Rec.FieldNo("Ready to Ship"));
                end;
            }
        }
        modify("Sales Orders - Open")
        {
            Visible = false;
        }
        addafter("Sales Orders - Open")
        {
            field("Sales Orders - Open Symta"; "Sales Orders - Open Symta")
            {
                ApplicationArea = Basic, Suite;
                DrillDownPageID = "Sales Order List";
                ToolTip = 'Specifies the number of sales orders that are not fully posted.';
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        ReadyToShipSymta := CountReadyToDeliverSalesOrders();
    end;

    trigger OnPageBackgroundTaskCompleted(TaskId: Integer; Results: Dictionary of [Text, Text])
    var
    begin
        UIHelperTriggers.GetCueStyle(Database::"Sales Cue", Rec.FieldNo("Ready to Ship"), ReadyToShipSymta, ReadyToShipStyleSymta);
    end;

    LOCAL PROCEDURE CountReadyToDeliverSalesOrders(): Integer
    var
        SalesHeader: Record "Sales Header";
    BEGIN
        SalesHeader.SETRANGE("Document Type", SalesHeader."Document Type"::Order);
        SalesHeader.SETRANGE("Completely Shipped", FALSE);
        SalesHeader.SETRANGE(Status, SalesHeader.Status::Preparate);
        SalesHeader.SETFILTER("Shipment Date", GETFILTER("Date Filter2"));
        exit(SalesHeader.Count);
    end;

    procedure ShowOrdersSymta(FieldNumber: Integer)
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.SETRANGE("Document Type", SalesHeader."Document Type"::Order);
        SalesHeader.SETRANGE("Completely Shipped", FALSE);
        SalesHeader.SETRANGE(Status, SalesHeader.Status::Preparate);
        SalesHeader.SETFILTER("Shipment Date", GETFILTER("Date Filter2"));
        PAGE.Run(PAGE::"Sales Order List", SalesHeader);
    end;

    trigger OnOpenPage()
    begin
        Rec.SETRANGE("Date Filter", 20140101D, WORKDATE() - 1);
    end;

    var
        UIHelperTriggers: Codeunit "UI Helper Triggers";
        ReadyToShipSymta: Integer;
        ReadyToShipStyleSymta: Text;
}

