pageextension 50508 "Assembly BOM" extends "Assembly BOM" //36
{
    layout
    {

        addafter("No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        addafter("Variant Code")
        {
            field("Parent Ref. Active"; Rec."Parent Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active nomenclature field.';
            }
        }
        addafter("Resource Usage Type")
        {
            field("Emplacement par Défaut"; Rec."Emplacement par Défaut")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Emplacement par Défaut field.';
            }
            field(Stock; Item.Inventory)
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Inventory field.';
            }
        }
    }
    actions
    {
    }
    trigger OnAfterGetRecord()
    begin
        CLEAR(Item);
        IF Rec.Type = Rec.Type::Item THEN
            IF Item.GET(Rec."No.") THEN Item.CALCFIELDS(Inventory);
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    BEGIN
        // MCO Le 18-01-2016 => SYMTA -> Demande de Nathalie
        Rec.Type := Rec.Type::Item;
        // FIN MCO Le 18-01-2016
    END;

    var
        Item: Record Item;

}

