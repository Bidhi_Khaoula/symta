pageextension 50650 "Sales Invoice" extends "Sales Invoice" //43
{
    layout
    {
        modify("Sell-to Customer No.")
        {
            Visible = false;
        }

        addafter("No.")
        {
            field("Sell-to Customer No.2"; Rec."Sell-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
            }
        }
        addafter("Assigned User ID")
        {
            field("Packing List No."; Rec."Packing List No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Packing List No. field.';
            }
        }

    }
    actions
    {
        addbefore(Approval)
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    FrmQuid.InitClient(Rec."Sell-to Customer No.");
                    FrmQuid.RUNMODAL();
                end;
            }
        }
    }


}

