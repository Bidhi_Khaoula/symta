pageextension 51384 "Posted Whse. Receipt Subform" extends "Posted Whse. Receipt Subform" //7331
{
    layout
    {
        addafter("Item No.")
        {
            field("Ref. Active"; rec_item."No. 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. 2 field.';
            }
        }
        addafter("Unit of Measure Code")
        {
            field("Vendor Shipment No."; Rec."Vendor Shipment No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
            }
            field("Prix Brut Origine"; Rec."Prix Brut Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix Brut Origine field.';
            }
            field("Remise 1 Origine"; Rec."Remise 1 Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 1 Origine field.';
            }
            field("Net 1 Origine"; Rec."Net 1 Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Net 1 Origine field.';
            }
            field("Total Net 1 Origine"; Rec."Total Net 1 Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total Net 1 Origine field.';
            }
            field("Remise 2 Origine"; Rec."Remise 2 Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 2 Origine field.';
            }
            field("Net 2 Origine"; Rec."Net 2 Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Net 2 Origine field.';
            }
            field("Total Origine"; Rec."Total Origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total Origine field.';
            }
            field("Référence fournisseur"; Rec."Référence fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence fournisseur field.';
            }
            field("Receipt Adjustment"; Rec."Receipt Adjustment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Receipt Adjustment field.';
            }
            field("Receipt Request"; Rec."Receipt Request")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Receipt Request field.';
            }
        }
        addafter(Description)
        {
            field(Désignation; Rec.GetInfoLigneTexte('DESC'))
            {
                Editable = true;
                Caption = 'Désignation  (ligne commande)';
                ToolTip = 'Specifies the description of the item in the line.';
                ApplicationArea = All;
            }
        }
        moveafter("Variant Code"; "Description 2")
    }

    var
        rec_item: Record Item;

    trigger OnAfterGetRecord()
    begin
        IF rec_item.GET(Rec."Item No.") THEN;
    end;
}

