pageextension 51324 "Warehouse Shipment" extends "Warehouse Shipment" //7335
{

    layout
    {
        addafter("No.")
        {
            field(GetDestinationName; Rec.GetDestinationName())
            {
                Caption = 'Donneur d''ordre';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Donneur d''ordre field.';
            }
        }
        addafter("Sorting Method")
        {
            field("Packing List No."; Rec."Packing List No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° liste de colisage field.';

                trigger OnLookup(Var Text: Text): Boolean;
                var
                    lPackingListHeader: Record "Packing List Header";
                begin
                    //IF  CONFIRM('On efface 1 ?') THEN
                    IF lPackingListHeader.GET(Rec."Packing List No.") THEN
                        AffichePL(lPackingListHeader);
                end;
            }
        }
        addafter(Status)
        {
            field(Blocked; Rec.Blocked)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Bloqué field.';
            }
        }
    }
    actions
    {
        modify("Autofill Qty. to Ship")
        {
            trigger OnBeforeAction()
            begin
                ERROR('Ne pas Utiliser cette fonction !'); // AD Le 23-03-2016 => Car des fois les quantit‚s repassent … la qte exp‚di‚e,
            end;
        }
        addafter("&Shipment")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin

                    CurrPage.WhseShptLines.PAGE.MultiConsultation();
                end;
            }
        }
        addafter("Create Pick")
        {
            separator(Separator1100284003)
            {
            }
            action("Packing List")
            {
                Caption = 'Packing List';
                Image = CreateWarehousePick;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Packing List action.';

                trigger OnAction();
                var
                    lPackingListHeader: Record "Packing List Header";
                begin
                    IF gPackingListMgmt.getPackingListForWhseShipment(lPackingListHeader, Rec."No.") THEN
                        AffichePL(lPackingListHeader);
                end;
            }
            action("Supprimer Packing List")
            {
                Caption = 'Supprimer Packing List';
                Image = DeleteQtyToHandle;
                ApplicationArea = All;
                ToolTip = 'Executes the Supprimer Packing List action.';

                trigger OnAction();
                var
                    lPackingListMgmt: Codeunit "Packing List Mgmt";
                begin
                    lPackingListMgmt.delWhseShipmentFromPackingList(Rec."No.")
                end;
            }
        }
    }

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";

    local procedure AffichePL(pPackingListHeader: Record "Packing List Header");
    var
        lPackingListHeaderPage: Page "Packing List Header";
    begin
        CLEAR(lPackingListHeaderPage);
        pPackingListHeader.FILTERGROUP(2);
        pPackingListHeader.SETRANGE("No.", pPackingListHeader."No.");
        pPackingListHeader.SETRECFILTER();
        pPackingListHeader.FILTERGROUP(0);
        lPackingListHeaderPage.SETRECORD(pPackingListHeader);
        lPackingListHeaderPage.RUN();
    end;

}

