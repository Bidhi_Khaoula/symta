pageextension 51692 "Purchase Invoices" extends "Purchase Invoices" //9308
{
    layout
    {
        modify(Amount)
        {
            Caption = 'Amount';
        }

        moveafter("Job Queue Status"; "Vendor Invoice No.")
        addafter("Vendor Invoice No.")
        {
            field("Montant HT facture"; Rec."Montant HT facture")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant TTC field.';
            }
            field("Amount Including VAT"; Rec."Amount Including VAT")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Amount Including VAT field.';
            }
        }
        addafter(Amount)
        {
            field("VAT Amount"; gVATAmount)
            {
                Caption = 'Montant TVA';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant TVA field.';
            }
        }
    }

    var
        gVATAmount: Decimal;

    trigger OnAfterGetRecord()
    begin
        Rec.CALCFIELDS("Amount Including VAT", Amount);
        gVATAmount := Rec."Amount Including VAT" - Rec.Amount;
    end;

    trigger OnAfterGetCurrRecord()
    begin
        Rec.CALCFIELDS("Amount Including VAT");
    end;
}

