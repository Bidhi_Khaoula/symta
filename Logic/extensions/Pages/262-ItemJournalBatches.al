pageextension 50172 "Item Journal Batches" extends "Item Journal Batches" //262
{
    layout
    {
        addafter("Reason Code")
        {
            field("Type Feuille Reclassement WIIO"; Rec."Type Feuille Reclassement WIIO")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Type Feuille Reclassement WIIO field.';
            }
        }
    }
    actions
    {
        modify("P&ost")
        {
            Visible = false;
        }
        addafter("P&ost")
        {
            action("P&ost SYM")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'P&ost';
                Image = PostOrder;
                RunObject = Codeunit "Item Jnl.-B.Post";
                ToolTip = 'Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.';
            }
        }
    }
}