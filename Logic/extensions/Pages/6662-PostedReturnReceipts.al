pageextension 51333 "Posted Return Receipts" extends "Posted Return Receipts" //6662
{
    layout
    {
        modify("Posting Date")
        {
            Visible = true;
        }
        modify("Currency Code")
        {
            Visible = false;
        }
        modify("No. Printed")
        {
            Visible = false;
        }
        movebefore("No."; "Posting Date")

        addafter("No.")
        {
            field("Payment Terms Code"; Rec."Payment Terms Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Payment Terms Code field.';
            }
        }
        addafter("Shipment Date")
        {
            field("Return Order No."; Rec."Return Order No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Return Order No. field.';
            }
            field(GetAmount; Rec.GetAmount())
            {
                Caption = 'Montant';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant field.';
            }
            field("Sell-to City"; Rec."Sell-to City")
            {
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sell-to City field.';
            }
            field("Type retour"; Rec."Type retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type retour field.';
            }
            field("En attente de facturation"; Rec."En attente de facturation")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the En attente de facturation field.';
            }
            field("User En Attente"; Rec."User En Attente")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Utilisateur mise en attente field.';
            }
        }
        modify("Sell-to Post Code")
        {
            Visible = true;
        }
        modify("Bill-to Customer No.")
        {
            Visible = true;
        }
        moveafter(GetAmount; "Sell-to Post Code")
        moveafter("Sell-to City"; "Bill-to Customer No.")
        movebefore(Control1900383207; Control1905767507)
    }
    actions
    {
        addafter("&Print")
        {
            action("&E-mail")
            {
                Caption = '&Email';
                Image = Email;
                ApplicationArea = All;
                ToolTip = 'Executes the &Email action.';

                trigger OnAction();
                var
                    ReturnReceiptHeader: Record "Return Receipt Header";
                begin
                    ReturnReceiptHeader := Rec;
                    CurrPage.SETSELECTIONFILTER(ReturnReceiptHeader);
                    ReturnReceiptHeader.EmailRecords(TRUE);
                end;
            }
            action("Envoyer Par Fax")
            {
                Caption = 'Envoyer Par Fax';
                Image = PrintCheck;
                Promoted = true;
                PromotedCategory = "Report";
                ApplicationArea = All;
                ToolTip = 'Executes the Envoyer Par Fax action.';

                trigger OnAction();
                begin
                    sendfaxmail('FAX')
                end;
            }
        }
        addafter("&Navigate")
        {
            action(Retour)
            {
                Image = ReturnOrder;
                Promoted = true;
                PromotedCategory = "Report";
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Retour action.';

                trigger OnAction();
                begin
                    Rec.ShowOrder();
                end;
            }

        }
    }
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("No.");
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;

    end;

    procedure sendfaxmail(_pType: Code[10]);
    var
        LDoc: Record "Return Receipt Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    end;
}

