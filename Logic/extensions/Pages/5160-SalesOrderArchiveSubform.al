pageextension 50782 "Sales Order Archive Subform" extends "Sales Order Archive Subform" //5160
{

    layout
    {
        addafter(Type)
        {
            field("Line No."; Rec."Line No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
        }
        addafter("No.")
        {
            field("Référence saisie"; Rec."Référence saisie")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence saisie field.';
            }
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter("Line Discount %")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
            field("Net Unit Price"; Rec."Net Unit Price")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix unitaire net field.';
            }
        }
    }

}

