pageextension 51297 "Sales Prices" extends "Sales Prices" //7002
{

    layout
    {
        modify("Sales Type")
        {
            Editable = true;
        }
        modify("Sales Code")
        {
            Editable = true;
        }
        modify("Item No.")
        {
            Editable = true;
        }
        modify("Currency Code")
        {
            Editable = true;
        }
        modify("Starting Date")
        {
            Editable = true;
        }
        addafter(CurrencyCodeFilterCtrl)
        {
            field(ManFilter; ManufacturerCodeFilter)
            {
                Caption = 'Filtre marque';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Filtre marque field.';

                trigger OnLookup(Var Text: Text): Boolean
                var
                    ManufacturerList: Page Manufacturers;
                begin
                    ManufacturerList.LOOKUPMODE := TRUE;
                    IF ManufacturerList.RUNMODAL() = ACTION::LookupOK THEN
                        Text := ManufacturerList.GetSelectionFilter()
                    ELSE
                        EXIT(FALSE);

                    EXIT(TRUE);
                end;

                trigger OnValidate();
                begin
                    ManufacturerFilterOnAfterValidate();
                end;
            }
            field(FalFukter; ItemCategoryCodeFilter)
            {
                Caption = 'Filtre famille';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Filtre famille field.';

                trigger OnLookup(var Text: Text): Boolean;
                var
                    FamilleList: Page "Item Categories";
                begin
                    FamilleList.LOOKUPMODE := TRUE;
                    IF FamilleList.RUNMODAL() = ACTION::LookupOK THEN
                        Text := FamilleList.GetSelectionFilter()
                    ELSE
                        EXIT(FALSE);

                    EXIT(TRUE);
                end;

                trigger OnValidate();
                begin
                    ItemCatFilterOnAfterValidate()
                end;
            }
            field(SsFamFilter; ProductGroupCodeFilter)
            {
                Caption = 'Filtre sous famille';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Filtre sous famille field.';
                //todo
                // trigger OnLookup(var Text: Text): Boolean;
                // var
                //     SsFamilleList: Page "Product Groups";
                // begin
                //     SsFamilleList.LOOKUPMODE := TRUE;
                //     IF SsFamilleList.RUNMODAL = ACTION::LookupOK THEN
                //         Text := SsFamilleList.GetSelectionFilter
                //     ELSE
                //         EXIT(FALSE);

                //     EXIT(TRUE);
                // end;

                trigger OnValidate();
                begin
                    ProductFilterOnAfterValidate()
                end;
            }
            field("Actif uniquement"; ActifUniquement)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the ActifUniquement field.';

                trigger OnValidate();
                begin
                    FctActifUniquement();
                end;
            }
        }
        addafter("Sales Code")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        addafter("Item No.")
        {
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
        addafter("VAT Bus. Posting Gr. (Price)")
        {
            field("Prix catalogue"; Rec."Prix catalogue")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix catalogue field.';
            }
            field(Coefficient; Rec.Coefficient)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Coefficient field.';
            }
            field(Utilisateur; Rec."user maj prix")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the user maj prix field.';
            }
            field("Manufacturer Code"; Rec."Manufacturer Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Manufacturer Code field.';
            }
            field("Item Category Code"; Rec."Item Category Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Item Category Code field.';
            }
            field("Product Group Code"; Rec."Product Group Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Product Group Code field.';
            }
        }
    }

    var
        ManufacturerCodeFilter: Text[250];
        ItemCategoryCodeFilter: Text[250];
        ProductGroupCodeFilter: Text[250];

    var
        ActifUniquement: Boolean;




    local procedure ManufacturerFilterOnAfterValidate();
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;

    local procedure ItemCatFilterOnAfterValidate();
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;

    local procedure ProductFilterOnAfterValidate();
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;


    procedure FctActifUniquement();
    begin
        // DZ Le 11-06-2012 => Pour avoir uniquement les données actives
        IF ActifUniquement THEN BEGIN
            Rec.SETRANGE("Starting Date", 0D, WORKDATE());
            Rec.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
        END
        ELSE BEGIN
            Rec.SETRANGE("Starting Date");
            Rec.SETRANGE("Ending Date");
        END;
        CurrPage.UPDATE(FALSE);
    end;
}

