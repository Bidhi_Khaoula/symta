pageextension 50587 "Item Journal" extends "Item Journal" //40
{
    layout
    {

        addafter("External Document No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the référence active field.';
            }
        }
    }
    actions
    {


        addafter("Bin Contents")
        {
            action("Ouvrir multiconsultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Ouvrir multiconsultation action.';

                trigger OnAction();
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
    }


}

