pageextension 50614 "Sales Order" extends "Sales Order" //42
{
    layout
    {
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                Importance = Promoted;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        modify("Sell-to Customer No.")
        {
            Visible = false;
        }
        addafter("Sell-to Customer No.")
        {
            field("Sell-to Customer No SYM"; Rec."Sell-to Customer No.")
            {
                ApplicationArea = All;
                Caption = 'Customer No.';
                Importance = Additional;
                NotBlank = true;
                StyleExpr = gStyleExp_Export;
                ToolTip = 'Rouge = Client EXPORT';

                trigger OnValidate()
                begin
                    // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
                    SetStyleExp_Export();
                    // FIN CFR le 05/04/2023
                end;
            }
        }
        modify("Sell-to Customer Name")
        {
            Caption = 'Customer';
            StyleExpr = gStyleExp_Export;
            trigger OnAfterValidate()
            begin
                // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
                SetStyleExp_Export();
                // FIN CFR le 05/04/2023

                CurrPage.Update();
            end;
        }
        modify("Quote No.")
        {
            Visible = true;
        }
        modify("Sell-to Address")
        {
            Editable = EditableEsk;
        }
        modify("Sell-to Address 2")
        {
            Editable = EditableEsk;
        }
        modify("Sell-to Post Code")
        {
            Editable = EditableEsk;
        }
        modify("Sell-to City")
        {
            Editable = EditableEsk;
        }
        modify("Sell-to Contact")
        {
            Editable = EditableEsk;
        }
        addafter("Sell-to Contact")
        {
            field(Centrale; Rec.GetCentraleName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCentraleName() field.';
            }
            field("Code Enseigne 1"; Rec."Code Enseigne 1")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
            }
            field("Code Enseigne 2"; Rec."Code Enseigne 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
            }
        }
        modify("No. of Archived Versions")
        {
            Visible = VisibleEsk;
        }
        addafter("No. of Archived Versions")
        {
            field("Source Document Type"; Rec."Source Document Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
            field("Statut Livraison"; status_livraison)
            {
                ApplicationArea = All;
                Editable = FALSE;
                QuickEntry = FALSE;
                ToolTip = 'Specifies the value of the status_livraison field.';
            }
        }
        moveafter("Source Document Type"; "External Document No.")
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
        }
        moveafter("Statut Livraison"; Status, "Your Reference")
        addafter("Document Date")
        {
            field("Modify Date"; Rec."Modify Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date modif. Cde field.';
            }
        }
        modify("Posting Date")
        {
            Importance = Additional;
        }
        modify("Due Date")
        {
            Editable = visibleesk;
        }
        modify("Promised Delivery Date")
        {
            Visible = false;
        }
        addafter("Salesperson Code")
        {
            field("Service Zone Code"; Rec."Service Zone Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Service Zone Code field.';
            }
        }
        addafter("Campaign No.")
        {
            field("Customer Disc. Group 2"; Rec."Customer Disc. Group")
            {
                ApplicationArea = All;
                Caption = 'Customer Disc. Group';
                Importance = Additional;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
            field("Payment Terms Code 3"; Rec."Payment Terms Code")
            {
                ApplicationArea = All;
                Caption = 'Payment Terms Code';
                Importance = Additional;
                ToolTip = 'Specifies the value of the Payment Terms Code field.';
            }
        }
        modify("Opportunity No.")
        {
            Visible = VisibleEsk;
        }
        modify("Responsibility Center")
        {
            Importance = Additional;
            Visible = VisibleEsk;
        }
        addafter("Responsibility Center")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Modify User ID"; Rec."Modify User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur modif. Cde field.';
            }
        }
        addafter("Job Queue Status")
        {
            field("Shipment Method Code2"; Rec."Shipment Method Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Shipping Agent Code2"; Rec."Shipping Agent Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipping Agent Code field.';
            }
            field("Mode d'expédition2"; Rec."Mode d'expédition")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
            field("Incoterm City"; Rec."Incoterm City")
            {
                ApplicationArea = All;
                Importance = Additional;
                Editable = gEditable_Export;
                ToolTip = 'Specifies the value of the Ville Incoterm ICC 2020 field.';
            }
            field("Incoterm Code"; Rec."Incoterm Code")
            {
                ApplicationArea = All;
                Importance = Additional;
                Editable = gEditable_Export;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Transporteur imperatif"; Rec."Transporteur imperatif")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur imperatif field.';
            }
            field("Saturday Delivery2"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field("Abandon remainder2"; Rec."Abandon remainder")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Abandon reliquat field.';
            }
            field("Web comment"; Rec."Web comment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Web comment field.';
            }
            field("Nb Web comment"; Rec."Nb Web comment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nb Commentaire Web field.';
            }
        }
        modify("Currency Code")
        {
            Editable = EditableEsk;
        }
        modify("Prices Including VAT")
        {
            Editable = EditableEsk;
        }
        modify("VAT Bus. Posting Group")
        {
            Editable = EditableEsk;
        }
        addafter("Payment Method Code")
        {
            field("Bill-to Customer No."; Rec."Bill-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
            }
            field(BillToName1; Rec."Bill-to Name")
            {
                ApplicationArea = All;
                Caption = 'Name';
                ToolTip = 'Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.';
                Importance = Promoted;
                trigger OnValidate()
                BEGIN
                    IF Rec.GETFILTER("Bill-to Customer No.") = xRec."Bill-to Customer No." THEN
                        IF Rec."Bill-to Customer No." <> xRec."Bill-to Customer No." THEN
                            Rec.SETRANGE("Bill-to Customer No.");

                    CurrPage.UPDATE();
                END;
            }
        }
        modify("Shortcut Dimension 1 Code")
        {
            Editable = EditableESK;
        }
        modify("Shortcut Dimension 2 Code")
        {
            Editable = EditableESK;
        }
        modify("Payment Discount %")
        {
            Visible = VisibleEsk;
        }
        modify("Pmt. Discount Date")
        {
            Visible = VisibleEsk;
        }
        addafter("Direct Debit Mandate ID")
        {
            field("Echéances fractionnées"; Rec."Echéances fractionnées")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Echéances fractionnées field.';
            }
            field("Payment Terms Code 2"; Rec."Payment Terms Code 2")
            {
                ApplicationArea = All;
                Editable = VisibleEchFractionBln;
                ToolTip = 'Specifies the value of the Payment Terms Code 2 field.';
            }
            field("Due Date 2"; Rec."Due Date 2")
            {
                ApplicationArea = All;
                Visible = visibleEsk;
                Editable = VisibleEchFractionBln;
                ToolTip = 'Specifies the value of the date d''échéance 2 field.';
            }
            field("Taux Premiere Fraction"; Rec."Taux Premiere Fraction")
            {
                ApplicationArea = All;
                Editable = VisibleEchFractionBln;
                ToolTip = 'Specifies the value of the Taux Premiere Fraction field.';
            }
            field("Invoice Type"; Rec."Invoice Type")
            {
                ApplicationArea = All;
                Editable = EditableEsk;
                ToolTip = 'Specifies the value of the Type Facturation field.';
            }
            field("Exclure RFA"; Rec."Exclure RFA")
            {
                ApplicationArea = All;
                Editable = EditableEsk;
                ToolTip = 'Specifies the value of the Exclure RFA field.';
            }
            field("Multi echéance"; Rec."Multi echéance")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multi echéance field.';
            }
            field("Customer Price Group"; Rec."Customer Price Group")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Price Group field.';
            }
            field("Customer Disc. Group"; Rec."Customer Disc. Group")
            {
                ApplicationArea = All;
                Importance = Promoted;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
            field("Campaign No. 2"; Rec."Campaign No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Campaign No. field.';
            }
            group("Centrale SYM")
            {
                Caption = 'Centrale';
                field("Avoid Active Central"; Rec."Avoid Active Central")
                {
                    ApplicationArea = All;
                    Caption = 'Avoid Active Central';
                    Importance = Additional;
                    ToolTip = 'Specifies the value of the Avoid Active Central field.';
                    trigger OnValidate()
                    BEGIN
                        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
                        gCentralEditable := NOT Rec."Avoid Active Central";
                    END;
                }
                field("Centrale Active"; Rec."Centrale Active")
                {
                    ApplicationArea = All;
                    Importance = Promoted;
                    Editable = gCentralEditable;
                    ToolTip = 'Specifies the value of the Centrale Active field.';
                }
            }
        }
        modify("Ship-to City")
        {
            Importance = Promoted;
        }
        modify("Ship-to Contact")
        {
            Editable = ShipToOptions = ShipToOptions::"Custom Address";
        }
        modify("Shipment Method Code")
        {
            Importance = Promoted;
            Editable = EditableEsk;
        }
        modify("Shipping Agent Code")
        {
            Importance = Promoted;
            Caption = 'Agent';
        }
        modify("Shipping Agent Service Code")
        {
            Caption = 'Agent Service';
            Editable = EditableEsk;
        }
        moveafter(Control91; "Shipping Time", "Late Order Shipping", "Shipment Date", "Shipping Advice", "Combine Shipments")
        addafter(Control91)
        {
            field("Abandon remainder"; Rec."Abandon remainder")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Abandon reliquat field.';
            }
            field("Chiffrage BL"; Rec."Chiffrage BL")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Chiffrage BL field.';
            }
            field("Saturday Delivery"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field("Instructions of Delivery"; Rec."Instructions of Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Instructions de livraison field.';
            }
            field("Insurance of Delivery"; Rec."Insurance of Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Assurance de livraison field.';
            }
            field("Cash on Delivery"; Rec."Cash on Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Contre remboursement field.';
            }
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = All;
                Importance = Promoted;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
            field("N° affrètement"; Rec."N° affrètement")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° affrètement field.';
            }
            field("Mode d'expédition3"; Rec."Mode d'expédition")
            {
                ApplicationArea = All;
                Caption = 'Mode d''expdition';
                ToolTip = 'Specifies the value of the Mode d''expdition field.';
            }
            field("Transporteur imperatif2"; Rec."Transporteur imperatif")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur imperatif field.';
            }
            field("Saturday Delivery5"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field(ShptMethodCode0; Rec."Shipment Method Code")
            {
                ApplicationArea = All;
                Caption = 'Code';
                ToolTip = 'Specifies how items on the sales document are shipped to the customer.';
                Importance = Promoted;
                Editable = EditableEsk;
            }
            field(ShippingAgentCode0; Rec."Shipping Agent Code")
            {
                ApplicationArea = All;
                Caption = 'Agent';
                ToolTip = 'Specifies which shipping agent is used to transport the items on the sales document to the customer.';
                Importance = Promoted;
            }
            field(ForceMandatoryShippingAgent; Rec."Force Mandatory Shipping Agent")
            {
                ApplicationArea = All;
                Caption = 'Forcer transporteur obligatoire';
                ToolTip = 'Specifies the value of the Forcer transporteur obligatoire field.';
            }
            field(ShiptTOCity0; Rec."Ship-to City")
            {
                ApplicationArea = All;
                Caption = 'City';
                ToolTip = 'Specifies the city that products on the sales document will be shipped to.';
                Importance = Promoted;
                Editable = ShipToOptions = ShipToOptions::"Custom Address";
            }
        }
        modify("Bill-to Address")
        {
            Editable = EditableEsk;
        }
        modify("Bill-to Address 2")
        {
            Editable = EditableEsk;
        }
        modify("Bill-to Post Code")
        {
            Editable = EditableEsk;
        }
        modify("Bill-to City")
        {
            Editable = EditableEsk;
        }
        modify("Bill-to Contact No.")
        {
            Editable = EditableEsk;
        }
        modify("Bill-to Contact")
        {
            Editable = EditableEsk;
        }
        modify("Location Code")
        {
            Editable = EditableEsk;
        }
        modify("Outbound Whse. Handling Time")
        {
            Editable = EditableEsk;
        }
        modify("EU 3-Party Trade")
        {
            Editable = EditableEsk;
        }
        modify("Transaction Specification")
        {
            Editable = EditableEsk;
        }
        modify("Transport Method")
        {
            Editable = EditableEsk;
        }
        modify("Exit Point")
        {
            Editable = EditableEsk;
        }
        modify("Area")
        {
            Editable = EditableEsk;
        }
        modify(Control1900201301)
        {
            Visible = false;
        }
        addfirst(factboxes)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                ApplicationArea = All;
                Caption = 'Devis Web';
            }
        }
        addafter(WorkflowStatus)
        {
            part("Item Web Info"; "Item Web Info")
            {
                ApplicationArea = All;
                SubPageLink = "No." = FIELD("No.");
                Provider = SalesLines;
            }
        }
    }
    actions
    {
        addfirst(navigation)
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin
                    CurrPage.SalesLines.PAGE.MultiConsultation();
                end;
            }
        }
        modify("Co&mments")
        {
            Promoted = true;
        }
        modify("S&hipments")
        {
            Promoted = true;
        }
        modify("Warehouse Shipment Lines")
        {
            Promoted = true;
        }
        modify(Release)
        {
            Promoted = true;
            PromotedIsBig = true;
        }
        modify(Reopen)
        {
            Promoted = true;
            PromotedIsBig = false;
        }
        addafter(Reopen)
        {
            action("A Préparer")
            {
                ApplicationArea = All;
                Caption = 'A Préparer';
                Image = UpdateShipment;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = false;
                ToolTip = 'Executes the A Préparer action.';

                trigger OnAction();
                begin
                    // AD Le 29-10-2015
                    APreparer();
                end;
            }
            action(BloquerAct)
            {
                ApplicationArea = All;
                Caption = 'Bloquer';
                Image = InactivityDescription;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = false;
                ToolTip = 'Executes the Bloquer action.';

                trigger OnAction();
                begin
                    // AD Le 02-11-2015
                    Bloquer();
                end;
            }
        }
        addafter("Send IC Sales Order")
        {
            action("Créer Litige")
            {
                ApplicationArea = All;
                Caption = 'Créer Litige';
                Image = Error;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ToolTip = 'Executes the Créer Litige action.';
                trigger OnAction();
                begin
                    Rec.CreerIncidentTransport();
                end;
            }
            action("Import Excel")
            {
                ApplicationArea = All;
                Caption = 'Import Excel';
                Image = ImportExcel;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ToolTip = 'Executes the Import Excel action.';
                trigger OnAction();
                begin
                    ImportExcel();
                end;
            }
        }
        modify("Create Inventor&y Put-away/Pick")
        {
            Promoted = false;
        }
        modify("Create &Warehouse Shipment")
        {
            Visible = false;
        }
        addafter("Create &Warehouse Shipment")
        {
            action("Créer &expédition [Direct]")
            {
                ApplicationArea = All;
                Caption = 'Créer &expédition [Direct]';
                ShortCutKey = F11;
                Promoted = true;
                PromotedIsBig = true;
                Image = NewShipment;
                PromotedCategory = Process;
                ToolTip = 'Executes the Créer &expédition [Direct] action.';
                trigger OnAction()
                BEGIN
                    GenereBP(TRUE);
                    //todo SendToTherefore(); // GR le 25-07-2017
                END;
            }
            action("Créer &expédition [Choix IMP]")
            {
                ApplicationArea = All;
                ShortCutKey = 'Shift+F11';
                Promoted = true;
                PromotedIsBig = true;
                Image = NewShipment;
                PromotedCategory = Process;
                ToolTip = 'Executes the Créer &expédition [Choix IMP] action.';
                trigger OnAction()
                BEGIN
                    GenereBP(FALSE);
                    //todo SendToTherefore(); // GR le 25-07-2017
                END;
            }
            action("PICS & PL")
            {
                ApplicationArea = All;
                Caption = 'PICS & PL';
                ToolTip = 'Expéditions entrepôt et listes de colisage';
                RunObject = Page "WSH / PL";
                RunPageView = SORTING("No.")
                                  ORDER(Ascending);
                RunPageLink = "Source Document" = CONST("Sales Order"),
                                  "Source No." = FIELD("No.");
                Promoted = true;
                PromotedIsBig = true;
                Image = CreateWarehousePick;
                PromotedCategory = Process;
            }
        }
        modify(Post)
        {
            Promoted = false;
        }
        modify("Print Confirmation")
        {
            Visible = NOT IsOfficeHost;
        }
        addafter("Print Confirmation")
        {
            action("Envoyer Par Fax")
            {
                ApplicationArea = All;
                Caption = 'Envoyer Par Fax';
                Image = PrintCheck;
                Promoted = true;
                PromotedCategory = "Report";
                ToolTip = 'Executes the Envoyer Par Fax action.';
                trigger OnAction();
                begin
                    SendFaxMail('FAX')
                end;
            }
            action(ReliquatAchat)
            {
                ApplicationArea = All;
                Caption = 'Reliquats Achats';
                Promoted = true;
                PromotedCategory = "Report";
                PromotedIsBig = true;
                ToolTip = 'Executes the Reliquats Achats action.';
                trigger OnAction();
                begin
                    Rec.MailReliquat();
                end;
            }
            group(Divers)
            {
                Caption = 'Divers';
                action("Cloturer les lignes")
                {
                    ApplicationArea = All;
                    Caption = 'Cloturer les lignes';
                    Image = Close;
                    ToolTip = 'Executes the Cloturer les lignes action.';
                    trigger OnAction();
                    var
                        SalesLine: Record "Sales Line";
                    begin

                        IF NOT CONFIRM(ESK001Qst) THEN EXIT;

                        SalesLine.RESET();
                        SalesLine.SETRANGE("Document Type", Rec."Document Type");
                        SalesLine.SETRANGE("Document No.", Rec."No.");
                        SalesLine.SETRANGE(Type, SalesLine.Type::Item);
                        SalesLine.SETFILTER("Outstanding Quantity", '>%1', 0);
                        IF SalesLine.FINDFIRST() THEN
                            REPEAT
                                SalesLine.CloseSalesLine();
                                SalesLine.MODIFY();
                            UNTIL SalesLine.NEXT() = 0;
                    end;
                }
                action("Insérer ligne de port")
                {
                    ApplicationArea = All;
                    Caption = 'Insérer ligne de port';
                    Image = Insert;
                    ToolTip = 'Executes the Insérer ligne de port action.';
                    trigger OnAction();
                    begin
                        // AD Le 31-08-2009 => Gestion des frais de port
                        CurrPage.SalesLines.PAGE.InsertPort();
                    end;
                }
                action("Insérer ligne de remise")
                {
                    ApplicationArea = All;
                    Caption = 'Insérer ligne de remise';
                    Image = Insert;
                    ToolTip = 'Executes the Insérer ligne de remise action.';
                    trigger OnAction();
                    begin
                        CurrPage.SalesLines.PAGE.InsertDiscount();
                    end;
                }
            }
        }
    }
    // var
    //     rec_salesline: Record "Sales Line";
    //     _qte_cde: Decimal;
    //     _qte_exp: Decimal;
    //     lArchiveManagement: Codeunit ArchiveManagement;
    //     ApplicationAreaSetup: Record "Application Area Setup";
    //     SalesCalcDiscountByType: Codeunit "Sales - Calc Discount By Type";
    //     lConfirmDeletion: Boolean;


    trigger OnAfterGetRecord()
    var
        rec_salesline: Record "Sales Line";
        _qte_cde: Decimal;
        _qte_exp: Decimal;
    begin
        VisibleEchFraction(); // ESK
                              //LM le 27-05-2013  =>'shipment status' erron‚, correctif ci dessous
        _qte_cde := 0;
        _qte_exp := 0;
        rec_salesline.RESET();
        rec_salesline.SETRANGE(rec_salesline."Document Type", Rec."Document Type");
        rec_salesline.SETRANGE(rec_salesline."Document No.", Rec."No.");
        IF rec_salesline.FindSet() THEN
            REPEAT
                _qte_cde += rec_salesline.Quantity;
                _qte_exp += rec_salesline."Quantity Shipped";
            UNTIL rec_salesline.NEXT() = 0;
        status_livraison := 'A livrer';
        IF _qte_exp > 0 THEN status_livraison := 'Reliquats';
        IF _qte_cde = _qte_exp THEN status_livraison := 'Livrée';

        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        gCentralEditable := NOT Rec."Avoid Active Central";
        // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
        SetStyleExp_Export();
        // FIN CFR le 05/04/2023
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        // MCO Le 03-10-2018
        Rec.SETRANGE("No.");
        // FIN MCO Le 03-10-2018
    end;

    trigger OnDeleteRecord(): Boolean
    var
        CodeunitsFunctions: Codeunit "Codeunits Functions";
        lConfirmDeletion: Boolean;
    begin
        // CFR le 05/04/2023 - R‚gie : Archivage des commandes … la suppression (si pas automatique)
        //EXIT(ConfirmDeletion); ANCIEN CODE
        lConfirmDeletion := Rec.ConfirmDeletion();
        IF lConfirmDeletion THEN
            CodeunitsFunctions.ArchiveSalesOrderOnDelete(Rec);
        // FIN CFR le 05/04/2023
    end;

    var
        status_livraison: Text[10];
        VisibleEsk: Boolean;
        VisibleEchFractionBln: Boolean;
        gCentralEditable: Boolean;
        gStyleExp_Export: Text;
        gEditable_Export: Boolean;
        IsOfficeHost: Boolean;
        ESK001Qst: Label 'Voulez vous lancer la commande ?';
        EditableEsk: Boolean;
        ESK002Msg: Label 'Il n''y a rien à livrer !';
        Text10800SYMErr: Label 'There are unposted prepayment amounts on the document of type %1 with the number %2.', comment = '%1 =Document Type ; %2 = Document No';
        Text10801SYMQst: Label 'There are unpaid prepayment invoices related to the document of type %1 with the number %2.', comment = '%1 =Document Type ; %2 = Document No';

    trigger OnOpenPage()
    var
        OfficeMgt: Codeunit "Office Management";
    begin
        IsOfficeHost := OfficeMgt.IsAvailable();
    end;

    procedure EnableFields();
    begin
        // AD Le 27-10-2009 => Pour maquer certain champ
        /* MIGRATION
        Currpage."Sell-to Contact No.".EDITABLE := FALSE;
        CurrForm."Sell-to Customer Name".EDITABLE := FALSE;
        CurrForm."Sell-to Address".EDITABLE := FALSE;
        CurrForm."Sell-to Address 2".EDITABLE := FALSE;
        CurrForm."Sell-to Post Code".EDITABLE := FALSE;
        CurrForm."Sell-to City".EDITABLE := FALSE;
        CurrForm."Sell-to Contact".EDITABLE := FALSE;
        CurrForm."Document Date".EDITABLE := FALSE;
        CurrForm."Bill-to Contact No.".EDITABLE := FALSE;
        CurrForm."Bill-to Name".EDITABLE := FALSE;
        CurrForm."Bill-to Address".EDITABLE := FALSE;
        CurrForm."Bill-to Address 2".EDITABLE := FALSE;
        CurrForm."Bill-to Post Code".EDITABLE := FALSE;
        CurrForm."Bill-to City".EDITABLE := FALSE;
        CurrForm."Bill-to Contact".EDITABLE := FALSE;
        CurrForm."Exclure RFA".EDITABLE := FALSE;
        CurrForm."Shortcut Dimension 1 Code".EDITABLE := FALSE;
        CurrForm."Shortcut Dimension 2 Code".EDITABLE := FALSE;
        CurrForm."Due Date".EDITABLE := FALSE;
        CurrForm."Payment Discount %".EDITABLE := FALSE;
        CurrForm."Pmt. Discount Date".EDITABLE := FALSE;
        CurrForm."Prices Including VAT".EDITABLE := FALSE;
        CurrForm."VAT Bus. Posting Group".EDITABLE := FALSE;
        CurrForm."Invoice Type".EDITABLE := FALSE;
        CurrForm."Location Code".EDITABLE := FALSE;
        CurrForm."Outbound Whse. Handling Time".EDITABLE := FALSE;

        // AD Le 16-06-2010 => DEMANDE DE FRANCK
        CurrForm."Shipment Method Code".EDITABLE := TRUE;
        // FIN AD Le 16-06-2010
        CurrForm."Shipping Agent Service Code".EDITABLE := FALSE;
        CurrForm."Shipping Time".EDITABLE := FALSE;
        CurrForm."Late Order Shipping".EDITABLE := FALSE;
        CurrForm."Package Tracking No.".EDITABLE := FALSE;
        CurrForm."Shipment Date".EDITABLE := FALSE;
        CurrForm."Shipping Advice".EDITABLE := FALSE;

        CurrForm."Currency Code".EDITABLE := FALSE;
        CurrForm."EU 3-Party Trade".EDITABLE := FALSE;
        CurrForm."Transaction Type".EDITABLE := FALSE;
        CurrForm."Transaction Specification".EDITABLE := FALSE;
        CurrForm."Transport Method".EDITABLE := FALSE;
        CurrForm."Exit Point".EDITABLE := FALSE;
        CurrForm.Area.EDITABLE := FALSE;
        CurrForm."Language Code".EDITABLE := FALSE;
        CurrForm."Customer Order No.".EDITABLE := FALSE;
        CurrForm."Prepayment %".EDITABLE := FALSE;
        CurrForm."Compress Prepayment".EDITABLE := FALSE;
        CurrForm."Prepmt. Payment Terms Code".EDITABLE := FALSE;
        CurrForm."Prepayment Due Date".EDITABLE := FALSE;
        CurrForm."Prepmt. Payment Discount %".EDITABLE := FALSE;
        CurrForm."Prepmt. Pmt. Discount Date".EDITABLE := FALSE;
        CurrForm."EDI Document".EDITABLE := FALSE;
        CurrForm."EDI Document No.".EDITABLE := FALSE;
        CurrForm."EDI Integration Date".EDITABLE := FALSE;
        CurrForm."EDI Integration Time".EDITABLE := FALSE;
        CurrForm."EDI Integration User".EDITABLE := FALSE;
        CurrForm."Due Date".VISIBLE := FALSE;
        CurrForm."Due Date 2".VISIBLE := FALSE;
        CurrForm."Payment Discount %".VISIBLE := FALSE;
        CurrForm."Pmt. Discount Date".VISIBLE := FALSE;

        */
        VisibleEsk := FALSE;
    end;

    procedure VisibleEchFraction();
    begin
        /*
        CurrForm."Payment Terms Code 2".EDITABLE:="Echéances fractionnées";
        CurrForm."Due Date 2".EDITABLE:="Echéances fractionnées";
        CurrForm."Taux Premiere Fraction".EDITABLE:="Echéances fractionnées";
        */
        VisibleEchFractionBln := Rec."Echéances fractionnées";
    end;

    procedure SendFaxMail(_pType: Code[10]);
    var
        LDoc: Record "Sales Header";
        LSendFax: Codeunit "Eskape Communication";
        DocPrint: Codeunit "Document-Print";
        LDocRef: RecordRef;
        Usage: Option "Order Confirmation","Work Order","Pick Instruction";
    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        //LM le 17-05-2013=>permet de modifier les options avant envoi
        DocPrint.PrintSalesOrder(Rec, Usage::"Order Confirmation");

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non géré !');
        END;
    end;

    procedure GenereBP(_pImpressionAuto: Boolean);
    var
        Rec_LigneVente: Record "Sales Line";
        lItem: Record Item;
        lShippingAgent: Record "Shipping Agent";
        GetSourceDocOutbound: Codeunit "Get Source Doc. Outbound";
        ReleaseSalesDoc: Codeunit "Release Sales Document";
        LSingleInstance: Codeunit SingleInstance;
        lInfo: Text;
        lErrorTxt: Label 'Les articles suivants ont un transporteur obligatoire qui n''est pas celui de la commande [%1] :\%2', comment = '%1 = transporteur ; %2 = details';
    begin
        // AD Le 21-10-2009 => Pour proposer les qtes en préparation
        Rec_LigneVente.RESET();
        Rec_LigneVente.SETCURRENTKEY("Document Type", "Document No.", Type);
        Rec_LigneVente.SETRANGE("Document Type", REC."Document Type");
        Rec_LigneVente.SETRANGE("Document No.", REC."No.");
        Rec_LigneVente.SETRANGE(Type, Rec_LigneVente.Type::Item);
        Rec_LigneVente.SETFILTER("Outstanding Quantity", '<>%1', 0);
        IF Rec_LigneVente.FINDSET() THEN
            REPEAT
                Rec_LigneVente.MajDelivry(Rec_LigneVente);
            UNTIL Rec_LigneVente.NEXT() = 0
        ELSE
            ERROR(ESK002Msg); // AD Le 23-03-2016 =>
        COMMIT(); // AD Le 23-05-2012
        // FIN AD Le 21-10-2009

        // AD Le 23-03-2016 =>
        CLEAR(Rec_LigneVente);
        Rec_LigneVente.SETCURRENTKEY("Document Type", "Document No.", Type);
        Rec_LigneVente.SETRANGE("Document Type", REC."Document Type");
        Rec_LigneVente.SETRANGE("Document No.", REC."No.");
        Rec_LigneVente.SETRANGE(Type, Rec_LigneVente.Type::Item);
        Rec_LigneVente.SETFILTER("Qty. to Ship", '<>%1', 0);
        IF Rec_LigneVente.ISEMPTY THEN ERROR(ESK002Msg);
        // FIN AD Le 23-03-2016

        // CFR le 30/11/2022 => Régie : transporteur préconisé peut être obligatoire
        lInfo := '';
        CLEAR(Rec_LigneVente);
        Rec_LigneVente.SETCURRENTKEY("Document Type", "Document No.", Type);
        Rec_LigneVente.SETRANGE("Document Type", Rec."Document Type");
        Rec_LigneVente.SETRANGE("Document No.", Rec."No.");
        Rec_LigneVente.SETRANGE(Type, Rec_LigneVente.Type::Item);
        Rec_LigneVente.SETFILTER("Qty. to Ship", '<>%1', 0);
        IF Rec_LigneVente.FINDSET() THEN
            REPEAT
                IF lShippingAgent.GET(Rec."Shipping Agent Code") THEN
                    // On ne fait pas le test si le transporteur n'est pas bloquant et si la commande n'est pas forcée
                    IF (lShippingAgent."Mandatory Non Blocking" <> TRUE) AND (NOT Rec."Force Mandatory Shipping Agent") THEN
                        // Test = il existe un article avec un transporteur obligatoire qui n'est pas celui de la commande
                        IF lItem.GET(Rec_LigneVente."No.") THEN
                            IF (lItem."Recommended Shipping Agent" <> '') AND (lItem."Mandatory Shipping Agent") AND
                               (lItem."Recommended Shipping Agent" <> Rec."Shipping Agent Code") THEN
                                IF STRPOS(lInfo, lItem."No. 2") = 0 THEN // évite les doublons d'article
                                    lInfo := lInfo + '- ' + lItem."No. 2" + ' [' + lItem."Recommended Shipping Agent" + '] ';

            UNTIL Rec_LigneVente.NEXT() = 0;
        IF (lInfo <> '') THEN
            ERROR(lErrorTxt, Rec."Shipping Agent Code", lInfo);
        // FIN CFR le 30/11/2022

        // CFR le 01/12/2022 => Régie : Commentaire transporteur
        IF lShippingAgent.GET(Rec."Shipping Agent Code") THEN
            IF NOT lShippingAgent.ShowComment(Rec."Ship-to Country/Region Code", Rec."Ship-to Post Code", TRUE) THEN
                ERROR('Traitement interrompu');
        // FIN CFR le 01/12/2022

        // AD Le 28-06-2011 => SYMTA ->
        IF Rec.Status = Rec.Status::Open THEN
            IF CONFIRM(ESK001Qst, TRUE) THEN
                ReleaseSalesDoc.PerformManualRelease(Rec);

        COMMIT(); // AD Le 23-05-2012

        LSingleInstance.SetHideDialog(TRUE);
        // FIN AD Le 28-06-2011

        LSingleInstance.SetImprimanteAuto(_pImpressionAuto);

        GetSourceDocOutbound.CreateFromSalesOrder(Rec);

        IF NOT Rec.FIND('=><') THEN
            Rec.INIT();
    end;

    local procedure APreparer();
    var
        ReleaseSalesDoc: Codeunit "Release Sales Document";
        PrepaymentMgt: Codeunit "Prepayment Mgt.";
        CodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // MCO Le 17-10-2017 => Réecriture suite modification standard
        IF PrepaymentMgt.TestSalesPrepayment(Rec) THEN
            ERROR(Format(STRSUBSTNO(Text10800SYMErr, Rec."Document Type", Rec."No.")))
        ELSE BEGIN
            IF PrepaymentMgt.TestSalesPayment(Rec) THEN BEGIN
                IF NOT CONFIRM(STRSUBSTNO(Text10801SYMQst, Rec."Document Type", Rec."No.")) THEN
                    EXIT;
                Rec.Status := Rec.Status::"Pending Prepayment";
                Rec.MODIFY();
                CurrPage.UPDATE();
            END ELSE BEGIN
                //ReleaseSalesDoc.PerformManualRelease(Rec);

                // AD Le 08-10-2009 => Permet de vérifier certaine donnée lors du lancement d'une commande EDI
                IF Rec."EDI Document" THEN
                    CodeunitsFunctions."Release EDI Order"(Rec);
                // FIN AD Le 08-10-2009
                ReleaseSalesDoc.PerformManualRelease(Rec);
            END;

            Rec.Status := Rec.Status::Preparate;
            Rec.MODIFY();
            CurrPage.UPDATE();
        END;
    end;

    local procedure Bloquer();
    var
        ReleaseSalesDoc: Codeunit "Release Sales Document";
        PrepaymentMgt: Codeunit "Prepayment Mgt.";
    begin
        // MCO Le 17-10-2017 => Réecriture suite modification standard
        IF PrepaymentMgt.TestSalesPrepayment(Rec) THEN
            ERROR(Format(STRSUBSTNO(Text10800SYMErr, Rec."Document Type", Rec."No.")))
        ELSE BEGIN
            IF PrepaymentMgt.TestSalesPayment(Rec) THEN BEGIN
                IF NOT CONFIRM(STRSUBSTNO(Text10801SYMQst, Rec."Document Type", Rec."No.")) THEN
                    EXIT;
                Rec.Status := Rec.Status::"Pending Prepayment";
                Rec.MODIFY();
                CurrPage.UPDATE();
            END ELSE
                ReleaseSalesDoc.PerformManualRelease(Rec);


            Rec.Status := Rec.Status::Bloqued;
            Rec.MODIFY();
            CurrPage.UPDATE();
        END;
    end;


    local procedure ImportExcel();
    var
        CuImportExcel: Codeunit "Import Ligne Document Vte XLS";
    begin
        CuImportExcel.ImportLigneCde(Rec);
    end;

    local procedure SetStyleExp_Export();
    begin
        //CFR le 05/04/2023 => Régie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        gEditable_Export := FALSE;
        IF (Rec."Salesperson Code" = '19') THEN BEGIN
            gStyleExp_Export := 'Unfavorable';
            gEditable_Export := TRUE;
        END;
        //FIN CFR le 05/04/2023
    end;

    procedure FctPerformManualRelease(var SalesHeader: Record "Sales Header")
    var
        BatchProcessingMgt: Codeunit "Batch Processing Mgt.";
        NoOfSelected: Integer;
        NoOfSkipped: Integer;
    begin
        NoOfSelected := SalesHeader.Count;
        SalesHeader.SetFilter(Status, '<>%1', SalesHeader.Status::Released);
        NoOfSkipped := NoOfSelected - SalesHeader.Count;
        BatchProcessingMgt.BatchProcess(SalesHeader, Codeunit::"Sales Manual Release", Enum::"Error Handling Options"::"Show Error", NoOfSelected, NoOfSkipped);
    end;
}