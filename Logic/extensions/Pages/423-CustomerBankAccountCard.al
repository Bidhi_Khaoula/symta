pageextension 51766 "Customer Bank Account Card" extends "Customer Bank Account Card" //423

{
    trigger OnAfterGetRecord()
    BEGIN
        rec."CheckRIB&IBAN"(); // MCO Le 21-11-2018 => R‚gie
    END;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    BEGIN
        rec."CheckRIB&IBAN"(); // MCO Le 21-11-2018 => R‚gie
    END;
}