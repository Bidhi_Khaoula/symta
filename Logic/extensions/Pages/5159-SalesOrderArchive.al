pageextension 50772 "Sales Order Archive" extends "Sales Order Archive" //5159
{
    layout
    {
        modify("Bill-to Customer No.")
        {
            Caption = 'Bill-to Customer No.';
        }
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                Importance = Promoted;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        modify("Sell-to Address")
        {
            Visible = false;
        }
        modify("Sell-to Address 2")
        {
            Visible = false;
        }
        addafter("Sell-to City")
        {
            field(Centrale; Rec.GetCentraleName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCentraleName() field.';
            }
        }
        addafter("External Document No.")
        {
            field("Quote No. For ARchive"; Rec."Quote No. For ARchive")
            {
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° Devis Archivage field.';
            }
        }
        addafter("Campaign No.")
        {
            field("Customer Disc. Group 2"; Rec."Cust. Disc. Group For Archive")
            {
                Caption = 'Customer Disc. Group';
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
        }
        addafter(Status)
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Customer Disc. Group _"; Rec."Cust. Disc. Group For Archive")
            {
                Caption = 'Customer Disc. Group';
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
            field("Cust. Price Group For Archive"; Rec."Cust. Price Group For Archive")
            {
                Caption = 'Customer Price Group';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Price Group field.';
            }
            field("Campaign No._"; Rec."Campaign No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Campaign No. field.';
            }
            field(Centrale_; Rec.GetCentraleName())
            {
                Caption = 'Centrale Active';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
            group("Description du travail")
            {
                Caption = 'Work Description';
                field(WorkDescription; WorkDescription)
                {
                    Importance = Standard;
                    MultiLine = true;
                    ShowCaption = false;
                    ToolTip = 'Specifies the products or service being offered';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addafter(Restore)
        {
            group(Documents)
            {
                Caption = 'Documents';
                Image = Documents;
                action("E&xpéditions")
                {
                    Caption = 'S&hipments';
                    Image = Shipment;
                    Promoted = true;
                    PromotedCategory = Process;
                    RunObject = Page "Posted Sales Shipments";
                    RunPageLink = "Order No." = FIELD("No.");
                    RunPageView = SORTING("Order No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the S&hipments action.';
                }
            }
        }
    }

    var
        WorkDescription: Text;

    trigger OnAfterGetRecord()
    begin
        // CFR le 06/04/2022 => R‚gie : Affichage du champ "Work Description"
        WorkDescription := Rec.GetWorkDescription();
    end;


}

