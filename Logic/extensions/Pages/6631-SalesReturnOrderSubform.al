pageextension 51243 "Sales Return Order Subform" extends "Sales Return Order Subform" //6631
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Line Discount %")
        {
            Editable = false;
            Visible = false;
        }
        addafter("Line Discount %")
        {
            field("Line Discount % SYM"; Rec."Line Discount %")
            {
                DecimalPlaces = 0 : 0;
                ApplicationArea = Basic, Suite;
                BlankZero = true;
                Enabled = not IsBlankNumber;
                Editable = false;
                ToolTip = 'Specifies the discount percentage that is granted for the item on the line.';

                trigger OnValidate()
                begin
                    DeltaUpdateTotals();
                end;
            }
        }
        modify("Bin Code")
        {
            Visible = FALSE;
            Editable = FALSE;
            QuickEntry = FALSE;
        }
        modify(Control37)
        {
            Visible = false;
        }
        modify(Control33)
        {
            Visible = false;
        }

        addafter(Control1)
        {
            field("Line No."; Rec."Line No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter("Return Reason Code")
        {
            field("RV Defective Location"; Rec."RV Defective Location")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Localisation field.';
            }
        }
        addafter("Line Amount")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
        }
        modify("Invoice Discount Amount")
        {
            visible = false;
        }
        modify("Invoice Disc. Pct.")
        {
            visible = false;
        }
        modify("Total Amount Incl. VAT")
        {
            visible = false;
        }
        addafter("ShortcutDimCode8")
        {
            field("Line type"; Rec."Line type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type ligne field.';
            }
        }
        addafter("Invoice Disc. Pct.")
        {
            field("Invoice Gross Weight"; TotalSalesLine."Gross Weight")
            {
                AutoFormatType = 1;
                Caption = 'Invoice Discount Amount';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Invoice Discount Amount field.';

                trigger OnValidate();
                var
                    SalesCalcDiscByType: Codeunit "Sales - Calc Discount By Type";
                    SalesHeader: Record "Sales Header";
                begin
                    SalesHeader.GET(Rec."Document Type", Rec."Document No.");
                    SalesCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalSalesLine."Inv. Discount Amount", SalesHeader);
                    CurrPage.UPDATE(FALSE);
                end;
            }



        }
    }
    actions
    {

        addbefore("F&unctions")
        {
            action(Comments2)
            {
                Caption = 'Co&mments';
                Image = ViewComments;
                ApplicationArea = All;
                ToolTip = 'Executes the Co&mments action.';

                trigger OnAction();
                begin
                    Rec.ShowLineComments();
                end;
            }
        }
    }


    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;

}

