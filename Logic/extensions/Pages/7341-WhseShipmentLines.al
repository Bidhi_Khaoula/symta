pageextension 51358 "Whse. Shipment Lines" extends "Whse. Shipment Lines" //7341
{
    layout
    {
        addafter("Item No.")
        {
            field("Ref. Active"; CUMultiRef.RechercheRefActive(Rec."Item No."))
            {
                Caption = 'Ref. Active';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        addafter(Status)
        {
            field("Packing List No."; Rec."Packing List No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Packing List No. field.';
            }
        }
        addafter("Line No.")
        {
            field("Date Flashage"; Rec."Date Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Flashage field.';
            }
            field("Heure Flashage"; Rec."Heure Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Flashage field.';
            }
            field("Utilisateur Flashage"; Rec."Utilisateur Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Utilisateur Flashage field.';
            }
            field("Utilisateur affecté"; GetUserAffectName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetUserAffectName() field.';
            }
        }
    }
    actions
    {
        modify("Show &Whse. Document")
        {
            Visible = false;
        }
        modify("ShowDocument")
        {
            Visible = false;
        }
        addafter("ShowDocument")
        {
            action(ShowDocumentSym)
            {
                ApplicationArea = Warehouse;
                Caption = 'Show Document';
                Image = ViewOrder;
                ShortCutKey = 'Return';
                ToolTip = 'View the related warehouse document.';

                trigger OnAction()
                begin
                    CLEAR(frm_Whse);
                    frm_Whse.SetNoFlash(Rec."No.");
                    frm_Whse.RUN();
                end;
            }
        }
    }

    var
        CUMultiRef: Codeunit "Gestion Multi-référence";
        frm_Whse: Page "Warehouse Shipment SYMTA";

    local procedure GetUserAffectName(): Text;
    var
        lGeneralsParameters: Record "Generals Parameters";
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        IF lWarehouseShipmentHeader.GET(Rec."No.") AND
          lGeneralsParameters.GET('MAG_EMPLOYES', lWarehouseShipmentHeader."Assigned User ID") THEN
            EXIT(lGeneralsParameters.LongDescription);

        EXIT('');
    end;


}

