pageextension 51308 "Get Return Shipment Lines" extends "Get Return Shipment Lines" //6648
{
    layout
    {
        addafter("Return Qty. Shipped Not Invd.")
        {
            field("Return Order No."; Rec."Return Order No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Return Order No. field.';
            }
            field("Unit Cost (LCY)"; Rec."Unit Cost (LCY)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Cost (LCY) field.';
            }
        }
    }


}

