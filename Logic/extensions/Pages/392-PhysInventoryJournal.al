pageextension 50558 "Phys. Inventory Journal" extends "Phys. Inventory Journal" //392
{

    layout
    {

        addafter("Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        addafter(Quantity)
        {
            field("Stock Dépot"; Stock - Rec."Qty. (Calculated)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. (Calculated) field.';
            }
        }
        modify("Unit Amount")
        {
            Editable = false;
        }
        modify(Amount)
        {
            Editable = false;
        }
        modify("Indirect Cost %")
        {
            Editable = false;
        }
        modify("Unit Cost")
        {
            Editable = false;
        }
    }
    actions
    {

        addafter("Bin Contents")
        {
            action("Ouvrir multiconsultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Ouvrir multiconsultation action.';

                trigger OnAction();
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
        addafter(CalculateInventory)
        {
            action(CalculateInventory2)
            {
                ApplicationArea = All;
                Caption = 'Calculate &Inventory';
                Ellipsis = true;
                Image = CalculateInventory;
                Promoted = true;
                PromotedCategory = Process;
                Scope = Repeater;
                ToolTip = 'Executes the Calculate &Inventory action.';

                trigger OnAction();
                begin
                    CalcQtyOnHand2.SetItemJnlLine(Rec);
                    CalcQtyOnHand2.RUNMODAL();
                    CLEAR(CalcQtyOnHand2);
                end;
            }
        }
    }

    var
        CalcQtyOnHand2: Report "Calculate Inventory Par Emplac";
        Stock: Decimal;

    trigger OnAfterGetRecord()
    var
        LItem: Record Item;
    begin
        // Calclul d'un FLOWFIELD
        IF LItem.GET(rec."Item No.") THEN;
        LItem.CALCFIELDS(Inventory);
        Stock := LItem.Inventory;
    end;

    trigger OnOpenPage()
    begin
        //CFR le 30/11/2022 => R‚gie : Tri par d‚faut sur [Code emplacement] >> ‚galement dans [SourceTableView] sinon c'est pas du 100%
        Rec.SETCURRENTKEY("Bin Code", "Item No.", "Variant Code", "Posting Date");
        Rec.SETASCENDING("Bin Code", TRUE);

    end;

}

