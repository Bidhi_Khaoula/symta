pageextension 50004 "Item Charge Assignment (Sales)" extends "Item Charge Assignment (Sales)" //5814
{
    layout
    {
        modify("Qty. Assigned")
        {
            Editable = true;
        }
    }
}