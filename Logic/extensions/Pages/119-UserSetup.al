pageextension 50628 "User Setup" extends "User Setup" //119
{
    layout
    {
        addafter("Time Sheet Admin.")
        {
            field("E-Mail"; Rec."E-Mail")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the E-Mail field.';
            }
        }
    }
}