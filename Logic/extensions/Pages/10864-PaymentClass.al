pageextension 50038 "Payment Class" extends "Payment Class" //10864
{
    layout
    {
        addafter("SEPA Transfer Type")
        {
            field(Sign; Rec.Sign)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sens field.';
            }
        }
    }
}

