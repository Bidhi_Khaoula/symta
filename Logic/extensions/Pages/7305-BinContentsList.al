pageextension 51339 "Bin Contents List" extends "Bin Contents List" //7305
{
    layout
    {
        addafter("Cross-Dock Bin")
        {
            field("capacité"; Rec.capacité)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the capacité field.';
            }
            field(Description; Rec.Description)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Description field.';
            }
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
            field("Date dernier Mvt"; Rec.GetCommentaireInfoEntrée('DATE'))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCommentaireInfoEntrée(''DATE'') field.';
            }
            field("Last Movement Date"; Rec."Last Movement Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Quantity (Base) field.';
            }
            field("Last Entry Date"; Rec."Last Entry Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date dernière entrée field.';
            }
            field("Bin Content Creation Date"; Rec."Bin Content Creation Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date création contenu emplacement field.';
            }
        }
    }
    trigger OnOpenPage()
    begin
        //FBRUN LE 23/12/22 tickert 11-14122022
        IF Rec.GETFILTER(Rec."Item No.") <> '' THEN BEGIN
            Rec.SETCURRENTKEY("Item No.", Rec."Location Code", Rec.Quantity);
            Rec.ASCENDING(FALSE);
            IF Rec.FINDFIRST() THEN;
        END;
        //FIN FBRUN
    end;

}

