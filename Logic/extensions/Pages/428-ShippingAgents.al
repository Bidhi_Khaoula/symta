pageextension 50082 "Shipping Agents" extends "Shipping Agents" //428
{
    layout
    {
        addafter("Internet Address")
        {
            field("Label Folder"; Rec."Label Folder")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Répertoire étiquette field.';
            }
        }

        addafter("Account No.")
        {
            field("Alert Package Weight"; Rec."Alert Package Weight")
            {
                ToolTip = 'Le poids indiqué déclenche une confirmation par l''utilisateur s''il est dépassé pour une livraison';
                ApplicationArea = All;
            }
            field("Poids colis maximum"; Rec."Poids colis maximum")
            {
                ToolTip = 'Le poids indiqué bloque la livraison pour le transporteur';

                ApplicationArea = all;
            }
            field("Maximum Length Item"; Rec."Maximum Length Item")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Maximum Length Item field.';
            }
            field("Recomended Color"; Rec."Recomended Color")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Couleur transporteur préconisé field.';
            }
            field("No Delivery On Saturday"; Rec."No Delivery On Saturday")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Pas de livraison le samedi field.';
            }
            field("Mandatory Non Blocking"; Rec."Mandatory Non Blocking")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur non bloquant field.';
            }
        }
    }

    actions
    {
        addafter(ShippingAgentServices)
        {
            action("Co&mmentaires")
            {
                Caption = 'Co&mments';
                Image = ViewComments;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                RunObject = Page "Shipping Agent Comment List";
                RunPageLink = "Shipping Agent Code" = FIELD(Code);
                ApplicationArea = All;
                ToolTip = 'Executes the Co&mments action.';
            }
        }
    }

}