pageextension 50764 "Misc. Article Information" extends "Misc. Article Information" //5219
{

    layout
    {
        modify("Employee No.")
        {
            ToolTip = '';
        }
        modify("Misc. Article Code")
        {
            ToolTip = '';
        }
        modify(Description)
        {
            ToolTip = '';
        }
        modify("Serial No.")
        {
            ToolTip = '';
        }
        modify("From Date")
        {
            ToolTip = '';
        }
        modify("To Date")
        {
            ToolTip = '';
        }
        modify("In Use")
        {
            ToolTip = '';
        }
        modify(Comment)
        {
            ToolTip = '';
        }
    }

}

