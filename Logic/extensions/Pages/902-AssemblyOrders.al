pageextension 51486 "Assembly Orders" extends "Assembly Orders" //902
{
    layout
    {
        modify("Document Type")
        {
            ToolTip = 'Specifies the type of assembly document the record represents in assemble-to-order scenarios.';
            StyleExpr = gStyleExp_DocumentType;
        }

        addafter("Document Type")
        {
            field("Ref Active"; Rec."Ref Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref Active field.';
            }
        }
        modify("Starting Date")
        {
            Visible = false;
        }
        modify("Ending Date")
        {
            Visible = false;
        }
    }
    actions
    {

        addafter(Reopen)
        {
            action("Réappro")
            {
                ApplicationArea = All;
                Image = Replan;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                RunObject = Report "Réappro Nomenclature";
                ToolTip = 'Créée des ordres d''assemblage pour les articles [Assemblage sur Stock] en fonction des attendus';
            }
        }
    }

    var
        gStyleExp_DocumentType: Text;

    trigger OnAfterGetRecord()
    begin
        // CFR le 11/05/2022 => R‚gie : Champ pour identifier graphiquement les KITS v‚rifi‚s
        IF (Rec.Checked) THEN
            gStyleExp_DocumentType := ''
        ELSE
            gStyleExp_DocumentType := 'Unfavorable';
        // FIN CFR le 11/05/2022    
    end;

}

