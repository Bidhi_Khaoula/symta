pageextension 50040 "Payment Slip" extends "Payment Slip" //10868
{

    layout
    {

        addafter("Partner Type")
        {
            group(Validation)
            {
                Caption = 'Validation';
                field("_Source Code"; Rec."Source Code")
                {
                    Importance = Promoted;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Code field.';
                }
                field("_Account Type"; Rec."Account Type")
                {
                    Importance = Promoted;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Account Type field.';
                }
                field("_Account No."; Rec."Account No.")
                {
                    Importance = Promoted;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Account No. field.';
                }
            }
        }
    }
    actions
    {
        addafter(SuggestCustomerPayments)
        {
            action("Recupérer Relevé client")
            {
                Caption = 'Recupérer Relevé client';
                Image = SuggestCustomerPayments;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Recupérer Relevé client action.';

                trigger OnAction();
                var
                    PaymentClass: Record "Payment Class";
                    CreateCustomerPmtSuggestion: Report "Relevé client->LCR";
                    Text002Msg: Label 'This payment class does not authorize customer suggestions.';
                    Text003Msg: Label 'You cannot suggest payments on a posted header.';

                begin

                    IF Rec."Status No." <> 0 THEN
                        MESSAGE(Text003Msg)
                    ELSE
                        IF PaymentClass.GET(Rec."Payment Class") THEN
                            IF PaymentClass.Suggestions = PaymentClass.Suggestions::Customer THEN BEGIN
                                CreateCustomerPmtSuggestion.SetGenPayLine(Rec);
                                CreateCustomerPmtSuggestion.RUNMODAL();
                                CLEAR(CreateCustomerPmtSuggestion);
                            END ELSE
                                MESSAGE(Text002Msg);
                end;
            }
        }
    }


}

