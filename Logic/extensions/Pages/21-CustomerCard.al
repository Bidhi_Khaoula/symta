pageextension 50195 "Customer Card" extends "Customer Card" //21
{
    layout
    {
        modify(General)
        {
            Editable = Editable;
        }
        addafter("No.")
        {
            field(Entete; Rec.Entete)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Entete field.';
            }
        }
        addafter("IC Partner Code")
        {
            field("FAX Comptabilité"; Rec."FAX Comptabilité")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the FAX Comptabilité field.';
            }
        }
        addafter("Copy Sell-to Addr. to Qte From")
        {
            field("Invoice Copies"; Rec."Invoice Copies")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Invoice Copies field.';
            }
        }
        addafter(Blocked)
        {
            field("Bloquer en commande"; Rec."Bloquer en commande")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Bloquer en commande field.';
            }
            field("Commentaire client bloqué"; Rec."Commentaire client bloqué")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Commentaire client bloqué field.';
            }
            field("Confirmation Centrale"; Rec."Confirmation Centrale")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Confirmation Centrale field.';
            }
        }
        modify(TotalSales2)
        {
            Visible = false;
        }
        modify("CustSalesLCY - CustProfit - AdjmtCostLCY")
        {
            visible = false;
        }
        modify(AdjCustProfit)
        {
            Visible = false;
        }
        modify(AdjProfitPct)
        {
            Visible = false;
        }
        addafter(AdjProfitPct)
        {
            field("APE Code"; Rec."APE Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the APE Code field.';
            }
            field("EORI Code"; Rec."EORI Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code EORI field.';
            }
            field(SIRET; Rec.SIRET)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the SIRET field.';
            }
        }
        modify("Address & Contact")
        {
            Editable = Editable;
        }
        movelast(ContactDetails; "Phone No.", MobilePhoneNo, "E-Mail", "Fax No.")
        modify("Fax No.")
        {
            trigger OnAssistEdit()
            VAR
                CompanyInfo: Record "Company Information";
                cuSendMail: Codeunit "Eskape Communication : Email";
                FaxNo: Code[10];
                FaxNoCheck: Integer;
                FaxNoOK: Boolean;
                SynthaxErr: Label '%1: syntaxe incorrecte.', Comment = '%1';
                mailErr: Label 'Veuillez renseigner "%1" dans les informations société', Comment = '%1';
            BEGIN

                // On doit avoir le domaine de messagerie du mailToFax de renseign‚
                WITH CompanyInfo DO BEGIN
                    GET();
                    IF "MailToFax Domain" = '' THEN
                        ERROR(Format(STRSUBSTNO(mailErr, FIELDCAPTION("MailToFax Domain"))));
                END;

                // Suppression des espaces
                Evaluate(FaxNo, DELCHR(Rec."Fax No."));

                FaxNoOK := FALSE;
                // Le num‚ro est de composer de 10 chiffres
                IF (STRLEN(FaxNo) = 10) AND EVALUATE(FaxNoCheck, FaxNo) THEN
                    // valeur convertie en num‚rique = valeur d'origine => c'est bien un num‚ro de 10 chiffres
                    IF PADSTR('', 10 - STRLEN(FORMAT(FaxNoCheck)), '0') + FORMAT(FaxNoCheck) = FaxNo THEN
                        FaxNoOK := TRUE;

                IF FaxNoOK THEN
                    cuSendMail.SendMail('', '', FaxNo + '@' + CompanyInfo."MailToFax Domain", '', '')
                ELSE
                    ERROR(Format(STRSUBSTNO(SynthaxErr, rec.FIELDCAPTION("Fax No."))));
            END;
        }
        addafter("Address & Contact")
        {
            group("Expédition")
            {
                Caption = 'Expédition';
                Editable = Editable;
                field("Shipping Agent Code2"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Transporteur imperatif2"; Rec."Transporteur imperatif")
                {
                    ApplicationArea = all;
                    Caption = 'Transporteur imperatif2';
                    ToolTip = 'Specifies the value of the Transporteur imperatif2 field.';
                }
                field("Mode d'expédition2"; Rec."Mode d'expédition")
                {
                    ApplicationArea = all;
                    Caption = 'Mode d''expédition2';
                    ToolTip = 'Specifies the value of the Mode d''expédition2 field.';
                }
                field("Adresse Expédition2"; Rec."Adresse Expédition")
                {
                    ApplicationArea = all;
                    Caption = 'Adresse Expédition2';
                    ToolTip = 'Specifies the value of the Adresse Expédition2 field.';
                }
                field("Adresse 2 Expédition2"; Rec."Adresse 2 Expédition")
                {
                    Caption = 'Adresse 2 Expédition2';
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Adresse 2 Expédition2 field.';
                }
                field("Code postal Expédition2"; Rec."Code postal Expédition")
                {
                    ApplicationArea = all;
                    Caption = 'Code postal Expédition2';
                    ToolTip = 'Specifies the value of the Code postal Expédition2 field.';
                }
                field("Ville Expédition2"; Rec."Ville Expédition")
                {
                    ApplicationArea = all;
                    Caption = 'Ville Expédition2';
                    ToolTip = 'Specifies the value of the Ville Expédition2 field.';
                }
                field("Code Expéditeur2"; Rec."Code Expéditeur")
                {
                    Caption = 'Code Expéditeur2';
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Expéditeur2 field.';
                }
            }
            group(Web)
            {
                caption = 'Web';
                Editable = Editable;
                field("Forced Web2"; Rec."Forced Web")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Web Forçé field.';
                }
                field("Droits Téléchargement WEB"; Rec."Droits Téléchargement WEB")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Droits Téléchargement WEB field.';
                }
                field("Login WEB"; Rec."Login WEB")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Login WEB field.';
                }
                field("Disable Web"; Rec."Disable Web")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Disable Web field.';
                }
                field("Mot de passe WEB"; Rec."Mot de passe WEB")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Mot de passe WEB field.';
                }
                field("Abandon remainder"; Rec."Abandon remainder")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Abandon reliquat field.';
                }
                field("Supprimé"; Rec."Supprimé")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Supprimé field.';
                }
                field("Acces Right"; Rec."Acces Right")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Access Right field.';
                }
                field("Consult Price Right"; Rec."Consult Price Right")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Consult Price Right field.';
                }
                field("AR Commande Therefore"; Rec."AR Commande Therefore")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the AR Commande Therefore field.';
                }
                field("E-Mail AR Cde Auto"; Rec."E-Mail AR Cde Auto")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Email AR Cde Auto field.';
                }
            }
        }
        moveafter("Abandon remainder"; "Last Date Modified")
        modify(Invoicing)
        {
            Editable = Editable;
        }
        addafter("Bill-to Customer No.")
        {
            field("Invoice Customer No."; Rec."Invoice Customer No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the N° Client facturé field.';
            }
        }
        addafter("Copy Sell-to Addr. to Qte From")
        {
            field("Envoi Facturation"; Rec."Envoi Facturation")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Envoi Facturation field.';
            }
            field("E-Mail Facturation"; Rec."E-Mail Facturation")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the E-Mail Facturation field.';
            }
            field("E-Mail secondaire"; Rec."E-Mail secondaire")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the E-Mail secondaire field.';
                trigger OnLookup(Var Text: Text): Boolean
                VAR
                    lEMailAddress: Record "E-Mail Address";
                    lEMailList: Page "E-Mail List";
                BEGIN
                    lEMailAddress.FILTERGROUP(2);
                    lEMailAddress.SETRANGE("Entry Type", lEMailAddress."Entry Type"::"Client facturation");
                    lEMailAddress.SETRANGE("Customer No.", Rec."No.");
                    lEMailAddress.FILTERGROUP(0);
                    lEMailList.SETTABLEVIEW(lEMailAddress);
                    lEMailList.EDITABLE(TRUE);
                    lEMailList.RUNMODAL();
                END;
            }
        }
        addafter("Customer Disc. Group")
        {
            field("Sous Groupe Tarifs"; Rec."Sous Groupe Tarifs")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Sous Groupe Tarifs field.';
            }
        }
        modify(Payments)
        {
            Editable = Editable;
        }
        addafter("Prepayment %")
        {
            field("DEEE Tax"; Rec."DEEE Tax")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Soumis à la taxe DEEE field.';
            }
            field("Code Assurance Credit"; Rec."Code Assurance Credit")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Assurance Credit field.';
            }
            field("Identif. Assurance Credit"; Rec."Identif. Assurance Credit")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Identif. Assurance Credit field.';
            }
            field("Regroupement Facturation"; Rec."Regroupement Facturation")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Regroupement Facturation field.';
            }
            field("Code Groupement RFA"; Rec."Code Groupement RFA")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Groupement RFA field.';
            }
            field("Invoice Type"; Rec."Invoice Type")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Type Facturation field.';
            }
            field("Combine Shipments 2"; Rec."Combine Shipments")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Combine Shipments field.';
            }
            field("Groupement payeur 2"; Rec."Groupement payeur")
            {
                Caption = 'Groupement payeur 2';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Groupement payeur 2 field.';
            }
            field("Masquer remise sur édition"; Rec."Masquer remise sur édition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Masquer remise sur édition field.';
            }
            field("Client Comptoir"; Rec."Client Comptoir")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Client Comptoir field.';
            }
            field("Inverser Adresses Factures"; Rec."Inverser Adresses Factures")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Inverser Adresses Factures field.';
            }
        }
        modify("Shipment Method Code")
        {
            Caption = 'Code conditions de livraison';
        }
        addafter("Shipment Method Code")
        {
            field("Incoterm City"; Rec."Incoterm City")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Ville Incoterm ICC 2020 field.';
            }
            field("Incoterm Code"; Rec."Incoterm Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
        }
        modify(Shipping)
        {
            Editable = Editable;
        }
        addafter("Shipping Agent Code")
        {
            field("Transporteur imperatif"; Rec."Transporteur imperatif")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Transporteur imperatif field.';
            }
        }
        addafter("Shipping Agent Service Code")
        {
            field("Adresse Expédition"; Rec."Adresse Expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Adresse Expédition field.';
            }
            field("Adresse 2 Expédition"; Rec."Adresse 2 Expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Adresse 2 Expédition field.';
            }
            field("Ville Expédition"; Rec."Ville Expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the City field.';
            }
            field("Code postal Expédition"; Rec."Code postal Expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Post Code field.';
            }
            field("Code Expéditeur"; Rec."Code Expéditeur")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Expéditeur field.';
            }
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
            field("Figuring Sales Shipment"; Rec."Figuring Sales Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Bon de livraison chiffré field.';
            }
        }
        moveafter("Transporteur imperatif"; "Shipping Time", "Base Calendar Code", "Customized Calendar")
        addafter(Shipping)
        {
            group(Divers)
            {
                Caption = 'Divers';
                Editable = Editable;
                field("Family Code 1"; Rec."Family Code 1")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Famille 1 field.';
                }
                field("Family Code 2"; Rec."Family Code 2")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Famille 2 field.';
                }
                field("Family Code 3"; Rec."Family Code 3")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Famille 3 field.';
                }
                field("Activity Code"; Rec."Activity Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Activité field.';
                }
                field("Direction Code"; Rec."Direction Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Direction field.';
                }
                group(Central)
                {
                    Caption = 'Central';
                    field("Centrale Active"; Rec."Centrale Active")
                    {
                        DrillDown = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Centrale Active field.';
                        Trigger OnAssistEdit()
                        VAR
                            lPurchaseCentralHistory: Record "Purchase Central History";
                            lPurchaseCentralHistoryList: Page "Purchase Central History List";
                        begin

                            // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
                            CLEAR(lPurchaseCentralHistory);
                            lPurchaseCentralHistory.SETRANGE("Customer No.", Rec."No.");
                            lPurchaseCentralHistoryList.SETTABLEVIEW(lPurchaseCentralHistory);
                            lPurchaseCentralHistoryList.EDITABLE(TRUE);
                            lPurchaseCentralHistoryList.RUNMODAL();
                        end;
                    }
                    field("Groupement payeur"; Rec."Groupement payeur")
                    {
                        ApplicationArea = all;
                        ToolTip = 'Specifies the value of the Groupement payeur field.';
                    }
                    field("Libre 3"; Rec."Libre 3")
                    {
                        ApplicationArea = all;
                        ToolTip = 'Specifies the value of the Code adhérent field.';
                    }

                }

                field(Holding; Rec.Holding)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Holding field.';
                }
                field("Territory Code"; Rec."Territory Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Territory Code field.';
                }
                field("Libre 1"; Rec."Libre 1")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Libre 1 field.';
                }
                field("Libre 2"; Rec."Libre 2")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Libre 2 field.';
                }

                field("Libre 4"; Rec."Libre 4")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Libre 4 field.';
                }
                field("Code Enseigne 1"; Rec."Code Enseigne 1")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
                }
                field("Code Enseigne 2"; Rec."Code Enseigne 2")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
                }
                field("Présent Web"; Rec."Présent Web")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Présent Web field.';
                }
                field("Forced Web"; Rec."Forced Web")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Web Forçé field.';
                }
                field("Surface de vente"; Rec."Surface de vente")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Surface de vente field.';
                }
                field(Responsable; Rec.Responsable)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Responsable field.';
                }
                field("Mailing Information"; Rec."Mailing Information")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Info mailing field.';
                }
                field("Rate Commission"; Rec."Rate Commission")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Coefficient de commission field.';
                }
                field("Customer Price Group2"; Rec."Customer Price Group")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Customer Price Group field.';
                }
                field("Customer Disc. Group2"; Rec."Customer Disc. Group")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Customer Disc. Group field.';
                }
                field("Sous Groupe Tarifs2"; Rec."Sous Groupe Tarifs")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Sous Groupe Tarifs field.';
                }
                field("Allow Line Disc.2"; Rec."Allow Line Disc.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Allow Line Disc. field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date de création field.';
                }

            }
        }
        addafter(Statistics)
        {
            group("E.D.I.")
            {
                Caption = 'E.D.I.';
                Editable = Editable;
                field("Customer EDI Code"; Rec."Customer EDI Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code EDI Client field.';
                }
                field("Code AR EDI"; Rec."Code AR EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code AR EDI field.';
                }
                field("Date Début Export AR EDI"; Rec."Date Début Export AR EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date Début Export AR EDI field.';
                }
                field("Code Livraison EDI"; Rec."Code Livraison EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Livraison EDI field.';
                }
                field("Date Début Export Livr. EDI"; Rec."Date Début Export Livr. EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date Début Export Livr. EDI field.';
                }
                field("Code Facturation EDI"; Rec."Code Facturation EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code Facturation EDI field.';
                }
                field("Date Début Export Facture EDI"; Rec."Date Début Export Facture EDI")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date Début Export Facture EDI field.';
                }
            }
        }
        addbefore(Details)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = all;
            }
        }
    }

    actions
    {
        addafter("&Customer")
        {
            action("Editable O/N")
            {
                Caption = 'Editable O/N';
                ApplicationArea = All;
                Promoted = true;
                Visible = Bonton_Editable;
                PromotedIsBig = true;
                Image = Edit;
                PromotedCategory = Process;
                ToolTip = 'Executes the Editable O/N action.';
                trigger OnAction()
                begin
                    EditableO_N();
                end;
            }
        }
        addafter(CustomerReportSelections)
        {
            action("Copier client vers ....")
            {
                ApplicationArea = All;
                Caption = 'Copier client vers ....';
                Image = Copy;
                ToolTip = 'Executes the Copier client vers .... action.';
                trigger OnAction()
                begin
                    Copierclient();
                end;
            }
        }
        addafter("Ledger E&ntries")
        {
            action("Extrait de compte")
            {
                Caption = 'Extrait de compte';
                ApplicationArea = All;
                Image = CustomerLedger;
                ToolTip = 'Executes the Extrait de compte action.';
                trigger OnAction()
                VAR
                    _client: Record Customer;
                    _extrait: Report "Extrait de compte client";
                BEGIN

                    CLEAR(_client);
                    CLEAR(_extrait);
                    _client := Rec;
                    _client.SETRECFILTER();
                    _extrait.SETTABLEVIEW(_client);
                    _extrait.RUNMODAL();
                end;
            }
        }
        modify("Invoice &Discounts")
        {
            Caption = 'Invoice &Discounts';
        }
        modify("Line Discounts")
        {
            Visible = false;
        }
        addafter("Line Discounts")
        {
            action("Line Discounts2")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Line Discounts';
                Image = LineDiscount;
                Visible = not ExtendedPriceEnabled2;
                ToolTip = 'View or set up different discounts for items that you sell to the customer. An item discount is automatically granted on invoice lines when the specified criteria are met, such as customer, quantity, or ending date.';
                ObsoleteState = Pending;
                ObsoleteReason = 'Replaced by the new implementation (V16) of price calculation.';
                ObsoleteTag = '17.0';

                trigger OnAction()
                var
                    SalesLineDiscount: Record "Sales Line Discount";
                begin
                    SalesLineDiscount.SetCurrentKey("Sales Type", "Sales Code");
                    SalesLineDiscount.SetRange("Sales Type", SalesLineDiscount."Sales Type"::"Customer Disc. Group");
                    SalesLineDiscount.SetRange("Sales Code", Rec."Customer Disc. Group");
                    Page.Run(Page::"Sales Line Discounts", SalesLineDiscount);
                end;
            }
        }
        addafter(Comment)
        {
            action("page Groupement/Marque")
            {
                ApplicationArea = All;
                Caption = 'Groupement/Marque';
                RunObject = Page "Groupement/Marque";
                RunPageLink = "Code client" = field("No.");
                ToolTip = 'Executes the Groupement/Marque action.';
            }
        }
    }
    var
        PriceCalculationMgt: Codeunit "Price Calculation Mgt.";
        Editable: Boolean;
        Bonton_Editable: Boolean;
        ExtendedPriceEnabled2: Boolean;

    trigger OnOpenPage()
    begin
        // FB Le 03-11-2015
        Bonton_Editable := FALSE;
        Editable := TRUE;
        IF (CurrPage.EDITABLE) AND (Rec."No." <> '') THEN BEGIN
            Bonton_Editable := TRUE;
            Editable := FALSE;
        END;
        // FIN FB Le 03-11-2015
        ExtendedPriceEnabled2 := PriceCalculationMgt.IsExtendedPriceCalculationEnabled();

    end;

    LOCAL PROCEDURE EditableO_N();
    BEGIN
        Editable := NOT Editable;
    END;

    LOCAL PROCEDURE Copierclient();
    VAR
        _Customer: Record Customer;
        Copycustomer: Page "Copy customer";
    BEGIN
        Copycustomer.init(Rec);
        Copycustomer.RUNMODAL();
        Copycustomer.returnNewCust(_Customer);
        IF _Customer."No." <> '' THEN
            IF CONFIRM('Voulez-vous allez sur la fiche du client ' + _Customer."No." + '?') THEN BEGIN
                rec.RESET();
                rec.GET(_Customer."No.");
                CurrPage.UPDATE(FALSE);

            END;
    END;
}