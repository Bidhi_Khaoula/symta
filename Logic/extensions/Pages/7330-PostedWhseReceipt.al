pageextension 51377 "Posted Whse. Receipt" extends "Posted Whse. Receipt" //7330
{
    layout
    {
        addafter("Assignment Time")
        {
            field("Total origine"; Rec."Total origine")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total origine field.';
            }
        }
        addafter(PostedWhseRcptLines)
        {
            part("Mise à jour"; "Update Whse. Posted Rcpt.")
            {
                ApplicationArea = all;
                Caption = 'Mise à jour';
                Provider = PostedWhseRcptLines;
                SubPageLink = "No." = FIELD("No."), "Line No." = FIELD("Line No.");
            }
        }
    }
}

