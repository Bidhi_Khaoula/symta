pageextension 51326 Zones extends Zones //7300
{
    layout
    {
        addafter("Zone Ranking")
        {
            field("BP Printer Name"; Rec."BP Printer Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Printer Name field.';
            }
            field("Label BP Printer Name"; Rec."Label BP Printer Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nom de l''imprimante Etiquette BP field.';
            }
        }
    }
    actions
    {

    }
}

