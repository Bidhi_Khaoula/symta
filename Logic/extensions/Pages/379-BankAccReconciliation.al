pageextension 50018 "Bank Acc. Reconciliation" extends "Bank Acc. Reconciliation"//379
{
    actions
    {
        modify("Transfer to General Journal")
        {
            Visible = false;
        }
        addafter("Transfer to General Journal")
        {
            action("Transfer to General Jnl Symta")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Transfer to General Journal';
                Ellipsis = true;
                Image = TransferToGeneralJournal;
                ToolTip = 'Transfer the lines from the current window to the general journal.';

                trigger OnAction()
                var
                    TempBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line" temporary;
                    TransBankRecToGenJnl: Report "Trans. Bank Rec. to Gen. Jnl.";
                    NoBankAccReconcilliationLineWithDiffSellectedErr: Label 'Select the bank statement lines that have differences to transfer to the general journal.';
                begin
                    CurrPage.StmtLine.PAGE.GetSelectedRecordsSymta(TempBankAccReconciliationLine);
                    TempBankAccReconciliationLine.Setrange(Difference, 0);
                    TempBankAccReconciliationLine.DeleteAll();
                    TempBankAccReconciliationLine.Setrange(Difference);
                    if TempBankAccReconciliationLine.IsEmpty() then
                        error(NoBankAccReconcilliationLineWithDiffSellectedErr);
                    TransBankRecToGenJnl.SetBankAccReconLine(TempBankAccReconciliationLine);
                    TransBankRecToGenJnl.SetBankAccRecon(Rec);
                    TransBankRecToGenJnl.Run();
                end;
            }
        }
        addafter("Transfer to General Journal_Promoted")
        {
            actionref(TransfertoGeneralJnlSymta; "Transfer to General Jnl Symta")
            {
            }
        }
        modify(MatchManually)
        {
            Visible = false;
        }
        addafter(MatchManually)
        {
            action(MatchManuallySymta)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Match Manually';
                Image = CheckRulesSyntax;
                ToolTip = 'Manually match selected lines in both panes to link each bank statement line to one or more related bank account ledger entries.';

                trigger OnAction()
                var
                    TempBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line" temporary;
                    TempBankAccountLedgerEntry: Record "Bank Account Ledger Entry" temporary;
                    MatchBankRecLines: Codeunit "Match Bank Rec. Lines";
                begin
                    CurrPage.StmtLine.PAGE.GetSelectedRecordsSymta(TempBankAccReconciliationLine);
                    CurrPage.ApplyBankLedgerEntries.PAGE.GetSelectedRecordsSymta(TempBankAccountLedgerEntry);
                    if ConfirmSelectedEntriesWithExternalMatchForModification(TempBankAccountLedgerEntry) then
                        MatchBankRecLines.MatchManually(TempBankAccReconciliationLine, TempBankAccountLedgerEntry);
                end;
            }
        }
        addafter(MatchManually_Promoted)
        {
            actionref(MatchManuallySymtaPromoted; MatchManuallySymta)
            {

            }
        }
        modify(RemoveMatch)
        {
            Visible = false;
        }
        addafter(RemoveMatch)
        {
            action(RemoveMatchSymta)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Remove Match';
                Image = RemoveContacts;
                ToolTip = 'Remove selection of matched bank statement lines.';

                trigger OnAction()
                var
                    TempBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line" temporary;
                    TempBankAccountLedgerEntry: Record "Bank Account Ledger Entry" temporary;
                    MatchBankRecLines: Codeunit "Match Bank Rec. Lines";
                begin
                    CurrPage.StmtLine.PAGE.GetSelectedRecordsSymta(TempBankAccReconciliationLine);
                    CurrPage.ApplyBankLedgerEntries.PAGE.GetSelectedRecordsSymta(TempBankAccountLedgerEntry);
                    if ConfirmSelectedEntriesWithExternalMatchForModification(TempBankAccountLedgerEntry) then
                        MatchBankRecLines.RemoveMatch(TempBankAccReconciliationLine, TempBankAccountLedgerEntry);
                end;
            }
        }
        addafter(RemoveMatch_Promoted)
        {
            actionref(RemoveMatchSymta_Promoted; RemoveMatchSymta)
            {

            }
        }
        modify(MatchDetails)
        {
            Visible = false;
        }
        addafter(MatchDetails)
        {
            action(MatchDetailsSymta)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Match Details';
                Image = ViewDetails;
                ToolTip = 'Show matching details about the selected bank statement line.';

                trigger OnAction()
                var
                    TempBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line" temporary;
                begin
                    CurrPage.StmtLine.PAGE.GetSelectedRecordsSymta(TempBankAccReconciliationLine);
                    if TempBankAccReconciliationLine."Applied Entries" > 0 then
                        Page.Run(Page::"Bank Rec. Line Match Details", TempBankAccReconciliationLine);
                end;
            }
        }
        addafter(MatchDetails_Promoted)
        {
            actionref(MatchDetailsSymta_Promoted; MatchDetailsSymta)
            {

            }
        }

    }

    local procedure ConfirmSelectedEntriesWithExternalMatchForModification(var TempBankAccountLedgerEntry: Record "Bank Account Ledger Entry" temporary): Boolean
    var
        ReturnValue: Boolean;
        ModifyBankAccLedgerEntriesForModificationQst: Label 'One or more of the selected entries have been matched on another bank account reconciliation.\\Do you want to continue?';

    begin
        TempBankAccountLedgerEntry.SetFilter("Statement No.", '<> %1 & <> ''''', Rec."Statement No.");
        if TempBankAccountLedgerEntry.IsEmpty() then
            ReturnValue := true
        else
            ReturnValue := Confirm(ModifyBankAccLedgerEntriesForModificationQst, false);
        // if ReturnValue then
        //     Session.LogMessage('0000JLM', '', Verbosity::Warning, DataClassification::SystemMetadata, TelemetryScope::ExtensionPublisher, 'Category', Rec.GetBankReconciliationTelemetryFeatureName());


        TempBankAccountLedgerEntry.SetRange("Statement No.");
        TempBankAccountLedgerEntry.FindSet();
        exit(ReturnValue);
    end;
}
