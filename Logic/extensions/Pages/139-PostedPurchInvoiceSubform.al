
pageextension 50268 "Posted Purch. Invoice Subform" extends "Posted Purch. Invoice Subform" //139
{
    layout
    {
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter("Return Reason Code")
        {
            field(GetReceiptNoName; GetReceiptNo())
            {
                Caption = 'N° Réception';
                Editable = false;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the N° Réception field.';
            }
        }
    }

    PROCEDURE GetReceiptNo(): Code[20];
    VAR
        lPostedWhseReceiptLine: Record "Posted Whse. Receipt Line";
    BEGIN
        //CFR le 19/11/2020 : R‚gie > Facture achat (Ajout nø r‚ception)
        IF (Rec.Type = Rec.Type::Item) AND (Rec."Receipt No." <> '') THEN BEGIN
            lPostedWhseReceiptLine.SETRANGE("Posted Source Document", lPostedWhseReceiptLine."Posted Source Document"::"Posted Receipt");
            lPostedWhseReceiptLine.SETRANGE("Posted Source No.", Rec."Receipt No.");
            IF lPostedWhseReceiptLine.FINDFIRST() THEN
                EXIT(lPostedWhseReceiptLine."Whse. Receipt No.");
        END
        ELSE
            EXIT('');
    END;
}