pageextension 50449 "Vendor List" extends "Vendor List" //27
{

    layout
    {

        addafter("Lead Time Calculation")
        {
            field("Groupe Tarif Fournisseur"; Rec."Groupe Tarif Fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Groupe Tarif Fournisseur field.';
            }
        }
        movebefore("Location Code"; "Responsibility Center")
        addafter("Balance Due (LCY)")
        {
            field("Code Appro"; Rec."Code Appro")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Appro field.';
            }
            field(Address; Rec.Address)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Address field.';
            }
            field("Address 2"; Rec."Address 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Address 2 field.';
            }
            field(City; Rec.City)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the City field.';
            }
            field("EORI Code"; Rec."EORI Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code EORI field.';
            }
        }
    }
    actions
    {

        addafter(History)
        {
            action("Extrait de compte")
            {
                Caption = 'Extrait de compte';
                Image = CustomerLedger;
                ApplicationArea = All;
                ToolTip = 'Executes the Extrait de compte action.';

                trigger OnAction();
                var
                    _Fournisseur: Record Vendor;
                    _extrait: Report "Extrait de compte fournisseur";
                begin

                    CLEAR(_Fournisseur);
                    CLEAR(_extrait);
                    _Fournisseur := Rec;
                    _Fournisseur.SETRECFILTER();
                    _extrait.SETTABLEVIEW(_Fournisseur);
                    _extrait.RUNMODAL();
                end;
            }
        }
    }

}

