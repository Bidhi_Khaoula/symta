pageextension 50809 "Purchase Order Archive" extends "Purchase Order Archive" //5167
{

    layout
    {
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
    }

}

