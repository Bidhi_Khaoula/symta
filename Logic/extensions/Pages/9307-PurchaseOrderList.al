pageextension 51688 "Purchase Order List" extends "Purchase Order List" //9307
{
    layout
    {
        addafter("No.")
        {
            field("Order Date"; Rec."Order Date")
            {
                Caption = 'Order Date';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
            field("Order Dispatch Date"; Rec."Order Dispatch Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date d''envoi commande field.';
            }
        }
        moveafter("Order Dispatch Date"; "Requested Receipt Date")
        addafter("Requested Receipt Date")
        {
            field("Promised Receipt Date"; Rec."Promised Receipt Date")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Promised Receipt Date field.';
            }
        }
        addafter("Buy-from Vendor Name")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Type field.';
            }
        }
        addafter("Assigned User ID")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
        modify("Vendor Order No.")
        {
            Visible = true;
        }
        moveafter("Job Queue Status"; "Vendor Order No.", Amount)
        addafter("Amount Including VAT")
        {
            field("Nb Lignes restantes"; Rec."Nb Lignes restantes")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nb Lignes restantes field.';
            }
            field(GetOutstandingAmount; Rec.GetOutstandingAmount())
            {
                Caption = 'Montant net à recevoir';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant net à recevoir field.';
            }
        }
    }
    actions
    {

        addafter(Print)
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    FrmQuid.InitClient(Rec."Sell-to Customer No.");
                    FrmQuid.RUNMODAL();
                end;
            }
        }
    }

}

