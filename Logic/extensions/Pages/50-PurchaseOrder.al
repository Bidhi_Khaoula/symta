pageextension 50647 "Purchase Order" extends "Purchase Order" //50
{
    layout
    {
        modify("Buy-from Vendor Name")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from Address")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from Address 2")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from Post Code")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from City")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from Contact No.")
        {
            QuickEntry = FALSE;
        }
        modify("Buy-from Contact")
        {
            QuickEntry = FALSE;
        }
        modify("Document Date")
        {
            Importance = Additional;
            QuickEntry = FALSE;
        }
        modify("Posting Date")
        {
            QuickEntry = FALSE;
        }
        modify("Vendor Invoice No.")
        {
            Importance = Additional;
            QuickEntry = FALSE;
        }
        modify("Purchaser Code")
        {
            QuickEntry = FALSE;
        }
        modify("No. of Archived Versions")
        {
            QuickEntry = FALSE;
        }
        modify("Order Date")
        {
            Caption = 'Order Date';
        }
        modify("Quote No.")
        {
            QuickEntry = FALSE;
        }
        modify("Vendor Order No.")
        {
            QuickEntry = TRUE;
        }
        modify("Vendor Shipment No.")
        {
            Importance = Additional;
            QuickEntry = FALSE;
        }
        modify("Responsibility Center")
        {
            QuickEntry = FALSE;
        }
        modify(Status)
        {
            QuickEntry = FALSE;
        }


        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Type field.';
            }
        }
        addafter("Buy-from Contact")
        {
            field("Requested Receipt Date2"; Rec."Requested Receipt Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Requested Receipt Date field.';
            }
            field("Promised Receipt Date2"; Rec."Promised Receipt Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Promised Receipt Date field.';
            }
        }
        addafter("Vendor Invoice No.")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
        addafter("Order Date")
        {
            field("Order Dispatch Date"; Rec."Order Dispatch Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date d''envoi commande field.';
            }
        }
        addafter(Status)
        {

            field("Seuil Franco"; rec_vendor."Seuil Franco")
            {
                Editable = false;
                QuickEntry = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Seuil Franco field.';
            }
        }
        addafter("Job Queue Status")
        {
            group("Description du travail")
            {
                Caption = 'Work Description';
                field(WorkDescription; WorkDescription)
                {
                    Importance = Additional;
                    MultiLine = true;
                    ShowCaption = false;
                    ToolTip = 'Specifies the products or service being offered';
                    ApplicationArea = All;

                    trigger OnValidate();
                    begin
                        Rec.SetWorkDescription(WorkDescription);
                    end;
                }
            }
        }
        addafter("Ship-to Contact")
        {
            field("Incoterm City"; Rec."Incoterm City")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ville Incoterm ICC 2020 field.';
            }
        }
    }
    actions
    {
        modify(SendCustom)
        {
            PromotedCategory = Report;
            Promoted = true;
        }


        addafter("O&rder")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin

                    CurrPage.PurchLines.PAGE.MultiConsultation();
                end;
            }
            action(LignesExcel)
            {
                Caption = 'Lignes Excel';
                Image = ExportToExcel;
                ToolTip = 'Export les lignes de la commande vers Excel';
                ApplicationArea = All;

                trigger OnAction();
                var
                    lPurchaseLine: Record "Purchase Line";
                    lExportCdeAchatPourtests: Report "Export Cde Achat Excel";
                begin
                    // CFR le 25/10/2023 - Régie : export des commandes achat vers Excel
                    lPurchaseLine.SETRANGE("Document Type", Rec."Document Type");
                    lPurchaseLine.SETRANGE("Document No.", Rec."No.");
                    lExportCdeAchatPourtests.SETTABLEVIEW(lPurchaseLine);
                    lExportCdeAchatPourtests.RUN();
                end;
            }
        }
        modify("Test Report")
        {
            trigger OnBeforeAction()
            var
                LSingleInstance: Codeunit SingleInstance;
            begin
                // MCO Le 18-01-2016
                LSingleInstance.SetPrintPurchEmail(FALSE);
                // FIN MCO Le 18-01-2016
            end;
        }
        addafter("F&unctions")
        {
            action(UpdateLineDirectUnitCost)
            {
                AccessByPermission = TableData "Vendor Invoice Disc." = R;
                ApplicationArea = Suite;
                Caption = 'Calculate &Invoice Discount';
                Image = UpdateUnitCost;
                ToolTip = 'Calculate the discount that can be granted based on all lines in the purchase document.';

                trigger OnAction();
                begin
                    // CFR le 10/05/2022 => Régie : UpdateLineDirectUnitCost()
                    Rec.UpdateLineDirectUnitCost();
                end;
            }
        }
        addafter("&Print")
        {
            action("Envoyer par EDI")
            {
                Promoted = true;
                PromotedCategory = "Report";
                ApplicationArea = All;
                ToolTip = 'Executes the Envoyer par EDI action.';

                trigger OnAction();
                begin

                    IF NOT CONFIRM(ESK001Qst) THEN
                        EXIT;

                    rec.ExporterEDI();
                end;
            }
            //todo
            // action("Envoyer par fax")
            // {
            //     Promoted = true;
            //     PromotedCategory = "Report";
            //     ApplicationArea = All;

            //     trigger OnAction();
            //     begin
            //         SendFaxMail('FAX')
            //     end;
            // }
        }
    }

    var
        rec_vendor: Record Vendor;
        // DocPrint: Codeunit "Document-Print";
        ESK001Qst: Label 'Export EDI de la commande ?';
        WorkDescription: Text;

    trigger OnOpenPage()
    begin
        // CFR le 14/09/2021 => R‚gie : affichage des commentaires … l'ouverture de la commande d'achat
        IF (Rec."Document Type" = Rec."Document Type"::Order) THEN
            Rec.ShowVendorComment()
    end;

    trigger OnAfterGetRecord()
    BEGIN
        IF rec_vendor.GET(Rec."Buy-from Vendor No.") THEN;  // MIG2015

        //CFR le 16/12/2020 - R‚gie : Description du travail
        WorkDescription := Rec.GetWorkDescription();
    END;

    // procedure SendFaxMail(_ptype: Code[10]);
    // var
    //     LDoc: Record "Purchase Header";
    //     LDocRef: RecordRef;
    //     LSendFax: Codeunit "Eskape Communication";
    //     cTFPostFunctions: Codeunit Codeunit52101147;
    // begin
    //     LDoc := Rec;
    //     LDoc.SETRECFILTER();
    //     LDocRef.GETTABLE(LDoc);

    //     //LM le 17-05-2013=>permet de modifier les options avant envoi
    //     DocPrint.PrintPurchHeader(Rec);

    //     //-TF
    //     cTFPostFunctions.AddDocumentToQueue(
    //       cTFPostFunctions.GetTableID(Rec.TABLENAME()), // Tablename
    //       Rec."No.",                                      // Document No.
    //       Rec."Posting Date",                             // Posting Date
    //       0,                                              // Entry No.
    //       "Document Type",                                // Document Type
    //       '',                                             // Identification ID
    //       "Business Case"                                 // Business Case
    //     );
    //     //+TF

    //     CASE _ptype OF
    //         'FAX':
    //             LSendFax.ReportToFax(LDocRef, 0, '');
    //         'MAIL':
    //             LSendFax.ReportToEmailPDF(LDocRef, 0, '');
    //         ELSE
    //             ERROR('Type Non Géré !');
    //     END;
    // end;
}

