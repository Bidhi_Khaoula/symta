pageextension 51696 "Purchase Line FactBox" extends "Purchase Line FactBox" //9100
{
    layout
    {

        addafter(PurchaseLineDiscounts)
        {
            field("Qté Cde Vte"; CduGFunctions.GetQtyItem('CDEVTE', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''CDEVTE'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnSalesOrder(Rec);
                end;
            }
            field("Qté Cde Ach"; CduGFunctions.GetQtyItem('CDEACH', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''CDEACH'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnPurchOrder(Rec);
                end;
            }
            field(Stock; CduGFunctions.GetQtyItem('PHYS', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''PHYS'', Rec) field.';
            }
            field("Stocké"; gItem.Stocké)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Stocké field.';
            }
            field("Date (Stocké)"; gItem."date maj stocké")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the date maj stocké field.';
            }
            field("Utilisateur (Stocké)"; gItem."user maj stocké")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the user maj stocké field.';
            }
        }
    }
    trigger OnAfterGetCurrRecord()
    begin
        // CFR le 16/04/2024 - R‚gie : ajout informations article
        CLEAR(gItem);
        IF (Rec.Type = Rec.Type::Item) AND gItem.GET(Rec."No.") THEN;
    end;

    var
        gItem: Record Item;
        CduGFunctions: Codeunit "Codeunits Functions";

}

