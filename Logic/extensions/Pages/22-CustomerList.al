pageextension 50336 "Customer List" extends "Customer List"  //22
{
    layout
    {

        addbefore("No.")
        {
            field("Centrale Active"; Rec."Centrale Active")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
            field("Groupement payeur"; Rec."Groupement payeur")
            {
                ApplicationArea = all;
                Visible = FALSE;
                ToolTip = 'Specifies the value of the Groupement payeur field.';
            }
        }
        modify(Blocked)
        {
            Visible = true;
        }
        movebefore("Centrale Active"; Blocked)
        modify("No.")
        {
            StyleExpr = gStyleExp_Export;
        }
        addafter("No.")
        {
            field("Code Groupement RFA"; Rec."Code Groupement RFA")
            {
                ApplicationArea = all;
                StyleExpr = gStyleExp_Export;
                ToolTip = 'Specifies the value of the Code Groupement RFA field.';
            }
        }
        modify("Responsibility Center")
        {
            Visible = false;
        }
        modify("Location Code")
        {
            Visible = false;
        }
        addafter("Country/Region Code")
        {
            field(City; Rec.City)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the City field.';
            }
            field("Code postal Expédition"; Rec."Code postal Expédition")
            {
                Visible = false;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Post Code field.';
            }
            field("Ville Expédition"; Rec."Ville Expédition")
            {
                ApplicationArea = all;
                Visible = false;
                ToolTip = 'Specifies the value of the City field.';
            }
        }
        addafter("Phone No.")
        {
            field("E-Mail"; Rec."E-Mail")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Email field.';
            }
        }
        modify(Contact)
        {
            Visible = false;
        }
        modify("Customer Disc. Group")
        {
            Visible = true;
        }
        modify("Payment Terms Code")
        {
            Visible = true;
        }
        modify("Shipping Agent Code")
        {
            Visible = true;
        }
        addafter("Base Calendar Code")
        {
            field("Shipment Method Code"; Rec."Shipment Method Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Payment Method Code"; Rec."Payment Method Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Payment Method Code field.';
            }
            field("Login WEB"; Rec."Login WEB")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Login WEB field.';
            }
            field("Mot de passe WEB"; Rec."Mot de passe WEB")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Mot de passe WEB field.';
            }
            field("Mobile Phone No."; Rec."Mobile Phone No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Mobile Phone No. field.';
            }
            field(Address; Rec.Address)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Address field.';
            }
            field("Fax No."; Rec."Fax No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Fax No. field.';
            }
            field("Address 2"; Rec."Address 2")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Address 2 field.';
            }
            field("Invoice Disc. Code"; Rec."Invoice Disc. Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Invoice Disc. Code field.';
            }
        }
        moveafter("Shipment Method Code"; "Payment Terms Code")
        addafter("Sales (LCY)")
        {
            field("Bloquer en commande"; Rec."Bloquer en commande")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Bloquer en commande field.';
            }
            field(Holding; Rec.Holding)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Holding field.';
            }

            field("EORI Code"; Rec."EORI Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code EORI field.';
            }
            field("Primary Contact No."; Rec."Primary Contact No.")
            {
                ApplicationArea = all;
                Visible = false;
                ToolTip = 'Specifies the value of the Primary Contact No. field.';
            }
        }
        addafter("Salesperson Code")
        {
            field("Code Enseigne 1"; Rec."Code Enseigne 1")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
            }
            field("Code Enseigne 2"; Rec."Code Enseigne 2")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
            }
        }
        addlast(factboxes)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = all;
            }
        }
    }

    actions
    {
        addafter(CustomerLedgerEntries)
        {
            action("Extrait de compte")
            {
                ApplicationArea = All;
                Caption = 'Extrait de compte';
                Promoted = true;
                Image = CustomerLedger;
                ToolTip = 'Executes the Extrait de compte action.';
                trigger OnAction()
                VAR
                    _client: Record Customer;
                    _extrait: Report "Extrait de compte client";
                BEGIN

                    CLEAR(_client);
                    CLEAR(_extrait);
                    _client := Rec;
                    _client.SETRECFILTER();
                    _extrait.SETTABLEVIEW(_client);
                    _extrait.RUNMODAL();
                END;
            }
        }
        modify(Sales_InvoiceDiscounts)
        {
            Caption = 'Invoice &Discounts';
        }
        modify(Sales_LineDiscounts)
        {
            Visible = false;
        }
        addafter(Sales_LineDiscounts)
        {
            action(Sales_LineDiscounts2)
            {
                ApplicationArea = Advanced;
                Caption = 'Line Discounts';
                Image = LineDiscount;
                ToolTip = 'View or set up different discounts for items that you sell to the customer. An item discount is automatically granted on invoice lines when the specified criteria are met, such as customer, quantity, or ending date.';
                Visible = not ExtendedPriceEnabled2;
                ObsoleteState = Pending;
                ObsoleteReason = 'Replaced by the new implementation (V16) of price calculation.';
                ObsoleteTag = '18.0';

                trigger OnAction()
                var
                    SalesLineDiscount: Record "Sales Line Discount";
                begin
                    SalesLineDiscount.SetCurrentKey("Sales Type", "Sales Code");
                    SalesLineDiscount.SetRange("Sales Type", SalesLineDiscount."Sales Type"::"Customer Disc. Group");
                    SalesLineDiscount.SetRange("Sales Code", Rec."Customer Disc. Group");
                    Page.Run(Page::"Sales Line Discounts", SalesLineDiscount);
                end;
            }
        }
        modify("ReportCustomerDetailTrial")
        {
            visible = false;
        }
        addafter(ReportCustomerDetailTrial)
        {
            action(ReportCustomerDetailTrial2)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Customer - Detail Trial Bal.';
                Image = "Report";
                RunObject = Report "Customer Detail Trial Balance";
                ToolTip = 'View the balance for customers with balances on a specified date. The report can be used at the close of an accounting period, for example, or for an audit.';
            }
        }
        modify(ReportCustomerTrialBalance)
        {
            Visible = false;
        }
        addafter(ReportCustomerTrialBalance)
        {
            action(ReportCustomerTrialBalance2)
            {
                ApplicationArea = Suite;
                Caption = 'Customer - Trial Balance';
                Image = "Report";
                RunObject = Report "Customer Trial Balance FR";
                ToolTip = 'View the beginning and ending balance for customers with entries within a specified period. The report can be used to verify that the balance for a customer posting group is equal to the balance on the corresponding general ledger account on a certain date.';
            }
            action("Customer - Payment Receipt")
            {
                ApplicationArea = All;
                Caption = 'Customer - Payment Receipt';
                RunObject = Report "Customer - Payment Receipt";
                Promoted = true;
                Image = Report;
                PromotedCategory = Report;
                ToolTip = 'Executes the Customer - Payment Receipt action.';
            }
        }
    }
    var
        PriceCalculationMgt: Codeunit "Price Calculation Mgt.";
        ExtendedPriceEnabled2: Boolean;
        gStyleExp_Export: Text;

    trigger OnOpenPage()
    begin
        // MIG2015
        rec.SETRANGE(Supprimé, FALSE);
        ExtendedPriceEnabled2 := PriceCalculationMgt.IsExtendedPriceCalculationEnabled();
        // CFR le 10/03/2021 - SFD20210201 historique centrale active
        rec.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
        rec.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());

    end;

    trigger OnAfterGetRecord()
    begin
        // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
        SetStyleExp_Export();
        // FIN CFR le 05/04/2023
    end;

    LOCAL PROCEDURE SetStyleExp_Export();
    BEGIN
        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        IF (Rec."Salesperson Code" = '19') THEN
            gStyleExp_Export := 'Unfavorable';
        //FIN CFR le 05/04/2023
    END;
}