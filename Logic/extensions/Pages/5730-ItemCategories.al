pageextension 50951 "Item Categories" extends "Item Categories" //5730
{
    layout
    {
        addafter(Description)
        {
            field("Exclude Express Discount"; Rec."Exclude Express Discount")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Exclure remise express field.';
            }
        }
    }
}