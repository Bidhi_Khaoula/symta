pageextension 50002 "Salespersons/Purchasers" extends "Salespersons/Purchasers" //14
{
    layout
    {
        addafter("Phone No.")
        {
            field("Export Salesperson"; Rec."Export Salesperson")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Export Salesperson field.';
            }
        }
    }
}