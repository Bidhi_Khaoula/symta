pageextension 50822 "Requisition Lines" extends "Requisition Lines" //517
{
    layout
    {
        addafter("Due Date")
        {
            field("Code Appro"; Rec."Code Appro")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Appro field.';
            }
        }
    }
}