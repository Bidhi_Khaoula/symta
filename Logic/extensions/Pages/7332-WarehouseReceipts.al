pageextension 51387 "Warehouse Receipts" extends "Warehouse Receipts" //7332
{

    layout
    {
        addafter("Assignment Date")
        {
            field("First Line Origin No."; Rec."First Line Origin No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° Origine Première Ligne field.';
            }
            field(Fournisseur; Rec.GetVendorNo())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetVendorNo() field.';
            }
            field(Nom; Rec.GetVendorName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetVendorName() field.';
            }
            field("Reçu"; recu)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the recu field.';
            }
            field("A Reçevoir"; a_recevoir)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the a_recevoir field.';
            }
            field("Vendor Shipment No."; Rec."Vendor Shipment No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
            }
            field("Vendor Type No."; Rec."Vendor Type No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type n° doc. fournisseur field.';
            }
            field(Information; Rec.Information)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Information field.';
            }
            field(Position; Rec.Position)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Position field.';
            }
        }
    }
    actions
    {
    }

    var

        a_recevoir: Decimal;
        recu: Decimal;

    trigger OnAfterGetRecord()
    begin
        jauge(Rec."No.", a_recevoir, recu);
    end;

    procedure jauge(_pNoFusion: Code[20]; var _pNbARecevoir: Decimal; var _pNbRecus: Decimal);
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
        TempMontantArticle: Record "Item Amount" temporary;
    begin
        // Cherche combien d'article a recevoir et conbien de recus
        TempMontantArticle.RESET();
        TempMontantArticle.DELETEALL();
        // Cherche le nb à recevoir
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        IF WhseReceiptLine.FindSet() THEN
            REPEAT
                TempMontantArticle.RESET();
                TempMontantArticle.SETRANGE("Item No.", WhseReceiptLine."Item No.");
                IF TempMontantArticle.FINDFIRST() THEN
                    TempMontantArticle.RENAME(TempMontantArticle.Amount + WhseReceiptLine.Quantity, TempMontantArticle."Amount 2", TempMontantArticle."Item No.")
                ELSE BEGIN
                    TempMontantArticle.INIT();
                    TempMontantArticle.Amount := WhseReceiptLine."Qty. Outstanding (Base)";
                    TempMontantArticle."Amount 2" := 0;
                    TempMontantArticle."Item No." := WhseReceiptLine."Item No.";
                    TempMontantArticle.INSERT(TRUE);
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Cherche le nb à de recus complets
        TempMontantArticle.RESET();
        _pNbARecevoir := TempMontantArticle.COUNT;
        _pNbRecus := 0;
        IF TempMontantArticle.FindSet() THEN
            REPEAT
                "Pré-reception".RESET();
                "Pré-reception".SETCURRENTKEY("No.", "Item No.");
                "Pré-reception".SETRANGE("No.", _pNoFusion);
                "Pré-reception".SETRANGE("Item No.", TempMontantArticle."Item No.");
                "Pré-reception".CALCSUMS(Quantity);
                IF "Pré-reception".Quantity >= TempMontantArticle.Amount THEN
                    _pNbRecus += 1;
            UNTIL TempMontantArticle.NEXT() = 0;
    end;

}

