pageextension 50597 "Sales Credit Memo" extends "Sales Credit Memo" //44
{

    layout
    {
        modify("Sell-to Customer No.")
        {
            Visible = false;
        }
        addafter("No.")
        {
            field("Sell-to Customer No.2"; Rec."Sell-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
            }
        }

    }
    actions
    {
        modify(Post)
        {
            trigger OnBeforeAction()
            var
                Text000Qst: Label 'La date de comptabilisation est au %1. Voulez-vous valider ce document ?', Comment = '%1 posting date';
                Text001Msg: Label 'Annulé';
            begin
                // MCO Le 21-11-2018 => R‚gie
                IF Rec."Posting Date" <> WORKDATE() THEN
                    IF NOT CONFIRM(STRSUBSTNO(Text000Qst, Rec."Posting Date")) THEN ERROR(Text001Msg);
                // FIN MCO Le 21-11-2018 => R‚gie
            end;
        }
    }

}

