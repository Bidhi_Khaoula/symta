pageextension 50964 "Warehouse Setup" extends "Warehouse Setup" //5775
{
    layout
    {
        addafter("Registered Whse. Movement Nos.")
        {
            group("Packing List")
            {
                Caption = 'Numbering';
                field("Packing List Nos."; Rec."Packing List Nos.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
                field("Item PL Report No."; Rec."Item PL Report No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° de report liste de colisage par article field.';
                }
                field("Package PL Report No."; Rec."Package PL Report No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° de report liste de colisage par colis field.';
                }
            }
        }
    }
}

