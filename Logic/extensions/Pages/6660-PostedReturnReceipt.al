pageextension 51327 "Posted Return Receipt" extends "Posted Return Receipt" //6660
{

    layout
    {
        addbefore("No.")
        {
            field("Type retour"; Rec."Type retour")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type retour field.';
            }
        }
        addafter("Posting Date")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
        }
        addafter("No. Printed")
        {
            field("User ID"; Rec."User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the User ID field.';
            }
        }
        addafter("Shortcut Dimension 2 Code")
        {
            field("En attente de facturation"; Rec."En attente de facturation")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the En attente de facturation field.';
            }
            field("Entierement facturé"; Rec."Entierement facturé")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Entierement facturé field.';
            }
            field("Regroupement Centrale"; Rec."Regroupement Centrale")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Regroupement Centrale field.';
            }
        }
        addafter("Shipment Date")
        {
            group(Billing2)
            {
                Caption = 'Billing2';
                Editable = true;
                field("<En attente de facturation>2"; Rec."En attente de facturation")
                {
                    Caption = 'En attente de facturation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the En attente de facturation field.';
                }
                field("User En Attente"; Rec."User En Attente")
                {
                    Caption = 'Utilisateur mise en attente';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur mise en attente field.';
                }
            }
        }
    }
    actions
    {
        addbefore("&Return Rcpt.")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin
                    CurrPage.ReturnRcptLines.PAGE.MultiConsultation();
                end;
            }
        }
        addafter("&Print")
        {
            action("&E-mail")
            {
                Caption = '&Email';
                Image = Email;
                ApplicationArea = All;
                AccessByPermission = TableData "Return Receipt Header" = rimd;
                ToolTip = 'Executes the &Email action.';

                trigger OnAction();
                var
                    ReturnReceiptHeader: Record "Return Receipt Header";
                begin
                    ReturnReceiptHeader := Rec;
                    CurrPage.SETSELECTIONFILTER(ReturnReceiptHeader);
                    ReturnReceiptHeader.EmailRecords(TRUE);
                end;
            }
            action("Envoyer Par Fax")
            {
                Caption = 'Envoyer Par Fax';
                Image = PrintCheck;
                Promoted = true;
                PromotedCategory = "Report";
                ApplicationArea = All;
                ToolTip = 'Executes the Envoyer Par Fax action.';

                trigger OnAction();
                begin
                    sendfaxmail('FAX')
                end;
            }
        }
        addafter("&Navigate")
        {
            action(Retour)
            {
                Image = ReturnOrder;
                Promoted = true;
                PromotedCategory = "Report";
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Retour action.';

                trigger OnAction();
                begin
                    Rec.ShowOrder();
                end;
            }

        }
    }

    procedure sendfaxmail(_pType: Code[10]);
    var
        LDoc: Record "Return Receipt Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    end;


}

