pageextension 50785 "Employee List" extends "Employee List" //5201
{
    layout
    {
        addafter("Last Name")
        {
            field(Gender; Rec.Gender)
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gender field.';
            }

        }
        modify("No.")
        {
            ToolTip = '';
        }
        modify(FullName)
        {
            Visible = false;
            ToolTip = '';
        }
        modify("First Name")
        {
            Visible = true;
            ToolTip = '';
        }
        modify("Middle Name")
        {
            ToolTip = '';
        }
        modify("Last Name")
        {
            ToolTip = '';
        }
        modify(Initials)
        {
            ToolTip = '';
        }
        modify("Job Title")
        {
            ToolTip = '';
        }
        modify("Country/Region Code")
        {
            ToolTip = '';
        }
        modify(Extension)
        {
            ToolTip = '';
        }
        modify("Phone No.")
        {
            ToolTip = '';
        }
        modify("Mobile Phone No.")
        {
            ToolTip = '';
        }
        modify("E-Mail")
        {
            ToolTip = '';
        }
        modify("Statistics Group Code")
        {
            ToolTip = '';
        }
        modify("Resource No.")
        {
            ToolTip = '';
        }
        modify("Search Name")
        {
            ToolTip = '';
        }
        modify(Comment)
        {
            ToolTip = '';
        }
        addafter("Job Title")
        {
            field(Address; Rec.Address)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Address field.';
            }
            field("Address 2"; Rec."Address 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Address 2 field.';
            }
            field(City; Rec.City)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the City field.';
            }
        }
        modify("Post Code")
        {
            Visible = true;
        }
        moveafter("Address 2"; "Post Code")
        addafter("Country/Region Code")
        {
            field("Social Security No."; Rec."Social Security No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Social Security No. field.';
            }
            field("Birth Date"; Rec."Birth Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Birth Date field.';
            }
        }

        addafter(Comment)
        {
            field("Termination Date"; Rec."Termination Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Termination Date field.';
            }
            field("Employment Date"; Rec."Employment Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Employment Date field.';
            }

        }
    }
    actions
    {
        modify("Absence Registration")
        {
            Visible = false;
        }
        modify("Dimensions-&Multiple")
        {
            Visible = false;
            Caption = 'Photo';
        }
        modify("&Relatives")
        {
            Caption = '&Relatives';
        }

        addafter("Mi&sc. Article Information")
        {
            action("Abs&ences par catégorie")
            {
                Caption = 'Absences by Ca&tegories';
                Image = AbsenceCategory;
                RunObject = Page "Empl. Absences by Categories";
                RunPageLink = "No." = FIELD("No."), "Employee No. Filter" = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Absences by Ca&tegories action.';
            }
        }

        addafter("Con&fidential Info. Overview")
        {
            action("Online Map")
            {
                Caption = 'Online Map';
                Image = Map;
                ApplicationArea = All;
                ToolTip = 'Executes the Online Map action.';

                trigger OnAction();
                begin
                    Rec.DisplayMap();
                end;
            }

        }
    }
}

