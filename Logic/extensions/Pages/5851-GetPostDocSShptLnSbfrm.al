pageextension 51066 "Get Post.Doc - S.ShptLn Sbfrm" extends "Get Post.Doc - S.ShptLn Sbfrm" //5851
{

    layout
    {
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter(QtyReturned)
        {
            field("Gerer par groupement"; Rec."Gerer par groupement")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gerer par groupement field.';
            }
            field("Centrale Active"; Rec."Centrale Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
        }
        addafter("Appl.-to Item Entry")
        {
            field("Qte Extraire sur Retour"; Rec."Qte Extraire sur Retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qte Extraire sur Retour field.';
            }
        }
    }
    Trigger OnOpenPage()
    BEGIN
        Rec.SetCurrentKey("Document No.", "Line No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    END;
}

