pageextension 51340 "Posted Whse. Shipment" extends "Posted Whse. Shipment" //7337
{

    actions
    {
        addafter("Co&mments")
        {
            group("Packing List")
            {
                Caption = '&Shipment';
                Image = Shipment;
                action("Afficher document")
                {
                    Caption = 'Afficher document';
                    Enabled = hasPACKING;
                    Image = CreateWarehousePick;
                    RunObject = Page "Packing List Header";
                    RunPageLink = "No." = FIELD("Packing List No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Afficher document action.';
                }
                action("Imprimer par article")
                {
                    Caption = 'Imprimer par article';
                    Enabled = hasPACKING;
                    Image = PrintCheck;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer par article action.';

                    trigger OnAction();
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."Packing List No.", TRUE, 'ARTICLE');
                    end;
                }
                action("Imprimer par colis")
                {
                    Caption = 'Imprimer par colis';
                    Enabled = HasPACKING;
                    Image = PrintCover;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer par colis action.';

                    trigger OnAction();
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."Packing List No.", TRUE, 'COLIS');
                    end;
                }
            }
        }
    }

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";
        hasPACKING: Boolean;


}

