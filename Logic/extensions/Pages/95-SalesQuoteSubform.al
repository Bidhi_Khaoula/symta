pageextension 51207 "Sales Quote Subform" extends "Sales Quote Subform" //95
{
    layout
    {
        modify("Line Discount %")
        {
            Editable = false;
        }
        addbefore(Type)
        {
            field("Line No."; Rec."Line No.")
            {
                ApplicationArea = all;
                QuickEntry = FALSE;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
            field("Line type"; Rec."Line type")
            {
                ApplicationArea = all;
                QuickEntry = FALSE;
                ToolTip = 'Specifies the value of the Type ligne field.';
            }
        }
        addafter(Type)
        {
            field("Has Comment"; Rec."Has Comment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Commentaire field.';
            }
        }
        modify(Type)
        {
            QuickEntry = FALSE;
        }
        addafter(Type)
        {
            field(Urgent; Rec.Urgent)
            {
                QuickEntry = False;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Urgent field.';
                trigger OnValidate()
                BEGIN

                    // CFR le 18/11/2020 : Régie > remises express
                    // s'il est possible d'appliquer une remise express
                    IF Rec.ExpressDiscount(gRemiseOrder, gRemiseExpress, gCoefficient, FALSE) THEN BEGIN
                        // si le coefficient est OK
                        IF (gCoefficient >= 1) THEN BEGIN
                            IF (Rec.Urgent) THEN BEGIN
                                // on pose la question pour appliquer le coefficient
                                IF CONFIRM(STRSUBSTNO(gExpressConfirm001Qst, gRemiseOrder, gRemiseExpress), TRUE) THEN BEGIN
                                    Rec.VALIDATE("Discount1 %", gRemiseExpress);
                                    Rec.VALIDATE("Requested Delivery Date", CALCDATE('<3J>', Rec."Requested Delivery Date"));
                                END
                                ELSE
                                    ERROR('');
                            END
                            ELSE BEGIN
                                Rec.VALIDATE("Discount1 %", gRemiseOrder);
                                Rec.VALIDATE("Requested Delivery Date", CALCDATE('<-3J>', Rec."Requested Delivery Date"));
                            END;
                        END
                        // si le coefficient est KO
                        ELSE
                            IF (Rec.Urgent) THEN BEGIN
                                MESSAGE(gExpressInfo001Msg, gCoefficient);
                                ERROR('');
                            END;
                    END
                    // s'il n'est PAS possible d'appliquer une remise express
                    ELSE
                        ERROR('');

                END;


            }
            field(Kit; rec.Kit)
            {
                ApplicationArea = all;
                StyleExpr = gStyleExp_Kit;
                QuickEntry = FALSE;
                ToolTip = 'Specifies the value of the Kit field.';
                trigger OnDrillDown()
                VAR
                    lBOMComponent: Record "BOM Component";
                    lPageAssemblyBOM: Page "Assembly BOM";
                BEGIN

                    // CFR le 22/09/2021 => Régie - Bloquer la modification des nomenclatures d'assemblage
                    IF (rec.Kit) THEN BEGIN
                        CLEAR(lBOMComponent);
                        lBOMComponent.SETRANGE("Parent Item No.", Rec."No.");
                        lPageAssemblyBOM.SETTABLEVIEW(lBOMComponent);
                        lPageAssemblyBOM.EDITABLE(FALSE);
                        lPageAssemblyBOM.RUNMODAL();
                    END;
                END;
            }
            field("Multiple de Vente"; Rec.GetSalesMultiple())
            {
                ApplicationArea = all;
                Caption = 'Multiple de Vente';
                DecimalPlaces = 0 : 2;
                BlankNumbers = BlankZero;
                QuickEntry = FALSE;
                ToolTip = 'Specifies the value of the Multiple de Vente field.';
            }
            field("Bin Code"; Rec."Bin Code")
            {
                QuickEntry = false;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Bin Code field.';
            }
            field("Référence saisie"; Rec."Référence saisie")
            {
                Editable = FALSE;
                QuickEntry = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Référence saisie field.';
            }
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                StyleExpr = gStyleExp_NonDispo;
                ToolTip = 'Specifies the value of the Recherche référence field.';
                Trigger OnValidate()
                BEGIN
                    Rec.ShowShortcutDimCode(ShortcutDimCode);
                    RechercheRéfOnAfterValidate();
                END;
            }
        }
        moveafter("Recherche référence"; Quantity)
        addafter(Quantity)
        {
            field("Qty. to Ship"; rec."Qty. to Ship")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Qty. to Ship field.';
            }
        }
        moveafter(Description; "Unit Price")
        addafter("Unit Price")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                QuickEntry = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise1 field.';
                trigger OnValidate()
                BEGIN
                    RedistributeTotalsOnAfterValidate();
                END;
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                QuickEntry = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise2 field.';
                trigger OnValidate()
                BEGIN
                    RedistributeTotalsOnAfterValidate();
                END;
            }
            field("Net Unit Price"; Rec."Net Unit Price")
            {
                QuickEntry = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Prix unitaire net field.';
            }
        }
        moveafter("Net Unit Price"; "Line Amount")
        addafter("Line Amount")
        {
            field("Requested Delivery Date"; Rec."Requested Delivery Date")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Requested Delivery Date field.';
            }
        }
        modify("No.")
        {
            Visible = false;
        }
        moveafter("No."; "Line Discount %")
        modify("Substitution Available")
        {
            StyleExpr = gStyleExp_Substitution;
            Trigger OnDrillDown()
            BEGIN
                // AD Le 23-03-2016
                rec.ShowItemSub();
                CurrPage.UPDATE();
            END;
        }
        addafter(Description)
        {
            field(GetItemRecommendedShippingAgentField; GetItemRecommendedShippingAgent())
            {
                Caption = 'Transporteur préconisé';
                StyleExpr = gStyleExp_Transporteur;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur préconisé field.';
            }
            field(GetItemMandatoryShippingAgentField; GetItemMandatoryShippingAgent())
            {
                Caption = 'Transporteur obligatoire';
                StyleExpr = gStyleExp_Transporteur;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur obligatoire field.';
            }
        }
        addafter("Unit Price")
        {
            field("Marge %"; Rec."Marge %")
            {
                Visible = false;
                StyleExpr = gStyleExp_Marge;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Marge / coût unitaire field.';
            }
        }
        addafter("Substitution Available")
        {
            field("Additional Available"; Rec."Additional Available")
            {
                ToolTip = 'Spécifie que des articles complémentaires sont disponibles pour l''article sur la ligne vente.';
                ApplicationArea = Suite;
                StyleExpr = gStyleExp_Additional;
                Trigger OnDrillDown()
                VAR
                    // lAdditionalItem: Record "Additional Item";
                    // lAdditionalItemList: Page "Additional Item List";
                    lSalesHeader: Record "Sales Header";
                    CduLFunctions: Codeunit "Codeunits Functions";
                BEGIN
                    IF NOT Rec."Additional Available" THEN
                        ERROR('Il n''existe pas d''article associé pour l''article n°%1', Rec."No.");

                    // CFR le 10/03/2021 - SFD20210201 articles compl‚mentaires
                    lSalesHeader.GET(Rec."Document Type", Rec."Document No.");
                    IF CduLFunctions.LookupAdditional(0, Rec) THEN
                        CurrPage.UPDATE();

                    /*{
                    CLEAR(lAdditionalItem);
                    lAdditionalItem.SETRANGE("Item No.", Rec."No.");

                    CLEAR(lAdditionalItemList);
                    lAdditionalItemList.SETTABLEVIEW(lAdditionalItem);
                    lAdditionalItemList.LOOKUPMODE := TRUE;
                    lAdditionalItemList.EDITABLE(FALSE);
                    lAdditionalItemList.RUN();
                    }*/
                END;
            }
        }
        modify(Description)
        {
            QuickEntry = false;
            StyleExpr = gStyleExp_Transporteur;
        }
        modify("Location Code")
        {
            Visible = false;
        }
        modify("Unit of Measure Code")
        {
            Visible = false;
        }
        modify("Qty. to Assign")
        {
            Visible = false;
        }
        modify("Qty. Assigned")
        {
            Visible = false;
        }
        addafter(ShortcutDimCode8)
        {
            field("Zone Code"; Rec."Zone Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code zone field.';
            }
            field(CtrlHasItemLink; Rec.HasItemLink())
            {
                ApplicationArea = all;
                Caption = 'Lien article';
                ToolTip = 'Specifies the value of the Lien article field.';
            }
        }
        modify("Subtotal Excl. VAT")
        {
            Visible = false;
        }
        modify("Invoice Discount Amount")
        {
            Visible = false;
        }
        modify("Invoice Disc. Pct.")
        {
            visible = false;
        }
        addafter("Invoice Disc. Pct.")
        {
            field("Invoice Gross Weight"; TotalSalesLine."Gross Weight")
            {
                AutoFormatType = 1;
                Editable = FALSE;
                Style = Subordinate;
                Caption = 'Invoice Discount Amount';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Invoice Discount Amount field.';
                trigger OnValidate()
                VAR
                    SalesHeader: Record "Sales Header";
                    SalesCalcDiscByType: Codeunit "Sales - Calc Discount By Type";
                BEGIN
                    SalesHeader.GET(Rec."Document Type", Rec."Document No.");
                    SalesCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalSalesLine."Inv. Discount Amount", SalesHeader);
                    CurrPage.UPDATE(FALSE);
                end;
            }
        }
        modify("Total VAT Amount")
        {
            Visible = false;
        }
        modify("Total Amount Incl. VAT")
        {
            Visible = false;
        }
    }

    actions
    {
        addafter(InsertExtTexts)
        {
            action("E&xplode BOM2")
            {
                AccessByPermission = TableData "BOM Component" = R;
                Caption = 'E&xplode BOM';
                ApplicationArea = All;
                Image = ExplodeBOM;
                ToolTip = 'Executes the E&xplode BOM action.';
                trigger OnAction()
                begin
                    ExplodeBOM();
                end;
            }
        }
        addafter(Dimensions)
        {
            action("Co&mments2")
            {
                Caption = 'Co&mments';
                ApplicationArea = All;
                Image = ViewComments;
                ToolTip = 'Executes the Co&mments action.';
                trigger OnAction()
                begin
                    rec.ShowLineComments();
                end;
            }
        }


    }
    var
        gStyleExp_Transporteur: Text;
        gStyleExp_Marge: Text;
        gStyleExp_Additional: Text;
        StyleSalesMultiple: Boolean;
        gStyleExp_NonDispo: Text;
        gStyleExp_Kit: Text;
        gStyleExp_Substitution: Text;
        gEditableUrgent: Boolean;
        gRemiseExpress: Decimal;
        gRemiseOrder: Decimal;
        gCoefficient: Decimal;
        gExpressInfo001Msg: Label 'Pas de remise express par défaut car le coefficient est inférieur à 1 (%1)', Comment = '%1 Coefficient';
        gExpressConfirm001Qst: Label 'Souhaitez vous modifier la remise de %1 à %2 ?', Comment = '%1 %2';

    trigger OnAfterGetRecord()
    var
        lItem: Record Item;
    begin
        FctSalesMultipleColor();// AD Le 29-12-2015

        // CFR le 18/11/2020 : Régie > Visibilité stock non disponible
        IF (Rec."Document Type" = Rec."Document Type"::Quote) OR (Rec."Document Type" = Rec."Document Type"::Order) THEN
            IF (Rec.Quantity <> Rec."Qty. to Ship") THEN
                gStyleExp_NonDispo := 'Attention'
            ELSE
                gStyleExp_NonDispo := '';


        IF (Rec."Document Type" = Rec."Document Type"::Quote) OR (Rec."Document Type" = Rec."Document Type"::Order) THEN BEGIN
            IF (Rec.Kit) THEN
                gStyleExp_Kit := 'Unfavorable'
            ELSE
                gStyleExp_Kit := '';

            IF (Rec."Substitution Available") THEN
                gStyleExp_Substitution := 'Unfavorable'
            ELSE
                gStyleExp_Substitution := '';
            //CFR le 24/09/2021 => SFD20210201 articles complémentaires
            IF (Rec."Additional Available") THEN
                gStyleExp_Additional := 'Unfavorable'
            ELSE
                gStyleExp_Additional := '';
        END;
        // Fin CFR le 18/11/2020
        //CFR le 06/04/2022 => Régie : [Couleur transporteur préconisé]
        gStyleExp_Transporteur := '';
        IF (Rec.Type = Rec.Type::Item) AND (Rec."No." <> '') AND (lItem.GET(Rec."No.")) THEN
            gStyleExp_Transporteur := lItem.GetRecommendedColor();
        //FIN CFR le 06/04/2022

        // CFR le 18/11/2020 : Régie > remises express
        gEditableUrgent := Rec.ExpressDiscount(gRemiseExpress, gRemiseOrder, gCoefficient, FALSE);
        // Fin CFR le 18/11/2020
        //CFR le 04/10/2023 - Régie ajout du calcul de marge
        gStyleExp_Marge := '';
        IF Rec."Marge %" < 0 THEN
            gStyleExp_Marge := 'Unfavorable';
        //FIN CFR le 04/10/2023
    end;

    PROCEDURE InsertItemCharge();
    VAR
        TransferItemCharge: Codeunit "Transfer Item Charge";
    BEGIN
        // AD Le 30-10-2006 => DEEE -> Insertion des charges associées
        CurrPage.SAVERECORD();
        IF TransferItemCharge.SalesCheckIfAnyLink(Rec) THEN
            TransferItemCharge.InsertSalesLink(Rec);

        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    END;

    PROCEDURE UpdateItemCharge();
    VAR
        TransferItemCharge: Codeunit "Transfer Item Charge";
    BEGIN
        // AD Le 30-10-2006 => DEEE -> Modification des qtes de charges associées

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateQtySalesLink(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    END;

    PROCEDURE UpdateLineType();
    VAR
        TransferItemCharge: Codeunit "Transfer Item Charge";
    BEGIN
        // AD Le 30-10-2006 => DEEE -> Modification du type de ligne

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateLineType(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    END;

    PROCEDURE DetailLigne();
    BEGIN
        // AD Le 12-12-2008 => Ouverture de l'écran détaillé

        IF (Rec.Type = Rec.Type::" ") THEN
            PAGE.RUNMODAL(PAGE::"Sales Line Détail Comment", Rec)
        ELSE
            PAGE.RUNMODAL(PAGE::"Sales Line Détail", Rec);
    END;

    PROCEDURE InsertPort();
    VAR
        Type: option Port,Emballage;
    BEGIN
        // AD Le 31-08-2009 => Gestion des frais de port
        rec.InsertPortLine(Type::Port);
    END;

    PROCEDURE GetGencod(): Text[13];
    VAR
        Rec_Item: Record item;
    BEGIN
        IF Rec_Item.GET(Rec."No.") THEN
            EXIT(Rec_Item."Gencod EAN13")
        ELSE
            EXIT('');
    END;

    PROCEDURE MultiConsultation();
    BEGIN
        Rec.OuvrirMultiConsultation();
    END;

    LOCAL PROCEDURE RechercheRéfOnAfterValidate();
    VAR
        rec_item2: Record Item;
        rec_param_gen: Record "Generals Parameters";
        rec_itemunitmes: Record "Item Unit of Measure";
    BEGIN
        IF
        (Rec."Item Category Code" = 'G') AND
        (rec."Shipping Agent Code" = 'CH') THEN
            MESSAGE('Transporteur incompatible avec article');

        //LM le 22-05-2013=>longeur hors norme
        rec_item2.INIT();
        IF rec_item2.GET(rec."No.") THEN;
        IF rec_param_gen.GET('DIM_HORS_NORME', 'LONG') THEN;
        IF rec_itemunitmes.GET(rec."No.", rec_item2."Base Unit of Measure") THEN;
        IF rec_itemunitmes.Length >= rec_param_gen.Decimal1 THEN MESSAGE('Longueur hors norme');

        InsertExtendedText(FALSE);
        IF (rec.Type = rec.Type::"Charge (Item)") AND (rec."No." <> xRec."No.") AND
           (xRec."No." <> '')
        THEN
            CurrPage.SAVERECORD();
        if (Rec.Type = Rec.Type::Item) and Rec.IsAsmToOrderRequired() then begin
            CurrPage.SaveRecord();
            Rec.AutoAsmToOrder();
        end;

        IF rec.Reserve = rec.Reserve::Always THEN BEGIN
            CurrPage.SAVERECORD();
            IF (rec."Outstanding Qty. (Base)" <> 0) AND (rec."No." <> xRec."No.") THEN BEGIN
                rec.AutoReserve();
                CurrPage.UPDATE(FALSE);
            END;
        END;

        CurrPage.SAVERECORD(); // AD Le 10-11-2015
    END;

    LOCAL PROCEDURE FctSalesMultipleColor();
    var
        CduLFunctions: codeunit "Codeunits Functions";
    BEGIN

        IF CduLFunctions.GetSalesMultiple(Rec) <> 0 THEN BEGIN
            IF (rec.Quantity MOD CduLFunctions.GetSalesMultiple(Rec)) <> 0 THEN
                StyleSalesMultiple := TRUE
            ELSE
                StyleSalesMultiple := FALSE;
        END
        ELSE
            StyleSalesMultiple := FALSE;
    END;

    LOCAL PROCEDURE GetItemRecommendedShippingAgent(): Code[20];
    VAR
        lItem: Record Item;
    BEGIN
        //CFR le 30/11/2022 => R‚gie : transporteur pr‚conis‚ peut ˆtre obligatoire
        IF (Rec.Type = Rec.Type::Item) THEN
            IF lItem.GET(Rec."No.") THEN
                EXIT(lItem."Recommended Shipping Agent");
        EXIT('');
    END;

    LOCAL PROCEDURE GetItemMandatoryShippingAgent(): Boolean;
    VAR
        lItem: Record Item;
    BEGIN
        //CFR le 30/11/2022 => R‚gie : transporteur pr‚conis‚ peut ˆtre obligatoire
        IF (Rec.Type = Rec.Type::Item) THEN
            IF lItem.GET(Rec."No.") THEN
                EXIT(lItem."Mandatory Shipping Agent");
        EXIT(FALSE);
    END;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        // AD Le 26-01-2012 => Pour ne pas avoir des lignes avec ,5 dans les prepas
        IF Rec.Type = Rec.Type::Item THEN
            IF (Rec."Line No." MOD 10000) <> 0 THEN
                ERROR('Impossible d''inserer un ligne ici !');
        // FIN AD Le 26-01-2012
    end;
}