pageextension 51645 "Sales Hist. Sell-to FactBox" extends "Sales Hist. Sell-to FactBox" //9080
{

    layout
    {
        modify("No.")
        {
            Visible = false;
        }
        modify("No. of Blanket Orders")
        {
            visible = false;
        }
        addafter("No. of Orders")
        {
            field("Sell-to No. Of Archived Doc."; Rec."Sell-to No. Of Archived Doc.")
            {
                Caption = 'Sell-to No. Of Archived Doc.';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sell-to No. Of Archived Doc. field.';
            }
        }
        addafter("No. of Pstd. Credit Memos")
        {
            field("No. of Orders For Date"; Rec."No. of Orders For Date")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Ongoing Sales Orders';
                DrillDownPageID = "Sales Order List";
                StyleExpr = gStyleExpCVJour;
                ToolTip = 'Specifies the number of sales orders that have been registered for the customer.';
            }
            field("No. of Pstd. Ship.  For Date"; Rec."No. of Pstd. Ship.  For Date")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Posted Sales Shipments';
                DrillDownPageID = "Posted Sales Shipments";
                StyleExpr = gStyleExpEEJour;
                ToolTip = 'Specifies the number of posted sales shipments that have been registered for the customer.';
            }
        }
    }

    var
        gStyleExpCVJour: Text[20];
        gStyleExpEEJour: Text[20];

    trigger OnAfterGetRecord()
    begin
        // CFR le 06/04/2022 => R‚gie : Coloration syntaxique des cde vente du jour et EVE du jour
        Rec.CALCFIELDS("No. of Orders For Date", "No. of Pstd. Ship.  For Date");
        IF (Rec."No. of Orders For Date" = 0) THEN
            gStyleExpCVJour := ''
        ELSE
            gStyleExpCVJour := 'Unfavorable';
        IF (Rec."No. of Pstd. Ship.  For Date" = 0) THEN
            gStyleExpEEJour := ''
        ELSE
            gStyleExpEEJour := 'Unfavorable';
        // FIN CFR le 06/04/2022
    end;

    trigger OnOpenPage()
    begin
        // MCO Le 21-11-2017 => Toujours visible (fonction mal r‚alis‚e par microsoft)
        // ShowCustomerNo := TRUE;

        // AD Le 29-01-2020 => REGIE -> Pour avoir le nb de commande et BL du jour
        Rec.SETRANGE("Date Filter", WORKDATE());
    end;

    procedure Vide();
    begin
        CLEAR(Rec);
    end;


}

