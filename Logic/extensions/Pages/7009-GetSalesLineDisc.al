pageextension 50005 "Get Sales Line Disc." extends "Get Sales Line Disc." //7009
{
    layout
    {
        modify("Line Discount %")
        {
            Editable = false;
        }
    }
}