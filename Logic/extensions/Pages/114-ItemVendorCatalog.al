pageextension 50641 "Item Vendor Catalog" extends "Item Vendor Catalog" //114
{
    layout
    {
        addafter("Vendor No.")
        {
            field("Vendor Name"; Rec."Vendor Name")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Vendor Name field.';
            }
        }
        addafter("Lead Time Calculation")
        {
            field("Purch. Unit of Measure"; Rec."Purch. Unit of Measure")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Purch. Unit of Measure field.';
            }
            field("Vendor Item Desciption"; Rec."Vendor Item Desciption")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Vendor Item No. field.';
            }
            field("Statut Qualité"; Rec."Statut Qualité")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Statut Qualité field.';
            }
            field("Statut Approvisionnement"; Rec."Statut Approvisionnement")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Statut Approvisionnement field.';
            }
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
            field("Code Remise"; Rec."Code Remise")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Code Remise field.';
            }
            field("Last Direct Cost"; Rec."Last Direct Cost")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Last Direct Cost field.';
            }
            field("Purch. Multiple"; Rec."Purch. Multiple")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Multiple d''achat field.';
            }
            field("Minimum Order Quantity"; Rec."Minimum Order Quantity")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Minimum Order Quantity field.';
            }
            field(Comment; Rec.Comment)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Commentaire field.';
            }
        }
    }

    actions
    {
        addafter("&Item Vendor")
        {
            action(Fiche)
            {
                Caption = 'Fiche';
                ApplicationArea = All;
                Promoted = true;
                PromotedIsBig = true;
                Image = VendorLedger;
                PromotedCategory = Process;
                ToolTip = 'Executes the Fiche action.';
                trigger OnAction()
                VAR
                    ItemVendor: Record "Item Vendor";
                    ItemVendorCard: Page "Item Vendor Card";
                BEGIN
                    ItemVendor.SETRANGE("Item No.", Rec."Item No.");
                    ItemVendorCard.SETRECORD(Rec);
                    ItemVendorCard.SETTABLEVIEW(ItemVendor);
                    ItemVendorCard.RUN();
                END;
            }
        }
    }
    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        //CFR le 05/04/2023 => R‚gie : cr‚ation d'une ligne de tarif achat … 0 s'il n'existe pas de ligne
        Rec.CreatePurchasePrice();
    end;
    //************************Non utiliser************************************************************
    // LOCAL PROCEDURE GetPurchPrice(): Decimal;
    // VAR
    //     SingleInstance: Codeunit SingleInstance;
    //     LPrixNet: Decimal;
    // BEGIN
    //     SingleInstance.GetBestPurchPrice(Rec."Item No.", 1, 3, LPrixNet, Rec."Vendor No.");
    //     EXIT(LPrixNet);
    // END;
    //************************Non utiliser************************************************************
}