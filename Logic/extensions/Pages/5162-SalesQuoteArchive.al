pageextension 50796 "Sales Quote Archive" extends "Sales Quote Archive" //5162
{

    layout
    {
        addafter(Status)
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
    }
    actions
    {

    }

}

