pageextension 50581 "Sales Quote" extends "Sales Quote" //41
{
    layout
    {
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                Importance = Promoted;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
            field("Sell-to Customer No. SYM"; Rec."Sell-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Rouge = Client EXPORT';
                StyleExpr = gStyleExp_Export;
                trigger OnValidate()
                BEGIN
                    // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
                    SetStyleExp_Export();
                    // FIN CFR le 05/04/2023
                END;
            }
        }
        modify("Sell-to Customer No.")
        {
            Visible = false;
        }
        modify("Sell-to Customer Name")
        {
            Caption = 'Customer';
            Editable = EditableESK;
            StyleExpr = gStyleExp_Export;
            trigger OnAfterValidate()
            begin
                // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
                SetStyleExp_Export();
                // FIN CFR le 05/04/2023

                CurrPage.Update();
            end;
        }
        modify("Sell-to Address")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Address 2")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Post Code")
        {
            Editable = EditableESK;
        }
        modify("Sell-to City")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Contact No.")
        {
            Editable = EditableESK;
        }
        modify("Sell-to Contact")
        {
            Editable = EditableESK;
        }
        addafter("Sell-to Contact")
        {
            field(Centrale; Rec.GetCentraleName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCentraleName() field.';
            }
            field("Code Enseigne 1"; Rec."Code Enseigne 1")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
            }
            field("Code Enseigne 2"; Rec."Code Enseigne 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
            }
        }
        modify("Sell-to Customer Templ. Code")
        {
            Editable = EditableESK;
        }
        modify("No. of Archived Versions")
        {
            Visible = false;
        }
        addafter("No. of Archived Versions")
        {
            field("Source Document Type"; Rec."Source Document Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
            field("Quote Validity Date"; Rec."Quote Validity Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date de validité du devis field.';
            }
        }
        modify("Your Reference")
        {
            Visible = false;
        }
        moveafter("Source Document Type"; "External Document No.", Status, "Your Reference")
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
        }
        addafter(Status)
        {
            field("Suivi Devis"; Rec."Suivi Devis")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Suivi Devis field.';
            }
        }
        addafter("Order Date")
        {
            field("Modify Date"; Rec."Modify Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date modif. Cde field.';
            }
        }
        modify("Document Date")
        {
            Editable = EditableESK;
            QuickEntry = false;
        }
        moveafter("Document Date"; "Requested Delivery Date")
        addafter("Requested Delivery Date")
        {
            field("Promised Delivery Date"; Rec."Promised Delivery Date")
            {
                ApplicationArea = All;
                Visible = false;
                ToolTip = 'Specifies the value of the Promised Delivery Date field.';
            }
            field("Quote Type"; Rec."Quote Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de devis field.';
            }
            field("Service Zone Code"; Rec."Service Zone Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Service Zone Code field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Modify User ID"; Rec."Modify User ID")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur modif. Cde field.';
            }
        }
        modify("Due Date")
        {
            Importance = Standard;
            Editable = EditableEsk;
        }
        moveafter("Modify User ID"; "Campaign No.", "Opportunity No.", "Responsibility Center", "Assigned User ID", "Due Date")
        addafter("Salesperson Code")
        {
            field("Shipment Method Code2"; Rec."Shipment Method Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Shipping Agent Code2"; Rec."Shipping Agent Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Shipping Agent Code field.';
            }
            field("Mode d'expédition3"; Rec."Mode d'expédition")
            {
                Caption = 'Mode d''expédition3';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Mode d''expédition3 field.';
            }
            field("Incoterm City"; Rec."Incoterm City")
            {
                ApplicationArea = All;
                Importance = Additional;
                Editable = gEditable_Export;
                ToolTip = 'Specifies the value of the Ville Incoterm ICC 2020 field.';
            }
            field("Incoterm Code"; Rec."Incoterm Code")
            {
                ApplicationArea = All;
                Importance = Additional;
                Editable = gEditable_Export;
                ToolTip = 'Specifies the value of the Shipment Method Code field.';
            }
            field("Transporteur imperatif"; Rec."Transporteur imperatif")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur imperatif field.';
            }
            field("Saturday Delivery"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field("Abandon remainder"; Rec."Abandon remainder")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Abandon reliquat field.';
            }
            field("Web comment"; Rec."Web comment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Web comment field.';
            }
            field("Nb Web comment"; Rec."Nb Web comment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nb Commentaire Web field.';
            }
            field("Due Date2"; Rec."Due Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Due Date field.';
            }
            field("Do Not Print Active Ref"; Rec."Do Not Print Active Ref")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Do Not Print Active Ref field.';
            }
        }
        modify("Prices Including VAT")
        {
            Editable = EditableESK;
        }
        modify("VAT Bus. Posting Group")
        {
            Editable = EditableESK;
        }
        modify("Payment Terms Code")
        {
            Importance = Standard;
        }
        movefirst("Invoice Details"; "Shipment Date", "Prices Including VAT", "VAT Bus. Posting Group", "Payment Terms Code", "Payment Method Code")
        addafter("Payment Method Code")
        {
            field("Bill-to Customer No."; Rec."Bill-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
            }
            field(BillToName1; Rec."Bill-to Name")
            {
                ApplicationArea = All;
                Caption = 'Name';
                Editable = BillToOptions = BillToOptions::"Another Customer";
                Enabled = EnableBillToCustomerNo;
                Importance = Promoted;
                ToolTip = 'Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.';

                trigger OnValidate()
                begin
                    if Rec.GetFilter("Bill-to Customer No.") = xRec."Bill-to Customer No." then
                        if Rec."Bill-to Customer No." <> xRec."Bill-to Customer No." then
                            Rec.SetRange("Bill-to Customer No.");

                    CurrPage.Update();
                end;
            }
        }
        modify("Shortcut Dimension 1 Code")
        {
            Editable = EditableESK;
        }
        modify("Shortcut Dimension 2 Code")
        {
            Editable = EditableESK;
        }
        modify("Location Code")
        {
            Editable = EditableESK;
        }
        addafter("Location Code")
        {
            field("Echéances fractionnées"; Rec."Echéances fractionnées")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Echéances fractionnées field.';
            }
            field("Payment Terms Code 2"; Rec."Payment Terms Code 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Payment Terms Code 2 field.';
            }
            field("Due Date 2"; Rec."Due Date 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the date d''échéance 2 field.';
            }
            field("Taux Premiere Fraction"; Rec."Taux Premiere Fraction")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Taux Premiere Fraction field.';
            }
            field("Invoice Type"; Rec."Invoice Type")
            {
                ApplicationArea = All;
                Editable = EditableEsk;
                ToolTip = 'Specifies the value of the Type Facturation field.';
            }
            field("Exclure RFA"; Rec."Exclure RFA")
            {
                ApplicationArea = All;
                Editable = EditableEsk;
                ToolTip = 'Specifies the value of the Exclure RFA field.';
            }
            field("Multi echéance"; Rec."Multi echéance")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multi echéance field.';
            }
            field("Customer Price Group"; Rec."Customer Price Group")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Price Group field.';
            }
            field("Customer Disc. Group"; Rec."Customer Disc. Group")
            {
                ApplicationArea = All;
                Importance = Promoted;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
            field("Campaign No. 2"; Rec."Campaign No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Campaign No. field.';
            }
            group("Centrale Group")
            {
                Caption = 'Centrale';
                field("Avoid Active Central"; Rec."Avoid Active Central")
                {
                    ApplicationArea = All;
                    Caption = 'Avoid Active Central';
                    Importance = Additional;
                    ToolTip = 'Specifies the value of the Avoid Active Central field.';
                    trigger OnValidate()
                    BEGIN
                        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
                        gCentralEditable := NOT Rec."Avoid Active Central";
                    END;
                }
                field("Centrale Active"; Rec."Centrale Active")
                {
                    ApplicationArea = All;
                    Importance = Promoted;
                    Editable = gCentralEditable;
                    ToolTip = 'Specifies the value of the Centrale Active field.';
                }
            }
        }
        modify("Shipping and Billing")
        {
            Enabled = Rec."Sell-to Customer No." <> '';
        }
        modify("Ship-to Post Code")
        {
            Importance = Promoted;
        }
        modify("Ship-to City")
        {
            Importance = Promoted;
        }
        modify("Ship-to Contact")
        {
            Editable = ShipToOptions = ShipToOptions::"Custom Address";
        }
        modify("Shipment Method Code")
        {
            Editable = EditableEsk;
        }
        modify("Shipping Agent Code")
        {
            Caption = 'Agent';
        }
        modify("Shipping Agent Service Code")
        {
            Caption = 'Agent service';
        }
        addafter(Control60)
        {
            field("Mode d'expédition2"; Rec."Mode d'expédition")
            {
                Caption = 'Mode d''expédition2';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Mode d''expédition2 field.';
            }
            field("Transporteur imperatif2"; Rec."Transporteur imperatif")
            {
                Caption = 'Transporteur imperatif2';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Transporteur imperatif2 field.';
            }
            field("Saturday Delivery22"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field("Abandon remainder 2"; Rec."Abandon remainder")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Abandon reliquat field.';
            }
            field("Combine Shipments"; Rec."Combine Shipments")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Combine Shipments field.';
            }
            field("Chiffrage BL"; Rec."Chiffrage BL")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Chiffrage BL field.';
            }
            field("Saturday Delivery 2"; Rec."Saturday Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Livraison le samedi field.';
            }
            field("Instructions of Delivery"; Rec."Instructions of Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Instructions de livraison field.';
            }
            field("Insurance of Delivery"; Rec."Insurance of Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Assurance de livraison field.';
            }
            field("Cash on Delivery"; Rec."Cash on Delivery")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Contre remboursement field.';
            }
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = All;
                Importance = Promoted;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
            field("N° affrètement"; Rec."N° affrètement")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° affrètement field.';
            }
            field("ShptMethodCode0"; Rec."Shipment Method Code")
            {
                ApplicationArea = All;
                Caption = 'Code';
                ToolTip = 'Specifies how items on the sales document are shipped to the customer.';
                Importance = Promoted;
                Editable = EditableEsk;
            }
            field(ShippingAgentCode0; Rec."Shipping Agent Code")
            {
                ApplicationArea = All;
                Caption = 'Agent';
                ToolTip = 'Specifies which shipping agent is used to transport the items on the sales document to the customer.';
                Importance = Promoted;
            }
            field(ShiptTOCity0; Rec."Ship-to City")
            {
                ApplicationArea = All;
                Caption = 'City';
                ToolTip = 'Specifies the city that products on the sales document will be shipped to.';
                Importance = Promoted;
                Editable = ShipToOptions = ShipToOptions::"Custom Address";
            }
        }
        modify("Bill-to Name")
        {
            Editable = EditableESK;
        }
        modify("Bill-to Address")
        {
            Editable = EditableESK;
        }
        modify("Bill-to Address 2")
        {
            Editable = EditableESK;
        }
        modify("Bill-to Post Code")
        {
            Editable = EditableESK;
        }
        modify("Bill-to City")
        {
            Editable = EditableESK;
        }
        modify("Bill-to Contact No.")
        {
            Editable = EditableESK;
        }
        modify("Bill-to Contact")
        {
            Editable = EditableESK;
        }
        modify("Currency Code")
        {
            Editable = EditableESK;
        }
        movefirst("Foreign Trade"; "Currency Code")
        modify("EU 3-Party Trade")
        {
            Editable = EditableESK;
        }
        modify("Transaction Specification")
        {
            Editable = EditableESK;
        }
        modify("Transport Method")
        {
            Editable = EditableESK;
        }
        modify("Exit Point")
        {
            Editable = EditableESK;
        }
        modify("Area")
        {
            Editable = EditableESK;
        }
        addfirst(factboxes)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                ApplicationArea = All;
                Caption = 'Devis Web';
            }
        }
        addafter(WorkflowStatus)
        {
            part("Item Web Info"; "Item Web Info")
            {
                ApplicationArea = All;
                SubPageLink = "No." = FIELD("No.");
                Provider = SalesLines;
            }
        }
    }
    actions
    {
        modify("C&ontact")
        {
            trigger onBeforeAction()
            VAR
                lContact: Record Contact;
                lPageContactCard: Page "Contact Card";
            BEGIN
                // CFR le 22/09/2021 => R‚gie - Bloquer la modification des contacts
                CLEAR(lContact);
                lContact.SETRANGE("No.", Rec."Sell-to Contact No.");
                lPageContactCard.SETTABLEVIEW(lContact);
                lPageContactCard.EDITABLE(FALSE);
                lPageContactCard.RUNMODAL();
            END;
        }
        addafter("C&ontact")
        {
            action(MailDelai)
            {
                ApplicationArea = All;
                Caption = 'Mail demande délai';
                Promoted = true;
                PromotedIsBig = true;
                Image = MailSetup;
                PromotedCategory = Category4;
                PromotedOnly = true;
                ToolTip = 'Executes the Mail demande délai action.';
                trigger OnAction()
                VAR
                    lMailReliquat: Codeunit "Mail Reliquat";
                BEGIN
                    lMailReliquat.MailDemandeDelai(Rec."No.");
                END;
            }
        }
        addfirst(processing)
        {
            action("Multi-Consultation")
            {
                ApplicationArea = All;
                Caption = 'Multi consultation';
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ToolTip = 'Executes the Multi consultation action.';

                trigger OnAction()
                begin
                    CurrPage.SalesLines.PAGE.MultiConsultation();
                end;
            }
        }
        modify("Co&mments")
        {
            Promoted = true;
            PromotedIsBig = true;
        }
        addafter(Email)
        {
            action("Envoyer Par Fax")
            {
                ApplicationArea = All;
                Caption = 'Envoyer Par Fax';
                Promoted = true;
                PromotedIsBig = true;
                Image = PrintCheck;
                PromotedCategory = Process;
                ToolTip = 'Executes the Envoyer Par Fax action.';
                trigger OnAction()
                BEGIN
                    SendFaxMail('FAX')
                END;
            }
        }
        modify(MakeOrder)
        {
            ShortCutKey = F9;
        }

        modify(Release)
        {
            Promoted = true;
            PromotedIsBig = true;
        }
        modify(Reopen)
        {
            Promoted = true;
            PromotedIsBig = true;
            PromotedCategory = New;
        }
        addafter("Archive Document")
        {
            action("Import Excel")
            {
                ApplicationArea = All;
                Caption = 'Import Excel';
                Promoted = true;
                PromotedIsBig = true;
                Image = ImportExcel;
                PromotedCategory = Process;
                ToolTip = 'Executes the Import Excel action.';
                trigger OnAction()
                BEGIN
                    ImportExcel()
                END;
            }
        }
        addafter(RemoveIncomingDoc)
        {
            action("Insérer ligne de port")
            {
                ApplicationArea = All;
                Caption = 'Insérer ligne de port';
                Image = Insert;
                ToolTip = 'Executes the Insérer ligne de port action.';
                trigger OnAction()
                BEGIN
                    // AD Le 31-08-2009 => Gestion des frais de port
                    CurrPage.SalesLines.PAGE.InsertPort();
                END;
            }
        }
    }
    var
        DocPrint: Codeunit "Document-Print";
        IsCustomerOrContactNotEmpty: Boolean;
        EditableESK: Boolean;
        IsOfficeAddin: Boolean;
        gCentralEditable: Boolean;
        gStyleExp_Export: Text;
        gEditable_Export: Boolean;

    trigger OnAfterGetRecord()
    begin
        SetControlAppearance();
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        gCentralEditable := NOT Rec."Avoid Active Central";
        // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
        SetStyleExp_Export();
        // FIN CFR le 05/04/2023
    end;

    trigger OnAfterGetCurrRecord()
    begin
        SetControlAppearance();
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        SetControlAppearance();
    end;

    trigger OnOpenPage()
    var
        OfficeMgt: Codeunit "Office Management";
    begin
        SetControlAppearance();
        IsOfficeAddin := OfficeMgt.IsAvailable();
    end;
    //*************************Non utiliser******************************************
    // local procedure CheckSalesCheckAllLinesHaveQuantityAssignedSYM()
    // var
    //     LinesInstructionMgt: Codeunit "Lines Instruction Mgt.";
    // begin
    //     LinesInstructionMgt.SalesCheckAllLinesHaveQuantityAssigned(Rec);
    // end;
    //*************************Non utiliser******************************************
    local procedure SetControlAppearance()
    begin
        IsCustomerOrContactNotEmpty := (Rec."Sell-to Customer No." <> '') or (Rec."Sell-to Contact No." <> '');
    end;

    procedure SendFaxMail(_pType: Code[10]);
    var
        LDoc: Record "Sales Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    //TODO cTFPostFunctions: Codeunit 52101147;
    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        //LM le 17-05-2013=>permet de modifier les options avant envoi
        DocPrint.PrintSalesHeader(Rec);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;

        //-TF
        //TODO cTFPostFunctions.AddDocumentToQueue(
        //   cTFPostFunctions.GetTableID(TABLENAME()), // Tablename
        //   "No.",                                      // Document No.
        //   "Posting Date",                             // Posting Date
        //   0,                                          // Entry No.
        //   "Document Type",                            // Document Type
        //   '',                                         // Identification ID
        //   "Business Case"                             // Business Case
        // );
        //+TF
    end;


    local procedure ImportExcel();
    var
        CuImportExcel: Codeunit "Import Ligne Document Vte XLS";
    begin
        CuImportExcel.ImportLigneCde(Rec);
    end;

    local procedure SetStyleExp_Export();
    begin
        //CFR le 05/04/2023 => Régie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        gEditable_Export := FALSE;
        IF (Rec."Salesperson Code" = '19') THEN BEGIN
            gStyleExp_Export := 'Unfavorable';
            gEditable_Export := TRUE;
        END;
        //FIN CFR le 05/04/2023
    end;
}