pageextension 51360 "Whse. Receipt Lines" extends "Whse. Receipt Lines" //7342
{

    layout
    {
        addafter("Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
            field("Référence fournisseur"; Rec."Référence fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence fournisseur field.';
            }
        }
    }
}

