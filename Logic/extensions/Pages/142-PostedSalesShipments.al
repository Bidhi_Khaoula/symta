pageextension 50235 "Posted Sales Shipments" extends "Posted Sales Shipments"  //142
{
    layout
    {
        movebefore("No."; "posting Date")

        modify("Posting Date")
        {
            Visible = true;
        }
        modify("Location Code")
        {
            Visible = false;
        }
        modify("Currency Code")
        {
            Visible = false;
        }
        modify("No. Printed")
        {
            Visible = false;
        }
        addafter("Shipment Date")
        {
            field("Order No."; Rec."Order No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Order No. field.';
            }
            field("Preparation No."; Rec."Preparation No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the N° BP field.';
            }
            field("Centrale Active"; Rec."Centrale Active")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
            field(Montant; Rec.GetAmount())
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the GetAmount() field.';
            }
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
            field("Nb Of Box"; Rec."Nb Of Box")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Nb de colis field.';
            }
            field(Weight; Rec.Weight)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Poids field.';
            }
            field("Montant Du Port"; Rec.GetPortAmount())
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the GetPortAmount() field.';
            }
            field("Ship-to City"; Rec."Ship-to City")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Ship-to City field.';
            }
            field(Preparateur; Rec.Preparateur)
            {
                ApplicationArea = all;
                Caption = 'Préparateur';
                ToolTip = 'Specifies the value of the Préparateur field.';
            }
            field(PreparateurName; rec.GetPreparateurName())
            {
                Caption = 'Nom préparateur';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nom préparateur field.';
            }
            field("Preparation Valid Date"; Rec."Preparation Valid Date")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Heure validation BP field.';
            }
        }
    }


    actions
    {
        addafter("&Track Package")
        {
            action(ActionName)
            {
                ApplicationArea = All;
                Caption = 'Créer Litige';
                Promoted = true;
                PromotedIsBig = true;
                Image = Error;
                PromotedCategory = Process;
                ToolTip = 'Executes the Créer Litige action.';
                trigger OnAction()
                begin
                    rec.CreerIncidentTransport();
                end;
            }
        }
        addafter("&Navigate")
        {
            action(Commande)
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedIsBig = true;
                Image = Order;
                PromotedCategory = Process;
                ToolTip = 'Executes the Commande action.';
                trigger OnAction()
                begin
                    rec.ShowOrder();
                end;
            }
        }
    }


    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("No.");
    end;
}