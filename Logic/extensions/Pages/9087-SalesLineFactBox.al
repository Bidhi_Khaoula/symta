pageextension 51669 "Sales Line FactBox" extends "Sales Line FactBox" //9087
{

    layout
    {
        modify("Item Availability")
        {
            Importance = Promoted;
            Style = Strong;
            StyleExpr = TRUE;
        }
        modify("Available Inventory")
        {
            Style = Strong;
            StyleExpr = TRUE;
        }
        modify(Substitutions)
        {
            Visible = false;
        }
        addafter(Substitutions)
        {
            field(Substitutions2; SalesInfoPaneMgt.CalcNoOfSubstitutions(Rec))
            {
                ApplicationArea = Suite;
                Caption = 'Substitutions';
                DrillDown = true;
                ToolTip = 'Specifies other items that are set up to be traded instead of the item in case it is not available.';

                trigger OnDrillDown()
                begin
                    CurrPage.SaveRecord();
                    Rec.ShowItemSub();
                    CurrPage.Update(true);
                    //GBO Le 29112108  => ticket 42715
                    //AutoReserve;
                    IF (Rec.Reserve = Rec.Reserve::Always) AND
                       (Rec."Outstanding Qty. (Base)" <> 0) AND
                       (Rec."Location Code" <> xRec."Location Code") THEN begin
                        Rec.AutoReserve();
                        CurrPage.Update(false);
                        //FIN GB
                    end;
                end;
            }
        }

        addafter("Reserved Requirements")
        {
            field("Multiple de Ventes"; CduGFunctions.GetSalesMultiple(Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetSalesMultiple(Rec) field.';
            }
            field("Qté Cde Vte"; CduGFunctions.GetQtyItem('CDEVTE', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''CDEVTE'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnSalesOrder(Rec);
                end;
            }
            field("Qté Cde Ach"; CduGFunctions.GetQtyItem('CDEACH', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''CDEACH'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnPurchOrder(Rec, 1);
                end;
            }
            field("Qté Retour Achat"; CduGFunctions.GetQtyItem('RTEACH', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''RTEACH'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnPurchOrder(Rec, 5);
                end;
            }
            field("Qté Retour Vente"; CduGFunctions.GetQtyItem('RTEVTE', Rec))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyItem(''RTEVTE'', Rec) field.';

                trigger OnDrillDown();
                begin
                    CduGFunctions.LookupQteOnSalesDoc(Rec, 5);
                end;
            }
        }
        addafter(SalesLineDiscounts)
        {
            field("SalesInfoPaneMgt.GetNbAdditional(0, Rec.""No."")"; CduGFunctions.GetNbAdditional(0, Rec."No."))
            {
                Caption = 'Articles complémentaires';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Articles complémentaires field.';

                trigger OnDrillDown();
                begin
                    // CFR le 10/03/2021 - SFD20210201 articles complémentaires

                    IF CduGFunctions.LookupAdditional(0, Rec) THEN
                        CurrPage.UPDATE();
                end;
            }
        }
    }

    var
        CduGFunctions: Codeunit "Codeunits Functions";
}

