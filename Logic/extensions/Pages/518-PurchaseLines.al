pageextension 50841 "Purchase Lines" extends "Purchase Lines" //518
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
        addbefore("Document Type")
        {
            field("Order Date"; Rec."Order Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
            field(GetDateHeader_3; rec.GetDateHeader(3))
            {
                Caption = 'Date d''envoi commande';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date d''envoi commande field.';
            }
        }
        addafter("Buy-from Vendor No.")
        {
            field("Nom Fournisseur"; Rec.GetVendorName())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetVendorName() field.';
            }
        }
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter(Quantity)
        {
            field("Quantity (Base)"; Rec."Quantity (Base)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Quantity (Base) field.';
            }
            field("Outstanding Qty. (Base)"; Rec."Outstanding Qty. (Base)")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Outstanding Qty. (Base) field.';
            }
        }
        addafter("Amt. Rcd. Not Invoiced (LCY)")
        {
            field("Requested Receipt Date"; Rec."Requested Receipt Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Requested Receipt Date field.';
            }
            field("Promised Receipt Date"; Rec."Promised Receipt Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Promised Receipt Date field.';
            }
            field(Départ; Rec.GetQtyOnDepart())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyOnDepart() field.';
            }
            field(Magasin; Rec.GetQtyOnMagasin())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyOnMagasin() field.';
            }
            field(Réception; Rec.GetQtyOnReceipt())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQtyOnReceipt() field.';
            }
            field(Pointage; Rec.GetIfPointage())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetIfPointage() field.';
            }
            field("Qté pointage;"; Rec.GetQuantityPointage())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetQuantityPointage() field.';
            }
            field("gPurchHeader.""Vendor Order No."""; gPurchHeader."Vendor Order No.")
            {
                CaptionClass = 'Your Ref';
                Editable = false;
                ToolTip = 'Non triable/filtrable > liaison avec en-tête';
                ApplicationArea = All;
            }
            field("gPurchHeader.""Create User ID"""; gPurchHeader."Create User ID")
            {
                Caption = 'Code utilisateur création';
                ToolTip = 'Non triable/filtrable > liaison avec en-tête';
                ApplicationArea = All;
            }
            field("gVendor.""Code Appro"""; gVendor."Code Appro")
            {
                Caption = 'Code appro.';
                ToolTip = 'Non triable/filtrable > liaison avec fournisseur';
                ApplicationArea = All;
            }
            field("gItem.""Manufacturer Code"""; gItem."Manufacturer Code")
            {
                Caption = 'Marque';
                ToolTip = 'Non triable/filtrable > liaison avec article';
                ApplicationArea = All;
            }
            field("Demande retour"; Rec."Demande retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Demande retour field.';
            }
            field("Regularisation retour"; Rec."Regularisation retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Regularisation retour field.';
            }
            field(Commentaires; Rec.Commentaires)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Commentaires field.';
            }
        }
    }

    actions
    {
        modify("Show Document")
        {
            Visible = false;
        }
        addafter("Show Document")
        {
            action("Show Document2")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Show Document';
                Image = View;
                ShortCutKey = 'Shift+F7';
                ToolTip = 'Open the document that the selected line exists on.';

                trigger OnAction()
                var
                    lPurchaseHeader: Record "Purchase Header";
                    PageManagement: Codeunit "Page Management";
                begin
                    lPurchaseHeader.Get(Rec."Document Type", Rec."Document No.");
                    PageManagement.PageRun(lPurchaseHeader);

                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        // MCO Le 08-06-2017 => R‚gie
        gPurchHeader.GET(rec."Document Type", rec."Document No.");
        // FIN MCO
        // CFR le 23/03/2023 => R‚gie
        IF gItem.GET(Rec."No.") THEN;
        IF gVendor.GET(gPurchHeader."Buy-from Vendor No.") THEN;
        // FIN CFR le 23/03/2023
    end;

    var
        gPurchHeader: Record "Purchase Header";
        gVendor: Record Vendor;
        gItem: Record Item;
}