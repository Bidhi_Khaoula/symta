pageextension 51452 "Bin Contents" extends "Bin Contents" //7374
{
    layout
    {
        addafter("Cross-Dock Bin")
        {
            field("capacité"; Rec.capacité)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the capacité field.';
            }
            field(Description; Rec.Description)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Description field.';
            }
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
    }
    actions
    {

    }
}

