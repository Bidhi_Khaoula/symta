pageextension 50001 "Posted Sales Document Lines" extends "Posted Sales Document Lines" //5850
{
    trigger OnOpenPage()
    begin
        CurrentMenuType := 0; // AD Le 18-01-2016
        ChangeSubMenu(CurrentMenuType);
        Rec.SetRange("No.", Rec."No.");
    end;
}