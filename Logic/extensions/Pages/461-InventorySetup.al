pageextension 50648 "Inventory Setup" extends "Inventory Setup" //461
{
    layout
    {
        addafter("Internal Movement Nos.")
        {
            group(SYMTA)
            {
                field("% Seuil Capacité Réap. Pick."; Rec."% Seuil Capacité Réap. Pick.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Seuil Capacité Réap. Pick. field.';
                }
                field("Nom Feuille Réappro. Picking"; Rec."Nom Feuille Réappro. Picking")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Feuille Réappro. Picking field.';
                }
                field("Nom Feuille Réap. Pick. Minil."; Rec."Nom Feuille Réap. Pick. Minil.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Feuille Réap. Pick. Minil. field.';
                }
                field("Mail Réappro. Picking"; Rec."Mail Réappro. Picking")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mail Réappro. Picking field.';
                }
                field("Nom Feuille Réappro. PU"; Rec."Nom Feuille Réappro. PU")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Feuille Réappro. PU field.';
                }
                field("Inventory Amount Level"; Rec."Inventory Amount Level")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Seuil montant inventaire coloration field.';
                }
            }
        }
    }
}