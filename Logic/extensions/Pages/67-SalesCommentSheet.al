pageextension 51200 "Sales Comment Sheet" extends "Sales Comment Sheet" //67
{
    layout
    {
        addafter(Code)
        {
            field("Commentaires Web"; Rec."Commentaires Web")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Commentaires Web field.';
            }
            field("Display Order"; Rec."Display Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Affichage commande field.';
            }
            field("Print Quote"; Rec."Print Quote")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression devis field.';
            }
            field("Print Order"; Rec."Print Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression commande/Retour field.';
            }
            field("Print Wharehouse Shipment"; Rec."Print Wharehouse Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression exp. magasin field.';
            }
            field("Print Shipment"; Rec."Print Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression expédition field.';
            }
            field("Print Invoice"; Rec."Print Invoice")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression facture field.';
            }
            field("No."; Rec."No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the No. field.';
            }
            field("Document Line No."; Rec."Document Line No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Document Line No. field.';
            }
        }
    }

    actions
    {
        addfirst(Processing)
        {
            action(AddPackingComment)
            {
                Caption = 'Packing list';
                Image = CreateWarehousePick;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Visible = gDisplayPackingList;
                ApplicationArea = All;
                ToolTip = 'Executes the Packing list action.';

                trigger OnAction();
                var
                    BL: Record "Sales Shipment Header";
                    ShipmentInvoiced: Record "Shipment Invoiced";
                    PackingHeader: Record "Packing List Header";
                    ShipmentHeader: Record "Sales Shipment Header";
                    Colis: Record "Packing List Package";
                    Comment: Record "Sales Comment Line";
                    lastNo: Text;
                    gBlNoFilter: Text;
                    gPackingFilter: Text;
                    nbColis: Integer;
                    PoidsBrut: Decimal;
                    PoidsNet: Decimal;
                    ColisInfo: array[30] of Text;
                    lineNo: Integer;
                    i: Integer;
                    InvoiceNo: Text;
                    typeColisLibelle: Text;
                begin
                    BL.GET(Rec."No.");

                    gBlNoFilter := BL."No.";

                    ShipmentInvoiced.SETRANGE("Shipment No.", BL."No.");
                    IF ShipmentInvoiced.FINDFIRST() THEN BEGIN
                        InvoiceNo := ShipmentInvoiced."Invoice No.";
                        lastNo := BL."No.";

                        // on récupère les n° de BL liés à facture
                        CLEAR(ShipmentInvoiced);
                        ShipmentInvoiced.SETRANGE("Invoice No.", InvoiceNo);
                        ShipmentInvoiced.SETFILTER("Shipment No.", '<>%1', BL."No.");
                        IF ShipmentInvoiced.FINDSET() THEN
                            REPEAT
                                IF lastNo <> ShipmentInvoiced."Shipment No." THEN BEGIN
                                    gBlNoFilter += '|';
                                    gBlNoFilter += ShipmentInvoiced."Shipment No.";
                                    lastNo := ShipmentInvoiced."Shipment No.";
                                END;
                            UNTIL ShipmentInvoiced.NEXT() = 0;
                    END;

                    // on en déduit les n° de packing list
                    ShipmentHeader.SETFILTER("Packing List No.", '<>%1', '');
                    ShipmentHeader.SETFILTER("No.", gBlNoFilter);
                    ShipmentHeader.FINDSET();
                    REPEAT
                        IF lastNo <> ShipmentHeader."Packing List No." THEN BEGIN
                            IF gPackingFilter <> '' THEN gPackingFilter += '|';
                            gPackingFilter += ShipmentHeader."Packing List No.";
                            lastNo := ShipmentHeader."Packing List No.";
                        END;
                    UNTIL ShipmentHeader.NEXT() = 0;


                    IF gPackingFilter = '' THEN ERROR('Aucune packing list pour %1', BL."No.");

                    // on récupère les infos depuis entete packing list
                    PackingHeader.SETAUTOCALCFIELDS("Theoretical Weight", "Calculate Header Weight");
                    PackingHeader.SETFILTER("No.", gPackingFilter);
                    PackingHeader.FINDSET();
                    REPEAT
                        PoidsBrut += PackingHeader."Calculate Header Weight";
                        PoidsNet += PackingHeader."Theoretical Weight";
                        Colis.SETRANGE("Packing List No.", PackingHeader."No.");
                        IF Colis.FINDSET() THEN
                            REPEAT
                                // et depuis la liste des colis
                                nbColis += 1;
                                IF (Colis."Package Type" = Colis."Package Type"::Palette) THEN
                                    typeColisLibelle := 'Palette / Pallet'
                                ELSE
                                    typeColisLibelle := 'Colis / Parcel';
                                ColisInfo[nbColis] := STRSUBSTNO('%1 %2 : %3cm x %4cm x %5cm   ;  Poids / Weight : %6kg',
                                    typeColisLibelle, Colis."Package No.",
                                    Colis.Length, Colis.Width, Colis.Height,
                                    Colis."Net Weight");
                            UNTIL Colis.NEXT() = 0;
                    UNTIL PackingHeader.NEXT() = 0;

                    // on crée les lignes commentaires
                    Comment.SETRANGE("Document Type", Comment."Document Type"::Shipment);
                    Comment.SETRANGE("No.", BL."No.");
                    IF Comment.FINDFIRST() THEN
                        lineNo := ROUND(Comment."Line No." / 10, 1)
                    ELSE
                        lineNo := 10000;

                    //MESSAGE('%1 ** %2 ** %3 ** %4', gBlNoFilter, gPackingFilter, nbColis, lineNo);

                    // CFR le 06/04/2023 - Régie : Incoterm ICC 2020
                    AddComment(lineNo, 'Incoterm ICC 2020 : ');
                    AddComment(lineNo, 'Transporteur : ');
                    AddComment(lineNo, 'Poids brut / Gross weight : ' + FORMAT(PoidsBrut) + 'kg');
                    AddComment(lineNo, 'Poids net / Net weight : ' + FORMAT(PoidsNet) + 'kg');
                    AddComment(lineNo, FORMAT(nbColis) + ' package');
                    FOR i := 1 TO nbColis DO
                        AddComment(lineNo, '    ' + ColisInfo[i]);

                    CurrPage.UPDATE();
                end;
            }
        }
    }
    Trigger OnOpenPage()
    BEGIN
        gDisplayPackingList := Rec."Document Type" = Rec."Document Type"::Shipment;
    END;

    local procedure AddComment(var lineNo: Integer; pComment: Text);
    var
        Comment: Record "Sales Comment Line";
    begin
        lineNo += 10;
        Comment.INIT();
        Comment.VALIDATE("Document Type", Comment."Document Type"::Shipment);
        Comment.VALIDATE("No.", Rec."No.");
        Comment.VALIDATE("Line No.", lineNo);
        Comment.VALIDATE("Print Invoice", TRUE);
        Comment.VALIDATE(Comment, pComment);
        Comment.VALIDATE(Date, TODAY);
        Comment.INSERT(TRUE);
    end;

    var
        gDisplayPackingList: Boolean;
}