pageextension 50197 "Vendor Statistics" extends "Vendor Statistics" //152
{
    layout
    {
        addafter(GetInvoicedPrepmtAmountLCY)
        {
            field("Gen. Bus. Posting Group"; Rec."Gen. Bus. Posting Group")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Gen. Bus. Posting Group field.';
            }
        }
        modify("VendPurchLCY[1]")
        {
            caption = 'Purchase (LCY)';
        }
        addafter("VendPurchLCY[1]")
        {
            field("VendPurchLCYDoc[1]"; VendPurchLCYDoc[1])
            {
                Caption = 'Achats DS Date Doc.';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Achats DS Date Doc. field.';
            }
        }
        modify("VendPurchLCY[2]")
        {
            caption = 'Purchase (LCY)';
        }
        addafter("VendPurchLCY[2]")
        {
            field("VendPurchLCYDoc[2]"; VendPurchLCYDoc[2])
            {
                Caption = 'Achats DS Date Doc.';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Achats DS Date Doc. field.';
            }
        }
        modify("VendPurchLCY[3]")
        {
            caption = 'Purchase (LCY)';
        }
        addafter("VendPurchLCY[3]")
        {
            field("VendPurchLCYDoc[3]"; VendPurchLCYDoc[3])
            {
                Caption = 'Achats DS Date Doc.';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Achats DS Date Doc. field.';
            }
        }
        modify("VendPurchLCY[4]")
        {
            caption = 'Purchase (LCY)';
        }
        addafter("VendPurchLCY[4]")
        {
            field("VendPurchLCYDoc[4]"; VendPurchLCYDoc[4])
            {
                Caption = 'Achats DS Date Doc.';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Achats DS Date Doc. field.';
            }
        }
    }
    trigger OnAfterGetRecord()
    var
        ESK001Qst: Label 'Exercice Fiscal ?';
    begin
        CurrentDate := 0D;
        if CurrentDate <> WorkDate() then begin
            CurrentDate := WorkDate();
            if Not CONFIRM(ESK001Qst + ' ?') THEN begin
                VendDateFilter[2] := STRSUBSTNO('%1..%2', CALCDATE('<-CY>', WORKDATE()), CALCDATE('<CY>', WORKDATE()));
                VendDateFilter[3] := STRSUBSTNO('%1..%2', CALCDATE('<-CY-1Y>', WORKDATE()), CALCDATE('<CY-1Y>', WORKDATE()));
            end else
                exit;

            Rec.SetRange("Date Filter", 0D, CurrentDate);

            for i := 1 to 4 do begin
                Rec.SetFilter("Date Filter", VendDateFilter[i]);
                Rec.CalcFields(
                  "Purchases (LCY)", "Inv. Discounts (LCY)", "Inv. Amounts (LCY)", "Pmt. Discounts (LCY)",
                  "Pmt. Disc. Tolerance (LCY)", "Pmt. Tolerance (LCY)",
                  "Fin. Charge Memo Amounts (LCY)", "Cr. Memo Amounts (LCY)", "Payments (LCY)",
                  "Reminder Amounts (LCY)", "Refunds (LCY)", "Other Amounts (LCY)");
                VendPurchLCY[i] := Rec."Purchases (LCY)";
                VendInvDiscAmountLCY[i] := Rec."Inv. Discounts (LCY)";
                InvAmountsLCY[i] := Rec."Inv. Amounts (LCY)";
                VendPaymentDiscLCY[i] := Rec."Pmt. Discounts (LCY)";
                VendPaymentDiscTolLCY[i] := Rec."Pmt. Disc. Tolerance (LCY)";
                VendPaymentTolLCY[i] := Rec."Pmt. Tolerance (LCY)";
                VendReminderChargeAmtLCY[i] := Rec."Reminder Amounts (LCY)";
                VendFinChargeAmtLCY[i] := Rec."Fin. Charge Memo Amounts (LCY)";
                VendCrMemoAmountsLCY[i] := Rec."Cr. Memo Amounts (LCY)";
                VendPaymentsLCY[i] := Rec."Payments (LCY)";
                VendRefundsLCY[i] := Rec."Refunds (LCY)";
                VendOtherAmountsLCY[i] := Rec."Other Amounts (LCY)";
                RecGVendor.SETFILTER("Date Filter", VendDateFilter[i]);
                // AD Le 06-02-2015 => Pour avoir les achats sur la date doc.
                RecGVendor.CALCFIELDS("Purchases (LCY) Date Doc.");
                VendPurchLCYDoc[i] := RecGVendor."Purchases (LCY) Date Doc.";
                // FIN AD Le 06-02-2015
            end;
            Rec.SetRange("Date Filter", 0D, CurrentDate);
        end;
    end;

    var
        RecGVendor: Record Vendor;
        VendPurchLCYDoc: ARRAY[4] OF Decimal;

}