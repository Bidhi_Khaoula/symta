pageextension 51320 "Purchase Prices" extends "Purchase Prices" //7012
{

    layout
    {
        addafter("Vendor No.")
        {
            field("Vendor Name"; Rec."Vendor Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Name field.';
            }
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
        addafter("Item No.")
        {
            field("Vendor Item No."; Rec."Vendor Item No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Item No. field.';
            }
            field("Vendor Item Description"; Rec."Vendor Item Description")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Item No. field.';
            }
        }
        addafter("Ending Date")
        {
            field("Code Remise"; Rec."Code Remise")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Remise field.';
            }
            field(GetStatutQualite; Rec.GetStatutQualite())
            {
                Caption = 'Statut qualité';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut qualité field.';
            }
            field(GetStatutApprovisionnement; Rec.GetStatutApprovisionnement())
            {
                Caption = 'Statut approvisionnement';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut approvisionnement field.';
            }
        }
    }
}

