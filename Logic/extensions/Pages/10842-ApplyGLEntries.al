pageextension 50033 "Apply G/L Entries" extends "Apply G/L Entries" //10842
{
    layout
    {
        modify("Debit - Credit")
        {
            visible = false;
        }
        modify(Credit)
        {
            visible = false;
        }
        modify(Debit)
        {
            visible = false;
        }
        modify(ApplnCode)
        {
            visible = false;
        }
        addafter(Amount)
        {
            field("Source Type"; Rec."Source Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Source Type field.';
            }
            field("Source No."; Rec."Source No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Source No. field.';
            }
        }
        addafter("Debit - Credit")
        {
            field("Solde Lettrage"; "Solde Lettraage")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Solde Lettraage field.';
            }
        }
    }
    actions
    {
        addafter(UnapplyEntries)
        {
            action("Na&viguer")
            {
                Caption = '&Navigate';
                Image = Navigate;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Navigate action.';

                trigger OnAction();
                var
                    Navigate: Page Navigate;
                begin
                    Navigate.SetDoc(Rec."Posting Date", Rec."Document No.");
                    Navigate.RUN();
                end;
            }
        }
    }
    trigger OnAfterGetCurrRecord()
    begin
        "Solde Lettraage" := 0;

        GLE2.COPYFILTERS(Rec);
        GLE2.SETRANGE("Applies-to ID", USERID);
        IF GLE2.FIND('-') THEN
            REPEAT
                "Solde Lettraage" := GLE2.Amount + "Solde Lettraage";
            UNTIL GLE2.NEXT() = 0;
    end;

    var
        GLE2: Record "G/L Entry";
        "Solde Lettraage": Decimal;

}

