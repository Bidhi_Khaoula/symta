pageextension 50758 "Purch. Invoice Subform" extends "Purch. Invoice Subform" //55

{
    layout
    {
        modify("Unit Cost (LCY)")
        {
            Editable = false;
        }
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
        modify("Bin Code")
        {
            editable = false;
        }
        modify("Line No.")
        {
            Visible = true;
            Editable = false;
        }
        movebefore(Type; "Line No.")
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        modify("Gen. Prod. Posting Group")
        {
            Visible = true;
        }
        movebefore(Nonstock; "Gen. Prod. Posting Group")
        addafter("Return Reason Code")
        {
            field("Whse Receipt No."; Rec."Whse Receipt No.")
            {
                Caption = 'N° Réception';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the N° Réception field.';
            }
        }
        addafter("Line Amount")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
        }
        moveafter("Discount2 %"; AmountBeforeDiscount, InvoiceDiscountAmount, "Invoice Disc. Pct.")
        modify(AmountBeforeDiscount)
        {
            Visible = false;
        }
        addafter(Control15)
        {
            group(Control152)
            {
                ShowCaption = false;

                field("Diff. H.T"; DiffHT)
                {
                    Editable = False;
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the DiffHT field.';
                }
            }
        }
        movebefore("Diff. H.T"; "Invoice Disc. Pct.", InvoiceDiscountAmount)
    }

    actions
    {
        modify(GetReceiptLines)
        {
            trigger OnAfterAction()
            var
                PurchHeader: Record "Purchase Header";
            begin
                // MIG 2017
                PurchHeader.GET(rec."Document Type", rec."Document No.");
                IF PurchHeader."Prices Including VAT" THEN BEGIN
                    TotalAmount2 := TotalPurchaseLine.Amount;
                    TotalAmount1 := TotalAmount2 + VATAmount;
                    TotalPurchaseLine."Line Amount" := TotalAmount1 + TotalPurchaseLine."Inv. Discount Amount";
                END ELSE BEGIN
                    TotalAmount1 := TotalPurchaseLine.Amount;
                    TotalAmount2 := TotalPurchaseLine."Amount Including VAT";
                END;

                IF (TotalAmount1 <> 0) AND (TotalAmount2 <> 0) THEN
                    DiffHT := (PurchHeader."Montant HT facture" - TotalAmount2) / (TotalAmount2 / TotalAmount1);

                // fin MIG 2017

                CurrPage.UPDATE();
            end;
        }
    }
    var
        TotalAmount1: Decimal;
        TotalAmount2: Decimal;
        DiffHT: Decimal;

    trigger OnAfterGetCurrRecord()
    var
        PurchHeader: Record "Purchase Header";
    begin
        PurchHeader.GET(rec."Document Type", rec."Document No.");
        // ANI Le 02-12-2015 : erreur division par 0
        IF PurchHeader."Prices Including VAT" THEN BEGIN
            TotalAmount2 := TotalPurchaseLine.Amount;
            TotalAmount1 := TotalAmount2 + VATAmount;
            TotalPurchaseLine."Line Amount" := TotalAmount1 + TotalPurchaseLine."Inv. Discount Amount";
        END ELSE BEGIN
            TotalAmount1 := TotalPurchaseLine.Amount;
            TotalAmount2 := TotalPurchaseLine."Amount Including VAT";
        END;
        IF PurchHeader.GET(rec."Document Type", rec."Document No.") THEN;
        DiffHT := 0;
        IF TotalAmount1 > 0 THEN
            IF (TotalAmount2 / TotalAmount1) > 0 THEN
                DiffHT := (PurchHeader."Montant HT facture" - TotalAmount2) / (TotalAmount2 / TotalAmount1);

    end;



}