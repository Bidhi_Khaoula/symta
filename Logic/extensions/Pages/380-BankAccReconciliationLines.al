pageextension 50556 "Bank Acc. Reconciliation Lines" extends "Bank Acc. Reconciliation Lines" //380
{

    layout
    {
        addbefore("Transaction Date")
        {
            field(Select; Rec.Select)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Select field.';
            }
        }
        addbefore(Balance)
        {
            field("Montant Pointage"; "Montant pointage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant pointage field.';
            }
        }
        addafter(TotalDiff)
        {
            field(TotalBalanceTotalBalance; TotalBalance + Rec."Statement Amount")
            {
                Caption = 'Solde final';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Solde final field.';
            }
        }
    }


    var
        releveSoldeDernier: Decimal;
        "Montant pointage": Decimal;
        TotalBalance: Decimal;

    trigger OnAfterGetCurrRecord()
    begin
        if Rec."Statement Line No." <> 0 then
            CalcTotalBalance();
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        CalcTotalBalance();
    end;

    procedure calc_pointage(): Decimal;
    var
        tmp: Record "Bank Acc. Reconciliation Line";
    begin
        tmp.COPYFILTERS(Rec);
        tmp.SETCURRENTKEY("Bank Account No.", "Statement No.", Pointage);
        tmp.SETRANGE(Pointage, TRUE);
        tmp.SETFILTER("Statement No.", '<>%1', Rec."Statement No.");
        tmp.CALCSUMS("Statement Amount");

        IF Rec.Pointage THEN
            EXIT(releveSoldeDernier + tmp."Statement Amount" + Rec."Statement Amount")
        ELSE
            EXIT(releveSoldeDernier + tmp."Statement Amount");
    end;

    procedure recup_solde(_solde: Decimal);
    begin
        releveSoldeDernier := _solde;
    end;

    procedure CalcTotalBalance()
    var
        CopyBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line";
        BankAccReconciliation: Record "Bank Acc. Reconciliation";
    begin
        CopyBankAccReconciliationLine.Copy(Rec);
        if BankAccReconciliation.Get(Rec."Statement Type", Rec."Bank Account No.", Rec."Statement No.") then;

        TotalBalance := BankAccReconciliation."Balance Last Statement" - Rec."Statement Amount";
        if CopyBankAccReconciliationLine.CalcSums("Statement Amount") then
            TotalBalance := TotalBalance + CopyBankAccReconciliationLine."Statement Amount";
    end;

    procedure GetSelectedRecordsSymta(var TempBankAccReconciliationLine: Record "Bank Acc. Reconciliation Line" temporary)
    var
        BankAccReconciliationLine: Record "Bank Acc. Reconciliation Line";
    begin
        //CurrPage.SETSELECTIONFILTER(BankAccReconciliationLine);
        BankAccReconciliationLine.COPY(Rec);
        BankAccReconciliationLine.SETRANGE(Select, TRUE);
        if BankAccReconciliationLine.FindSet() then
            repeat
                TempBankAccReconciliationLine := BankAccReconciliationLine;
                TempBankAccReconciliationLine.Insert();
            until BankAccReconciliationLine.Next() = 0;
    end;
}

