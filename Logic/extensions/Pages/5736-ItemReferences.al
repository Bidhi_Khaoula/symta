pageextension 50931 "Item References" extends "Item References" //5736
{
    layout
    {
        addafter(Description)
        {
            field("publiable web"; Rec."publiable web")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the publiable web field.';
            }
            field("Code Constructeur"; Rec."Code Constructeur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Constructeur field.';
            }
        }
    }


}

