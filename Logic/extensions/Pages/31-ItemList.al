pageextension 50490 "Item List" extends "Item List" //31
{
    layout
    {

        addafter("No.")
        {
            field("No. 2"; Rec."No. 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. 2 field.';
            }
        }
        modify("Shelf No.")
        {
            Editable = false;
        }
        addafter("Item Tracking Code")
        {
            field("Date Dernière Sortie"; Rec."Date Dernière Sortie")
            {
                Caption = 'Last Output Date';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Last Output Date field.';
            }
            field("Stock Mort"; Rec."Stock Mort")
            {
                Caption = 'Died Inventory';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Died Inventory field.';
            }
            field("Phys Invt Counting Period Code"; Rec."Phys Invt Counting Period Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Phys Invt Counting Period Code field.';
            }
            field("Last Counting Period Update"; Rec."Last Counting Period Update")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Last Counting Period Update field.';
            }
            field("Last Phys. Invt. Date"; Rec."Last Phys. Invt. Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Last Phys. Invt. Date field.';
            }
            field("Sales multiple"; Rec."Sales multiple")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multiple de vente field.';
            }
            field("Gross Weight"; Rec."Gross Weight")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gross Weight field.';
            }
            field("Units per Parcel"; Rec."Units per Parcel")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Units per Parcel field.';
            }
            field("Unit Volume"; Rec."Unit Volume")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Volume field.';
            }
            field("Product Type"; Rec."Product Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Produit field.';
            }
            field("Process Blocked"; Rec."Process Blocked")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Processus bloqué field.';
            }
            field("Date dernière entrée"; Rec."Date dernière entrée")
            {
                Caption = 'Last Input Date';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Last Input Date field.';
            }
            field("Manufacturer Code"; Rec."Manufacturer Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Manufacturer Code field.';
            }
            field("Groupe Marque"; Rec."Groupe Marque")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Groupe Marque field.';
            }
            field("Groupe Famille"; Rec."Groupe Famille")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Groupe Famille field.';
            }
            field("Fusion en attente sur"; Rec."Fusion en attente sur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Fusion en attente sur field.';
            }
            field("Prix unitaire"; Rec.GetSalesPriceAllCustomer())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetSalesPriceAllCustomer() field.';
            }
            field("Emplacement par défaut"; Rec."Emplacement par défaut")
            {
                Caption = 'Default Bin';
                Editable = false;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Default Bin field.';
            }
            field("Description 3"; Rec."Description 3")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Informations vente field.';
            }
            field("Stocké"; Rec.Stocké)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Stocké field.';
            }
            field("Super Famille Marketing By Def"; Rec."Super Famille Marketing By Def")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Super Famille Marketing By Def field.';
            }
            field("Famille Marketing By Def"; Rec."Famille Marketing By Def")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Famille Marketing By Def field.';
            }
            field("Sous Famille Marketing By Def"; Rec."Sous Famille Marketing By Def")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sous Famille Marketing By Def field.';
            }
            field("Code Appro"; Rec."Code Appro")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Appro field.';
            }
            field(Publiable; Rec.Publiable)
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Publiable field.';
            }
        }
        addafter("Default Deferral Template Code")
        {
            field("Comodity Code"; Rec."Comodity Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the PCC/CNH field.';
            }
            field("Code Concurrence"; Rec."Code Concurrence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the MPL - MPC/CNH field.';
            }
        }
        modify("Unit Cost")
        {
            Visible = false;
        }
        addafter("Unit Cost")
        {
            field("Unit Cost2"; Rec."Unit Cost")
            {
                ApplicationArea = Basic, Suite;
                DecimalPlaces = 0 : 5;
                ToolTip = 'Specifies the cost per unit of the item.';
            }

        }
        addbefore(PowerBIEmbeddedReportPart)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
        }
        modify("Description 2")
        {
            Visible = true;
        }
        moveafter("Emplacement par défaut"; "Description 2")
    }
    actions
    {
        modify("<Action32>")
        {
            Visible = false;
        }
        addafter("<Action32>")
        {
            action("<Action32>2")
            {
                ApplicationArea = Assembly;
                Caption = 'Assembly BOM';
                Image = BOM;
                RunObject = Page "Assembly BOM";
                RunPageMode = View;
                RunPageLink = "Parent Item No." = field("No.");
                ToolTip = 'View or edit the bill of material that specifies which items and resources are required to assemble the assembly item.';
            }
        }
        addafter("E&xtended Texts")
        {
            action("Liste des familles")
            {
                Caption = 'Liste des familles';
                Image = MapSetup;
                Promoted = true;
                PromotedCategory = Process;
                RunObject = Page "Item Hierarchies";
                RunPageLink = "Code Article" = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Liste des familles action.';
            }
        }
        addafter("Inventory - Sales Back Orders")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }

    }

    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("No. 2");
    end;
}

