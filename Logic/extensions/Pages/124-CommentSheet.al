pageextension 50632 "Comment Sheet" extends "Comment Sheet" //124
{
    layout
    {
        addbefore(Date)
        {
            field("Table Name"; Rec."Table Name")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Table Name field.';
            }
        }
        addafter(Code)
        {
            field("Afficher Multiconsultation"; Rec."Afficher Multiconsultation")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Afficher Multiconsultation field.';
            }
            field("Afficher Appro"; Rec."Afficher Appro")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Afficher Appro field.';
            }
            field("Print Wharehouse Shipment"; Rec."Print Wharehouse Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression exp. magasin field.';
            }
            field("Print Shipment"; Rec."Print Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression expédition field.';
            }
            field("Print Invoice"; Rec."Print Invoice")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression facture field.';
            }
            field("Display Sales Order"; Rec."Display Sales Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Affichage commande Vente/Devis/Retour field.';
            }
            field("Print Purch. Order"; Rec."Print Purch. Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression Commande achat field.';
            }
            field("Display Purch. Receipt"; Rec."Display Purch. Receipt")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Affichage Réception Magasin field.';
            }
            field("Display Web"; Rec."Display Web")
            {
                Enabled = isItemComment;
                Caption = 'Item Comment Only';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Item Comment Only field.';
            }
            field("Language Web"; Rec."Language Web")
            {
                Enabled = isItemComment;
                caption = 'Item Comment Only';
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Item Comment Only field.';
            }
            field("No."; Rec."No.")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the No. field.';
            }
            field("No. 2"; Rec."No. 2")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the No. 2 field.';
            }
        }
    }

    var
        isItemComment: Boolean;

    trigger OnOpenPage()
    begin
        // CFR Le 17/05/2021 => SFD20210423 : Commentaire article web
        IF (Rec."Table Name" = Rec."Table Name"::Item) THEN
            isItemComment := TRUE
        ELSE
            isItemComment := FALSE;
    end;

    trigger OnAfterGetRecord()
    begin
        // CFR Le 17/05/2021 => SFD20210423 : Commentaire article web
        IF (Rec."Table Name" = Rec."Table Name"::Item) THEN
            isItemComment := TRUE
        ELSE
            isItemComment := FALSE;
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        // CFR Le 17/05/2021 => SFD20210423 : Commentaire article web
        IF (Rec."Table Name" = Rec."Table Name"::Item) THEN
            isItemComment := TRUE
        ELSE
            isItemComment := FALSE;
    end;
}