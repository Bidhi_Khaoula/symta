pageextension 50182 "Sales Statistics" extends "Sales Statistics" //160
{
    layout
    {
        addafter(TotalAmount2)
        {
            field("Gross Total"; TotalAmountBrut)
            {
                Caption = 'Gross Total';
                AutoFormatType = 1;
                AutoFormatExpression = 'Currency Code';
                Editable = FALSE;
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Gross Total field.';
            }
        }
    }

    procedure FctSetTotalAmountBrut(DecPTotalAmountBrut: Decimal)
    begin
        TotalAmountBrut := DecPTotalAmountBrut;
    end;

    var
        TotalAmountBrut: Decimal;
}