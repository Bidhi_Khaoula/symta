pageextension 51671 "Sales Order List" extends "Sales Order List" //9305
{

    layout
    {
        modify("Bill-to Customer No.")
        {
            Visible = true;
        }
        modify("Ship-to Post Code")
        {
            Visible = true;
        }
        modify("Posting Date")
        {
            Visible = true;
        }
        modify("Salesperson Code")
        {
            Visible = true;
        }
        modify("Assigned User ID")
        {
            Visible = false;
        }
        modify("Sell-to Customer No.")
        {
            ToolTip = 'Specifies the number of the customer who will receive the products and be billed by default.';
            StyleExpr = gStyleExp_Export;
        }
        modify("Sell-to Customer Name")
        {
            ToolTip = 'Specifies the name of the customer who will receive the products and be billed by default.';
            StyleExpr = gStyleExp_Export;
        }
        addbefore("No.")
        {
            field("Order Date"; Rec."Order Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
        }
        addafter("No.")
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        addafter("Sell-to Customer Name")
        {
            field("Code Enseigne 1"; Rec."Code Enseigne 1")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 1 field.';
            }
            field("Code Enseigne 2"; Rec."Code Enseigne 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Enseigne 2 field.';
            }
        }
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Shipment Status"; Rec."Shipment Status")
            {
                Caption = 'Statut livraison';
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut livraison field.';
            }
        }
        moveafter("Order Create User"; Status)
        moveafter("Shipment Status"; "Completely Shipped")
        addafter("Ship-to Post Code")
        {
            field("Ship-to City"; Rec."Ship-to City")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ship-to City field.';
            }
        }
        moveafter("Job Queue Status"; Amount)
        addafter(Amount)
        {
            field(Marge; Rec.CalcCost(1))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the CalcCost(1) field.';
            }
            field("% Marge"; Rec.CalcCost(2))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the CalcCost(2) field.';
            }
            field("Statut Livraison"; GetStatutLivraison())
            {
                Editable = false;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetStatutLivraison() field.';
            }
            field("Source Document Type"; Rec."Source Document Type")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
        }
        addafter("Amount Including VAT")
        {
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
        }
        addbefore("Attached Documents")
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
        }
    }
    actions
    {
        modify("Create &Warehouse Shipment")
        {
            Visible = false;
        }
        addafter(Release)
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    FrmQuid.InitClient(Rec."Sell-to Customer No.");
                    FrmQuid.RUNMODAL();
                end;
            }
        }

    }


    var
        gStyleExp_Export: Text;

    local procedure GetStatutLivraison() status_livraison: Text[80];
    var
        rec_salesline: Record "Sales Line";
        _qte_cde: Decimal;
        _qte_exp: Decimal;
    begin
        //LM le 27-05-2013  =>'shipment status' erroné, correctif ci dessous
        _qte_cde := 0;
        _qte_exp := 0;
        rec_salesline.RESET();
        rec_salesline.SETRANGE("Document Type", Rec."Document Type");
        rec_salesline.SETRANGE(rec_salesline."Document No.", Rec."No.");
        rec_salesline.CALCSUMS(Quantity, "Quantity Shipped");
        status_livraison := 'A livrer';
        IF rec_salesline."Quantity Shipped" > 0 THEN status_livraison := 'Reliquats';
        IF rec_salesline."Quantity Shipped" = rec_salesline.Quantity THEN status_livraison := 'Livrée';

        /*
        IF rec_salesline.FINDFIRST () THEN
          REPEAT
            _qte_cde+=rec_salesline.Quantity;
            _qte_exp+=rec_salesline."Quantity Shipped";
          UNTIL rec_salesline.NEXT=0;
          status_livraison:='A livrer';
          IF _qte_exp>0 THEN status_livraison:='Reliquats';
          IF _qte_cde=_qte_exp THEN status_livraison:='Livrée';
         */

    end;

    local procedure SetStyleExp_Export();
    begin
        //CFR le 05/04/2023 => Régie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        IF (Rec."Salesperson Code" = '19') THEN
            gStyleExp_Export := 'Unfavorable';
        //FIN CFR le 05/04/2023
    end;

    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("Document Type", "No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;

    trigger OnAfterGetRecord()
    begin
        // CFR le 05/04/2023 => R‚gie : [Couleur] sur client 19 [EXPORT]
        SetStyleExp_Export();
        // FIN CFR le 05/04/2023
        // CFR le 18/04/2024 - R‚gie : Reprise du code de la page 42 pour le statut livr‚ : non car on perd le filtre !!
        /*{
          //LM le 27-05-2013  =>'shipment status' erron‚, correctif ci dessous
          _qte_cde:=0;
          _qte_exp:=0;
          rec_salesline.RESET();
          rec_salesline.SETRANGE("Document Type", "Document Type");
          rec_salesline.SETRANGE("Document No.",  "No.");
          IF rec_salesline.FINDSET() THEN
            REPEAT
              _qte_cde += rec_salesline.Quantity;
              _qte_exp += rec_salesline."Quantity Shipped";
            UNTIL rec_salesline.NEXT() = 0;
          gStatusLivraison := 'A livrer';
          IF (_qte_exp > 0)       THEN gStatusLivraison := 'Reliquats';
          IF (_qte_cde=_qte_exp)  THEN gStatusLivraison := 'Livr‚e';
        }*/
        // FIN CFR le 18/04/2024
    end;
}

