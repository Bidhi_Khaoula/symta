pageextension 50937 "Item Reference List" extends "Item Reference List" //5735
{
    layout
    {
        addafter(Description)
        {
            field("Code Constructeur"; Rec."Code Constructeur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Constructeur field.';
            }
        }
    }
}

