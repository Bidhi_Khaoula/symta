pageextension 51350 "Warehouse Shipment List" extends "Warehouse Shipment List" //7339
{
    layout
    {
        addafter("Assigned User ID")
        {
            field(GetDestinationName; Rec.GetDestinationName())
            {
                Caption = 'Donneur d''ordre';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Donneur d''ordre field.';
            }
        }
        modify("Shipment Method Code")
        {
            Visible = true;
        }
        addafter("Shipment Method Code")
        {
            field("Source No."; Rec."Source No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Source No. field.';
            }
            field("Destination Type"; Rec."Destination Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Destination Type field.';
            }
            field("Destination No."; Rec."Destination No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Destination No. field.';
            }
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
            field("Packing List No."; Rec."Packing List No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° liste de colisage field.';
            }
            field("Date Création"; Rec."Date Création")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Création field.';
            }
            field("Heure Création"; Rec."Heure Création")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Création field.';
            }
            field("Utilisateur Création"; Rec."Utilisateur Création")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Utilisateur Création field.';
            }
            field("Date Impression"; Rec."Date Impression")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Impression field.';
            }
            field("Heure Impression"; Rec."Heure Impression")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Impression field.';
            }
            field("Date Flashage"; Rec."Date Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Flashage field.';
            }
            field("Heure Flashage"; Rec."Heure Flashage")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Flashage field.';
            }
        }
    }
    actions
    {

    }
}
