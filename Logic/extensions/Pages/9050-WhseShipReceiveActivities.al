pageextension 51572 "Whse Ship & Receive Activities" extends "Whse Ship & Receive Activities" //9050
{
    layout
    {
        modify("Rlsd. Sales Orders Until Today")
        {
            Visible = false;
        }
        modify("Exp. Purch. Orders Until Today")
        {
            Visible = false;
        }
        modify(Internal)
        {
            Visible = false;
        }
        addafter("Rlsd. Sales Orders Until Today")
        {
            field("Whse Shipment"; Rec."Whse Shipment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Expéditiion field.';
            }
        }
        addafter("Exp. Purch. Orders Until Today")
        {
            field("Whse Receip"; Rec."Whse Receip")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Réception field.';
            }
        }
    }

}

