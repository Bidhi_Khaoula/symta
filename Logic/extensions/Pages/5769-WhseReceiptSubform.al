pageextension 50941 "Whse. Receipt Subform" extends "Whse. Receipt Subform" //5769
{

    layout
    {
        modify("Qty. to Receive")
        {
            Caption = 'Qty. to Receive';
            Style = Strong;
            StyleExpr = TRUE;
            trigger OnBeforeValidate()
            begin
                // CFR le 22/03/2023 - R‚gie : modifier la tarification des lignes … la r‚ception
                gWarehouseReceiptManagement.ChangeAmountLineOrderFromLineReceipt(Rec, dec_PrixBrut, dec_Remise1, dec_Remise2);
                CurrPage.UPDATE();
                // Fin CFR le 22/03/2023  
            end;
        }
        modify("Qty. Received")
        {
            Caption = 'Qty. Received';
            Importance = Promoted;
        }

        addafter(Control1)
        {
            field("Line No."; Rec."Line No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
        }
        addafter("Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
        modify(Description)
        {
            Visible = false;
        }
        addafter("Variant Code")
        {
            field("Référence fournisseur"; Rec."Référence fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence fournisseur field.';
            }
            field("Vendor Shipment No."; Rec."Vendor Shipment No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
            }
            field("Désignation"; txt_Desc)
            {
                Caption = 'Désignation  (ligne commande)';
                Editable = true;
                ToolTip = 'Specifies the description of the item in the line.';
                ApplicationArea = All;

                trigger OnValidate();
                begin
                    Rec.SetInfoLigneText('DESC', txt_Desc); // MCO Le 20-11-2018 => Régie
                end;
            }
            field("Vendor Order No."; Rec."Vendor Order No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Order No. field.';
            }
            field("Vendor Order Type"; Rec."Vendor Order Type")
            {
                Visible = VisibleOrderType;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Vendor Order Type field.';
            }
        }
        modify("Zone Code")
        {
            Visible = false;
        }
        addafter("Location Code")
        {
            field("Source Line No."; Rec."Source Line No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Source Line No. field.';
            }
        }

        addafter("Shelf No.")
        {
            field("Close Line"; Rec."Close Line")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Solder ligne field.';
            }
            field("Qte Reçue théorique"; Rec."Qte Reçue théorique")
            {
                Caption = 'Qté/FAC ou BL';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qté/FAC ou BL field.';
            }
            field("Litige prix"; Rec."Litige prix")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Litige prix field.';
            }
        }
        addafter("Qty. Received")
        {
            field("Prix Brut"; dec_PrixBrut)
            {
                DecimalPlaces = 2 : 5;
                Importance = Promoted;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the dec_PrixBrut field.';

                trigger OnValidate();
                begin
                    // CFR le 22/03/2023 - Régie : modifier la tarification des lignes à la réception
                    //SetInfoLigne('PXBRUT', dec_PrixBrut);
                    gWarehouseReceiptManagement.ChangeAmountLineOrderFromLineReceipt(Rec, dec_PrixBrut, dec_Remise1, dec_Remise2);
                    CurrPage.UPDATE();
                    // FIN CFR le 22/03/2023
                end;
            }
            field(Remise1; dec_Remise1)
            {
                Caption = 'Remise 1';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 1 field.';

                trigger OnValidate();
                begin
                    // CFR le 22/03/2023 - Régie : modifier la tarification des lignes à la réception
                    //SetInfoLigne('REMISE1', dec_Remise1);
                    gWarehouseReceiptManagement.ChangeAmountLineOrderFromLineReceipt(Rec, dec_PrixBrut, dec_Remise1, dec_Remise2);
                    CurrPage.UPDATE();
                    // FIN CFR le 22/03/2023
                end;
            }
            field("GetInfoLigne('NET1')"; Rec.GetInfoLigne('NET1'))
            {
                Caption = 'NET 1';
                DecimalPlaces = 2 : 5;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the NET 1 field.';
            }
            field("GetInfoLigne('TOTALNET1')"; Rec.GetInfoLigne('TOTALNET1'))
            {
                Caption = 'Total Net 1';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total Net 1 field.';
            }
            field(Remise2; dec_Remise2)
            {
                Caption = 'Remise 2';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 2 field.';

                trigger OnValidate();
                begin
                    // CFR le 22/03/2023 - Régie : modifier la tarification des lignes à la réception
                    //SetInfoLigne('REMISE2', dec_Remise2); // AD Le 30-01-2020 => Correction Bug avant dec_Remise1
                    gWarehouseReceiptManagement.ChangeAmountLineOrderFromLineReceipt(Rec, dec_PrixBrut, dec_Remise1, dec_Remise2);
                    CurrPage.UPDATE();
                    // FIN CFR le 22/03/2023
                end;
            }
            field("GetInfoLigne('NET2')"; Rec.GetInfoLigne('NET2'))
            {
                Caption = 'Prix Net';
                DecimalPlaces = 2 : 5;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix Net field.';
            }
            field("GetInfoLigne('TOTALNET2')"; Rec.GetInfoLigne('TOTALNET2'))
            {
                Caption = 'Total Net';
                Style = Strong;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total Net field.';
            }
        }
        addafter("Qty. per Unit of Measure")
        {
            field("Receipt Adjustment"; Rec."Receipt Adjustment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Régularisation Réception field.';
            }
            field("Receipt Request"; Rec."Receipt Request")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Demande Réception field.';
            }
            field("rec_purchline.""Requested Receipt Date"""; rec_purchline."Requested Receipt Date")
            {
                Caption = 'Date de réception demandée';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date de réception demandée field.';
            }
            field("rec_purchline.""Promised Receipt Date"""; rec_purchline."Promised Receipt Date")
            {
                Caption = 'Date de réception confirmée';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date de réception confirmée field.';
            }
        }
    }
    actions
    {
        addafter(ItemTrackingLines)
        {
            action(MultiCOnsultation)
            {
                Caption = 'Multi Consultation';
                ApplicationArea = All;
                Image = Open;
                ToolTip = 'Executes the Multi Consultation action.';
                trigger OnAction();
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
    }

    var
        rec_purchline: Record "Purchase Line";
        gWarehouseReceiptManagement: Codeunit "Warehouse Receipt Management";
        ref_fou: Text[50];
        dec_PrixBrut: Decimal;
        dec_Remise1: Decimal;
        dec_Remise2: Decimal;
        VisibleOrderType: Boolean;
        txt_Desc: Text;

    trigger OnAfterGetRecord()
    begin
        ref_fou := '';
        rec_purchline.RESET();
        rec_purchline.SETRANGE(rec_purchline."Document No.", Rec."Source No.");
        rec_purchline.SETRANGE(rec_purchline."Line No.", Rec."Source Line No.");

        IF rec_purchline.FINDFIRST() THEN
            ref_fou := rec_purchline."Item Reference No.";


        dec_Remise1 := Rec.GetInfoLigne('REMISE1');
        dec_Remise2 := Rec.GetInfoLigne('REMISE2');
        dec_PrixBrut := Rec.GetInfoLigne('PXBRUT');
        txt_Desc := Rec.GetInfoLigneTexte('DESC'); // MCO Le 20-11-2018 => R‚gie

        // ---------------
        // PMA le 28-03-18
        // ---------------
        //VisibleOrderType := ("Source Document"="Source Document"::"Purchase Order");
        VisibleOrderType := TRUE;
    end;

    procedure GetPrixNet(): Decimal;
    var
        ligneAchat: Record "Purchase Line";
    begin
        IF NOT ligneAchat.GET(ligneAchat."Document Type"::Order, Rec."Source No.", Rec."Source Line No.") THEN
            EXIT(0)
        ELSE
            EXIT(ligneAchat."Net Unit Cost");
    end;

    procedure GetPrixBrut(): Decimal;
    var
        ligneAchat: Record "Purchase Line";
    begin
        IF NOT ligneAchat.GET(ligneAchat."Document Type"::Order, Rec."Source No.", Rec."Source Line No.") THEN
            EXIT(0)
        ELSE
            EXIT(ligneAchat."Direct Unit Cost");
    end;

    procedure PositionEnBas();
    begin
        Rec.FINDLAST();
        CurrPage.UPDATE(FALSE);
    end;

    procedure MultiConsultation2();
    var
    // WhseRcptLine: Record "Warehouse Receipt Line";
    begin

        //WhseRcptLine.COPY(Rec);
        //WhseRcptLine.SETRECFILTER();
        //ERROR('ad :%1 - %2', "Item No.", WhseRcptLine."Line No.");
        //WhseRcptLine.
        Rec.OuvrirMultiConsultation();
    end;

    local procedure CheckRecFilters();
    var
        lText001Err: Label 'Vous devez supprimer le filtre posé sur les lignes avant de valider cette réception.';
    begin
        // CFR le 15/09/2021 => Régie : Validation des réception en totalité
        IF (Rec.GETFILTERS() <> '') THEN
            ERROR(lText001Err);
    end;


    procedure WhsePostRcptYesNoSymta()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        // CFR le 15/09/2021 => R‚gie : Validation des r‚ception en totalit‚
        CheckRecFilters();
        // FIN CFR le 15/09/2021
        WhseRcptLine.Copy(Rec);
        CODEUNIT.Run(CODEUNIT::"Whse.-Post Receipt (Yes/No)", WhseRcptLine);
        Rec.Reset();
        Rec.SetCurrentKey("No.", "Sorting Sequence No.");
        CurrPage.Update(false);
    end;

    procedure WhsePostRcptPrintSymta()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        // CFR le 15/09/2021 => R‚gie : Validation des r‚ception en totalit‚
        CheckRecFilters();
        // FIN CFR le 15/09/2021

        WhseRcptLine.Copy(Rec);
        CODEUNIT.Run(CODEUNIT::"Whse.-Post Receipt + Print", WhseRcptLine);
        Rec.Reset();
        Rec.SetCurrentKey("No.", "Sorting Sequence No.");
        CurrPage.Update(false);
    end;

    procedure WhsePostRcptPrintPostedRcptSymta()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        // CFR le 15/09/2021 => R‚gie : Validation des r‚ception en totalit‚
        CheckRecFilters();
        // FIN CFR le 15/09/2021

        WhseRcptLine.Copy(Rec);
        CODEUNIT.Run(CODEUNIT::"Whse.-Post Receipt + Pr. Pos.", WhseRcptLine);
        Rec.Reset();
        CurrPage.Update(false);
    end;
}

