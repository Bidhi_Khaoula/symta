pageextension 51361 "Item Bin Contents" extends "Item Bin Contents" //7379
{

    layout
    {
        addafter("Item No.")
        {
            field("Référence Active"; Rec."Référence Active")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
        addafter("Zone Code")
        {
            field("Date dernier Mvt"; Rec.GetCommentaireInfoEntrée('DATE'))
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCommentaireInfoEntrée(''DATE'') field.';
            }
            field("Commentaire dernier Mvt."; Rec.GetCommentaireInfoEntrée('COMMENT'))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCommentaireInfoEntrée(''COMMENT'') field.';
            }
            field("Return Vendor Name"; Rec."Return Vendor Name")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Name field.';
            }
            field("capacité"; Rec.capacité)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the capacité field.';
            }
            field("% Seuil Capacité Réap. Pick."; Rec."% Seuil Capacité Réap. Pick.")
            {
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Seuil Capacité Réap. Pick. field.';
            }
            field("Last Movement Date"; Rec."Last Movement Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Quantity (Base) field.';
            }
            field("Last Entry Date"; Rec."Last Entry Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date dernière entrée field.';
            }
            field("Bin Content Creation Date"; Rec."Bin Content Creation Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date création contenu emplacement field.';
            }
        }
    }
    trigger OnOpenPage()
    begin
        //FBRUN LE 23/12/22 tickert 11-14122022
        Rec.SETCURRENTKEY("Item No.", "Location Code", Quantity);
        Rec.ASCENDING(FALSE);
        IF Rec.FINDFIRST() THEN;
        //FIN FBRUN
    END;
}

