pageextension 51292 "Purchase Return Order Subform" extends "Purchase Return Order Subform" //6641
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
        modify("Bin Code")
        {
            editable = false;
        }
        modify("Location Code")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
                Rec.CheckUpdateQtyOnReturn();
                // FIN MCO Le 15-01-2018
            end;
        }
        modify(Quantity)
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
                Rec.CheckUpdateQtyOnReturn();
                // FIN MCO Le 15-01-2018
            end;
        }
        modify("Unit of Measure")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
                Rec.CheckUpdateQtyOnReturn();
                // FIN MCO Le 15-01-2018
            end;
        }
        modify("Unit of Measure Code")
        {
            trigger OnAfterValidate()
            begin
                // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
                Rec.CheckUpdateQtyOnReturn();
                // FIN MCO Le 15-01-2018
            end;
        }
        addafter(Control1)
        {
            field("Line No."; Rec."Line No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Line No. field.';
            }
            field("Regularisation retour"; Rec."Regularisation retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Regularisation retour field.';
            }
            field("Demande retour"; Rec."Demande retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Demande retour field.';
            }
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';

                trigger OnValidate();
                begin
                    // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
                    Rec.CheckUpdateQtyOnReturn();
                    // FIN MCO Le 15-01-2018
                end;
            }
        }
        addafter("No.")
        {
            field("Expected Receipt Date"; Rec."Expected Receipt Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Expected Receipt Date field.';
            }
        }
        addafter("Bin Code")
        {
            field("Location Code Temp"; Rec."Location Code Temp")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Return Location Code field.';
            }
            field("Bin Code Temp"; Rec."Bin Code Temp")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Return Bin Code field.';
            }
        }
        addafter("Line Amount")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
        }
        addafter("ShortcutDimCode8")
        {
            field(GetPamp; Rec.GetPamp())
            {
                Caption = 'Item Cost';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Item Cost field.';
            }
        }
    }
    actions
    {

        addafter("F&unctions")
        {
            action("Ajuster Quantité")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Ajuster Quantité action.';
                trigger OnAction();
                begin
                    Rec.ArrondirQte(0);
                end;
            }
        }
    }


    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;

}

