pageextension 50173 "Purchase Journal" extends "Purchase Journal" //254
{
    layout
    {
        addafter(Comment)
        {
            field("VAT Base Amount"; Rec."VAT Base Amount")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the VAT Base Amount field.';
            }
        }
        modify("Amount (LCY)")
        {
            Visible = true;
        }
        moveafter("VAT Base Amount"; Correction, "Amount (LCY)")

    }
}

