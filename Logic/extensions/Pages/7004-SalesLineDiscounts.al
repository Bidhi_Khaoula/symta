pageextension 51307 "Sales Line Discounts" extends "Sales Line Discounts" //7004
{
    layout
    {
        modify(SalesTypeFilter)
        {
            OptionCaption = 'Customer,Sous Groupe tarif,Centrale,Customer Discount Group,All Customers,Campaign,None';
        }

        addbefore(SalesType)
        {
            field("Type de commande"; Rec."Type de commande")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type de commande field.';
            }
        }
        addafter(SalesCode)
        {
            field("Code Marque"; Rec."Code Marque")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Marque field.';
            }
        }
        addafter("Code")
        {
            field("Code Famille"; Rec."Code Famille")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Famille field.';
            }
            field("Code Sous Famille"; Rec."Code Sous Famille")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Sous Famille field.';
            }
            field("Code Article"; Rec."Code Article")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Article field.';
            }
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
        modify("Line Discount %")
        {
            Editable = false;
        }
        addafter("Line Discount %")
        {
            field("Line Discount 1 %"; Rec."Line Discount 1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise ligne 1 field.';
            }
            field("Line Discount 2 %"; Rec."Line Discount 2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise ligne 2 field.';
            }
        }
    }

}

