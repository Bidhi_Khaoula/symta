pageextension 50000 "Company Information" extends "Company Information" //1
{
    layout
    {
        addafter(Picture)
        {
            field("Picture Address"; Rec."Picture Address")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Image avec adresse field.';
            }
        }
        addafter("System Indicator")
        {
            group(EDI)
            {
                field("Company EDI Code"; Rec."Company EDI Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Code EDI Société field.';
                }
                field("Import EDI Folder"; Rec."Import EDI Folder")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Répertoire Import EDI field.';
                }
                field("Export EDI Folder"; Rec."Export EDI Folder")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Répertoire Export EDI field.';
                }
            }
        }
        modify(CISD)
        {
            Visible = false;
        }
        addafter(CISD)
        {
            field(EORI; Rec.EORI)
            {
                ToolTip = 'Le numéro EORI (Economic Operator Registration and Identification) doit être utilisé pour l''identification des opérateurs économiques et d''autres personnes dans leurs relations avec les autorités douanières (importation ou exportation de marchandises hors de lÉUnion européenne).';
                ApplicationArea = All;
            }
        }
    }
}