pageextension 51566 "Order Processor Role Center" extends "Order Processor Role Center" //9006
{
    layout
    {
        modify(Control1901851508)
        {
            Visible = false;
        }
    }
    actions
    {

        addafter("Sales Orders - Microsoft Dynamics 365 Sales")
        {
            action(MySalesOrders)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Sales Orders';
                Image = "Order";
                RunObject = Page "My Sales Order List";
                ToolTip = 'Open the list of sales orders where you can sell items and services.';
            }
        }
        modify("Blanket Sales Orders")
        {
            Visible = false;
        }
        addafter("Sales &Quote")
        {
            action("Mes Devis")
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Sales Quotes';
                Image = Quote;
                RunObject = Page "My Sales Quotes";
                ToolTip = 'Open the list of sales quotes where you offer items or services to customers.';
            }
            action(MyPick)
            {
                Caption = 'Mes pics en cours';
                RunObject = Page "My Warehouse Shipment List";
                ApplicationArea = All;
                Image = Navigate;
                ToolTip = 'Executes the Mes pics en cours action.';
            }
        }
        addafter("Sales Credit Memos")
        {
            action("Devis Web")
            {
                Caption = 'Devis Web';
                RunObject = Page "Sales Quotes Web";
                ApplicationArea = All;
                Image = Navigate;
                ToolTip = 'Executes the Devis Web action.';
            }
        }

        addafter(Customers)
        {
            action(Litiges)
            {
                Caption = 'Litiges';
                RunObject = Page "Liste Incident Transport";
                ApplicationArea = All;
                Image = Navigate;
                ToolTip = 'Executes the Litiges action.';
            }
            action("Reliquats Clients")
            {
                Caption = 'Reliquats Clients';
                RunObject = Page "Sales Lines";
                RunPageView = SORTING("Document Type", "Document No.", "Line No.") ORDER(Ascending) WHERE("Document Type" = CONST(Order), "Outstanding Quantity" =
FILTER(<> 0), Type = CONST(Item));
                ApplicationArea = All;
                ToolTip = 'Executes the Reliquats Clients action.';
            }
        }
        addafter("Posted Sales Invoices")
        {
            action("Devis Archivés")
            {
                Caption = 'Devis Archivés';
                RunObject = Page "Sales Quote Archives";
                Image = Archive;
                ApplicationArea = All;
                ToolTip = 'Executes the Devis Archivés action.';
            }
            action("Commandes vente archivées")
            {
                Caption = 'Commandes vente archivées';
                RunObject = Page "Sales Order Archives";
                Image = Archive;
                ApplicationArea = All;
                ToolTip = 'Executes the Commandes vente archivées action.';
            }
        }
        addafter("Navi&gate")
        {
            action("Multi-Consultation")
            {
                Caption = 'Multi-Consultation';
                Image = CalculateConsumption;
                RunObject = Page "Multi -consultation";
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';
            }
        }
    }
}

