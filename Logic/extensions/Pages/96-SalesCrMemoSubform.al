pageextension 51280 "Sales Cr. Memo Subform" extends "Sales Cr. Memo Subform" //96
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Bin Code")
        {
            Editable = false;
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
        addafter("Line Amount")
        {
            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }

        }
        modify("Line Discount %")
        {
            Visible = false;
            Editable = false;
        }
    }
}