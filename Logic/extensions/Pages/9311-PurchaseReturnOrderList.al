pageextension 51711 "Purchase Return Order List" extends "Purchase Return Order List" //9311
{
    layout
    {
        addafter("Vendor Authorization No.")
        {
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
        }
    }
    actions
    {

    }

}

