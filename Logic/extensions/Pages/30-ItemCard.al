pageextension 50515 "Item Card" extends "Item Card" //30
{

    layout
    {
        modify(Item)
        {
            Editable = Editable;
        }
        modify("Shelf No.")
        {
            Editable = false;
        }
        modify("Gross Weight")
        {
            Editable = false;
        }
        modify("Net Weight")
        {
            Editable = false;
        }
        modify("Unit Volume")
        {
            Editable = false;
        }
        modify(InventoryGrp)
        {
            Editable = Editable;
        }
        modify("Prices & Sales")
        {
            Editable = Editable;
        }
        modify("Cost Details")
        {
            Editable = Editable;
        }
        modify(ForeignTrade)
        {
            Editable = Editable;
        }
        modify(Replenishment)
        {
            Editable = Editable;
        }
        modify(Purchase)
        {
            Editable = Editable;
        }
        modify(Replenishment_Production)
        {
            Editable = Editable;
        }
        modify(Replenishment_Assembly)
        {
            Editable = Editable;
        }
        modify(Planning)
        {
            Editable = Editable;
        }
        modify(ReorderPointParameters)
        {
            Editable = Editable;
        }
        modify(OrderModifiers)
        {
            Editable = Editable;
        }
        modify(ItemTracking)
        {
            Editable = Editable;
        }
        modify(Warehouse)
        {
            Editable = Editable;
        }
        modify("No.")
        {
            Visible = true;
            Importance = Standard;
        }


        modify("Include Inventory")
        {
            Importance = Additional;
        }
        modify("Lot Accumulation Period")
        {
            Importance = Additional;
        }
        modify("Rescheduling Period")
        {
            Importance = Additional;
        }

        modify(InventoryNonFoundation)
        {
            Visible = false;
        }

        addafter("No.")
        {
            field("No. 2"; Rec."No. 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. 2 field.';
            }
        }
        modify("Description 2")
        {
            Visible = true;
        }
        addafter("Description 2")
        {
            field("Description 3"; Rec."Description 3")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Informations vente field.';
            }
        }
        modify(Blocked)
        {
            Visible = false;
        }
        modify("Manufacturer Code")
        {
            Visible = true;
        }
        moveafter(GTIN; "Manufacturer Code")
        addafter("Item Category Code")
        {
            field("Gencod EAN13"; Rec."Gencod EAN13")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Gencod EAN13 field.';
            }
        }
        addafter("Automatic Ext. Texts")
        {
            field("Create Date"; Rec."Create Date")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Création field.';
            }
        }
        addafter("Created From Nonstock Item")
        {
            field("Catégorie produit"; Rec."Catégorie produit")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Catégorie produit field.';
            }
            field("Product Type"; Rec."Product Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Produit field.';
            }
        }
        modify(Inventory)
        {
            Visible = false;
        }
        addafter(Inventory)
        {
            field(Inventory2; Rec.Inventory)
            {
                ApplicationArea = Basic, Suite;
                Enabled = IsInventoriable;
                HideValue = IsNonInventoriable;
                Importance = Promoted;
                ToolTip = 'Specifies how many units, such as pieces, boxes, or cans, of the item are in inventory.';
                trigger OnAssistEdit()
                var
                    AdjustInventory: Page "Adjust Inventory";
                    RecRef: RecordRef;
                begin
                    RecRef.GetTable(Rec);

                    if RecRef.IsDirty() then begin
                        Rec.Modify(true);
                        Commit();
                    end;

                    AdjustInventory.SetItem(Rec."No.");
                    if AdjustInventory.RunModal() in [ACTION::LookupOK, ACTION::OK] then
                        Rec.Get(Rec."No.");
                    CurrPage.Update()
                end;
            }
        }
        addafter(PreventNegInventoryDefaultNo)
        {
            field("Process Blocked"; Rec."Process Blocked")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Processus bloqué field.';
            }
            field(Comment; rec.Comment)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Comment field.';
            }
        }
        moveafter("Gross Weight"; "Unit Volume")
        modify("Unit Cost")
        {
            Visible = false;
        }
        addafter("Unit Cost")
        {
            field("Unit Cost 2"; Rec."Unit Cost")
            {
                ApplicationArea = Basic, Suite;
                Editable = UnitCostEditable;
                Enabled = UnitCostEnable;
                Importance = Promoted;
                ToolTip = 'Specifies the cost of one unit of the item or resource on the line.';
                DecimalPlaces = 0 : 5;
                trigger OnDrillDown()
                var
                    ShowAvgCalcItem: Codeunit "Show Avg. Calc. - Item";
                begin
                    ShowAvgCalcItem.DrillDownAvgCostAdjmtPoint(Rec)
                end;
            }
        }
        addafter("Item Disc. Group")
        {
            field("Code pays/région provenance"; Rec."Code pays/région provenance")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code pays/région provenance field.';
            }
        }
        modify("Last Direct Cost")
        {
            visible = false;
        }
        addafter("Last Direct Cost")
        {
            field("Last Direct Cost 2"; Rec."Last Direct Cost")
            {
                ApplicationArea = Basic, Suite;
                Importance = Additional;
                DecimalPlaces = 0 : 5;
                ToolTip = 'Specifies the most recent direct unit cost of the item.';
            }
        }
        addafter("Application Wksh. User ID")
        {
            field("Item Charge DEEE"; Rec."Item Charge DEEE")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Frais annexe DEEE field.';
            }
            field("Item Charge DEEE Amount"; Rec."Item Charge DEEE Amount")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant frais annexe DEE field.';
            }
            field("Official DEEE Code"; Rec."Official DEEE Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Officiel DEEE field.';
            }
            field("Number Of Elements"; Rec."Number Of Elements")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nombre d''élements field.';
            }
        }
        addafter("Purch. Unit of Measure")
        {
            field("Lead Time Calculation2"; Rec."Lead Time Calculation")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Lead Time Calculation field.';
            }
            field("Stocké"; Rec.Stocké)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Stocké field.';
            }
            field("Code Appro"; Rec."Code Appro")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Appro field.';
            }
        }
        addafter("Assembly Policy")
        {
            field("Marquage réception"; Rec."Marquage réception")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Marquage réception field.';
            }
            field("Date Dernière Sortie"; Rec."Date Dernière Sortie")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Dernière Sortie field.';
            }
            field("Date dernière entrée"; Rec."Date dernière entrée")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date dernière entrée field.';
            }
            field("Sales multiple"; Rec."Sales multiple")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multiple de vente field.';
            }
            field("date maj multiple"; Rec."date maj multiple")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the date maj multiple field.';
            }
            field("user maj multiple"; Rec."user maj multiple")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the user maj multiple field.';
            }
            field("Purch. Multiple"; Rec."Purch. Multiple")
            {
                Editable = Editable;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multiple d''achat field.';
            }
        }
        moveafter(ReorderPointParameters; LotForLotParameters)
        addafter(Planning)
        {
            group("Commerce electronique")
            {
                Caption = 'Commerce electronique';
                Editable = Editable;
                field("Super Famille Marketing By Def"; Rec."Super Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Super Famille Marketing By Def field.';
                }
                field("Date Modif. Super Fam. Mark"; Rec."Date Modif. Super Fam. Mark")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modif. Super Fam. Mark field.';
                }
                field("Famille Marketing By Def"; Rec."Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Famille Marketing By Def field.';
                }
                field("Date Modif. Fam. Mark"; Rec."Date Modif. Fam. Mark")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modif. Fam. Mark field.';
                }
                field("Sous Famille Marketing By Def"; Rec."Sous Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille Marketing By Def field.';
                }
                field(Publiable; Rec.Publiable)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Publiable field.';
                }
                field("Date Modif. Publiable Web"; Rec."Date Modif. Publiable Web")
                {
                    Caption = 'Date Modif. Publiable Web';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modif. Publiable Web field.';
                }
                field("Net Price Only"; Rec."Net Price Only")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net price only field.';
                }
                field("Technical Manual"; Rec."Technical Manual")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Notice technique field.';
                }
                field("Technical Manual Name"; Rec."Technical Manual Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom du fichier notice field.';
                }
                field("Kit Web Status"; Rec."Kit Web Status")
                {
                    Editable = gAssemblyBOM;
                    Enabled = gAssemblyBOM;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Etat du kit (Web) field.';
                }
            }
        }
        addafter(Warehouse)
        {
            group(Divers)
            {
                Caption = 'Divers';
                Editable = Editable;
                field("Stocké2"; Rec.Stocké)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field("date maj stocké"; Rec."date maj stocké")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date maj stocké field.';
                }
                field("user maj stocké"; Rec."user maj stocké")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the user maj stocké field.';
                }
                field("Fusion en attente sur"; Rec."Fusion en attente sur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fusion en attente sur field.';
                }
                field("Code Appro2"; Rec."Code Appro")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
                field("Comodity Code"; Rec."Comodity Code")
                {
                    Caption = 'PCC/CNH';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the PCC/CNH field.';
                }
                field("Code Concurrence"; Rec."Code Concurrence")
                {
                    Caption = 'MPL - MPC/CNH';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MPL - MPC/CNH field.';
                }
                field(Nature; Rec.Nature)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nature field.';
                }
                field("Stock Mort"; Rec."Stock Mort")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Mort field.';
                }
                field("date maj stock mort"; Rec."date maj stock mort")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date maj stock mort field.';
                }
                field("user maj stock mort"; Rec."user maj stock mort")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the user maj stock mort field.';
                }
                field("Rate Commission"; Rec."Rate Commission")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coefficient de commission field.';
                }
                field("Stock Maxi Publiable"; Rec."Stock Maxi Publiable")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Maxi Publiable field.';
                }
                field(Longueur; Rec.Longueur)
                {
                    Caption = 'Longueur (Cm)';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Longueur (Cm) field.';
                }
                field(Largeur; Rec.Largeur)
                {
                    Caption = 'Largeur  (Cm)';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Largeur  (Cm) field.';
                }
                field(Epaisseur; Rec.Epaisseur)
                {
                    Caption = 'Epaisseur (Cm)';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Epaisseur (Cm) field.';
                }
                field("Box Type"; Rec."Box Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de bac field.';
                }
                field("Nb Contenu Emplacement"; Rec."Nb Contenu Emplacement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nb Contenu Emplacement field.';
                }
                field("Respect Sales Multiple"; Rec."Respect Sales Multiple")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Respect Sales Multiple field.';
                }
                field("Une Etq Reception Par art"; Rec."Une Etq Reception Par art")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Une Etq Reception Par art field.';
                }
                field("Recommended Shipping Agent"; Rec."Recommended Shipping Agent")
                {
                    Caption = 'Shipping Agent Code';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Mandatory Shipping Agent"; Rec."Mandatory Shipping Agent")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
            }
        }
        addbefore(ItemPicture)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
        }
    }
    actions
    {

        addafter(Identifiers)
        {
            action("Liste des critères par article")
            {
                Caption = 'Liste des critères par article';
                RunObject = Page "Item Critere List";
                RunPageLink = "Code Article" = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Liste des critères par article action.';
            }
            action("Liste des hierarchies marketing")
            {
                Caption = '<Liste ades critères par article>';
                Image = MapSetup;
                Promoted = true;
                PromotedCategory = Process;
                RunObject = Page "Item Hierarchies";
                RunPageLink = "Code Article" = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the <Liste ades critères par article> action.';
            }
            action(Equipements)
            {
                Image = Components;
                RunObject = Page "Item Equipment";
                RunPageLink = "Item No." = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Equipements action.';

                trigger OnAction();
                begin
                    //OpenEquipmentPage();
                end;
            }
        }
        addlast(Flow)
        {
            separator(Separator1100284040)
            {
            }
            action("Implémenter prix d'achat")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Implémenter prix d''achat action.';

                trigger OnAction();
                begin
                    Rec.ImplementerPrixAchat();
                    MESSAGE('Mise à jour terminée');
                end;
            }
            action("Prix Net")
            {
                ApplicationArea = All;
                Caption = 'Prix Net';
                Image = Price;
                ToolTip = 'Executes the Prix Net action.';
                trigger OnAction();
                var
                    LItem: Record Item;
                    RPrix: Report "Prix D'achat Net";
                begin
                    LItem := Rec;
                    LItem.SETRECFILTER();

                    RPrix.SETTABLEVIEW(LItem);
                    RPrix.RUNMODAL();
                end;
            }
        }
        addafter(ApplyTemplate)
        {
            separator(Separator1100284042)
            {
            }
            action("Envoyer au MiniLoad")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Envoyer au MiniLoad action.';

                trigger OnAction();
                var
                    LItem: Record Item;
                    Lcu_MiniLoad: Codeunit "Gestion MINILOAD";
                begin

                    LItem.GET(Rec."No.");
                    LItem.SETRECFILTER();
                    CLEAR(Lcu_MiniLoad);
                    Lcu_MiniLoad.Article_MsgPRO(LItem);
                end;
            }
        }
        addafter("Item Tracing")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
        addafter(ApprovalEntries)
        {
            action("Editable O/N")
            {
                Image = Edit;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Visible = Bonton_Editable;
                ApplicationArea = All;
                ToolTip = 'Executes the Editable O/N action.';

                trigger OnAction();
                begin
                    EditableO_N();
                end;
            }
            action("Additional Item temp")
            {
                ApplicationArea = All;
                Caption = 'Articles complémentaires';
                Image = CoupledItem;
                RunObject = Page "Additional Item List";
                RunPageLink = "Item No." = FIELD("No.");
                RunPageView = SORTING("Item No.", "Additional Item No.") ORDER(Ascending);
                ToolTip = 'Executes the Articles complémentaires action.';
            }
        }
        addafter(Action163)
        {
            separator(Separator1100284037)
            {
            }
            action("Implémenter Prix de vente")
            {
                ApplicationArea = All;
                Caption = 'Implémenter Prix de vente';
                Image = ImplementPriceChange;
                ToolTip = 'Executes the Implémenter Prix de vente action.';
                trigger OnAction();
                var
                    LItem: Record Item;
                    LReport: Report "Maj prix de vente / tab_tar";
                begin
                    // ESK
                    LItem := Rec;
                    LItem.SETRECFILTER();
                    LReport.SETTABLEVIEW(LItem);
                    LReport.RUNMODAL();
                end;
            }
        }
        modify("Assembly BOM")
        {
            Visible = false;
        }
        addafter("Assembly BOM")
        {
            action("Assembly BOM2")
            {
                AccessByPermission = TableData "BOM Component" = R;
                ApplicationArea = Assembly;
                Caption = 'Assembly BOM';
                Image = BOM;
                RunObject = Page "Assembly BOM";
                RunPageLink = "Parent Item No." = field("No.");
                RunPageMode = View;
                ToolTip = 'View or edit the bill of material that specifies which items and resources are required to assemble the assembly item.';
            }
        }
    }

    var
        Editable: Boolean;
        Bonton_Editable: Boolean;

    var
        IsFromCopieArticle: Boolean;
        gAssemblyBOM: Boolean;

    trigger OnOpenPage()
    begin
        // FB Le 03-11-2015
        Bonton_Editable := FALSE;
        Editable := TRUE;
        // MCO Le 13-12-2017 => Etre modifiable depuis copier article
        IF CurrPage.EDITABLE AND (Rec."No." <> '') AND (NOT IsFromCopieArticle) THEN BEGIN
            //IF CurrPage.EDITABLE AND ("No." <> '') THEN
            // FIN MCO Le 13-12-2017

            Bonton_Editable := TRUE;
            Editable := FALSE;
        END;
        // FIN FB Le 03-11-2015

    end;

    trigger OnAfterGetRecord()
    begin
        // CFR le 19/10/2022 => R‚gie : Ajout champ 50132 [Kit Web Status] onglet [Commerce ‚lectronique]
        Rec.CALCFIELDS("Assembly BOM");
        gAssemblyBOM := Rec."Assembly BOM";
    end;

    procedure EditableO_N();
    begin
        Editable := NOT Editable;
    end;

    local procedure OpenEquipmentPage();
    var
        recL_EquipItem: Record "Modele / Article";
        pgeL_EquipItem: Page "Item Equipment";
    begin
        CLEAR(recL_EquipItem);
        CLEAR(pgeL_EquipItem);

        recL_EquipItem.SETRANGE("Item No.", Rec."No.");
        pgeL_EquipItem.SETTABLEVIEW(recL_EquipItem);
        pgeL_EquipItem.RUN();
    end;

    procedure IsCopieArticle(new_copiearticle: Boolean);
    begin
        IsFromCopieArticle := new_copiearticle;
    end;

}

