pageextension 51073 "Get Post.Doc - S.InvLn Subform" extends "Get Post.Doc - S.InvLn Subform" //5852
{
    layout
    {
        addafter("No.")
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
    }
    Trigger OnOpenPage()
    BEGIN
        Rec.SetCurrentKey("Document No.", "Line No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    END;
}

