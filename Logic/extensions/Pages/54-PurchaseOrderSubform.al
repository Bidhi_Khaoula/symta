pageextension 50854 "Purchase Order Subform" extends "Purchase Order Subform" //54
{

    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
        modify("Indirect Cost %")
        {
            editable = false;
        }
        modify("Bin Code")
        {
            editable = false;
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';

                trigger OnValidate();
                begin
                    CurrPage.SAVERECORD();
                end;
            }
        }
        modify("Item Reference No.")
        {
            Visible = true;
        }
        addafter("item Reference No.")
        {
            field("Lien Commande client"; Rec."Lien Commande client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Lien Commande client field.';
            }
        }
        addafter(Quantity)
        {
            field("Outstanding Quantity"; Rec."Outstanding Quantity")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Outstanding Quantity field.';
            }
        }
        modify("Line No.")
        {
            Visible = true;
        }
        moveafter(ShortcutDimCode8; "Line No.")
        addafter("Line No.")
        {

            field("Discount1 %"; Rec."Discount1 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise1 field.';
            }
            field("Discount2 %"; Rec."Discount2 %")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the % Remise2 field.';
            }
            field("Direct Unit Cost Ctrl Fac"; Rec."Direct Unit Cost Ctrl Fac")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Direct Unit Cost field.';
            }
            field("Net Unit Cost ctrl fac"; Rec."Net Unit Cost ctrl fac")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Unit Cost field.';
            }
            field("Initial Quantity ctrl fac"; Rec."Initial Quantity ctrl fac")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Quantité initiale origine field.';
            }
            field("Commentaire Réception"; Rec."Commentaire Réception")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Commentaire Réception field.';
            }
            field(PAMP; Rec.GetPamp())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetPamp() field.';
            }
        }
        addafter("Document No.")
        {
            field("Coût indirect fournisseur"; Rec.GetItemVendorIndirectCost())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetItemVendorIndirectCost() field.';
            }
            field(Commentaires; Rec.Commentaires)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Commentaires field.';
            }
        }
        addafter(Control37)
        {
            field("Invoice Gross Weight"; TotalPurchaseLine."Gross Weight")
            {
                AutoFormatType = 1;
                Caption = 'Invoice Discount Amount';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Invoice Discount Amount field.';
            }
        }
        modify("Total VAT Amount")
        {
            Visible = false;
        }
        modify("Total Amount Incl. VAT")
        {
            Visible = false;
        }
    }
    actions
    {
        addbefore("&Line")
        {
            action("Multi-Consultation")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    IF Rec.Type <> Rec.Type::Item THEN EXIT;

                    FrmQuid.InitArticle(Rec."No.");
                    FrmQuid.RUNMODAL();
                end;
            }
            action("Fiche article fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Fiche article fournisseur action.';

                trigger OnAction();
                begin
                    Rec.ShowItemVendorCard();
                end;
            }
        }
        addafter("F&unctions")
        {
            action("Ajuster Quantité")
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Ajuster Quantité action.';

                trigger OnAction();
                begin
                    Rec.ArrondirQte(0);
                end;
            }
        }
    }
    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        // AD Le 24-03-2016 => Demande de Nathalie car pose des problŠmes
        IF Rec.Type = Rec.Type::Item THEN
            IF (Rec."Line No." MOD 10000) <> 0 THEN
                ERROR('Impossible d''insérer une ligne ici !');
        // FIN AD Le 24-03-2016
    end;

    procedure ArrondirQte();
    begin
        Rec.ArrondirQte(0);
    end;

    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;


}

