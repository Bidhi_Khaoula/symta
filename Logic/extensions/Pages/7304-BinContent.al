pageextension 51337 "Bin Content" extends "Bin Content" //7304
{
    layout
    {
        addafter("Item No.")
        {
            field("Référence Active"; Rec."Référence Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence Active field.';
            }
        }
        addafter("Unit of Measure Code")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
            field(Description; Rec.Description)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Description field.';
            }
            field("Commentaire dernier Mvt."; Rec.GetCommentaireInfoEntrée('COMMENT'))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetCommentaireInfoEntrée(''COMMENT'') field.';
            }
        }
    }


}

