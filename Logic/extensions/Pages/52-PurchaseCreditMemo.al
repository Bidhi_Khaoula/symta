pageextension 50762 "Purchase Credit Memo" extends "Purchase Credit Memo" //52
{

    layout
    {
        modify("Buy-from Vendor No.")
        {
            Importance = Promoted;
        }

        addafter("Buy-from Vendor No.")
        {
            field(Periode; Rec.Periode)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Periode field.';
            }
        }
        addafter(Status)
        {
            field("Montant HT facture"; Rec."Montant HT facture")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant TTC field.';

                trigger OnValidate();
                begin
                    CurrPage.UPDATE();
                end;
            }
        }
    }
    actions
    {

        addafter("Remove From Job Queue")
        {
            action(test)
            {
                ApplicationArea = All;
                ToolTip = 'Executes the test action.';

                trigger OnAction();
                begin
                    Rec.NEXT();
                end;
            }
        }
    }
    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec.Periode := xRec.Periode;
    end;

}

