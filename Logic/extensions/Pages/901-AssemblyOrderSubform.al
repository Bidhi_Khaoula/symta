pageextension 51585 "Assembly Order Subform" extends "Assembly Order Subform" //901
{
    layout
    {
        addafter("No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
        }
    }
    actions
    {
        modify("Order &Tracking")
        {
            Visible = false;
        }
        addafter("Order &Tracking")
        {
            action("Order &Tracking2")
            {
                ApplicationArea = Planning;
                Caption = 'Order &Tracking';
                Image = OrderTracking;
                ToolTip = 'Tracks the connection of a supply to its corresponding demand. This can help you find the original demand that created a specific production order or purchase order.';
                trigger OnAction()
                begin
                    Rec.ShowTracking2();
                end;
            }
        }
    }
    procedure MultiConsultation();
    begin
        Rec.OuvrirMultiConsultation();
    end;

}

