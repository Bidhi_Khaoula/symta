pageextension 50910 "Purch. Comment Sheet" extends "Purch. Comment Sheet" //66
{
    layout
    {
        addafter(Code)
        {
            field("Display Order"; Rec."Display Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Display Order field.';
            }
            field("Print Order"; Rec."Print Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Print Order field.';
            }
            field("Display Receipt"; Rec."Display Receipt")
            {
                ApplicationArea = all;
                Caption = 'Affichage réception Magasin';
                ToolTip = 'Specifies the value of the Affichage réception Magasin field.';
            }
        }
    }

}