pageextension 50009 "Purchase Return Orders" extends "Purchase Return Orders" //6643
{
    layout
    {
        modify("No.")
        {
            editable = false;
        }
        modify("Line Discount %")
        {
            editable = false;
        }
    }
}