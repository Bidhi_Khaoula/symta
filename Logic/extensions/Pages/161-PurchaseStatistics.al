pageextension 50196 "Purchase Statistics" extends "Purchase Statistics" //161
{
    layout
    {
        addafter(TotalAmount1)
        {
            field("Diff. H.T."; (Rec."Montant HT facture" - TotalAmount2) / (TotalAmount2 / TotalAmount1))
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Montant HT facture - TotalAmount2) / (TotalAmount2 / TotalAmount1) field.';
            }
        }

    }
}