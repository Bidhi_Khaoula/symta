pageextension 50625 "Sales Invoice Subform" extends "Sales Invoice Subform" //47
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Line Discount %")
        {
            Editable = false;
        }
        modify("Bin Code")
        {
            Editable = false;
        }
        addafter(Type)
        {
            field("Recherche référence"; Rec."Recherche référence")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recherche référence field.';
            }
        }
    }

}

