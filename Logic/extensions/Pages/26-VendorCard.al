pageextension 50486 "Vendor Card" extends "Vendor Card" //26
{
    layout
    {
        modify(General)
        {
            Editable = Editable;
        }
        modify("Address & Contact")
        {
            Editable = Editable;
        }
        modify(Invoicing)
        {
            Editable = Editable;
        }
        modify(Payments)
        {
            Editable = Editable;
        }
        modify(Receiving)
        {
            Editable = Editable;
        }
        addafter("No.")
        {
            field("Vendor Type"; Rec."Vendor Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type Fournisseur field.';
            }
        }
        addafter("Responsibility Center")
        {
            field("EORI Code"; Rec."EORI Code")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code EORI field.';
            }
            field(SIRET; Rec.SIRET)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the SIRET field.';
            }
        }
        addafter(ShowMap)
        {
            field(OurAccountNo; Rec."Our Account No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Our Account No. field.';
            }
        }
        movelast(Contact; "Phone No.", MobilePhoneNo, "E-Mail", "Fax No.")
        addafter("Address & Contact")
        {
            group(Divers)
            {
                Editable = Editable;
                field("Groupe Tarif Fournisseur"; Rec."Groupe Tarif Fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Groupe Tarif Fournisseur field.';
                }
                field("implement tarif par ref_active"; Rec."implement tarif par ref_active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the implement tarif par ref_active field.';
                }
                field("Privilège_appro"; Rec.Privilège_appro)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Privilège_appro field.';
                }
                field("Masquer Ref. Active"; Rec."Masquer Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Masquer Ref. Active field.';
                }
                field("Masquer Logo Société"; Rec."Masquer Logo Société")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Masquer Logo Société field.';
                }
                field("Code Appro"; Rec."Code Appro")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
            }

        }
        addafter(Divers)
        {
            group("E.D.I.")
            {
                Editable = Editable;
                field("Vendor EDI Code"; Rec."Vendor EDI Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code EDI Fournisseur field.';
                }
                field("Code Cde EDI"; Rec."Code Cde EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Cde EDI field.';
                }
                field("Date Début Export Cde EDI"; Rec."Date Début Export Cde EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Début Export Cde EDI field.';
                }
                field("Code Reception EDI"; Rec."Code Reception EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Reception EDI field.';
                }
                field("Date Début Export Recept. EDI"; Rec."Date Début Export Recept. EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Début Export Recept. EDI field.';
                }
                field("Code Facturation EDI"; Rec."Code Facturation EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Facturation EDI field.';
                }
                field("Date Début Export Facture EDI"; Rec."Date Début Export Facture EDI")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Début Export Facture EDI field.';
                }
            }
        }

        moveafter("Date Début Export Facture EDI"; "Home Page", "Our Account No.", "Language Code")
        addafter("Invoice Disc. Code")
        {
            field("Vendor Disc. Group"; Rec."Vendor Disc. Group")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Customer Disc. Group field.';
            }
        }
        addafter("Creditor No.")
        {
            field("Seuil Franco"; Rec."Seuil Franco")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Seuil Franco field.';
            }
        }
        addafter("Shipment Method Code")
        {
            field("Incoterm City"; Rec."Incoterm City")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ville Incoterm field.';
            }
        }
    }
    actions
    {
        modify(OrderAddresses)
        {
            Caption = 'Order &Addresses';
        }


        addafter("Ven&dor")
        {
            action("Editable O/N")
            {
                Image = Edit;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Visible = Bonton_Editable;
                ApplicationArea = All;
                ToolTip = 'Executes the Editable O/N action.';

                trigger OnAction();
                begin
                    EditableO_N();
                end;
            }
        }
        addafter("&Payment Addresses")
        {
            action("Copier fournisseur vers ....")
            {
                Caption = 'Copier fournisseur vers ....';
                Image = Copy;
                ApplicationArea = All;
                ToolTip = 'Executes the Copier fournisseur vers .... action.';

                trigger OnAction();
                begin
                    CopierFournisseur();
                end;
            }
        }
        addafter(History)
        {
            action("Extrait de compte")
            {
                Caption = 'Extrait de compte';
                Image = CustomerLedger;
                ApplicationArea = all;
                ToolTip = 'Executes the Extrait de compte action.';
                trigger OnAction();
                var
                    _Fournisseur: Record Vendor;
                    _extrait: Report "Extrait de compte fournisseur";
                begin

                    CLEAR(_Fournisseur);
                    CLEAR(_extrait);
                    _Fournisseur := Rec;
                    _Fournisseur.SETRECFILTER();
                    _extrait.SETTABLEVIEW(_Fournisseur);
                    _extrait.RUNMODAL();
                end;
            }
        }
        addafter("Item &Tracking Entries")
        {
            action(LignesFactures)
            {
                Caption = 'Lignes Factures';
                Image = Invoice;
                RunObject = Page "Lignes Facture Achat";
                RunPageLink = "Buy-from Vendor No." = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Lignes Factures action.';
            }
            action(LignesAvoirs)
            {
                Caption = 'Lignes Avoirs';
                Image = Invoice;
                RunObject = Page "Lignes Avoir Achat";
                RunPageLink = "Buy-from Vendor No." = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Lignes Avoirs action.';
            }
            action(Litige)
            {
                RunObject = Page "Gestion Litige";
                RunPageLink = "Vendor No." = FIELD("No.");
                RunPageView = SORTING("Whse Receipt No.", "Line No.") ORDER(Ascending);
                ApplicationArea = All;
                ToolTip = 'Executes the Litige action.';
            }
        }
        addafter("Vendor - Balance to Date")
        {
            action(Litiges)
            {
                Caption = 'Litiges';
                Image = BreakpointsList;
                Promoted = true;
                PromotedCategory = Process;
                RunObject = Page "Gestion Litige";
                RunPageLink = "Vendor No." = FIELD("No.");
                RunPageView = SORTING("Whse Receipt No.", "Line No.") ORDER(Ascending);
                ApplicationArea = All;
                ToolTip = 'Executes the Litiges action.';
            }
        }
    }

    var
        Editable: Boolean;
        Bonton_Editable: Boolean;

    trigger OnOpenPage()
    begin
        // FB Le 03-11-2015
        Bonton_Editable := FALSE;
        Editable := TRUE;
        IF (CurrPage.EDITABLE) AND (rec."No." <> '') THEN BEGIN
            Bonton_Editable := TRUE;
            Editable := FALSE;
        END;
        // FIN FB Le 03-11-2015

    end;

    local procedure EditableO_N();
    begin
        Editable := NOT Editable;
    end;

    local procedure CopierFournisseur();
    var
        lVendor: Record Vendor;
        lCopyVendor: Page "Copy Vendor";
    begin
        lCopyVendor.init(Rec);
        lCopyVendor.RUNMODAL();
        lCopyVendor.returnNewVendor(lVendor);
        IF lVendor."No." <> '' THEN
            IF CONFIRM('Voulez-vous ouvrir la fiche du fournisseur ' + lVendor."No." + ' ?') THEN BEGIN
                Rec.RESET();
                Rec.GET(lVendor."No.");
                CurrPage.UPDATE(FALSE);
            END;
    end;


}

