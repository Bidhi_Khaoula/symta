pageextension 50041 "Payment Slip Subform" extends "Payment Slip Subform" //10869
{
    layout
    {
        addafter("Account No.")
        {
            field(isBlocked; FctisBlocked())
            {
                Caption = 'Bloqué';
                ToolTip = 'Info bloqué pour type client/fournisseur';
                ApplicationArea = All;
            }
        }
        addafter("Document No.")
        {
            field("Désignation"; Rec.Désignation)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Désignation field.';
            }
        }
    }

    var
        P_header: Record "Payment Header";

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        P_header.GET(Rec."No.");
        IF P_header."Status No." > 0 THEN
            ERROR('Interdit');
    end;

    trigger OnModifyRecord(): Boolean
    begin
        P_header.GET(Rec."No.");
        IF P_header."Status No." > 0 THEN
            ERROR('Interdit');
    end;

    trigger OnDeleteRecord(): Boolean
    begin
        P_header.GET(Rec."No.");
        IF P_header."Status No." > 0 THEN
            ERROR('Interdit');
    end;

    local procedure FctisBlocked(): Text[50];
    var
        lCustomer: Record Customer;
        lVendor: Record Vendor;
    begin
        // CFR le 22/09/2021 => Régie : info compte bloqué
        IF (Rec."Account No." = '') THEN
            EXIT('');

        IF (Rec."Account Type" = Rec."Account Type"::Customer) THEN
            IF lCustomer.GET(Rec."Account No.") THEN
                EXIT(FORMAT(lCustomer.Blocked));

        IF (Rec."Account Type" = Rec."Account Type"::Vendor) THEN
            IF lVendor.GET(Rec."Account No.") THEN
                EXIT(FORMAT(lVendor.Blocked));

        EXIT('');
    end;



}

