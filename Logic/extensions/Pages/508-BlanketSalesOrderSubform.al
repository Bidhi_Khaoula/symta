pageextension 50011 "Blanket Sales Order Subform" extends "Blanket Sales Order Subform" //508
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Line Discount %")
        {
            Editable = false;
        }
    }

}