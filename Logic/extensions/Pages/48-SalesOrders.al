pageextension 50010 "Sales Orders" extends "Sales Orders" //48
{
    layout
    {
        modify("No.")
        {
            Editable = false;
        }
        modify("Line Discount %")
        {
            Editable = false;
        }
    }
}