pageextension 51659 "Sales Return Order List" extends "Sales Return Order List" //9304
{
    layout
    {
        addafter("No.")
        {
            field("Type retour"; Rec."Type retour")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Type retour field.';
            }
        }
        addafter("Job Queue Status")
        {
            field("Taux Abattement"; Rec."Taux Abattement")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Taux Abattement field.';
            }
            field("Port Charge Client"; Rec."Port Charge Client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Port Charge Client field.';
            }
            field("Enlevement Charge Client"; Rec."Enlevement Charge Client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Enlevement Charge Client field.';
            }
            field("Return Status"; Rec."Return Status")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut Retour field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field("Order Date"; Rec."Order Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Order Date field.';
            }
        }
        moveafter("Enlevement Charge Client"; Amount)
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
            field("Transport Information"; Rec."Transport Information")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Info. transport field.';
            }
            field("Source Document Type"; Rec."Source Document Type")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.SetCurrentKey("Document Type", "No.");
        Rec.Ascending(false);
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;



}

