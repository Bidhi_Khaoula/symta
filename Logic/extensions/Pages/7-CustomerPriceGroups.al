pageextension 50407 "Customer Price Groups" extends "Customer Price Groups" //7

{

    actions
    {
        modify(SalesPrices)
        {
            Visible = false;
        }
        addafter(SalesPrices)
        {
            action(SalesPrices2)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Sales &Prices';
                Image = SalesPrices;
                Visible = not ExtendedPriceEnabled2;
                ToolTip = 'Define how to set up sales price agreements. These sales prices can be for individual customers, for a group of customers, for all customers, or for a campaign.';
                ObsoleteState = Pending;
                ObsoleteReason = 'Replaced by the new implementation (V16) of price calculation.';
                ObsoleteTag = '17.0';

                trigger OnAction()
                var
                    SalesPrice: Record "Sales Price";
                begin
                    SalesPrice.SetCurrentKey("Sales Type", "Sales Code");
                    SalesPrice.SetRange("Sales Type", SalesPrice."Sales Type"::"Sous Groupe tarif");
                    SalesPrice.SetRange("Sales Code", Rec.Code);
                    Page.Run(Page::"Sales Prices", SalesPrice);
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        ExtendedPriceEnabled2 := PriceCalculationMgt.IsExtendedPriceCalculationEnabled();

    end;

    var
        PriceCalculationMgt: Codeunit "Price Calculation Mgt.";
        ExtendedPriceEnabled2: Boolean;
}