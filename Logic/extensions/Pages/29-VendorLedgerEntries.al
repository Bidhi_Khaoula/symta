pageextension 50491 "Vendor Ledger Entries" extends "Vendor Ledger Entries" //29
{
    layout
    {
        modify("Original Amount")
        {
            Caption = 'Original Amount';
        }
        modify(Amount)
        {
            Caption = 'Amount';
        }
        moveafter("Document No."; "Document Date")

        modify("Document Date")
        {
            Visible = false;
        }
        addafter("Vendor No.")
        {
            field("Country/Region Code"; GetCountryRegionCode())
            {
                Caption = 'Code pays/région';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code pays/région field.';
            }
            field("Code appro"; GetCodeAppro())
            {
                Caption = 'Code appro';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code appro field.';
            }
        }
        addafter(Amount)
        {
            field("Purchase (LCY)"; Rec."Purchase (LCY)")
            {
                Caption = 'Purchase (LCY)';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Purchase (LCY) field.';
            }
        }
    }
    actions
    {
    }
    trigger OnOpenPage()
    begin
        // CFR le 22/09/2021 => R‚gie : tri par [Entry No.] D‚croissant et se positionne sur la premiŠre
        IF rec.FINDFIRST() THEN;
    end;

    local procedure GetCountryRegionCode(): Code[10];
    var
        lVendor: Record Vendor;
    begin
        // CFR le 22/03/2022 => Régie : Ajout d'une fonction GetCountryRegionCode() pour récupérer le codePays/Région du fournisseur
        IF lVendor.GET(Rec."Vendor No.") THEN
            EXIT(lVendor."Country/Region Code")
        ELSE
            EXIT('');
    end;

    local procedure GetCodeAppro(): Code[10];
    var
        lVendor: Record Vendor;
    begin
        // CFR le 23/03/2022 => Régie : Ajout d'une fonction GetCodeAppro() pour récupérer le Code Appro du fournisseur
        IF lVendor.GET(Rec."Vendor No.") THEN
            EXIT(lVendor."Code Appro")
        ELSE
            EXIT('');
    end;


}

