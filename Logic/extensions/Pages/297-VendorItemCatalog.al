pageextension 50510 "Vendor Item Catalog" extends "Vendor Item Catalog" //297
{

    layout
    {
        addafter("Vendor Item No.")
        {
            field("Ref. Active"; Rec."Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active field.';
            }
            field("Minimum Order Quantity"; Rec."Minimum Order Quantity")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Minimum Order Quantity field.';
            }
            field("Purch. Multiple"; Rec."Purch. Multiple")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Multiple d''achat field.';
            }
            field("Code Remise"; Rec."Code Remise")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Remise field.';
            }
        }
        addafter("Lead Time Calculation")
        {
            field("Purch. Unit of Measure"; Rec."Purch. Unit of Measure")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Purch. Unit of Measure field.';
            }
        }
    }



}

