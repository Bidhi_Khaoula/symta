pageextension 51518 "Assembly Order" extends "Assembly Order" //900
{

    layout
    {

        addafter(Description)
        {
            field("Posting No. Series"; Rec."Posting No. Series")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Posting No. Series field.';
            }
        }

        moveafter("Posting No. Series"; "Location Code", "Bin Code")
        modify("Starting Date")
        {
            Visible = false;
        }
        modify("Ending Date")
        {
            Visible = false;
        }
    }

    actions
    {

        addbefore("Item Availability by")
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction();
                begin

                    //Multi-consultation sur les composants
                    //CurrPage.Lines.PAGE.MultiConsultation;
                    //Multi-consultation sur l'article
                    Rec.OuvrirMultiConsultation();
                end;
            }
        }
        modify("Assembly BOM")
        {
            Visible = false;
        }
        addafter("Assembly BOM")
        {
            action("Assembly BOM 2")
            {
                ApplicationArea = Assembly;
                Caption = 'Assembly BOM';
                Image = AssemblyBOM;
                ToolTip = 'View or edit the bill of material that specifies which items and resources are required to assemble the assembly item.';

                trigger OnAction()
                VAR
                    lBOMComponent: Record "BOM Component";
                    lPageAssemblyBOM: Page "Assembly BOM";
                BEGIN
                    // CFR le 11/05/2022 => R‚gie : Ne pas pouvoir modifier la nomenclature … partir de la consultation
                    //ShowAssemblyList;
                    Rec.TESTFIELD("Item No.");

                    lBOMComponent.SETRANGE("Parent Item No.", Rec."Item No.");
                    lPageAssemblyBOM.EDITABLE(FALSE);
                    lPageAssemblyBOM.SETTABLEVIEW(lBOMComponent);
                    lPageAssemblyBOM.RUN();
                    // FIN CFR le 11/05/2022
                end;
            }

        }
        modify("Order &Tracking")
        {
            Visible = false;
        }
    }


    trigger OnAfterGetRecord()
    begin
        // CFR le 11/05/2022 => R‚gie : Champ pour identifier graphiquement les KITS v‚rifi‚s
        Rec.Checked := TRUE;
        Rec.MODIFY(FALSE);
        COMMIT();
    end;


}

