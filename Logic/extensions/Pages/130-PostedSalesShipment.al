pageextension 50606 "Posted Sales Shipment" extends "Posted Sales Shipment" //130
{
    layout
    {
        modify(General)
        {
            Editable = false;
        }
        addafter("External Document No.")
        {
            field("Material Information"; Rec."Material Information")
            {
                ApplicationArea = all;
                Editable = false;
                ToolTip = 'Specifies the value of the Info. matériel field.';
            }
        }
        addafter("No.")
        {
            field("Sell-to Customer No."; rec."Sell-to Customer No.")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
            }
        }
        modify("Sell-to")
        {
            Editable = false;
        }
        addafter("Responsibility Center")
        {
            field("Entierement facturé"; Rec."Entierement facturé")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Entierement facturé field.';
            }
            field("Source Document Type"; Rec."Source Document Type")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Type Origine Document field.';
            }
            field("Order Create User"; Rec."Order Create User")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
            }
            field(Preparateur; Rec.Preparateur)
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Preparateur field.';
            }
            field(PreparateurName; Rec.GetPreparateurName())
            {
                Caption = 'Nom préparateur';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nom préparateur field.';
            }
            field("Preparation Valid Date"; Rec."Preparation Valid Date")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Heure validation BP field.';
            }
        }
        //todo
        // modify(SalesShipmLines)
        // {
        //     Editable = false;
        // }
        modify(Shipping)
        {
            Editable = false;
        }
        modify("Shipment Method")
        {
            Editable = false;
        }
        modify(Billing)
        {
            Editable = false;
        }
        addafter("Shipping Agent Code")
        {
            field("Mode d'expédition"; Rec."Mode d'expédition")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Mode d''expédition field.';
            }
        }
        modify("Shipping Agent Service Code")
        {
            Visible = false;
        }
        addafter("Shortcut Dimension 2 Code")
        {
            field("Centrale Active"; Rec."Centrale Active")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Centrale Active field.';
            }
            field("Regroupement Centrale"; Rec."Regroupement Centrale")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Regroupement Centrale field.';
            }
            field("Regroupement Facturation"; Rec."Regroupement Facturation")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Regroupement Facturation field.';
            }
            field("Multi echéance"; Rec."Multi echéance")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Multi echéance field.';
            }
            field("Echéances fractionnées"; Rec."Echéances fractionnées")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Echéances fractionnées field.';
            }
            field("Payment Terms Code"; Rec."Payment Terms Code")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Payment Terms Code field.';
            }
            field("Payment Method Code"; Rec."Payment Method Code")
            {
                applicationarea = all;
                ToolTip = 'Specifies the value of the Payment Method Code field.';
            }
            group(billing2)
            {
                Caption = 'billing2';
                field("En attente de facturation"; Rec."En attente de facturation")
                {
                    applicationarea = all;
                    ToolTip = 'Specifies the value of the En attente de facturation field.';
                }
                field("User En Attente"; Rec."User En Attente")
                {
                    Caption = 'Utilisateur mise en attente';
                    applicationarea = all;
                    ToolTip = 'Specifies the value of the Utilisateur mise en attente field.';
                }
            }
        }
    }

    actions
    {
        addbefore("&Shipment")
        {
            action("Multi-Consultation")
            {
                ApplicationArea = All;

                Caption = 'Multi-Consultation';
                ShortCutKey = 'Ctrl+M';
                Promoted = true;
                PromotedIsBig = false;
                Image = CalculateConsumption;
                PromotedCategory = New;
                ToolTip = 'Executes the Multi-Consultation action.';
                Trigger OnAction()
                BEGIN
                    CurrPage.SalesShipmLines.PAGE.MultiConsultation();
                END;
            }
        }
        addafter(PrintCertificateofSupply)
        {
            group("&Shipment2")
            {
                Caption = 'Shipment';
                Image = Shipment;
                action("Shipment")
                {
                    Caption = 'Afficher document';
                    RunObject = Page "Packing List Header";
                    RunPageLink = "No." = FIELD("Packing List No.");
                    Enabled = hasPACKING;
                    Image = CreateWarehousePick;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Afficher document action.';
                }
                action("Imprimer par article")
                {
                    ApplicationArea = All;
                    Caption = 'Imprimer par article';
                    Enabled = hasPACKING;
                    Image = PrintCheck;
                    ToolTip = 'Executes the Imprimer par article action.';
                    Trigger OnAction()
                    BEGIN
                        gPackingListMgmt.PrintPackingListItem(Rec."Packing List No.", TRUE, 'ARTICLE');
                    END;
                }
                action("Imprimer par colis")
                {
                    ApplicationArea = All;
                    Caption = 'Imprimer par colis';
                    Visible = FALSE;
                    Enabled = HasPACKING;
                    Image = PrintCover;
                    ToolTip = 'Executes the Imprimer par colis action.';
                    Trigger OnAction()
                    BEGIN
                        gPackingListMgmt.PrintPackingListItem(Rec."Packing List No.", TRUE, 'COLIS');
                    END;
                }
            }
        }
        addafter("&Track Package")
        {
            action("Créer Litige")
            {
                Promoted = true;
                PromotedIsBig = true;
                Image = Error;
                PromotedCategory = Process;
                ApplicationArea = All;
                Caption = 'Créer Litige';
                ToolTip = 'Executes the Créer Litige action.';
                trigger OnAction()
                begin
                    Rec.CreerIncidentTransport();
                end;
            }
            action(EntierrementFacture)
            {
                Caption = 'Entièrement facturé';
                Image = CoupledSalesInvoice;
                ToolTip = 'Vérifie si le BL peut être coché Entièrement facturé (cas des lignes négatives notamment)';
                ApplicationArea = All;

                trigger OnAction();
                var
                    lGestionSYMTA: Codeunit "Gestion SYMTA";
                begin
                    // CFR le 05/10/2023 - Régie : ajout action EntierementFacture() pour corriger pb ponctuel
                    lGestionSYMTA.T110_EntierementFacture(Rec."No.");
                    CurrPage.UPDATE(FALSE);
                end;
            }
        }
        addafter("&Print")
        {
            action("&Email")
            {
                Image = Email;
                Caption = 'Email';
                ApplicationArea = All;
                ToolTip = 'Executes the Email action.';

                trigger OnAction()
                var
                    SalesShipmentHeader: Record "Sales Shipment Header";
                BEGIN
                    SalesShipmentHeader := Rec;
                    CurrPage.SETSELECTIONFILTER(SalesShipmentHeader);
                    SalesShipmentHeader.EmailRecords(TRUE);
                END;
            }
            action("Envoyer Par Fax")
            {
                Caption = 'Envoyer Par Fax';
                ApplicationArea = All;
                Promoted = true;
                Image = PrintCheck;
                PromotedCategory = Report;
                ToolTip = 'Executes the Envoyer Par Fax action.';
                trigger OnAction()
                begin
                    SendFaxMail('FAX')
                end;
            }
        }
        addafter("&Navigate")
        {
            action(Commande)
            {
                ApplicationArea = All;
                Caption = 'Commande';
                Promoted = true;
                PromotedIsBig = true;
                Image = Order;
                PromotedCategory = Process;
                ToolTip = 'Executes the Commande action.';
                trigger OnAction()
                begin
                    rec.ShowOrder();
                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        //WIIO -> ESKVN2.0 : PACKING V2
        hasPACKING := (Rec."Packing List No." <> '');
    end;

    PROCEDURE SendFaxMail(_pType: Code[10]);
    VAR
        LDoc: Record "Sales Shipment Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    BEGIN
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);

        CASE _pType OF
            'FAX':
                LSendFax.ReportToFax(LDocRef, 0, '');
            'MAIL':
                LSendFax.ReportToEmailPDF(LDocRef, 0, '');
            ELSE
                ERROR('Type Non Géré !');
        END;
    END;

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";
        hasPACKING: Boolean;
}