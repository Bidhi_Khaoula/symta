pageextension 51294 "Sales Comment List" extends "Sales Comment List" //69
{
    layout
    {
        addafter(Comment)
        {
            field("Display Order"; Rec."Display Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Affichage commande field.';
            }
            field("Print Quote"; Rec."Print Quote")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression devis field.';
            }
            field("Print Order"; Rec."Print Order")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression commande/Retour field.';
            }
            field("Print Wharehouse Shipment"; Rec."Print Wharehouse Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression exp. magasin field.';
            }
            field("Print Shipment"; Rec."Print Shipment")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression expédition field.';
            }
            field("Print Invoice"; Rec."Print Invoice")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Impression facture field.';
            }
        }
    }

}