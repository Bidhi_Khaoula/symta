pageextension 50992 "Item Substitution Entries" extends "Item Substitution Entries" //5718
{
    layout
    {
        addafter("Substitute No.")
        {
            field("Substitute Ref. Active"; Rec."Substitute Ref. Active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref. Active substitut field.';
            }
        }
        addafter(Description)
        {
            field("Info. achat"; ItemSubst."Description 2")
            {
                ApplicationArea = all;
                Caption = 'Info. achat';
                ToolTip = 'Specifies the value of the Info. achat field.';
            }
        }
        addafter(Inventory)
        {
            field("ItemSubst.PrixTarif(0D,0,TypeCde)"; ItemSubst.PrixTarif(0D, 0, TypeCde))
            {
                Caption = 'Prix Unitaire';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix Unitaire field.';
            }
        }
        addafter(Condition)
        {
            field("Qté Ouverte sur achat"; ItemSubst."Qty. on Purch. Order")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Purch. Order field.';
            }
            field("Qté Ouverte sur vente"; ItemSubst."Qty. on Sales Order")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Sales Order field.';
            }
        }
    }
    actions
    {

    }

    var
        ItemSubst: Record Item;
        TypeCde: Integer;

    trigger OnAfterGetRecord()
    begin
        CLEAR(ItemSubst);
        // AD Le 18-01-2016
        IF Rec."Substitute Type" = Rec."Substitute Type"::Item THEN
            IF ItemSubst.GET(Rec."Substitute No.") THEN
                // AD Le 03-02-2016
                ItemSubst.CALCFIELDS("Qty. on Purch. Order", "Qty. on Sales Order");

        // FIN AD Le 03-02-2016
        // FIN AD Le 18-01-2016
    END;

    procedure InitTypeCde(_pTypeCde: Integer);
    begin
        // AD Le 18-01-2016 => Pour retrouver le prix en fonction du type de commande
        TypeCde := _pTypeCde;
    end;

}

