pageextension 50684 "Contact List" extends "Contact List" //5052
{

    layout
    {
        addafter("Country/Region Code")
        {
            field("Info Contact"; Rec."Info Contact")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Info Contact field.';
            }
        }
        addafter("Fax No.")
        {
            field("First  Job Responsibilitie"; Rec."First  Job Responsibilitie")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Première responsabilité field.';
            }
        }
        addafter("Search Name")
        {
            field("Organizational Level Code"; Rec."Organizational Level Code")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Organizational Level Code field.';
            }
            field("Change Coordinate"; Rec."Change Coordinate")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Recalculer les coordonnées field.';
            }
            field(Latitude; Rec.Latitude)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Latitude field.';
            }
            field(Longitude; Rec.Longitude)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Longitude field.';
            }
            field("Company No."; Rec."Company No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Company No. field.';
            }
        }
    }
    actions
    {
    }

}