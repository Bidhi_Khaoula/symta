pageextension 50013 "Gen. Business Posting Groups" extends "Gen. Business Posting Groups" //312
{
    layout
    {
        addafter("Auto Insert Default")
        {
            field("Print Costoms Code"; Rec."Print Costoms Code")
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the value of the Print Costoms Code field.';
            }
        }
    }
}