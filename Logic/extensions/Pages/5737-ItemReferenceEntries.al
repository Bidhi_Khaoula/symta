pageextension 51000 "Item Reference Entries" extends "Item Reference Entries" //5737
{
    layout
    {
        addafter(Description)
        {
            field("publiable web"; Rec."publiable web")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the publiable web field.';
            }
            field("Code Constructeur"; Rec."Code Constructeur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Constructeur field.';
            }
            field("Create User ID"; Rec."Create User ID")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Création field.';
            }
            field("Create Date"; Rec."Create Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Création field.';
            }
            field("Create Time"; Rec."Create Time")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Création field.';
            }
            field("Modify User ID"; Rec."Modify User ID")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code utilisateur Modification field.';
            }
            field("Modify Date"; Rec."Modify Date")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Modification field.';
            }
            field("Modify Time"; Rec."Modify Time")
            {
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure Modification field.';
            }
            field("Item No."; Rec."Item No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Item No. field.';
            }
            field("Réf. active"; gGestionMultiReference.RechercheRefActive(rec."Item No."))
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the RechercheRefActive(rec.Item No.) field.';
            }
        }
    }

    var
        gGestionMultiReference: Codeunit "Gestion Multi-référence";

}

