reportextension 50008 Journals extends Journals//10801
{
    RDLCLayout = './Objets/Reports/Layouts/Journals.rdlc';
    dataset
    {
        add(Date)
        {
            column(Source_No_Caption; Source_No_CaptionLbl)
            {
            }
        }
        add("G/L Entry")
        {
            column(G_L_Entry__Source_No; "Source No.")
            {
            }
        }
    }
    var
        Source_No_CaptionLbl: Label 'Description';
}