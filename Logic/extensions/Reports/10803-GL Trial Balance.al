reportextension 50009 "G/L Trial Balance" extends "G/L Trial Balance" //10803
{
    RDLCLayout = './Objets/Reports/Layouts/GL Trial Balance.rdlc';
    dataset
    {
        modify("G/L Account")
        {
            trigger OnAfterAfterGetRecord()
            begin
                //
                IF "Total Debit/Credit" THEN BEGIN
                    CALCFIELDS("Net Change");
                    IF "Net Change" > 0 THEN BEGIN
                        "Debit Amount" := "Net Change";
                        "Credit Amount" := 0
                    END ELSE BEGIN
                        "Debit Amount" := 0;
                        "Credit Amount" := -"Net Change"
                    END
                END;
            end;
        }
    }

    requestpage
    {
        layout
        {
            addlast(Options)
            {
                field("Total Debit/Credit"; "Total Debit/Credit")
                {
                    ApplicationArea = all;
                    caption = 'Total Debit/Credit';
                    ToolTip = 'Specifies the value of the Total Debit/Credit field.';
                }
            }

        }
    }
    var
        "Total Debit/Credit": Boolean;
}