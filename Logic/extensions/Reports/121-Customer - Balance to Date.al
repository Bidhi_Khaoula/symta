reportextension 50000 "Customer - Balance to Date" extends "Customer - Balance to Date"//121
{
    RDLCLayout = './Objets/Reports/Layouts/Customer - Balance to Date.rdlc';
    dataset
    {
        add(CustLedgEntry3)
        {
            column(Date_echeance; FORMAT("Due Date"))
            {

            }
            column(Date_Echcaption; LblDate_EchcaptionLbl)
            {

            }
        }
        modify("Detailed Cust. Ledg. Entry")
        {
            trigger OnAfterPreDataItem()
            begin
                "Detailed Cust. Ledg. Entry".SetRange("Posting Date");
            end;
        }

    }
    var
        LblDate_EchcaptionLbl: Label 'Date d''échéance';
}