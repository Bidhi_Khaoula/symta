reportextension 50003 "Phys. Inventory List" extends "Phys. Inventory List"//722
{
    RDLCLayout = './Objets/Reports/Layouts/Phys. Inventory List.rdlc';
    dataset
    {
        modify("Item Journal Line")
        {
            trigger OnAfterAfterGetRecord()
            begin
                // MCO Le 28-09-2016
                stkdepot := 0;
                IF Item.GET("Item No.") THEN BEGIN
                    Item.CALCFIELDS(Inventory);
                    stkdepot := Item.Inventory - "Qty. (Calculated)"
                END;
                // FIN MCO Le 28-09
            end;
        }
        add("Item Journal Line")
        {
            column(RefActive_ItemJournalLine; "Ref. Active")
            {
            }
            column(StockDepot; stkdepot)
            {
            }
        }
    }
    var
        Item: Record Item;
        stkdepot: Decimal;
}