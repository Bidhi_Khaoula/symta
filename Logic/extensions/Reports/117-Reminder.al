reportextension 50007 "Reminder" extends Reminder //117
{
    RDLCLayout = './Objets/Reports/Layouts/Reminder.rdlc';
    dataset
    {
        add(Integer)
        {
            column(CompanyInformation_Picture; CompanyInformation.Picture)
            {
            }
        }
        add("Issued Reminder Header")
        {
            column(Level_IssuedReminderHeader; "Reminder Level")
            {
            }
        }
    }
    var
        CompanyInformation: Record "Company Information";

    trigger OnPreReport()
    begin
        CompanyInformation.Get();
        CompanyInformation.CalcFields(Picture);
    end;
}