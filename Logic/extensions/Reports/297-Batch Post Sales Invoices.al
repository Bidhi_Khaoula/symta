reportextension 50001 "Batch Post Sales Invoices" extends "Batch Post Sales Invoices"//297
{
    dataset
    {
        modify("Sales Header")
        {
            RequestFilterFields = "No.", Status, "Payment Method Code";
        }
    }
}