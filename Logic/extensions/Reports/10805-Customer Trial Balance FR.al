reportextension 50010 "Customer Trial Balance FR" extends "Customer Trial Balance FR"//10805
{
    RDLCLayout = './Objets/Reports/Layouts/Customer Trial Balance.rdlc';
    dataset
    {
        modify(Customer)
        {
            trigger OnAfterAfterGetRecord()
            begin
                CalcDebitCreditAmount(Customer);
                // GR le 03/02/2016 : filtrage par type d'op‚ration (d‚bit/cr‚dit)
                IF TypeOperation = TypeOperation::Créditeurs THEN BEGIN
                    IF ((PreviousDebitAmountLCYG + PeriodDebitAmountLCYG) - (PreviousCreditAmountLCYG + PeriodCreditAmountLCYG)) >= 0 THEN
                        CurrReport.SKIP();
                END
                ELSE
                    IF TypeOperation = TypeOperation::Débiteurs THEN
                        IF (PreviousCreditAmountLCYG + PeriodCreditAmountLCYG) - (PreviousDebitAmountLCYG + PeriodDebitAmountLCYG) >= 0 THEN
                            CurrReport.SKIP();

            end;
        }
    }

    requestpage
    {
        layout
        {
            addlast(Options)
            {
                field(TypeOperation; TypeOperation)
                {
                    Caption = 'Operations';
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Operations field.';
                }
            }
        }
    }

    var
        TypeOperation: option Toutes,Créditeurs,Débiteurs;
        PeriodDebitAmountLCYG: Decimal;
        PeriodCreditAmountLCYG: decimal;
        PreviousCreditAmountLCYG: Decimal;
        PreviousDebitAmountLCYG: Decimal;



    local procedure CalcDebitCreditAmount(Customer: record Customer)
    var
        CustLedgEntry: record "Detailed Cust. Ledg. Entry";
        PreviousEndDateG: date;
        StartDateG: Date;
        EndDateG: date;
    begin
        StartDateG := Customer.getrangeMin("Date Filter");
        PreviousEndDateG := ClosingDate(StartDateG - 1);
        if CopyStr(Customer.GetFilter("Date Filter"), StrLen(Customer.GetFilter("Date Filter")), 1) = '.' then
            EndDateG := 0D
        else
            EndDateG := Customer.GetRangeMax("Date Filter");
        CustLedgEntry.SetCurrentKey(
                          "Customer No.", "Posting Date", "Entry Type", "Initial Entry Global Dim. 1", "Initial Entry Global Dim. 2",
                          "Currency Code");
        CustLedgEntry.SetRange("Customer No.", Customer."No.");
        if Customer.GetFilter("Global Dimension 1 Filter") <> '' then
            CustLedgEntry.SetRange("Initial Entry Global Dim. 1", Customer.GetFilter("Global Dimension 1 Filter"));
        if Customer.GetFilter("Global Dimension 2 Filter") <> '' then
            CustLedgEntry.SetRange("Initial Entry Global Dim. 2", Customer.GetFilter("Global Dimension 2 Filter"));
        if Customer.GetFilter("Currency Filter") <> '' then
            CustLedgEntry.SetRange("Currency Code", Customer.GetFilter("Currency Filter"));
        CustLedgEntry.SetRange("Posting Date", 0D, PreviousEndDateG);
        CustLedgEntry.SetFilter("Entry Type", '<>%1', CustLedgEntry."Entry Type"::Application);
        if CustLedgEntry.FindSet() then
            repeat
                PreviousDebitAmountLCYG += CustLedgEntry."Debit Amount (LCY)";
                PreviousCreditAmountLCYG += CustLedgEntry."Credit Amount (LCY)";
            until CustLedgEntry.Next() = 0;
        CustLedgEntry.SetRange("Posting Date", StartDateG, EndDateG);
        if CustLedgEntry.FindSet() then
            repeat
                PeriodDebitAmountLCYG += CustLedgEntry."Debit Amount (LCY)";
                PeriodCreditAmountLCYG += CustLedgEntry."Credit Amount (LCY)";
            until CustLedgEntry.Next() = 0;
    end;
}