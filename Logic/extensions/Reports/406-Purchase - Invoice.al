reportextension 50002 "Purchase - Invoice" extends "Purchase - Invoice"//406
{
    RDLCLayout = './Objets/Reports/Layouts/Purchase - Invoice.rdlc';
    dataset
    {
        add("Purch. Inv. Line")
        {
            column(NetUnitCostCaption; NetUnitCostCaptionLbl)
            {

            }
            column(NetUnitCost; "Unit Cost")
            {

            }
            column(LineAmountCaption; FIELDCAPTION("Line Amount"))
            {

            }
            column(LineAmount; "Line Amount")
            {

            }
            column(Discount1; "Purch. Inv. Line"."Discount1 %")
            {

            }
            column(Discount2; "Purch. Inv. Line"."Discount2 %")
            {

            }
        }
    }

    var
        NetUnitCostCaptionLbl: Label 'Net unit cost';
}