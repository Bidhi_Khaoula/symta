reportextension 50004 "Assembly Order" extends "Assembly Order"//902
{
    RDLCLayout = './Objets/Reports/Layouts/Assembly Order.rdlc';
    dataset
    {
        add("Assembly Header")
        {
            column(RefActive_AssemblyHeader; "Assembly Header"."Ref Active")
            {
            }
        }
        add("Assembly Line")
        {
            column(RefActive_AssemblyLine; "Assembly Line"."Ref. Active")
            {
            }
        }
    }
}