query 50057 Liste_des_commandes
{
    Caption = 'Liste_des_commandes';
    TopNumberOfRows = 2000;
    OrderBy = Descending(Order_Date);
    elements
    {
        dataitem(Sales_Header; "Sales Header")
        {
            column(Document_Type; "Document Type")
            {

            }
            column(No; "No.")
            {

            }
            column(Order_Date; "Order Date")
            {

            }
            column(Devis_Web; "Devis Web")
            {

            }
            column(Source_Document_Type; "Source Document Type")
            {

            }
        }
    }

    trigger OnBeforeOpen()
    begin
        CurrQuery.SETFILTER(Order_Date, FORMAT(TODAY));

    end;
}