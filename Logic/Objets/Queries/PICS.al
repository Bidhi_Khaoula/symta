query 50055 PICS
{
    Caption = 'PICS';
    TopNumberOfRows = 5000;
    OrderBy = Descending(Date_Impression);
    elements
    {
        dataitem(Warehouse_Shipment_Header; "Warehouse Shipment Header")
        {
            column(No; "No.")
            {

            }
            column(Date_Impression; "Date Impression")
            {

            }
            column(Heure_Impression; "Heure Impression")
            {

            }
            column(Utilisateur_Impression; "Utilisateur Impression")
            {

            }
            column(Date_Flashage; "Date Flashage")
            {

            }
            column(Heure_Flashage; "Heure Flashage")
            {

            }
            column(Utilisateur_Flashage; "Utilisateur Flashage")
            {

            }
            column(Shipping_Agent_Code; "Shipping Agent Code")
            {

            }

        }
    }
}