query 50053 "Audit Fiscal"
{
    Caption = 'Audit Fiscal';

    elements
    {
        dataitem(Item; Item)
        {
            column(No; "No.")
            {

            }
            column(Description; Description)
            {

            }
            column(Manufacturer_Code; "Manufacturer Code")
            {

            }
            column(Item_Category_Code; "Item Category Code")
            {

            }
            //TODOcolumn(Product_Group_Code; "Product Group Code") {}
            dataitem(Value_Entry; "Value Entry")
            {
                DataItemTableFilter = "Posting Date" = FILTER('01/11/13');
                DataItemLink = "Item No." = Item."No.";
                column(Posting_Date; "Posting Date")
                {

                }
                column(Entry_No; "Entry No.")
                {

                }
                column(Valued_Quantity; "Valued Quantity")
                {

                }
                column(Cost_Amount_Actual; "Cost Amount (Actual)")
                {

                }
                column(Cost_Amount_Expected; "Cost Amount (Expected)")
                {

                }
                column(Sales_Amount_Actual; "Sales Amount (Actual)")
                {

                }
                column(Sales_Amount_Expected; "Sales Amount (Expected)")
                {

                }
            }

        }
    }
}