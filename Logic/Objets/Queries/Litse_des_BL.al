query 50056 Litse_des_BL
{
    Caption = 'Litse_des_BL';
    TopNumberOfRows = 5000;
    OrderBy = Descending(Posting_Date);
    elements
    {
        dataitem(Sales_Shipment_Header; "Sales Shipment Header")
        {
            column(Posting_Date; "Posting Date")
            {

            }
            column(No; "No.")
            {

            }
            column(Poids; Weight)
            {

            }
            column(NB_Colis; "Nb Of Box")
            {

            }
            column(Transporteur; "Shipping Agent Code")
            {

            }
            column(Preparateur; Preparateur)
            {

            }
        }
    }
    trigger OnBeforeOpen()
    begin
        CurrQuery.SETFILTER(Posting_Date, FORMAT(TODAY));
    end;
}