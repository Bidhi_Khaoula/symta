query 50050 "CommandesAchats"
{
    Caption = 'CommandesAchats';

    elements
    {
        dataitem(Purch_Inv_Header; "Purch. Inv. Header")
        {
            column(Numero_fournisseur; "Buy-from Vendor No.")
            {

            }
            column(Numero_facture; "No.")
            {

            }
            column(Date_facture; "Document Date")
            {

            }
            dataitem(Purch_Inv_Line; "Purch. Inv. Line")
            {
                DataItemTableFilter = "No." = FILTER(<> '');
                DataItemLink = "Document No." = Purch_Inv_Header."No.";
                //DataItemLinkType=Use Default Values if No Match
                column(Line_No; "Line No.")
                {

                }
                column(No; "No.")
                {

                }
                column(Unit_Cost_LCY; "Unit Cost (LCY)")
                {

                }
                column(Quantity; Quantity)
                {

                }
                column(Amount; Amount)
                {

                }
                column(Reference_Type_No; "Item Reference Type No.")
                {

                }
                column(Direct_Unit_Cost; "Direct Unit Cost")
                {

                }
                column(Vendor_Item_No; "Vendor Item No.")
                {

                }
                column(Type_de_commande; "Type de commande")
                {

                }
                column(Buy_from_Vendor_No; "Buy-from Vendor No.")
                {

                }
                dataitem(Item; Item)
                {
                    DataItemLink = "No." = Purch_Inv_Line."No.";
                    column(Manufacturer_Code; "Manufacturer Code")
                    {

                    }
                    column(Item_Category_Code; "Item Category Code")
                    {

                    }
                    //TODOcolumn(Product_Group_Code; "Product Group Code"){}
                    column(No_2; "No. 2")
                    {

                    }
                    dataitem(Vendor; Vendor)
                    {
                        DataItemLink = "No." = Purch_Inv_Header."Pay-to Vendor No.";
                        column(Name; Name)
                        {

                        }
                    }
                }
            }
        }

    }
}