query 50052 "AvoirssAchats"
{
    Caption = 'AvoirssAchats';

    elements
    {
        dataitem(Purch_Cr_Memo_Hdr; "Purch. Cr. Memo Hdr.")
        {
            column(Numero_fournisseur; "Buy-from Vendor No.")
            {

            }
            column(Numero_facture; "No.")
            {

            }
            column(Date_facture; "Document Date")
            {

            }
            dataitem(Purch_Cr_Memo_Line; "Purch. Cr. Memo Line")
            {
                DataItemTableFilter = "No." = FILTER(<> '');
                DataItemLink = "Document No." = Purch_Cr_Memo_Hdr."No.";
                //DataItemLinkType=Use Default Values if No Match;
                column(Line_No; "Line No.")
                {

                }
                column(No; "No.")
                {

                }
                column(Unit_Cost_LCY; "Unit Cost (LCY)")
                {

                }
                column(Quantity; Quantity)
                {

                }
                column(Amount; Amount)
                {

                }
                column(Item_Reference_Type_No; "Item Reference Type No.")
                {

                }
                column(Direct_Unit_Cost; "Direct Unit Cost")
                {

                }
                column(Vendor_Item_No; "Vendor Item No.")
                {

                }
                column(Type_de_commande; "Type de commande")
                {

                }
                column(Buy_from_Vendor_No; "Buy-from Vendor No.")
                {

                }
                dataitem(Item; Item)
                {
                    DataItemLink = "No." = Purch_Cr_Memo_Hdr."No.";
                    column(Manufacturer_Code; "Manufacturer Code")
                    {
                    }
                    column(Item_Category_Code; "Item Category Code")
                    {
                    }
                    //TODOcolumn(Product_Group_Code; "Product Group Code"){}
                    column(No_2; "No. 2")
                    {
                    }
                    dataitem(Vendor; Vendor)
                    {
                        DataItemLink = "No." = Purch_Cr_Memo_Hdr."Buy-from Vendor No.";
                        column(Name; Name)
                        {

                        }
                    }
                }
            }
        }
    }
}