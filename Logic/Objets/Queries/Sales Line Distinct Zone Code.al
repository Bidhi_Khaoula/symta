query 50001 "Sales Line Distinct Zone Code"
{
    Caption = 'Sales Line Distinct Zone Code';
    elements
    {
        dataitem(Sales_Line; "Sales Line")
        {
            column(Document_Type; "Document Type")
            {

            }
            column(Document_No; "Document No.")
            {

            }
            column(Zone_Code; "Zone Code")
            {


            }
            column(Count)
            {
                Method = Count;
                //MethodType =Totals;

            }
            filter(Quantity; Quantity)
            {

            }
            filter(Qty_to_Ship; "Qty. to Ship")
            {

            }
        }
    }
}