query 50054 "Chiffres_du_jour"
{
    Caption = 'Chiffres_du_jour';
    TopNumberOfRows = 5000;
    OrderBy = Descending(Posting_Date);
    elements
    {
        dataitem(Sales_Shipment_Header; "Sales Shipment Header")
        {
            column(Posting_Date; "Posting Date")
            {

            }
            column(No; "No.")
            {

            }
            column(Poids; Weight)
            {

            }
            column(NB_Colis; "Nb Of Box")
            {

            }
            column(Transporteur; "Shipping Agent Code")
            {

            }
            column(Preparateur; Preparateur)
            {

            }
            dataitem(Sales_Shipment_Line; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = Sales_Shipment_Header."No.";
                column(Line_No; "Line No.")
                {

                }
                column(prix; "Net Unit Price")
                {

                }
                column(Quantite; Quantity)
                {

                }
            }

        }

    }
    trigger OnBeforeOpen()
    begin
        CurrQuery.SETFILTER(Posting_Date, FORMAT(TODAY));

    end;
}