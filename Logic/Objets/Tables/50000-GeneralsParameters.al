tableextension 50200 "Generals Parameters" extends "Generals Parameters" //50000
{
    DrillDownPageID = "List Parameters";
    LookupPageID = "List Parameters";

    fields
    {
        modify("Code")
        {
            trigger OnAfterValidate()
            begin
                IF (Type = 'DEFAUT') AND (xRec.Code <> '') THEN
                    ERROR('Impossible de renommé ce champs');
            end;
        }
    }

    trigger OnDelete()
    begin
        // on n'autorise plus la suppression
        // modif CS le 21/04/04

        IF Type = 'DEFAUT' THEN
            ERROR('Impossible de suprimer une en-tête');

        /*
       BEGIN
          DELETE;
          RESET();
          SETRANGE(Type, Code);
          IF FIND('-') THEN
          REPEAT
             DELETE;
          UNTIL NEXT=0;

          MESSAGE (Text50000);
          RESET();
          SETRANGE(Type, 'DEFAUT');

      END
       */
        // fin modif

    end;

    trigger OnRename()
    begin
        /*IF Type='DEFAUT' THEN
        BEGIN
           MESSAGE('Vous n''êtes pas autorisés à  modifier ce champ');
           Type:=SaveType
        END;
         */

    end;

    var
    // Text50000: Label 'Lignes supprimées';
    // SaveType: Code[10];
}

