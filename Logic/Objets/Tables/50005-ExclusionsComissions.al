tableextension 50205 "Exclusions Comissions" extends "Exclusions Comissions" //50005
{

    fields
    {
        modify("Salesperson Type")
        {
            trigger OnAfterValidate()
            begin
                IF "Salesperson Type" <> xRec."Salesperson Type" THEN
                    VALIDATE("Salesperson Code", '');
            end;
        }
        modify("Salesperson Code")
        {
            trigger OnAfterValidate()
            begin
                IF "Salesperson Code" <> '' THEN
                    CASE "Salesperson Type" OF
                        "Salesperson Type"::Tous:
                            ERROR(Text001Err, FIELDCAPTION("Salesperson Code"));
                    END;
            end;
        }
        modify("Sales Type")
        {
            trigger OnAfterValidate()
            begin
                IF "Sales Type" <> xRec."Sales Type" THEN
                    VALIDATE("Sales Code", '');
            end;
        }
        modify("Sales Code")
        {
            trigger OnAfterValidate()
            begin
                IF "Sales Code" <> '' THEN
                    CASE "Sales Type" OF
                        "Sales Type"::Tous:
                            ERROR(Text001Err, FIELDCAPTION("Sales Code"));
                    END;
            end;
        }
        modify(Type)
        {
            trigger OnAfterValidate()
            begin
                IF Type <> xRec.Type THEN
                    VALIDATE(Code, '');
            end;
        }
        modify("Code")
        {
            trigger OnAfterValidate()
            begin
                IF Code <> '' THEN
                    CASE Type OF
                        Type::Tous:
                            ERROR(Text001Err, FIELDCAPTION(Code));
                    END;
            end;
        }
    }

    var
        Text001Err: Label '%1 must be blank.', Comment = '%1 = Field';
}

