tableextension 50215 "Relance Telephonique" extends "Relance Telephonique" //50015
{
    procedure FormatAddr(var AddrLines: array[8] of Text[50])
    var
        Cust: Record Customer;
        FormatAddrCodeunit: Codeunit "Format Address";
    begin
        IF Cust.GET("N° client") THEN
            FormatAddrCodeunit.Customer(AddrLines, Cust);
    end;
}

