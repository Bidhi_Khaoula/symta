tableextension 50276 "Demande transporteur" extends "Demande transporteur" //50076
{
    fields
    {
        modify("Customer No.")
        {
            trigger OnAfterValidate();
            var
                Customer: Record Customer;
            begin
                Customer.GET("Customer No.");
                "Ship-to Name" := Customer.Name;
                "Ship-to Name 2" := Customer."Name 2";
                "Ship-to Address" := Customer.Address;
                "Ship-to Address 2" := Customer."Address 2";
                "Ship-to City" := Customer.City;
                "Ship-to Post Code" := Customer."Post Code";
                "Ship-to Country/Region Code" := Customer."Country/Region Code";
                "Ship-to Phone" := Customer."Phone No.";
                "Ship-to E-mail" := Customer."E-Mail";

                "Incoterm City" := Customer."Incoterm City";
                "Shipment Method Code" := Customer."Shipment Method Code";

                CheckAxeTiersCustomer();
            end;
        }
        modify("Vendor No.")
        {
            trigger OnAfterValidate();
            var
                Vendor: Record Vendor;
            begin
                Vendor.GET("Vendor No.");
                "Ship-to Name" := Vendor.Name;
                "Ship-to Name 2" := Vendor."Name 2";
                "Ship-to Address" := Vendor.Address;
                "Ship-to Address 2" := Vendor."Address 2";
                "Ship-to City" := Vendor.City;
                "Ship-to Post Code" := Vendor."Post Code";
                "Ship-to Country/Region Code" := Vendor."Country/Region Code";
                "Ship-to Phone" := Vendor."Phone No.";
                "Ship-to E-mail" := Vendor."E-Mail";
                "Incoterm City" := Vendor."Incoterm City";
                "Shipment Method Code" := Vendor."Shipment Method Code";
                "Code Appro" := Vendor."Code Appro";

                CheckAxeTiersVendor();
            end;
        }
        modify("Shipping Vendor No.")
        {
            trigger OnAfterValidate();
            var
                Vendor: Record Vendor;
            begin
                //CheckAxeShippingAgent();
                IF "Shipping Vendor No." <> '' THEN BEGIN
                    Vendor.GET("Shipping Vendor No.");
                    "Shipping Vendor Name" := Vendor.Name;
                    "Shipping Email" := Vendor."E-Mail";
                END;
            end;
        }
        modify("Code Appro")
        {
            trigger OnAfterValidate();
            begin
                // ANI Le 23-06-2016 FE20160617 -> annulation du traitement car calculé par une tâche planifiée
                /*
                // ANI Le 16-06-2016 FE20150424
                IF xRec."Code Appro" <> "Code Appro" THEN
                  IF CONFIRM(STRSUBSTNO(ESK001, "Code Appro")) THEN
                    UpdateItemCodeAppro();
                */
                // ANI Le 23-06-2016 FE20160617

            end;
        }
    }
    trigger OnDelete();
    var
        DemandeLink: Record "Demande Trans. Link";
        DemandeReceipt: Record "Receipt Packing List";
    begin
        DemandeLink.SETRANGE("Demande No.", Rec."No.");
        DemandeLink.DELETEALL();

        DemandeReceipt.SETRANGE("Demande No.", Rec."No.");
        DemandeReceipt.DELETEALL();
    end;

    trigger OnInsert();
    var
        NoSeriesMgt: Codeunit NoSeriesManagement;
    begin
        "Created At" := TODAY;
        Evaluate("Created By", USERID);

        IF "No." = '' THEN
            NoSeriesMgt.InitSeries('DEM-TRANSP', xRec."No. Series", 0D, "No.", "No. Series");
    END;

    var
        CduGFunctions: Codeunit Functions;

    procedure SetCommentaire(NewWorkDescription: Text);
    var
        TempBlob: Record "Upgrade Blob Storage" temporary;
    begin
        CLEAR(Commentaire);
        IF NewWorkDescription = '' THEN
            EXIT;
        TempBlob.Blob := Commentaire;
        CduGFunctions.WriteAsText(NewWorkDescription, TEXTENCODING::Windows);
        Commentaire := TempBlob.Blob;
        MODIFY();
    end;

    procedure GetCommentaire(): Text;
    var
        TempBlob: Record "Upgrade Blob Storage" temporary;
        CR: Text[1];
    begin
        CALCFIELDS(Commentaire);
        IF NOT Commentaire.HASVALUE THEN
            EXIT('');
        CR[1] := 10;
        TempBlob.Blob := Commentaire;
        EXIT(CduGFunctions.ReadAsText(CR, TEXTENCODING::Windows));
    end;

    procedure GetValueVente(): Decimal;
    var
        PackingList: Record "Packing List Line";
        ShipmentLine: Record "Sales Shipment Line";
        value: Decimal;
    begin
        IF Type <> Type::Expédition THEN EXIT(0);
        IF "Packing List No." = '' THEN EXIT(0);
        PackingList.SETRANGE("Packing List No.", Rec."Packing List No.");
        IF PackingList.FINDSET() THEN
            REPEAT
                IF ShipmentLine.GET(PackingList."Posted Source No.", PackingList."source Line No.") THEN
                    value += ShipmentLine.Quantity * ShipmentLine."Net Unit Price";
            UNTIL PackingList.NEXT() = 0;
        EXIT(value);
    end;

    procedure GetValueAchat(): Decimal;
    var
        WhseReceiptHeader: Record "Warehouse Receipt Header";
        WhseReceiptLine: Record "Warehouse Receipt Line";
        DemandeLink: Record "Demande Trans. Link";
        PurchInvoice: Record "Purch. Inv. Header";
        PurchHeader: Record "Purchase Header";
        CurrencyRate: Record "Currency Exchange Rate";
        PurchOrder: Record "Purchase Header";
        value: Decimal;
        amount: Decimal;
        lCurrencyCode: Code[10];
        lCurrencyFactor: Decimal;
    begin
        IF Type <> Type::Réception THEN EXIT(0);
        DemandeLink.SETRANGE("Demande No.", Rec."No.");
        IF DemandeLink.FINDSET() THEN
            REPEAT
                lCurrencyCode := '';
                amount := 0;

                CASE DemandeLink.Type OF
                    // Réception
                    DemandeLink.Type::Receipt:

                        IF WhseReceiptHeader.GET(DemandeLink."No.") THEN BEGIN

                            WhseReceiptLine.SETRANGE("No.", DemandeLink."No.");
                            IF WhseReceiptLine.FINDSET() THEN
                                REPEAT
                                    amount += WhseReceiptLine.GetInfoLigne('TOTALNET2');
                                    ;
                                    IF lCurrencyCode = '' THEN
                                        IF PurchOrder.GET(PurchOrder."Document Type"::Order, WhseReceiptLine."Source No.") THEN BEGIN
                                            lCurrencyCode := PurchOrder."Currency Code";
                                            lCurrencyFactor := PurchOrder."Currency Factor";
                                        END;
                                UNTIL WhseReceiptLine.NEXT() = 0;

                            IF (lCurrencyCode <> '') AND (lCurrencyCode <> 'EUR') THEN
                                amount := CurrencyRate.ExchangeAmtFCYToLCY(WORKDATE(), lCurrencyCode, amount, lCurrencyFactor);
                            value += amount;
                        END;


                    // Facture achat
                    DemandeLink.Type::"Purch. Invoice":
                        BEGIN
                            PurchHeader.SETAUTOCALCFIELDS(Amount);
                            IF PurchHeader.GET(PurchHeader."Document Type"::Invoice, DemandeLink."No.") THEN BEGIN
                                amount := PurchHeader.Amount;
                                IF (PurchHeader."Currency Code" <> '') AND (PurchHeader."Currency Code" <> 'EUR') THEN
                                    amount := CurrencyRate.ExchangeAmtFCYToLCY(WORKDATE(), PurchHeader."Currency Code", amount, PurchHeader."Currency Factor");
                                value += amount;
                            END;
                        END;

                    // Facture achat enregistrée
                    DemandeLink.Type::"Purch. Invoice Archive":
                        BEGIN
                            PurchInvoice.SETAUTOCALCFIELDS(Amount);
                            IF PurchInvoice.GET(DemandeLink."No.") THEN BEGIN
                                amount := PurchInvoice.Amount;
                                IF (PurchInvoice."Currency Code" <> '') AND (PurchInvoice."Currency Code" <> 'EUR') THEN
                                    amount := CurrencyRate.ExchangeAmtFCYToLCY(WORKDATE(), PurchInvoice."Currency Code", amount, PurchInvoice."Currency Factor");
                                value += amount;
                            END;
                        END;
                END;
            UNTIL DemandeLink.NEXT() = 0;

        EXIT(value);
    end;

    procedure GetCoutGlobal(): Decimal;
    begin
        EXIT("Frais annexe" + "Frais douane" + "Frais emballage" + "Frais Taxe Gasoil" + "Cout Transport");
    end;

    procedure GetReceiptVolume(): Decimal;
    var
        ReceiptLine: Record "Receipt Packing List";
        value: Decimal;
    begin
        ReceiptLine.SETRANGE("Demande No.", Rec."No.");
        IF ReceiptLine.FINDSET() THEN
            REPEAT
                value += ReceiptLine.GetVolume();
            UNTIL ReceiptLine.NEXT() = 0;
        EXIT(value);
    end;

    procedure GetReceiptPoids(): Decimal;
    var
        ReceiptLine: Record "Receipt Packing List";
        value: Decimal;
    begin
        ReceiptLine.SETRANGE("Demande No.", Rec."No.");
        IF ReceiptLine.FINDSET() THEN
            REPEAT
                value += ReceiptLine.Quantité * ReceiptLine.Poids;
            UNTIL ReceiptLine.NEXT() = 0;
        EXIT(value);
    end;

    local procedure CheckAxeTiersCustomer();
    var
    // DimensionValue: Record "Dimension Value";
    // Customer: Record Customer;
    // Vendor: Record Vendor;
    // DefaultDimension: Record "Default Dimension";
    begin
        exit;
        //*******************************Non utiliser*********************************************
        // IF "Customer No." <> '' THEN BEGIN
        //     DefaultDimension.SETRANGE("Table ID", DATABASE::Customer);
        //     DefaultDimension.SETRANGE("Dimension Code", 'TIERS');
        //     DefaultDimension.SETRANGE("No.", "Customer No.");
        //     IF DefaultDimension.ISEMPTY THEN BEGIN
        //         Customer.GET("Customer No.");

        //         DimensionValue.INIT();
        //         DimensionValue.VALIDATE("Dimension Code", 'TIERS');
        //         DimensionValue.VALIDATE(Code, "Customer No.");
        //         DimensionValue.VALIDATE(Name, Customer.Name);
        //         DimensionValue.INSERT(TRUE);

        //         CLEAR(DefaultDimension);
        //         DefaultDimension.VALIDATE("Table ID", DATABASE::Customer);
        //         DefaultDimension.VALIDATE("No.", "Customer No.");
        //         DefaultDimension.VALIDATE("Dimension Code", 'TIERS');
        //         DefaultDimension.VALIDATE("Dimension Value Code", "Customer No.");
        //         DefaultDimension.INSERT(TRUE);
        //     END;
        // END;
        //*******************************Non utiliser*********************************************
    end;

    local procedure CheckAxeTiersVendor();
    var
    // DimensionValue: Record "Dimension Value";
    // Customer: Record Customer;
    // Vendor: Record Vendor;
    // DefaultDimension: Record "Default Dimension";
    begin
        EXIT;
        //*******************************Non utiliser*********************************************
        // IF "Vendor No." <> '' THEN BEGIN
        //     DefaultDimension.SETRANGE("Table ID", DATABASE::Vendor);
        //     DefaultDimension.SETRANGE("Dimension Code", 'TIERS');
        //     DefaultDimension.SETRANGE("No.", "Vendor No.");
        //     IF DefaultDimension.ISEMPTY THEN BEGIN
        //         Vendor.GET("Vendor No.");

        //         DimensionValue.INIT();
        //         DimensionValue.VALIDATE("Dimension Code", 'TIERS');
        //         DimensionValue.VALIDATE(Code, "Vendor No.");
        //         DimensionValue.VALIDATE(Name, Vendor.Name);
        //         DimensionValue.INSERT(TRUE);

        //         CLEAR(DefaultDimension);
        //         DefaultDimension.VALIDATE("Table ID", DATABASE::Vendor);
        //         DefaultDimension.VALIDATE("No.", "Vendor No.");
        //         DefaultDimension.VALIDATE("Dimension Code", 'TIERS');
        //         DefaultDimension.VALIDATE("Dimension Value Code", "Vendor No.");
        //         DefaultDimension.INSERT(TRUE);
        //     END;
        // END;
        //*******************************Non utiliser*********************************************
    end;
    //*******************************Non utiliser*********************************************
    // local procedure CheckAxeShippingAgent();
    // var
    //     DimensionValue: Record "Dimension Value";
    //     ShippingAgent: Record "Shipping Agent";
    //     DefaultDimension: Record "Default Dimension";
    // begin
    //     EXIT;
    // IF "Shipping Vendor No." <> '' THEN BEGIN
    //     DefaultDimension.SETRANGE("Table ID", DATABASE::"Shipping Agent");
    //     DefaultDimension.SETRANGE("Dimension Code", 'TRANSPORTEUR');
    //     DefaultDimension.SETRANGE("No.", "Shipping Vendor No.");
    //     IF DefaultDimension.ISEMPTY THEN BEGIN
    //         ShippingAgent.GET("Shipping Vendor No.");

    //         DimensionValue.INIT();
    //         DimensionValue.VALIDATE("Dimension Code", 'TRANSPORTEUR');
    //         DimensionValue.VALIDATE(Code, "Shipping Vendor No.");
    //         DimensionValue.VALIDATE(Name, ShippingAgent.Name);
    //         DimensionValue.INSERT(TRUE);

    //         CLEAR(DefaultDimension);
    //         DefaultDimension.VALIDATE("Table ID", DATABASE::"Shipping Agent");
    //         DefaultDimension.VALIDATE("No.", "Shipping Vendor No.");
    //         DefaultDimension.VALIDATE("Dimension Code", 'TRANSPORTEUR');
    //         DefaultDimension.VALIDATE("Dimension Value Code", "Shipping Vendor No.");
    //         DefaultDimension.INSERT(TRUE);
    //     END;
    // END;
    // end;
    //*******************************Non utiliser*********************************************

    PROCEDURE GetCalculateWeigh(): Decimal;
    VAR
        lPackingListHeader: Record "Packing List Header";
    BEGIN
        IF lPackingListHeader.GET(Rec."Packing List No.") THEN BEGIN
            lPackingListHeader.CALCFIELDS("Calculate Header Weight");
            EXIT(lPackingListHeader."Calculate Header Weight")
        END
        ELSE
            EXIT(0);
    END;
}

