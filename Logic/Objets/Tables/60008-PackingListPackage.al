tableextension 60008 "Packing List Package" extends "Packing List Package" //60008
{
    fields
    {
        modify("Package Weight")
        {
            trigger OnAfterValidate()
            begin
                //>>SFD20201005
                CalculateNetWeight();
                //<<SFD20201005
            end;
        }
        modify(Length)
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume()
            end;
        }
        modify(Width)
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume()
            end;
        }
        modify(Height)
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume()
            end;
        }
        modify("Tare Weight")
        {
            trigger OnAfterValidate()
            begin
                //>>SFD20201005
                CalculateNetWeight();
                //<<SFD20201005
            end;
        }
    }

    trigger OnDelete()
    var
        lPackingListLine: Record "Packing List Line";
    begin
        CheckPackageStatusOpen();
        // Mise à jour des lignes
        lPackingListLine.SETRANGE("Packing List No.", Rec."Packing List No.");
        lPackingListLine.SETRANGE("Package No.", Rec."Package No.");
        IF lPackingListLine.FINDSET() THEN
            lPackingListLine.MODIFYALL("Package No.", '');
    end;

    trigger OnInsert()
    var
        lPackingListHeader: Record "Packing List Header";
    begin
        // Numérotation automatique
        IF (Rec."Package No." = '') AND (lPackingListHeader.GET(Rec."Packing List No.")) THEN
            lPackingListHeader.GetNextColisNoIndice(Rec."Package No.", Rec."Indice Colis");


        CalculateVolume();
    end;

    trigger OnRename()
    var
        lPackingListLine: Record "Packing List Line";
        lChar1: Char;
        lChar2: Char;
        lEntier1: Integer;
        lEntier2: Integer;
    begin
        CheckPackageStatusOpen();
        // Recalcule l'indice
        IF (Rec."Package No." <> xRec."Package No.") THEN BEGIN
            IF STRLEN(Rec."Package No.") = 1 THEN BEGIN
                lChar1 := Rec."Package No."[1];
                EVALUATE(lEntier1, FORMAT(lChar1, 0, '<NUMBER>'));
                Rec."Indice Colis" := lEntier1 - 64;
            END;
            IF STRLEN(Rec."Package No.") = 2 THEN BEGIN
                lChar1 := Rec."Package No."[1];
                EVALUATE(lEntier1, FORMAT(lChar1, 0, '<NUMBER>'));
                lChar2 := Rec."Package No."[2];
                EVALUATE(lEntier2, FORMAT(lChar2, 0, '<NUMBER>'));
                Rec."Indice Colis" := ((lEntier1 - 64) * 26) + (lEntier2 - 64);
            END;
        END;
        // Mise à jour des lignes
        lPackingListLine.SETRANGE("Packing List No.", Rec."Packing List No.");
        lPackingListLine.SETRANGE("Package No.", xRec."Package No.");
        IF lPackingListLine.FINDSET() THEN
            lPackingListLine.MODIFYALL("Package No.", Rec."Package No.");
    end;

    local procedure CalculateVolume()
    begin
        // Volume en m3 et dimensions en cm >> donc facture 100 x 100 x 100 = 1 000 000
        Volume := Length * Width * Height / 1000000;
    end;

    local procedure CheckPackageStatusOpen()
    var
        lPackingListHeader: Record "Packing List Header";
    begin
        // la notion de statut du package est identique à celle de l'en-tête
        IF lPackingListHeader.GET("Packing List No.") THEN
            lPackingListHeader.CheckHeaderStatusOpen();
    end;

    local procedure CalculateNetWeight()
    begin
        //>>SFD20201005
        "Net Weight" := "Package Weight" - "Tare Weight";
        //<<SFD20201005
    end;
}

