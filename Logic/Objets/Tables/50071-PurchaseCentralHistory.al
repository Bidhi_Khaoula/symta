tableextension 50271 "Purchase Central History" extends "Purchase Central History" //50071
{
    // DrillDownPageID = 50300;
    // LookupPageID = 50300;

    fields
    {
        modify("Starting Date")
        {
            trigger OnAfterValidate()
            begin
                IF ("Starting Date" > "Ending Date") AND ("Ending Date" <> 0D) THEN
                    ERROR(Text000Err, FIELDCAPTION("Starting Date"), FIELDCAPTION("Ending Date"));
            end;
        }
        modify("Ending Date")
        {
            trigger OnAfterValidate()
            begin
                VALIDATE("Starting Date");
            end;
        }

    }

    trigger OnInsert()
    begin
        CheckDates(FALSE);
    end;

    trigger OnModify()
    begin
        CheckDates(FALSE);
    end;

    trigger OnRename()
    begin
        CheckDates(TRUE);
    end;

    var
        Text000Err: Label '%1 cannot be after %2', Comment = '%1 = Starting Date ; %2 = Ending Date';

    local procedure CheckDates(pRename: Boolean)
    var
        lPurchaseCentralHistory: Record "Purchase Central History";
        lText000Err: Label 'Il existe déjà une centrale active (%1) pour ce client (%2) sur cette période', Comment = '%1 = Centrale active ; %2 = N° client';
    begin
        TESTFIELD("Customer No.");
        TESTFIELD("Active Central");
        IF ("Ending Date" = 0D) THEN
            VALIDATE("Ending Date", 99991231D);
        // Une seule centrale active sur une même période pour un même client
        lPurchaseCentralHistory.RESET();
        lPurchaseCentralHistory.SETRANGE("Customer No.", "Customer No.");
        IF pRename THEN
            lPurchaseCentralHistory.SETFILTER("Starting Date", '<>%1', xRec."Starting Date")
        ELSE
            lPurchaseCentralHistory.SETFILTER("Starting Date", '<>%1', "Starting Date");
        IF lPurchaseCentralHistory.FINDSET() THEN
            REPEAT
                IF ("Starting Date" = 0D) AND ("Ending Date" = 0D) THEN
                    ERROR(lText000Err, lPurchaseCentralHistory."Active Central", lPurchaseCentralHistory."Customer No.");
                IF ("Starting Date" = 0D) AND ("Ending Date" > 0D) AND
                    (lPurchaseCentralHistory."Starting Date" <= "Ending Date") THEN
                    ERROR(lText000Err, lPurchaseCentralHistory."Active Central", lPurchaseCentralHistory."Customer No.");
                IF ("Starting Date" > 0D) AND ("Ending Date" = 0D) AND
                    ((lPurchaseCentralHistory."Ending Date" >= "Starting Date") OR (lPurchaseCentralHistory."Ending Date" = 0D)) THEN
                    ERROR(lText000Err, lPurchaseCentralHistory."Active Central", lPurchaseCentralHistory."Customer No.");
                IF ("Starting Date" > 0D) AND ("Ending Date" > 0D) AND
                    (lPurchaseCentralHistory."Starting Date" <= "Ending Date") AND
                    ((lPurchaseCentralHistory."Ending Date" >= "Starting Date") OR (lPurchaseCentralHistory."Ending Date" = 0D)) THEN
                    ERROR(lText000Err, lPurchaseCentralHistory."Active Central", lPurchaseCentralHistory."Customer No.");
            UNTIL lPurchaseCentralHistory.NEXT() = 0;
    end;

    procedure GetCentralFromCustomerDate(pCustomerNo: Code[20]; pDate: Date): Code[20]
    var
        lPurchaseCentralHistory: Record "Purchase Central History";
    begin
        lPurchaseCentralHistory.RESET();
        lPurchaseCentralHistory.SETRANGE("Customer No.", "Customer No.");
        lPurchaseCentralHistory.SETFILTER("Starting Date", '>=', pDate);
        IF lPurchaseCentralHistory.FINDSET() THEN
            REPEAT
                IF ((lPurchaseCentralHistory."Ending Date" >= pDate) OR (lPurchaseCentralHistory."Ending Date" = 0D)) THEN
                    EXIT(lPurchaseCentralHistory."Active Central");
            UNTIL lPurchaseCentralHistory.NEXT() = 0;
        EXIT('');
    end;

    procedure GetAdherentFromCustomerCentralDate(pCustomerNo: Code[20]; pCentralNo: Code[20]; pDate: Date): Code[20]
    var
        lPurchaseCentralHistory: Record "Purchase Central History";
    begin
        // Retourne le dernier code adhérent d'un client pour une centrale à une date donnée
        // utilisé dans l'export des factures EDI
        lPurchaseCentralHistory.SETCURRENTKEY("Customer No.", "Starting Date");
        lPurchaseCentralHistory.RESET();
        lPurchaseCentralHistory.SETRANGE("Customer No.", pCustomerNo);
        lPurchaseCentralHistory.SETRANGE("Active Central", pCentralNo);
        lPurchaseCentralHistory.SETFILTER("Starting Date", '<=%1', pDate);
        IF lPurchaseCentralHistory.FINDLAST() THEN
            EXIT(lPurchaseCentralHistory."Libre 3");
        EXIT('');
    end;
}

