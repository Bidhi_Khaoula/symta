tableextension 50257 "Article Critere" extends "Article Critere" //50057
{
    LookupPageID = "Item Critere List";

    fields
    {
        modify(Text)
        {
            trigger OnAfterValidate()
            begin
                Rec_Critere.GET("Code Critere");
                Rec_Critere.TESTFIELD(Type, Rec_Critere.Type::Texte);
            end;
        }
        modify("Booléen")
        {
            trigger OnAfterValidate()
            begin
                Rec_Critere.GET("Code Critere");
                Rec_Critere.TESTFIELD(Type, Rec_Critere.Type::Booléen);
            end;
        }
        modify(Entier)
        {
            trigger OnAfterValidate()
            begin
                Rec_Critere.GET("Code Critere");
                Rec_Critere.TESTFIELD(Type, Rec_Critere.Type::Entier);
            end;
        }
        modify("Décimal")
        {

            trigger OnAfterValidate()
            begin
                Rec_Critere.GET("Code Critere");
                Rec_Critere.TESTFIELD(Type, Rec_Critere.Type::Décimal);
            end;
        }

    }

    trigger OnDelete()
    var
        lItem: Record Item;
    begin
        // ANI Le 07-09-2017 Ticket 21670 - Régie : actualisation de la date de mise à jour de l'article
        IF lItem.GET("Code Article") THEN
            lItem.MODIFY(TRUE);
    end;

    trigger OnInsert()
    var
        LItem: Record Item;
        LFamCrite: Record "Famille Critere";
    begin



        IF LItem.GET("Code Article") THEN BEGIN
            IF LFamCrite.GET(LItem."Super Famille Marketing", LItem."Famille Marketing", LItem."Sous Famille Marketing", "Code Critere") THEN
                Priorité := LFamCrite.Priorité;

            // ANI Le 07-09-2017 Ticket 21670 - Régie : actualisation de la date de mise à jour de l'article
            LItem.MODIFY(TRUE);
        END;

        // ESKAPE => Traçabilité des enregistrements -> FE20190525
        Evaluate("Create User ID", USERID);
        "Create Date" := TODAY;
        "Create Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    trigger OnModify()
    var
        lItem: Record Item;
    begin
        // ANI Le 07-09-2017 Ticket 21670 - Régie : actualisation de la date de mise à jour de l'article
        IF lItem.GET("Code Article") THEN
            lItem.MODIFY(TRUE);

        // ESKAPE => Traçabilité des enregistrements -> FE20190525
        Evaluate("Modify User ID", USERID);
        "Modify Date" := TODAY;
        "Modify Time" := TIME;
        // FIN ESKAPE => Traçabilité des enregistrements
    end;

    var
        Rec_Critere: Record Critere;

    procedure GetTypeCritere(): Text[50]
    begin
        IF Rec_Critere.GET("Code Critere") THEN 
            EXIT(FORMAT(Rec_Critere.Type));
    end;

    procedure GetTypeValue(): Text[250]
    begin
        CASE GetTypeCritere() OF
            'Text', 'Texte':
                EXIT(Text);
            'Booléen':
                EXIT(FORMAT(Booléen));
            'Entier':
                EXIT(FORMAT(Entier));
            'Décimal':
                EXIT(FORMAT(Décimal));
        END;
    end;

    procedure GetNameCritere(): Text[100]
    begin
        IF Rec_Critere.GET("Code Critere") THEN
            EXIT(Rec_Critere.Nom)
        ELSE
            EXIT('');
    end;

    procedure GetUinteCritere(): Text[30]
    begin
        IF Rec_Critere.GET("Code Critere") THEN
            EXIT(Rec_Critere.Unité)
        ELSE
            EXIT('');
    end;
}

