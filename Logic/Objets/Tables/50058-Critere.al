tableextension 50258 "Critere" extends Critere //50058
{
    LookupPageID = "Critere List";

    fields
    {
        modify(Type)
        {
            trigger OnAfterValidate()
            var
                rec_ItemCriteria: Record "Article Critere";
            begin
                //----------------------------------------------------------------------------------------------------------------------------------
                //WF le 18/02/2011
                rec_ItemCriteria.RESET();
                rec_ItemCriteria.SETRANGE("Code Critere", Code);
                IF rec_ItemCriteria.COUNT > 0 THEN ERROR('Impossible le critère est utilisé');
                //Fin WF le 18/02/2011
                //----------------------------------------------------------------------------------------------------------------------------------
            end;
        }
    }
    trigger OnDelete()
    var
        rec_ItemCriteria: Record "Article Critere";
    begin
        //----------------------------------------------------------------------------------------------------------------------------------
        //WF le 18/02/2011
        rec_ItemCriteria.RESET();
        rec_ItemCriteria.SETRANGE("Code Critere", Code);
        IF rec_ItemCriteria.COUNT > 0 THEN ERROR('Supprimer le critère utilisé dans chaque articles utilisant');
        //Fin WF le 18/02/2011
        //----------------------------------------------------------------------------------------------------------------------------------
    end;
}

