tableextension 50269 "Translation SYMTA" extends "Translation SYMTA" //50069
{

    fields
    {
        modify("Table No")
        {
            trigger OnAfterValidate()
            var
                "Field": Record Field;
            begin
                CLEAR(Field);
                Field.GET("Table No", "Champ table");
                IF (STRLEN(Description)) > (Field.Len) THEN
                    ERROR(Format(STRSUBSTNO(Error001Err, Caption, Field.Len)));
                Nom := Field.FieldName;
                Caption := Field."Field Caption";
            end;
        }
        modify("Champ table")
        {
            trigger OnAfterValidate()
            var
                "Field": Record Field;
            begin
                Field.RESET();
                Field.GET(Field.TableNo, "Champ table");
                IF (STRLEN(Description)) > (Field.Len) THEN
                    ERROR(Format(STRSUBSTNO(Error001Err, Caption, Field.Len)));
                Nom := Field.FieldName;
                Caption := Field."Field Caption";
            end;
        }
    }

    var
        Error001Err: Label 'Le champ %1 a une longueur maximale de %2 caractères.', Comment = '%1 = Caption ; %2 = Length';
}

