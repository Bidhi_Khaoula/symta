tableextension 50221 "Saisis Réception Magasin" extends "Saisis Réception Magasin"//50021
{
    procedure GetRefActive(): Code[50]
    var
        lItem: Record Item;
    begin
        lItem.GET("Item No.");
        EXIT(lItem."No. 2");
    end;

    procedure GetRefFournisseur(): Code[50]
    var
        lWhseReceiptHeader: Record "Warehouse Receipt Header";
        lItemVendor: Record "Item Vendor";
    begin
        lWhseReceiptHeader.GET("No.");
        lItemVendor.RESET();
        lItemVendor.SETRANGE("Item No.", "Item No.");
        lItemVendor.SETRANGE("Vendor No.", lWhseReceiptHeader.GetVendorNo());
        IF lItemVendor.FINDFIRST() THEN
            EXIT(lItemVendor."Vendor Item No.");
    end;
}

