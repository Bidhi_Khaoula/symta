tableextension 50218 "Ventes Manuelles" extends "Ventes Manuelles" //50018
{

    trigger OnInsert()
    begin
        TESTFIELD("Item No.");
        TESTFIELD("Posting Date");

        VALIDATE("Utilisateur Création", USERID);
        VALIDATE("Date Création", TODAY);
        VALIDATE("Heure Création", TIME);
    end;
}

