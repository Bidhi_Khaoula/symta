tableextension 60007 "Packing List Line" extends "Packing List Line" //60007
{
    fields
    {
        modify("Package No.")
        {
            trigger OnAfterValidate()
            var
                lPackingListPackage: Record "Packing List Package";
                lConfirmLbl: Label 'Le n° %1 ne correspond pas à un colis existant. Souhaitez vous le créer ?', Comment = '%1 = N° colis';
            begin
                CheckLineStatusOpen();
                IF (Rec."Package No." <> '') THEN
                    // Propose la création d'un nouveau colis si c'est la première fois
                    IF (NOT lPackingListPackage.GET(Rec."Packing List No.", Rec."Package No.")) THEN
                        IF CONFIRM(STRSUBSTNO(lConfirmLbl, Rec."Package No."), TRUE) THEN BEGIN
                            lPackingListPackage."Packing List No." := Rec."Packing List No.";
                            lPackingListPackage."Package No." := Rec."Package No.";
                            // type par défaut
                            lPackingListPackage."Package Type" := lPackingListPackage."Package Type"::Carton;
                            lPackingListPackage.INSERT();
                        END;
            end;
        }

        modify("Quantity to Ship")
        {
            trigger OnAfterValidate()
            begin
                // vérification des quantité
                VerifyQuantity();
            end;
        }
        modify("Line Quantity")
        {
            trigger OnAfterValidate()
            begin
                // vérification des quantité
                VerifyQuantity();
                // Calcul du poids de la ligne
                CalculateWeight();
            end;
        }
        modify("Unit Weight")
        {
            trigger OnAfterValidate()
            begin
                CalculateWeight()
            end;
        }
        modify("WIIO Préparation")
        {
            trigger OnAfterValidate()
            var
            // LItem: Record Item;
            begin
                /*
                IF "Term Missing" THEN BEGIN
                  LItem.GET("Item No.");
                  LItem.SETRANGE("Location Filter", "Location Code");
                  LItem.CALCFIELDS(Inventory);
                  VALIDATE("Stock on validation", LItem.Inventory);
                END ELSE
                  VALIDATE("Stock on validation", 0);
                */

            end;
        }
    }

    trigger OnDelete()
    var
        lWhseShipmentHeader: Record "Warehouse Shipment Header";
        lPackingListLine: Record "Packing List Line";
    begin
        CheckLineStatusOpen();

        // suppression des lignes
        lPackingListLine.SETRANGE("Packing List No.", Rec."Packing List No.");
        lPackingListLine.SETRANGE("Whse. Shipment No.", Rec."Whse. Shipment No.");
        lPackingListLine.SETFILTER("Line No.", '<>%1', Rec."Line No.");
        IF lPackingListLine.IsEmpty() THEN
            // maj de l'en-tˆte
            IF lWhseShipmentHeader.GET(Rec."Whse. Shipment No.") THEN BEGIN
                lWhseShipmentHeader."Packing List No." := '';
                lWhseShipmentHeader.MODIFY(TRUE);
            END;
    end;

    trigger OnInsert()
    var
        lTmpPackingListLine: Record "Packing List Line";
    begin
        // n° de Packing List obligatoire
        Rec.TESTFIELD("Packing List No.");
        // statut
        Rec."Line Status" := Rec."Line Status"::Open;

        // Attribution du n° de ligne
        IF (Rec."Line No." = 0) THEN BEGIN
            lTmpPackingListLine.SETRANGE("Packing List No.", "Packing List No.");
            IF lTmpPackingListLine.FINDLAST() THEN
                Rec."Line No." := lTmpPackingListLine."Line No." + 10000
            ELSE
                Rec."Line No." := 10000;
        END;

        // Calcul du poids
        CalculateWeight();
        // vérification des quantité
        VerifyQuantity();
    end;

    trigger OnModify()
    var
        lPackingListHeader: Record "Packing List Header";
    begin
        IF lPackingListHeader.GET(Rec."Packing List No.") THEN
            lPackingListHeader.RefreshHeaderStatus();
        // Calcul du poids
        CalculateWeight();
        // vérification des quantité
        VerifyQuantity();
    end;

    local procedure CalculateWeight()
    begin
        Rec."Line Weight" := Rec."Line Quantity" * Rec."Unit Weight";
    end;

    local procedure VerifyQuantity()
    var
        lPackingListLine: Record "Packing List Line";
        lSomme: Decimal;
        lErrorLbl: Label 'Quantité ligne(s) (%1) ne peut pas être supérieure à Quantité à expédier (%2) pour article %3', Comment = '%1 = Somme ; %2 = Quantité à expédier; %3 = N° article ';
    begin
        lPackingListLine.SETCURRENTKEY("Packing List No.", "Whse. Shipment No.", "Whse. Shipment Line No.");
        lPackingListLine.SETRANGE("Packing List No.", Rec."Packing List No.");
        lPackingListLine.SETRANGE("Whse. Shipment No.", Rec."Whse. Shipment No.");
        lPackingListLine.SETRANGE("Whse. Shipment Line No.", Rec."Whse. Shipment Line No.");
        lPackingListLine.SETFILTER("Line No.", '<>%1', Rec."Line No.");
        // CFR Le 14/09/2020 : WIIO > on ne prend en compte que ce qui est déjà en carton...
        lPackingListLine.SETFILTER("Package No.", '<>%1', '');
        lPackingListLine.CALCSUMS("Line Quantity");
        lSomme := lPackingListLine."Line Quantity";
        IF Rec."Package No." <> '' THEN
            lSomme := lSomme + Rec."Line Quantity";
        IF (lSomme > Rec."Quantity to Ship") THEN
            ERROR(lErrorLbl, lSomme, Rec."Quantity to Ship", Rec."Item No.");
    END;

    local procedure CheckLineStatusOpen()
    var
        lErrorLbl: Label 'La ligne %1 n''''est pas au statut ouvert : Action impossible', Comment = '%1= N° Ligne';
    begin
        // si modification du statut : pas de test
        IF xRec."Line Status" <> Rec."Line Status" THEN
            EXIT;
        IF Rec."Line Status" <> Rec."Line Status"::Open THEN
            ERROR(lErrorLbl, Rec."Line No.");
    end;
}

