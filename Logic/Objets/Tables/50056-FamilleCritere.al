tableextension 50256 "Famille Critere" extends "Famille Critere" //50056
{
    LookupPageID = "Famille Critere List";

    fields
    {

        modify("Priorité")
        {
            trigger OnAfterValidate()
            var
                Rec_ListeCritereArticle: Record "Article Critere";
            begin
                // MCO Le 17-01-2018 => Réecriture des hierarchies
                Rec_ListeCritereArticle.RESET();
                Rec_ListeCritereArticle.SETRANGE("Super Famille Marketing", "Super Famille");
                Rec_ListeCritereArticle.SETRANGE("Famille Marketing", Famille);
                Rec_ListeCritereArticle.SETRANGE("Sous Famille Marketing", "Sous Famille");
                Rec_ListeCritereArticle.SETRANGE("Code Critere", "Code Critere");
                Rec_ListeCritereArticle.MODIFYALL(Priorité, Priorité);
                // FIN MCO Le 17-01-2018
                /*
                // AD Le 09-11-2010 => Modif. Des critères
                  Rec_Item.RESET();
                  // ANI Le 07-09-2010 ticket 24714 - Régie
                  //Rec_Item.SETRANGE("Item Category Code", Rec.Famille);
                  //IF "Sous Famille" <> '' THEN
                  //  Rec_Item.SETRANGE("Product Group Code","Sous Famille");
                  IF "Super Famille" <> '' THEN
                    Rec_Item.SETRANGE("Super Famille Marketing", Rec."Super Famille");
                  IF Famille <> '' THEN
                    Rec_Item.SETRANGE("Famille Marketing", Rec.Famille);
                  IF "Sous Famille" <> '' THEN
                    Rec_Item.SETRANGE("Famille Marketing","Sous Famille");
                  // FIN ANI Le 07-09-2010
                  IF Rec_Item.FINDFIRST () THEN BEGIN
                    REPEAT
                      Rec_ListeCritereArticle.INIT();
                      Rec_ListeCritereArticle.SETRANGE("Code Article",Rec_Item."No.");
                      Rec_ListeCritereArticle.SETRANGE("Code Critere", "Code Critere");
                      Rec_ListeCritereArticle.MODIFYALL(Priorité,Priorité);
                    UNTIL Rec_Item.NEXT=0
                  END;
                */

            end;
        }
    }

    trigger OnDelete()
    var
        Rec_ListeCritereArticle: Record "Article Critere";
    begin
        // MCO Le 17-01-2018 => Réecriture des hierarchies
        IF CONFIRM('Voulez-vous supprimer le critère aux articles de la famille ?') THEN BEGIN
            Rec_ListeCritereArticle.INIT();
            Rec_ListeCritereArticle.SETRANGE("Super Famille Marketing", "Super Famille");
            Rec_ListeCritereArticle.SETRANGE("Famille Marketing", Famille);
            Rec_ListeCritereArticle.SETRANGE("Sous Famille Marketing", "Sous Famille");
            Rec_ListeCritereArticle.SETRANGE("Code Critere", "Code Critere");
            Rec_ListeCritereArticle.SETRANGE(Priorité, Priorité);
            Rec_ListeCritereArticle.DELETEALL();
        END;
        // FIN MCO Le 17-01-2018


        /*
        IF CONFIRM('Voulez-vous supprimer le critère aux articles de la famille ?') THEN BEGIN
          Rec_Item.RESET();
          Rec_Item.SETRANGE("Super Famille Marketing", "Super Famille");
          Rec_Item.SETRANGE("Famille Marketing", Rec.Famille);
          IF "Sous Famille" <> '' THEN
            Rec_Item.SETRANGE("Sous Famille Marketing","Sous Famille");
        
          IF Rec_Item.FINDFIRST () THEN BEGIN
            REPEAT
              Rec_ListeCritereArticle.INIT();
              Rec_ListeCritereArticle.SETRANGE("Code Article", Rec_Item."No.");
              Rec_ListeCritereArticle.SETRANGE("Code Critere", "Code Critere");
              Rec_ListeCritereArticle.SETRANGE(Priorité,Priorité);
              Rec_ListeCritereArticle.DELETEALL();
            UNTIL Rec_Item.NEXT=0
          END;
        END;
        */

    end;

    trigger OnInsert()
    var
        Rec_ListeCritereArticle: Record "Article Critere";
        LVariante: Record "Item Variant";
        rec_ItemHierarchy: Record "Item Hierarchies";
    begin
        // MCO Le 17-01-2018 => Réecriture des hierarchies
        IF CONFIRM('Voulez-vous affecter le critère aux articles de la famille ?') THEN BEGIN
            rec_ItemHierarchy.RESET();
            rec_ItemHierarchy.SETRANGE("Super Famille Marketing", "Super Famille");
            rec_ItemHierarchy.SETRANGE("Famille Marketing", Rec.Famille);
            IF "Sous Famille" <> '' THEN
                rec_ItemHierarchy.SETRANGE("Sous Famille Marketing", "Sous Famille");
            IF rec_ItemHierarchy.FINDSET() THEN
                REPEAT
                    Rec_ListeCritereArticle.INIT();
                    Rec_ListeCritereArticle.VALIDATE("Code Article", rec_ItemHierarchy."Code Article");
                    Rec_ListeCritereArticle.VALIDATE("Code Critere", "Code Critere");
                    Rec_ListeCritereArticle.VALIDATE(Priorité, Priorité);
                    Rec_ListeCritereArticle.VALIDATE("Variant Code", '');
                    Rec_ListeCritereArticle."Super Famille Marketing" := "Super Famille";
                    Rec_ListeCritereArticle."Famille Marketing" := Famille;
                    Rec_ListeCritereArticle."Sous Famille Marketing" := "Sous Famille";
                    IF Rec_ListeCritereArticle.INSERT() THEN;

                    // AD Le 01-03-2013 => On ajoute aussi pour les variantes
                    LVariante.RESET();
                    LVariante.SETRANGE("Item No.", rec_ItemHierarchy."Code Article");
                    IF LVariante.FindSet() THEN
                        REPEAT
                            Rec_ListeCritereArticle.VALIDATE("Variant Code", LVariante.Code);
                            IF Rec_ListeCritereArticle.INSERT() THEN;
                        UNTIL LVariante.NEXT() = 0;
                // FIN AD Le 01-03-2013

                UNTIL rec_ItemHierarchy.NEXT() = 0
        END;
        // FIN MCO Le 17-01-2018 => Récriture des hierarchies
        /*
        IF CONFIRM('Voulez-vous affecter le critère aux articles de la famille ?') THEN BEGIN
          Rec_Item.RESET();
          Rec_Item.SETRANGE("Super Famille Marketing", "Super Famille");
          Rec_Item.SETRANGE("Famille Marketing", Rec.Famille);
          IF "Sous Famille" <> '' THEN
            Rec_Item.SETRANGE("Sous Famille Marketing","Sous Famille");
          IF Rec_Item.FINDFIRST () THEN BEGIN
            REPEAT
              Rec_ListeCritereArticle.INIT();
              Rec_ListeCritereArticle.VALIDATE("Code Article", Rec_Item."No.");
              Rec_ListeCritereArticle.VALIDATE("Code Critere", "Code Critere");
              Rec_ListeCritereArticle.VALIDATE(Priorité,Priorité);
              Rec_ListeCritereArticle.VALIDATE("Variant Code", '');
              IF Rec_ListeCritereArticle.INSERT THEN;
        
              // AD Le 01-03-2013 => On ajoute aussi pour les variantes
              LVariante.RESET();
              LVariante.SETRANGE("Item No.", Rec_Item."No.");
              IF LVariante.FINDFIRST () THEN
                REPEAT
                  Rec_ListeCritereArticle.VALIDATE("Variant Code", LVariante.Code);
                  IF Rec_ListeCritereArticle.INSERT THEN;
                UNTIL LVariante.NEXT() = 0;
              // FIN AD Le 01-03-2013
        
            UNTIL Rec_Item.NEXT=0
          END;
        END;
        */

    end;

    trigger OnRename()
    begin
        ERROR('Vous devez supprimer et recréer');
    end;


    procedure get_CriteriaLabel() str_Return: Text[250]
    var
        rec_Criteria: Record Critere;
    begin
        str_Return := '';

        IF rec_Criteria.GET("Code Critere") THEN 
            str_Return := rec_Criteria.Nom;
    end;

    procedure get_CriteriaLabel_Transl(pLanguage: Code[10]) str_Return: Text[250]
    var
        rec_Criteria: Record Critere;
        rec_CriteriaTranslate: Record "Traduction Critere";
    begin
        str_Return := '';

        IF rec_CriteriaTranslate.GET("Code Critere", pLanguage) THEN 
            str_Return := rec_CriteriaTranslate.Nom;

        IF rec_Criteria.GET("Code Critere") THEN BEGIN
            IF str_Return = '' THEN 
                str_Return := rec_Criteria.Nom;
            IF rec_Criteria.Unité <> '' THEN 
                str_Return += '(' + rec_Criteria.Unité + ')';
        END;
    end;

    procedure get_CriteriaUnit() str_Return: Text[250]
    var
        rec_Criteria: Record Critere;
    begin
        str_Return := '';

        IF rec_Criteria.GET("Code Critere") THEN 
            str_Return := rec_Criteria.Unité;
    end;

    procedure get_CriteriaType() str_Return: Text[250]
    var
        rec_Criteria: Record Critere;
    begin
        str_Return := '';

        IF rec_Criteria.GET("Code Critere") THEN 
            str_Return := FORMAT(rec_Criteria.Type);
    end;
}

