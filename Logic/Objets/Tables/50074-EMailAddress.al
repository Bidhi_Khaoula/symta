tableextension 50274 "E-Mail Address" extends "E-Mail Address" //50074
{

    trigger OnInsert();
    begin
        FILTERGROUP(2);
        IF GETFILTER("Entry Type") = FORMAT("Entry Type"::"Client facturation") THEN
            "Entry Type" := "Entry Type"::"Client facturation";
        IF GETFILTER("Customer No.") <> '' THEN
            Evaluate("Customer No.", GETFILTER("Customer No."));
        FILTERGROUP(0);
    end;
}

