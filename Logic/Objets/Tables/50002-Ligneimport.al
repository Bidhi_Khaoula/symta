tableextension 50202 "Ligne import" extends "Ligne import" //50002
{
    procedure "Charge compte lookup"()
    var
        Gl_account: Record "G/L Account";
        client: Record Customer;
        fournisseur: Record Vendor;
    begin
        CASE type_cpt OF
            'G':
                IF PAGE.RUNMODAL(0, Gl_account) = ACTION::LookupOK THEN
                    cpt := Gl_account."No.";

            'T':
                IF COPYSTR(cpt, 1, 2) = '41' THEN
                    IF PAGE.RUNMODAL(0, client) = ACTION::LookupOK THEN
                        cpt := client.CodeComptaDbx
                    ELSE
                        IF PAGE.RUNMODAL(0, fournisseur) = ACTION::LookupOK THEN
                            cpt := fournisseur.CodeComptaDbx;

        END;
    end;

    procedure "verif compte"()
    var
        Gl_account: Record "G/L Account";
        client: Record Customer;
        fournisseur: Record Vendor;
    begin
        CASE type_cpt OF
            'G':
                Gl_account.GET(cpt);

            'T':
                CASE COPYSTR(cpt, 1, 2) OF
                    '41':
                        BEGIN
                            client.SETRANGE(CodeComptaDbx, cpt);
                            client.FINDFIRST();
                        END;
                    '40':
                        BEGIN
                            fournisseur.SETRANGE(CodeComptaDbx, cpt);
                            fournisseur.FINDFIRST();
                        END;
                    ELSE
                        ERROR('erreur saisi compte');
                END;
        END;
    end;
}

