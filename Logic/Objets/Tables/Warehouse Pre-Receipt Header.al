// table 50009 "Warehouse Pre-Receipt Header"
// {

//     Caption = 'Warehouse Receipt Header';
//     LookupPageID = "Warehouse Receipts";
//     Permissions = TableData "Warehouse Receipt Line" = rd;

//     fields
//     {
//         field(1; "No."; Code[20])
//         {
//             Caption = 'No.';

//             trigger OnValidate()
//             begin
//                 WhseSetup.GET();
//                 IF "No." <> xRec."No." THEN BEGIN
//                     NoSeriesMgt.TestManual(WhseSetup."Whse. Receipt Nos.");
//                     "No. Series" := '';
//                 END;
//             end;
//         }
//         field(2; "Location Code"; Code[10])
//         {
//             Caption = 'Location Code';
//             TableRelation = Location WHERE("Use As In-Transit" = CONST(false));

//             trigger OnValidate()
//             var
//                 WhseRcptLine: Record "Warehouse Receipt Line";
//             begin
//                 IF NOT WmsManagement.LocationIsAllowed("Location Code") THEN
//                     ERROR(Text003, "Location Code");

//                 IF "Location Code" <> xRec."Location Code" THEN BEGIN
//                     rec."Zone Code" := '';
//                     rec."Bin Code" := '';
//                     rec."Cross-Dock Zone Code" := '';
//                     rec."Cross-Dock Bin Code" := '';
//                     WhseRcptLine.SETRANGE("No.", "No.");
//                     IF WhseRcptLine.FIND('-') THEN
//                         ERROR(
//                           Text001,
//                           FIELDCAPTION("Location Code"));
//                 END;

//                 GetLocation("Location Code");
//                 Location.TESTFIELD("Require Receive");
//                 IF Location."Directed Put-away and Pick" THEN BEGIN
//                     VALIDATE("Bin Code", Location."Receipt Bin Code");
//                     VALIDATE("Cross-Dock Bin Code", Location."Cross-Dock Bin Code");
//                 END;

//                 IF USERID <> '' THEN BEGIN
//                     FILTERGROUP := 2;
//                     SETRANGE("Location Code", "Location Code");
//                     FILTERGROUP := 0;
//                 END;
//             end;
//         }
//         field(3; "Assigned User ID"; Code[20])
//         {
//             Caption = 'Assigned User ID';
//             TableRelation = "Warehouse Employee" WHERE("Location Code" = FIELD("Location Code"));

//             trigger OnValidate()
//             begin
//                 IF "Assigned User ID" <> '' THEN BEGIN
//                     "Assignment Date" := TODAY;
//                     "Assignment Time" := TIME;
//                 END ELSE BEGIN
//                     "Assignment Date" := 0D;
//                     "Assignment Time" := 0T;
//                 END;
//             end;
//         }
//         field(4; "Assignment Date"; Date)
//         {
//             Caption = 'Assignment Date';
//             Editable = false;
//         }
//         field(5; "Assignment Time"; Time)
//         {
//             Caption = 'Assignment Time';
//             Editable = false;
//         }
//         field(6; "Sorting Method"; Option)
//         {
//             Caption = 'Sorting Method';
//             OptionCaption = ' ,Item,Document,Shelf or Bin,Due Date,Ship-To';
//             OptionMembers = " ",Item,Document,"Shelf or Bin","Due Date","Ship-To";

//             trigger OnValidate()
//             begin
//                 IF "Sorting Method" <> xRec."Sorting Method" THEN
//                     SortWhseDoc;
//             end;
//         }
//         field(7; "No. Series"; Code[10])
//         {
//             Caption = 'No. Series';
//             TableRelation = "No. Series";
//         }
//         field(10; "Document Status"; Option)
//         {
//             Caption = 'Document Status';
//             Editable = false;
//             OptionCaption = ' ,Partially Received,Completely Received';
//             OptionMembers = " ","Partially Received","Completely Received";
//         }
//         field(11; Comment; Boolean)
//         {
//             CalcFormula = Exist("Warehouse Comment Line" WHERE("Table Name" = CONST("Whse. Receipt"), Type = CONST(" "), "No." = FIELD("No.")));
//             Caption = 'Comment';
//             Editable = false;
//             FieldClass = FlowField;
//         }
//         field(12; "Posting Date"; Date)
//         {
//             Caption = 'Posting Date';
//         }
//     }

//     keys
//     {
//         key(Key1; "No.")
//         {
//         }
//         key(Key2; "Location Code")
//         {
//         }
//     }

//     fieldgroups
//     {
//     }

//     trigger OnDelete()
//     begin
//         DeleteRelatedLines(TRUE);
//     end;

//     trigger OnInsert()
//     var
//         PurchSetup: Record "Purchases & Payables Setup";
//     begin
//         WhseSetup.GET();
//         IF "No." = '' THEN BEGIN
//             WhseSetup.TESTFIELD("Whse. Receipt Nos.");
//             NoSeriesMgt.InitSeries(WhseSetup."Whse. Receipt Nos.", xRec."No. Series", "Posting Date", "No.", "No. Series");
//         END;

//         NoSeriesMgt.SetDefaultSeries("Receiving No. Series", WhseSetup."Posted Whse. Receipt Nos.");

//         GetLocation("Location Code");
//         VALIDATE("Bin Code", Location."Receipt Bin Code");
//         VALIDATE("Cross-Dock Bin Code", Location."Cross-Dock Bin Code");
//         "Posting Date" := WORKDATE;

//         // AD Le 22-09-2009 => FARGROUP -> Cout -> Recherche des infos par defaut
//         PurchSetup.GET();
//         rec.VALIDATE("% Facture HK", PurchSetup."% Facture HK");
//         rec.VALIDATE("% Assurance", PurchSetup."% Assurance");
//         rec.VALIDATE("% Coef Réception", PurchSetup."% Coef Réception");
//         // FIN AD Le 22-09-2009
//     end;

//     trigger OnRename()
//     begin
//         ERROR(Text000, TABLECAPTION);
//     end;

//     var
//         Location: Record Location;
//         WhseRcptHeader: Record "Warehouse Receipt Header";
//         WhseRcptLine: Record "Warehouse Receipt Line";
//         WhseSetup: Record "Warehouse Setup";
//         WhseCommentLine: Record "Warehouse Comment Line";
//         NoSeriesMgt: Codeunit NoSeriesManagement;
//         WmsManagement: Codeunit "WMS Management";
//         Text000: Label 'You cannot rename a %1.';
//         Text001: Label 'You cannot change the %1, because the document has one or more lines.';
//         Text002: Label 'You must first set up user %1 as a warehouse employee.';
//         Text003: Label 'You are not allowed to use location code %1.';
//         Text005: Label 'must not be the %1 of the %2';
//         Text006: Label 'You have changed %1 on the %2, but it has not been changed on the existing Warehouse Receipt Lines.\';
//         Text007: Label 'You must update the existing Warehouse Receipt Lines manually.';
//         HideValidationDialog: Boolean;
//         Text008: Label 'The Whse. Receipt is not completely received.\Do you really want to delete the Whse. Receipt?';
//         Text009: Label 'Cancelled.';

//     procedure AssistEdit(OldWhseRcptHeader: Record "Warehouse Receipt Header"): Boolean
//     begin
//         WhseSetup.GET();
//         WITH WhseRcptHeader DO BEGIN
//             WhseRcptHeader := Rec;
//             WhseSetup.TESTFIELD("Whse. Receipt Nos.");
//             IF NoSeriesMgt.SelectSeries(
//               WhseSetup."Whse. Receipt Nos.", OldWhseRcptHeader."No. Series", "No. Series")
//             THEN BEGIN
//                 NoSeriesMgt.SetSeries("No.");
//                 Rec := WhseRcptHeader;
//                 EXIT(TRUE);
//             END;
//         END;
//     end;

//     procedure SortWhseDoc()
//     var
//         SequenceNo: Integer;
//     begin
//         WhseRcptLine.RESET();
//         WhseRcptLine.SETRANGE("No.", "No.");
//         GetLocation("Location Code");
//         CASE "Sorting Method" OF
//             "Sorting Method"::Item:
//                 WhseRcptLine.SETCURRENTKEY("No.", "Item No.");
//             "Sorting Method"::Document:
//                 WhseRcptLine.SETCURRENTKEY("No.", "Source Document", "Source No.");
//             "Sorting Method"::"Shelf or Bin":
//                 BEGIN
//                     IF Location."Bin Mandatory" THEN
//                         WhseRcptLine.SETCURRENTKEY("No.", "Bin Code")
//                     ELSE
//                         WhseRcptLine.SETCURRENTKEY("No.", "Shelf No.");
//                 END;
//             "Sorting Method"::"Due Date":
//                 WhseRcptLine.SETCURRENTKEY("No.", "Due Date");
//         END;

//         IF WhseRcptLine.FIND('-') THEN BEGIN
//             SequenceNo := 10000;
//             REPEAT
//                 WhseRcptLine."Sorting Sequence No." := SequenceNo;
//                 WhseRcptLine.MODIFY();
//                 SequenceNo := SequenceNo + 10000;
//             UNTIL WhseRcptLine.NEXT() = 0;
//         END;
//     end;

//     local procedure MessageIfRcptLinesExist(ChangedFieldName: Text[80])
//     var
//         WhseRcptLine: Record "Warehouse Receipt Line";
//     begin
//         WhseRcptLine.SETRANGE("No.", "No.");
//         IF WhseRcptLine.FIND('-') THEN
//             IF NOT HideValidationDialog THEN
//                 MESSAGE(
//                   STRSUBSTNO(
//                     Text006, ChangedFieldName, TABLECAPTION) + Text007);
//     end;

//     procedure DeleteRelatedLines(UseTableTrigger: Boolean)
//     var
//         CrossDockOpp: Record 5768;
//         Confirmed: Boolean;
//     begin
//         WhseRcptLine.RESET();
//         WhseRcptLine.SETRANGE("No.", "No.");
//         IF UseTableTrigger THEN BEGIN
//             IF WhseRcptLine.FIND('-') THEN BEGIN
//                 REPEAT
//                     IF (WhseRcptLine.Quantity <> WhseRcptLine."Qty. Outstanding") AND
//                        (WhseRcptLine."Qty. Outstanding" <> 0) THEN
//                         IF NOT CONFIRM(Text008, FALSE) THEN
//                             ERROR(Text009)
//                         ELSE
//                             Confirmed := TRUE;
//                 UNTIL (WhseRcptLine.NEXT() = 0) OR Confirmed;
//                 WhseRcptLine.DELETEALL();
//             END;
//         END ELSE
//             WhseRcptLine.DELETEALL(UseTableTrigger);

//         CrossDockOpp.SETRANGE("Source Template Name", '');
//         CrossDockOpp.SETRANGE("Source Name/No.", "No.");
//         CrossDockOpp.DELETEALL();

//         WhseCommentLine.SETRANGE("Table Name", WhseCommentLine."Table Name"::"Whse. Receipt");
//         WhseCommentLine.SETRANGE(Type, WhseCommentLine.Type::" ");
//         WhseCommentLine.SETRANGE("No.", "No.");
//         WhseCommentLine.DELETEALL();
//     end;

//     procedure GetHeaderStatus(LineNo: Integer): Integer
//     var
//         WhseReceiptLine2: Record "Warehouse Receipt Line";
//         OrderStatus: Option " ","Partially Received","Completely Received";
//         First: Boolean;
//     begin
//         First := TRUE;
//         WhseReceiptLine2.SETRANGE("No.", "No.");
//         WITH WhseReceiptLine2 DO BEGIN
//             IF LineNo <> 0 THEN
//                 SETFILTER("Line No.", '<>%1', LineNo);
//             IF FIND('-') THEN
//                 REPEAT
//                     CASE OrderStatus OF
//                         OrderStatus::" ":
//                             OrderStatus := Status;
//                         OrderStatus::"Completely Received":
//                             IF Status = Status::"Partially Received" THEN
//                                 OrderStatus := OrderStatus::"Partially Received";
//                     END;
//                 UNTIL NEXT () = 0;
//         END;
//         EXIT(OrderStatus);
//     end;

//     procedure LookupWhseRcptHeader(var WhseRcptHeader: Record "Warehouse Receipt Header"): Boolean
//     begin
//         COMMIT;
//         IF USERID <> '' THEN BEGIN
//             WhseRcptHeader.FILTERGROUP := 2;
//             WhseRcptHeader.SETRANGE("Location Code");
//         END;
//         IF page.RUNMODAL(0, WhseRcptHeader) = ACTION::LookupOK THEN;
//         IF USERID <> '' THEN BEGIN
//             WhseRcptHeader.FILTERGROUP := 2;
//             WhseRcptHeader.SETRANGE("Location Code", WhseRcptHeader."Location Code");
//             WhseRcptHeader.FILTERGROUP := 0;
//         END;
//     end;

//     procedure LookupLocation(var WhseRcptHeader: Record "Warehouse Receipt Header"): Boolean
//     var
//         Location: Record Location;
//     begin
//         COMMIT;
//         Location.FILTERGROUP := 2;
//         Location.SETRANGE(Code);
//         IF page.RUNMODAL(page::7347, Location) = ACTION::LookupOK THEN
//             WhseRcptHeader.VALIDATE("Location Code", Location.Code);
//         Location.FILTERGROUP := 0;
//     end;

//     local procedure GetLocation(LocationCode: Code[10])
//     begin
//         IF LocationCode = '' THEN
//             Location.GetLocationSetup(LocationCode, Location)
//         ELSE
//             IF Location.Code <> LocationCode THEN
//                 Location.GET(LocationCode);
//     end;

//     procedure SetHideValidationDialog(NewHideValidationDialog: Boolean)
//     begin
//         HideValidationDialog := NewHideValidationDialog;
//     end;

//     procedure FindFirstAllowedRec(Which: Text[1024]): Boolean
//     var
//         WhseRcptHeader: Record "Warehouse Receipt Header";
//         WMSManagement: Codeunit "WMS Management";
//     begin
//         IF FIND(Which) THEN BEGIN
//             WhseRcptHeader := Rec;
//             WHILE TRUE DO BEGIN
//                 IF WMSManagement.LocationIsAllowedToView("Location Code") THEN
//                     EXIT(TRUE);

//                 IF NEXT(1) = 0 THEN BEGIN
//                     Rec := WhseRcptHeader;
//                     IF FIND(Which) THEN
//                         WHILE TRUE DO BEGIN
//                             IF WMSManagement.LocationIsAllowedToView("Location Code") THEN
//                                 EXIT(TRUE);

//                             IF NEXT(-1) = 0 THEN
//                                 EXIT(FALSE);
//                         END;
//                 END;
//             END;
//         END;
//         EXIT(FALSE);
//     end;

//     procedure FindNextAllowedRec(Steps: Integer): Integer
//     var
//         WhseRcptHeader: Record "Warehouse Receipt Header";
//         WMSManagement: Codeunit "WMS Management";
//         RealSteps: Integer;
//         NextSteps: Integer;
//     begin
//         RealSteps := 0;
//         IF Steps <> 0 THEN BEGIN
//             WhseRcptHeader := Rec;
//             REPEAT
//                 NextSteps := NEXT(Steps / ABS(Steps));
//                 IF WMSManagement.LocationIsAllowedToView("Location Code") THEN BEGIN
//                     RealSteps := RealSteps + NextSteps;
//                     WhseRcptHeader := Rec;
//                 END;
//             UNTIL (NextSteps = 0) OR (RealSteps = Steps);
//             Rec := WhseRcptHeader;
//             IF NOT FIND THEN;
//         END;
//         EXIT(RealSteps);
//     end;

//     procedure ErrorIfUserIsNotWhseEmployee()
//     var
//         WhseEmployee: Record "Warehouse Employee";
//     begin
//         IF USERID <> '' THEN BEGIN
//             WhseEmployee.SETRANGE("User ID", USERID);
//             IF NOT WhseEmployee.FIND('-') THEN
//                 ERROR(Text002, USERID);
//         END;
//     end;

//     procedure "--- ESKAPE --"()
//     begin
//     end;

//     procedure GetOrderCurency(): Code[10]
//     var
//         WarehouseReceiptLine: Record "Warehouse Receipt Line";
//         PurchHeader: Record "Purchase Header";
//     begin
//         // Retourne na devise de la première les ligne de commande
//         WarehouseReceiptLine.RESET();
//         WarehouseReceiptLine.SETRANGE("No.", "No.");
//         WarehouseReceiptLine.SETRANGE("Source Type", 39);
//         WarehouseReceiptLine.SETRANGE("Source Subtype", 1);
//         IF WarehouseReceiptLine.FINDFIRST () THEN BEGIN
//             PurchHeader.GET(PurchHeader."Document Type"::Order, WarehouseReceiptLine."Source No.");
//             EXIT(PurchHeader."Currency Code");


//         END;
//     end;
// }

