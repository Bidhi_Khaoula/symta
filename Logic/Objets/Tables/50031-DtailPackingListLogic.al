tableextension 50031 "Détail Packing List Logic" extends "Détail Packing List" //50031
{

    fields
    {
        modify(Length)
        {
            trigger OnAfterValidate()
            begin
                CalcCubage();
            end;
        }
        modify(Width)
        {
            trigger OnAfterValidate()
            begin
                CalcCubage();
            end;
        }
        modify(Height)
        {
            trigger OnAfterValidate()
            begin
                CalcCubage();
            end;
        }
    }

    procedure CalcCubage()
    begin
        Cubage := Length * Width * Height;
    end;
}

