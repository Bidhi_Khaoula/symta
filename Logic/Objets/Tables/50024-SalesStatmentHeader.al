tableextension 50224 "Sales Statment Header" extends "Sales Statment Header" //50024
{
    trigger OnDelete()
    var
        "Cust. Ledger Entry": Record "Cust. Ledger Entry";
    begin

        "Cust. Ledger Entry".SETCURRENTKEY("Customer No.", "Statement No.");
        "Cust. Ledger Entry".SETRANGE("Customer No.", "Customer No.");
        "Cust. Ledger Entry".SETRANGE("Statement No.", "Statement No.");
        "Cust. Ledger Entry".SETRANGE("Statement Date", "Statement Date");
        IF "Cust. Ledger Entry".FINDFIRST() THEN
            REPEAT
                "Cust. Ledger Entry"."Statement No." := '';
                "Cust. Ledger Entry"."Statement Date" := 0D;
                "Cust. Ledger Entry"."No. Printed Statement" := 0;
                "Cust. Ledger Entry".MODIFY();
            UNTIL "Cust. Ledger Entry".NEXT() = 0;
    end;
}

