tableextension 50223 "Interdiction Vente" extends "Interdiction Vente" //50023
{
    trigger OnInsert()
    begin
        //Vérifie que le champ "Code Article" n'est pas vide pour le cas ou pas "Tous" n'est sélectionné
        IF "Type Article" <> "Type Article"::Tous THEN TESTFIELD("Code Article");

        //Vérifie que le champ "Code Vente" n'est pas vide pour le cas ou pas "Tous" n'est sélectionné
        IF "Type Vente" <> "Type Vente"::Tous THEN TESTFIELD("Code Vente");

        //Vérifie qu'une date début et une date fin est bien présente
        TESTFIELD("Date Debut");
        TESTFIELD("Date Fin");
    end;

    trigger OnModify()
    begin
        //Vérifie que le champ "Code Article" n'est pas vide pour le cas ou pas "Tous" n'est sélectionné
        IF "Type Article" <> "Type Article"::Tous THEN TESTFIELD("Code Article");

        //Vérifie que le champ "Code Vente" n'est pas vide pour le cas ou pas "Tous" n'est sélectionné
        IF "Type Vente" <> "Type Vente"::Tous THEN TESTFIELD("Code Vente");

        //Vérifie qu'une date début et une date fin est bien présente
        TESTFIELD("Date Debut");
        TESTFIELD("Date Fin");
    end;
}

