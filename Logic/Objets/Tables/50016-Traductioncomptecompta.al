tableextension 50216 "Traduction compte compta" extends "Traduction compte compta" //50016
{

    fields
    {
        modify("Compte Navision")
        {
            trigger OnAfterValidate()
            var
                "G/L Account": Record "G/L Account";
            begin
                "G/L Account".GET("Compte Navision");
                Désignation := "G/L Account".Name;
            end;
        }
    }
}

