tableextension 50263 "Affectation Réception" extends "Affectation Réception" //50063
{
    procedure GetNbOfNonCheckedLine(): Integer
    var
        recL_Affectation: Record "Affectation Réception";
    begin
        recL_Affectation.RESET();
        recL_Affectation.SETRANGE(recL_Affectation.Closed, FALSE);
        EXIT(recL_Affectation.COUNT);
    end;

    procedure OpenPage()
    var
        recL_Affectation: Record "Affectation Réception";
        pgeL_Affectation: Page "Liste des affectations";
    begin
        recL_Affectation.RESET();
        recL_Affectation.SETRANGE(recL_Affectation.Closed, FALSE);

        pgeL_Affectation.SETTABLEVIEW(recL_Affectation);
        pgeL_Affectation.RUNMODAL();
    end;
}

