tableextension 50207 "Historisation Taux SAV / Porte" extends "Historisation Taux SAV / Porte" //50007
{
    trigger OnInsert()
    begin
        IF Date = 0D THEN
            Date := WORKDATE();

        IF Heure = 0T THEN
            Heure := TIME;
    end;
}

