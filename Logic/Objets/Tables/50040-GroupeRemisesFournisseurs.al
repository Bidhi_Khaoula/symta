tableextension 50240 "Groupe Remises Fournisseurs" extends "Groupe Remises Fournisseurs" //50040
{

    fields
    {
        modify("Starting Date")
        {
            trigger OnAfterValidate()
            begin
                IF ("Starting Date" > "Ending Date") AND ("Ending Date" <> 0D) THEN
                    ERROR(Text000Err, FIELDCAPTION("Starting Date"), FIELDCAPTION("Ending Date"));
            end;
        }
        modify("Ending Date")
        {
            trigger OnAfterValidate()
            begin

                VALIDATE("Starting Date");
            end;
        }
        modify("Line Discount 1 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
        modify("Line Discount 2 %")
        {
            trigger OnAfterValidate()
            begin
                // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
                VALIDATE("Line Discount %", UpdateTotalDiscount());
            end;
        }
    }

    var
        Text000Err: Label '%1 cannot be after %2', Comment = '%1 = Starting Date ; %2 = Ending Date';


    procedure UpdateTotalDiscount() DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // AD Le 08-12-2008 => Gestion d'une remise 1 et remise 2
        Disc1 := 1 - ("Line Discount 1 %" / 100);
        Disc2 := 1 - ("Line Discount 2 %" / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;
}

