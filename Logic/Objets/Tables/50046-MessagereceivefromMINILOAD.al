tableextension 50246 "Message receive from  MINILOAD" extends "Message receive from  MINILOAD" //50046
{
    procedure GetNextEntryNo() int_rtn: Integer
    var
        Message: Record "Message receive from  MINILOAD";
    begin
        //** Fonction renvoyant le prochain numéro de clé. **//
        IF Message.FINDLAST() THEN
            int_rtn := Message."Entry No." + 1
        ELSE
            int_rtn := 1;
    end;
}

