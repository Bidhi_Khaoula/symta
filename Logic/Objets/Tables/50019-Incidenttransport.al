tableextension 50219 "Incident transport" extends "Incident transport"//50019
{
    LookupPageID = "Liste Incident Transport";

    fields
    {
        modify("No.")
        {
            trigger OnAfterValidate()
            begin
                IF "No." <> xRec."No." THEN BEGIN
                    SalesSetup.GET();
                    NoSeriesMgt.TestManual(SalesSetup."Shipment Damage Nos.");
                    "No. Series" := '';
                END;
            end;
        }
        modify("Document Date")
        {
            trigger OnBeforeValidate()
            begin
                rec.TESTFIELD(Status, rec.Status::Ouvert);
            end;
        }
        modify("Order No.")
        {
            trigger OnAfterValidate()
            var
                LSalesHeader: Record "Sales Header";
            begin
                TESTFIELD(Status, Status::Ouvert);

                // AD Le 27-09-2016 => REGIE ->
                IF "Customer No." = '' THEN BEGIN
                    LSalesHeader.GET(LSalesHeader."Document Type"::Order, "Order No.");
                    VALIDATE("Contact No.", LSalesHeader."Sell-to Contact No.");
                    VALIDATE(Contact, LSalesHeader."Sell-to Contact");
                    VALIDATE("External Document No.", LSalesHeader."External Document No.");
                    VALIDATE("Shipping Agent Code", LSalesHeader."Shipping Agent Code");
                    VALIDATE("Customer No.", LSalesHeader."Sell-to Customer No.");
                END;
            end;
        }
        modify("Sales Shipment No.")
        {
            trigger OnAfterValidate()
            var
                LSalsShipHeader: Record "Sales Shipment Header";
                LCust: Record Customer;
                LLitige: Record "Incident transport";
            begin
                TESTFIELD(Status, Status::Ouvert);

                LSalsShipHeader.GET("Sales Shipment No.");

                // AD Le 05-02-2015 => Vérification que ce BL n'est pas déjà sur un litige
                LLitige.SETFILTER("No.", '<>%1', "No.");
                LLitige.SETRANGE("Sales Shipment No.", "Sales Shipment No.");
                IF NOT LLitige.ISEMPTY THEN
                    IF NOT CONFIRM(ESK001Qst, FALSE, LLitige.COUNT) THEN
                        ERROR(ESK002Err);

                // FIN AD Le 05-02-2015

                VALIDATE("Contact No.", LSalsShipHeader."Sell-to Contact No.");
                VALIDATE(Contact, LSalsShipHeader."Sell-to Contact");
                VALIDATE("External Document No.", LSalsShipHeader."External Document No.");
                VALIDATE("Shipping Agent Code", LSalsShipHeader."Shipping Agent Code");
                VALIDATE("Customer No.", LSalsShipHeader."Sell-to Customer No.");
                VALIDATE("Order No.", LSalsShipHeader."Order No.");


                LCust.GET(LSalsShipHeader."Sell-to Customer No.");
                VALIDATE("Phone No.", LCust."Phone No.");
            end;
        }
        modify("Customer No.")
        {
            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Ouvert);
            end;
        }
        modify("Contact No.")
        {
            trigger OnAfterValidate()
            var
                LContact: Record Contact;
            begin
                TESTFIELD(Status, Status::Ouvert);

                IF LContact.GET("Contact No.") THEN BEGIN
                    VALIDATE(Contact, LContact.Name);
                    VALIDATE("Phone No.", LContact."Phone No.");
                END;
            end;
        }
        modify(Contact)
        {
            Caption = 'Contact';

            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Ouvert);
            end;
        }
        modify("Phone No.")
        {
            Caption = 'Téléphone';

            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Ouvert);
            end;
        }
        modify("Shipping Agent Code")
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";

            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Ouvert);
            end;
        }
        modify(Status)
        {
            trigger OnAfterValidate()
            begin
                CASE Status OF
                    Status::Ouvert:
                        BEGIN
                            VALIDATE("Traité par", '');
                            VALIDATE("Traité le", 0D);
                            VALIDATE("Traité à", 0T);
                        END;
                    Status::Créé:
                        BEGIN
                            VALIDATE("Traité par", '');
                            VALIDATE("Traité le", 0D);
                            VALIDATE("Traité à", 0T);
                        END;

                    Status::Traité:
                        BEGIN
                            VALIDATE("Traité par", USERID);
                            VALIDATE("Traité le", WORKDATE());
                            VALIDATE("Traité à", TIME);
                        END;
                    Status::Cloturé:
                        BEGIN
                            VALIDATE("Cloturé par", USERID);
                            VALIDATE("Cloturé le", WORKDATE());
                            VALIDATE("Cloturé à", TIME);
                        END;

                END;
            end;
        }
        modify("Commentaire Incident")
        {

            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Ouvert);
            end;
        }
        modify("Commentaire Traitement")
        {

            trigger OnAfterValidate()
            begin
                TESTFIELD(Status, Status::Créé);
            end;
        }
    }

    trigger OnDelete()
    begin
        TESTFIELD(Status, Status::Ouvert);
    end;

    trigger OnInsert()
    begin
        SalesSetup.GET();

        IF "No." = '' THEN BEGIN
            SalesSetup.TESTFIELD("Order Nos.");
            NoSeriesMgt.InitSeries(SalesSetup."Shipment Damage Nos.", xRec."No. Series", WORKDATE(), "No.", "No. Series");
        END;

        InitRecord();
        // InsertMode := TRUE;
    end;

    var
        SalesSetup: Record "Sales & Receivables Setup";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        // InsertMode: Boolean;
        ESK001Qst: Label '%1 litige(s) existant sur ce B.L. ! Continuer ?', Comment = '%1 = Nombre';
        ESK002Err: Label 'Erreur pour respecter l''alerte !';

    procedure InitRecord()
    begin
        "Document Date" := WORKDATE();

        Evaluate("Create User ID", USERID);
        "Create Date" := WORKDATE();
        "Create Time" := TIME;
    end;

    procedure "Créer"()
    begin
        TESTFIELD(Status, Status::Ouvert);
        //TESTFIELD("Commentaire Incident");
        TESTFIELD("Sales Shipment No.");
        TESTFIELD("Phone No.");
        TESTFIELD("Shipping Agent Code");
        VALIDATE(Status, Status::Créé);
        MODIFY();
    end;

    procedure Traiter()
    begin
        TESTFIELD(Status, Status::Créé);
        //TESTFIELD("Commentaire Traitement");
        VALIDATE(Status, Status::Traité);
        MODIFY();
    end;

    procedure Rouvrir()
    begin
        VALIDATE(Status, Status::Ouvert);
        MODIFY();
    end;

    procedure Cloturer()
    begin
        // AD Le 27-09-2016 => REGIE
        VALIDATE(Status, Status::Cloturé);
        MODIFY();
    end;

    procedure GetDateBL(cod_BL: Code[20]) dat_Return: Date
    var
        rec_BL: Record "Sales Shipment Header";
    begin
        // DZ Le 01/06/2012 => Ajout des informations Client et Date BL
        IF rec_BL.GET(cod_BL) THEN
            EXIT(rec_BL."Posting Date");
        // Fin DZ Le 01/06/2012
    end;

    procedure GetCustNom(cod_Cust: Code[20]) str_Return: Text[100]
    var
        rec_Cust: Record Customer;
    begin
        // DZ Le 01/06/2012 => Ajout des informations Client et Date BL
        IF rec_Cust.GET(cod_Cust) THEN
            EXIT(rec_Cust.Name);
        // Fin DZ Le 01/06/2012
    end;

    procedure GetCustCP(cod_Cust: Code[20]) str_Return: Text[30]
    var
        rec_Cust: Record Customer;
    begin
        // DZ Le 01/06/2012 => Ajout des informations Client et Date BL
        IF rec_Cust.GET(cod_Cust) THEN
            EXIT(rec_Cust."Post Code");
        // Fin DZ Le 01/06/2012
    end;

    procedure GetCustVille(cod_Cust: Code[20]) str_Return: Text[30]
    var
        rec_Cust: Record Customer;
    begin
        // DZ Le 01/06/2012 => Ajout des informations Client et Date BL
        IF rec_Cust.GET(cod_Cust) THEN
            EXIT(rec_Cust.City);
        // Fin DZ Le 01/06/2012
    end;
}

