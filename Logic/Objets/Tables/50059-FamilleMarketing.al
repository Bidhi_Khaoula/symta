tableextension 50259 "Famille Marketing" extends "Famille Marketing" //50059
{
    LookupPageID = "Famille Marketing";
    trigger OnRename()
    begin
        // Super Famille = Code [Type=CONST("Super family")]
        // Famille = Code [Type=CONST(Family),Code Niveau supérieur=FIELD("Super Famille"))]
        // Sous Famille = Code [Type=CONST("Sous family"),Code Niveau supérieur=FIELD(Famille)]

        IF (xRec.Type <> xRec.Type::"Sous family") OR
           ((xRec.Type = xRec.Type::"Sous family") AND (Rec.Type <> xRec.Type)) THEN
            ERROR('Enregistrement non modifiable');

        IF (Type = Type::"Sous family") THEN BEGIN
            // Famille critère
            FamilleCritere.RESET();
            FamilleCritere.SETRANGE(Famille, xRec."Code Niveau supérieur");
            FamilleCritere.SETRANGE("Sous Famille", xRec.Code);
            IF FamilleCritere.FINDSET() THEN
                REPEAT
                    FamilleCritere2 := FamilleCritere;
                    // Super Famille,Famille,Sous Famille,Code Critere
                    FamilleCritere3.COPY(FamilleCritere2);
                    FamilleCritere3.Famille := "Code Niveau supérieur";
                    FamilleCritere3."Sous Famille" := Code;
                    FamilleCritere3.Insert();
                    //FamilleCritere2.RENAME(FamilleCritere2."Super Famille", "Code Niveau supérieur", Code, FamilleCritere2."Code Critere");
                    FamilleCritere2.DELETE();
                UNTIL FamilleCritere.NEXT() = 0;

            // Article
            Item.RESET();
            Item.SETRANGE("Famille Marketing", xRec."Code Niveau supérieur");
            Item.SETRANGE("Sous Famille Marketing", xRec.Code);
            IF Item.FINDSET() THEN
                REPEAT
                    Item2 := Item;
                    Item2."Famille Marketing" := "Code Niveau supérieur";
                    Item2."Sous Famille Marketing" := Code;
                    Item2.MODIFY(TRUE);
                UNTIL Item.NEXT() = 0;
        END;
    end;

    var
        FamilleCritere: Record "Famille Critere";
        FamilleCritere2: Record "Famille Critere";
        FamilleCritere3: Record "Famille Critere";
        Item: Record Item;
        Item2: Record Item;
}

