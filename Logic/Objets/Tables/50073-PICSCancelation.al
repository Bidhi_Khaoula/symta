tableextension 50273 "PICS Cancelation" extends "PICS Cancelation" //50073
{

    procedure Create(pWSHNo: Code[20]): Boolean;
    var
        lPICSCancelation: Record "PICS Cancelation";
        lWSH: Record "Warehouse Shipment Header";
        lSH: Record "Sales Header";
        lGeneralsParameters: Record "Generals Parameters";
        lESK001Msg: Label 'Attention :\Vous allez supprimer un PICS qui est en cours de préparation par %1.', Comment = '%1 =ID utilisateur attribué';
        lESK002Msg: Label 'Attention :\Vous allez supprimer un PICS qui est en cours de préparation par %1 (%2).', Comment = '%1 = description ; %2 = Code';
    begin
        // Test initial
        IF NOT lWSH.GET(pWSHNo) THEN
            // Retour insertion impossible le PICS (EE) n'existe pas
            EXIT(FALSE);

        IF (lWSH."Assigned User ID" = '') THEN
            // Retour insertion inutile : le PICS n'est pas en cours de préparation
            EXIT(FALSE)
        ELSE
            IF lGeneralsParameters.GET('MAG_EMPLOYES', lWSH."Assigned User ID") THEN
                MESSAGE(lESK002Msg, lGeneralsParameters.LongDescription, lGeneralsParameters.Code)
            ELSE
                MESSAGE(lESK001Msg, lWSH."Assigned User ID");


        // Initialisation
        lPICSCancelation.RESET();
        lPICSCancelation."WSH No." := lWSH."No.";

        // Création
        IF lPICSCancelation.INSERT() THEN BEGIN

            // Informations annulation
            Evaluate(lPICSCancelation."Cancel User ID", USERID());
            lPICSCancelation."Cancel Date" := TODAY();
            lPICSCancelation."Cancel Time" := TIME();
            lPICSCancelation."Cancel DateTime" := CREATEDATETIME(lPICSCancelation."Cancel Date", lPICSCancelation."Cancel Time");
            IF lPICSCancelation.MODIFY() THEN;

            // Informations PICS
            lPICSCancelation."WSH Assigned User ID" := lWSH."Assigned User ID";
            lPICSCancelation."WSH Assignment Date" := lWSH."Assignment Date";
            lPICSCancelation."WSH Assignment Time" := lWSH."Assignment Time";
            lPICSCancelation."WSH Source Document" := lWSH."Source Document";
            lPICSCancelation."WSH Source No." := lWSH."Source No.";
            IF lPICSCancelation.MODIFY() THEN;

            // Informations CDE
            IF lSH.GET(lWSH."Source Document", lWSH."Source No.") THEN BEGIN
                IF (lSH."Ship-to Name" <> '') THEN
                    lPICSCancelation."SH Ship-to Name" := lSH."Ship-to Name"
                ELSE
                    lPICSCancelation."SH Ship-to Name" := lSH."Sell-to Customer Name";
                IF (lSH."Ship-to City" <> '') THEN
                    lPICSCancelation."SH Ship-to City" := lSH."Ship-to City"
                ELSE
                    lPICSCancelation."SH Ship-to City" := lSH."Sell-to City";
                IF (lSH."Ship-to Post Code" <> '') THEN
                    lPICSCancelation."SH Ship-to Post Code" := lSH."Ship-to Post Code"
                ELSE
                    lPICSCancelation."SH Ship-to Post Code" := lSH."Sell-to Post Code";
                IF lPICSCancelation.MODIFY() THEN;
            END;

            // Retour insertion OK
            EXIT(TRUE);
        END;

        // Retour insertion KO
        EXIT(FALSE);
    end;
}

