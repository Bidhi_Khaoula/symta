tableextension 50065 "Additional Item" extends "Additional Item" //50065
{
    LookupPageID = "Additional Item List";

    FIELDS
    {
        modify("Additional Item No.")
        {
            Trigger OnAfterValidate()
            BEGIN
                IF "Additional Item No." = "Item No." THEN
                    ERROR('Vous ne pouvez associer  un article à lui même !');
            END;

        }

        modify("Additional Search Reference")
        {
            Trigger OnAfterValidate()
            VAR
                lGestionMultiReference: Codeunit "Gestion Multi-référence";
                lSearchReference: Code[30];
            BEGIN
                lSearchReference := lGestionMultiReference.RechercheMultiReference("Additional Search Reference");
                IF lSearchReference <> 'RIENTROUVE' THEN
                    VALIDATE("Additional Item No.", lSearchReference)
            END;
        }
    }
    Trigger OnInsert()
    BEGIN
        VALIDATE("Create User ID", USERID);
        VALIDATE("Create Date", WORKDATE());
        VALIDATE("Create Time", TIME);
    END;

    Trigger OnModify()
    BEGIN
        VALIDATE("Modify User ID", USERID);
        VALIDATE("Modify Date", WORKDATE());
        VALIDATE("Modify Time", TIME);
    END;
}