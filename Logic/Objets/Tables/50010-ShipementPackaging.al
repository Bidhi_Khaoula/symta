tableextension 50210 "Shipement Packaging" extends "Shipement Packaging" //50010
{

    fields
    {
        modify("Packaging Code")
        {
            trigger OnAfterValidate()
            var
                parametre: Record "Generals Parameters";
            begin
                parametre.GET('EMBALLAGE', "Packaging Code");
                weight := parametre.Decimal1;
            end;
        }
    }

}