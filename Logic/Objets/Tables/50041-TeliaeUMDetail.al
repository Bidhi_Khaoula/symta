tableextension 50241 "Teliae UM Detail" extends "Teliae UM Detail" //50041
{
    fields
    {
        modify("Length UM (cm)")
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume();
            end;
        }
        modify("Width UM (cm)")
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume();
            end;
        }
        modify("Height UM (cm)")
        {
            trigger OnAfterValidate()
            begin
                CalculateVolume();
            end;
        }
    }
    trigger OnInsert()
    begin
        // Numérotation automatique
        IF (Rec."Reference UM" = '') THEN BEGIN
            gSalesReceivablesSetup.GET();
            gSalesReceivablesSetup.TESTFIELD("Package Nos.");
            Rec."Reference UM" := gNoSeriesMgt.GetNextNo(gSalesReceivablesSetup."Package Nos.", WORKDATE(), TRUE);
        END;

        CalculateVolume();
    end;

    PROCEDURE InitRecords(pNoEEE: Code[20]; pNoBP: Code[20]; pNbUM: Integer; pWeight: Decimal);
    VAR
        lTeliaeUMDetail: Record "Teliae UM Detail";
        i: Integer;
    BEGIN
        // On recherche tous les enregistrements pour le nø BP...
        Rec.SETRANGE("No. BP", pNoBP);

        // s'il y en a trop : on supprime tout et on repart … 0
        IF (Rec.COUNT() > pNbUM) THEN
            Rec.DELETEALL();

        // on cr‚‚ les enregistrements n‚cessaires si plus de 1 (sinon on a d‚j… les info dans le BP)
        IF (pNbUM > 1) THEN
            FOR i := (Rec.COUNT() + 1) TO pNbUM DO BEGIN
                CLEAR(lTeliaeUMDetail);
                lTeliaeUMDetail."No. EEE" := pNoEEE;
                lTeliaeUMDetail."No. BP" := pNoBP;
                lTeliaeUMDetail."Weight UM (Kg)" := ROUND(pWeight / pNbUM, 0.01);
                lTeliaeUMDetail.INSERT(TRUE);
            END;
    end;

    var
        gSalesReceivablesSetup: Record "Sales & Receivables Setup";
        gNoSeriesMgt: Codeunit NoSeriesManagement;

    local procedure CalculateVolume()
    begin
        Rec."Volume UM (m3)" := (Rec."Height UM (cm)" * Rec."Width UM (cm)" * Rec."Length UM (cm)") / 1000000;
    end;
}

