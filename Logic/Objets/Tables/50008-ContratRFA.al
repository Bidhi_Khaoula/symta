tableextension 50208 "Contrat RFA" extends "Contrat RFA" //50008
{

    fields
    {
        modify(Type)
        {
            trigger OnAfterValidate()
            begin
                IF Type <> xRec.Type THEN
                    VALIDATE(Code, '');
            end;
        }
        modify("Code")
        {
            trigger OnAfterValidate()
            begin
                IF Code <> '' THEN
                    CASE Type OF
                        Type::Tous:
                            ERROR(Text001, FIELDCAPTION(Code));
                    END;
            end;
        }
    }

    var
        Text001: Label '%1 must be blank.';
}

