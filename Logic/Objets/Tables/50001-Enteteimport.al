tableextension 50201 "Entete import" extends "Entete import" //50001
{
    DrillDownPageID = "Movement Casier 2015";
    LookupPageID = "Movement Casier 2015";



    trigger OnDelete()
    var
        ligne: Record "Ligne import";
    begin
        ligne.SETRANGE(num_ecriture, num_ecriture);
        ligne.SETRANGE(journ, journ);

        IF ligne.FINDFIRST() THEN
            ligne.DELETEALL();
    end;
}

