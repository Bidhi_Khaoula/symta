tableextension 60006 "Packing List Header" extends "Packing List Header" //60006
{
    trigger OnDelete()
    var
        lPackingListLine: Record "Packing List Line";
        lPackingListPackage: Record "Packing List Package";
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        // vérifie le statut
        CheckHeaderStatusOpen();
        // Suppression des lignes
        lPackingListLine.SETRANGE("Packing List No.", Rec."No.");
        IF lPackingListLine.FINDSET() THEN
            lPackingListLine.DELETEALL(TRUE);
        // Suppression des colis
        lPackingListPackage.SETRANGE("Packing List No.", Rec."No.");
        IF lPackingListPackage.FINDSET() THEN
            lPackingListPackage.DELETEALL(TRUE);
        // mise à jour des expéditions entrepôt
        lWarehouseShipmentHeader.SETRANGE("Packing List No.", Rec."No.");
        IF lWarehouseShipmentHeader.FINDSET() THEN
            lWarehouseShipmentHeader.MODIFYALL("Packing List No.", '', FALSE);
    end;

    trigger OnInsert()
    begin
        // Numérotation automatique
        IF (Rec."No." = '') THEN BEGIN
            gWarehouseSetup.GET();
            gWarehouseSetup.TESTFIELD("Packing List Nos.");
            Rec."No." := gNoSeriesMgt.GetNextNo(gWarehouseSetup."Packing List Nos.", WORKDATE(), TRUE);
        END;
        // statut
        Rec."Header Status" := Rec."Header Status"::Ouvert;
        // Paramètres de création
        Rec."Creation User" := COPYSTR(USERID(), 1, 250);
        Rec."Creation Date" := WORKDATE();
        Rec."Creation Time" := TIME();
    end;

    trigger OnRename()
    var
        lPackingListLine: Record "Packing List Line";
        lPackingListPackage: Record "Packing List Package";
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        // vérifie le statut
        CheckHeaderStatusOpen();
        // MAJ des lignes
        lPackingListLine.SETRANGE("Packing List No.", xRec."No.");
        IF lPackingListLine.FINDSET() THEN
            REPEAT
                lPackingListLine.FINDFIRST();
                lPackingListLine.RENAME(Rec."No.", lPackingListLine."Line No.");
            UNTIL lPackingListLine.ISEMPTY();
        // MAJ des colis
        lPackingListPackage.SETRANGE("Packing List No.", xRec."No.");
        IF lPackingListPackage.FINDSET() THEN
            REPEAT
                lPackingListPackage.FINDFIRST();
                lPackingListPackage.RENAME(Rec."No.", lPackingListPackage."Package No.");
            UNTIL lPackingListPackage.ISEMPTY();
        // Mise à jour des expéditions entrepôt
        lWarehouseShipmentHeader.SETRANGE("Packing List No.", xRec."No.");
        IF lWarehouseShipmentHeader.FINDSET() THEN
            lWarehouseShipmentHeader.MODIFYALL("Packing List No.", Rec."No.", FALSE);
    end;

    var
        gWarehouseSetup: Record "Warehouse Setup";
        gNoSeriesMgt: Codeunit NoSeriesManagement;

    procedure RefreshHeaderStatus()
    var
        lPackingListLine: Record "Packing List Line";
        lNbLineOpen: Integer;
        lNbLinePosted: Integer;
    begin
        // Recherche des lignes
        lPackingListLine.SETRANGE("Packing List No.", Rec."No.");

        lPackingListLine.SETRANGE("Line Status", lPackingListLine."Line Status"::Open);
        lNbLineOpen := lPackingListLine.COUNT();

        lPackingListLine.SETRANGE("Line Status", lPackingListLine."Line Status"::Close);
        lNbLinePosted := lPackingListLine.COUNT();

        Rec."Header Status" := Rec."Header Status"::Ouvert;

        IF (lNbLinePosted > 0) AND (lNbLineOpen = 0) THEN
            Rec."Header Status" := Rec."Header Status"::Enregistré;
        IF (lNbLinePosted > 0) AND (lNbLineOpen > 0) THEN
            Rec."Header Status" := Rec."Header Status"::Lancé;
        Rec.MODIFY(TRUE);
    end;

    procedure CheckHeaderStatusOpen()
    begin
        TESTFIELD("Header Status", Rec."Header Status"::Ouvert);
    end;

    procedure GetDestinationName(): Text[100]
    var
        lCustomer: Record Customer;
    begin
        IF ("Sell-to Customer No." = '') THEN
            EXIT('');
        IF lCustomer.GET("Sell-to Customer No.") THEN
            EXIT(Format(lCustomer.Name + '-' + lCustomer.City));

        EXIT('');
    end;

    procedure GetNextColisNoIndice(var pNo: Code[20]; var pIndice: Integer): Boolean
    var
        LPackingListPack: Record "Packing List Package";
        LNoColis: Code[20];
        LIndice: Integer;
        LIndiceLetter: Char;
        LPartieEntiere: Integer;
        LIndiceLetter2: Char;
    begin
        // Recherche du dernier colis
        CLEAR(LPackingListPack);
        LPackingListPack.SETRANGE("Packing List No.", Rec."No.");
        IF LPackingListPack.ISEMPTY THEN
            LIndice := 1
        ELSE BEGIN
            // max indice + 1 => nouvel indice
            LPackingListPack.SETCURRENTKEY("Indice Colis");
            LPackingListPack.SETASCENDING("Indice Colis", FALSE);
            LPackingListPack.FINDFIRST();
            LIndice := LPackingListPack."Indice Colis" + 1;
        END;
        LIndiceLetter := LIndice + 64; // 1 => 1+64 = 65 => A
                                       //LNoColis := LWhseShipHeader."Packing List No." + '/' + FORMAT(LIndiceLetter);
                                       //LNoColis := pNoBp + '/' + FORMAT(LIndiceLetter);
                                       // 01/09/2020 - CFR (R‚gie) : 1 seul colis A pour un nø de Packing List avec gestion de A … ZZ soient 26*26=676 nø possibles
                                       //LNoColis := FORMAT(LIndiceLetter);
        LPartieEntiere := ROUND(LIndice / 26, 1, '>');
        IF LPartieEntiere <= 1 THEN BEGIN
            LIndiceLetter := LIndice + 64;
            LNoColis := FORMAT(LIndiceLetter);
        END
        ELSE BEGIN
            LIndiceLetter := LPartieEntiere + 63;
            LIndiceLetter2 := (LIndice - (LPartieEntiere - 1) * 26) + 64;
            LNoColis := FORMAT(LIndiceLetter) + FORMAT(LIndiceLetter2);
        END;

        pNo := LNoColis;
        pIndice := LIndice;
        EXIT(TRUE);
    end;
}

