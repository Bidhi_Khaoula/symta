tableextension 50275 "Shipping Agent Comment Line" extends "Shipping Agent Comment Line"//50075

{
    fields
    {
        modify(Department)
        {
            trigger OnAfterValidate();
            var
                lInteger: Integer;
            begin
                //Formatage sur 2 digits avec 0 significatif
                IF (STRLEN(Department) = 1) THEN
                    Department := '0' + Department;
                //Formatage sur 2 digits avec 0 significatif
                IF NOT EVALUATE(lInteger, Department) THEN
                    ERROR('%1 n''est pas un n° de département valide.', Department);
            end;
        }
    }
}

