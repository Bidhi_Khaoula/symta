tableextension 50264 "Item Hierarchies" extends "Item Hierarchies" //50064
{

    fields
    {
        modify("Super Famille Marketing")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
            begin

                // AD Le 31-05-2019 => FE20190425
                IF "By Default" THEN
                    IF "Super Famille Marketing" <> xRec."Super Famille Marketing" THEN BEGIN
                        LItem.GET("Code Article");
                        LItem."Date Modif. Super Fam. Mark" := WORKDATE();
                        LItem.MODIFY(FALSE);
                    END;
            end;
        }
        modify("Famille Marketing")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
            begin
                // AD Le 31-05-2019 => FE20190425
                IF "By Default" THEN
                    IF "Famille Marketing" <> xRec."Famille Marketing" THEN BEGIN
                        LItem.GET("Code Article");
                        LItem."Date Modif. Fam. Mark" := WORKDATE();
                        LItem.MODIFY(FALSE);
                    END;
            end;
        }
        modify("By Default")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
            begin
                CheckOnceByDefault();


                // AD Le 31-05-2019 => FE20190425
                IF xRec."By Default" <> "By Default" THEN BEGIN
                    LItem.GET("Code Article");
                    LItem."Date Modif. Super Fam. Mark" := WORKDATE();
                    LItem."Date Modif. Fam. Mark" := WORKDATE();
                    LItem.MODIFY(FALSE);
                END;
            end;
        }
    }

    trigger OnDelete()
    begin
        ErrorIfItemCriteria();
    end;

    trigger OnInsert()
    var
        Rec_ListeCritereArticle: Record "Article Critere";
        rec_CritereFamille: Record "Famille Critere";
        LVariante: Record "Item Variant";
    begin
        // Création en automatique des critères de la hierarchie
        rec_CritereFamille.RESET();
        rec_CritereFamille.SETRANGE("Super Famille", "Super Famille Marketing");
        rec_CritereFamille.SETRANGE(Famille, "Famille Marketing");
        rec_CritereFamille.SETRANGE("Sous Famille", "Sous Famille Marketing");
        IF rec_CritereFamille.FINDSET() THEN
            REPEAT
                Rec_ListeCritereArticle.INIT();
                Rec_ListeCritereArticle.VALIDATE("Code Article", "Code Article");
                Rec_ListeCritereArticle.VALIDATE("Code Critere", rec_CritereFamille."Code Critere");
                Rec_ListeCritereArticle.VALIDATE(Priorité, rec_CritereFamille.Priorité);
                Rec_ListeCritereArticle.VALIDATE("Variant Code", '');
                Rec_ListeCritereArticle."Super Famille Marketing" := "Super Famille Marketing";
                Rec_ListeCritereArticle."Famille Marketing" := "Famille Marketing";
                Rec_ListeCritereArticle."Sous Famille Marketing" := "Sous Famille Marketing";
                IF Rec_ListeCritereArticle.INSERT() THEN;

                // AD Le 01-03-2013 => On ajoute aussi pour les variantes
                LVariante.RESET();
                LVariante.SETRANGE("Item No.", "Code Article");
                IF LVariante.FindSet() THEN
                    REPEAT
                        Rec_ListeCritereArticle.VALIDATE("Variant Code", LVariante.Code);
                        IF Rec_ListeCritereArticle.INSERT() THEN;
                    UNTIL LVariante.NEXT() = 0;
            // FIN AD Le 01-03-2013
            UNTIL rec_CritereFamille.NEXT() = 0;
    end;

    trigger OnRename()
    begin
        IF NOT DoNotCheckRename THEN
            ERROR('Vous devez supprimer la ligne et en créer une nouvelle');
    end;

    var
        Text000Err: Label 'Il existe déjà une hierarchie principale pour l''article !';
        DoNotCheckRename: Boolean;

    local procedure CheckOnceByDefault()
    var
        recL_WebHierarchieItem: Record "Item Hierarchies";
    begin
        recL_WebHierarchieItem.SETRANGE("Code Article", "Code Article");
        recL_WebHierarchieItem.SETRANGE("By Default", TRUE);
        IF NOT recL_WebHierarchieItem.ISEMPTY THEN
            ERROR(Text000Err);
    end;

    local procedure ErrorIfItemCriteria()
    var
        recL_ItemCriteria: Record "Article Critere";
        Error001Err: Label 'Des critères articles existent pour cette hierarchie, vous devez au préalable les supprimer.';
    begin
        recL_ItemCriteria.RESET();
        recL_ItemCriteria.SETRANGE("Code Article", "Code Article");
        recL_ItemCriteria.SETRANGE("Super Famille Marketing", "Super Famille Marketing");
        recL_ItemCriteria.SETRANGE("Famille Marketing", "Famille Marketing");
        recL_ItemCriteria.SETRANGE("Sous Famille Marketing", "Sous Famille Marketing");
        IF NOT recL_ItemCriteria.ISEMPTY THEN
            ERROR(Error001Err);
    end;

    procedure SetDoNotCheckRename(pDoNotCheckRename: Boolean)
    begin
        DoNotCheckRename := pDoNotCheckRename;
    end;
}

