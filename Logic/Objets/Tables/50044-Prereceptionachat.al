tableextension 50244 "Pre-reception achat" extends "Pre-reception achat"//50044
{

    fields
    {
        modify("Réference fournisseur")
        {
            trigger OnAfterValidate()
            var
                "Item Cross Reference": Record "Item Reference";
            begin
                "Ref. Active" := "Réference fournisseur";
                // Recherche dans les multireferences fournisseur
                // -------------------------------------
                "Item Cross Reference".RESET();
                "Item Cross Reference".SETRANGE("Reference Type", "Item Cross Reference"."Reference Type"::Vendor);
                "Item Cross Reference".SETRANGE("Reference Type No.", "N° fournisseur");
                "Item Cross Reference".SETRANGE("Reference No.", "Réference fournisseur");
                IF "Item Cross Reference".FINDFIRST() THEN BEGIN
                    item.GET("Item Cross Reference"."Item No.");
                    "Ref. Active" := item."No. 2";
                    "Code Article" := "Item Cross Reference"."Item No.";
                END;

                "Code Article" := gestionRef.RechercheMultiReference("Ref. Active");

                IF item.GET("Code Article") THEN
                    Designation := item.Description;
            end;
        }
        modify("Code Article")
        {
            trigger OnAfterValidate();
            begin
                GetCost();
            end;
        }
        modify(Quantite)
        {
            trigger OnAfterValidate();
            begin
                GetCost();
                UpdateAmount();
            end;
        }
        modify("Cout Net")
        {
            trigger OnAfterValidate();
            begin
                "Cout brut" := "Cout Net";
                Remise := 0;
                "Remise 1" := 0;
                "Remise 2" := 0;

                UpdateAmount();
            end;
        }

        modify("Recherche Référence")
        {
            trigger OnAfterValidate();
            begin
                RechercheRef("Recherche Référence")
            end;
        }
        modify("Type de commande")
        {
            trigger OnAfterValidate();
            begin
                IF "Code Article" <> '' THEN BEGIN

                    GetCost();
                    UpdateAmount();
                END;
            end;
        }
    }

    var
        item: Record Item;
        TempExcelBuf: Record "Excel Buffer" temporary;
        gestionRef: Codeunit "Gestion Multi-référence";
        Excel002Lbl: Label 'Data';
        Excel003Lbl: Label 'Customer - Order Detail';

    procedure UpdateAmount();
    begin
        "Montant Ligne" := Quantite * "Cout Net";
    end;

    procedure RechercheRef(p_recherche: Code[20]);
    var
        LItem: Record Item;
        GestionMultiReference: Codeunit "Gestion Multi-référence";
        NoRef: Code[30];
        txt_RefSaisie: Code[40];
    begin
        // MC Le 06-06-2011 => SYMTA -> Stocke la référence saisie.
        txt_RefSaisie := p_recherche;
        // FIN MC Le 06-06-2011

        // AD Le 11-12-2009 => GDI -> Recherche de la référence
        NoRef := GestionMultiReference.RechercheMultiReference(p_recherche);
        IF NoRef <> 'RIENTROUVE' THEN BEGIN
            VALIDATE("Code Article", NoRef);
            LItem.GET("Code Article");
            "Ref. Active" := LItem."No. 2";
            Designation := LItem.Description;
        END
        ELSE BEGIN
            LItem.RESET();
            LItem.SETCURRENTKEY("No. 2");
            LItem.SETFILTER("No. 2", p_recherche + '*');
            IF PAGE.RUNMODAL(PAGE::"Item List", LItem) = ACTION::LookupOK THEN BEGIN
                "Ref. Active" := LItem."No. 2";
                VALIDATE("Code Article", LItem."No.");
                Designation := LItem.Description;
            END
            ELSE BEGIN
                Designation := '';
                "Ref. Active" := '';
                "Code Article" := '';
            END;
        END;
        // AD Le 11-12-2009
    end;

    procedure GetCost();
    var
        TempToPurchDisc: Record "Purchase Line Discount" temporary;
        TempToPurchPrice: Record "Purchase Price" temporary;
        rec_Item: Record Item;
        ItemUOM: Record "Item Unit of Measure";
        rec_ItemByVendor: Record "Item Vendor";
        "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
        dec_BrutPurchPrice: Decimal;
        dec_PurchDisc: Decimal;
        dec_NetPurchPrice: Decimal;
    begin
        //Insertion des enregistrements de la table

        //Initialisation des variables
        // Initialisation des variables;
        dec_BrutPurchPrice := 0;
        dec_PurchDisc := 0;
        dec_NetPurchPrice := 0;


        //On récupère la liste des articles vendeurs.
        rec_Item.GET("Code Article");

        rec_ItemByVendor.RESET();
        rec_ItemByVendor.SETRANGE("Item No.", "Code Article");
        rec_ItemByVendor.SETRANGE("Vendor No.", "N° fournisseur");

        // Parcours des enregistrements.
        IF rec_ItemByVendor.FINDFIRST() THEN;

        // Récupère le prix d'achat et la remise pour le fournisseur.
        //On récupère le prix d'achat
        "Purch Price Calc. Mgt.".FindPurchPrice(TempToPurchPrice, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                , '', WORKDATE(), FALSE);

        // Rajout du filtre sur la quantité minimum
        TempToPurchPrice.SETRANGE("Minimum Quantity", 0, Quantite);


        IF TempToPurchPrice.FINDLAST() THEN BEGIN
            // Récupère les informations du prix d'achat.
            dec_BrutPurchPrice := TempToPurchPrice."Direct Unit Cost";

            IF ItemUOM.GET(rec_ItemByVendor."Item No.", rec_ItemByVendor."Item No.") THEN
                dec_BrutPurchPrice := dec_BrutPurchPrice / ItemUOM."Qty. per Unit of Measure";

        END;

        // On récupère la remise d'achat
        "Purch Price Calc. Mgt.".FindPurchLineDisc(TempToPurchDisc, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                   , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                   , '', WORKDATE(), FALSE, "Type de commande", Quantite);

        // Rajout du filtre sur la quantité minimum
        TempToPurchDisc.SETRANGE("Minimum Quantity", 0, Quantite);

        IF TempToPurchDisc.FINDLAST() THEN
            dec_PurchDisc := TempToPurchDisc."Line Discount %";

        dec_NetPurchPrice := ROUND(
                                           (dec_BrutPurchPrice * (1 - dec_PurchDisc / 100))
                                           /// UOMMgt.GetQtyPerUnitOfMeasure(Item, Item."Purch. Unit of Measure")
                                           //* "Qty. per Unit of Measure"
                                           , 0.01);
        IF dec_NetPurchPrice = 0 THEN
            dec_NetPurchPrice := dec_BrutPurchPrice;



        // Mise à jour du record*
        VALIDATE("Cout Net", dec_NetPurchPrice);
        Remise := dec_PurchDisc;
        "Remise 1" := TempToPurchDisc."Line Discount 1 %";
        "Remise 2" := TempToPurchDisc."Line Discount 2 %";
        "Cout brut" := dec_BrutPurchPrice;
    end;

    procedure MakeExcelDataHeader();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn('No Facture (O)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date Facture (F)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('No Commande (F)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('No Ligne Commande (F)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Code SYMTA (O)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Code Article Fournisseur (F)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Désignation (F)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Quantité (O)', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pu Brut', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Remise 1', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Remise 2', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pu Net', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Total Net', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        TempExcelBuf.NewRow();


        TempExcelBuf.AddColumn("N° document", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(WORKDATE(), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Ref. Active", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Réference fournisseur", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Designation, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(Quantite), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Cout brut"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Remise 1"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Remise 2"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Cout Net"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Montant Ligne"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBook('', Excel002Lbl);
        TempExcelBuf.WriteSheet(Excel003lBL, COMPANYNAME, USERID);
        // TempExcelBuf.GiveUserControl;
        ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[30];
    begin
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    end;
}

