tableextension 50213 "Shipping Label" extends "Shipping Label"//50013
{
    fields
    {
        modify("Shipping Agent Services")
        {
            trigger OnAfterValidate()
            begin
                IF "prestation transport".GET("Shipping Agent Code", "Shipping Agent Services") THEN BEGIN
                    "Product Code" := "prestation transport"."Product Code";
                    Account := "prestation transport".Account;
                END;
            end;
        }
        modify("Date of  bl")
        {
            trigger OnAfterValidate()
            begin
                "Day of BL" := DATE2DMY("Date of  bl", 1);
                "Month Of BL" := DATE2DMY("Date of  bl", 2);
                "Year Of BL" := DATE2DMY("Date of  bl", 3);
            end;
        }
        modify("Imperative date of Delivery")
        {
            trigger OnAfterValidate()
            begin
                "Imperative day of Delivery" := DATE2DMY("Imperative date of Delivery", 1);
                "Imperative month of Delivery" := DATE2DMY("Imperative date of Delivery", 2);
                "Imperative Year of Delivery" := DATE2DMY("Imperative date of Delivery", 3);
            end;
        }

    }

    trigger OnInsert()
    begin
        IF "Entry No." = '' THEN BEGIN
            GLsetup.GET();
            "Entry No." := NoSeriesMgt.GetNextNo(GLsetup."Shipment label Entry Nos.", WORKDATE(), TRUE);
        END;
    end;

    trigger OnModify()
    begin
        IF "Transmission status" = 'TR' THEN
            ERROR(Text50000Err);

        IF xRec."Recipient Name" <> "Recipient Name" THEN
            IF NOT CONFIRM(Text50001Qst) THEN
                ERROR(Text50002Err);
    end;

    var
        "prestation transport": Record "Shipping Agent Services";
        GLsetup: Record "Sales & Receivables Setup";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        Text50000Err: Label 'IMPOSSIBLE ! Génération effectué.';
        Text50001Qst: Label 'Changement de l''adresse ?';
        Text50002Err: Label 'Erreur générée pour repecter l''alerte !';
}

