tableextension 50260 "Equipment" extends "Equipment" //50060
{
    DrillDownPageID = "Equipment List";
    LookupPageID = "Equipment List";

    fields
    {
        modify(Manufacturer)
        {
            trigger OnAfterValidate()
            begin
                CheckUniqueInformations()
            end;
        }
        modify(Model)
        {
            trigger OnAfterValidate()
            begin
                CheckUniqueInformations()
            end;
        }
        modify("Equipment Type")
        {
            trigger OnAfterValidate()
            begin
                CheckUniqueInformations()
            end;
        }
        modify(Options)
        {
            Trigger OnAfterValidate()
            BEGIN
                //CFR le 09/12/2021 - FA20210912 : ajout champ 13
                CheckUniqueInformations()
            END;
        }
    }

    trigger OnDelete()
    var
        recL_EquipmentItem: Record "Modele / Article";
    begin
        recL_EquipmentItem.RESET();
        recL_EquipmentItem.SETRANGE(recL_EquipmentItem."Equipment No.", "Equipment No.");
        IF NOT recL_EquipmentItem.ISEMPTY THEN
            IF NOT CONFIRM(Text001Msg) THEN ERROR(Text002Err);
        recL_EquipmentItem.DELETEALL();
    end;

    var
        Text000Err: Label 'La marque, modèle et type existe déjà pour l''équipement %1.', Comment = '%1 Equipment No.';
        Text001Msg: Label 'Attention cela va supprimer le paramètrage articles équipements pour cet équipement.';
        Text002Err: Label 'Annulé';

    local procedure CheckUniqueInformations()
    var
        recL_Equipments: Record Equipment;
    begin
        recL_Equipments.SETRANGE(Manufacturer, Manufacturer);
        recL_Equipments.SETRANGE(Model, Model);
        recL_Equipments.SETRANGE("Equipment Type", "Equipment Type");
        //CFR le 09/12/2021 - FA20210912 : ajout champ 13
        recL_Equipments.SETRANGE(Options, Options);
        //FIN CFR le 09/12/2021
        IF recL_Equipments.FINDFIRST() THEN
            ERROR(Text000Err, recL_Equipments."Equipment No.");
    end;
}

