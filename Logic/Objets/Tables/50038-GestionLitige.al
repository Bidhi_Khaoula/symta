tableextension 50238 "Gestion Litige" extends "Gestion Litige" //50038
{
    fields
    {
        modify("Item No.")
        {
            trigger OnAfterValidate()
            var
                LItem: Record Item;
            begin
                LItem.GET("Item No.");
                "Ref. Active" := LItem."No. 2";
            end;
        }
        modify("Quantité Théorique")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Receipt Quantity")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Prix Brut Théorique")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Remise 1 Théorique")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Remise 2 Théorique")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Prix Brut Réceptionné")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Remise 1 Réceptionnée")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
        modify("Remise 2 Réceptionnée")
        {
            trigger OnAfterValidate()
            begin
                CalcTotalNet();
            end;
        }
    }

    trigger OnInsert()
    begin
        IF GETFILTER("Vendor No.") <> '' THEN
            IF GETRANGEMIN("Vendor No.") = GETRANGEMAX("Vendor No.") THEN
                VALIDATE("Vendor No.", GETRANGEMIN("Vendor No."));
    end;

    procedure CalcTotalNet()
    begin

        // AD Le 31-01-2020 => REGIE -> Gestion du prox net
        /* ANCIEN CODE
        
        "Total Net Théorique" := ("Quantité Théorique" *
                                    ("Prix Brut Théorique" * (1 - "Remise 1 Théorique" / 100)) *
                                                             (1 - "Remise 2 Théorique" / 100));
        
        
        "Total Net  Réceptionné" := ("Receipt Quantity" *
                                    ("Prix Brut Réceptionné" * (1 - "Remise 1 Réceptionnée" / 100)) *
                                                             (1 - "Remise 2 Réceptionnée" / 100));
        */
        "Prix Net Théorique" := ("Prix Brut Théorique" * (1 - "Remise 1 Théorique" / 100)) *
                                                             (1 - "Remise 2 Théorique" / 100);
        "Total Net Théorique" := "Quantité Théorique" * "Prix Net Théorique";


        "Prix Net Réceptionné" := ("Prix Brut Réceptionné" * (1 - "Remise 1 Réceptionnée" / 100)) *
                                                             (1 - "Remise 2 Réceptionnée" / 100);
        "Total Net  Réceptionné" := "Receipt Quantity" * "Prix Net Réceptionné";


        Ecart := "Total Net  Réceptionné" - "Total Net Théorique";

    end;
}

