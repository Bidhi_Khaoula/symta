tableextension 50033 "Import facture tempo. fourn." extends "Import facture tempo. fourn." //50033
{
    LookupPageID = "Liste prepa-facture achat";

    fields
    {
        modify("Nouveau Pu Brut")
        {
            trigger OnAfterValidate()
            var
                TotalDisc: Decimal;
            begin
                // Mise à jour du cout net quand modification du cout brut ou d'une des remises
                TotalDisc := UpdateTotalDiscount("Nouvelle Remise 1", "Nouvelle Remise 2");
                IF TotalDisc <> 0 THEN
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut" - ("Nouveau Pu Brut" * TotalDisc) / 100, 0.01)
                ELSE
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut", 0.01);
            end;
        }
        modify("Nouvelle Remise 1")
        {

            trigger OnAfterValidate()
            var
                TotalDisc: Decimal;
            begin
                // Mise à jour du cout net quand modification du cout brut ou d'une des remises
                TotalDisc := UpdateTotalDiscount("Nouvelle Remise 1", "Nouvelle Remise 2");
                IF TotalDisc <> 0 THEN
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut" - ("Nouveau Pu Brut" * TotalDisc) / 100, 0.01)
                ELSE
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut", 0.01);
            end;
        }
        modify("Nouvelle Remise 2")
        {

            trigger OnAfterValidate()
            var
                TotalDisc: Decimal;
            begin
                // Mise à jour du cout net quand modification du cout brut ou d'une des remises
                TotalDisc := UpdateTotalDiscount("Nouvelle Remise 1", "Nouvelle Remise 2");
                IF TotalDisc <> 0 THEN
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut" - ("Nouveau Pu Brut" * TotalDisc) / 100, 0.01)
                ELSE
                    "Nouveau cout net" := ROUND("Nouveau Pu Brut", 0.01);
            end;
        }
    }

    trigger OnDelete()
    var
        rec_ImpFac: Record "Import facture tempo. fourn.";
        rec_ImpFacEntete: Record "Import facture tempo. fourn.";
    begin
        // MC LE 06-06-2011 => SYMTA -> Si dernière ligne d'une facture alors suppression de la ligne d'en-tête.
        rec_ImpFac.RESET();
        rec_ImpFac.SETRANGE("No Facture", "No Facture");
        rec_ImpFac.SETRANGE("Type ligne", rec_ImpFac."Type ligne"::Ligne);
        rec_ImpFac.SETFILTER("No séquence", '<>%1', "No séquence");
        IF rec_ImpFac.ISEMPTY THEN BEGIN
            rec_ImpFacEntete.SETRANGE("No Facture", "No Facture");
            rec_ImpFacEntete.SETRANGE("Type ligne", rec_ImpFac."Type ligne"::Entete);
            IF rec_ImpFacEntete.FINDSET() THEN
                rec_ImpFacEntete.DELETEALL();
        END;
    end;

    var
        RecGFac: Record "Import facture tempo. fourn.";

    procedure ChargeValeurCommande()
    var
        LigneCommande: Record "Purchase Line";
    begin
        //** Cette fonction permet de charger par défault les valeurs de la commande trouvée **//
        IF ("No commande affecté" = '') OR ("No ligne de commande" = 0) THEN
            EXIT;

        LigneCommande.GET(LigneCommande."Document Type"::Order, "No commande affecté", "No ligne de commande");

        "Code article commande" := LigneCommande."No.";
        "Quantité restante commande" := LigneCommande."Outstanding Quantity";
        // MC le 06-06-2011 => On ramène le prix net de la ligne de commande.
        //"Pu net commande":=LigneCommande."Direct Unit Cost";
        "Pu net commande" := LigneCommande."Net Unit Cost";
        // FIN MC Le 06-06-2011.
        "Pu brut commande" := LigneCommande."Direct Unit Cost";
        "Remise 1 commande" := LigneCommande."Discount1 %";
        "Remise 2 commande" := LigneCommande."Discount2 %";
        "Nouvelle quantité" := "Quantité restante commande";
        VALIDATE("Nouvelle Remise 1", LigneCommande."Discount1 %");
        VALIDATE("Nouvelle Remise 2", LigneCommande."Discount2 %");
        VALIDATE("Nouveau Pu Brut", LigneCommande."Direct Unit Cost");
        MODIFY();
    end;

    procedure "Nom Fournisseur"(): Text[100]
    var
        vendor: Record Vendor;
    begin
        IF vendor.GET("Code fournisseur") THEN
            EXIT(vendor.Name)
        ELSE
            EXIT('');
    end;

    procedure GetNextEntryNo(): Integer
    begin
        //** Cette fonction permet de récupérer le prochain numéro de séquence **//
        RecGFac.RESET();
        IF RecGFac.FINDLAST() THEN
            EXIT(RecGFac."No séquence" + 1)
        ELSE
            EXIT(1);
    end;

    procedure UpdateTotalDiscount(Discount1: Decimal; Discount2: Decimal) DiscTotal: Decimal
    var
        Disc1: Decimal;
        Disc2: Decimal;
    begin
        // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
        // Renvoit la remise en cascade à partir de deux remises en paramètres.
        Disc1 := 1 - (Discount1 / 100);
        Disc2 := 1 - (Discount2 / 100);
        DiscTotal := 100 - (Disc1 * Disc2 * 100);
    end;
}

