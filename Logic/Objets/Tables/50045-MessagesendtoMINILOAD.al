tableextension 50245 "Message send to MINILOAD" extends "Message send to MINILOAD" //50045
{

    procedure GetNextEntryNo() int_rtn: Integer
    var
        Message: Record "Message send to MINILOAD";
    begin
        //** Fonction renvoyant le prochain numéro de clé. **//
        IF Message.FINDLAST() THEN
            int_rtn := Message."Entry No." + 1
        ELSE
            int_rtn := 1;
    end;
}

