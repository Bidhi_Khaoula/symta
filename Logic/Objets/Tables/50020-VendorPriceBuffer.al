tableextension 50220 "Vendor Price Buffer" extends "Vendor Price Buffer"//50020
{
    fields
    {
        modify("Item No.")
        {
            trigger OnAfterValidate()
            var
                rec_Item: Record Item;
            begin
                // Récupèration des désignations.
                IF "Item No." <> '' THEN BEGIN
                    GetItem(rec_Item);
                    Description := rec_Item.Description;
                    "Description 2" := rec_Item."Description 2";
                END;
            end;
        }
        modify("Vendor No.")
        {
            trigger OnAfterValidate()
            var
                rec_Vendor: Record Vendor;
            begin
                // Récupèration le nom du fournisseur.
                IF rec_Vendor.GET("Vendor No.") THEN
                    Name := rec_Vendor.Name;
            end;
        }

    }

    var
        gBufferPrice: Record "Vendor Price Buffer" temporary;
        PriceSelected: Boolean;

    procedure GetItem(var pRecItem: Record Item)
    begin
        pRecItem.GET("Item No.");
    end;

    procedure SetRecords(par_RequisitionLine: Record "Requisition Line"; var rec_Buffer: Record "Vendor Price Buffer" temporary)
    var
        rec_Item: Record Item;
        TempToPurchDisc: Record "Purchase Line Discount" temporary;
        TempToPurchPrice: Record "Purchase Price" temporary;
        rec_ItemByVendor: Record "Item Vendor";
        ItemUOM: Record "Item Unit of Measure";
        LCurrExchRate: Record "Currency Exchange Rate";
        LVendor: Record Vendor;
        "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
        dec_BrutPurchPrice: Decimal;
        dec_PurchDisc: Decimal;
        dec_NetPurchPrice: Decimal;
        MeilleurPrixVenteNet: Decimal;
        BestPrice: Decimal;
        lTarifFournisseurAExclure: Boolean;
    begin
        //Insertion des enregistrements de la table

        //Initialisation des variables

        // Calcul du meilleur Prix Net
        MeilleurPrixVenteNet := GetMeilleurPrixNet(par_RequisitionLine."No.");


        //On récupère la liste des articles vendeurs.
        rec_Item.GET(par_RequisitionLine."No.");

        rec_ItemByVendor.RESET();
        rec_ItemByVendor.SETRANGE("Item No.", par_RequisitionLine."No.");

        // AD Le 31-05-2011
        rec_ItemByVendor.SETFILTER("Statut Qualité", '%1|%2', rec_ItemByVendor."Statut Qualité"::Conforme,
            rec_ItemByVendor."Statut Qualité"::"Contrôle en cours");


        rec_ItemByVendor.SETRANGE("Statut Approvisionnement", rec_ItemByVendor."Statut Approvisionnement"::"Non Bloqué");
        // FIN AD Le 31-05-2011


        BestPrice := 99999999;


        // Parcours des enregistrements.
        IF rec_ItemByVendor.FINDSET() THEN
            REPEAT
                //CFR le 14/09/2021 => Régie
                CLEAR(TempToPurchDisc);
                CLEAR(TempToPurchPrice);
                //FIN CFR le 14/09/2021 => Régie
                // Initialisation des variables;
                dec_BrutPurchPrice := 0;
                dec_PurchDisc := 0;
                dec_NetPurchPrice := 0;


                LVendor.GET(rec_ItemByVendor."Vendor No.");

                // Récupère le prix d'achat et la remise pour le fournisseur.
                //On récupère le prix d'achat
                "Purch Price Calc. Mgt.".FindPurchPrice(TempToPurchPrice, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                        , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                        // AD Le 10-04-2012 => Gestion Devise
                                                        //,'', WORKDATE,FALSE);// CODE D'ORIGINE
                                                        , LVendor."Currency Code", WORKDATE(), FALSE);
                // FIN AD Le 10-04-2012

                // Rajout du filtre sur la quantité minimum
                // TempToPurchPrice.SETRANGE("Minimum Quantity", 0, par_RequisitionLine.Quantity); => AD Le 12-01-2012 => On veut tous les prix
                //CFR le 18/10/2022 => R‚gie : ajout [FiltreFournisseurAExclure]
                lTarifFournisseurAExclure := FALSE;
                IF STRPOS(par_RequisitionLine.FiltreFournisseurAExclure, LVendor."No.") > 0 THEN
                    lTarifFournisseurAExclure := TRUE;
                //IF TempToPurchPrice.FINDFIRST () THEN
                IF TempToPurchPrice.FindSet() AND (NOT lTarifFournisseurAExclure) THEN
                    //FIN CFR le 18/10/2022
                        REPEAT
                            // Récupère les informations du prix d'achat.
                            dec_BrutPurchPrice := TempToPurchPrice."Direct Unit Cost";

                            // AD Le 10-04-2012 => Gestion Devise
                            IF TempToPurchPrice."Currency Code" <> '' THEN
                                dec_BrutPurchPrice := ROUND(
                                            LCurrExchRate.ExchangeAmtFCYToLCY(WORKDATE(), TempToPurchPrice."Currency Code",
                                            TempToPurchPrice."Direct Unit Cost",
                                            LCurrExchRate.ExchangeRate(WORKDATE(), TempToPurchPrice."Currency Code")));



                            // FIN AD Le 10-04-2012


                            IF ItemUOM.GET(rec_ItemByVendor."Item No.", rec_ItemByVendor."Purch. Unit of Measure") THEN
                                dec_BrutPurchPrice := dec_BrutPurchPrice / ItemUOM."Qty. per Unit of Measure";



                            // On récupère la remise d'achat
                            "Purch Price Calc. Mgt.".FindPurchLineDisc(TempToPurchDisc, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                                       , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                                       , '', WORKDATE(), FALSE, par_RequisitionLine."Type de commande", Quantity);

                            //CFR le 14/09/2021 => Régie : La remise achat est fonction de la quantité minimum de commande
                            /* ANCIEN CODE
                            // Rajout du filtre sur la quantité minimum
                            // TempToPurchDisc.SETRANGE("Minimum Quantity", 0, par_RequisitionLine.Quantity);  => AD Le 12-01-2012 => On veut tous les prix

                            IF TempToPurchDisc.FINDLAST THEN
                              dec_PurchDisc := TempToPurchDisc."Line Discount %";
                            */
                            TempToPurchDisc.SETRANGE("Minimum Quantity", 0, TempToPurchPrice."Minimum Quantity");
                            IF TempToPurchDisc.FINDLAST() THEN
                                dec_PurchDisc := TempToPurchDisc."Line Discount %";
                            //FIN CFR le 14/09/2021

                            //IF TempToPurchPrice."Ne pas Appliquer les remises" THEN
                            //  dec_PurchDisc := 0;

                            dec_NetPurchPrice := ROUND(
                                                               (dec_BrutPurchPrice * (1 - dec_PurchDisc / 100))
                                                               /// UOMMgt.GetQtyPerUnitOfMeasure(Item, Item."Purch. Unit of Measure")
                                                               //* "Qty. per Unit of Measure"
                                                               , 0.01);
                            IF dec_NetPurchPrice = 0 THEN
                                dec_NetPurchPrice := dec_BrutPurchPrice;

                            IF dec_NetPurchPrice <> 0 THEN
                                WITH rec_Buffer DO BEGIN
                                    // Insertion de l'enregistrement dans la table.
                                    INIT();
                                    VALIDATE("Worksheet Template Name", par_RequisitionLine."Worksheet Template Name");
                                    VALIDATE("Journal Batch Name", par_RequisitionLine."Journal Batch Name");
                                    VALIDATE("Line No.", par_RequisitionLine."Line No.");
                                    VALIDATE("Item No.", par_RequisitionLine."No.");
                                    VALIDATE("Vendor No.", rec_ItemByVendor."Vendor No.");


                                    VALIDATE("Unit Price", dec_BrutPurchPrice);
                                    VALIDATE(Discount, dec_PurchDisc);
                                    VALIDATE("Net Unit Price", dec_NetPurchPrice);
                                    VALIDATE("Minimum order", rec_ItemByVendor."Minimum Order Quantity");
                                    VALIDATE("By How Many", rec_ItemByVendor."Purch. Multiple");
                                    VALIDATE("Profit %", CalcMarge(dec_NetPurchPrice, MeilleurPrixVenteNet, rec_ItemByVendor."Indirect Cost"));
                                    VALIDATE("Statut Qualité", rec_ItemByVendor."Statut Qualité");
                                    VALIDATE("Statut Approvisionnement", rec_ItemByVendor."Statut Approvisionnement");
                                    VALIDATE("Référence Fournisseur", rec_ItemByVendor."Vendor Item No.");
                                    VALIDATE("Vendor Item Desciption", rec_ItemByVendor."Vendor Item Desciption"); // AD Le 30-01-2020 => REGIE
                                    VALIDATE("Code Remise", rec_ItemByVendor."Code Remise");
                                    VALIDATE("Meilleur Prix Vente", MeilleurPrixVenteNet);
                                    VALIDATE("Unité d'achat", rec_ItemByVendor."Purch. Unit of Measure");
                                    VALIDATE("Date début", TempToPurchPrice."Starting Date");
                                    VALIDATE("Date Fin", TempToPurchPrice."Ending Date"); // AD Le 01-04-2015
                                    VALIDATE("Minimum Qty", TempToPurchPrice."Minimum Quantity");
                                    IF NOT INSERT() THEN MODIFY();
                                    IF par_RequisitionLine."Vendor No." = rec_ItemByVendor."Vendor No." THEN
                                        // MCO Le 04-10-2018 => Régie : Si le prix séléctionné est passé en paramétre alors on ne gère que lui sinon on prend le meilleur prix du fournisseur
                                        IF (FORMAT(par_RequisitionLine.RECORDID) <> '') AND (par_RequisitionLine."Price Selectionned") THEN BEGIN
                                            IF (CheckSameRecords(par_RequisitionLine, rec_Buffer)) THEN BEGIN// On peut séléctionner un prix depuis la fenêtre d'appro
                                                Quantity := par_RequisitionLine.Quantity;
                                                Selected := TRUE;
                                                MODIFY();
                                            END;
                                        END
                                        ELSE
                                            IF NOT CheckLineVendor(par_RequisitionLine, rec_ItemByVendor, dec_NetPurchPrice, rec_Buffer) THEN BEGIN

                                                Quantity := par_RequisitionLine.Quantity;
                                                Selected := TRUE;
                                                MODIFY();
                                            END;
                                    // FIN MCO Le 04-10-2018
                                END;
                    UNTIL TempToPurchPrice.NEXT() = 0;
            UNTIL rec_ItemByVendor.NEXT() = 0;

    end;

    procedure SetRecordsByMulti(ItemNo: Code[20]; Quantite_Dde: Decimal; VendorNo: Code[20]; var rec_Buffer: Record "Vendor Price Buffer" temporary; TypeCommande: Integer; _pToutFournisseur: Boolean)
    var
        rec_Item: Record Item;
        rec_ItemByVendor: Record "Item Vendor";
        ItemUOM: Record "Item Unit of Measure";
        LVendor: Record Vendor;
        LCurrExchRate: Record "Currency Exchange Rate";
        TempToPurchDisc: Record "Purchase Line Discount" temporary;
        TempToPurchPrice: Record "Purchase Price" temporary;
        "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
        CduLCodeunitsFunctions: Codeunit "Codeunits Functions";
        dec_BrutPurchPrice: Decimal;
        dec_PurchDisc: Decimal;
        dec_NetPurchPrice: Decimal;
        MeilleurPrixVenteNet: Decimal;
    begin
        //Insertion des enregistrements de la table

        //Initialisation des variables

        // Calcul du meilleur Prix Net
        MeilleurPrixVenteNet := GetMeilleurPrixNet(ItemNo);


        //On récupère la liste des articles vendeurs.
        rec_Item.GET(ItemNo);

        rec_ItemByVendor.RESET();
        rec_ItemByVendor.SETRANGE("Item No.", ItemNo);

        // AD Le 31-05-2011
        IF NOT _pToutFournisseur THEN BEGIN
            // MC Le 19-07-2012 => Demande de L.M ne pas filtrer sur contrôle en cours.
            // Ancien Code
            //rec_ItemByVendor.SETFILTER("Statut Qualité", '%1|%2', rec_ItemByVendor."Statut Qualité"::Conforme,
            //    rec_ItemByVendor."Statut Qualité"::"Contrôle en cours");
            // Nouveau Code
            rec_ItemByVendor.SETRANGE("Statut Qualité", rec_ItemByVendor."Statut Qualité"::Conforme);
            rec_ItemByVendor.SETRANGE("Statut Approvisionnement", rec_ItemByVendor."Statut Approvisionnement"::"Non Bloqué");
        END;
        // FIN AD Le 31-05-2011

        // Parcours des enregistrements.
        IF rec_ItemByVendor.FINDSET() THEN
            REPEAT
                // AD Le 01-04-2015
                CLEAR(TempToPurchDisc);
                CLEAR(TempToPurchPrice);
                // FIN AD Le 01-04-2015

                // Initialisation des variables;
                dec_BrutPurchPrice := 0;
                dec_PurchDisc := 0;
                dec_NetPurchPrice := 0;

                LVendor.GET(rec_ItemByVendor."Vendor No.");

                // Récupère le prix d'achat et la remise pour le fournisseur.
                //On récupère le prix d'achat
                //CFR Le 23/03/2022 => R‚gie : Recherce des tarifs avec cout null pour Export des donn‚es articles (Report 50048)
                /*
                                "Purch Price Calc. Mgt.".FindPurchPrice(TempToPurchPrice, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                                        , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                                        // AD Le 10-04-2012 => Gestion Devise
                                                                        //,'', WORKDATE,FALSE);// CODE D'ORIGINE
                                                                        , LVendor."Currency Code", WORKDATE, FALSE);
                                // FIN AD Le 10-04-2012
*/
                CduLCodeunitsFunctions.FindPurchPriceWithNull(TempToPurchPrice, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                                 , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                                 // AD Le 10-04-2012 => Gestion Devise
                                                                 //,'', WORKDATE,FALSE);// CODE D'ORIGINE
                                                                 , LVendor."Currency Code", WORKDATE(), FALSE);
                // FIN AD Le 10-04-2012

                // Rajout du filtre sur la quantité minimum
                //TempToPurchPrice.SETRANGE("Minimum Quantity", 0, Quantite_Dde); => AD Le 12-01-2012 => On veut tous les prix


                IF TempToPurchPrice.FindSet() THEN // MCO Le 21-11-2018 => Régie => Avant c'était findlast
                    REPEAT // MCO Le 21-11-2018 => Régie
                           // Récupère les informations du prix d'achat.
                        dec_BrutPurchPrice := TempToPurchPrice."Direct Unit Cost";

                        // AD Le 10-04-2012 => Gestion Devise
                        IF TempToPurchPrice."Currency Code" <> '' THEN
                            dec_BrutPurchPrice := ROUND(
                                        LCurrExchRate.ExchangeAmtFCYToLCY(WORKDATE(), TempToPurchPrice."Currency Code",
                                        TempToPurchPrice."Direct Unit Cost",
                                        LCurrExchRate.ExchangeRate(WORKDATE(), TempToPurchPrice."Currency Code")));


                        // FIN AD Le 10-04-2012


                        IF ItemUOM.GET(rec_ItemByVendor."Item No.", rec_ItemByVendor."Purch. Unit of Measure") THEN
                            dec_BrutPurchPrice := dec_BrutPurchPrice / ItemUOM."Qty. per Unit of Measure";


                        // On récupère la remise d'achat
                        "Purch Price Calc. Mgt.".FindPurchLineDisc(TempToPurchDisc, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                                                   , rec_ItemByVendor."Variant Code", rec_ItemByVendor."Purch. Unit of Measure"
                                                                   , '', WORKDATE(), FALSE, TypeCommande, Quantity); // Pas de filtre sur type de commande. Le 26/02/12 Pourquoi  ????? je remets

                        //CFR le 14/09/2021 => Régie : La remise achat est fonction de la quantité minimum de commande
                        /* ANCIEN CODE
                        // Rajout du filtre sur la quantité minimum
                        //TempToPurchDisc.SETRANGE("Minimum Quantity", 0, Quantite_Dde);    => AD Le 12-01-2012 => On veut tous les prix

                        IF TempToPurchDisc.FINDLAST THEN
                          dec_PurchDisc := TempToPurchDisc."Line Discount %";
                        */
                        TempToPurchDisc.SETRANGE("Minimum Quantity", 0, TempToPurchPrice."Minimum Quantity");
                        IF TempToPurchDisc.FINDLAST() THEN
                            dec_PurchDisc := TempToPurchDisc."Line Discount %";
                        //FIN CFR le 14/09/2021

                        //IF TempToPurchPrice."Ne pas Appliquer les remises" THEN
                        //  dec_PurchDisc := 0;

                        dec_NetPurchPrice := ROUND(
                                                           (dec_BrutPurchPrice * (1 - dec_PurchDisc / 100))
                                                           /// UOMMgt.GetQtyPerUnitOfMeasure(Item, Item."Purch. Unit of Measure")
                                                           //* "Qty. per Unit of Measure"
                                                           , 0.01);
                        IF dec_NetPurchPrice = 0 THEN
                            dec_NetPurchPrice := dec_BrutPurchPrice;

                        // AD Le 06-02-2015 => Nathalie veut aussi les fournisseurs sans Tarifs
                        //IF dec_NetPurchPrice <> 0 THEN
                        IF (dec_NetPurchPrice <> 0) OR (_pToutFournisseur) THEN
                            // FIN AD Le 06-02-2015

                            WITH rec_Buffer DO BEGIN
                                // Insertion de l'enregistrement dans la table.
                                INIT();
                                VALIDATE("Worksheet Template Name", 'MULTI');
                                VALIDATE("Journal Batch Name", 'MULTI');
                                VALIDATE("Line No.", 10000);
                                VALIDATE("Item No.", ItemNo);
                                VALIDATE("Vendor No.", rec_ItemByVendor."Vendor No.");
                                IF VendorNo = rec_ItemByVendor."Vendor No." THEN BEGIN
                                    Quantity := Quantite_Dde;
                                    Selected := TRUE;
                                END;
                                VALIDATE("Unit Price", dec_BrutPurchPrice);
                                VALIDATE(Discount, dec_PurchDisc);
                                VALIDATE("Net Unit Price", dec_NetPurchPrice);
                                VALIDATE("Minimum order", rec_ItemByVendor."Minimum Order Quantity");
                                VALIDATE("By How Many", rec_ItemByVendor."Purch. Multiple");
                                VALIDATE("Profit %", CalcMarge(dec_NetPurchPrice, MeilleurPrixVenteNet, rec_ItemByVendor."Indirect Cost"));
                                VALIDATE("Statut Qualité", "Statut Qualité");
                                VALIDATE("Statut Approvisionnement", "Statut Approvisionnement");
                                //CFR le 14/09/2021 => Régie : homgénisation des fonctions SetRecords() & SetRecordsByMulti()
                                VALIDATE("Référence Fournisseur", rec_ItemByVendor."Vendor Item No.");
                                VALIDATE("Vendor Item Desciption", rec_ItemByVendor."Vendor Item Desciption"); // AD Le 30-01-2020 => REGIE
                                VALIDATE("Code Remise", rec_ItemByVendor."Code Remise");
                                VALIDATE("Meilleur Prix Vente", MeilleurPrixVenteNet);
                                VALIDATE("Unité d'achat", rec_ItemByVendor."Purch. Unit of Measure");
                                //FIN CFR le 14/09/2021
                                VALIDATE("Date début", TempToPurchPrice."Starting Date");
                                VALIDATE("Date Fin", TempToPurchPrice."Ending Date");
                                VALIDATE("Minimum Qty", TempToPurchPrice."Minimum Quantity");// MCO Le 21-11-2018 => Régie
                                IF NOT INSERT() THEN MODIFY();
                            END;
                    UNTIL TempToPurchPrice.NEXT() = 0; // MCO Le 21-11-2018 => Régie
            UNTIL rec_ItemByVendor.NEXT() = 0;

    end;

    procedure GetMeilleurPrixNet(_ItemNo: Code[20]): Decimal
    var
        TempSalesPrice: Record "Sales Price" temporary;
        rec_Item: Record Item;
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        dec_RemCliMax: Decimal;
        remise: Decimal;
        prix: Decimal;
        PrixNet: Decimal;
    begin
        rec_Item.GET(_ItemNo);

        // récupération de la remise maximum pour l'article
        dec_RemCliMax := LCodeunitsFunctions.GetBestSalesDiscForAll(rec_Item."No.", '', '', '', WORKDATE());

        // Récupération de du prix brut pour le type STD
        IF NOT LCodeunitsFunctions.GetUnitPrice(
                  TempSalesPrice, '', '', 'STD',
                  '', rec_Item."No.", '', rec_Item."Base Unit of Measure", '', WORKDATE(), FALSE, 0, remise, prix,
                  '', '', 0) THEN;// MC Le 19-04-2011 => SYMTA -> Tarifs ventes.


        // Calcul du prix Net
        PrixNet := ROUND(prix - (prix * dec_RemCliMax) / 100, 0.01);

        //MESSAGE('Meilleur Prix de vente %1', PrixNet);

        EXIT(PrixNet);
    end;

    PROCEDURE CalcMarge(pPrixAchatNet: Decimal; pPrixVenteNet: Decimal; pCoutIndirect: Decimal): Decimal;
    VAR
        Retour_Marge: Decimal;
    BEGIN
        //CFR le 27/09/2023 => R‚gie : Fonction CalcMarge() : Visibilit‚ pour page 50026 + int‚gration des co–t indirect
        IF (pPrixVenteNet <> 0) THEN
            //Retour_Marge := ROUND(100 * (1 - _PaNet / pPrixVenteNet),0.00001)
            Retour_Marge := ROUND(100 * (pPrixVenteNet - (pPrixAchatNet + pCoutIndirect)) / pPrixVenteNet, 0.00001)
        ELSE
            Retour_Marge := 0;

        EXIT(Retour_Marge);
    END;

    local procedure CheckLineVendor(par_requisitioneline: Record "Requisition Line"; par_itemvendor: Record "Item Vendor"; par_netunitprice: Decimal; var rec_Buffer: Record "Vendor Price Buffer" temporary): Boolean
    begin
        // MCO Le 04-10-2018 => Régie : Fonction permettant de vérifier s'il n'y a pas déjà un meilleur prix pour le fournisseur
        rec_Buffer.RESET();

        rec_Buffer.SETRANGE("Worksheet Template Name", par_requisitioneline."Worksheet Template Name");
        rec_Buffer.SETRANGE("Journal Batch Name", par_requisitioneline."Journal Batch Name");
        rec_Buffer.SETRANGE("Line No.", par_requisitioneline."Line No.");
        rec_Buffer.SETRANGE("Vendor No.", par_itemvendor."Vendor No.");
        rec_Buffer.SETRANGE("Item No.", par_itemvendor."Item No.");
        IF rec_Buffer.FINDSET() THEN
            REPEAT
                IF rec_Buffer."Net Unit Price" < par_netunitprice THEN BEGIN
                    rec_Buffer.RESET();
                    EXIT(TRUE);
                END
                ELSE BEGIN // On décoche séléctionné la ligne précédente
                    rec_Buffer.Selected := FALSE;
                    rec_Buffer.Quantity := 0;
                    rec_Buffer.MODIFY();
                END;
            UNTIL rec_Buffer.NEXT() = 0;
        rec_Buffer.RESET();
    end;

    procedure SetVendorPriceSelected(var pVendorPriceBuffer: Record "Vendor Price Buffer")
    begin
        gBufferPrice.DELETEALL();
        gBufferPrice := pVendorPriceBuffer;
        gBufferPrice.Insert();
        PriceSelected := TRUE;
    end;

    local procedure CheckSameRecords(par_requisitioneline: Record "Requisition Line"; var par_BufferPrice: Record "Vendor Price Buffer"): Boolean
    begin
        // MCO => On contrôle que le prix en global est passé en paramétre
        //MESSAGE(FORMAT(par_requisitioneline."Buffer Price Record ID"));
        //MESSAGE(FORMAT(par_BufferPrice.RECORDID));
        IF FORMAT(par_requisitioneline."Buffer Price Record ID") = FORMAT(par_BufferPrice.RECORDID) THEN
            EXIT(TRUE);
    end;
}

