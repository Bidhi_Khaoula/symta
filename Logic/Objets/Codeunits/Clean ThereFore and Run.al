// codeunit 50064 "Clean ThereFore and Run"
// {
//     // // Programme à pour but d'épurer les documents n'existant plus avant de lancer therefore &afin de ne plus avoir de plantage

//     TableNo = "Job Queue Entry";

//     trigger OnRun()
//     var
//         cu_ThereFore: Codeunit "50068";
//         recL_ThereForeList: Record "52101147";
//         recL_SalesHeader: Record "Sales Header";
//         recL_ThereForeList2: Record "52101147";
//     begin
//         // Épuration des documents
//         recL_ThereForeList.RESET();
//         recL_ThereForeList.SETRANGE("Table ID", 36);
//         IF recL_ThereForeList.FINDSET() THEN
//             REPEAT
//                 IF NOT recL_SalesHeader.GET(recL_ThereForeList."Document Type", recL_ThereForeList."Document No.") THEN BEGIN
//                     recL_ThereForeList2 := recL_ThereForeList;
//                     recL_ThereForeList2.DELETE(TRUE);
//                 END
//                 // MCO Le 26-07-2017 => Ticket 25547

//                 ELSE
//                     IF recL_SalesHeader.Status = recL_SalesHeader.Status::Open THEN BEGIN
//                         recL_ThereForeList2 := recL_ThereForeList;
//                         recL_ThereForeList2.DELETE(TRUE);
//                     END
//                     // FIN MCO Le 26-07-2017 => Ticket 25547


//                     //VD Le 26-07-2017 purge des docs en erreur
//                     ELSE
//                         IF recL_ThereForeList.Error = TRUE THEN BEGIN
//                             recL_ThereForeList2 := recL_ThereForeList;
//                             recL_ThereForeList2.DELETE(TRUE);
//                         END;
//             //FIN VD


//             UNTIL recL_ThereForeList.NEXT() = 0;

//         // On lance le codeunit
//         cu_ThereFore.RUN();
//     end;
// }

