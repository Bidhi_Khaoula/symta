codeunit 50023 "Gestion Impression Zone"
{
    SingleInstance = true;

    trigger OnRun()
    begin
    end;

    var
        CodeMagasin: Code[10];
        CodeZone: Code[10];

    procedure SetZone(_pCodeMagasin: Code[20]; _pCodeZone: Code[20])
    begin
        CodeMagasin := _pCodeMagasin;
        CodeZone := _pCodeZone;
    end;

    procedure GetZone(): Code[10]
    begin
        EXIT(CodeZone);
    end;

    procedure GetZonePrinter(): Text[30]
    var
        Zone: Record Zone;
    begin
        IF CodeZone = '' THEN EXIT('');

        IF NOT Zone.GET(CodeMagasin, CodeZone) THEN EXIT('');


        EXIT(Zone."BP Printer Name");
    end;
}

