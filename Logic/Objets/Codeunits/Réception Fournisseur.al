codeunit 50032 "Réception Fournisseur"
{
    trigger OnRun()
    begin
    end;

    var
        GestionMultiRef: Codeunit "Gestion Multi-référence";

    procedure "Extraire Ligne"(var WarehouseReceiptHeader: Record "Warehouse Receipt Header")
    var

        EnteteFacture: Record "Import facture tempo. fourn.";
        LigneFacture: Record "Import facture tempo. fourn.";
        whseRcptLine: Record "Warehouse Receipt Line";
         commande: Record "Purchase Header";
        GetSourceBatch: Report "Get Source Documents SYM";
        cu: Codeunit "Réception Fournisseur";
        release: Codeunit "Release Purchase Document";
        FrmFacture: Page "facture fournisseur temporaire";
        "FrmligneNonAffecté": Page "Ligne facture four. tempo";
        sortir: Boolean;
        TEXT001Lbl: Label '''Lancé la commande %1 ?''' , Comment = '%1 = Num de Commande';
        Text: Text[50];


        dec_Qte: Decimal;
        "Plusd'erreur": Boolean;
    begin
        //** Cette fonction permet de corriger les éventuelles erreurs sur les lignes de factures temporaires
        // Puis de les extraire sur la réception. Fonction Appelée depuis la réception **

        //Récuperation des entetes
        CLEAR(FrmFacture);
        FrmFacture.LOOKUPMODE := TRUE;
        IF NOT (FrmFacture.RUNMODAL() = ACTION::LookupOK) THEN
            EXIT;
        FrmFacture.recup_filtre(EnteteFacture);
        "Traitement des erreurs"(EnteteFacture);
        "Plusd'erreur" := FALSE;
        //corection et verifié que toutes les erreurs sont corrigées
        IF EnteteFacture.FINDFIRST() THEN
            REPEAT
                sortir := FALSE;
                REPEAT
                    COMMIT();
                    LigneFacture.SETRANGE("No Facture", EnteteFacture."No Facture");
                    LigneFacture.SETRANGE("Code fournisseur", EnteteFacture."Code fournisseur");
                    CLEAR(FrmligneNonAffecté);
                    FrmligneNonAffecté.LOOKUPMODE := TRUE;
                    FrmligneNonAffecté.SETTABLEVIEW(LigneFacture);
                    IF FrmligneNonAffecté.RUNMODAL() <> ACTION::LookupOK THEN
                        sortir := TRUE;
                    LigneFacture.SETRANGE("Type erreur", 1, 9999);
                    "Plusd'erreur" := LigneFacture.ISEMPTY;
                    LigneFacture.SETRANGE("Type erreur");
                UNTIL "Plusd'erreur" OR sortir;

                IF sortir THEN EXIT;
            UNTIL EnteteFacture.NEXT() = 0;

        //lancement des commandes achat
        IF EnteteFacture.FINDFIRST() THEN
            REPEAT
                CLEAR(LigneFacture);
                LigneFacture.SETRANGE("Type ligne", LigneFacture."Type ligne"::Ligne);
                LigneFacture.SETRANGE("No Facture", EnteteFacture."No Facture");
                LigneFacture.SETRANGE("Code fournisseur", EnteteFacture."Code fournisseur");
                LigneFacture.SETRANGE("date intégration", 0D);
                LigneFacture.FINDFIRST();
                REPEAT
                    commande.GET(commande."Document Type"::Order, LigneFacture."No commande affecté");
                    Text := STRSUBSTNO(TEXT001Lbl, commande."No.");
                    IF commande.Status = commande.Status::Open THEN
                        IF CONFIRM(Text, TRUE) THEN
                            release.RUN(commande)
                        ELSE
                            ERROR('Arrêt utilisateur');
                UNTIL LigneFacture.NEXT() = 0;

            UNTIL EnteteFacture.NEXT() = 0;

        CLEAR(LigneFacture);
        //Creation des lignes de reception magasin
        IF EnteteFacture.FINDFIRST() THEN
            REPEAT
                LigneFacture.SETRANGE("Type ligne", LigneFacture."Type ligne"::Ligne);
                LigneFacture.SETRANGE("No Facture", EnteteFacture."No Facture");
                LigneFacture.SETRANGE("Code fournisseur", EnteteFacture."Code fournisseur");
                LigneFacture.SETRANGE("date intégration", 0D);
                LigneFacture.SETFILTER("No ligne de commande", '>0');
                IF LigneFacture.FINDFIRST() THEN
                    REPEAT
                        // Avant de créer les lignes de réceptions, il faut vérifier que celles-ci n'existent pas dans la réception.
                        whseRcptLine.RESET();
                        whseRcptLine.SETRANGE("Source Type", 39);
                        whseRcptLine.SETRANGE("Source No.", LigneFacture."No commande affecté");
                        whseRcptLine.SETRANGE(whseRcptLine."Source Line No.", LigneFacture."No ligne de commande");
                        whseRcptLine.SETRANGE("No.", WarehouseReceiptHeader."No.");
                        IF whseRcptLine.FINDFIRST() THEN BEGIN
                            dec_Qte := whseRcptLine."Qty. to Receive" + LigneFacture.Quantité;
                            whseRcptLine.VALIDATE("Qty. to Receive", dec_Qte);
                            // AD Le 20-03-2013 => Gestion des litiges
                            whseRcptLine.VALIDATE("Qte Reçue théorique", whseRcptLine."Qty. to Receive");
                            whseRcptLine.VALIDATE("Prix Brut Théorique", LigneFacture."Pu brut commande");
                            whseRcptLine.VALIDATE("Remise 1 Théorique", LigneFacture."Remise 1 commande");
                            whseRcptLine.VALIDATE("Remise 2 Théorique", LigneFacture."Remise 2 commande");
                            // FIN AD Le 20-03-2013
                            whseRcptLine.MODIFY();
                        END
                        ELSE BEGIN
                            // Appel du programme standard de lancement de reception.
                            CLEAR(GetSourceBatch);
                            // MC Le 14-09-2011 => Possibilité de mettre une ligne de commande sur deux réceptions différentes.
                            GetSourceBatch.HideCheckIfLineInWhseDoc(TRUE);
                            // Recherche si la ligne est déjà sur cette reception
                            whseRcptLine.RESET();
                            whseRcptLine.SETRANGE("Source Type", 39);
                            whseRcptLine.SETRANGE("Source No.", LigneFacture."No commande affecté");
                            whseRcptLine.SETRANGE(whseRcptLine."Source Line No.", LigneFacture."No ligne de commande");
                            whseRcptLine.SETRANGE("No.", WarehouseReceiptHeader."No.");
                            IF whseRcptLine.FINDLAST() THEN BEGIN
                                whseRcptLine.VALIDATE("Qty. to Receive", whseRcptLine."Qty. to Receive" + LigneFacture.Quantité);
                                // AD Le 20-03-2013 => Gestion des litiges
                                whseRcptLine.VALIDATE("Qte Reçue théorique", whseRcptLine."Qty. to Receive");
                                whseRcptLine.VALIDATE("Prix Brut Théorique", LigneFacture."Pu brut commande");
                                whseRcptLine.VALIDATE("Remise 1 Théorique", LigneFacture."Remise 1 commande");
                                whseRcptLine.VALIDATE("Remise 2 Théorique", LigneFacture."Remise 2 commande");
                                // FIN AD Le 20-03-2013

                                whseRcptLine.MODIFY();
                            END
                            ELSE BEGIN
                                // FIN MC Le 14-09-2011
                                GetSourceBatch.SetOneCreatedReceiptHeader(WarehouseReceiptHeader);
                                cu.SetFilterExt(GetSourceBatch,
                                      WarehouseReceiptHeader."Location Code", LigneFacture."No commande affecté", LigneFacture."No ligne de commande");
                                GetSourceBatch.USEREQUESTPAGE(FALSE);
                                GetSourceBatch.RUNMODAL();
                                whseRcptLine.RESET();
                                whseRcptLine.SETRANGE("Source Type", 39);
                                whseRcptLine.SETRANGE("Source No.", LigneFacture."No commande affecté");
                                whseRcptLine.SETRANGE(whseRcptLine."Source Line No.", LigneFacture."No ligne de commande");
                                whseRcptLine.SETRANGE("No.", WarehouseReceiptHeader."No.");
                                whseRcptLine.FINDLAST();
                                whseRcptLine.VALIDATE("Qty. to Receive", LigneFacture.Quantité);
                                // AD Le 20-03-2013 => Gestion des litiges
                                whseRcptLine.VALIDATE("Qte Reçue théorique", whseRcptLine."Qty. to Receive");
                                whseRcptLine.VALIDATE("Prix Brut Théorique", LigneFacture."Pu brut commande");
                                whseRcptLine.VALIDATE("Remise 1 Théorique", LigneFacture."Remise 1 commande");
                                whseRcptLine.VALIDATE("Remise 2 Théorique", LigneFacture."Remise 2 commande");
                                // FIN AD Le 20-03-2013

                                whseRcptLine.MODIFY();
                            END;
                        END;
                    UNTIL LigneFacture.NEXT() = 0;
            UNTIL EnteteFacture.NEXT() = 0;

        //Modifier les quantités a expédier
        IF EnteteFacture.FINDFIRST() THEN
            REPEAT
                ChangeQuantité(WarehouseReceiptHeader, EnteteFacture);
            UNTIL EnteteFacture.NEXT() = 0;
    end;

    procedure "Traitement des erreurs"(var Entete_import_facture: Record "Import facture tempo. fourn."): Boolean
    var
        Ligne_import_facture: Record "Import facture tempo. fourn.";
        nbErreur: Integer;
             
    begin
        //** Cette fonction permet de traiter les différentes erreurs possibles, elle est aiguillée vers deux fonctions :
        // La première : "erreur ligne non affecté" regarde les lignes qui n'ont pas de commande et les mets en erreur ou les affectent
        // La seconde  : "erreur ligne affecté" regarde les lignes ayant des affectations et peux renvoyer sur la fonction précédente **

        // Vérification du nombre d'erreurs.
        Ligne_import_facture.RESET();
        Entete_import_facture."erreur integration" := '';
        nbErreur := 0;
        Ligne_import_facture.SETCURRENTKEY("No Facture");
        Ligne_import_facture.SETRANGE("No Facture", Entete_import_facture."No Facture");
        Ligne_import_facture.SETRANGE("Type ligne", Ligne_import_facture."Type ligne"::Ligne);
        Ligne_import_facture.SETRANGE("Code fournisseur", Entete_import_facture."Code fournisseur");
        Ligne_import_facture.SETRANGE("date intégration", 0D);
        IF Ligne_import_facture.FINDFIRST() THEN
            REPEAT
                IF Ligne_import_facture."No commande" = '' THEN
                    nbErreur += "erreur ligne non affecté"(Ligne_import_facture)
                ELSE
                    nbErreur += "erreur ligne affecté"(Ligne_import_facture);
            UNTIL Ligne_import_facture.NEXT() = 0;

        // modification de l'en-tête si il y a des erreurs.
        IF nbErreur <> 0 THEN
            Entete_import_facture."erreur integration" := STRSUBSTNO('%1 erreurs', nbErreur);

        IF Entete_import_facture.MODIFY() THEN;

        EXIT(nbErreur = 0);
    end;

    procedure "erreur ligne non affecté"(var Ligne_import_facture: Record "Import facture tempo. fourn.") nbErreur: Integer
    var
        "ligne achat": Record "Purchase Line";
         NbLine: Integer;
      /*   Find: Boolean;
        dec_QteSurFac: Decimal; */
    begin
        IF Ligne_import_facture."Code article SYMTA" <> 'RIENTROUVE' THEN
            Ligne_import_facture."Code Article Origine" := Ligne_import_facture."Code article SYMTA";
        Ligne_import_facture."Code article SYMTA" := GestionMultiRef.RechercheMultiReference(Ligne_import_facture."Code article SYMTA");
        Ligne_import_facture."erreur integration" := '';
        Ligne_import_facture."Plusieur ligne possible" := FALSE;
        Ligne_import_facture."Erreur Quantité" := FALSE;
        Ligne_import_facture."Erreur côut" := FALSE;
        Ligne_import_facture."Aucune ligne trouvé" := FALSE;
        Ligne_import_facture."Type erreur" := 0;
        Ligne_import_facture."No commande affecté" := '';
        Ligne_import_facture."No ligne de commande" := 0;
        Ligne_import_facture."No commande" := '';
        Ligne_import_facture."ligne commande" := 0;
        Ligne_import_facture.MODIFY();

        //ERREUR SUR Quantité =0
        IF Ligne_import_facture.Quantité = 0 THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 1;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Qte=0 ';
            Ligne_import_facture."Erreur Quantité" := TRUE;
            Ligne_import_facture.MODIFY();
        END;

        //ERREUR SUR total net =0
        IF Ligne_import_facture."Total Net" = 0 THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 1;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Total Net=0 ';
            Ligne_import_facture."Erreur côut" := TRUE;
            Ligne_import_facture.MODIFY();
        END;

        //Si pas d'article erreur
        IF (Ligne_import_facture."Code article SYMTA" = '') AND (Ligne_import_facture."Code article Fournisseur" = '') THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 2;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Code Article vide ';
            Ligne_import_facture.MODIFY();
        END
        ELSE
         // Sinon on fait toutes les autres vérifications et affecte la commande si aucune autre erreur
         BEGIN
            "ligne achat".RESET();
            "ligne achat".SETRANGE("Document Type", "ligne achat"."Document Type"::Order);
            "ligne achat".SETRANGE("Buy-from Vendor No.", Ligne_import_facture."Code fournisseur");
            "ligne achat".SETRANGE(Type, "ligne achat".Type::Item);
            "ligne achat".SETRANGE("item Reference No.", Ligne_import_facture."Code article Fournisseur");
            // MC Le 13-02-2012 -> Si la référence est vide alors le programme ramène toutes les lignes de commande n'ayant pas de référence
            // donc correction du BUG en testant si le champ est vide.
            IF ("ligne achat".ISEMPTY) OR (Ligne_import_facture."Code article Fournisseur" = '') THEN
             // FIN MC Le 13-02-2012
             BEGIN
                IF Ligne_import_facture."Code article SYMTA" <> '' THEN BEGIN
                    "ligne achat".SETRANGE("item Reference No.");
                    //"ligne achat".SETRANGE("Recherche référence",Ligne_import_facture."Code article SYMTA");
                    "ligne achat".SETRANGE("No.", Ligne_import_facture."Code article SYMTA");
                END;
            END;
            "ligne achat".SETFILTER("Outstanding Quantity", '<>0');
            IF NOT "ligne achat".FINDFIRST() THEN BEGIN
                //Si pas de commande trouvé quelque soit la quantité
                nbErreur := nbErreur + 1;
                Ligne_import_facture."Type erreur" := 2;
                Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'commande non trouvée ';
                Ligne_import_facture."Aucune ligne trouvé" := TRUE;
                Ligne_import_facture.MODIFY();
            END
            ELSE BEGIN
                // On ne tiens pas en compte des quantités
                //si plusieur ligne trouvé
                IF "ligne achat".COUNT > 1 THEN BEGIN
                    // MC Le 13-09-2011 => On ne prend pas en compte la quantité exacte.
                    //"ligne achat".SETRANGE("Outstanding Quantity",Ligne_import_facture.Quantité);
                    //IF "ligne achat".COUNT<>1 THEN
                    //BEGIN
                    // FIN MC Le 13-09-2011

                    // On vérifie que pour les lignes trouvés la quantité n'est pas totalement en réception

                    NbLine := 0;
                    IF "ligne achat".FINDFIRST() THEN
                        REPEAT
                            "ligne achat".CALCFIELDS("Qty On Fac Receipt", "Qty On Receipt Order Manual");
                            /*
                            IF "ligne achat"."Outstanding Quantity" >
                            ("ligne achat"."Qty On Fac Receipt" + "ligne achat"."Qty On Receipt Order Manual" +
                            Ligne_import_facture.Quantité)
                            */
                            //IF ("ligne achat"."Outstanding Quantity" > 0 THEN BEGIN
                            IF ("ligne achat"."Outstanding Quantity" - "ligne achat"."Qty On Receipt Order Manual" -
                                  "ligne achat"."Qty On Fac Receipt" + Ligne_import_facture.Quantité) > Ligne_import_facture.Quantité THEN BEGIN

                                NbLine += 1;
                                "ligne achat".MARK(TRUE);
                            END;
                        UNTIL "ligne achat".NEXT() = 0;
                    "ligne achat".MARKEDONLY(TRUE);
                    // Si on a trouvé plus d'une ligne alors on met en erreur plusieurs lignes possible
                    IF NbLine > 1 THEN BEGIN
                        Ligne_import_facture."Plusieur ligne possible" := TRUE;
                        // MC Le 13-07-2011 => Cette erreur est prioritaire par rapport aux erreurs de quantité
                        IF (Ligne_import_facture."Type erreur" = 0) OR (Ligne_import_facture."Type erreur" = 1) THEN BEGIN
                            Ligne_import_facture."Type erreur" := 2;
                            Ligne_import_facture."Erreur côut" := FALSE;
                            Ligne_import_facture."Erreur Quantité" := FALSE;
                        END;
                        Ligne_import_facture.MODIFY();
                    END;
                END;
            END;

            IF Ligne_import_facture."Plusieur ligne possible" = FALSE THEN
                IF ("ligne achat".FINDFIRST()) AND (NOT Ligne_import_facture."Plusieur ligne possible") THEN BEGIN
                    //vérification du montant
                    IF (Ligne_import_facture."Total Net" <> 0) THEN BEGIN
                        IF (Ligne_import_facture."Pu net" = 0) THEN BEGIN
                            Ligne_import_facture."Pu net" := ROUND(Ligne_import_facture."Total Net" / Ligne_import_facture.Quantité, 0.01);
                            Ligne_import_facture.MODIFY();
                        END;
                        // MC Le 06-06-2011 => On prend le cout net de la commande d'achat.
                        //IF Ligne_import_facture."Pu net"<>"ligne achat"."Direct Unit Cost" THEN
                        IF (
                        ABS(Ligne_import_facture."Pu net" - "ligne achat"."Net Unit Cost") > 0.01
                        ) THEN
                          // FIN MC Le 06-06-2011
                          BEGIN
                            nbErreur := nbErreur + 1;
                            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'erreur prix ';
                            Ligne_import_facture."Erreur côut" := TRUE;
                            Ligne_import_facture."Type erreur" := 1;
                            Ligne_import_facture.MODIFY();
                        END
                        ELSE BEGIN
                            IF "ligne achat"."Direct Unit Cost" = 0 THEN BEGIN
                                Ligne_import_facture."Erreur côut" := FALSE;
                                Ligne_import_facture.MODIFY();
                                nbErreur := nbErreur - 1;
                            END;
                        END;
                    END;
                    //affectation de la ligne d'achat
                    // On vérifie que la quantité est bonne
                    "ligne achat".CALCFIELDS("Qty On Fac Receipt", "Qty On Receipt Order Manual");

                    IF "ligne achat"."Outstanding Quantity" < "ligne achat"."Qty On Fac Receipt" +
                       "ligne achat"."Qty On Receipt Order Manual" + Ligne_import_facture.Quantité THEN BEGIN
                        //  IF USERID='eskape' THEN
                        //     MESSAGE('%1',"ligne achat"."Qty On Fac Receipt"+ "ligne achat"."Qty On Receipt Order Manual" );

                        nbErreur := nbErreur + 1;
                        Ligne_import_facture."Type erreur" := 2;
                        Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'commande non trouvée ';
                        Ligne_import_facture."Aucune ligne trouvé" := TRUE;
                        Ligne_import_facture.MODIFY();
                    END
                    ELSE BEGIN
                        Ligne_import_facture."No commande affecté" := "ligne achat"."Document No.";
                        Ligne_import_facture."No ligne de commande" := "ligne achat"."Line No.";
                        Ligne_import_facture."No commande" := "ligne achat"."Document No.";
                        ;
                        Ligne_import_facture."ligne commande" := "ligne achat"."Line No.";
                        IF "ligne achat"."Outstanding Quantity" < Ligne_import_facture.Quantité THEN BEGIN
                            //Si quantité supérieur
                            nbErreur := nbErreur + 1;
                            Ligne_import_facture."Type erreur" := 1;
                            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'erreur Quantité ';
                            Ligne_import_facture."Erreur Quantité" := TRUE;
                        END;

                        Ligne_import_facture.MODIFY();
                        Ligne_import_facture.ChargeValeurCommande();
                        Ligne_import_facture.MODIFY();
                    END;
                END
                // Aucune ligne trouvé correspondante
                ELSE BEGIN
                    Ligne_import_facture."Plusieur ligne possible" := TRUE;
                    // MC Le 13-07-2011 => Cette erreur est prioritaire par rapport aux erreurs de quantité
                    IF (Ligne_import_facture."Type erreur" = 0) OR (Ligne_import_facture."Type erreur" = 1) THEN BEGIN
                        Ligne_import_facture."Type erreur" := 2;
                        Ligne_import_facture."Erreur côut" := FALSE;
                        Ligne_import_facture."Erreur Quantité" := FALSE;
                    END;
                    Ligne_import_facture.MODIFY();
                END;
        END;
        EXIT(nbErreur);

    end;

    procedure "erreur ligne affecté"(var Ligne_import_facture: Record "Import facture tempo. fourn.") nbErreur: Integer
    var
        "ligne achat": Record "Purchase Line";
    begin
        Ligne_import_facture."Code article SYMTA" := GestionMultiRef.RechercheMultiReference(Ligne_import_facture."Code article SYMTA");

        Ligne_import_facture."erreur integration" := '';
        Ligne_import_facture."Plusieur ligne possible" := FALSE;
        Ligne_import_facture."Erreur Quantité" := FALSE;
        Ligne_import_facture."Erreur côut" := FALSE;
        Ligne_import_facture."Aucune ligne trouvé" := FALSE;
        Ligne_import_facture."Type erreur" := 0;

        //ERREUR SUR Quantité =0
        IF Ligne_import_facture.Quantité = 0 THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 1;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Qte=0 ';
            Ligne_import_facture."Erreur Quantité" := TRUE;
            Ligne_import_facture.MODIFY();
        END;

        //ERREUR SUR total net =0
        IF Ligne_import_facture."Total Net" = 0 THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 1;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Total Net=0 ';
            Ligne_import_facture."Erreur côut" := TRUE;
            Ligne_import_facture.MODIFY();
        END;

        //Si pas d'article erreur
        IF (Ligne_import_facture."Code article SYMTA" = '') AND (Ligne_import_facture."Code article Fournisseur" = '') THEN BEGIN
            nbErreur := nbErreur + 1;
            Ligne_import_facture."Type erreur" := 1;
            Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Code Article vide ';
            Ligne_import_facture.MODIFY();
        END
        ELSE BEGIN
            "ligne achat".RESET();
            IF Ligne_import_facture."ligne commande" = 0 THEN BEGIN
                "ligne achat".RESET();
                "ligne achat".SETRANGE("Document Type", "ligne achat"."Document Type"::Order);
                "ligne achat".SETRANGE("Document No.", Ligne_import_facture."No commande");
                "ligne achat".SETRANGE("Outstanding Quantity", Ligne_import_facture.Quantité, 999999);
                "ligne achat".SETRANGE("item Reference No.", Ligne_import_facture."Code article Fournisseur");
                IF NOT "ligne achat".ISEMPTY AND (Ligne_import_facture."Code article Fournisseur" <> '') THEN BEGIN
                    "ligne achat".FINDFIRST();
                    Ligne_import_facture."ligne commande" := "ligne achat"."Line No."
                END
                ELSE BEGIN
                    "ligne achat".SETRANGE("item Reference No.");
                    "ligne achat".SETRANGE("No.", Ligne_import_facture."Code article SYMTA");
                    IF NOT "ligne achat".ISEMPTY AND (Ligne_import_facture."Code article SYMTA" <> '') THEN BEGIN
                        "ligne achat".FINDFIRST();
                        Ligne_import_facture."ligne commande" := "ligne achat"."Line No.";
                    END;
                END;

            END;
            "ligne achat".RESET();
            IF NOT "ligne achat".GET("ligne achat"."Document Type"::Order, Ligne_import_facture."No commande",
                              Ligne_import_facture."ligne commande")
              THEN BEGIN
                // MC Le 24-04-2012 => FE20120002
                // Nouveau Code
                IF Ligne_import_facture."No commande" <> '' THEN BEGIN
                    nbErreur := nbErreur + 1;
                    Ligne_import_facture."Type erreur" := 1;
                    Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Commande introuvable ';
                    Ligne_import_facture.MODIFY();
                    EXIT(nbErreur);
                END
                ELSE BEGIN
                    nbErreur := "erreur ligne non affecté"(Ligne_import_facture);
                    EXIT(nbErreur);
                END;
                // FIN MC Le 24-04-2012
            END;

            IF (Ligne_import_facture."Code article SYMTA" <> '') AND ("ligne achat"."No." = Ligne_import_facture."Code article SYMTA") THEN BEGIN
                //LA C'EST BON
            END
            ELSE
                IF (Ligne_import_facture."Code article Fournisseur" <> '') AND
                   ("ligne achat"."item Reference No." = Ligne_import_facture."Code article Fournisseur") THEN BEGIN
                    //LA C'EST BON
                END ELSE
                    IF (Ligne_import_facture."Code article SYMTA" <> '') AND ("ligne achat"."No." <> Ligne_import_facture."Code article SYMTA") THEN BEGIN
                        //si l'article ne corespond pas a la ligne d'achat on reprend l'algo comme si il n'avait pas d'aafectation
                        nbErreur := "erreur ligne non affecté"(Ligne_import_facture);
                        EXIT(nbErreur);
                    END
                    ELSE
                        IF (Ligne_import_facture."Code article Fournisseur" <> '') AND
                           ("ligne achat"."item Reference No." <> Ligne_import_facture."Code article Fournisseur") THEN BEGIN
                            //si l'article ne corespond pas a la ligne d'achat on reprend l'algo comme si il n'avait pas d'aafectation
                            nbErreur := "erreur ligne non affecté"(Ligne_import_facture);
                            EXIT(nbErreur);
                        END;

            // SI la ligne est déjà réceptionnée entièrement on reprend comme s'il n'y avait pas d'affectation.
            IF "ligne achat"."Outstanding Quantity" = 0 THEN BEGIN
                // MC Le 24-04-2012 => FE20120002
                // Nouveau Code
                IF Ligne_import_facture."No commande" <> '' THEN BEGIN
                    nbErreur := nbErreur + 1;
                    Ligne_import_facture."Type erreur" := 1;
                    Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Commande introuvable ';
                    Ligne_import_facture.MODIFY();
                    EXIT(nbErreur);
                END
                ELSE BEGIN
                    nbErreur := "erreur ligne non affecté"(Ligne_import_facture);
                    EXIT(nbErreur);
                END;
                // FIN MC Le 24-04-2012
            END
            ELSE BEGIN
                "ligne achat".CALCFIELDS("Qty On Receipt Order Manual", "Qty On Fac Receipt");
                IF ("ligne achat"."Outstanding Quantity") < Ligne_import_facture.Quantité THEN BEGIN
                    //Si quantité supérieur
                    nbErreur := nbErreur + 1;
                    Ligne_import_facture."Type erreur" := 1;
                    Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'erreur Quantité ';
                    Ligne_import_facture."Erreur Quantité" := TRUE;
                    Ligne_import_facture.MODIFY();
                END;

                // SI déjà en reception et que l'on dépasse la quantité de la commande
                IF ("ligne achat"."Outstanding Quantity" - "ligne achat"."Qty On Receipt Order Manual" -
                    "ligne achat"."Qty On Fac Receipt" + Ligne_import_facture.Quantité) < Ligne_import_facture.Quantité THEN BEGIN
                    // MC Le 24-04-2012 => FE20120002
                    // Nouveau Code
                    IF Ligne_import_facture."No commande" <> '' THEN BEGIN
                        nbErreur := nbErreur + 1;
                        Ligne_import_facture."Type erreur" := 1;
                        Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'Commande introuvable ';
                        Ligne_import_facture.MODIFY();
                        EXIT(nbErreur);
                    END
                    ELSE BEGIN
                        nbErreur := "erreur ligne non affecté"(Ligne_import_facture);
                        EXIT(nbErreur);
                    END;
                END;

                //vérification du montant
                IF (Ligne_import_facture."Total Net" <> 0) THEN BEGIN
                    IF (Ligne_import_facture."Pu net" = 0) THEN BEGIN
                        Ligne_import_facture."Pu net" := ROUND(Ligne_import_facture."Total Net" / Ligne_import_facture.Quantité, 0.01);
                        Ligne_import_facture.MODIFY();
                    END;
                    // MC Le 06-06-2011 => On prend le cout net de la commande d'achat.
                    //IF Ligne_import_facture."Pu net"<>"ligne achat"."Direct Unit Cost" THEN
                    IF ABS(Ligne_import_facture."Pu net" - "ligne achat"."Net Unit Cost") > 0.01 THEN BEGIN
                        nbErreur := nbErreur + 1;
                        Ligne_import_facture."erreur integration" := Ligne_import_facture."erreur integration" + 'erreur prix ';
                        Ligne_import_facture."Erreur côut" := TRUE;
                        Ligne_import_facture."Type erreur" := 1;
                        Ligne_import_facture.MODIFY();
                    END
                    ELSE BEGIN
                        IF "ligne achat"."Direct Unit Cost" = 0 THEN BEGIN
                            Ligne_import_facture."Erreur côut" := FALSE;
                            Ligne_import_facture.MODIFY();
                            nbErreur := nbErreur - 1;
                        END;
                    END;
                END;
                //affectation de la ligne d'achat
                Ligne_import_facture."No commande affecté" := "ligne achat"."Document No.";
                Ligne_import_facture."No ligne de commande" := "ligne achat"."Line No.";
                Ligne_import_facture.MODIFY();
            END;
        END;
        Ligne_import_facture.ChargeValeurCommande();
        Ligne_import_facture.MODIFY();

        EXIT(nbErreur);
    end;

    procedure SetFilterExt(var GetSourceBatch: Report "Get Source Documents SYM"; LocationCode: Code[10]; NoCommande: Code[20]; noLigne: Integer)
    var
        WhseRequest: Record "Warehouse Request";
        PurchLine: Record "Purchase Line";
        "Source Document": Code[10];
    begin
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Purchase Order";
        WhseRequest.SETFILTER("Source Document", "Source Document");
        WhseRequest.SETRANGE("Source No.", NoCommande);
        WhseRequest.SETRANGE("Location Code", LocationCode);
        PurchLine.SETRANGE("Line No.", noLigne);
        GetSourceBatch.SETTABLEVIEW(WhseRequest);
        GetSourceBatch.SETTABLEVIEW(PurchLine);
        GetSourceBatch.SetDoNotFillQtytoHandle(FALSE);
    end;

    procedure "Correction Erreur"(var Entete_import_facture: Record "Import facture tempo. fourn.")
    var
        Ligne_import_facture: Record "Import facture tempo. fourn.";
         begin
        Ligne_import_facture.SETCURRENTKEY("No Facture");
        Ligne_import_facture.SETRANGE("No Facture", Entete_import_facture."No Facture");
        Ligne_import_facture.SETRANGE("Type ligne", Ligne_import_facture."Type ligne"::Ligne);
        Ligne_import_facture.SETRANGE("Code fournisseur", Entete_import_facture."Code fournisseur");
        Ligne_import_facture.SETRANGE("date intégration", 0D);
        IF Ligne_import_facture.FINDFIRST() THEN
            REPEAT
                "Correction Erreur ligne"(Ligne_import_facture);
            UNTIL Ligne_import_facture.NEXT() = 0;
    end;

    procedure "Correction Erreur ligne"(var Ligne_import_facture: Record "Import facture tempo. fourn.")
    var
        "Entete achat": Record "Purchase Header";
        "ligne achat": Record "Purchase Line";
        release: Codeunit "Release Purchase Document";
        "Quantitésuplementaire": Decimal;
          begin
        "Entete achat".GET("Entete achat"."Document Type"::Order, Ligne_import_facture."No commande affecté");
        release.Reopen("Entete achat");
        "ligne achat".GET("Entete achat"."Document Type"::Order, Ligne_import_facture."No commande affecté",
                          Ligne_import_facture."No ligne de commande");
        "ligne achat".SuspendStatusCheck(TRUE);
        // On ne veut pas mettre à jour les infos d'origines.
        "ligne achat".HideValidationFromFac(TRUE);

        IF Ligne_import_facture."Erreur Quantité" THEN BEGIN
            Ligne_import_facture.Quantité := Ligne_import_facture."Nouvelle quantité";
            Ligne_import_facture.MODIFY();
            //Modification de la commande d'achat
            Quantitésuplementaire := Ligne_import_facture.Quantité - "ligne achat"."Outstanding Quantity";
            IF Quantitésuplementaire > 0 THEN BEGIN
                "ligne achat".VALIDATE(Quantity, "ligne achat".Quantity + Quantitésuplementaire);
                "ligne achat".MODIFY();
            END;
        END;

        IF Ligne_import_facture."Erreur côut" THEN BEGIN

            Ligne_import_facture."Pu net" := Ligne_import_facture."Nouveau cout net";
            Ligne_import_facture."Total Net" := Ligne_import_facture."Pu net" * Ligne_import_facture.Quantité;
            Ligne_import_facture.MODIFY();
            // MC Le 06-06-2011 => SYMTA -> Prix net. On recalcule la remise correspondante.
            // Mise à jour des champs Prix brut et remise 1.
            "ligne achat".VALIDATE("Direct Unit Cost", Ligne_import_facture."Nouveau Pu Brut");
            "ligne achat".VALIDATE("Discount1 %", Ligne_import_facture."Nouvelle Remise 1");
            "ligne achat".VALIDATE("Discount2 %", Ligne_import_facture."Nouvelle Remise 2");
            //decNewDiscount:= 100 - (Ligne_import_facture."Nouveau cout net" *100 / "ligne achat"."Direct Unit Cost");
            //"ligne achat".VALIDATE("Line Discount %", decNewDiscount);
            // FIN MC Le 06-06-2011
            "ligne achat".MODIFY();
        END;
        release.RUN("Entete achat");
    end;

    procedure "ChangeQuantité"(var WarehouseReceiptHeader: Record "Warehouse Receipt Header"; var Entete_import_facture: Record "Import facture tempo. fourn.")
    var
        WarehouseLine: Record "Warehouse Receipt Line";
        Ligne_import_facture: Record "Import facture tempo. fourn.";
    begin
        Ligne_import_facture.SETCURRENTKEY("No Facture");
        Ligne_import_facture.SETRANGE("No Facture", Entete_import_facture."No Facture");
        Ligne_import_facture.SETRANGE("Type ligne", Ligne_import_facture."Type ligne"::Ligne);
        Ligne_import_facture.SETRANGE("Code fournisseur", Entete_import_facture."Code fournisseur");
        Ligne_import_facture.SETRANGE("date intégration", 0D);
        IF Ligne_import_facture.FINDFIRST() THEN
            REPEAT
                WarehouseLine.SETRANGE("No.", WarehouseReceiptHeader."No.");
                WarehouseLine.SETRANGE("Source No.", Ligne_import_facture."No commande affecté");
                WarehouseLine.SETRANGE("Source Line No.", Ligne_import_facture."No ligne de commande");
                WarehouseLine.FINDFIRST();
                WarehouseLine."N° sequence facture fourn." := Ligne_import_facture."No séquence";
                // On le fait déjà avant
                //WarehouseLine.VALIDATE("Qty. to Receive",Ligne_import_facture.Quantité);
                WarehouseLine.MODIFY(TRUE);
                Ligne_import_facture."date intégration" := TODAY;
                Ligne_import_facture."heure intégration" := TIME;
                Ligne_import_facture."utilisateur integration" := USERID;
                Ligne_import_facture."No de reception" := WarehouseLine."No.";
                Ligne_import_facture."No ligne reception" := WarehouseLine."Line No.";
                Ligne_import_facture.MODIFY();

            UNTIL Ligne_import_facture.NEXT() = 0;

        Entete_import_facture."date intégration" := TODAY;
        Entete_import_facture."heure intégration" := TIME;
        Entete_import_facture."utilisateur integration" := USERID;
        Entete_import_facture."No de reception" := WarehouseReceiptHeader."No.";
        Entete_import_facture.MODIFY();
    end;
}

