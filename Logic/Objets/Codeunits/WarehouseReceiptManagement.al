codeunit 50071 "Warehouse Receipt Management"
{
    // Gestion des receptions suplémentaires
    // CFR le 22/03/2023 - R‚gie : modifier la tarification des lignes … la r‚ception


    trigger OnRun()
    begin
    end;

    var
        
        Text60001Err: Label 'Impossible d''ajouter une ligne intermédiaire.';
        //ESK000: Label 'Reçu en plus';
        ESK001Lbl: Label 'Impossible car l''unité d''achat %1 est différent de l''unité de l''article %2.', Comment = '%1 = Uinité de mesure ; %2 = Unité de mesure de base de l''article';
        gDescriptionPlusTxt: Label 'Reçu en plus %1/%2 %3' , Comment = '%1 = Quantité qui va étre recu ; %2 = Qty recu ; %3 = No Quantité';

    procedure DeleteWarehouseDocument(_pWhseRecptLine: Record "Warehouse Receipt Line")
    var
        WhseRecptLine2: Record "Warehouse Receipt Line";
        WhseRecptHeader: Record "Warehouse Receipt Header";
    begin
        //Suppression de l'expédition magasin

        WhseRecptLine2.RESET();
        WhseRecptLine2.SETRANGE("No.", _pWhseRecptLine."No.");
        IF WhseRecptLine2.FINDFIRST() THEN
            WhseRecptLine2.DELETEALL();

        WhseRecptHeader.RESET();
        IF WhseRecptHeader.GET(_pWhseRecptLine."No.") THEN
            WhseRecptHeader.DELETE();
    end;

    procedure AdditionalReceipt(WarehouseReceiptLine: Record "Warehouse Receipt Line")
    var
        PurchaseLine: Record "Purchase Line";        
        Purchaseheader: Record "Purchase Header";
         OriginePurchaseLine: Record "Purchase Line";
           WareReceptLine: Record "Warehouse Receipt Line";
           lItem: Record Item;
            ReleaseDoc: Codeunit "Release Purchase Document";
        LineNo: Integer;
        LineNoWare: Integer;
       
      
        
    begin
        WITH WarehouseReceiptLine DO BEGIN
            // ESKVN1 => Réceptionné plus que la commande initiale.
            CASE "Source Document" OF
                "Source Document"::"Purchase Order":
                    BEGIN
                        // MCO Le 20-11-2018 => Régie : Ne pas pouvoir réceptionner plus si l'unité n'est pas celle de base
                        lItem.GET("Item No.");
                        IF lItem."Base Unit of Measure" <> WarehouseReceiptLine."Unit of Measure Code" THEN
                            ERROR(STRSUBSTNO(ESK001Lbl, WarehouseReceiptLine."Unit of Measure Code", lItem."Base Unit of Measure"));
                        // FIN MCO Le 20-11-2018

                        Purchaseheader.GET(Purchaseheader."Document Type"::Order, "Source No.");
                        ReleaseDoc.Reopen(Purchaseheader);
                        //ligne de commentaire
                        /*
                        CLEAR(PurchaseLine);
                        PurchaseLine.SETRANGE("Document Type",PurchaseLine."Document Type"::Order);
                        PurchaseLine.SETRANGE("Document No." ,"Source No.");
                        PurchaseLine.FINDLAST;
                        No_ligne_com:=PurchaseLine."Line No."+10000;


                        PurchaseLine.INIT();
                        PurchaseLine."Document Type":=PurchaseLine."Document Type"::Order;
                        PurchaseLine.VALIDATE("Document No." ,"Source No.");
                        PurchaseLine.Type:=PurchaseLine.Type::" ";
                        PurchaseLine."Line No.":=No_ligne_com;
                        PurchaseLine."Attached to Line No.":=LineNo;
                        PurchaseLine.Description:='Ajoutées à la récèption';
                        PurchaseLine."Recept Exced":=TRUE;
                        //PurchaseLine.Insert(); // MCO Le 03-10-2018 => POur SYMTA pas de ligne de commentaire
                        */

                        //On récupère le prochain numéro de ligne
                        PurchaseLine.RESET();
                        PurchaseLine.SETRANGE("Document Type", PurchaseLine."Document Type"::Order);
                        PurchaseLine.SETRANGE("Document No.", "Source No.");
                        PurchaseLine.FINDLAST();
                        LineNo := PurchaseLine."Line No." + 10000;


                        //On récupère la ligne d'origine pour pouvoir reprendre le cout et la remise.
                        OriginePurchaseLine.RESET();
                        OriginePurchaseLine.SETRANGE("Document Type", PurchaseLine."Document Type"::Order);
                        OriginePurchaseLine.SETRANGE("Document No.", "Source No.");
                        OriginePurchaseLine.SETRANGE("Line No.", "Source Line No.");
                        OriginePurchaseLine.FINDFIRST();

                        PurchaseLine.INIT();
                        PurchaseLine."Document Type" := PurchaseLine."Document Type"::Order;
                        PurchaseLine.VALIDATE("Document No.", "Source No.");

                        PurchaseLine."Line No." := LineNo;
                        PurchaseLine.INSERT(TRUE);
                        PurchaseLine.Type := PurchaseLine.Type::Item;
                        PurchaseLine.VALIDATE("No.", "Item No.");
                        PurchaseLine.VALIDATE("Variant Code", "Variant Code");
                        PurchaseLine.VALIDATE("Location Code", "Location Code");
                        IF "Bin Code" <> '' THEN
                            PurchaseLine.VALIDATE("Bin Code", "Bin Code");

                        //PurchaseLine."Description 2" := PurchaseLine.Description;
                        // CFR le 16/04/2024 - R‚gie : champ Description pour R‚ception suppl‚mentaire
                        //PurchaseLine.VALIDATE(Description, ESK000);
                        PurchaseLine.VALIDATE(Description, STRSUBSTNO(gDescriptionPlusTxt, "Qty. to Receive" + "Qty. Received", Quantity, "No."));
                        // FIN CFR le 16/04/2024 - R‚gie : champ Description pour R‚ception suppl‚mentaire

                        PurchaseLine.VALIDATE(Quantity, "Qty. to Receive" - Quantity + "Qty. Received");
                        PurchaseLine."Recept Exced" := TRUE;

                        //On prend le cout et la remise
                        PurchaseLine.VALIDATE("Direct Unit Cost", OriginePurchaseLine."Direct Unit Cost");
                        PurchaseLine.VALIDATE("Line Discount %", OriginePurchaseLine."Line Discount %");
                        PurchaseLine.MODIFY();
                        WareReceptLine.RESET();

                        //Insertion des lignes après le code article concerné

                        WareReceptLine.SETRANGE("No.", "No.");
                        WareReceptLine := WarehouseReceiptLine;

                        IF WareReceptLine.FIND('>') THEN BEGIN
                            LineNoWare :=
                              (WareReceptLine."Line No." - "Line No.") DIV 2;
                            IF LineNoWare = 0 THEN
                                ERROR(Text60001Err);
                        END
                        ELSE
                            LineNoWare := 10000;

                        WareReceptLine.INIT();
                        WareReceptLine.TRANSFERFIELDS(WarehouseReceiptLine);

                        WareReceptLine."Line No." += LineNoWare;
                        WareReceptLine."Source Line No." := LineNo;
                        WareReceptLine.Description := PurchaseLine.Description;
                        WareReceptLine.VALIDATE(Quantity, "Qty. to Receive" - Quantity + "Qty. Received");
                        WareReceptLine."Qty. Outstanding" := WareReceptLine.Quantity;
                        WareReceptLine."Qty. Outstanding (Base)" := WareReceptLine."Qty. (Base)";
                        WareReceptLine."Qty. to Receive" := WareReceptLine.Quantity;
                        WareReceptLine."Qty. to Receive (Base)" := WareReceptLine."Qty. (Base)";
                        WareReceptLine."Qty. Received" := 0;
                        WareReceptLine."Qty. Received (Base)" := 0;
                        WareReceptLine."Qte Reçue théorique" := WareReceptLine."Qty. to Receive";

                        WareReceptLine.INSERT(TRUE);
                        ReleaseDoc.RUN(Purchaseheader);
                    END;

                ELSE
                    //Pour le moment seul les commande d'achat sont gérer
                    EXIT;
            END;
        END;

    end;

    PROCEDURE ChangeAmountLineOrderFromLineReceipt(VAR pWarehouseReceiptLine: Record "Warehouse Receipt Line"; pPrixBrut: Decimal; pRemise1: Decimal; pRemise2: Decimal);
    VAR
        lPurchaseheader: Record "Purchase Header";
        lOriginePurchaseLine: Record "Purchase Line";        
        lPurchaseLine: Record "Purchase Line";
        lReleasePurchaseDocument: Codeunit "Release Purchase Document";
        lLineNo: Integer;
        lQtyToReceive: Decimal;
        lQteRecuetheorique: Decimal;
    BEGIN
        // CFR le 22/03/2023 - R‚gie : modifier la tarification des lignes … la r‚ception
        WITH pWarehouseReceiptLine DO BEGIN
            CASE "Source Document" OF
                "Source Document"::"Purchase Order":
                    BEGIN
                        // R‚cupŠre l'en-tˆte de commande achat & la ligne
                        IF NOT lPurchaseheader.GET(lPurchaseheader."Document Type"::Order, "Source No.") THEN
                            EXIT;
                        IF NOT lOriginePurchaseLine.GET(lOriginePurchaseLine."Document Type"::Order, "Source No.", "Source Line No.") THEN
                            EXIT;

                        //V‚rifie la ligne d'origine pour savoir s'il y a besoin de corriger
                        //IF ((lOriginePurchaseLine."Outstanding Quantity" - pWarehouseReceiptLine."Qty. to Receive") >= 0) AND //r‚ception partielle
                        IF (lOriginePurchaseLine."Direct Unit Cost" = pPrixBrut) AND
                           (lOriginePurchaseLine."Discount1 %" = pRemise1) AND
                           (lOriginePurchaseLine."Discount2 %" = pRemise2) THEN
                            EXIT;

                        //IF ((lOriginePurchaseLine."Outstanding Quantity" - pWarehouseReceiptLine."Qty. to Receive") = 0) THEN BEGIN
                        IF (lOriginePurchaseLine."Quantity Received" = 0) THEN BEGIN
                            // On modifie la ligne d'origine
                            IF NOT CONFIRM('Valider la modification des coûts/remises sur la ligne achat ?') THEN
                                ERROR('');
                            //Ouvre la commande pour faire la modification
                            lReleasePurchaseDocument.Reopen(lPurchaseheader);

                            lOriginePurchaseLine.HideValidationFromFac(TRUE); // Ne pas ‚craser les info d'origines
                            lOriginePurchaseLine.VALIDATE("Direct Unit Cost", pPrixBrut);
                            lOriginePurchaseLine.VALIDATE("Discount1 %", pRemise1);
                            lOriginePurchaseLine.VALIDATE("Discount2 %", pRemise2);
                            lOriginePurchaseLine.MODIFY();
                        END
                        ELSE BEGIN
                            // On modifie la ligne d'origine
                            IF NOT CONFIRM('Valider la modification des coûts/remises sur la ligne achat et la création d''une nouvelle ligne pour le solde ?') THEN
                                ERROR('');
                            //Ouvre la commande pour faire la modification
                            lReleasePurchaseDocument.Reopen(lPurchaseheader);

                            //On r‚cupŠre le prochain num‚ro de ligne de commande
                            lPurchaseLine.RESET();
                            lPurchaseLine.SETRANGE("Document Type", lPurchaseLine."Document Type"::Order);
                            lPurchaseLine.SETRANGE("Document No.", "Source No.");
                            lPurchaseLine.FINDLAST();
                            lLineNo := lPurchaseLine."Line No." + 10000;

                            // Copie de la ligne d'origine commande dans la nouvelle ligne avec quantit‚s restantes de la
                            CLEAR(lPurchaseLine);
                            lPurchaseLine.INIT();
                            lPurchaseLine.TRANSFERFIELDS(lOriginePurchaseLine);
                            lPurchaseLine."Line No." := lLineNo;
                            lPurchaseLine.Quantity := lOriginePurchaseLine."Outstanding Quantity";
                            lPurchaseLine."Quantity (Base)" := lOriginePurchaseLine."Outstanding Qty. (Base)";
                            lPurchaseLine."Quantity Received" := 0;
                            lPurchaseLine."Qty. Received (Base)" := 0;
                            lPurchaseLine."Quantity Invoiced" := 0;
                            lPurchaseLine."Qty. Invoiced (Base)" := 0;
                            lPurchaseLine.InitOutstanding();
                            lPurchaseLine.InitQtyToReceive();
                            lPurchaseLine.HideValidationFromFac(TRUE); // Ne pas ‚craser les info d'origines
                            lPurchaseLine.VALIDATE("Direct Unit Cost", pPrixBrut);
                            lPurchaseLine.VALIDATE("Discount1 %", pRemise1);
                            lPurchaseLine.VALIDATE("Discount2 %", pRemise2);
                            lPurchaseLine.INSERT();

                            // On modifie la ligne de commande d'origine (Qt‚) et on recalcul les quantit‚s/montants
                            lOriginePurchaseLine.Quantity := lOriginePurchaseLine."Quantity Received";
                            lOriginePurchaseLine."Quantity (Base)" := lOriginePurchaseLine."Qty. Received (Base)";
                            lOriginePurchaseLine.InitOutstanding();
                            lOriginePurchaseLine.InitQtyToReceive();
                            lOriginePurchaseLine.VALIDATE("Direct Unit Cost");
                            lOriginePurchaseLine.MODIFY();

                            // On modifie la ligne de r‚ception d'origine (Qt‚ et liaison avec ligne de commande)
                            pWarehouseReceiptLine."Source Line No." := lLineNo;

                            lQtyToReceive := pWarehouseReceiptLine."Qty. to Receive";
                            lQteRecuetheorique := pWarehouseReceiptLine."Qte Reçue théorique";
                            pWarehouseReceiptLine.VALIDATE("Qty. to Receive", 0);
                            pWarehouseReceiptLine.VALIDATE("Qty. Received", 0);
                            pWarehouseReceiptLine.VALIDATE("Qty. Received (Base)", 0);

                            pWarehouseReceiptLine.VALIDATE(Quantity, lPurchaseLine.Quantity);
                            pWarehouseReceiptLine.VALIDATE("Qty. (Base)", lPurchaseLine."Quantity (Base)");
                            pWarehouseReceiptLine.VALIDATE("Qty. Outstanding", pWarehouseReceiptLine.Quantity);
                            pWarehouseReceiptLine.VALIDATE("Qty. Outstanding (Base)", pWarehouseReceiptLine."Qty. (Base)");
                            pWarehouseReceiptLine.VALIDATE("Qty. to Receive", lQtyToReceive);
                            pWarehouseReceiptLine."Qte Reçue théorique" := lQteRecuetheorique;
                            pWarehouseReceiptLine.MODIFY();
                        END;

                        //Lance la commande
                        lReleasePurchaseDocument.RUN(lPurchaseheader);
                    END;
            END;
        END;
    END;
}

