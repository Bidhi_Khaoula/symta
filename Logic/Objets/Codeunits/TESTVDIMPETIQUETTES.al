codeunit 50065 "TEST VD IMP ETIQUETTES"
{

    trigger OnRun()
    begin
        //AJOUT d'un séparateur pour lisibilité du casier

        _Emplacement := 'M013001';

        IF STRPOS('0123456789', COPYSTR(_Emplacement, 1, 1)) > 0 THEN BEGIN
            IF STRPOS(_Emplacement, 'T') > 0 THEN
                _Emplacement := INSSTR(_Emplacement, ' ', STRPOS(_Emplacement, 'T'))
            ELSE
                IF STRPOS('ABCDEFGHIJKLMNOPQRSTUVWXYZ', COPYSTR(_Emplacement, (STRLEN(_Emplacement)), 1)) > 0 THEN
                    _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 3)
                ELSE
                    IF COPYSTR(_Emplacement, (STRLEN(_Emplacement) - 2), 1) = '$' THEN
                        _Emplacement := _Emplacement
                    ELSE
                        IF COPYSTR(_Emplacement, (STRLEN(_Emplacement) - 2), 1) = '&' THEN
                            _Emplacement := _Emplacement
                        ELSE
                            IF STRLEN(_Emplacement) = 5 THEN
                                _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 2)
                            ELSE
                                IF STRLEN(_Emplacement) = 6 THEN
                                    _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 3)

        END

        ELSE IF STRPOS('M0', COPYSTR(_Emplacement, 1, 2)) > 0 THEN BEGIN

            IF STRPOS('12345678', COPYSTR(_Emplacement, 3, 1)) > 0 THEN
                _Emplacement := INSSTR(_Emplacement, '-', 4);
            _Emplacement := INSSTR(_Emplacement, ' ', 9);
        END

        ELSE 
            _Emplacement := _Emplacement;

          MESSAGE(FORMAT(_Emplacement));
    end;

    var
        _Emplacement: Text;
}

