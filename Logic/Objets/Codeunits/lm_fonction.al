codeunit 50056 lm_fonction
{
    // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
    // CFR Le 28/09/2023 => R‚gie : fonction remise_cli_max_sub() ne pas tenir compte du [Code vendeur] = 19 >> tarifs exports
    trigger OnRun()
    begin
    end;

    var
        rec_item: Record Item;
        rec_saleslinedisc: Record "Sales Line Discount";
        rec_custom: Record Customer;
        rec_salesprice: Record "Sales Price";
     

    procedure remise_cli_max_sub(var rem_cod: Text[30]; var rem_max: Decimal)
    begin
        IF rec_saleslinedisc.FINDFIRST() THEN
            REPEAT
                IF rec_saleslinedisc."Line Discount %" > rem_max THEN BEGIN
                    rec_custom.SETRANGE("Customer Disc. Group", rec_saleslinedisc."Sales Code");
                    // CFR Le 28/09/2023 => R‚gie : fonction remise_cli_max_sub() ne pas tenir compte du [Code vendeur] = 19 >> tarifs exports
                    rec_custom.SETFILTER("Salesperson Code", '<>%1', '19');
                    // FIN CFR Le 28/09/2023
                    IF rec_custom.COUNT > 0 THEN BEGIN
                        rem_max := rec_saleslinedisc."Line Discount %";
                        rem_cod := rec_saleslinedisc."Sales Code";
                    END
                END
            UNTIL rec_saleslinedisc.NEXT() = 0;
        EXIT
    end;

    procedure remise_cli_max(cod_art: Code[20]; var rem_cod: Text[30]; var rem_max: Decimal)
    begin
        rem_max := 0;
        rem_cod := '';

        IF NOT rec_item.GET(cod_art) THEN EXIT;

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", rec_item."No.");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", '');
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());

        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", rec_item."Manufacturer Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", rec_item."Item Category Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", rec_item."Item Category Code");
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());

        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", rec_item."Item Category Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", rec_item."Item Category Code");
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());

        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", rec_item."Item Category Code");
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());

        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", rec_item."Manufacturer Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", rec_item."Item Category Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", '');
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());

        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", rec_item."Item Category Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", '');
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());


        remise_cli_max_sub(rem_cod, rem_max);

        rec_saleslinedisc.RESET();
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Article", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Marque", rec_item."Manufacturer Code");
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Famille", '');
        rec_saleslinedisc.SETRANGE(rec_saleslinedisc."Code Sous Famille", '');
        // CFR Le 27/09/2023 => R‚gie : fonction remise_cli_max() gestion des dates d‚but/fin
        rec_saleslinedisc.SETFILTER("Starting Date", '<= %1', WORKDATE());
        rec_saleslinedisc.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
        remise_cli_max_sub(rem_cod, rem_max);

        EXIT
    end;

    procedure prix_vente(cod_art: Code[20]; var prix_vente: Decimal; var _pPxNet: Boolean)
    begin
        rec_salesprice.RESET();
        rec_salesprice.SETRANGE(rec_salesprice."Item No.", cod_art);
        rec_salesprice.SETRANGE(rec_salesprice."Sales Code", '');
        rec_salesprice.SETRANGE(rec_salesprice."Type de commande", 0);
        rec_salesprice.SETRANGE(rec_salesprice."Sales Type", 4);
        rec_salesprice.SETFILTER(rec_salesprice."Starting Date", '<= %1', WORKDATE());
        rec_salesprice.SETFILTER(rec_salesprice."Ending Date", '%1|>=%2', 0D, WORKDATE());

        prix_vente := 0;
        IF rec_salesprice.FINDFIRST() THEN BEGIN
            prix_vente := rec_salesprice."Prix catalogue" * rec_salesprice.Coefficient;
            _pPxNet := NOT rec_salesprice."Allow Line Disc.";// AD Le 06-02-2015
        END;

        EXIT;
    end;

    procedure user_appro_entree() val: Boolean
    var
       
        rec_user_appro: Record "user appro";
        PurchSetup: Record "Purchases & Payables Setup";
         liste_user: Text[250];
    begin
        // MCO Le 01-04-2015 => Régie
        PurchSetup.GET();
        // FIN MCO Le 01-04-2015 => Régie



        liste_user := 'Utilisateurs en cours appro: ';
        val := TRUE;
        // MCO Le 01-04-2015 => Régie
        // Ajout de la condition une seule réappro
        IF (rec_user_appro.FINDFIRST() AND PurchSetup."Une seule réappro") THEN
        // FIN MCO Le 01-04-2015 => Régie
          BEGIN
            REPEAT
                liste_user := liste_user + ' ' + rec_user_appro.User;
            UNTIL rec_user_appro.NEXT() = 0;
            MESSAGE(liste_user);
            // MCO Le 21-11-2018
            IF NOT GUIALLOWED THEN BEGIN
                ERROR('Une appro est déjà en cours');
                EXIT;
            END;
            // FIN MCO Le 21-11-2018
            IF CONFIRM('Voulez-vous quand meme lancer le traitement ?') THEN BEGIN
                IF NOT rec_user_appro.GET(USERID) THEN BEGIN
                    rec_user_appro.INIT();
                    rec_user_appro.User := USERID;
                    rec_user_appro.Insert();
                END;
            END
            ELSE
                val := FALSE;
        END
        ELSE BEGIN
            IF NOT rec_user_appro.GET(USERID) THEN BEGIN
                rec_user_appro.INIT();
                rec_user_appro.User := USERID;
                IF NOT rec_user_appro.INSERT() THEN MESSAGE('erreur insert');
            END;
        END;

        EXIT
    end;

    procedure user_appro_sortie()
    var
        rec_user_appro: Record "user appro";
    begin
        IF rec_user_appro.GET(USERID) THEN BEGIN
            rec_user_appro.INIT();
            rec_user_appro.User := USERID;
            IF NOT rec_user_appro.DELETE() THEN MESSAGE('erreur delete');
        END;
    end;

    procedure encours_insert(no_devis: Code[20]) val: Boolean
    var
        rec_salesline: Record "Sales Line";
        cpt1: Integer;
        cpt2: Integer;
    begin
        val := FALSE;

        rec_salesline.RESET();
        rec_salesline.SETRANGE("Document No.", no_devis);
        cpt1 := rec_salesline.COUNT;

        SLEEP(3000);

        rec_salesline.RESET();
        rec_salesline.SETFILTER("Document No.", no_devis);
        cpt2 := rec_salesline.COUNT;

        IF cpt2 <> cpt1 THEN BEGIN
            MESSAGE('Ligne en cours insertion, réessayer plus tard');
            val := TRUE;
        END;

        EXIT
    end;

    procedure dat_der_commande_client(cod_art: Code[20]) dat_der_cde: Date
    var
        rec_saleslinearch: Record "Sales Line Archive";
        rec_salesheaderarch: Record "Sales Header Archive";
        rec_salesline: Record "Sales Line";
        rec_salesheader: Record "Sales Header";
    begin
        dat_der_cde := 0D;

        rec_salesline.RESET();
        rec_salesline.SETCURRENTKEY("Document Type", Type, "No.", "Outstanding Quantity"); // AD Le 04-03-2016

        rec_salesline.SETRANGE("Document Type", rec_salesline."Document Type"::Order);
        rec_salesline.SETRANGE(Type, rec_salesline.Type::Item);
        rec_salesline.SETRANGE("No.", cod_art);
        IF rec_salesline.FINDLAST() THEN BEGIN
            IF rec_salesheader.GET(rec_salesline."Document Type", rec_salesline."Document No.") THEN
                dat_der_cde := rec_salesheader."Order Date";
            // MESSAGE(FORMAT(dat_der_cde,0,'<Month Text> <Day>'));
        END
        ELSE BEGIN
            rec_saleslinearch.RESET();
            rec_saleslinearch.SETCURRENTKEY("Document Type", Type, "No."); // AD Le 04-03-2016
            rec_saleslinearch.SETRANGE("Document Type", rec_saleslinearch."Document Type"::Order);
            rec_saleslinearch.SETRANGE(Type, rec_saleslinearch.Type::Item);
            rec_saleslinearch.SETRANGE("No.", cod_art);
            IF rec_saleslinearch.FINDLAST() THEN BEGIN
                // IF rec_salesheaderarch.GET(rec_saleslinearch."Document Type", rec_saleslinearch."Document No.") THEN
                // AD Le 04-03-2016
                //rec_salesheaderarch.SETRANGE("Document Type", rec_saleslinearch."Document Type");
                //rec_salesheaderarch.SETRANGE("No.", rec_saleslinearch."Document No.");
                rec_salesheaderarch.GET(rec_saleslinearch."Document Type", rec_saleslinearch."Document No.",
                      rec_saleslinearch."Doc. No. Occurrence", rec_saleslinearch."Version No.");
                // FIN AD Le 04-03-2016
                // MCO Le 07-06-2017 => Régie : Mise en commentaire
                //IF rec_salesheaderarch.FINDLAST THEN
                // FIN MCO LE 07-06-2017 => Régie
                dat_der_cde := rec_salesheaderarch."Order Date";

                //MESSAGE(FORMAT(rec_saleslinearch."Document Type") + '- ' + FORMAT(rec_saleslinearch."Document No.")
                // + '- ' + FORMAT(rec_salesheaderarch."No.") );
                //MESSAGE(FORMAT(dat_der_cde,0,'<Month Text> <Day>'));

            END;
        END;
        EXIT
    end;

    procedure prix_net_achat(c_article: Code[20]; c_four: Code[20]; type_cde: Integer) px_net: Decimal
    var
        rec_purch_price: Record "Purchase Price";
        rec_grp_rem_fou: Record "Groupe Remises Fournisseurs";
        rec_vendor: Record Vendor;
        rec_item_vendor: Record "Item Vendor";
        rec_devise: Record "Currency Exchange Rate";
        devise: Decimal;
        rec_unit_mesure: Record "Item Unit of Measure";
        unit_mesure: Decimal;
    begin

        // prix achat
        rec_purch_price.RESET();
        rec_purch_price.SETRANGE(rec_purch_price."Item No.", c_article);
        rec_purch_price.SETRANGE(rec_purch_price."Vendor No.", c_four);
        rec_purch_price.SETFILTER(rec_purch_price."Starting Date", '%1|<=%2', 0D, WORKDATE());
        rec_purch_price.SETFILTER(rec_purch_price."Ending Date", '%1|>=%2', 0D, WORKDATE());

        IF rec_purch_price.FINDFIRST() THEN;

        // fournisseur
        IF rec_vendor.GET(c_four) THEN;

        // article fournisseur
        rec_item_vendor.RESET();
        rec_item_vendor.SETRANGE(rec_item_vendor."Vendor No.", c_four);
        rec_item_vendor.SETRANGE(rec_item_vendor."Item No.", c_article);
        IF rec_item_vendor.FINDFIRST() THEN;

        // remise
        rec_grp_rem_fou.RESET();
        rec_grp_rem_fou.SETRANGE(rec_grp_rem_fou."Vendor Disc. Group", rec_vendor."Vendor Disc. Group");
        rec_grp_rem_fou.SETRANGE(rec_grp_rem_fou."Discount Code", rec_item_vendor."Code Remise");
        rec_grp_rem_fou.SETFILTER(rec_grp_rem_fou."Starting Date", '%1|<=%2', 0D, WORKDATE());
        rec_grp_rem_fou.SETFILTER(rec_grp_rem_fou."Ending Date", '%1|>=%2', 0D, WORKDATE());
        rec_grp_rem_fou.SETRANGE(rec_grp_rem_fou."Type de commande", type_cde);
        IF rec_grp_rem_fou.FINDFIRST() THEN;


        //devise
        rec_devise.RESET();
        rec_devise.SETRANGE(rec_devise."Currency Code", rec_vendor."Currency Code");
        rec_devise.SETFILTER(rec_devise."Starting Date", '%1|<=%2', 0D, WORKDATE());
        IF rec_devise.FINDLAST() THEN
            devise := rec_devise."Relational Adjmt Exch Rate Amt"
        ELSE
            devise := 1;

        // unite de mesure
        rec_unit_mesure.RESET();
        rec_unit_mesure.SETRANGE(rec_unit_mesure."Item No.", c_article);
        rec_unit_mesure.SETRANGE(rec_unit_mesure.Code, rec_purch_price."Unit of Measure Code");
        IF rec_unit_mesure.FINDFIRST() THEN
            unit_mesure := rec_unit_mesure."Qty. per Unit of Measure"
        ELSE
            unit_mesure := 1;

        px_net := rec_purch_price."Direct Unit Cost" * (100 - rec_grp_rem_fou."Line Discount %") / 100 * devise / unit_mesure;
    end;

    procedure coef_vente(cod_art: Code[20]; var Pcoef_vente: Decimal)
    begin
        rec_salesprice.RESET();
        rec_salesprice.SETRANGE(rec_salesprice."Item No.", cod_art);
        rec_salesprice.SETRANGE(rec_salesprice."Sales Code", '');
        rec_salesprice.SETRANGE(rec_salesprice."Type de commande", 0);
        rec_salesprice.SETRANGE(rec_salesprice."Sales Type", 4);
        rec_salesprice.SETFILTER(rec_salesprice."Starting Date", '<= %1', WORKDATE());
        rec_salesprice.SETFILTER(rec_salesprice."Ending Date", '%1|>=%2', 0D, WORKDATE());

        Pcoef_vente := 0;
        IF rec_salesprice.FINDFIRST() THEN
            Pcoef_vente := rec_salesprice.Coefficient;

        EXIT
    end;
}

