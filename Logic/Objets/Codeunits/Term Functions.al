codeunit 50070 "Term Functions"
{

    trigger OnRun()
    begin
    end;

    var
        TempItemJnlLine: Record "Item Journal Line" temporary;
        GeneralsParameters: Record "Generals Parameters";
               ItemJnlMgt: Codeunit ItemJnlManagement; 
        NewBinCode: Code[20];       
        
        BinContentCapacity: Decimal;
         CurrentJnlBatchName: Code[10];

        Text001Err: Label 'Veuillez créer une feuille de reclassement %1.', Comment = '%1 = Article';
        //ESK005: Label 'Article %1 Inexistant !';
        ESK001Err: Label 'Quantité Incorrecte';
        ESK002Err: Label 'Format Obligatoire';
        ESK003Err: Label 'Imprimante Obligatoire';
        ESK004Err: Label 'Rangement dans le même casier !';
        ESK006Err: Label 'Type empl. obligatoire';
        ESK009Err: Label 'Veuillez d''abord choisir un article.';
       // ESK010Qst: Label 'Voulez vous modifier le poids de l''article à %1 ?', Comment = '';
        InfoMaskLbl: Label '%1:%2', Comment = '%1 = Qty Article ; %2 =Qty Ordonée';
        QtyOnSalesOrderLbl: Label 'Qty. on Sales Order';
        

    procedure Login(pUser: Text[50]; pLocation: Code[20]): Boolean
    var
        LWhseEmployee: Record "Warehouse Employee";
    begin
        /// Authentification

        WITH LWhseEmployee DO 
            IF GET(pUser, pLocation) OR GET(STRSUBSTNO('SYMTA\%1', pUser), pLocation) THEN
                EXIT(TRUE);
         EXIT(FALSE);
    end;

    procedure ValidateEmplSrc(pLocation: Code[20]; pItemNo: Code[20]; var pBinCode: Code[20]; var pCapacity: Decimal; var pNewBinCode: Code[20]; var pQty: Decimal; var pCodeZone: Code[10]): Boolean
    var
        // LBinContent: Record "Bin Content";
                 lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lItemNo: Code[30];
    begin
        pCapacity := 0;
        pQty := 0;

        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        // on test d'abords l'existance de l'emplacement
        IF NOT BinContentExists(pLocation, lItemNo, pBinCode, pCodeZone) THEN EXIT(FALSE);

        InitMvt(pLocation, lItemNo, pBinCode);
        pCapacity := BinContentCapacity;
        pBinCode := TempItemJnlLine."Bin Code";
        pQty := TempItemJnlLine.Quantity;
        pNewBinCode := NewBinCode;

        EXIT(TRUE);
    end;

    procedure ValidateItem(var pItemNo: Code[20]; var pItemNo2: Code[20]; var Description: Text[30]): Boolean
    var
        LItem: Record Item;
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lItemNo: Code[30];
    begin
        /// Validation de l'existance d'un article avec retour de la designation
        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        IF lItemNo <> 'RIENTROUVE' THEN
            IF LItem.GET(lItemNo) THEN BEGIN
                pItemNo := LItem."No.";
                pItemNo2 := LItem."No. 2";
                Description := LItem.Description;
                EXIT(TRUE);
            END;

        EXIT(FALSE);
    end;

    procedure ValidateEmpl(pLocation: Code[20]; pBinCode: Code[20]; pItemNo: Code[20]; var pCodeZone: Code[10]; var pCapacity: Decimal; var pConfirm: Text[250]): Boolean
    var
        //LBin: Record Bin;
       // LBinContent: Record "Bin Content";
        
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lItemNo: Code[30];
    begin
        pCapacity := 0;
        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        //IF NOT BinContentExists(pLocation, lItemNo, pBinCode, pCodeZone) THEN EXIT(FALSE);

        InitMvt(pLocation, lItemNo, pBinCode);
        pCapacity := BinContentCapacity;

        EXIT(TRUE);
        /*
        /// Validation que l'emplacement existe bien
        IF LBin.GET(pLocation, pBinCode) THEN BEGIN
          IF pItemNo <> '' THEN BEGIN
          {
            IF LBin."Type Emplacement" = LBin."Type Emplacement"::Picking THEN
              WITH LBinContent DO BEGIN
                RESET();
                SETRANGE("Location Code", pLocation);
                SETRANGE("Bin Code", LBin.Code);
                SETRANGE("Item No.", pItem);
                SETRANGE(Fixed, TRUE);
                SETRANGE(Default, TRUE);
                IF ISEMPTY THEN BEGIN
                  pConfirm := 'L''emplacement n''est pas le picking de cet article. Continuer ?';
                END;
            END;
            }
          END;
          EXIT(TRUE);
        END ELSE BEGIN
          EXIT(FALSE);
        END;
        */

    end;

    procedure CreateMvtEmpl(pDocNo: Code[20]; pLocation: Code[20]; pItemNo: Code[20]; pEmpl: Code[20]; pPsmId: Code[20]; pEmplSrc: Code[20]; pQty: Decimal; pType: Option Defaut,Surstock; pComment: Text[40]): Boolean
    var
     LLigneVente: Record "Sales Line";
        LLigneBP: Record "Warehouse Shipment Line";
        LLigneAchat: Record "Purchase Line";
        LLigneBR: Record "Warehouse Receipt Line";
         LItemJnlLine: Record "Item Journal Line";
      
         LSourceCodeSetup: Record "Source Code Setup";
                 lItemJnlBatch: Record "Item Journal Batch";
                  LBinContent: Record "Bin Content";
        //lBinCont: Record "Bin Content";
       //  LBinSrc: Record Bin;
        LBinDest: Record Bin;
        //LItem: Code[20];
      //  LQty: Integer;
       
       // WhseJnlPostLine: Codeunit "Whse. Jnl.-Register Line";
       
        
       // WMSMngt: Codeunit "WMS Management";
         ItemJnlPost: Codeunit "Item Jnl.-Post";
        CduLFunctions: Codeunit "Codeunits Functions";
        LOldDefaultBinContent: Boolean;
       
      
        
        JnlSelected: Boolean;
        "lCapacité": Decimal;
        lUnit: Code[10];

        lSeuil: Decimal;
       
    begin
        /// Création d'un mouvement magasin
        LSourceCodeSetup.GET();
        IF (pEmpl = pEmplSrc) THEN
            ERROR(ESK004Err);
        IF (pType = -1) THEN
            ERROR(ESK006Err);

        ItemJnlMgt.TemplateSelection(PAGE::"Item Reclass. Journal", 1, FALSE, LItemJnlLine, JnlSelected);
        IF NOT JnlSelected THEN ERROR('');
        CurrentJnlBatchName := pPsmId;
        IF NOT lItemJnlBatch.GET('RECLASS', CurrentJnlBatchName) THEN BEGIN
            lItemJnlBatch.INIT();
            lItemJnlBatch."Journal Template Name" := 'RECLASS';
            lItemJnlBatch.SetupNewBatch();
            lItemJnlBatch.PSM := TRUE;
            lItemJnlBatch.Name := CurrentJnlBatchName;
            lItemJnlBatch.Description := '';
            lItemJnlBatch.INSERT(TRUE);
            COMMIT();
        END;
        CurrentJnlBatchName := lItemJnlBatch.Name;
        ItemJnlMgt.OpenJnl(CurrentJnlBatchName, LItemJnlLine);
        IF CurrentJnlBatchName <> pPsmId THEN ERROR(Text001Err, pPsmId);

        IF pType = pType::Defaut THEN BEGIN
            WITH LBinContent DO BEGIN
                // On regarde si le casier source etait le casier par defaut de l'article
                CLEAR(LBinContent);
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE("Bin Code", pEmplSrc);
                FINDFIRST();
                LOldDefaultBinContent := Default;

                // recherche du casier actuel
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE(Default, TRUE);
                IF FINDFIRST() THEN BEGIN
                    Fixed := TRUE;
                    Default := FALSE;
                    lCapacité := capacité;
                    lSeuil := "% Seuil Capacité Réap. Pick."; // MCO Le 04-10-2018 => Régie
                    lUnit := "Unit of Measure Code";
                    MODIFY();
                END;

                //affectation du casier par defaut
                RESET();
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE("Bin Code", pEmpl);
                IF FINDFIRST() THEN BEGIN
                    Fixed := TRUE;
                    Default := TRUE;
                    // MCO Le 16-01-2018
                    IF LOldDefaultBinContent THEN BEGIN
                        // FIN MCO Le 16-01-2018
                        capacité := lCapacité;

                        "% Seuil Capacité Réap. Pick." := lSeuil; // MCO Le 04-10-2018 => Régie
                    END;
                    MODIFY();
                END ELSE BEGIN
                    RESET();
                    INIT();
                    VALIDATE("Location Code", pLocation);
                    VALIDATE("Item No.", pItemNo);
                    VALIDATE("Unit of Measure Code", lUnit);
                    VALIDATE("Bin Code", pEmpl);

                    LBinDest.GET(pLocation, pEmpl);

                    VALIDATE("Zone Code", LBinDest."Zone Code");
                    Fixed := TRUE;
                    Default := TRUE;
                    // MCO Le 16-01-2018
                    IF LOldDefaultBinContent THEN BEGIN
                        // FIN MCO Le 16-01-2018
                        capacité := lCapacité;
                        "% Seuil Capacité Réap. Pick." := lSeuil; // MCO Le 04-10-2018 => Régie
                    END;
                    INSERT(TRUE);
                END;
            END;

            // Si le casier source était le casier par defaut, on va le modifier sur les transfert
            IF LOldDefaultBinContent THEN BEGIN
                CLEAR(LItemJnlLine);
                LItemJnlLine.SETRANGE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
                LItemJnlLine.SETRANGE("Item No.", pItemNo);
                LItemJnlLine.SETRANGE("New Location Code", pLocation);
                LItemJnlLine.SETRANGE("New Bin Code", pEmplSrc);
                LItemJnlLine.MODIFYALL("New Bin Code", pEmpl);
            END;

            LLigneVente.SETRANGE(Type, LLigneVente.Type::Item);
            LLigneVente.SETRANGE("No.", pItemNo);
            LLigneVente.SETRANGE("Bin Code", pEmplSrc);
            LLigneVente.MODIFYALL("Bin Code", pEmpl);

            LLigneBP.SETRANGE("Item No.", pItemNo);
            LLigneBP.SETRANGE("Bin Code", pEmplSrc);
            LLigneBP.MODIFYALL("Bin Code", pEmpl);

            // achat
            LLigneAchat.SETRANGE(Type, LLigneAchat.Type::Item);
            LLigneAchat.SETRANGE("No.", pItemNo);
            LLigneAchat.SETRANGE("Bin Code", pEmplSrc);
            LLigneAchat.MODIFYALL("Bin Code", pEmpl);

            LLigneBR.SETRANGE("Item No.", pItemNo);
            LLigneBR.SETRANGE("Bin Code", pEmplSrc);
            LLigneBR.MODIFYALL("Bin Code", pEmpl);

        END;

        IF pQty <> 0 THEN BEGIN // ANI Le 07-09-2017 Ticket 24541 Régie
                                // section code avec reclassement par transfert
            WITH LItemJnlLine DO BEGIN
                CLEAR(LItemJnlLine);
                INIT();
                VALIDATE("Journal Template Name", 'RECLASS');
                VALIDATE("Entry Type", "Entry Type"::Transfer);
                VALIDATE("Journal Batch Name", CurrentJnlBatchName);

                VALIDATE("Posting Date", WORKDATE());
                VALIDATE("Document No.", USERID);
                VALIDATE("A Valider", TRUE);   // ???
                VALIDATE("Item No.", pItemNo);

                // emplacement source
                VALIDATE("Location Code", pLocation);
                VALIDATE("Bin Code", pEmplSrc);
                // emplacement de destination
                VALIDATE("New Location Code", pLocation);
                VALIDATE("New Bin Code", pEmpl);

                LBinDest.GET("Location Code", "Bin Code");

                VALIDATE(Quantity, pQty);
                VALIDATE(Commentaire, pComment);
                INSERT(TRUE);
            END;
            CduLFunctions.SetHideValidationDialog(TRUE);
            ItemJnlPost.RUN(LItemJnlLine);

        END;

        EXIT(TRUE);
    end;

    local procedure GetSpecReceiptBinCode(pLocation: Code[20]): Code[20]
    var
       // LLocation: Record Location;
    begin
        /// Lecture de l'emplacement QUAI d'un magasin
        /*
        WITH LLocation DO BEGIN
          IF GET(pLocation) THEN
            EXIT("Spec Receipt Bin Code");
        END;
        */
        EXIT('');

    end;

    local procedure CreateWhseJnlLine(ItemJnlLine: Record "Item Journal Line"; var TempWhseJnlLine: Record "Warehouse Journal Line" temporary)
    var
      Location: Record Location;
        WhseMgt: Codeunit "Whse. Management";
        WMSMgmt: Codeunit "WMS Management";
      
    begin
        Location.GET(ItemJnlLine."Location Code");

        WMSMgmt.CheckAdjmtBin(Location, ItemJnlLine.Quantity, TRUE);
        WMSMgmt.CreateWhseJnlLine(ItemJnlLine, 0, TempWhseJnlLine, FALSE);
        TempWhseJnlLine."Source Type" := DATABASE::"Purchase Line";

        WhseMgt.GetSourceDocument(
          TempWhseJnlLine."Source Type", TempWhseJnlLine."Source Subtype");
        TempWhseJnlLine."Source No." := ItemJnlLine."Document No.";
        TempWhseJnlLine."Source Line No." := ItemJnlLine."Line No.";
        TempWhseJnlLine."Reference No." := ItemJnlLine."Document No.";
    end;

    procedure TransferPicking(pLocation: Code[20]; pEmpl: Code[20]; pItemNo: Code[20])
    begin
        // ANI Le 26-11-2015 FE20151109 - Transfert picking
        //GestionLog.TransferPicking(pLocation, pEmpl, pItemNo);
    end;

    procedure GetDefaultBin(pItemNo: Code[20]; pVariantCode: Code[10]; pLocationCode: Code[20]; var BinCode: Code[20])
    var
        LWMSMngt: Codeunit "WMS Management";
    begin
        // ANI Le 05-01-2016 FE20151109 - Transfert picking
        LWMSMngt.GetDefaultBin(pItemNo, pVariantCode, pLocationCode, BinCode);
    end;

    local procedure InitMvt(pLocation: Code[20]; pItem: Code[20]; pBinCode: Code[20])
    var
      lBinCont: Record "Bin Content";
        JnlSelected: Boolean;
          begin
        InitValue();
        // OnOpenPage
        ItemJnlMgt.TemplateSelection(PAGE::"Item Reclass. Journal", 1, FALSE, TempItemJnlLine, JnlSelected);
        IF NOT JnlSelected THEN
            ERROR('');

        ItemJnlMgt.OpenJnl(CurrentJnlBatchName, TempItemJnlLine);
        TempItemJnlLine.VALIDATE("Journal Batch Name", CurrentJnlBatchName);
        InitValue();

        // OnValidate ItemNo
        TempItemJnlLine.VALIDATE("Recherche référence", pItem);
        TempItemJnlLine.VALIDATE("Location Code", pLocation);
        TempItemJnlLine.VALIDATE("New Location Code", pLocation);
        IF pBinCode <> '' THEN 
            TempItemJnlLine.VALIDATE("Bin Code", pBinCode);
                OnAfterValidateBinCode();

        CLEAR(lBinCont);
        IF lBinCont.GET(TempItemJnlLine."Location Code", TempItemJnlLine."Bin Code", TempItemJnlLine."Item No.", '', TempItemJnlLine."Unit of Measure Code") THEN
            BinContentCapacity := lBinCont.capacité;
    end;

    local procedure InitValue()
    begin
        WITH TempItemJnlLine DO BEGIN
            VALIDATE("Journal Template Name", 'RECLASS');
            VALIDATE("Posting Date", WORKDATE());
            VALIDATE("Entry Type", "Entry Type"::Transfer);
            VALIDATE("Document No.", USERID);
        END;
    end;

    local procedure LoadQty(): Decimal
    var
        lBin: Record "Bin Content";
    begin
        WITH lBin DO BEGIN
            SETRANGE("Location Code", TempItemJnlLine."Location Code");
            SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            SETRANGE("Item No.", TempItemJnlLine."Item No.");
            SETRANGE("Qty. per Unit of Measure", TempItemJnlLine."Qty. per Unit of Measure");
            IF FINDFIRST() THEN BEGIN
                CALCFIELDS("Quantity (Base)");
                EXIT("Quantity (Base)");
            END;
        END;

        EXIT(0);
    end;

    local procedure OnAfterValidateBinCode()
    begin
        TempItemJnlLine.VALIDATE(Quantity, LoadQty());
        //NewBinCode := TempItemJnlLine."New Bin Code";
        GetDefaultBin(TempItemJnlLine."Item No.", '', TempItemJnlLine."Location Code", NewBinCode);
    end;

    local procedure BinContentExists(pLocation: Code[20]; pItemNo: Code[20]; pBinCode: Code[20]; var pCodeZone: Code[10]): Boolean
    var
        lBinContent: Record "Bin Content";
    begin
        IF pItemNo <> '' THEN BEGIN
            IF pBinCode = '' THEN
                GetDefaultBin(pItemNo, '', pLocation, pBinCode);
            WITH lBinContent DO BEGIN
                RESET();
                SETRANGE("Location Code", pLocation);
                SETRANGE("Bin Code", pBinCode);
                SETRANGE("Item No.", pItemNo);
                IF FINDFIRST() THEN BEGIN
                    pCodeZone := "Zone Code";
                    EXIT(TRUE);
                END;
            END;
        END;

        EXIT(FALSE);
    end;

    procedure ValidateNewBin(pCodeZone: Code[10]; pCodeLocation: Code[10]; pCodeEmpl: Code[20]): Boolean
    var
        lRecordBin: Record Bin;
    begin
        // Verification de l'existance de l'emplacement
        IF pCodeEmpl = '' THEN
            EXIT(CheckZoneExists(pCodeZone, pCodeLocation))

        ELSE BEGIN
            IF (NOT (CheckZoneExists(pCodeZone, pCodeLocation))) THEN
                ERROR('Code zone inconnu');


            IF lRecordBin.GET(pCodeLocation, pCodeEmpl) THEN
                EXIT(FALSE)
            ELSE
                EXIT(TRUE);
        END;
    end;

    local procedure CheckZoneExists(pCodeZone: Code[10]; pCodeLocation: Code[10]): Boolean
    var
        lRecordZone: Record Zone;
    begin
        // Verification de l'existance de la zone

        lRecordZone.RESET();

        lRecordZone.SETRANGE("Location Code", pCodeLocation);
        lRecordZone.SETRANGE(Code, pCodeZone);


        IF lRecordZone.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure CreateNewBin(pCodeZone: Code[10]; pCodeLocation: Code[10]; pCodeEmpl: Code[20]; pCodeItem: Code[10]): Boolean
    var
        lbin: Record Bin;
    begin
        // Création d'un nouvel emplacement

        IF NOT (CheckZoneExists(pCodeZone, pCodeLocation)) THEN
            ERROR('Code zone inconnu');


        IF NOT lbin.GET(pCodeLocation, pCodeEmpl) THEN BEGIN
            IF pCodeEmpl = '' THEN ERROR('Veuillez saisir un code emplacement');
            lbin.INIT();
            lbin.VALIDATE("Location Code", pCodeLocation);
            lbin.VALIDATE("Zone Code", pCodeZone);

            lbin.VALIDATE(Code, pCodeEmpl);

            IF (pCodeItem <> '') THEN
                lbin.VALIDATE("Item Filter", pCodeItem);


            lbin.INSERT(TRUE);

            EXIT(TRUE);

        END

        ELSE
            EXIT(FALSE);
    end;

    procedure ValidateItemLabel(var pItemNo: Code[20]; var pItemNo2: Code[20]; var Description: Text[30]; var QtyPer: Integer): Boolean
    var
        lItem: Record Item;
      
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
          lItemNo: Code[30];
    begin
        /// Validation de l'existance d'un article avec retour de la designation
        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        IF lItemNo <> 'RIENTROUVE' THEN
            IF lItem.GET(lItemNo) THEN BEGIN
                pItemNo := lItem."No.";
                pItemNo2 := lItem."No. 2";
                Description := lItem.Description;
                QtyPer := lItem."Sales multiple";
                IF QtyPer < 1 THEN QtyPer := 1;
                EXIT(TRUE);
            END;

        EXIT(FALSE);
    end;

    procedure ValidateFormat(pFormat: Code[10]; var pPrinter: Code[20])
    var
        lParam: Record "Generals Parameters";
        lParam2: Record "Generals Parameters";
    begin
        IF lParam.GET('NICELBL_FRM', pFormat) THEN
            IF lParam2.GET('NICELBL_IMP', lParam.LongCode) THEN
                pPrinter := lParam2.Code;
    end;

    procedure PrintLabel(pItemNo: Code[20]; pQtyPer: Integer; pQtyLbl: Integer; pFormat: Code[10]; pPrinter: Code[20]; pText: Code[20])
    var
     lItem: Record Item;
        lItemLabelMgt: Codeunit "Etiquettes Articles";
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lGestionMagasin: Codeunit "Gestion Magasin";
        lTabComment: array[3] of Text[30];
        lItemNo: Code[30];
       
    begin
        IF pQtyLbl < 1 THEN
            ERROR(ESK001Err);
        IF pFormat = '' THEN
            ERROR(ESK002Err);
        IF pPrinter = '' THEN
            ERROR(ESK003Err);

        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        IF lItemNo <> 'RIENTROUVE' THEN
            IF lItem.GET(lItemNo) THEN;

        //ERROR('> %1, %2, %3, %4, %5', lItem."No.", FormatCondit(pQtyPer), pQtyLbl, pFormat, pPrinter);
        // Impression pour un article
        IF lItem."No." <> '' THEN
            lItemLabelMgt.EditionEtiquetteUniquNiceLabel(lItem, FormatCondit(pQtyPer), pText, '', lGestionMagasin.GetCasierDefaut(lItem."No."),
                lTabComment, '', pQtyLbl, pFormat, pPrinter);
    end;

    local procedure FormatCondit(pValeur: Integer): Integer
    begin
        IF pValeur < 1 THEN pValeur := 1;
        EXIT(pValeur);
    end;

    procedure GetFormats(var Values: BigText)
    var
        CR: Char;
        LF: Char;
        CrLf: Text[2];
    begin
        CR := 13;
        LF := 10;
        CrLf[1] := CR;
        CrLf[2] := LF;

        CLEAR(Values);
        WITH GeneralsParameters DO BEGIN
            RESET();
            SETRANGE(Type, 'NICELBL_FRM');
            IF FINDSET() THEN
                REPEAT
                    IF Values.LENGTH > 0 THEN Values.ADDTEXT(CrLf);
                    Values.ADDTEXT(Code);
                UNTIL NEXT() = 0;
        END;
    end;

    procedure GetPrinters(var Values: BigText)
    var
        CR: Char;
        LF: Char;
        CrLf: Text[2];
    begin
        CR := 13;
        LF := 10;
        CrLf[1] := CR;
        CrLf[2] := LF;

        CLEAR(Values);
        WITH GeneralsParameters DO BEGIN
            RESET();
            SETFILTER(Type, 'NICELBL_IMP|IMP_ETQ_RECEPT');
            IF FINDSET() THEN
                REPEAT
                    IF Values.LENGTH > 0 THEN Values.ADDTEXT(CrLf);
                    Values.ADDTEXT(Code);
                UNTIL NEXT() = 0;
        END;
    end;

    procedure UpdateCapacity(pLocation: Code[20]; pItemNo: Code[20]; pBinCode: Code[20]; pCodeZone: Code[10]; pNewCapacity: Decimal): Boolean
    var
       lBinCont: Record "Bin Content";
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        
         lItemNo: Code[30];
    begin
        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        // on test d'abords l'existance de l'emplacement
        IF NOT BinContentExists(pLocation, lItemNo, pBinCode, pCodeZone) THEN EXIT(FALSE);

        InitMvt(pLocation, lItemNo, pBinCode);

        IF lBinCont.GET(pLocation, pBinCode, lItemNo, '', TempItemJnlLine."Unit of Measure Code") THEN BEGIN
            //ERROR('> %1 - %2', lBinCont.capacité, pNewCapacity);
            lBinCont.capacité := pNewCapacity;
            lBinCont.MODIFY();
        END;

        EXIT(TRUE);
    end;

    procedure GetInfoItem(pItemNo: Code[20]; var _info: BigText; var _conso: BigText): Boolean
    var
       
        lItem: Record Item;
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lTabValeur: array[5, 12] of Decimal;
        lTabHeader: array[5] of Text[20];
        lTabValue: array[5] of Text;
        lLabel: Text;
         lItemNo: Code[20];
        i: Integer;
        j: Integer;
        k: Integer;
        lTotal: Decimal;
    begin
        GLOBALLANGUAGE(1036);
        CLEAR(_info);
        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);
        IF NOT lItem.GET(lItemNo) THEN EXIT(FALSE);
        // Quantité
        lItem.CALCFIELDS("Qty. on Sales Order", "Qty. on Purch. Order");
        AddLineToBigText(_info, STRSUBSTNO(InfoMaskLbl, QtyOnSalesOrderLbl, FormatDecimal(lItem."Qty. on Sales Order", '')));
        //AddLineToBigText(_info,STRSUBSTNO(InfoMask,QtyOnPurchOrderLbl,FormatDecimal(lItem."Qty. on Purch. Order",'')));

        // Sorties mensuelles
        CalculerSortiesMens(lItem."No.", WORKDATE(), lTabValeur, lTabHeader);

        // ligne d'entete
        AddLineToBigText(_conso, '|' + Implode('|', lTabHeader));
        // lignes valeurs (4 trim. + total)
        FOR i := 1 TO 5 DO BEGIN
            CLEAR(lTabValue);
            j := 0;
            IF i < 5 THEN BEGIN
                lLabel := 'T' + FORMAT(i);
                REPEAT
                    j += 1;
                    lTabValue[j] := FormatDecimal(lTabValeur[j, (3 * (i - 1)) + 1] + lTabValeur[j, (3 * (i - 1)) + 2] + lTabValeur[j, (3 * (i - 1)) + 3], '');
                UNTIL j > ARRAYLEN(lTabHeader) - 1;
            END ELSE BEGIN
                lLabel := 'Total';
                REPEAT
                    j += 1;
                    lTotal := 0;
                    FOR k := 1 TO 12 DO
                        lTotal += lTabValeur[j, k];
                    lTabValue[j] := FormatDecimal(lTotal, '');
                UNTIL j > ARRAYLEN(lTabHeader) - 1;
            END;
            AddLineToBigText(_conso, lLabel + '|' + Implode('|', lTabValue));
        END;

        EXIT(TRUE);
    end;

    local procedure AddLineToBigText(var _BigText: BigText; pText: Text[1024])
    var
        lLF: Char;
        lCR: Char;
    begin
        lLF := 10;
        lCR := 13;
        IF _BigText.LENGTH > 0 THEN
            pText := FORMAT(lCR, 0, '<CHAR>') + FORMAT(lLF, 0, '<CHAR>') + pText;
        _BigText.ADDTEXT(pText);
    end;

    local procedure FormatDecimal(pValue: Decimal; pPrecision: Text): Text[50]
    begin
        IF pPrecision = '' THEN pPrecision := ':2';
        EXIT(FORMAT(pValue, 0, '<Precision,' + pPrecision + '><Sign><Integer><Decimals><Comma,,>'));
    end;

    local procedure CalculerSortiesMens(pItemNo: Code[20]; pDateBase: Date; var TabValeur: array[5, 12] of Decimal; var TabHeader: array[5] of Text[20])
    var  
    lCalculConso: Codeunit CalculConso;
        lDateDeb: Date;
        "lAnnée": Integer;
       // lMois: Integer;
       // "lNbAnnée": Integer;
        "lNbMoisAnnéeN": Integer;
        "lMoyenneAnnée": array[5] of Decimal;
        lStkDispo: array[5] of Decimal;
       // lTabValeurTot: array[5] of Decimal;
        lDispo: Decimal;
    begin
        CLEAR(lMoyenneAnnée);
        lNbMoisAnnéeN := DATE2DMY(pDateBase, 2) - 1; // On compte pas le mois en cours

        // On part du 1er janvier de l'année N-5
        lDateDeb := DMY2DATE(1, 1, DATE2DMY(pDateBase, 3) - 4);

        lCalculConso.CalculerConsoSur4ans(pItemNo, TabValeur, lMoyenneAnnée, WORKDATE());


        IF lNbMoisAnnéeN <> 0 THEN lMoyenneAnnée[1] := lMoyenneAnnée[1] / lNbMoisAnnéeN;
        lMoyenneAnnée[4] := lMoyenneAnnée[4] / 12;
        lMoyenneAnnée[3] := lMoyenneAnnée[3] / 12;
        lMoyenneAnnée[2] := lMoyenneAnnée[2] / 12;
        lMoyenneAnnée[5] := lMoyenneAnnée[5] / 12;

        FOR lAnnée := 1 TO 5 DO BEGIN
            TabHeader[lAnnée] := FORMAT(DATE2DMY(pDateBase, 3) - (lAnnée - 1));
            IF lMoyenneAnnée[lAnnée] <> 0 THEN
                lStkDispo[lAnnée] := lDispo / lMoyenneAnnée[lAnnée];
        END;
    end;

    local procedure Implode(glue: Text[5]; pieces: array[99] of Text) Result: Text
    var
        i: Integer;
    begin
        Result := '';

        REPEAT
            i += 1;
            IF Result <> '' THEN
                Result += glue;
            Result += pieces[i];
        UNTIL i > ARRAYLEN(pieces) - 1;
    end;

    procedure UpdateItemWeight(pItemNo: Code[20]; pWeight: Decimal; pForce: Boolean)
    var
        lItem: Record Item;
        lLItemUoM: Record "Item Unit of Measure";
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
        lItemNo: Code[50];
    begin
        IF pItemNo = '' THEN
            ERROR(ESK009Err);

        lItemNo := lGestionMultiReference.RechercheMultiReference(pItemNo);

        lItem.GET(lItemNo);

        //IF NOT pForce THEN ERROR(ESK010, pWeight);

        lLItemUoM.GET(lItem."No.", lItem."Base Unit of Measure");
        lLItemUoM.TESTFIELD(Weight, 0);
        lLItemUoM.VALIDATE(Weight, pWeight);
        lLItemUoM.MODIFY(TRUE);
    end;
}

