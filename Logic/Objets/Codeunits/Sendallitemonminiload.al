codeunit 50034 "Send all item on miniload"
{

    trigger OnRun()
    var
        selection: Integer;
    begin
        selection := STRMENU('Envoyer tous les articles,Envoyer les nouveaux articles,Annuler', 3);
        CASE selection OF
            1:
                SendAll();
            2:
                SendItemNeverSend();
            3:
                EXIT;
        END;
    end;

    var
        cu_MiniLoad: Codeunit "Gestion MINILOAD";

    procedure SendAll()
    var
        rec_Item: Record Item;
    begin
        rec_Item.RESET();
        IF CONFIRM('Etes vous sur de vouloir envoyer tous les articles sur miniload (operation tres lourde) ?') THEN
            IF CONFIRM('Etes vous vraiment sur ?') THEN
                cu_MiniLoad.Article_MsgPRO(rec_Item);
    end;

    procedure SendItemNeverSend()
    var
        rec_Item: Record Item;
    begin
        IF CONFIRM('Etes vous sur de vouloir envoyer tous les nouveaux articles sur MiniLoad ?') THEN BEGIN
            rec_Item.RESET();
            rec_Item.SETRANGE(rec_Item."Envoyé MINILOAD", FALSE);
            IF NOT rec_Item.ISEMPTY THEN
                cu_MiniLoad.Article_MsgPRO(rec_Item);
        END;
    end;
}

