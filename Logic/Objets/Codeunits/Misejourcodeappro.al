codeunit 50017 "Mise à jour code appro"
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    var
        CalculCodeAppro: Codeunit "Calcul Code Appro";
    begin
        // MCO Le 09-02-2017 => Régie
        // MCO Le 09-02-2017 => Régie  => Fait dans un code unit à part
        CalculCodeAppro.SetHideValidationDialog(TRUE);
        CalculCodeAppro.RUN();
        // FIN MCO Le 09-02-2017 => Régie
    end;
}

