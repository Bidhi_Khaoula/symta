codeunit 50072 "Import Ligne Document Vte XLS"
{

    trigger OnRun()
    var
        LSalesHeader: Record "Sales Header";
    begin

        LSalesHeader.GET(LSalesHeader."Document Type"::Order, 'CV-20007675');
        ImportLigneCde(LSalesHeader);
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        CuMultiRef: Codeunit "Gestion Multi-référence";
        FileName: Text[250];

        SheetName: Text[250];


        Fenetre: Dialog;
        NbLigne: Integer;
        NoLigne: Integer;
        NoLigneCde: Integer;

        Text006Lbl: Label 'Import Fichier Excel';
        ExcelExtensionTok: Label '.xlsx', Locked = true;

    procedure ImportLigneCde(_pSalesHeader: Record "Sales Header")
    var
        LSalesLine: Record "Sales Line";
        RecInteger: Record Integer;
       
        LSalesCommentLine: Record "Sales Comment Line";
        FileMgt: Codeunit "File Management";
        LNoLigneComm: Integer;
         LRefSP: Code[20];
        LRefActive: Code[20];
        LRefSaisie: Code[20];
        LQte: Decimal;
        
    begin
        // Veirf Cde
        _pSalesHeader.GET(_pSalesHeader."Document Type", _pSalesHeader."No.");

        // Lecture du classeur

        FileName := FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok);
        SheetName := TempExcelBuf.SelectSheetsName(FileName);

        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();

        // No Ligne Cde
        LSalesLine.SETRANGE("Document Type", _pSalesHeader."Document Type");
        LSalesLine.SETRANGE("Document No.", _pSalesHeader."No.");
        IF LSalesLine.FINDLAST() THEN NoLigneCde := LSalesLine."Line No.";

        // Lecture Excel
        TempExcelBuf.FINDLAST();
        RecInteger.SETRANGE(Number, 1, TempExcelBuf."Row No." - 1);
        IF RecInteger.ISEMPTY THEN ERROR('Aucune ligne à intégrer!');
        RecInteger.FINDSET();

        //On se positionne sur la 1ere ligne des données (entete=1)
        NbLigne := TempExcelBuf."Row No.";
        NoLigne := 1;
        Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');

        REPEAT
            NoLigne := NoLigne + 1;
            Fenetre.UPDATE(1, ROUND(NoLigne / NbLigne * 10000, 1));

            // Vide les variables
            LRefSP := '';
            LRefActive := '';
            LRefSaisie := '';
            CLEAR(LSalesLine);
            LQte := 0;

            // LEcture & vérif des données obligatoires
            // Lecture Ref SP
            IF GetCell('A', NoLigne) THEN
                LRefSP := TempExcelBuf."Cell Value as Text";

            // Lecture RefActive
            IF GetCell('B', NoLigne) THEN
                LRefActive := TempExcelBuf."Cell Value as Text";

            // Lecture RefSaisie
            IF GetCell('C', NoLigne) THEN
                LRefSaisie := TempExcelBuf."Cell Value as Text";

            IF (STRLEN(LRefSP) + STRLEN(LRefActive) + STRLEN(LRefSaisie)) = 0 THEN ERROR('Référence obligatoire sur ligne %1', NoLigne);

            // Lecture qte
            IF NOT GetCell('D', NoLigne) THEN
                ERROR('Quantité obligatoire sur ligne %1', NoLigne)
            ELSE
                IF NOT EVALUATE(LQte, TempExcelBuf."Cell Value as Text") THEN
                    ERROR('Problème de quantité sur ligne %1', NoLigne);

            // Lecture Prix
            IF GetCell('F', NoLigne) THEN
                IF NOT EVALUATE(LSalesLine."Unit Price", TempExcelBuf."Cell Value as Text") THEN
                    ERROR('Problème de prix sur ligne %1', NoLigne);

            // Fin du controle des données

            // Créaation de la ligne
            NoLigneCde += 10000;
            CLEAR(LSalesLine);
            LSalesLine.VALIDATE("Document Type", _pSalesHeader."Document Type");
            LSalesLine.VALIDATE("Document No.", _pSalesHeader."No.");
            LSalesLine."Line No." := NoLigneCde;


            // Affectation de l'article
            LSalesLine.VALIDATE(Type, LSalesLine.Type::Item);
            IF LRefSP <> '' THEN
                LSalesLine.VALIDATE("No.", LRefSP)
            ELSE
                IF LRefActive <> '' THEN
                    LSalesLine.VALIDATE("No.", CuMultiRef.RechercheArticleByActive(LRefActive))
                ELSE BEGIN
                    LRefSP := CuMultiRef.RechercheMultiReference(LRefSaisie);
                    IF LRefSP <> 'RIENTROUVE' THEN
                        LSalesLine.VALIDATE("No.", LRefSP)
                    ELSE
                        ERROR('Article introuvable pour la ligne %1', NoLigne);
                END;

            // Affecation DEBUGGER la refSaisie
            IF LRefSaisie <> '' THEN
                LSalesLine."Référence saisie" := LRefSaisie
            ELSE
                IF LRefActive <> '' THEN
                    LSalesLine."Référence saisie" := LRefActive
                ELSE
                    LSalesLine."Référence saisie" := LRefSP;

            LSalesLine."Recherche référence" := CuMultiRef.RechercheRefActive(LSalesLine."No.");

            LSalesLine.INSERT(TRUE);

            // Qte
            LSalesLine.VALIDATE(Quantity, LQte);

            // Prix
            IF GetCell('F', NoLigne) THEN BEGIN
                EVALUATE(LSalesLine."Unit Price", TempExcelBuf."Cell Value as Text");
                LSalesLine.VALIDATE("Unit Price");
                LSalesLine.VALIDATE("Discount1 %", 0);
                LSalesLine.VALIDATE("Discount2 %", 0);
            END;
            LSalesLine.MODIFY(TRUE);

            IF GetCell('E', NoLigne) THEN BEGIN
                CLEAR(LSalesCommentLine);
                LSalesCommentLine.SETRANGE("Document Type", LSalesLine."Document Type");
                LSalesCommentLine.SETRANGE("No.", LSalesLine."Document No.");
                LSalesCommentLine.SETRANGE("Document Line No.", LSalesLine."Line No.");
                IF NOT LSalesCommentLine.ISEMPTY THEN
                    LSalesCommentLine.FINDLAST();
                LNoLigneComm := LSalesCommentLine."Line No." + 10000;

                LSalesCommentLine.VALIDATE("Document Type", LSalesLine."Document Type");
                LSalesCommentLine.VALIDATE("No.", LSalesLine."Document No.");
                LSalesCommentLine.VALIDATE("Document Line No.", LSalesLine."Line No.");
                LSalesCommentLine.VALIDATE("Line No.", LNoLigneComm);
                LSalesCommentLine.VALIDATE(Date, WORKDATE());
                LSalesCommentLine.VALIDATE(Code, 'XLS');
                LSalesCommentLine.VALIDATE(Comment, TempExcelBuf."Cell Value as Text");
                LSalesCommentLine.VALIDATE("Commentaires Web", TRUE);
                LSalesCommentLine.VALIDATE("Display Order", TRUE);
                LSalesCommentLine.VALIDATE("Print Shipment", TRUE);
                LSalesCommentLine.INSERT(TRUE);
            END;

        UNTIL RecInteger.NEXT() = 0;
    end;

    local procedure ReadExcelSheet()
    begin
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    local procedure GetCell(_pRow: Text[5]; _pLigne: Integer): Boolean
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.SETRANGE("Row No.", _pLigne);
        TempExcelBuf.SETRANGE(xlColID, _pRow);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;
}

