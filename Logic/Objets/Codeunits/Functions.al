codeunit 50043 Functions
{
    Permissions = TableData "Warehouse Entry" = m;
    PROCEDURE ValidateUserID(UserName: Code[50]);
    VAR
        User: Record User;
        Text000Err: label 'The user name %1 does not exist.', Comment = '%1 = Nom Utilisateur';

    BEGIN
        IF UserName <> '' THEN BEGIN
            User.SETCURRENTKEY("User Name");
            User.SETRANGE("User Name", UserName);
            IF NOT User.FINDFIRST() THEN BEGIN
                User.RESET();
                IF NOT User.ISEMPTY THEN
                    ERROR(Text000Err, UserName);
            END;
        END;
    END;

    procedure SendBlockedItemNotification(var SalesLine: Record "Sales Line")
    var
        NotificationLifecycleMgt: Codeunit "Notification Lifecycle Mgt.";
        NotificationToSend: Notification;
        BlockedItemNotificationMsg: Label 'Item %1 is blocked, but it is allowed on this type of document.', Comment = '%1 is Item No.';

    begin
        NotificationToSend.Id := '963A9FD3-11E8-4CAA-BE3A-7F8CEC9EF8EC';
        NotificationToSend.Recall();
        NotificationToSend.Message := StrSubstNo(BlockedItemNotificationMsg, SalesLine."No.");
        NotificationLifecycleMgt.SendNotification(NotificationToSend, SalesLine.RecordId);
    end;

    procedure HandleDedicatedBin(var SalesLine: Record "Sales Line"; IssueWarning: Boolean)
    var
        WhseIntegrationMgt: Codeunit "Whse. Integration Management";
    begin
        if SalesLine.IsInbound() or (SalesLine."Quantity (Base)" = 0) or (SalesLine."Document Type" = SalesLine."Document Type"::"Blanket Order") then
            exit;

        WhseIntegrationMgt.CheckIfBinDedicatedOnSrcDoc(SalesLine."Location Code", SalesLine."Bin Code", IssueWarning);
    end;

    procedure IsShipmentBinOverridesDefaultBin(Location: Record Location): Boolean
    var
        Bin: Record Bin;
        ShipmentBinAvailable: Boolean;
    begin
        ShipmentBinAvailable := Bin.Get(Location.Code, Location."Shipment Bin Code");
        exit(Location."Require Shipment" and ShipmentBinAvailable);
    end;

    PROCEDURE LookupWhseShptHeader(VAR WhseShptHeader: Record "Warehouse Shipment Header");
    BEGIN
        COMMIT();
        IF USERID <> '' THEN BEGIN
            WhseShptHeader.FILTERGROUP := 2;
            WhseShptHeader.SETRANGE("Location Code");
        END;
        IF PAGE.RUNMODAL(0, WhseShptHeader) = ACTION::LookupOK THEN;
        IF USERID <> '' THEN BEGIN
            WhseShptHeader.FILTERGROUP := 2;
            WhseShptHeader.SETRANGE("Location Code", WhseShptHeader."Location Code");
            WhseShptHeader.FILTERGROUP := 0;
        END;
    END;

    procedure CalcTotalOutstandingAmt(var Rec: Record Customer) Result: Decimal
    var
        SalesLine: Record "Sales Line";
        ServLine: Record "Service Line";
        SalesOutstandingAmountFromShipment: Decimal;
        ServOutstandingAmountFromShipment: Decimal;

    begin

        Rec.CalcFields(
          "Outstanding Invoices (LCY)", "Outstanding Orders (LCY)", "Outstanding Serv.Invoices(LCY)", "Outstanding Serv. Orders (LCY)");
        SalesOutstandingAmountFromShipment := SalesLine.OutstandingInvoiceAmountFromShipment(Rec."No.");
        ServOutstandingAmountFromShipment := ServLine.OutstandingInvoiceAmountFromShipment(Rec."No.");

        exit(
          Rec."Outstanding Orders (LCY)" + Rec."Outstanding Invoices (LCY)" + Rec."Outstanding Serv. Orders (LCY)" +
          Rec."Outstanding Serv.Invoices(LCY)" - SalesOutstandingAmountFromShipment - ServOutstandingAmountFromShipment);
    end;

    procedure CalcReturnAmounts(Rec: Record Customer; var OutstandingRetOrdersLCY2: Decimal; var RcdNotInvdRetOrdersLCY2: Decimal)
    var
        SalesLine: Record "Sales Line";
    begin
        SalesLine.Reset();
        SalesLine.SetCurrentKey("Document Type", "Bill-to Customer No.", "Currency Code");
        SalesLine.SetRange("Document Type", SalesLine."Document Type"::"Return Order");
        SalesLine.SetRange("Bill-to Customer No.", Rec."No.");
        SalesLine.CalcSums("Outstanding Amount (LCY)", "Return Rcd. Not Invd. (LCY)");
        OutstandingRetOrdersLCY2 := SalesLine."Outstanding Amount (LCY)";
        RcdNotInvdRetOrdersLCY2 := SalesLine."Return Rcd. Not Invd. (LCY)";
    end;

    PROCEDURE WriteAsText(Content: Text; Encoding: TextEncoding);
    VAR
        OutStr: OutStream;
    BEGIN
        CLEAR(RecGUpgradeBlobStorage.Blob);
        IF Content = '' THEN
            EXIT;
        RecGUpgradeBlobStorage.Blob.CREATEOUTSTREAM(OutStr, Encoding);
        OutStr.WRITETEXT(Content);
    END;

    PROCEDURE ReadAsText(LineSeparator: Text; Encoding: TextEncoding) Content: Text;
    VAR
        InStream: InStream;
        ContentLine: Text;
    BEGIN
        RecGUpgradeBlobStorage.Blob.CREATEINSTREAM(InStream, Encoding);

        InStream.READTEXT(Content);
        WHILE NOT InStream.EOS DO BEGIN
            InStream.READTEXT(ContentLine);
            Content += LineSeparator + ContentLine;
        END;
    END;

    procedure ChangeZoneCode(pRecBin: Record Bin)
    var

        lBinContent: Record "Bin Content";
        lSalesLine: Record "Sales Line";
        lPurchLine: Record "Purchase Line";
        lWarehouseEntry: Record "Warehouse Entry";
        ESK001Err: label 'Erreur générée pour respecter l''alerte !';
        ESK002Qst: label 'Emplacement non vide. Confirmez-vous le changement de zone ?';

    begin
        // ANI Le 18-05-2016 FE20160427

        // il doit y avoir changement de zone
        // IF xRec."Zone Code" = "Zone Code" THEN EXIT;

        // _CheckEmptyBinPourZone(Text007);

        // si l'emplacement n'est pas vide...
        IF NOT pRecBin.Empty THEN
            IF NOT CONFIRM(ESK002Qst) THEN ERROR(ESK001Err);

        IF pRecBin.Code = '' THEN
            pRecBin.SetUpNewLine();

        // mise à jour des contenus emplacement (Bin Content)
        CLEAR(lBinContent);
        lBinContent.SETRANGE("Location Code", pRecBin."Location Code");
        lBinContent.SETRANGE("Bin Code", pRecBin.Code);
        lBinContent.MODIFYALL("Zone Code", pRecBin."Zone Code");

        // mise à jour des documents en cours (Commandes vente/achat)
        CLEAR(lSalesLine);
        lSalesLine.SETCURRENTKEY("Bin Code", "Location Code");
        lSalesLine.SETRANGE("Location Code", pRecBin."Location Code");
        lSalesLine.SETRANGE("Bin Code", pRecBin.Code);
        //lSalesLine.SETRANGE("Zone Code", xRec."Zone Code");
        lSalesLine.MODIFYALL("Zone Code", pRecBin."Zone Code");

        CLEAR(lPurchLine);
        lPurchLine.SETCURRENTKEY("Bin Code", "Location Code");
        lPurchLine.SETRANGE("Location Code", pRecBin."Location Code");
        lPurchLine.SETRANGE("Bin Code", pRecBin.Code);
        //lPurchLine.SETRANGE("Zone Code", xRec."Zone Code");
        lPurchLine.MODIFYALL("Zone Code", pRecBin."Zone Code");

        // mise à jour des écritures entrepots
        CLEAR(lWarehouseEntry);
        lWarehouseEntry.SETCURRENTKEY("Bin Code", "Location Code");
        lWarehouseEntry.SETRANGE("Bin Code", pRecBin.Code);
        lWarehouseEntry.SETRANGE("Location Code", pRecBin."Location Code");
        lWarehouseEntry.MODIFYALL("Zone Code", pRecBin."Zone Code");
    end;

    var
        RecGUpgradeBlobStorage: Record "Upgrade Blob Storage";
}