codeunit 50041 "Hierarchies Upd"
{

    trigger OnRun()
    var
        Item: Record Item;
        Hierarchy: Record "Item Hierarchies";
        critere: Record "Article Critere";
        Window: Dialog;
        Nb: Integer;
    begin
        Item.SETFILTER("Super Famille Marketing", '<>%1', '');
        Item.FINDSET();
        Window.OPEN('Processing data1... @1@@@@@@@@@@');
        recNumber := Item.COUNT;
        REPEAT
            Nb += 1;
            IF Nb MOD (recNumber DIV 100) = 0 THEN
                Window.UPDATE(1, (Nb / recNumber * 10000) DIV 1);
            Hierarchy.INIT();
            Hierarchy."Code Article" := Item."No.";
            Hierarchy."Super Famille Marketing" := Item."Super Famille Marketing";
            Hierarchy."Famille Marketing" := Item."Famille Marketing";
            Hierarchy."Sous Famille Marketing" := Item."Sous Famille Marketing";
            Hierarchy."By Default" := TRUE;
            Hierarchy.Insert();

            critere.RESET();
            critere.SETRANGE("Code Article", Item."No.");
            critere.MODIFYALL("Super Famille Marketing", Item."Super Famille Marketing");
            critere.MODIFYALL("Famille Marketing", Item."Famille Marketing");
            critere.MODIFYALL("Sous Famille Marketing", Item."Sous Famille Marketing");

        UNTIL Item.NEXT() = 0;
        Window.CLOSE();
    end;

    var
        recNumber: Integer;
}

