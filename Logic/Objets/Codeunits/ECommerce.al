codeunit 50025 "E-Commerce"
{

    trigger OnRun()
    begin
        IF gSalesHeader.GET(gSalesHeader."Document Type"::"Return Order", 'RV-20000320') THEN BEGIN
            InsertCommentReturnHeader(gSalesHeader, 'Commentaire CFR test en tête');
            InsertCommentReturnLine(gSalesHeader, 20000, 'Commentaire CFR test ligne');
        END;

        /*IF gSalesHeader.GET(gSalesHeader."Document Type"::"Return Order", 'RV-20000477') THEN BEGIN
          //MESSAGE(FORMAT(InsertReturnLine(gSalesHeader,'BL-19154877', 20000, 3,'ERC')));
          //InsertReturnLine(gSalesHeader,'BL-20008995', 10000, 3,'ERC');
          ReleaseReturn(gSalesHeader);
          MESSAGE('FIN');
        END;*/
    end;

    Var
        gSalesHeader: Record "Sales Header";


    procedure InsertSalesLine(p_SalesHeader: Record "Sales Header"; p_CodeArticle: Code[20]; p_RefActive: Code[20]; p_RefCommandee: Code[20]; p_QteCommandee: Decimal; p_PrixBrut: Decimal; p_Remise1: Decimal; p_Remise2: Decimal; p_PrixNet: Decimal; p_MontantHT: Decimal; p_CommentaireClient: Text[250])
    var
        rec_SalesLine: Record "Sales Line";
        SalesCommentLine: Record "Sales Comment Line";
        SalesLine: Record "Sales Line";
        SalesCalcDiscByType: Codeunit "Sales - Calc Discount By Type";
        NextLineNo: Integer;
    begin
        //** Cette fonction permet de créer une ligne de commande pour une commande passé en en-tête ** //

        // Récupère le numéro de ligne si il existe déjà.
        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", p_SalesHeader."Document Type");
        SalesLine.SETRANGE("Document No.", p_SalesHeader."No.");

        IF SalesLine.FIND('+') THEN
            NextLineNo := SalesLine."Line No." + 10000
        ELSE
            NextLineNo := 10000;


        //Initialisation de la ligne de la commande vente.
        rec_SalesLine.INIT();
        rec_SalesLine.VALIDATE("Document Type", p_SalesHeader."Document Type");
        rec_SalesLine.VALIDATE("Document No.", p_SalesHeader."No.");
        rec_SalesLine.VALIDATE(Type, rec_SalesLine.Type::Item);
        rec_SalesLine.VALIDATE("Line No.", NextLineNo);
        rec_SalesLine.VALIDATE("Recherche référence", p_RefActive);
        rec_SalesLine.VALIDATE("Référence saisie", p_RefCommandee);
        rec_SalesLine.VALIDATE(Quantity, p_QteCommandee);
        rec_SalesLine.VALIDATE("Unit Price", p_PrixBrut);
        rec_SalesLine.VALIDATE("Discount1 %", p_Remise1);
        rec_SalesLine.VALIDATE("Discount2 %", p_Remise2);
        rec_SalesLine.VALIDATE("Net Unit Price", p_PrixNet);
        rec_SalesLine.VALIDATE("Line Amount", p_MontantHT);
        rec_SalesLine.INSERT(TRUE);

        //Insertion d'une ligne de commentaire.
        IF p_CommentaireClient <> '' THEN BEGIN
            SalesCommentLine.INIT();
            SalesCommentLine.VALIDATE("Document Type", p_SalesHeader."Document Type");
            SalesCommentLine.VALIDATE("No.", p_SalesHeader."No.");
            SalesCommentLine.VALIDATE("Document Line No.", rec_SalesLine."Line No.");
            SalesCommentLine.VALIDATE(Date, WORKDATE());
            SalesCommentLine.VALIDATE(Comment, p_CommentaireClient);
            SalesCommentLine.VALIDATE("Display Order", TRUE);
            SalesCommentLine.VALIDATE("Print Order", TRUE);
            SalesCommentLine.VALIDATE("Commentaires Web", TRUE);// AD Le 28-09-2016 => REGIE ->
            SalesCommentLine.VALIDATE("Print Shipment", TRUE);// MCO Le 07-06-2017 => Régie : Que sur les lignes
            SalesCommentLine.INSERT(TRUE);
        END;

        SalesCalcDiscByType.ResetRecalculateInvoiceDisc(p_SalesHeader);   // AD Le 08-12-2015
    end;

    procedure InsertCommentHeader(p_SalesHeader: Record "Sales Header"; p_Commentaire: Text[60])
    var
        SalesCommentLine: Record "Sales Comment Line";
        LineNo: Integer;
    begin
        IF p_Commentaire <> '' THEN BEGIN
            SalesCommentLine.RESET();
            SalesCommentLine.SETRANGE("Document Type", p_SalesHeader."Document Type");
            SalesCommentLine.SETRANGE("No.", p_SalesHeader."No.");
            IF SalesCommentLine.FINDLAST() THEN
                LineNo := SalesCommentLine."Line No." + 10000
            ELSE
                LineNo := 10000;

            SalesCommentLine.INIT();
            SalesCommentLine.VALIDATE("Document Type", p_SalesHeader."Document Type");
            SalesCommentLine.VALIDATE("No.", p_SalesHeader."No.");
            SalesCommentLine."Line No." := LineNo;
            SalesCommentLine.VALIDATE(Date, WORKDATE());
            SalesCommentLine.VALIDATE(Comment, p_Commentaire);
            SalesCommentLine.VALIDATE("Display Order", TRUE);
            SalesCommentLine.VALIDATE("Print Order", TRUE);
            SalesCommentLine.VALIDATE("Commentaires Web", TRUE);// AD Le 28-09-2016 => REGIE ->
            SalesCommentLine.INSERT(TRUE);
        END;
    end;

    procedure FinaliserCommande(_pSalesHeader: Record "Sales Header")
    begin
        _pSalesHeader.VALIDATE("Devis Web Intégré", TRUE);
        _pSalesHeader.MODIFY();
    end;

    PROCEDURE UpdateReturnOrder(pSalesHeader: Record "Sales Header"; pSourceDocumentType: Code[20]; pCodeAbattement: Code[10]; pSelltoContactNo: Code[20]; pUserId: Code[50]; pTypeRetour: Integer; pEnlevementChargeClient: Boolean; pPortChargeClient: Boolean);
    VAR
        lUser: Record User;
        lContact: Record Contact;
    BEGIN
        // CFR le 11/03/2021 - R‚gie : gestion des retour > Mise … jour du retour aprŠs insertion
        IF (pSalesHeader."Document Type" <> pSalesHeader."Document Type"::"Return Order") THEN EXIT;

        // Evite les messages intempestifs (notamment pour le changement de contact
        pSalesHeader.SetHideValidationDialog(TRUE);

        IF (pSourceDocumentType <> '') THEN
            pSalesHeader.VALIDATE("Source Document Type", pSourceDocumentType);

        IF (pCodeAbattement <> '') THEN
            pSalesHeader.VALIDATE("Code Abattement", pCodeAbattement);

        IF (pSelltoContactNo <> '') THEN
            IF lContact.GET(pSelltoContactNo) THEN
                pSalesHeader.VALIDATE("Sell-to Contact No.", pSelltoContactNo);

        IF (pUserId <> '') THEN BEGIN
            lUser.SETRANGE("User Name", pUserId);
            IF lUser.FINDFIRST() THEN BEGIN
                pSalesHeader.VALIDATE("Create User ID", pUserId);
                pSalesHeader.VALIDATE("Order Create User", pUserId);
            END;
        END;

        pSalesHeader.VALIDATE("Type retour", pTypeRetour);
        pSalesHeader.VALIDATE("Enlevement Charge Client", pEnlevementChargeClient);
        pSalesHeader.VALIDATE("Port Charge Client", pPortChargeClient);

        pSalesHeader.MODIFY();
    END;

    PROCEDURE InsertReturnLine(pSalesHeader: Record "Sales Header"; pShipmentHeaderNo: Code[20]; pShipmentLineNo: Integer; pReturnQuantity: Decimal; pReturnReasonCode: Code[10]): Integer;
    VAR

        lSalesLine: Record "Sales Line";

        FromSalesShptLine: Record "Sales Shipment Line";
        FromSalesInvLine: Record "Sales Invoice Line";
        FromSalesCrMemoLine: Record "Sales Cr.Memo Line";
        FromReturnRcptLine: Record "Return Receipt Line";
        langage: Record Language;
        CduLFunctions: Codeunit "Codeunits Functions";
        CopyDocMgt: Codeunit "Copy Document Mgt.";
        lLastLineNo: Integer;
        SalesDocType: option Quote,"Blanket Order",Order,Invoice,"Return Order","Credit Memo","Posted Shipment","Posted Invoice","Posted Return Receipt","Posted Credit Memo";
        LinesNotCopied: Integer;
        MissingExCostRevLink: Boolean;
        OriginalQuantity: Boolean;
    BEGIN
        // CFR le 11/03/2021 - R‚gie : gestion des retour > cr‚ation des lignes … partir d'un BL avec la fonction Extraire document
        // retourne le nø de ligne si insertion, 0 sinon

        //fbrun le 14/09/2022 -> ticket 67826
        IF langage.GET(pSalesHeader."Language Code") THEN
            GLOBALLANGUAGE(langage."Windows Language ID")
        ELSE
            GLOBALLANGUAGE(1036);
        //FIN FBRUN

        IF (pSalesHeader."Document Type" <> pSalesHeader."Document Type"::"Return Order") THEN EXIT(0);
        // code raison obligatoire
        IF (pReturnReasonCode = '') THEN EXIT(0);
        // CFR le 19/10/2021 : Correction InsertReturnLine v‚rifier si la ligne a bien ‚t‚ ins‚r‚e
        lSalesLine.RESET();
        lSalesLine.SETRANGE("Document Type", pSalesHeader."Document Type");
        lSalesLine.SETRANGE("Document No.", pSalesHeader."No.");
        IF lSalesLine.FINDLAST() THEN lLastLineNo := lSalesLine."Line No.";

        IF (FromSalesShptLine.GET(pShipmentHeaderNo, pShipmentLineNo)) THEN BEGIN
            FromSalesShptLine.SETRECFILTER();
            OriginalQuantity := FALSE;
            CopyDocMgt.SetProperties(FALSE, FALSE, FALSE, FALSE, TRUE, TRUE, OriginalQuantity);
            CopyDocMgt.CopySalesLinesToDoc(
                  SalesDocType::"Posted Shipment", pSalesHeader,
                  FromSalesShptLine, FromSalesInvLine, FromReturnRcptLine, FromSalesCrMemoLine, LinesNotCopied, MissingExCostRevLink);

            // AD Le 10-10-2011 => Application de l'abattement
            CduLFunctions.ApplicationAbattement(pSalesHeader);
            // FIN AD Le 10-10-2011

            CLEAR(CopyDocMgt);

            // R‚cupŠre la derniŠre ligne
            lSalesLine.RESET();
            lSalesLine.SETRANGE("Document Type", pSalesHeader."Document Type");
            lSalesLine.SETRANGE("Document No.", pSalesHeader."No.");
            lSalesLine.SETFILTER("Line No.", '>%1', lLastLineNo);

            IF lSalesLine.FINDLAST() THEN BEGIN
                lSalesLine.VALIDATE(Quantity, pReturnQuantity);
                lSalesLine.VALIDATE("Return Reason Code", pReturnReasonCode);
                lSalesLine.MODIFY();
                EXIT(lSalesLine."Line No.");
            END;

        END;

        EXIT(0);
    END;

    PROCEDURE InsertReturnItem(pSalesHeader: Record Microsoft.Sales.Document."Sales Header"; pItemNo: Code[20]; pQuantity: Decimal; pUnitAmount: Decimal; pReturnReasonCode: Code[10]): Integer;
    VAR

        lSalesLine: Record "Sales Line";
        langage: Record Language;
        lLastLineNo: Integer;
    BEGIN
        // CFR le 01/12/2022 : Retours vente : ajout ligne frais
        // retourne le nø de ligne si insertion, 0 sinon

        //fbrun le 14/09/2022 -> ticket 67826
        IF langage.GET(pSalesHeader."Language Code") THEN
            GLOBALLANGUAGE(langage."Windows Language ID")
        ELSE
            GLOBALLANGUAGE(1036);
        //FIN FBRUN

        IF (pSalesHeader."Document Type" <> pSalesHeader."Document Type"::"Return Order") THEN EXIT(0);

        lLastLineNo := 10000;
        lSalesLine.RESET();
        lSalesLine.SETRANGE("Document Type", pSalesHeader."Document Type");
        lSalesLine.SETRANGE("Document No.", pSalesHeader."No.");
        IF lSalesLine.FINDLAST() THEN lLastLineNo += lSalesLine."Line No.";

        CLEAR(lSalesLine);
        lSalesLine.INIT();
        lSalesLine.RESET();
        lSalesLine.VALIDATE("Document Type", pSalesHeader."Document Type");
        lSalesLine.VALIDATE("Document No.", pSalesHeader."No.");
        lSalesLine.VALIDATE(Type, lSalesLine.Type::Item);
        lSalesLine.VALIDATE("Line No.", lLastLineNo);
        lSalesLine.VALIDATE("No.", pItemNo);
        lSalesLine.VALIDATE(Quantity, pQuantity);
        lSalesLine.VALIDATE("Unit Price", pUnitAmount);
        IF (pReturnReasonCode <> '') THEN
            lSalesLine.VALIDATE("Return Reason Code", pReturnReasonCode);
        IF lSalesLine.INSERT(TRUE) THEN
            // R‚cupŠre la derniŠre ligne
            EXIT(lSalesLine."Line No.");
        EXIT(0);
    END;

    PROCEDURE InsertCommentReturnHeader(pSalesHeader: Record "Sales Header"; pComment: Text[60]);
    VAR
        lSalesCommentLine: Record "Sales Comment Line";
        lLineNo: Integer;
    BEGIN
        // CFR le 05/04/2022 : Commentaires sur retours vente
        IF (pComment <> '') AND (pSalesHeader."Document Type" = pSalesHeader."Document Type"::"Return Order") THEN BEGIN
            lSalesCommentLine.RESET();
            lSalesCommentLine.SETRANGE("Document Type", pSalesHeader."Document Type");
            lSalesCommentLine.SETRANGE("No.", pSalesHeader."No.");
            IF lSalesCommentLine.FINDLAST() THEN
                lLineNo := lSalesCommentLine."Line No." + 10000
            ELSE
                lLineNo := 10000;

            lSalesCommentLine.INIT();
            lSalesCommentLine.VALIDATE("Document Type", pSalesHeader."Document Type");
            lSalesCommentLine.VALIDATE("No.", pSalesHeader."No.");
            lSalesCommentLine."Line No." := lLineNo;
            lSalesCommentLine.VALIDATE("Document Line No.", 0);
            lSalesCommentLine.VALIDATE(Date, WORKDATE());
            lSalesCommentLine.VALIDATE(Comment, pComment);
            lSalesCommentLine.VALIDATE("Print Order", TRUE);
            lSalesCommentLine.VALIDATE("Commentaires Web", TRUE);
            lSalesCommentLine.INSERT(TRUE);
        END;
    END;

    PROCEDURE InsertCommentReturnLine(pSalesHeader: Record "Sales Header"; pSalesLineNo: Integer; pComment: Text[60]);
    VAR
        lSalesCommentLine: Record "Sales Comment Line";
        lLineNo: Integer;
    BEGIN
        // CFR le 05/04/2022 : Commentaires sur retours vente
        IF (pComment <> '') AND (pSalesHeader."Document Type" = pSalesHeader."Document Type"::"Return Order") THEN BEGIN
            lSalesCommentLine.RESET();
            lSalesCommentLine.SETRANGE("Document Type", pSalesHeader."Document Type");
            lSalesCommentLine.SETRANGE("No.", pSalesHeader."No.");
            IF lSalesCommentLine.FINDLAST() THEN
                lLineNo := lSalesCommentLine."Line No." + 10000
            ELSE
                lLineNo := 10000;

            lSalesCommentLine.INIT();
            lSalesCommentLine.VALIDATE("Document Type", pSalesHeader."Document Type");
            lSalesCommentLine.VALIDATE("No.", pSalesHeader."No.");
            lSalesCommentLine."Line No." := lLineNo;
            lSalesCommentLine.VALIDATE("Document Line No.", pSalesLineNo);
            lSalesCommentLine.VALIDATE(Date, WORKDATE());
            lSalesCommentLine.VALIDATE(Comment, pComment);
            lSalesCommentLine.VALIDATE("Print Order", TRUE);
            lSalesCommentLine.VALIDATE("Commentaires Web", TRUE);
            lSalesCommentLine.INSERT(TRUE);
        END;
    END;

    // ******************************************************************************************
    //  CodeUnit publi‚ comme Webservice sous le nom [SalesHeader]
    //  c.a.d. quil est associ‚ … la page 50232 ‚galement publi‚e sous le nom [SalesHeader]
    //  >> donc pour ˆtre visibles,
    //     TOUTES les fonctions doivent avoir en paramŠtre nø1 un record de type "Sales header"
    // ******************************************************************************************

    // CFR le 11/03/2021 - R‚gie : gestion des retour
    // CFR le 03/09/2021 : Correction ReleaseReturn
    // CFR le 19/10/2021 : Correction InsertReturnLine
    // CFR le 05/04/2022 : SFD20210311 Commentaires sur retours vente
    // CFR le 01/12/2022 : Retours vente : ajout ligne frais
}

