codeunit 50054 "CustEntry-Apply Posted TMP"
{
    Permissions = TableData 25 = rimd;
    TableNo = 21;

    trigger OnRun()
    var
        EntriesToApply: Record "Cust. Ledger Entry";
        RecLApplyUnapplyParameters: Record "Apply Unapply Parameters";
        ApplicationDate: Date;

    begin
        WITH Rec DO BEGIN
            IF NOT PaymentToleracenMgt.PmtTolCust(Rec) THEN
                EXIT;
            GET("Entry No.");

            ApplicationDate := 0D;
            EntriesToApply.SETCURRENTKEY("Customer No.", "Applies-to ID");
            EntriesToApply.SETRANGE("Customer No.", "Customer No.");
            EntriesToApply.SETRANGE("Applies-to ID", "Applies-to ID");
            EntriesToApply.FIND('-');
            REPEAT
                IF EntriesToApply."Posting Date" > ApplicationDate THEN
                    ApplicationDate := EntriesToApply."Posting Date";
            UNTIL EntriesToApply.NEXT() = 0;
            //PostApplication.SetValues("Document No.", ApplicationDate);//BEFORE
            RecLApplyUnapplyParameters.SetRange("Document No.", rec."Document No.");
            RecLApplyUnapplyParameters.SetRange("Posting Date", ApplicationDate);
            PostApplication.SetParameters(RecLApplyUnapplyParameters);
            PostApplication.LOOKUPMODE(TRUE);
            //IF ACTION::LookupOK = PostApplication.RUNMODAL THEN BEGIN
            //  GenJnlLine.INIT();
            //PostApplication.GetValues(GenJnlLine."Document No.", GenJnlLine."Posting Date");//BEFORE
            RecLApplyUnapplyParameters.SetRange("Document No.", GenJnlLine."Document No.");
            RecLApplyUnapplyParameters.SetRange("Posting Date", GenJnlLine."Posting Date");
            PostApplication.GetParameters(RecLApplyUnapplyParameters);
            //    IF GenJnlLine."Posting Date" < ApplicationDate THEN
            //     ERROR(
            //      Text003,
            //      GenJnlLine.FIELDCAPTION("Posting Date"),FIELDCAPTION("Posting Date"),TABLECAPTION);
            //END ELSE
            // EXIT;

            Window.OPEN(Text001Msg);

            SourceCodeSetup.GET();

            GenJnlLine."Account Type" := GenJnlLine."Account Type"::Customer;
            GenJnlLine."Account No." := "Customer No.";
            CALCFIELDS("Debit Amount", "Credit Amount", "Debit Amount (LCY)", "Credit Amount (LCY)");
            GenJnlLine.Correction :=
              ("Debit Amount" < 0) OR ("Credit Amount" < 0) OR
              ("Debit Amount (LCY)" < 0) OR ("Credit Amount (LCY)" < 0);
            GenJnlLine."Document Type" := "Document Type";
            GenJnlLine.Description := Description;
            GenJnlLine."Shortcut Dimension 1 Code" := "Global Dimension 1 Code";
            GenJnlLine."Shortcut Dimension 2 Code" := "Global Dimension 2 Code";
            GenJnlLine."Posting Group" := "Customer Posting Group";
            GenJnlLine."Source Type" := GenJnlLine."Source Type"::Customer;
            GenJnlLine."Source No." := "Customer No.";
            GenJnlLine."Source Code" := SourceCodeSetup."Sales Entry Application";
            GenJnlLine."System-Created Entry" := TRUE;
            GenJnlLine."Due Date" := "Due Date";
            EntryNoBeforeApplication := FindLastApplDtldCustLedgEntry();

            GenJnlPostLine.CustPostApplyCustLedgEntry(GenJnlLine, Rec);

            EntryNoAfterApplication := FindLastApplDtldCustLedgEntry();
            //IF EntryNoAfterApplication = EntryNoBeforeApplication THEN
            //ERROR(Text004);

            COMMIT();
            Window.CLOSE();
            //MESSAGE(Text002);
        END;
    end;

    var
        SourceCodeSetup: Record "Source Code Setup";
        GenJnlLine: Record "Gen. Journal Line";

        GenJnlCheckLine: Codeunit "Gen. Jnl.-Check Line";
        GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line";
        PaymentToleracenMgt: Codeunit "Payment Tolerance Management";
        PostApplication: Page "Post Application";
        Window: Dialog;
        EntryNoBeforeApplication: Integer;
        EntryNoAfterApplication: Integer;
        Text001Msg: Label 'Posting application...';
        // Text002Msg: Label 'The application was successfully posted.';
        /*  Text003: Label 'The %1 entered must not be before the %2 on the %3.';
         Text004: Label 'The application was successfully posted though no entries have been applied.';
         Text005: Label 'Before you can unapply this entry, you must first unapply all application entries that were posted after this entry.';
  */
        Text006Err: Label '%1 No. %2 does not have an application entry.', Comment = '%1 = Customer Ledger entry Cap ; %2 =  Customer ledger entry No';
        Text007Qst: Label 'Do you want to unapply the entries?';
        Text008Msg: Label 'Unapplying and posting...';
        Text009Msg: Label 'The entries were successfully unapplied.';
        Text010Err: Label 'There is nothing to unapply. ';
        Text011Qst: Label 'To unapply these entries, the program will post correcting entries.\';
        Text012Err: Label 'Before you can unapply this entry, you must first unapply all application entries in %1 No. %2 that were posted after this entry.',  Comment = '%1 = Customer Ledger entry  ; %2 =  Customer ledger entry No';
        Text013Err: Label '%1 is not within your range of allowed posting dates in %2 No. %3.' ,  Comment = '%1 = Posting Date; %2 =  Caption ; %3 = Entry No' ;
        Text014Err: Label '%1 is not within your range of allowed posting dates.' ,  Comment = '%1 = Posting Date';
        Text015Err: Label 'The latest %3 must be an application in %1 No. %2.' , Comment = '%1 = CustLedgEntry TABLECAPTION ; %2 = CustomerLedger Entry No ; %3 =  Transaction Caption' ;
                       
        Text016Err: Label 'You cannot unapply the entry with the posting date %1, because the exchange rate for the additional reporting currency has been changed. ' , Comment = '%1 = Posting Date';
        MaxPostingDate: Date;
        Text017Err: Label 'You cannot unapply %1 No. %2 because the entry has been involved in a reversal.' , Comment = ' %1 = Customer Ledger Entry Caption , %2 = Customer Ledger Entry No';

    local procedure FindLastApplDtldCustLedgEntry(): Integer
    var
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
    begin
        DtldCustLedgEntry.LOCKTABLE();
        if DtldCustLedgEntry.FIND('+') then
            exit(DtldCustLedgEntry."Entry No.")
        else
            exit(0);
    end;

    local procedure FindLastApplEntry(CustLedgEntryNo: Integer): Integer
    var
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        ApplicationEntryNo: Integer;
    begin
        DtldCustLedgEntry.SETCURRENTKEY("Cust. Ledger Entry No.", "Entry Type");
        DtldCustLedgEntry.SETRANGE("Cust. Ledger Entry No.", CustLedgEntryNo);
        DtldCustLedgEntry.SETRANGE("Entry Type", DtldCustLedgEntry."Entry Type"::Application);
        ApplicationEntryNo := 0;
        IF DtldCustLedgEntry.FIND('-') THEN
            REPEAT
                IF (DtldCustLedgEntry."Entry No." > ApplicationEntryNo) AND NOT DtldCustLedgEntry.Unapplied THEN
                    ApplicationEntryNo := DtldCustLedgEntry."Entry No.";
            UNTIL DtldCustLedgEntry.NEXT() = 0;
        EXIT(ApplicationEntryNo);
    end;

    local procedure FindLastTransactionNo(CustLedgEntryNo: Integer): Integer
    var
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        LastTransactionNo: Integer;
    begin
        DtldCustLedgEntry.SETCURRENTKEY("Cust. Ledger Entry No.", "Entry Type");
        DtldCustLedgEntry.SETRANGE("Cust. Ledger Entry No.", CustLedgEntryNo);
        LastTransactionNo := 0;
        IF DtldCustLedgEntry.FIND('-') THEN
            REPEAT
                IF (DtldCustLedgEntry."Transaction No." > LastTransactionNo) AND NOT DtldCustLedgEntry.Unapplied THEN
                    LastTransactionNo := DtldCustLedgEntry."Transaction No.";
            UNTIL DtldCustLedgEntry.NEXT() = 0;
        EXIT(LastTransactionNo);
    end;

    procedure UnApplyDtldCustLedgEntry(DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry")
    var
        ApplicationEntryNo: Integer;
    begin
        DtldCustLedgEntry.TESTFIELD("Entry Type", DtldCustLedgEntry."Entry Type"::Application);
        DtldCustLedgEntry.TESTFIELD(Unapplied, FALSE);
        ApplicationEntryNo := FindLastApplEntry(DtldCustLedgEntry."Cust. Ledger Entry No.");

        //IF DtldCustLedgEntry."Entry No." <> ApplicationEntryNo THEN
        //  ERROR(Text005);
        CheckReversal(DtldCustLedgEntry."Cust. Ledger Entry No.");
        UnApplyCustomer(DtldCustLedgEntry);
    end;

    procedure UnApplyCustLedgEntry(CustLedgEntryNo: Integer)
    var
        CustLedgentry: Record "Cust. Ledger Entry";
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        ApplicationEntryNo: Integer;
    begin
        CheckReversal(CustLedgEntryNo);
        ApplicationEntryNo := FindLastApplEntry(CustLedgEntryNo);
        IF ApplicationEntryNo = 0 THEN
            ERROR(Text006Err, CustLedgentry.TABLECAPTION, CustLedgEntryNo);
        DtldCustLedgEntry.GET(ApplicationEntryNo);
        UnApplyCustomer(DtldCustLedgEntry);
    end;

    local procedure UnApplyCustomer(DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry")
    var
        UnapplyCustEntries: Page "Unapply Customer Entries";
    begin
        WITH DtldCustLedgEntry DO BEGIN
            TESTFIELD("Entry Type", "Entry Type"::Application);
            TESTFIELD(Unapplied, FALSE);
            UnapplyCustEntries.SetDtldCustLedgEntry("Entry No.");
            UnapplyCustEntries.LOOKUPMODE(TRUE);
            UnapplyCustEntries.RUNMODAL();
        END;
    end;

    procedure PostUnApplyCustomer(var DtldCustLedgEntryBuf: Record "Detailed Cust. Ledg. Entry"; DtldCustLedgEntry2: Record "Detailed Cust. Ledg. Entry"; var DocNo: Code[20]; var PostingDate: Date)
    var
        GLEntry: Record "G/L Entry";
        CustLedgEntry: Record "Cust. Ledger Entry";
        SourceCodeSetup: Record "Source Code Setup";
        GenJnlLine: Record "Gen. Journal Line";
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        DateComprReg: Record "Date Compr. Register";
        GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line";
        Window: Dialog;
        ApplicationEntryNo: Integer;
        LastTransactionNo: Integer;
        PostingDateChecked: Boolean;
    begin
        IF NOT DtldCustLedgEntryBuf.FIND('-') THEN
            ERROR(Text010Err);
        IF NOT CONFIRM(Text011Qst + Text007Qst, FALSE) THEN
            EXIT;
        MaxPostingDate := 0D;
        GLEntry.LOCKTABLE();
        DtldCustLedgEntry.LOCKTABLE();
        CustLedgEntry.LOCKTABLE();
        CustLedgEntry.GET(DtldCustLedgEntry2."Cust. Ledger Entry No.");
        CheckPostingDate(PostingDate, '', 0);
        DtldCustLedgEntry.SETCURRENTKEY("Transaction No.", "Customer No.", "Entry Type");
        DtldCustLedgEntry.SETRANGE("Transaction No.", DtldCustLedgEntry2."Transaction No.");
        DtldCustLedgEntry.SETRANGE("Customer No.", DtldCustLedgEntry2."Customer No.");
        IF DtldCustLedgEntry.FIND('-') THEN
            REPEAT
                IF (DtldCustLedgEntry."Entry Type" <> DtldCustLedgEntry."Entry Type"::"Initial Entry") AND
                   NOT DtldCustLedgEntry.Unapplied
                THEN BEGIN
                    IF NOT PostingDateChecked THEN BEGIN
                        CheckPostingDate(
                          DtldCustLedgEntry."Posting Date",
                          DtldCustLedgEntry.TABLECAPTION,
                          DtldCustLedgEntry."Entry No.");
                        CheckAdditionalCurrency(PostingDate, DtldCustLedgEntry."Posting Date");
                        PostingDateChecked := TRUE;
                    END;
                    CheckReversal(DtldCustLedgEntry."Cust. Ledger Entry No.");
                    IF DtldCustLedgEntry."Entry Type" = DtldCustLedgEntry."Entry Type"::Application then begin
                        ApplicationEntryNo :=
                          FindLastApplEntry(DtldCustLedgEntry."Cust. Ledger Entry No.");
                        IF (ApplicationEntryNo <> 0) AND (ApplicationEntryNo <> DtldCustLedgEntry."Entry No.") THEN
                            ERROR(Text012Err, CustLedgEntry.TABLECAPTION, DtldCustLedgEntry."Cust. Ledger Entry No.");
                    END;
                    LastTransactionNo := FindLastTransactionNo(DtldCustLedgEntry."Cust. Ledger Entry No.");
                    IF (LastTransactionNo <> 0) AND (LastTransactionNo <> DtldCustLedgEntry."Transaction No.") THEN
                        ERROR(
                          Text015Err,
                          CustLedgEntry.TABLECAPTION,
                          DtldCustLedgEntry."Cust. Ledger Entry No.",
                          CustLedgEntry.FIELDCAPTION("Transaction No."));
                END;
            UNTIL DtldCustLedgEntry.NEXT() = 0;

        DateComprReg.CheckMaxDateCompressed(MaxPostingDate, 0);

        WITH DtldCustLedgEntry2 DO BEGIN
            SourceCodeSetup.GET();
            CustLedgEntry.GET("Cust. Ledger Entry No.");
            GenJnlLine."Document No." := DocNo;
            GenJnlLine."Posting Date" := PostingDate;
            GenJnlLine."Account Type" := GenJnlLine."Account Type"::Customer;
            GenJnlLine."Account No." := "Customer No.";
            GenJnlLine.Correction := TRUE;
            GenJnlLine."Document Type" := GenJnlLine."Document Type"::" ";
            GenJnlLine.Description := CustLedgEntry.Description;
            GenJnlLine."Shortcut Dimension 1 Code" := CustLedgEntry."Global Dimension 1 Code";
            GenJnlLine."Shortcut Dimension 2 Code" := CustLedgEntry."Global Dimension 2 Code";
            GenJnlLine."Posting Group" := CustLedgEntry."Customer Posting Group";
            GenJnlLine."Source Type" := GenJnlLine."Source Type"::Customer;
            GenJnlLine."Source No." := "Customer No.";
            GenJnlLine."Source Code" := SourceCodeSetup."Unapplied Sales Entry Appln.";
            GenJnlLine."System-Created Entry" := TRUE;
            Window.OPEN(Text008Msg);
            GenJnlPostLine.UnapplyCustLedgEntry(GenJnlLine, DtldCustLedgEntry2);
            DtldCustLedgEntryBuf.DELETEALL();
            DocNo := '';
            PostingDate := 0D;
            COMMIT();
            Window.CLOSE();
            MESSAGE(Text009Msg);
        END;
    end;

    local procedure CheckPostingDate(PostingDate: Date; Caption: Text[50]; EntryNo: Integer)
    var
        CustLedgEntry: Record "Cust. Ledger Entry";
    begin
        if GenJnlCheckLine.DateNotAllowed(PostingDate) then begin
            if Caption <> '' THEN
                ERROR(Text013Err, CustLedgEntry.FIELDCAPTION("Posting Date"), Caption, EntryNo)
            ELSE
                ERROR(Text014Err, CustLedgEntry.FIELDCAPTION("Posting Date"));
        END;
        IF PostingDate > MaxPostingDate THEN
            MaxPostingDate := PostingDate;
    end;

    local procedure CheckAdditionalCurrency(OldPostingDate: Date; NewPostingDate: Date)
    var
        GLSetup: Record "General Ledger Setup";
        CurrExchRate: Record "Currency Exchange Rate";
    begin
        IF OldPostingDate = NewPostingDate THEN
            EXIT;
        GLSetup.GET();
        IF GLSetup."Additional Reporting Currency" <> '' THEN
            IF CurrExchRate.ExchangeRate(OldPostingDate, GLSetup."Additional Reporting Currency") <>
               CurrExchRate.ExchangeRate(NewPostingDate, GLSetup."Additional Reporting Currency")
            THEN
                ERROR(Text016Err, NewPostingDate);
    end;

    procedure CheckReversal(CustLedgEntryNo: Integer)
    var
        CustLedgEntry: Record "Cust. Ledger Entry";
    begin
        CustLedgEntry.GET(CustLedgEntryNo);
        IF CustLedgEntry.Reversed THEN
            ERROR(Text017Err, CustLedgEntry.TABLECAPTION, CustLedgEntryNo);
    end;
}

