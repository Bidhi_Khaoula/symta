codeunit 50020 "Export Mail V370"
{

    trigger OnRun()
    begin
    end;

    var
        //TODO MAPIHandler: Automation;
        HideDialog: Boolean;
       

    procedure OpenNewMessage(ToName: Text[80]): Boolean
    begin
        NewMessage(ToName, '', '', '', '', TRUE);
    end;

    procedure NewMessage(ToName: Text[80]; CCName: Text[80]; Subject: Text[260]; Body: Text[260]; AttachFileName: Text[260]; OpenDialog: Boolean) MailSent: Boolean
    var
        
    begin

        //TODO
        /*IF ISCLEAR(MAPIHandler) THEN BEGIN
            SLEEP(1000);
            CREATE(MAPIHandler);
        END
        ELSE
            SLEEP(1000);


        ErrorNo := 0;
        MAPIHandler.ToName := ToName;
        MAPIHandler.CCName := CCName;
        MAPIHandler.Subject := Subject;
        IF Body <> '' THEN
            MAPIHandler.Body := Body;
        MAPIHandler.AttachFileName := AttachFileName;
        MAPIHandler.OpenDialog := OpenDialog;


        SLEEP(1000);
        MailSent := MAPIHandler.Send;
        ErrorNo := MAPIHandler.ErrorStatus;
        txt := MAPIHandler.ErrorDescription;
        MESSAGE('%1', txt);*/
    end;
    //TODO
    /*procedure AddBodyline(TextLine: Text[260]): Boolean
    begin
        IF ISCLEAR(MAPIHandler) THEN
            CREATE(MAPIHandler);

        IF TextLine <> '' THEN
            EXIT(MAPIHandler.AddBodyText(TextLine));
    end;

    procedure Send() MailSent: Boolean
    begin
        MailSent := MAPIHandler.Send;
        ErrorNo := MAPIHandler.ErrorStatus;
    end;

    procedure GetErrorCode(): Integer
    begin
        EXIT(MAPIHandler.ErrorStatus);
    end;

    procedure GetErrorDesc(): Text[260]
    begin
        EXIT(MAPIHandler.ErrorDescription);
    end;
*/
    procedure SetDialogState(NewHideDialog: Boolean)
    begin
        HideDialog := NewHideDialog;
    end;

    procedure ShowAddresses(ContactNo: Code[20]; ContAltAddrCode: Code[10]): Text[260]
    var
        TempContactThrough: Record "Communication Method" temporary;
        Contact: Record Contact;
        Contact2: Record Contact;
        ContAltAddr: Record "Contact Alt. Address";
        ContAltAddrCode2: Code[10];
        I: Integer;
    begin
        IF NOT Contact.GET(ContactNo) THEN
            EXIT;

        IF ContAltAddrCode = '' THEN
            ContAltAddrCode2 := Contact.ActiveAltAddress(TODAY);

        WITH TempContactThrough DO BEGIN
            INIT();
            "Contact No." := ContactNo;
            Name := Contact.Name;
            IF Contact."E-Mail" <> '' THEN BEGIN
                I := I + 1;
                Description := Contact.FIELDCAPTION("E-Mail");
                "E-Mail" := Contact."E-Mail";
                Type := Contact.Type;
                INSERT();
            END;

            // Alternative address
            IF ContAltAddr.GET(Contact."No.", ContAltAddrCode2) THEN
                IF ContAltAddr."E-Mail" <> '' THEN BEGIN
                    I := I + 1;
                    Key := I;
                    Description := TrimCode(ContAltAddr.Code) + ' - ' + ContAltAddr.FIELDCAPTION("E-Mail");
                    "E-Mail" := ContAltAddr."E-Mail";
                    Type := Contact.Type;
                    INSERT();
                END;
        END;

        // Get linked Company Addresses
        IF (Contact.Type = Contact.Type::Person) AND (Contact."Company No." <> '') THEN BEGIN
            Contact2.GET(Contact."Company No.");

            IF ContAltAddrCode = '' THEN
                ContAltAddrCode2 := Contact2.ActiveAltAddress(TODAY);

            WITH TempContactThrough DO BEGIN
                "Contact No." := ContactNo;
                Name := Contact2.Name;
                IF Contact2."E-Mail" <> '' THEN BEGIN
                    I := I + 1;
                    Key := I;
                    Description := Contact2.FIELDCAPTION("E-Mail");
                    "E-Mail" := Contact2."E-Mail";
                    Type := Contact2.Type;
                    INSERT();
                END;

                // Alternative address
                IF ContAltAddr.GET(Contact2."No.", ContAltAddrCode2) THEN BEGIN
                    IF ContAltAddr."E-Mail" <> '' THEN BEGIN
                        I := I + 1;
                        Key := I;
                        Description := TrimCode(ContAltAddr.Code) + ' - ' + ContAltAddr.FIELDCAPTION("E-Mail");
                        "E-Mail" := ContAltAddr."E-Mail";
                        Type := Contact2.Type;
                        INSERT();
                    END;
                END;
            END;
        END;

        IF page.RUNMODAL(page::"Contact Through", TempContactThrough) = ACTION::LookupOK THEN
            EXIT(TempContactThrough."E-Mail");
    end;

    local procedure TrimCode("Code": Code[20]) TrimString: Text[20]
    begin
        TrimString := COPYSTR(Code, 1, 1) + LOWERCASE(COPYSTR(Code, 2, STRLEN(Code) - 1))
    end;

    procedure MailContCustVendBank(TableNo: Integer; No: Code[20]; Email: Text[80])
    var
        InteractionTmplSetup: Record "Interaction Template Setup";
        ContBusRel: Record "Contact Business Relation";
        Contact: Record Contact;
        TempSegmentLine: Record "Segment Line" temporary;
        Mail: Codeunit "Export Mail V370";
    begin
        // Changement du Mail cu 393 en 50020

        InteractionTmplSetup.GET();

        IF InteractionTmplSetup."E-Mails" = '' THEN BEGIN
            Mail.OpenNewMessage(Email);
            EXIT;
        END;

        IF TableNo = DATABASE::Contact THEN
            Contact.GET(No)
        ELSE BEGIN
            ContBusRel.RESET();
            ContBusRel.SETCURRENTKEY("Link to Table", "No.");
            CASE TableNo OF
                DATABASE::Customer:
                    ContBusRel.SETRANGE("Link to Table", ContBusRel."Link to Table"::Customer);
                DATABASE::Vendor:
                    ContBusRel.SETRANGE("Link to Table", ContBusRel."Link to Table"::Vendor);
                DATABASE::"Bank Account":
                    ContBusRel.SETRANGE("Link to Table", ContBusRel."Link to Table"::"Bank Account");
            END;
            ContBusRel.SETRANGE("No.", No);
            IF ContBusRel.FindFirst() THEN
                Contact.GET(ContBusRel."Contact No.")
            ELSE BEGIN
                Mail.OpenNewMessage(Email);
                EXIT;
            END;
        END;

        TempSegmentLine.DELETEALL();
        TempSegmentLine.INIT();
        WITH Contact DO BEGIN
            IF Type = Type::Person THEN
                TempSegmentLine.SETRANGE("Contact No.", "No.")
            ELSE
                TempSegmentLine.SETRANGE("Contact Company No.", "Company No.");
            IF Email <> '' THEN
                TempSegmentLine."Contact Via" := Email
            ELSE
                TempSegmentLine."Contact Via" := "E-Mail";
            TempSegmentLine.VALIDATE("Contact No.", "No.");
            TempSegmentLine.VALIDATE(Date, TODAY);
            TempSegmentLine.Insert();
            TempSegmentLine.VALIDATE("Interaction Template Code", InteractionTmplSetup."E-Mails");
            TempSegmentLine."Correspondence Type" := TempSegmentLine."Correspondence Type"::"Email";
            TempSegmentLine.MODIFY();
        END;

        //TODO page.RUNMODAL(page::"Create Mail", TempSegmentLine);
    end;
}

