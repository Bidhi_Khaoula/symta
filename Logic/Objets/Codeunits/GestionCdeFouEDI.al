codeunit 50026 "Gestion Cde Fou EDI"
{

    trigger OnRun()
    begin
    end;

    var
        ESK001Msg: Label 'Terminé !';

    procedure ExportCommande(_pNoDoucment: Code[20])
    var

        "InfoSociété": Record "Company Information";
        PurchHeader: Record "Purchase Header";
        PurchLine: Record "Purchase Line";
        LVendor: Record Vendor;
        Lparam: Record "Generals Parameters";
        Fichier: File;
        NomFichier: Text[60];
        LNoLigne: Integer;

    begin
        PurchHeader.RESET();
        PurchHeader.SETRANGE("Document Type", PurchHeader."Document Type"::Order);
        PurchHeader.SETRANGE("No.", _pNoDoucment);
        PurchHeader.FINDFIRST();


        LVendor.GET(PurchHeader."Buy-from Vendor No.");
        Lparam.GET('FIC_EDI', LVendor."Code Cde EDI");
        Lparam.TESTFIELD(LongDescription2);


        Fichier.TEXTMODE(TRUE);
        Fichier.WRITEMODE(TRUE);

        InfoSociété.GET();


        CASE LVendor."Code Cde EDI" OF
            'CDE_SKF':
                NomFichier := InfoSociété."Export EDI Folder" + '\SKF' + _pNoDoucment + '.txt';
            'CDE_LAV':
                NomFichier := InfoSociété."Export EDI Folder" + '\' + LVendor."Vendor EDI Code" + '.txt';
            'CDE_TIM':
                NomFichier := InfoSociété."Export EDI Folder" + '\' + _pNoDoucment + '.txt';
            ELSE
                NomFichier := InfoSociété."Export EDI Folder" + '\' + Lparam.LongDescription2;
        END;


        //IF FILE.EXISTS(NomFichier) THEN
        //  Fichier.OPEN(NomFichier)
        //ELSE
        Fichier.CREATE(NomFichier);

        Fichier.SEEK(Fichier.LEN);


        CASE LVendor."Code Cde EDI" OF
            'CDE_SKF':
                EnteteSKF(PurchHeader, Fichier);
            'CDE_LAV':
                EnteteLAVERDA(PurchHeader, Fichier);
        END;

        LNoLigne := 0;

        PurchLine.RESET();
        PurchLine.SETRANGE("Document Type", PurchHeader."Document Type");
        PurchLine.SETRANGE("Document No.", PurchHeader."No.");
        PurchLine.SETRANGE(Type, PurchLine.Type::Item);
        IF PurchLine.FINDFIRST() THEN
            REPEAT
                LNoLigne += 1;
                CASE LVendor."Code Cde EDI" OF
                    'CDE_SKF':
                        LigneSKF(PurchLine, LNoLigne, Fichier);
                    'CDE_LAV':
                        LigneLAVERDA(PurchLine, LNoLigne, Fichier);
                    'CDE_TIM':
                        LigneTIMKEN(PurchLine, LNoLigne, Fichier);
                END;


            UNTIL PurchLine.NEXT() = 0;


        MESSAGE(ESK001Msg);
    end;

    procedure EnteteSKF(_pEntete: Record "Purchase Header"; var _Fichier: File)
    var
        LCompanyInfo: Record "Company Information";
        LPays: Record "Country/Region";
        LVendor: Record Vendor;
    begin

        LCompanyInfo.GET();


        _Fichier.WRITE('H010' + _pEntete."No.");
        _Fichier.WRITE('H020' + FORMAT(_pEntete."Order Date"));
        _Fichier.WRITE('H030' + _pEntete."Vendor Order No.");
        _Fichier.WRITE('H040' + FORMAT(_pEntete."Type de commande"));
        _Fichier.WRITE('H050' + FORMAT(_pEntete."Requested Receipt Date"));
        _Fichier.WRITE('H060' + _pEntete."Order Create User");
        _Fichier.WRITE('H070' + '        ');
        _Fichier.WRITE('H080' + LCompanyInfo.Name);
        _Fichier.WRITE('H090' + LCompanyInfo.Address);
        _Fichier.WRITE('H100' + LCompanyInfo."Address 2");
        _Fichier.WRITE('H110' + LCompanyInfo."Post Code");
        _Fichier.WRITE('H120' + LCompanyInfo.City);
        LPays.GET(LCompanyInfo."Country/Region Code");
        _Fichier.WRITE('H130' + UPPERCASE(LPays.Name));

        LVendor.GET(_pEntete."Buy-from Vendor No.");
        _Fichier.WRITE('H140' + LVendor."Vendor EDI Code");
        _Fichier.WRITE('H150' + _pEntete."Buy-from Vendor Name");
        _Fichier.WRITE('H160' + _pEntete."Buy-from Address");
        _Fichier.WRITE('H170' + _pEntete."Buy-from Address 2");
        _Fichier.WRITE('H180' + _pEntete."Pay-to Post Code");
        _Fichier.WRITE('H190' + _pEntete."Buy-from City");
        LPays.GET(_pEntete."Pay-to Country/Region Code");
        _Fichier.WRITE('H200' + UPPERCASE(LPays.Name));
        _Fichier.WRITE('H210' + '        ');
        _Fichier.WRITE('H220' + _pEntete."Ship-to Name");
        _Fichier.WRITE('H230' + _pEntete."Ship-to Address");
        _Fichier.WRITE('H240' + _pEntete."Ship-to Address 2");
        _Fichier.WRITE('H250' + _pEntete."Ship-to Post Code");
        _Fichier.WRITE('H260' + _pEntete."Ship-to City");
        LPays.GET(_pEntete."Ship-to Country/Region Code");
        _Fichier.WRITE('H270' + UPPERCASE(LPays.Name));
    end;

    procedure EnteteLAVERDA(_pEntete: Record "Purchase Header"; var _Fichier: File)
    begin
        _Fichier.WRITE('C440004|6A40|7|1|' + _pEntete."No." + '|000|EUR||||' +
            FORMAT(_pEntete."Order Date", 0, '<Year4><Month,2><Day,2>'));
    end;

    procedure LigneSKF(_pLigne: Record "Purchase Line"; _pNoLigne: Integer; var _Fichier: File)
    var
        _RefFourni: Code[20];
        Ligne: Text[1024];
    begin

        IF _pLigne."Vendor Item No." = '' THEN
            _RefFourni := COPYSTR(_pLigne."Recherche référence", 4, STRLEN(_pLigne."Recherche référence"))
        ELSE
            _RefFourni := _pLigne."Item Reference No.";


        Ligne := 'L';
        Ligne += FORMAT(_pNoLigne, 0, '<Integer,4><Filler Character, >');
        Ligne += PADSTR(_RefFourni, 20, ' ');
        Ligne += PADSTR(_pLigne.Description, 30, ' ');
        Ligne += FORMAT(_pLigne.Quantity, 0, '<Precision,2:2><Sign,1><Integer,8><Filler Character, ><Decimals><Comma,.>');
        Ligne += FORMAT(_pLigne."Net Unit Cost", 0, '<Precision,3:3><Sign,1><Integer,10><Filler Character, ><Decimals><Comma,.>');
        Ligne += PADSTR(_pLigne."Recherche référence", 20, ' ');
        _Fichier.WRITE(Ligne);
    end;

    procedure LigneLAVERDA(_pLigne: Record "Purchase Line"; _pNoLigne: Integer; var _Fichier: File)
    var
        _RefFourni: Code[20];
        Ligne: Text[1024];
        i: Integer;
    begin

        IF _pLigne."Vendor Item No." = '' THEN BEGIN
            IF EVALUATE(i, _pLigne."Recherche référence") THEN
                _RefFourni := FORMAT(i, 0, '<Integer,11><Filler Character,0>') + '0'
            ELSE
                _RefFourni := _pLigne."Recherche référence";
        END
        ELSE
            _RefFourni := _pLigne."Item Reference No.";


        Ligne := _RefFourni + '|';
        Ligne += FORMAT(_pLigne.Quantity, 0, '<Integer,6><Filler Character,0>') + '|';
        _Fichier.WRITE(Ligne);
    end;

    procedure LigneTIMKEN(_pLigne: Record "Purchase Line"; _pNoLigne: Integer; var _Fichier: File)
    var

        Ligne: Text[1024];
        i: Integer;
    begin


        Ligne := FORMAT(_pLigne.Quantity, 0, 1) + ';';
        Ligne += _pLigne.Description + ';';
        Ligne += FORMAT(_pLigne."Requested Receipt Date", 0, '<Day,2>.<Month,2>.<Year4>');

        _Fichier.WRITE(Ligne);
    end;
}

