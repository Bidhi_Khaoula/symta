codeunit 50029 "Gestion MINILOAD"
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    var

    begin
        // Gestion des messages retours CSO
        GetMsgCSO();
        // Gestion des messages retours CEP
        GetMsgCEP();
        // Gestion des messages d'erreurs renvoyés par le MiniLoad
        GetMsgEIM();
    end;

    var
        Char13: Char;
        Char10: Char;
        i: Integer;
        DebutFichierLbl: Label '´ˆŠ';
        ErreurArticleLbl: Label 'Le code article ne correspond pas entre le fichier et la ligne.';
        // ErreurQuantite: Label 'La quantité est déjà renseignée pour cette ligne.';
        ErreurFeuilleLbl: Label 'La feuille %1 n''existe pas/plus !', Comment = '%1 = Feuille Article';
        SortieQst: Label 'Aucune réponse. Voulez vous interrompre le traitement ?';
        txtFolderIN: Text[1024];
        txtFolderOut: Text[1024];

    procedure InitGlobal()
    var
        rec_InventorySetup: Record "Inventory Setup";
    begin
        Char13 := 13;
        Char10 := 10;


        rec_InventorySetup.GET();
        txtFolderOut := rec_InventorySetup."Message send to MiniLoad";
        txtFolderIN := rec_InventorySetup."Message receive from MiniLoad";
    end;

    procedure FileExist(pPath: Text[1000]; pName: Text[250]): Boolean
    var
        FileList: Record File;
    begin
        CLEAR(FileList);
        FileList.SETRANGE(Path, pPath);
        FileList.SETRANGE("Is a file", TRUE);
        FileList.SETFILTER(Name, pName);
        IF FileList.FINDFIRST() THEN
            MESSAGE('%1', FileList);
        EXIT(Not FileList.IsEmpty())
    end;

    procedure OpenFile(var _PNomFichier: Text[50]; var _pFichier: File)
    var
        _PNomFichierComplet: Text[250];
    begin


        _pFichier.TEXTMODE(TRUE);
        _pFichier.WRITEMODE(TRUE);

        _PNomFichier := _PNomFichier + FORMAT(WORKDATE(), 0, '<Year4><Month,2><Day,2>') +
                         FORMAT(TIME, 0, '<Hour,2><Minute,2><Second,2>') + '.txt';
        _PNomFichierComplet := txtFolderOut + _PNomFichier;


        IF FILE.EXISTS(_PNomFichierComplet) THEN
            _pFichier.OPEN(_PNomFichierComplet)
        ELSE
            _pFichier.CREATE(_PNomFichierComplet);
    end;

    local procedure LoadFile(NomFic: Text[1024]; Position: Integer; Longueur: Integer) rtn_txt: Text[1024]
    var

        LineFile: BigText;
        HeaderFile: File;
        Lstream: InStream;

    begin
        // Fonction permettant de gérer la lecture d'un fichier texte dans un blob
        HeaderFile.TEXTMODE(TRUE);
        HeaderFile.OPEN(NomFic);

        HeaderFile.CREATEINSTREAM(Lstream);
        LineFile.READ(Lstream);

        IF LineFile.LENGTH <> 0 THEN
            LineFile.GETSUBTEXT(rtn_txt, Position, Longueur);

    end;

    procedure FormatNumerique(_Valeur: Decimal; _pNbCaractere: Integer; _pNbDecimal: Integer): Text[30]

    begin
        IF _pNbDecimal > 1 THEN
            FOR i := 1 TO _pNbDecimal DO _Valeur := _Valeur * 10;

        EXIT(FORMAT(_Valeur, 0, '<Integer,' + FORMAT(_pNbCaractere) + '><Filler Character,0>'));
    end;

    procedure FormatTexteToNumerique(_Valeur: Text[50]; _pNbCaractere: Integer; _pNbDecimal: Integer) rtn_dec: Decimal

    begin
        EVALUATE(rtn_dec, _Valeur);
        IF _pNbDecimal > 1 THEN
            FOR i := 1 TO _pNbDecimal DO rtn_dec := rtn_dec / 10;
    end;

    procedure UpdateEntries(_txtMessage: Text[250])
    var
        rec_Message: Record "Message send to MINILOAD";
    begin
        //** Cette fonction permet de mettre à jour les écritures MINILOAD : Table ou est stocké tt les envois de fichiers **//
        WITH rec_Message DO BEGIN
            INIT();
            "Entry No." := GetNextEntryNo();
            Evaluate("User ID", USERID);
            Evaluate("Message No.", _txtMessage);
            Date := TODAY;
            Heure := TIME;
            INSERT();
        END;
    end;

    procedure GetPrinterName(_UserID: Code[20]; _ReportNumber: Integer) rtn_txt: Text[1024]
    var
        rec_SelectionPrinter: Record "Printer Selection";
    begin
        //** Cette fonction ramène l'imprimante par défaut pour un utilisateur et un numéro de report l'imprimante **//
        IF rec_SelectionPrinter.GET(_UserID, _ReportNumber) THEN
            rtn_txt := rec_SelectionPrinter."Printer Name";
    end;

    procedure GetDate(txtDte: Text[8]) rtn_dte: Date
    var
        int_day: Integer;
        int_month: Integer;
        int_year: Integer;
    begin
        //** Cette fonction renvoit une date au bon format à partir de YYYYMMDD //**
        EVALUATE(int_year, COPYSTR(txtDte, 1, 4));
        EVALUATE(int_month, COPYSTR(txtDte, 5, 2));
        EVALUATE(int_day, COPYSTR(txtDte, 7, 2));

        rtn_dte := DMY2DATE(int_day, int_month, int_year);
    end;

    procedure "---- EXPORT DE FICHIER ----"()
    begin
    end;

    procedure TypeArticle_MsgTPR(var _pMarque: Record Manufacturer)
    var
        LNomFichier: Text[50];
        LFichier: File;
        LTabTxt: array[100] of Text[500];
        LLigne: Text[1024];
        LLigne2: Text[1024];
    begin
        InitGlobal();

        LNomFichier := 'TPR01';
        OpenFile(LNomFichier, LFichier);

        IF _pMarque.FINDFIRST() THEN
            REPEAT


                CLEAR(LTabTxt);
                LTabTxt[1] := 'F';
                LTabTxt[2] := PADSTR(_pMarque.Code, 12);
                LTabTxt[3] := PADSTR(_pMarque.Name, 80);
                LTabTxt[4] := PADSTR(' ', 12);



                LLigne := '';
                LLigne2 := '';

                FOR i := 1 TO 4 DO LLigne += LTabTxt[i];


                LFichier.WRITE(LLigne + LLigne2);


                _pMarque."Envoyé MINILOAD" := TRUE;
                _pMarque."Date Envoi MINILOAD" := WORKDATE();
                _pMarque."Heure Envoi MINILOAD" := TIME;
                _pMarque.MODIFY();


            UNTIL _pMarque.NEXT() = 0;


        // Mise à jour de la table historique
        UpdateEntries(LNomFichier);

        LFichier.CLOSE();
    end;

    procedure Article_MsgPRO(var _pItem: Record Item)
    var
        LUOM: Record "Unit of Measure";
        LCalcConso: Codeunit CalculConso;
        LNomFichier: Text[50];
        LFichier: File;
        LTabTxt: array[100] of Text[500];
        LLigne: Text[1024];
        LLigne2: Text[1024];
        LConso: Decimal;
    begin
        InitGlobal();

        LNomFichier := 'PRO07';
        OpenFile(LNomFichier, LFichier);

        IF _pItem.FINDFIRST() THEN
            REPEAT

                // Conso sur 1 an
                LConso := LCalcConso.RechecherConsommation(_pItem."No.", CALCDATE('<1Y>', WORKDATE()), WORKDATE());
                IF LConso > 50 THEN
                    _pItem."Phys Invt Counting Period Code" := 'A'
                ELSE
                    IF LConso > 20 THEN
                        _pItem."Phys Invt Counting Period Code" := 'B'
                    ELSE
                        _pItem."Phys Invt Counting Period Code" := 'C';

                IF NOT LUOM.GET(_pItem."Base Unit of Measure") THEN
                    _pItem."Base Unit of Measure" := '1';
                LUOM.GET(_pItem."Base Unit of Measure");

                CLEAR(LTabTxt);
                LTabTxt[1] := 'F';
                LTabTxt[2] := PADSTR(_pItem."No.", 50);
                LTabTxt[3] := PADSTR(' ', 50);
                LTabTxt[4] := PADSTR(' ', 50);
                LTabTxt[5] := PADSTR(_pItem."Manufacturer Code", 12);
                LTabTxt[6] := PADSTR('0', 1);
                LTabTxt[7] := PADSTR(_pItem."Phys Invt Counting Period Code", 2);
                LTabTxt[8] := PADSTR(' ', 186);
                // MC Le 10-05-2012 => On rajoute la référence active en plus de la description
                LTabTxt[9] := PADSTR(_pItem."No. 2" + '' + _pItem.Description, 255);
                // FIN MC Lee 10-05-2012
                LTabTxt[10] := PADSTR(' ', 389);
                LTabTxt[11] := PADSTR(_pItem."Base Unit of Measure", 10);
                LTabTxt[12] := PADSTR(LUOM.Description, 30);

                // Gestion de ALLOW COMMINGLE et ALLOW SPLIT
                LTabTxt[13] := PADSTR('00', 32);
                LTabTxt[14] := PADSTR('0', 1);
                LTabTxt[15] := PADSTR(' ', 12);
                LTabTxt[16] := PADSTR(FORMAT(_pItem."Box Type"), 10);
                // Gestion du champ PACKAGE
                LTabTxt[17] := PADSTR('0', 1);
                LTabTxt[18] := PADSTR('ID', 2);
                LTabTxt[19] := PADSTR(' ', 421);
                LTabTxt[20] := '0000000001';

                LLigne := '';
                FOR i := 1 TO 10 DO LLigne += LTabTxt[i];

                LLigne2 := '';
                FOR i := 11 TO 20 DO LLigne2 += LTabTxt[i];

                LFichier.WRITE(LLigne + LLigne2);

                // Gestion du détail préparation
                CLEAR(LTabTxt);
                LTabTxt[1] := '/';
                IF _pItem."Envoyé MINILOAD" THEN
                    LTabTxt[2] := 'M'
                ELSE
                    LTabTxt[2] := 'F';
                LTabTxt[3] := PADSTR(_pItem."Base Unit of Measure", 10);
                LTabTxt[4] := PADSTR(LUOM.Description, 30);
                LTabTxt[5] := PADSTR(FormatNumerique(1, 12, 5), 12);
                LTabTxt[6] := PADSTR(FormatNumerique(_pItem."Net Weight", 12, 5), 12);
                LTabTxt[7] := PADSTR(' ', 48);
                LTabTxt[8] := PADSTR(FORMAT(_pItem."Box Type"), 10);
                LTabTxt[9] := PADSTR(FORMAT(_pItem."Box Type"), 60);
                LTabTxt[10] := PADSTR(' ', 61);
                LTabTxt[11] := PADSTR(FormatNumerique(1, 12, 5), 12);
                LTabTxt[12] := PADSTR(' ', 18);

                LLigne := '';
                FOR i := 1 TO 12 DO LLigne += LTabTxt[i];
                LFichier.WRITE(LLigne);


                _pItem."Envoyé MINILOAD" := TRUE;
                _pItem."Date Envoi MINILOAD" := WORKDATE();
                _pItem."Heure Envoi MINILOAD" := TIME;
                _pItem.MODIFY();


            UNTIL _pItem.NEXT() = 0;

        // Mise à jour de la table historique
        UpdateEntries(LNomFichier);

        LFichier.CLOSE();
    end;

    procedure Article_MsgPST(var _pItem: Record Item; _txtLocationCode: Code[20])
    var
        LNomFichier: Text[50];
        LFichier: File;
        LTabTxt: array[100] of Text[500];
        LLigne: Text[1024];
    begin
        //** Cette fonction permet de créer un fichier .PST, le fichier doit demander au miniload le stock de l'article **//
        InitGlobal();

        LNomFichier := 'PST04';
        OpenFile(LNomFichier, LFichier);

        IF _pItem.FINDFIRST() THEN BEGIN
            CLEAR(LTabTxt);
            LTabTxt[1] := PADSTR(_txtLocationCode, 10);
            LTabTxt[2] := PADSTR(_pItem."No.", 50);
            LTabTxt[3] := PADSTR(' ', 50);
            LTabTxt[4] := PADSTR(' ', 50);
            LTabTxt[5] := PADSTR(' ', 14);
            LTabTxt[6] := PADSTR(' ', 12);
            ;
            LTabTxt[7] := PADSTR(' ', 3);
            LTabTxt[8] := PADSTR(' ', 50);
            LTabTxt[9] := PADSTR(' ', 14);
            LTabTxt[10] := PADSTR(' ', 12);
            LTabTxt[11] := 'T';
            LTabTxt[12] := PADSTR(' ', 305);

            LLigne := '';
            FOR i := 1 TO 12 DO LLigne += LTabTxt[i];

            LFichier.WRITE(LLigne);
        END;

        // Mise à jour de la table historique
        UpdateEntries(LNomFichier);

        LFichier.CLOSE();
    end;

    procedure "EntréesSortie_MsgSOR"(var _pItemJnlLine: Record "Item Journal Line")
    var

        rec_ItemJnlBatch: Record "Item Journal Batch";
        LNomFichier: Text[50];
        LFichier: File;
        LTabTxt: array[100] of Text[1000];
        LLigne: Text[1024];
        LLigne2: Text[1024];
    begin
        InitGlobal();

        LNomFichier := 'SOR09';
        OpenFile(LNomFichier, LFichier);

        IF _pItemJnlLine.FINDFIRST() THEN BEGIN
            CLEAR(LTabTxt);
            LTabTxt[1] := 'F';
            LTabTxt[2] := PADSTR(_pItemJnlLine."Journal Batch Name", 50);
            LTabTxt[3] := PADSTR(_pItemJnlLine."Location Code", 10);
            LTabTxt[4] := PADSTR('', 100);
            LTabTxt[5] := PADSTR('', 255);
            LTabTxt[6] := PADSTR('CORDER', 30);
            LTabTxt[7] := PADSTR('', 641);
            LTabTxt[8] := PADSTR('', 142);
            //LTabTxt[9] := PADSTR('', 1);
            //LTabTxt[9] := PADSTR('', 40);
            // Gestion de l'information poste de picking
            // On récupère le record ItemJnlBatch pour avoir le champ : Poste de picking
            rec_ItemJnlBatch.GET(_pItemJnlLine."Journal Template Name", _pItemJnlLine."Journal Batch Name");
            LTabTxt[9] := '[' + FORMAT(rec_ItemJnlBatch."Station MiniLoad") + '][][][][][][][][][][][][][][][][][][][]';
            // Nombre de lignes en détail
            LTabTxt[10] := FormatNumerique(_pItemJnlLine.COUNT, 10, 0);

            LLigne := '';
            FOR i := 1 TO 6 DO LLigne += LTabTxt[i];

            LLigne2 := '';
            FOR i := 7 TO 10 DO LLigne2 += LTabTxt[i];

            LFichier.WRITE(LLigne + LLigne2);


            REPEAT


                CLEAR(LTabTxt);
                LTabTxt[1] := '/';
                // MC Le 19-06-2012 => Gestion du champ Line No Miniload
                LTabTxt[2] := FormatNumerique(_pItemJnlLine."Line No for MiniLoad", 10, 0);
                // FIN MC Le 19-06-2012
                LTabTxt[3] := PADSTR(_pItemJnlLine."Item No.", 50);
                LTabTxt[4] := PADSTR('', 50);
                LTabTxt[5] := FormatNumerique(_pItemJnlLine.Quantity, 12, 5);
                LTabTxt[6] := PADSTR('', 124);
                LTabTxt[7] := PADSTR(_pItemJnlLine."Unit of Measure Code", 10);
                LTabTxt[8] := PADSTR('', 1);
                LTabTxt[9] := PADSTR('', 637);
                LTabTxt[10] := PADSTR('0', 1);
                LTabTxt[11] := FormatNumerique(0, 12, 5);
                LTabTxt[12] := PADSTR('0', 1);
                LTabTxt[13] := PADSTR('', 221);
                LTabTxt[14] := PADSTR('', 40);

                LLigne := '';
                FOR i := 1 TO 10 DO LLigne += LTabTxt[i];

                LLigne2 := '';
                FOR i := 11 TO 14 DO LLigne2 += LTabTxt[i];

                LFichier.WRITE(LLigne + LLigne2);




                _pItemJnlLine."Envoyé MINILOAD" := TRUE;
                _pItemJnlLine."Date Envoi MINILOAD" := WORKDATE();
                _pItemJnlLine."Heure Envoi MINILOAD" := TIME;
                _pItemJnlLine.MODIFY();


            UNTIL _pItemJnlLine.NEXT() = 0;
        END;

        // Mise à jour de la table historique
        UpdateEntries(LNomFichier);

        LFichier.CLOSE();
    end;

    procedure "EntréesSortie_MsgREP"(var _pItemJnlLine: Record "Item Journal Line")
    var
        LNomFichier: Text[50];
        LFichier: File;
        LTabTxt: array[100] of Text[1000];
        LLigne: Text[1024];
    begin
        InitGlobal();

        LNomFichier := 'REP03';
        OpenFile(LNomFichier, LFichier);

        IF _pItemJnlLine.FINDFIRST() THEN BEGIN
            CLEAR(LTabTxt);
            LTabTxt[1] := 'A';
            LTabTxt[2] := PADSTR(_pItemJnlLine."Journal Batch Name", 50);
            LTabTxt[3] := PADSTR(_pItemJnlLine."Location Code", 10);
            LTabTxt[4] := PADSTR('TP_IN_01', 20);
            LTabTxt[5] := PADSTR('1', 1);
            LTabTxt[6] := FormatNumerique(_pItemJnlLine.COUNT, 10, 0);

            LLigne := '';
            FOR i := 1 TO 6 DO LLigne += LTabTxt[i];

            LFichier.WRITE(LLigne);


            REPEAT


                CLEAR(LTabTxt);
                LTabTxt[1] := '/';
                LTabTxt[2] := PADSTR(_pItemJnlLine."Item No.", 50);
                LTabTxt[3] := PADSTR(_pItemJnlLine."Unit of Measure Code", 10);
                LTabTxt[4] := FormatNumerique(_pItemJnlLine.Quantity, 12, 5);
                LTabTxt[5] := PADSTR('', 153); //-60
                                               // Champ qualité à activer quand MECALUX aura corrigé
                                               //LTabTxt[6] := PADSTR('1', 50);
                                               // Ligne a supprimer
                LTabTxt[6] := PADSTR('1', 50);

                // FIN Champ Qualité
                LTabTxt[7] := PADSTR('', 140); // +60
                                               // MC Le 19-06-2012 => Gestion du champ Line No Miniload
                LTabTxt[8] := FormatNumerique(_pItemJnlLine."Line No for MiniLoad", 5, 0);
                // FIN MC Le 19-06-2012
                LTabTxt[9] := PADSTR('', 574);

                LLigne := '';
                FOR i := 1 TO 9 DO LLigne += LTabTxt[i];
                LFichier.WRITE(LLigne);




                _pItemJnlLine."Envoyé MINILOAD" := TRUE;
                _pItemJnlLine."Date Envoi MINILOAD" := WORKDATE();
                _pItemJnlLine."Heure Envoi MINILOAD" := TIME;
                _pItemJnlLine.MODIFY();


            UNTIL _pItemJnlLine.NEXT() = 0;
        END;

        // Mise à jour de la table historique
        UpdateEntries(LNomFichier);

        LFichier.CLOSE();
    end;

    procedure GetMsgEIM()
    var
        // CuFile: Codeunit "File Management";
        // NbFile: Integer;
        // newnamfile: Text[320];
        // errnamfile: Text[320];
        PathFile: Text[250];
        // Fichier: File;
        NameFile: Text[100];
    // recFile: Record File;
    //TODO folder: DotNet Directory;
    //TODO Lst: DotNet List_Of_T;
    //TODO Obj: DotNet Object;
    begin
        //** Cette fonction permet d'intégrer les messages EIM, ces message sont des messages d'erreurs suite à un reclassement et doivent
        // ** être imprimés de manière automatique sur l'imprimante de l'utilisateur **//

        InitGlobal();
        // Structure du chemin des fichiers dans la variable
        PathFile := txtFolderIN;

        // Nom du fichier
        NameFile := 'EIM';
        // AD Le 18-11-2015 => MIG2015
        /*
        // Recherche des fichiers
        // -------------------------------------
        // Recherche des fichiers
        // -------------------------------------
        // Pour actualiser la liste des fichiers il faut changer le filtre et le refaire
        // Incompréhensible mais cela fonctionne
        CLEAR(recFile);
        recFile.SETRANGE(Path,TEMPORARYPATH);
        recFile.SETRANGE("Is a file",FALSE);
        //recFile.SETFILTER(Name, 'toto@*');
         IF recFile.FINDFIRST () THEN;
        
        recFile.SETRANGE(Path,  PathFile);
        recFile.SETRANGE("Is a file",TRUE);
        recFile.SETFILTER(Name, NameFile+'@*');
         IF recFile.FINDFIRST () THEN
           REPEAT
             NameFile := recFile.Path + recFile.Name;
             IF FILE.EXISTS(NameFile) THEN BEGIN
               // Traitement du fichier
               ReadEIMFile(NameFile);
               // Sauvegarde du fichier
               newnamfile:= txtFolderIN + '\save\' + recFile.Name;
               RENAME(NameFile,newnamfile);
             END;
           UNTIL recFile.NEXT() = 0;
        */
        /* //TODO
                Obj := folder.GetFiles(PathFile, NameFile + '*');
                Lst := Lst.List;
                Lst.AddRange(Obj);
                //MESSAGE('%1',Lst.Count);
                FOR NbFile := 0 TO Lst.Count - 1 DO BEGIN
                    ReadEIMFile(Lst.Item(NbFile));

                    CuFile.CopyServerFile(Lst.Item(NbFile), txtFolderIN + 'save\' + CuFile.GetFileName(Lst.Item(NbFile)), TRUE);
                    CuFile.DeleteServerFile(Lst.Item(NbFile));

                END;
        */

        // FIN AD Le 18-11-2015 => MIG2015

    end;

    procedure ReadEIMFile(_pFileName: Text[1024])
    var
        rec_Message: Record "Message receive from  MINILOAD";
        rec_MessageSent: Record "Message send to MINILOAD";
        fil_Import: File;
        str_Line: Text[1024];
        txt_MessageFilter: Text[1024];
    begin
        //** Cette fonction permet la gestion du fichier EIM.
        InitGlobal();

        // Gestion de la lecture di fichier avec variable 'file'
        CLEAR(fil_Import);
        fil_Import.TEXTMODE(TRUE);
        IF fil_Import.OPEN(_pFileName) THEN
            WHILE fil_Import.READ(str_Line) > 0 DO BEGIN
                // Patch pour retirer caractères impromptus du fichier
                str_Line := DELCHR(str_Line, '=', DebutFichierLbl);
                // Pour chaque ligne du fichier on insère uen ligne dans la table 50046
                WITH rec_Message DO BEGIN
                    INIT();
                    "Entry No." := GetNextEntryNo();
                    "Message No." := COPYSTR(str_Line, 1, 50);
                    // Construction du filtre pour imprimer seuelement les messages importés.
                    IF STRPOS(txt_MessageFilter, "Message No.") = 0 THEN
                        IF txt_MessageFilter = '' THEN
                            txt_MessageFilter := "Message No."
                        ELSE
                            txt_MessageFilter += '|' + "Message No.";
                    Code := COPYSTR(str_Line, 117, 5);
                    Description := COPYSTR(str_Line, 122, 250);
                    // Récupération de la date
                    Date := GetDate(COPYSTR(str_Line, 377, 8));
                    EVALUATE(Heure, COPYSTR(str_Line, 385, 6));
                    INSERT();
                END;
            END;

        // Après avoir inséré le message on compare avec le message envoyé pour trouver l'utilisateur pour lequel il faut imprimer l'erreu
        rec_MessageSent.RESET();
        rec_MessageSent.SETRANGE("Message No.", rec_Message."Message No.");
        IF rec_MessageSent.FINDFIRST() THEN;

        // On récupère l'imprimante sur laquelle envoyer l'édition de l'erreur.
        rec_Message.SETFILTER("Message No.", '%1', txt_MessageFilter);
        //txtPrinterName := GetPrinterName(rec_MessageSent."User ID", 50064); // On récupère le nom de l'imprimante à utiliser
        // On imprime le report sur l'imprimante
        REPORT.RUN(50064, FALSE, FALSE, rec_Message);



        // Code avec gestion du blob
        /*
         INIT();
        "Entry No."   := GetNextEntryNo;
        "Message No." := LoadFile(_pFileName, 4, 50);
        Code := LoadFile(_pFileName, 120, 5);
        Description := LoadFile(_pFileName, 125, 250);
        // Récupération de la date
        Date := GetDate(LoadFile(_pFileName, 380, 8));
        EVALUATE(Heure, LoadFile(_pFileName, 388, 6));
        INSERT;
        EVALUATE(chr_St, LoadFile(_pFileName, 394, 1));
        IF NOT (chr_St = Char13) AND NOT (chr_St = Char10) THEN
          EOF := TRUE;
        */

    end;

    procedure GetMsgCSO()
    var
        /*   newnamfile: Text[320];
          errnamfile: Text[320]; */
        PathFile: Text[250];
        // Fichier: File;
        NameFile: Text[100];
    //  recFile: Record File;
    //TODO folder: DotNet Directory;
    //TODO Lst: DotNet List_Of_T;
    //TODO Obj: DotNet Object;
    /*   CuFile: Codeunit "File Management";
      NbFile: Integer; */
    begin
        //** Cette fonction permet d'intégrer les messages CSO ** //

        InitGlobal();
        // Structure du chemin des fichiers dans la variable
        PathFile := txtFolderIN;

        // Nom du fichier
        NameFile := 'CSO';


        /* AD Le 18-11-2015 => MIG2015
        // Recherche des fichiers
        // -------------------------------------
        // Recherche des fichiers
        // -------------------------------------
        // Pour actualiser la liste des fichiers il faut changer le filtre et le refaire
        // Incompréhensible mais cela fonctionne
        CLEAR(recFile);
        recFile.SETRANGE(Path,TEMPORARYPATH);
        recFile.SETRANGE("Is a file",FALSE);
        //recFile.SETFILTER(Name, 'toto@*');
         IF recFile.FINDFIRST () THEN;
        
        recFile.SETRANGE(Path,  PathFile);
        recFile.SETRANGE("Is a file",TRUE);
        recFile.SETFILTER(Name, NameFile+'@*');
         IF recFile.FINDFIRST () THEN
            REPEAT
             NameFile := recFile.Path + recFile.Name;
             IF FILE.EXISTS(NameFile) THEN BEGIN
               // Traitement du fichier
               ReadCSOFile(NameFile);
               // Sauvegarde du fichier
               newnamfile:= txtFolderIN +'save\' + recFile.Name;
               RENAME(NameFile,newnamfile);
             END;
           UNTIL recFile.NEXT() = 0;
        
        */
        /*//TODO
                Obj := folder.GetFiles(PathFile, NameFile + '*');
                Lst := Lst.List;
                Lst.AddRange(Obj);
                MESSAGE('AD : %1', Lst.Count);
                FOR NbFile := 0 TO Lst.Count - 1 DO BEGIN
                    ReadCSOFile(Lst.Item(NbFile));


                    CuFile.CopyServerFile(Lst.Item(NbFile), txtFolderIN + 'save\' + CuFile.GetFileName(Lst.Item(NbFile)), TRUE);
                    CuFile.DeleteServerFile(Lst.Item(NbFile));

                END;
        */

        // FIN AD Le 18-11-2015 => MIG2015

    end;

    procedure ReadCSOFile(_pFileName: Text[1024])
    var
        rec_Message: Record "Message receive from  MINILOAD";
        rec_ItemJnlLine: Record "Item Journal Line";

        CduLFunctions: Codeunit "Codeunits Functions";
        fil_Import: File;
        str_Line: Text[1024];
        int_NbLines: Integer;
        int_NbLinesDetail: Integer;
        i: Integer;
        j: Integer;
        bln_Detail: Boolean;
        txt_CodeMess: Text[30];
        txt_LocationCOde: Code[20];
        txt_NoCommande: Code[20];
        dte_Dte: Date;

        bln_firstline: Boolean;
        test_integer: Integer;
        LItem: Record Item;

    begin
        //** Cette fonction permet la gestion du fichier CSO.
        InitGlobal();
        bln_firstline := TRUE;
        // Gestion de la lecture di fichier avec variable 'file'
        CLEAR(fil_Import);
        fil_Import.TEXTMODE(TRUE);
        IF fil_Import.OPEN(_pFileName) THEN
            REPEAT
                // Patch pour retirer caractères impromptus du fichier
                IF bln_firstline THEN BEGIN
                    // La première ligne du fichier est l'entête de la commande
                    fil_Import.READ(str_Line);
                    // Patch pour retirer caractères impromptus du fichier
                    str_Line := DELCHR(str_Line, '=', DebutFichierLbl);
                    bln_firstline := FALSE;
                END;

                // message('%1-%2',_pFileName,bln_firstline);

                // Récupération des valeurs d'entêtes
                txt_CodeMess := DELSTR(fil_Import.NAME, 1, STRLEN(txtFolderOut));
                txt_LocationCOde := COPYSTR(str_Line, 1, 10);
                txt_NoCommande := COPYSTR(str_Line, 11, 50);
                IF EVALUATE(test_integer, COPYSTR(str_Line, 91, 8)) THEN
                    dte_Dte := GetDate(COPYSTR(str_Line, 91, 8))
                ELSE
                    ERROR('erreur :%1', str_Line);

                // On récupère le nombre de ligne du fichier. (A noter que pour une ligne le fichier contient aussi 'X' lignes de détail)
                EVALUATE(int_NbLines, COPYSTR(str_Line, 359, 10));
                // Après avoir récupéré le nombre de ligne restantes du fichier on boucle dessus
                FOR i := 1 TO int_NbLines DO BEGIN
                    fil_Import.READ(str_Line);
                    // Initialisation de l'enregistrement du message.
                    WITH rec_Message DO BEGIN
                        INIT();
                        "Entry No." := GetNextEntryNo();
                        "Message No." := txt_CodeMess;
                        "Location Code" := txt_LocationCOde;
                        "Order No" := txt_NoCommande;
                        Date := dte_Dte;
                        Heure := TIME;

                        EVALUATE("Line No", COPYSTR(str_Line, 1, 10));
                        Code := COPYSTR(str_Line, 61, 50);
                        Quantity := FormatTexteToNumerique(COPYSTR(str_Line, 335, 12), 12, 5);
                        INSERT();
                        // Mise à jour de la quantité traitée dans [Item Journal Line]
                        rec_ItemJnlLine.SETRANGE("Journal Template Name", 'RECLASS');
                        rec_ItemJnlLine.SETRANGE("Journal Batch Name", "Order No");
                        rec_ItemJnlLine.SETRANGE("Line No for MiniLoad", "Line No");
                        // On vérifie que la ligne existe sinon on traçe en erreur et on sort
                        IF rec_ItemJnlLine.FINDFIRST() THEN BEGIN
                            // Si le N° SP ne convient pas alors on ne modifie pas mais on traçe dans la description des messages
                            IF rec_ItemJnlLine."Item No." = Code THEN BEGIN
                                rec_ItemJnlLine."Quantité traitée par WMS" := Quantity;
                                // AD Le 07-06-2016 =>
                                CduLFunctions.ESK_GetDefaultBin(rec_ItemJnlLine."Item No.", rec_ItemJnlLine."Variant Code",
                                    rec_ItemJnlLine."New Location Code", rec_ItemJnlLine."New Bin Code", rec_ItemJnlLine."New Zone Code");
                                // FIN AD Le 07-06-2016
                                rec_ItemJnlLine.MODIFY();
                            END
                            ELSE BEGIN
                                Description := ErreurArticleLbl;
                                MODIFY();
                            END;
                        END
                        ELSE BEGIN
                            Description := STRSUBSTNO(ErreurFeuilleLbl, "Order No");
                            MODIFY();
                            EXIT;
                        END;
                    END;
                    // On stock le nombre de ligne de détail pour chaque ligne de commande
                    EVALUATE(int_NbLinesDetail, COPYSTR(str_Line, 445, 10));
                    // On fait un ReadLine pour chaque ligne de détail afin de ne pas les prendre en compte
                    FOR j := 1 TO int_NbLinesDetail DO
                        fil_Import.READ(str_Line);
                END;
            UNTIL fil_Import.READ(str_Line) = 0;
    end;

    procedure GetMsgSTO(txt_Item: Code[20])
    var
        recFile: Record File;
        rec_message: Record "Message receive from  MINILOAD";
        GestionFichier: Codeunit "Gestion Fichier";
        newnamfile: Text[320];

        PathFile: Text[250];

        NameFile: Text[100];

        window: Dialog;
        bln_found: Boolean;


        nbFiles: Integer;

        //TODO scr: Automation;
        oldTime: Time;

    begin
        //** Cette fonction permet d'intégrer les messages STO **//

        // Création et initialisation de l'automation.
        InitGlobal();
        // Structure du chemin des fichiers dans la variable
        PathFile := txtFolderIN;

        // Nom du fichier
        NameFile := 'STO';

        // Recherche des fichiers
        // -------------------------------------
        recFile.RESET();
        recFile.SETRANGE(Path, PathFile);
        recFile.SETRANGE("Is a file", TRUE);
        recFile.SETFILTER(Name, 'STO*');


        // On parcours les fichiers et tant que l'on intègre pas un fichier concernant notre article alors on boucle
        window.OPEN('Attente de réponse');
        // Stock le temps de début
        oldTime := TIME;
        WHILE (NOT GestionFichier.FileExist(PathFile, 'STO*', NameFile)) AND (NOT bln_found) DO BEGIN
            nbFiles := 1;

            // On recherche s'il y'a deja des lignes dans la table si oui on sort
            rec_message.RESET();
            rec_message.SETCURRENTKEY("Message No.", Code, Date);
            rec_message.SETFILTER("Message No.", '%1*', 'STO');
            rec_message.SETRANGE(Code, txt_Item);
            rec_message.SETRANGE(Date, TODAY);
            IF NOT rec_message.ISEMPTY THEN bln_found := TRUE;

            // SI plus de 10 secondes on demande à l'utilisateur s'il veut sortir
            IF TIME - oldTime > 10000 THEN BEGIN
                // Gestion du temps
                IF CONFIRM(SortieQst) THEN bln_found := TRUE;
                oldTime := TIME;
            END;
        END;
        window.CLOSE();

        IF recFile.FindSet() THEN
            REPEAT

                NameFile := recFile.Path + recFile.Name;
                IF FILE.EXISTS(NameFile) THEN BEGIN
                    // Traitement du fichier
                    ReadSTOFile(NameFile);
                    // Sauvegarde du fichier
                    newnamfile := txtFolderIN + 'save\' + recFile.Name;
                    RENAME(NameFile, newnamfile);
                END;
            UNTIL recFile.NEXT() = 0;
    end;

    procedure ReadSTOFile(_pFileName: Text[1024])
    var
        rec_Message: Record "Message receive from  MINILOAD";


        fil_Import: File;
        str_Line: Text[1024];

    begin
        //** Cette fonction permet la gestion du fichier STO. **//
        InitGlobal();

        // Gestion de la lecture di fichier avec variable 'file'
        CLEAR(fil_Import);
        fil_Import.TEXTMODE(TRUE);
        IF fil_Import.OPEN(_pFileName) THEN BEGIN
            // Pour le fichier STO nous ne devons pas boucler une seule ligne.
            fil_Import.READ(str_Line);
            // Patch pour retirer caractères impromptus du fichier
            str_Line := DELCHR(str_Line, '=', DebutFichierLbl);
            // Pour chaque ligne du fichier on insère uen ligne dans la table 50046
            WITH rec_Message DO BEGIN
                INIT();
                "Entry No." := GetNextEntryNo();
                "Message No." := DELSTR(fil_Import.NAME, 1, STRLEN(txtFolderIN));
                "Location Code" := COPYSTR(str_Line, 1, 10);
                Code := COPYSTR(str_Line, 11, 50);
                Quantity := FormatTexteToNumerique(COPYSTR(str_Line, 400, 12), 12, 5);

                // Récupération de la date
                Date := TODAY;
                Heure := TIME;
                INSERT();
            END;
        END;
    end;

    procedure GetMsgCEP()
    var
        /*  newnamfile: Text[320];
         errnamfile: Text[320]; */
        PathFile: Text[250];
        // Fichier: File;
        NameFile: Text[100];
    /*   recFile: Record File;
      GestionFichier: Codeunit "Gestion Fichier"; */
    //TODO folder: DotNet Directory;
    //TODO Lst: DotNet List_Of_T;
    //TODO Obj: DotNet Object;
    /*   CuFile: Codeunit "File Management";
      NbFile: Integer; */
    begin
        //** Cette fonction permet d'intégrer les messages CEP **//

        InitGlobal();
        // Structure du chemin des fichiers dans la variable
        PathFile := txtFolderIN;

        // Nom du fichier
        NameFile := 'CEP';

        /* AD Le 18-11-2015 => MIG2015
        // Recherche des fichiers
        // -------------------------------------
        // Pour actualiser la liste des fichiers il faut changer le filtre et le refaire
        // Incompréhensible mais cela fonctionne
        CLEAR(recFile);
        recFile.SETRANGE(Path,TEMPORARYPATH);
        recFile.SETRANGE("Is a file",FALSE);
        //recFile.SETFILTER(Name, 'toto@*');
         IF recFile.FINDFIRST () THEN;
        
        recFile.SETRANGE(Path,  PathFile);
        recFile.SETRANGE("Is a file",TRUE);
        recFile.SETFILTER(Name, NameFile+'@*');
         IF recFile.FINDFIRST () THEN
           REPEAT
             NameFile := recFile.Path + recFile.Name;
             IF FILE.EXISTS(NameFile) THEN BEGIN
               // Traitement du fichier
               ReadCEPFile(NameFile);
               // Sauvegarde du fichier
               newnamfile:= txtFolderIN +'save\' + recFile.Name;
               RENAME(NameFile,newnamfile);
             END;
           UNTIL recFile.NEXT() = 0;
        */
        /*//TODO
                        Obj := folder.GetFiles(PathFile, NameFile + '*');
                        Lst := Lst.List;
                        Lst.AddRange(Obj);
                        //MESSAGE('%1',Lst.Count);
                        FOR NbFile := 0 TO Lst.Count - 1 DO BEGIN
                            ReadCEPFile(Lst.Item(NbFile));

                            CuFile.CopyServerFile(Lst.Item(NbFile), txtFolderIN + 'save\' + CuFile.GetFileName(Lst.Item(NbFile)), TRUE);
                            CuFile.DeleteServerFile(Lst.Item(NbFile));

                        END;
        */

        // FIN AD Le 18-11-2015 => MIG2015

    end;

    procedure ReadCEPFile(_pFileName: Text[1024])
    var
        rec_Message: Record "Message receive from  MINILOAD";
        rec_ItemJnlLine: Record "Item Journal Line";
        fil_Import: File;
        str_Line: Text[1024];
        int_NbLines: Integer;
        int_NbLinesDetail: Integer;
        i: Integer;
        j: Integer;
        txt_CodeMess: Text[30];
        txt_LocationCOde: Code[20];
        txt_NoCommande: Code[20];


        bln_firstline: Boolean;
    begin
        //** Cette fonction permet la gestion du fichier CEP.
        InitGlobal();
        bln_firstline := TRUE;
        // Gestion de la lecture di fichier avec variable 'file'
        CLEAR(fil_Import);
        fil_Import.TEXTMODE(TRUE);
        IF fil_Import.OPEN(_pFileName) THEN BEGIN
            REPEAT

                // Patch pour retirer caractères impromptus du fichier
                IF bln_firstline THEN BEGIN
                    fil_Import.READ(str_Line);
                    str_Line := DELCHR(str_Line, '=', DebutFichierLbl);
                    bln_firstline := FALSE;
                END;
                // Récupération des valeurs d'entêtes
                txt_CodeMess := DELSTR(fil_Import.NAME, 1, STRLEN(txtFolderOut));
                txt_LocationCOde := COPYSTR(str_Line, 1, 10);
                txt_NoCommande := COPYSTR(str_Line, 11, 50);
                // On récupère le nombre de ligne du fichier. (A noter que pour une ligne le fichier contient aussi 'X' lignes de détail)
                EVALUATE(int_NbLines, COPYSTR(str_Line, 81, 10));

                // Après avoir récupéré le nombre de ligne restantes du fichier on boucle dessus
                FOR i := 1 TO int_NbLines DO BEGIN
                    // Chaque ligne indique le nombre de lignes suivante, nous devons donc les parcourir
                    fil_Import.READ(str_Line);
                    // On stock le nombre de ligne de détail pour chaque ligne de commande
                    EVALUATE(int_NbLinesDetail, COPYSTR(str_Line, 51, 10));
                    // On fait un ReadLine pour chaque ligne de gérer les lignes de retour
                    FOR j := 1 TO int_NbLinesDetail DO BEGIN
                        fil_Import.READ(str_Line);
                        // Initialisation de l'enregistrement du message.
                        WITH rec_Message DO BEGIN
                            INIT();
                            "Entry No." := GetNextEntryNo();
                            "Message No." := txt_CodeMess;
                            "Location Code" := txt_LocationCOde;
                            "Order No" := txt_NoCommande;
                            Date := TODAY;
                            Heure := TIME;
                            Code := COPYSTR(str_Line, 1, 50);
                            EVALUATE("Line No", COPYSTR(str_Line, 73, 10));
                            Quantity := FormatTexteToNumerique(COPYSTR(str_Line, 334, 12), 12, 5);
                            INSERT();
                            // Mise à jour de la quantité traitée dans [Item Journal Line]
                            rec_ItemJnlLine.SETRANGE("Journal Template Name", 'RECLASS');
                            rec_ItemJnlLine.SETRANGE("Journal Batch Name", "Order No");
                            rec_ItemJnlLine.SETRANGE("Line No for MiniLoad", "Line No");
                            // Il est possible qu la feuille n'existe plus donc on skip et on trace l'erreur
                            IF rec_ItemJnlLine.FINDFIRST() THEN BEGIN
                                // Si le N° SP ne convient pas alors on ne modifie pas mais on traçe dans la description des messages
                                IF (rec_ItemJnlLine."Item No." = Code) THEN BEGIN
                                    // Incrémentation car peut rentrer ds plusieurs bacs et dans ce cas MiniLoad renvoie plusieurs lignes
                                    rec_ItemJnlLine."Quantité traitée par WMS" += Quantity;
                                    rec_ItemJnlLine.MODIFY();
                                END
                                ELSE BEGIN
                                    Description := ErreurArticleLbl;
                                    MODIFY();
                                END;
                            END
                            ELSE BEGIN
                                Description := STRSUBSTNO(ErreurFeuilleLbl, "Order No");
                                MODIFY();
                                EXIT;
                            END;
                        END;
                    END;
                END;
            UNTIL fil_Import.READ(str_Line) = 0;
        END;
    end;
}

