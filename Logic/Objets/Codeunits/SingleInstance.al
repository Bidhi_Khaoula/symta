codeunit 50092 SingleInstance
{
    SingleInstance = true;




    procedure GetDropInvoiceDiscountAmountValue(): Boolean
    
    begin
        Exit(BooGDropInvoiceDiscountAmount);
    end;

    procedure SetDropInvoiceDiscountAmountValue(BooPDropInvoiceDiscountAmount: Boolean)
    begin
        BooGDropInvoiceDiscountAmount := BooPDropInvoiceDiscountAmount;
    end;

    procedure FctSetIfFunctionUpdateAmountsExecuted(BooPUpdateAmounts: boolean)
    begin
        T37_UpdateAmounts := BooPUpdateAmounts;
    end;

    procedure FctIfUpdateAmountsIsExecuted(): Boolean
    begin
        exit(T37_UpdateAmounts);
    end;

    procedure FctSaveRecords(RecPCurrency: record Currency; RecPSalesOrderLine: record "Sales Line")
    begin
        RecGCurrency := RecPCurrency;
        ReGSalesOrderLine := RecPSalesOrderLine;
    end;

    procedure FctGetSavedRecords(Var RecPCurrency: record Currency; Var RecPSalesOrderLine: record "Sales Line")
    begin
        RecPCurrency := RecGCurrency;
        RecPSalesOrderLine := ReGSalesOrderLine;
    end;

    var

    var
        ReGSalesOrderLine: Record "Sales Line";
        RecGCurrency: Record Currency;
        
        RecGItemJournalLine: Record "Item Journal Line";
        BooGDropInvoiceDiscountAmount: Boolean;
        IsNewLocationCode: Boolean;

       // Multiple: Decimal;
       // QteProposée: Decimal;

    //>> Mig CU 229
    PROCEDURE SetPrintPurchEmail(pbln_SendPurchEmail: Boolean);
    BEGIN
        CU229_blnG_SendPurchEmail := pbln_SendPurchEmail;
    END;

    var
        CU229_blnG_SendPurchEmail: Boolean;
    //<< Mig CU 229
    //>>Mig CU 241
    PROCEDURE SetHideValidationDialog(NewHideValidationDialog: Boolean);
    BEGIN
        // MCO Le 27-09-2016 => R‚gie
        Cu241_HideValidationDialog := NewHideValidationDialog;
    END;

    PROCEDURE GetHideValidationDialog(): Boolean;
    BEGIN
        // MCO Le 27-09-2016 => R‚gie
        Exit(Cu241_HideValidationDialog);
    END;
    //<<Mig CU 241
    //>> Mig CU 333
    procedure FctSetRecPrintOrder(BooPMarkedOnly: Boolean)
    begin
        CU333_RecPrintOrder.MARKEDONLY := BooPMarkedOnly;
    end;

    procedure FctGetRecPrintOrder(): record "purchase Header"
    begin
        Exit(CU333_RecPrintOrder);
    end;

    procedure CU333_FctGetDesc(var PurchaseLine: Record "Purchase Line")
    begin
        PurchaseLine.Description := CU333_Description;
        PurchaseLine."Description 2" := CU333_Description2;
    end;

    procedure CU333_FctGetRecord(PurchaseHeader: Record "Purchase Header")
    begin
        IF CU333_RecPrintOrder.GET(PurchaseHeader."Document Type", PurchaseHeader."No.") THEN
            FctSetRecPrintOrder(true)
    end;

    procedure CU333_FctSetDesc(TxtPDesc: Text[100]; TxtPDesc2: text[50])
    begin
        CU333_Description := TxtPDesc;
        CU333_Description2 := TxtPDesc2;
    end;

    var
        CU333_RecPrintOrder: Record "Purchase Header";
        CU333_Description: Text[100];
        CU333_Description2: Text[50];
    //<< Mig CU 333
    //>> Mig CU392
    procedure FctSaveCurrencyCode(CurrencyCode: Code[20])
    begin
        CodGCurrencyCode := CurrencyCode;
    end;

    procedure FctGetCurrencyCode(): code[10]
    begin
        Exit(CodGCurrencyCode);
    end;
    //<< Mig CU392

    //>> Mig CU 5063
    procedure FctSetSalesVersionNo(intPVersionNo: Integer)
    begin
        CU5063_SalesVersionNo := intPVersionNo;
    end;

    procedure FctGetSalesVersionNo(): Integer
    begin
        Exit(CU5063_SalesVersionNo);
    end;

    procedure FctGetPurchVersionNo(): Integer
    begin
        Exit(CU5063_PurchVersionNo);
    end;

    procedure FctSetPurchVersionNo(intPVersionNo: Integer)
    begin
        CU5063_PurchVersionNo := intPVersionNo;
    end;

    procedure FctDeleteSalesLine5063(RecPSalesHeader: Record "Sales Header")
    begin
        CU5705_SalesLines.RESET();
        CU5705_SalesLines.SETRANGE("Document Type", RecPSalesHeader."Document Type");
        CU5705_SalesLines.SETRANGE("Document No.", RecPSalesHeader."No.");
        IF CU5705_SalesLines.FINDSET() THEN
            REPEAT
                CU5705_SalesLines.DELETE();
            UNTIL CU5705_SalesLines.NEXT() = 0;
    end;

    procedure FctHideDialog(Hide: Boolean)
    begin
        CU5705_SalesLines.SetHideValidationDialog(Hide);
    end;

    var

        CU5063_SalesVersionNo: Integer;
        CU5063_PurchVersionNo: Integer;
    //<< Mig CU 5063
    //>> Mig CU 5705
    procedure FctSetLoansNo(CodPLoansNo: Code[20])
    begin
        CU5705_LoansNo := CodPLoansNo;
    end;

    procedure FctGetLoansNo(): Code[20]
    begin
        exit(CU5705_LoansNo);
    end;

    var
       
        CU5705_SalesLines: Record "Sales Line";
         CU5705_LoansNo: Code[20];
    //<< Mig CU 5705
    //>> Mig CU 80
    procedure FctSetQtyToInvoiceBase(DecPQtyToInvoiceBase: Decimal)
    begin
        CU80_QtyToInvoiceBase := DecPQtyToInvoiceBase;
    end;

    procedure FctGetQtyToInvoiceBase(): Decimal
    begin
        Exit(CU80_QtyToInvoiceBase);
    end;

    procedure FctSetShould(BooPShould: Boolean)
    begin
        CU80_Should := BooPShould;
    end;

    procedure FctGetShould(): Boolean
    begin
        Exit(CU80_Should);
    end;

    var
        CU80_Should: Boolean;
        CU80_QtyToInvoiceBase: Decimal;
    //<< Mig CU 80
    //>> Mig CU 90
    procedure FctSaveWhseRcptHeaderNo(CodPWhseRcptHeaderNo: Code[20])
    begin
        CU90_WhseRcptHeaderNo := CodPWhseRcptHeaderNo;
    end;

    procedure FctGetWhseRcptHeaderNo(): Code[20]
    begin
        Exit(CU90_WhseRcptHeaderNo);
    end;

    var
        CU90_WhseRcptHeaderNo: Code[20];
    //<< Mig CU 90
    //>> Mig CU 7010
    PROCEDURE GetBestPurchPrice(p_ItemNo: Code[20]; p_Quantity: Decimal; p_TypeDeCde: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3"; VAR p_NetCost: Decimal; FiltreFournisseur: Text[30]) txt_vendor: Code[20];
    VAR
        rec_Item: Record Item;
        rec_ItemByVendor: Record "Item Vendor";
        TempToPurchDisc: Record "Purchase Line Discount" TEMPORARY;
        TempToPurchPrice: Record "Purchase Price" TEMPORARY;
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        dec_BrutPurchPrice: Decimal;
        dec_PurchDisc: Decimal;
        dec_NetPurchPrice: Decimal;
    BEGIN
        //** Cette fonction renvoit pour un article et/ une quantit‚ le meilleur fournisseur et son prix **//

        // Initialisation des valeurs de retours.
        p_NetCost := 1000000000;
        txt_vendor := '';

        // R‚cup‚ration du record article.
        rec_Item.GET(p_ItemNo);

        // Recherche les fournisseurs de l'article.
        rec_ItemByVendor.RESET();
        rec_ItemByVendor.SETRANGE("Item No.", p_ItemNo);

        IF FiltreFournisseur <> '' THEN
            rec_ItemByVendor.SETFILTER("Vendor No.", FiltreFournisseur);

        rec_ItemByVendor.SETFILTER("Statut Qualité", '%1|%2', rec_ItemByVendor."Statut Qualité"::Conforme,
            rec_ItemByVendor."Statut Qualité"::"Contrôle en cours");
        rec_ItemByVendor.SETRANGE("Statut Approvisionnement", rec_ItemByVendor."Statut Approvisionnement"::"Non Bloqué");

        CLEAR(TempCU7010_TabMeilleurFrn);
        TempCU7010_TabMeilleurFrn.DELETEALL();

        // Parcours des enregistrements.
        IF rec_ItemByVendor.FINDSET() THEN
            REPEAT
                // Initialisation des variables;
                dec_BrutPurchPrice := 0;
                dec_PurchDisc := 0;
                dec_NetPurchPrice := 0;

                // R‚cupŠre le prix d'achat et la remise pour le fournisseur.
                //On r‚cupŠre le prix d'achat
                PurchPriceCalcMgt.FindPurchPrice(TempToPurchPrice, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                       , rec_ItemByVendor."Variant Code", rec_Item."Purch. Unit of Measure"
                                                        , '', WORKDATE(), FALSE);

                // Rajout du filtre sur la quantit‚ minimum
                TempToPurchPrice.SETRANGE("Minimum Quantity", 0, p_Quantity);

                IF TempToPurchPrice.FINDLAST() THEN BEGIN
                    // R‚cupŠre les informations du prix d'achat.
                    dec_BrutPurchPrice := TempToPurchPrice."Direct Unit Cost";

                    // On r‚cupŠre la remise d'achat
                    /*TODO PurchPriceCalcMgt.FindPurchLineDisc(TempToPurchDisc, rec_ItemByVendor."Vendor No.", rec_ItemByVendor."Item No."
                                      , rec_ItemByVendor."Variant Code", rec_Item."Purch. Unit of Measure"
                                      , '', WORKDATE, FALSE, p_TypeDeCde, '');*/


                    // Rajout du filtre sur la quantit‚ minimum
                    //TODO Qty := p_Quantity;
                    //TempToPurchDisc.SETRANGE("Minimum Quantity", 0, p_Quantity);

                    PurchPriceCalcMgt.CalcBestLineDisc(TempToPurchDisc);

                    //IF TempToPurchDisc.FINDLAST THEN
                    dec_PurchDisc := TempToPurchDisc."Line Discount %";

                    //IF TempToPurchPrice."Ne pas Appliquer les remises" THEN
                    //  dec_PurchDisc := 0;

                    dec_NetPurchPrice := ROUND(
                                                       (dec_BrutPurchPrice * (1 - dec_PurchDisc / 100))
                                                       , 0.01);
                    IF dec_NetPurchPrice = 0 THEN
                        dec_NetPurchPrice := dec_BrutPurchPrice;

                    // AD Le 02-07-2014 => Table avec les meilleur frn
                    TempCU7010_TabMeilleurFrn."Amount (LCY)" := dec_NetPurchPrice;
                    TempCU7010_TabMeilleurFrn."Amount 2 (LCY)" := 0;
                    TempCU7010_TabMeilleurFrn."Vendor No." := rec_ItemByVendor."Vendor No.";
                    TempCU7010_TabMeilleurFrn.Insert();
                    // FIN AD Le 02-07-2014


                    // SI c'est le plus petit prix alors on met … jour;
                    IF dec_NetPurchPrice < p_NetCost THEN BEGIN
                        p_NetCost := dec_NetPurchPrice;
                        txt_vendor := rec_ItemByVendor."Vendor No.";
                    END;
                END;
            UNTIL rec_ItemByVendor.NEXT() = 0;


        // AD Le 12-12-2011
        IF p_NetCost = 1000000000 THEN
            p_NetCost := 0;
    END;

    PROCEDURE RetourneXMeilleurFrn(_pRang: Integer): Code[20];
    VAR
        i: Integer;
    BEGIN
        IF (_pRang < 1) OR (_pRang > TempCU7010_TabMeilleurFrn.COUNT) OR (TempCU7010_TabMeilleurFrn.COUNT = 0) THEN
            EXIT('');

        i := 1;
        TempCU7010_TabMeilleurFrn.FINDFIRST();
        IF _pRang = 1 THEN
            EXIT(TempCU7010_TabMeilleurFrn."Vendor No.")
        ELSE
            REPEAT
                TempCU7010_TabMeilleurFrn.NEXT();
                i += 1;
            UNTIL i = _pRang;

        EXIT(TempCU7010_TabMeilleurFrn."Vendor No.")
        ;
    END;

    var
        TempCU7010_TabMeilleurFrn: Record "Vendor Amount" TEMPORARY;
    //<< Mig CU 7010
    //>> Mig CU 260

    PROCEDURE SetRecipientName(p_RecipientName: Text);
    BEGIN
        // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
        CU260_txt_RecipientName := p_RecipientName;
    END;

    PROCEDURE GetRecipientName(): Text;
    BEGIN
        // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
        Exit(CU260_txt_RecipientName);
    END;

    var
        CU260_txt_RecipientName: Text;
    //<< Mig CU 260
    //>> Mig CU 5760
    procedure FctSetMtReception(DecPMtReception: Decimal)
    begin
        CU5760_MtReception := DecPMtReception;
    end;

    procedure FctGetMtReception(): Decimal
    begin
        exit(CU5760_MtReception);
    end;

    var
        CU5760_MtReception: Decimal;

    //<< Mig CU 5760
    //>> Mig CU 5763
    procedure FctSet2BLPARcommande(BooP2BLPARcommande: Boolean)
    begin
        CU5763_2BLPARcommande := BooP2BLPARcommande;
    end;

    procedure FctGet2BLPARcommande(): Boolean
    begin
        Exit(CU5763_2BLPARcommande);
    end;

    procedure FctSetOldValuesCU5763(CodPModifyUserID_Old: Code[40]; DatPModifyDate_Old: Date; TimPModifyTime_Old: time)
    begin
        CU5763_lModifyUserID_Old := CodPModifyUserID_Old;
        CU5763_lModifyDate_Old := DatPModifyDate_Old;
        CU5763_lModifyTime_Old := TimPModifyTime_Old;
    end;

    procedure FctGetOldValuesCU5763(var RecPSalesHeader: Record "Sales Header")
    begin
        RecPSalesHeader."Modify User ID" := CU5763_lModifyUserID_Old;
        RecPSalesHeader."Modify Date" := CU5763_lModifyDate_Old;
        RecPSalesHeader."Modify Time" := CU5763_lModifyTime_Old;
    end;

    procedure FctSaveNoWhseShptHeaderParam(WhseShptHeaderParamNo: code[20])
    begin
        CU5763_WhseShptHeaderParamNo := WhseShptHeaderParamNo;
    end;

    procedure FctGetNoWhseShptHeaderParam(): code[40]
    begin
        Exit(CU5763_WhseShptHeaderParamNo);
    end;

    var
        CU5763_WhseShptHeaderParamNo: Code[40];
        CU5763_2BLPARcommande: Boolean;
        CU5763_lModifyUserID_Old: Code[40];
        CU5763_lModifyDate_Old: Date;
        CU5763_lModifyTime_Old: Time;
    //<< Mig CU 5763
    //>> Mig CU 5752

    procedure FctGetZoneCode(): Code[10]
    begin
        exit(CU5752_ZoneCode);
    end;

    procedure FctGetHideDialog_CU5752(): Boolean;
    begin
        Exit(CU5752_HideDialog);
    end;

    PROCEDURE SetHideDialog(NewHideDialog: Boolean);
    BEGIN
        // AD le 06-10-2005 => Fonction ajouter pour lancer en mode silencieux
        CU5752_HideDialog := NewHideDialog;
    END;

    PROCEDURE SetZoneCode(NewZoneCode: Code[10]);
    BEGIN
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // Cette fonction permet de cr‚er le BP juste pour les lignes de la zone s‚l‚ctionn‚e. (Selection de commande).
        CU5752_ZoneCode := NewZoneCode;
    END;

    PROCEDURE SetImprimanteAuto(_pImprimanteAuto: Boolean);
    BEGIN
        CU5752_ImprimanteAuto := _pImprimanteAuto;
    END;

    procedure FctSetSkipFunctionValue(BooPValue: Boolean)
    begin
        BooGSkipFunction := BooPValue;
    end;

    procedure FctGetSkipFunctionValue(): Boolean
    begin
        Exit(BooGSkipFunction);
    end;

    procedure FctSetShowAvailibilityFunctionValue(_pShowAvailibilty: Boolean)
    begin
        CU905_gShowAvailibilty := _pShowAvailibilty;
    end;

    procedure FctGetShowAvailibilityValue(): Boolean
    begin
        Exit(CU905_gShowAvailibilty);
    end;

    procedure FctSaveItemJournalLineValues(RecPItemJournalLine: record "Item Journal Line"; BooPIsNewLocationCode: Boolean)
    begin
        RecGItemJournalLine := RecPItemJournalLine;
        IsNewLocationCode := BooPIsNewLocationCode;
    end;

    procedure FctGetItemJournalLineValues(Var RecPItemJournalLine: record "Item Journal Line"; Var BooPIsNewLocationCode: Boolean)
    begin
        RecPItemJournalLine := RecGItemJournalLine;
        BooPIsNewLocationCode := IsNewLocationCode;
    end;

    procedure FctSaveItemReferenceTypeValue(EnuPItemRefType: Enum "Item Reference Type")
    begin
        T5717_ItemRefType := EnuPItemRefType;
        T5717_Ischanged := true;
    end;

    procedure FctGetItemReferenceTypeValue(var EnuPItemRefType: Enum "Item Reference Type")
    begin
        if T5717_Ischanged then
            EnuPItemRefType := T5717_ItemRefType;
        T5717_Ischanged := false;
    end;

    var
        T37_UpdateAmounts: boolean;
        T5717_ItemRefType: Enum "Item Reference Type";
        T5717_Ischanged: Boolean;
        BooGSkipFunction: Boolean;
        CU5752_HideDialog: Boolean;
        CU5752_ZoneCode: Code[10];
        CU5752_ImprimanteAuto: Boolean;
        Cu241_HideValidationDialog: Boolean;
        CodGCurrencyCode: Code[10];
    //<< Mig CU 5752
    //<< Mig CU 5895
    procedure FctIsOnlineAdjmt(): Boolean
    begin
        exit(CU5895_IsOnlineAdjmt);
    end;

    procedure FctSetIsOnlineAdjmtVAlue(IsOnlineAdjmt: Boolean)
    begin
        CU5895_IsOnlineAdjmt := IsOnlineAdjmt;
    end;

    var
        CU5895_IsOnlineAdjmt: Boolean;
    //>> Mig CU 5895
    //>> Mig CU 6620
    procedure FctSetPostingDate(PostingDate: Date)
    begin
        CU6620_PostingDate := PostingDate;
    end;

    procedure FctGetPostingDate(): date
    begin
        Exit(CU6620_PostingDate);
    end;

    var
        CU6620_PostingDate: Date;
    //<< Mig CU 6620
    //>>Mig CU 99000815
    procedure FctSaveOldDate(DatPDate: Date)
    begin
        DatGDate := DatPDate;
        DateIsChanged := true;
    end;

    procedure FctGetOldDate(DatPDate: Date): Date
    begin
        if DateIsChanged then begin
            DateIsChanged := false;
            exit(DatGDate)
        end;
        exit(DatPDate)
    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item-Check Avail.", 'OnBeforeSetFilterOnItem', '', false, false)]
    local procedure CU311_OnBeforeSetFilterOnItem(var Item: Record Item; ItemNo: Code[20]; ItemVariantCode: Code[10]; ItemLocationCode: Code[10]; ShipmentDate: Date; UseOrderPromise: Boolean; var IsHandled: Boolean; UnitOfMeasureCode: Code[10])
    begin
        CU311_gItemVariantCode := ItemVariantCode;
    end;
    //>> Mig CU 311
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item-Check Avail.", 'OnBeforeCreateAndSendNotification', '', false, false)]
    local procedure CU311_OnBeforeCreateAndSendNotification(ItemNo: Code[20]; UnitOfMeasureCode: Code[20]; InventoryQty: Decimal; GrossReq: Decimal; ReservedReq: Decimal; SchedRcpt: Decimal; ReservedRcpt: Decimal; CurrentQuantity: Decimal; CurrentReservedQty: Decimal; TotalQuantity: Decimal; EarliestAvailDate: Date; RecordId: RecordID; LocationCode: Code[10]; ContextInfo: Dictionary of [Text, Text]; var Rollback: Boolean; var IsHandled: Boolean)
    var
        NotificationLifecycleMgt: Codeunit "Notification Lifecycle Mgt.";
        ItemCheckAvail: Codeunit "Item-Check Avail.";
        ItemAvailabilityCheck: Page "Item Availability Check";
        AvailabilityCheckNotification: Notification;
        NotificationMsg: Label 'The available inventory for item %1 is lower than the entered quantity at this location.', Comment = '%1=Item No.';
        DetailsTxt: Label 'Show details';
        DontShowAgainTxt: Label 'Don''t show again';
        ESK002Err: Label 'Traitement annulé.';
    begin
        AvailabilityCheckNotification.Id(CreateGuid());
        AvailabilityCheckNotification.Message(StrSubstNo(NotificationMsg, ItemNo));
        AvailabilityCheckNotification.Scope(NOTIFICATIONSCOPE::LocalScope);
        AvailabilityCheckNotification.AddAction(DetailsTxt, CODEUNIT::"Item-Check Avail.", 'ShowNotificationDetails');
        AvailabilityCheckNotification.AddAction(DontShowAgainTxt, CODEUNIT::"Item-Check Avail.", 'DeactivateNotification');
        ItemAvailabilityCheck.PopulateDataOnNotification(AvailabilityCheckNotification, ItemNo, UnitOfMeasureCode,
          InventoryQty, GrossReq, ReservedReq, SchedRcpt, ReservedRcpt, CurrentQuantity, CurrentReservedQty,
          TotalQuantity, EarliestAvailDate, LocationCode);
        ItemAvailabilityCheck.PopulateDataOnNotification(AvailabilityCheckNotification, 'VariantCode', CU311_gItemVariantCode);
        NotificationLifecycleMgt.SendNotificationWithAdditionalContext(
          AvailabilityCheckNotification, RecordId, ItemCheckAvail.GetItemAvailabilityNotificationId());
        // MCO Le 29-11-2017 => SYMTA souyhaite que la notification soit affichée
        IF NOT CONFIRM(STRSUBSTNO(NotificationMsg, ItemNo)) THEN
            ERROR(ESK002Err);
        // FIN MCO Le 29-11-2017
        CU311_gItemVariantCode := '';
        Rollback := false;
        IsHandled := true;
    end;

    procedure FctInsertSKU(ItemJournalLine: Record "Item Journal Line")
    begin
        TempUpdatedStdCostSKU."Item No." := ItemJournalLine."Item No.";
        TempUpdatedStdCostSKU."Location Code" := ItemJournalLine."Location Code";
        TempUpdatedStdCostSKU."Variant Code" := ItemJournalLine."Variant Code";
        TempUpdatedStdCostSKU.Insert();
    end;

    procedure FctDeleteSKU()
    begin
        TempUpdatedStdCostSKU.DeleteAll();
    end;

    var
        TempUpdatedStdCostSKU: Record "Stockkeeping Unit" temporary;
        CU311_gItemVariantCode: Code[10];
        CU905_gShowAvailibilty: Boolean;
        DateIsChanged: Boolean;
        DatGDate: Date;
        T10866_Posted: Boolean;
    //<<Mig CU 99000815
    procedure FctSetPosted(Posted: Boolean)
    begin
        T10866_Posted := Posted;
    end;

    procedure FctGetPosted(): Boolean
    begin
        exit(T10866_Posted);
    end;



}