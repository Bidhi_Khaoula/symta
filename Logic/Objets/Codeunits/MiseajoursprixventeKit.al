codeunit 50067 "Mise a jours prix vente Kit"
{
    trigger OnRun()
    var
        FiltreItem: Report "MAj prix vente Kit";
        SelectItem: Page "MAJ prix vente kit";
    begin
        //Lancement du report
        FiltreItem.RUNMODAL();
        FiltreItem.Result(TemItem);
        IF NOT TemItem.FINDSET() THEN EXIT;
        SelectItem.Init(TemItem);
        SelectItem.RUNMODAL();
    end;

    var
        TemItem: Record Item temporary;
}

