codeunit 50038 "EDI avoirs Socodicor"
{// CFR le 24/09/2021 - SFD20210201 historique Centrale Active

    trigger OnRun()
    var
        
        "InfoSociété": Record "Company Information";
        SalesInvHeader: Record "Sales Cr.Memo Header";
         PagLInvDate: page "Invoicing Date";
        NomFichier: Text[60];
        date_fac: Date;
       
    begin

        //dialog_window.OPEN ('Date facture (JJMMAA) #1#########\',date_fac);
        //dialog_window.INPUT(1,date_fac);
        //dialog_window.CLOSE;
        // EVALUATE(date_fac, dialog_window.InputBox('Date facture (JJMMAA)', 'INPUT', '', 100, 100));
        IF PagLInvDate.RunModal() = action::OK then
            date_fac := PagLInvDate.FctGetInvDate()
        else
            exit;
        Fichier.TEXTMODE(TRUE);
        Fichier.WRITEMODE(TRUE);

        InfoSociété.GET();

        NomFichier := InfoSociété."Export EDI Folder" + '\socodicor_avoir.txt';

        Fichier.CREATE(NomFichier);

        Fichier.SEEK(Fichier.LEN);

        SalesInvHeader.RESET();
        SalesInvHeader.SETRANGE(SalesInvHeader."Sell-to Customer No.", '150008-FAC');
        SalesInvHeader.SETRANGE(SalesInvHeader."Document Date", date_fac);
        IF SalesInvHeader.FindSet() THEN
            REPEAT
                ExportFacture(SalesInvHeader."No.");
            UNTIL SalesInvHeader.NEXT() = 0;

        MESSAGE(ESK001Lbl);
    end;

    var
        gPurchaseCentralHistory: Record "Purchase Central History";

        ESK001Lbl: Label 'Terminé !';
        Fichier: File;
        no_ligne: Integer;

    procedure ExportFacture(_pNoDoucment: Code[20])
    var
        SalesInvHeader: Record "Sales Cr.Memo Header";
        SalesInvLine: Record "Sales Cr.Memo Line";
        LFormat: Code[10];
        Lparam: Record "Generals Parameters";
        SalesShptHeader: Record "Return Receipt Header";
        no_bl: Code[20];
    begin

        SalesInvHeader.GET(_pNoDoucment);

        EnteteSOCO(SalesInvHeader, Fichier);

        no_bl := '';
        SalesInvLine.RESET();
        SalesInvLine.SETRANGE(SalesInvLine."Document No.", _pNoDoucment);
        no_ligne := 0;
        IF SalesInvLine.FINDFIRST() THEN
            REPEAT
                IF SalesInvLine.Quantity <> 0 THEN
                    LigneSOCO(SalesInvLine, Fichier);
                IF (SalesInvLine."Return Receipt No." <> '') AND (no_bl = '') THEN no_bl := SalesInvLine."Return Receipt No.";
            UNTIL SalesInvLine.NEXT() = 0;

        IF SalesShptHeader.GET(no_bl) THEN;
        AdrSOCO(SalesShptHeader, Fichier);
    end;

    procedure EnteteSOCO(_pEntete: Record "Sales Cr.Memo Header"; var _Fichier: File)
    var
        
        _pLigne: Record "Sales Cr.Memo Line";
        _pCustomer: Record Customer;
        salesinvline: Record "Sales Cr.Memo Line";
        salesshptheader: Record "Return Receipt Header";
        p_group: Record Customer;
         Ligne: Text[1024];
         no_bl: Code[20];
          ht: Decimal;
        ttc: Decimal;
    begin
        // somme ht et ttc
        ht := 0;
        ttc := 0;
        _pLigne.RESET();
        _pLigne.SETRANGE(_pLigne."Document No.", _pEntete."No.");
        IF _pLigne.FindSet() THEN
            REPEAT
                ht += _pLigne.Amount;
                ttc += _pLigne."Amount Including VAT";
            UNTIL _pLigne.NEXT() = 0;


        // code adhérent  via le dervnier bl
        no_bl := '';
        salesinvline.RESET();
        salesinvline.SETRANGE(salesinvline."Document No.", _pEntete."No.");
        IF salesinvline.FindSet() THEN
            REPEAT
                IF (salesinvline."Return Receipt No." <> '') AND (no_bl = '') THEN no_bl := salesinvline."Return Receipt No.";
            UNTIL salesinvline.NEXT() = 0;
        IF salesshptheader.GET(no_bl) THEN;
        IF _pCustomer.GET(salesshptheader."Sell-to Customer No.") THEN;

        IF p_group.GET('150008-FAC') THEN; // code groupement socodicor

        Ligne := 'ENT';
        Ligne += PADSTR(' ', 9, ' '); // no commande origine

        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        //Ligne+=PADSTR(p_group."Libre 3",35,' ');    // code socodicor
        //Ligne+=PADSTR(_pCustomer."Libre 3",35,' ');    // code societaire
        Ligne += PADSTR(gPurchaseCentralHistory.GetAdherentFromCustomerCentralDate(p_group."No.", p_group."No.", _pEntete."Document Date"), 35, ' ');    // code socodicor
        Ligne += PADSTR(gPurchaseCentralHistory.GetAdherentFromCustomerCentralDate(_pCustomer."No.", p_group."No.", _pEntete."Document Date"), 35, ' ');    // code societaire
                                                                                                                                                            // FIN CFR le 24/09/2021

        Ligne += FORMAT(_pEntete."Document Date", 0, '<Day,2><Month,2><Year4>'); // date facture
        Ligne += PADSTR(' ', 35, ' '); //reference interne
        Ligne += PADSTR(_pEntete."No.", 35, ' '); // no facture
        Ligne += PADSTR(_pEntete."Payment Method Code", 3, ' '); // mode reglement
        Ligne += FORMAT(_pEntete."Due Date", 0, '<Day,2><Month,2><Year4>'); // date echeance
        Ligne += FORMAT(_pEntete."Payment Discount %", 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>'); //esc
        Ligne += FORMAT(ht, 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>'); //ht
        Ligne += FORMAT(ttc, 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>');//ttc

        _Fichier.WRITE(Ligne);
    end;

    procedure LigneSOCO(_pLigne: Record "Sales Cr.Memo Line"; var _Fichier: File)
    var
            Ligne: Text[1024];
    begin

        Ligne := 'LIG';
        no_ligne += 1;
        Ligne += FORMAT(no_ligne, 0, '<Integer,6>'); // no ligne
        Ligne += PADSTR(_pLigne."Recherche référence", 35, ' '); // reference
        Ligne += PADSTR(_pLigne.Description, 70, ' '); // designation
        Ligne += FORMAT(_pLigne.Quantity, 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>'); //qte
        Ligne += PADSTR(' ', 16, ' ');
        Ligne += FORMAT(_pLigne."Line No.", 0, '<Integer,35>'); // no ligne
        Ligne += PADSTR(' ', 35, ' '); //ref ligne origine
        Ligne += FORMAT(_pLigne."Unit Price", 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>');//px achat
        Ligne += FORMAT(_pLigne."Line Discount %", 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>');//%rem
        Ligne += PADSTR('1', 16, ' '); //unite de prix
        Ligne += PADSTR(' ', 35, ' '); //libre
        Ligne += FORMAT(_pLigne."Line Amount", 0, '<Precision,2:2><Sign,1><Integer,12><Filler Character, ><Decimals><Comma,.>');//mt ligne


        _Fichier.WRITE(Ligne);
    end;

    procedure AdrSOCO(_pEntete: Record "Return Receipt Header"; var _Fichier: File)
    var
        ligne: Text[1024];
    begin

        ligne := 'ADR';
        ligne += '001'; // type adresse
        // ANI Le 18-01-2017 Ticket 17685
        /*
        ligne+=PADSTR(_pEntete."Ship-to Name",35,' '); // raison scociale
        ligne+=PADSTR(_pEntete."Ship-to Name 2",35,' ');// raison scociale suite
        ligne+=PADSTR(_pEntete."Ship-to Address",35,' ');// adresse 1
        ligne+=PADSTR(_pEntete."Ship-to Address 2",35,' ');//adresse 2
        ligne+=PADSTR(' ',35,' ');// adresse 3
        ligne+=PADSTR(_pEntete."Ship-to Post Code",9,' '); //cp
        ligne+=PADSTR(_pEntete."Ship-to City",35,' ');//ville
        */
        ligne += PADSTR(_pEntete."Sell-to Customer Name", 35, ' '); // raison scociale
        ligne += PADSTR(_pEntete."Sell-to Customer Name 2", 35, ' ');// raison scociale suite
        ligne += PADSTR(_pEntete."Sell-to Address", 35, ' ');// adresse 1
        ligne += PADSTR(_pEntete."Sell-to Address 2", 35, ' ');//adresse 2
        ligne += PADSTR(' ', 35, ' ');// adresse 3
        ligne += PADSTR(_pEntete."Sell-to Post Code", 9, ' '); //cp
        ligne += PADSTR(_pEntete."Sell-to City", 35, ' ');//ville
                                                          // FIN ANI Le 18-01-2017 Ticket 17685

        ligne += 'FRA';//pays

        _Fichier.WRITE(ligne);

    end;
}

