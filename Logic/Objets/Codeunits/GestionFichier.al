codeunit 50061 "Gestion Fichier"
{

    trigger OnRun()
    begin
    end;

    procedure FileExist(pPath: Text[1000]; pName: Text[250]; var PathName: Text[1000]): Boolean
    var
        FileList: Record File;
    begin
        // Pour actualiser la liste des fichiers il faut changer le filtre et le refaire
        // Incompréhensible mais cela fonctionne
        CLEAR(FileList);
        FileList.SETRANGE(Path, 'c:');
        FileList.SETRANGE("Is a file", TRUE);
        FileList.SETFILTER(Name, pName);
        IF FileList.FINDFIRST() THEN;

        FileList.SETRANGE(Path, pPath);
        FileList.SETRANGE("Is a file", TRUE);
        FileList.SETFILTER(Name, pName);
        IF FileList.FINDFIRST() THEN BEGIN
            PathName := FileList.Path + FileList.Name;
            EXIT(TRUE);
        END
        ELSE
            EXIT(FALSE)
    end;
}

