codeunit 50036 "Gestion des litiges"
{

    trigger OnRun()
    begin
    end;

    procedure "CréerLitige"(_pWhseReceiptLine: Record "Warehouse Receipt Line"; _pReceiptLine: Record "Purch. Rcpt. Line")
    var
        LLitige: Record "Gestion Litige";
        LPurchHeader: Record "Purchase Header";
        LPurchLine: Record "Purchase Line";
        _WarehouseReceiptHeader: Record "Warehouse Receipt Header";
    begin
        IF (NOT _pWhseReceiptLine."Litige prix") AND (_pWhseReceiptLine."Qte Reçue théorique" = _pWhseReceiptLine."Qty. to Receive") THEN
            EXIT;

        CLEAR(LLitige);
        LLitige.VALIDATE("Whse Receipt No.", _pWhseReceiptLine."No.");
        LLitige.VALIDATE("Line No.", _pWhseReceiptLine."Line No.");
        LLitige.VALIDATE("Item No.", _pWhseReceiptLine."Item No.");
        LLitige.VALIDATE("Quantité Théorique", _pWhseReceiptLine."Qte Reçue théorique");
        LLitige.VALIDATE("Receipt Quantity", _pWhseReceiptLine."Qty. to Receive");

        //LM le 30-05-2013
        //LLitige.VALIDATE("Prix Brut Théorique", _pWhseReceiptLine."Prix Brut Théorique");
        //LLitige.VALIDATE("Remise 1 Théorique", _pWhseReceiptLine."Remise 1 Théorique");
        //LLitige.VALIDATE("Remise 2 Théorique", _pWhseReceiptLine."Remise 2 Théorique");

        IF LPurchHeader.GET(_pWhseReceiptLine."Source Subtype", _pWhseReceiptLine."Source No.") THEN BEGIN
            IF LPurchLine.GET(LPurchHeader."Document Type", LPurchHeader."No.", _pWhseReceiptLine."Source Line No.") THEN BEGIN
                LLitige.VALIDATE("Prix Brut Théorique", LPurchLine."Direct Unit Cost");
                LLitige.VALIDATE("Remise 1 Théorique", LPurchLine."Discount1 %");
                LLitige.VALIDATE("Remise 2 Théorique", LPurchLine."Discount2 %");
            END;
        END;
        // FIN LM le 30-05-2013

        LLitige.VALIDATE("Date Litige", WORKDATE());
        LLitige.VALIDATE("Litige Prix", _pWhseReceiptLine."Litige prix");
        LLitige.VALIDATE("Liitige Quantité", _pWhseReceiptLine."Qte Reçue théorique" <> _pWhseReceiptLine."Qty. to Receive");

        IF _pWhseReceiptLine."Source Type" = 39 THEN BEGIN
            LPurchHeader.GET(_pWhseReceiptLine."Source Subtype", _pWhseReceiptLine."Source No.");
            LLitige.VALIDATE("Vendor No.", LPurchHeader."Buy-from Vendor No.");

            IF LPurchLine.GET(LPurchHeader."Document Type", LPurchHeader."No.", _pWhseReceiptLine."Source Line No.") THEN BEGIN
                LLitige.VALIDATE("Prix Brut Réceptionné", LPurchLine."Direct Unit Cost");
                LLitige.VALIDATE("Remise 1 Réceptionnée", LPurchLine."Discount1 %");
                LLitige.VALIDATE("Remise 2 Réceptionnée", LPurchLine."Discount2 %");
                LLitige."Ref. Fournisseur" := LPurchLine."Item Reference No.";
            END;
        END;



        LLitige.VALIDATE("Receipt No.", _pReceiptLine."Document No.");
        LLitige.VALIDATE("Receipt Line No.", _pReceiptLine."Line No.");
        LLitige."Date de création" := TODAY;

        //recherche de l'entete


        IF _pWhseReceiptLine."Vendor Shipment No." = '' THEN BEGIN
            _WarehouseReceiptHeader.GET(_pWhseReceiptLine."No.");
            LLitige."N° BL fournisseur" := _WarehouseReceiptHeader."Vendor Shipment No.";
        END ELSE
            LLitige."N° BL fournisseur" := _pWhseReceiptLine."Vendor Shipment No.";



        // CFR le 16/04/2024 - R‚gie : ajout du champ Description
        LLitige.Description := _pWhseReceiptLine.GetInfoLigneTexte('DESC');
        LLitige.INSERT(TRUE);
    end;

    procedure "CréerIncidentTransport"(_pNoCommande: Code[20]; _pNoBl: Code[20])
    var
        LIncident: Record "Incident transport";
        LFrmIncident: Page "Fiche Incident Transport";
    begin
        // Création du litige
        CLEAR(LIncident);
        LIncident.INSERT(TRUE);

        // Affectation du numéro
        IF _pNoBl <> '' THEN
            LIncident.VALIDATE("Sales Shipment No.", _pNoBl)
        ELSE
            IF _pNoCommande <> '' THEN
                LIncident.VALIDATE("Order No.", _pNoCommande);

        LIncident.MODIFY();

        // Ouverture du litige
        LIncident.SETRECFILTER();
        LFrmIncident.SETRECORD(LIncident);
        LFrmIncident.SETTABLEVIEW(LIncident);
        LFrmIncident.RUN();
    end;
}

