codeunit 50035 "Gestion des tarifs Fourn. Temp"
{
    trigger OnRun()
    begin

        //FctInsertTempIntoTabTar('627510-FAC', TRUE, 'DIV');

        //FctStatistiquesTarifs('627510-FAC', '627510-FAC', TRUE, TRUE);
    end;

    var
        EnregistrementTemp2: Record "Temp Texte 250";

        ESK001Err: Label 'Type de format inconnu !';
        TempEnregistrement: Record "Temp Texte 250";
        ESK002Err: Label 'Le fournisseur doit être renseigné !';
        ESK003Lbl: Label ' MAJ  :  #1########## \ @2@@@@@@@@@@@@@@';
        NbEnr: Integer;
        NoENr: Integer;
        Window: Dialog;
        ESK004Err: Label 'La hausse doit être comprise en 0 et 200 % !';
        ESK005Err: Label 'Date Obligatoire !';



    procedure FctInsertTempIntoTabTar(_Fournisseur: Code[20]; _Raz: Integer; _Format: Code[10])
    var
        RecTarif: Record Tab_tar_tmp;
        RecTarifMaj: Record Tab_tar_tmp;
        RecTarifMaj2: Record Tab_tar_tmp;
        int_insert: Integer;
        int_modify: Integer;
    begin

        // Insertion de la table temporaire dans tab Tar

        IF NOT (_Format IN ['NH', 'DIV', 'QTE']) THEN
            ERROR(ESK001Err);


        // Remise à blanc des tarifs
        IF _Raz >= 1 THEN BEGIN
            RecTarif.RESET();
            RecTarif.SETRANGE(c_fournisseur, _Fournisseur);
            RecTarif.SETRANGE(Status, 'N');
            IF _Raz = 1 THEN BEGIN
                RecTarif.MODIFYALL(px_brut, 0);
                RecTarif.MODIFYALL(code_remise, '');
            END
            ELSE
                RecTarif.DELETEALL();
        END;


        // MCO Le 08-02-2017 => Régie
        // Nouveau code

        //Détection doublons
        IF (_Format IN ['DIV', 'QTE']) THEN BEGIN
            TempEnregistrement.RESET();
            IF TempEnregistrement.FINDSET() THEN BEGIN
                REPEAT
                    EnregistrementTemp2.RESET();
                    EnregistrementTemp2.SETRANGE(Ref_Fourn, TempEnregistrement.Ref_Fourn);
                    NbEnr := EnregistrementTemp2.COUNT;
                    IF NbEnr > 1 THEN
                        ERROR('Doublons : ' + TempEnregistrement.Ref_Fourn + '(' + FORMAT(EnregistrementTemp2.COUNT) + ')');
                UNTIL TempEnregistrement.NEXT() = 0;
            END;
        END;
        // FIN Détection doublons

        // Parcours la table pour insérer tab tar
        CLEAR(Window);
        Window.OPEN(ESK003Lbl);
        NoENr := 0;
        TempEnregistrement.RESET();
        NbEnr := TempEnregistrement.COUNT;
        IF TempEnregistrement.FINDFIRST() THEN
            REPEAT
                NoENr += 1;
                Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
                Window.UPDATE(2, ROUND((NoENr / 10000) / NbEnr, 1));
                RecTarif.INIT();
                RecTarif.c_fournisseur := _Fournisseur;
                RecTarif.Status := 'N';
                RecTarif.Date_Traitement := WORKDATE();
                // Chargement des variables en focntion du format
                CASE _Format OF
                    'NH':
                        FctChargeFormatNH(TempEnregistrement.Tmp, RecTarif);
                    'DIV':
                        FctChargeFormatDIV(TempEnregistrement.Tmp, RecTarif);
                    'QTE':
                        FctChargeFormatQTE(TempEnregistrement.Tmp, RecTarif);
                END;

                //Suppression si références fournisseurs déjà présente avec une réf active différente (MCO Le 08-02-2017)
                RecTarifMaj.RESET();
                RecTarifMaj.SETFILTER(Societe, '%1|%2', 'SP', '');
                RecTarifMaj.SETRANGE(Status, 'N');
                RecTarifMaj.SETRANGE(c_fournisseur, RecTarif.c_fournisseur);
                RecTarifMaj.SETRANGE(ref_fourni, RecTarif.ref_fourni);
                IF RecTarifMaj.FINDSET() THEN
                    REPEAT
                        //VD le 25-01-2017

                        IF (RecTarif.ref_active <> '') THEN BEGIN
                            IF ((RecTarif.ref_active <> RecTarifMaj.ref_active) AND (RecTarif.ref_fourni = RecTarifMaj.ref_fourni) AND (RecTarif.date_tarif <> RecTarifMaj.date_tarif)) THEN BEGIN
                                RecTarifMaj2 := RecTarifMaj;
                                RecTarifMaj2.DELETE();
                            END;
                        END
                    UNTIL RecTarifMaj.NEXT() = 0;
                // FIN Suppression si références fournisseurs déjà présente avec une réf active différente (MCO Le 08-02-2017)

                // Verif si existant
                RecTarifMaj.RESET();
                RecTarifMaj.SETFILTER(Societe, '%1|%2', 'SP', '');
                RecTarifMaj.SETRANGE(Status, 'N');
                RecTarifMaj.SETRANGE(c_fournisseur, RecTarif.c_fournisseur);
                RecTarifMaj.SETRANGE(ref_active, RecTarif.ref_active);
                IF RecTarifMaj.FINDFIRST() THEN BEGIN
                    //VD le 26/01/2017 condition de MAJ du tarif
                    IF (RecTarif.px_brut <> RecTarifMaj.px_brut) OR (RecTarif.code_remise <> RecTarifMaj.code_remise) OR (RecTarif.date_tarif <> RecTarifMaj.date_tarif) THEN BEGIN
                        // Ancien code
                        //LM le 17-09-12 maj que si prix achat inferieur
                        // IF RecTarif.px_brut< RecTarifMaj.px_brut THEN
                        //BEGIN
                        RecTarifMaj.px_brut := RecTarif.px_brut;
                        RecTarifMaj.code_remise := RecTarif.code_remise;
                        RecTarifMaj.date_tarif := RecTarif.date_tarif;
                        // RecTarifMaj.Date_Traitement := RecTarif.Date_Traitement;
                        RecTarifMaj.Date_Traitement := WORKDATE();
                        CASE _Format OF
                            'NH':
                                BEGIN
                                END;
                            'DIV':
                                BEGIN
                                    RecTarifMaj.ref_fourni := RecTarif.ref_fourni;
                                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
                                    RecTarifMaj.divers5 := RecTarif.divers5;
                                    RecTarifMaj.poids := RecTarif.poids;
                                    RecTarifMaj.condit_achat := RecTarif.condit_achat;
                                    // MCO Le 08-02-2017 => Régie
                                    RecTarifMaj.unite_achat := RecTarif.unite_achat;
                                    // FIN MCO Le 08-02-2017
                                END;
                            'QTE':
                                BEGIN
                                    RecTarifMaj.ref_fourni := RecTarif.ref_fourni;
                                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
                                    RecTarifMaj.divers5 := RecTarif.divers5;
                                    RecTarifMaj.poids := RecTarif.poids;
                                    RecTarifMaj.condit_achat := RecTarif.condit_achat;
                                    // MCO Le 08-02-2017 => Régie
                                    RecTarifMaj.unite_achat := RecTarif.unite_achat;
                                    // FIN MCO Le 08-02-2017
                                    RecTarifMaj.qte_col := RecTarif.qte_col;
                                    RecTarifMaj.qte_col1 := RecTarif.qte_col1;
                                    RecTarifMaj.prix_brut1 := RecTarif.prix_brut1;
                                    RecTarifMaj.qte_col2 := RecTarif.qte_col2;
                                    RecTarifMaj.prix_brut2 := RecTarif.prix_brut2;
                                    RecTarifMaj.qte_col3 := RecTarif.qte_col3;
                                    RecTarifMaj.prix_brut3 := RecTarif.prix_brut3;
                                END;

                        END;
                        RecTarifMaj.MODIFY();
                        int_modify := int_modify + 1;
                    END
                END
                ELSE BEGIN
                    RecTarifMaj := RecTarif;
                    RecTarifMaj.Insert();
                    int_insert := int_insert + 1;
                END;

            UNTIL TempEnregistrement.NEXT() = 0;
        Window.CLOSE();
        //MESSAGE('insertion %1- modification %2',int_insert,int_modify);

        // Ancien code fonctionnel
        //CLEAR(Window);
        //Window.OPEN(ESK003);

        //EnregistrementTemp.RESET();
        //NbEnr := EnregistrementTemp.COUNT;
        //IF EnregistrementTemp.FINDFIRST () THEN
        //  REPEAT
        //    NoENr += 1;
        //    Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
        //    Window.UPDATE(2, ROUND((NoENr / 10000) / NbEnr,1));
        //    RecTarif.c_fournisseur := _Fournisseur;
        //    RecTarif.Status := 'N';
        //    RecTarif.Date_Traitement := WORKDATE;
        //    // Chargement des variables en focntion du format
        //    CASE _Format OF
        //       'NH'  : FctChargeFormatNH(EnregistrementTemp.Tmp, RecTarif);
        //       'DIV' : FctChargeFormatDIV(EnregistrementTemp.Tmp, RecTarif);
        //       'QTE' : FctChargeFormatQTE(EnregistrementTemp.Tmp, RecTarif);
        //    END;

        //    // Verif si existant
        //    RecTarifMaj.RESET();
        //    RecTarifMaj.SETFILTER(Societe,'%1|%2','SP','');
        //    RecTarifMaj.SETRANGE(Status, 'N');
        //    RecTarifMaj.SETRANGE(c_fournisseur, RecTarif.c_fournisseur);
        //    RecTarifMaj.SETRANGE(ref_active, RecTarif.ref_active);
        //    IF RecTarifMaj.FINDFIRST () THEN
        //      BEGIN
        //        //LM le 17-09-12 maj que si prix achat inferieur
        //        IF RecTarif.px_brut< RecTarifMaj.px_brut THEN
        //        BEGIN
        //          RecTarifMaj.px_brut       := RecTarif.px_brut;
        //          RecTarifMaj.code_remise   := RecTarif.code_remise;
        //          RecTarifMaj.date_tarif    := RecTarif.date_tarif;
        //          RecTarifMaj.Date_Traitement := RecTarif.Date_Traitement;
        //          CASE _Format OF
        //            'NH'  : BEGIN END;
        //            'DIV' : BEGIN
        //                    RecTarifMaj.ref_fourni    := RecTarif.ref_fourni;
        //                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
        //                    RecTarifMaj.divers5       := RecTarif.divers5;
        //                    RecTarifMaj.poids         := RecTarif.poids;
        //                    RecTarifMaj.condit_achat  := RecTarif.condit_achat;
        //                    END;
        //            'QTE' : BEGIN
        //                    RecTarifMaj.ref_fourni    := RecTarif.ref_fourni;
        //                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
        //                    RecTarifMaj.divers5       := RecTarif.divers5;
        //                    RecTarifMaj.poids         := RecTarif.poids;
        //                    RecTarifMaj.condit_achat  := RecTarif.condit_achat;
        //                    RecTarifMaj.qte_col       := RecTarif.qte_col;
        //                    RecTarifMaj.qte_col1      := RecTarif.qte_col1;
        //                    RecTarifMaj.prix_brut1    := RecTarif.prix_brut1;
        //                    RecTarifMaj.qte_col2      := RecTarif.qte_col2;
        //                    RecTarifMaj.prix_brut2    := RecTarif.prix_brut2;
        //                    RecTarifMaj.qte_col3      := RecTarif.qte_col3;
        //                    RecTarifMaj.prix_brut3    := RecTarif.prix_brut3;
        //                    END;

        //          END;
        //          RecTarifMaj.MODIFY();
        //        END
        //      END
        //    ELSE
        //      BEGIN
        //        RecTarifMaj := RecTarif;
        //        RecTarifMaj.Insert();
        //      END;

        //  UNTIL EnregistrementTemp.NEXT() = 0;
        //Window.CLOSE;
        // FIN MCO Le 08-02-2017



        //Détection doublons

        ////IF EnregistrementTemp.FINDSET() THEN
        ////BEGIN
        ////      REPEAT
        ////
        ////  imp_ref_fourni := '*'+COPYSTR(EnregistrementTemp.Tmp, 21, 20)+'*';
        ////  imp_ref_fourni := CONVERTSTR(imp_ref_fourni, '.', ' ') ;
        ////  imp_ref_fourni := CONVERTSTR(imp_ref_fourni, '....', '!!!!');
        ////
        ////     EnregistrementTemp2.SETFILTER(Tmp, imp_ref_fourni);

        ////       NbEnr := EnregistrementTemp2.COUNT;

        ////     UNTIL EnregistrementTemp.NEXT() = 0 ;
        ////     IF  NbEnr > 1 THEN
        ////     ERROR('Doublons : '+imp_ref_fourni+'('+FORMAT(EnregistrementTemp2.COUNT)+')');
        //// END;
        ////Fin dédection coublons




        //CLEAR(Window);
        //Window.OPEN(ESK003);



        //EnregistrementTemp.RESET();

        //NbEnr := EnregistrementTemp.COUNT;
        //IF EnregistrementTemp.FINDFIRST () THEN
        //  REPEAT
        //    NoENr += 1;
        //    Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
        //    Window.UPDATE(2, ROUND((NoENr / 10000) / NbEnr,1));
        //    RecTarif.c_fournisseur := _Fournisseur;
        //    RecTarif.Status := 'N';
        //    RecTarif.Date_Traitement := WORKDATE;
        //    // Chargement des variables en focntion du format
        //    CASE _Format OF
        //       'NH'  : FctChargeFormatNH(EnregistrementTemp.Tmp, RecTarif);
        //       'DIV' : FctChargeFormatDIV(EnregistrementTemp.Tmp, RecTarif);
        //       'QTE' : FctChargeFormatQTE(EnregistrementTemp.Tmp, RecTarif);
        //    END;

        //// //Suppression si références fournisseurs déjà présente avec une réf active différente (VD 25/01/2017)


        ////    RecTarifMaj.RESET();
        ////    RecTarifMaj.SETFILTER(Societe,'%1|%2','SP','');
        ////    RecTarifMaj.SETRANGE(Status, 'N');
        ////    RecTarifMaj.SETRANGE(c_fournisseur, RecTarif.c_fournisseur);
        ////    RecTarifMaj.SETRANGE(ref_fourni, RecTarif.ref_fourni);

        ////    IF RecTarifMaj.FINDFIRST () THEN
        ////        BEGIN
        ////           //VD le 25-01-2017
        ////                IF (RecTarif.ref_active <> '') THEN
        ////                BEGIN
        ////                 IF ((RecTarif.ref_active <> RecTarifMaj.ref_active)  AND (RecTarif.ref_fourni = RecTarifMaj.ref_fourni) AND (RecTarif.date_tarif <> RecTarifMaj.date_tarif)) THEN
        ////          RecTarifMaj.DELETE;
        ////          END
        ////          END;

        //    // Verif si existant
        //    RecTarifMaj.RESET();
        //    RecTarifMaj.SETFILTER(Societe,'%1|%2','SP','');
        //    RecTarifMaj.SETRANGE(Status, 'N');
        //    RecTarifMaj.SETRANGE(c_fournisseur, RecTarif.c_fournisseur);
        //    RecTarifMaj.SETRANGE(ref_active, RecTarif.ref_active);

        //    IF RecTarifMaj.FINDFIRST () THEN
        //    BEGIN
        //        //VD le 26/01/2017 condition de MAJ du tarif
        //        IF (RecTarif.px_brut <> RecTarifMaj.px_brut) OR (RecTarif.code_remise <> RecTarifMaj.code_remise) OR (RecTarif.date_tarif <> RecTarifMaj.date_tarif) THEN
        //        BEGIN
        //          RecTarifMaj.px_brut       := RecTarif.px_brut;
        //          RecTarifMaj.code_remise   := RecTarif.code_remise;
        //          RecTarifMaj.date_tarif    := RecTarif.date_tarif;
        //          RecTarifMaj.Date_Traitement := RecTarif.Date_Traitement;
        //          RecTarifMaj.ref_active    := RecTarif.ref_active;

        //          CASE _Format OF
        //            'NH'  : BEGIN END;
        //            'DIV' : BEGIN
        //                    RecTarifMaj.ref_fourni    := RecTarif.ref_fourni;
        //                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
        //                    RecTarifMaj.divers5       := RecTarif.divers5;
        //                    RecTarifMaj.poids         := RecTarif.poids;
        //                    RecTarifMaj.condit_achat  := RecTarif.condit_achat;
        //                    END;
        //            'QTE' : BEGIN
        //                    RecTarifMaj.ref_fourni    := RecTarif.ref_fourni;
        //                    RecTarifMaj.design_fourni := RecTarif.design_fourni;
        //                    RecTarifMaj.divers5       := RecTarif.divers5;
        //                    RecTarifMaj.poids         := RecTarif.poids;
        //                    RecTarifMaj.condit_achat  := RecTarif.condit_achat;
        //                    RecTarifMaj.qte_col       := RecTarif.qte_col;
        //                    RecTarifMaj.qte_col1      := RecTarif.qte_col1;
        //                    RecTarifMaj.prix_brut1    := RecTarif.prix_brut1;
        //                    RecTarifMaj.qte_col2      := RecTarif.qte_col2;
        //                    RecTarifMaj.prix_brut2    := RecTarif.prix_brut2;
        //                    RecTarifMaj.qte_col3      := RecTarif.qte_col3;
        //                    RecTarifMaj.prix_brut3    := RecTarif.prix_brut3;
        //                    END;

        //          END;

        //          ERROR('MODIFY');
        //
        //          IF RecTarifMaj.ref_active <> '' THEN
        //
        //          RecTarifMaj.MODIFY();

        //        END
        //      END
        //    ELSE
        //      BEGIN

        //        ERROR('INSERT');
        //        RecTarifMaj := RecTarif;
        //        RecTarifMaj.Insert();
        //      END;

        //  UNTIL EnregistrementTemp.NEXT() = 0;
        //Window.CLOSE;
    end;

    procedure FctChargeFormatNH(_ChaineTmp: Text[250]; var RecTarif: Record Tab_tar_tmp)
    var
        jj: Integer;
        mm: Integer;
        aa: Integer;
    begin

        RecTarif.ref_active := COPYSTR(_ChaineTmp, 1, 18);
        RecTarif.ref_fourni := COPYSTR(_ChaineTmp, 1, 18);
        // MC Le 30-03-2012 => Correction du BUG sur la longueur
        RecTarif.design_fourni := COPYSTR(_ChaineTmp, 19, 30);
        //RecTarif.design_fourni  :=  COPYSTR(_ChaineTmp, 19, 40);
        // FIN MC Le 30-03-2012
        RecTarif.divers1 := COPYSTR(_ChaineTmp, 59, 1);

        EVALUATE(jj, COPYSTR(_ChaineTmp, 67, 2));
        EVALUATE(mm, COPYSTR(_ChaineTmp, 65, 2));
        EVALUATE(aa, COPYSTR(_ChaineTmp, 61, 4));
        RecTarif.date_tarif := DMY2DATE(jj, mm, aa);


        IF EVALUATE(RecTarif.px_brut, COPYSTR(_ChaineTmp, 69, 11)) THEN;
        RecTarif.px_brut := RecTarif.px_brut / 100;

        IF EVALUATE(RecTarif.poids, COPYSTR(_ChaineTmp, 80, 13)) THEN;

        RecTarif.condit_achat := COPYSTR(_ChaineTmp, 93, 5);
        RecTarif.divers2 := COPYSTR(_ChaineTmp, 99, 3);
        RecTarif.code_remise := COPYSTR(_ChaineTmp, 102, 1);
        RecTarif.comodity_code := COPYSTR(_ChaineTmp, 103, 5);
        RecTarif.divers4 := COPYSTR(_ChaineTmp, 108, 5);
        RecTarif.code_retour := COPYSTR(_ChaineTmp, 113, 1);
    end;

    procedure FctChargeFormatDIV(_ChaineTmp: Text[250]; var RecTarif: Record Tab_tar_tmp)
    var
       
    begin

        RecTarif.ref_active := COPYSTR(_ChaineTmp, 1, 20);
        RecTarif.ref_fourni := COPYSTR(_ChaineTmp, 21, 20);
        // CFR le 10/05/2022 => R‚gie : FctChargeFormatDIV & FctChargeFormatQTE : D‚signation passe de 30 … 50 donc d‚calage des champs suivants
        RecTarif.design_fourni := COPYSTR(_ChaineTmp, 41, 50);//41, 30);
        RecTarif.condit_achat := COPYSTR(_ChaineTmp, 91, 10);//71, 10);... et ainsi de suite jusqu'au bout...
        IF EVALUATE(RecTarif.poids, COPYSTR(_ChaineTmp, 101, 10)) THEN;
        RecTarif.code_remise := COPYSTR(_ChaineTmp, 111, 10);
        IF EVALUATE(RecTarif.date_tarif, COPYSTR(_ChaineTmp, 121, 10)) THEN;
        IF EVALUATE(RecTarif.divers5, COPYSTR(_ChaineTmp, 131, 10)) THEN;
        IF EVALUATE(RecTarif.px_brut, COPYSTR(_ChaineTmp, 141, 10)) THEN;
        // MCO Le 08-02-2017 => R‚gie
        RecTarif.unite_achat := COPYSTR(_ChaineTmp, 151, 10);
        // FIN MCO Le 08-02-2017

        //IF EVALUATE(RecTarif.divers2   , COPYSTR(_ChaineTmp, 131,3)) THEN;
        //IF EVALUATE(RecTarif.comodity_code  , COPYSTR(_ChaineTmp, 134, 5)) THEN;
        //IF EVALUATE(RecTarif.divers4   , COPYSTR(_ChaineTmp, 139, 5)) THEN;
    end;

    procedure FctChargeFormatQTE(_ChaineTmp: Text[250]; var RecTarif: Record Tab_tar_tmp)
    var
        jj: Integer;
        mm: Integer;
        aa: Integer;
    begin

        RecTarif.ref_active := COPYSTR(_ChaineTmp, 1, 20);
        RecTarif.ref_fourni := COPYSTR(_ChaineTmp, 21, 20);
        // CFR le 10/05/2022 => R‚gie : FctChargeFormatDIV & FctChargeFormatQTE : D‚signation passe de 30 … 50 donc d‚calage des champs suivants
        RecTarif.design_fourni := COPYSTR(_ChaineTmp, 41, 50);//41, 30);
        RecTarif.condit_achat := COPYSTR(_ChaineTmp, 91, 10);//71, 10);... et ainsi de suite jusqu'au bout...
        IF EVALUATE(RecTarif.poids, COPYSTR(_ChaineTmp, 101, 10)) THEN;
        RecTarif.code_remise := COPYSTR(_ChaineTmp, 111, 10);
        IF EVALUATE(RecTarif.date_tarif, COPYSTR(_ChaineTmp, 121, 10)) THEN;
        IF EVALUATE(RecTarif.divers5, COPYSTR(_ChaineTmp, 131, 10)) THEN;
        // MCO Le 08-02-2017 => R‚gie
        RecTarif.unite_achat := COPYSTR(_ChaineTmp, 141, 10);
        // FIN MCO Le 08-02-2017
        IF EVALUATE(RecTarif.qte_col, COPYSTR(_ChaineTmp, 151, 10)) THEN;
        IF EVALUATE(RecTarif.px_brut, COPYSTR(_ChaineTmp, 161, 10)) THEN;
        IF EVALUATE(RecTarif.qte_col1, COPYSTR(_ChaineTmp, 171, 10)) THEN;
        IF EVALUATE(RecTarif.prix_brut1, COPYSTR(_ChaineTmp, 181, 10)) THEN;
        IF EVALUATE(RecTarif.qte_col2, COPYSTR(_ChaineTmp, 191, 10)) THEN;
        IF EVALUATE(RecTarif.prix_brut2, COPYSTR(_ChaineTmp, 201, 10)) THEN;
        IF EVALUATE(RecTarif.qte_col3, COPYSTR(_ChaineTmp, 211, 10)) THEN;
        IF EVALUATE(RecTarif.prix_brut3, COPYSTR(_ChaineTmp, 221, 10)) THEN;
    end;

    procedure "--- Statistiques ---"()
    begin
    end;

    procedure FctStatistiquesTarifs(_FournisseurArticle: Code[20]; _FournisseurTarif: Code[20]; _ParRefActive: Boolean; _AvecAncienneRemise: Boolean)
    var
        ItemVendor: Record "Item Vendor";
        Item: Record Item;
        lVendor: Record Vendor;
        RecTarif: Record Tab_tar_tmp;
        lSalesPrice: Record "Sales Price";
        TempPurchPrice: Record "Purchase Price" temporary;
        TempPurchDisc: Record "Purchase Line Discount" temporary;
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        SingleInstance: Codeunit SingleInstance;
        CalcConso: Codeunit CalculConso;
        Ligne: Text[1024];

        AncienPrixBrut: Decimal;
        AncienPrixNet: Decimal;
        AncienneRemise: Decimal;
        NvPrixBrut: Decimal;
        NvPrixNet: Decimal;
        NvRemise: Decimal;
        VariationLigneBrut: Decimal;
        VariationLigneNet: Decimal;
        AncienMeilleurFrn: Code[10];
        AncienMeilleurPrix: Decimal;
        NouveauMeilleurFrn: Code[10];
        NouveauMeilleurPrix: Decimal;
        ChangementFrn: Code[10];
        TotalAncienMeilleurPrix: Decimal;
        TotalNouveauMeilleurPrix: Decimal;
        TotalAncienBrut: Decimal;
        TotalAncienNet: Decimal;
        TotalNvBrut: Decimal;
        TotalNvNet: Decimal;
        NbChangementRemise: Integer;
        NbChangementFrn: Integer;
        FichierStats: File;
        Fichier: Text[1024];

        DateDeb: Date;
        DateFin: Date;
        ConsommationAnnuelle: Decimal;

        lPrixCatalogue: Decimal;
        lCoefVente: Decimal;
        lUnitPrice: Decimal;
        lStartingDate: Date;
        lAncienMeilleurFrnName: Text[50];
        lNouveauMeilleurFrnName: Text[50];

        lCodeRemiseInteger: Integer;
        lCodeRemiseCode: Code[10];
        IsInteger: Boolean;
    begin

        FichierStats.TEXTMODE(TRUE);
        FichierStats.WRITEMODE(TRUE);

        Fichier := '\\srv-fichiers\Partage\Achats\Fichiers pour analyse tarif achat CNH\Stats_' + CONVERTSTR(DELCHR(USERID, '=', '/: '), '\', '_') + '.csv';
        IF FILE.EXISTS(Fichier) THEN
            FichierStats.OPEN(Fichier)
        ELSE
            FichierStats.CREATE(Fichier);

        FichierStats.SEEK(FichierStats.LEN);

        IF NOT FILE.EXISTS(Fichier) THEN
            FichierStats.CREATE(Fichier);


        DateFin := CALCDATE('<-1D>', DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3)));
        DateDeb := CALCDATE('<-1D>', DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3) - 1));

        Ligne := 'ref_active;designation;marque;anc prix brut;anc remise;anc prix net;anc Qté mini;anc date tarif;nouv prix brut;nouv remise' +
        // CFR le 23/03/2022 => R‚gie : ajout nouvelles valeurs statistiques
        //  ';nouv prix net;nouv Qt‚ mini;nouv date tarif;maxi1;variation brut;variation net;anc_mf_p;anc_mf_f;nou_mf_p;nou_mf_f;chang_four;ref_fourni;' +
        ';nouv prix net;nouv Qté mini;nouv date tarif;maxi1;variation brut (%);variation net (%);Variation nette (Euros);anc_mf_p;anc_mf_f;Nom ancien meilleur four;nou_mf_p;nou_mf_f;Nom nouveau meilleur four;chang_four;ref_fourni;' +

        'stock_mort;date stock mort;utilisa. stock mort;code appro;Statut Approvisionement; Statut Qualite;' +
        // CFR le 23/03/2022 => R‚gie : ajout nouvelles valeurs statistiques
        'Prix catalogue (Tous client);Coef vente (Tous client);Px unitaire vente (Tous client);Date tarif vente (Tous client);';
        // FIN CFR le 23/03/2022
        FichierStats.WRITE(Ligne);



        CLEAR(Window);
        Window.OPEN(ESK003Lbl);



        ItemVendor.RESET();
        ItemVendor.SETRANGE("Vendor No.", _FournisseurArticle);

        //ItemVendor.SETRANGE("Item No.", 'SP095168');

        NbEnr := ItemVendor.COUNT;
        IF ItemVendor.FINDFIRST() THEN
            REPEAT

                NoENr += 1;
                Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
                Window.UPDATE(2, ROUND(NoENr * 10000 / NbEnr, 1));

                Item.GET(ItemVendor."Item No.");

                AncienMeilleurFrn := '';
                lAncienMeilleurFrnName := '';
                AncienMeilleurPrix := 0;
                NouveauMeilleurFrn := '';
                lNouveauMeilleurFrnName := '';
                NouveauMeilleurPrix := 0;


                // Recherche dans tab_tar
                CLEAR(RecTarif);
                RecTarif.RESET();
                RecTarif.SETFILTER(Societe, '%1|%2', 'SP', '');
                RecTarif.SETRANGE(c_fournisseur, _FournisseurTarif);
                RecTarif.SETRANGE(Status, 'N');
                IF _ParRefActive THEN
                    RecTarif.SETRANGE(ref_active, Item."No. 2")
                ELSE
                    RecTarif.SETRANGE(ref_fourni, ItemVendor."Vendor Item No.");

                IF RecTarif.FINDLAST() THEN;

                // Recherche de l'ancien prix d'achat net
                AncienPrixBrut := 0;
                AncienPrixNet := 0;
                AncienneRemise := 0;
                LCodeunitsFunctions.GetUnitCost(TempPurchPrice, _FournisseurArticle, Item."No.", '', '', '', WORKDATE(), TRUE, 1, '', AncienPrixBrut);
                AncienneRemise := LCodeunitsFunctions.GetPurchDisc(TempPurchDisc, _FournisseurArticle, Item."No.", '', '', '', WORKDATE(),
                TRUE, 1, 3, '', 0);
                // CFR le 23/03/2022 => R‚gie : Si la quantit‚ mini du tarif est sup‚rieure … 1, alors de l'utilise quand mˆme
                IF (AncienPrixBrut = 0) AND (TempPurchPrice.COUNT() > 0) THEN BEGIN
                    TempPurchPrice.FINDFIRST();
                    AncienPrixBrut := TempPurchPrice."Direct Unit Cost";
                END;
                // FIN CFR le 23/03/2022
                //TEST VD

                //ERROR(FormatDecimal());
                //ERROR(FORMAT(AncienneRemise));


                // Message(FORMAT(AncienneRemise));

                AncienPrixNet := FctCalcPrixNet(AncienPrixBrut, AncienneRemise);
                // CFR le 17/04/2024 - R‚gie : dans les statitiques correction pour les Tab_Tar_Tmp.code_remise de type integer(suppression des d‚cimales)
                IsInteger := FALSE;
                lCodeRemiseCode := RecTarif.code_remise;
                IF EVALUATE(lCodeRemiseInteger, lCodeRemiseCode) THEN
                    IsInteger := TRUE
                ELSE BEGIN
                    lCodeRemiseCode := CONVERTSTR(lCodeRemiseCode, '.', ',');
                    IF EVALUATE(lCodeRemiseInteger, lCodeRemiseCode) THEN
                        IsInteger := TRUE
                    ELSE BEGIN
                        lCodeRemiseCode := CONVERTSTR(lCodeRemiseCode, ',', '.');
                        IF EVALUATE(lCodeRemiseInteger, lCodeRemiseCode) THEN
                            IsInteger := TRUE
                    END;
                END;
                IF (IsInteger) THEN
                    RecTarif.code_remise := FORMAT(lCodeRemiseInteger);

                // FIN CFR le 17/04/2024

                // Recherche nouveau Prix
                NvPrixBrut := 0;
                NvPrixNet := 0;
                NvRemise := 0;
                NvPrixBrut := RecTarif.px_brut;
                IF _AvecAncienneRemise THEN
                    NvRemise := LCodeunitsFunctions.GetPurchDisc(TempPurchDisc, _FournisseurArticle, Item."No.", '', '', '', WORKDATE(), TRUE, 1, 3,
                          ItemVendor."Code Remise", 0)
                ELSE BEGIN
                    // CFR le 23/03/2022 => R‚gie : Si pas de code remise dans TAB_TAR, on ne va pas chercher de remise
                    IF (RecTarif.code_remise <> '') THEN
                        NvRemise := LCodeunitsFunctions.GetPurchDisc(TempPurchDisc, _FournisseurArticle, Item."No.", '', '', '', WORKDATE(), TRUE, 1, 3,
                                    RecTarif.code_remise, 0);
                END;
                NvPrixNet := FctCalcPrixNet(NvPrixBrut, NvRemise);


                //TEST VD

                //ERROR(FormatDecimal());
                //ERROR(FORMAT(NvRemise+'..'));




                IF (NvPrixNet <> 0) AND (AncienPrixNet <> 0) THEN BEGIN
                    ConsommationAnnuelle := CalcConso.RechecherConsommation(Item."No.", DateDeb, DateFin);
                    // Calcul des variables
                    VariationLigneBrut := (NvPrixBrut - AncienPrixBrut) / AncienPrixBrut * 100;
                    TotalAncienBrut += AncienPrixBrut * ConsommationAnnuelle;
                    TotalNvBrut += NvPrixBrut * ConsommationAnnuelle;

                    VariationLigneNet := (NvPrixNet - AncienPrixNet) / AncienPrixNet * 100;
                    TotalAncienNet += AncienPrixNet * ConsommationAnnuelle;
                    TotalNvNet += NvPrixNet * ConsommationAnnuelle;

                    // Un peu flou pour moi mais doit correspondre à ce que fait DBX
                    AncienMeilleurFrn := SingleInstance.GetBestPurchPrice(Item."No.", 1, 2, AncienMeilleurPrix, '');
                    IF lVendor.GET(AncienMeilleurFrn) THEN
                        lAncienMeilleurFrnName := lVendor.Name;
                    NouveauMeilleurFrn := SingleInstance.GetBestPurchPrice(Item."No.", 1, 2, NouveauMeilleurPrix, '<>' + _FournisseurTarif);
                    IF lVendor.GET(NouveauMeilleurFrn) THEN
                        lNouveauMeilleurFrnName := lVendor.Name;
                    IF (NvPrixNet <> 0) AND (NvPrixNet < NouveauMeilleurPrix) THEN BEGIN
                        NouveauMeilleurFrn := RecTarif.c_fournisseur;
                        NouveauMeilleurPrix := NvPrixNet;
                    END;

                    ChangementFrn := '';
                    // CFR le 13/04/2022 => R‚gie : Gestion du nouveau fournisseur
                    //IF NouveauMeilleurFrn <> AncienMeilleurFrn THEN
                    IF (NouveauMeilleurFrn <> '') AND (NouveauMeilleurFrn <> AncienMeilleurFrn) THEN BEGIN
                        ChangementFrn := 'CHANGE FRN';
                        NbChangementFrn += 1;
                    END;

                    TotalAncienMeilleurPrix += AncienMeilleurPrix * ConsommationAnnuelle;
                    TotalNouveauMeilleurPrix += NouveauMeilleurPrix * ConsommationAnnuelle;

                    IF (NOT _AvecAncienneRemise) AND (RecTarif.code_remise <> ItemVendor."Code Remise") THEN
                        NbChangementRemise += 1;

                    //Supprimé option AvecAncienne remise
                    IF _AvecAncienneRemise THEN
                        RecTarif.code_remise := ItemVendor."Code Remise";

                    // CFR le 23/03/2022 => R‚gie : ajout nouvelles valeurs statistiques
                    IF lVendor.GET(AncienMeilleurFrn) THEN
                        lAncienMeilleurFrnName := lVendor.Name;
                    IF lVendor.GET(NouveauMeilleurFrn) THEN
                        lNouveauMeilleurFrnName := lVendor.Name;

                    lSalesPrice.SETRANGE("Item No.", Item."No.");
                    lSalesPrice.SETRANGE("Sales Type", lSalesPrice."Sales Type"::"All Customers");
                    lSalesPrice.SETRANGE("Type de commande", lSalesPrice."Type de commande"::Tous);
                    lSalesPrice.SETFILTER("Starting Date", '..%1', TODAY());
                    lSalesPrice.SETFILTER("Ending Date", '%1..|%2', TODAY(), 0D);
                    IF lSalesPrice.FINDFIRST() THEN BEGIN
                        lPrixCatalogue := lSalesPrice."Prix catalogue";
                        lCoefVente := lSalesPrice.Coefficient;
                        lUnitPrice := lSalesPrice."Unit Price";
                        lStartingDate := lSalesPrice."Starting Date";
                    END
                    ELSE BEGIN
                        lPrixCatalogue := 0;
                        lCoefVente := 0;
                        lUnitPrice := 0;
                        lStartingDate := 19000101D;
                    END;
                    // FIN CFR le 23/03/2022

                    Ligne := '';
                    Ligne += Item."No. 2" + ';';
                    Ligne += Item.Description + ';';
                    Ligne += Item."Manufacturer Code" + ';';
                    Ligne += FormatDecimal(AncienPrixBrut) + ';';
                    Ligne += ItemVendor."Code Remise" + ';';
                    Ligne += FormatDecimal(AncienPrixNet) + ';';
                    Ligne += FormatDecimal(TempPurchPrice."Minimum Quantity") + ';';
                    Ligne += FORMAT(TempPurchPrice."Starting Date") + ';'; // GR le 10-04-2019 : ajout ancien date tarif
                    Ligne += FormatDecimal(RecTarif.px_brut) + ';';
                    Ligne += RecTarif.code_remise + ';';
                    Ligne += FormatDecimal(NvPrixNet) + ';';
                    Ligne += FormatDecimal(RecTarif.qte_col) + ';';
                    Ligne += FORMAT(RecTarif.date_tarif) + ';';  //Ligne += FORMAT(RecTarif.Date_Traitement) + ';';// CFR le 13/04/2022 => R‚gie
                    Ligne += FormatDecimal(ConsommationAnnuelle) + ';';
                    Ligne += FormatDecimal(VariationLigneBrut) + ';';
                    Ligne += FormatDecimal(VariationLigneNet) + ';';
                    Ligne += FormatDecimal(NvPrixNet - AncienPrixNet) + ';'; //Variation nette en euro - // CFR le 13/04/2022 => R‚gie
                    Ligne += FormatDecimal(AncienMeilleurPrix) + ';';
                    Ligne += FORMAT(AncienMeilleurFrn) + ';';
                    Ligne += FORMAT(lAncienMeilleurFrnName) + ';';           //Nom fournisseur ancien - // CFR le 13/04/2022 => R‚gie
                    Ligne += FormatDecimal(NouveauMeilleurPrix) + ';';
                    Ligne += NouveauMeilleurFrn + ';';
                    Ligne += FORMAT(lNouveauMeilleurFrnName) + ';';          //Nom fournisseur nouveau - // CFR le 13/04/2022 => R‚gie
                    Ligne += ChangementFrn + ';';
                    Ligne += ItemVendor."Vendor Item No." + ';';
                    //Ajout VD 12/05/2015
                    Ligne += FORMAT(Item."Stock Mort") + ';';
                    Ligne += FORMAT(Item."date maj stock mort") + ';';
                    Ligne += Item."user maj stock mort" + ';';
                    Ligne += Item."Code Appro" + ';';
                    Ligne += FORMAT(ItemVendor."Statut Approvisionnement") + ';';
                    Ligne += FORMAT(ItemVendor."Statut Qualité") + ';';
                    // CFR le 23/03/2022 => R‚gie : ajout nouvelles valeurs statistiques
                    Ligne += FormatDecimal(lPrixCatalogue) + ';';            //Prix catalogue
                    Ligne += FormatDecimal(lCoefVente) + ';';                //coef de vente
                    Ligne += FormatDecimal(lUnitPrice) + ';';                //prix unitaire vente
                    Ligne += FORMAT(lStartingDate) + ';';                    //date d‚but
                                                                             // FIN CFR le 23/03/2022


                    FichierStats.WRITE(Ligne);

                END;

            UNTIL ItemVendor.NEXT() = 0;

        Ligne := 'Variation Tarif Brut en % : ';
        IF TotalAncienBrut <> 0 THEN
            Ligne += FormatDecimal((TotalNvBrut - TotalAncienBrut) / TotalAncienBrut * 100);
        FichierStats.WRITE(Ligne);

        Ligne := 'Variation Tarif Net  en % : ';
        IF TotalAncienNet <> 0 THEN
            Ligne += FormatDecimal((TotalNvNet - TotalAncienNet) / TotalAncienNet * 100);
        FichierStats.WRITE(Ligne);

        Ligne := 'Variation Achat      en % : ';
        IF TotalNouveauMeilleurPrix <> 0 THEN
            Ligne += FormatDecimal((TotalNouveauMeilleurPrix - TotalAncienMeilleurPrix) /
                                                        TotalAncienMeilleurPrix * 100);
        FichierStats.WRITE(Ligne);
        Ligne := 'Nb Changement Remise      : ' + FORMAT(NbChangementRemise);
        FichierStats.WRITE(Ligne);
        Ligne := 'Nb Changement Forunisseur : ' + FORMAT(NbChangementFrn);
        FichierStats.WRITE(Ligne);


        FichierStats.CLOSE();
        Window.CLOSE();
    end;

    procedure FctCalcPrixNet(_Prix: Decimal; _Remise: Decimal): Decimal
    var
        PuNet: Decimal;
    begin
        PuNet := _Prix * (1 - _Remise / 100);
        EXIT(PuNet);
    end;

    procedure "--- Implémentation des tarifs"()
    begin
    end;

    procedure "ImplémenterTarif"(_Reference: Code[50]; _Fournisseur: Code[20]; _CLoture: Boolean)
    var
        Fournisseur: Record Vendor;
        RecTarif: Record Tab_tar_tmp;
        FournisseurDuGroupe: Record Vendor;
        Item: Record Item;
        LAncPrix: Record "Purchase Price";
        ItemVendor: Record "Item Vendor";
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        DeviseOrigineTarif: Code[10];
        "TarifAppliqué": Boolean;
        ExisteItemVendor: Boolean;
        ModifierCdePourRefFou: Boolean;

    begin

        //LM le 02-08-2012 =>fct mise en commantaire car 10 fois la question en création article
        //ModifierCdePourRefFou := CONFIRM(ESK006, TRUE, _Fournisseur);
        ModifierCdePourRefFou := FALSE;

        IF _Fournisseur <> '' THEN Fournisseur.GET(_Fournisseur);



        // Recherche des infos à intégrer
        RecTarif.RESET();
        RecTarif.SETCURRENTKEY(Societe, Status, c_fournisseur, ref_active); // AD Le 09-03-2016
        RecTarif.SETFILTER(Societe, '%1|%2', 'SP', '');
        RecTarif.SETRANGE(Status, 'N');
        IF _Reference = '' THEN BEGIN
            IF _Fournisseur = '' THEN ERROR(ESK002Err);
            RecTarif.SETRANGE(c_fournisseur, _Fournisseur);
        END
        ELSE
            IF _Fournisseur <> '' THEN
                RecTarif.SETRANGE(ref_fourni, _Reference)
            ELSE
                RecTarif.SETRANGE(ref_active, _Reference);

        //ADCLEAR(Window);
        //ADWindow.OPEN(ESK003);

        //IF CONFIRM('test ad') THEN
        //  RecTarif.SETRANGE(c_fournisseur, 'EURFRA-FAC');

        IF NOT RecTarif.FINDFIRST() THEN EXIT;

        // MCO Le 07-09-2017 => Clôture de tout les tarifs du fournisseur
        // Cloture des anciens tarifs
        IF _CLoture THEN BEGIN
            LAncPrix.RESET();
            LAncPrix.SETRANGE("Vendor No.", _Fournisseur);
            LAncPrix.SETRANGE("Starting Date", 0D, CALCDATE('<-1J>', WORKDATE()));
            LAncPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
            IF LAncPrix.FINDFIRST() THEN
                REPEAT
                    LAncPrix.VALIDATE("Ending Date", CALCDATE('<-1J>', WORKDATE()));
                    LAncPrix.MODIFY();
                UNTIL LAncPrix.NEXT() = 0;
        END;
        // FIN MCO
        NbEnr := RecTarif.COUNT;
        REPEAT
            NoENr += 1;
            //ADWindow.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
            //ADWindow.UPDATE(2, ROUND(NoENr/(NbEnr/10000),1));

            //CFR le 10/05/2022 => R‚gie : [Divers 4]>>[MPL - MPC/CNH]=[Code concurrence] + [Comodity Code]>>[PCC/CNH]=[Comodity Code]
            IF (RecTarif.c_fournisseur = 'EURFRA-FAC') AND
                Item.GET(GestionMultiRef.RechercheArticleByActive(RecTarif.ref_active)) THEN BEGIN
                Item.VALIDATE("Comodity Code", RecTarif.comodity_code);
                Item.VALIDATE("Code Concurrence", RecTarif.divers4);
                Item.MODIFY(TRUE);
            END;
            //FIN CFR le 10/05/2022

            // Enregistrement de la devise du tarif
            Fournisseur.GET(RecTarif.c_fournisseur);
            DeviseOrigineTarif := Fournisseur."Currency Code";

            // Application aux fournisseur du groupe
            FournisseurDuGroupe.RESET();
            FournisseurDuGroupe.SETRANGE("Groupe Tarif Fournisseur", RecTarif.c_fournisseur);
            FournisseurDuGroupe.FINDFIRST();
            REPEAT
                // AD Le 17-04-2012
                // v1 IF ((FournisseurDuGroupe."No." = _Fournisseur) OR (_Fournisseur = '')) OR (_Reference = '') THEN
                // v2 IF ((FournisseurDuGroupe."Groupe Tarif Fournisseur" = _Fournisseur) OR (_Fournisseur = '')) OR (_Reference = '') THEN
                // FIN AD Le 17-04-2012

                // LM le 03-08-2012 verion 3
                IF (FournisseurDuGroupe."No." = _Fournisseur)
                    OR (FournisseurDuGroupe."Groupe Tarif Fournisseur" = _Fournisseur)
                    OR (_Fournisseur = '')
                    OR (_Reference = '') THEN BEGIN
                    TarifAppliqué := FALSE;

                    // Recherche par la référence active de l'article
                    IF ((_Reference = '') OR (_Fournisseur = ''))
                    AND FournisseurDuGroupe."implement tarif par ref_active" = TRUE THEN  // => AD LE 12-10-2011 => OR à la place de AND
                      BEGIN
                        IF Item.GET(GestionMultiRef.RechercheArticleByActive(RecTarif.ref_active)) THEN BEGIN
                            ExisteItemVendor := ItemVendor.GET(FournisseurDuGroupe."No.", Item."No.", '');
                            MajTarifFournisseur(FournisseurDuGroupe."No.", Item."No.", RecTarif, ItemVendor, ExisteItemVendor,
                                WORKDATE(), DeviseOrigineTarif, ModifierCdePourRefFou);
                            TarifAppliqué := TRUE;
                        END;
                    END;

                    // Recherche par la référence fournisseur
                    IF (_Reference = '') OR (_Fournisseur <> '') THEN // => AD LE 12-10-2011 => OR à la place de AND
                      BEGIN
                        //--IF ItemVendor.GET(_Fournisseur, RecTarif.ref_fourni, '')THEN
                        ItemVendor.RESET();
                        ItemVendor.SETRANGE("Vendor No.", FournisseurDuGroupe."No.");
                        ItemVendor.SETRANGE("Variant Code", '');
                        ItemVendor.SETRANGE("Vendor Item No.", RecTarif.ref_fourni);
                        IF ItemVendor.FINDFIRST() THEN
                            REPEAT
                            // LM le 03-08-2012 =>ajout boucle et pas seulement 1er enregistrement
                            BEGIN
                                Item.GET(ItemVendor."Item No.");
                                MajTarifFournisseur(FournisseurDuGroupe."No.", Item."No.", RecTarif, ItemVendor, TRUE,
                                    WORKDATE(), DeviseOrigineTarif, ModifierCdePourRefFou);
                                TarifAppliqué := TRUE;
                            END;
                            UNTIL ItemVendor.NEXT() = 0;
                    END;
                    /*
                            IF (NOT TarifAppliqué) AND (_Reference = '') THEN
                              IF Item.GET(GestionMultiRef.RechercheArticleByActive(RecTarif.ref_active)) THEN
                                BEGIN
                                  CLEAR(ItemVendor);
                                  IF ItemVendor.GET(FournisseurDuGroupe."No.", Item."No.", '') THEN ;
                                  LogImpémentationTarif(RecTarif.c_fournisseur, 'Multiref RefActive', RecTarif, ItemVendor, Item);
                                END
                              ELSE
                                IF Item.GET(GestionMultiRef.RechercheMultiReference(RecTarif.ref_fourni)) THEN
                                  BEGIN
                                    CLEAR(ItemVendor);
                                    IF ItemVendor.GET(FournisseurDuGroupe."No.", Item."No.", '') THEN;
                                    LogImpémentationTarif(RecTarif.c_fournisseur, 'Multiref Ref Fourni', RecTarif, ItemVendor, Item);
                                  END;
                      */
                END;
            UNTIL FournisseurDuGroupe.NEXT() = 0;
        UNTIL RecTarif.NEXT() = 0;
        //ADWindow.CLOSE;

    end;

    procedure MajTarifFournisseur(_CFournisseur: Code[20]; _CArticle: Code[20]; _RecTarif: Record Tab_tar_tmp; var _ItemVendor: Record "Item Vendor"; _ExisteItemVendor: Boolean; _DateMAJ: Date; _DeviseDuTarif: Code[10]; _ModifierCdePourRefFou: Boolean)
    var

       
        Item: Record Item;
        AncPrix: Record "Purchase Price";
        recL_ItemUnitOfMeasure: Record "Item Unit of Measure";
        NvPrix: Record "Purchase Price";
        recL_UnitOfMeasure: Record "Unit of Measure";
        CurrExchRate: Record "Currency Exchange Rate";
        lPurchaseHeader: Record "Purchase Header";
        i: Integer;
        modif: Boolean;
        DateApplicationTarif: Date;
        decL_QtyPer: Decimal;

    begin

        // Si création de la ligne, initialisation des variables
        IF NOT _ExisteItemVendor THEN BEGIN
            Item.GET(_CArticle);
            // MCO Le 08-02-2017 => Régie
            _ItemVendor.INIT();
            // FIN MCO Le 08-02-2017
            //LM le 18-09-2012 =>init status appro et qualite
            _ItemVendor.VALIDATE("Statut Qualité", _ItemVendor."Statut Qualité"::Conforme);
            _ItemVendor.VALIDATE("Statut Approvisionnement", _ItemVendor."Statut Approvisionnement"::"Non Bloqué");
            //fin LM le 18-09-2012
            _ItemVendor.VALIDATE("Vendor No.", _CFournisseur);
            _ItemVendor.VALIDATE("Item No.", _CArticle);
            _ItemVendor.VALIDATE("Variant Code", '');


            IF _ItemVendor."Purch. Unit of Measure" = '' THEN
                _ItemVendor.VALIDATE("Purch. Unit of Measure", Item."Purch. Unit of Measure");
        END;

        modif := _ItemVendor."Vendor Item No." <> _RecTarif.ref_fourni;

        _ItemVendor."Vendor Item No." := _RecTarif.ref_fourni;

        IF _ModifierCdePourRefFou AND modif THEN BEGIN
            // CFR le 16/04/2024 => R‚gie : ModifierCde(Question) >> ModifierAchats(Type, Question)
            _ItemVendor.ModifierAchats(lPurchaseHeader."Document Type"::Order, FALSE);
            _ItemVendor.ModifierAchats(lPurchaseHeader."Document Type"::"Return Order", FALSE);
            _ItemVendor.ModifierAchats(lPurchaseHeader."Document Type"::Quote, FALSE);
        END;



        // MAJ de item vendor
        IF (_RecTarif.design_fourni <> '') AND (COPYSTR(_RecTarif.design_fourni, 1, 1) <> ' ') THEN
            _ItemVendor.VALIDATE("Vendor Item Desciption", _RecTarif.design_fourni);

        // MCO Le 08-02-2017 => Régie
        IF (_RecTarif.unite_achat <> '') AND (EVALUATE(decL_QtyPer, _RecTarif.unite_achat)) THEN BEGIN
            // Recherche si une unité article existe par rapport à la quantité
            recL_ItemUnitOfMeasure.RESET();
            recL_ItemUnitOfMeasure.SETRANGE("Item No.", _CArticle);
            recL_ItemUnitOfMeasure.SETRANGE("Qty. per Unit of Measure", decL_QtyPer);
            IF NOT recL_ItemUnitOfMeasure.FINDFIRST() THEN BEGIN
                // Si on ne trouve pas alors on créer l'unité article
                // Avant on regarde si on a une unité correspondant
                IF NOT recL_UnitOfMeasure.GET(_RecTarif.unite_achat) THEN BEGIN
                    recL_UnitOfMeasure.INIT();
                    recL_UnitOfMeasure.Code := _RecTarif.unite_achat;
                    recL_UnitOfMeasure.Description := _RecTarif.unite_achat;
                    recL_UnitOfMeasure.INSERT(TRUE);
                END;
                // Création de l'unité article
                recL_ItemUnitOfMeasure.INIT();
                recL_ItemUnitOfMeasure.VALIDATE("Item No.", _CArticle);
                recL_ItemUnitOfMeasure.VALIDATE(Code, recL_UnitOfMeasure.Code);
                recL_ItemUnitOfMeasure.VALIDATE("Qty. per Unit of Measure", decL_QtyPer);
                recL_ItemUnitOfMeasure.INSERT(TRUE);
            END;
            _ItemVendor.VALIDATE("Purch. Unit of Measure", recL_ItemUnitOfMeasure.Code);
        END;
        // FIN MCO Le 08-02-2017 => Régie


        IF DELCHR(_RecTarif.code_remise, '=', ' ') <> '' THEN
            _ItemVendor.VALIDATE("Code Remise", _RecTarif.code_remise);

        _ItemVendor.VALIDATE("Date Mise a Jour", WORKDATE());

        IF _RecTarif.date_tarif = 0D THEN
            DateApplicationTarif := WORKDATE()
        ELSE
            DateApplicationTarif := _RecTarif.date_tarif;

        IF _RecTarif.unite_achat <> '' THEN
            _ItemVendor.VALIDATE("Purch. Unit of Measure", _RecTarif.unite_achat);

        // AD Le 13-12-2011 =>
        IF EVALUATE(i, _RecTarif.condit_achat) THEN BEGIN
            _ItemVendor.VALIDATE("Order Multiple", i);
            _ItemVendor.VALIDATE("Purch. Multiple", i);
        END;
        // FIN AD Le 13-12-2011

        IF EVALUATE(_ItemVendor."Minimum Order Quantity", _RecTarif.divers5) THEN;


        // Gestion du mode
        GestionContôleQualité(_RecTarif.px_brut, _ItemVendor, _ExisteItemVendor);

        // Insertion des modification
        IF NOT _ItemVendor.INSERT(TRUE) THEN
            _ItemVendor.MODIFY(TRUE);

        // Enregistrement des tarifs
        // Cloture des anciens tarifs
        AncPrix.RESET();
        AncPrix.SETRANGE("Item No.", _ItemVendor."Item No.");
        AncPrix.SETRANGE("Vendor No.", _ItemVendor."Vendor No.");
        AncPrix.SETRANGE("Starting Date", 0D, CALCDATE('<-1J>', DateApplicationTarif));
        AncPrix.SETRANGE("Variant Code", _ItemVendor."Variant Code");
        AncPrix.SETRANGE("Unit of Measure Code", _ItemVendor."Purch. Unit of Measure");
        // AD Le 27-06-2012 => Bug
        //AncPrix.SETFILTER("Ending Date",'%1|>=%2', 0D, NvPrix."Starting Date"); CODE D'ORIGINE
        AncPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, DateApplicationTarif);
        // FIN AD Le 27-06-2012
        // AD Le 06-02-2015 => Pour faire le modify
        //AncPrix.MODIFYALL("Ending Date", CALCDATE('-1J', DateApplicationTarif);
        IF AncPrix.FINDFIRST() THEN
            REPEAT
                AncPrix.VALIDATE("Ending Date", CALCDATE('<-1J>', DateApplicationTarif));
                AncPrix.MODIFY();
            UNTIL AncPrix.NEXT() = 0;

        // Insertion des nouveauw prix
        CLEAR(NvPrix);
        NvPrix.VALIDATE("Item No.", _ItemVendor."Item No.");
        NvPrix.VALIDATE("Vendor No.", _ItemVendor."Vendor No.");
        NvPrix.VALIDATE("Starting Date", DateApplicationTarif);
        NvPrix.VALIDATE("Variant Code", _ItemVendor."Variant Code");
        NvPrix.VALIDATE("Unit of Measure Code", _ItemVendor."Purch. Unit of Measure");

        IF _RecTarif.px_brut <> 0 THEN BEGIN
            NvPrix.VALIDATE("Minimum Quantity", _RecTarif.qte_col);
            NvPrix.VALIDATE("Direct Unit Cost",
              CurrExchRate.ExchangeAmtFCYToFCY(WORKDATE(), _DeviseDuTarif, NvPrix."Currency Code", _RecTarif.px_brut));
            IF NOT NvPrix.INSERT(TRUE) THEN
                NvPrix.MODIFY(TRUE);
        END;

        IF _RecTarif.prix_brut1 <> 0 THEN BEGIN
            NvPrix.VALIDATE("Minimum Quantity", _RecTarif.qte_col1);
            NvPrix.VALIDATE("Direct Unit Cost",
              CurrExchRate.ExchangeAmtFCYToFCY(WORKDATE(), _DeviseDuTarif, NvPrix."Currency Code", _RecTarif.prix_brut1));
            IF NOT NvPrix.INSERT(TRUE) THEN
                NvPrix.MODIFY(TRUE);
        END;

        IF _RecTarif.prix_brut2 <> 0 THEN BEGIN
            NvPrix.VALIDATE("Minimum Quantity", _RecTarif.qte_col2);
            NvPrix.VALIDATE("Direct Unit Cost",
              CurrExchRate.ExchangeAmtFCYToFCY(WORKDATE(), _DeviseDuTarif, NvPrix."Currency Code", _RecTarif.prix_brut2));
            IF NOT NvPrix.INSERT(TRUE) THEN
                NvPrix.MODIFY(TRUE);
        END;

        IF _RecTarif.prix_brut3 <> 0 THEN BEGIN
            NvPrix.VALIDATE("Minimum Quantity", _RecTarif.qte_col3);
            NvPrix.VALIDATE("Direct Unit Cost",
              CurrExchRate.ExchangeAmtFCYToFCY(WORKDATE(), _DeviseDuTarif, NvPrix."Currency Code", _RecTarif.prix_brut3));
            IF NOT NvPrix.INSERT(TRUE) THEN
                NvPrix.MODIFY(TRUE);

        END;
        // CFR le 12/05/2022 => R‚gie : Besoin d'ins‚rer les lignes de tarif … 0
        IF (_RecTarif.px_brut = 0) AND (_RecTarif.prix_brut1 = 0) AND (_RecTarif.prix_brut2 = 0) AND (_RecTarif.prix_brut3 = 0) THEN BEGIN
            NvPrix.VALIDATE("Minimum Quantity", 0);
            NvPrix.VALIDATE("Direct Unit Cost", 0);
            IF NOT NvPrix.INSERT(TRUE) THEN
                NvPrix.MODIFY(TRUE);
        END;
        // FIN CFR le 12/05/2022
    end;

    procedure "GestionContôleQualité"(_PxBut: Decimal; var _ItemVendor: Record "Item Vendor"; _ExisteItemVendor: Boolean)
    begin
    end;

    procedure "LogImpémentationTarif"(_Frn: Code[20]; _Commentaire: Text[100]; _recTarif: Record Tab_tar_tmp; _ItemVendor: Record "Item Vendor"; _Item: Record Item)
    var
        Log: Record "LOG Maj Tarif";
        IdLog: Integer;
    begin
        Log.RESET();
        IF NOT Log.FINDLAST() THEN Log.Id := 0;
        ;

        IdLog := Log.Id + 1;

        CLEAR(Log);
        Log.Id := IdLog;
        Log.Date := WORKDATE();
        Log.Heure := TIME;
        Log.User := USERID;
        Log.Libellé := _Frn;
        Log.Commentaire := _Commentaire;
        Log.Marque := _Item."Manufacturer Code";
        Log."Ref Active TARIF" := _recTarif.ref_active;
        Log."Ref Fournisseur TARIF" := _recTarif.ref_fourni;
        Log."Date MAJ TARIF" := _recTarif.date_tarif;
        Log."Prix TARIF" := _recTarif.px_brut;
        Log."Ref Active ARTICLE" := _Item."No. 2";
        Log."Ref Fournisseur  ARTICLE" := _ItemVendor."Vendor Item No.";
        Log."Date MAJ ARTICLE" := _ItemVendor."Date Mise a Jour";
        Log."Prix ARTICLE" := 0;
        Log.Insert();
    end;

    procedure HausseEnPourcentage(_pFournisseur: Code[20]; _pHausse: Decimal; _pDateNvTarif: Date)
    var
        LVendor: Record Vendor;
        LAncPrix: Record "Purchase Price";
        TempLNvPrix: Record "Purchase Price" temporary;
        LNvPrix2: Record "Purchase Price";

    begin
        // VERIFICATION DES PARAMETRES

        IF _pFournisseur = '' THEN
            ERROR(ESK002Err);

        // CFR le 12/12/2023 => R‚gie : hausse n‚gative possible
        IF (_pHausse < -50) OR (_pHausse > 200) THEN
            ERROR(ESK004Err);

        IF _pDateNvTarif = 0D THEN
            ERROR(ESK005Err);

        LVendor.GET(_pFournisseur);


        CLEAR(Window);
        Window.OPEN(ESK003Lbl);

        // FILTRE LES ANCIENS TARIFS COURANTS
        LAncPrix.RESET();
        LAncPrix.SETRANGE("Vendor No.", LVendor."No.");
        LAncPrix.SETRANGE("Starting Date", 0D, _pDateNvTarif);
        LAncPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, _pDateNvTarif);

        NbEnr := LAncPrix.COUNT * 2;
        TempLNvPrix.DELETEALL();

        IF LAncPrix.FINDSET() THEN
            REPEAT
                NoENr += 1;
                Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
                Window.UPDATE(2, ROUND(NoENr * 10000 / NbEnr, 1));


                LAncPrix.VALIDATE("Ending Date", CALCDATE('<-1D>', _pDateNvTarif));
                LAncPrix.MODIFY(TRUE);


                // ENREGISTREMENT DANS UNE TABLE TEMPORAIRE SINON IL BOUCLE SUR LE NOUVEAUX ENREGISTRMENT/
                TempLNvPrix := LAncPrix;
                TempLNvPrix.VALIDATE("Ending Date", 0D);
                TempLNvPrix.VALIDATE("Starting Date", _pDateNvTarif);
                TempLNvPrix.VALIDATE("Direct Unit Cost", ROUND(LAncPrix."Direct Unit Cost" * (1 + (_pHausse / 100)), 0.01));
                IF TempLNvPrix.INSERT(TRUE) THEN
                    TempLNvPrix.MODIFY(TRUE);

            UNTIL LAncPrix.NEXT() = 0;

        IF TempLNvPrix.FINDset() THEN
            REPEAT
                NoENr += 1;
                Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
                Window.UPDATE(2, ROUND(NoENr * 10000 / NbEnr, 1));

                LNvPrix2 := TempLNvPrix;
                IF LNvPrix2.INSERT(TRUE) THEN
                    LNvPrix2.MODIFY(TRUE);

            UNTIL TempLNvPrix.NEXT() = 0;

        Window.CLOSE();
    end;

    procedure FormatDecimal(Value: Decimal): Text[50]
    begin
        // CFR le 23/03/2022 => R‚gie : s‚parateur de d‚cimal chang‚ de , … .
        //EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals><Comma,,>'));
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals><Comma,.>'));
    end;
}

