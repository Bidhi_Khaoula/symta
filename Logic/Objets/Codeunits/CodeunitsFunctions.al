codeunit 50004 "Codeunits Functions"
{

    procedure GetItem(var Item: Record item; var SalesLine: Record "Sales Line") Result: Boolean

    begin
        with Item do begin
            if (SalesLine.Type <> SalesLine.Type::Item) or (SalesLine."No." = '') then
                exit(false);

            if SalesLine."No." <> "No." then
                Get(SalesLine."No.");

            exit(true);
        end;
    end;
    //>>Mig CU 46
    procedure GetSelectionFilterForBin(var Bin: Record Bin): Text;
    var
        lSelectionFilterManagement: Codeunit SelectionFilterManagement;
        RecRef: RecordRef;
    begin
        RecRef.GETTABLE(Bin);
        exit(lSelectionFilterManagement.GetSelectionFilter(RecRef, Bin.FIELDNO(Code)));
    end;

    PROCEDURE GetSelectionFilterForManufacturer(VAR Manufacturer: Record Manufacturer): Text;
    VAR
        RecRef: RecordRef;
    BEGIN
        RecRef.GETTABLE(Manufacturer);
        EXIT(CduGSelectionFilterManagement.GetSelectionFilter(RecRef, Manufacturer.FIELDNO(Code)));
    END;

    PROCEDURE GetSelectionFilterForItemCategory(VAR ItemCat: Record "Item Category"): Text;
    VAR
        RecRef: RecordRef;
    BEGIN
        RecRef.GETTABLE(ItemCat);
        EXIT(CduGSelectionFilterManagement.GetSelectionFilter(RecRef, ItemCat.FIELDNO(Code)));
    END;

    /*//TODOPROCEDURE GetSelectionFilterForProductGroup(VAR ProductGroup: Record "Product Group"): Text;
    VAR
        RecRef: RecordRef;
    BEGIN
        RecRef.GETTABLE(ProductGroup);
        EXIT(CduGSelectionFilterManagement.GetSelectionFilter(RecRef, ProductGroup.FIELDNO(Code)));
    END;*/

    var
        CduGSelectionFilterManagement: Codeunit SelectionFilterManagement;
    //<<Mig CU 46
    //>> Mig CU 7302
    PROCEDURE ESK_GetDefaultBin(ItemNo: Code[20]; VariantCode: Code[10]; LocationCode: Code[10]; VAR BinCode: Code[20]; VAR ZoneCode: Code[20]): Boolean;
    VAR
        BinContent: Record "Bin Content";
        Location: Record Location;
    BEGIN
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        Location.GET(LocationCode);
        IF NOT Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 24-04-2011
                IF NOT (Location."Bin Mandatory" AND (NOT Location."Require Receive") AND (NOT Location."Require Shipment")) THEN
                    EXIT(FALSE);

        BinContent.SETCURRENTKEY(Default);
        BinContent.SETRANGE(Default, TRUE);
        BinContent.SETRANGE("Location Code", LocationCode);
        BinContent.SETRANGE("Item No.", ItemNo);
        BinContent.SETRANGE("Variant Code", VariantCode);
        IF BinContent.FIND('-') THEN BEGIN
            BinCode := BinContent."Bin Code";
            // MC Le 27-04-2011 => SYMTA -> Modification par rapport … la version standard.
            ZoneCode := BinContent."Zone Code";
            // FIN MC Le 27-04-2011
            EXIT(TRUE);
        END;
    END;
    //<< Mig CU 7302
    //>>Mig CU 7171
    PROCEDURE OuvrirMulticonsultation(SalesLine: Record "Sales Line");
    VAR
        FrmQuid: Page "Multi -consultation";
    BEGIN
        //FrmQuid.InitClient(SalesLine."Sell-to Customer No.");
        FrmQuid.InitArticle(SalesLine."No.");
        FrmQuid.RUNMODAL();
    END;

    PROCEDURE LookupCustomer(CustNo: Code[20]);
    BEGIN
        // AD Le 21-10-2009 => Pour ouvrir la fiche client.
        GetCust(CustNo);
        IF Cust."No." = '' THEN
            EXIT;
        PAGE.RUNMODAL(PAGE::"Customer Card", Cust);
    END;

    PROCEDURE CalcDispKit(CodeArticle: Code[20]; Fabricable: Boolean; LocationCode: Code[10]) QtyTot: Decimal;
    VAR
    /*  NStkItem: Record Item;
     NotPreparate: Boolean;
     ProdBOMHeader: Record "Production BOM Header";
     ProdBOMLine: Record "Production BOM Line";
     "Current Quantity": Decimal;
     Item: Record Item;
     "Able to Make Qty.": Decimal;
     AbleToMakeQty: Decimal;
     CheckItemAvail: Page "Check Availability";
     RecItem: Record Item; */
    BEGIN
        ERROR('AD MIGRATION 2015 => Fonction a revoir si elle est utilisée');
        /*  {
           //CPH 21/12/09 : calculer le dispo de la mˆme maniŠre que la multi-consultation

           RecItem.SETRANGE("Location Filter", LocationCode);

             IF NOT RecItem.GET(CodeArticle) THEN EXIT(0);

             IF RecItem."Kit BOM No." = '' THEN EXIT(0);

             IF (Fabricable) AND (NOT RecItem."Automatic Build Kit BOM") THEN EXIT(0);

             RecItem.CALCFIELDS("Substitutes Exist");

             WITH KitComp DO BEGIN
                 INIT();
                 "Item No." := RecItem."No.";
                 Description := RecItem.Description;
                 CheckItemAvail.GetParam(Inventory, "Gross Requirement", "Scheduled Receipt", "Current Quantity",
                                         "Total Quantity", "Earliest Available Date");
                 "Total Quantity" := "Total Quantity" + "Current Quantity";
             END;

             QtyTot := 10000000;
             NotPreparate := FALSE;
             ProdBOMHeader.GET(RecItem."Kit BOM No.");
             ProdBOMHeader.TESTFIELD(Status, ProdBOMHeader.Status::Certified);
             ProdBOMHeader.TESTFIELD(Type, ProdBOMHeader.Type::Kitting);
             TempKitSalesLines.RESET();
             ProdBOMLine.SETRANGE("Production BOM No.", ProdBOMHeader."No.");
             IF ProdBOMLine.FINDSET() THEN
                 REPEAT
                     WITH KitComp2 DO BEGIN
                         INIT();
                         "Line No." := ProdBOMLine."Line No.";
                         "Item No." := ProdBOMLine."No.";
                         Description := ProdBOMLine.Description;
                         "Quantity per" := ProdBOMLine."Quantity per";
                         Item.GET("Item No.");
                         Item.SETRANGE("Location Filter", LocationCode);
                         Item.CALCFIELDS(Inventory,
                                        "Qty. on Purch. Order",
                                        "Qty. on Sales Order",
                                        "Qty. on Kit Sales Lines",
                                        "Scheduled Need (Qty.)",
                                        "Scheduled Receipt (Qty.)",
                                        "Qty. in Transit",
                                        "Trans. Ord. Receipt (Qty.)",
                                        "Trans. Ord. Shipment (Qty.)",
                                        "Qty. on Service Order");
                         Inventory := Item.Inventory;
                         "Gross Requirement" := Item."Qty. on Sales Order" + Item."Qty. on Kit Sales Lines";


                         IF (Item.Inventory - "Gross Requirement" > 0) THEN
                             AbleToMakeQty := ROUND((Item.Inventory - "Gross Requirement") / "Quantity per", 1, '<')
                         ELSE
                             AbleToMakeQty := 0;
                     END;

                     IF QtyTot > AbleToMakeQty THEN
                         QtyTot := AbleToMakeQty;
                 UNTIL ProdBOMLine.NEXT() = 0;
             EXIT(QtyTot);
           }*/
    END;

    PROCEDURE GetSalesMultiple(rec_SalesLine: Record "Sales Line") dec_Return: Decimal;
    VAR
        rec_Item: Record Item;
    BEGIN
        IF GetItem(Item, rec_SalesLine) THEN BEGIN
            rec_Item.GET(rec_SalesLine."No.");
            EXIT(rec_Item."Sales multiple");
        END;
    END;

    PROCEDURE GetQtyItem(_pType: Code[20]; _pSalesLine: Record "Sales Line"): Decimal;
    VAR
        LItem: Record Item;
    BEGIN
        // AD Le 05-02-2015 => Permet de retourner des qtes de la fiche article
        IF (_pSalesLine.Type <> _pSalesLine.Type::Item) OR (_pSalesLine."No." = '') THEN
            EXIT(0);

        LItem.GET(_pSalesLine."No.");
        LItem.SETRANGE("Location Filter", _pSalesLine."Location Code");
        CASE _pType OF
            'CDEVTE':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Sales Order");
                    EXIT(LItem."Qty. on Sales Order");
                END;
            'CDEACH':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Purch. Order");
                    EXIT(LItem."Qty. on Purch. Order");
                END;
            'RTEACH':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Purch. Return");
                    EXIT(LItem."Qty. on Purch. Return");
                END;
            'RTEVTE':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Sales Return");
                    EXIT(LItem."Qty. on Sales Return");
                END;
        END;
    END;

    PROCEDURE LookupQteOnSalesOrder(_pSalesLine: Record "Sales Line");
    VAR
    BEGIN
        LookupQteOnSalesDoc(_pSalesLine, 1);
    END;

    PROCEDURE LookupQteOnPurchOrder(_pSalesLine: Record "Sales Line"; _pDocType: Integer);
    VAR
        LPurchLine: Record "Purchase Line";
        LFrmPurchLine: Page "Purchase Lines";
    BEGIN
        IF (_pSalesLine.Type <> _pSalesLine.Type::Item) OR (_pSalesLine."No." = '') THEN
            EXIT;

        CLEAR(LPurchLine);

        LPurchLine.SETRANGE("Document Type", _pDocType);  // AD Le 23-03-2016 => Je passe le type de document en param.
        LPurchLine.SETRANGE(Type, _pSalesLine.Type);
        LPurchLine.SETRANGE("No.", _pSalesLine."No.");
        LPurchLine.SETCURRENTKEY("Document Type", Type, "No.");
        LPurchLine.SETRANGE("Location Code", _pSalesLine."Location Code");
        // MCO Le 31-03-2015 => Régie
        LPurchLine.SETFILTER("Outstanding Qty. (Base)", '<>%1', 0);
        // MCO Le 31-03-2015 => Régie


        LFrmPurchLine.EDITABLE(FALSE);
        LFrmPurchLine.SETTABLEVIEW(LPurchLine);
        LFrmPurchLine.RUNMODAL();
    END;

    PROCEDURE LookupQteOnSalesDoc(_pSalesLine: Record "Sales Line"; _pDocType: Integer);
    VAR
        LsalesLine: Record "Sales Line";
        LFrmsalesLine: Page "Sales Lines";
    BEGIN

        IF (_pSalesLine.Type <> _pSalesLine.Type::Item) OR (_pSalesLine."No." = '') THEN
            EXIT;

        CLEAR(LsalesLine);

        LsalesLine.SETRANGE("Document Type", _pDocType);
        LsalesLine.SETRANGE(Type, _pSalesLine.Type);
        LsalesLine.SETRANGE("No.", _pSalesLine."No.");
        LsalesLine.SETCURRENTKEY("Document Type", Type, "No.");
        LsalesLine.SETRANGE("Location Code", _pSalesLine."Location Code");
        // MCO Le 31-03-2015 => Régie
        LsalesLine.SETFILTER("Outstanding Qty. (Base)", '<>%1', 0);
        // MCO Le 31-03-2015 => Régie

        LFrmsalesLine.EDITABLE(FALSE);
        LFrmsalesLine.SETTABLEVIEW(LsalesLine);
        LFrmsalesLine.RUNMODAL();
    END;

    LOCAL PROCEDURE GetCust(CustNo: Code[20]);
    BEGIN
        IF CustNo <> '' THEN BEGIN
            IF CustNo <> Cust."No." THEN
                IF NOT Cust.GET(CustNo) THEN
                    CLEAR(Cust);
        END ELSE
            CLEAR(Cust);
    END;

    PROCEDURE DocExist(CurrentSalesHeader: Record "Sales Header"; CustNo: Code[20]): Boolean;
    VAR
        SalesInvHeader: Record "Sales Invoice Header";
        SalesShptHeader: Record "Sales Shipment Header";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        ReturnReceipt: Record "Return Receipt Header";
        SalesHeader: Record "Sales Header";
    BEGIN
        IF CustNo = '' THEN
            EXIT(FALSE);
        WITH SalesInvHeader DO BEGIN
            SETCURRENTKEY("Sell-to Customer No.");
            SETRANGE("Sell-to Customer No.", CustNo);
            IF NOT ISEMPTY THEN
                EXIT(TRUE);
        END;
        WITH SalesShptHeader DO BEGIN
            SETCURRENTKEY("Sell-to Customer No.");
            SETRANGE("Sell-to Customer No.", CustNo);
            IF NOT ISEMPTY THEN
                EXIT(TRUE);
        END;
        WITH SalesCrMemoHeader DO BEGIN
            SETCURRENTKEY("Sell-to Customer No.");
            SETRANGE("Sell-to Customer No.", CustNo);
            IF NOT ISEMPTY THEN
                EXIT(TRUE);
        END;
        WITH SalesHeader DO BEGIN
            SETCURRENTKEY("Sell-to Customer No.");
            SETRANGE("Sell-to Customer No.", CustNo);
            IF FINDFIRST() THEN BEGIN
                IF ("Document Type" <> CurrentSalesHeader."Document Type") OR
                   ("No." <> CurrentSalesHeader."No.")
                THEN
                    EXIT(TRUE);
                IF FIND('>') THEN
                    EXIT(TRUE);
            END;
        END;
        WITH ReturnReceipt DO BEGIN
            SETCURRENTKEY("Sell-to Customer No.");
            SETRANGE("Sell-to Customer No.", CustNo);
            IF NOT ISEMPTY THEN
                EXIT(TRUE);
        END;
    END;

    PROCEDURE GetNbAdditional(pType: Integer; pItemNo: Code[20]): Integer;
    VAR
        lAdditionalItem: Record "Additional Item";
    BEGIN
        // CFR le 24/09/2021 - SFD20210201 articles complémentaires
        CLEAR(lAdditionalItem);
        // pType permet de partir de l'article et trouver les complémentaire [0] ou de partir du complémentaire pour trouver l'article [1]
        CASE pType OF
            0:
                lAdditionalItem.SETRANGE("Item No.", pItemNo);
            1:
                lAdditionalItem.SETRANGE("Additional Item No.", pItemNo);
        END;
        EXIT(lAdditionalItem.COUNT());
    END;

    PROCEDURE LookupAdditional(pType: Integer; pSalesLine: Record "Sales Line"): Boolean;
    VAR
        lAdditionalItem: Record "Additional Item";
        lSalesLine: Record "Sales Line";
        lAdditionalItemList: Page "Additional Item List";
        lLineNo: Integer;
        lInsert: Boolean;
        lItemNo: Code[20];
    BEGIN
        lItemNo := pSalesLine."No.";
        // CFR le 24/09/2021 - SFD20210201 articles complémentaires
        lInsert := FALSE;
        CLEAR(lAdditionalItem);
        // pType permet de partir de l'article et trouver les complémentaire [0] ou de partir du complémentaire pour trouver l'article [1]
        CASE pType OF
            0:
                lAdditionalItem.SETRANGE("Item No.", lItemNo);
            1:
                lAdditionalItem.SETRANGE("Additional Item No.", lItemNo);
        END;

        CLEAR(lAdditionalItemList);
        lAdditionalItemList.SETTABLEVIEW(lAdditionalItem);
        lAdditionalItemList.LOOKUPMODE := TRUE;
        lAdditionalItemList.EDITABLE(FALSE);
        IF lAdditionalItemList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            // RécupŠre les lignes sélectionnées
            lAdditionalItemList.SETSELECTIONFILTER(lAdditionalItem);
            IF lAdditionalItem.FINDSET() THEN
                IF CONFIRM(STRSUBSTNO('Nombre d''articles sélectionnés : %1\Souhaitez vous les ajouter au document ?', lAdditionalItem.COUNT())) THEN BEGIN
                    // Dernier nø de ligne
                    lSalesLine.RESET();
                    lSalesLine.SETRANGE("Document Type", pSalesLine."Document Type");
                    lSalesLine.SETRANGE("Document No.", pSalesLine."Document No.");
                    IF lSalesLine.FINDLAST() THEN
                        lLineNo := lSalesLine."Line No."
                    ELSE
                        lLineNo := 0;
                    CLEAR(lSalesLine);
                    lSalesLine.VALIDATE("Document Type", pSalesLine."Document Type");
                    lSalesLine.VALIDATE("Document No.", pSalesLine."Document No.");
                    // Insertion des articles
                    REPEAT
                        lLineNo += 10000;
                        lSalesLine.VALIDATE("Line No.", lLineNo);
                        lSalesLine.VALIDATE(Type, lSalesLine.Type::Item);
                        IF (lAdditionalItem."Additional Search Reference" <> '') THEN
                            lSalesLine.VALIDATE("Recherche référence", lAdditionalItem."Additional Search Reference")
                        ELSE
                            lSalesLine.VALIDATE("No.", lAdditionalItem."Additional Item No.");
                        lSalesLine.VALIDATE(Quantity, lAdditionalItem.Quantity * pSalesLine.Quantity);
                        IF lSalesLine.INSERT(TRUE) THEN
                            lInsert := TRUE;
                    UNTIL lAdditionalItem.NEXT() = 0;
                END;
        END;
        EXIT(lInsert);
    END;

    var
        Cust: Record Customer;
        Item: Record Item;

    //<<Mig CU 7171
    //>>Mig CU 6620
    PROCEDURE ApplicationAbattement(VAR _pToSalesHeader: Record "Sales Header");
    VAR
        LToSalesLine: Record "Sales Line";
    BEGIN
        // AD Le 10-10-2011 => Application de l'abattement
        LToSalesLine.RESET();
        LToSalesLine.SETRANGE("Document Type", _pToSalesHeader."Document Type");
        LToSalesLine.SETRANGE("Document No.", _pToSalesHeader."No.");
        LToSalesLine.SETRANGE(Type, LToSalesLine.Type::Item);
        LToSalesLine.SETRANGE("Ne pas recalculer abattement", FALSE);
        IF LToSalesLine.FINDSET() THEN
            REPEAT
                LToSalesLine.VALIDATE("Prix Unitaire Doc. Origine", LToSalesLine."Unit Price");
                LToSalesLine.VALIDATE("Unit Price", ROUND(LToSalesLine."Unit Price" * (1 - _pToSalesHeader."Taux Abattement" / 100), 0.01));
                LToSalesLine."Ne pas recalculer abattement" := TRUE;
                LToSalesLine.MODIFY();

            UNTIL LToSalesLine.NEXT() = 0;
    END;
    //<<Mig CU 6620
    //>>Mig CU 241
    PROCEDURE SetHideValidationDialog(NewHideValidationDialog: Boolean);
    var
        HideValidationDialog: Boolean;
    BEGIN
        // MCO Le 27-09-2016 => R‚gie
        HideValidationDialog := NewHideValidationDialog;
    END;
    //<<Mig CU 241
    //>>Mig CU 7600
    PROCEDURE GetCalendarCode(SourceType: option Company,Customer,Vendor,Location,"Shipping Agent",Service; SourceCode: Code[20]; AdditionalSourceCode: Code[20]): Code[10];
    var
        Customer: Record Customer;
        Vendor: Record Vendor;
        Location: Record Location;
        CompanyInfo: Record "Company Information";
        ServMgtSetup: Record "Service Mgt. Setup";
        Shippingagent: Record "Shipping Agent Services";
    BEGIN
        CASE SourceType OF
            SourceType::Company:
                IF CompanyInfo.GET() THEN
                    EXIT(CompanyInfo."Base Calendar Code");
            SourceType::Customer:
                IF Customer.GET(SourceCode) THEN
                    EXIT(Customer."Base Calendar Code");
            SourceType::Vendor:
                IF Vendor.GET(SourceCode) THEN
                    EXIT(Vendor."Base Calendar Code");
            SourceType::"Shipping Agent":
                BEGIN
                    IF Shippingagent.GET(SourceCode, AdditionalSourceCode) THEN
                        EXIT(Shippingagent."Base Calendar Code");

                    IF CompanyInfo.GET() THEN
                        EXIT(CompanyInfo."Base Calendar Code");
                END;
            SourceType::Location:
                BEGIN
                    IF Location.GET(SourceCode) THEN
                        IF Location."Base Calendar Code" <> '' THEN
                            EXIT(Location."Base Calendar Code");
                    IF CompanyInfo.GET() THEN
                        EXIT(CompanyInfo."Base Calendar Code");
                END;
            SourceType::Service:
                IF ServMgtSetup.GET() THEN
                    EXIT(ServMgtSetup."Base Calendar Code");
        END;
    END;
    //<<Mig CU 7600
    //>>Mig CU 240
    PROCEDURE LookupNameMiniLoad(VAR CurrentJnlBatchName: Code[10]; VAR ItemJnlLine: Record "Item Journal Line"): Boolean;
    VAR
        ItemJnlBatch: Record "Item Journal Batch";
        CduLItemJnlManagement: Codeunit ItemJnlManagement;
    BEGIN
        COMMIT();
        ItemJnlBatch."Journal Template Name" := ItemJnlLine.GETRANGEMAX("Journal Template Name");
        ItemJnlBatch.Name := ItemJnlLine.GETRANGEMAX("Journal Batch Name");
        ItemJnlBatch.FILTERGROUP(2);
        ItemJnlBatch.SETRANGE("Journal Template Name", ItemJnlBatch."Journal Template Name");
        ItemJnlBatch.FILTERGROUP(0);
        IF PAGE.RUNMODAL(50089, ItemJnlBatch) = ACTION::LookupOK THEN BEGIN
            CurrentJnlBatchName := ItemJnlBatch.Name;
            CduLItemJnlManagement.SetName(CurrentJnlBatchName, ItemJnlLine);
        END;
    END;

    PROCEDURE SetNewBatchMiniLoad() txt_rtn: Code[20];
    VAR
        ItemJnlBatch: Record "Item Journal Batch";
        rec_InventorySetup: Record "Inventory Setup";
        lName: Text;
    BEGIN
        //** Cette fonction permet de renvoyer le prochain num‚ro de feuille pour miniload **//
        rec_InventorySetup.GET();
        ItemJnlBatch.RESET();
        ItemJnlBatch.SETRANGE(MiniLoad, TRUE);
        // CFR le 17/09/2020 : WIIO >> le nom de la feuille par d‚faut cr‚‚e par les utilisateurs doit conserver la souche de param stock
        lName := COPYSTR(rec_InventorySetup."Numero debut feuille MiniLoad", 1, 4) + '*';
        ItemJnlBatch.SETFILTER(Name, lName);
        // Fin CFR le 17/09/2020
        IF ItemJnlBatch.FINDLAST() THEN
            txt_rtn := INCSTR(ItemJnlBatch.Name)
        ELSE BEGIN
            rec_InventorySetup.TESTFIELD(rec_InventorySetup."Numero debut feuille MiniLoad");
            txt_rtn := INCSTR(rec_InventorySetup."Numero debut feuille MiniLoad");
        END;
    END;

    PROCEDURE OpenJnlMiniLoad(VAR CurrentJnlBatchName: Code[10]; VAR ItemJnlLine: Record "Item Journal Line");
    VAR
        ItemJnlBatch: Record "Item Journal Batch";
    BEGIN
        ItemJnlBatch.RESET();
        ItemJnlBatch.SETRANGE(MiniLoad, TRUE);
        IF ItemJnlBatch.FINDLAST() THEN 
            CurrentJnlBatchName := ItemJnlBatch.Name
        ELSE BEGIN
            ItemJnlBatch.SETRANGE("Journal Template Name", 'RECLASS');
            IF NOT ItemJnlBatch.GET('RECLASS', CurrentJnlBatchName) THEN BEGIN
                IF NOT ItemJnlBatch.FIND('-') THEN BEGIN
                    ItemJnlBatch.INIT();
                    ItemJnlBatch."Journal Template Name" := 'RECLASS';
                    ItemJnlBatch.MiniLoad := TRUE;
                    ItemJnlBatch.Name := SetNewBatchMiniLoad();
                    ItemJnlBatch.SetupNewBatch();
                    ItemJnlBatch.INSERT(TRUE);
                    COMMIT();
                END;
            END;
        END;

        ItemJnlLine.FILTERGROUP := 2;
        ItemJnlLine.SETRANGE("Journal Batch Name", CurrentJnlBatchName);
        ItemJnlLine.FILTERGROUP := 0;
    END;

    PROCEDURE OnAfterInputItemNo(VAR Text: Text[1024]): Text[1024];
    VAR
        Item: Record Item;
        Number: Integer;
    BEGIN
        IF Text = '' THEN
            EXIT;

        IF EVALUATE(Number, Text) THEN
            EXIT;

        Item."No." := Text;
        IF Item.FIND('=>') THEN
            IF COPYSTR(Item."No.", 1, STRLEN(Text)) = UPPERCASE(Text) THEN BEGIN
                Text := Item."No.";
                EXIT;
            END;

        Item.SETCURRENTKEY("Search Description");
        Item."Search Description" := Text;
        Item."No." := '';
        IF Item.FIND('=>') THEN
            IF COPYSTR(Item."Search Description", 1, STRLEN(Text)) = UPPERCASE(Text) THEN
                Text := Item."No.";
    END;
    //<<Mig CU 240
    //>>Mig CU 7181
    PROCEDURE OuvrirMulticonsultation(PurchLine: Record "Purchase Line");
    VAR
        FrmQuid: Page "Multi -consultation";
    BEGIN
        //FrmQuid.InitClient(SalesLine."Sell-to Customer No.");
        FrmQuid.InitArticle(PurchLine."No.");
        FrmQuid.RUNMODAL();
    END;

    PROCEDURE GetQtyItem(_pType: Code[20]; _pPurchLine: Record "Purchase Line"): Decimal;
    VAR
        LItem: Record Item;
    BEGIN
        // AD Le 05-02-2015 => Permet de retourner des qtes de la fiche article
        IF (_pPurchLine.Type <> _pPurchLine.Type::Item) OR (_pPurchLine."No." = '') THEN
            EXIT(0);

        LItem.GET(_pPurchLine."No.");
        LItem.SETRANGE("Location Filter", _pPurchLine."Location Code");
        CASE _pType OF
            'CDEVTE':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Sales Order");
                    EXIT(LItem."Qty. on Sales Order");
                END;
            'CDEACH':
                BEGIN
                    LItem.CALCFIELDS("Qty. on Purch. Order");
                    EXIT(LItem."Qty. on Purch. Order");
                END;
            'PHYS':
                BEGIN
                    LItem.CALCFIELDS(Inventory);
                    EXIT(LItem.Inventory);
                END;

        END;
    END;

    PROCEDURE LookupQteOnSalesOrder(_pPurchLine: Record "Purchase Line");
    VAR
        LsalesLine: Record "Sales Line";
        LFrmsalesLine: Page "Sales Lines";
    BEGIN

        IF (_pPurchLine.Type <> _pPurchLine.Type::Item) OR (_pPurchLine."No." = '') THEN
            EXIT;

        CLEAR(LsalesLine);

        LsalesLine.SETRANGE("Document Type", _pPurchLine."Document Type");
        LsalesLine.SETRANGE(Type, _pPurchLine.Type);
        LsalesLine.SETRANGE("No.", _pPurchLine."No.");
        LsalesLine.SETCURRENTKEY("Document Type", Type, "No.");
        LsalesLine.SETRANGE("Location Code", _pPurchLine."Location Code");
        LsalesLine.SETFILTER("Outstanding Qty. (Base)", '<>%1', 0);

        LFrmsalesLine.EDITABLE(FALSE);
        LFrmsalesLine.SETTABLEVIEW(LsalesLine);
        LFrmsalesLine.RUNMODAL();
    END;

    PROCEDURE LookupQteOnPurchOrder(_pPurchLine: Record "Purchase Line");
    VAR
        LPurchLine: Record "Purchase Line";
        LFrmPurchLine: Page "Purchase Lines";
    BEGIN
        IF (_pPurchLine.Type <> _pPurchLine.Type::Item) OR (_pPurchLine."No." = '') THEN
            EXIT;

        CLEAR(LPurchLine);

        LPurchLine.SETRANGE("Document Type", _pPurchLine."Document Type");
        LPurchLine.SETRANGE(Type, _pPurchLine.Type);
        LPurchLine.SETRANGE("No.", _pPurchLine."No.");
        LPurchLine.SETCURRENTKEY("Document Type", Type, "No.");
        LPurchLine.SETRANGE("Location Code", _pPurchLine."Location Code");
        LPurchLine.SETFILTER("Outstanding Qty. (Base)", '<>%1', 0);

        LFrmPurchLine.EDITABLE(FALSE);
        LFrmPurchLine.SETTABLEVIEW(LPurchLine);
        LFrmPurchLine.RUNMODAL();
    END;

    PROCEDURE GetStkItem(_pPurchLine: Record "Purchase Line"): Boolean;
    VAR
        LItem: Record Item;
    BEGIN
        IF (_pPurchLine.Type <> _pPurchLine.Type::Item) OR (_pPurchLine."No." = '') THEN
            EXIT(FALSE);

        LItem.GET(_pPurchLine."No.");
        EXIT(LItem.Stocké);
    end;
    //<<Mig CU 7181
    //>>Mig CU 260
    LOCAL PROCEDURE GetToAddressFromContact(_pContactNo: Code[20]; _pRecordRef: RecordRef): Text[50];
    VAR
        Contact: Record Contact;
        ToAddress: Text;
        EskapeCom: Codeunit "Eskape Communication";
    BEGIN
        /*
        IF Contact.GET(_pContactNo) THEN
          ToAddress := Contact."E-Mail";

        EXIT(ToAddress);
        */
        ToAddress := EskapeCom.GetRecipient(0, _pRecordRef);
        EXIT(ToAddress);
    END;

    PROCEDURE EmailFileFromSalesShipmentHeader(SalesShipmentHeader: Record "Sales Shipment Header"; AttachmentFilePath: Text[250]);
    VAR
        LDocRef: RecordRef;
        ShipTxt: Label 'Credit Memos';
    BEGIN
        SalesShipmentHeader.SETRECFILTER(); // MCO Le 09-12-2015
        LDocRef.GETTABLE(SalesShipmentHeader);// AD Le 18-11-2015

        EmailFileByContact(AttachmentFilePath,
          SalesShipmentHeader."No.",
          SalesShipmentHeader."Sell-to Customer No.",
          SalesShipmentHeader."Sell-to Customer Name",
          ShipTxt
          // AD Le 18-11-2015 => Pouvoir prendre le contact
          , ''
          , LDocRef
          // FIN AD Le 18-11-2015

          );
    END;

    PROCEDURE EmailFileFromReturnReceiptHeader(ReturnReceiptHeader: Record "Return Receipt Header"; AttachmentFilePath: Text[250]);
    VAR
        LDocRef: RecordRef;
        ReturnrReceiptTxt: Label 'Retour';
    BEGIN
        ReturnReceiptHeader.SETRECFILTER(); // MCO Le 09-12-2015
        LDocRef.GETTABLE(ReturnReceiptHeader);// AD Le 18-11-2015

        EmailFileByContact(AttachmentFilePath,
          ReturnReceiptHeader."No.",
          ReturnReceiptHeader."Sell-to Customer No.",
          ReturnReceiptHeader."Sell-to Customer Name",
          ReturnrReceiptTxt
          // AD Le 18-11-2015 => Pouvoir prendre le contact
          , ''
          , LDocRef
          // FIN AD Le 18-11-2015

          );
    END;

    PROCEDURE EmailFileFromPurchHeader(PurchHeader: Record "Purchase Header"; AttachmentFilePath: Text[250]);
    VAR
        LDocRef: RecordRef;
    BEGIN
        PurchHeader.SETRECFILTER();
        LDocRef.GETTABLE(PurchHeader);// AD Le 18-11-2015

        EmailFilePurch(AttachmentFilePath,
          PurchHeader."No.",
          PurchHeader."Pay-to Vendor No.",
          PurchHeader."Pay-to Name",
          FORMAT(PurchHeader."Document Type")
          // AD Le 18-11-2015 => Pouvoir prendre le contact
          , PurchHeader."Pay-to Contact No."
          , LDocRef
          // FIN AD Le 18-11-2015
          );
    END;

    LOCAL PROCEDURE EmailFilePurch(AttachmentFilePath: Text[250]; PostedDocNo: Code[20]; SendEmaillToCustNo: Code[20]; SendEmaillToCustName: Text[50]; EmailDocName: Text[50]; _pContactNo: Code[20]; _pRecordRef: RecordRef);
    VAR
        TempEmailItem: Record "Email Item" TEMPORARY;
        DocumentMailing: Codeunit "Document-Mailing"; 
        AttachmentFileName: Text[250];
        ReportAsPdfFileNameMsgPurchLbl: Label 'Sales %1 %2.pdf;FRA=%1 achats %2.pdf' , Comment  = '%1 = Doc Name ; %2 = Doc No';
        EmailSubjectCapTxt: Label '%1 - %2 %3;', Comment = '%1 = Nom du Client ; %2 = Doc Name ; %3 = Doc No ';
    BEGIN
        AttachmentFileName := STRSUBSTNO(ReportAsPdfFileNameMsgPurchLbl, EmailDocName, PostedDocNo);

        WITH TempEmailItem DO BEGIN
            // AD Le 18-11-2015 => Pouvoir prendre le contact
            "Send to" := GetToAddressFromContact(_pContactNo, _pRecordRef);
            IF "Send to" = '' THEN
                // FIN AD Le 18-11-2015
                // MCO Le 18-01-2016 => Gestion au fournisseur
                "Send to" := DocumentMailing.GetToAddressFromVendor(SendEmaillToCustNo);
            Subject := COPYSTR(
                STRSUBSTNO(
                  EmailSubjectCapTxt, SendEmaillToCustName, EmailDocName, PostedDocNo), 1,
                MAXSTRLEN(Subject));
            "Attachment File Path" := AttachmentFilePath;
            "Attachment Name" := AttachmentFileName;
            Send(FALSE, Enum::"Email Scenario"::Default);
        END;
    END;

    LOCAL PROCEDURE EmailFileByContact(AttachmentFilePath: Text[250]; PostedDocNo: Code[20]; SendEmaillToCustNo: Code[20]; SendEmaillToCustName: Text[50]; EmailDocName: Text[50]; _pContactNo: Code[20]; _pRecordRef: RecordRef);
    VAR
        TempEmailItem: Record "Email Item" TEMPORARY;
        AttachmentFileName: Text[250];
        EmailSubjectCapTxt: Label '%1 - %2 %3;' , Comment = '%1 = Nom du Client ; %2 = Doc Name ; %3 = Doc No ';
        ReportAsPdfFileNameMsg: Label 'Sales %1 %2.pdf' , Comment = '%1 = Doc Name ; %2= Doc No';
        CduLDocumentMailing: codeunit "Document-Mailing";
    BEGIN
        AttachmentFileName := STRSUBSTNO(ReportAsPdfFileNameMsg, EmailDocName, PostedDocNo);

        WITH TempEmailItem DO BEGIN
            // AD Le 18-11-2015 => Pouvoir prendre le contact
            "Send to" := GetToAddressFromContact(_pContactNo, _pRecordRef);
            IF "Send to" = '' THEN
                // FIN AD Le 18-11-2015
                "Send to" := CduLDocumentMailing.GetToAddressFromCustomer(SendEmaillToCustNo);
            Subject := COPYSTR(
                STRSUBSTNO(
                  EmailSubjectCapTxt, SendEmaillToCustName, EmailDocName, PostedDocNo), 1,
                MAXSTRLEN(Subject));
            "Attachment File Path" := AttachmentFilePath;
            "Attachment Name" := AttachmentFileName;
            Send(FALSE, Enum::"Email Scenario"::Default);
        END;
    END;
    //<<Mig CU 260
    //>>Mig CU 80
    PROCEDURE ShipmentLabel(SalesShptHeader: Record "Sales Shipment Header");
    VAR
        SalesSetup: Record "Sales & Receivables Setup";
        ShipLabel: Record "Shipping Label";
        Quantité_collis: Integer;
        transporteur: Record "Shipping Agent";
        EtqLogiflux: Codeunit "Edition Etiquette LOGIFLUX";
        lShipLine: Record "Sales Shipment Line";
        LGestionBP: Codeunit "Gestion Bon Préparation";
        LCust: Record Customer;
        NoSeriesMgt: Codeunit NoSeriesManagement;
    BEGIN

        // AD Le 07-04-2007 => Gestion des ‚tiquettes transports

        SalesSetup.GET();

        // AD Le 14-09-2011 => SYMTA -> A cause des groupement, il peut y avoir plusieurs BL pour le m^me BP donc une seule etq
        ShipLabel.RESET();
        ShipLabel.SETRANGE("Preparation No.", SalesShptHeader."Preparation No.");
        IF ShipLabel.FINDFIRST() THEN EXIT;
        // FIN AD Le 14-09-2011

        // Generation de l'enregistrement dans la table.
        // ---------------------------------------------
        ShipLabel.INIT();

        ShipLabel."Shipping Agent Code" := SalesShptHeader."Shipping Agent Code";
        ShipLabel.VALIDATE("Shipping Agent Services", SalesShptHeader."Shipping Agent Service Code");
        ShipLabel."Entry No." := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Entry Nos.", WORKDATE(), TRUE);

        ShipLabel."Prepare Code" := SalesShptHeader.Preparateur;

        ShipLabel."BL No." := SalesShptHeader."No.";
        ShipLabel."Preparation No." := SalesShptHeader."Preparation No.";
        ShipLabel."Date of  bl" := SalesShptHeader."Posting Date";

        ShipLabel."Recipient Code" := SalesShptHeader."Sell-to Customer No.";

        // AD Le 05-12-2011
        IF LCust.GET(ShipLabel."Recipient Code") THEN
            ShipLabel."Network Code" := LCust."Code Expéditeur";
        // FIN AD Le 05-12-2011

        //CJ 16/12/09 - DEBUT
        //ShipLabel."Recipient Person"       := SalesShptHeader."Ship-to Contact";
        ShipLabel."Recipient Person" := COPYSTR(SalesShptHeader."Ship-to Contact", 1, 30);
        //CJ 16/12/09 -FIN

        ShipLabel."Recipient Name" := SalesShptHeader."Ship-to Name";
        ShipLabel."Recipient Address" := SalesShptHeader."Ship-to Address";
        ShipLabel."Recipient Address 2" := SalesShptHeader."Ship-to Address 2";
        ShipLabel."Recipient Post Code" := SalesShptHeader."Ship-to Post Code";
        ShipLabel."Recipient City" := SalesShptHeader."Ship-to City";
        ShipLabel."Recipient Country Code" := SalesShptHeader."Ship-to Country/Region Code";

        IF SalesShptHeader."Promised Delivery Date" <> 0D THEN BEGIN
            //ShipLabel."Imperative date of Delivery" := SalesShptHeader."Promised Delivery Date";
            ShipLabel."Imperative date of Delivery" := SalesShptHeader."Requested Delivery Date";
        END;

        // AD Le 05-12-2011 =>
        /*
        ShipLabel."Imperative date of Delivery" := CALCDATE('<+1D>', SalesShptHeader."Posting Date");

        IF (DATE2DWY(ShipLabel."Imperative date of Delivery", 1) = 6) AND
         (NOT (SalesShptHeader."Mode d'exp‚dition" IN [SalesShptHeader."Mode d'exp‚dition"::"Contre Rembourssement Samedi",
                  SalesShptHeader."Mode d'exp‚dition"::"Express Samedi"])) THEN
            ShipLabel."Imperative date of Delivery" := CALCDATE('<+2D>', ShipLabel."Imperative date of Delivery");

        */
        ShipLabel."Imperative date of Delivery" :=
                LGestionBP."ChercheDateLivr.Logiflux"(SalesShptHeader."Posting Date", SalesShptHeader."Mode d'expédition");

        // FIN AD Le 05-12-2011 =>

        ShipLabel."Instruction of Delivery" := SalesShptHeader."Instructions of Delivery";
        ShipLabel."Cash on delivery" := SalesShptHeader."Cash on Delivery";

        ShipLabel."External Document No." := SalesShptHeader."External Document No.";

        ShipLabel."Nb Of Pallets" := SalesShptHeader."Nb Of Box";
        ShipLabel."Nb Of Box" := SalesShptHeader."Nb Of Box";

        ShipLabel."Assured Box" := SalesShptHeader."Insurance of Delivery";

        ShipLabel.Weight := SalesShptHeader.Weight;
        ShipLabel."Mode d'expédition" := SalesShptHeader."Mode d'expédition";

        ShipLabel.Insert();

        // Affectation des num‚ros de colis uniques.
        // -----------------------------------------
        IF ShipLabel."Nb Of Pallets" <> 0 THEN BEGIN
            ShipLabel."No. start label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
            ShipLabel."No. end label" := ShipLabel."No. start label";

            Quantité_collis := 1;
            WHILE Quantité_collis < ShipLabel."Nb Of Pallets" DO BEGIN
                ShipLabel."No. end label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
                Quantité_collis += 1;
            END;

            IF NOT ShipLabel.MODIFY() THEN
                ShipLabel.Insert();
        END;



        // Edition de l'‚tiquette.
        // -----------------------
        IF ShipLabel."No. start label" <> '' THEN BEGIN
            IF transporteur.GET(ShipLabel."Shipping Agent Code") THEN
                IF transporteur."Label Report ID" <> 0 THEN BEGIN
                    //Modif parce que ne marche pas pour SCHENKER et urgent.
                    ShipLabel.SETRECFILTER();
                    REPORT.RUNMODAL(transporteur."Label Report ID", FALSE, FALSE, ShipLabel);
                    ShipLabel.RESET();
                    //FIN MODIF
                END
                ELSE
                    EtqLogiflux.EditionEtiquetteLogiflux(ShipLabel, 'M');

        END;
    END;

    PROCEDURE UpdateOrderShipmentStatus(VAR SalesHeader: Record "Sales Header");
    VAR
        SalesLine: Record "Sales Line";
    BEGIN
        // AD Le 24-05-2007 => Gestion de l'‚tat de livraison de la commande.
        // AD Le 27-09-2016 => REGIE -> Gestion pour les retours
        // IF  SalesHeader."Document Type" <> SalesHeader."Document Type"::Order THEN
        IF NOT (SalesHeader."Document Type" IN [SalesHeader."Document Type"::Order,
                                                 SalesHeader."Document Type"::"Return Order"]) THEN
            // FIN AD Le 27-09-2016
            EXIT;

        SalesHeader.CALCFIELDS("Completely Shipped");
        IF SalesHeader."Completely Shipped" THEN BEGIN
            CASE SalesHeader."Document Type" OF
                SalesHeader."Document Type"::Order:
                    SalesHeader."Shipment Status" := SalesHeader."Shipment Status"::Livrée;
                SalesHeader."Document Type"::"Return Order":
                    SalesHeader."Return Status" := SalesHeader."Return Status"::Retourné;
            END;
            SalesHeader.Status := SalesHeader.Status::Released;
        END
        ELSE BEGIN

            CASE SalesHeader."Document Type" OF
                SalesHeader."Document Type"::Order:
                    SalesHeader."Shipment Status" := SalesHeader."Shipment Status"::"A Livrer";
                SalesHeader."Document Type"::"Return Order":
                    SalesHeader."Return Status" := SalesHeader."Shipment Status"::"A Livrer";
            END;

            SalesLine.RESET();
            SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
            SalesLine.SETRANGE("Document No.", SalesHeader."No.");
            SalesLine.SETRANGE(Type, SalesLine.Type::Item);

            CASE SalesHeader."Document Type" OF
                SalesHeader."Document Type"::Order:
                    SalesLine.SETFILTER("Quantity Shipped", '<>0');
                SalesHeader."Document Type"::"Return Order":
                    SalesLine.SETFILTER("Return Qty. Received", '<>0');
            END;

            IF SalesLine.FINDFIRST() THEN
                CASE SalesHeader."Document Type" OF
                    SalesHeader."Document Type"::Order:
                        SalesHeader."Shipment Status" := SalesHeader."Shipment Status"::Reliquat;
                    SalesHeader."Document Type"::"Return Order":
                        SalesHeader."Return Status" := SalesHeader."Shipment Status"::Reliquat;
                END;



            // Si la commande n'est pas livr‚e entiŠrement, on la remet a pr‚parer
            CASE SalesHeader."Document Type" OF
                SalesHeader."Document Type"::Order:
                    SalesHeader.Status := SalesHeader.Status::Preparate;
                SalesHeader."Document Type"::"Return Order":
                    SalesHeader.Status := SalesHeader.Status::Released;
            END;


        END;


        SalesHeader.MODIFY();
        EXIT;
    END;

    PROCEDURE PostItemJnlLineLoans2(SalesLine: Record "Sales Line"; QtyToBeShipped: Decimal; QtyToBeShippedBase: Decimal; QtyToBeInvoiced: Decimal; QtyToBeInvoicedBase: Decimal; ItemLedgShptEntryNo: Integer; ItemChargeNo: Code[20]; TrackingSpecification: Record "Tracking Specification"): Integer;
    VAR
        ItemChargeSalesLine: Record "Sales Line";
        WhseJnlLine: Record "Warehouse Journal Line";
        WhseJnlLine2: Record "Warehouse Journal Line";
        OriginalItemJnlLine: Record "Item Journal Line";
        PostWhseJnlLine: Boolean;
        CheckApplFromItemEntry: Boolean;
    BEGIN
        ERROR('PAS DE PRET EN 2015 !');

        /*
        Fonction … Recoder pour SYMPTA ! Il n'UTILISE PAS LE MODULE DE PRET !!!

        //fonction de création des écriture articles sur le magasin pret
        //MC Le 29-09-2010 => Pour ne pas avoir de problŠme avec la DEE
        IF SalesLine.Type = SalesLine.Type::Item THEN BEGIN
              IF NOT ItemJnlRollRndg THEN BEGIN
                  RemAmt := 0;
                  RemDiscAmt := 0;
              END;
              WITH SalesLine DO BEGIN
                  ItemJnlLine.INIT();
                  ItemJnlLine."Posting Date" := SalesHeader."Posting Date";
                  ItemJnlLine."Document Date" := SalesHeader."Document Date";
                  ItemJnlLine."Source Posting Group" := SalesHeader."Customer Posting Group";
                  ItemJnlLine."Salespers./Purch. Code" := SalesHeader."Salesperson Code";
                  ItemJnlLine."Country/Region Code" := SalesHeader."VAT Country/Region Code";
                  ItemJnlLine."Reason Code" := SalesHeader."Reason Code";
                  ItemJnlLine."Item No." := "No.";
                  ItemJnlLine.Description := Description;
                  ItemJnlLine."Shortcut Dimension 1 Code" := "Shortcut Dimension 1 Code";
                  ItemJnlLine."Shortcut Dimension 2 Code" := "Shortcut Dimension 2 Code";

                  ItemJnlLine."Location Code" := SalesSetup."Loans Loacation";
                  ItemJnlLine."Bin Code" := "Bin Code";
                  ItemJnlLine."Variant Code" := "Variant Code";
                  ItemJnlLine."Inventory Posting Group" := "Posting Group";
                  ItemJnlLine."Gen. Bus. Posting Group" := "Gen. Bus. Posting Group";
                  ItemJnlLine."Gen. Prod. Posting Group" := "Gen. Prod. Posting Group";
                  ItemJnlLine."Applies-to Entry" := "Appl.-to Item Entry";
                  ItemJnlLine."Transaction Type" := "Transaction Type";
                  ItemJnlLine."Transport Method" := "Transport Method";
                  ItemJnlLine."Entry/Exit Point" := "Exit Point";
                  ItemJnlLine.Area := Area;
                  ItemJnlLine."Transaction Specification" := "Transaction Specification";
                  ItemJnlLine."Drop Shipment" := "Drop Shipment";
                  ItemJnlLine."Entry Type" := ItemJnlLine."Entry Type"::Sale;
                  ItemJnlLine."Unit of Measure Code" := "Unit of Measure Code";
                  ItemJnlLine."Qty. per Unit of Measure" := "Qty. per Unit of Measure";
                  ItemJnlLine."Derived from Blanket Order" := "Blanket Order No." <> '';
                  ItemJnlLine."Cross-Reference No." := "Cross-Reference No.";
                  ItemJnlLine."Originally Ordered No." := "Originally Ordered No.";
                  ItemJnlLine."Originally Ordered Var. Code" := "Originally Ordered Var. Code";
                  ItemJnlLine."Out-of-Stock Substitution" := "Out-of-Stock Substitution";
                  ItemJnlLine."Item Category Code" := "Item Category Code";
                  ItemJnlLine.Nonstock := Nonstock;
                  ItemJnlLine."Purchasing Code" := "Purchasing Code";
                  ItemJnlLine."Product Group Code" := "Product Group Code";
                  ItemJnlLine."Return Reason Code" := "Return Reason Code";

                  ItemJnlLine."Planned Delivery Date" := "Planned Delivery Date";
                  ItemJnlLine."Order Date" := SalesHeader."Order Date";

                  ItemJnlLine."Serial No." := TrackingSpecification."Serial No.";
                  ItemJnlLine."Lot No." := TrackingSpecification."Lot No.";





                  IF QtyToBeShipped = 0 THEN BEGIN
                      IF "Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"] THEN
                          ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Credit Memo"
                      ELSE
                          ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Invoice";
                      ItemJnlLine."Document No." := GenJnlLineDocNo;
                      ItemJnlLine."External Document No." := GenJnlLineExtDocNo;
                      ItemJnlLine."Posting No. Series" := SalesHeader."Posting No. Series";
                  END ELSE BEGIN
                      IF "Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"] THEN BEGIN
                          ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Return Receipt";
                          ItemJnlLine."Document No." := ReturnRcptHeader."No.";
                          ItemJnlLine."External Document No." := ReturnRcptHeader."External Document No.";
                          ItemJnlLine."Posting No. Series" := ReturnRcptHeader."No. Series";
                      END ELSE BEGIN
                          ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Shipment";
                          ItemJnlLine."Document No." := SalesShptHeader."No.";
                          ItemJnlLine."External Document No." := SalesShptHeader."External Document No.";
                          ItemJnlLine."Posting No. Series" := SalesShptHeader."No. Series";
                      END;
                      IF QtyToBeInvoiced <> 0 THEN BEGIN
                          ItemJnlLine."Invoice No." := GenJnlLineDocNo;
                          ItemJnlLine."External Document No." := GenJnlLineExtDocNo;
                          IF ItemJnlLine."Document No." = '' THEN BEGIN
                              IF "Document Type" = "Document Type"::"Credit Memo" THEN
                                  ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Credit Memo"
                              ELSE
                                  ItemJnlLine."Document Type" := ItemJnlLine."Document Type"::"Sales Invoice";
                              ItemJnlLine."Document No." := GenJnlLineDocNo;

                          END;
                          ItemJnlLine."Posting No. Series" := SalesHeader."Posting No. Series";
                      END;
                  END;
                  IF ItemJnlLine."Document No." = '' THEN
                      ItemJnlLine."Document No." := SalesInvHeader."No.";


                  ItemJnlLine."Document Line No." := "Line No.";
                  ItemJnlLine.Quantity := QtyToBeShipped;
                  ItemJnlLine."Quantity (Base)" := QtyToBeShippedBase;
                  ItemJnlLine."Invoiced Quantity" := QtyToBeInvoiced;
                  ItemJnlLine."Invoiced Qty. (Base)" := QtyToBeInvoicedBase;
                  ItemJnlLine."Unit Cost" := "Unit Cost (LCY)";
                  ItemJnlLine."Source Currency Code" := SalesHeader."Currency Code";
                  ItemJnlLine."Unit Cost (ACY)" := "Unit Cost";
                  ItemJnlLine."Value Entry Type" := ItemJnlLine."Value Entry Type"::"Direct Cost";

                  IF ItemChargeNo <> '' THEN BEGIN
                      ItemJnlLine."Item Charge No." := ItemChargeNo;
                      "Qty. to Invoice" := QtyToBeInvoiced;
                  END ELSE
                      ItemJnlLine."Applies-from Entry" := "Appl.-from Item Entry";

                  IF QtyToBeInvoiced <> 0 THEN BEGIN
                      ItemJnlLine.Amount := (Amount * (QtyToBeInvoiced / "Qty. to Invoice") - RemAmt);
                      IF SalesHeader."Prices Including VAT" THEN
                          ItemJnlLine."Discount Amount" :=
                            (("Line Discount Amount" + "Inv. Discount Amount") / (1 + "VAT %" / 100) *
                              (QtyToBeInvoiced / "Qty. to Invoice") - RemDiscAmt)
                      ELSE
                          ItemJnlLine."Discount Amount" :=
                            (("Line Discount Amount" + "Inv. Discount Amount") * (QtyToBeInvoiced / "Qty. to Invoice") - RemDiscAmt);
                      RemAmt := -(ItemJnlLine.Amount - ROUND(ItemJnlLine.Amount));
                      RemDiscAmt := -(ItemJnlLine."Discount Amount" - ROUND(ItemJnlLine."Discount Amount"));
                      ItemJnlLine.Amount := -ROUND(ItemJnlLine.Amount);
                      ItemJnlLine."Discount Amount" := -ROUND(ItemJnlLine."Discount Amount");
                  END ELSE BEGIN
                      IF SalesHeader."Prices Including VAT" THEN
                          ItemJnlLine.Amount :=
                            ((QtyToBeShipped * "Unit Price" * (1 - SalesLine."Line Discount %" / 100) / (1 + "VAT %" / 100)) - RemAmt)
                      ELSE
                          ItemJnlLine.Amount :=
                            ((QtyToBeShipped * "Unit Price" * (1 - SalesLine."Line Discount %" / 100)) - RemAmt);
                      RemAmt := -(ItemJnlLine.Amount - ROUND(ItemJnlLine.Amount));
                      IF SalesHeader."Currency Code" <> '' THEN
                          ItemJnlLine.Amount :=
                            ROUND(
                              CurrExchRate.ExchangeAmtFCYToLCY(
                                SalesHeader."Posting Date", SalesHeader."Currency Code",
                                ItemJnlLine.Amount, SalesHeader."Currency Factor"))
                      ELSE
                          ItemJnlLine.Amount := ROUND(ItemJnlLine.Amount);
                  END;

                  ItemJnlLine."Source Type" := ItemJnlLine."Source Type"::Customer;
                  ItemJnlLine."Source No." := "Sell-to Customer No.";
                  ItemJnlLine."Source Code" := SrcCode;
                  ItemJnlLine."Item Shpt. Entry No." := ItemLedgShptEntryNo;

                  IF NOT JobContractLine THEN BEGIN
                      IF SalesSetup."Exact Cost Reversing Mandatory" AND (Type = Type::Item) THEN
                          IF "Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"] THEN
                              CheckApplFromItemEntry := Quantity > 0
                          ELSE
                              CheckApplFromItemEntry := Quantity < 0;
                      ItemJnlLine."Shipment Method Code" := SalesHeader."Shipment Method Code";

                      IF ("Location Code" <> '') AND (Type = Type::Item) AND (ItemJnlLine.Quantity <> 0) THEN BEGIN
                          GetLocation("Location Code");
                          IF (("Document Type" IN ["Document Type"::Invoice, "Document Type"::"Credit Memo"]) AND
                              (Location."Directed Put-away and Pick")) OR
                             (Location."Bin Mandatory" AND NOT (WhseShip OR WhseReceive OR InvtPickPutaway OR "Drop Shipment"))
                          THEN BEGIN
                              CreateWhseJnlLine(ItemJnlLine, SalesLine, TempWhseJnlLine);
                              PostWhseJnlLine := TRUE;
                          END;
                      END;

                      IF (QtyToBeShippedBase <> 0) THEN
                          IF "Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"] THEN
                              ReserveSalesLine.TransferSalesLineToItemJnlLine(SalesLine, ItemJnlLine, QtyToBeShippedBase, CheckApplFromItemEntry, FALSE)
                          ELSE
                              TransferReservToItemJnlLine(
                                SalesLine, ItemJnlLine, -QtyToBeShippedBase, TempTrackingSpecification, CheckApplFromItemEntry);

                      IF CheckApplFromItemEntry THEN
                          TESTFIELD("Appl.-from Item Entry");


                      IF SalesLine."Build Kit" AND (QtyToBeShipped <> 0) THEN
                          PostBOMJnlLine(ItemJnlLine, SalesLine);

                      OriginalItemJnlLine := ItemJnlLine;
                      ItemJnlPostLine.RunWithCheck(ItemJnlLine, TempJnlLineDim);

                      IF ItemJnlPostLine.CollectTrackingSpecification(TempHandlingSpecification) THEN
                          IF TempHandlingSpecification.FINDSET() THEN
                              REPEAT
                                  TempTrackingSpecification := TempHandlingSpecification;
                                  TempTrackingSpecification."Source Type" := DATABASE::"Sales Line";
                                  TempTrackingSpecification."Source Subtype" := "Document Type";
                                  TempTrackingSpecification."Source ID" := "Document No.";
                                  TempTrackingSpecification."Source Batch Name" := '';
                                  TempTrackingSpecification."Source Prod. Order Line" := 0;
                                  TempTrackingSpecification."Source Ref. No." := "Line No.";
                                  IF TempTrackingSpecification.INSERT THEN;
                                  IF QtyToBeInvoiced <> 0 THEN BEGIN
                                      TempTrackingSpecificationInv := TempTrackingSpecification;
                                      IF TempTrackingSpecificationInv.INSERT THEN;
                                  END;
                              UNTIL TempHandlingSpecification.NEXT() = 0;
                      IF PostWhseJnlLine THEN BEGIN
                          ItemTrackingMgt.SplitWhseJnlLine(TempWhseJnlLine, TempWhseJnlLine2, TempTrackingSpecification, FALSE);
                          IF TempWhseJnlLine2.FINDSET() THEN
                              REPEAT
                                  WhseJnlPostLine.RUN(TempWhseJnlLine2);
                              UNTIL TempWhseJnlLine2.NEXT() = 0;
                      END;

                      IF (Type = Type::Item) AND SalesHeader.Invoice THEN BEGIN
                          ClearItemChargeAssgntFilter;
                          TempItemChargeAssgntSales.SETCURRENTKEY(
                            "Applies-to Doc. Type", "Applies-to Doc. No.", "Applies-to Doc. Line No.");
                          TempItemChargeAssgntSales.SETRANGE("Applies-to Doc. Type", "Document Type");
                          TempItemChargeAssgntSales.SETRANGE("Applies-to Doc. No.", "Document No.");
                          TempItemChargeAssgntSales.SETRANGE("Applies-to Doc. Line No.", "Line No.");
                          IF TempItemChargeAssgntSales.FINDSET() THEN
                              REPEAT
                                  SalesLine.TESTFIELD("Allow Item Charge Assignment");
                                  GetItemChargeLine(ItemChargeSalesLine);
                                  ItemChargeSalesLine.CALCFIELDS("Qty. Assigned");
                                  IF (ItemChargeSalesLine."Qty. to Invoice" <> 0) OR
                                     (ABS(ItemChargeSalesLine."Qty. Assigned") < ABS(ItemChargeSalesLine."Quantity Invoiced"))
                                  THEN BEGIN
                                      OriginalItemJnlLine."Item Shpt. Entry No." := ItemJnlLine."Item Shpt. Entry No.";
                                      PostItemChargePerOrder(OriginalItemJnlLine, ItemChargeSalesLine);
                                      TempItemChargeAssgntSales.MARK(TRUE);
                                  END;
                              UNTIL TempItemChargeAssgntSales.NEXT() = 0;
                      END;
                  END;
              END;

              EXIT(ItemJnlLine."Item Shpt. Entry No.");
          END;
        */
    END;

    PROCEDURE CreationBordereau(VAR BordereauNo: Code[20]; VAR SalesHeader: Record "Sales Header");
    VAR
        Paiement: Record "Payment Method";
        Entete_paiement: Record "Payment Header";
        Process: Record "Payment Class";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        Steps: Record "Payment Step";
        Payment_Status: Record "Payment Status";
        PostingStatement: Codeunit "Payment Management";
    BEGIN
        //cr‚ation de l'entete bordereau

        CLEAR(Entete_paiement);
        Process.GET(SalesHeader."Scénario paiement");

        Entete_paiement.SETRANGE("Payment Class", Process.Code);
        Entete_paiement.SETRANGE("Posting Date", SalesHeader."Posting Date");
        Entete_paiement.SETRANGE(System, TRUE);
        Entete_paiement.SETRANGE("Status No.", 10000);
        IF Entete_paiement.FINDFIRST() THEN BEGIN
            BordereauNo := Entete_paiement."No.";
            EXIT;
        END;

        CLEAR(Entete_paiement);
        Entete_paiement.VALIDATE("Payment Class", Process.Code);
        Entete_paiement.InitHeader();
        NoSeriesMgt.InitSeries(Process."Header No. Series", '', 0D, Entete_paiement."No.", Entete_paiement."No. Series");
        Entete_paiement.Insert();
        Entete_paiement.VALIDATE("Posting Date", SalesHeader."Posting Date");
        Entete_paiement."Status No." := 10000;
        Entete_paiement.System := TRUE;
        Entete_paiement.MODIFY();
        BordereauNo := Entete_paiement."No.";
    END;

    PROCEDURE InsertionLigneBordereau(BordereauNo: Code[20]; NoDocument: Code[20]; date_echeance: Date; montant: Decimal; Pourcentage: Decimal; VAR SalesHeader: Record "Sales Header");
    VAR
        Entete_paiement: Record "Payment Header";
        PaymentLine: Record "Payment Line";
        Noligne: Integer;
        Cust2: Record Customer;
    BEGIN
        CLEAR(PaymentLine);
        Entete_paiement.GET(BordereauNo);
        PaymentLine.SETRANGE("No.", BordereauNo);
        IF PaymentLine.FINDLAST() THEN;
        Noligne := PaymentLine."Line No." + 10000;


        CLEAR(PaymentLine);
        PaymentLine."No." := BordereauNo;
        PaymentLine."Line No." := Noligne;
        PaymentLine."Account Type" := PaymentLine."Account Type"::Customer;
        PaymentLine.VALIDATE("Account No.", SalesHeader."Bill-to Customer No.");
        PaymentLine.VALIDATE("Document No.", NoDocument);
        PaymentLine."Acceptation Code" := PaymentLine."Acceptation Code"::LCR;

        Cust2.GET(SalesHeader."Bill-to Customer No.");
        // PaymentLine.VALIDATE("Bank Account Code", Cust2."Default Bank Account Code"); // AD MIG2015 => LE cham n'existe plus en 2015
        PaymentLine."Payment Class" := Entete_paiement."Payment Class";


        PaymentLine.VALIDATE(Amount, montant);
        PaymentLine.VALIDATE("Due Date", date_echeance);

        PaymentLine.VALIDATE("Status No.", 10000);

        PaymentLine."Pourcentage montant initial" := Pourcentage;
        PaymentLine.Insert();
    END;
    //<<Mig CU 80

    //>> Mig CU 22
    PROCEDURE InsertStatConso(_ItemLedgEntry: Record "Item Ledger Entry");
    VAR
        Rec_StatsConso: Record "Stats Conso";
        Date_conso: Date;
        LReturn: Record "Return Receipt Line";
        LItemLedgEntry2: Record "Item Ledger Entry";
        rec_ReturnReceiptLine: Record "Return Receipt Line";
        rec_FromItemLedgerEntry: Record "Item Ledger Entry";
    BEGIN

        //NE PRENDRE QUE LES SORTIES DE STOCK
        // AD Le 17-04-2012 =>
        // IF (_ItemLedgEntry.Positive)  THEN CODE D'ORIGINE
        IF (_ItemLedgEntry.Positive)
          AND (_ItemLedgEntry."Document Type" <> _ItemLedgEntry."Document Type"::"Sales Return Receipt")
          // MC Le 31-01-2013 => Prise en compte des annulations de livraison dans les consomations
          // Mise en place du code d'AD
          // AD Le 04-12-2012 => A mettre en place pour tests.
          AND (_ItemLedgEntry."Document Type" <> _ItemLedgEntry."Document Type"::"Sales Shipment")
          // AD Le 04-12-2012
          // FIN MC Le 31-01-2013
          // FIN AD Le 17-04-2012
          THEN
            EXIT;


        WITH _ItemLedgEntry DO BEGIN
            Date_conso := DMY2DATE(1,
                        DATE2DMY("Posting Date", 2),
                        DATE2DMY("Posting Date", 3));


            // MC Le 31-01-2013 => Stats conso retour … la date de l'‚criture d'origine
            // Le code suivant permet pour un retour de poster la consommation … la date du mouvement d'origine
            // Seulement du fait que la ligne : R‚ception Retour enregistr‚e n'existe pas encore cela ne fonctionne pas
            // Ancien code
            /*
            // AD Le 17-04-2012 =>
            IF _ItemLedgEntry."Document Type" = _ItemLedgEntry."Document Type"::"Sales Return Receipt" THEN
              IF LReturn.GET(_ItemLedgEntry."Document No.", _ItemLedgEntry."Document Line No.") THEN
                IF LReturn."Appl.-from Item Entry" <> 0 THEN
                  IF LItemLedgEntry2.GET(LReturn."Appl.-from Item Entry") THEN
                    BEGIN
                        Date_conso:=DMY2DATE(1,
                        DATE2DMY(LItemLedgEntry2."Posting Date",2),
                        DATE2DMY(LItemLedgEntry2."Posting Date",3));
                    END;
            // FIN AD Le 17-04-2012
            */
            // Nouveau code
            IF _ItemLedgEntry."Document Type" = _ItemLedgEntry."Document Type"::"Sales Return Receipt" THEN
                IF LItemLedgEntry2.GET(_ItemLedgEntry."Return Appl.-from Item Entry") THEN BEGIN
                    Date_conso := DMY2DATE(1,
                    DATE2DMY(LItemLedgEntry2."Posting Date", 2),
                    DATE2DMY(LItemLedgEntry2."Posting Date", 3));
                END;
            // FIN MC Le 31-01-2013

            IF NOT Rec_StatsConso.GET("Item No.", Date_conso, "Entry Type", "Location Code") THEN BEGIN
                Rec_StatsConso.INIT();
                Rec_StatsConso."Item No." := "Item No.";
                Rec_StatsConso."Posting Date" := Date_conso;
                Rec_StatsConso."Item Ledger Entry Type" := "Entry Type".AsInteger();
                Rec_StatsConso."Location Code" := "Location Code";
                Rec_StatsConso.Insert();

            END;

            Rec_StatsConso."Valued Quantity" := Rec_StatsConso."Valued Quantity" + Quantity;
            Rec_StatsConso.MODIFY();
        END;
    END;

    PROCEDURE MAJDateDerniereEntrée(_ItemLedgEntry: Record "Item Ledger Entry");
    VAR
        _item: Record Item;
    BEGIN

        //NE PRENDRE QUE LES entr‚e DE STOCK
        IF NOT _ItemLedgEntry.Positive THEN
            EXIT;

        WITH _ItemLedgEntry DO BEGIN
            IF ("Entry Type" = "Entry Type"::Purchase) AND (_ItemLedgEntry."Source No." <> '') THEN BEGIN
                _item.GET(_ItemLedgEntry."Item No.");
                _item."Date dernière entrée" := _ItemLedgEntry."Posting Date";
                _item.MODIFY();
            END;

            // AD Le 04-12-2012 => Demande de LM & BB
            IF ("Entry Type" IN ["Entry Type"::Output]) AND
               (Positive) THEN BEGIN
                _item.GET(_ItemLedgEntry."Item No.");
                _item."Date dernière entrée" := _ItemLedgEntry."Posting Date";
                _item.MODIFY();
            END;
            // FIN AD Le 04-12-2012
        END;
    END;
    //<< Mig CU 22
    //>> Mig CU 23
    PROCEDURE ValiderLigneReclassement(pItemJournalLine: Record "Item Journal Line"): Boolean;
    VAR
        lItemJournalBatch: Record "Item Journal Batch";
        remplirFeuillePicking: Boolean;
        LNomFeuilleIN: Code[10];
        lItemJournalBatchIN: Record "Item Journal Batch";
        lItemJournalLineIN: Record "Item Journal Line";
        LNoLigneIN: Integer;
        LLocation: Record Location;
        LEmplacementParDefaut: Code[20];
        LWMSMngt: Codeunit "WMS Management";
    BEGIN
        // CFR Le 04/09/2020 : Comportement WIIO > on ajoute une ligne Picking … la validation d'une ligne surstock
        pItemJournalLine.SETRANGE("Journal Template Name", pItemJournalLine."Journal Template Name");
        pItemJournalLine.SETRANGE("Journal Batch Name", pItemJournalLine."Journal Batch Name");

        // Cherche la  feuille WIIO
        IF NOT lItemJournalBatch.GET('RECLASS', pItemJournalLine."Journal Batch Name") THEN
            EXIT;

        // ICI : on repŠre s'il faut mettre … jour la feuille Picking (*_I) correspondante … la feuille Surstock (*_O)
        remplirFeuillePicking := FALSE;
        IF lItemJournalBatch."Type Feuille Reclassement WIIO" = lItemJournalBatch."Type Feuille Reclassement WIIO"::Surstock THEN BEGIN
            LNomFeuilleIN := COPYSTR(lItemJournalBatch.Name, 1, STRLEN(lItemJournalBatch.Name) - 2) + '_I';
            IF (lItemJournalBatchIN.GET('RECLASS', LNomFeuilleIN)) AND
                  (NOT lItemJournalBatchIN.MiniLoad) AND
                  (lItemJournalBatchIN."Type Feuille Reclassement WIIO" = lItemJournalBatchIN."Type Feuille Reclassement WIIO"::Picking) THEN BEGIN
                remplirFeuillePicking := TRUE;
                // Cherche no ligne feuille _I
                CLEAR(lItemJournalLineIN);
                lItemJournalLineIN.SETRANGE("Journal Template Name", lItemJournalBatchIN."Journal Template Name");
                lItemJournalLineIN.SETRANGE("Journal Batch Name", lItemJournalBatchIN.Name);
                IF lItemJournalLineIN.FINDLAST() THEN
                    LNoLigneIN := lItemJournalLineIN."Line No." + 10000
                ELSE
                    LNoLigneIN := 10000;
            END;
        END;

        //*****
        // ICI : mettre … jour la feuille Picking (*_I) correspondante … la feuille Surstock (*_O)
        IF (pItemJournalLine.FINDSET()) THEN
            REPEAT

                IF (remplirFeuillePicking) AND (pItemJournalLine."A Valider") THEN BEGIN

                    LLocation.GET(pItemJournalLine."Location Code");
                    LLocation.TESTFIELD("Emplacement Tampon WIIO");
                    LWMSMngt.GetDefaultBin(pItemJournalLine."Item No.", '', LLocation.Code, LEmplacementParDefaut);

                    CLEAR(lItemJournalLineIN);
                    lItemJournalLineIN.VALIDATE("Journal Template Name", lItemJournalBatchIN."Journal Template Name");
                    lItemJournalLineIN.VALIDATE("Journal Batch Name", lItemJournalBatchIN.Name);
                    lItemJournalLineIN.VALIDATE("Entry Type", lItemJournalLineIN."Entry Type"::Transfer);
                    lItemJournalLineIN.VALIDATE("Posting Date", WORKDATE());
                    lItemJournalLineIN.VALIDATE("Document No.", 'FROM-' + pItemJournalLine."Journal Batch Name");
                    lItemJournalLineIN.VALIDATE("Line No.", LNoLigneIN);
                    lItemJournalLineIN.VALIDATE("Item No.", pItemJournalLine."Item No.");
                    lItemJournalLineIN.VALIDATE("Location Code", pItemJournalLine."Location Code");
                    lItemJournalLineIN.VALIDATE("Bin Code", LLocation."Emplacement Tampon WIIO");
                    lItemJournalLineIN.VALIDATE("New Location Code", pItemJournalLine."New Location Code");
                    lItemJournalLineIN.VALIDATE("New Bin Code", LEmplacementParDefaut);
                    // … la fin... sinon message d'erreur sur le "Bin Code"
                    lItemJournalLineIN.VALIDATE(Quantity, pItemJournalLine.Quantity);
                    lItemJournalLineIN.INSERT();

                    LNoLigneIN := LNoLigneIN + 10000
                END;

            UNTIL pItemJournalLine.NEXT() = 0;
        //*****

        EXIT(TRUE);
    END;
    //<< Mig CU 23
    //>> Mig CU 57
    PROCEDURE CalculateSalesTotalsToShip(VAR TotalSalesLine: Record "Sales Line"; VAR VATAmount: Decimal; VAR SalesLine: Record "Sales Line");
    VAR
        lSalesLineToShip: Record "Sales Line";
        lGrossWeightToShip: Decimal;
        lAmountToShip: Decimal;
    BEGIN
        // CFR le 04/10/2023 - R‚gie gestion des totaux … exp‚dier
        // [Copier coller de CalculateSalesTotals() avec filtre suppl‚mentaire sur Qt‚ … exp‚dier]

        TotalSalesLine.SETRANGE("Document Type", SalesLine."Document Type");
        TotalSalesLine.SETRANGE("Document No.", SalesLine."Document No.");
        TotalSalesLine.SETFILTER("Qty. to Ship", '>%1', 0);

        // MCO Le 21-11-2017 => Calcul du poids
        //TotalSalesLine.CALCSUMS("Line Amount",Amount,"Amount Including VAT","Inv. Discount Amount","VAT Difference");
        TotalSalesLine.CALCSUMS("Line Amount", Amount, "Amount Including VAT", "Inv. Discount Amount", "VAT Difference", "Gross Weight");

        // MCO Le 15-01-2018 => Pour le poids il faut calculer en fonction de la quantit‚ donc obliger de parcourir les lignes.
        lSalesLineToShip.SETRANGE("Document Type", SalesLine."Document Type");
        lSalesLineToShip.SETRANGE("Document No.", SalesLine."Document No.");
        lSalesLineToShip.SETFILTER("Qty. to Ship", '>%1', 0);
        IF lSalesLineToShip.FINDSET() THEN
            REPEAT
                lGrossWeightToShip += ROUND(lSalesLineToShip."Qty. to Ship" * lSalesLineToShip."Gross Weight", 0.00001);
                lAmountToShip += ROUND(lSalesLineToShip.Amount / lSalesLineToShip.Quantity * lSalesLineToShip."Qty. to Ship", 0.00001);
            UNTIL lSalesLineToShip.NEXT() = 0;
        TotalSalesLine."Gross Weight" := lGrossWeightToShip;
        TotalSalesLine.Amount := lAmountToShip;
        // MCO Le 21-11-2017 => Calcul du poids

        VATAmount := TotalSalesLine."Amount Including VAT" - TotalSalesLine.Amount;
    END;
    //<< Mig CU 57
    //>> Mig CU 229
    LOCAL PROCEDURE SavePurchHeaderReportAsPdf(VAR PurchHeader: Record "Purchase Header"; ReportId: Integer): Text[250];
    VAR
        FileManagement: Codeunit "File Management";
        ServerAttachmentFilePath: Text;
    BEGIN
        ServerAttachmentFilePath := FileManagement.ServerTempFileName('pdf');

        REPORT.SAVEASPDF(ReportId, ServerAttachmentFilePath, PurchHeader);
        IF NOT EXISTS(ServerAttachmentFilePath) THEN
            //ERROR(ServerSaveAsPdfFailedErr);

            EXIT(ServerAttachmentFilePath);
    END;
    //<< Mig CU 229
    //>> Mig CU 312
    PROCEDURE ShowNotificationDetailsWithoutRunModal(CreditLimitNotification: Notification);
    VAR
        CreditLimitNotificationPage: Page "Credit Limit Notification";
    BEGIN
        CreditLimitNotificationPage.SetHeading(CreditLimitNotification.MESSAGE);
        CreditLimitNotificationPage.InitializeFromNotificationVar(CreditLimitNotification);
        CreditLimitNotificationPage.RUN();
    END;

    PROCEDURE SalesHeaderCheckOld(SalesHeader: Record "Sales Header");
    var
        CustCheckCreditLimit: Page "Check Credit Limit";
        OK: Boolean;
        ESK002Err: Label 'Traitement annulé.';
    BEGIN
        IF NOT GUIALLOWED THEN
            EXIT;
        IF CustCheckCreditLimit.SalesHeaderShowWarning(SalesHeader) THEN BEGIN
            OK := CustCheckCreditLimit.RUNMODAL() = ACTION::Yes;
            CLEAR(CustCheckCreditLimit);
            IF NOT OK THEN
                ERROR(ESK002Err);
        END;
    END;
    //<< Mig CU 312
    //>> Mig CU 313
    PROCEDURE SentFaxPdf(VAR Rec_SalesHeader: Record "Sales Shipment Header");
    BEGIN
        // JB Le 17-11-2008 => MIGV5
        // AD Le 04-03-2008 => Gestion des envois FAX
        Rec_SalesHeader.FIND();
        Rec_SalesHeader."Date Envoi Fax/pdf" := WORKDATE();
        Rec_SalesHeader."Heure Envoi Fax/Pdf" := TIME;
        Rec_SalesHeader."Utilisateur Envoi Fax/Pdf" := USERID;
        Rec_SalesHeader.MODIFY();
        COMMIT();
    END;

    //<< Mig CU 313
    //>> Mig CU 314
    // PROCEDURE SentFaxPdf(salesShipHeader: Record "Sales Shipment Header")
    // BEGIN
    //     // AD Le 04-03-2008 => Gestion des envois FAX
    //     salesShipHeader.FIND;
    //     salesShipHeader."Date Envoi Fax/pdf" := WORKDATE;
    //     salesShipHeader."Heure Envoi Fax/Pdf" := TIME;
    //     salesShipHeader."Utilisateur Envoi Fax/Pdf" := USERID;
    //     salesShipHeader.MODIFY();
    //     COMMIT;
    // END;
    //<< Mig CU 314
    //>> Mig CU 315
    PROCEDURE SentFaxPdf(salesInvHeader: Record "Sales Invoice Header");
    BEGIN
        // AD Le 04-03-2008 => Gestion des envois FAX
        salesInvHeader.FIND();
        salesInvHeader."Date Envoi Fax/pdf" := WORKDATE();
        salesInvHeader."Heure Envoi Fax/Pdf" := TIME;
        salesInvHeader."Utilisateur Envoi Fax/Pdf" := USERID;
        salesInvHeader.MODIFY();
        COMMIT();
    END;
    //<< Mig CU 315
    //>> Mig CU 316
    PROCEDURE SentFaxPdf(SalesCrMemoHeader: Record "Sales Cr.Memo Header");
    BEGIN
        // AD Le 04-03-2008 => Gestion des envois FAX
        SalesCrMemoHeader.FIND();
        SalesCrMemoHeader."Date Envoi Fax/pdf" := WORKDATE();
        SalesCrMemoHeader."Heure Envoi Fax/Pdf" := TIME;
        SalesCrMemoHeader."Utilisateur Envoi Fax/Pdf" := USERID;
        SalesCrMemoHeader.MODIFY();
        COMMIT();
    END;
    //<< Mig CU 316
    //>> Mig CU 317
    PROCEDURE SentFaxPdf(PurchHeader: Record "Purchase Header");
    BEGIN
        // AD Le 04-03-2008 => Gestion des envois FAX
        PurchHeader.FIND();
        PurchHeader."Date Envoi Fax/pdf" := WORKDATE();
        PurchHeader."Heure Envoi Fax/Pdf" := TIME;
        PurchHeader."Utilisateur Envoi Fax/Pdf" := USERID;
        PurchHeader.MODIFY();
        COMMIT();
    END;
    //<< Mig CU 317
    //>> Mig CU 365
    PROCEDURE CustomerShipping(VAR AddrArray: ARRAY[8] OF Text[50]; VAR Cust: Record Customer);
    var
        FormatAddress: Codeunit "Format Address";
    BEGIN
        //CFR le 21/09/2021 => R‚gie : afficher l'adresse de livraison du client
        FormatAddress.FormatAddr(
          AddrArray, Cust.Name, Cust."Name 2", Cust.Contact, Cust."Adresse Expédition", Cust."Adresse 2 Expédition",
          Cust."Ville Expédition", Cust."Code postal Expédition", Cust."County Expédition", Cust."Country/Region Code Expédition");
    END;
    //<< Mig CU 365
    //>> Mig CU 414
    PROCEDURE "Release EDI Order"(SalesHeader: Record "Sales Header");
    VAR
        SalesLine: Record "Sales Line";
        Cust: Record Customer;
        CustCheckCreditLimit: Codeunit "Cust-Check Cr. Limit";
        ItemCheckAvail: Codeunit "Item-Check Avail.";
    BEGIN
        // AD Le 08-10-2009 => Permet de v‚rifier certaine donn‚e lors du lancement d'une commande EDI
        // -------------------------------------------------------------------------------------------

        Cust.GET(SalesHeader."Sell-to Customer No.");

        // Verif. Bloquage document
        Cust.CheckBlockedCustOnDocs(Cust, SalesHeader."Document Type", FALSE, FALSE);

        // Verif. Encours
        CustCheckCreditLimit.SalesHeaderCheck(SalesHeader);

        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
        SalesLine.SETRANGE("Document No.", SalesHeader."No.");
        IF SalesLine.FINDFIRST() THEN
            REPEAT
                IF (SalesLine."Document Type" IN [SalesLine."Document Type"::Order, SalesLine."Document Type"::Invoice]) AND
                   (SalesLine.Type = SalesLine.Type::Item) AND (SalesLine."No." <> '') AND
                   (SalesLine."Outstanding Quantity" > 0) AND
                   (SalesLine."Job Contract Entry No." = 0) AND
                   NOT (SalesLine.Nonstock OR SalesLine."Special Order")
                THEN
                    IF ItemCheckAvail.SalesLineCheck(SalesLine) THEN
                        ItemCheckAvail.RaiseUpdateInterruptedError();

            UNTIL SalesLine.NEXT() = 0;
    END;
    //<< Mig CU 414
    //>> Mig CU 415
    PROCEDURE CreateMvtEmplReturn(_PurchLine: Record "Purchase Line");
    VAR
        PurchLine: Record "Purchase Line";
         LItem: Record Item;        
        LItemJnlLine: Record "Item Journal Line";
        LBinDest: Record Bin;
        SingleInstance: Codeunit SingleInstance;
        ItemJnlPost: Codeunit "Item Jnl.-Post";
        NoDoc: Text[20];
        PosSlash: Integer;
        LUserID: Text[30];
        LBinContentOrigine: Record "Bin Content";
        ESK000Err: Label 'La quantité de l''emplacement %1 ne permet pas de couvrir la quantité nécéssaire de la ligne %2.' ,Comment = '%1 = Emplacement ; %2  = No Ligne';
       
    BEGIN
        // GB Le 18/09/2017 => Reprise des developpements perdu NAv 2009->2015  FR20150608
        // Cr‚ation fonctin
        // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
        CLEAR(LItemJnlLine);
        LItemJnlLine.SETRANGE("Journal Template Name", 'RECLASS');
        LItemJnlLine.SETRANGE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
        LItemJnlLine.SETRANGE("Journal Batch Name", 'DEFAUT_RET');
        IF LItemJnlLine.FINDSET() THEN
            LItemJnlLine.DELETEALL();
        // FIN CFR le 17/01/2024
        WITH LItemJnlLine DO BEGIN
            CLEAR(LItemJnlLine);
            INIT();
            VALIDATE("Journal Template Name", 'RECLASS');
            VALIDATE("Entry Type", "Entry Type"::Transfer);
            VALIDATE("Journal Batch Name", 'DEFAUT_RET');

            VALIDATE("Posting Date", WORKDATE());
            VALIDATE("Document No.", _PurchLine."Document No.");
            // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
            VALIDATE("Line No.", _PurchLine."Line No.");
            // FIN CFR le 17/01/2024
            VALIDATE("A Valider", TRUE);
            VALIDATE("Item No.", _PurchLine."No.");

            // emplacement source
            VALIDATE("Location Code", _PurchLine."Location Code");
            VALIDATE("Bin Code", _PurchLine."Bin Code");
            // emplacement de destination
            VALIDATE("New Location Code", _PurchLine."Location Code Temp");
            VALIDATE("New Bin Code", _PurchLine."Bin Code Temp");

            LBinDest.GET("Location Code", "Bin Code");
            // MCO Le 16-07-2018 => FE20180612_B - SYMTA Í Gestion des retours achats
            Item.GET(_PurchLine."No.");
            VALIDATE("Unit of Measure Code", Item."Base Unit of Measure");
            VALIDATE(Quantity, _PurchLine."Quantity (Base)");
            //VALIDATE(Quantity,  _PurchLine.Quantity);
            //VALIDATE("Unit of Measure Code", _PurchLine."Unit of Measure Code");

            // MCO Le 13-02-2018 => Contr“le la quantit‚ de l'emplacement d'origine
            //LBinContentOrigine.GET(_PurchLine."Location Code",_PurchLine."Bin Code",_PurchLine."No.",_PurchLine."Variant Code",_PurchLine."Unit of Measure Code");
            LBinContentOrigine.GET(_PurchLine."Location Code", _PurchLine."Bin Code", _PurchLine."No.", _PurchLine."Variant Code", Item."Base Unit of Measure");
            // FIN MCO Le 16-07-2018
            IF LBinContentOrigine.CalcQtyAvailToTakeUOM() < _PurchLine.Quantity THEN
                ERROR(STRSUBSTNO(ESK000Err, LBinContentOrigine."Bin Code", _PurchLine.Quantity));
            // MCO Le 13-02-2018 => Contr“le la quantit‚ de l'emplacement d'origine
            VALIDATE(Commentaire, 'Mvt pour gestion retour achat');
            INSERT(TRUE);
        END;
        SingleInstance.SetHideValidationDialog(TRUE);
        ItemJnlPost.RUN(LItemJnlLine);
        // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
        CLEAR(LItemJnlLine);
        LItemJnlLine.SETRANGE("Journal Template Name", 'RECLASS');
        LItemJnlLine.SETRANGE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
        LItemJnlLine.SETRANGE("Journal Batch Name", 'DEFAUT_RET');
        IF LItemJnlLine.FINDSET() THEN
            LItemJnlLine.DELETEALL();
        // FIN CFR le 17/01/2024
    END;
    //<< Mig CU 415
    //>> Mig CU 5063
    PROCEDURE ArchiveSalesOrderOnDelete(VAR SalesHeader: Record "Sales Header");
    VAR
        SalesReceivablesSetup: Record "Sales & Receivables Setup";
        ArchiveManagement: Codeunit ArchiveManagement;
    BEGIN
        // CFR le 05/04/2023 - R‚gie : Archivage des commandes … la suppression (si pas automatique)

        SalesReceivablesSetup.GET();
        // si on revient dans le standard, alors la fonction n'est pu utile
        IF SalesReceivablesSetup."Archive Orders" then //"Archive Quotes and Orders" THEN
            EXIT;
        // la demande ne porte que sur les commandes
        IF NOT (SalesHeader."Document Type" IN [SalesHeader."Document Type"::Order]) THEN
            EXIT;
        // On reprend le code qu'il y a derriŠre l'action [Archiver document]
        ArchiveManagement.ArchiveSalesDocument(SalesHeader);
    END;
    //<< Mig CU 5063
    //>> Mig CU 5705

    PROCEDURE SalesFinished(SalesHeader: Record "Sales Header"): Boolean;
    VAR
        SalesLines: Record "Sales Line";
    BEGIN
        SalesLines.RESET();
        SalesLines.SETRANGE("Document Type", SalesHeader."Document Type");
        SalesLines.SETRANGE("Document No.", SalesHeader."No.");
        IF SalesLines.FINDSET() THEN
            REPEAT
                IF SalesLines."Outstanding Quantity" <> 0 THEN
                    EXIT(FALSE);
                IF SalesLines."Qty. Shipped Not Invoiced" <> 0 THEN
                    EXIT(FALSE);


            UNTIL SalesLines.NEXT() = 0;
        EXIT(TRUE);
    END;

    PROCEDURE UpdateAttachedLine(NoPret: Code[20]; SalesLine: Record "Sales Line"; TransRcptLine: Record "Transfer Receipt Line");
    VAR
        SalesAttachedLine: Record "Sales Line";
    BEGIN
        //On r‚cupŠre les lignes attach‚es :
        SalesAttachedLine.RESET();
        SalesAttachedLine.SETRANGE("Document Type", SalesLine."Document Type"::Order);
        SalesAttachedLine.SETRANGE("Document No.", NoPret);
        SalesAttachedLine.SETRANGE("Attached to Line No.", SalesLine."Line No.");
        IF SalesAttachedLine.FINDSET() THEN BEGIN
            REPEAT
                IF (SalesAttachedLine.Type.AsInteger() > 0) THEN BEGIN
                    //mettre bonne quantit‚ re‡u PRET
                    SalesAttachedLine."Return Qty Loans" := SalesAttachedLine."Return Qty Loans" + TransRcptLine.Quantity;
                    SalesAttachedLine."Return Qty Loans(Base)" := SalesAttachedLine."Return Qty Loans(Base)" + TransRcptLine."Quantity (Base)";
                    SalesAttachedLine."Qty. Shipped Not Invoiced" := SalesAttachedLine."Qty. Shipped Not Invoiced" - TransRcptLine.Quantity;
                    SalesAttachedLine."Qty. Shipped Not Invd. (Base)" := SalesAttachedLine."Qty. Shipped Not Invd. (Base)"
                                                                       - TransRcptLine."Quantity (Base)";
                    SalesAttachedLine."Quantity Shipped" := SalesAttachedLine."Quantity Shipped" - TransRcptLine.Quantity;
                    SalesAttachedLine."Qty. Shipped (Base)" := SalesAttachedLine."Qty. Shipped (Base)" - TransRcptLine."Quantity (Base)";
                    SalesAttachedLine.Quantity := SalesAttachedLine.Quantity - TransRcptLine.Quantity;
                    SalesAttachedLine."Quantity (Base)" := SalesAttachedLine."Quantity (Base)" - TransRcptLine.Quantity;
                    SalesAttachedLine."Qty. to Invoice" := SalesAttachedLine."Qty. Shipped Not Invoiced";
                    SalesAttachedLine."Qty. to Invoice (Base)" := SalesAttachedLine."Qty. Shipped Not Invd. (Base)";
                    SalesAttachedLine."Qty To Receive Loans" := 0;
                    SalesAttachedLine."Qty To Receive Loans(Base)" := 0;
                    SalesAttachedLine.MODIFY();
                END;
            UNTIL SalesAttachedLine.NEXT() = 0;
        END;
    END;
    //<< Mig CU 5705
    //>> Mig CU 5750
    PROCEDURE CheckIfPurchLine2ReceiptHeader(pWhseReceiptHeader: Record "Warehouse Receipt Header"; pPurchLine: Record "Purchase Line"): Boolean;
    VAR
        lWhseRcptLine: Record "Warehouse Receipt Line";
    BEGIN
        // MCO Le 03-10-2018 => R‚gie : Fonction qui renvoit oui si la ligne de cde achat est d‚j… pr‚sente sur la r‚ception
        lWhseRcptLine.RESET();
        lWhseRcptLine.SETRANGE("No.", pWhseReceiptHeader."No.");
        lWhseRcptLine.SETRANGE("Source Type", 39);
        lWhseRcptLine.SETRANGE("Source Subtype", pPurchLine."Document Type");
        lWhseRcptLine.SETRANGE("Source No.", pPurchLine."Document No.");
        lWhseRcptLine.SETRANGE("Source Line No.", pPurchLine."Line No.");
        IF lWhseRcptLine.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    END;
    //<< Mig CU 5750
    //>> Mig CU 5804
    procedure CalcLastAdjEntryAvgCost(var Item: Record Item; var AverageCost: Decimal; var AverageCostACY: Decimal)
    var
        ValueEntry: Record "Value Entry";
        ItemLedgEntry: Record "Item Ledger Entry";
        ItemCostManagement: Codeunit ItemCostManagement;
        ComputeThisEntry: Boolean;
        IsSubOptimal: Boolean;
        AvgCostCalculated: Boolean;
    begin
        if AvgCostCalculated then
            exit;

        AverageCost := 0;
        AverageCostACY := 0;

        if CalculateQuantity(Item) <> 0 then
            exit;
        if not HasOpenEntries(Item) then
            exit;

        with ValueEntry do begin
            ItemCostManagement.SetFilters(ValueEntry, Item);
            if Find('+') then
                repeat
                    ComputeThisEntry := ("Item Ledger Entry Quantity" < 0) and not Adjustment and not "Drop Shipment";
                    if ComputeThisEntry then begin
                        ItemLedgEntry.Get("Item Ledger Entry No.");
                        IsSubOptimal :=
                          ItemLedgEntry.Correction or
                          ((Item."Costing Method" = Item."Costing Method"::Average) and not "Valued By Average Cost");

                        if not IsSubOptimal or (IsSubOptimal and (AverageCost = 0)) then begin
                            ItemLedgEntry.CalcFields(
                              "Cost Amount (Expected)", "Cost Amount (Actual)",
                              "Cost Amount (Expected) (ACY)", "Cost Amount (Actual) (ACY)");
                            AverageCost :=
                              (ItemLedgEntry."Cost Amount (Expected)" +
                               ItemLedgEntry."Cost Amount (Actual)") /
                              ItemLedgEntry.Quantity;
                            AverageCostACY :=
                              (ItemLedgEntry."Cost Amount (Expected) (ACY)" +
                               ItemLedgEntry."Cost Amount (Actual) (ACY)") /
                              ItemLedgEntry.Quantity;

                            if (AverageCost <> 0) and not IsSubOptimal then
                                exit;
                        end;
                    end;
                until Next(-1) = 0;
        end;
    end;

    local procedure CalculateQuantity(var Item: Record Item) CalcQty: Decimal
    var
        ValueEntry: Record "Value Entry";
        ItemCostManagement: Codeunit ItemCostManagement;
    begin
        with ValueEntry do begin
            ItemCostManagement.SetFilters(ValueEntry, Item);
            CalcSums("Item Ledger Entry Quantity");
            CalcQty := "Item Ledger Entry Quantity";
            exit(CalcQty);
        end;
    end;

    local procedure HasOpenEntries(var Item: Record Item): Boolean
    var
        ItemLedgEntry: Record "Item Ledger Entry";
    begin
        with ItemLedgEntry do begin
            Reset();
            SetCurrentKey("Item No.", Open);
            SetRange("Item No.", Item."No.");
            SetRange(Open, true);
            SetFilter("Location Code", Item.GetFilter("Location Filter"));
            SetFilter("Variant Code", Item.GetFilter("Variant Filter"));
            exit(not FindFirst())
        end;
    end;
    //<< Mig CU 5804
    //>> Mig CU 6620
    PROCEDURE UpdateAdressClientComptoir(VAR ToSalesHeader: Record "Sales Header");
    VAR
        LClientLivre: Record Customer;
    BEGIN
        // MCO Le 28-11-2017 => Si client Comptoir alors adresse de livraison = adresse de facturation
        IF LClientLivre.GET(ToSalesHeader."Sell-to Customer No.") THEN
            IF LClientLivre."Client Comptoir" THEN BEGIN
                ToSalesHeader.CopyShipToAddressToSellToAddress();
                ToSalesHeader.CopyShipToAddressToBillToAddress();
            END;
        // FIN MC Le 23-11-2011
    END;

    PROCEDURE CopyValueEntryToDoc(ToSalesHeader: Record "Sales Header"; VAR FromValueEntry: Record "Value Entry"; VAR LinesNotCopied: Integer; VAR MissingExCostRevLink: Boolean);
    VAR
        CopyDocumentMgt: Codeunit "Copy Document Mgt.";
        QteRetour: Decimal;
        PrixRetour: Decimal;
        CoutRetour: Decimal;
        FromLineCounter: Integer;
        ToSalesLine: Record "Sales Line";
        CUMultiRef: Codeunit "Gestion Multi-référence";
        LineNo: Integer;
        Window: Dialog;
        Text022Lbl: Label 'Copying document lines...\';
        Text023Lbl: Label 'Processing source lines      #1######\' , Comment = '%1 = Sources lines';
        Text024Lbl: Label 'Creating new lines           #2######' , Comment = '%1 = New Lines'; 
    BEGIN
        MissingExCostRevLink := FALSE;
        InitCurrency(ToSalesHeader."Currency Code");
        Window.Open(Text022Lbl +
            Text023Lbl +
            Text024Lbl);

        ToSalesLine.RESET();
        ToSalesLine.SETRANGE("Document Type", ToSalesHeader."Document Type");
        ToSalesLine.SETRANGE("Document No.", ToSalesHeader."No.");
        IF ToSalesLine.FINDFIRST() THEN
            LineNo := ToSalesLine."Line No." + 10000
        ELSE
            LineNo := 10000;



        // Fill sales line buffer
        WITH FromValueEntry DO
            IF FINDSET() THEN
                REPEAT
                    FromLineCounter := FromLineCounter + 1;
                    IF CopyDocumentMgt.IsTimeForUpdate() THEN
                        Window.UPDATE(1, FromLineCounter);

                    QteRetour := FromValueEntry."Reste à Retourné";
                    PrixRetour := FromValueEntry."Sales Amount (Actual)" / QteRetour;
                    CoutRetour := FromValueEntry."Cost per Unit";

                    ToSalesLine.INIT();
                    ToSalesLine.VALIDATE("Document Type", ToSalesHeader."Document Type");
                    ToSalesLine.VALIDATE("Document No.", ToSalesHeader."No.");
                    ToSalesLine.VALIDATE("Line No.", LineNo);
                    ToSalesLine.VALIDATE(Type, ToSalesLine.Type::Item);
                    ToSalesLine.VALIDATE("No.", FromValueEntry."Item No.");
                    ToSalesLine.VALIDATE(Quantity, QteRetour);
                    ToSalesLine.VALIDATE("Unit Price", PrixRetour);
                    ToSalesLine.VALIDATE("Discount1 %", 0);
                    ToSalesLine.VALIDATE("Discount2 %", 0);
                    ToSalesLine.VALIDATE("Unit Cost (LCY)", CoutRetour);
                    ToSalesLine."No Ecriture Histo Retournée" := FromValueEntry."Entry No.";
                    ToSalesLine."Recherche référence" := CUMultiRef.RechercheRefActive(ToSalesLine."No.");
                    ToSalesLine.INSERT(TRUE);

                    LineNo += 10000;
                UNTIL NEXT() = 0;

        // Create sales line from buffer
        Window.UPDATE(1, FromLineCounter);

        Window.CLOSE();
    END;

    local procedure InitCurrency(CurrencyCode: Code[10])
    var
        Currency: Record Currency;
    begin
        if CurrencyCode <> '' then
            Currency.Get(CurrencyCode)
        else
            Currency.InitRoundingPrecision();

        Currency.TestField("Unit-Amount Rounding Precision");
        Currency.TestField("Amount Rounding Precision");
    end;
    //<< Mig CU 6620
    //>> Mig CU 7000
    /*
    procedure FindSalesPrice(var ToSalesPrice: Record "Sales Price"; CustNo: Code[20]; ContNo: Code[20]; CustPriceGrCode: Code[10]; CampaignNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; SousGroupeNo: Code[20]; CentraleNo: Code[20]; _pTypeCommande: Option Tous,Dépannage,Réappro,Stock)
    var
        FromSalesPrice: Record "Sales Price";
        TempTargetCampaignGr: Record "Campaign Target Group" temporary;
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        TempTableErr: Label 'The table passed as a parameter must be temporary.';
    begin
        // MC Le 19-04-2011 => SYMTA -> Gestions des tarifs ventes.
        //  --> Rajout des deux derniers paramŠtres de la fonction.
        if not ToSalesPrice.IsTemporary then
            Error(TempTableErr);

        ToSalesPrice.Reset();
        ToSalesPrice.DeleteAll();

        with FromSalesPrice do begin
            SetRange("Item No.", ItemNo);
            SetFilter("Variant Code", '%1|%2', VariantCode, '');
            SetFilter("Ending Date", '%1|>=%2', 0D, StartingDate);
            if not ShowAll then begin
                SetFilter("Currency Code", '%1|%2', CurrencyCode, '');
                if UOM <> '' then
                    SetFilter("Unit of Measure Code", '%1|%2', UOM, '');
                SetRange("Starting Date", 0D, StartingDate);
            end;
            // AD Le 04-11-2011 => Tarif pas type de commande
            SETFILTER("Type de commande", '%1|%2', _pTypeCommande, FromSalesPrice."Type de commande"::Tous);
            // FIN AD Le 04-11-2011

            SetRange("Sales Type", "Sales Type"::"All Customers");
            SetRange("Sales Code");
            CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);

            if CustNo <> '' then begin
                SetRange("Sales Type", "Sales Type"::Customer);
                SetRange("Sales Code", CustNo);
                CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
            end;

            if CustPriceGrCode <> '' then begin
                SetRange("Sales Type", "Sales Type"::"Customer Price Group");
                SetRange("Sales Code", CustPriceGrCode);
                CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
            end;

            // AD Le 21-10-2009 => Possibilit‚ de ne pas activer une campagne
            /*
            if not ((CustNo = '') and (ContNo = '') and (CampaignNo = '')) then begin
                SetRange("Sales Type", "Sales Type"::Campaign);
                if SalesPriceCalcMgt.ActivatedCampaignExists(TempTargetCampaignGr, CustNo, ContNo, CampaignNo) then
                    repeat
                        SetRange("Sales Code", TempTargetCampaignGr."Campaign No.");
                        CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
                    until TempTargetCampaignGr.Next() = 0;
            end;
            */
    /*
    IF NOT ((CustNo = '') AND (CampaignNo = '')) THEN BEGIN
        SETRANGE("Sales Type", "Sales Type"::Campaign);
        SETRANGE("Sales Code", CampaignNo);
        CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
    END;
    // FIN AD Le 21-10-2009

    // MC Le 19-04-2011 => SYMTA -> Gestions des tarifs ventes.

    // Gestion des centrales.
    IF NOT ((CustNo = '') AND (CentraleNo = '')) THEN BEGIN
        SETRANGE("Sales Type", "Sales Type"::Centrale);
        SETRANGE("Sales Code", CentraleNo);
        CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
    END;

    // Gestion des sous-groupes tarifs.
    IF NOT ((CustNo = '') AND (SousGroupeNo = '')) THEN BEGIN
        SETRANGE("Sales Type", "Sales Type"::"Sous Groupe tarif");
        SETRANGE("Sales Code", SousGroupeNo);
        CopySalesPriceToSalesPrice(FromSalesPrice, ToSalesPrice);
    END;
    // FIN MC Le 19-04-2011
end;
end;

procedure FindSalesLineDisc(var ToSalesLineDisc: Record "Sales Line Discount"; CustNo: Code[20]; ContNo: Code[20]; CustDiscGrCode: Code[20]; CampaignNo: Code[20]; ItemNo: Code[20]; ItemDiscGrCode: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; SousGroupeNo: Code[20]; CentraleNo: Code[20]; TypeCommande: Option Dépannage,Réappro,Stock)
var
FromSalesLineDisc: Record "Sales Line Discount";
TempCampaignTargetGr: Record "Campaign Target Group" temporary;
SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
InclCampaigns: Boolean;
begin
// MC Le 19-04-2011 => SYMTA -> Gestions des tarifs ventes.
//  --> Rajout des trois derniers paramŠtres de la fonction.
with FromSalesLineDisc do begin
    SetFilter("Ending Date", '%1|>=%2', 0D, StartingDate);
    SetFilter("Variant Code", '%1|%2', VariantCode, '');
    if not ShowAll then begin
        SetRange("Starting Date", 0D, StartingDate);
        SetFilter("Currency Code", '%1|%2', CurrencyCode, '');
        if UOM <> '' then
            SetFilter("Unit of Measure Code", '%1|%2', UOM, '');
    end;
    // On pose le filtre sur le type de commande.
    FromSalesLineDisc.SETRANGE("Type de commande", TypeCommande);

    ToSalesLineDisc.Reset();
    ToSalesLineDisc.DeleteAll();
    for "Sales Type" := "Sales Type"::Customer to "Sales Type"::Campaign do
        if ("Sales Type" = "Sales Type"::"All Customers") or
           (("Sales Type" = "Sales Type"::Customer) and (CustNo <> '')) or
           (("Sales Type" = "Sales Type"::"Customer Disc. Group") and (CustDiscGrCode <> '')) or
     // MC Le 19-04-2011 => SYMTA -> Gestion des remises ventes. Prise en compte des nouvelles options.
     (("Sales Type" = "Sales Type"::Centrale) AND (CentraleNo <> '')) OR
     (("Sales Type" = "Sales Type"::"Sous Groupe tarif") AND (SousGroupeNo <> '')) OR
           // FIN MC Le 19-04-2011 => SYMTA -> Gestion des remises ventes. Prise en compte des nouvelles options.
           (("Sales Type" = "Sales Type"::Campaign) and
            not ((CustNo = '') and (ContNo = '') and (CampaignNo = '')))
        then begin
            InclCampaigns := false;

            SetRange("Sales Type", "Sales Type");
            case "Sales Type" of
                "Sales Type"::"All Customers":
                    SetRange("Sales Code");
                "Sales Type"::Customer:
                    SetRange("Sales Code", CustNo);
                // MC Le 19-04-2011 => SYMTA -> Gestions des remises ventes. Prise en compte des nouvelles options.
                // Gestion des centrales.
                "Sales Type"::Centrale:
                    SETRANGE("Sales Code", CentraleNo);
                // Gestion des sous groupe tarifs.
                "Sales Type"::"Sous Groupe tarif":
                    SETRANGE("Sales Code", SousGroupeNo);
                // FIN MC Le 19-04-2011
                "Sales Type"::"Customer Disc. Group":
                    SetRange("Sales Code", CustDiscGrCode);
                "Sales Type"::Campaign:
                    begin
                        InclCampaigns := SalesPriceCalcMgt.ActivatedCampaignExists(TempCampaignTargetGr, CustNo, ContNo, CampaignNo);
                        SetRange("Sales Code", TempCampaignTargetGr."Campaign No.");
                    end;
            end;
            // AD Le 06-06-2011 => Pour une recherche sur tous les clients / groupes .. pour connaitre la meilleur remise
            //{MIS EN COMMENTAIRE PAR AD LE 26-10-2011 CAR CA PLANTE LE QUID ET JE SAIS PLUS POURQUOI JE FAIS CA
            // AD Le 27-01-2012 => C'est utile pour la recherche de la meileur remise toute condition confondue
            IF (CustNo = '') AND (ContNo = '') AND (CustDiscGrCode = '') AND (CampaignNo = '')
            AND (SousGroupeNo = '') AND (CentraleNo = '') AND (TypeCommande = 0) AND RechercheMeilleureRemise THEN BEGIN
                SETRANGE("Sales Code");
                SETRANGE("Sales Type");
                SETRANGE("Type de commande");
            END;
            //}
            // FIN AD Le 06-06-2011
            repeat
                // Recherche pour l'article => STANDARD
                SetRange(Type, Type::Item);
                SetRange(Code, ItemNo);
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);

                // Recherche pour groupe remise => STANDARD
                if ItemDiscGrCode <> '' then begin
                    SetRange(Type, Type::"Item Disc. Group");
                    SetRange(Code, ItemDiscGrCode);
                    CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                end;

                // --------------------------------------------
                // MC Le 20-04-2011 => SYMTA -> REMISES VENTES
                // --------------------------------------------

                Item.GET(ItemNo);
                FromSalesLineDisc.SETRANGE(Code);
                // 1- negatif
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", '');
                FromSalesLineDisc.SETRANGE("Code Famille", '');
                FromSalesLineDisc.SETRANGE("Code Sous Famille", '');
                FromSalesLineDisc.SETFILTER("Line Discount %", '<0');
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;
                FromSalesLineDisc.SETRANGE("Line Discount %");

                // 1- Recherche article
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", Item."No.");
                FromSalesLineDisc.SETRANGE("Code Marque", '');
                FromSalesLineDisc.SETRANGE("Code Famille", '');
                FromSalesLineDisc.SETRANGE("Code Sous Famille", '');
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;

                // 2 - Recherche marque / famille / sous famille
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", Item."Manufacturer Code");
                FromSalesLineDisc.SETRANGE("Code Famille", Item."Item Category Code");
                FromSalesLineDisc.SETRANGE("Code Sous Famille", Item."Product Group Code");
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;

                // 3 - Recherche famille / sous famille
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", '');
                FromSalesLineDisc.SETRANGE("Code Famille", Item."Item Category Code");
                FromSalesLineDisc.SETRANGE("Code Sous Famille", Item."Product Group Code");
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;

                // 4- Recherche sous famille seule
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", '');
                FromSalesLineDisc.SETRANGE("Code Famille", '');
                FromSalesLineDisc.SETRANGE("Code Sous Famille", Item."Product Group Code");
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;


                // 5- Recherche marque / famille
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", Item."Manufacturer Code");
                FromSalesLineDisc.SETRANGE("Code Famille", Item."Item Category Code");
                FromSalesLineDisc.SETRANGE("Code Sous Famille", '');
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;


                // 6- Recherche famille
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", '');
                FromSalesLineDisc.SETRANGE("Code Famille", Item."Item Category Code");
                FromSalesLineDisc.SETRANGE("Code Sous Famille", '');
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;


                // 7 - Recherche marque
                SETRANGE(Type, Type::"All Item");
                FromSalesLineDisc.SETRANGE("Code Article", '');
                FromSalesLineDisc.SETRANGE("Code Marque", Item."Manufacturer Code");
                FromSalesLineDisc.SETRANGE("Code Famille", '');
                FromSalesLineDisc.SETRANGE("Code Sous Famille", '');
                CopySalesDiscToSalesDisc(FromSalesLineDisc, ToSalesLineDisc);
                IF ToSalesLineDisc.COUNT > 0 THEN EXIT;

                // --------------------
                // FIN MC Le 20-04-2011
                // --------------------

                if InclCampaigns then begin
                    InclCampaigns := TempCampaignTargetGr.Next() <> 0;
                    SetRange("Sales Code", TempCampaignTargetGr."Campaign No.");
                end;
            until not InclCampaigns;
        end;
end;
end;

local procedure CopySalesPriceToSalesPrice(var FromSalesPrice: Record "Sales Price"; var ToSalesPrice: Record "Sales Price")
begin
with ToSalesPrice do
    if FromSalesPrice.FindSet() then
        repeat
            ToSalesPrice := FromSalesPrice;
            Insert();
        until FromSalesPrice.Next() = 0;
end;

local procedure CopySalesDiscToSalesDisc(var FromSalesLineDisc: Record "Sales Line Discount"; var ToSalesLineDisc: Record "Sales Line Discount")
begin
with ToSalesLineDisc do
    if FromSalesLineDisc.FindSet() then
        repeat
            ToSalesLineDisc := FromSalesLineDisc;
            Insert();
        until FromSalesLineDisc.Next() = 0;
end;
*/
    PROCEDURE PurgeBestUnitPrice(VAR SalesPrice: Record "Sales Price");
    BEGIN
        // AD Le 08-10-2009 => FARGROUP -> Si on a des prix net, on ne prend pas en compte les prix brut dans le bestPrice
        IF SalesPrice.FINDFIRST() THEN
            REPEAT
            /*TODO
            IF NOT IsInMinQty(SalesPrice."Unit of Measure Code", SalesPrice."Minimum Quantity") THEN
                SalesPrice.DELETE
                */
            UNTIL SalesPrice.NEXT() = 0;

        SalesPrice.SETFILTER(SalesPrice."Sales Type", '<>%1', SalesPrice."Sales Type"::"All Customers");
        IF SalesPrice.COUNT <> 0 THEN BEGIN
            SalesPrice.SETRANGE(SalesPrice."Sales Type", SalesPrice."Sales Type"::"All Customers");
            SalesPrice.DELETEALL();
        END;

        SalesPrice.SETRANGE(SalesPrice."Sales Type");
    END;

    procedure GetCustNoForSalesHeader(SalesHeader: Record "Sales Header"): Code[20]
    var
        CustNo: Code[20];
    begin
        CustNo := SalesHeader."Bill-to Customer No.";
        exit(CustNo);
    end;

    PROCEDURE GetUnitPrice(VAR TempSalesPrice: Record "Sales Price"; CustNo: Code[20]; ContNo: Code[20]; CustPriceGrCode: Code[10]; CampaignNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; Qty: Decimal; VAR "Discount %": Decimal; VAR "Unit Price": Decimal; SousGroupeNo: Code[20]; CentraleNo: Code[20]; _pTypeCommande: Option Tous,Dépannage,Réappro,Stock): Boolean;
    VAR
        ItemUoM: Record "Item Unit of Measure";
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
    BEGIN
        // Cette fonction retoune Vrai si il existe des tarif et faut si il en existe pas.
        // Elle renvoi en paramŠtre le prix et la remise (la remise … 0 si le prix est net)

        // Charge l'article
        IF NOT Item.GET(ItemNo) THEN
            EXIT(FALSE);

        // Charge les qtes
        IF UOM <> '' THEN BEGIN
            ItemUoM.GET(ItemNo, UOM);
            SalesPriceCalcMgt.SetUoM(Qty, ItemUoM."Qty. per Unit of Measure");
        END ELSE
            SalesPriceCalcMgt.SetUoM(Qty, 1);

        // Cherche les prix
        /*TODO SalesPriceCalcMgt.FindSalesPrice(TempSalesPrice, CustNo, ContNo, CustPriceGrCode,
            // AD Le 04-11-2011 => Ajout du type de commande
            '', ItemNo, VariantCode, UOM, CurrencyCode, StartingDate, ShowAll, SousGroupeNo, CentraleNo, _pTypeCommande + 1);*/

        // Cherche le meilleur prix
        SalesPriceCalcMgt.CalcBestUnitPrice(TempSalesPrice);


        /*TODO IF SalesPriceCalcMgt.FoundSalesPrice THEN BEGIN
            // Si c'est un prix net, on met la remise … 0
            IF NOT TempSalesPrice."Allow Line Disc." THEN
                "Discount %" := 0;
            "Unit Price" := TempSalesPrice."Unit Price";
            EXIT(TRUE);
        END
        ELSE
            EXIT(FALSE);*/
    END;

    PROCEDURE GetSalesDisc(VAR TempSalesLineDisc: Record "Sales Line Discount"; CustNo: Code[20]; ContNo: Code[20]; CustDiscGrCode: Code[20]; CampaignNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; Qty: Decimal; TypeRemise: Integer; SousGroupeNo: Code[20]; CentraleNo: Code[20]; TypeCommande: Option Dépannage,Réappro,Stock): Decimal;
    VAR
        ItemUoM: Record "Item Unit of Measure";
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        "Discount %": Decimal;
    BEGIN
        // MC Le 20-04-2011 => SYMTA -> Gestions des remises ventes.
        //  --> Rajout des trois derniers paramŠtres de la fonction.

        // Cette fonction retoune la remise apllicable pour un article … un client.

        // Charge l'article
        IF NOT Item.GET(ItemNo) THEN
            EXIT(0);

        // Charge les qty
        IF UOM <> '' THEN BEGIN
            ItemUoM.GET(ItemNo, UOM);
            SalesPriceCalcMgt.SetUoM(Qty, ItemUoM."Qty. per Unit of Measure");
        END
        ELSE
            SalesPriceCalcMgt.SetUoM(Qty, 1);

        // Charge les remises
        /*TODO SalesPriceCalcMgt.FindSalesLineDisc(TempSalesLineDisc, CustNo, ContNo, CustDiscGrCode,
            // MC Le 20-04-2011 => Rajout de deux paramŠtres.
            //'', ItemNo , Item."Item Disc. Group", VariantCode , UOM, CurrencyCode, StartingDate, ShowAll);
            '', ItemNo, Item."Item Disc. Group", VariantCode, UOM, CurrencyCode, StartingDate, ShowAll, SousGroupeNo, CentraleNo,
            TypeCommande);
            */
        // FIN MC Le 20-04-2011.
        // Cherche la meilleure remise
        SalesPriceCalcMgt.CalcBestLineDisc(TempSalesLineDisc);

        // AD Le 15-01-2008 => MIG V5 -> Ajout du TypeRemise
        CASE TypeRemise OF
            0:
                "Discount %" := TempSalesLineDisc."Line Discount %";
            1:
                "Discount %" := TempSalesLineDisc."Line Discount 1 %";
            2:
                "Discount %" := TempSalesLineDisc."Line Discount 2 %";
        END;

        EXIT("Discount %");
    END;

    PROCEDURE GetBestSalesDiscForAll(ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date): Decimal;
    var
        TempSalesLineDisc: Record "Sales Line Discount" TEMPORARY;
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        RechercheMeilleureRemise: Boolean;
    BEGIN
        // Charge les remises

        RechercheMeilleureRemise := TRUE;
        /*TODO SalesPriceCalcMgt.FindSalesLineDisc(TempSalesLineDisc, '', '', '',
            '', ItemNo, Item."Item Disc. Group", VariantCode, UOM, CurrencyCode, StartingDate, FALSE, '', '', 0);*/
        RechercheMeilleureRemise := FALSE;

        TempSalesLineDisc.RESET();

        // Cherche la meilleure remise
        TempSalesLineDisc.FINDFIRST();
        REPEAT
        UNTIL TempSalesLineDisc.NEXT() = 0;

        TempSalesLineDisc.SETFILTER("Line Discount %", '>%1', 0);

        SalesPriceCalcMgt.CalcBestLineDisc(TempSalesLineDisc);

        //MESSAGE('%1  %2  %3  %4  %5  %6', TempSalesLineDisc."Sales Type", TempSalesLineDisc."Sales Code",
        //TempSalesLineDisc."Code Marque", TempSalesLineDisc."Code Famille", TempSalesLineDisc."Code Sous Famille"
        //,TempSalesLineDisc."Line Discount %");

        EXIT(TempSalesLineDisc."Line Discount %");
    END;
    //<< Mig CU 7000
    //>> Mig CU 7010
    PROCEDURE FindPurchPriceWithNull(VAR ToPurchPrice: Record "Purchase Price"; VendorNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean);
    VAR
        FromPurchPrice: Record "Purchase Price";
    BEGIN
        //CFR Le 23/03/2022 => R‚gie : Recherce des tarifs avec cout null pour Export des donn‚es articles (Report 50048)
        WITH FromPurchPrice DO BEGIN
            SETRANGE("Item No.", ItemNo);
            SETRANGE("Vendor No.", VendorNo);
            SETFILTER("Ending Date", '%1|>=%2', 0D, StartingDate);
            SETFILTER("Variant Code", '%1|%2', VariantCode, '');
            IF NOT ShowAll THEN BEGIN
                SETRANGE("Starting Date", 0D, StartingDate);
                SETFILTER("Currency Code", '%1|%2', CurrencyCode, '');
                SETFILTER("Unit of Measure Code", '%1|%2', UOM, '');
            END;

            ToPurchPrice.RESET();
            ToPurchPrice.DELETEALL();
            IF FIND('-') THEN
                REPEAT
                    ToPurchPrice := FromPurchPrice;
                    ToPurchPrice.Insert();
                UNTIL NEXT() = 0;
        END;
    END;

    procedure PurchHeaderStartDate(var PurchHeader: Record "Purchase Header"; var DateCaption: Text[30]) StartDate: Date
    begin
        with PurchHeader do
            if "Document Type" in ["Document Type"::Invoice, "Document Type"::"Credit Memo"] then begin
                DateCaption := FieldCaption("Posting Date");
                exit("Posting Date")
            end else begin
                DateCaption := FieldCaption("Order Date");
                exit("Order Date");
            end;
    end;

    PROCEDURE GetUnitCost(VAR TempPurchPrice: Record "Purchase Price"; VendorNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; Qty: Decimal; LocationCode: Code[10]; VAR "Direct Unit Cost": Decimal): Boolean;
    VAR
        ItemUoM: Record "Item Unit of Measure";
        SKU: Record "Stockkeeping Unit";
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
    BEGIN
        // Cette fonction retoune Vrai si il existe des tarif et faut si il en existe pas.
        // Elle renvoi en paramŠtre le prix et la remise (la remise … 0 si le prix est net)

        // Charge l'article
        IF NOT Item.GET(ItemNo) THEN
            EXIT(FALSE);

        // Charge le fournisseur
        //TODO Vend.GET(VendorNo);

        // Charge les qtes
        IF UOM <> '' THEN BEGIN
            ItemUoM.GET(ItemNo, UOM);
            PurchPriceCalcMgt.SetUoM(Qty, ItemUoM."Qty. per Unit of Measure");
        END
        ELSE
            PurchPriceCalcMgt.SetUoM(Qty, 1);

        //TODO PriceInSKU := SKU.GET(LocationCode, ItemNo, VariantCode);

        // Cherche les prix
        PurchPriceCalcMgt.FindPurchPrice(TempPurchPrice, VendorNo,
            ItemNo, VariantCode, UOM, CurrencyCode, StartingDate, ShowAll);

        // Cherche le meilleur prix
        PurchPriceCalcMgt.CalcBestDirectUnitCost(TempPurchPrice);

        /*TODO IF FoundPurchPrice THEN BEGIN
            "Direct Unit Cost" := TempPurchPrice."Direct Unit Cost";
            EXIT(TRUE);
        END
        ELSE
            EXIT(FALSE);*/
    END;

    PROCEDURE GetPurchDisc(VAR TempPurchLineDisc: Record "Purchase Line Discount"; VendorNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean; Qty: Decimal; TypeCommande: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3"; GroupeRemise: Code[10]; TypeRemise: Integer): Decimal;
    VAR
        ItemUoM: Record "Item Unit of Measure";
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        "Discount %": Decimal;
    BEGIN
        // Cette fonction retoune la remise apllicable pour un article … un client.

        // Charge l'article
        IF NOT Item.GET(ItemNo) THEN
            EXIT(0);

        // Charge les qty
        IF UOM <> '' THEN BEGIN
            ItemUoM.GET(ItemNo, UOM);
            PurchPriceCalcMgt.SetUoM(Qty, ItemUoM."Qty. per Unit of Measure");
        END
        ELSE
            PurchPriceCalcMgt.SetUoM(Qty, 1);

        // Charge les remises
        /*TODO PurchPriceCalcMgt.FindPurchLineDisc(TempPurchLineDisc, VendorNo,
            ItemNo, VariantCode, UOM, CurrencyCode, StartingDate, ShowAll, TypeCommande, GroupeRemise);*/

        // Cherche la meilleure remise
        PurchPriceCalcMgt.CalcBestLineDisc(TempPurchLineDisc);

        //"Discount %" := TempPurchLineDisc."Line Discount %";

        CASE TypeRemise OF
            0:
                "Discount %" := TempPurchLineDisc."Line Discount %";
            1:
                "Discount %" := TempPurchLineDisc."Line Discount 1 %";
            2:
                "Discount %" := TempPurchLineDisc."Line Discount 2 %";
        END;


        EXIT("Discount %");
    END;

    PROCEDURE NoOfPurchPriceByItemVendor(VendorNo: Code[20]; ItemNo: Code[20]; VariantCode: Code[10]; UOM: Code[10]; CurrencyCode: Code[10]; StartingDate: Date; ShowAll: Boolean) rtn_dec: Decimal;
    var
        TempPurchPrice: Record "Purchase Price" temporary;
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
    BEGIN
        // Cherche les prix
        PurchPriceCalcMgt.FindPurchPrice(TempPurchPrice, VendorNo,
            ItemNo, VariantCode, UOM, CurrencyCode, StartingDate, ShowAll);

        EXIT(TempPurchPrice.COUNT);
    END;
    //<< Mig CU 7010
    //>> Mig CU 5760
    PROCEDURE DeleteWarehouseDocument(WhseReceiptLine: Record "Warehouse Receipt Line");
    VAR
        WhseReceiptLine2: Record "Warehouse Receipt Line";
        WhseReceiptHeader: Record "Warehouse Receipt Header";
    BEGIN
        // AD Le 24-05-2007 => Supprssion syst‚matique de l'entete pour repasser dans le cycle normal de selection
        WhseReceiptLine2.RESET();
        WhseReceiptLine2.SETRANGE("No.", WhseReceiptLine."No.");
        IF WhseReceiptLine2.FINDFIRST() THEN
            WhseReceiptLine2.DELETEALL();

        WhseReceiptHeader.RESET();
        IF WhseReceiptHeader.GET(WhseReceiptLine."No.") THEN
            WhseReceiptHeader.DELETE();
    END;

    PROCEDURE InitSourceDocumentLinesCout(VAR WhseRcptLine: Record "Warehouse Receipt Line"; LMtReception: Decimal);
    VAR
        WhseRcptLine2: Record "Warehouse Receipt Line";
        TransLine: Record "Transfer Line";
        SalesLine: Record "Sales Line";
        PurchLine: Record "Purchase Line";
        WhseRcptHeader: Record "Warehouse Receipt Header";
        ModifyLine: Boolean;
    BEGIN
        if WhseRcptHeader.GET(WhseRcptLine."No.") then;
        PurchLine.RESET();
        WhseRcptLine2.COPY(WhseRcptLine);
        WITH WhseRcptLine2 DO BEGIN
            CASE "Source Type" OF
                DATABASE::"Purchase Line":
                    BEGIN
                        PurchLine.SETRANGE("Document Type", "Source Subtype");
                        PurchLine.SETRANGE("Document No.", "Source No.");
                        IF PurchLine.FIND('-') THEN
                            REPEAT
                                SETRANGE("Source Line No.", PurchLine."Line No.");
                                IF FIND('-') THEN BEGIN
                                    IF "Source Document" = "Source Document"::"Purchase Order" THEN BEGIN
                                        ModifyLine := TRUE;

                                        IF ModifyLine THEN BEGIN
                                            PurchLine.SuspendStatusCheck(TRUE);
                                            PurchLine.VALIDATE("% Facture HK", WhseRcptHeader."% Facture HK");
                                            PurchLine.VALIDATE("% Assurance", WhseRcptHeader."% Assurance");
                                            PurchLine.VALIDATE("% Coef Réception", WhseRcptHeader."% Coef Réception");
                                            IF "Qty. to Receive" <> 0 THEN BEGIN
                                                PurchLine.VALIDATE("Frais LCY", (WhseRcptHeader."Frais Port France (DS)" *
                                                            ("Qty. to Receive" * PurchLine."Direct Unit Cost"
                                                                                  / LMtReception)) / "Qty. to Receive");
                                                PurchLine.VALIDATE("Frais FCY", (WhseRcptHeader."Frais Port Maritime (DC)" *
                                                            ("Qty. to Receive" * PurchLine."Direct Unit Cost"
                                                                                  / LMtReception)) / "Qty. to Receive");
                                                //ERROR('ad %1', PurchLine."Overhead Rate");
                                            END;
                                        END;
                                    END;
                                END;
                                IF ModifyLine THEN
                                    PurchLine.MODIFY();
                            UNTIL PurchLine.NEXT() = 0;
                    END;
            END;
            SETRANGE("Source Line No.");
        END;
    END;

    PROCEDURE CalcMtReception(WhseRcptLine: Record "Warehouse Receipt Line"): Decimal;
    VAR
        WhseRcptLine2: Record "Warehouse Receipt Line";
        PurchLine: Record "Purchase Line";
        LMtReception: Decimal;
    BEGIN
        // CALCUL DU MT DE LA RECEPTION
        // ----------------------------
        LMtReception := 0;
        WhseRcptLine2.SETRANGE("No.", WhseRcptLine."No.");
        WITH WhseRcptLine2 DO BEGIN
            IF FINDFIRST() THEN
                REPEAT
                    CASE "Source Type" OF
                        DATABASE::"Purchase Line":
                            BEGIN
                                PurchLine.GET(PurchLine."Document Type"::Order, "Source No.", "Source Line No.");
                                LMtReception += "Qty. to Receive" * PurchLine."Direct Unit Cost";
                            END;
                    END;
                UNTIL NEXT() = 0;
        END;

        EXIT(LMtReception);
    END;
    //<< Mig CU 5760
    //>> Mig CU 5763
    PROCEDURE DeleteWarehouseDocument(WhseShptLine: Record "Warehouse Shipment Line");
    VAR
        WhseShptLine2: Record "Warehouse Shipment Line";
        WhseShptHeader: Record "Warehouse Shipment Header";
    BEGIN
        //IF WORKDATE = 101115D THEN EXIT;
        // AD Le 24-05-2007 => Supprssion syst‚matique de l'entete pour repasser dans le cycle normal de selection
        WhseShptLine2.RESET();
        WhseShptLine2.SETRANGE("No.", WhseShptLine."No.");
        IF WhseShptLine2.FINDFIRST() THEN
            WhseShptLine2.DELETEALL();

        WhseShptHeader.RESET();
        IF WhseShptHeader.GET(WhseShptLine."No.") THEN
            WhseShptHeader.DELETE();
    END;

    PROCEDURE AbandonmentOfRemainder(WhseShptLine2: Record "Warehouse Shipment Line");
    VAR
        WhseShptLine3: Record "Warehouse Shipment Line";
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        assocLine: Record "Sales Line";
    BEGIN
        // fonction pour sidamo abandon de reliquat
        // fonction parti du principe qu'il peut avoir plusieur commande dans l'exp‚dition (au cas ou)
        //AbandonmentOfRemainder(WhseItemLine : Record "Warehouse Shipment Line")

        // MCO Le 23-03-2016 => Ne fonctionne plus avec ordre d'assemblages
        EXIT;
        // FIN MCO Le 23-03-2016


        IF WhseShptLine2."Source Document" <> WhseShptLine2."Source Document"::"Sales Order" THEN
            EXIT;

        WhseShptLine3.RESET();
        WhseShptLine3.SETFILTER("No.", WhseShptLine2."No.");
        IF WhseShptLine3.FIND('-') THEN
            REPEAT
                IF (WhseShptLine3.Quantity <> WhseShptLine3."Qty. to Ship") THEN BEGIN
                    SalesHeader.GET(WhseShptLine3."Source Document", WhseShptLine3."Source No.");
                    IF SalesHeader."Abandon remainder" THEN BEGIN
                        SalesLine.GET(SalesHeader."Document Type", SalesHeader."No.", WhseShptLine3."Source Line No.");
                        SalesLine.SuspendStatusCheck(TRUE);
                        SalesLine.ShortClose();
                        SalesLine.SetHideValidationDialog(TRUE);
                        SalesLine."Initial Quantity" := SalesLine.Quantity;
                        SalesLine.VALIDATE(Quantity, WhseShptLine3."Qty. to Ship");
                        SalesLine.VALIDATE("Quantity (Base)", WhseShptLine3."Qty. to Ship (Base)");
                        SalesLine.VALIDATE("Qty. to Ship", 0);

                        IF WhseShptLine3."Qty. to Ship" = 0 THEN BEGIN
                            SalesLine.VALIDATE("Unit Price", 0);
                            SalesLine.VALIDATE(Amount, 0);
                        END;
                        SalesLine."Completely Shipped" := TRUE;
                        SalesLine.SuspendStatusCheck(FALSE);
                        SalesLine.SetHideValidationDialog(FALSE);
                        SalesLine.MODIFY();

                        //WhseItemRemLine.VALIDATE(Quantity,WhseItemRemLine."Qty. to Ship");
                        //WhseShptLine3.MODIFY();


                        // AD Le 11-12-2006 => Abandon de reliquat des frais associ‚s.
                        assocLine.RESET();
                        assocLine.SETRANGE("Document Type", SalesHeader."Document Type");
                        assocLine.SETRANGE("Document No.", SalesHeader."No.");
                        assocLine.SETRANGE("Attached to Line No.", WhseShptLine3."Source Line No.");
                        // AD Le 30-11-2009 => DEEE
                        assocLine.SETRANGE(Type, assocLine.Type::"Charge (Item)");
                        // FIN AD Le 30-11-2009

                        IF assocLine.FIND('-') THEN
                            REPEAT
                                assocLine.SuspendStatusCheck(TRUE);
                                assocLine.ShortClose();
                                assocLine."Initial Quantity" := assocLine.Quantity;
                                assocLine.VALIDATE(Quantity, WhseShptLine3."Qty. to Ship");
                                assocLine.VALIDATE("Quantity (Base)", WhseShptLine3."Qty. to Ship (Base)");
                                assocLine.VALIDATE("Qty. to Ship", 0);
                                IF WhseShptLine3."Qty. to Ship" = 0 THEN BEGIN
                                    assocLine.VALIDATE("Unit Price", 0);
                                    assocLine.VALIDATE(Amount, 0);
                                END;
                                assocLine."Completely Shipped" := TRUE;
                                assocLine.SuspendStatusCheck(FALSE);
                                assocLine.MODIFY();

                            UNTIL assocLine.NEXT() = 0;
                        // FIN AD Le 11-12-2006

                        // AD Le 30-11-2009 => DEEE Passage en resource
                        assocLine.RESET();
                        assocLine.SETRANGE("Document Type", SalesHeader."Document Type");
                        assocLine.SETRANGE("Document No.", SalesHeader."No.");
                        assocLine.SETRANGE("Attached to Line No.", WhseShptLine3."Source Line No.");
                        // AD Le 30-11-2009 => DEEE
                        assocLine.SETRANGE(Type, assocLine.Type::Resource);
                        // FIN AD Le 30-11-2009

                        IF assocLine.FIND('-') THEN
                            REPEAT
                                assocLine.SuspendStatusCheck(TRUE);
                                assocLine.ShortClose();
                                assocLine."Initial Quantity" := assocLine.Quantity;
                                assocLine.VALIDATE(Quantity, WhseShptLine3."Qty. to Ship");
                                assocLine.VALIDATE("Quantity (Base)", WhseShptLine3."Qty. to Ship (Base)");
                                assocLine.VALIDATE("Qty. to Ship", 0);
                                IF WhseShptLine3."Qty. to Ship" = 0 THEN BEGIN
                                    assocLine.VALIDATE("Unit Price", 0);
                                    assocLine.VALIDATE(Amount, 0);
                                END;
                                assocLine."Completely Shipped" := TRUE;
                                assocLine.SuspendStatusCheck(FALSE);
                                assocLine.MODIFY();

                            UNTIL assocLine.NEXT() = 0;



                        //SalesHeader.Status:=SalesHeader.Status::Preparation;
                        SalesHeader.MODIFY();

                    END
                END;
            UNTIL WhseShptLine3.NEXT() = 0
    END;

    procedure MakePreliminaryChecks(WhseShptHeader: Record "Warehouse Shipment Header")
    var
        GenJnlCheckLine: Codeunit "Gen. Jnl.-Check Line";
        Text007Err: Label 'is not within your range of allowed posting dates';
    begin
        with WhseShptHeader do begin
            if GenJnlCheckLine.DateNotAllowed("Posting Date") then
                FieldError("Posting Date", Text007Err);
        end;
    end;
    //<< Mig CU 5763
    //>> Mig CU 99000830 & 99000832
    PROCEDURE ActivErrorOnReservation(): Boolean;
    VAR
        SYMTAErr: Label 'Erreur de reservation. Contacter NATH.';
    BEGIN
        // MCO Le 06-07-2018 => Ticket 37625
        ERROR(SYMTAErr);// Mettre en commentaire pour d‚sactiver le contr“le
    END;
    //>> Mig CU 99000830 & 99000832
    //>>Mig CU5702
    procedure FillDescription(var PurchaseLine: Record "Purchase Line")
    var
        GlobalItemVariant: Record "Item Variant";
        GlobalItem: Record Item;
    begin
        if PurchaseLine."Variant Code" <> '' then begin
            GlobalItemVariant.Get(PurchaseLine."No.", PurchaseLine."Variant Code");
            PurchaseLine.Description := GlobalItemVariant.Description;
            PurchaseLine."Description 2" := GlobalItemVariant."Description 2";
        end else begin
            GlobalItem.Get(PurchaseLine."No.");
            PurchaseLine.Description := GlobalItem.Description;
            PurchaseLine."Description 2" := GlobalItem."Description 2";
        end;
    end;
    //<<Mig CU5702
}