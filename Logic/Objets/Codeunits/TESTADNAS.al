codeunit 50028 "TEST AD NAS"
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    var
        t: Record "Generals Parameters";
    begin
        IF NOT t.GET('NAS', 'NAS') THEN BEGIN
            t.VALIDATE(Type, 'NAS');
            t.VALIDATE(Code, 'NAS');
            t.Insert();
        END;

        t.LongDescription := FORMAT(CURRENTDATETIME);
        t.MODIFY();
    end;
}

