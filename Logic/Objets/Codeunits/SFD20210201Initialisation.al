codeunit 50074 "SFD20210201 - Initialisation"
{
    trigger OnRun()
    begin
        //MESSAGE('Pas de mise à jour');
        //Initialise_PurchaseCentralHistory();
        //Rest_ItemStockMort();
        Initialise_PurchaseCentralHistory2();
    end;

    local procedure Initialise_PurchaseCentralHistory()
    var
        lCustomer: Record Customer;
        lPurchaseCentralHistory: Record "Purchase Central History";
        lCompteur: Integer;
    begin
        IF NOT CONFIRM('Voulez vous VRAIMENT lancer le traitement ?\ > Cela va supprimer la table [Historique centrale d''achat]') THEN BEGIN
            MESSAGE('Traitement interrompu');
            EXIT;
        END;

        lPurchaseCentralHistory.DELETEALL(FALSE);

        lCustomer.SETFILTER("Centrale Active", '<>%1', '');
        IF lCustomer.FINDSET() THEN
            REPEAT
                lPurchaseCentralHistory.VALIDATE("Customer No.", lCustomer."No.");
                lPurchaseCentralHistory.VALIDATE("Active Central", lCustomer."Centrale Active");
                lPurchaseCentralHistory.VALIDATE("Groupement payeur", lCustomer."Groupement payeur");
                lPurchaseCentralHistory.VALIDATE("Libre 3", lCustomer."Libre 3");
                IF lPurchaseCentralHistory.INSERT(TRUE) THEN
                    lCompteur += 1;
            UNTIL lCustomer.NEXT() = 0;

        IF lPurchaseCentralHistory.FINDSET() THEN;
        MESSAGE('Clients : %1 - Compteur : %2 - Insertion : %3', lCustomer.COUNT(), lCompteur, lPurchaseCentralHistory.COUNT());
    end;

    local procedure Initialise_PurchaseCentralHistory2()
    var
        lCustomer: Record Customer;
        lPurchaseCentralHistory: Record "Purchase Central History";
        lCompteur: Integer;
    begin

        lPurchaseCentralHistory.SETRANGE("Starting Date", 0D, TODAY);
        lPurchaseCentralHistory.SETFILTER("Ending Date", '>=%1', TODAY);
        lPurchaseCentralHistory.SETFILTER("Groupement payeur", '<>%1', TRUE);
        lPurchaseCentralHistory.SETFILTER("Libre 3", '<>%1', '');
        IF lPurchaseCentralHistory.FINDSET() THEN
            ERROR('Initialisation déjà effectuée.\Traitement interrompu.');

        lPurchaseCentralHistory.SETRANGE("Groupement payeur");
        lPurchaseCentralHistory.SETRANGE("Libre 3");
        IF lPurchaseCentralHistory.FINDSET() THEN
            REPEAT
                IF lCustomer.GET(lPurchaseCentralHistory."Customer No.") THEN BEGIN
                    lPurchaseCentralHistory."Groupement payeur" := lCustomer."Groupement payeur";
                    lPurchaseCentralHistory."Libre 3" := lCustomer."Libre 3";
                    lPurchaseCentralHistory.MODIFY(TRUE);
                    lCompteur += 1;
                END;
            UNTIL lPurchaseCentralHistory.NEXT() = 0;

        lPurchaseCentralHistory.FINDSET();
        MESSAGE('Histo : %1 - Compteur : %2', lPurchaseCentralHistory.COUNT(), lCompteur);
    end;

    local procedure Rest_ItemStockMort()
    var
        lItem: Record Item;
        lCompteur: Integer;
    begin
        lItem.SETRANGE("Stock Mort", lItem."Stock Mort"::"Plus fourni");
        IF lItem.FINDSET() THEN
            REPEAT
                lItem."Stock Mort" := lItem."Stock Mort"::Non;
                IF lItem.MODIFY(TRUE) THEN
                    lCompteur += 1;
            UNTIL lItem.NEXT() = 0;
        MESSAGE('Articles : %1', lCompteur);
    end;
}

