codeunit 50057 "Mail Reliquat"
{

    trigger OnRun()
    begin
        MailReliquat('CV-16004126');
    end;

    var
        LigneTableauTxt: Label '<TR><TD>%1</TD><TD>%2</TD><TD>%3</TD><TD>%4</TD><TD>%5</TD><TD>%6</TD><TD>%7</TD><TD>%8</TD><TD>%9</TD><TD>%10</TD><TD>%11</TD><TD>%12</TD></TR>';
                LigneEnteteTxt: Label '<B>%1 : </B>%2<BR>';
        ESK001Err: Label 'Aucune ligne de reliquat pour cette commande !';
        LigneTableauDevisTxt: Label '<TR><TD>%1</TD><TD>%2</TD><TD>%3</TD><TD>%4</TD><TD>%5</TD><TD>%6</TD><TD>%7</TD><TD>%8</TD><TD>%9</TD><TD>%10</TD><TD>%11</TD></TR>';
        ESK002Err: Label 'Aucune ligne concernée pour ce devis !';

    procedure MailReliquat(_pOrderNo: Code[20])
    var
        
        LSalesHeader: Record "Sales Header";
         LItem: Record Item;
        LSalesLine: Record "Sales Line";
        LMail: Codeunit Mail;
        LSujet: Text;
        LBody: Text;     
       
        LUrgent: Boolean;
    begin

        LSalesHeader.GET(LSalesHeader."Document Type"::Order, _pOrderNo);
        CLEAR(LSalesLine);
        LSalesLine.SETRANGE("Document Type", LSalesHeader."Document Type");
        LSalesLine.SETRANGE("Document No.", LSalesHeader."No.");
        LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
        LSalesLine.SETFILTER("Outstanding Quantity", '<>%1', 0);
        IF NOT LSalesLine.FINDFIRST() THEN ERROR(ESK001Err);

        // On demande si urgent
        LUrgent := CONFIRM('Urgent', FALSE);


        // Construction du sujet
        LSujet := '';
        IF LUrgent THEN
            LSujet := '[URGENT] ';
        LSujet += FORMAT(LSalesHeader."Document Type") + ' ' + LSalesHeader."No." + ' ' + LSalesHeader."Ship-to Name";

        // Initialisation du message
        LMail.CreateMessage('', '', '', LSujet, '', TRUE, TRUE);

        // Construction de l'entête du message
        IF LUrgent THEN BEGIN
            LBody := STRSUBSTNO(LigneEnteteTxt, '<FONT COLOR="red" size=10>URGENT</FONT>', '');
            LMail.AddBodyline(LBody);

        END;

        LBody := STRSUBSTNO(LigneEnteteTxt, 'Commande', LSalesHeader."No.");
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Client', LSalesHeader."Sell-to Customer No." + '-' + LSalesHeader."Ship-to Name");
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Date Commande', FORMAT(LSalesHeader."Order Date"));
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Utilisateur', LSalesHeader."Order Create User");
        LMail.AddBodyline(LBody);


        // Entête du tableau
        LBody := '<BR><BR>';
        LMail.AddBodyline(LBody);

        LBody := '<TABLE style="border:1px solid" border=1 cellspacing=3 cellpadding=3>' +
                    '<TR><TD><B>No Cde</TD><TD><B>No Ligne Cde</TD>' +
                    '<TD><B>Réf. SP</TD><TD><B>Référence</TD><TD><B>Désignation</TD><TD><B>Quantité Restante</TD><TD><B>Date demandée</TD>' +
                    '<TD><B>Prix Unitaire</TD><TD><B>Remises</TD><TD><B>Prix Unitaire Net</TD><TD><B>Appro</TD><TD><B>Stocké</TD></TR>';
        LMail.AddBodyline(LBody);


        // Corp du tableau
        REPEAT
            IF (LSalesLine.Quantity - LSalesLine."Qty. to Ship" - LSalesLine."Quantity Shipped" <> 0) THEN BEGIN
                LItem.GET(LSalesLine."No.");
                LBody := STRSUBSTNO(LigneTableauTxt, LSalesLine."Document No.", LSalesLine."Line No." / 10000,
                          LItem."No.", LItem."No. 2", LSalesLine.Description,
                         LSalesLine.Quantity - LSalesLine."Qty. to Ship" - LSalesLine."Quantity Shipped",
                         LSalesLine."Requested Delivery Date", LSalesLine."Unit Price",
                        FORMAT(LSalesLine."Discount1 %") + '% - ' + FORMAT(LSalesLine."Discount2 %") + '%',
                   LSalesLine."Net Unit Price",
                    LItem."Code Appro", LItem.Stocké);
                LMail.AddBodyline(LBody);
            END;
        UNTIL LSalesLine.NEXT() = 0;

        // Pied du tableau
        LBody := '</TABLE>';
        LMail.AddBodyline(LBody);

        //Ouverture du mail
        LMail.Send();
    END;

    PROCEDURE MailDemandeDelai(pQuoteNo: Code[20]);
    VAR
        LSalesHeader: Record 36;        
        LItem: Record 27;
        LSalesLine: Record 37;
        LMail: Codeunit 397;
        LSujet: Text;
        LBody: Text;
        
    BEGIN

        LSalesHeader.GET(LSalesHeader."Document Type"::Quote, pQuoteNo);
        CLEAR(LSalesLine);
        LSalesLine.SETRANGE("Document Type", LSalesHeader."Document Type");
        LSalesLine.SETRANGE("Document No.", LSalesHeader."No.");
        LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
        IF LSalesLine.FINDSET() THEN
            REPEAT
                IF (LSalesLine."Qty. to Ship" <> LSalesLine.Quantity) THEN
                    LSalesLine.MARK(TRUE);
            UNTIL LSalesLine.NEXT() = 0;

        LSalesLine.MARKEDONLY(TRUE);
        IF NOT LSalesLine.FINDFIRST() THEN ERROR(ESK002Err);


        // Construction du sujet
        LSujet := '';
        LSujet += 'Demande de délai ' + LSalesHeader."No." + ' ' + LSalesHeader."Ship-to Name";

        // Initialisation du message
        LMail.CreateMessage('', '', '', LSujet, '', TRUE, TRUE);

        // Construction de l'entˆte du message
        LBody := STRSUBSTNO(LigneEnteteTxt, '<FONT COLOR="blue" size=4>MERCI DE ME RENSEIGNER DELAI POUR DEVIS</FONT>', '');
        LMail.AddBodyline(LBody);

        LBody := STRSUBSTNO(LigneEnteteTxt, 'Devis', LSalesHeader."No.");
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Client', LSalesHeader."Sell-to Customer No." + '-' + LSalesHeader."Ship-to Name");
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Date devis', FORMAT(LSalesHeader."Order Date"));
        LMail.AddBodyline(LBody);
        LBody := STRSUBSTNO(LigneEnteteTxt, 'Utilisateur', LSalesHeader."Order Create User");
        LMail.AddBodyline(LBody);


        // Entˆte du tableau
        LBody := '<BR><BR>';
        LMail.AddBodyline(LBody);

        LBody := '<TABLE style="border:1px solid" border=1 cellspacing=3 cellpadding=3>' +
                    '<TR><TD><B>No devis</TD><TD><B>No Ligne devis</TD>' +
                    '<TD><B>Réf. SP</TD><TD><B>R‚f‚rence</TD><TD><B>Désignation</TD><TD><B>Quantité Restante</TD>' +
                    '<TD><B>Prix Unitaire</TD><TD><B>Remises</TD><TD><B>Prix Unitaire Net</TD><TD><B>Appro</TD><TD><B>Stocké</TD></TR>';
        LMail.AddBodyline(LBody);


        // Corp du tableau
        REPEAT
            IF (LSalesLine.Quantity - LSalesLine."Qty. to Ship" - LSalesLine."Quantity Shipped" <> 0) THEN BEGIN
                LItem.GET(LSalesLine."No.");
                LBody := STRSUBSTNO(LigneTableauDevisTxt, LSalesLine."Document No.", LSalesLine."Line No." / 10000,
                          LItem."No.", LItem."No. 2", LSalesLine.Description,
                         LSalesLine.Quantity - LSalesLine."Qty. to Ship" - LSalesLine."Quantity Shipped",
                         LSalesLine."Unit Price",
                         FORMAT(LSalesLine."Discount1 %") + '% - ' + FORMAT(LSalesLine."Discount2 %") + '%',
                         LSalesLine."Net Unit Price",
                          LItem."Code Appro", LItem.Stocké);
                LMail.AddBodyline(LBody);
            END;
        UNTIL LSalesLine.NEXT() = 0;

        // Pied du tableau
        LBody := '</TABLE>';
        LMail.AddBodyline(LBody);

        //Ouverture du mail
        LMail.Send();
    END;
}

