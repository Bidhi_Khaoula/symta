codeunit 50059 "Calcul Code Appro"
{

    trigger OnRun()
    begin
        Code();
    end;

    var
        HideValidationDIalog: Boolean;

    local procedure "Code"()
    var
        lItem: Record Item;
        lVendor: Record Vendor;
        lFrmBufferPrice: Page "Vendor Price Buffer";
        lVendorNo: Code[20];
        lCpt: Integer;
        lCount: Integer;
        lStartTime: DateTime;
        lEndTime: DateTime;
        lUpd: Integer;
    begin
        CLEAR(lItem);
        //lItem.SETFILTER(lItem."No. 2", '983843*');
        //lItem.SETRANGE("Code Appro", 'N');

        lCpt := 0;
        lCount := lItem.COUNT;
        lUpd := 0;
        lStartTime := CURRENTDATETIME;
        LogSetup('TSTART', FORMAT(lStartTime));
        // on parcours tous les articles
        IF lItem.FINDFIRST() THEN
            REPEAT
                lCpt += 1;
                // Utilisation de la page Vendor Price Buffer pour calculer le meilleur fournisseur
                CLEAR(lFrmBufferPrice);
                lFrmBufferPrice.HideSelection(TRUE);
                // Recherche des fournisseurs avec un calcul de prix pour 99999 quantité et pour les commandes de niveau 3
                lFrmBufferPrice.InsertBufferRecordByMulti(lItem."No.", 99999, '', 3);
                // Lecture du premier vendeur trouvé (le meilleur)
                lVendorNo := lFrmBufferPrice.GetFirstVendorNo();
                // on copie le code appro du fournisseur vers l'article
                IF lVendor.GET(lVendorNo) AND (lVendor."Code Appro" <> lItem."Code Appro") THEN BEGIN
                    lItem."Code Appro" := lVendor."Code Appro";
                    lItem.MODIFY(FALSE);
                    lUpd += 1;
                END;
                LogSetup('COUNT', STRSUBSTNO('%1 / %2', lCpt, lCount));
            UNTIL lItem.NEXT() = 0;
        lEndTime := CURRENTDATETIME;
        LogSetup('TEND', FORMAT(lEndTime));
        LogSetup('TOTAL', FORMAT(lUpd));
        LogSetup('COUNT', STRSUBSTNO('%1 / %2', lCpt, lCount));
        IF GUIALLOWED THEN
            // MCO Le 09-02-2017 => Régie
            IF NOT HideValidationDIalog THEN
                // FIN MCO Le 09-02-2017
                MESSAGE('Terminé %1 - %2', lStartTime, lEndTime);
    end;

    local procedure LogSetup(pCode: Code[20]; pValue: Text[20])
    var
        lSetup: Record "Generals Parameters";
    begin
        IF lSetup.GET('UPD_ART_CODE_APPRO', pCode) THEN BEGIN
            lSetup.ShortDescription := pValue;
            lSetup.MODIFY();
        END;
    end;

    procedure SetHideValidationDialog(NewHideValidationDialog: Boolean)
    begin
        // MCO Le 09-02-2017 => Régie
        HideValidationDIalog := NewHideValidationDialog;
    end;
}

