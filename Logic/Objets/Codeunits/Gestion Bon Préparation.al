codeunit 50013 "Gestion Bon Préparation"
{
    trigger OnRun()
    
    begin
        /* {
             lWarehouseShipmentHeader.SETRANGE("No.", 'EX23000025');
         IF lWarehouseShipmentHeader.FINDSET() THEN
             ImprimeEtiquettePrepa(lWarehouseShipmentHeader, 0);
             }*/
    end;

    var
      GestionMultiRef: Codeunit "Gestion Multi-référence";
        ESK001Msg: Label 'Aucune ligne à exporter vers les MODULA !';
     
        Number: Integer;
        _Number: Text[30];

    procedure ExportBlsModula(var WhseShipHeader: Record "Warehouse Shipment Header")
    var
        WhseShipLine: Record "Warehouse Shipment Line";
            Param: Record "Generals Parameters";
        LigneAExporter: Boolean;
         FichierOuvert: Boolean;
        Ligne: Text[1024];
        FichierModula: File;
    begin
        // Exportation d'une liste de BP vers le modula

        IF UPPERCASE(USERID) = 'ESKAPE' THEN
            EXIT;

        // Parcours des lignes pour chaque modula
        Param.RESET();
        Param.SETRANGE(Type, 'MODULA');
        IF Param.FINDFIRST() THEN
            REPEAT
                FichierOuvert := FALSE;
                WhseShipHeader.FINDFIRST();
                REPEAT
                    WhseShipLine.SETRANGE("No.", WhseShipHeader."No.");
                    WhseShipLine.SETFILTER("Bin Code", '%1', Param.Code + '*');
                    IF WhseShipLine.FINDFIRST() THEN
                        REPEAT
                            LigneAExporter := TRUE;
                            // Ouverture du fichier
                            IF NOT FichierOuvert THEN BEGIN
                                //IF USERID = 'eskape' THEN Param.LongDescription2 := 'C:\tmp';
                                FichierModula.CREATE(Param.LongDescription2 + '\' + Param.Code + WhseShipHeader."No." + '.TXT');
                                FichierModula.TEXTMODE(TRUE);
                                FichierOuvert := TRUE;
                                Ligne := WhseShipHeader."No." + '|P|Liste import MODULA';
                                FichierModula.WRITE(Ligne);
                            END;
                            // Génération de la ligne
                            Ligne := WhseShipHeader."No." + '||' + GestionMultiRef.RechercheRefActive(WhseShipLine."Item No.");
                            Ligne += '||||' + FORMAT(WhseShipLine."Qty. to Ship") + '|||||';
                            //Ligne += COPYSTR(WhseShipLine."Bin Code", STRLEN(Param.Code) + 1, STRLEN(WhseShipLine."Bin Code") - STRLEN(Param.Code));
                            Ligne += COPYSTR(WhseShipLine."Bin Code", 4, 4);
                            Ligne += '|0|' + COPYSTR(WhseShipLine."Bin Code", 8, 3);
                            Ligne += '|O||';
                            FichierModula.WRITE(Ligne);
                        UNTIL WhseShipLine.NEXT() = 0;
                    IF FichierOuvert THEN
                        FichierModula.CLOSE();
                UNTIL WhseShipHeader.NEXT() = 0;
            UNTIL Param.NEXT() = 0;



        IF NOT LigneAExporter THEN 
            MESSAGE(ESK001Msg);
        end;

    procedure ImprimeEtiquettePrepa(var WhseShipHeader: Record "Warehouse Shipment Header"; _pNoLigneWhse: Integer)
    var
        TempWhseShipLine: Record "Warehouse Shipment Line" temporary;
        WhseShipLine: Record "Warehouse Shipment Line";
        WhseSetup: Record "Warehouse Setup";
        Item: Record Item;  
         LWhseShipHeader: Record "Warehouse Shipment Header";
        LWhseSetup: Record "Warehouse Setup";  
         TempAsmHeader: Record "Assembly Header" temporary;
        TempFromAsmLine: Record "Assembly Line" temporary;
        AsmHeader: Record "Assembly Header";
        LZone: Record Zone;
        LSalesLineComment: Record "Sales Comment Line";    
        LSalesLine: Record "Sales Line";
         LGestionEtq: Codeunit "Etiquettes Articles";
          cu_Ansi: Codeunit "ANSI  <->  ASCII converter";
        FichierEtiquette: File;
        NomFichierTmp: Text[250];
        NomFichier: Text[250];
        _RefArticle: Code[20];
        _Designation: Text[50];
        "_QuantitéLiv": Text[6];
        _Physique: Code[10];
        _NoBl: Code[20];
        _Emplacement: Code[20];
        _NoLigne: Code[10];
        _CommentCli: Text[50];
        Entier: Integer;
        NoLigne: Integer;
        
        NoTri: Integer;
        txtFiltre: Text[1024];
        NiceLabel: Boolean;
       
        LLigne: Text[250];
       
       
        CommentClientWeb: Text[50];
       
    begin
        // WIIO => Gestion de l'impression d'une seule ligne

        NiceLabel := TRUE;


        //IF UPPERCASE(USERID) = 'ESKAPE' THEN
        BEGIN
            WhseShipHeader.FINDFIRST();
            REPEAT
                txtFiltre += WhseShipHeader."No." + '|';
            UNTIL WhseShipHeader.NEXT() = 0;
            txtFiltre := COPYSTR(txtFiltre, 1, STRLEN(txtFiltre) - 1);
            //MESSAGE('%1', txtFiltre);
        END;



        NoLigne := 0;
        // On parcours tout les bls et on places les lignes dans une table temporaire
        WhseShipHeader.FINDFIRST();
        REPEAT
            // AD Le 31-03-2015
            LWhseShipHeader.GET(WhseShipHeader."No."); // Car WhseShipHeader est temporaire
            LWhseShipHeader."Date Flashage" := WORKDATE();
            LWhseShipHeader."Heure Flashage" := TIME;
            LWhseShipHeader."Utilisateur Flashage" := USERID;
            LWhseShipHeader.MODIFY();
            // FIN AD Le 31-03-2015
            WhseShipLine.RESET();
            WhseShipLine.SETCURRENTKEY("Bin Code", "Location Code");
            WhseShipLine.SETFILTER("No.", WhseShipHeader."No.");//txtFiltre);

            IF _pNoLigneWhse <> 0 THEN WhseShipLine.SETRANGE("Line No.", _pNoLigneWhse); // WIIO => Gestion de l'impression d'une seule ligne

            IF WhseShipLine.FINDFIRST() THEN
                REPEAT
                    TempWhseShipLine := WhseShipLine;

                    TempWhseShipLine."Bin Code" := WhseShipLine."Bin Code" + '_';

                    NoLigne += 1;


                    // Si la ligne d'origine est un kit en constitution auto, on insert une ligne par composant
                    LSalesLine.GET(WhseShipLine."Source Subtype", WhseShipLine."Source No.", WhseShipLine."Source Line No.");
                    // MIG2015 =>
                    /*
                    IF LSalesLine."Build Kit" THEN
                      BEGIN
                        LKitSalesLine.RESET();
                        LKitSalesLine.SETRANGE("Document Type", LSalesLine."Document Type");
                        LKitSalesLine.SETRANGE("Document No.", LSalesLine."Document No.");
                        LKitSalesLine.SETRANGE("Document Line No.", LSalesLine."Line No.");
                        IF LKitSalesLine.FINDFIRST () THEN
                          REPEAT
                            TempWhseShipLine."Line No." := (TempWhseShipLine."Line No." + (LKitSalesLine."Line No." / 1000) ) * -1;
                            TempWhseShipLine."Bin Code"  := LKitSalesLine."Bin Code" + '_';
                            TempWhseShipLine."Item No."  := LKitSalesLine."No.";
                            TempWhseShipLine."Qty. to Ship" := TempWhseShipLine."Qty. to Ship" * LKitSalesLine."Quantity per";
                            TempWhseShipLine.Description   := LKitSalesLine.Description;
                            TempWhseShipLine."Source Line No." := NoLigne;
                            NoTri += 1;
                            TempWhseShipLine."Sorting Sequence No." := NoTri;
                            TempWhseShipLine.Insert();
                          UNTIL LKitSalesLine.NEXT() = 0;
                      END
                    */
                    TempAsmHeader.INIT();
                    TempAsmHeader."Document Type" := LSalesLine."Document Type";

                    IF (LSalesLine."Qty. to Assemble to Order" <> 0) AND
                       (LSalesLine.AsmToOrderExists(AsmHeader)) THEN BEGIN
                        TempAsmHeader := AsmHeader;
                        TempAsmHeader."Item No." := LSalesLine."No.";
                        TempAsmHeader."Variant Code" := LSalesLine."Variant Code";
                        TempAsmHeader."Location Code" := LSalesLine."Location Code";
                        TempAsmHeader."Bin Code" := LSalesLine."Bin Code";
                        TempAsmHeader."Due Date" := LSalesLine."Shipment Date";
                        TempAsmHeader.ValidateDates(TempAsmHeader.FIELDNO("Due Date"), TRUE);
                        TempAsmHeader."Unit of Measure Code" := LSalesLine."Unit of Measure Code";
                        TempAsmHeader."Qty. per Unit of Measure" := LSalesLine."Qty. per Unit of Measure";
                        TempAsmHeader.Quantity := LSalesLine."Qty. to Assemble to Order" - AsmHeader."Assembled Quantity";
                        TempAsmHeader."Quantity (Base)" := LSalesLine."Qty. to Asm. to Order (Base)" - AsmHeader."Assembled Quantity (Base)";
                        TempAsmHeader.InitRemainingQty();
                        TempAsmHeader.Insert();
                        TempFromAsmLine.SETRANGE("Document Type", TempAsmHeader."Document Type");
                        TempFromAsmLine.SETRANGE("Document No.", TempAsmHeader."No.");
                        TempFromAsmLine.SETRANGE(Type, TempFromAsmLine.Type::Item);
                        TempFromAsmLine.FIND('-'); // AD Le 03-08-2015 => Pourle moment je laisse sans if pour tester
                        REPEAT
                            TempWhseShipLine."Line No." := (TempWhseShipLine."Line No." + (TempFromAsmLine."Line No." / 1000)) * -1;
                            TempWhseShipLine."Bin Code" := TempFromAsmLine."Bin Code" + '_';
                            TempWhseShipLine."Item No." := TempFromAsmLine."No.";
                            TempWhseShipLine."Qty. to Ship" := LSalesLine."Qty. to Ship" * TempFromAsmLine."Quantity per";
                            TempWhseShipLine.Description := TempFromAsmLine.Description;
                            TempWhseShipLine."Source Line No." := NoLigne;
                            NoTri += 1;
                            TempWhseShipLine."Sorting Sequence No." := NoTri;
                            TempWhseShipLine.Insert();

                        UNTIL TempFromAsmLine.NEXT() = 0;

                    END
                    // FIN MIG2015
                    ELSE BEGIN
                        NoTri += 1;
                        TempWhseShipLine."Sorting Sequence No." := NoTri;
                        TempWhseShipLine.Insert();
                    END;


                UNTIL WhseShipLine.NEXT() = 0;
        UNTIL WhseShipHeader.NEXT() = 0;

        // On tri par code emplacement
        TempWhseShipLine.RESET();
        TempWhseShipLine.SETCURRENTKEY("Bin Code", "Location Code");
        TempWhseShipLine.FINDFIRST();

        // On ouvre le fichier

        IF NOT NiceLabel THEN BEGIN
            WhseSetup.GET();
            WhseSetup.TESTFIELD("Empl. Etiquettes Prépara");
            NomFichierTmp := TEMPORARYPATH + '\' + 'ETQ' + WhseShipHeader."No.";
            NomFichier := WhseSetup."Empl. Etiquettes Prépara" + '\' + 'ETQ' + WhseShipHeader."No." + '.TXT';
            IF UPPERCASE(USERID) = 'ESKAPE' THEN NomFichier := 'c:\tmp' + '\' + 'ETQ' + WhseShipHeader."No." + '.TXT';
            FichierEtiquette.CREATE(NomFichierTmp);
            FichierEtiquette.TEXTMODE(TRUE);
        END ELSE 
            LGestionEtq.OuvrirEtiquetteNiceLabel('ETQ' + WhseShipHeader."No.", FichierEtiquette);
        
        NoLigne := 0;

        // Pour chaque ligne
        REPEAT
            NoLigne += 1;
            Item.GET(TempWhseShipLine."Item No.");
            Item.CALCFIELDS(Inventory);
            // AD Le 27-09-2016 => REGIE
            LZone.GET(TempWhseShipLine."Location Code", TempWhseShipLine."Zone Code");
            LZone.TESTFIELD("Label BP Printer Name");
            // AD Le 27-09-2016
            // CFR le 18/04/2024 - R‚gie : AJOUT d'une ‚tiquette commentaire d‚but DE PIC
            IF (NoLigne = 1) THEN BEGIN
                LSalesLineComment.RESET();
                LSalesLineComment.SETRANGE("Document Type", LSalesLineComment."Document Type"::Order);
                LSalesLineComment.SETRANGE("No.", TempWhseShipLine."Source No.");
                LSalesLineComment.SETRANGE("Document Line No.", 0);
                LSalesLineComment.SETRANGE("Print Wharehouse Shipment", TRUE);
                IF LSalesLineComment.FINDSET() THEN BEGIN
                    LWhseSetup.GET();
                    //lCommentEnTete := '';
                    REPEAT
                        //lCommentEnTete += LSalesLineComment.Comment + ' ';
                        // CFR le 23/04/2024 - R‚gie : 1 ‚tiquette par commentaire
                        LLigne := '';
                        LLigne += '' + ';';   // SP
                        LLigne += '' + ';';   // RefClient
                        LLigne += '' + ';';   // Designation
                        LLigne += '' + ';';   // Designation 2
                        LLigne += '' + ';';   // Designation Du format
                        LLigne += '' + ';';   // Quantit‚
                        LLigne += '' + ';';   // No du BP
                        LLigne += '' + ';';   // Emplacement
                        LLigne += '' + ';';   // Code barre
                        LLigne += '1' + ';';   // Qte Etiquette
                        LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + 'Etiquette_45x45-picking_first_comment.lbl' + ';';      // Nom du format
                                                                                                                                              //VD Deuxieme imprimante etiquette PICKING
                        IF UPPERCASE(USERID) = 'SYMTA\MAGASIN6' THEN 
                            LLigne += 'ZEBRA_PIC_2' + ';'                        
                        ELSE 
                            LLigne += LZone."Label BP Printer Name" + ';';// Nom de l'imprimante
                        
                        //FIN VD
                        LLigne += cu_Ansi.Ascii2Ansi(COPYSTR(LSalesLineComment.Comment, 1, 125)) + ';';   // Commentaire 1
                        LLigne += '' + ';';   // Commentaire 2
                        LLigne += '' + ';';   // Commentaire 3
                        LLigne += '' + ';';   // CommentaireClientWeb
                        FichierEtiquette.WRITE(LLigne);

                    UNTIL LSalesLineComment.NEXT() = 0;
                END;
            END;
            // FIN CFR le 18/04/2024 - R‚gie : AJOUT d'une ‚tiquette commentaire d‚but DE PIC

            // Remplissage des variables
            IF EVALUATE(Entier, GestionMultiRef.RechercheRefActive(TempWhseShipLine."Item No.")) THEN
                _RefArticle := FORMAT(Entier, 0, '<Integer><1000Character, >')
            ELSE
                _RefArticle := GestionMultiRef.RechercheRefActive(TempWhseShipLine."Item No.");
            //Plus de suppression de SKF VD le 05/04/2017
            //IF COPYSTR(_RefArticle, 1, 3) = 'SKF' THEN
            // _RefArticle := COPYSTR(_RefArticle, 4, STRLEN(_RefArticle) - 3);


            _Designation := TempWhseShipLine.Description;
            // MCO Le 05-12-2016 => Ticket 17456
            _Designation := DELCHR(_Designation, '=', ';');
            // MCO Le 05-12-2016 => Ticket 17456

            // MCO Le 08-02-2017 => Régie :  Impression du 1er commentaire ligne web
            LSalesLineComment.RESET();
            LSalesLineComment.SETRANGE("Document Type", TempWhseShipLine."Source Subtype");
            LSalesLineComment.SETRANGE("No.", TempWhseShipLine."Source No.");
            LSalesLineComment.SETRANGE("Document Line No.", TempWhseShipLine."Source Line No.");
            LSalesLineComment.SETRANGE("Commentaires Web", TRUE);
            IF LSalesLineComment.FINDFIRST() THEN 
                CommentClientWeb := cu_Ansi.Ascii2Ansi(COPYSTR(LSalesLineComment.Comment, 1, 50))
                //cu_Ansi.modifie_accent(CommentClientWeb);
            
            ELSE
                CommentClientWeb := '';
            // FIN MCO Le 08-02-2017

            IF EVALUATE(Entier, FORMAT(TempWhseShipLine."Qty. to Ship")) THEN
                _QuantitéLiv := FORMAT(Entier)
            ELSE
                _QuantitéLiv := FORMAT(TempWhseShipLine."Qty. to Ship", 0, '<Integer><Decimals>');

            //JUSTIFIE A DROITE
            IF NOT NiceLabel THEN
                _QuantitéLiv := PADSTR('', 6 - STRLEN(_QuantitéLiv)) + _QuantitéLiv;

            _Physique := FORMAT(Item.Inventory, 0, '<Integer>');

            _NoBl := TempWhseShipLine."No.";
            _Emplacement := COPYSTR(TempWhseShipLine."Bin Code", 1, STRLEN(TempWhseShipLine."Bin Code") - 1);

            //AJOUT d'un séparateur pour lisibilité du casier

            IF STRPOS('0123456789', COPYSTR(_Emplacement, 1, 1)) > 0 THEN BEGIN
                IF STRPOS(_Emplacement, 'T') > 0 THEN
                    _Emplacement := INSSTR(_Emplacement, ' ', STRPOS(_Emplacement, 'T'))
                ELSE
                    IF STRPOS('ABCDEFGHIJKLMNOPQRSTUVWXYZ', COPYSTR(_Emplacement, (STRLEN(_Emplacement)), 1)) > 0 THEN
                        _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 3)
                    ELSE
                        IF COPYSTR(_Emplacement, (STRLEN(_Emplacement) - 2), 1) = '$' THEN
                            _Emplacement := _Emplacement
                        ELSE
                            IF COPYSTR(_Emplacement, (STRLEN(_Emplacement) - 2), 1) = '&' THEN
                                _Emplacement := _Emplacement
                            ELSE
                                IF STRLEN(_Emplacement) = 5 THEN
                                    _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 2)
                                ELSE
                                    IF STRLEN(_Emplacement) = 6 THEN
                                        _Emplacement := INSSTR(_Emplacement, ' ', (STRLEN(_Emplacement) + 1) - 3)

            END

            ELSE
                IF STRPOS('M0', COPYSTR(_Emplacement, 1, 2)) > 0 THEN BEGIN
                    IF STRPOS('12345678', COPYSTR(_Emplacement, 3, 1)) > 0 THEN
                    begin
                        _Emplacement := INSSTR(_Emplacement, '-', 4);
                    _Emplacement := INSSTR(_Emplacement, ' ', 9);
         
                END

                ELSE
                    _Emplacement := _Emplacement;

                

            _NoLigne := FORMAT(NoLigne);
            // AD Le 26-01-2012 => On prend le no de la cde d'origine
            _NoLigne := FORMAT(TempWhseShipLine."Source Line No." / 10000);
            // FIN AD Le 26-01-2012
            _CommentCli := '';

            IF NOT NiceLabel THEN BEGIN
                // Ecriture dans le fichier
                FichierEtiquette.WRITE('^XA');
                FichierEtiquette.WRITE('^PR6');
                FichierEtiquette.WRITE('^LH30,30');
                FichierEtiquette.WRITE('^FO0,10^ABN,36,15^FD' + _RefArticle + '^FS');
                FichierEtiquette.WRITE('^FO0,50^AE^FD' + _Designation + '^FS');
                FichierEtiquette.WRITE('^FO0,90^ABN,36,11^FDQuantite:' + _QuantitéLiv + '^FS');
                FichierEtiquette.WRITE('^FO0,130^AE^FD' + _NoBl + '^FS');
                FichierEtiquette.WRITE('^FO0,170^ABN,30,11^FD' + _Emplacement + '^FS');
                FichierEtiquette.WRITE('^FO0,200^AE^FD     ' + _Physique + '-99^FS');
                FichierEtiquette.WRITE('^FO0,230^AE^FDNo Ligne : ' + _NoLigne + '^FS');
                FichierEtiquette.WRITE('^FO0,260^AE^FD' + _CommentCli + '^FS');
                FichierEtiquette.WRITE('^XZ');
            END ELSE BEGIN
                LWhseSetup.GET();
                LLigne := '';
                LLigne += Item."No." + ';';    // SP
                LLigne += _RefArticle + ';';   // RefClient
                LLigne += _Designation + ';';  // Designation
                LLigne += '' + ';';            // Designation 2
                LLigne += '' + ';';            // Designation Du format
                LLigne += _QuantitéLiv + ';';  // Quantité
                LLigne += _NoBl + ';';         // No du BP
                LLigne += _Emplacement + ';';  // Emplacement
                LLigne += Item."No." + ';';    // Code barre
                LLigne += '1' + ';';            // Qte Etiquette
                LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + 'Etiquette_45x45-picking.lbl' + ';';      // Nom du format

                //VD Deuxieme imprimante etiquette PICKING
                IF UPPERCASE(USERID) = 'SYMTA\MAGASIN6' THEN 
                    LLigne += 'ZEBRA_PIC_2' + ';'
                
                ELSE 
                    // AD Le 27-09-2016 => REGIE
                    //LLigne += 'ZEBRA_PIC' + ';';     // Nom de l'imprimante
                    LLigne += LZone."Label BP Printer Name" + ';';// Nom de l'imprimante
                                                                  // FIN AD Le 27-09-2016

                END;
                //FIN VD


                LLigne += _NoLigne + ';';       // Commentaire 1
                LLigne += _Physique + '-99;';      // Commentaire 2
                LLigne += '' + ';';             // Commentaire 3
                                                // MCO Le 08-02-2017 => Régie
                LLigne += CommentClientWeb + ';'; // CommentaireClientWeb
                                                  // FIN MCO Le 08-02-2017 => Régie


                FichierEtiquette.WRITE(LLigne);
            END;

        UNTIL TempWhseShipLine.NEXT() = 0;

        //VD AJOUT d'une étiquette FIN DE PIC

        RANDOMIZE();
        Number := RANDOM(10);

        _Number := FORMAT(Number);



        //VD AJOUT d'une étiquette FIN DE PIC
        LWhseSetup.GET();
        LLigne := '';
        LLigne += '' + ';';     // SP
        LLigne += '' + ';';    // RefClient
        LLigne += '' + ';';   // Designation
        LLigne += '' + ';';            // Designation 2
        LLigne += '' + ';';            // Designation Du format
        LLigne += '' + ';';   // Quantité
        LLigne += '' + ';';         // No du BP
        LLigne += '' + ';';   // Emplacement
        LLigne += '' + ';';     // Code barre
        LLigne += '1' + ';';            // Qte Etiquette
        LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + 'Etiquette_45x45-picking_blank_v2.lbl' + ';';      // Nom du format

        //VD Deuxieme imprimante etiquette PICKING
        IF UPPERCASE(USERID) = 'SYMTA\MAGASIN6' THEN 
            LLigne += 'ZEBRA_PIC_2' + ';'
           ELSE 
            // AD Le 27-09-2016 => REGIE
            //LLigne += 'ZEBRA_PIC' + ';';     // Nom de l'imprimante
            LLigne += LZone."Label BP Printer Name" + ';';// Nom de l'imprimante
                                                          // FIN AD Le 27-09-2016

            //FIN VD



        LLigne += '' + ';';        // Commentaire 1
        LLigne += 'C:\Users\administrateur\Pictures\smiley' + _Number + '.jpg' + ';';       // Commentaire 2
        LLigne += '' + ';';             // Commentaire 3
                                        // MCO Le 08-02-2017 => Régie
        LLigne += '' + ';'; // CommentaireClientWeb
                            // FIN MCO Le 08-02-2017 => Régie

        FichierEtiquette.WRITE(LLigne);



        IF NOT NiceLabel THEN BEGIN
            FichierEtiquette.WRITE('^XA');
            FichierEtiquette.WRITE('^PR6');
            FichierEtiquette.WRITE('^LH30,30');
            FichierEtiquette.WRITE('^FO0,10^ABN,36,15^FD ^FS');
            FichierEtiquette.WRITE('^XZ');

            // fermeture du fichier et deplacement dans le répertoire définitif
            FichierEtiquette.CLOSE();
            RENAME(NomFichierTmp, NomFichier);

        END ELSE 
            LGestionEtq.FermeEtiquetteNiceLabel('ETQ' + WhseShipHeader."No.", FichierEtiquette)
        

    end;

    procedure ImprimerEtiquetteColis(_pPackListNo: Code[20]; _pPackNo: Code[20]; _pCodeImprimante: Code[20])
    var
        LPackingListPack: Record "Packing List Package";
         LWhseSetup: Record "Warehouse Setup";
        
        LParam: Record "Generals Parameters";
        LPackingListHeader: Record "Packing List Header";
        LGestionEtq: Codeunit "Etiquettes Articles";
        FichierEtiquette: File;
        LLigne: Text;
       
    begin
        LWhseSetup.GET();

        CLEAR(LPackingListPack);
        LPackingListPack.SETRANGE("Packing List No.", _pPackListNo);
        IF _pPackNo <> '' THEN LPackingListPack.SETRANGE("Package No.", _pPackNo);
        IF LPackingListPack.ISEMPTY THEN EXIT;

        LPackingListPack.FINDSET();

        // LEcture du premier BP pour avoir le nom
        //CLEAR(LWhseHeader);
        //LWhseHeader.SETRANGE("Packing List No.", LPackingListPack."Packing List No.");
        //LWhseHeader.FINDFIRST(); // Normal qu'il y est pas de IF !!
        CLEAR(LPackingListHeader);
        LPackingListHeader.GET(_pPackListNo);

        // Ouverture du fichier
        LGestionEtq.OuvrirEtiquetteNiceLabel('ETQ' + LPackingListPack."Packing List No." + '_' + FORMAT(LPackingListPack."Indice Colis"), FichierEtiquette);
        REPEAT
            LLigne := '';
            LLigne += '' + ';';    // SP
            LLigne += '' + ';';   // RefClient
            LLigne += '' + ';';  // Designation
            LLigne += '' + ';';            // Designation 2
            LLigne += '' + ';';            // Designation Du format
            LLigne += '' + ';';  // Quantité
            LLigne += LPackingListPack."Packing List No." + ';';         // No du BP
            LLigne += LPackingListPack."Package No." + ';';  // Emplacement
            LLigne += '' + ';';    // Code barre
            LLigne += '1' + ';';            // Qte Etiquette
            //LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + 'Packing_List.lbl' + ';';      // Nom du format
            LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + 'Etiquette_PKL.lbl' + ';';      // Nom du format

            // on place le nom windows de l'imprimante dans le fichier
            IF (_pCodeImprimante = '') THEN
                _pCodeImprimante := '28-QTE';
            IF NOT LParam.GET('NICELBL_IMP', _pCodeImprimante) THEN
                IF NOT LParam.GET('IMP_ETQ_RECEPT', _pCodeImprimante) THEN;

            LLigne += LParam.LongDescription2 + ';';
            LLigne += '0' + ';';       // Commentaire 1
            LLigne += '0' + ';';     // Commentaire 2
            LLigne += '' + ';';             // Commentaire 3
            LLigne += LPackingListHeader.GetDestinationName() + ';'; // CommentaireClientWeb

            FichierEtiquette.WRITE(LLigne);
        UNTIL LPackingListPack.NEXT() = 0;

        // Fermeture
        LGestionEtq.FermeEtiquetteNiceLabel('ETQ' + LPackingListPack."Packing List No." + '_' + FORMAT(LPackingListPack."Indice Colis"), FichierEtiquette);
    end;

    procedure "ChercheDateLivr.Logiflux"(_pDateBl: Date; _pModeExpedition: Enum "Mode Expédition"): Date
    var
     TargetCustomizedCalendarChange: Record "Customized Calendar Change";
        CduLFunctions: Codeunit "Codeunits Functions";
        CalendarManagement: Codeunit "Calendar Management";
        LDescr: Text[30];
        LDate: Date;
       

    begin
        CduLFunctions.GetCalendarCode(0, 'EXPEDITION', '');

        LDate := _pDateBl;

        WHILE TRUE DO BEGIN
            LDate := CALCDATE('<+1D>', LDate);
            TargetCustomizedCalendarChange.SetRange("Base Calendar Code", 'EXPEDITION');
            TargetCustomizedCalendarChange.SetRange(Date, LDate);
            TargetCustomizedCalendarChange.SetRange(Description, LDescr);
            if TargetCustomizedCalendarChange.FindSet() then
                CalendarManagement.CheckDateStatus(TargetCustomizedCalendarChange);
            if not TargetCustomizedCalendarChange.Nonworking THEN BEGIN
                IF DATE2DWY(LDate, 1) = 6 THEN BEGIN
                    IF _pModeExpedition IN [_pModeExpedition::"Contre Rembourssement Samedi",
                      _pModeExpedition::"Express Samedi"] THEN
                        EXIT(LDate);
                END
                ELSE
                    EXIT(LDate);
            END;
        END;
    end;
}

