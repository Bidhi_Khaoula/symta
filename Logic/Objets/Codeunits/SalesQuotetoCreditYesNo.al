// codeunit 50003 "Sales-Quote to Credit (Yes/No)"
// {
//     TableNo = "Sales Header";

//     trigger OnRun()
//     begin
//         rec.TESTFIELD("Document Type", rec."Document Type"::Quote);
//         IF GUIALLOWED THEN
//             IF NOT CONFIRM(Text000, FALSE) THEN
//                 EXIT;

//         IF (rec."Document Type" = rec."Document Type"::Quote) THEN
//             IF rec.CheckCustomerCreated(TRUE) THEN
//                 rec.GET(rec."Document Type"::Quote, rec."No.")
//             ELSE
//                 EXIT;

//         SalesQuoteToCredit.RUN(Rec);
//         SalesQuoteToCredit.GetSalesOrderHeader(SalesHeader2);
//         COMMIT;
//         MESSAGE(Text001, rec."No.", SalesHeader2."No.");
//     end;

//     var
//         Text000: Label 'Do you want to convert the quote to an order?';
//         Text001: Label 'Quote %1 has been changed to order %2.';
//         SalesHeader2: Record "Sales Header";
//         SalesQuoteToCredit: Codeunit "Sales-Quote to Credit";
// }

