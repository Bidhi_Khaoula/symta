codeunit 50008 "ESK Events"
{
    // CFR 08/07/2019 : FE20190624 => Event sur le CodeUnit 414
    // CFR 11/07/2019 : Test limité aux commandes de ventes
    // CFR 16/07/2019 : Test limité aux lignes avec qté à expédier
    // CFR le 18/11/2020 : Régie > Doublon de commande
    //CFR le 30/11/2022 => R‚gie : blocage de certains mode d'exp‚dition

    trigger OnRun()
    begin
    end;

    [EventSubscriber(ObjectType::Codeunit, 414, 'OnAfterReleaseSalesDoc', '', false, false)]
    local procedure C414_OnAfterReleaseSalesDoc(var SalesHeader: Record "Sales Header"; PreviewMode: Boolean)
    var
        lShippingAgent: Record "Shipping Agent";
        lSalesLine: Record "Sales Line";
        lItemUOM: Record "Item Unit of Measure";
        lSalesHeader: Record "Sales Header";
        ESK001Err: Label 'Error.  \At least one item (%1) exceeds the length authorized by the Shipping Agent : %2', Comment = '%1 = Item No ; %2 = Agent No';      
         lListe: Text;
        INFO001Msg: Label 'Attention : Il existe déjà un document (%1) pour ce client (%2) à la même date pour un montant équivalent (%3).\Souhaitez vous lancer le document ?' , Comment = '%1 = Doc No ; %2 = Cus No ; %3 = Amount';
    begin

        // CFR le 18/11/2020 : Régie > Doublon de commande
        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::Quote) OR (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) THEN BEGIN
            lSalesHeader.SETRANGE("Document Type", SalesHeader."Document Type");
            lSalesHeader.SETFILTER("No.", '<>%1', SalesHeader."No.");
            lSalesHeader.SETRANGE("Sell-to Customer No.", SalesHeader."Sell-to Customer No.");
            lSalesHeader.SETRANGE("Create Date", SalesHeader."Create Date");
            IF lSalesHeader.FINDSET() THEN BEGIN
                SalesHeader.CALCFIELDS(Amount);
                REPEAT
                    lSalesHeader.CALCFIELDS(Amount);
                    IF SalesHeader.Amount = lSalesHeader.Amount THEN 
                        IF NOT CONFIRM(STRSUBSTNO(INFO001Msg, lSalesHeader."No.", lSalesHeader."Sell-to Customer No.", lSalesHeader.Amount), TRUE) THEN
                            ERROR('');
                        //MESSAGE(INFO001, lSalesHeader."No.", lSalesHeader."Sell-to Customer No.", lSalesHeader.Amount);
                     UNTIL lSalesHeader.NEXT() = 0;
            END;
        END;
        // Fin CFR le 18/11/2020

        // CFR 11/07/2019 : Test limité aux commandes de ventes
        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) THEN BEGIN

            // CFR 08/07/2019 : FE20190624
            IF (lShippingAgent.GET(SalesHeader."Shipping Agent Code")) THEN BEGIN
                lListe := '';

                // Longueur à 0 = pas de paramétrage => pas de test
                IF (lShippingAgent."Maximum Length Item" = 0) THEN
                    EXIT;

                lSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                lSalesLine.SETRANGE("Document No.", SalesHeader."No.");
                lSalesLine.SETRANGE(Type, lSalesLine.Type::Item);
                //CFR 16/07/2019 : Test limité aux lignes avec qté à expédier
                lSalesLine.SETFILTER("Qty. to Ship", '>%1', 0);
                IF (lSalesLine.FINDSET()) THEN BEGIN
                    REPEAT
                        IF lItemUOM.GET(lSalesLine."No.", lSalesLine."Unit of Measure Code") THEN BEGIN
                            IF (lItemUOM.Length > lShippingAgent."Maximum Length Item") THEN BEGIN
                                IF (lListe = '')
                                  THEN
                                    lListe := lItemUOM."Item No."
                                ELSE
                                    lListe := lListe + ' - ' + lItemUOM."Item No.";
                            END;
                        END;
                    UNTIL (lSalesLine.NEXT() = 0);
                END;

                IF (lListe <> '')
                  THEN
                    ERROR(ESK001Err, lListe, lShippingAgent."Maximum Length Item");
            END;

        END;
        //CFR le 30/11/2022 => R‚gie : blocage de certains mode d'exp‚dition
        CheckSaturdayShipping(SalesHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, 22, 'OnAfterInitItemLedgEntry', '', false, false)]
    local procedure CU22_OnAfterInitItemLedgEntry(var NewItemLedgEntry: Record "Item Ledger Entry"; var ItemJournalLine: Record "Item Journal Line")
    begin
        NewItemLedgEntry."Date Réelle Ecriture" := TODAY;

        // AD Le 30-01-2020 => REGIE
        NewItemLedgEntry."User Entry" := USERID;
        NewItemLedgEntry."Date Arrivage Marchandise" := ItemJournalLine."Date Arrivage Marchandise";

        //fbrun le 10/08/20 FE20200728
        NewItemLedgEntry."Référence saisie" := ItemJournalLine."Référence saisie";
    end;

    [EventSubscriber(ObjectType::Codeunit, 22, 'OnBeforeInsertValueEntry', '', false, false)]
    local procedure CU22_OnBeforeInsertValueEntry(var ValueEntry: record "Value Entry"; ItemJournalLine: Record "Item Journal Line")
    begin
        ValueEntry."Date Réelle Ecriture" := TODAY;

        // AD Le 30-01-2020 => REGIE
        ValueEntry."User Entry" := USERID;
        ValueEntry."Date Arrivage Marchandise" := ItemJournalLine."Date Arrivage Marchandise";
    end;

    [EventSubscriber(ObjectType::Codeunit, 415, 'OnBeforeReleasePurchaseDoc', '', false, false)]
    local procedure CU415_OnBeforeReleasePurchaseDoc(var PurchaseHeader: Record "Purchase Header"; PreviewMode: Boolean)
    var
        LPurchLine: Record "Purchase Line";
    begin
        LPurchLine.SETRANGE("Document Type", PurchaseHeader."Document Type");
        LPurchLine.SETRANGE("Document No.", PurchaseHeader."No.");
        LPurchLine.SETFILTER(Type, '>0');
        LPurchLine.SETFILTER(Quantity, '<>0');

        IF LPurchLine.ISEMPTY THEN EXIT;
        LPurchLine.FINDSET();
        REPEAT
            LPurchLine.TESTFIELD("Unit of Measure Code");
        UNTIL LPurchLine.NEXT() = 0;
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Header", 'OnAfterValidateEvent', "Shipping Agent Code", false, false)]
    LOCAL PROCEDURE T36_OnAfterValidateEventShippingAgentCode(VAR Rec: Record Microsoft.Sales.Document."Sales Header"; VAR xRec: Record Microsoft.Sales.Document."Sales Header"; CurrFieldNo: Integer);
    BEGIN
        //CFR le 30/11/2022 => R‚gie : blocage de certains mode d'exp‚dition
        CheckSaturdayShipping(Rec);
    END;

    [EventSubscriber(ObjectType::Table, database::"Sales Header", 'OnAfterValidateEvent', "Mode d'expédition", false, false)]
    LOCAL PROCEDURE T36_OnAfterValidateEventModeExpedition(VAR Rec: Record Microsoft.Sales.Document."Sales Header"; VAR xRec: Record Microsoft.Sales.Document."Sales Header"; CurrFieldNo: Integer);
    BEGIN
        //CFR le 30/11/2022 => R‚gie : blocage de certains mode d'exp‚dition
        CheckSaturdayShipping(Rec);
    END;

    [EventSubscriber(ObjectType::Table, database::"G/L Entry", 'OnAfterCopyGLEntryFromGenJnlLine', '', false, false)]
    LOCAL PROCEDURE T17_OnAfterCopyGLEntryFromGenJnlLine(VAR GLEntry: Record Microsoft.Finance.GeneralLedger.Ledger."G/L Entry"; VAR GenJournalLine: Record Microsoft.Finance.GeneralLedger.Journal."Gen. Journal Line");
    VAR
        DemandeTransporteur: Record "Demande transporteur";
    BEGIN
        IF GenJournalLine."Document Type" = GenJournalLine."Document Type"::DemandeTransporteur THEN
            IF DemandeTransporteur.GET(GenJournalLine."Document No.") THEN
                IF NOT DemandeTransporteur."Facturé" THEN BEGIN
                    DemandeTransporteur.Facturé := TRUE;
                    DemandeTransporteur.MODIFY();
                END;
             END;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    LOCAL PROCEDURE CU90_OnAfterPostPurchaseDoc(VAR PurchaseHeader: Record Microsoft.Purchases.Document."Purchase Header"; VAR GenJnlPostLine: Codeunit Microsoft.Finance.GeneralLedger.Posting."Gen. Jnl.-Post Line"; PurchRcpHdrNo: Code[20]; RetShptHdrNo: Code[20]; PurchInvHdrNo: Code[20]; PurchCrMemoHdrNo: Code[20]);
    VAR
        DemandeTransLink: Record "Demande Trans. Link";
    BEGIN
        // GRI le 09/05/2023 : on transforme lien avec FA => lien avec FAE
        IF PurchInvHdrNo <> '' THEN BEGIN
            DemandeTransLink.SETRANGE(Type, DemandeTransLink.Type::"Purch. Invoice");
            DemandeTransLink.SETRANGE("No.", PurchaseHeader."No.");
            IF DemandeTransLink.FINDSET() THEN
                REPEAT
                    DemandeTransLink.RENAME(DemandeTransLink."Demande No.", DemandeTransLink.Type::"Purch. Invoice Archive", PurchInvHdrNo);
                UNTIL DemandeTransLink.NEXT() = 0;
        END;
    END;

    LOCAL PROCEDURE CheckSaturdayShipping(VAR pSalesHeader: Record Microsoft.Sales.Document."Sales Header");
    VAR
     lShippingAgent: Record "Shipping Agent";
        lErrorTxt: Label 'Mode d''expédition %1 n''est pas possible avec le transporteur %2.' , Comment = '%1 = Exped Mode ; %2 = Transporteur Code';
       
    BEGIN
        //CFR le 30/11/2022 => R‚gie : blocage de certains mode d'exp‚dition
        IF NOT GUIALLOWED THEN
            EXIT;
        IF (lShippingAgent.GET(pSalesHeader."Shipping Agent Code")) THEN
            IF (lShippingAgent."No Delivery On Saturday") THEN
                IF (pSalesHeader."Mode d'expédition" = pSalesHeader."Mode d'expédition"::"Express Samedi") THEN
                    ERROR(lErrorTxt, pSalesHeader."Mode d'expédition", pSalesHeader."Shipping Agent Code");
    END;
}

