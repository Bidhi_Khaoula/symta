codeunit 50011 "Maj des couts AUTO"
{
    // Job Queue Entry

    TableNo = "Job Queue Entry";

    trigger OnRun()
    var
        ItemLedgEntry: Record "Item Ledger Entry";
        ValueEntry: record "Value Entry";
        ItemApplnEntry: Record "Item Application Entry";
        AvgCostAdjmtEntryPoint: Record "Avg. Cost Adjmt. Entry Point";
        Item: Record Item;
         ModeNAS: Boolean;
    begin
        Item.RESET();


        CASE rec."Parameter String" OF
            'AUTO':
                ModeNAS := TRUE;
        END;


        ItemApplnEntry.LOCKTABLE();
        IF NOT ItemApplnEntry.FindLast() THEN
            EXIT;
        ItemLedgEntry.LOCKTABLE();
        IF NOT ItemLedgEntry.FindLast() THEN
            EXIT;
        AvgCostAdjmtEntryPoint.LOCKTABLE();
        IF AvgCostAdjmtEntryPoint.FindLast() THEN;
        ValueEntry.LOCKTABLE();
        IF NOT ValueEntry.FindLast() THEN
            EXIT;


        InvtAdjmt.SetProperties(TRUE, PostToGL);
        CduGSingleInstance.FctSetIsOnlineAdjmtVAlue(true);
        InvtAdjmt.SetFilterItem(Item);

        InvtAdjmt.MakeMultiLevelAdjmt();
        CduGSingleInstance.FctSetIsOnlineAdjmtVAlue(false);
        // MCO Le 09-02-2017 => Régie  => Fait dans un code unit à part
        // CODEUNIT.RUN(CODEUNIT::"Calcul Code Appro");
        // FIN MCO Le 09-02-2017 => Régie

        //UpdateItemAnalysisView.UpdateAll(0,TRUE);
        IF GUIALLOWED AND (NOT ModeNAS) THEN MESSAGE('Terminee');
    end;

    var
       CduGSingleInstance: Codeunit SingleInstance;
        InvtAdjmt: Codeunit "Inventory Adjustment";
        PostToGL: Boolean;
        
}
