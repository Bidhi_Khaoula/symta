codeunit 50101 "Substitute Process"
{
    EventSubscriberInstance = Manual;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforeGetSalesLineQty', '', false, false)]
    local procedure CU80_OnBeforeGetSalesLineQty(SalesLine: Record "Sales Line"; QtyType: Option General,Invoicing,Shipping; var SalesLineQty: Decimal; var IsHandled: Boolean)
    begin
        if QtyType = 3 then begin
            SalesLineQty := SalesLine."Outstanding Quantity";
            IsHandled := true;
        end;
    end;

}