#pragma warning disable AA0215
codeunit 50006 "Procédure de Nuit"
#pragma warning restore AA0215
{
    TableNo = "Job Queue Entry";

    trigger OnRun()
    begin
        CalculPortefeuilleVente();
        CalculValeurDuStock();

        //Permet de mettre à jour les xml pour le E-commerce
        CU_Web.RUN();
    end;

    var
        CU_Web: Codeunit "Export XML - eCommerce";

    procedure CalculPortefeuilleVente()
    var
        Salesheader: Record "Sales Header";
        SalesLine: Record "Sales Line";       
        Histo: Record "Historisation Taux SAV / Porte";
         TotalAmountP: Decimal;
    begin
        // Calcul du portefeuille des commandes de vente
        // ---------------------------------------------
        TotalAmountP := 0;

        Salesheader.RESET();
        Salesheader.SETCURRENTKEY("Order Date", Salesheader."Document Type");
        Salesheader.SETFILTER(Salesheader."Document Type", '%1|%2', Salesheader."Document Type"::Order,
        Salesheader."Document Type"::"Blanket Order");
        IF Salesheader.FIND('-') THEN
            REPEAT
                SalesLine.RESET();
                SalesLine.SETCURRENTKEY(SalesLine."Document Type", SalesLine."Document No.", SalesLine."Line No.");
                SalesLine.SETRANGE(SalesLine."Document Type", Salesheader."Document Type");
                SalesLine.SETRANGE("Document No.", Salesheader."No.");
                IF SalesLine.FIND('-') THEN
                    REPEAT
                        IF SalesLine.Quantity <> 0 THEN
                            TotalAmountP += SalesLine."Outstanding Quantity" * SalesLine."Net Unit Price";
                    UNTIL SalesLine.NEXT() = 0;
            UNTIL Salesheader.NEXT() = 0;

        // AD Le 22-11-2010 => Suppression si existant
        Histo.RESET();
        Histo.SETRANGE("Type Ligne", Histo."Type Ligne"::"Portefeuille Commande");
        Histo.SETRANGE(Date, TODAY);
        Histo.DELETEALL();
        // FIN AD le 22-11-2010



        CLEAR(Histo);
        Histo.VALIDATE("Type Ligne", Histo."Type Ligne"::"Portefeuille Commande");
        Histo.VALIDATE(Date, TODAY);
        Histo.VALIDATE(Heure, TIME);
        Histo.VALIDATE(Montant, TotalAmountP);

        IF NOT Histo.INSERT() THEN Histo.MODIFY();
    end;

    procedure CalculValeurDuStock()
    var
        
        Item: Record Item;
        Histo: record "Historisation Taux SAV / Porte";
        pamp: Decimal;
        ValeurStock: Decimal;
        
    begin
        ValeurStock := 0;
        Item.RESET();
        Item.SETRANGE(Item."Sans Mouvements de Stock", FALSE);
        Item.SETFILTER("Date Filter", '..%1', TODAY);
        IF Item.FIND('-') THEN
            REPEAT
                Item.CALCFIELDS("Inventory At Date");
                IF Item."Inventory At Date" <> 0 THEN BEGIN
                    pamp := 0;
                    IF Item."Costing Method" = Item."Costing Method"::Average THEN
                        pamp := Item."Unit Cost";
                    IF pamp = 0 THEN
                        pamp := Item."Standard Cost";
                    IF pamp = 0 THEN
                        pamp := Item."Unit Cost";
                    ValeurStock += pamp * Item."Inventory At Date";
                END;
            UNTIL Item.NEXT() = 0;

        // AD Le 22-11-2010 => Suppression si existant
        Histo.RESET();
        Histo.SETRANGE("Type Ligne", Histo."Type Ligne"::"Valeur du stock");
        Histo.SETRANGE(Date, TODAY);
        Histo.DELETEALL();
        // FIN AD le 22-11-2010


        CLEAR(Histo);
        Histo.VALIDATE("Type Ligne", Histo."Type Ligne"::"Valeur du stock");
        Histo.VALIDATE(Date, TODAY);
        Histo.VALIDATE(Heure, TIME);
        Histo.VALIDATE(Montant, ValeurStock);
        IF NOT Histo.INSERT() THEN Histo.MODIFY();
    end;
}

