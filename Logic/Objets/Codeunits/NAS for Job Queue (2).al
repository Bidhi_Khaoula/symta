// codeunit 50068 "NAS for Job Queue (2)"
// {

//     trigger OnRun()
//     begin
//         StartNAS();
//     end;

//     var
//         cTFFuncs: Codeunit "52101146";
//         recCompanySetup: Record "52101153";
//         recNASTempTable: Record "52101159" temporary;
//         Text001: Label 'Processing:';
//         Text002: Label 'Archive';
//         Text003: Label 'XML-Import';
//         Text004: Label 'Get Documents';
//         Text005: Label 'Delete old Entries';
//         Error001: Label 'Error';
//         Text006: Label 'Start...';

//     procedure StartNAS()
//     begin
//         IF NOT recCompanySetup.GET() THEN
//           EXIT;

//         recCompanySetup."Rank Process archive Documents" := 1;
//         recCompanySetup."Rank Process XML-Import" := 2;
//         recCompanySetup."Rank Process Get Documents" := 3;

//         IF( GUIALLOWED() ) THEN
//           recNASTempTable."Manual Processing" := TRUE;

//         IF( recCompanySetup."NAS Runtime [s]" < 1 ) THEN
//           recCompanySetup."NAS Runtime [s]" := 1;

//         ProcessQueueNAS();
//     end;

//     procedure ProcessQueueNAS()
//     var
//         cTFFuncs: Codeunit "52101146";
//         bStart: Boolean;
//         iIndex: Integer;
//         cPartnerFuncs: Codeunit "52101149";
//         i: Integer;
//     begin
//         cTFFuncs.LogNAS( Text006, '' );

//         IF( GUIALLOWED ) THEN
//           cTFFuncs.DialogOpen( 1, TRUE );

//         iIndex := 1;

//         recCompanySetup.GET();
//         bStart := FALSE;

//         IF( GUIALLOWED ) THEN
//           bStart := TRUE;
//         IF( (NOT GUIALLOWED) AND recCompanySetup."NAS active" ) THEN // NAS
//           IF( cPartnerFuncs.SchedulingActive() ) THEN // Scheduling aktiv
//             bStart := TRUE;

//         IF( bStart ) THEN BEGIN
//           NAS_Archive( iIndex );
//           NAS_XML( iIndex );
//           NAS_Docs( iIndex );
//           NAS_DeleteLogs();

//           iIndex += 1;
//           IF( iIndex > 3 ) THEN
//             iIndex := 1;
//         END;

//         IF( GUIALLOWED ) THEN
//           cTFFuncs.DialogClose();
//     end;

//     procedure NAS_Archive(iIndex: Integer)
//     begin
//         //IF( iIndex <> recCompanySetup."Rank Process archive Documents" ) THEN
//         //  EXIT;

//         cTFFuncs.LogNAS( Text002, '' );
//         CLEARLASTERROR();
//         IF( NOT CODEUNIT.RUN( CODEUNIT::"NAS Archive", recNASTempTable ) ) THEN
//           cTFFuncs.LogNAS( TruncateText(GETLASTERRORTEXT()), Error001 );
//     end;

//     procedure NAS_XML(iIndex: Integer)
//     begin
//         //IF( iIndex <> recCompanySetup."Rank Process XML-Import" ) THEN
//         //  EXIT;

//         cTFFuncs.LogNAS( Text003, '' );
//         CLEARLASTERROR();
//         IF( NOT CODEUNIT.RUN( CODEUNIT::"NAS XML-Import", recNASTempTable ) ) THEN
//           cTFFuncs.LogNAS( TruncateText(GETLASTERRORTEXT()), Error001 );
//     end;

//     procedure NAS_Docs(iIndex: Integer)
//     var
//         recMappingHeader: Record "52101151";
//     begin
//         //IF( iIndex <> recCompanySetup."Rank Process Get Documents" ) THEN
//         //  EXIT;

//         cTFFuncs.LogNAS( Text004, '' );
//         CLEARLASTERROR();
//         IF( NOT CODEUNIT.RUN( CODEUNIT::"NAS Get Documents", recNASTempTable ) ) THEN
//           cTFFuncs.LogNAS( TruncateText(GETLASTERRORTEXT()), Error001 );
//     end;

//     procedure NAS_DeleteLogs()
//     begin
//         cTFFuncs.LogNAS( Text005, '' );
//         CLEARLASTERROR();
//         IF( NOT CODEUNIT.RUN( CODEUNIT::"NAS Delete Log", recNASTempTable ) ) THEN
//           cTFFuncs.LogNAS( TruncateText(GETLASTERRORTEXT()), Error001 );
//     end;

//     local procedure TruncateText(str: Text): Text[250]
//     begin
//         EXIT(COPYSTR(str, 1, 250));
//     end;
// }

