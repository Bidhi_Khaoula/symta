codeunit 50039 "stat expedition"
{

    trigger OnRun()
    var
        NomFichier: Text[60];
        "InfoSociété": Record "Company Information";
        SalesShipHead: Record "Sales Shipment Header";
        SalesShipLine: Record "Sales Shipment Line";
        date_deb: Date;
        ligne: Text[1000];
        mt: Decimal;
        mtp: Decimal;
        date_fin: Date;
    begin
        /*
        dialog_window.OPEN ('Date facture (JJMMAA) #1#########\',date_deb);
        dialog_window.INPUT(1,date_deb);
        dialog_window.CLOSE;
        
        dialog_window.OPEN ('Date facture (JJMMAA) #1#########\',date_fin);
        dialog_window.INPUT(1,date_fin);
        dialog_window.CLOSE;
        */

        Fichier.TEXTMODE(TRUE);
        Fichier.WRITEMODE(TRUE);
        InfoSociété.GET();
        NomFichier := InfoSociété."Export EDI Folder" + '\stat expedition.txt';
        Fichier.CREATE(NomFichier);
        Fichier.SEEK(Fichier.LEN);

        SalesShipHead.RESET();
        //SalesShipHead.SETRANGE(SalesShipHead."Due Date",'>=%1 and <=%2',format(date_deb),format(date_fin));
        //SalesShipHead.SETRANGE(SalesShipHead."No.",'BL-11136666');

        IF SalesShipHead.FINDFIRST() THEN
            REPEAT
                SalesShipHead.GET(SalesShipHead."No.");

                SalesShipLine.RESET();
                SalesShipLine.SETRANGE(SalesShipLine."Document No.", SalesShipHead."No.");

                mt := 0;
                mtp := 0;
                IF SalesShipLine.FindSet() THEN
                    REPEAT
                        IF SalesShipLine."No." = '70818300' THEN
                            mtp += SalesShipLine."Unit Price" * SalesShipLine.Quantity
                        ELSE
                            mt += SalesShipLine."Item Charge Base Amount";
                    UNTIL SalesShipLine.NEXT() = 0;

                ligne := SalesShipHead."No." + ';'
                + SalesShipHead."Sell-to Post Code" + ';'
                + FORMAT(SalesShipHead."Nb Of Box") + ';'
                + FORMAT(SalesShipHead.Weight) + ';'
                + FORMAT(mt) + ';'
                + FORMAT(mtp) + ';'
                + FORMAT(SalesShipHead."Document Date") + ';'
                + SalesShipHead."Shipping Agent Code" + ';'
                + SalesShipHead."Shipment Method Code" + ';'
                ;

                Fichier.WRITE(ligne);

            UNTIL SalesShipHead.NEXT() = 0;
        MESSAGE(ESK001Msg);

    end;

    var
        ESK001Msg: Label 'Terminé !';
        Fichier: File;
}

