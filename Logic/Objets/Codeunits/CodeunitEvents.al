codeunit 50018 "Codeunit Events"
{
    //>> Mig CU 11
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Gen. Jnl.-Check Line", 'OnBeforeCheckICPartner', '', false, false)]
    local procedure CU11_OnBeforeCheckICPartner(AccountType: Enum "Gen. Journal Account Type"; AccountNo: Code[20];
                                                                 DocumentType: Option; var CheckDone: Boolean; GenJnlLine: Record "Gen. Journal Line")
    var
        Customer: Record Customer;
        ICPartner: Record "IC Partner";
        GenJnlTemplate: Record "Gen. Journal Template";
    begin
        if GenJnlTemplate.Get(GenJnlLine."Journal Template Name") then;
        if AccountType = AccountType::Customer then begin
            if Customer.Get(AccountNo) then begin
                // MIG2017 Customer.CheckBlockedCustOnJnls(Customer, DocumentType, true);
                if (Customer."IC Partner Code" <> '') and (GenJnlTemplate.Type = GenJnlTemplate.Type::Intercompany) and
                   ICPartner.Get(Customer."IC Partner Code")
                then
                    ICPartner.CheckICPartnerIndirect(Format(AccountType), AccountNo);
            end;
            CheckDone := true;
        end;
    end;
    //<< Mig CU 11
    //>> Mig CU 21
    [EventSubscriber(ObjectType::Codeunit, codeunit::"Item Jnl.-Check Line", 'OnRunCheckOnBeforeTestFieldAppliesToEntry', '', false, false)]
    local procedure CU21_OnRunCheckOnBeforeTestFieldAppliesToEntry(var ItemJournalLine: Record "Item Journal Line"; var IsHandled: Boolean)
    begin
        if (ItemJournalLine."Value Entry Type" <> ItemJournalLine."Value Entry Type"::"Direct Cost") or (ItemJournalLine."Item Charge No." <> '') then
            if ItemJournalLine."Inventory Value Per" = ItemJournalLine."Inventory Value Per"::" " then
                IF NOT ItemJournalLine."Sans mouvement de stock" THEN
                    // FIN AD Le 17-09-2009
                    ItemJournalLine.TestField("Applies-to Entry", ErrorInfo.Create());
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Check Line", 'OnBeforeDateNotAllowed', '', false, false)]
    local procedure CU21_OnBeforeDateNotAllowed(ItemJnlLine: Record "Item Journal Line"; var DateCheckDone: Boolean)
    var
        UserSetupManagement: Codeunit "User Setup Management";
        PostingDateRangeErr: Label 'The Posting Date is not within your range of allowed posting dates.';
    begin
        DateCheckDone := true;
        if not UserSetupManagement.IsPostingDateValid(ItemJnlLine."Posting Date") then begin
            MESSAGE('%1', ItemJnlLine."Posting Date");
            Error(ErrorInfo.Create(PostingDateRangeErr, true));
        end;

    end;
    //<< Mig CU 21
    //>> Mig CU 22
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforePostLineByEntryType', '', false, false)]
    local procedure CU22_OnBeforePostLineByEntryType(var ItemJournalLine: Record "Item Journal Line"; CalledFromAdjustment: Boolean; CalledFromInvtPutawayPick: Boolean)
    var
        ItemNotStk: Record Item;
    begin
        // AD Le 17-09-2009 => Non Stock‚
        IF ItemNotStk.GET(ItemJournalLine."Item No.") THEN
            IF ItemNotStk."Sans Mouvements de Stock" THEN
                ItemJournalLine."Sans mouvement de stock" := TRUE;
        // FIN AD Le 17-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeUpdateUnitCost', '', false, false)]
    local procedure CU22_OnBeforeUpdateUnitCost(var ValueEntry: Record "Value Entry"; var IsHandled: Boolean; ItemJournalLine: Record "Item Journal Line")
    var
        Item: Record Item;
        GLSetup: Record "General Ledger Setup";
        rec_ItemVendor: Record "Item Vendor";
        ItemCostMgt: Codeunit ItemCostManagement;
        LastDirectCost: Decimal;
        TotalAmount: Decimal;
        UpdateSKU: Boolean;
    begin
        IsHandled := true;

        GLSetup.Get();
        if ItemJournalLine."Item No." <> '' then
            Item.Get(ItemJournalLine."Item No.");
        if (ValueEntry."Valued Quantity" > 0) and not (ValueEntry."Expected Cost" or ItemJournalLine.Adjustment) then begin
            Item.LockTable();
            if not Item.Find() then
                exit;

            if ValueEntry.IsInbound() and
               ((ValueEntry."Cost Amount (Actual)" + ValueEntry."Discount Amount" > 0) or Item.IsNonInventoriableType()) and
               (ItemJournalLine."Value Entry Type" = ItemJournalLine."Value Entry Type"::"Direct Cost") and
               (ItemJournalLine."Item Charge No." = '') and not Item."Inventory Value Zero"
            then begin
                TotalAmount := ItemJournalLine.Amount + ItemJournalLine."Discount Amount";
                //LastDirectCost := Round(TotalAmount / ValueEntry."Valued Quantity", GLSetup."Unit-Amount Rounding Precision")
                LastDirectCost := ROUND(
                    // AD Le 21-01-2014 => On Veut le prix Net
                    // (ItemJnlLine.Amount + ItemJnlLine."Discount Amount") / ""Valued Quantity",
                    (ItemJournalLine.Amount) / ValueEntry."Invoiced Quantity",
                    // FIN AD Le 21-01-2014
                    GLSetup."Unit-Amount Rounding Precision");
            end;

            if ValueEntry."Drop Shipment" then begin
                if LastDirectCost <> 0 then begin
                    Item."Last Direct Cost" := LastDirectCost;
                    Item.Modify();
                    // MC Le 11-05-2011 => SYMTA -> Modification du champ [Last Direct Cost] de la table [Item Vendor]
                    IF rec_ItemVendor.GET(ValueEntry."Source No.", ValueEntry."Item No.", ValueEntry."Variant Code") THEN BEGIN
                        rec_ItemVendor."Last Direct Cost" := LastDirectCost;
                        rec_ItemVendor.Modify();
                    END;
                    // FIN MC Le 11-05-2011 => SYMTA
                    ItemCostMgt.SetProperties(false, ValueEntry."Invoiced Quantity");
                    ItemCostMgt.FindUpdateUnitCostSKU(Item, ValueEntry."Location Code", ValueEntry."Variant Code", true, LastDirectCost);
                end;
            end else begin
                UpdateSKU := true;
                ItemCostMgt.SetProperties(false, ValueEntry."Invoiced Quantity");
                ItemCostMgt.UpdateUnitCost(Item, ValueEntry."Location Code", ValueEntry."Variant Code", LastDirectCost, 0, UpdateSKU, true, false, 0);
                // MC Le 11-05-2011 => SYMTA -> Modification du champ [Last Direct Cost] de la table [Item Vendor]
                IF rec_ItemVendor.GET(ValueEntry."Source No.", ValueEntry."Item No.", ValueEntry."Variant Code") THEN BEGIN
                    rec_ItemVendor."Last Direct Cost" := LastDirectCost;
                    rec_ItemVendor.Modify();
                END;
                // FIN MC Le 11-05-2011 => SYMTA
            end;
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnAfterInitItemLedgEntry', '', false, false)]
    local procedure CU22_OnAfterInitItemLedgEntry(var NewItemLedgEntry: Record "Item Ledger Entry"; var ItemJournalLine: Record "Item Journal Line"; var ItemLedgEntryNo: Integer)
    begin
        // AD Le 17-09-2009 => Non Stock‚
        IF ItemJournalLine."Sans mouvement de stock" THEN BEGIN
            Clear(NewItemLedgEntry);
            NewItemLedgEntry.INIT();
            ItemLedgEntryNo := 0;
            NewItemLedgEntry."Entry No." := ItemLedgEntryNo;
            NewItemLedgEntry."Source Type" := ItemJournalLine."Source Type";
            NewItemLedgEntry."Source No." := ItemJournalLine."Source No.";
            NewItemLedgEntry."Location Code" := ItemJournalLine."Location Code";
            NewItemLedgEntry."Job No." := ItemJournalLine."Job No.";
            NewItemLedgEntry."Job Task No." := ItemJournalLine."Job Task No.";
            // GR MIG2015
            //NewItemLedgEntry."Prod. Order No." := ItemJournalLine."Prod. Order No.";
            //NewItemLedgEntry."Prod. Order Line No." := ItemJournalLine."Prod. Order Line No.";
            NewItemLedgEntry."Prod. Order Comp. Line No." := ItemJournalLine."Prod. Order Comp. Line No.";
        END;
        // FIN AD Le 17-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeInsertItemLedgEntryProcedure', '', false, false)]
    local procedure CU22_OnBeforeInsertItemLedgEntryProcedure(var ItemLedgerEntry: Record "Item Ledger Entry"; var IsHandled: Boolean; var ItemJournalLine: Record "Item Journal Line")
    begin
        // AD Le 17-09-2009 => Non Stocké
        IF ItemJournalLine."Sans mouvement de stock" THEN
            IsHandled := true;
        // FIN AD Le 17-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeInsertItemLedgEntry', '', false, false)]
    local procedure CU22_OnBeforeInsertItemLedgEntry(var ItemLedgerEntry: Record "Item Ledger Entry"; ItemJournalLine: Record "Item Journal Line"; TransferItem: Boolean; OldItemLedgEntry: Record "Item Ledger Entry"; ItemJournalLineOrigin: Record "Item Journal Line")
    var
        Item: Record Item;
        CodeunitsFunctions: codeunit "Codeunits Functions";
    begin
        // AD Le 07-09-2009 => Suivi des champs
        ItemLedgerEntry."Campaign No." := ItemJournalLine."Campaign No.";
        ItemLedgerEntry."Sales Line type" := ItemJournalLine."Sales Line type";
        ItemLedgerEntry."Document Code" := ItemJournalLine."Document Code";

        // CFR le 24/06/2022 - SFD20210929 : Inventaires >> T32 : suivi [Journal Batch Name]
        ItemLedgerEntry."Journal Batch Name" := ItemJournalLine."Journal Batch Name";
        // FIN CFR le 24/06/2022

        // MC Le 31-01-2013 => Stats conso retour à la date de l'écriture d'origine
        ItemLedgerEntry."Return Appl.-from Item Entry" := ItemJournalLine."Return Appl.-from Item Entry";
        // FIN MC Le 31-01-2013

        // AD Le 24-03-2015 => No de BL fournisseur à la ligne de réception
        ItemLedgerEntry."External Document No. 2" := ItemJournalLine."External Document No. 2";
        // FIN AD Le 24-03-2015


        // ANI Le 27-11-2017 FE20171019
        ItemLedgerEntry."Warehouse Document No." := ItemJournalLine."Warehouse Document No.";
        ItemLedgerEntry."Source Document No." := ItemJournalLine."Source Document No.";
        // FIN ANI Le 27-11-2017 FE20171019

        /*
          IF ValueEntry."Source Type" = ValueEntry."Source Type"::Customer THEN
            BEGIN
              IF LCust.GET(ValueEntry."Source No.") THEN
              BEGIN

                ValueEntry."Territory Code" := LCust."Territory Code";
                ValueEntry."Source Family Code 1" := LCust."Family Code 1";
                ValueEntry."Source Family Code 2" := LCust."Family Code 2";
                ValueEntry."Source Family Code 3" := LCust."Family Code 3";
                ValueEntry."Source Activity Code" := LCust."Activity Code";
                ValueEntry."Source Direction Code" := LCust."Direction Code";
              END;
            END;
        */
        ItemLedgerEntry.Commentaire := ItemJournalLine.Commentaire;

        Item.GET(ItemJournalLine."Item No.");
        ItemLedgerEntry."Hors Normes" := Item.GetHorsNorme(ItemJournalLine.Quantity);
        //ItemLedgEntry."Manufacturer Code" := "Manufacturer Code";

        // AD Le 21-01-2014 =>
        Item.CALCFIELDS(Inventory);
        ItemLedgerEntry."Stock Physique" := Item.Inventory;
        // FIN AD Le 21-01-2014

        // FIN AD Le 07-09-2009
        //FBRUN
        CodeunitsFunctions.InsertStatConso(ItemLedgerEntry);
        CodeunitsFunctions.MAJDateDerniereEntrée(ItemLedgerEntry);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeInsertPhysInvtLedgEntry', '', false, false)]
    local procedure CU22_OnBeforeInsertPhysInvtLedgEntry(var PhysInventoryLedgerEntry: Record "Phys. Inventory Ledger Entry"; ItemJournalLine: Record "Item Journal Line"; LastSplitItemJournalLine: Record "Item Journal Line")
    var
        lBinInventoryLedgerEntry: Record "Bin Inventory Ledger Entry";
        Item: Record Item;
        lGestionMultireference: Codeunit "Gestion Multi-référence";
    begin
        // CFR le 16/03/2022 - SFD20210929 : Inventaires >> T281 : suivi [Code emplacement] + [Ecriture inventaire emplacement]
        PhysInventoryLedgerEntry."Bin Code" := ItemJournalLine."Bin Code";

        IF NOT lBinInventoryLedgerEntry.GET(ItemJournalLine."Posting Date", ItemJournalLine."Document No.", ItemJournalLine."Location Code", ItemJournalLine."Bin Code") THEN BEGIN
            lBinInventoryLedgerEntry.INIT();
            lBinInventoryLedgerEntry.VALIDATE("Posting Date", ItemJournalLine."Posting Date");
            lBinInventoryLedgerEntry.VALIDATE("Document No.", ItemJournalLine."Document No.");
            lBinInventoryLedgerEntry.VALIDATE("Location Code", ItemJournalLine."Location Code");
            lBinInventoryLedgerEntry.VALIDATE("Bin Code", ItemJournalLine."Bin Code");
            // CFR le 24/06/2022 - SFD20210929 : Inventaires >> T50072 : suivi [Journal Template Name] [Journal Batch Name]
            lBinInventoryLedgerEntry.VALIDATE("Journal Template Name", ItemJournalLine."Journal Template Name");
            lBinInventoryLedgerEntry.VALIDATE("Journal Batch Name", ItemJournalLine."Journal Batch Name");
            // FIN CFR le 24/06/2022
            IF (lBinInventoryLedgerEntry.INSERT()) THEN;
        END;
        // FIN CFR le 16/03/2022

        // CFR le 05/04/2022 - SFD20210929 : Inventaires >> T281 : suivi [Code magasinier]
        PhysInventoryLedgerEntry."Warehouse User ID" := ItemJournalLine."Warehouse User ID";
        // FIN CFR le 05/04/2022
        // CFR le 24/06/2022 - SFD20210929 : Inventaires >> T281 : suivi [R‚f‚rence active]
        PhysInventoryLedgerEntry."No. 2" := lGestionMultireference.RechercheRefActive(ItemJournalLine."Item No.");
        // FIN CFR le 24/06/2022

        // GRI le 21/09/2022 SFD20210929
        if ItemJournalLine."Item No." <> '' then begin
            Item.Get(ItemJournalLine."Item No.");
            PhysInventoryLedgerEntry.Description := Item.Description;
            PhysInventoryLedgerEntry."Product Type" := Item."Product Type";
            PhysInventoryLedgerEntry."Item Category Code" := Item."Item Category Code";
            PhysInventoryLedgerEntry."Manufacturer Code" := Item."Manufacturer Code";
        end;
        // fin GRI le 21/09/2022 SFD20210929
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeInsertValueEntry', '', false, false)]
    local procedure CU22_OnBeforeInsertValueEntry(var ValueEntry: Record "Value Entry"; ItemJournalLine: Record "Item Journal Line"; var ItemLedgerEntry: Record "Item Ledger Entry"; var ValueEntryNo: Integer; var InventoryPostingToGL: Codeunit "Inventory Posting To G/L"; CalledFromAdjustment: Boolean; var OldItemLedgEntry: Record "Item Ledger Entry"; var Item: Record Item; TransferItem: Boolean; var GlobalValueEntry: Record "Value Entry")
    var
        LCust: Record Customer;
        Campaign: Record Campaign;
    begin
        // AD Le 07-09-2009 => Suivi des champs
        ValueEntry."Invoice Customer No." := ItemJournalLine."Invoice Customer No.";
        ValueEntry."Manufacturer Code" := ItemJournalLine."Manufacturer Code";
        ValueEntry."Item Category Code" := ItemJournalLine."Item Category Code";
        //TODO ValueEntry."Product Group Code" := ItemJournalLine."Product Group Code";
        ValueEntry."Campaign No." := ItemJournalLine."Campaign No.";
        IF ValueEntry."Campaign No." <> '' THEN BEGIN
            Campaign.GET(ValueEntry."Campaign No.");
            ValueEntry.Promo := Campaign.Promo;
            ValueEntry."Exclure RFA" := Campaign."Exclure RFA";
        END;

        // Si la commande est exclure RFA, on force sinon on prend la campagne
        IF ItemJournalLine."Exclure RFA" THEN
            ValueEntry."Exclure RFA" := TRUE;

        // AD Le 24-03-2015 => No de BL fournisseur … la ligne de r‚ception
        ValueEntry."External Document No. 2" := ItemJournalLine."External Document No. 2";
        // FIN AD Le 24-03-2015


        // AD Le 24-06-2011 => SYMTA
        ValueEntry."Requested Delivery Date" := ItemJournalLine."Requested Delivery Date";
        IF ValueEntry."Requested Delivery Date" = 0D THEN
            ValueEntry."Requested Delivery Date" := ValueEntry."Posting Date";

        //ValueEntry."Commentaire" := "Commentaire";
        // FIN AD Le 24-06-2011

        ValueEntry."Opération ADV" := ItemJournalLine."Opération ADV";
        ValueEntry."Commission %" := ItemJournalLine."Commission %";

        ValueEntry."Sales Line type" := ItemJournalLine."Sales Line type";
        ValueEntry."Document Code" := ItemJournalLine."Document Code";
        ValueEntry."Source Responsable" := ItemJournalLine."Source Responsable";

        IF ValueEntry."Source Type" = ValueEntry."Source Type"::Customer THEN
            IF LCust.GET(ValueEntry."Source No.") THEN BEGIN
                ValueEntry."Territory Code" := LCust."Territory Code";
                ValueEntry."Source Family Code 1" := LCust."Family Code 1";
                ValueEntry."Source Family Code 2" := LCust."Family Code 2";
                ValueEntry."Source Family Code 3" := LCust."Family Code 3";
                ValueEntry."Source Activity Code" := LCust."Activity Code";
                ValueEntry."Source Direction Code" := LCust."Direction Code";
                ValueEntry.Holding := LCust.Holding; // AD Le 29-01-2020
            END;


        Item.GET(ItemJournalLine."Item No.");
        ValueEntry."Hors Normes" := Item.GetHorsNorme(ItemJournalLine.Quantity);

        // Pour les achats on fait suivre la devise et le taux
        ValueEntry."Currency Code" := ItemJournalLine."Currency Code";
        ValueEntry."Currency Factor" := ItemJournalLine."Currency Factor";

        // FIN AD Le 07-09-2009

        // AD Le 17-09-2009 => Non Stock‚
        ValueEntry."Sans mouvement de stock" := ItemJournalLine."Sans mouvement de stock";
        // FIN AD Le 17-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeInsertCorrItemLedgEntry', '', false, false)]
    local procedure CU22_OnBeforeInsertCorrItemLedgEntry(var NewItemLedgerEntry: Record "Item Ledger Entry"; var OldItemLedgerEntry: Record "Item Ledger Entry"; var ItemJournalLine: Record "Item Journal Line")
    begin
        // MC Le 31-01-2013 => Stats conso retour … la date de l'‚criture d'origine
        NewItemLedgerEntry."Return Appl.-from Item Entry" := OldItemLedgerEntry."Return Appl.-from Item Entry";
        // FIN MC Le 31-01-2013
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnAfterInsertCorrItemLedgEntry', '', false, false)]
    local procedure CU22_OnAfterInsertCorrItemLedgEntry(var NewItemLedgerEntry: Record "Item Ledger Entry"; var ItemJournalLine: Record "Item Journal Line"; var OldItemLedgerEntry: Record "Item Ledger Entry")
    var
        CodeunitsFunctions: codeunit "Codeunits Functions";
    begin
        // MC Le 31-01-2013 => Prise en compte des annulations de livraison dans les consomations
        CodeunitsFunctions.InsertStatConso(NewItemLedgerEntry);
        // FIN MC Le 31-01-2013
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Line", 'OnBeforeGetSourceNo', '', false, false)]
    local procedure CU22_OnBeforeGetSourceNo(ItemJournalLine: Record "Item Journal Line"; var Result: Code[20]; var IsHandled: Boolean)
    begin
        Result := ItemJournalLine."Source No.";
        IsHandled := true;
    end;
    //<< Mig CU 22
    //>> Mig CU 23
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Batch", 'OnBeforeCode', '', false, false)]
    local procedure CU23_OnBeforeCode(var ItemJournalLine: Record "Item Journal Line")
    var
        CodeunitsFunctions: codeunit "Codeunits Functions";
    begin
        // CFR Le 04/09/2020 : Comportement WIIO > on ajoute une ligne Picking … la validation d'une ligne surstock
        CodeunitsFunctions.ValiderLigneReclassement(ItemJournalLine);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Batch", 'OnBeforeRaiseExceedLengthError', '', false, false)]
    local procedure CU23_OnBeforeRaiseExceedLengthError(var ItemJournalBatch: Record "Item Journal Batch"; var RaiseError: Boolean)
    var
        ESK000Txt: Label 'cannot exceed %1 characters', Comment = '%1 = Characterers Max length';
    begin
        // CFR Le 16/09/2020 : on incr‚mente une feuille de type WIIO
        //IF NOT ItemJournalBatch.PSM THEN // ANI Le 07-06-2017 on incr‚mente pas une feuille PSM
        IF NOT (ItemJournalBatch.PSM OR (ItemJournalBatch."Type Feuille Reclassement WIIO" <> ItemJournalBatch."Type Feuille Reclassement WIIO"::" ")) THEN
            // FIN CFR Le 16/09/2020 : on incr‚mente une feuille de type WIIO
            IF STRLEN(INCSTR(ItemJournalBatch.Name)) > MAXSTRLEN(ItemJournalBatch.Name) THEN
                ItemJournalBatch.FIELDERROR(
                  Name,
                  STRSUBSTNO(
                    ESK000Txt,
                    MAXSTRLEN(ItemJournalBatch.Name)));
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Batch", 'OnBeforeIncrBatchName', '', false, false)]
    local procedure CU23_OnBeforeIncrBatchName(var ItemJournalLine: Record "Item Journal Line"; var IncrBatchName: Boolean)
    var
        ItemJnlBatch: Record "Item Journal Batch";
    begin
        if ItemJnlBatch.Get(ItemJournalLine."Journal Template Name", ItemJournalLine."Journal Batch Name") then
            // CFR Le 16/09/2020 : on incr‚mente une feuille de type WIIO
            //IF NOT ItemJnlBatch.PSM THEN // ANI Le 07-06-2017 on incr‚mente pas une feuille PSM
            IF (ItemJnlBatch.PSM OR (ItemJnlBatch."Type Feuille Reclassement WIIO" <> ItemJnlBatch."Type Feuille Reclassement WIIO"::" ")) THEN
                // FIN CFR Le 16/09/2020 : on incr‚mente une feuille de type WIIO
                IncrBatchName := false;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post Batch", 'OnHandleNonRecurringLineOnAfterItemJnlLineModify', '', false, false)]
    local procedure CU23_OnHandleNonRecurringLineOnAfterItemJnlLineModify(var ItemJournalLine: Record "Item Journal Line")
    begin
        // MCO Le 10-12-2015 => Ticket 6593 => Migration
        // MC LE 17-07-2012
        // On fait suivre automatiquement en num‚ro de document le nom de la feuille
        IF ItemJournalLine.MiniLoad THEN begin
            ItemJournalLine."Document No." := ItemJournalLine."Journal Batch Name";
            ItemJournalLine.modify();
        end;
        // FIN MC
        // MCO Le 10-12-2015 => Ticket 6593 => Migration
    end;
    //<< Mig CU 23
    //>> Mig CU 57
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Document Totals", 'OnAfterSalesLineSetFilters', '', false, false)]
    local procedure CU57_OnAfterSalesLineSetFilters(var TotalSalesLine: Record "Sales Line"; SalesLine: Record "Sales Line")
    var
        recL_SalesLine: Record "Sales Line";
        GrossWeight: Decimal;
    begin
        TotalSalesLine.CALCSUMS("VAT Difference", "Gross Weight");
        // MCO Le 15-01-2018 => Pour le poids il faut calculer en fonction de la quantité donc obliger de parcourir les lignes.

        recL_SalesLine.SETRANGE("Document Type", SalesLine."Document Type");
        recL_SalesLine.SETRANGE("Document No.", SalesLine."Document No.");
        IF recL_SalesLine.FINDSET() THEN
            REPEAT
                GrossWeight += ROUND(recL_SalesLine.Quantity * recL_SalesLine."Gross Weight", 0.00001);
            UNTIL recL_SalesLine.NEXT() = 0;
        TotalSalesLine."Gross Weight" := GrossWeight;

        // MCO Le 21-11-2017 => Calcul du poids
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Document Totals", 'OnAfterPurchaseLineSetFilters', '', false, false)]
    local procedure CU57_OnAfterPurchaseLineSetFilters(var TotalPurchaseLine: Record "Purchase Line"; PurchaseLine: Record "Purchase Line")
    var
        recL_PurchLine: Record "Purchase Line";
        GrossWeight: Decimal;
    begin
        // MCO Le 15-01-2018 => Pour le poids il faut calculer en fonction de la quantit‚ donc obliger de parcourir les lignes.
        recL_PurchLine.SETRANGE("Document Type", PurchaseLine."Document Type");
        recL_PurchLine.SETRANGE("Document No.", PurchaseLine."Document No.");
        IF recL_PurchLine.FINDSET() THEN
            REPEAT
                GrossWeight += ROUND(recL_PurchLine.Quantity * recL_PurchLine."Gross Weight", 0.00001);
            UNTIL recL_PurchLine.NEXT() = 0;
        TotalPurchaseLine."Gross Weight" := GrossWeight;
        // MCO Le 21-11-2017 => Calcul du poids
    end;
    //<< Mig CU 57
    //>> Mig CU 60
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Calc. Discount", 'OnRunOnBeforeCalculateInvoiceDiscount', '', false, false)]
    local procedure CU60_OnRunOnBeforeCalculateInvoiceDiscount(var SalesLine: Record "Sales Line"; var TempSalesHeader: Record "Sales Header" temporary; var TempSalesLine: Record "Sales Line" temporary; var UpdateHeader: Boolean; var IsHandled: Boolean)
    begin
        //TMP
        IF SalesLine."Document No." = '' THEN
            IsHandled := true;
        //
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Calc. Discount", 'OnBeforeCalculateIncDiscForHeader', '', false, false)]
    local procedure CU60_OnBeforeCalculateIncDiscForHeader(var TempSalesHeader: Record "Sales Header" temporary; var IsHandled: Boolean; var SalesLine: Record "Sales Line"; var TempSalesLine: Record "Sales Line" temporary; var UpdateHeader: Boolean)
    var
        CustInvoiceDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
    begin
        //FBRUN ->22/09/2009 AJOUT LIGNES REMISE SUPLEMENTAIRE
        CustInvoiceDiscSup.Calc_Disc_line(TempSalesHeader, 0);
        CustInvoiceDiscSup.Calc_Disc_Header(TempSalesHeader, 0);
    end;
    //<< Mig CU 60
    //>> Mig CU 63
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Explode BOM", 'OnBeforeToSalesLineModify', '', false, false)]
    local procedure CU63_OnBeforeToSalesLineModify(var ToSalesLine: Record "Sales Line"; FromSalesLine: Record "Sales Line")
    begin
        ToSalesLine.Type := ToSalesLine.Type::" "; // AD Le 04-11-2015
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Explode BOM", 'OnExplodeBOMCompLinesOnAfterAssignType', '', false, false)]
    local procedure CU63_OnExplodeBOMCompLinesOnAfterAssignType(var ToSalesLine: Record "Sales Line"; SalesLine: Record "Sales Line"; BOMComponent: Record "BOM Component"; var SalesHeader: Record "Sales Header");
    var
        GestionMultiref: Codeunit "Gestion Multi-référence";
    begin
        IF ToSalesLine.Type <> ToSalesLine.Type::" " THEN BEGIN
            BOMComponent.TESTFIELD("No.");

            // AD Le 24-11-2011
            ToSalesLine.SetHideValidationDialog(TRUE);
            // FIN AD Le 24-11-2011


            // AD Le 29-11-2011
            ToSalesLine."Recherche référence" := GestionMultiref.RechercheRefActive(ToSalesLine."No.");
            // FIN AD Le 29-11-2011
            // MCO Le 31-03-2015 => R‚gie
            ToSalesLine."référence saisie" := SalesLine."référence saisie";
            // FIN MCO Le 31-03-2015
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Explode BOM", 'OnInsertOfExplodedBOMLineToSalesLine', '', false, false)]
    local procedure CU63_OnInsertOfExplodedBOMLineToSalesLine(var ToSalesLine: Record "Sales Line"; SalesLine: Record "Sales Line"; BOMComponent: Record "BOM Component"; var SalesHeader: Record "Sales Header"; LineSpacing: Integer)
    begin
        if ToSalesLine.Type <> ToSalesLine.Type::" " then
            // AD Le 29-11-2011
            if ToSalesLine.Type = ToSalesLine.Type::Item then
                ToSalesLine.VALIDATE("Qty. to Ship", ToSalesLine.CalcQteLivrableSYMPA(15));
        // FIN AD Le 29-11-2011
        ToSalesLine.Description := 'Kit ' + ToSalesLine.Description;// AD Le 18-11-2015 => MIG2015
    end;
    //<< Mig CU 63
    //>> Mig CU 64
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Get Shipment", 'OnRunAfterFilterSalesShpLine', '', false, false)]
    local procedure CU64_OnRunAfterFilterSalesShpLine(var SalesShptLine: Record "Sales Shipment Line"; SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    begin
        //fbRUN BUG SUR LES RESSOURCES LIE A UNR LIGNE
        SalesShptLine.SETRANGE("Attached to Line No.", 0);
    end;
    //<< Mig CU 64
    //>> Mig CU 74
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Get Receipt", 'OnAfterPurchRcptLineSetFilters', '', false, false)]
    local procedure CU74_OnAfterPurchRcptLineSetFilters(var PurchRcptLine: Record "Purch. Rcpt. Line"; PurchaseHeader: Record "Purchase Header")
    begin
        PurchRcptLine.Reset();
        //PurchRcptLine.SETCURRENTKEY("Pay-to Vendor No.");
        //PurchRcptLine.SETCURRENTKEY("Pay-to Vendor No.","Document No.","Line No.");
        PurchRcptLine.SETCURRENTKEY("Pay-to Vendor No.", "Document No.", "Qty. Rcd. Not Invoiced");
        PurchRcptLine.SETRANGE("Pay-to Vendor No.", PurchaseHeader."Pay-to Vendor No.");
        //PurchRcptLine.SETRANGE("Buy-from Vendor No.",PurchHeader."Buy-from Vendor No.");
        PurchRcptLine.SETFILTER("Qty. Rcd. Not Invoiced", '<>0');
        PurchRcptLine.SETRANGE("Currency Code", PurchaseHeader."Currency Code");
        PurchRcptLine.SETRANGE("Déjà Extraite sur Facture", FALSE); // AD le 19-06-2015 => Avavt dans la form
        PurchRcptLine.SETFILTER("Whse. Receipt No.", '<>%1', '');
        //PurchRcptLine.SETFILTER("Whse. Receipt No.",'<>%1|%2','',''); CT le 23/03/2018 ticket 30216
    end;
    //<< Mig CU 74
    //>> Mig CU 83
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order (Yes/No)", 'OnBeforeRun', '', false, false)]
    local procedure CU83_OnBeforeRun(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    var
        LSalesLine: Record "Sales Line";
        ESK001_Qst: Label 'ATTENTION ! Au moins une ligne en dimension spéciale ! Prévoir du port ! Continuer ?';
    begin
        SalesHeader.TestField("Document Type", SalesHeader."Document Type"::Quote);
        // AD Le 04-05-2016 =>
        IF SalesHeader."Devis Web" THEN
            SalesHeader.TESTFIELD("Devis Web Intégré", TRUE);
        // FIN AD Le 04-05-2016
        // AD Le 24-03-2016 =>
        IF GUIALLOWED THEN BEGIN
            CLEAR(LSalesLine);
            LSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
            LSalesLine.SETRANGE("Document No.", SalesHeader."No.");
            LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
            LSalesLine.SETRANGE("Dimension Hors Norme", TRUE);
            IF NOT LSalesLine.ISEMPTY() THEN
                IF NOT CONFIRM(ESK001_Qst, TRUE) THEN
                    IsHandled := true;
        END;
        // FIN AD Le 24-03-2016
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order (Yes/No)", 'OnBeforeConfirmConvertToOrder', '', false, false)]
    local procedure CU83_OnBeforeConfirmConvertToOrder(SalesHeader: Record "Sales Header"; var Result: Boolean; var IsHandled: Boolean)
    var
        ConfirmConvertToOrderQst: Label 'Do you want to convert the quote to an order?';
    begin
        IsHandled := true;

        if GuiAllowed then
            if not Confirm(ConfirmConvertToOrderQst, true) then
                Result := false;
    end;
    //<< Mig CU 83
    //>> Mig CU 86
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order", 'OnBeforeCreateSalesHeader', '', false, false)]
    local procedure CU86_OnBeforeCreateSalesHeader(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.ValidationCentrale(); // AD Le 10-11-2016 => Demande de Philippe

        //MC Le 19-05-2010 => Analyse compl‚mentaire Les utilisateurs doivent pouvoir archiver le devis
        SalesHeader.AskArchivage();
        //FIN MC
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order", 'OnBeforeModifySalesOrderHeader', '', false, false)]
    local procedure CU86_OnBeforeModifySalesOrderHeader(var SalesOrderHeader: Record "Sales Header"; SalesQuoteHeader: Record "Sales Header")
    begin
        SalesOrderHeader."Type de commande" := SalesQuoteHeader."Type de commande"; // AD Le 02-12-2011
        SalesOrderHeader."Source Document Type" := SalesQuoteHeader."Source Document Type"; // DZ Le 15-06-2012
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order", 'OnTransferQuoteToOrderLinesOnBeforeDefaultDeferralCode', '', false, false)]
    local procedure CU86_OnTransferQuoteToOrderLinesOnBeforeDefaultDeferralCode(var SalesLineOrder: Record "Sales Line"; var SalesHeaderOrder: Record "Sales Header"; var SalesLineQuote: Record "Sales Line"; var IsHandled: Boolean)
    begin
        // MCO Le 31-03-2015 => R‚gie
        SalesLineOrder.SetHideQuoteToOrder(TRUE);
        IF NOT SalesLineQuote.Urgent THEN // CFR le 06/04/2023 - R‚gie : Date de livraison demand‚e est fonction du champ Urgent
            SalesLineOrder.VALIDATE("Requested Delivery Date", SalesHeaderOrder."Requested Delivery Date");
        // MCO Le 31-03-2015 => R‚gie
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Quote to Order", 'OnAfterInsertSalesOrderLine', '', false, false)]
    local procedure CU86_OnAfterInsertSalesOrderLine(var SalesOrderLine: Record "Sales Line"; SalesOrderHeader: Record "Sales Header"; SalesQuoteLine: Record "Sales Line"; SalesQuoteHeader: Record "Sales Header")
    var
        rec_item: Record Item;
        rec_demande_client: Record demande_client;
    begin
        // LM le 07-08-2012 trace toutes les demandes clients
        IF rec_item.GET(SalesOrderLine."No.") THEN BEGIN
            rec_item.CALCFIELDS(Inventory);
            rec_demande_client.INIT();
            rec_demande_client.date_demande := CREATEDATETIME(WORKDATE(), TIME);
            rec_demande_client.code_client := SalesOrderLine."Sell-to Customer No.";
            rec_demande_client.référence := SalesOrderLine."No.";
            rec_demande_client.qte_demandé := SalesOrderLine.Quantity;
            rec_demande_client.qte_stock := rec_item.Inventory;
            rec_demande_client.user := USERID();
            // AD Le 27-09-2016 => REGIE -> Pb De Cl‚ si on a 2 fois la ref dans NAV
            rec_demande_client.VALIDATE("No Document", SalesOrderLine."Document No.");
            rec_demande_client.VALIDATE("No Ligne Document", SalesOrderLine."Line No.");
            // FIN AD Le 27-09-2016
            rec_demande_client.INSERT();
        END;
        // FIN LM Le 07-08-2012
    end;
    //<< Mig CU 86
    //>> Mig CU 87
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnRunOnAfterBlanketOrderSalesLineSetFilters', '', false, false)]
    local procedure CU87_OnRunOnAfterBlanketOrderSalesLineSetFilters(var BlanketOrderSalesLine: Record "Sales Line")
    begin
        // AD Le 28-10-2009 => Que les ligne avec une qte > 0
        BlanketOrderSalesLine.SETFILTER("Qty. to Ship", '>%1', 0);
        // FIN AD Le 28-10-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnRunOnBeforeResetQuantityFields', '', false, false)]
    local procedure CU87_OnRunOnBeforeResetQuantityFields(var BlanketOrderSalesLine: Record "Sales Line"; var SalesOrderLine: Record "Sales Line")
    begin
        // AD Le 08-10-2009 => FARGROUP -> Pouvoir changer le client lors de la transformation
        SalesOrderLine.VALIDATE("Sell-to Customer No.", BlanketOrderSalesLine."Sell-to Customer No.");
        SalesOrderLine.VALIDATE("Bill-to Customer No.", BlanketOrderSalesLine."Bill-to Customer No.");
        // FIN AD Le 08-10-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnBeforeSalesOrderLineValidateQuantity', '', false, false)]
    local procedure CU87_OnBeforeSalesOrderLineValidateQuantity(var SalesOrderLine: Record "Sales Line"; BlanketOrderSalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        // AD Le 04-11-2009
        SalesOrderLine.SetHideValidationDialog(TRUE);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnBeforeInsertSalesOrderLine', '', false, false)]
    local procedure CU87_OnBeforeInsertSalesOrderLine(var SalesOrderLine: Record "Sales Line"; SalesOrderHeader: Record "Sales Header"; BlanketOrderSalesLine: Record "Sales Line"; BlanketOrderSalesHeader: Record "Sales Header")
    var
        BlanketOrderSalesLineCharge: Record "Sales Line";
    begin
        // AD Le 05-11-2009 => DEEE -> On force la copie de la ligne de DEEE
        BlanketOrderSalesLineCharge.RESET();
        BlanketOrderSalesLineCharge.SETRANGE("Document Type", BlanketOrderSalesHeader."Document Type");
        BlanketOrderSalesLineCharge.SETRANGE("Document No.", BlanketOrderSalesHeader."No.");
        BlanketOrderSalesLineCharge.SETRANGE(Type, BlanketOrderSalesLine.Type::"Charge (Item)");
        BlanketOrderSalesLineCharge.SETRANGE("Attached to Line No.", BlanketOrderSalesLine."Line No.");
        IF BlanketOrderSalesLineCharge.FINDFIRST() THEN BEGIN
            BlanketOrderSalesLineCharge.VALIDATE("Qty. to Ship", BlanketOrderSalesLine."Qty. to Ship");
            BlanketOrderSalesLineCharge.MODIFY();
        END;
        // FIN Le 05-11-2009
        // AD Le 30-11-2009 => DEEE -> Passage en ressource
        BlanketOrderSalesLineCharge.RESET();
        BlanketOrderSalesLineCharge.SETRANGE("Document Type", BlanketOrderSalesHeader."Document Type");
        BlanketOrderSalesLineCharge.SETRANGE("Document No.", BlanketOrderSalesHeader."No.");
        BlanketOrderSalesLineCharge.SETRANGE(Type, BlanketOrderSalesLine.Type::Resource);
        BlanketOrderSalesLineCharge.SETRANGE("Attached to Line No.", BlanketOrderSalesLine."Line No.");
        IF BlanketOrderSalesLineCharge.FINDFIRST() THEN BEGIN
            BlanketOrderSalesLineCharge.VALIDATE("Qty. to Ship", BlanketOrderSalesLine."Qty. to Ship");
            BlanketOrderSalesLineCharge.MODIFY();
        END;
        // FIN AD Le 30-11-2009

        // AD Le 21-10-2009 => Pour suivre les qte transform‚e en commande
        SalesOrderLine."Blanket Order Quantity Outstan" := 0;

        SalesOrderLine.GetComission();
        // FIN AD Le
    end;



    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnRunOnBeforeValidateBlanketOrderSalesLineQtytoShip', '', false, false)]
    local procedure CU87_OnRunOnBeforeValidateBlanketOrderSalesLineQtytoShip(var BlanketOrderSalesLine: Record "Sales Line"; SalesOrderLine: Record "Sales Line"; SalesOrderHeader: Record "Sales Header"; BlanketOrderSalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    VAR
        CUTransferCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 21-10-2009 => DEEE
        IF SalesOrderLine.Type = SalesOrderLine.Type::"Charge (Item)" THEN
            CUTransferCharge.AssignChargeItemLink(SalesOrderLine);
        IF SalesOrderLine.Type = SalesOrderLine.Type::Resource THEN
            CUTransferCharge.AssignChargeItemLink(SalesOrderLine);
        // FIN AD Le 21-10-2009
        if BlanketOrderSalesLine."Qty. to Ship" <> 0 then begin
            // AD Le 21-10-2009 => Pour suivre les qte transform‚e en commande
            BlanketOrderSalesLine."Blanket Order Quantity Outstan" -= BlanketOrderSalesLine."Qty. to Ship";
            BlanketOrderSalesLine.Modify();
            // FIN AD Le 21-10-2009
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnBeforeInsertSalesOrderHeader', '', false, false)]
    local procedure CU87_OnBeforeInsertSalesOrderHeader(var SalesOrderHeader: Record "Sales Header"; var BlanketOrderSalesHeader: Record "Sales Header")
    begin
        // AD Le 08-10-2009 => FARGROUP -> Pouvoir changer le client lors de la transformation
        BlanketOrderSalesHeader.TESTFIELD("Client final commande ouverte");
        SalesOrderHeader.VALIDATE("Sell-to Customer No.", BlanketOrderSalesHeader."Client final commande ouverte");
        BlanketOrderSalesHeader."Client final commande ouverte" := '';
        BlanketOrderSalesHeader.MODIFY();
        // FIN AD Le 08-10-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Blanket Sales Order to Order", 'OnCreateSalesHeaderOnAfterSalesOrderHeaderInsert', '', false, false)]
    local procedure CU87_OnCreateSalesHeaderOnAfterSalesOrderHeaderInsert(SalesHeader: Record "Sales Header"; var SalesOrderHeader: Record "Sales Header")
    begin
        //MC Le 30-09-2010 Validate sur la date comptabilit‚ pour modifier date ‚ch‚ance
        SalesOrderHeader.VALIDATE("Posting Date");
        //FIN MC Le 30-09-2010
    end;
    //<< Mig CU 87
    //>>Mig CU 241
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Jnl.-Post", 'OnBeforeCode', '', false, false)]
    local procedure CU241_OnBeforeCode(var ItemJournalLine: Record "Item Journal Line"; var HideDialog: Boolean; var IsHandled: Boolean; var SuppressCommit: Boolean)
    begin
        // MCO Le 27-09-2016 => R‚gie
        IF CduGSingleInstance.GetHideValidationDialog() THEN
            IF CduGSingleInstance.GetHideValidationDialog() THEN
                // FIN MCO Le 27-09-2016
                HideDialog := true;
    end;
    //<<Mig CU 241
    //>>Mig CU 260
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Document-Mailing", 'OnBeforeGetEmailSubject', '', false, false)]
    local procedure CU260_OnBeforeGetEmailSubject(PostedDocNo: Code[20]; EmailDocumentName: Text[250]; ReportUsage: Integer; var EmailSubject: Text[250]; var IsHandled: Boolean)
    var
        EmailParameter: Record "Email Parameter";
        CompanyInformation: Record "Company Information";
        EmailSubjectPluralCapTxt: Label '%1 - %2', Comment = '%1 = RecipientName ; %2 = EmailDocumentName';
        EmailSubjectCapTxt: Label '%1 =  - %2 %3', Comment = '%1 = RecipientName ; %2 = EmailDocumentName ; %3 = PostedDocNo';
    begin
        IsHandled := true;

        if EmailParameter.GetParameterWithReportUsage(PostedDocNo, "Report Selection Usage".FromInteger(ReportUsage), EmailParameter."Parameter Type"::Subject) then
            EmailSubject := CopyStr(EmailParameter.GetParameterValue(), 1, 250);
        CompanyInformation.Get();
        // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
        IF CduGSingleInstance.GetRecipientName() = '' THEN
            CduGSingleInstance.SetRecipientName(CompanyInformation.Name);
        // CU260_txt_RecipientName := CompanyInformation.Name;
        // FIN MCO Le 15-01-2018
        if PostedDocNo = '' then
            EmailSubject := CopyStr(
                              // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
                              //StrSubstNo(EmailSubjectPluralCapTxt, CompanyInformation.Name, EmailDocumentName), 1, MaxStrLen(EmailSubject))
                              STRSUBSTNO(EmailSubjectPluralCapTxt, CduGSingleInstance.GetRecipientName(), EmailDocumentName), 1, MAXSTRLEN(EmailSubject))
        // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
        else
            EmailSubject := CopyStr(
                              // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015
                              //StrSubstNo(EmailSubjectCapTxt, CompanyInformation.Name, EmailDocumentName, PostedDocNo), 1, MaxStrLen(EmailSubject))
                              STRSUBSTNO(EmailSubjectCapTxt, CduGSingleInstance.GetRecipientName(), EmailDocumentName, PostedDocNo), 1, MAXSTRLEN(EmailSubject));
        // MCO Le 15-01-2018 => Pour alimenter le sujet du mail comme en 2015

        CduGSingleInstance.SetRecipientName('');
    end;
    //<<Mig CU 260


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item-Check Avail.", 'OnBeforeShowWarningForThisItem', '', false, false)]
    local procedure CU311_OnBeforeShowWarningForThisItem(Item: Record Item; var ShowWarning: Boolean; var IsHandled: Boolean)
    begin
        IsHandled := true;
        // AD Le 21-10-2009 => Non stock‚ -> Pas d'alerte pour les articles non stock‚s
        //IF Item.GET(ItemNo) THEN
        IF Item."Sans Mouvements de Stock" THEN
            ShowWarning := FALSE;
        // FIN AD Le 21-10-2009
    end;
    //<< Mig CU 311


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", 'OnAfterCode', '', false, false)]
    local procedure CU333_OnAfterCode(var RequisitionLine: Record "Requisition Line"; OrderLineCounter: Integer; OrderCounter: Integer; PrintPurchOrders: Boolean; SuppressCommit: Boolean)
    var
        RecPrintOrder: Record "Purchase Header";
    begin
        RecPrintOrder := CduGSingleInstance.FctGetRecPrintOrder();
        //FBRUN Impression de la liste de commande
        CduGSingleInstance.FctSetRecPrintOrder(True);
        IF PrintPurchOrders THEN
            REPORT.RUN(50162, FALSE, TRUE, RecPrintOrder);
        CduGSingleInstance.FctSetRecPrintOrder(false);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", 'OnInitPurchOrderLineOnAfterValidateLineDiscount', '', false, false)]
    local procedure CU333_OnInitPurchOrderLineOnAfterValidateLineDiscount(var PurchOrderLine: Record "Purchase Line"; PurchOrderHeader: Record "Purchase Header"; RequisitionLine: Record "Requisition Line")
    begin
        CduGSingleInstance.CU333_FctSetDesc(PurchOrderLine.Description, PurchOrderLine."Description 2");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", 'OnAfterInitPurchOrderLine', '', false, false)]
    local procedure CU333_OnAfterInitPurchOrderLine(var PurchaseLine: Record "Purchase Line"; RequisitionLine: Record "Requisition Line")
    begin
        CduGSingleInstance.CU333_FctGetDesc(PurchaseLine);
        CduGSingleInstance.CU333_FctSetDesc('', '');
        if RequisitionLine."Due Date" <> 0D then begin
            PurchaseLine.Validate("Expected Receipt Date", 0D);
            PurchaseLine."Requested Receipt Date" := 0D;
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", OnBeforePrintPurchOrder, '', false, false)]
    local procedure CU333_OnBeforePrintPurchOrderOnBeforePrintPurchOrder(PurchaseHeader: Record "Purchase Header"; PrintPurchOrders: Boolean; var IsHandled: Boolean)
    var
        CarryOutAction: Codeunit "Carry Out Action";
    begin
        IsHandled := true;

        if PurchaseHeader."No." <> '' then begin
            CarryOutAction.SetPrintOrder(PrintPurchOrders);
            //CarryOutAction.PrintPurchaseOrder(PurchaseHeader);
            //FBRUN Impression de la liste de commande
            CduGSingleInstance.CU333_FctGetRecord(PurchaseHeader);
        end;
    end;
    //<< Mig CU 333
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", 'OnInitPurchOrderLineOnBeforeValidateLineDiscount', '', false, false)]
    local procedure CU333_OnInitPurchOrderLineOnBeforeValidateLineDiscount(var PurchOrderLine: Record "Purchase Line"; PurchOrderHeader: Record "Purchase Header"; RequisitionLine: Record "Requisition Line")
    begin
        // AD Le 17-11-2011 => Ajustement de la quantit‚
        // MCO Le 04-10-2108 => AJustement de la quantit‚ => R‚gie voir nathalie
        PurchOrderLine.ArrondirQte(RequisitionLine."Price Qty");
        // FIN AD Le 17-11-2011
    end;



    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Req. Wksh.-Make Order", 'OnAfterInsertPurchOrderHeader', '', false, false)]
    local procedure CU333_OnAfterInsertPurchOrderHeader(var RequisitionLine: Record "Requisition Line"; var PurchaseOrderHeader: Record "Purchase Header"; CommitIsSuppressed: Boolean; SpecialOrder: Boolean)
    begin
        // AD Le 29-11-2011 => Toute les commandes de l'appro sont du m^zme type
        PurchaseOrderHeader.VALIDATE("Type de commande", RequisitionLine."Type de commande");
    end;
    //<< Mig CU 333
    //>> Mig CU 365
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Format Address", 'OnFormatAddrOnAfterGetCountry', '', false, false)]
    local procedure CU365_OnFormatAddrOnAfterGetCountry(var AddrArray: array[8] of Text[100]; var Name: Text[100]; var Name2: Text[100]; var Contact: Text[100]; var Addr: Text[100]; var Addr2: Text[50]; var City: Text[50]; var PostCode: Code[20]; var County: Text[50]; var CountryCode: Code[10]; LanguageCode: Code[10]; var IsHandled: Boolean; var Country: Record "Country/Region")
    begin
        //FBRUN SUPPRESSION DU CONTACT
        Contact := '';
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Format Address", 'OnBeforeSalesInvSellTo', '', false, false)]
    local procedure CU365_OnBeforeSalesInvSellTo(var AddrArray: array[8] of Text[100]; var SalesInvoiceHeader: Record "Sales Invoice Header"; var Handled: Boolean)
    var
        FormatAddress: Codeunit "Format Address";
    begin
        Handled := true;

        FormatAddress.FormatAddr(
                    //FBRUN LE 17/11/11 pas de contact dans l'adresse de facturation
                    //AddrArray, SalesInvoiceHeader."Sell-to Customer Name", SalesInvoiceHeader."Sell-to Customer Name 2", SalesInvoiceHeader."Sell-to Contact", SalesInvoiceHeader."Sell-to Address", SalesInvoiceHeader."Sell-to Address 2",
                    AddrArray, SalesInvoiceHeader."Sell-to Customer Name", SalesInvoiceHeader."Sell-to Customer Name 2", '', SalesInvoiceHeader."Sell-to Address", SalesInvoiceHeader."Sell-to Address 2",
          SalesInvoiceHeader."Sell-to City", SalesInvoiceHeader."Sell-to Post Code", SalesInvoiceHeader."Sell-to County", SalesInvoiceHeader."Sell-to Country/Region Code");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Format Address", 'OnBeforeSalesInvBillTo', '', false, false)]
    local procedure CU365_OnBeforeSalesInvBillTo(var AddrArray: array[8] of Text[100]; var SalesInvHeader: Record "Sales Invoice Header"; var Handled: Boolean)
    var
        FormatAddress: Codeunit "Format Address";
    begin
        Handled := true;

        FormatAddress.FormatAddr(
                        //AddrArray, SalesInvHeader."Bill-to Name", SalesInvHeader."Bill-to Name 2", SalesInvHeader."Bill-to Contact", SalesInvHeader."Bill-to Address", SalesInvHeader."Bill-to Address 2",
                        AddrArray, SalesInvHeader."Bill-to Name", SalesInvHeader."Bill-to Name 2", '', SalesInvHeader."Bill-to Address", SalesInvHeader."Bill-to Address 2", SalesInvHeader."Bill-to City", SalesInvHeader."Bill-to Post Code", SalesInvHeader."Bill-to County", SalesInvHeader."Bill-to Country/Region Code");
    end;
    //<< Mig CU 365
    //>> Mig CU 375
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bank Acc. Entry Set Recon.-No.", 'OnBeforeModifyBankAccReconLine', '', false, false)]
    local procedure CU375_OnBeforeModifyBankAccReconLine(var BankAccReconciliationLine: Record "Bank Acc. Reconciliation Line")
    begin
        //ESK0.1
        BankAccReconciliationLine.Select := FALSE;
        //ESK0.1
    end;
    //<< Mig CU 375
    //>> Mig CU 391
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Shipment Header - Edit", 'OnBeforeSalesShptHeaderModify', '', false, false)]
    local procedure CU391_OnBeforeSalesShptHeaderModify(var SalesShptHeader: Record "Sales Shipment Header"; FromSalesShptHeader: Record "Sales Shipment Header")
    begin
        SalesShptHeader."En attente de facturation" := FromSalesShptHeader."En attente de facturation";

        // CFR le 22/09/2021 => R‚gie - Suivre l'utilisateur qui a mis en attente
        SalesShptHeader."User En Attente" := FromSalesShptHeader."User En Attente";

        // AD Le 17-04-2012
        SalesShptHeader."Chiffrage BL" := FromSalesShptHeader."Chiffrage BL";
        // FIN AD Le 17-04-2012
    end;
    //<< Mig CU 391
    //>> Mig CU 392
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reminder-Make", 'OnMarkReminderCandidateOnAfterCalcIsGracePeriodExpired', '', false, false)]
    local procedure CU392_OnMarkReminderCandidateOnAfterCalcIsGracePeriodExpired(var ReminderLevel: Record "Reminder Level"; var ReminderDueDate: Date; var ReminderHeaderReq: Record "Reminder Header"; var ReminderTerms: Record "Reminder Terms"; var CustLedgerEntry: Record "Cust. Ledger Entry"; var ReminderHeader: Record "Reminder Header"; var LineLevel: Integer; var IsGracePeriodExpired: Boolean)
    begin
        // MIG 2017
        IF ReminderDueDate = 0D THEN
            ReminderDueDate := TODAY();
        // fin MIG 2017
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reminder-Make", 'OnBeforeMakeReminder', '', false, false)]
    local procedure CU392_OnBeforeMakeReminder(CurrencyCode: Code[10])
    begin
        CduGSingleInstance.FctSaveCurrencyCode(CurrencyCode);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reminder-Make", 'OnAfterFilterCustLedgEntryReminderLevel', '', false, false)]
    local procedure CU392_OnAfterFilterCustLedgEntryReminderLevel(var CustLedgerEntry: Record "Cust. Ledger Entry"; var ReminderLevel: Record "Reminder Level"; ReminderTerms: Record "Reminder Terms"; Customer: Record Customer; ReminderHeaderReq: Record "Reminder Header"; ReminderHeader: Record "Reminder Header")
    begin
        CustLedgerEntry.Reset();
        CustLedgerEntry.SetCurrentKey("Customer No.", Open, Positive, "Due Date", "Currency Code");
        CustLedgerEntry.SetRange(Open, true);
        CustLedgerEntry.SetRange("Customer No.", Customer."No.");
        // CustLedgerEntry.SetRange("Due Date");
        CustLedgerEntry.SetRange("Currency Code", CduGSingleInstance.FctGetCurrencyCode());
        ReminderLevel.SetRange("Reminder Terms Code", ReminderTerms.Code);
    end;
    //<< Mig CU 392
    //>> Mig CU 414
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Release Sales Document", 'OnBeforeCalcInvDiscount', '', false, false)]
    local procedure CU414_OnBeforeCalcInvDiscount(var SalesHeader: Record "Sales Header"; PreviewMode: Boolean; var LinesWereModified: Boolean; var SalesLine: Record "Sales Line")
    var
        CustInvoiceDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
    begin
        //FBRUN ->22/09/2009 AJOUT LIGNES REMISE SUPLEMENTAIRE
        CustInvoiceDiscSup.Calc_Disc_line(SalesHeader, 0);
        CustInvoiceDiscSup.Calc_Disc_Header(SalesHeader, 0);
        //FIN FBRUN
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Release Sales Document", 'OnBeforeModifySalesDoc', '', false, false)]
    local procedure CU414_OnBeforeModifySalesDoc(var SalesHeader: Record "Sales Header"; PreviewMode: Boolean; var IsHandled: Boolean)
    var
        LSalesLine: Record "Sales Line";
        SalesSetup: Record "Sales & Receivables Setup";
        Text50000_Err: Label 'No de document externe obligatoire';
        Text50004_Err: Label 'Adresse de facturation non remplie.';
        ESK002Err: Label 'Il existe %1 ligne(s) article sans code !', Comment = '%1 = Nb de lignes';
        ESK003Err: Label 'Il existe %1 ligne(s) article sans %2 !', Comment = '%1 = Nb de lignes ; %2 = Reason Code ';
    begin
        SalesSetup.GET();
        // AD Le 19-09-2009 => Contr“les ESKAPE
        IF SalesHeader."Document Type" <> SalesHeader."Document Type"::Invoice THEN
            IF (SalesSetup."Ext. Doc. No. Mandatory") AND (SalesHeader."External Document No." = '') THEN
                ERROR(Text50000_Err);

        IF (SalesHeader."Bill-to Name" = '') AND (SalesHeader."Bill-to Address" = '') AND (SalesHeader."Bill-to Address 2" = '')
           AND (SalesHeader."Bill-to City" = '') THEN
            ERROR(Text50004_Err);

        SalesHeader.TESTFIELD("Salesperson Code");
        // AD Le 07-12-2015 =>
        CLEAR(LSalesLine);
        LSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
        LSalesLine.SETRANGE("Document No.", SalesHeader."No.");
        LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
        LSalesLine.SETRANGE("No.", '');
        IF NOT LSalesLine.ISEMPTY THEN ERROR(ESK002Err, LSalesLine.COUNT);
        // FIN AD Le 07-12-2015
        // FIN AD Le 19-09-2009

        // AD Le 27-09-2016 => REGIE -> V‚rifi motif sur tout les lignes
        IF SalesHeader."Document Type" = SalesHeader."Document Type"::"Return Order" THEN BEGIN
            CLEAR(LSalesLine);
            LSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
            LSalesLine.SETRANGE("Document No.", SalesHeader."No.");
            LSalesLine.SETRANGE(Type, LSalesLine.Type::Item);
            LSalesLine.SETRANGE("Return Reason Code", '');
            IF NOT LSalesLine.ISEMPTY THEN ERROR(ESK003Err, LSalesLine.COUNT, LSalesLine.FIELDCAPTION("Return Reason Code"));
        END;



        // AD Le 28-11-2011 => SYMTA
        //IF "Mode d'exp‚dition" = "Mode d'exp‚dition"::" " THEN
        //  ERROR(ESK001_Qst);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Release Sales Document", 'OnAfterUpdateSalesDocLines', '', false, false)]
    local procedure CU414_OnAfterUpdateSalesDocLines(var SalesHeader: Record "Sales Header"; var LinesWereModified: Boolean; PreviewMode: Boolean)
    var
        GenereAREDI: Codeunit "Génération AR Edi";
    begin
        SalesHeader.ValidationCentrale(); // AD Le 28-09-2016 => REGIE -> Bloquer la commande si controle centrale


        // AD Le 02-11-2009 => EDI -> Generation de l'AR EDI
        IF SalesHeader."EDI Document" AND (NOT SalesHeader."EDI AR Generate") THEN
            GenereAREDI.GenerateEDIOrders(SalesHeader);
        // FIN AD Le 02-11-2009
    end;
    //<< Mig CU 414
    //>> Mig CU 415
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Release Purchase Document", 'OnCodeOnAfterCheckPurchaseReleaseRestrictions', '', false, false)]
    local procedure CU415_OnCodeOnAfterCheckPurchaseReleaseRestrictions(var PurchaseHeader: Record "Purchase Header"; var IsHandled: Boolean)
    begin
        // MIG 2017
        IF (PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Invoice) OR (PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::"Credit Memo") THEN BEGIN
            PurchaseHeader.TESTFIELD(Periode);
            IF PurchaseHeader.Periode <> DATE2DMY(PurchaseHeader."Posting Date", 2) THEN
                ERROR('Date comtabilisation hors période (%1)', PurchaseHeader.Periode);
        END;
        // fin MIG 2017
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Release Purchase Document", 'OnCodeOnBeforeModifyHeader', '', false, false)]
    local procedure CU415_OnCodeOnBeforeModifyHeader(var PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line"; PreviewMode: Boolean; var LinesWereModified: Boolean)
    var
        CodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // GB Le 18/09/2017 => Reprise des developpements perdu NAv 2009->2015  FR20150608
        IF (PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::"Return Order") THEN BEGIN
            CLEAR(PurchaseLine);
            PurchaseLine.SETRANGE("Document Type", PurchaseHeader."Document Type");
            PurchaseLine.SETRANGE("Document No.", PurchaseHeader."No.");
            PurchaseLine.SETFILTER(Type, '>0');
            PurchaseLine.SETFILTER(Quantity, '<>0');
            IF PurchaseLine.FINDSET() THEN
                REPEAT
                    // Pas de mouvment si valeurs vide
                    IF (PurchaseLine."Location Code Temp" <> '') AND (PurchaseLine."Bin Code Temp" <> '') then begin
                        IF PurchaseLine.Quantity <> 0 then begin
                            CodeunitsFunctions.CreateMvtEmplReturn(PurchaseLine);
                            PurchaseLine.VALIDATE("Location Code Orig", PurchaseLine."Location Code");
                            PurchaseLine.VALIDATE("Bin Code Orig", PurchaseLine."Bin Code");
                            // MCO Le 15-01-2018 => MIgration ne pas recalculer les dates
                            IF PurchaseLine."Location Code" <> PurchaseLine."Location Code Temp" then
                                // FIN MCO Le 15-01-2018
                                PurchaseLine.VALIDATE("Location Code", PurchaseLine."Location Code Temp");
                            PurchaseLine.VALIDATE("Bin Code", PurchaseLine."Bin Code Temp");
                            PurchaseLine."Location Code Temp" := '';
                            PurchaseLine."Bin Code Temp" := '';
                            PurchaseLine.MODIFY(TRUE);
                            // CFR le 17/01/2024 - R‚gie : Suivi de [Demande retour] sur Commentaire dernier mouvement
                            PurchaseLine.SetWarehouseEntryComment();
                        END;
                    END;
                UNTIL PurchaseLine.NEXT() = 0;
        END;
        //FIN GB Le 18/09/2017
    end;
    //<< Mig CU 415
    //>> Mig CU 550
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"VAT Rate Change Conversion", 'OnUpdateRecOnBeforeValidateGenProdPostingGroup', '', false, false)]
    local procedure CU550_OnUpdateRecOnBeforeValidateGenProdPostingGroup(VatRateChangeConversion: Record "VAT Rate Change Conversion"; sender: Codeunit "VAT Rate Change Conversion"; var RecRef: RecordRef; FldRef: FieldRef; var IsHandled: Boolean)
    begin
        IsHandled := true;
        IF NOT (RecRef.NUMBER = DATABASE::"Gen. Product Posting Group") THEN
            FldRef.VALIDATE(VATRateChangeConversion."To Code")
        ELSE
            FldRef.VALUE := VATRateChangeConversion."To Code";
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"VAT Rate Change Conversion", 'OnCanUpdateSalesOnAfterLoopIteration', '', false, false)]
    local procedure CU550_OnCanUpdateSalesOnAfterLoopIteration(SalesLine: Record "Sales Line"; var DescriptionTxt: Text[250])
    var
        WhseValidateSourceLine: Codeunit "Whse. Validate Source Line";
        ESK0004Txt: Label 'Order line %1 has a drop shipment purchasing code. Update the order manually.', Comment = '%1 = Order line No ';
        ESK0005Txt: Label 'Order line %1 has a special order purchasing code. Update the order manually.', Comment = '%1 = Order line No ';
        ESK0006Txt: Label 'The order has a partially shipped line with link to a WHSE document. Update the order manually.';
        ESK0011Txt: Label 'Documents that have posted prepayment must be converted manually.';
        ESK0014Txt: Label 'The order line %1 of type %2 have been partial Shipped/Invoiced . Update the order manually.', Comment = '%1 = Order line No ; %2 = Order Type ';
        ESK0018Txt: Label 'This document is linked to an assembly order. You must convert the document manually.';
    begin
        DescriptionTxt := '';
        IF (SalesLine."Prepmt. Amount Inv. Incl. VAT" = 0) or (SalesLine."Prepmt. Amount Inv. (LCY)" = 0) THEN begin
            if SalesLine."Drop Shipment" and (SalesLine."Purchase Order No." <> '') then
                DescriptionTxt := StrSubstNo(ESK0004Txt, SalesLine."Line No.");
            if SalesLine."Special Order" and (SalesLine."Special Order Purchase No." <> '') then
                DescriptionTxt := StrSubstNo(ESK0005Txt, SalesLine."Line No.");
            if (SalesLine."Outstanding Quantity" <> SalesLine.Quantity) and
                    WhseValidateSourceLine.WhseLinesExist(Database::"Sales Line", SalesLine."Document Type".AsInteger(), SalesLine."Document No.", SalesLine."Line No.", 0, SalesLine.Quantity)
                 then
                DescriptionTxt := ESK0006Txt;
            if (SalesLine."Outstanding Quantity" <> SalesLine.Quantity) and (SalesLine.Type = SalesLine.Type::"Charge (Item)") then
                DescriptionTxt := StrSubstNo(ESK0014Txt, SalesLine."Line No.", SalesLine.Type::"Charge (Item)");
            IF SalesLine."Prepmt. Amount Inv. (LCY)" <> 0 THEN
                DescriptionTxt := ESK0011Txt;
            if (SalesLine."Qty. to Assemble to Order" = 0) and (SalesLine."Prepmt. Amount Inv. Incl. VAT" <> 0) then
                DescriptionTxt := ESK0018Txt;
        end;
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"VAT Rate Change Conversion", 'OnBeforeUpdatePurchase', '', false, false)]
    // local procedure CU550_OnBeforeUpdatePurchase(var PurchaseHeader: Record "Purchase Header"; var VATRateChangeSetup: Record "VAT Rate Change Setup"; var IsHandled: Boolean)
    // var
    //     PurchaseHeader2: Record "Purchase Header";
    //     PurchaseLine: Record "Purchase Line";
    //     PurchaseLineOld: Record "Purchase Line";
    //     VATRateChangeLogEntry: Record "VAT Rate Change Log Entry";
    //     RecRef: RecordRef;
    //     NewVATProdPotingGroup: Code[20];
    //     NewGenProdPostingGroup: Code[20];
    //     StatusChanged: Boolean;
    //     ConvertVATProdPostingGroup: Boolean;
    //     ConvertGenProdPostingGroup: Boolean;
    //     RoundingPrecision: Decimal;
    //     IsModified: Boolean;
    //     ShouldProcessLine: Boolean;
    //     CduGVATRateChangeConversion: Codeunit "VAT Rate Change Conversion";
    // begin
    //     ConvertVATProdPostingGroup := CduGVATRateChangeConversion.ConvertVATProdPostGrp(VATRateChangeSetup."Update Purchase Documents");
    //     ConvertGenProdPostingGroup := CduGVATRateChangeConversion.ConvertGenProdPostGrp(VATRateChangeSetup."Update Purchase Documents");
    //     PurchaseHeader.SetFilter(
    //       "Document Type", '%1..%2|%3', PurchaseHeader."Document Type"::Quote, PurchaseHeader."Document Type"::Invoice,
    //       PurchaseHeader."Document Type"::"Blanket Order");
    //     if PurchaseHeader.Find('-') then
    //         repeat
    //             StatusChanged := false;
    //             if CanUpdatePurchase(PurchaseHeader, ConvertGenProdPostingGroup, ConvertVATProdPostingGroup) then begin
    //                 if VATRateChangeSetup."Ignore Status on Purch. Docs." then
    //                     if PurchaseHeader.Status <> PurchaseHeader.Status::Open then begin
    //                         PurchaseHeader2 := PurchaseHeader;
    //                         PurchaseHeader.Status := PurchaseHeader.Status::Open;
    //                         PurchaseHeader.Modify();
    //                         StatusChanged := true;
    //                     end;
    //                 if PurchaseHeader.Status = PurchaseHeader.Status::Open then begin
    //                     PurchaseLine.SetRange("Document Type", PurchaseHeader."Document Type");
    //                     PurchaseLine.SetRange("Document No.", PurchaseHeader."No.");
    //                     if PurchaseLine.FindSet() then
    //                         repeat
    //                             ShouldProcessLine := LineInScope(PurchaseLine."Gen. Prod. Posting Group", PurchaseLine."VAT Prod. Posting Group", ConvertGenProdPostingGroup, ConvertVATProdPostingGroup);
    //                             if ShouldProcessLine then
    //                                 if (PurchaseLine."Receipt No." = '') and
    //                                    (PurchaseLine."Return Shipment No." = '') and IncludePurchLine(PurchaseLine.Type, PurchaseLine."No.")
    //                                 then
    //                                     if PurchaseLine.Quantity = PurchaseLine."Outstanding Quantity" then begin
    //                                         if PurchaseHeader."Prices Including VAT" then
    //                                             PurchaseLineOld := PurchaseLine;

    //                                         RecRef.GetTable(PurchaseLine);
    //                                         UpdateRec(
    //                                           RecRef, ConvertVATProdPostGrp(VATRateChangeSetup."Update Purchase Documents"),
    //                                           ConvertGenProdPostGrp(VATRateChangeSetup."Update Purchase Documents"));

    //                                         PurchaseLine.Find();
    //                                         IsModified := false;
    //                                         if PurchaseHeader."Prices Including VAT" and VATRateChangeSetup."Perform Conversion" and
    //                                            (PurchaseLine."VAT %" <> PurchaseLineOld."VAT %") and
    //                                            UpdateUnitPriceInclVAT(PurchaseLine.Type)
    //                                         then begin
    //                                             RecRef.SetTable(PurchaseLine);
    //                                             RoundingPrecision := GetRoundingPrecision(PurchaseHeader."Currency Code");
    //                                             PurchaseLine.Validate(
    //                                               "Direct Unit Cost",
    //                                               Round(
    //                                                 PurchaseLineOld."Direct Unit Cost" * (100 + PurchaseLine."VAT %") / (100 + PurchaseLineOld."VAT %"),
    //                                                 RoundingPrecision));
    //                                             IsModified := true;
    //                                         end;
    //                                         if PurchaseLine."Prepayment %" <> 0 then begin
    //                                             PurchaseLine.UpdatePrepmtSetupFields();
    //                                             IsModified := true;
    //                                         end;
    //                                         if IsModified then begin
    //                                             PurchaseLine.Modify(true);
    //                                         end;
    //                                     end else
    //                                         if VATRateChangeSetup."Perform Conversion" and (PurchaseLine."Outstanding Quantity" <> 0) then begin
    //                                             NewVATProdPotingGroup := PurchaseLine."VAT Prod. Posting Group";
    //                                             NewGenProdPostingGroup := PurchaseLine."Gen. Prod. Posting Group";
    //                                             if ConvertVATProdPostingGroup then
    //                                                 if VATRateChangeConversion.Get(
    //                                                      VATRateChangeConversion.Type::"VAT Prod. Posting Group", PurchaseLine."VAT Prod. Posting Group")
    //                                                 then
    //                                                     NewVATProdPotingGroup := VATRateChangeConversion."To Code";
    //                                             if ConvertGenProdPostingGroup then
    //                                                 if VATRateChangeConversion.Get(
    //                                                      VATRateChangeConversion.Type::"Gen. Prod. Posting Group", PurchaseLine."Gen. Prod. Posting Group")
    //                                                 then
    //                                                     NewGenProdPostingGroup := VATRateChangeConversion."To Code";
    //                                             AddNewPurchaseLine(PurchaseLine, NewVATProdPotingGroup, NewGenProdPostingGroup);
    //                                         end else begin
    //                                             RecRef.GetTable(PurchaseLine);
    //                                             InitVATRateChangeLogEntry(
    //                                               VATRateChangeLogEntry, RecRef, PurchaseLine."Outstanding Quantity", PurchaseLine."Line No.");
    //                                             VATRateChangeLogEntry.UpdateGroups(
    //                                               PurchaseLine."Gen. Prod. Posting Group", PurchaseLine."Gen. Prod. Posting Group",
    //                                               PurchaseLine."VAT Prod. Posting Group", PurchaseLine."VAT Prod. Posting Group");
    //                                             WriteLogEntry(VATRateChangeLogEntry);
    //                                         end;
    //                         until PurchaseLine.Next() = 0;
    //                 end;
    //                 if StatusChanged then begin
    //                     PurchaseHeader.Status := PurchaseHeader2.Status;
    //                     PurchaseHeader.Modify();
    //                 end;
    //             end;
    //         until PurchaseHeader.Next() = 0;
    // end;
    //<< Mig CU 550
    //>> Mig CU 900
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Assembly-Post", 'OnBeforeOnRun', '', false, false)]
    local procedure CU900_OnBeforeOnRun(var AssemblyHeader: Record "Assembly Header"; SuppressCommit: Boolean)
    begin
        // MCO Le 13-02-2018 => R‚gie : Demande de Nathalie
        AssemblyHeader."Posting Date" := WORKDATE();
        // FIN MCO Le 13-02-2018 => R‚gie : Demande de Nathalie
    end;
    //<< Mig CU 900
    //>> Mig CU 5055
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"CustVendBank-Update", 'OnAfterUpdateCustomer', '', false, false)]
    local procedure CU5055_OnAfterUpdateCustomer(var Customer: Record Customer; Contact: Record Contact; var ContBusRel: Record "Contact Business Relation")
    begin
        // MCO Le 23-03-2016 => Gestion du mobile
        Customer."Mobile Phone No." := Contact."Mobile Phone No.";
        // FIN MCO Le 23-03-2016 => Gestion du mobile
    end;
    //<< Mig CU 5055
    //>> Mig CU 5056
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"CustCont-Update", 'OnAfterTransferFieldsFromCustToCont', '', false, false)]
    local procedure CU5056_OnAfterTransferFieldsFromCustToCont(var Contact: Record Contact; Customer: Record Customer)
    begin
        // MCO Le 23-03-2016 => Gestion du mobile
        Contact."Mobile Phone No." := Customer."Mobile Phone No.";
        // FIN MCO Le 23-03-2016 => Gestion du mobile
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"CustCont-Update", 'OnInsertNewContactOnBeforeAssignNo', '', false, false)]
    local procedure CU5056_OnInsertNewContactOnBeforeAssignNo(var Contact: Record Contact; var IsHandled: Boolean; Customer: Record Customer)
    var
        RMSetup: Record "Marketing Setup";
    begin
        // MCO Le 23-03-2016 => Gestion du mobile
        Contact."Mobile Phone No." := Customer."Mobile Phone No.";
        // FIN MCO Le 23-03-2016 => Gestion du mobile

        IsHandled := true;
        Contact."No." := '';
        Contact."No. Series" := '';
        RMSetup.Get();
        RMSetup.TestField("Contact Nos.");
        // AD Le 03-11-2009 => CRM -> Pour que le contact est le mˆme no que le client
        //NoSeriesMgt.InitSeries(RMSetup."Contact Nos.", '', 0D, "No.", "No. Series");
        Contact."No." := Customer."No.";
        // FIN AD Le 03-11-2009
    end;
    //<< Mig CU 5056
    //>> Mig CU 5057
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"VendCont-Update", 'OnInsertNewContactOnBeforeAssignNo', '', false, false)]
    local procedure CU5057_OnInsertNewContactOnBeforeAssignNo(Vendor: Record Vendor; var Contact: Record Contact; var IsHandled: Boolean; LocalCall: Boolean; MarketingSetup: Record "Marketing Setup")
    var
        RMSetup: Record "Marketing Setup";
    begin
        RMSetup.Get();
        IsHandled := true;
        Contact."No." := '';
        Contact."No. Series" := '';
        RMSetup.TestField("Contact Nos.");
        // AD Le 03-11-2009 => CRM -> Pour que le contact est le mˆme no que le client
        //NoSeriesMgt.InitSeries(RMSetup."Contact Nos.",'',0D,"No.","No. Series");
        Contact."No." := Vendor."No.";
        // FIN AD Le 03-11-2009
    end;
    //<< Mig CU 5057
    //>> Mig CU 5063
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ArchiveManagement, 'OnBeforeStoreSalesDocument', '', false, false)]
    local procedure CU5063_OnBeforeStoreSalesDocument(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    var
        SalesHeaderArchive: Record "Sales Header Archive";
        ArchiveManagement: Codeunit ArchiveManagement;
    begin
        // AD Le 24-05-2007 => Archivage en 1 exemplaire des devis.
        // AD Le 07-12-2007 => Archivage en 1 exemplaire des commandes.
        // On recherche les exemplaires pr‚c‚dent pour les supprimer
        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::Quote) OR
           (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) THEN BEGIN
            // AD Le 04-07-2007 => Pour le devis on garde le no de version.
            // CU5063_SalesVersionNo := ArchiveManagement.GetNextVersionNo(
            //                    DATABASE::"Sales Header", SalesHeader."Document Type".AsInteger(), SalesHeader."No.", SalesHeader."Doc. No. Occurrence");
            CduGSingleInstance.FctSetSalesVersionNo(ArchiveManagement.GetNextVersionNo(
                                DATABASE::"Sales Header", SalesHeader."Document Type".AsInteger(), SalesHeader."No.", SalesHeader."Doc. No. Occurrence"));
            // FIN AD Le 04-07-2007
            SalesHeaderArchive.LOCKTABLE();
            SalesHeaderArchive.SETRANGE("Document Type", SalesHeader."Document Type");
            SalesHeaderArchive.SETRANGE("No.", SalesHeader."No.");
            if SalesHeaderArchive.FINDFIRST() then
                repeat
                    SalesHeaderArchive.DELETE(true);
                until SalesHeaderArchive.NEXT() = 0;
        end;
        // FIN AD Le 24-05-2007
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::ArchiveManagement, 'OnBeforeSalesHeaderArchiveInsert', '', false, false)]
    local procedure CU5063_OnBeforeSalesHeaderArchiveInsert(var SalesHeaderArchive: Record "Sales Header Archive"; SalesHeader: Record "Sales Header")
    begin
        // AD Le 04-07-2007 => Pour le devis on garde le no de version.
        // AD Le 07-12-2007 => Archivage en 1 exemplaire des commandes (demande de Jeremy).
        if (SalesHeader."Document Type" = SalesHeader."Document Type"::Quote) or
           (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) then begin
            SalesHeaderArchive."Version No." := CduGSingleInstance.FctGetSalesVersionNo();
            // CU5063_SalesVersionNo := 0;
            CduGSingleInstance.FctSetSalesVersionNo(0);
        end;
        // FIN AD Le 07-07-2007

        // AD Le 26-02-2016 => Le champ n'exite pas en stardard
        SalesHeaderArchive."Quote No. For ARchive" := SalesHeader."Quote No.";
        SalesHeaderArchive."Cust. Price Group For Archive" := SalesHeader."Customer Price Group";
        SalesHeaderArchive."Cust. Disc. Group For Archive" := SalesHeader."Customer Disc. Group";
        // FIN AD Le 26-02-2016
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::ArchiveManagement, 'OnBeforeStorePurchDocument', '', false, false)]
    local procedure CU5063_OnBeforeStorePurchDocument(var PurchHeader: Record "Purchase Header"; var IsHandled: Boolean)
    var
        PurchHeaderArchive: Record "Purchase Header Archive";
        ArchiveManagement: Codeunit ArchiveManagement;
    begin
        // AD Le 24-05-2007 => Archivage en 1 exemplaire des devis.
        // AD Le 07-12-2007 => Archivage en 1 exemplaire des commandes.
        // On recherche les exemplaires pr‚c‚dent pour les supprimer
        IF (PurchHeader."Document Type" = PurchHeader."Document Type"::Order) THEN BEGIN
            // AD Le 04-07-2007 => Pour le devis on garde le no de version.
            // CU5063_PurchVersionNo := ArchiveManagement.GetNextVersionNo(
            //                    DATABASE::"Purchase Header", PurchHeader."Document Type".AsInteger(), PurchHeader."No.", PurchHeader."Doc. No. Occurrence");
            CduGSingleInstance.FctSetPurchVersionNo(ArchiveManagement.GetNextVersionNo(
                               DATABASE::"Purchase Header", PurchHeader."Document Type".AsInteger(), PurchHeader."No.", PurchHeader."Doc. No. Occurrence"));
            // FIN AD Le 04-07-2007
            PurchHeaderArchive.LOCKTABLE();
            PurchHeaderArchive.SETRANGE("Document Type", PurchHeader."Document Type");
            PurchHeaderArchive.SETRANGE("No.", PurchHeader."No.");
            IF PurchHeaderArchive.FINDFIRST() THEN
                REPEAT
                    PurchHeaderArchive.DELETE(TRUE);
                UNTIL PurchHeaderArchive.NEXT() = 0;
        END;
        // FIN AD Le 24-05-2007
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::ArchiveManagement, 'OnBeforePurchHeaderArchiveInsert', '', false, false)]
    local procedure CU5063_OnBeforePurchHeaderArchiveInsert(var PurchaseHeaderArchive: Record "Purchase Header Archive"; PurchaseHeader: Record "Purchase Header")
    begin
        // AD Le 04-07-2007 => Pour le devis on garde le no de version.
        // AD Le 07-12-2007 => Archivage en 1 exemplaire des commandes (demande de Jeremy).
        IF (PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Order) THEN begin
            PurchaseHeaderArchive."Version No." := CduGSingleInstance.FctGetPurchVersionNo();
            // CU5063_PurchVersionNo := 0;
            CduGSingleInstance.FctSetPurchVersionNo(0);
        end;
        // FIN AD Le 07-07-2007
    end;
    //<< Mig CU 5063
    //>> Mig CU 5400
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Available Management", 'OnAfterCalcAvailableQty', '', false, false)]
    local procedure CU5400_OnAfterCalcAvailableQty(var Item: Record Item; CalcAvailable: Boolean; PlannedOrderReceiptDate: Date; var AvailableQty: Decimal)
    begin
        // AD Le 21-10-2009 => FARGROUP -> Prise en compte des qte sur commande ouverte dans le reserve
        Item.CALCFIELDS("Qty. on Blanket Sales Order");
        // FIN AD Le 21-10-2009

        // AD Le 21-10-2009 => FARGROUP -> Prise en compte des qte sur commande ouverte dans le reserve
        AvailableQty -= Item."Qty. on Blanket Sales Order";
        // FIN AD Le 21-10-2009
    end;
    //<< Mig CU 5400
    //>> Mig CU 5404
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post Shipment", 'OnBeforeInsertTransShptHeader', '', false, false)]
    local procedure CU5404_OnBeforeInsertTransShptHeader(var TransShptHeader: Record "Transfer Shipment Header"; TransHeader: Record "Transfer Header"; CommitIsSuppressed: Boolean)
    begin
        //FBRUN -> PRET suivi du champs Nø de prˆt
        TransShptHeader."Loans No." := TransHeader."Loans No.";
        //fin PRET
    end;
    //<< Mig CU 5404
    //>> Mig CU 5701
    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Subst.", 'OnBeforeItemSubstGetPopulateTempSalesLine', '', false, false)]
    // local procedure CU5701_OnBeforeItemSubstGetPopulateTempSalesLine(var TempSalesline: Record "Sales Line" temporary; var TempItemSubstitution: Record "Item Substitution" temporary; var IsHandled: Boolean; SaveItemNo: Code[20]; SaveVariantCode: Code[10])
    // var
    // LItem: Record Item;
    // begin
    //     // AD Le 05-02-2015 => Si on vient du quid, on retourne le code article
    //     IF TempSalesLine."Document No." = 'MULTICONSSULT' THEN BEGIN
    //         LItem.GET(TempItemSubstitution."Substitute No.");
    //         EXIT(LItem."No. 2");
    //     END;

    //     // FIN AD Le 05-02-2015
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Subst.", 'OnItemSubstGetOnAfterSubstSalesLineItem', '', false, false)]
    local procedure CU5701_OnItemSubstGetOnAfterSubstSalesLineItem(var SalesLine: Record "Sales Line"; var SourceSalesLine: Record "Sales Line"; var TempItemSubstitution: Record "Item Substitution" temporary)
    var
        LItem: Record Item;
    begin
        // AD
        LItem.GET(TempItemSubstitution."Substitute No.");
        SalesLine."Recherche référence" := LItem."No. 2";
        // FIN AD
        SalesLine.SuiviCommentaireArticle();// AD Le 09-03-2016
        SalesLine.VALIDATE("Requested Delivery Date"); // MCO Le 21-11-2018
    end;
    //<< Mig CU 5701
    //>> Mig CU 5705
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post Receipt", 'OnBeforeTransRcptHeaderInsert', '', false, false)]
    local procedure CU5705_OnBeforeTransRcptHeaderInsert(var TransferReceiptHeader: Record "Transfer Receipt Header"; TransferHeader: Record "Transfer Header")
    begin
        //PRET suivi du champs Nø de prˆt
        TransferReceiptHeader."Loans No." := TransferHeader."Loans No.";
        //fin PRET
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post Receipt", 'OnBeforeTransferOrderPostReceipt', '', false, false)]
    local procedure CU5705_OnBeforeTransferOrderPostReceipt(var TransferHeader: Record "Transfer Header"; var CommitIsSuppressed: Boolean; var ItemJnlPostLine: Codeunit "Item Jnl.-Post Line")
    begin
        //FBRUN -> PRET Initialiser N° prêt
        // CU5705_LoansNo := TransferHeader."Loans No.";
        CduGSingleInstance.FctSetLoansNo(TransferHeader."Loans No.");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post Receipt", 'OnAfterTransferOrderPostReceipt', '', false, false)]
    local procedure CU5705_OnAfterTransferOrderPostReceipt(var TransferHeader: Record "Transfer Header"; CommitIsSuppressed: Boolean; var TransferReceiptHeader: Record "Transfer Receipt Header")
    var
        SalesHeader: Record "Sales Header";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        ArchiveManagement: Codeunit ArchiveManagement;
    begin
        //BRUN -> PRET Archivage
        IF SalesHeader.GET(SalesHeader."Document Type"::Order, CduGSingleInstance.FctGetLoansNo()) THEN
            IF LCodeunitsFunctions.SalesFinished(SalesHeader) THEN BEGIN
                ArchiveManagement.ArchiveSalesDocument(SalesHeader);
                CduGSingleInstance.FctDeleteSalesLine5063(SalesHeader);
                SalesHeader.DELETE();
            END;
        //FIn PRET Archivage
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post Receipt", 'OnBeforeInsertTransRcptLine', '', false, false)]
    local procedure CU5705_OnBeforeInsertTransRcptLine(var TransRcptLine: Record "Transfer Receipt Line"; TransLine: Record "Transfer Line"; CommitIsSuppressed: Boolean; var IsHandled: Boolean; TransferReceiptHeader: Record "Transfer Receipt Header")
    var
        SalesLine: Record "Sales Line";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        LCodeunitsFunctions2: Codeunit "Codeunits Functions 2";
        NoLoans: Code[20];
    begin
        //FBRUN -> PRET mise … jour des lignes de prˆt lors du retour
        if TransferReceiptHeader."Loans No." <> '' then begin
            NoLoans := TransferReceiptHeader."Loans No.";
            SalesLine.RESET();
            SalesLine.SETRANGE(SalesLine."Document Type", SalesLine."Document Type"::Order);
            SalesLine.SETRANGE(SalesLine."Document No.", TransferReceiptHeader."Loans No.");
            SalesLine.SETRANGE(SalesLine."Line No.", TransLine."Line No.");
            //SalesLine.SETRANGE(Loans,TRUE);
            if SalesLine.FIND('-') then begin
                //mettre bonne quantit‚ re‡u PRET
                SalesLine."Return Qty Loans" := SalesLine."Return Qty Loans" + TransRcptLine.Quantity;
                SalesLine."Return Qty Loans(Base)" := SalesLine."Return Qty Loans(Base)" + TransRcptLine."Quantity (Base)";
                SalesLine."Qty. Shipped Not Invoiced" := SalesLine."Qty. Shipped Not Invoiced" - TransRcptLine.Quantity;
                SalesLine."Qty. Shipped Not Invd. (Base)" := SalesLine."Qty. Shipped Not Invd. (Base)" - TransRcptLine."Quantity (Base)";
                SalesLine."Quantity Shipped" := SalesLine."Quantity Shipped" - TransRcptLine.Quantity;
                SalesLine."Qty. Shipped (Base)" := SalesLine."Qty. Shipped (Base)" - TransRcptLine."Quantity (Base)";
                CduGSingleInstance.FctHideDialog(true);
                SalesLine.VALIDATE(Quantity, SalesLine.Quantity - TransRcptLine.Quantity);
                SalesLine."Qty. to Invoice" := SalesLine."Qty. Shipped Not Invoiced";
                SalesLine."Qty. to Invoice (Base)" := SalesLine."Qty. Shipped Not Invd. (Base)";
                SalesLine."Qty To Receive Loans" := 0;
                SalesLine."Qty To Receive Loans(Base)" := 0;
                SalesLine.MODIFY();
                //MC Le 29-09-2010 => On met aussi les quantit‚s … jour pour les lignes attach‚es. (Ex : DEEE)
                LCodeunitsFunctions.UpdateAttachedLine(NoLoans, SalesLine, TransRcptLine);
                //FIN MC Le 29-09-2010
                LCodeunitsFunctions2.UpdateSalesShipLine(TransRcptLine);
            END;
        END;
        //FIn pret
    end;

    var
    //<< Mig CU 5705
    //>> Mig CU 5707
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"TransferOrder-Post + Print", 'OnRunOnBeforePrepareAndPrintReport', '', false, false)]
    local procedure CU5707_OnRunOnBeforePrepareAndPrintReport(var TransferHeader: Record "Transfer Header"; var DefaultNumber: Integer; var Selection: Option; var IsHandled: Boolean)
    var
        TransShptHeader: Record "Transfer Shipment Header";
        TransRcptHeader: Record "Transfer Receipt Header";
        TransferPostShipment: Codeunit "TransferOrder-Post Shipment";
        TransferPostReceipt: Codeunit "TransferOrder-Post Receipt";
        TransferOrderPostPrint: Codeunit "TransferOrder-Post + Print";
        ESK001_Qst: Label '&Expédier,&Réceptionner,Expédier Et Réceptionner';
    begin
        if not TransferHeader."Direct Transfer" then begin
            if DefaultNumber = 0 then
                DefaultNumber := 1;
            // AD Le 23-11-2010 => Valider reception et exp‚dition en mˆme temps
            // Selection := StrMenu(Text000, DefaultNumber); => CODE D'ORIGINE
            Selection := STRMENU(ESK001_Qst, DefaultNumber);
            // FIN AD Le 23-11-2010
            case Selection of
                0:
                    exit;
                1:
                    TransferPostShipment.Run(TransferHeader);
                2:
                    TransferPostReceipt.Run(TransferHeader);
                // AD Le 23-11-2010 => Valider reception et exp‚dition en mˆme temps
                3:
                    BEGIN
                        TransferPostShipment.RUN(TransferHeader);
                        TransShptHeader."No." := TransferHeader."Last Shipment No.";
                        TransShptHeader.SETRECFILTER();
                        TransShptHeader.PrintRecords(FALSE);

                        TransferPostReceipt.RUN(TransferHeader);
                        TransRcptHeader."No." := TransferHeader."Last Receipt No.";
                        TransRcptHeader.SETRECFILTER();
                        TransRcptHeader.PrintRecords(FALSE);
                    END;
            // FIN AD Le 23-11-2010
            end;
            TransferOrderPostPrint.PrintReport(TransferHeader, Selection);

            IsHandled := true;
        end;
    end;
    //<< Mig CU 5707
    //>> Mig CU 5750
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Warehouse Mgt.", 'OnFromSalesLine2ShptLineOnBeforeCreateShipmentLine', '', false, false)]
    local procedure CU5991_OnFromSalesLine2ShptLineOnBeforeCreateShipmentLine(WarehouseShipmentHeader: Record "Warehouse Shipment Header"; SalesLine: Record "Sales Line"; var TotalOutstandingWhseShptQty: Decimal; var TotalOutstandingWhseShptQtyBase: Decimal)
    begin
        // AD Le 03-11-2011 => SYMTA
        IF SalesLine."Qty. to Ship" = 0 THEN
            TotalOutstandingWhseShptQtyBase := 0;
        // FIN AD Le 03-11-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Warehouse Mgt.", 'OnBeforeCreateShptLineFromSalesLine', '', false, false)]
    local procedure CU5991_OnBeforeCreateShptLineFromSalesLine(var WarehouseShipmentLine: Record "Warehouse Shipment Line"; WarehouseShipmentHeader: Record "Warehouse Shipment Header"; SalesLine: Record "Sales Line"; SalesHeader: Record "Sales Header")
    var
        LItem: Record Item;
    begin
        // AD Le 07-04-2007 => Pour prendre en compte la qte a pr‚parer
        // IF NOT "Build Kit" THEN => AD Le 28-11-2011 => Meme pour les kits
        // AD Le 21-11-2011 => Pour SYMTA, La qte a livrer
        // VALIDATE("Qty. to Ship",ABS(SalesLine."A Traiter en pr‚paration"));
        WarehouseShipmentLine.VALIDATE(Quantity, ABS(SalesLine."Outstanding Quantity")); // AD Le 08-12-2015 => MIG2015
        WarehouseShipmentLine.VALIDATE("Qty. to Ship", ABS(SalesLine."Qty. to Ship"));
        // FIN AD Le 21-11-2011

        WarehouseShipmentLine."Gerer par groupement" := SalesLine."Gerer par groupement"; //FBRUN le 26/04/2011 =>Symta gestion des centrales

        // AD Le 19-09-2009 => Gestion ESKAPE
        LItem.GET(WarehouseShipmentLine."Item No.");
        WarehouseShipmentLine."Shelf No." := LItem."Shelf No.";
        WarehouseShipmentLine.Weight := SalesLine."Net Weight";
        // FIN AD Le 19-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Warehouse Mgt.", 'OnAfterInitNewWhseShptLine', '', false, false)]
    local procedure CU5991_OnAfterInitNewWhseShptLine(var WarehouseShipmentLine: Record "Warehouse Shipment Line"; SalesLine: Record "Sales Line")
    begin
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // Suivi du champ : [Code Zone].
        WarehouseShipmentLine."Zone Code" := SalesLine."Zone Code";
        // FIN MC Le 27-04-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Warehouse Mgt.", 'OnSalesLine2ReceiptLineOnBeforeUpdateReceiptLine', '', false, false)]
    local procedure CU5991_OnSalesLine2ReceiptLineOnBeforeUpdateReceiptLine(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; SalesLine: Record "Sales Line")
    begin
        // AD Le 30-01-2020 => REGIE ->
        WarehouseReceiptLine."Origine Type" := WarehouseReceiptLine."Origine Type"::Customer;
        WarehouseReceiptLine."Origin n No." := SalesLine."Sell-to Customer No.";
        // AD Le 30-01-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purchases Warehouse Mgt.", 'OnPurchLine2ReceiptLineOnAfterSetQtysOnRcptLine', '', false, false)]
    local procedure CU5992_OnPurchLine2ReceiptLineOnAfterSetQtysOnRcptLine(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; PurchaseLine: Record "Purchase Line")
    var
        WhseReceiptHeader: Record "Warehouse Receipt Header";
    begin
        if WhseReceiptHeader.GET(WarehouseReceiptLine."No.") then;
        // MC Le 30-01-2013 => Une ligne de commande peut ˆtre sur plusieurs r‚ceptions
        WarehouseReceiptLine.VALIDATE("Qty. to Receive", PurchaseLine."Outstanding Quantity" - PurchaseLine.GetQtyOnDepart());
        // FIN MC Le 30-01-2013

        // AD Le 20-03-2013 => Gestion des litiges
        WarehouseReceiptLine.VALIDATE("Qte Reçue théorique", WarehouseReceiptLine."Qty. to Receive");
        WarehouseReceiptLine.VALIDATE("Prix Brut Théorique", PurchaseLine."Direct Unit Cost");
        WarehouseReceiptLine.VALIDATE("Remise 1 Théorique", PurchaseLine."Discount1 %");
        WarehouseReceiptLine.VALIDATE("Remise 2 Théorique", PurchaseLine."Discount2 %");
        // FIN AD Le 20-03-2013

        // AD Le 24-03-2015 => Suivi du no de Bl … la ligne
        WarehouseReceiptLine."Vendor Shipment No." := WhseReceiptHeader."Vendor Shipment No.";
        // AD Le 24-03-2015

        // MCO Le 01-04-2015 => R‚gie
        WarehouseReceiptLine."Référence fournisseur" := PurchaseLine."Item Reference No.";
        // FIN MCO Le 01-04-2015

        // AD Le 30-01-2020 => REGIE ->
        WarehouseReceiptLine."Origine Type" := WarehouseReceiptLine."Origine Type"::Vendor;
        WarehouseReceiptLine."Origin n No." := PurchaseLine."Buy-from Vendor No.";
        // AD Le 30-01-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Transfer Warehouse Mgt.", 'OnBeforeUpdateRcptLineFromTransLine', '', false, false)]
    local procedure CU5993_OnBeforeUpdateRcptLineFromTransLine(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; TransferLine: Record "Transfer Line")
    begin
        // AD Le 30-01-2020 => REGIE ->
        WarehouseReceiptLine."Origine Type" := WarehouseReceiptLine."Origine Type"::Location;
        WarehouseReceiptLine."Origin n No." := TransferLine."Transfer-from Code";
        // AD Le 30-01-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Create Source Document", 'OnBeforeWhseReceiptLineInsert', '', false, false)]
    local procedure CU5750_OnBeforeWhseReceiptLineInsert(var WarehouseReceiptLine: Record "Warehouse Receipt Line")
    var
        Item: Record Item;
    begin
        Item."No." := WarehouseReceiptLine."Item No.";
        Item.ItemSKUGet(Item, WarehouseReceiptLine."Location Code", WarehouseReceiptLine."Variant Code");
        WarehouseReceiptLine."Ref. Active" := Item."No. 2"; // AD Le 21-03-2013
    end;
    //<< Mig CU 5750
    //>>Mig CU 5752
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Get Source Doc. Outbound", 'OnBeforeCreateWhseShipmentHeaderFromWhseRequest', '', false, false)]
    local procedure CU5752_OnBeforeCreateWhseShipmentHeaderFromWhseRequest(var WarehouseRequest: Record "Warehouse Request"; var Rusult: Boolean; var IsHandled: Boolean; var GetSourceDocuments: Report "Get Source Documents")
    var
        WhseShptHeader: Record "Warehouse Shipment Header";
    begin
        IsHandled := true;

        if WarehouseRequest.IsEmpty() then
            Rusult := false;

        Clear(GetSourceDocuments);
        GetSourceDocuments.UseRequestPage(false);
        GetSourceDocuments.SetTableView(WarehouseRequest);
        // AD le 06-10-2005 => Fonction ajouter pour lancer en mode silencieux
        if CduGSingleInstance.FctGetHideDialog_CU5752() then
            CduGSingleInstance.SetHideDialog(true);

        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // --> Rajout du filtre : [Filtre_zone].
        if CduGSingleInstance.FctGetZoneCode() <> '' then
            CduGSingleInstance.SetZoneCode(CduGSingleInstance.FctGetZoneCode());
        // FIN MC Le 27-04-2011
        CduGSingleInstance.SetHideDialog(true);
        GetSourceDocuments.RunModal();

        // AD Le 26-05-2011 => Impression des BPs g‚n‚r‚s
        /*TODO WHILE GetSourceDocuments.GetLstShpHeaderCreate(NoBP) DO BEGIN
            WhseShptHeader.RESET();
            WhseShptHeader.GET(NoBP);
            WhseShptHeader.SETRANGE("No.", NoBP);
            GestionImpZone.SetZone(WhseShptHeader."Location Code", WhseShptHeader."Zone Code");
            COMMIT;
            REPORT.RUN(50012, NOT CU5752_ImprimanteAuto, FALSE, WhseShptHeader);
            GestionImpZone.SetZone('', '');
        END;*/
        // FIN AD le 26-05-2011

        GetSourceDocuments.GetLastShptHeader(WhseShptHeader);

        Rusult := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Get Source Doc. Outbound", 'OnBeforeOpenWarehouseShipmentPage', '', false, false)]
    local procedure CU5752_OnBeforeOpenWarehouseShipmentPage(var GetSourceDocuments: Report "Get Source Documents"; var IsHandled: Boolean)
    var
        WarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        IsHandled := true;
        // AD le 06-10-2005 => Fonction ajouter pour lancer en mode silencieux
        IF NOT CduGSingleInstance.FctGetHideDialog_CU5752() THEN
            PAGE.Run(PAGE::"Warehouse Shipment", WarehouseShipmentHeader);
    end;
    //<<Mig CU 5752
    //>> Mig CU 5765
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment + Print", 'OnBeforeCode', '', false, false)]
    local procedure CU5765_OnBeforeCode(var WhseShptLine: Record "Warehouse Shipment Line"; var HideDialog: Boolean; var Invoice: Boolean; var IsPosted: Boolean; var Selection: Integer)
    var
        WhseShipHeader: Record "Warehouse Shipment Header";
        WhsePostShipment: Codeunit "Whse.-Post Shipment";
        Text50000_Qst: Label '&Livrer';
        Text50001_Qst: Label 'Ce document est bloqué !';
    begin
        IsPosted := true;

        if WhseShptLine.Find() then
            if not HideDialog then begin
                // AD Le 24-05-2007 => On autorise uniquement la livraison -> Modification du text000
                // Selection := StrMenu(ShipInvoiceQst, 1); => CODE ORIGINE
                Selection := STRMENU(Text50000_Qst, 1);
                // FIN AD Le 24-05-2007
                if Selection = 0 then
                    exit;
                Invoice := (Selection = 2);
            end;

        // AD Le 28-08-2009 => FARGROUP -> Possibilit‚ de bloquer un BP avant la validation
        WhseShipHeader.GET(WhseShptLine."No.");
        IF WhseShipHeader.Blocked THEN
            ERROR(Text50001_Qst);
        // FIN AD Le 28-08-2009

        // MCO Le 23-03-2016 => Plus utilis‚ par le client et ne fonctionne plus en 2016
        //WhsePostShipment.AbandonmentOfRemainder(WhseShipLine);
        // FIN MCO Le 23-03-2016
        WhsePostShipment.SetPostingSettings(Invoice);
        WhsePostShipment.SetPrint(true);
        WhsePostShipment.Run(WhseShptLine);
        WhsePostShipment.GetResultMessage();
        Clear(WhsePostShipment);
    end;
    //<< Mig CU 5765
    //>> Mig CU 5790
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Available to Promise", 'OnBeforeCalcQtyAvailableToPromise', '', false, false)]
    local procedure CU5790_OnBeforeCalcQtyAvailableToPromise(var Item: Record Item; var AvailabilityDate: Date; var GrossRequirement: Decimal; var ScheduledReceipt: Decimal; PeriodType: Enum "Analysis Period Type"; LookaheadDateFormula: DateFormula; var AvailableToPromise: Decimal; var IsHandled: Boolean)
    var
        AvailabletoPromiseCU: Codeunit "Available to Promise";
        DtDeb: Date;
        DtFin: Date;
    begin
        IsHandled := true;

        ScheduledReceipt := AvailabletoPromiseCU.CalcScheduledReceipt(Item);
        GrossRequirement := AvailabletoPromiseCU.CalcGrossRequirement(Item);

        if AvailabilityDate <> 0D then
            // AD Le 17-09-2009 => FARGROUP -> Dispo = physique - en cde
            DtDeb := Item.GETRANGEMIN("Date Filter");
        DtFin := Item.GETRANGEMAX("Date Filter");

        //Item.SETRANGE("Date Filter",0D, CALCDATE('+10A', WORKDATE));
        Item.SETRANGE("Date Filter", 0D, WORKDATE()); // AD Le 29-11-2011 => POUR SYMTA

        GrossRequirement := AvailabletoPromiseCU.CalcGrossRequirement(Item);
        ScheduledReceipt := 0;
        //ScheduledReceipt := CalcScheduledReceipt(Item);
        // FIN AD Le 17-09-2009

        if AvailabilityDate = 0D then
            AvailabilityDate := WORKDATE;  // AD Le 02-11-2015

        //IF FORMAT(LookaheadDateFormula) <> '' THEN

        GrossRequirement +=
            AvailabletoPromiseCU.CalculateForward(
                Item, PeriodType,
                AvailabilityDate + 1,
                AvailabletoPromiseCU.GetForwardPeriodEndDate(LookaheadDateFormula, PeriodType, AvailabilityDate));

        AvailableToPromise :=
            AvailabletoPromiseCU.CalcAvailableInventory(Item) +
            (ScheduledReceipt - AvailabletoPromiseCU.CalcReservedReceipt(Item)) -
            (GrossRequirement - AvailabletoPromiseCU.CalcReservedRequirement(Item));
        // AD Le 17-09-2009 => FARGROUP -> Dispo = physique - en cde
        //EXIT(  CalcAvailableInventory(Item) +  (ScheduledReceipt - CalcReservedReceipt(Item)) -  (GrossRequirement - CalcReservedRequirement(Item)));
        Item.SETRANGE("Date Filter", DtDeb, DtFin);
        AvailableToPromise := Item.Inventory - Item."Reserved Qty. on Inventory" - GrossRequirement;
        // FIN AD Le 17-09-2009

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Available to Promise", 'OnBeforeCalcGrossRequirement', '', false, false)]
    local procedure CU5790_OnBeforeCalcGrossRequirement(var Item: Record Item; var GrossRequirement: Decimal; var IsHandled: Boolean)
    begin
        IsHandled := true;

        // AD Le 21-10-2009 => FARGROUP -> Prise en compte des qte sur commande ouverte dans le reserve
        Item.CALCFIELDS("Qty. on Blanket Sales Order");
        // FIN AD Le 21-10-2009

        GrossRequirement :=
      Item."Qty. on Component Lines" +
      Item."Planning Issues (Qty.)" +
      Item."Planning Transfer Ship. (Qty)." +
      Item."Qty. on Sales Order" +
      Item."Qty. on Service Order" +
      Item."Qty. on Job Order" +
      Item."Trans. Ord. Shipment (Qty.)" +
      // AD Le 21-10-2009 => FARGROUP -> Prise en compte des qte sur commande ouverte dans le reserve
      Item."Qty. on Blanket Sales Order" +
      // FIN AD Le 21-10-2009
      Item."Qty. on Asm. Component" +
      Item."Qty. on Purch. Return";
    end;
    //<< Mig CU 5790
    //>> Mig CU 5804
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ItemCostManagement, 'OnBeforeCalcUnitCostFromAverageCost', '', false, false)]
    local procedure CU5804_OnBeforeCalcUnitCostFromAverageCost(var Item: Record Item; var CostCalcMgt: Codeunit "Cost Calculation Management"; GLSetup: Record "General Ledger Setup"; var IsHandled: Boolean)
    var
        Currency: Record Currency;
        ItemCostManagement: Codeunit ItemCostManagement;
        CodeunitsFunctions: Codeunit "Codeunits Functions";
        AverageCost: Decimal;
        AverageCostACY: Decimal;
        RndgSetupRead: Boolean;
    begin
        IsHandled := true;

        CostCalcMgt.GetRndgSetup(GLSetup, Currency, RndgSetupRead);
        if ItemCostManagement.CalculateAverageCost(Item, AverageCost, AverageCostACY) then begin
            if AverageCost <> 0 then
                // AD Le 04-12-2012 => Je ne suis pas sur des impacts de la modif du param‚trage donc pour le moment je touche pas
                //"Unit Cost" := Round(AverageCost, GLSetup."Unit-Amount Rounding Precision");
                Item."Unit Cost" := ROUND(AverageCost, 0.001);
        end else begin
            CodeunitsFunctions.CalcLastAdjEntryAvgCost(Item, AverageCost, AverageCostACY);
            if AverageCost <> 0 then
                // AD Le 04-12-2012 => Je ne suis pas sur des impacts de la modif du param‚trage donc pour le moment je touche pas
                //"Unit Cost" := Round(AverageCost, GLSetup."Unit-Amount Rounding Precision");
                Item."Unit Cost" := ROUND(AverageCost, 0.001);
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::ItemCostManagement, 'OnAfterSetFilters', '', false, false)]
    local procedure CU5804_OnAfterSetFilters(var ValueEntry: Record "Value Entry"; var Item: Record Item)
    begin
        // AD Le 17-09-2009 => Non Stock‚
        ValueEntry.SETRANGE("Sans mouvement de stock", FALSE);
        // FIN AD Le 17-09-2009
    end;
    //<< Mig CU 5804
    //>> Mig CU 5815
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Sales Shipment Line", 'OnAfterCode', '', false, false)]
    local procedure CU5815_OnAfterCode(var SalesShipmentLine: Record "Sales Shipment Line")
    var
        TmpSalesShptHeader: Record "Sales Shipment Header";
        LCUSalesPost: Codeunit "Sales-Post";
        CodeunitsFunctions: Codeunit "Codeunits Functions";
        LSalesHeader: Record "Sales Header";
    begin
        // MC Le 15-11-2011 => On repasse la commande dans le bon ‚tat
        /*IF LSalesHeader.GET(LSalesHeader."Document Type"::Order, SalesShipmentLine."Order No.") THEN
          LCUSalesPost.UpdateOrderShipmentStatus(LSalesHeader);*/
        // FIN MC Le 15-11-2011

        TmpSalesShptHeader.GET(SalesShipmentLine."Document No.");
        TmpSalesShptHeader.MajEntierementFacturé();

        LSalesHeader.GET(LSalesHeader."Document Type"::Order, SalesShipmentLine."Order No.");
        CodeunitsFunctions.UpdateOrderShipmentStatus(LSalesHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Sales Shipment Line", 'OnGetInvoicedShptEntriesOnAfterSetFilters', '', false, false)]
    local procedure CU5815_OnGetInvoicedShptEntriesOnAfterSetFilters(var ItemLedgerEntry: Record "Item Ledger Entry"; SalesShipmentLine: Record "Sales Shipment Line")
    var
        SalesShipmentLine2: Record "Sales Shipment Line";
    begin
        if not ItemLedgerEntry.FindSet() then begin
            SalesShipmentLine2.get(ItemLedgerEntry.GetFilter("Document No."), ItemLedgerEntry.GetFilter("Document Line No."));
            if SalesShipmentLine2."Sans mouvement de stock" then
                Clear(ItemLedgerEntry);
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Sales Shipment Line", 'OnCheckSalesShptLineOnBeforeHasInvoicedNotReturnedQuantity', '', false, false)]
    local procedure CU5815_OnCheckSalesShptLineOnBeforeHasInvoicedNotReturnedQuantity(SalesShptLine: Record "Sales Shipment Line"; var IsHandled: Boolean)
    var
        Text005_Err: Label 'This shipment has already been invoiced. Undo Shipment can be applied only to posted, but not invoiced shipments.';
    begin
        // AD Le 28-04-2010 => GDI -> on veut annuler des comptes gen‚raux
        // AD Le 04-01-2012 => SYMTA -> Pour annuler des not stock‚
        IF (SalesShptLine.Type = SalesShptLine.Type::"G/L Account") OR (SalesShptLine.Type = SalesShptLine.Type::Resource) OR (SalesShptLine."Sans mouvement de stock") THEN BEGIN
            IF SalesShptLine."Qty. Shipped Not Invoiced" <> SalesShptLine.Quantity THEN
                ERROR(Text005_Err);
            EXIT;
        END;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Sales Shipment Line", 'OnBeforePostItemJnlLine', '', false, false)]
    local procedure CU5815_OnBeforePostItemJnlLine(var SalesShipmentLine: Record "Sales Shipment Line"; var DocLineNo: Integer; var ItemLedgEntryNo: Integer; var IsHandled: Boolean; var TempGlobalItemLedgEntry: Record "Item Ledger Entry" temporary; var TempGlobalItemEntryRelation: Record "Item Entry Relation" temporary; var TempWhseJnlLine: Record "Warehouse Journal Line" temporary; var NextLineNo: Integer)
    begin
        // AD Le 28-04-2010 => GDI -> on veut annuler des comptes gen‚raux
        IF (SalesShipmentLine.Type = SalesShipmentLine.Type::"G/L Account") OR (SalesShipmentLine.Type = SalesShipmentLine.Type::Resource) THEN begin
            ItemLedgEntryNo := 0;
            IsHandled := true;
        end;
        // FIN AD Le 28-04-2010
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Sales Shipment Line", 'OnAfterCopyItemJnlLineFromSalesShpt', '', false, false)]
    local procedure CU5815_OnAfterCopyItemJnlLineFromSalesShpt(var ItemJournalLine: Record "Item Journal Line"; SalesShipmentHeader: Record "Sales Shipment Header"; SalesShipmentLine: Record "Sales Shipment Line"; var TempWhseJnlLine: Record "Warehouse Journal Line" temporary; var WhseUndoQty: Codeunit "Whse. Undo Quantity")
    begin
        // AD Le 04-01-2012 => SYMTA -> Pour annuler des not stock‚
        IF SalesShipmentLine."Sans mouvement de stock" THEN BEGIN
            ItemJournalLine.Correction := FALSE;
            ItemJournalLine."Sans mouvement de stock" := SalesShipmentLine."Sans mouvement de stock";
        END;
        // FIN AD Le 04-01-2012
    end;
    //<< Mig CU 5815
    //>> Mig CU 5817
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Posting Management", 'OnUpdateSalesLineOnBeforeInitOustanding', '', false, false)]
    local procedure CU5817_OnUpdateSalesLineOnBeforeInitOustanding(var SalesLine: Record "Sales Line"; var UndoQty: Decimal; var UndoQtyBase: Decimal)
    begin
        if SalesLine."Document Type" = SalesLine."Document Type"::Order then
            SalesLine."Qty. to Ship" := SalesLine."Outstanding Quantity"; // AD Le 09-03-2016
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Posting Management", 'OnUpdateSalesLineOnBeforeSalesLineModify', '', false, false)]
    local procedure CU5817_OnUpdateSalesLineOnBeforeSalesLineModify(var SalesLine: Record "Sales Line")
    var
        SalesSetup: Record "Sales & Receivables Setup";
    begin
        if SalesLine."Document Type" <> SalesLine."Document Type"::Order then
            exit;
        SalesSetup.Get();
        if SalesSetup."Default Quantity to Ship" = SalesSetup."Default Quantity to Ship"::Blank then
            SalesLine.InitQtyToShip();
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Undo Posting Management", 'OnBeforeTestPostedWhseShipmentLine', '', false, false)]
    local procedure CU5817_OnBeforeTestPostedWhseShipmentLine(var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;
    //<< Mig CU 5817
    //<< Mig CU 5847
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Get Average Cost Calc Overview", 'OnEntriesExistOnBeforeFind', '', false, false)]
    local procedure CU5847_OnEntriesExistOnBeforeFind(var ValueEntry: Record "Value Entry"; var Item: Record Item; var AverageCostCalcOverview: Record "Average Cost Calc. Overview")
    begin
        // AD Le 23-09-2009 => Non Stock‚
        ValueEntry.SETRANGE("Sans mouvement de stock", FALSE);
        // FIN AD Le 23-09-2009
    end;
    //>> Mig CU 5847
    //>> Mig CU 5895
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Inventory Adjustment", 'OnInvtToAdjustExistOnBeforeCopyItemToItem', '', false, false)]
    local procedure CU5895_OnInvtToAdjustExistOnBeforeCopyItemToItem(var Item: Record Item)
    begin
        // AD Le 07-05-2010 => Pourquoi cette ligne ?
        //  IF IsOnlineAdjmt THEN
        Item.SETRANGE("Allow Online Adjustment");
        // FIN AD Le 07-05-2010

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Inventory Adjustment", 'OnBeforeUpdateWindow', '', false, false)]
    local procedure CU5895_OnBeforeUpdateWindow(var IsHandled: Boolean)
    begin
        if CduGSingleInstance.FctIsOnlineAdjmt() then
            IsHandled := true;
    end;
    //<< Mig CU 5895
    //>> Mig CU 6620
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnCopySalesDocOnBeforeCopySalesDocInvLine', '', false, false)]
    local procedure CU6620_OnCopySalesDocOnBeforeCopySalesDocInvLine(var FromSalesInvoiceHeader: Record "Sales Invoice Header"; var ToSalesHeader: Record "Sales Header")
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // MCO Le 28-11-2017 => Suivi des informations de livraison pour client comptoir
        LCodeunitsFunctions.UpdateAdressClientComptoir(ToSalesHeader);
        // FIN MCO Le 28-11-2017 => Suivi des informations de livraison pour client comptoir
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnCopySalesShptLinesToDocOnAfterCalcNextLineNo', '', false, false)]
    local procedure CU6620_OnCopySalesShptLinesToDocOnAfterCalcNextLineNo(FromSalesHeader: Record "Sales Header"; FromSalesShptLine: Record "Sales Shipment Line"; var InsertDocNoLine: Boolean; var NextLineNo: Integer; var ToSalesHeader: Record "Sales Header")
    begin
        CduGSingleInstance.FctSetPostingDate(FromSalesShptLine."Posting Date");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnBeforeInsertOldSalesDocNoLine', '', false, false)]
    local procedure CU6620_OnBeforeInsertOldSalesDocNoLine(var ToSalesHeader: Record "Sales Header"; var ToSalesLine: Record "Sales Line"; OldDocNo: Code[20]; OldDocType: Option; var IsHandled: Boolean)
    var
        GPostingDate: Date;
        ESK001Lbl: Label 'Du %1', Comment = '%1 = Posting Date ';
        ESK015Lbl: label '%1 %2:', Comment = '%1 = Doc Type ; %2 = DOc No ';
        ESK013Lbl: label 'Shipment No.,Invoice No.,Return Receipt No.,Credit Memo No.';
    begin
        ToSalesLine.Type := ToSalesLine.Type::" "; // AD Le 13-11-2015 => MIG 2015 (type article par d‚faut)
        ToSalesLine.Description := STRSUBSTNO(ESK015Lbl, SELECTSTR(OldDocType, ESK013Lbl), OldDocNo);
        GPostingDate := CduGSingleInstance.FctGetPostingDate();
        IF GPostingDate <> 0D THEN
            ToSalesLine.Description += ' ' + STRSUBSTNO(ESK001Lbl, FORMAT(GPostingDate, 0, '<Day,2>/<Month,2>/<Year4>'));  // AD Le 27-09-2016 => REGIE
                                                                                                                           // FIN CFR le 19/10/2022 => R‚gie #67826   
        CduGSingleInstance.FctSetPostingDate(0D);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnCopySalesDocLineOnAfterCalcCopyThisLine', '', false, false)]
    local procedure CU6620_OnCopySalesDocLineOnAfterCalcCopyThisLine(var ToSalesHeader: Record "Sales Header"; var FromSalesHeader: Record "Sales Header"; var ToSalesLine: Record "Sales Line"; RoundingLineInserted: Boolean; var CopyThisLine: Boolean; RecalculateLines: Boolean)
    begin
        if not CopyThisLine then
            exit;
        // AD Le 30-11-2009 => DEEE -> On ne copie jamais les frais attach‚s
        IF (ToSalesLine."Attached to Line No." <> 0) AND (ToSalesLine.Type IN [ToSalesLine.Type::Resource,
                                                                                   ToSalesLine.Type::"Charge (Item)"])
           AND (ToSalesLine."No." = 'DEEE') THEN
            CopyThisLine := FALSE;
        // FIN AD Le 30-11-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnAfterInsertToSalesLine', '', false, false)]
    local procedure CU6620_OnAfterInsertToSalesLine(var ToSalesLine: Record "Sales Line"; FromSalesLine: Record "Sales Line"; RecalculateLines: Boolean; DocLineNo: Integer; FromSalesDocType: Enum "Sales Document Type From"; FromSalesHeader: Record "Sales Header"; var NextLineNo: Integer; var ToSalesHeader: Record "Sales Header")
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-11-2009 => DEEE -> Ajout de la DEEE
        IF TransferItemCharge.SalesCheckIfAnyLink(ToSalesLine) THEN BEGIN
            TransferItemCharge.InsertSalesLink(ToSalesLine);
            NextLineNo := NextLineNo + 10000;
            IF TransferItemCharge.MakeUpdate() THEN 
                TransferItemCharge.UpdateQtySalesLink(ToSalesLine);
             END;
        // FIN AD Le 30-11-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnUpdateSalesLine', '', false, false)]
    local procedure CU6620_OnUpdateSalesLine(var ToSalesLine: Record "Sales Line"; var FromSalesLine: Record "Sales Line")
    begin
        //LM le 22-05-2013 copie ref_active
        ToSalesLine.VALIDATE(ToSalesLine."Recherche référence", FromSalesLine."Recherche référence");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnBeforeInsertOldSalesCombDocNoLineProcedure', '', false, false)]
    local procedure CU6620_OnBeforeInsertOldSalesCombDocNoLineProcedure(var ToSalesHeader: Record "Sales Header"; var ToSalesLine: Record "Sales Line"; CopyFromInvoice: Boolean; OldDocNo: Code[20]; OldDocNo2: Code[20]; var NextLineNo: Integer; var IsHandled: Boolean)
    var
        TranslationHelper: Codeunit "Translation Helper";
        ESK016Txt: Label 'Inv. No. ,Shpt. No. ,Cr. Memo No. ,Rtrn. Rcpt. No. ';
        ESK018Txt: Label '%1 - %2:' , Comment = '%1 =Doc No ; %2 = Doc No2 ';
    begin
        IsHandled := true;

        NextLineNo := NextLineNo + 10000;
        ToSalesLine.Init();
        ToSalesLine."Line No." := NextLineNo;
        ToSalesLine."Document Type" := ToSalesHeader."Document Type";
        ToSalesLine."Document No." := ToSalesHeader."No.";
        ToSalesLine.Type := ToSalesLine.Type::" "; // AD Le 13-11-2015 => MIG 2015 (type article par d‚faut)

        TranslationHelper.SetGlobalLanguageByCode(ToSalesHeader."Language Code");
        if CopyFromInvoice then
            ToSalesLine.Description :=
              StrSubstNo(
                ESK018Txt,
                CopyStr(SelectStr(1, ESK016Txt) + OldDocNo, 1, 48),
                CopyStr(SelectStr(2, ESK016Txt) + OldDocNo2, 1, 48))
        else
            ToSalesLine.Description :=
              StrSubstNo(
                ESK018Txt,
                CopyStr(SelectStr(3, ESK016Txt) + OldDocNo, 1, 48),
                CopyStr(SelectStr(4, ESK016Txt) + OldDocNo2, 1, 48));
        TranslationHelper.RestoreGlobalLanguage();
        ToSalesLine.Insert();
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnBeforeInsertOldPurchDocNoLine', '', false, false)]
    local procedure CU6620_OnBeforeInsertOldPurchDocNoLine(var ToPurchLine: Record "Purchase Line"; ToPurchHeader: Record "Purchase Header"; var IsHandled: Boolean; OldDocType: Option; OldDocNo: Code[20])
    var
        ESK014Txt: Label 'Receipt No.,Invoice No.,Return Shipment No.,Credit Memo No.';
        ESK015Txt: Label '%1 %2:', Comment = '%1 = Ol DOc  Type ; %2 = Old Doc No';
    begin
        ToPurchLine.Type := ToPurchLine.Type::" "; // AD Le 13-11-2015 => MIG 2015 (type article par d‚faut)
        ToPurchLine.Description := STRSUBSTNO(ESK015Txt, SELECTSTR(OldDocType, ESK014Txt), OldDocNo);

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Copy Document Mgt.", 'OnBeforeInsertOldPurchCombDocNoLine', '', false, false)]
    local procedure CU6620_OnBeforeInsertOldPurchCombDocNoLine(var ToPurchLine: Record "Purchase Line"; var ToPurchHeader: Record "Purchase Header"; CopyFromInvoice: Boolean; OldDocNo2: Code[20]; OldDocNo: Code[20])
    begin
        ToPurchLine.Type := ToPurchLine.Type::" "; // AD Le 13-11-2015 => MIG 2015 (type article par d‚faut)

    end;
    //<< Mig CU 6620
    //>> Mig CU 6648
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Get Return Shipments", 'OnRunOnAfterSetReturnShptLineFilters', '', false, false)]
    local procedure CU6648_OnRunOnAfterSetReturnShptLineFilters(var ReturnShipmentLine: Record "Return Shipment Line"; PurchaseHeader: Record "Purchase Header")
    begin
        // CFR le 14/09/2021 => R‚gie : Gestion champ [D‚j… extraite sur avoir] de la table 6651
        // CFR le 21/09/2021 => R‚gie : Ordre des champs de filtre, laisser le return Order No. en dernier
        ReturnShipmentLine.SETRANGE("Déjà Extraite sur Avoir", FALSE);

        ReturnShipmentLine.SETFILTER("Return Order No.", '<>%1|%2', '', '');      // MCO Le 09-12-2015 => Demande de bernard
    end;
    //<< Mig CU 6648
    //>> Mig CU 7301
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse. Jnl.-Register Line", 'OnBeforeCode', '', false, false)]
    local procedure CU7301_OnBeforeCode(var WarehouseJournalLine: Record "Warehouse Journal Line"; var WhseEntryNo: Integer)
    begin
        // AD Le 03-07-2012 => Pour g‚n‚rer les lignes magasin pour les qte n‚gatives sur les commandes en validation par BP
        WhseEntryNo := 0; // Pour chercher a chaque fois le nø de ligne
        // FIN AD Le 03-07-2012 => + le commentaire du END 4 Lignes + loin
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse. Jnl.-Register Line", 'OnDeleteFromBinContentOnAfterSetFiltersForBinContent', '', false, false)]
    local procedure CU7301_OnDeleteFromBinContentOnAfterSetFiltersForBinContent(var BinContent: Record "Bin Content"; WarehouseEntry: Record "Warehouse Entry"; var WhseReg: Record "Warehouse Register"; var WhseJnlLine: Record "Warehouse Journal Line"; var WhseEntryNo: Integer; var IsHandled: Boolean)
    begin
        BinContent.CalcFields("Quantity (Base)", "Positive Adjmt. Qty. (Base)", "Put-away Quantity (Base)");
        if BinContent."Quantity (Base)" + WarehouseEntry."Qty. (Base)" <> 0 then
            IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse. Jnl.-Register Line", 'OnInitWhseEntryCopyFromWhseJnlLine', '', false, false)]
    local procedure CU7301_OnInitWhseEntryCopyFromWhseJnlLine(var WarehouseEntry: Record "Warehouse Entry"; var WarehouseJournalLine: Record "Warehouse Journal Line"; OnMovement: Boolean; Sign: Integer; Location: Record Location; BinCode: Code[20]; var IsHandled: Boolean)
    begin
        // AD Le 13-01-2012 => Suivi des champs
        WarehouseEntry.Commentaire := WarehouseJournalLine.Commentaire;
        // FIN AD Le 13-01-2012
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse. Jnl.-Register Line", 'OnBeforeDeleteFromBinContent', '', false, false)]
    // local procedure CU7301_OnBeforeDeleteFromBinContent(var WarehouseEntry: Record "Warehouse Entry"; var IsHandled: Boolean)
    // var
    //     FromBinContent: Record "Bin Content";
    //     WhseEntry2: Record "Warehouse Entry";
    //     WhseItemTrackingSetup: Record "Item Tracking Setup";
    //     ItemTrackingMgt: Codeunit "Item Tracking Management";
    //     Sign: Integer;
    // begin
    //     IsHandled := true;

    //     FromBinContent.Get(
    //         WarehouseEntry."Location Code", WarehouseEntry."Bin Code", WarehouseEntry."Item No.", WarehouseEntry."Variant Code",
    //         WarehouseEntry."Unit of Measure Code");
    //     ItemTrackingMgt.GetWhseItemTrkgSetup(FromBinContent."Item No.", WhseItemTrackingSetup);
    //     WhseItemTrackingSetup.CopyTrackingFromWhseEntry(WarehouseEntry);
    //     FromBinContent.SetTrackingFilterFromItemTrackingSetupIfRequired(WhseItemTrackingSetup);
    //     FromBinContent.CalcFields("Quantity (Base)", "Positive Adjmt. Qty. (Base)", "Put-away Quantity (Base)");
    //     if FromBinContent."Quantity (Base)" + WarehouseEntry."Qty. (Base)" = 0 then begin
    //         WhseEntry2.SetCurrentKey(
    //             "Item No.", "Bin Code", "Location Code", "Variant Code", "Unit of Measure Code");
    //         WhseEntry2.SetRange("Item No.", WarehouseEntry."Item No.");
    //         WhseEntry2.SetRange("Bin Code", WarehouseEntry."Bin Code");
    //         WhseEntry2.SetRange("Location Code", WarehouseEntry."Location Code");
    //         WhseEntry2.SetRange("Variant Code", WarehouseEntry."Variant Code");
    //         WhseEntry2.SetRange("Unit of Measure Code", WarehouseEntry."Unit of Measure Code");
    //         WhseEntry2.SetTrackingFilterFromItemTrackingSetupIfRequired(WhseItemTrackingSetup);
    //         WhseEntry2.CalcSums(Cubage, Weight, "Qty. (Base)");
    //         WarehouseEntry.Cubage := -WhseEntry2.Cubage;
    //         WarehouseEntry.Weight := -WhseEntry2.Weight;
    //         if WhseEntry2."Qty. (Base)" + WarehouseEntry."Qty. (Base)" <> 0 then
    //             RegisterRoundResidual(WarehouseEntry, WhseEntry2);

    //         FromBinContent.ClearTrackingFilters();
    //         FromBinContent.CalcFields("Quantity (Base)");
    //         if FromBinContent."Quantity (Base)" + WarehouseEntry."Qty. (Base)" = 0 then
    //             if (FromBinContent."Positive Adjmt. Qty. (Base)" = 0) and
    //                 (FromBinContent."Put-away Quantity (Base)" = 0) and
    //                 (not FromBinContent.Fixed)
    //             then begin
    //                 FromBinContent.Delete();
    //             end;
    //         /*
    // end else begin
    //     FromBinContent.CalcFields(Quantity);
    //     if FromBinContent.Quantity + WarehouseEntry.Quantity = 0 then begin
    //         WarehouseEntry."Qty. (Base)" := -FromBinContent."Quantity (Base)";
    //         Sign := WhseJnlLine."Qty. (Base)" / WhseJnlLine."Qty. (Absolute, Base)";
    //         WhseJnlLine."Qty. (Base)" := WarehouseEntry."Qty. (Base)" * Sign;
    //         WhseJnlLine."Qty. (Absolute, Base)" := Abs(WarehouseEntry."Qty. (Base)");
    //     end else
    //         if FromBinContent."Quantity (Base)" + WarehouseEntry."Qty. (Base)" < 0 then begin
    //                 FromBinContent.FieldError(
    //                     "Quantity (Base)",
    //                     StrSubstNo(Text000, FromBinContent."Quantity (Base)", -(FromBinContent."Quantity (Base)" + WarehouseEntry."Qty. (Base)")));
    //         end;
    //         */
    //     end;
    // end;
    //<< Mig CU 7301
    //>> Mig CU 7302
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"WMS Management", 'OnAfterCreateWhseJnlLine', '', false, false)]
    local procedure CU7302_OnAfterCreateWhseJnlLine(var WhseJournalLine: Record "Warehouse Journal Line"; ItemJournalLine: Record "Item Journal Line"; ToTransfer: Boolean)
    begin
        // AD Le 13-01-2012
        WhseJournalLine.Commentaire := ItemJournalLine.Commentaire;
        // FIN AD Le 13-01-2012
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"WMS Management", 'OnBeforeGetDefaultBin', '', false, false)]
    local procedure CU7302_OnBeforeGetDefaultBin(ItemNo: Code[20]; VariantCode: Code[10]; LocationCode: Code[10]; var BinCode: Code[20]; var Result: Boolean; var IsHandled: Boolean)
    var
        Location: Record Location;
    begin
        Location.GET(LocationCode);
        IF NOT Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 24-04-2011
                IF NOT (Location."Bin Mandatory" AND (NOT Location."Require Receive") AND (NOT Location."Require Shipment")) THEN begin
                    Result := FALSE;
                    IsHandled := true;
                end;
    end;
    //<< Mig CU 7302
    //>> Mig CU 3719
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bin Content Create", 'OnBinCreateOnBeforeInsertBinContent', '', false, false)]
    local procedure CU7319_OnBinCreateOnBeforeInsertBinContent(var BinContent: Record "Bin Content"; BinCreateLine2: Record "Bin Creation Worksheet Line")
    begin
        // CFR le 25/10/2023 - R‚gie : alimenter le [Zone Code]
        BinContent."Zone Code" := BinCreateLine2."Zone Code";
        // FIN CFR le 25/10/2023
    end;
    //<< Mig CU 3919
    //>> Mig CU 3720
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse. Undo Quantity", 'OnBeforeTempWhseJnlLineInsert', '', false, false)]
    local procedure CU7310_OnBeforeTempWhseJnlLineInsert(var WarehouseJournalLine: Record "Warehouse Journal Line"; WarehouseEntry: Record "Warehouse Entry"; ItemJournalLine: Record "Item Journal Line")
    var
        LBincontent: Record "Bin Content";
    begin
        // AD Le 31-03-2015 => Symta -> On veut remettre
        CLEAR(LBincontent);
        LBincontent.SETRANGE("Location Code", WarehouseJournalLine."Location Code");
        LBincontent.SETRANGE("Item No.", ItemJournalLine."Item No.");
        LBincontent.SETRANGE("Variant Code", ItemJournalLine."Variant Code");
        LBincontent.SETRANGE("Unit of Measure Code", WarehouseEntry."Unit of Measure Code");
        LBincontent.SETRANGE(Fixed, TRUE);
        LBincontent.SETRANGE(Default, TRUE);
        IF LBincontent.FINDFIRST() THEN BEGIN
            WarehouseJournalLine."Zone Code" := LBincontent."Zone Code";
            WarehouseJournalLine."Bin Code" := LBincontent."Bin Code";
        END;
        // FIN AD Le 31-03-2015
    end;
    //<< Mig CU 3920
    //>> Mig CU 10860
    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Payment Management", 'OnCopyLigBorOnBeforeToPaymentLineInsert', '', false, false)]
    // local procedure CU10860_OnCopyLigBorOnBeforeToPaymentLineInsert(var ToPaymentLine: Record "Payment Line"; var Process: Record "Payment Class")
    // var
    // begin
    //     //FB
    //     IF Step."Sauvegarder date de Valeur" THEN
    //         ToPaymentLine."Date de valeur" := PaymentHeader."Posting Date";
    //     //     ELSE
    //     //      ToPaymentLine."Date de valeur":=0D;
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Payment Management", 'OnGenerInvPostingBufferOnAfterGetDescriptionForInvPostingBuffer', '', false, false)]
    local procedure CU10860_OnGenerInvPostingBufferOnAfterGetDescriptionForInvPostingBuffer(var StepLedger: record "Payment Step Ledger"; var PaymentHeader: record "Payment Header"; var PaymentLine: record "Payment Line"; var Description: Text[98])
    begin
        IF (StepLedger.Description = '') THEN
            Description := PaymentLine.Désignation
    end;
    //<< Mig CU 10860
    //>> Mig CU 80
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterCheckAndUpdate', '', false, false)]
    local procedure CU80_OnAfterCheckAndUpdate(var SalesHeader: Record "Sales Header"; CommitIsSuppressed: Boolean; PreviewMode: Boolean)
    var
        CustInvoiceDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
    begin
        //FBRUN le 22/09/2009 -> gestion des remises supl‚mentaire
        IF SalesHeader."Document Type" = SalesHeader."Document Type"::Order THEN BEGIN
            CLEAR(CustInvoiceDiscSup);
            CustInvoiceDiscSup.Calc_Disc_line(SalesHeader, 0);
            CustInvoiceDiscSup.Calc_Disc_Header(SalesHeader, 0);
        END;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesLines', '', false, false)]
    local procedure CU80_OnAfterPostSalesLines(var SalesHeader: Record "Sales Header"; var SalesShipmentHeader: Record "Sales Shipment Header"; var SalesInvoiceHeader: Record "Sales Invoice Header"; var SalesCrMemoHeader: Record "Sales Cr.Memo Header"; var ReturnReceiptHeader: Record "Return Receipt Header"; WhseShip: Boolean; WhseReceive: Boolean; var SalesLinesProcessed: Boolean; CommitIsSuppressed: Boolean; EverythingInvoiced: Boolean; var TempSalesLineGlobal: Record "Sales Line" temporary)
    var
        WhseEmployee: Record "Warehouse Employee";
        CodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // MCO Le 26-10-2017 => Migration
        IF SalesHeader.Ship THEN
            IF WhseEmployee.GET(USERID, SalesShipmentHeader."Location Code") THEN
                IF WhseEmployee."Validation Logistique Direct" THEN BEGIN
                    CodeunitsFunctions.ShipmentLabel(SalesShipmentHeader);
                END;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostGLAndCustomer', '', false, false)]
    local procedure CU80_OnAfterPostGLAndCustomer(var SalesHeader: Record "Sales Header"; var GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line"; TotalSalesLine: Record "Sales Line"; TotalSalesLineLCY: Record "Sales Line"; CommitIsSuppressed: Boolean;
        WhseShptHeader: Record "Warehouse Shipment Header"; WhseShip: Boolean; var TempWhseShptHeader: Record "Warehouse Shipment Header"; var SalesInvHeader: Record "Sales Invoice Header"; var SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        var CustLedgEntry: Record "Cust. Ledger Entry"; var SrcCode: Code[10]; GenJnlLineDocNo: Code[20]; GenJnlLineExtDocNo: Code[35]; var GenJnlLineDocType: Enum "Gen. Journal Document Type"; PreviewMode: Boolean;
                                                                                                                                                                   DropShipOrder: Boolean)
    var
        WhseEmployee: Record "Warehouse Employee";
        SalesSetup: Record "Sales & Receivables Setup";
        TabEchéance: ARRAY[3] OF Date;
        TabMontant: ARRAY[3] OF Decimal;
        TabPourcentage: ARRAY[3] OF Decimal;
        BordereauNo: Code[20];
        index: Integer;
        CodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        SalesSetup.GET();

        IF SalesHeader.Invoice THEN BEGIN

            //FBRUN le 20/04/2011 =>CREATION BORDEREAU
            IF (SalesHeader."Multi echéance") AND (SalesHeader."Due Date" > SalesSetup."Due Date 3") THEN
                SalesHeader."Multi echéance" := FALSE;

            //IF ("Multi echéance") AND ("Scénario paiement"='') THEN
            //ERROR(text50001);

            CLEAR(TabEchéance);
            CLEAR(TabMontant);

            // AD Le 26-01-2012 => Changement de méthode pour les LCR
            // IF ("Scénario paiement"<>'') AND ("Document Type"<>"Document Type"::"Credit Memo") THEN CODE D'ORIGINE
            IF (SalesHeader."Multi echéance" OR SalesHeader."Echéances fractionnées") THEN
            // FIN AD Le 26-01-2012
            BEGIN
                CodeunitsFunctions.CreationBordereau(BordereauNo, SalesHeader);
                IF SalesHeader."Multi echéance" THEN BEGIN
                    TabEchéance[1] := SalesSetup."Due Date 1";
                    TabEchéance[2] := SalesSetup."Due Date 2";
                    TabEchéance[3] := SalesSetup."Due Date 3";
                    TabMontant[1] := ROUND(TotalSalesLine."Amount Including VAT" * (SalesSetup."Pourcentage échéance 1" / 100), 0.01, '=');
                    TabMontant[2] := ROUND(TotalSalesLine."Amount Including VAT" * (SalesSetup."Pourcentage échéance 2" / 100), 0.01, '=');
                    TabMontant[3] := TotalSalesLine."Amount Including VAT" - (TabMontant[1] + TabMontant[2]);
                    TabPourcentage[1] := SalesSetup."Pourcentage échéance 1";
                    TabPourcentage[2] := SalesSetup."Pourcentage échéance 2";
                    TabPourcentage[3] := 100 - (SalesSetup."Pourcentage échéance 1" + SalesSetup."Pourcentage échéance 2");
                    IF DATE2DMY(SalesHeader."Posting Date", 3) = DATE2DMY(TabEchéance[1], 3) THEN
                        WHILE DATE2DMY(SalesHeader."Posting Date", 2) >= DATE2DMY(TabEchéance[1], 2) DO BEGIN
                            TabEchéance[1] := TabEchéance[2];
                            TabEchéance[2] := TabEchéance[3];
                            TabEchéance[3] := 0D;

                            TabMontant[1] := TabMontant[1] + TabMontant[2];
                            TabMontant[2] := TabMontant[3];
                            TabMontant[3] := 0;

                            TabPourcentage[1] := TabPourcentage[1] + TabPourcentage[2];
                            TabPourcentage[2] := TabPourcentage[3];
                            TabPourcentage[3] := 0;
                        END;
                END;

                IF SalesHeader."Echéances fractionnées" THEN BEGIN
                    TabEchéance[1] := SalesHeader."Due Date";
                    TabEchéance[2] := SalesHeader."Due Date 2";
                    TabEchéance[3] := 0D;
                    TabMontant[1] := ROUND(TotalSalesLine."Amount Including VAT" * (SalesHeader."Taux Premiere Fraction" / 100), 0.01, '=');
                    TabMontant[2] := TotalSalesLine."Amount Including VAT" - TabMontant[1];
                    TabMontant[3] := 0;

                    TabPourcentage[1] := SalesHeader."Taux Premiere Fraction";
                    TabPourcentage[2] := 100 - (SalesHeader."Taux Premiere Fraction");
                    TabPourcentage[3] := 0;
                END;

                index := 1;
                IF TabEchéance[1] = 0D THEN
                    CodeunitsFunctions.InsertionLigneBordereau(BordereauNo, GenJnlLineDocNo, SalesHeader."Due Date", TotalSalesLine."Amount Including VAT", 100, SalesHeader)
                ELSE BEGIN
                    FOR index := 1 TO 3 DO BEGIN
                        IF TabEchéance[index] <> 0D THEN
                            CodeunitsFunctions.InsertionLigneBordereau(BordereauNo, GenJnlLineDocNo, TabEchéance[index], TabMontant[index], TabPourcentage[index], SalesHeader);
                    END;
                END;
            END;
        END;
        //FIN FBRUN
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterFinalizePosting', '', false, false)]
    local procedure CU80_OnAfterFinalizePosting(var SalesHeader: Record "Sales Header"; var SalesShipmentHeader: Record "Sales Shipment Header"; var SalesInvoiceHeader: Record "Sales Invoice Header"; var SalesCrMemoHeader: Record "Sales Cr.Memo Header"; var ReturnReceiptHeader: Record "Return Receipt Header"; var GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line"; CommitIsSuppressed: Boolean; PreviewMode: Boolean)
    var
        SalesSetup: Record "Sales & Receivables Setup";
        recPerm: Record "License Permission";
    //TODO cTFPostFunctions: Codeunit 52101147;
    begin
        SalesSetup.GET();
        /*TODO 
        //-TF  (GR le 30/11/2016 : ajout Therefore)
        recPerm.GET(recPerm."Object Type"::Codeunit, CODEUNIT::"Pre-Post Functions");
        IF (recPerm."Execute Permission" = recPerm."Execute Permission"::Yes) THEN BEGIN
            IF SalesHeader.Ship THEN
                IF (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) OR
                   ((SalesHeader."Document Type" = SalesHeader."Document Type"::Invoice) AND SalesSetup."Shipment on Invoice")
                THEN
                    cTFPostFunctions.AddDocumentToQueue(
                      cTFPostFunctions.GetTableID(SalesShipmentHeader.TABLENAME()), // Tablename
                      SalesShipmentHeader."No.",                                      // Document No.
                      SalesShipmentHeader."Posting Date",                             // Posting Date
                      0,                                                          // Entry No.
                      0,                                                          // Document Type
                      '',                                                         // Identification ID
                      SalesShipmentHeader."Business Case"                             // Business Case
                    );

            IF SalesHeader.Receive THEN
                IF (SalesHeader."Document Type" = SalesHeader."Document Type"::"Return Order") OR
                   ((SalesHeader."Document Type" = SalesHeader."Document Type"::"Credit Memo") AND SalesSetup."Return Receipt on Credit Memo")
                THEN
                    cTFPostFunctions.AddDocumentToQueue(
                      cTFPostFunctions.GetTableID(ReturnReceiptHeader.TABLENAME()), // Tablename
                      ReturnReceiptHeader."No.",                                      // Document No.
                      ReturnReceiptHeader."Posting Date",                             // Posting Date
                      0,                                                           // Entry No.
                      0,                                                           // Document Type
                      '',                                                          // Identification ID
                      ReturnReceiptHeader."Business Case"                             // Business Case
                    );


            IF SalesHeader.Invoice THEN
                IF SalesHeader."Document Type" IN [SalesHeader."Document Type"::Order, SalesHeader."Document Type"::Invoice] THEN BEGIN
                    cTFPostFunctions.AddDocumentToQueue(
                      cTFPostFunctions.GetTableID(SalesInvoiceHeader.TABLENAME()), // Tablename
                      SalesInvoiceHeader."No.",                                      // Document No.
                      SalesInvoiceHeader."Posting Date",                             // Posting Date
                      0,                                                         // Entry No.
                      0,                                                         // Document Type
                      '',                                                        // Identification ID
                      SalesInvoiceHeader."Business Case"                             // Business Case
                    );
                END ELSE BEGIN
                    cTFPostFunctions.AddDocumentToQueue(
                      cTFPostFunctions.GetTableID(SalesCrMemoHeader.TABLENAME()), // Tablename
                      SalesCrMemoHeader."No.",                                      // Document No.
                      SalesCrMemoHeader."Posting Date",                             // Posting Date
                      0,                                                            // Entry No.
                      0,                                                            // Document Type
                      '',                                                           // Identification ID
                      SalesCrMemoHeader."Business Case"                             // Business Case
                    );
                END;
        END;
        //+TF
        */
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostSalesLineOnAfterInsertReturnReceiptLine', '', false, false)]
    local procedure CU80_OnPostSalesLineOnAfterInsertReturnReceiptLine(var SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; ReturnRcptHeader: Record "Return Receipt Header"; RoundingLineInserted: Boolean)
    begin
        IF ReturnRcptHeader."No." <> '' THEN BEGIN
            // FBRUN le 02/05/2011 =>gestion des centrales
            ReturnRcptHeader.RécupérerFacturationCentral();
            ReturnRcptHeader.MajEntierementFacturé();
        END;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostSalesLineOnBeforeInsertInvoiceLine', '', false, false)]
    local procedure CU80_OnPostSalesLineOnBeforeInsertInvoiceLine(SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line"; var IsHandled: Boolean; xSalesLine: Record "Sales Line"; SalesInvHeader: Record "Sales Invoice Header"; var ShouldInsertInvoiceLine: Boolean)
    var
        LignesFraisAssociés: Record "Sales Line";
        AffectationFrais: Record "Item Charge Assignment (Sales)";
    begin
        //DEEE AD Le 30-10-2006 => DEEE -> Gestion Frais associ‚s  Pour la facturation.
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", xSalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", xSalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, SalesLine.Type::"Charge (Item)");
        LignesFraisAssociés.SETRANGE("Attached to Line No.", xSalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Invoice", xSalesLine."Qty. to Invoice");
            LignesFraisAssociés.MODIFY();
            AffectationFrais.RESET();
            AffectationFrais.SETRANGE("Document Type", xSalesLine."Document Type");
            AffectationFrais.SETRANGE("Document No.", xSalesLine."Document No.");
            AffectationFrais.SETRANGE("Document Line No.", LignesFraisAssociés."Line No.");
            AffectationFrais.SETRANGE("Item Charge No.", LignesFraisAssociés."No.");
            AffectationFrais.SETRANGE("Applies-to Doc. Line No.", xSalesLine."Line No.");
            IF AffectationFrais.FIND('-') THEN BEGIN
                AffectationFrais."Qty. to Assign" := xSalesLine."Qty. to Invoice";
                AffectationFrais."Amount to Assign" := AffectationFrais."Unit Cost" * AffectationFrais."Qty. to Assign";
                AffectationFrais.MODIFY();
            END;
        END;

        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", xSalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", xSalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, SalesLine.Type::Resource);
        LignesFraisAssociés.SETRANGE("Attached to Line No.", xSalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Invoice", xSalesLine."Qty. to Invoice");
            LignesFraisAssociés.MODIFY();
        END;
        // AD Le 30-11-2009 => DEEE
        //FIN DEEE AD Le 23-10-2006
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostInvoice', '', false, false)]
    local procedure CU80_OnBeforePostInvoice(var SalesHeader: Record "Sales Header"; var CustLedgerEntry: Record "Cust. Ledger Entry"; CommitIsSuppressed: Boolean; PreviewMode: Boolean; var GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line"; var IsHandled: Boolean; GenJnlLineDocNo: Code[20]; GenJnlLineExtDocNo: Code[35]; GenJnlLineDocType: Enum "Gen. Journal Document Type"; SrcCode: Code[10])
    begin
        // MCO Le 22-11-2017 => Refonte du codeunit 80 : Gestion du champ [Posting Description] :
        IF SalesHeader.Invoice THEN BEGIN
            IF SalesHeader."Document Type" IN [SalesHeader."Document Type"::Order, SalesHeader."Document Type"::Invoice] THEN BEGIN
                // CFR le 19/10/2022 => R‚gie : Correction du libell‚ comptable
                /*
                //FBRUN LE 03/11/2009 modification du lib‚ller comptable
                SalesHeader."Posting Description":='Facture'+' '+SalesHeader."Bill-to Name";
                */
                SalesHeader."Posting Description" := COPYSTR('Facture' + ' ' + SalesHeader."Bill-to Name", 1, 50);
                // FIN CFR le 19/10/2022
            END
            ELSE
                IF SalesHeader."Document Type" IN [SalesHeader."Document Type"::"Return Order", SalesHeader."Document Type"::"Credit Memo"] THEN BEGIN
                    // CFR le 19/10/2022 => R‚gie : Correction du libell‚ comptable
                    /*
                    //FBRUN LE 03/11/2009 modification du lib‚ller comptable
                    SalesHeader."Posting Description":='Avoir'+' '+SalesHeader."Bill-to Name";
                    */
                    SalesHeader."Posting Description" := COPYSTR('Avoir' + ' ' + SalesHeader."Bill-to Name", 1, 50);
                    // FIN CFR le 19/10/2022
                END;
        END;
        // FIN MCO Le 22-11-2017 => Refonte du codeunit 80 : Gestion du champ [Posting Description]
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostItemJnlLineOnBeforeCopyTrackingFromSpec', '', false, false)]
    // local procedure CU80_OnPostItemJnlLineOnBeforeCopyTrackingFromSpec(TrackingSpecification: Record "Tracking Specification"; var ItemJnlLine: Record "Item Journal Line"; SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; SalesInvHeader: Record "Sales Invoice Header"; SalesCrMemoHeader: Record "Sales Cr.Memo Header"; IsATO: Boolean)
    // begin
    //     // ANI le 10-04-2018 => Suivi du nø d'exp‚dition FE20171019
    //     ItemJnlLine."Warehouse Document No." := WhseShptHeader."No.";
    //     // FIN ANI le 10-04-2018
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostItemJnlLineBeforePost', '', false, false)]
    local procedure CU80_OnBeforePostItemJnlLineBeforePost(SalesLine: Record "Sales Line"; var ItemJnlLine: Record "Item Journal Line"; var TempWhseJnlLine: Record "Warehouse Journal Line" temporary; Location: Record Location; var PostWhseJnlLine: Boolean; QtyToBeShippedBase: Decimal; var IsHandled: Boolean; TrackingSpecification: Record "Tracking Specification")
    var
        SalesSetup: Record "Sales & Receivables Setup";
        TempTrackingSpecification: Record "Tracking Specification" temporary;
        SalesPostL: codeunit "Sales-Post";
        SalesLineReserve: Codeunit "Sales Line-Reserve";
        CheckApplFromItemEntry: Boolean;
        NewWhseShip: Boolean;
        NewWhseReceive: Boolean;
        NewInvtPickPutaway: Boolean;
    begin
        Ishandled := true;

        SalesPostL.GetGlobalWhseFlags(NewWhseShip, NewWhseReceive, NewInvtPickPutaway);
        if SalesSetup."Exact Cost Reversing Mandatory" and (SalesLine.Type = SalesLine.Type::Item) then
            if SalesLine.IsCreditDocType() then
                CheckApplFromItemEntry := SalesLine.Quantity > 0
            else
                CheckApplFromItemEntry := SalesLine.Quantity < 0;

        if (SalesLine."Location Code" <> '') and (SalesLine.Type = SalesLine.Type::Item) and (ItemJnlLine.Quantity <> 0) then
            if SalesPostL.ShouldPostWhseJnlLine(SalesLine) then begin
                SalesPostL.CreateWhseJnlLine(ItemJnlLine, SalesLine, TempWhseJnlLine);
                PostWhseJnlLine := true;
            end;

        // AD Le 03-07-2012 => Pour g‚n‚rer les lignes magasin pour les qte n‚gatives sur les commandes en validation par BP
        IF ((ItemJnlLine."Document Type" IN [SalesLine."Document Type"::Order]) AND NewWhseShip AND (SalesLine.Quantity < 0))
        THEN BEGIN
            SalesPostL.CreateWhseJnlLine(ItemJnlLine, SalesLine, TempWhseJnlLine);
            PostWhseJnlLine := TRUE;
        END;
        // FIN AD Le 03-07-2012

        if QtyToBeShippedBase <> 0 then begin
            if SalesLine.IsCreditDocType() then
                SalesLineReserve.TransferSalesLineToItemJnlLine(SalesLine, ItemJnlLine, QtyToBeShippedBase, CheckApplFromItemEntry, false)
            else
                SalesPostL.TransferReservToItemJnlLine(
                  SalesLine, ItemJnlLine, -QtyToBeShippedBase, TempTrackingSpecification, CheckApplFromItemEntry);

            if CheckApplFromItemEntry and SalesLine.IsInventoriableItem() then
                SalesLine.TestField("Appl.-from Item Entry");
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforeTestSalesLineItemCharge', '', false, false)]
    local procedure CU80_OnBeforeTestSalesLineItemCharge(SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        IsHandled := true;

        //DEEE  AD Le 30-10-2006 => DEEE -> Mis en commentaire pour valider des lignes à 0
        // if (Amount = 0) and (Quantity <> 0) then
        //     Error(ItemChargeZeroAmountErr, "No.");
        // FIN AD Le AD Le 30-10-2006
        SalesLine.TestField("Job No.", '', ErrorInfo.Create());
        SalesLine.TestField("Job Contract Entry No.", 0, ErrorInfo.Create());
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnFinalizePostingOnAfterUpdateItemChargeAssgnt', '', false, false)]
    local procedure CU80_OnFinalizePostingOnAfterUpdateItemChargeAssgnt(var SalesHeader: Record "Sales Header"; var TempDropShptPostBuffer: Record "Drop Shpt. Post. Buffer" temporary)
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // AD Le 24-05-2007 => Gestion de l'‚tat de livraison de la commande.
        LCodeunitsFunctions.UpdateOrderShipmentStatus(SalesHeader);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnRoundAmountOnBeforeIncrAmount', '', false, false)]
    local procedure CU80_OnRoundAmountOnBeforeIncrAmount(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; SalesLineQty: Decimal; var TotalSalesLine: Record "Sales Line"; var TotalSalesLineLCY: Record "Sales Line"; var xSalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        LTmpVendorBuffer: Record "Vendor Price Buffer" TEMPORARY;
        LTmpVendorBuffer2: Record "Vendor Price Buffer" TEMPORARY;
        SalesPostL: Codeunit "Sales-Post";
    begin
        IsHandled := true;

        SalesPostL.IncrAmount(SalesHeader, SalesLine, TotalSalesLineLCY);
        // AD Le 01-04-2015 => Si cout = 0 alors on prend le meilleur prix frn
        IF (SalesLine."Unit Cost (LCY)" = 0) AND (SalesLine.Type = SalesLine.Type::Item) THEN BEGIN
            LTmpVendorBuffer.DELETEALL();
            LTmpVendorBuffer2.SetRecordsByMulti(SalesLine."No.", 999999, '', LTmpVendorBuffer, 2, TRUE);
            LTmpVendorBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
            LTmpVendorBuffer.SETFILTER("Net Unit Price", '>%1', 0);
            IF LTmpVendorBuffer.FINDFIRST() THEN
                TotalSalesLineLCY."Unit Cost (LCY)" := TotalSalesLineLCY."Unit Cost (LCY)" + ROUND(SalesLineQty * LTmpVendorBuffer."Net Unit Price")
            ELSE
                TotalSalesLineLCY."Unit Cost (LCY)" := TotalSalesLineLCY."Unit Cost (LCY)" + ROUND(SalesLineQty * SalesLine."Unit Cost (LCY)");
        END ELSE
            // FIN AD Le 01-04-2015
            TotalSalesLineLCY."Unit Cost (LCY)" := TotalSalesLineLCY."Unit Cost (LCY)" + Round(SalesLineQty * SalesLine."Unit Cost (LCY)");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnUpdateBlanketOrderLineOnBeforeCheckSellToCustomerNo', '', false, false)]
    local procedure CU80_OnUpdateBlanketOrderLineOnBeforeCheckSellToCustomerNo(var BlanketOrderSalesLine: Record "Sales Line"; SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        // AD Le 08-10-2009 => FARGROUP -> Pouvoir changer le client lors de la transformation
        IsHandled := true;
        // FIN AD Le 08-10-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterSetApplyToDocNo', '', false, false)]
    local procedure CU80_OnAfterSetApplyToDocNo(var GenJournalLine: Record "Gen. Journal Line"; SalesHeader: Record "Sales Header")
    begin
        // MIG2015
        IF GenJournalLine."Bal. Account No." = '41300000' THEN
            GenJournalLine.Description := 'LCR GENERER'
        ELSE
            GenJournalLine.Description := 'PAIEMENT FACTURE ' + GenJournalLine."Document No.";
        // FIN MIG2015
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforeCheckPostRestrictions', '', false, false)]
    local procedure CU80_OnBeforeCheckPostRestrictions(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    begin
        //FBRUN ne gerer le statut bloqué du client
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnInsertPostedHeadersOnBeforeInsertInvoiceHeader', '', false, false)]
    local procedure CU80_OnInsertPostedHeadersOnBeforeInsertInvoiceHeader(SalesHeader: Record "Sales Header"; var IsHandled: Boolean; SalesInvHeader: Record "Sales Invoice Header"; var GenJnlLineDocType: Enum "Gen. Journal Document Type"; var GenJnlLineDocNo: Code[20]; var GenJnlLineExtDocNo: Code[35])
    var
        CustInvoiceDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
    begin
        //FBRUN le 22/09/2009 -> gestion des remises supl‚mentaire
        IF SalesHeader.Ship THEN BEGIN
            CLEAR(CustInvoiceDiscSup);
            CustInvoiceDiscSup.Calc_Disc_line(SalesHeader, 2);
            CustInvoiceDiscSup.Calc_Disc_Header(SalesHeader, 2);
        END;
        //FIN le 22/09/2009

        //FBRUN le 22/09/2009 -> gestion des remises supl‚mentaire
        IF SalesHeader.Invoice THEN BEGIN
            CLEAR(CustInvoiceDiscSup);
            CustInvoiceDiscSup.Calc_Disc_line(SalesHeader, 1);
            CustInvoiceDiscSup.Calc_Disc_Header(SalesHeader, 1);
        END;
        //FIN le 22/09/2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforeSalesShptHeaderInsert', '', false, false)]
    local procedure CU80_OnBeforeSalesShptHeaderInsert(var SalesShptHeader: Record "Sales Shipment Header"; SalesHeader: Record "Sales Header"; CommitIsSuppressed: Boolean; var IsHandled: Boolean; var TempWhseRcptHeader: Record "Warehouse Receipt Header" temporary; WhseReceive: Boolean; var TempWhseShptHeader: Record "Warehouse Shipment Header" temporary; WhseShip: Boolean; InvtPickPutaway: Boolean)
    begin
        // CFR le 05-06-2020 => WIIO -> ESKVN2.0 : PACKING V2 (R‚cup Navin‚goce BC)
        SalesShptHeader."Packing List No." := TempWhseShptHeader."Packing List No.";
        // FIN CFR le 05-06-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterSalesShptHeaderInsert', '', false, false)]
    local procedure CU80_OnAfterSalesShptHeaderInsert(var SalesShipmentHeader: Record "Sales Shipment Header"; SalesHeader: Record "Sales Header"; SuppressCommit: Boolean; WhseShip: Boolean; WhseReceive: Boolean; var TempWhseShptHeader: Record "Warehouse Shipment Header"; var TempWhseRcptHeader: Record "Warehouse Receipt Header"; PreviewMode: Boolean)
    var
        WhseEmployee: Record "Warehouse Employee";
        CustInvoiceDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
    begin
        // AD Le 07-04-2007 => Gestion des ‚tiquettes transport
        SalesShipmentHeader.Preparateur := TempWhseShptHeader."Assigned User ID";
        SalesShipmentHeader."Mode d'expédition" := TempWhseShptHeader."Mode d'expédition";
        SalesShipmentHeader."Shipping Agent Code" := TempWhseShptHeader."Shipping Agent Code";
        IF WhseEmployee.GET(USERID, SalesShipmentHeader."Location Code") THEN
            IF WhseEmployee."Validation Logistique Direct" THEN BEGIN
                IF WhseShip THEN BEGIN
                    SalesShipmentHeader.Weight := TempWhseShptHeader.Poids;
                    SalesShipmentHeader."Nb Of Box" := TempWhseShptHeader."Nb Of Box";
                    SalesShipmentHeader."Insurance of Delivery" := TempWhseShptHeader.Assurance;
                END;

                //ShipmentLabel();
            END;

        //FBRUN le 22/09/2009 -> gestion des remises supl‚mentaire
        IF SalesHeader.Ship THEN BEGIN
            CLEAR(CustInvoiceDiscSup);
            CustInvoiceDiscSup.Calc_Disc_line(SalesHeader, 2);
            CustInvoiceDiscSup.Calc_Disc_Header(SalesHeader, 2);
        END;
        //FIN le 22/09/2009
    END;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnInsertInvoiceHeaderOnBeforeSalesInvHeaderTransferFields', '', false, false)]
    local procedure CU80_OnInsertInvoiceHeaderOnBeforeSalesInvHeaderTransferFields(var SalesHeader: Record "Sales Header")
    begin
        // CFR le 19/10/2022 => R‚gie : Correction du libell‚ comptable
        /*
        //FBRUN LE 03/11/2009 modification du lib‚ller comptable
        SalesHeader."Posting Description":='Facture'+' '+SalesHeader."Bill-to Name";
        */
        SalesHeader."Posting Description" := COPYSTR('Facture' + ' ' + SalesHeader."Bill-to Name", 1, 50);
        // FIN CFR le 19/10/2022
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnInsertCrMemoHeaderOnBeforeSalesCrMemoHeaderTransferFields', '', false, false)]
    local procedure CU80_OnInsertCrMemoHeaderOnBeforeSalesCrMemoHeaderTransferFields(var SalesHeader: Record "Sales Header")
    begin
        // CFR le 19/10/2022 => R‚gie : Correction du libell‚ comptable
        /*
        //FBRUN LE 03/11/2009 modification du lib‚ller comptable
        SalesHeader."Posting Description":='Avoir'+' '+SalesHeader."Bill-to Name";
        */
        SalesHeader."Posting Description" := COPYSTR('Avoir' + ' ' + SalesHeader."Bill-to Name", 1, 50);
        // FIN CFR le 19/10/2022
    end;

    [EventSubscriber(ObjectType::Table, DataBase::"Sales Shipment Line", 'OnAfterInitFromSalesLine', '', false, false)]
    local procedure CU80_OnAfterInitFromSalesLine(SalesShptHeader: Record "Sales Shipment Header"; SalesLine: Record "Sales Line"; var SalesShptLine: Record "Sales Shipment Line")
    var
        LignesFraisAssociés: Record "Sales Line";
        AffectationFrais: Record "Item Charge Assignment (Sales)";
    begin
        //DEEE  AD Le 30-10-2006 => DEEE -> Gestion Frais associ‚s Pour le bl.
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", SalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", SalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, SalesLine.Type::"Charge (Item)");
        LignesFraisAssociés.SETRANGE("Attached to Line No.", SalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Ship", SalesLine."Qty. to Ship");
            LignesFraisAssociés.MODIFY();
            AffectationFrais.SETRANGE("Document Type", SalesLine."Document Type");
            AffectationFrais.SETRANGE("Document No.", SalesLine."Document No.");
            AffectationFrais.SETRANGE("Document Line No.", LignesFraisAssociés."Line No.");
            AffectationFrais.SETRANGE("Item Charge No.", LignesFraisAssociés."No.");
            AffectationFrais.SETRANGE("Applies-to Doc. Line No.", SalesLine."Line No.");
            IF AffectationFrais.FIND('-') THEN BEGIN
                AffectationFrais."Qty. to Assign" := SalesLine."Qty. to Ship";
                AffectationFrais."Amount to Assign" := AffectationFrais."Unit Cost" * AffectationFrais."Qty. to Assign";
                AffectationFrais.MODIFY();
            END
            ELSE
                ERROR('Erreur de frais ! Contactez eskape %1', AffectationFrais.GETFILTERS);
        END;

        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", SalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", SalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, SalesLine.Type::Resource);
        LignesFraisAssociés.SETRANGE("Attached to Line No.", SalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Ship", SalesLine."Qty. to Ship");
            LignesFraisAssociés.MODIFY();
        END;
        // FIN AD Le 30-11-2009

        // FIN DEEE AD Le 23-10-2006
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterSalesShptLineInsert', '', false, false)]
    local procedure CU80_OnAfterSalesShptLineInsert(var SalesShipmentLine: Record "Sales Shipment Line"; SalesLine: Record "Sales Line"; ItemShptLedEntryNo: Integer; WhseShip: Boolean; WhseReceive: Boolean; CommitIsSuppressed: Boolean; SalesInvoiceHeader: Record "Sales Invoice Header"; var TempWhseShptHeader: Record "Warehouse Shipment Header" temporary; var TempWhseRcptHeader: Record "Warehouse Receipt Header" temporary; SalesShptHeader: Record "Sales Shipment Header"; SalesHeader: Record "Sales Header")
    begin
        //IF WhseShip THEN // MCO Le 22-09-2017 => On est toujours en livraison dans cette portion de code.
        //BEGIN
        // FBRUN le 02/05/2011 =>gestion des centrales
        SalesShptHeader.RécupérerFacturationCentral();
        //END;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforeSendPostedDocumentRecord', '', false, false)]
    local procedure CU80_OnBeforeSendPostedDocumentRecord(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean; var DocumentSendingProfile: Record "Document Sending Profile")
    var
        SalesInvHeader: Record "Sales Invoice Header";
        SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        OfficeManagement: Codeunit "Office Management";
        NotSupportedDocumentTypeErr: Label 'Document type %1 is not supported.', Comment = '%1 = Document Type';
    begin
        IsHandled := true;
        case SalesHeader."Document Type" of
            SalesHeader."Document Type"::Order:
                begin
                    if SalesHeader.Invoice then begin
                        SalesInvHeader.Get(SalesHeader."Last Posting No.");
                        SalesInvHeader.SetRecFilter();
                        SalesInvHeader.SendProfile(DocumentSendingProfile);
                    end;
                    if SalesHeader.Ship and SalesHeader.Invoice and not OfficeManagement.IsAvailable() then
                        // if not ConfirmDownloadShipment(SalesHeader) then // MCO Le 03-10-2018 => R‚gie
                        exit;
                    if SalesHeader.Ship then begin
                        SalesShipmentHeader.Get(SalesHeader."Last Shipping No.");
                        SalesShipmentHeader.SetRecFilter();
                        SalesShipmentHeader.SendProfile(DocumentSendingProfile);
                    end;
                end;
            SalesHeader."Document Type"::Invoice:
                begin
                    if SalesHeader."Last Posting No." = '' then
                        SalesInvHeader.Get(SalesHeader."No.")
                    else
                        SalesInvHeader.Get(SalesHeader."Last Posting No.");

                    SalesInvHeader.SetRecFilter();
                    SalesInvHeader.SendProfile(DocumentSendingProfile);
                end;
            SalesHeader."Document Type"::"Credit Memo":
                begin
                    if SalesHeader."Last Posting No." = '' then
                        SalesCrMemoHeader.Get(SalesHeader."No.")
                    else
                        SalesCrMemoHeader.Get(SalesHeader."Last Posting No.");
                    SalesCrMemoHeader.SetRecFilter();
                    SalesCrMemoHeader.SendProfile(DocumentSendingProfile);
                end;
            SalesHeader."Document Type"::"Return Order":
                if SalesHeader.Invoice then begin
                    if SalesHeader."Last Posting No." = '' then
                        SalesCrMemoHeader.Get(SalesHeader."No.")
                    else
                        SalesCrMemoHeader.Get(SalesHeader."Last Posting No.");
                    SalesCrMemoHeader.SetRecFilter();
                    SalesCrMemoHeader.SendProfile(DocumentSendingProfile);
                end;
            else begin
                Error(NotSupportedDocumentTypeErr, SalesHeader."Document Type");
            end;
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostItemTrackingForShipmentOnAfterSetFilters', '', false, false)]
    local procedure CU80_OnPostItemTrackingForShipmentOnAfterSetFilters(var SalesShipmentLine: Record "Sales Shipment Line"; SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line")
    var
        TmpSalesShptHeader: Record "Sales Shipment Header";
    begin
        if SalesShipmentLine.FindFirst() then begin
            //FBRUN LE 07/11/2011->Verifi‚ que le BL ne soit pas en attente de paiement
            IF TmpSalesShptHeader.GET(SalesShipmentLine."Document No.") THEN;
            TmpSalesShptHeader.TESTFIELD("En attente de facturation", FALSE);
            //FIN FBRUN
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostItemTrackingForShipment', '', false, false)]
    local procedure CU80_OnBeforePostItemTrackingForShipment(var SalesInvoiceHeader: Record "Sales Invoice Header"; var SalesShipmentLine: Record "Sales Shipment Line"; var TempTrackingSpecification: Record "Tracking Specification" temporary; var TrackingSpecificationExists: Boolean; SalesLine: Record "Sales Line"; QtyToBeInvoiced: Decimal; QtyToBeInvoicedBase: Decimal)
    var
        TmpSalesShptHeader: Record "Sales Shipment Header";
    begin
        //FBRUN le 10/05/2011 => regarder si entierement facturer
        IF TmpSalesShptHeader.GET(SalesShipmentLine."Document No.") THEN
            TmpSalesShptHeader.MajEntierementFacturé();
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostItemTrackingForShipment', '', false, false)]
    local procedure CU80_OnAfterPostItemTrackingForShipment(var SalesInvoiceHeader: Record "Sales Invoice Header"; var SalesShipmentLine: Record "Sales Shipment Line"; var TempTrackingSpecification: Record "Tracking Specification" temporary; var TrackingSpecificationExists: Boolean; SalesLine: Record "Sales Line"; QtyToBeInvoiced: Decimal; QtyToBeInvoicedBase: Decimal)
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        //MC Le 21-09-2010 => Correction du bug en facturation de prŠt.
        //MC Le 21-09-2010 => cr‚ation d'‚criture article lors de la facture sortie en stock sur magasin de prˆts
        IF SalesLine.Loans THEN
            LCodeunitsFunctions.PostItemJnlLineLoans2(SalesLine, -QtyToBeInvoiced, -QtyToBeInvoicedBase, 0, 0, 0, '',
            TempTrackingSpecification);
        //FIN MC Le 21-09-2010
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostUpdateOrderLineOnBeforeSetInvoiceFields', '', false, false)]
    local procedure CU80_OnPostUpdateOrderLineOnBeforeSetInvoiceFields(var SalesHeader: Record "Sales Header"; var TempSalesLine: Record "Sales Line"; var ShouldSetInvoiceFields: Boolean)
    var
        LignesFraisAssociés: Record "Sales Line";
        AffectationFrais: Record "Item Charge Assignment (Sales)";
    begin
        //DEEE AD Le 30-10-2006 => DEEE -> Gestion Frais associ‚s Pour les quantit‚ a livrer sur la commande.
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", TempSalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", TempSalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, TempSalesLine.Type::"Charge (Item)");
        LignesFraisAssociés.SETRANGE("Attached to Line No.", TempSalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Ship", TempSalesLine."Qty. to Ship");
            LignesFraisAssociés.VALIDATE("Qty. to Ship (Base)", TempSalesLine."Qty. to Ship (Base)");
            LignesFraisAssociés.MODIFY();
            AffectationFrais.SETRANGE("Document Type", TempSalesLine."Document Type");
            AffectationFrais.SETRANGE("Document No.", TempSalesLine."Document No.");
            AffectationFrais.SETRANGE("Document Line No.", LignesFraisAssociés."Line No.");
            AffectationFrais.SETRANGE("Item Charge No.", LignesFraisAssociés."No.");
            AffectationFrais.SETRANGE("Applies-to Doc. Line No.", TempSalesLine."Line No.");
            IF AffectationFrais.FINDSET() THEN BEGIN
                AffectationFrais."Qty. to Assign" := TempSalesLine."Qty. to Ship";
                AffectationFrais."Amount to Assign" := AffectationFrais."Unit Cost" * AffectationFrais."Qty. to Assign";
                AffectationFrais.MODIFY();
            END
            ELSE
                ERROR('Erreur de frais ! Contactez eskape');

        END;
        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        LignesFraisAssociés.RESET();
        LignesFraisAssociés.SETRANGE("Document Type", TempSalesLine."Document Type");
        LignesFraisAssociés.SETRANGE("Document No.", TempSalesLine."Document No.");
        LignesFraisAssociés.SETRANGE(Type, TempSalesLine.Type::Resource);
        LignesFraisAssociés.SETRANGE("Attached to Line No.", TempSalesLine."Line No.");
        IF LignesFraisAssociés.FIND('-') THEN BEGIN
            LignesFraisAssociés.VALIDATE("Qty. to Ship", TempSalesLine."Qty. to Ship");
            LignesFraisAssociés.VALIDATE("Qty. to Ship (Base)", TempSalesLine."Qty. to Ship (Base)");
            LignesFraisAssociés.MODIFY();
        END;
        // FIN AD Le 30-11-2009

        //FIN DEEE AD Le 23-10-2006
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostItemLineOnAfterMakeSalesLineToShip', '', false, false)]
    local procedure CU80_OnPostItemLineOnAfterMakeSalesLineToShip(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var TempDropShptPostBuffer: Record "Drop Shpt. Post. Buffer" temporary; var TempPostedATOLink: Record "Posted Assemble-to-Order Link" temporary; var RemQtyToBeInvoiced: Decimal; var RemQtyToBeInvoicedBase: Decimal)
    begin
        CduGSingleInstance.FctSetQtyToInvoiceBase(RemQtyToBeInvoicedBase);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostItemLineOnBeforePostItemJnlLineForInvoiceDoc', '', false, false)]
    local procedure CU80_OnPostItemLineOnBeforePostItemJnlLineForInvoiceDoc(SalesHeader: Record "Sales Header"; var SalesLineToShip: Record "Sales Line"; Ship: Boolean; var ItemLedgShptEntryNo: Integer; var GenJnlLineDocNo: Code[20]; var GenJnlLineExtDocNo: Code[35]; SalesShptHeader: Record "Sales Shipment Header"; var TempHandlingSpecification: Record "Tracking Specification" temporary; var TempTrackingSpecificationInv: Record "Tracking Specification" temporary; var TempTrackingSpecification: Record "Tracking Specification" temporary; var IsHandled: Boolean; QtyToInvoice: Decimal; TempAssembletoOrderLink: Record "Posted Assemble-to-Order Link" temporary)
    var
        DummyTrackingSpecification: Record "Tracking Specification";
        LSalesPost: Codeunit "Sales-Post";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        IsHandled := true;
        if Abs(SalesLineToShip."Qty. to Ship") > Abs(QtyToInvoice) + Abs(TempAssembletoOrderLink."Assembled Quantity") then //MC => Le 21/09/10 Correction du bug
                                                                                                                            //Sur la facturation des prŠts rajout du BEGIN
            ItemLedgShptEntryNo :=
                LSalesPost.PostItemJnlLine(
                    SalesHeader, SalesLineToShip,
                    SalesLineToShip."Qty. to Ship" - TempAssembletoOrderLink."Assembled Quantity" - QtyToInvoice,
                    SalesLineToShip."Qty. to Ship (Base)" - TempAssembletoOrderLink."Assembled Quantity (Base)" - CduGSingleInstance.FctGetQtyToInvoiceBase(),
                    0, 0, 0, '', DummyTrackingSpecification, false);

        CduGSingleInstance.FctSetQtyToInvoiceBase(0);
        //FBRUN -> PRET cr‚ation d'‚criture article lors du Bl entr‚ en stock sur magasin de prˆts
        IF SalesLineToShip.Loans THEN
            LCodeunitsFunctions.PostItemJnlLineLoans2(SalesLineToShip, SalesLineToShip."Qty. to Ship", SalesLineToShip."Qty. to Ship (Base)", 0, 0, 0, '',
             DummyTrackingSpecification);
        //fin PRET
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnInsertShipmentLineOnAfterInitQuantityFields', '', false, false)]
    local procedure CU80_OnInsertShipmentLineOnAfterInitQuantityFields(var SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; var SalesShptLine: Record "Sales Shipment Line")
    begin
        // AD Le 03-11-2011 => Possibilit‚ de mettre des lignes n‚gatives sur la cde sans BP
        // IF IF (SalesLine.Type = SalesLine.Type::Item) AND (SalesLine."Qty. to Ship" <> 0) THEN BEGIN //=> CODE D'ORIGINE
        CduGSingleInstance.FctSetShould((SalesLine.Type = SalesLine.Type::Item) AND (xSalesLine."Qty. to Ship" > 0));
        // FIN AD Le 03-11-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnInsertShipmentLineOnAfterCalcShouldProcessShipmentRelation', '', false, false)]
    local procedure CU80_OnInsertShipmentLineOnAfterCalcShouldProcessShipmentRelation(var SalesLine: Record "Sales Line"; var ShouldProcessShipmentRelation: Boolean)
    begin
        ShouldProcessShipmentRelation := CduGSingleInstance.FctGetShould();
        CduGSingleInstance.FctSetShould(false);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnPostInvoiceOnAfterPostLines', '', false, false)]
    local procedure CU80_OnPostInvoiceOnAfterPostLines(var SalesHeader: Record "Sales Header"; SrcCode: Code[10]; GenJnlLineDocType: Enum "Gen. Journal Document Type"; GenJnlLineDocNo: Code[20]; GenJnlLineExtDocNo: Code[35]; var GenJnlPostLine: Codeunit "Gen. Jnl.-Post Line")
    begin
        //Multi ech‚ance
        IF (SalesHeader."Multi echéance" OR SalesHeader."Echéances fractionnées") THEN
            SalesHeader."Bal. Account No." := '41300000';
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnFinalizePostingOnBeforeDeleteApprovalEntries', '', false, false)]
    local procedure CU80_OnFinalizePostingOnBeforeDeleteApprovalEntries(var SalesHeader: Record "Sales Header"; var EverythingInvoiced: Boolean)
    var
        ArchiveManagement: Codeunit ArchiveManagement;
    begin
        // AD Le 16-01-2009 => Archivage avant suppression.
        ArchiveManagement.StoreSalesDocument(SalesHeader, FALSE);
        // FIN AD Le 16-01-2009
        // MCO Le 14-02-2018 => La gestion d'archivage est faite par le code du dessus et ne tient pas compte du param‚trage standard vente
        //ArchiveManagement.AutoArchiveSalesDocument(SalesHeader);
        // FIN MCO Le 14-02-2018
    end;
    //<< Mig CU 80
    //>> Mig CU 90
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnBeforePostPurchaseDoc', '', false, false)]
    local procedure CU90_OnBeforePostPurchaseDoc(var PurchaseHeader: Record "Purchase Header"; PreviewMode: Boolean; CommitIsSupressed: Boolean; var HideProgressWindow: Boolean; var ItemJnlPostLine: Codeunit "Item Jnl.-Post Line"; var IsHandled: Boolean)
    var
        Text000: Label 'Erreur entre le TTC attendu %1 et le TTC des lignes %2';
    begin
        // ESKAPE
        IF PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Invoice THEN BEGIN
            PurchaseHeader.TESTFIELD(Periode);
            IF PurchaseHeader.Periode <> DATE2DMY(PurchaseHeader."Posting Date", 2) THEN
                ERROR('Date comtabilisation hors periode (%1)', PurchaseHeader.Periode);

            /*
             IF (DATE2DMY(TODAY,2)<> DATE2DMY(PurchaseHeader."Posting Date",2)) OR (DATE2DMY(TODAY,3)<> DATE2DMY(PurchaseHeader."Posting Date",3)) THEN
              BEGIN
               IF NOT CONFIRM(ESK50001) THEN
                ERROR('');
              END;
            */

            // On contrôle montant ttc
            PurchaseHeader.CALCFIELDS("Amount Including VAT");
            IF PurchaseHeader."Amount Including VAT" <> PurchaseHeader."Montant HT facture" THEN // Le champ est en fait MontantTTC ...
                ERROR(STRSUBSTNO(Text000, PurchaseHeader."Montant HT facture", PurchaseHeader."Amount Including VAT"));
        END;
        // fin ESKAPE
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostCombineSalesOrderShipment', '', false, false)]
    local procedure CU90_OnAfterPostCombineSalesOrderShipment(var PurchaseHeader: Record "Purchase Header"; var TempDropShptPostBuffer: Record "Drop Shpt. Post. Buffer" temporary)
    var
        ArchiveManagement: Codeunit ArchiveManagement;
    begin
        // AD Le 16-01-2009 => Archivage avant suppression.
        ArchiveManagement.StorePurchDocument(PurchaseHeader, FALSE);
        // FIN AD Le 16-01-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnPostItemJnlLineOnAfterCopyDocumentFields', '', false, false)]
    local procedure CU90_OnPostItemJnlLineOnAfterCopyDocumentFields(var ItemJournalLine: Record "Item Journal Line"; WarehouseReceiptHeader: Record "Warehouse Receipt Header")
    begin
        // MCO Le 16-01-2018 => Suivi du Nø de r‚ception FE20171019
        ItemJournalLine."Warehouse Document No." := WarehouseReceiptHeader."No.";
        // FIN MCO Le 16-01-2018
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post (Yes/No)", 'OnBeforeSelectPostReturnOrderOption', '', false, false)]
    local procedure CU91_OnBeforeSelectPostReturnOrderOption(DefaultOption: Integer; var IsHandled: Boolean; var PurchaseHeader: Record "Purchase Header"; var Result: Boolean)

    var
        UserSetupManagement: Codeunit "User Setup Management";
        ConfirmManagement: Codeunit "Confirm Management";
        Selection: Integer;
        ESK000Qst: Label 'Ship';
        ShipConfirmQst: Label 'Do you want to post the shipment?';
    begin
        if not (PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::"Return Order") then exit;
        UserSetupManagement.GetPurchaseInvoicePostingPolicy(PurchaseHeader.Ship, PurchaseHeader.Invoice);
        case true of
            not PurchaseHeader.Ship and not PurchaseHeader.Invoice:
                begin
                    Selection := StrMenu(ESK000Qst, 1);
                    if Selection = 0 then
                        Result := false;
                    PurchaseHeader.Ship := Selection in [1, 3];
                    PurchaseHeader.Invoice := Selection in [2, 3];
                end;
            PurchaseHeader.Ship and not PurchaseHeader.Invoice:
                if not ConfirmManagement.GetResponseOrDefault(ShipConfirmQst, true) then
                    Result := false;
        end;
        Result := true;
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnInsertReceiptLineOnBeforeCreatePostedRcptLine', '', false, false)]
    local procedure CU90_OnInsertReceiptLineOnBeforeCreatePostedRcptLine(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; var PurchRcptLine: Record "Purch. Rcpt. Line"; PostedWhseReceiptHeader: Record "Posted Whse. Receipt Header")
    begin
        // AD Le 20-01-2014
        PurchRcptLine."Posted Whse. Receipt No." := PostedWhseReceiptHeader."No.";
        PurchRcptLine."Whse. Receipt No." := WarehouseReceiptLine."No.";
        // FIN AD Le 20-01-2014
        // AD Le 24-03-2015 => No de BL fournisseur … la ligne de r‚ception
        IF WarehouseReceiptLine."Vendor Shipment No." <> '' THEN
            PurchRcptLine."Vendor Shipment No." := WarehouseReceiptLine."Vendor Shipment No.";
        // FIN AD Le 24-03-2015 
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnInsertReceiptLineOnAfterInitPurchRcptLine', '', false, false)]
    local procedure CU90_OnInsertReceiptLineOnAfterInitPurchRcptLine(var PurchRcptLine: Record "Purch. Rcpt. Line"; PurchLine: Record "Purchase Line"; ItemLedgShptEntryNo: Integer; xPurchLine: Record "Purchase Line"; var PurchRcptHeader: Record "Purch. Rcpt. Header"; var CostBaseAmount: Decimal; PostedWhseRcptHeader: Record "Posted Whse. Receipt Header"; WhseRcptHeader: Record "Warehouse Receipt Header"; var WhseRcptLine: Record "Warehouse Receipt Line")
    begin
        CduGSingleInstance.FctSaveWhseRcptHeaderNo(WhseRcptHeader."No.");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPurchRcptLineInsert', '', false, false)]
    local procedure CU90_OnAfterPurchRcptLineInsert(PurchaseLine: Record "Purchase Line"; var PurchRcptLine: Record "Purch. Rcpt. Line"; ItemLedgShptEntryNo: Integer; WhseShip: Boolean; WhseReceive: Boolean; CommitIsSupressed: Boolean; PurchInvHeader: Record "Purch. Inv. Header"; var TempTrackingSpecification: Record "Tracking Specification" temporary; PurchRcptHeader: Record "Purch. Rcpt. Header"; TempWhseRcptHeader: Record "Warehouse Receipt Header"; xPurchLine: Record "Purchase Line"; var TempPurchLineGlobal: Record "Purchase Line" temporary)
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        // AD Le 20-03-2013 => Gestion des litiges
        IF WhseReceive THEN BEGIN
            WhseRcptLine.SETCURRENTKEY(
              "No.", "Source Type", "Source Subtype", "Source No.", "Source Line No.");
            WhseRcptLine.SETRANGE("No.", CduGSingleInstance.FctGetWhseRcptHeaderNo());
            WhseRcptLine.SETRANGE("Source Type", DATABASE::"Purchase Line");
            WhseRcptLine.SETRANGE("Source Subtype", PurchaseLine."Document Type");
            WhseRcptLine.SETRANGE("Source No.", PurchaseLine."Document No.");
            WhseRcptLine.SETRANGE("Source Line No.", PurchaseLine."Line No.");
            IF WhseRcptLine.FINDFIRST() THEN BEGIN
                //LGestionLitige.Cr‚erLitige(WhseRcptLine, PurchRcptLine);
                PurchRcptLine."Qte Sur Facture Frn" := WhseRcptLine."Qte Reçue théorique";
                PurchRcptLine.modify();
            END;
        END;
        CduGSingleInstance.FctSaveWhseRcptHeaderNo('');
        // FIN AD Le 20-03-2013
    end;

    //<< Mig CU 90
    //>> Mig CU 7000
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnFindSalesLinePriceOnItemTypeOnAfterSetUnitPrice', '', false, false)]
    local procedure CU7000_OnFindSalesLinePriceOnItemTypeOnAfterSetUnitPrice(var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var TempSalesPrice: Record "Sales Price" temporary; CalledByFieldNo: Integer; FoundSalesPrice: Boolean)
    begin
        // AD Le 16-11-2009 => Pour faire suivre une campagne … la ligne (Tarif + RFA))
        IF TempSalesPrice."Sales Type" = TempSalesPrice."Sales Type"::Campaign THEN
            SalesLine."Campaign No." := TempSalesPrice."Sales Code";
        // FIN AD Le 16-11-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnAfterFindSalesLinePrice', '', false, false)]
    local procedure CU7000_OnAfterFindSalesLinePrice(var SalesLine: Record "Sales Line"; var SalesHeader: Record "Sales Header"; var SalesPrice: Record "Sales Price"; var ResourcePrice: Record "Resource Price"; CalledByFieldNo: Integer; FoundSalesPrice: Boolean)
    begin
        if SalesLine.Type = SalesLine.Type::Item then begin
            // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
            IF NOT SalesLine."Allow Line Disc." THEN BEGIN
                SalesLine."Discount1 %" := 0;
                SalesLine."Discount2 %" := 0;
            END;
            // FIN AD Le 08-12-2008

            // AD Le 15-09-2011 =>
            IF SalesLine."Line type" = SalesLine."Line type"::Gratuit THEN BEGIN
                SalesLine."Unit Price" := 0;
                SalesLine."Discount1 %" := 0;
                SalesLine."Discount2 %" := 0;
            END;

            // AD Le 15-11-2011 => Gestion des retours
            IF (SalesHeader."Document Type" = SalesHeader."Document Type"::"Return Order") AND (SalesHeader."Taux Abattement" <> 0) THEN BEGIN
                SalesLine."Unit Price" := ROUND(SalesLine."Unit Price" * (1 - SalesHeader."Taux Abattement" / 100), 0.01);
            END;
        end
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnAfterFindSalesLineLineDisc', '', false, false)]
    local procedure CU90_OnAfterFindSalesLineLineDisc(var SalesLine: Record "Sales Line"; var SalesHeader: Record "Sales Header"; var SalesLineDiscount: Record "Sales Line Discount")
    begin
        IF SalesLine.Type = SalesLine.Type::Item THEN BEGIN
            // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
            SalesLine."Discount1 %" := SalesLineDiscount."Line Discount 1 %";
            SalesLine."Discount2 %" := SalesLineDiscount."Line Discount 2 %";
            // FIN AD Le 08-12-2008
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnBeforeCalcBestUnitPrice', '', false, false)]
    local procedure CU7000_OnBeforeCalcBestUnitPrice(var SalesPrice: Record "Sales Price"; var IsHandled: Boolean)
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // AD Le 08-10-2009 => FARGROUP -> Si on a des prix net, on ne prend pas en compte les prix brut dans le bestPrice
        LCodeunitsFunctions.PurgeBestUnitPrice(SalesPrice);
        // FIN AD Le 08-10-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnBeforeCalcBestLineDisc', '', false, false)]
    local procedure CU7000_OnBeforeCalcBestLineDisc(var SalesLineDisc: Record "Sales Line Discount"; Item: Record Item; var IsHandled: Boolean; QtyPerUOM: Decimal; Qty: Decimal)
    var
        BestSalesLineDisc: Record "Sales Line Discount";
        NegatifSalesLineDisc: Record "Sales Line Discount";
    begin
        IsHandled := true;
        if SalesLineDisc.FindSet() then
            repeat
            /*if IsInMinQty("Unit of Measure Code", "Minimum Quantity") then
                case true of
                    ((BestSalesLineDisc."Currency Code" = '') and ("Currency Code" <> '')) or
                  ((BestSalesLineDisc."Variant Code" = '') and ("Variant Code" <> '')):
                        BestSalesLineDisc := SalesLineDisc;
                    ((BestSalesLineDisc."Currency Code" = '') or ("Currency Code" <> '')) and
                  ((BestSalesLineDisc."Variant Code" = '') or ("Variant Code" <> '')):
                        begin
                            if BestSalesLineDisc."Line Discount %" < "Line Discount %" then
                                BestSalesLineDisc := SalesLineDisc;
                            //SI UNE REMISE EST NEGATIF ELLE DOIT ETRE PRISE
                            IF "Line Discount %" < 0 THEN
                                NegatifSalesLineDisc := SalesLineDisc;
                        end;
                end;*/
            until SalesLineDisc.Next() = 0;

        IF NegatifSalesLineDisc."Line Discount %" < 0 THEN
            SalesLineDisc := NegatifSalesLineDisc
        ELSE
            SalesLineDisc := BestSalesLineDisc;
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnBeforeFindSalesPrice', '', false, false)]
    // local procedure CU7000_OnBeforeFindSalesPrice(var ToSalesPrice: Record "Sales Price"; var FromSalesPrice: Record "Sales Price"; var QtyPerUOM: Decimal; var Qty: Decimal; var CustNo: Code[20]; var ContNo: Code[20]; var CustPriceGrCode: Code[10]; var CampaignNo: Code[20]; var ItemNo: Code[20]; var VariantCode: Code[10]; var UOM: Code[10]; var CurrencyCode: Code[10]; var StartingDate: Date; var ShowAll: Boolean)
    // begin
    //     // AD Le 04-11-2011 => Tarif pas type de commande
    //     FromSalesPrice.SETFILTER("Type de commande", '%1|%2', _pTypeCommande, FromSalesPrice."Type de commande"::Tous);
    //     // FIN AD Le 04-11-2011
    // end;
    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnFindSalesLineDiscOnAfterSetFilters', '', false, false)]
    // local procedure CU7000_OnFindSalesLineDiscOnAfterSetFilters(var SalesLineDiscount: Record "Sales Line Discount")
    // begin
    //     // On pose le filtre sur le type de commande.
    //     SalesLineDiscount.SETRANGE("Type de commande", TypeCommande);
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnBeforeSalesLinePriceExists', '', false, false)]
    local procedure CU7000_OnBeforeSalesLinePriceExists(var SalesLine: Record "Sales Line"; var SalesHeader: Record "Sales Header"; var TempSalesPrice: Record "Sales Price" temporary; Currency: Record Currency; CurrencyFactor: Decimal; StartingDate: Date; Qty: Decimal; QtyPerUOM: Decimal; ShowAll: Boolean; var InHandled: Boolean)
    var
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        DateCaption: Text[30];
    begin
        InHandled := true;
        SalesPriceCalcMgt.FindSalesPrice(
              TempSalesPrice, LCodeunitsFunctions.GetCustNoForSalesHeader(SalesHeader), SalesHeader."Bill-to Contact No.",
              // AD Le 21-10-2009 => Possibilit‚ de ne pas activer une campagne
              //"Customer Price Group", '', "No.", "Variant Code", "Unit of Measure Code",
              SalesLine."Customer Price Group", SalesHeader."Campaign No.", SalesLine."No.", SalesLine."Variant Code", SalesLine."Unit of Measure Code",
              // FIN AD Le 21-10-2009
              SalesHeader."Currency Code", SalesPriceCalcMgt.SalesHeaderStartDate(SalesHeader, DateCaption), ShowAll);
        // MC Le 19-04-2011 => Rajout des deux paramŠtres. Prendre en compte la centrale ou le sous groupe tarif.
        //SalesHeader."Sous Groupe Tarifs",SalesHeader."Centrale Active",  SalesHeader."Type de commande" + 1); //AD Ajout du type cde
        // FIN MC Le 19-04-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Price Calc. Mgt.", 'OnBeforeSalesLineLineDiscExists', '', false, false)]
    local procedure CU7000_OnBeforeSalesLineLineDiscExists(var SalesLine: Record "Sales Line"; var SalesHeader: Record "Sales Header"; var TempSalesLineDisc: Record "Sales Line Discount" temporary; StartingDate: Date; Qty: Decimal; QtyPerUOM: Decimal; ShowAll: Boolean; var IsHandled: Boolean)
    var
        Item: Record Item;
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        DateCaption: Text[30];
    begin
        IsHandled := true;
        Item.Get(SalesLine."No.");
        SalesPriceCalcMgt.FindSalesLineDisc(
            TempSalesLineDisc, LCodeunitsFunctions.GetCustNoForSalesHeader(SalesHeader), SalesHeader."Bill-to Contact No.",
            SalesLine."Customer Disc. Group", '', SalesLine."No.", Item."Item Disc. Group", SalesLine."Variant Code", SalesLine."Unit of Measure Code",
            SalesHeader."Currency Code", SalesPriceCalcMgt.SalesHeaderStartDate(SalesHeader, DateCaption), ShowAll);
        // MC Le 20-04-2011 => Rajout des deux paramŠtres. Prendre en compte la centrale ou le sous groupe tarif.
        //SalesHeader."Sous Groupe Tarifs",SalesHeader."Centrale Active", SalesHeader."Type de commande");
        // FIN MC LE 20-04-2011
    end;
    //<< Mig CU 7000
    //>> Mig CU 7010
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnAfterFindPurchLineLineDisc', '', false, false)]
    local procedure CU7010_OnAfterFindPurchLineLineDisc(var PurchaseLine: Record "Purchase Line"; var PurchaseHeader: Record "Purchase Header"; var TempPurchLineDisc: Record "Purchase Line Discount" temporary)
    begin
        IF PurchaseLine.Type = PurchaseLine.Type::Item THEN BEGIN
            // MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE
            PurchaseLine."Discount1 %" := TempPurchLineDisc."Line Discount 1 %";
            PurchaseLine."Discount2 %" := TempPurchLineDisc."Line Discount 2 %";
            // MC Le 09-05-2011
        end;
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnCalcBestDirectUnitCostOnBeforeNoPriceFound', '', false, false)]
    // local procedure CU7010_OnCalcBestDirectUnitCostOnBeforeNoPriceFound(var PurchasePrice: Record "Purchase Price"; Item: Record Item; var IsHandled: Boolean)
    // begin
    //     IsHandled := true;
    //     PriceInSKU := PriceInSKU and (SKU."Last Direct Cost" <> 0);
    //     if PriceInSKU then
    //         PurchasePrice."Direct Unit Cost" := SKU."Last Direct Cost"
    //     else
    //         PurchasePrice."Direct Unit Cost" := Item."Last Direct Cost";

    //     // AD Le 09-12-2009 => Si pas de tarif on retourne 0 et non pas le dernier prix
    //     PurchasePrice."Direct Unit Cost" := 0;
    //     // FIN AD Le 09-12-2009
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnBeforeCalcBestLineDisc', '', false, false)]
    local procedure CU7010_OnBeforeCalcBestLineDisc(var PurchLineDisc: Record "Purchase Line Discount"; Item: Record Item; var IsHandled: Boolean);
    var
        BestPurchLineDisc: Record "Purchase Line Discount";
    begin
        IsHandled := true;
        if PurchLineDisc.Find('-') then
            repeat
            /*TODO if IsInMinQty("Unit of Measure Code", "Minimum Quantity") then
                case true of
                    ((BestPurchLineDisc."Currency Code" = '') and ("Currency Code" <> '')) or
                  ((BestPurchLineDisc."Variant Code" = '') and ("Variant Code" <> '')):
                        BestPurchLineDisc := PurchLineDisc;
                    ((BestPurchLineDisc."Currency Code" = '') or ("Currency Code" <> '')) and
                  ((BestPurchLineDisc."Variant Code" = '') or ("Variant Code" <> '')):
                        //GB Le 20-12-2016 => FE20161128 Gestion %remise n‚gative
                        // if BestPurchLineDisc."Line Discount %" < "Line Discount %" then
                        IF ABS(BestPurchLineDisc."Line Discount %") < ABS("Line Discount %") THEN
                            //FIN GB Le 20-12-2016
                            BestPurchLineDisc := PurchLineDisc;
                end;*/
            until PurchLineDisc.Next() = 0;

        PurchLineDisc := BestPurchLineDisc;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnFindPurchLineDiscOnAfterSetFilters', '', false, false)]
    local procedure CU7010_OnFindPurchLineDiscOnAfterSetFilters(var FromPurchLineDisc: Record "Purchase Line Discount");
    begin
        // --------------------------------------------
        // MC Le 11-05-2011 => SYMTA -> REMISES ACHATS
        // --------------------------------------------

        // On pose le filtre sur le type de commande.
        //TODO FromPurchLineDisc.SETRANGE("Type de commande", TypeCommande);
        // FIN MC
    end;

    // [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnAfterFindPurchLineDisc', '', false, false)]
    // local procedure CU7010_OnAfterFindPurchLineDisc(var ToPurchaseLineDiscount: Record "Purchase Line Discount"; var FromPurchaseLineDiscount: Record "Purchase Line Discount"; ItemNo: Code[20]; QuantityPerUoM: Decimal; Quantity: Decimal; ShowAll: Boolean)
    // var
    //       GroupeRemiseAchat : Record "Groupe Remises Fournisseurs";
    //   LItemVendor : Record "Item Vendor";
    //   LVendor : Record Vendor;
    // begin
    //     // MC Le 12-05-2011 => SYMTA -> Prise en compte de la table des groupes remises.
    //     WITH GroupeRemiseAchat DO BEGIN
    //         // R‚cup‚ration du groupe remise de la fiche Article/Fournisseur
    //         IF LVendor.GET(FromPurchaseLineDiscount."Vendor No.") THEN
    //             IF LItemVendor.GET(VendorNo, ItemNo, VariantCode) THEN BEGIN
    //                 SETRANGE("Vendor Disc. Group", LVendor."Vendor Disc. Group");

    //                 // AD Le 01-09-2011 =>
    //                 // SETRANGE("Discount Code", LItemVendor."Code Remise"); CODE D'ORIGINE
    //                 IF GroupeRemise = '' THEN
    //                     GroupeRemise := LItemVendor."Code Remise";
    //                 SETRANGE("Discount Code", GroupeRemise);
    //                 // FIN AD Le 01-09-2011

    //                 SETRANGE("Type de commande", TypeCommande);
    //                 SETFILTER("Ending Date", '%1|>=%2', 0D, StartingDate);
    //                 IF NOT ShowAll THEN BEGIN
    //                     SETRANGE("Starting Date", 0D, StartingDate);
    //                     //SETFILTER("Unit of Measure Code",'%1|%2',UOM,'');
    //                 END;

    //                 IF FIND('-') THEN
    //                     REPEAT
    //                         IF GroupeRemiseAchat."Line Discount %" <> 0 THEN BEGIN
    //                             ToPurchLineDisc."Item No." := ItemNo;
    //                             ToPurchLineDisc."Vendor No." := VendorNo;
    //                             ToPurchLineDisc."Starting Date" := GroupeRemiseAchat."Starting Date";
    //                             //ToPurchLineDisc."Currency Code"        := GroupeRemiseAchat."Currency Code";
    //                             ToPurchLineDisc."Variant Code" := '';
    //                             ToPurchLineDisc."Unit of Measure Code" := '';
    //                             ToPurchLineDisc."Minimum Quantity" := GroupeRemiseAchat."Minimum Quantity";
    //                             ToPurchLineDisc."Line Discount %" := GroupeRemiseAchat."Line Discount %";
    //                             ToPurchLineDisc."Line Discount 1 %" := GroupeRemiseAchat."Line Discount 1 %";
    //                             ToPurchLineDisc."Line Discount 2 %" := GroupeRemiseAchat."Line Discount 2 %";
    //                             ToPurchLineDisc."Type de commande" := GroupeRemiseAchat."Type de commande";
    //                             ToPurchLineDisc."Ending Date" := GroupeRemiseAchat."Ending Date";
    //                             ToPurchLineDisc.Insert();
    //                         END;
    //                     UNTIL NEXT () = 0;
    //             END;
    //     END;
    // end;
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnBeforePurchLinePriceExists', '', false, false)]
    local procedure CU7010_OnBeforePurchLinePriceExists(var PurchaseLine: Record "Purchase Line"; var PurchaseHeader: Record "Purchase Header"; var TempPurchasePrice: Record "Purchase Price" temporary; ShowAll: Boolean; var IsHandled: Boolean; var DateCaption: Text[30])
    var
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        IsHandled := true;
        PurchPriceCalcMgt.FindPurchPrice(
              // AD Le 11-01-2012 => SYMTA -> Tarif sur le fournisseur preneur
              // TempPurchasePrice, "Pay-to Vendor No.", "No.", "Variant Code", "Unit of Measure Code",
              TempPurchasePrice, PurchaseLine."Buy-from Vendor No.", PurchaseLine."No.", PurchaseLine."Variant Code", PurchaseLine."Unit of Measure Code",
              // FIN AD Le 11-01-2012
              PurchaseHeader."Currency Code", LCodeunitsFunctions.PurchHeaderStartDate(PurchaseHeader, DateCaption), ShowAll);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch. Price Calc. Mgt.", 'OnBeforePurchLineLineDiscExists', '', false, false)]
    local procedure CU7010_OnBeforePurchLineLineDiscExists(var PurchaseLine: Record "Purchase Line"; var PurchaseHeader: Record "Purchase Header"; var TempPurchLineDisc: Record "Purchase Line Discount" temporary; ShowAll: Boolean; var IsHandled: Boolean; var DateCaption: Text[30])
    var
        Item: Record Item;
        PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        TypeCommande: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
    begin
        IsHandled := true;
        // MCO Le 14-02-2018 => R‚gie
        IF (PurchaseLine."Document Type" = PurchaseLine."Document Type"::Invoice) OR (PurchaseLine."Document Type" = PurchaseLine."Document Type"::"Credit Memo") THEN
            TypeCommande := PurchaseLine."Type de commande"
        ELSE
            TypeCommande := PurchaseHeader."Type de commande";
        // FIN MCO Le 14-02-2018

        IF (PurchaseLine.Type = PurchaseLine.Type::Item) AND Item.GET(PurchaseLine."No.") THEN
            PurchPriceCalcMgt.FindPurchLineDisc(
                          // AD Le 11-01-2012 => SYMTA -> Tarif sur le fournisseur preneur
                          // TempPurchLineDisc, PurchaseLine."Pay-to Vendor No.", PurchaseLine."No.", PurchaseLine."Variant Code", PurchaseLine."Unit of Measure Code",
                          TempPurchLineDisc, PurchaseLine."Buy-from Vendor No.", PurchaseLine."No.", PurchaseLine."Variant Code", PurchaseLine."Unit of Measure Code",
              // FIN AD Le 11-01-2012
              PurchaseHeader."Currency Code", LCodeunitsFunctions.PurchHeaderStartDate(PurchaseHeader, DateCaption), ShowAll,
              PurchaseLine."Qty. per Unit of Measure", PurchaseLine.Quantity);
        //,PurchHeader."Type de commande", ''); //MC Le 11-05-11 Rajout de l'avant dernier paramŠtre // AD Le 01-09-2011 => derner param
        //,TypeCommande, ''); //MCO Le 14-02-2018 => Finalement on retire

    end;
    //<< Mig CU 7010
    //>> Mig CU 5764
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment (Yes/No)", 'OnBeforeConfirmWhseShipmentPost', '', false, false)]
    local procedure CU5764_OnBeforeConfirmWhseShipmentPost(var WhseShptLine: Record "Warehouse Shipment Line"; var HideDialog: Boolean; var Invoice: Boolean; var IsPosted: Boolean; var Selection: Integer)
    var
        WhsePostShipment: Codeunit "Whse.-Post Shipment";
        ESK000Qst: Label '&Livrer';
    begin
        IsPosted := true;

        if WhseShptLine.Find() then
            if not HideDialog then begin
                // CFR le 22/09/2021 => R‚gie vente : bloquer le [Livrer Facturer] … partir du BP
                Selection := STRMENU(ESK000Qst, 1);
                //if not PostingSelectionManagement.ConfirmPostWhseShipment(WhseShptLine, Selection) then
                IF Selection = 0 THEN
                    exit;
                Invoice := (Selection = 2);
            end;

        WhsePostShipment.SetPostingSettings(Invoice);
        WhsePostShipment.SetPrint(false);
        WhsePostShipment.Run(WhseShptLine);
        WhsePostShipment.GetResultMessage();
        Clear(WhsePostShipment);
    end;
    //<< Mig CU 5764
    //>> Mig CU 5760
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnBeforeCheckUnitOfMeasureCode', '', false, false)]
    local procedure CU5760_OnBeforeCheckUnitOfMeasureCode(WarehouseReceiptLine: Record "Warehouse Receipt Line"; var IsHandled: Boolean)
    begin
        // MC Le 02-11-2012 => SYMTA -> BUG des r‚ceptions
        WarehouseReceiptLine.TESTFIELD("Bin Code");
        // FIN MC Le 02-11-2012
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnCodeOnAfterGetWhseRcptHeader', '', false, false)]
    local procedure CU5760_OnCodeOnAfterGetWhseRcptHeader(var WarehouseReceiptHeader: Record "Warehouse Receipt Header")
    begin
        // AD Le 02-03-2012 => SYMTA -> Permet de forcer la date de validation du BP … la date de travail
        WarehouseReceiptHeader.VALIDATE("Posting Date", WORKDATE());
        // FIN AD Le 02-03-2012

        WarehouseReceiptHeader.TESTFIELD("Date Arrivage Marchandise"); // AD Le 30-01-2020 => REGIE -> Date d'arrivage obligatoire
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnAfterCheckWhseRcptLines', '', false, false)]
    local procedure CU5760_OnAfterCheckWhseRcptLines(var WhseRcptHeader: Record "Warehouse Receipt Header"; var WhseRcptLine: Record "Warehouse Receipt Line")
    begin
        // MC Le 26-10-2011 => SYMTA -> Obligation de mettre un utilisateur
        WhseRcptHeader.TESTFIELD("Assigned User ID");
        // FIN MC Le 26-10-2011

        // MC Le 15-09-2011 => SYMTA -> Non g‚r‚.
        // Mise en commentaire
        // AD Le 18-12-2009 => FARGROUP -> Cout -> Demande de Pierre pour un message si pas de frais
        //IF WhseRcptHeader."Frais Port France (DS)" + WhseRcptHeader."Frais Port Maritime (DC)" = 0 THEN
        //  IF NOT CONFIRM(ESK50000) THEN
        //    ERROR(ESK50001);
        // FIN AD Le 18-12-2009
        // FIN MC Le 15-09-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnCodeOnBeforeWhseRcptHeaderModify', '', false, false)]
    local procedure CU5760_OnCodeOnBeforeWhseRcptHeaderModify(var WarehouseReceiptHeader: Record "Warehouse Receipt Header"; WarehouseReceiptLine: Record "Warehouse Receipt Line")
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // AD Le 22-09-2009 => FARGROUP -> Cout
        CduGSingleInstance.FctSetMtReception(LCodeunitsFunctions.CalcMtReception(WarehouseReceiptLine));
        // FIN AD le 22-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnAfterInitSourceDocumentHeader', '', false, false)]
    local procedure CU5760_OnAfterInitSourceDocumentHeader(var WarehouseReceiptLine: Record "Warehouse Receipt Line")
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // AD Le 22-09-2009 => FARGROUP -> Cout
        LCodeunitsFunctions.InitSourceDocumentLinesCout(WarehouseReceiptLine, CduGSingleInstance.FctGetMtReception());
        CduGSingleInstance.FctSetMtReception(0);
        // FIN AD le 22-09-2009
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnAfterCode', '', false, false)]
    local procedure CU5760_OnAfterCode(var WarehouseReceiptHeader: Record "Warehouse Receipt Header"; WarehouseReceiptLine: Record "Warehouse Receipt Line"; CounterSourceDocTotal: Integer; CounterSourceDocOK: Integer)
    var
        recL_PurchLine: Record "Purchase Line";
        recL_SalesLine: Record "Sales Line";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        // MCO Le 09-02-2017 => R‚gie : Solder mˆme les lignes … z‚ro
        WarehouseReceiptLine.SETCURRENTKEY("No.");
        WarehouseReceiptLine.SETRANGE("No.", WarehouseReceiptLine."No.");
        WarehouseReceiptLine.SETRANGE("Qty. to Receive", 0);
        WarehouseReceiptLine.SETRANGE("Close Line", TRUE);
        IF WarehouseReceiptLine.FINDSET() THEN
            REPEAT
                CASE WarehouseReceiptLine."Source Type" OF
                    DATABASE::"Purchase Line":
                        BEGIN
                            recL_PurchLine.GET(WarehouseReceiptLine."Source Subtype", WarehouseReceiptLine."Source No.", WarehouseReceiptLine."Source Line No.");
                            recL_PurchLine.ShortClose();
                            recL_PurchLine.MODIFY();
                        END;
                    DATABASE::"Sales Line": // Return Order
                        BEGIN
                            recL_SalesLine.GET(WarehouseReceiptLine."Source Subtype", WarehouseReceiptLine."Source No.", WarehouseReceiptLine."Source Line No.");
                            recL_SalesLine.ShortClose();
                            recL_SalesLine.MODIFY();
                        END;
                END;
            UNTIL WarehouseReceiptLine.NEXT() = 0;
        // FIN MCO Le 09-02-2017


        // AD Le 24-05-2007 => Supprssion syst‚matique de l'entete pour repasser dans le cycle normal de selection
        LCodeunitsFunctions.DeleteWarehouseDocument(WarehouseReceiptLine);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnInitSourceDocumentHeaderOnBeforePurchHeaderModify', '', false, false)]
    local procedure CU5760_OnInitSourceDocumentHeaderOnBeforePurchHeaderModify(var PurchaseHeader: Record "Purchase Header"; var WarehouseReceiptHeader: Record "Warehouse Receipt Header"; var ModifyHeader: Boolean)
    begin
        // MC Le 26-10-2011 => SYMTA -> Suivi des champs depuis le bon de r‚ception
        IF STRLEN(WarehouseReceiptHeader."Assigned User ID") <= MAXSTRLEN(PurchaseHeader.Réceptionneur) THEN
            PurchaseHeader.Réceptionneur := WarehouseReceiptHeader."Assigned User ID";
        ModifyHeader := TRUE;
        // FIN MC Le 26-10-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnInitSourceDocumentLinesOnAfterSourcePurchLineFound', '', false, false)]
    local procedure CU5760_OnInitSourceDocumentLinesOnAfterSourcePurchLineFound(var PurchaseLine: Record "Purchase Line"; WhseRcptLine: Record "Warehouse Receipt Line"; var ModifyLine: Boolean; WhseRcptHeader: Record "Warehouse Receipt Header")
    var
        WMSMgt: Codeunit "WMS Management";
        LCodeunitsFunctions: codeunit "Codeunits Functions";
        txt_BinCode: Code[20];
        txt_ZoneCode: Code[20];
    begin
        if WhseRcptLine."Source Document" = WhseRcptLine."Source Document"::"Purchase Order" then begin
            // AD Le 28-08-2009 => Possibilit‚ de solder la ligne d'origine
            IF WhseRcptLine."Close Line" THEN BEGIN
                PurchaseLine.ShortClose();
                ModifyLine := TRUE;
            END;
            // FIN AD Le 28-08-2009
            // AD Le 24-03-2015 =>
            IF WhseRcptLine."Vendor Shipment No." = '' THEN
                WhseRcptLine."Vendor Shipment No." := WhseRcptHeader."Vendor Shipment No.";

            IF WhseRcptLine."Vendor Shipment No." <> PurchaseLine."Vendor Shipment No." THEN BEGIN
                PurchaseLine."Vendor Shipment No." := WhseRcptLine."Vendor Shipment No.";
                ModifyLine := TRUE;
            END;
            // FIN AD Le 24-03-2015
            PurchaseLine."Date Arrivage Marchandise" := WhseRcptHeader."Date Arrivage Marchandise"; // AD Le 30-01-2020

            // MC Le 30-01-2013 => On r‚cupŠre toujours le code emplacement par d‚faut de l'article pour une r‚ception achat
            IF (WhseRcptLine."Source Type" = 39) AND (WhseRcptLine."Source Subtype" = 1) THEN BEGIN
                txt_BinCode := WhseRcptLine."Bin Code";
                LCodeunitsFunctions.ESK_GetDefaultBin(WhseRcptLine."Item No.", WhseRcptLine."Variant Code", WhseRcptLine."Location Code", txt_BinCode, txt_ZoneCode);
                WhseRcptLine."Bin Code" := txt_BinCode;
                WhseRcptLine."Zone Code" := txt_ZoneCode;
                WhseRcptLine.MODIFY();
            END;
            IF PurchaseLine."Zone Code" <> WhseRcptLine."Zone Code" THEN BEGIN
                PurchaseLine."Zone Code" := WhseRcptLine."Zone Code";
                ModifyLine := TRUE;
            END;
            // FIN MC Le 30-01-2013
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnInitSourceDocumentLinesOnAfterSourceSalesLineFound', '', false, false)]
    local procedure CU5760_OnInitSourceDocumentLinesOnAfterSourceSalesLineFound(var SalesLine: Record "Sales Line"; WhseRcptLine: Record "Warehouse Receipt Line"; var ModifyLine: Boolean; WhseRcptHeader: Record "Warehouse Receipt Header"; OldWhseRcptLine: Record "Warehouse Receipt Line")
    begin
        if not (WhseRcptLine."Source Document" = WhseRcptLine."Source Document"::"Sales Order") then begin
            // AD Le 28-08-2009 => Possibilit‚ de solder la ligne d'origine
            IF WhseRcptLine."Close Line" THEN BEGIN
                SalesLine.ShortClose();
                ModifyLine := TRUE;
            END;
            // FIN AD Le 28-08-2009
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnBeforeCheckUpdateSalesLineBinCode', '', false, false)]
    local procedure CU5760_OnBeforeCheckUpdateSalesLineBinCode(var SalesLine: Record "Sales Line"; WhseRcptLine: Record "Warehouse Receipt Line"; var ModifyLine: Boolean; var IsHandled: Boolean)
    var
        LCodeunitsFunctions: codeunit "Codeunits Functions";
        txt_BinCode: Code[20];
        txt_ZoneCode: Code[20];
    begin
        // MC Le 24-10-2012 => FE20121022 -> On r‚cupŠre toujours le code emplacement par d‚faut de l'article pour un retour
        IF (WhseRcptLine."Source Type" = 37) AND (WhseRcptLine."Source Subtype" = 5) THEN BEGIN
            txt_BinCode := WhseRcptLine."Bin Code";
            LCodeunitsFunctions.ESK_GetDefaultBin(WhseRcptLine."Item No.", WhseRcptLine."Variant Code", WhseRcptLine."Location Code", txt_BinCode, txt_ZoneCode);
            WhseRcptLine."Bin Code" := txt_BinCode;
            WhseRcptLine."Zone Code" := txt_ZoneCode;
            WhseRcptLine.MODIFY();
        END;

        IF SalesLine."Zone Code" <> WhseRcptLine."Zone Code" THEN BEGIN
            SalesLine."Zone Code" := WhseRcptLine."Zone Code";
            ModifyLine := TRUE;
        END;

        // FIN MC Le 24-10-2012
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnBeforeUpdateWhseRcptLine', '', false, false)]
    local procedure CU5760_OnBeforeUpdateWhseRcptLine(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; var WarehouseReceiptLineBuf: Record "Warehouse Receipt Line"; var IsHandled: Boolean)
    begin
        IsHandled := true;

        WarehouseReceiptLine.Validate("Qty. Received", WarehouseReceiptLineBuf."Qty. Received" + WarehouseReceiptLineBuf."Qty. to Receive");
        // MC Le 16-11-2012 => SYMTA -> Ne pas valider les reliquats si plantage
        //WarehouseReceiptLine.Validate("Qty. Outstanding", "Qty. Outstanding" - "Qty. to Receive");
        WarehouseReceiptLine.VALIDATE("Qty. Outstanding", 0);
        // FIN MC Le 16-11-2012
        WarehouseReceiptLine."Qty. to Cross-Dock" := 0;
        WarehouseReceiptLine."Qty. to Cross-Dock (Base)" := 0;
        WarehouseReceiptLine.Status := WarehouseReceiptLine.GetLineStatus();
        WarehouseReceiptLine.Modify();
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Receipt", 'OnBeforePostedWhseRcptLineInsert', '', false, false)]
    local procedure CU5760_OnBeforePostedWhseRcptLineInsert(var PostedWhseReceiptLine: Record "Posted Whse. Receipt Line"; WarehouseReceiptLine: Record "Warehouse Receipt Line")
    var
        PostedWhseRcptHeader: Record "Posted Whse. Receipt Header";
    begin
        if PostedWhseRcptHeader.Get(PostedWhseReceiptLine."No.") then;
        //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [50060] et [50061]
        PostedWhseReceiptLine.SetPartner();
        //FIN CFR le 27/09/2023

        // AD Le 27-01-2012
        PostedWhseReceiptLine."Prix Brut Origine" := WarehouseReceiptLine.GetInfoLigne('PXBRUT');
        PostedWhseReceiptLine."Remise 1 Origine" := WarehouseReceiptLine.GetInfoLigne('REMISE1');
        PostedWhseReceiptLine."Net 1 Origine" := WarehouseReceiptLine.GetInfoLigne('NET1');
        PostedWhseReceiptLine."Total Net 1 Origine" := WarehouseReceiptLine.GetInfoLigne('TOTALNET1');
        PostedWhseReceiptLine."Remise 2 Origine" := WarehouseReceiptLine.GetInfoLigne('REMISE2');
        PostedWhseReceiptLine."Net 2 Origine" := WarehouseReceiptLine.GetInfoLigne('NET2');
        PostedWhseReceiptLine."Total Origine" := WarehouseReceiptLine.GetInfoLigne('TOTALNET2');
        // FIN AD Le 27-01-2012

        // AD Le 24-03-2015 => No de BL fournisseur … la ligne de r‚ception
        IF WarehouseReceiptLine."Vendor Shipment No." <> '' THEN
            PostedWhseReceiptLine."Vendor Shipment No." := WarehouseReceiptLine."Vendor Shipment No."
        ELSE
            PostedWhseReceiptLine."Vendor Shipment No." := PostedWhseRcptHeader."Vendor Shipment No.";
        // FIN AD Le 24-03-2015

        //fbrun le 12/08/20  FE20200728
        PostedWhseReceiptLine."Date Arrivage Marchandise" := PostedWhseRcptHeader."Date Arrivage Marchandise";
    end;
    //<< Mig CU 5760
    //>> Mig CU 5763
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnCodeOnAfterGetWhseShptHeader', '', false, false)]
    local procedure CU5763_OnCodeOnAfterGetWhseShptHeader(var WarehouseShipmentHeader: Record "Warehouse Shipment Header")
    var
        Text50000_Err: Label 'Ce document est bloqué !';
    begin
        // AD Le 19-11-2009 => FARGROUP -> Permet de forcer la date de validation du BP … la date de travail
        WarehouseShipmentHeader.VALIDATE("Posting Date", WORKDATE());
        // FIN AD Le 19-11-2009
        // AD Le 28-08-2009 => FARGROUP -> Possibilit‚ de bloquer un BP avant la validation
        if WarehouseShipmentHeader.Blocked then
            ERROR(Text50000_Err);
        // FIN AD Le 28-08-2009

        // MC Le 26-10-2011 => SYMTA -> Obligation de saisir un utilisatur
        WarehouseShipmentHeader.TESTFIELD("Assigned User ID");
        // FIN MC L 26-10-2011

        // MCO Le 20-11-2018 => SYMTA
        WarehouseShipmentHeader."CheckWeigth&Box"();
        // FIN MCO Le 20-11-2018
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterSetCurrentKeyForWhseShptLine', '', false, false)]
    local procedure CU5763_OnAfterSetCurrentKeyForWhseShptLine(var WarehouseShipmentLine: Record "Warehouse Shipment Line")
    var
        articleAvecRegroupement: Boolean;
        articleSansRegroupement: Boolean;
    begin
        //FBRUN le 27/04/2011 =>Gestion si plusieur regroupement alors plusieur BL
        WarehouseShipmentLine.SETRANGE("Gerer par groupement", TRUE);
        articleAvecRegroupement := WarehouseShipmentLine.FINDFIRST();
        WarehouseShipmentLine.SETRANGE("Gerer par groupement", FALSE);
        articleSansRegroupement := WarehouseShipmentLine.FINDFIRST();
        CduGSingleInstance.FctSet2BLPARcommande(articleAvecRegroupement AND articleSansRegroupement);
        // AD Le 28-06-2011
        WarehouseShipmentLine.SETRANGE("Gerer par groupement");
        // FIN AD Le 28-06-2011
        IF WarehouseShipmentLine.FINDFIRST() THEN;

        IF CduGSingleInstance.FctGet2BLPARcommande() THEN BEGIN
            WarehouseShipmentLine.SETRANGE("Gerer par groupement", FALSE);
            WarehouseShipmentLine.FILTERGROUP := 1;
            WarehouseShipmentLine.SETRANGE("Gerer par groupement", FALSE);
            WarehouseShipmentLine.FILTERGROUP := 0;
        END;
        //FIN FBRUN

        //WarehouseShipmentLine.SetCurrentKey("No.", "Source Type", "Source Subtype", "Source No.", "Source Line No.");
        WarehouseShipmentLine.SETCURRENTKEY("No.", "Source Type", "Source Subtype", "Source No.", "Gerer par groupement");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterPostWhseShipment', '', false, false)]
    local procedure CU5763_OnAfterPostWhseShipment(var WarehouseShipmentHeader: Record "Warehouse Shipment Header"; SuppressCommit: Boolean; var IsHandled: Boolean)
    var
        WhseShptLine: Record "Warehouse Shipment Line";
        SalesHeader: Record "Sales Header";
        PurchHeader: Record "Purchase Header";
        TransHeader: Record "Transfer Header";
        ServiceHeader: Record "Service Header";
        SourceHeader: Variant;
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        //FBRUN le 27/04/2011 =>Gestion si plusieur regroupement alors plusieur BL
        //creation du 2iem BL
        // with WhseShptLine do begin
        //     IF CduGSingleInstance.FctGet2BLPARcommande THEN BEGIN
        //         RESET();
        //         SETRANGE("Gerer par groupement", TRUE);
        //         SETRANGE("No.", "No.");
        //         SETFILTER("Qty. to Ship", '>0');


        //         FILTERGROUP := 1;
        //         SETRANGE("Gerer par groupement", TRUE);
        //         FILTERGROUP := 0;
        //         FINDFIRST;
        //         REPEAT

        //             SETRANGE("Source Type", "Source Type");
        //             SETRANGE("Source Subtype", "Source Subtype");
        //             SETRANGE("Source No.", "Source No.");
        //             //GetSourceDocument(WhseShptLine);
        //             case "Source Type" of
        //                 DATABASE::"Sales Line":
        //                     begin
        //                         SalesHeader.Get("Source Subtype", "Source No.");
        //                         SourceHeader := SalesHeader;
        //                     end;
        //                 DATABASE::"Purchase Line": // Return Order
        //                     begin
        //                         PurchHeader.Get("Source Subtype", "Source No.");
        //                         SourceHeader := PurchHeader;
        //                     end;
        //                 DATABASE::"Transfer Line":
        //                     begin
        //                         TransHeader.Get("Source No.");
        //                         SourceHeader := TransHeader;
        //                     end;
        //                 DATABASE::"Service Line":
        //                     begin
        //                         ServiceHeader.Get("Source Subtype", "Source No.");
        //                         SourceHeader := ServiceHeader;
        //                     end;
        //             end;
        //             //
        //             LCodeunitsFunctions.MakePreliminaryChecks(WarehouseShipmentHeader);

        //             /*TODO
        //             InitSourceDocumentLines(WhseShptLine);

        //             // AD Le 05-12-2011 => SYMTA
        //             SalesHeader.Weight := WarehouseShipmentHeader.Poids;
        //             SalesHeader."Nb Of Box" := WarehouseShipmentHeader."Nb Of Box";
        //             // FIN AD Le 05-12-2011

        //             InitSourceDocumentHeader;
        //             COMMIT;

        //             CounterSourceDocTotal := CounterSourceDocTotal + 1;
        //             PostSourceDocument(WhseShptLine);
        //             */
        //             SETCURRENTKEY("No.", "Source Type", "Source Subtype", "Source No.", "Gerer par groupement");
        //             IF FINDLAST THEN;

        //             SETRANGE("Source Type");
        //             SETRANGE("Source Subtype");
        //             SETRANGE("Source No.");
        //             SETRANGE("Gerer par groupement");
        //         UNTIL NEXT () = 0;

        //     END;

        //     FILTERGROUP := 1;
        //     SETRANGE("Gerer par groupement");
        //     FILTERGROUP := 0;

        // END;
        // //FIN FBRUN
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnInitSourceDocumentHeaderOnBeforeReopenSalesHeader', '', false, false)]
    local procedure CU5763_OnInitSourceDocumentHeaderOnBeforeReopenSalesHeader(var SalesHeader: Record "Sales Header"; Invoice: Boolean; var NewCalledFromWhseDoc: Boolean)
    begin
        // CFR le 05/10/2023 - R‚gie : ne pas changer les informations de modifications de l'en-tˆte
        CduGSingleInstance.FctsetOldValuesCU5763(SalesHeader."Modify User ID", SalesHeader."Modify Date", SalesHeader."Modify Time");
        // FIN CFR le 05/10/2023
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnPostUpdateWhseDocumentsOnBeforeUpdateWhseShptHeader', '', false, false)]
    local procedure CU5763_OnPostUpdateWhseDocumentsOnBeforeUpdateWhseShptHeader(var WhseShptHeaderParam: Record "Warehouse Shipment Header")
    var
        WhseShptLine2: Record "Warehouse Shipment Line";
    begin
        CduGSingleInstance.FctSaveNoWhseShptHeaderParam(WhseShptHeaderParam."No.");
        WhseShptHeaderParam."No." := WhseShptLine2."No.";
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnPostUpdateWhseDocumentsOnBeforeWhseShptHeaderParamModify', '', false, false)]
    local procedure CU5763_OnPostUpdateWhseDocumentsOnBeforeWhseShptHeaderParamModify(var WhseShptHeaderParam: Record "Warehouse Shipment Header"; var WhseShptHeader: Record "Warehouse Shipment Header")
    begin
        WhseShptHeaderParam."No." := CduGSingleInstance.FctGetNoWhseShptHeaderParam();
        CduGSingleInstance.FctSaveNoWhseShptHeaderParam('');
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnInitSourceDocumentHeaderOnBeforeSalesHeaderModify', '', false, false)]
    local procedure CU5763_OnInitSourceDocumentHeaderOnBeforeSalesHeaderModify(var SalesHeader: Record "Sales Header"; var WarehouseShipmentHeader: Record "Warehouse Shipment Header"; var ModifyHeader: Boolean; Invoice: Boolean; var WarehouseShipmentLine: Record "Warehouse Shipment Line")
    begin
        IF (SalesHeader."Posting Date" = 0D) OR
   (SalesHeader."Posting Date" <> WarehouseShipmentHeader."Posting Date") THEN BEGIN
            // CFR le 05/10/2023 - R‚gie : ne pas changer les informations de modifications de l'en-tˆte
            CduGSingleInstance.FctGetOldValuesCU5763(SalesHeader);
            CduGSingleInstance.FctSetOldValuesCU5763('', 0D, 0T);
            // FIN CFR le 05/10/2023
        end;

        // MC Le 27-04-2011 => SYMTA -> Suivi du num‚ro de BP et de la date de validation.
        SalesHeader."Preparation No." := WarehouseShipmentHeader."No.";
        SalesHeader."Preparation Valid Date" := TIME;
        // MC Le 04-05-2011 => SYMTA -> Force le pr‚parateur avec le champ du BP.
        SalesHeader.Preparateur := WarehouseShipmentHeader."Assigned User ID";

        // MC Le 04-05-2011 => SYMTA -> Force le poids r‚els.
        SalesHeader.Weight := WarehouseShipmentHeader.Poids;
        // FIN MC Le 27-04-2011
        // MC Le 07-06-2011 => SYMTA -> Suivi des champs si pied de BL saisit sur BP.
        SalesHeader."Code Emballage" := WarehouseShipmentHeader."Code Emballage";
        SalesHeader."Nb Of Box" := WarehouseShipmentHeader."Nb Of Box";
        SalesHeader."Mode d'expédition" := WarehouseShipmentHeader."Mode d'expédition";


        SalesHeader."Zone Code" := WarehouseShipmentHeader."Zone Code";
        ModifyHeader := TRUE;
        // FIN MC Le 07-06-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterCheckWhseShptLines', '', false, false)]
    local procedure CU5763_OnAfterCheckWhseShptLines(var WhseShptHeader: Record "Warehouse Shipment Header"; var WhseShptLine: Record "Warehouse Shipment Line"; Invoice: Boolean; var SuppressCommit: Boolean)
    var
        Text50000_Err: Label 'Ce document est bloqué !';
    begin
        // AD Le 28-08-2009 => FARGROUP -> Possibilit‚ de bloquer un BP avant la validation
        if WhseShptHeader.Blocked then
            Error(Text50000_Err);
        // FIN AD Le 28-08-2009

        // MC Le 26-10-2011 => SYMTA -> Obligation de saisir un utilisatur
        WhseShptHeader.TESTFIELD("Assigned User ID");
        // FIN MC L 26-10-2011

        // MCO Le 20-11-2018 => SYMTA
        WhseShptHeader."CheckWeigth&Box"();
        // FIN MCO Le 20-11-2018
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterPostSourceDocument', '', false, false)]
    local procedure CU5763_OnAfterPostSourceDocument(var WarehouseShipmentLine: Record "Warehouse Shipment Line"; var Print: Boolean)
    begin
        WarehouseShipmentLine.SETCURRENTKEY("No.", "Source Type", "Source Subtype", "Source No.", "Gerer par groupement");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterReleaseSourceForFilterWhseShptLine', '', false, false)]
    local procedure CU5763_OnAfterReleaseSourceForFilterWhseShptLine(var WarehouseShipmentLine: Record "Warehouse Shipment Line")
    begin
        WarehouseShipmentLine.SETRANGE("Gerer par groupement");
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnAfterRun', '', false, false)]
    local procedure CU5763_OnAfterRun(var WarehouseShipmentLine: Record "Warehouse Shipment Line")
    var
        WhseShptHeader: Record "Warehouse Shipment Header";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        gPackingListMgmt: Codeunit "Packing List Mgmt";
    begin
        if WhseShptHeader.Get(WarehouseShipmentLine."No.") then
            // CFR le 05-06-2020 => WIIO -> ESKVN2.0 : PACKING V2 (R‚cup Navin‚goce BC)
            gPackingListMgmt.PackingListPostHeader(WhseShptHeader."No.", WhseShptHeader."Packing List No.");
        // FIN CFR le 05-06-2020

        // AD Le 24-05-2007 => Supprssion syst‚matique de l'entete pour repasser dans le cycle normal de selection
        LCodeunitsFunctions.DeleteWarehouseDocument(WarehouseShipmentLine);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnPrintDocumentsOnAfterPrintSalesShipment', '', false, false)]
    local procedure CU5763_OnPrintDocumentsOnAfterPrintSalesShipment(ShipmentNo: Code[20])
    var
        DetPackinglist: Record "Détail Packing List";
        SalesShptHeader: Record "Sales Shipment Header";
        SalesShptHeader2: Record "Sales Shipment Header";
        gPackingListMgmt: Codeunit "Packing List Mgmt";
    begin
        // MC Le 30-04-2010 => Analyse compl‚mentaire --> Module Packing List
        //"paking list".SETCURRENTKEY("Nø expedition magasin");
        DetPackinglist.Reset();
        DetPackinglist.SETRANGE("N° expedition magasin", ShipmentNo);
        IF DetPackinglist.FINDFIRST() THEN BEGIN
            SalesShptHeader2.SETRANGE("No.", DetPackinglist."N° BL");
            REPORT.RUN(50069, FALSE, FALSE, SalesShptHeader2);
            REPORT.RUN(50070, FALSE, FALSE, SalesShptHeader2);
        END;
        //FIN MC

        if SalesShptHeader.Get(ShipmentNo) then
            // CFR le 05-06-2020 => WIIO -> ESKVN2.0 : PACKING V2 (R‚cup Navin‚goce BC)
            gPackingListMgmt.PrintPackingListItem(SalesShptHeader."Packing List No.", TRUE, 'COLIS');
        // FIN CFR le 05-06-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnBeforePostedWhseShptHeaderInsert', '', false, false)]
    local procedure CU5763_OnBeforePostedWhseShptHeaderInsert(var PostedWhseShipmentHeader: Record "Posted Whse. Shipment Header"; WarehouseShipmentHeader: Record "Warehouse Shipment Header")
    begin
        // MC Le 02-05-2011 => SYMTA -> Gestion de la pr‚paration.
        PostedWhseShipmentHeader."Source Document" := WarehouseShipmentHeader."Source Document";
        PostedWhseShipmentHeader."Source No." := WarehouseShipmentHeader."Source No.";
        PostedWhseShipmentHeader."Destination Type" := WarehouseShipmentHeader."Destination Type";
        PostedWhseShipmentHeader."Destination No." := WarehouseShipmentHeader."Destination No.";
        PostedWhseShipmentHeader.Poids := WarehouseShipmentHeader.Poids;
        PostedWhseShipmentHeader."Mode d'expédition" := WarehouseShipmentHeader."Mode d'expédition";
        // FIN MC Le 02-05-2011.

        // CFR le 05-06-2020 => WIIO -> ESKVN2.0 : PACKING V2 (R‚cup Navin‚goce BC)
        PostedWhseShipmentHeader."Packing List No." := WarehouseShipmentHeader."Packing List No.";
        // FIN CFR le 05-06-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnCreatePostedShptLineOnBeforePostedWhseShptLineInsert', '', false, false)]
    local procedure CU5763_OnCreatePostedShptLineOnBeforePostedWhseShptLineInsert(var PostedWhseShptLine: Record "Posted Whse. Shipment Line"; WhseShptLine: Record "Warehouse Shipment Line")
    begin
        // AD Le 04-11-2009 => Pour stocker la quantit‚ qui devait ˆtre livr‚e (pour le taux de service notament)
        PostedWhseShptLine."Qté Ouverte avant validation" := WhseShptLine."Qty. Outstanding";
        // FIN AD Le 04-11-2009
        // MC Le 04-05-2011 => SYMTA -> On fait suivre le poids;
        PostedWhseShptLine.Weight := WhseShptLine.Weight;
        // FIN MC Le 04-05-2011
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnCreatePostedShptLineOnBeforePostWhseJnlLine', '', false, false)]
    local procedure CU5763_OnCreatePostedShptLineOnBeforePostWhseJnlLine(var PostedWhseShipmentLine: Record "Posted Whse. Shipment Line"; var TempTrackingSpecification: Record "Tracking Specification" temporary; WarehouseShipmentLine: Record "Warehouse Shipment Line")
    var
        DetPackinglist: Record "Détail Packing List";
        PackingList: Record "Packing List";
        gPackingListMgmt: Codeunit "Packing List Mgmt";
    begin
        // MC Le 30-04-2010 => Analyse compl‚mentaire --> Module Packing List
        //PACKING LIST -> Mise a jour du no de bl dans la Packing List
        PackingList.RESET();
        PackingList.SETCURRENTKEY("N° expedition magasin", "N° Ligne origine");
        PackingList.SETRANGE("N° expedition magasin", WarehouseShipmentLine."No.");
        PackingList.SETRANGE("N° Ligne origine", WarehouseShipmentLine."Line No.");
        IF PackingList.FIND('-') THEN BEGIN
            PackingList.MODIFYALL("N° expedition enregistré", PostedWhseShipmentLine."No.");
            PackingList.MODIFYALL("N° BL", PostedWhseShipmentLine."Posted Source No.");
        END;
        // => Idem pour le d‚tail des colis.
        DetPackinglist.RESET();
        DetPackinglist.SETRANGE("N° expedition magasin", WarehouseShipmentLine."No.");
        IF DetPackinglist.FIND('-') THEN BEGIN
            DetPackinglist.MODIFYALL("N° expedition enregistré", PostedWhseShipmentLine."No.");
            DetPackinglist.MODIFYALL("N° BL", PostedWhseShipmentLine."Posted Source No.");
        END;
        //FIN MC

        // CFR le 05-06-2020 => WIIO -> ESKVN2.0 : PACKING V2 (R‚cup Navin‚goce BC)
        gPackingListMgmt.PackingListPostLine(PostedWhseShipmentLine, WarehouseShipmentLine, PostedWhseShipmentLine."Posted Source No.");
        // FIN CFR le 05-06-2020
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Post Shipment", 'OnHandleSalesLineOnNonWhseLineOnAfterCalcModifyLine', '', false, false)]
    local procedure CU5763_OnHandleSalesLineOnNonWhseLineOnAfterCalcModifyLine(var SalesLine: Record "Sales Line"; var ModifyLine: Boolean; WhseShptLine: Record "Warehouse Shipment Line")
    var
        WhseShptHeader: Record "Warehouse Shipment Header";
    begin
        if WhseShptHeader.Get(WhseShptLine."No.") then;
        // MC le 24-09-2012 => Gestion des lignes n‚gatives sur groupement
        IF SalesLine.Quantity < 0 THEN
            IF SalesLine."Outstanding Quantity" <> 0 THEN BEGIN
                // MCO Le 08-09-2017 => Correction suite modification de rŠgle
                IF (ABS(SalesLine."Qty To Ship. Save") <= ABS(SalesLine."Outstanding Quantity")) AND (SalesLine."Qty To Ship. Save" <> 0) THEN BEGIN
                    SalesLine.VALIDATE("Qty. to Ship", SalesLine."Qty To Ship. Save");
                END;
                // FIN MCO Le 08-09-2017
                // MCO Le 09-02-2017 => Plus de mise … jour de la ligne quantit‚ renseign‚e manuellement
                /*
                SalesLine.VALIDATE("Qty. to Ship", SalesLine."Outstanding Quantity");
                */
                // FIN MCO Le 09-02-2017
                SalesLine.MODIFY();

            END;
        // FIN le 24-09-2012

        // CPH 02/02/11 -> Pour livrer en automatique les frais de port (compte g‚n‚ral)
        IF (SalesLine.Type = SalesLine.Type::"G/L Account") AND
           (SalesLine."Internal Line Type" IN [SalesLine."Internal Line Type"::Shipment,
                                              SalesLine."Internal Line Type"::"Discount Header"]) THEN
            ModifyLine := FALSE;
        // FIN CPH 02/02/11

        // AD Le 03-11-2011 -> Pour livrer en automatique les lignes N‚gatives
        IF (SalesLine.Type = SalesLine.Type::Item) AND
           ((SalesLine."Qty. to Ship" < 0) OR
           (SalesLine."Return Qty. to Receive" < 0))
           AND (SalesLine."Zone Code" = WhseShptHeader."Zone Code") THEN BEGIN
            // AD Le 12-01-2012 (a voir dans le cas ou c'est la
            // seule ligne de la zone !!!)

            // MC le 24-09-2012 => Gestion des lignes n‚gatives sur groupement
            //MESSAGE(FORMAT(SalesLine."Gerer par groupement") + ' BP :' + FORMAT("Gerer par groupement"));
            IF (SalesLine."Gerer par groupement" = WhseShptLine."Gerer par groupement") THEN BEGIN
                ModifyLine := FALSE;
            END;
        END;
        // FIN MC le 24-09-2012

        // MCO Le 08-09-2017 => Correction suite modification de rŠgle
        SalesLine."Qty To Ship. Save" := 0;
        IF (ModifyLine) AND (SalesLine."Qty. to Ship" < 0) THEN BEGIN
            SalesLine."Qty To Ship. Save" := SalesLine."Qty. to Ship";
        END;
        SalesLine.MODIFY();
        // FIN MCO Le 08-09-2017
    end;
    //<< Mig CU 5763
    //>> Mig CU 99000830
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Create Reserv. Entry", 'OnCreateEntryOnBeforeSurplusCondition', '', false, false)]
    local procedure CU99000830_OnCreateEntryOnBeforeSurplusCondition(var ReservEntry: Record "Reservation Entry"; QtyToHandleAndInvoiceIsSet: Boolean; var InsertReservEntry: Record "Reservation Entry")
    var
        LCodeunitsFunctions: codeunit "Codeunits Functions";
    begin
        // MCO Le 06-07-2018 => Ticket 37625
        LCodeunitsFunctions.ActivErrorOnReservation();
    end;
    //<< Mig CU 99000830
    //>> Mig CU 99000832
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Line-Reserve", 'OnBeforeCreateReservation', '', false, false)]
    local procedure CU99000832_OnBeforeCreateReservation(var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        LCodeunitsFunctions: codeunit "Codeunits Functions";
    begin
        // MCO Le 06-07-2018 => Ticket 37625
        LCodeunitsFunctions.ActivErrorOnReservation();
    end;
    //<< Mig CU 99000832
    //>> Mig CU 1
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"ReportManagement", 'OnAfterGetPrinterName', '', false, false)]
    local procedure CU44_GetPrinterName(ReportID: Integer; var PrinterName: Text[250]; PrinterSelection: Record "Printer Selection")
    var
        EskapeCommunication: Codeunit "Eskape Communication";
        CUGestionImpZone: Codeunit "Gestion Impression Zone";
        ImpZone: Text[30];
        CompanyInfo: Record "Company Information";
    begin
        // AD Le 26-05-2011 => SYMTA -> pour la gestion des imprimantes par zone
        ImpZone := CUGestionImpZone.GetZonePrinter();
        IF ImpZone <> '' THEN begin
            PrinterName := ImpZone;
            EXIT;
        end;
        // FIN AD Le 26-05-2011
        // AD Le 03-10-2011 => Gestion Impression PDF
        IF EskapeCommunication.GetImpressionPdf() THEN BEGIN
            CompanyInfo.GET();
            CompanyInfo.TESTFIELD(PDFPrinter);
            PrinterName := CompanyInfo.PDFPrinter;
            exit;
        END;
    end;
    //<< Mig CU 1
    //>>Mig CU 99000815
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reservation-Check Date Confl.", 'OnBeforeDateConflict', '', false, false)]
    local procedure CU99000815_OnBeforeDateConflict(var ReservationEntry: Record "Reservation Entry"; var IsConflict: Boolean; var IsHandled: Boolean; var Date: Date)
    var
        ReservationEntry2: Record "Reservation Entry";
    begin

        ReservationEntry2.Copy(ReservationEntry);

        if not ReservationEntry2.FindFirst() then begin
            exit;
        end;

        if ReservationEntry2."Quantity (Base)" >= 0 then begin
            CduGSingleInstance.FctSaveOldDate(Date);
            Date := 0D;
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Reservation-Check Date Confl.", 'OnAfterDateConflict', '', false, false)]
    local procedure CU99000815_OnAfterDateConflict(var ReservationEntry: Record "Reservation Entry"; var Date: Date; var IsConflict: Boolean; var ForceRequest: Boolean)
    begin
        Date := CduGSingleInstance.FctGetOldDate(Date);
    end;
    //<<Mig CU 99000815

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Explode BOM", 'OnRunOnBeforeExplodeBOMCompLines', '', false, false)]
    local procedure CU63_OnRunOnBeforeExplodeBOMCompLines(var SalesLine: Record "Sales Line"; var ToSalesLine: Record "Sales Line"; var NoOfBOMComp: Integer; var Selection: Integer; var IsHandled: Boolean; var BOMItemNo: Code[20])
    begin
        WITH SalesLine DO BEGIN
            ToSalesLine.RESET();
            ToSalesLine.SETRANGE("Document Type", "Document Type");
            ToSalesLine.SETRANGE("Document No.", "Document No.");
            ToSalesLine := SalesLine;
            IF ToSalesLine.FIND('>') THEN
                ERROR('Impossible d''éclater ici !');
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bank Acc. Entry Set Recon.-No.", 'OnAfterSetReconNo', '', false, false)]
    local procedure CU375_OnAfterSetReconNo(var BankAccountLedgerEntry: Record "Bank Account Ledger Entry")
    begin
        //ESK0.1
        BankAccountLedgerEntry.Select := false;
        BankAccountLedgerEntry.Modify();
        //ESK0.1
    end;


    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bank Acc. Entry Set Recon.-No.", 'OnAfterRemoveReconNo', '', false, false)]
    local procedure CU375_OnAfterRemoveReconNo(var BankAccountLedgerEntry: Record "Bank Account Ledger Entry"; Test: Boolean)
    begin
        //ESK0.1
        BankAccountLedgerEntry.Select := false;
        BankAccountLedgerEntry.Modify();
        //ESK0.1
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"VAT Rate Change Conversion", 'OnConvertOnBeforeStartConvert', '', false, false)]
    local procedure CU550_OnConvertOnBeforeStartConvert(var VATRateChangeSetup: Record "VAT Rate Change Setup")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Assembly Line Management", 'OnBeforeUpdateAssemblyLines', '', false, false)]
    local procedure CU905_OnBeforeUpdateAssemblyLines(var AsmHeader: Record "Assembly Header"; OldAsmHeader: Record "Assembly Header"; FieldNum: Integer; ReplaceLinesFromBOM: Boolean; CurrFieldNo: Integer; CurrentFieldNum: Integer; var IsHandled: Boolean; HideValidationDialog: Boolean)
    begin
        CduGSingleInstance.FctSetShowAvailibilityFunctionValue(true);

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Assembly Line Management", 'OnBeforeShowAvailability', '', false, false)]
    local procedure CU905_OnBeforeShowAvailability(var TempAssemblyHeader: Record "Assembly Header" temporary; var TempAssemblyLine: Record "Assembly Line" temporary; ShowPageEvenIfEnoughComponentsAvailable: Boolean; var IsHandled: Boolean; var RollBack: Boolean)

    begin
        if CduGSingleInstance.FctGetShowAvailibilityValue() then begin
            IsHandled := true;
            RollBack := false;
            CduGSingleInstance.FctSetShowAvailibilityFunctionValue(false);
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Data Migration Notifier", 'SkipListEmptyNotification', '', false, false)]
    local procedure CU1802_SkipListEmptyNotification(TableId: Integer; var SkipNotification: Boolean)
    begin
        if TableId = 27 then
            SkipNotification := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Reference Management", 'OnBeforeFindItemReferenceForSalesLine', '', false, false)]
    local procedure CU5702_OnBeforeFindItemReferenceForSalesLine(SalesLine: Record "Sales Line"; var ItemReference: Record "Item Reference"; var Found: Boolean; var IsHandled: Boolean)
    var
        GlobalItemReference: Record "Item Reference";
        ToDate: Date;
    begin
        GlobalItemReference.Reset();
        GlobalItemReference.SetRange("Item No.", SalesLine."No.");
        GlobalItemReference.SetRange("Variant Code", SalesLine."Variant Code");
        GlobalItemReference.SetRange("Unit of Measure", SalesLine."Unit of Measure Code");
        ToDate := SalesLine.GetDateForCalculations();
        if ToDate <> 0D then begin
            GlobalItemReference.SetFilter("Starting Date", '<=%1', ToDate);
            GlobalItemReference.SetFilter("Ending Date", '>=%1|%2', ToDate, 0D);
        end;
        GlobalItemReference.SetRange("Reference Type", SalesLine."Item Reference Type"::Customer);
        // AD Le 22-09-2009 => On prend les donn‚es du client factur‚
        // GlobalItemReference.SetRange("Reference Type No.", SalesLine."Sell-to Customer No.");
        GlobalItemReference.SetRange("Reference Type No.", SalesLine."Bill-to Customer No.");
        // FIN AD Le 22-09-2009
        GlobalItemReference.SetRange("Reference No.", SalesLine."Item Reference No.");
        if GlobalItemReference.FindFirst() then
            Found := true
        else begin
            GlobalItemReference.SetRange("Reference No.");
            Found := GlobalItemReference.FindFirst();
        end;

        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Reference Management", 'OnAfterSalesItemReferenceFound', '', false, false)]
    local procedure CU5702_OnAfterSalesItemReferenceFound(var SalesLine: Record "Sales Line"; ItemReference: Record "Item Reference")
    begin
        IF ItemReference.Description <> '' THEN BEGIN
            //MC Le 13/08/10 -> Depassement de capacit‚ puisque description de la table ItemCross = 60 car alors que sales line 50 car
            IF STRLEN(ItemReference.Description) <= MAXSTRLEN(SalesLine.Description) THEN BEGIN
                SalesLine.Description := ItemReference.Description;
                SalesLine."Description 2" := '';
            END
            ELSE BEGIN
                SalesLine.Description := COPYSTR(ItemReference.Description, 1, MAXSTRLEN(SalesLine.Description));
                SalesLine."Description 2" := COPYSTR(ItemReference.Description, MAXSTRLEN(SalesLine.Description),
                                   STRLEN(ItemReference.Description));
            END;
            //FIN MC
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Reference Management", 'OnAfterPurchItemItemRefNotFound', '', false, false)]
    local procedure CU5702_OnAfterPurchItemItemRefNotFound(var PurchaseLine: Record "Purchase Line"; var ItemVariant: Record "Item Variant")
    var
        LItemVendor: Record "Item Vendor";
        CodeunitsFunctions: Codeunit "Codeunits Functions";
    begin
        WITH PurchaseLine DO
            IF LItemVendor.GET("Buy-from Vendor No.", "No.", "Variant Code") THEN BEGIN
                "Item Reference No." := LItemVendor."Vendor Item No.";
                "Item Reference Unit of Measure" := LItemVendor."Purch. Unit of Measure";
                "Item Reference Type" := "Item Reference Type"::Vendor;
                "Item Reference Type No." := "Buy-from Vendor No.";
                IF LItemVendor."Vendor Item Desciption" <> '' THEN BEGIN
                    Description := LItemVendor."Vendor Item Desciption";
                    "Description 2" := '';
                END;
            END
            ELSE BEGIN
                PurchaseLine."Item Reference No." := '';
                PurchaseLine."Item Reference Type" := PurchaseLine."Item Reference Type"::" ";
                PurchaseLine."Item Reference Type No." := '';
                CodeunitsFunctions.FillDescription(PurchaseLine);
                PurchaseLine.GetItemTranslation();
            end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Item Reference Management", 'OnBeforeCreateItemReference', '', false, false)]
    local procedure CU5702_OnBeforeCreateItemReference(ItemVendor: Record "Item Vendor"; var IsHandled: Boolean);
    var
    begin
        // AD Le 24-11-2011 => Pas de synchro pour SYMTA
        IsHandled := true;
        // FIN AD Le 24-11-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnBeforeDeleteItemReference', '', false, false)]
    local procedure CU5702_OnBeforeDeleteItemReference(var ItemVendor: Record "Item Vendor"; xItemVendor: Record "Item Vendor"; var IsHandled: Boolean)
    var
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnBeforeUpdateItemReference', '', false, false)]
    local procedure CU5702_OnBeforeUpdateItemReference(var ItemVendor: Record "Item Vendor"; var IsHandled: Boolean; var xItemVendor: Record "Item Vendor")
    var
    begin
        IsHandled := true;
    end;

    var
        CduGSingleInstance: Codeunit SingleInstance;

}