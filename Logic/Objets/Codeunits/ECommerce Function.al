codeunit 50040 "ECommerce Function"
{// CFR Le 17/05/2021 => SFD20210423 : Commentaire article web
 // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
    trigger OnRun()
    begin

    end;

    var
        Error_002: label 'Article [%1] inconnu';
        Text000: label 'Aucune famille trouvée.';
        Text001: label 'Plusieurs familles avec ce code.';
        Text002: label 'Aucun critère trouvé.';

    PROCEDURE GetPrice(pCustomerNo: Code[20]; pItemNo: Code[20]; pQuantity: Decimal; pDate: Date; pTypeCommande: option Dépannage,Réappro,Stock; VAR pMsg: Text[250]; VAR pRemittance1: Decimal; VAR pRemittance2: Decimal; VAR pPrice: Decimal; VAR pNetPrice: Decimal);
    VAR

         rec_Item: Record Item;
        rec_Customer: Record Customer;
        TempSalesPrice: Record "Sales Price" TEMPORARY;
        TempSalesLineDisc: Record "Sales Line Discount" TEMPORARY;
      /*   rec_Kit: Record "Production BOM Line";
        rec_ItemKit: Record Item; */
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        decDiscount: Decimal;
        decPrice: Decimal;
        decNetPrice: Decimal;
        //  Discount: Decimal;
    BEGIN
        //Initialisation des retours
        pRemittance1 := 0;
        pRemittance2 := 0;
        decPrice := 0;
        decNetPrice := 0;
        pMsg := '';

        //R‚cupŠre l'article
        IF NOT rec_Item.GET(pItemNo) THEN BEGIN
            pMsg := STRSUBSTNO(Error_002, pItemNo);
            EXIT;
        END;


        //R‚cupŠre la remise pour l'article s‚lectionn‚
        decDiscount :=
                              LCodeunitsFunctions.GetSalesDisc(
                               TempSalesLineDisc, '', '', '',
                               '', rec_Item."No.", '', rec_Item."Base Unit of Measure", '', WORKDATE(), FALSE, pQuantity, 0,
                               '', '', pTypeCommande); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        decPrice := rec_Item."Unit Price";



        //R‚cupŠre le client
        IF rec_Customer.GET(pCustomerNo) THEN BEGIN

            // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
            rec_Customer.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
            rec_Customer.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
            rec_Customer.CALCFIELDS("Centrale Active");
            // FIN CFR le 10/03/2021 - SFD20210201 historique centrale active

            //R‚cupŠre la remise pour l'article et pour le client
            decDiscount :=
                         LCodeunitsFunctions.GetSalesDisc(
                         TempSalesLineDisc, rec_Customer."No.", '', rec_Customer."Customer Disc. Group",
                         '', rec_Item."No.", '', rec_Item."Base Unit of Measure", '', WORKDATE(), FALSE, pQuantity, 0,
                         rec_Customer."Sous Groupe Tarifs", rec_Customer."Centrale Active",
                         pTypeCommande); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.


            //Recherche si des prix tarif existe => Sinon on prend le prix de la fiche.
            IF NOT LCodeunitsFunctions.GetUnitPrice(
                        TempSalesPrice, rec_Customer."No.", '', rec_Customer."Customer Price Group",
                        '', rec_Item."No.", '', rec_Item."Base Unit of Measure", '', WORKDATE(), FALSE, pQuantity, decDiscount,
                        decPrice, rec_Customer."Sous Groupe Tarifs", rec_Customer."Centrale Active", pTypeCommande)

                THEN 
                decPrice := rec_Item."Unit Price";
                END;



        //Retour du prix Net
        // Calcul du net et total
        IF decDiscount <> 0 THEN
            decNetPrice := ROUND(decPrice - (decPrice * decDiscount) / 100, 0.01)
        ELSE
            decNetPrice := ROUND(decPrice, 0.01);

        pRemittance1 := decDiscount;
        pPrice := decPrice;
        pNetPrice := decNetPrice;


        //Ancien Code mis en commentaire a cause du COMMIT
        /*{
        //Initialisation des retours
        pRemittance1 := 0;
        pRemittance2 := 0;
        decPrice    := 0;
        decNetPrice := 0;
        pMsg        := '';

        //R‚cupŠre l'article
        IF NOT rec_Item.GET(pItemNo) THEN
            BEGIN
                pMsg := STRSUBSTNO(Error_002, pItemNo);
                EXIT;
            END;



        rec_TempSalesHeader.INIT();
        rec_TempSalesHeader.RESET();
        rec_TempSalesHeader.VALIDATE("Document Type", rec_TempSalesHeader."Document Type"::Quote);
        rec_TempSalesHeader."No." := 'TOTO';
        rec_TempSalesHeader.VALIDATE("Posting Date", pDate);
        rec_TempSalesHeader.VALIDATE("Sell-to Customer No.", pCustomerNo);
        rec_TempSalesHeader.INSERT(TRUE);

        rec_TempSalesLine.INIT();
        rec_TempSalesLine.RESET();
        rec_TempSalesLine.VALIDATE("Document Type"       , rec_TempSalesHeader."Document Type");
        rec_TempSalesLine.VALIDATE("Document No."        , rec_TempSalesHeader."No.");
        rec_TempSalesLine.VALIDATE("Line No."            , 10000);

        rec_TempSalesLine.VALIDATE("Sell-to Customer No.", pCustomerNo);
        rec_TempSalesLine.VALIDATE(Type   , rec_TempSalesLine.Type::Item);
        rec_TempSalesLine.VALIDATE("No."  , pItemNo);
        rec_TempSalesLine.VALIDATE(Quantity, pQuantity);


        pPrice       := rec_TempSalesLine."Unit Price";
        pRemittance1 := rec_TempSalesLine."Discount1 %";
        pRemittance2 := rec_TempSalesLine."Discount1 %";
        Discount := rec_TempSalesLine."Line Discount %";
        rec_TempSalesHeader.DELETE(TRUE);

        //Retour du prix Net
        IF Discount <> 0 THEN
            BEGIN
                pNetPrice := ROUND(pPrice - (pPrice * Discount) / 100, 0.01);
            END
        ELSE
            BEGIN
                pNetPrice := ROUND(pPrice, 0.01);
            END;
        COMMIT;
        }*/
    END;

    PROCEDURE GetMarketingTranslate(pType: Code[20]; pCode: Code[20]; VAR pFrValue: Text[50]; VAR pEnValue: Text[50]);
    VAR
        recL_Family: Record "Famille Marketing";
        recL_Criteria: Record Critere;
        RecordRef: RecordRef;
    BEGIN

        CASE pType OF
            'FAMILLE':
                BEGIN

                    recL_Family.SETRANGE(Code, pCode);
                    // Detection des erreurs
                    IF recL_Family.ISEMPTY THEN
                        ERROR(Text000);
                    IF recL_Family.COUNT > 1 THEN
                        ERROR(Text001);
                    // R‚cup‚ration du record
                    recL_Family.FINDFIRST();
                    pFrValue := recL_Family.Description;
                    // R‚cup‚ration de la traduction anglaise
                    RecordRef.GETTABLE(recL_Family);
                    pEnValue := GetTranslation(RecordRef, 'ENU', recL_Family.FIELDNO(Description));
                END;
            'CRITERE':
                BEGIN
                    recL_Criteria.GET(pCode);
                    pFrValue := recL_Criteria.Nom;
                    // R‚cup‚ration de la traduction anglaise
                    RecordRef.GETTABLE(recL_Criteria);
                    pEnValue := GetTranslation(RecordRef, 'ENU', recL_Criteria.FIELDNO(Nom));
                END;
        END;

        ;
    END;

    LOCAL PROCEDURE GetTranslation(pRecordRef: RecordRef; pLanguageCode: Code[20]; pFieldNo: Integer): Text;
    VAR
        recL_Translation: Record "Translation SYMTA";
    BEGIN
        recL_Translation.RESET();
        recL_Translation.SETRANGE(Record_ID, pRecordRef.RECORDID);
        recL_Translation.SETRANGE("Table No", pRecordRef.NUMBER);
        recL_Translation.SETRANGE("Language Code", 'ENU');
        recL_Translation.SETRANGE("Champ table", pFieldNo);
        IF recL_Translation.FINDFIRST() THEN
            EXIT(recL_Translation.Description);
    END;

    PROCEDURE GetItemWebComment(pItemNo: Code[20]; VAR pFRValue: Text; VAR pENValue: Text);
    VAR
        lItem: Record Item;
        lCommentLine: Record "Comment Line";
    BEGIN
        // CFR Le 17/05/2021 => SFD20210423 : Commentaire article web
        pFRValue := '';
        pENValue := '';

        IF lItem.GET(pItemNo) THEN BEGIN
            lCommentLine.SETRANGE("Table Name", lCommentLine."Table Name"::Item);
            lCommentLine.SETRANGE("No.", lItem."No.");
            lCommentLine.SETFILTER("Language Web", '<>%1', lCommentLine."Language Web"::" ");
            IF lCommentLine.FINDSET() THEN
                REPEAT
                    CASE lCommentLine."Language Web" OF
                        lCommentLine."Language Web"::FR:
                            BEGIN
                                IF (pFRValue = '') THEN
                                    pFRValue := lCommentLine.Comment
                                ELSE
                                    pFRValue := pFRValue + ' ' + lCommentLine.Comment;
                            END;
                        lCommentLine."Language Web"::EN:
                            BEGIN
                                IF (pENValue = '') THEN
                                    pENValue := lCommentLine.Comment
                                ELSE
                                    pENValue := pENValue + ' ' + lCommentLine.Comment;
                            END;
                    END;
                UNTIL lCommentLine.NEXT() = 0;
        END;
    END;

}