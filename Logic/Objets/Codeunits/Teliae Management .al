codeunit 50044 "Teliae Management"
{// CFR 27/04/2020 - FE20190930
    trigger OnRun()
    begin
        MESSAGE(FORMAT(CURRENTDATETIME, 0, '<Day,2><Month,2><Year>-<Hours24,2><Minutes,2><Seconds,2>'));
        MESSAGE(FORMAT(CURRENTDATETIME, 0, 3));
    end;

    var
     gSalesReceivablesSetup: Record "Sales & Receivables Setup";

        gFileTeliae: File;
        gFileName: Text[250];
        gFileSeparator: Code[1];
        confirm01Msg: label 'Do you really want to delete %1 selected labels ?', comment = 'Souhaitez vous réellement supprimer les %1 étiquettes sélectionnées ?';
        confirm02Msg: label 'Do you want to generate end of dayn %1 ?', comment = 'Souhaitez vous générer la fin de journée %1 = Sho Lab?';

    PROCEDURE DeleteLabel(VAR pShippingLabel: Record "Shipping Label");
    VAR
        lFileLine: Text[1024];
    BEGIN

        IF (pShippingLabel.COUNT() = 0) THEN
            EXIT;

        IF NOT CONFIRM(STRSUBSTNO(confirm01Msg, pShippingLabel.COUNT())) THEN
            EXIT;

        InitGlobalFile('SUP');

        // Ecriture ligne [PARAM]
        //=======================
        lFileLine := 'PARAM';
        lFileLine += gFileSeparator + 'INTEGRATION=OUI';
        lFileLine += gFileSeparator + 'ETIQ=NON';
        lFileLine += gFileSeparator + 'MAIL_ERR=' + gSalesReceivablesSetup."Teliae Error Mail";
        lFileLine += gFileSeparator + 'SUPPR=TOUT';
        gFileTeliae.WRITE(lFileLine);

        // Ecriture ligne [COM] Suppression
        //=================================
        IF pShippingLabel.FINDSET() THEN
            REPEAT
                lFileLine := 'COM';
                lFileLine += gFileSeparator + 'SUP';
                lFileLine += gFileSeparator + pShippingLabel."BL No.";
                gFileTeliae.WRITE(lFileLine);

                // Mise … jour de l'enregistrement
                pShippingLabel."Teliae Deleted" := TRUE;
                pShippingLabel."Teliae Deleting Date" := TODAY();
                pShippingLabel."Teliae Deleting Time" := TIME();
                pShippingLabel."Teliae Deleting User" := USERID();
                pShippingLabel.MODIFY();

            UNTIL pShippingLabel.NEXT() = 0;

        // ## Lib‚ration du fichier
        //=========================
        gFileTeliae.CLOSE();
    END;

    PROCEDURE EndOfDay(VAR pOnlyCurrentDay: Boolean);
    VAR
        lShippingLabel: Record "Shipping Label";
        lFileLine: Text[1024];
    BEGIN

        lShippingLabel.SETRANGE("Teliae End Of Day", FALSE);
        IF (pOnlyCurrentDay) THEN
            lShippingLabel.SETRANGE("Date of  bl", TODAY());

        IF (lShippingLabel.COUNT() = 0) THEN
            EXIT;

        IF NOT CONFIRM(STRSUBSTNO(confirm02Msg, lShippingLabel.COUNT())) THEN
            EXIT;

        InitGlobalFile('FDJ');

        // Ecriture ligne [PARAM]
        //=======================
        lFileLine := 'PARAM';
        lFileLine += gFileSeparator + 'INTEGRATION=OUI';
        lFileLine += gFileSeparator + 'ETIQ=NON';
        lFileLine += gFileSeparator + 'MAIL_ERR=' + gSalesReceivablesSetup."Teliae Error Mail";
        lFileLine += gFileSeparator + 'IMPR_FDJ=OUI';
        lFileLine += gFileSeparator + 'FDJ=OUI';
        gFileTeliae.WRITE(lFileLine);

        // Ecriture ligne [COM] fin de journ‚e
        //=================================
        IF lShippingLabel.FINDSET() THEN
            REPEAT
                lFileLine := 'COM';
                lFileLine += gFileSeparator + 'FDJ';
                lFileLine += gFileSeparator + lShippingLabel."BL No.";
                gFileTeliae.WRITE(lFileLine);

                // Mise … jour de l'enregistrement
                lShippingLabel."Teliae End Of Day" := TRUE;
                lShippingLabel.MODIFY();

            UNTIL lShippingLabel.NEXT() = 0;

        // ## Lib‚ration du fichier
        //=========================
        gFileTeliae.CLOSE();
    END;

    PROCEDURE ImportReturn(): Integer;
    VAR
        lNameValueBuffer: Record "Name/Value Buffer";
        lTeliaeStandardReturn: Record "Teliae Standard Return";
        lFileManagement: Codeunit "File Management";
        lNbEnregistrement: Integer;
        j: Integer;
        
        
        lDirectoryName: Text[250];
        lEntryNo: Integer;
        lTabEnregistrement: ARRAY[400, 50] OF Text[1024];
        lImportNo: Integer;
    BEGIN

        IF NOT gSalesReceivablesSetup.GET() THEN EXIT(0);

        IF gSalesReceivablesSetup."Teliae Return Folder" = '' THEN BEGIN
            MESSAGE('Paramétrage fichier retour manquant');
            EXIT(0);
        END;

        // Recherche du fichier Retour
        //============================
        lDirectoryName := gSalesReceivablesSetup."Teliae Return Folder";
        lFileManagement.GetServerDirectoryFilesList(lNameValueBuffer, lDirectoryName);

        IF (lNameValueBuffer.FINDSET()) THEN
            REPEAT
                // Lecture du fichier Retour
                //===========================
                CLEAR(lTabEnregistrement);
                LoadFile(lNameValueBuffer.Name, lTabEnregistrement, lNbEnregistrement);

                // Import du fichier Retour
                //=========================
                IF lTeliaeStandardReturn.FINDLAST() THEN
                    lImportNo := lTeliaeStandardReturn."Import No." + 1
                ELSE
                    lImportNo := 1;
                FOR j := 1 TO lNbEnregistrement DO BEGIN
                    IF lTeliaeStandardReturn.FINDLAST() THEN
                        lEntryNo := lTeliaeStandardReturn."Entry No." + 1
                    ELSE
                        lEntryNo := 1;
                    lTeliaeStandardReturn.INIT();
                    lTeliaeStandardReturn."Entry No." := lEntryNo;
                    lTeliaeStandardReturn."Import No." := lImportNo;
                    lTeliaeStandardReturn."Import Date" := TODAY();
                    EVALUATE(lTeliaeStandardReturn."Reference UM", lTabEnregistrement[j] [1]);
                    EVALUATE(lTeliaeStandardReturn."Info Colis UM", lTabEnregistrement[j] [2]);
                    EVALUATE(lTeliaeStandardReturn."Numero UM", lTabEnregistrement[j] [3]);
                    EVALUATE(lTeliaeStandardReturn."Poids UM", lTabEnregistrement[j] [4]);
                    EVALUATE(lTeliaeStandardReturn."URL Tracking UM", lTabEnregistrement[j] [5]);
                    EVALUATE(lTeliaeStandardReturn."Code Barre de tri (UM)", lTabEnregistrement[j] [6]);
                    EVALUATE(lTeliaeStandardReturn."Code Barre Client", lTabEnregistrement[j] [7]);
                    EVALUATE(lTeliaeStandardReturn."Code Barre Transporteur", lTabEnregistrement[j] [8]);
                    EVALUATE(lTeliaeStandardReturn."Numero Recepisse", lTabEnregistrement[j] [9]);
                    EVALUATE(lTeliaeStandardReturn."Numero BL", lTabEnregistrement[j] [10]);
                    EVALUATE(lTeliaeStandardReturn."URL Tracking Expedition", lTabEnregistrement[j] [11]);
                    EVALUATE(lTeliaeStandardReturn."Nom Expédition", lTabEnregistrement[j] [12]);
                    EVALUATE(lTeliaeStandardReturn."Code Transporteur", lTabEnregistrement[j] [13]);
                    EVALUATE(lTeliaeStandardReturn."Code Produit", lTabEnregistrement[j] [14]);
                    lTeliaeStandardReturn."Date Expedition" := EvaluateDate(lTabEnregistrement[j] [15]);
                    EVALUATE(lTeliaeStandardReturn."Numero Bordereau", lTabEnregistrement[j] [16]);
                    lTeliaeStandardReturn."Date creation Bordereau" := EvaluateDateTime(lTabEnregistrement[j] [17]);
                    EVALUATE(lTeliaeStandardReturn."Nom Compte Chargeur", lTabEnregistrement[j] [18]);
                    EVALUATE(lTeliaeStandardReturn."Reference Destinataire", lTabEnregistrement[j] [19]);
                    EVALUATE(lTeliaeStandardReturn."Code Tournee", lTabEnregistrement[j] [20]);
                    EVALUATE(lTeliaeStandardReturn."Code Preparateur", lTabEnregistrement[j] [21]);
                    lTeliaeStandardReturn.Insert();
                END;

                // Sauvegarde du fichier Retour
                //=============================
                PostingFile(lNameValueBuffer.Name);

            UNTIL lNameValueBuffer.NEXT() = 0;

        EXIT(lNameValueBuffer.COUNT());
    END;

    LOCAL PROCEDURE InitGlobalFile(pCode: Code[10]);
    VAR
        lHorodatage: Text[30];
    BEGIN

        IF NOT gSalesReceivablesSetup.GET() THEN EXIT;
        gFileSeparator := ';';
        lHorodatage := DELCHR(FORMAT(CURRENTDATETIME, 0, 3), '=', '-/:');

        //IF UPPERCASE(USERID) = 'ESKAPE' THEN
        //  MonFichierEtq := STRSUBSTNO('%1\%2.csv','c:\tmp\',
        //  "Shipping Label"."Entry No.")
        //ELSE
        gFileName := STRSUBSTNO('%1\%2.csv',
                            gSalesReceivablesSetup."Teliae Export Folder",
                            DELCHR(USERID, '=', '\') + '_' + pCode + '_' + lHorodatage);

        // Cr‚ation du fichier
        //====================
        gFileTeliae.CREATE(gFileName);
        gFileTeliae.TEXTMODE(TRUE);
    END;

    LOCAL PROCEDURE LoadFile(pFileName: Text[1024]; VAR pTabEnregistrement: ARRAY[400, 50] OF Text[1024]; VAR pNbEnregistrement: Integer);
    VAR
    lFileLine: BigText;
        lFile: File;
        lStream: InStream;
        
        i: Integer;
        lChamp: Integer;
        lChar: Text[1];
        
        lChar13: Char;
        lChar10: Char;
        lSeparator: Text[1];
        lNewLine: Boolean;
    BEGIN
        lSeparator := ';';
        lChar13 := 13;
        lChar10 := 10;
        lChamp := 1;
        lNewLine := FALSE;

        pNbEnregistrement := 0;
        CLEAR(pTabEnregistrement);

        lFile.TEXTMODE(TRUE);
        lFile.OPEN(pFileName);
        lFile.CREATEINSTREAM(lStream);
        lFileLine.READ(lStream);

        IF lFileLine.LENGTH <> 0 THEN BEGIN
            pNbEnregistrement := 1;
            FOR i := 1 TO lFileLine.LENGTH DO BEGIN
                lFileLine.GETSUBTEXT(lChar, i, 1);
                IF (lChar <> FORMAT(lChar13)) AND (lChar <> FORMAT(lChar10)) THEN BEGIN
                    lNewLine := FALSE;
                    IF lChar = lSeparator THEN
                        lChamp += 1
                    ELSE
                        pTabEnregistrement[pNbEnregistrement] [lChamp] += lChar
                END
                ELSE
                    IF (lChar = FORMAT(lChar13)) THEN BEGIN
                        lChamp := 1;
                        pNbEnregistrement += 1;
                        lNewLine := TRUE;
                    END;
            END;
        END;

        IF (lNewLine) THEN pNbEnregistrement -= 1;
    END;

    PROCEDURE PostingFile(pFileName: Text[1024]);
    VAR
    lFileManagement: Codeunit "File Management";
        lHorodatage: Text[50];
        lFileNameNew: Text[1024];
        
    BEGIN

        lHorodatage := DELCHR(FORMAT(CURRENTDATETIME, 0, 3), '=', '-/:');

        IF EXISTS(pFileName) THEN BEGIN
            lFileNameNew := lFileManagement.GetDirectoryName(pFileName) + '\SAVE\';
            IF NOT lFileManagement.ServerDirectoryExists(lFileNameNew) THEN
                lFileManagement.ServerCreateDirectory(lFileNameNew);
            lFileNameNew += lFileManagement.GetFileNameWithoutExtension(pFileName);
            lFileNameNew += '-' + lHorodatage + '.' + lFileManagement.GetExtension(pFileName);
            file.Copy(pFileName, lFileNameNew);
            File.Erase(pFileName);
        END;
    END;

    LOCAL PROCEDURE EvaluateDate(pText: Text[8]): Date;
    VAR
        lYear: Integer;
        lMonth: Integer;
        lDay: Integer;
    BEGIN
        IF NOT EVALUATE(lMonth, COPYSTR(pText, 1, 2)) THEN EXIT(0D);
        IF NOT EVALUATE(lDay, COPYSTR(pText, 3, 2)) THEN EXIT(0D);
        IF NOT EVALUATE(lYear, COPYSTR(pText, 5, 4)) THEN EXIT(0D);

        EXIT(DMY2DATE(lDay, lMonth, lYear));
    END;

    LOCAL PROCEDURE EvaluateDateTime(pText: Text[14]): DateTime;
    VAR
        lYear: Integer;
        lMonth: Integer;
        lDay: Integer;
        lTime: Time;
    BEGIN
        IF NOT EVALUATE(lMonth, COPYSTR(pText, 1, 2)) THEN EXIT(0DT);
        IF NOT EVALUATE(lDay, COPYSTR(pText, 3, 2)) THEN EXIT(0DT);
        IF NOT EVALUATE(lYear, COPYSTR(pText, 5, 4)) THEN EXIT(0DT);

        IF NOT EVALUATE(lTime, COPYSTR(pText, 9, 6)) THEN EXIT(CREATEDATETIME(DMY2DATE(lDay, lMonth, lYear), 0T));

        EXIT(CREATEDATETIME(DMY2DATE(lDay, lMonth, lYear), lTime));
    END;
}