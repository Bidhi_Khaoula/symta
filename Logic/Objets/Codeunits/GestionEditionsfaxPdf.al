codeunit 50099 "Gestion Editions / fax / Pdf"
{

    trigger OnRun()
    begin
    end;

    var
        Text001Err: Label 'Type Gestion Fax Inconnu !';

    procedure "GetInfoFax-Pdf"(Type: Text[10]; Document_No: Code[20]; var No_fax: Text[30]; var Objet_fax: Text[250]; var Attention_fax: Text[50]; var Nom_pdf: Text[30])
    var
        Rec_SalesHeader: Record "Sales Header";
        Rec_SalesInvoiceHeader: Record "Sales Invoice Header";
        Rec_SalesCrMemoHeader: Record "Sales Cr.Memo Header";
        Rec_SalesShipmentHeader: Record "Sales Shipment Header";
        Rec_PurchaseHeader: Record "Purchase Header";
        Rec_Client: Record Customer;
        Rec_ServiceHeader: Record "Service Header";
        No_client: Code[20];
    begin

        //ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order
        //FRA=Devis,Commande,Facture,Avoir,Commande ouverte,Retour

        CASE Type OF
            'DEVIS':
                BEGIN
                    Rec_SalesHeader.GET(Rec_SalesHeader."Document Type"::Quote, Document_No);
                    No_client := Rec_SalesHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETDEVIS : '
                                     + Rec_SalesHeader."No."
                                     + '-' + Rec_SalesHeader."Sell-to Customer Name" + '-'
                                     + Rec_SalesHeader."External Document No." + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'DEV-' + Rec_SalesHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'COMMANDE':
                BEGIN
                    Rec_SalesHeader.GET(Rec_SalesHeader."Document Type"::Order, Document_No);
                    No_client := Rec_SalesHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETAR : '
                                     + Rec_SalesHeader."No."
                                     + '-' + Rec_SalesHeader."Sell-to Customer Name" + '-'
                                     + Rec_SalesHeader."External Document No." + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'AR-' + Rec_SalesHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;

                END;
            'BL':
                BEGIN
                    Rec_SalesShipmentHeader.GET(Document_No);
                    No_client := Rec_SalesShipmentHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETBL : '
                                     + Rec_SalesShipmentHeader."No."
                                     + '-' + Rec_SalesShipmentHeader."Sell-to Customer Name" + '-'
                                     + Rec_SalesShipmentHeader."External Document No." + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'BL-' + Rec_SalesShipmentHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'FACTURE':
                BEGIN
                    Rec_SalesInvoiceHeader.GET(Document_No);
                    No_client := Rec_SalesInvoiceHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETFAC : '
                                     + Rec_SalesInvoiceHeader."No."
                                     + '-' + Rec_SalesInvoiceHeader."Sell-to Customer Name" + '-'
                                     + Rec_SalesInvoiceHeader."External Document No." + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'FAC-' + Rec_SalesInvoiceHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'AVOIR':
                BEGIN
                    Rec_SalesCrMemoHeader.GET(Document_No);
                    No_client := Rec_SalesHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETFAC : '
                                     + Rec_SalesCrMemoHeader."No."
                                     + '-' + Rec_SalesCrMemoHeader."Sell-to Customer Name" + '-'
                                     + Rec_SalesCrMemoHeader."External Document No." + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'AVO-' + Rec_SalesCrMemoHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'COMMANDE ACHAT':
                BEGIN
                    Rec_PurchaseHeader.GET(Rec_PurchaseHeader."Document Type"::Order, Document_No);
                    No_client := Rec_PurchaseHeader."Sell-to Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETCDE FRN : '
                                     + Rec_PurchaseHeader."No."
                                     + '-' + Rec_PurchaseHeader."Buy-from Vendor Name" + '-' + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'CACH-' + Rec_PurchaseHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'DEVISSAV':
                BEGIN
                    Rec_ServiceHeader.GET(Rec_ServiceHeader."Document Type"::Quote, Document_No);
                    No_client := Rec_ServiceHeader."Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETDEVISSAV : '
                                     + Rec_ServiceHeader."No."
                                     + '-' + Rec_ServiceHeader.Name + '-'
                                     //+ Rec_SalesHeader."External Document No."
                                     + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'DEVSAV-' + Rec_ServiceHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;
            'CDESAV':
                BEGIN
                    Rec_ServiceHeader.GET(Rec_ServiceHeader."Document Type"::Order, Document_No);
                    No_client := Rec_ServiceHeader."Customer No.";
                    IF Objet_fax = '' THEN
                        Objet_fax := '##OBJETCDESAV : '
                                     + Rec_ServiceHeader."No."
                                     + '-' + Rec_ServiceHeader.Name + '-'
                                     //+ Rec_SalesHeader."External Document No."
                                     + '##';
                    IF Nom_pdf = '' THEN BEGIN
                        Nom_pdf := 'CDESAV-' + Rec_ServiceHeader."No.";
                        Nom_pdf := '##FICHIER' + Nom_pdf + '##';
                    END;
                END;

            ELSE
                ERROR(Text001Err);
        END;

        IF No_fax = '' THEN BEGIN
            IF Rec_Client.GET(No_client) THEN
                No_fax := Rec_Client."Fax No.";
        END;

        No_fax := DELCHR(FORMAT(No_fax), '=', '-');
        No_fax := DELCHR(FORMAT(No_fax), '=', '.');
        No_fax := '##FAX' + DELCHR(FORMAT(No_fax)) + '##';


        // AD Le 23-01-2007 => Demande de virginie
        IF Attention_fax <> '' THEN
            Attention_fax := 'A l''attention de : ' + Attention_fax;
    end;
}

