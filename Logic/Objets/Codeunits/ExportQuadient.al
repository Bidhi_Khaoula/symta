// Codeunit 50076 "Export Quadient"
// {
//     Trigger OnRun()
//     BEGIN
//         IF GUIALLOWED THEN gDialog.OPEN('#1###############');

//         ExportSociete();
//         ExportCodesTVA();
//         ExportCompteGeneraux();
//         ExportConditionsPaiements();
//         ExportDevises();
//         ExportVendor();
//         ExportVendorBankAccount();

//         ExportOrders();
//         ExportOrderLines();
//         ExportReceiptLines();

//         IF GUIALLOWED THEN BEGIN
//             gDialog.CLOSE();
//             MESSAGE('Export termin‚');
//         END;
//     END;

//     VAR
//         gLine: Text[1024];
//         PurchSetup: Record 312;
//         gDialog: Dialog;

//     LOCAL PROCEDURE ExportOrders();
//     VAR
//         PurchOrder: Record 38;
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//     BEGIN

//         filepath := Init('PurchaseorderHeaders');

//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);

//         Headers := 'CompanyCode__,VendorNumber__,DifferentInvoicingParty__,OrderNumber__,OrderDate__,OrderedAmount__,DeliveredAmount__,InvoicedAmount__,Currency__,Buyer__,Receiver__,IsLocalPO__,IsCreatedInERP__,NoMoreInvoiceExpected__';
//         StreamWriter.WriteLine(Headers);

//         // on cherche les commandes achat de moins de 3 ans
//         PurchOrder.SETFILTER("Order Date", '%1..', CALCDATE('<-3Y>'));
//         PurchOrder.SETRANGE("Document Type", PurchOrder."Document Type"::Order);
//         PurchOrder.FINDSET;
//         REPEAT
//             ExportOrder(PurchOrder);
//             StreamWriter.WriteLine(gLine);
//         UNTIL PurchOrder.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportOrder(pPurchHeader: Record 38);
//     VAR
//         PurchLine: Record 39;
//         Invoiced: Boolean;
//         AmountReceived: Decimal;
//         AmountInvoiced: Decimal;
//         lAmount: Decimal;
//     BEGIN
//         Invoiced := TRUE; // // NoMoreInvoiceExpected
//         PurchLine.SETRANGE("Document Type", PurchLine."Document Type"::Order);
//         PurchLine.SETRANGE("Document No.", pPurchHeader."No.");
//         IF PurchLine.FINDSET() THEN
//             REPEAT
//                 IF PurchLine."Quantity Invoiced" <> PurchLine.Quantity THEN Invoiced := FALSE;
//                 lAmount += PurchLine.Amount;
//                 AmountInvoiced += CalcAmount(PurchLine.Amount, PurchLine.Quantity, PurchLine."Quantity Invoiced");
//                 AmountReceived += CalcAmount(PurchLine.Amount, PurchLine.Quantity, PurchLine."Quantity Received");
//             UNTIL PurchLine.NEXT() = 0;


//         InitLine();
//         WITH pPurchHeader DO BEGIN
//             Add("Buy-from Vendor No.");
//             Add("Pay-to Vendor No.");
//             Add("No.");
//             AddDate("Order Date");
//             AddDecimal(lAmount);
//             AddDecimal(AmountReceived);
//             AddDecimal(AmountInvoiced);
//             Add("Currency Code");
//             Add('');
//             Add('');
//             AddBool(FALSE);
//             AddBool(TRUE);
//             AddBool(Invoiced);
//         END;
//     END;

//     LOCAL PROCEDURE ExportOrderLines();
//     VAR
//         PurchLine: Record 39;
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//     BEGIN

//         filepath := Init('PurchaseorderItems');

//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);

//         Headers := 'CompanyCode__,VendorNumber__,OrderNumber__,OrderDate__,ItemNumber__,PartNumber__,ItemType__,Description__,';
//         Headers += 'GLAccount__,Group__,CostCenter__,ProjectCode__,FreeDimension1__,FreeDimension1ID__,';
//         Headers += 'BudgetID__,UnitPrice__,OrderedAmount__,UnitOfMeasureCode__,OrderedQuantity__,InvoicedAmount__,InvoicedQuantity__,DeliveredAmount__,DeliveredQuantity__,';
//         Headers += 'Currency__,TaxCode__,TaxRate__,NonDeductibleTaxRate__,Receiver__,CostType__,IsLocalPO__,IsCreatedInERP__,GRIV__,NoMoreInvoiceExpected__,NoGoodsReceipt__';
//         StreamWriter.WriteLine(Headers);

//         // on cherche les lignes achat de moins de 3 ans
//         PurchLine.SETFILTER("Order Date", '%1..%2', CALCDATE('<-3Y>'), 31122099D);
//         PurchLine.SETRANGE("Document Type", PurchLine."Document Type"::Order);

//         PurchLine.FINDSET;
//         REPEAT
//             ExportOrderLine(PurchLine);
//             StreamWriter.WriteLine(gLine);
//         UNTIL PurchLine.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportOrderLine(pPurchLine: Record 39);
//     BEGIN
//         InitLine();
//         WITH pPurchLine DO BEGIN

//             Add("Buy-from Vendor No.");
//             Add("Document No.");
//             AddDate("Order Date");

//             AddDecimal("Line No.");
//             Add("No.");
//             Add('Quantité');
//             Add(Description);

//             Add('');
//             Add('');
//             Add('');
//             Add('');
//             Add("Shortcut Dimension 1 Code");
//             Add("Shortcut Dimension 2 Code");
//             Add('');

//             AddDecimal("Unit Cost");
//             AddDecimal(Amount); // OrderedAmount__
//             Add("Unit of Measure Code");
//             AddDecimal(Quantity); // OrderedQuantity__

//             AddDecimal(CalcAmount(Amount, Quantity, "Quantity Invoiced")); // InvoicedAmount__
//             AddDecimal("Quantity Invoiced"); // InvoicedQuantity__
//             AddDecimal(CalcAmount(Amount, Quantity, "Quantity Received")); // DeliveredQuantity__
//             AddDecimal("Quantity Received"); // DeliveredQuantity__

//             Add("Currency Code");
//             Add("VAT Prod. Posting Group");
//             AddDecimal("VAT %");
//             Add('0');

//             Add(''); //Receiver__
//             Add(''); //CostType__
//             AddBool(FALSE); // IsLocalPO__
//             AddBool(TRUE); // IsCreatedInERP__
//             AddBool(TRUE); // GRIV__
//             AddBool("Quantity Invoiced" = Quantity); //NoMoreInvoiceExpected
//             AddBool("Quantity Received" = Quantity); // NoGoodsReceipt__
//         END;
//     END;

//     LOCAL PROCEDURE ExportSociete();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         CompanyInfo: Record 79;
//     BEGIN
//         filepath := Init('Companycodes');

//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);

//         Headers := 'CompanyCode__,CompanyName__,SIRET__,PhoneNumber__,FaxNumber__,VATNumber__,ContactEmail__,Currency__,PurchasingOrganization__,PurchasingGroup__,';
//         Headers += 'DeliveryAddressID__,ERP__,DefaultConfiguration__,DefaultExpenseReportType__,DeterminationKeyword__,Sub__,Street__,PostOfficeBox__,City__,PostalCode__,Region__,Country__';
//         StreamWriter.WriteLine(Headers);

//         InitLine();
//         CompanyInfo.GET();
//         WITH CompanyInfo DO BEGIN
//             Add(Name);
//             Add("Registration No.");
//             Add("Phone No.");
//             Add("Fax No.");
//             Add('');
//             Add('');
//             Add('EUR');
//             Add('');
//             Add('');
//             Add('');
//             Add('');
//             Add('Default');
//             Add('');
//             Add('');
//             Add('');
//             Add(Address);
//             Add('');
//             Add(City);
//             Add("Post Code");
//             Add('');
//             Add('FR');
//         END;
//         StreamWriter.WriteLine(gLine);


//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportCodesTVA();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         VAT: Record 324;
//         VATPostingSetup: Record 325;
//     BEGIN
//         filepath := Init('Taxcodes');

//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,TaxCode__,Description__,TaxRate__,NonDeductibleTaxRate__,TaxAccount__,TaxAccountForCollection__,TaxType__,TaxClassification__,TaxRoundingPriority__';
//         StreamWriter.WriteLine(Headers);


//         VAT.FINDSET;
//         REPEAT
//             InitLine();
//             VATPostingSetup.SETRANGE("VAT Prod. Posting Group", VAT.Code);
//             VATPostingSetup.SETRANGE("VAT Bus. Posting Group", 'NATIONAL');
//             IF VATPostingSetup.FINDFIRST () THEN;

//             Add(VAT.Code);
//             Add(VAT.Description);
//             AddDecimal(VATPostingSetup."VAT %");
//             AddDecimal(0);
//             Add(VATPostingSetup."Purchase VAT Account");
//             Add(VATPostingSetup."Sales VAT Account");
//             Add('');
//             Add('');
//             Add('');
//             StreamWriter.WriteLine(gLine);
//         UNTIL VAT.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportReceiptLines();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         RcptLine: Record 7319;
//         RcptHeader: Record 7318;
//         PurchLine: Record 39;
//     BEGIN
//         filepath := Init('GoodsReceiptItems');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,OrderNumber__,ItemNumber__,GoodsReceipt__,RequisitionNumber__,Status__,DeliveryNote__,DeliveryDate__,Amount__,Quantity__,ItemUnit__,InvoicedAmount__,InvoicedQuantity__,DeliveryCompleted__,CostType__,BudgetID__';
//         StreamWriter.WriteLine(Headers);

//         RcptHeader.SETFILTER("Posting Date", '%1..', CALCDATE('<-3Y>'));
//         RcptHeader.FINDSET();
//         REPEAT
//             RcptLine.SETRANGE("Source Document", RcptLine."Source Document"::"Purchase Order");
//             RcptLine.SETRANGE("No.", RcptHeader."No.");
//             IF RcptLine.FINDSET() THEN
//                 REPEAT
//                     InitLine();
//                     WITH RcptLine DO BEGIN
//                         IF PurchLine.GET(PurchLine."Document Type"::Order, "Source No.", "Source Line No.") THEN;

//                         Add("Source No.");
//                         AddDecimal("Source Line No.");
//                         Add("No." + '-' + FORMAT("Line No.")); // GRNumber__
//                         Add(''); // RequisitionNumber
//                         Add('Re‡u'); // Status__
//                         Add(RcptHeader."Vendor Shipment No."); // DeliveryNote__
//                         AddDate(RcptHeader."Date Arrivage Marchandise"); // DeliveryDate__
//                         AddDecimal("Total Origine");
//                         AddDecimal(Quantity);
//                         Add("Unit of Measure Code");
//                         AddDecimal(CalcAmount(PurchLine.Amount, PurchLine.Quantity, PurchLine."Quantity Invoiced")); // InvoicedAmount__
//                         AddDecimal(PurchLine."Quantity Invoiced"); // InvoicedQuantity
//                         AddBool(PurchLine."Quantity Received" = PurchLine.Quantity); // DeliveryCompleted
//                         Add(''); // CostType__
//                         Add(''); // BudgetID__
//                     END;
//                     StreamWriter.WriteLine(gLine);
//                 UNTIL RcptLine.NEXT() = 0;
//         UNTIL RcptHeader.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportCompteGeneraux();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         GLAccount: Record 15;
//     BEGIN
//         filepath := Init('GLaccount');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,Account__,Description__,Group__,Allocable1__,Allocable2__,Allocable3__,Allocable4__,Allocable5__,Manager__';
//         StreamWriter.WriteLine(Headers);

//         GLAccount.FINDSET;
//         REPEAT
//             InitLine();
//             WITH GLAccount DO BEGIN
//                 Add("No.");
//                 Add(Name);
//                 Add('');

//                 AddBool(FALSE);
//                 AddBool(FALSE);
//                 AddBool(FALSE);
//                 AddBool(FALSE);
//                 AddBool(FALSE);
//                 Add('');
//             END;
//             StreamWriter.WriteLine(gLine);
//         UNTIL GLAccount.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportDevises();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         Currency: Record 4;
//         CurrencyExchangeRate: Record 330;
//         Rate: Decimal;
//     BEGIN
//         filepath := Init('Currencies');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,CurrencyFrom__,RatioFrom__,RatioTo__,Rate__';
//         StreamWriter.WriteLine(Headers);

//         Currency.FINDSET;
//         REPEAT
//             InitLine();
//             Rate := ROUND(CurrencyExchangeRate.GetCurrentCurrencyFactor(Currency.Code), 0.00001);
//             IF Rate = 0 THEN Rate := 1;

//             Add(Currency.Code);
//             Add('1');
//             Add('1');
//             AddDecimal(Rate);
//             StreamWriter.WriteLine(gLine);
//         UNTIL Currency.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportVendor();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         Vendor: Record 23;
//         lAddress: Text;
//     BEGIN
//         filepath := Init('Vendors');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,Number__,Name__,PhoneNumber__,FaxNumber__,VATNumber__,Siret__,DUNSNumber__,PreferredInvoiceType__,PaymentTermCode__,Email__,GeneralAccount__,TaxSystem__,';
//         Headers += 'Currency__,ParafiscalTax__,SupplierDue__,Sub__,Street__,PostOfficeBox__,City__,PostalCode__,Region__,Country__';
//         StreamWriter.WriteLine(Headers);

//         Vendor.FINDSET;
//         REPEAT
//             InitLine();
//             WITH Vendor DO BEGIN
//                 lAddress := Address;
//                 IF "Address 2" <> '' THEN lAddress += ', ' + "Address 2";
//                 Add("No.");
//                 Add(Name);
//                 Add("Phone No.");
//                 Add("Fax No.");
//                 Add("VAT Registration No.");
//                 Add(SIRET);
//                 Add('');
//                 Add("Payment Terms Code");
//                 Add("E-Mail");
//                 Add('');
//                 Add('');
//                 Add("Currency Code");
//                 Add('');
//                 Add('');

//                 Add('');
//                 Add(lAddress);
//                 Add('');
//                 Add(City);
//                 Add("Post Code");
//                 Add('');
//                 Add("Country/Region Code");
//                 Add('');
//             END;
//             StreamWriter.WriteLine(gLine);
//         UNTIL Vendor.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportVendorBankAccount();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         BankAccount: Record 288;
//     BEGIN
//         filepath := Init('Bankdetails');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,VendorNumber__,BankCountry__,AccountHolder__,IBAN__,BankKey__,BankAccount__,ControlKey__,SWIFT_BICCode__,RoutingCode__,Currency__,BankName__,BankAccountID__';
//         StreamWriter.WriteLine(Headers);

//         BankAccount.FINDSET;
//         REPEAT
//             InitLine();
//             WITH BankAccount DO BEGIN
//                 Add("Vendor No.");
//                 Add("Country/Region Code");
//                 Add('');
//                 Add(IBAN);
//                 Add("Bank Branch No.");
//                 Add("Bank Account No.");
//                 AddDecimal("RIB Key");
//                 Add("SWIFT Code");
//                 Add('');
//                 Add("Currency Code");
//                 Add(Name);
//                 Add('');
//             END;
//             StreamWriter.WriteLine(gLine);
//         UNTIL BankAccount.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE ExportConditionsPaiements();
//     VAR
//         Headers: Text;
//         filepath: Text[1024];
//         StreamWriter: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StreamWriter";
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         PaymentTerms: Record 3;
//         lFormula: Text;
//         lFinMois: Boolean;
//         pos: Integer;
//         PaymentDay: Text;
//         Multiplicateur: Integer;
//         DayLimit: Integer;
//         tab: ARRAY[3] OF Text[10];
//     BEGIN
//         filepath := Init('Paymentterms');
//         Encoding := Encoding.UTF8Encoding(TRUE); // True => with BOM
//         StreamWriter := StreamWriter.StreamWriter(CSVFile.Open(filepath, FileMode.Create), Encoding);
//         Headers := 'CompanyCode__,PaymentTermCode__,Description__,DayLimit__,LatePaymentFeeRate__,ReferenceDate__,PaymentDay__,EndOfMonth__,DiscountPeriod__,EnableDynamicDiscounting__,DiscountRate30Days__,DiscountRate__';
//         StreamWriter.WriteLine(Headers);


//         PaymentTerms.FINDSET;
//         REPEAT
//             InitLine();
//             WITH PaymentTerms DO BEGIN
//                 Multiplicateur := 1;
//                 DayLimit := 0;

//                 // on d‚coupe en tableau 3 valeurs (exemple 2M+FM+10J)
//                 lFormula := FORMAT("Due Date Calculation");
//                 tab[1] := Token(lFormula, '+'); // 2M
//                 tab[2] := Token(lFormula, '+'); // FM
//                 tab[3] := Token(lFormula, '+'); // 10J

//                 // fin de mois (FM)
//                 // jour du mois
//                 IF tab[2] = 'FM' THEN BEGIN
//                     lFinMois := TRUE;
//                     PaymentDay := tab[3];
//                 END ELSE BEGIN
//                     lFinMois := FALSE;
//                     PaymentDay := tab[2]; // par exemple dns le cas 2M+10J
//                 END;
//                 PaymentDay := DELCHR(PaymentDay, '=', 'J');

//                 // jours paiement (2M => 2x30)
//                 lFormula := tab[1];
//                 pos := STRPOS(lFormula, 'M');
//                 IF pos > 0 THEN BEGIN
//                     lFormula := COPYSTR(tab[1], 1, pos - 1);
//                     Multiplicateur := 30;
//                 END;
//                 IF EVALUATE(DayLimit, lFormula) THEN
//                     DayLimit *= Multiplicateur;

//                 Add(Code);
//                 Add(Description);
//                 AddDecimal(DayLimit); // DayLimit__
//                 Add('0');
//                 Add('Invoice Date'); // ReferenceDate__
//                 Add(PaymentDay); // PaymentDay__
//                 AddBool(lFinMois); // EndOfMonth__
//                 Add('');
//                 Add('');
//                 Add('');
//                 Add('');
//             END;
//             StreamWriter.WriteLine(gLine);
//         UNTIL PaymentTerms.NEXT() = 0;

//         StreamWriter.Close();
//     END;

//     LOCAL PROCEDURE Add(pText: Text);
//     BEGIN
//         IF gLine <> '' THEN gLine += ',';
//         pText := CONVERTSTR(pText, '"', '''');
//         gLine += '"' + pText + '"';
//     END;

//     LOCAL PROCEDURE AddDate(pDate: Date);
//     BEGIN
//         Add(FORMAT(pDate, 0, '<Year4>-<Month,2>-<Day,2>'));
//     END;

//     LOCAL PROCEDURE AddDecimal(pValue: Decimal);
//     BEGIN
//         Add(CONVERTSTR(FORMAT(pValue, 0, '<Sign><Integer><Decimals>'), ',', '.'));
//     END;

//     LOCAL PROCEDURE AddBool(pValue: Boolean);
//     BEGIN
//         IF pValue THEN
//             Add('yes')
//         ELSE
//             Add('no');
//     END;

//     LOCAL PROCEDURE Init(filename: Text): Text;
//     VAR
//         Encoding: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.UTF8Encoding";
//         FileMode: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.FileMode";
//         CSVFile: DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
//         filepath: Text;
//         horodatage: Text;
//     BEGIN
//         IF GUIALLOWED THEN gDialog.UPDATE(1, filename);

//         PurchSetup.GET();
//         PurchSetup.TESTFIELD("Quadient Export Path");

//         horodatage := FORMAT(CURRENTDATETIME, 0, '<Year4><Month,2><Day,2><Hour,2><Min,2><Second,2>');
//         filepath := STRSUBSTNO('%1\ERP__%2__%3.csv', PurchSetup."Quadient Export Path", filename, horodatage);
//         IF EXISTS(filepath) THEN ERASE(filepath);

//         EXIT(filepath);
//     END;

//     LOCAL PROCEDURE InitLine();
//     BEGIN
//         gLine := '';
//         Add(COMPANYNAME);
//     END;

//     LOCAL PROCEDURE Token(VAR Text: Text[1024]; Separator: Text[1]) Token: Text[1024];
//     VAR
//         pos: Integer;
//     BEGIN
//         pos := STRPOS(Text, Separator);
//         IF pos > 0 THEN BEGIN
//             Token := COPYSTR(Text, 1, pos - 1);
//             IF pos + 1 <= STRLEN(Text) THEN
//                 Text := COPYSTR(Text, pos + 1)
//             ELSE
//                 Text := '';
//         END ELSE BEGIN
//             Token := Text;
//             Text := '';
//         END;
//     END;

//     LOCAL PROCEDURE CalcAmount(pTotal: Decimal; pQtyTotal: Decimal; pQty: Decimal): Decimal;
//     BEGIN
//         IF pQtyTotal = pQty THEN EXIT(pTotal);
//         IF pQtyTotal = 0 THEN EXIT(0);
//         EXIT(ROUND((pQty / pQtyTotal) * pTotal, 0.01));
//     END;

// }

