codeunit 50007 "Gestion Multi-référence"
{
    trigger OnRun()
    var
        str_Code: Code[10];
    begin
        str_Code := '5110467';
        ERROR(RechercheArticleByActive(str_Code));
    end;

    procedure RechercheMultiReference(var Reference: Code[40]): Code[20]
    var
        Item: Record Item;
        "Item Cross Reference": Record "Item Reference";
    begin
        // -------------------------------------------------------------------------
        // AD Le 11-12-2009 => GDI -> Multireference -> Permet de rechercher la référence en fct de multi ref
        // -------------------------------------------------------------------------

        IF Reference = '' THEN
            EXIT(' ');

        // 1 - Recherche avec le code article
        // ----------------------------------
        IF STRLEN(Reference) <= 20 THEN
            IF Item.GET(Reference) THEN BEGIN
                Reference := Item."No. 2";
                EXIT(Item."No.");
            END;


        // 2 - Recherche sur la référence active
        // -------------------------------------
        IF STRLEN(Reference) <= 20 THEN BEGIN
            Item.RESET();
            Item.SETRANGE("No. 2", Reference);
            IF Item.FINDFIRST() THEN BEGIN
                Reference := Item."No. 2";
                EXIT(Item."No.");
            END;
        END;

        // 3 - Recherche sur le gencode
        // ----------------------------
        IF STRLEN(Reference) <= 13 THEN BEGIN
            Item.RESET();
            Item.SETRANGE("Gencod EAN13", Reference);
            IF Item.FINDFIRST() THEN BEGIN
                Reference := Item."No. 2";
                EXIT(Item."No.");
            END;
        END;

        // 4 - Recherche dans les multireference
        // -------------------------------------
        "Item Cross Reference".RESET();
        "Item Cross Reference".SETRANGE("Reference No.", Reference);
        IF "Item Cross Reference".FINDFIRST() THEN BEGIN
            Item.GET("Item Cross Reference"."Item No.");
            Reference := Item."No. 2";
            EXIT("Item Cross Reference"."Item No.");
        END;

        EXIT('RIENTROUVE');
    end;

    procedure RechercheRefActive(ItemNo: Code[20]): Code[20]
    var
        Item: Record Item;
    begin
        // -----------------------------------------------------------------
        // Retourne la référence active pour une société et un article donné
        // -----------------------------------------------------------------

        IF ItemNo = '' THEN
            EXIT(' ');

        IF NOT Item.GET(ItemNo) THEN EXIT(ItemNo); // AD Le 20-01-2014 -> Je retourne la SP au lieu de vide

        EXIT(Item."No. 2");
    end;

    procedure RechercheRefOrigine(ItemNo: Code[20]): Code[50]
    var
        Item: Record Item;
        ItemCrossRef: Record "Item Reference";
    begin
        // -----------------------------------------------------------------
        // Retourne la référence origine pour un article donné
        // -----------------------------------------------------------------

        CLEAR(ItemCrossRef);

        Item.GET(ItemNo);

        ItemCrossRef.RESET();
        ItemCrossRef.SETRANGE("Item No.", Item."No.");
        ItemCrossRef.SETRANGE("Unit of Measure", Item."Base Unit of Measure");
        ItemCrossRef.SETRANGE("Reference Type", ItemCrossRef."Reference Type"::Origine);
        ItemCrossRef.SETRANGE("Reference Type No.", '');

        IF ItemCrossRef.FINDFIRST() THEN;

        EXIT(ItemCrossRef."Reference No.");
    end;

    procedure RechercheRefGroupeClient(ItemNo: Code[20]; CustNo: Code[20]): Code[50]
    var
        Item: Record Item;
        Customer: Record Customer;
        ItemCrossRef: Record "Item Reference";
    begin
        // -----------------------------------------------------------------
        // Retourne la référence client pour un article donné
        // -----------------------------------------------------------------
        CLEAR(ItemCrossRef);

        Item.GET(ItemNo);
        Customer.GET(CustNo);

        ItemCrossRef.RESET();
        ItemCrossRef.SETRANGE("Item No.", Item."No.");
        ItemCrossRef.SETRANGE("Unit of Measure", Item."Base Unit of Measure");
        ItemCrossRef.SETRANGE("Reference Type", ItemCrossRef."Reference Type"::Customer);
        ItemCrossRef.SETRANGE("Reference Type No.", CustNo);

        IF ItemCrossRef.FINDFIRST() THEN;

        EXIT(ItemCrossRef."Reference No.");
    end;

    procedure RechercheArticleByActive(var Reference: Code[20]): Code[20]
    var
        Item: Record Item;
    begin
        // Recherche sur la référence active
        // -------------------------------------
        Item.RESET();
        Item.SETRANGE("No. 2", Reference);
        IF Item.FINDFIRST() THEN BEGIN
            Reference := Item."No. 2";
            EXIT(Item."No.");
        END;
    end;

    procedure RechercheAncienneActive(ItemNo: Code[20]): Code[50]
    var
        Item: Record Item;
        ItemCrossRef: Record "Item Reference";
    begin
        // -----------------------------------------------------------------
        // Retourne la référence ancienne active pour un article donné
        // -----------------------------------------------------------------

        CLEAR(ItemCrossRef);

        Item.GET(ItemNo);


        ItemCrossRef.RESET();
        ItemCrossRef.SETRANGE("Item No.", Item."No.");
        ItemCrossRef.SETRANGE("Unit of Measure", Item."Base Unit of Measure");
        ItemCrossRef.SETRANGE("Reference Type", ItemCrossRef."Reference Type"::"Ancienne Active");
        ItemCrossRef.SETRANGE("Reference Type No.", '');

        IF ItemCrossRef.FINDFIRST() THEN;

        EXIT(ItemCrossRef."Reference No.");
    end;

    procedure "---"()
    begin
    end;

    procedure ReAffectActiveSALESPRICE(ItemNo: Code[20])
    var
    // SalesPrice: Record "Sales Price";
    
    begin
        /*
        SalesPrice.RESET();
        
        IF ItemNo <> '' THEN
          SalesPrice.SETRANGE("Item No.", ItemNo);
        
        IF SalesPrice.FINDFIRST () THEN
        REPEAT
          SalesPrice.AffecRefActive;
          SalesPrice.MODIFY();
        UNTIL SalesPrice.NEXT() = 0;
        */

    end;

    procedure ReAffectActivePURCHASEPRICE(ItemNo: Code[20])
    var
    // PurchPrice: Record "Purchase Price";
    begin
        /*
        PurchPrice.RESET();
        
        IF ItemNo <> '' THEN
          PurchPrice.SETRANGE("Item No.", ItemNo);
        
        IF PurchPrice.FINDFIRST () THEN
        REPEAT
          PurchPrice.AffecRefActive;
          PurchPrice.MODIFY();
        UNTIL PurchPrice.NEXT() = 0;
        */

    end;

    procedure ReAffectActiveITEMCROSSREF(ItemNo: Code[20])
    var
    // ItemCrossRef: Record "Item Reference";
    begin
        /*
        ItemCrossRef.RESET();
        
        IF ItemNo <> '' THEN
          ItemCrossRef.SETRANGE("Item No.", ItemNo);
        
        ItemCrossRef.FINDFIRST();
        REPEAT
          ItemCrossRef.AffecRefActive;
          ItemCrossRef.MODIFY();
        UNTIL ItemCrossRef.NEXT() = 0;
        */

    end;

    procedure ReAffectActiveAllITEMS(ItemNo: Code[20])
    var
    // Item: Record Item;
    // ItemCrossRef: Record "Item Reference";
    // Item2: Record Item;
    begin
        /*
        Item.RESET();
        ItemCrossRef.RESET();
        
        IF ItemNo <> '' THEN
          Item.SETRANGE("No.", ItemNo);
        
        
        IF Item.FINDSET() THEN
          REPEAT
            ItemCrossRef.SETRANGE("Item No.", Item."No.");
            IF ItemCrossRef.FINDSET() THEN
              REPEAT
                ItemCrossRef.AffecRefActive;
                ItemCrossRef.MODIFY();
              UNTIL ItemCrossRef.NEXT() = 0;
          UNTIL Item.NEXT=0;
        */

    end;

    procedure ReAffectActiveITEMVENDOR(ItemNo: Code[20])
    var
    // ItemVendor: Record "Item Vendor";
    begin
        /*
        ItemVendor.RESET();
        
        IF ItemNo <> '' THEN
          ItemVendor.SETRANGE("Item No.", ItemNo);
        
        IF ItemVendor.FINDFIRST () THEN
        REPEAT
          ItemVendor.AffecRefActive;
          ItemVendor.MODIFY();
        UNTIL ItemVendor.NEXT() = 0;
        */

    end;
}

