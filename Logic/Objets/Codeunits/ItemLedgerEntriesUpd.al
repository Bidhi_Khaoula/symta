codeunit 50045 "Item Ledger Entries Upd"
{
    Permissions = TableData "Item Ledger Entry" = rimd;

    trigger OnRun()
    begin
        Code();
        MESSAGE('Terminé');
    end;

    var
        recNumber: Integer;

    local procedure "Code"()
    var
        lItemLedgerEntry: Record "Item Ledger Entry";
       
        lPostedWhseShipmentLine: Record "Posted Whse. Shipment Line";
       
        lReturnReceiptLine: Record "Return Receipt Line";
        Nb: Integer;
        Window: Dialog;
    begin
        Window.OPEN('Processing ACHAT/BR/CA... @1@@@@@@@@@@' +
                    'Processing ACHAT/RAE/RA... @2@@@@@@@@@@' +
                    'Processing VENTE/BL/CV... @3@@@@@@@@@@' +
                    'Processing VENTE/RVE/RV... @4@@@@@@@@@@');

        WITH lItemLedgerEntry DO BEGIN
            // ACHAT/BR/CA
            /*
            RESET();
            SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Purchase);
            SETFILTER("Document Type", '=%1', "Document Type"::"Purchase Receipt");
            recNumber := COUNT;
            Nb := 0;
            IF FINDSET(TRUE,FALSE) THEN
              REPEAT
                Nb += 1;
                IF Nb MOD (recNumber DIV 100) = 0 THEN
                Window.UPDATE(1,(Nb / recNumber * 10000) DIV 1);

                lPostedWhseReceiptLine.RESET();
                lPostedWhseReceiptLine.SETRANGE("Item No.", "Item No.");
                lPostedWhseReceiptLine.SETRANGE("Source Line No.", "Document Line No.");
                lPostedWhseReceiptLine.SETRANGE("Posted Source Document", lPostedWhseReceiptLine."Posted Source Document"::"Posted Receipt");
                lPostedWhseReceiptLine.SETRANGE("Posted Source No.", "Document No.");
                //lPostedWhseReceiptLine.SETFILTER(Quantity, '>=%1', 0);

                //IF lPostedWhseReceiptLine.COUNT = 0 THEN
                //  ERROR('BR / Aucune ligne... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");

                IF lPostedWhseReceiptLine.COUNT > 1 THEN BEGIN
                  lPostedWhseReceiptLine.SETFILTER(Quantity, '>=%1', 0);
                  IF lPostedWhseReceiptLine.COUNT > 1 THEN
                    ERROR('BR / Trop de lignes... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");
                  lPostedWhseReceiptLine.SETRANGE(Quantity);
                  IF lPostedWhseReceiptLine.FINDFIRST () THEN BEGIN
                    "Warehouse Document No." := lPostedWhseReceiptLine."Whse. Receipt No.";
                    "Source Document No." := lPostedWhseReceiptLine."Source No.";
                    MODIFY;
                  END;
                END ELSE BEGIN
                  IF lPostedWhseReceiptLine.FINDFIRST () THEN BEGIN
                    "Warehouse Document No." := lPostedWhseReceiptLine."Whse. Receipt No.";
                    "Source Document No." := lPostedWhseReceiptLine."Source No.";
                    MODIFY;
                  END;
                END;
              UNTIL lItemLedgerEntry.NEXT() = 0;

            // ACHAT/RAE/RA
            RESET();
            SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Purchase);
            SETFILTER("Document Type", '=%1', "Document Type"::"Purchase Return Shipment");
            recNumber := COUNT;
            Nb := 0;
            IF FINDSET(TRUE,FALSE) THEN
              REPEAT
                Nb += 1;
                IF Nb MOD (recNumber DIV 100) = 0 THEN
                  Window.UPDATE(2,(Nb / recNumber * 10000) DIV 1);

                lReturnShipmentLine.RESET();
                lReturnShipmentLine.SETRANGE("Document No.", "Document No.");
                lReturnShipmentLine.SETRANGE("No.", "Item No.");
                lReturnShipmentLine.SETRANGE("Line No.", "Document Line No.");
                //lReturnShipmentLine.SETFILTER(Quantity, '>=%1', 0);

                IF lReturnShipmentLine.COUNT > 1 THEN BEGIN
                  lReturnShipmentLine.SETFILTER(Quantity, '>=%1', 0);
                  IF lReturnShipmentLine.COUNT > 1 THEN
                    ERROR('RAE / Trop de lignes... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");
                  lReturnShipmentLine.SETRANGE(Quantity);
                  IF lReturnShipmentLine.FINDFIRST () THEN BEGIN
                    "Warehouse Document No." := '';
                    "Source Document No." := lReturnShipmentLine."Return Order No.";
                    MODIFY;
                  END;
                END ELSE BEGIN
                  IF lReturnShipmentLine.FINDFIRST () THEN BEGIN
                    "Warehouse Document No." := '';
                    "Source Document No." := lReturnShipmentLine."Return Order No.";
                    MODIFY;
                  END;
                END;
              UNTIL lItemLedgerEntry.NEXT() = 0;
            */
            // VENTE/BL/CV
            RESET();
            SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Sale);
            SETFILTER("Document Type", '=%1', "Document Type"::"Sales Shipment");
            recNumber := COUNT;
            Nb := 0;
            IF FINDSET() THEN
                REPEAT
                    Nb += 1;
                    IF Nb MOD (recNumber DIV 100) = 0 THEN
                        Window.UPDATE(3, (Nb / recNumber * 10000) DIV 1);
                    lPostedWhseShipmentLine.RESET();
                    lPostedWhseShipmentLine.SETRANGE("Item No.", "Item No.");
                    lPostedWhseShipmentLine.SETRANGE("Source Line No.", "Document Line No.");
                    lPostedWhseShipmentLine.SETRANGE("Posted Source Document", lPostedWhseShipmentLine."Posted Source Document"::"Posted Shipment");
                    lPostedWhseShipmentLine.SETRANGE("Posted Source No.", "Document No.");
                    //lPostedWhseShipmentLine.SETFILTER(Quantity, '>=%1', 0);

                    //IF lPostedWhseShipmentLine.COUNT = 0 THEN
                    //  ERROR('BL / Aucune ligne... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");

                    IF lPostedWhseShipmentLine.COUNT > 1 THEN BEGIN
                        lPostedWhseShipmentLine.SETFILTER(Quantity, '>=%1', 0);
                        IF lPostedWhseShipmentLine.COUNT > 1 THEN
                            ERROR('BL / Trop de lignes... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");
                        lPostedWhseShipmentLine.SETRANGE(Quantity);
                        IF lPostedWhseShipmentLine.FINDFIRST() THEN BEGIN
                            "Warehouse Document No." := lPostedWhseShipmentLine."Whse. Shipment No.";
                            "Source Document No." := lPostedWhseShipmentLine."Source No.";
                            MODIFY();
                        END;
                    END ELSE BEGIN
                        IF lPostedWhseShipmentLine.FINDFIRST() THEN BEGIN
                            "Warehouse Document No." := lPostedWhseShipmentLine."Whse. Shipment No.";
                            "Source Document No." := lPostedWhseShipmentLine."Source No.";
                            MODIFY();
                        END;
                    END;
                UNTIL lItemLedgerEntry.NEXT() = 0;

            // VENTE/RVE/RV
            RESET();
            SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Sale);
            SETFILTER("Document Type", '=%1', "Document Type"::"Sales Return Receipt");
            recNumber := COUNT;
            Nb := 0;
            IF FINDSET() THEN
                REPEAT
                    Nb += 1;
                    IF Nb MOD (recNumber DIV 100) = 0 THEN
                        Window.UPDATE(4, (Nb / recNumber * 10000) DIV 1);

                    lReturnReceiptLine.RESET();
                    lReturnReceiptLine.SETRANGE("Document No.", "Document No.");
                    lReturnReceiptLine.SETRANGE("No.", "Item No.");
                    lReturnReceiptLine.SETRANGE("Line No.", "Document Line No.");
                    //lReturnReceiptLine.SETFILTER(Quantity, '>=%1', 0);

                    IF lReturnReceiptLine.COUNT > 1 THEN BEGIN
                        lReturnReceiptLine.SETFILTER(Quantity, '>=%1', 0);
                        IF lReturnReceiptLine.COUNT > 1 THEN
                            ERROR('RVE / Trop de lignes... %1 / %2 / %3', "Item No.", "Document Line No.", "Document No.");
                        lReturnReceiptLine.SETRANGE(Quantity);
                        IF lReturnReceiptLine.FINDFIRST() THEN BEGIN
                            "Warehouse Document No." := '';
                            "Source Document No." := lReturnReceiptLine."Return Order No.";
                            MODIFY();
                        END;
                    END ELSE BEGIN
                        IF lReturnReceiptLine.FINDFIRST() THEN BEGIN
                            "Warehouse Document No." := '';
                            "Source Document No." := lReturnReceiptLine."Return Order No.";
                            MODIFY();
                        END;
                    END;
                UNTIL lItemLedgerEntry.NEXT() = 0;

        END;
        Window.CLOSE();

    end;
}

