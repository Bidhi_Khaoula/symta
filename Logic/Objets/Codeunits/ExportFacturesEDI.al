codeunit 50022 "Export Factures EDI"
{

    Permissions = TableData "Sales Invoice Header" = rm,
                  TableData "Sales Cr.Memo Header" = rm;

    trigger OnRun()
    var
        Client: Record Customer;
    begin
        ExportInvoiceEDI(0, WORKDATE(), WORKDATE(), '');
    end;

    var
          InfoComptabilite: Record "General Ledger Setup";
        "InfoSociété": Record "Company Information";

        FichierEdi: File;

        sep: Code[1];
        CodeFraisDEEE: Code[10];
        Ligne: Text[1024];
        i: Integer;
              TabCommentaire: array[5] of Text[250];
        Text001bQSt: Label 'Voulez vous exporter les factures EDI pour le code [%1] ?';
        Text002Err: Label 'La date de début doit être > à la date de fin !';
        Text003Err: Label 'Les date des début et fin doivent être renseignées !';
        Text004Err: Label 'Code Exportation Obligatoire !';
        Text009Err: Label 'ATTENTION ! Plusieurs Bl pour la facture %1, ligne %2 ! Risque de problème coté EDI client !';
        Text010Err: Label 'Type adresse %1 non géré !';
        Window: Dialog;
        Text001aQst: Label 'Voulez vous exporter les factures EDI ?';
        Text011: Label 'Lecture du frais annexe %1 impossible.';
        Text012: Label 'Plusieurs dans de livraison pour la facture %1. Impossible d''exporter !';
        Text100: Label 'Type facturation EDI ################1#\\';
        Text101: Label 'Code Client               ################2#\\';
        Text102: Label 'No Facture                ################3#\\';

    procedure ExportInvoiceEDI(TypeExportation: Option "Génération","Re-Génération",Maintenance; "DateDébut": Date; DateFin: Date; CodeExportation: Code[10])
    var
        Param: Record "Generals Parameters";
        EnteteFacture: Record "Sales Invoice Header";
        EnteteAvoir: Record "Sales Cr.Memo Header";
        Customer: Record Customer;
        LMois: Code[6];
    begin
        sep := ';';
        CodeFraisDEEE := 'DEEE';            // Correspond au code du frais annexe de l'éco contribution

        // Lecture des informations générales
        // ----------------------------------
        InfoComptabilite.GET();
        InfoSociété.GET();

        // Test des paramétres
        // -------------------
        CASE TypeExportation OF
            TypeExportation::Génération:
                BEGIN
                END;
            TypeExportation::"Re-Génération":
                BEGIN
                    IF (DateDébut = 0D) OR (DateFin = 0D) THEN
                        ERROR(Text003Err);

                    IF (DateDébut > DateFin) THEN
                        ERROR(Text002Err);

                    IF (CodeExportation = '') THEN
                        ERROR(Text004Err);
                END;
            TypeExportation::Maintenance:
                BEGIN
                    ERROR('Pas codé');
                END;
        END;

        // CFR le 28/04/2021 => Mise en production CAMPA
        /*
        IF NOT CONFIRM(Text001, TRUE) THEN
          EXIT;
        */
        IF (CodeExportation = '') THEN BEGIN
            IF NOT CONFIRM(Text001aQst, TRUE) THEN
                EXIT;
        END
        ELSE BEGIN
            IF NOT CONFIRM(Text001bQSt, TRUE, CodeExportation) THEN
                EXIT;
        END;
        // FIN CFR le 28/04/2021


        Window.OPEN(Text100 + Text101 + Text102);

        LMois := FORMAT(DateDébut, 0, '<Year4><Month,2>');

        CASE TypeExportation OF
            // #################################################
            // Génrétaion de toutes les factures avoirs non émis
            // #################################################
            TypeExportation::Génération:
                BEGIN
                    // Lecture des <> paramètres
                    // -------------------------
                    Param.RESET();
                    Param.SETRANGE(Type, 'FIC_EDI');
                    // CFR le 28/04/2021 => Mise en production CAMPA
                    /*
                    // AD Le 29-04-2020 => Tous dans le même CU
                    CASE CodeExportation OF
                      'FAC_SCAR' : Param.SETFILTER(Code, 'FAC_SCAR');
                      ELSE Param.SETFILTER(Code, '<>%1', 'FAC_SCAR');
                    END;
                    */
                    IF (CodeExportation <> '') THEN
                        Param.SETRANGE(Code, CodeExportation)
                    ELSE
                        Param.SETFILTER(Code, '<>%1', 'FAC_SCAR');
                    // FIN CFR le 28/04/2021

                    IF Param.FINDFIRST() THEN
                        REPEAT
                            Window.UPDATE(1, Param.Code);

                            // Rechcerche si des clients avec ce Code
                            // --------------------------------------
                            Customer.RESET();
                            Customer.SETCURRENTKEY("Code Facturation EDI");
                            Customer.SETRANGE("Code Facturation EDI", Param.Code);
                            IF Customer.FINDFIRST() THEN
                                REPEAT

                                    Window.UPDATE(2, Customer."No.");

                                    // Pose des filtres
                                    // ----------------
                                    Customer.TESTFIELD("Date Début Export Facture EDI");
                                    EnteteFacture.RESET();
                                    EnteteFacture.SETFILTER("Posting Date", '%1..', Customer."Date Début Export Facture EDI");
                                    EnteteFacture.SETRANGE("Sell-to Customer No.", Customer."No.");
                                    EnteteFacture.SETRANGE("EDI Exportation", FALSE);
                                    //EnteteFacture.SETRANGE("No.", 'FVE-2102*');

                                    EnteteAvoir.RESET();
                                    EnteteAvoir.SETFILTER("Posting Date", '%1..', Customer."Date Début Export Facture EDI");
                                    EnteteAvoir.SETRANGE("Sell-to Customer No.", Customer."No.");
                                    EnteteAvoir.SETRANGE("EDI Exportation", FALSE);
                                    EnteteAvoir.SETRANGE("No.", 'A0902006fdfd3'); // On verra les avoirs après

                                    // Exportation des données pour les filtres
                                    // ----------------------------------------
                                    RechercheFactureAvoir(Customer."Code Facturation EDI", EnteteFacture, EnteteAvoir, Customer."No.", LMois);
                                UNTIL Customer.NEXT() = 0;
                        UNTIL Param.NEXT() = 0;


                END;
            // ##########################################
            // Re-Génération des factures avoirs demandés
            // ##########################################
            TypeExportation::"Re-Génération":
                BEGIN
                    // Pose des filtres
                    // ----------------
                    EnteteFacture.RESET();
                    EnteteFacture.SETRANGE("Posting Date", DateDébut, DateFin);
                    //EnteteFacture.SETRANGE("Code Export EDI", CodeExportation);
                    //EnteteFacture.SETRANGE("Export EDI", TRUE);

                    EnteteAvoir.RESET();
                    EnteteAvoir.SETRANGE("Posting Date", DateDébut, DateFin);
                    //EnteteAvoir.SETRANGE("Code Export EDI", CodeExportation);
                    //EnteteAvoir.SETRANGE("Export EDI", TRUE);

                    // Exportation des données pour les filtres
                    // ----------------------------------------
                    RechercheFactureAvoir(CodeExportation, EnteteFacture, EnteteAvoir, Customer."No.", LMois);

                END;
            // ########################################
            // Re-Génrétaion des factures selectionnées
            // ########################################
            TypeExportation::Maintenance:
                BEGIN
                END;
        END;
        //ERROR('AD');

    end;

    procedure OuvrirFichierEdi(CodeFic: Code[10]; _pCodeClient: Code[10]; _pMois: Code[10])
    var
        param: Record "Generals Parameters";
       
        LCust: Record Customer;
         NomFichier: Text[254];
    begin
        // Ouverture du fichier pour ce client
        // -----------------------------------
        FichierEdi.TEXTMODE(TRUE);
        FichierEdi.WRITEMODE(TRUE);

        InfoSociété.GET();

        param.GET('FIC_EDI', CodeFic);
        param.TESTFIELD(param.LongDescription2);

        CASE CodeFic OF
            'FAC_SCAR':
                BEGIN
                    LCust.GET(_pCodeClient);
                    NomFichier := InfoSociété."Export EDI Folder" + '\' + LCust.Name + '-' + _pMois + '.txt';
                END;
            ELSE
                NomFichier := '\\srv-edi\MESSAGES\DEPART' + '\' + param.LongDescription2; // Voir pour le répertoire
        END;


        IF FILE.EXISTS(NomFichier) THEN
            FichierEdi.OPEN(NomFichier)
        ELSE
            FichierEdi.CREATE(NomFichier);

        FichierEdi.SEEK(FichierEdi.LEN);
    end;

    procedure RechercheFactureAvoir(LCodeExportation: Code[10]; var LEnteteFacture: Record "Sales Invoice Header"; var LEnteteAvoir: Record "Sales Cr.Memo Header"; _pNoClient: Code[10]; _pMois: Code[6])
    begin

        // Aucune facture ni avoir à exporter
        // ----------------------------------
        IF (LEnteteFacture.COUNT = 0) AND (LEnteteAvoir.COUNT = 0) THEN
            EXIT;

        // Ouverture du fichier EDI
        // ------------------------
        OuvrirFichierEdi(LCodeExportation, _pNoClient, _pMois);

        // Export des données
        // ------------------
        IF LEnteteFacture.FIND('-') THEN
            REPEAT
                Window.UPDATE(2, LEnteteFacture."Sell-to Customer No.");
                Window.UPDATE(3, LEnteteFacture."No.");
                CASE LCodeExportation OF
                    'FAC_SCAR':
                        ExportFactureSCAR(LEnteteFacture, LCodeExportation, _pMois);
                    ELSE
                        ExportFacture(LEnteteFacture, LCodeExportation);
                END;
            UNTIL LEnteteFacture.NEXT() = 0;

        IF LEnteteAvoir.FIND('-') THEN
            REPEAT
                Window.UPDATE(2, LEnteteAvoir."Sell-to Customer No.");
                Window.UPDATE(3, LEnteteAvoir."No.");

                CASE LCodeExportation OF
                    'FAC_SCAR':
                        ExportAvoirSCAR(LEnteteAvoir, LCodeExportation, _pMois);
                    ELSE
                        ExportAvoir(LEnteteAvoir, LCodeExportation);
                END;
            UNTIL LEnteteFacture.NEXT() = 0;

        // Fermeture du fichier EDI
        // ------------------------
        FichierEdi.CLOSE();
    end;

    procedure ExportFacture(LEnteteFacture: Record "Sales Invoice Header"; LCodeExportation: Code[10])
    var
        "Payment Method": Record "Payment Method";
        SalesInvLine: Record "Sales Invoice Line";
        "Sales Comment Line": Record "Sales Comment Line";
        "Shipment Invoiced": Record "Shipment Invoiced";
        "Sales Shipment Header": Record "Sales Shipment Header";
        Client: Record Customer;
        CustAmount: Decimal;
        AmountInclVAT: Decimal;
        InvDiscAmount: Decimal;
        VATAmount: Decimal;
        LineQty: Decimal;
        VATPercentage: Decimal;
        NoBl: Code[20];
        
    begin

        Ligne := '';

        // ------------------------
        // Verification des données
        // ------------------------
        //LEnteteFacture.TESTFIELD("External Document No.");
        //LEnteteFacture.TESTFIELD("Your Reference");

        // ----------------------------
        // Lecture du mode de règlement
        // ----------------------------
        "Payment Method".GET(LEnteteFacture."Payment Method Code");
        "Payment Method".TESTFIELD("EDI Code");

        // ---------------------------
        // Recherche du 1er BL associé
        // ---------------------------
        "Shipment Invoiced".INIT();
        "Shipment Invoiced".RESET();
        "Shipment Invoiced".SETRANGE("Invoice No.", LEnteteFacture."No.");
        NoBl := 'XXX';
        IF "Shipment Invoiced".FindSet() THEN
            REPEAT
                IF NoBl = 'XXX' THEN
                    NoBl := "Shipment Invoiced"."Shipment No.";

                // AD Le 05-03-2020 => Normal chez SYMTA
                IF NoBl <> "Shipment Invoiced"."Shipment No." THEN
                    //  ERROR(Text009, LEnteteFacture."No.", 0);
                    NoBl := 'MULTIPLE'; // Si plusieurs BL, on passe Vide

            UNTIL "Shipment Invoiced".NEXT() = 0;

        IF "Shipment Invoiced".FINDFIRST() THEN
            "Sales Shipment Header".GET("Shipment Invoiced"."Shipment No.");

        IF NoBl = 'MULTIPLE' THEN BEGIN
            NoBl := '';
            CLEAR("Sales Shipment Header");
        END;


        Client.GET(LEnteteFacture."Bill-to Customer No.");

        Ligne += 'ENT' + sep;
        Ligne += Client."Customer EDI Code" + sep;
        Ligne += LEnteteFacture."No." + sep;                    // No facture

        Ligne += "Sales Shipment Header"."External Document No." + sep;  // No Commande client
        Ligne += FormatDate(LEnteteFacture."Posting Date") + sep;   // Date Facture
        Ligne += FormatDate(LEnteteFacture."Order Date") + sep;     // Date Commande
        Ligne += "Sales Shipment Header"."No." + sep;                                      // No Bl
        Ligne += FormatDate("Sales Shipment Header"."Posting Date") + sep; // Date Bl
        Ligne += FormatDate(LEnteteFacture."Due Date") + sep;   // Date d'échéance

        Ligne += "Payment Method"."EDI Code" + sep;          // Code Mode de reglement
        Ligne += "Payment Method".Description + sep;            // Libelle Mode de reglement

        IF LEnteteFacture."Currency Code" = '' THEN
            Ligne += InfoComptabilite."LCY Code" + sep            // Code Devise
        ELSE
            Ligne += LEnteteFacture."Currency Code" + sep;        // Code Devise

        Ligne += '0' + sep;                                     // Frais
        Ligne += '0' + sep;                                     // Frais de port
        Ligne += '0' + sep;                                     // Escompte
        Ligne += '0' + sep;                                     // % Remise 1
        Ligne += '0' + sep;                                     // Montant Remise 1
        Ligne += '0' + sep;                                     // % Remise 2
        Ligne += '0' + sep;                                     // Montant Remise 2

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        SalesInvLine.RESET();
        SalesInvLine.SETRANGE("Document No.", LEnteteFacture."No.");
        SalesInvLine.SETFILTER(Type, '>0');
        IF SalesInvLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + SalesInvLine.Amount;
                AmountInclVAT := AmountInclVAT + SalesInvLine."Amount Including VAT";
                IF LEnteteFacture."Prices Including VAT" THEN
                    InvDiscAmount := InvDiscAmount + SalesInvLine."Inv. Discount Amount" / (1 + SalesInvLine."VAT %" / 100)
                ELSE
                    InvDiscAmount := InvDiscAmount + SalesInvLine."Inv. Discount Amount";
                LineQty := LineQty + SalesInvLine.Quantity;
                IF SalesInvLine."VAT %" <> VATPercentage THEN
                    IF VATPercentage = 0 THEN
                        VATPercentage := SalesInvLine."VAT %"
                    ELSE
                        VATPercentage := -1;
            UNTIL SalesInvLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        InvDiscAmount := ROUND(InvDiscAmount, 0.01);

        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        Ligne += FormatDecimal(VATPercentage) + sep;          // % de TVA si unique sinon -1
        Ligne += FormatDecimal(CustAmount) + sep;             // Base TVA
        Ligne += FormatDecimal(VATAmount) + sep;              // Montant TVA
        Ligne += FormatDecimal(0) + sep;                      // Montant Accompte
        Ligne += FormatDecimal(CustAmount) + sep;             // Base HT
        Ligne += FormatDecimal(AmountInclVAT) + sep;          // Montant TTC
        Ligne += FormatDecimal(AmountInclVAT) + sep;          // Net a payer

        Ligne += '0' + sep;                                     // Nb Colis

        // ------------------------
        // Recherche des commentaires
        // ------------------------
        CLEAR(TabCommentaire);
        i := 0;

        "Sales Comment Line".RESET();
        "Sales Comment Line".SETRANGE("Document Type", "Sales Comment Line"."Document Type"::"Posted Invoice");
        "Sales Comment Line".SETRANGE("No.", LEnteteFacture."No.");
        "Sales Comment Line".SETRANGE("Sales Comment Line"."Print Invoice", TRUE);
        IF "Sales Comment Line".FIND('-') THEN
            REPEAT
                i += 1;
                TabCommentaire[i] := "Sales Comment Line".Comment;
                IF i = 5 THEN
                    "Sales Comment Line".FIND('+');
            UNTIL "Sales Comment Line".NEXT() = 0;
        // ---------------------------------
        // Fin de recherche des commentaires
        // ---------------------------------

        FOR i := 1 TO 5 DO
            Ligne += TabCommentaire[i] + sep;

        FichierEdi.WRITE(PurgeAccents(Ligne));

        // ----------------------------
        // Export des Lignes de remises
        // ----------------------------
        ExportRemiseEnteteFacture(LEnteteFacture);

        // ---------------------------
        // Export des Lignes d'adresse
        // ---------------------------
        ExportAdresseFournisseur(LEnteteFacture."No.");
        ExportAdresseClientFacture(LEnteteFacture, 'IV', LEnteteFacture."Bill-to Customer No.");
        ExportAdresseClientFacture(LEnteteFacture, 'BY', LEnteteFacture."Sell-to Customer No.");
        ExportAdresseClientFacture(LEnteteFacture, 'DP', LEnteteFacture."Sell-to Customer No.");

        // -----------------------------
        // Export des lignes de factures
        // -----------------------------
        ExportLignesFacture(LEnteteFacture);

        LEnteteFacture."EDI Exportation" := TRUE;
        LEnteteFacture."EDI Exportation Date" := TODAY;
        LEnteteFacture."EDI Exportation Time" := TIME;
        LEnteteFacture."EDI Exportation User" := USERID;
        LEnteteFacture."EDI Exportation Code" := LCodeExportation;
        LEnteteFacture.MODIFY();
    end;

    procedure ExportRemiseEnteteFacture(LEnteteFacture: Record "Sales Invoice Header")
    var
        LignesFacture: Record "Sales Invoice Line";
        "Item Charge": Record "Item Charge";
        MtPort: Decimal;
    begin
        // Export des remises et charges de la factures
        // ############################################

        // ------------------------
        // Export des frais de port
        // ------------------------
        LignesFacture.RESET();
        //LignesFacture.SETCURRENTKEY("Document No.",Type,"No.");
        LignesFacture.SETRANGE("Document No.", LEnteteFacture."No.");
        //LignesFacture.SETRANGE(Type, LignesFacture.Type::"G/L Account");
        LignesFacture.SETRANGE("Internal Line Type", LignesFacture."Internal Line Type"::Shipment);
        MtPort := 0;
        IF LignesFacture.FindSet() THEN
            REPEAT
                MtPort += LignesFacture.Amount;
            UNTIL LignesFacture.NEXT() = 0;

        IF MtPort <> 0 THEN BEGIN
            Ligne := '';
            Ligne += 'REE' + sep;
            Ligne += LEnteteFacture."No." + sep;                                      // No Facture
            Ligne += 'C' + sep;                                                       // Type
            Ligne += '1' + sep;                                                        // Ordre
            Ligne += 'FC' + sep;                                                       // Code remise  ++ ICI FRAIS DE PORT ++
            Ligne += 'FRAIS DE PORT' + sep;                                            // Libellé remise  ++ ICI FRAIS DE PORT ++
            Ligne += '0' + sep;                                                        // % Remise 1
            Ligne += '0' + sep;                                                        // % Remise 2
            Ligne += '0' + sep;                                                        // % Remise 3
            Ligne += '0' + sep;                                                        // % Remise 4
            Ligne += 'V' + sep;                                                        // Valeur ou pourcentage
            Ligne += '0' + sep;                                                        // Base du pourcentage
            Ligne += FormatDecimal(MtPort) + sep;                        // Montant
            Ligne += FormatDecimal(MtPort) + sep;                        // Prix

            FichierEdi.WRITE(PurgeAccents(Ligne));
        END;


        // -----------------
        // Export de la DEEE
        // -----------------
        IF CodeFraisDEEE <> '' THEN BEGIN
            // Recherche du libellé du frais DEEE
            // ----------------------------------
            IF NOT "Item Charge".GET(CodeFraisDEEE) THEN
                ERROR(Text011, CodeFraisDEEE);

            // Recherche du montant des lignes
            // -------------------------------
            LignesFacture.RESET();
            LignesFacture.SETCURRENTKEY("Document No.", Type, "No.");
            LignesFacture.SETRANGE("Document No.", LEnteteFacture."No.");
            LignesFacture.SETFILTER(Type, '%1|%2', LignesFacture.Type::"Charge (Item)", LignesFacture.Type::Resource);
            LignesFacture.SETRANGE("No.", "Item Charge"."No.");
            LignesFacture.CALCSUMS(Amount);
            IF LignesFacture.Amount <> 0 THEN BEGIN
                Ligne := '';
                Ligne += 'REE' + sep;
                Ligne += LEnteteFacture."No." + sep;                                      // No Facture
                Ligne += 'C' + sep;                                                       // Type
                Ligne += '1' + sep;                                                        // Ordre
                Ligne += 'DEEE' + sep;                                                     // Code remise  ++ ICI FRAIS DEEE ++
                Ligne += "Item Charge".Description + sep;                                  // Libellé remise  ++ ICI FRAIS DEEE ++
                Ligne += '0' + sep;                                                        // % Remise 1
                Ligne += '0' + sep;                                                        // % Remise 2
                Ligne += '0' + sep;                                                        // % Remise 3
                Ligne += '0' + sep;                                                        // % Remise 4
                Ligne += 'V' + sep;                                                        // Valeur ou pourcentage
                Ligne += '0' + sep;                                                        // Base du pourcentage
                Ligne += FormatDecimal(LignesFacture.Amount) + sep;                        // Montant
                Ligne += FormatDecimal(LignesFacture.Amount) + sep;                        // Prix

                FichierEdi.WRITE(PurgeAccents(Ligne));
            END;
        END;
    end;

    procedure ExportLignesFacture(LEnteteFacture: Record "Sales Invoice Header")
    var
        LignesFacture: Record "Sales Invoice Line";
        "Shipment Invoiced": Record "Shipment Invoiced";
        "Sales Shipment Header": Record "Sales Shipment Header";
        Item: Record Item;
        CommentLine: Record "Sales Invoice Line";
        LineNo: Integer;
    begin

        LineNo := 0;

        LignesFacture.RESET();
        LignesFacture.SETRANGE("Document No.", LEnteteFacture."No.");
        LignesFacture.SETRANGE(Type, LignesFacture.Type::Item);
        LignesFacture.SETFILTER("No.", '<>%1', 'PORT');
        // +++ SPECIF SIDAMO => PORT EN TANT QU'ARTICLE +++
        IF LignesFacture.FIND('-') THEN
            REPEAT

                // ---------------------------
                // Recherche du 1er BL associé
                // ---------------------------
                "Shipment Invoiced".INIT();
                "Shipment Invoiced".RESET();
                "Shipment Invoiced".SETRANGE("Invoice No.", LignesFacture."Document No.");
                "Shipment Invoiced".SETRANGE("Invoice Line No.", LignesFacture."Line No.");
                IF "Shipment Invoiced".COUNT > 1 THEN
                    ERROR(Text009Err, LignesFacture."Document No.", LignesFacture."Line No.");

                IF "Shipment Invoiced".FindFirst() THEN
                    "Sales Shipment Header".GET("Shipment Invoiced"."Shipment No.");

                // ----------------------
                // Recherche de l'article
                // ----------------------
                Item.GET(LignesFacture."No.");

                LineNo += 1;

                Ligne := '';
                Ligne += 'LIG' + sep;
                Ligne += FormatInteger(LineNo) + sep;                            // No de ligne
                Ligne += LignesFacture."Document No." + sep;                                      // No Facture
                Ligne += "Sales Shipment Header"."External Document No." + sep;                    // No commande client
                                                                                                   //Ligne += "Sales Shipment Header"."Your Reference" + sep;                    // No commande client
                Ligne += FormatDate("Sales Shipment Header"."Order Date") + sep;                   // Date commande client
                Ligne += "Sales Shipment Header"."No." + sep;                                      // No commande client
                Ligne += FormatDate("Sales Shipment Header"."Posting Date") + sep;                 // Date commande client
                Ligne += getGenCod(LignesFacture."No.") + sep;                                     // Gencode article
                Ligne += LignesFacture."No." + sep;                                                // Code de l'article
                Ligne += LignesFacture.Description + sep;                                          // Désignation article
                Ligne += LignesFacture."Description 2" + sep;                                      // Désignation article 2
                Ligne += FormatInteger(0) + sep;                                                   // Qte Commandée en EDI *** NON GERE ***
                Ligne += FormatDecimal(LignesFacture."Quantity (Base)") + sep;                     // Qte Facturée en unité de base
                Ligne += FormatInteger(0) + sep;                                                   // Qte Livrée en EDI *** NON GERE ***
                Ligne += FormatDecimal(LignesFacture.Quantity) + sep;                              // Qte Facturée en nb unité
                Ligne += FormatDecimal(LignesFacture."VAT %") + sep;                               // % TVA

                // AD Le 13-02-2009 => WORMS -> 1 seule remise
                Ligne += FormatDecimal(LignesFacture."Line Discount %") + sep;                     // % remise 1
                                                                                                   //Ligne += FormatDecimal(LignesFacture."Discount2 %") + sep;                       // % remise 2
                Ligne += FormatDecimal(0) + sep;                                                   // % remise 2

                Ligne += FormatDecimal(LignesFacture."Unit Price") + sep;                          // Prix Brut

                // AD Le 13-02-2009 => WORMS -> Pas de prix net
                //Ligne += FormatDecimal(LignesFacture."Net Unit Price") + sep;                    // Prix Net
                Ligne += FormatDecimal(ROUND(LignesFacture."Unit Price" * (1 - LignesFacture."Line Discount %" / 100), 0.01)
                                                                     ) + sep;                      // Prix Net

                Ligne += FormatDecimal(LignesFacture."VAT Base Amount") + sep;                     // Base TVA  = Montant HT
                Ligne += FormatDecimal(LignesFacture."Amount Including VAT" -
                                       LignesFacture."VAT Base Amount") + sep;                     // Montant TVA
                Ligne += FormatDecimal(LignesFacture."Amount Including VAT") + sep;                // Montant TTC

                // ------------------------------------------------------------------------------------------------
                // Recherche des commentaires associés à la ligne (Utilisé en 3,70 à revoir pour les version > 5.0)
                // ------------------------------------------------------------------------------------------------
                CLEAR(TabCommentaire);
                i := 0;

                CommentLine.RESET();
                CommentLine.SETRANGE("Document No.", LignesFacture."Document No.");
                CommentLine.SETRANGE(Type, CommentLine.Type::" ");
                CommentLine.SETRANGE("Attached to Line No.", LignesFacture."Line No.");
                IF CommentLine.FIND('-') THEN
                    REPEAT
                        i += 1;
                        TabCommentaire[i] := CommentLine.Description + ' ' + CommentLine."Description 2";
                        IF i = 5 THEN
                            CommentLine.FIND('+');
                    UNTIL CommentLine.NEXT() = 0;
                // ---------------------------------
                // Fin de recherche des commentaires
                // ---------------------------------

                FOR i := 1 TO 5 DO
                    Ligne += TabCommentaire[i] + sep;

                FichierEdi.WRITE(PurgeAccents(Ligne));

                ExportRemiseLigneFacture(LEnteteFacture, LignesFacture);


            UNTIL LignesFacture.NEXT() = 0;
    end;

    procedure ExportRemiseLigneFacture(LEnteteFacture: Record "Sales Invoice Header"; LLigneFacture: Record "Sales Invoice Line")
    var
        LigneFactureAttach: Record "Sales Invoice Line";
        "Item Charge": Record "Item Charge";
    begin
        // Export des remises et charges de la ligne de factures
        // #####################################################


        // -------------------------
        // Export des remises 1 et 2
        // -------------------------
        // AD Le 13-02-2009 => WORMS -> 1 seule remise
        IF LLigneFacture."Line Discount %" <> 0 THEN BEGIN
            Ligne := '';
            Ligne += 'REL' + sep;
            Ligne += LEnteteFacture."No." + sep;                                      // No Facture
            Ligne += 'A' + sep;                                                       // Type
            Ligne += '1' + sep;                                                        // Ordre
            Ligne += 'TD' + sep;                                                       // Code remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += 'REMISE COMMERCIALE' + sep;                                       // Libellé remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += FormatDecimal(LLigneFacture."Line Discount %") + sep;             // % Remise 1
            Ligne += '0' + sep;                                                        // % Remise 2
            Ligne += '0' + sep;                                                        // % Remise 3
            Ligne += '0' + sep;                                                        // % Remise 4
            Ligne += 'P' + sep;                                                        // Valeur ou pourcentage
            Ligne += '0' + sep;                                                        // Base du pourcentage
            Ligne += '0' + sep;                                                        // Montant
            Ligne += '0' + sep;                                                        // Prix

            FichierEdi.WRITE(PurgeAccents(Ligne));
        END;

        // AD Le 13-02-2009 => WORMS -> 1 seule remise
        IF LLigneFacture."Discount2 %" <> 0 THEN BEGIN
            Ligne := '';
            Ligne += 'REL' + sep;
            Ligne += LEnteteFacture."No." + sep;                                      // No Facture
            Ligne += 'A' + sep;                                                       // Type
            Ligne += '2' + sep;                                                        // Ordre
            Ligne += 'TD' + sep;                                                       // Code remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += 'REMISE COMMERCIALE' + sep;                                       // Libellé remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += FormatDecimal(LLigneFacture."Discount2 %") + sep;                 // % Remise 1
            Ligne += '0' + sep;                                                        // % Remise 2
            Ligne += '0' + sep;                                                        // % Remise 3
            Ligne += '0' + sep;                                                        // % Remise 4
            Ligne += 'P' + sep;                                                        // Valeur ou pourcentage
            Ligne += '0' + sep;                                                        // Base du pourcentage
            Ligne += '0' + sep;                                                        // Montant
            Ligne += '0' + sep;                                                        // Prix


            FichierEdi.WRITE(PurgeAccents(Ligne));
        END;

        // ------------------------------------
        // Export de la DEEE attaché à la ligne
        // ------------------------------------
        IF CodeFraisDEEE <> '' THEN BEGIN
            // Recherche du libellé du frais DEEE
            // ----------------------------------
            IF NOT "Item Charge".GET(CodeFraisDEEE) THEN
                ERROR(Text011, CodeFraisDEEE);

            // Recherche de la ligne de facture
            // --------------------------------
            LigneFactureAttach.RESET();
            LigneFactureAttach.SETRANGE("Document No.", LEnteteFacture."No.");
            LigneFactureAttach.SETFILTER(Type, '%1|%2', LigneFactureAttach.Type::"Charge (Item)",
                      LigneFactureAttach.Type::Resource);
            LigneFactureAttach.SETRANGE("No.", "Item Charge"."No.");
            LigneFactureAttach.SETRANGE("Attached to Line No.", LLigneFacture."Line No.");

            IF LigneFactureAttach.FindFirst() THEN BEGIN
                Ligne := '';
                Ligne += 'REL' + sep;
                Ligne += LEnteteFacture."No." + sep;                                      // No Facture
                Ligne += 'C' + sep;                                                       // Type
                Ligne += '1' + sep;                                                        // Ordre
                Ligne += 'DEEE' + sep;                                                     // Code remise  ++ ICI FRAIS DEEE ++
                Ligne += "Item Charge".Description + sep;                                  // Libellé remise  ++ ICI FRAIS DEEE ++
                Ligne += '0' + sep;                                                        // % Remise 1
                Ligne += '0' + sep;                                                        // % Remise 2
                Ligne += '0' + sep;                                                        // % Remise 3
                Ligne += '0' + sep;                                                        // % Remise 4
                Ligne += 'V' + sep;                                                        // Valeur ou pourcentage
                Ligne += '0' + sep;                                                        // Base du pourcentage
                Ligne += FormatDecimal(LigneFactureAttach.Amount) + sep;                        // Montant
                Ligne += FormatDecimal(LigneFactureAttach."Net Unit Price") + sep;                        // Prix

                FichierEdi.WRITE(PurgeAccents(Ligne));
            END;
        END;
    end;

    procedure ExportAvoir(LEnteteAvoir: Record "Sales Cr.Memo Header"; LCodeExportation: Code[20])
    var
        "Payment Method": Record "Payment Method";
        SalesCreLine: Record "Sales Cr.Memo Line";
        "Sales Comment Line": Record "Sales Comment Line";
        CustAmount: Decimal;
        AmountInclVAT: Decimal;
        InvDiscAmount: Decimal;
        VATAmount: Decimal;
        LineQty: Decimal;
        VATPercentage: Decimal;
    begin

        Ligne := '';

        // ------------------------
        // Verification des données
        // ------------------------
        //LEnteteFacture.TESTFIELD("External Document No.");
        //LEnteteFacture.TESTFIELD("Your Reference");

        // ----------------------------
        // Lecture du mode de règlement
        // ----------------------------
        "Payment Method".INIT();
        IF "Payment Method".GET(LEnteteAvoir."Payment Method Code") THEN
            "Payment Method".TESTFIELD("EDI Code");

        Ligne += 'ENT' + sep;
        Ligne += 'Destinataire' + sep;
        Ligne += LEnteteAvoir."No." + sep;                    // No facture

        //Ligne += LEnteteFacture."External Document No." + sep;  // No Commande client
        Ligne += LEnteteAvoir."Your Reference" + sep;  // No Commande client

        Ligne += FormatDate(LEnteteAvoir."Posting Date") + sep;   // Date Facture
        Ligne += FormatDate(LEnteteAvoir."Posting Date") + sep;     // Date Commande
        LEnteteAvoir.TESTFIELD("Applies-to Doc. No.");
        Ligne += LEnteteAvoir."Applies-to Doc. No." + sep;                                        // No Bl
        Ligne += '' + sep;                                        // Date Bl
        Ligne += FormatDate(LEnteteAvoir."Due Date") + sep;     // Date d'échéance

        Ligne += "Payment Method"."EDI Code" + sep;          // Code Mode de reglement
        Ligne += "Payment Method".Description + sep;            // Libelle Mode de reglement

        IF LEnteteAvoir."Currency Code" = '' THEN
            Ligne += InfoComptabilite."LCY Code" + sep            // Code Devise
        ELSE
            Ligne += LEnteteAvoir."Currency Code" + sep;        // Code Devise

        Ligne += '0' + sep;                                     // Frais
        Ligne += '0' + sep;                                     // Frais de port
        Ligne += '0' + sep;                                     // Escompte
        Ligne += '0' + sep;                                     // % Remise 1
        Ligne += '0' + sep;                                     // Montant Remise 1
        Ligne += '0' + sep;                                     // % Remise 2
        Ligne += '0' + sep;                                     // Montant Remise 2

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        SalesCreLine.RESET();
        SalesCreLine.SETRANGE("Document No.", LEnteteAvoir."No.");
        SalesCreLine.SETFILTER(Type, '>0');
        IF SalesCreLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + SalesCreLine.Amount;
                AmountInclVAT := AmountInclVAT + SalesCreLine."Amount Including VAT";
                IF LEnteteAvoir."Prices Including VAT" THEN
                    InvDiscAmount := InvDiscAmount + SalesCreLine."Inv. Discount Amount" / (1 + SalesCreLine."VAT %" / 100)
                ELSE
                    InvDiscAmount := InvDiscAmount + SalesCreLine."Inv. Discount Amount";
                LineQty := LineQty + SalesCreLine.Quantity;
                IF SalesCreLine."VAT %" <> VATPercentage THEN
                    IF VATPercentage = 0 THEN
                        VATPercentage := SalesCreLine."VAT %"
                    ELSE
                        VATPercentage := -1;
            UNTIL SalesCreLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        InvDiscAmount := ROUND(InvDiscAmount, 0.01);

        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        Ligne += FormatDecimal(VATPercentage) + sep;          // % de TVA si unique sinon -1
        Ligne += FormatDecimal(CustAmount * -1) + sep;             // Base TVA
        Ligne += FormatDecimal(VATAmount * -1) + sep;              // Montant TVA
        Ligne += FormatDecimal(0) + sep;                      // Montant Accompte
        Ligne += FormatDecimal(CustAmount * -1) + sep;             // Base HT
        Ligne += FormatDecimal(AmountInclVAT * -1) + sep;          // Montant TTC
        Ligne += FormatDecimal(AmountInclVAT * -1) + sep;          // Net a payer

        Ligne += '0' + sep;                                     // Nb Colis

        // ------------------------
        // Recherche des commentaires
        // ------------------------
        CLEAR(TabCommentaire);
        i := 0;

        "Sales Comment Line".RESET();
        "Sales Comment Line".SETRANGE("Document Type", "Sales Comment Line"."Document Type"::"Posted Credit Memo");
        "Sales Comment Line".SETRANGE("No.", LEnteteAvoir."No.");
        "Sales Comment Line".SETRANGE("Print Invoice", TRUE);
        IF "Sales Comment Line".FIND('-') THEN
            REPEAT
                i += 1;
                TabCommentaire[i] := "Sales Comment Line".Comment;
                IF i = 5 THEN
                    "Sales Comment Line".FIND('+');
            UNTIL "Sales Comment Line".NEXT() = 0;
        // ---------------------------------
        // Fin de recherche des commentaires
        // ---------------------------------

        FOR i := 1 TO 5 DO
            Ligne += TabCommentaire[i] + sep;

        FichierEdi.WRITE(PurgeAccents(Ligne));

        // ----------------------------
        // Export des Lignes de remises
        // ----------------------------
        //ExportRemiseEnteteFacture(LEnteteFacture);

        // ---------------------------
        // Export des Lignes d'adresse
        // ---------------------------
        ExportAdresseFournisseur(LEnteteAvoir."No.");
        ExportAdresseClientAvoir(LEnteteAvoir, 'IV', LEnteteAvoir."Bill-to Customer No.");
        ExportAdresseClientAvoir(LEnteteAvoir, 'BY', LEnteteAvoir."Sell-to Customer No.");
        ExportAdresseClientAvoir(LEnteteAvoir, 'DP', LEnteteAvoir."Sell-to Customer No.");

        // -----------------------------
        // Export des lignes de factures
        // -----------------------------
        ExportLignesAvoir(LEnteteAvoir);

        LEnteteAvoir."EDI Exportation" := TRUE;
        LEnteteAvoir."EDI Exportation Date" := TODAY;
        LEnteteAvoir."EDI Exportation Time" := TIME;
        LEnteteAvoir."EDI Exportation User" := USERID;
        LEnteteAvoir."EDI Exportation Code" := LCodeExportation;
        LEnteteAvoir.MODIFY();
    end;

    procedure ExportLignesAvoir(LEnteteAvoir: Record "Sales Cr.Memo Header")
    var
        LignesAvoir: Record "Sales Cr.Memo Line";
        Item: Record Item;
        CommentLine: Record "Sales Cr.Memo Line";
        LineNo: Integer;
    begin

        LineNo := 0;

        LignesAvoir.RESET();
        LignesAvoir.SETRANGE("Document No.", LEnteteAvoir."No.");
        LignesAvoir.SETRANGE(Type, LignesAvoir.Type::Item);
        LignesAvoir.SETFILTER("No.", '<>%1', 'PORT');
        // +++ SPECIF SIDAMO => PORT EN TANT QU'ARTICLE +++
        IF LignesAvoir.FIND('-') THEN
            REPEAT

                // ----------------------
                // Recherche de l'article
                // ----------------------
                Item.GET(LignesAvoir."No.");

                LineNo += 1;

                Ligne := '';
                Ligne += 'LIG' + sep;
                Ligne += FormatInteger(LineNo) + sep;                            // No de ligne
                Ligne += LignesAvoir."Document No." + sep;                                      // No Facture
                                                                                                //Ligne += "Sales Shipment Header"."External Document No." + sep;                 // No commande client
                Ligne += '' + sep;                          // No commande client
                Ligne += '' + sep;                   // Date commande client
                Ligne += '' + sep;                                      // No commande client
                Ligne += '' + sep;                 // Date commande client
                Ligne += getGenCod(LignesAvoir."No.") + sep;                                     // Gencode article
                Ligne += LignesAvoir."No." + sep;                                                // Code de l'article
                Ligne += LignesAvoir.Description + sep;                                          // Désignation article
                Ligne += LignesAvoir."Description 2" + sep;                                      // Désignation article 2
                Ligne += FormatInteger(0) + sep;                                                   // Qte Commandée en EDI *** NON GERE ***
                Ligne += FormatDecimal(LignesAvoir."Quantity (Base)") + sep;                     // Qte Facturée en unité de base
                Ligne += FormatInteger(0) + sep;                                                   // Qte Livrée en EDI *** NON GERE ***
                Ligne += FormatDecimal(LignesAvoir.Quantity) + sep;                              // Qte Facturée en nb unité
                Ligne += FormatDecimal(LignesAvoir."VAT %") + sep;                               // % TVA

                // AD Le 13-02-2009 => WORMS -> 1 seule remise
                Ligne += FormatDecimal(LignesAvoir."Line Discount %") + sep;                     // % remise 1
                                                                                                 //Ligne += FormatDecimal(LignesFacture."Discount2 %") + sep;                       // % remise 2
                Ligne += FormatDecimal(0) + sep;                                                   // % remise 2

                Ligne += FormatDecimal(LignesAvoir."Unit Price") + sep;                          // Prix Brut

                // AD Le 13-02-2009 => WORMS -> Pas de prix net
                //Ligne += FormatDecimal(LignesFacture."Net Unit Price") + sep;                    // Prix Net
                Ligne += FormatDecimal(ROUND(LignesAvoir."Unit Price" * (1 - LignesAvoir."Line Discount %" / 100), 0.01)
                                                                     ) + sep;                      // Prix Net

                Ligne += FormatDecimal(LignesAvoir."VAT Base Amount") + sep;                     // Base TVA  = Montant HT
                Ligne += FormatDecimal(LignesAvoir."Amount Including VAT" -
                                       LignesAvoir."VAT Base Amount") + sep;                     // Montant TVA
                Ligne += FormatDecimal(LignesAvoir."Amount Including VAT") + sep;                // Montant TTC

                // ------------------------------------------------------------------------------------------------
                // Recherche des commentaires associés à la ligne (Utilisé en 3,70 à revoir pour les version > 5.0)
                // ------------------------------------------------------------------------------------------------
                CLEAR(TabCommentaire);
                i := 0;

                CommentLine.RESET();
                CommentLine.SETRANGE("Document No.", LignesAvoir."Document No.");
                CommentLine.SETRANGE(Type, CommentLine.Type::" ");
                CommentLine.SETRANGE("Attached to Line No.", LignesAvoir."Line No.");
                IF CommentLine.FIND('-') THEN
                    REPEAT
                        i += 1;
                        TabCommentaire[i] := CommentLine.Description + ' ' + CommentLine."Description 2";
                        IF i = 5 THEN
                            CommentLine.FIND('+');
                    UNTIL CommentLine.NEXT() = 0;
                // ---------------------------------
                // Fin de recherche des commentaires
                // ---------------------------------

                FOR i := 1 TO 5 DO
                    Ligne += TabCommentaire[i] + sep;

                FichierEdi.WRITE(PurgeAccents(Ligne));

                ExportRemiseLigneAvoir(LEnteteAvoir, LignesAvoir);

            UNTIL LignesAvoir.NEXT() = 0;
    end;

    procedure ExportRemiseLigneAvoir(LEnteteAvoir: Record "Sales Cr.Memo Header"; LLigneAvoir: Record "Sales Cr.Memo Line")
    var
      
    begin
        // Export des remises et charges de la ligne de factures
        // #####################################################


        // -------------------------
        // Export des remises 1 et 2
        // -------------------------
        // AD Le 13-02-2009 => WORMS -> 1 seule remise
        IF LLigneAvoir."Line Discount %" <> 0 THEN BEGIN
            Ligne := '';
            Ligne += 'REL' + sep;
            Ligne += LEnteteAvoir."No." + sep;                                      // No Facture
            Ligne += 'A' + sep;                                                       // Type
            Ligne += '1' + sep;                                                        // Ordre
            Ligne += 'TD' + sep;                                                       // Code remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += 'REMISE COMMERCIALE' + sep;                                       // Libellé remise  ++ ICI REMISE COMMERCIALE ++
            Ligne += FormatDecimal(LLigneAvoir."Line Discount %") + sep;             // % Remise 1
            Ligne += '0' + sep;                                                        // % Remise 2
            Ligne += '0' + sep;                                                        // % Remise 3
            Ligne += '0' + sep;                                                        // % Remise 4
            Ligne += 'P' + sep;                                                        // Valeur ou pourcentage
            Ligne += '0' + sep;                                                        // Base du pourcentage
            Ligne += '0' + sep;                                                        // Montant
            Ligne += '0' + sep;                                                        // Prix

            FichierEdi.WRITE(PurgeAccents(Ligne));
        END;

        // AD Le 13-02-2009 => WORMS -> 1 seule remise

        // ------------------------------------
        // Export de la DEEE attaché à la ligne
        // ------------------------------------
        IF CodeFraisDEEE <> '' THEN ;
    end;

    procedure ExportAdresseFournisseur(DocNo: Code[20])
    begin
        // Export les données société dans la ligne d'adresse.
        // ---------------------------------------------------

        // ------------------------
        // Verification des données
        // ------------------------
        InfoSociété.TESTFIELD("Company EDI Code");

        Ligne := '';
        Ligne += 'ADR' + sep;
        Ligne += 'FOU' + sep;                        // Type adresse
        Ligne += DocNo + sep;         // No Facture
        Ligne += InfoSociété."Company EDI Code" + sep;   // Code Ean
        Ligne += InfoSociété.Name + sep;
        Ligne += InfoSociété.Address + sep;
        Ligne += InfoSociété."Address 2" + sep;
        Ligne += InfoSociété."Post Code" + sep;
        Ligne += InfoSociété.City + sep;
        Ligne += InfoSociété."Country/Region Code" + sep;
        Ligne += InfoSociété."VAT Registration No." + sep;
        Ligne += InfoSociété."Registration No." + sep;
        Ligne += InfoSociété."Stock Capital" + sep;
        Ligne += InfoSociété."Trade Register" + sep;
        Ligne += InfoSociété."Legal Form" + sep;

        FichierEdi.WRITE(PurgeAccents(Ligne));
    end;

    procedure ExportAdresseClientFacture(LEnteteFacture: Record "Sales Invoice Header"; TypeAdresse: Code[10]; CodeClient: Code[20])
    var
        Client: Record Customer;
        LLigneFacture: Record "Sales Invoice Line";
        LBl: Record "Sales Shipment Header";
        ClientBL: Record Customer;
    begin
        // Export les données société dans la ligne d'adresse.
        // ---------------------------------------------------

        Client.GET(CodeClient);

        // ------------------------
        // Verification des données
        // ------------------------
        Client.TESTFIELD("Customer EDI Code");


        Ligne := '';
        Ligne += 'ADR' + sep;
        Ligne += TypeAdresse + sep;             // Type adresse
        Ligne += LEnteteFacture."No." + sep;    // No Facture


        // AD Le 03-04-2020 => Spécif. SYMTA. Je mets un testfield car je ne sais pas comment on devra gérer les autres cas
        //LEnteteFacture.TESTFIELD("Regroupement Facturation", LEnteteFacture."Regroupement Facturation"::"1 Facture pas donneur d'ordre");

        LLigneFacture.SETRANGE("Document No.", LEnteteFacture."No.");
        LLigneFacture.SETRANGE(Type, LLigneFacture.Type::Item);
        LLigneFacture.FINDFIRST();
        IF NOT LBl.GET(LLigneFacture."Shipment No.") THEN ERROR('A VOIR');
        ClientBL.GET(LBl."Sell-to Customer No.");
        ClientBL.TESTFIELD("Customer EDI Code");



        CASE TypeAdresse OF
            'IV':
                BEGIN
                    Ligne += Client."Customer EDI Code" + sep;            // Code Ean
                    Ligne += LEnteteFacture."Bill-to Name" + sep;
                    Ligne += LEnteteFacture."Bill-to Address" + sep;
                    Ligne += LEnteteFacture."Bill-to Address 2" + sep;
                    Ligne += LEnteteFacture."Bill-to Post Code" + sep;
                    Ligne += LEnteteFacture."Bill-to City" + sep;
                    Ligne += LEnteteFacture."Bill-to Country/Region Code" + sep;
                    Ligne += Client."VAT Registration No." + sep;
                END;
            'BY':
                BEGIN
                    Ligne += ClientBL."Customer EDI Code" + sep;            // Code Ean
                    Ligne += LBl."Sell-to Customer Name" + sep;
                    Ligne += LBl."Sell-to Address" + sep;
                    Ligne += LBl."Sell-to Address 2" + sep;
                    Ligne += LBl."Sell-to Post Code" + sep;
                    Ligne += LBl."Sell-to City" + sep;
                    Ligne += LBl."Sell-to Country/Region Code" + sep;
                    Ligne += ClientBL."VAT Registration No." + sep;
                END;
            'DP':
                BEGIN
                    Ligne += ClientBL."Customer EDI Code" + sep;            // Code Ean
                    Ligne += LBl."Sell-to Customer Name" + sep;
                    Ligne += LBl."Sell-to Address" + sep;
                    Ligne += LBl."Sell-to Address 2" + sep;
                    Ligne += LBl."Sell-to Post Code" + sep;
                    Ligne += LBl."Sell-to City" + sep;
                    Ligne += LBl."Sell-to Country/Region Code" + sep;
                    Ligne += ClientBL."VAT Registration No." + sep;
                END;

            ELSE
                ERROR(Text010Err, TypeAdresse)
        END;



        Ligne += '' + sep;
        Ligne += '' + sep;
        Ligne += '' + sep;
        Ligne += '' + sep;

        FichierEdi.WRITE(PurgeAccents(Ligne));
    end;

    procedure ExportAdresseClientAvoir(LEnteteAvoir: Record "Sales Cr.Memo Header"; TypeAdresse: Code[10]; CodeClient: Code[20])
    var
        Client: Record Customer;
    begin
        // Export les données société dans la ligne d'adresse.
        // ---------------------------------------------------

        Client.GET(CodeClient);

        // ------------------------
        // Verification des données
        // ------------------------
        Client.TESTFIELD("Customer EDI Code");


        Ligne := '';
        Ligne += 'ADR' + sep;
        Ligne += TypeAdresse + sep;             // Type adresse
        Ligne += LEnteteAvoir."No." + sep;    // No Facture
        Ligne += Client."Customer EDI Code" + sep;            // Code Ean

        CASE TypeAdresse OF
            'IV':
                BEGIN
                    Ligne += LEnteteAvoir."Bill-to Name" + sep;
                    Ligne += LEnteteAvoir."Bill-to Address" + sep;
                    Ligne += LEnteteAvoir."Bill-to Address 2" + sep;
                    Ligne += LEnteteAvoir."Bill-to Post Code" + sep;
                    Ligne += LEnteteAvoir."Bill-to City" + sep;
                    Ligne += LEnteteAvoir."Bill-to Country/Region Code" + sep;
                END;
            'BY':
                BEGIN
                    Ligne += LEnteteAvoir."Sell-to Customer Name" + sep;
                    Ligne += LEnteteAvoir."Sell-to Address" + sep;
                    Ligne += LEnteteAvoir."Sell-to Address 2" + sep;
                    Ligne += LEnteteAvoir."Sell-to Post Code" + sep;
                    Ligne += LEnteteAvoir."Sell-to City" + sep;
                    Ligne += LEnteteAvoir."Sell-to Country/Region Code" + sep;
                END;
            'DP':
                BEGIN
                    Ligne += LEnteteAvoir."Sell-to Customer Name" + sep;
                    Ligne += LEnteteAvoir."Sell-to Address" + sep;
                    Ligne += LEnteteAvoir."Sell-to Address 2" + sep;
                    Ligne += LEnteteAvoir."Sell-to Post Code" + sep;
                    Ligne += LEnteteAvoir."Sell-to City" + sep;
                    Ligne += LEnteteAvoir."Sell-to Country/Region Code" + sep;
                END;

            ELSE
                ERROR(Text010Err, TypeAdresse)
        END;


        Ligne += Client."VAT Registration No." + sep;
        Ligne += '' + sep;
        Ligne += '' + sep;
        Ligne += '' + sep;
        Ligne += '' + sep;

        FichierEdi.WRITE(PurgeAccents(Ligne));
    end;

    procedure "--- Formatage des données ---"()
    begin
    end;

    procedure FormatBoolean(Value: Boolean): Text[1]
    begin
        EXIT(FORMAT(Value, 0, 2));
    end;

    procedure FormatInteger(Value: Integer): Text[30]
    begin
        EXIT(FORMAT(Value));
    end;

    procedure FormatDecimal(Value: Decimal): Text[50]
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals><Comma,,>'));
    end;

    procedure FormatDate(Value: Date): Text[10]
    begin
        EXIT(FORMAT(Value, 0, '<Year4>-<Month,2>-<Day,2>'));
    end;

    procedure "--- Divers ---"()
    begin
    end;

    procedure PurgeAccents(chaine: Text[1024]): Text[1024]
    var
        NewChaine: Text[1024];
    begin
        NewChaine := CONVERTSTR(chaine, 'àäéèêëïîöô', 'aaeeeeiioo');
        EXIT(NewChaine);
    end;

    procedure "--- Spécifique ---"()
    begin
    end;

    procedure getGenCod(numArt: Code[20]) gcod: Code[20]
    var
        Item: Record Item;
    begin
        // Cette fonction renvoi le gencod d'un article
        Item.GET(numArt);

        EXIT(Item."Gencod EAN13");
    end;

    procedure ExportFactureSCAR(LEnteteFacture: Record "Sales Invoice Header"; LCodeExportation: Code[10]; _pMois: Code[6])
    var
        LClient: Record Customer;
        LSalesInvLine: Record "Sales Invoice Line";
         CliLiv: Record Customer;
        CustAmount: Decimal;
        AmountInclVAT: Decimal;
        LMtPortHt: Decimal;
        LMtPortTTC: Decimal;
        VATAmount: Decimal;
       
    begin

        LClient.GET(LEnteteFacture."Sell-to Customer No.");
        LEnteteFacture.CALCFIELDS("Amount Including VAT", Amount);

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        LSalesInvLine.RESET();
        LSalesInvLine.SETRANGE("Document No.", LEnteteFacture."No.");
        LSalesInvLine.SETFILTER(Type, '>0');

        IF LSalesInvLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + LSalesInvLine.Amount;
                AmountInclVAT := AmountInclVAT + LSalesInvLine."Amount Including VAT";
            UNTIL LSalesInvLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        // ----------------------------
        // Recherche du montant du port
        // ----------------------------
        LSalesInvLine.RESET();
        LSalesInvLine.SETRANGE("Document No.", LEnteteFacture."No.");
        LSalesInvLine.SETRANGE("Internal Line Type", LSalesInvLine."Internal Line Type"::Shipment);
        IF LSalesInvLine.FindSet() THEN
            REPEAT
                LMtPortHt += LSalesInvLine.Amount;
                LMtPortTTC += LSalesInvLine."Amount Including VAT";
            UNTIL LSalesInvLine.NEXT() = 0;
        // ----------------------------
        // FIN Recherche du montant du port
        // ----------------------------

        //----------------------------------------------------------------------------------------------------------------------------------
        // WF le 06/03/2012 => Bug si le client n'existe pas, on ajout le test et initialise "Libre 3" à vide
        //CliLiv.GET(LSalesInvLine."N° client Livré");
        IF NOT CliLiv.GET(LSalesInvLine."N° client Livré") THEN CliLiv."Libre 3" := '';
        //Fin WF le 06/03/2012
        //----------------------------------------------------------------------------------------------------------------------------------

        Ligne := '';
        Ligne += '000206';
        Ligne += _pMois;
        Ligne += 'FV' + COPYSTR(LEnteteFacture."No.", 5, 8); // METTRE SUR 10 CARACTERES
        Ligne += PADSTR(CliLiv."Libre 3", 6);
        Ligne += 'F';
        Ligne += FormatDecimal(AmountInclVAT); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(CustAmount - LMtPortHt); // METTRE SUR 10 CARACTERE
        Ligne += '          '; // 10 ESPACES
        Ligne += FormatDecimal(VATAmount); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(LMtPortHt);
        Ligne += FORMAT(LEnteteFacture."Due Date", 0, '<Year4><Month,2><Day,2>');
        Ligne += '   '; // 3 ESPACES
        Ligne += FormatDecimal(LMtPortTTC - LMtPortHt);
        //Ligne += '............................';


        FichierEdi.WRITE(PurgeAccents(Ligne));



        LEnteteFacture."EDI Exportation" := TRUE;
        LEnteteFacture."EDI Exportation Date" := TODAY;
        LEnteteFacture."EDI Exportation Time" := TIME;
        LEnteteFacture."EDI Exportation User" := USERID;
        LEnteteFacture."EDI Exportation Code" := LCodeExportation;
        LEnteteFacture.MODIFY()
    end;

    procedure ExportAvoirSCAR(LEnteteAvoir: Record "Sales Cr.Memo Header"; LCodeExportation: Code[20]; _pMois: Code[6])
    var
        LSalesCreLine: Record "Sales Cr.Memo Line";
         CliLiv: Record Customer;
        LClient: Record Customer;
        LMtPortHt: Decimal;
        LMtPortTTC: Decimal;
        CustAmount: Decimal;
        AmountInclVAT: Decimal;
        VATAmount: Decimal;
       
    begin
        Ligne := '';

        LClient.GET(LEnteteAvoir."Sell-to Customer No.");
        LEnteteAvoir.CALCFIELDS("Amount Including VAT", Amount);

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        LSalesCreLine.RESET();
        LSalesCreLine.SETRANGE("Document No.", LEnteteAvoir."No.");
        LSalesCreLine.SETFILTER(Type, '>0');
        IF LSalesCreLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + LSalesCreLine.Amount;
                AmountInclVAT := AmountInclVAT + LSalesCreLine."Amount Including VAT";
            UNTIL LSalesCreLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        // ----------------------------
        // Recherche du montant du port
        // ----------------------------
        LSalesCreLine.RESET();
        LSalesCreLine.SETRANGE("Document No.", LEnteteAvoir."No.");
        LSalesCreLine.SETRANGE("Internal Line Type", LSalesCreLine."Internal Line Type"::Shipment);
        IF LSalesCreLine.FindSet() THEN
            REPEAT
                LMtPortHt += LSalesCreLine.Amount;
                LMtPortTTC += LSalesCreLine."Amount Including VAT";
            UNTIL LSalesCreLine.NEXT() = 0;
        // ----------------------------
        // FIN Recherche du montant du port
        // ----------------------------

        CliLiv.GET(LSalesCreLine."N° client Livré");

        Ligne := '';
        Ligne += '000206';
        Ligne += _pMois;
        Ligne += 'AV' + COPYSTR(LEnteteAvoir."No.", 5, 8); // METTRE SUR 10 CARACTERES
        Ligne += PADSTR(CliLiv."Libre 3", 6);

        Ligne += 'A';
        Ligne += FormatDecimal(AmountInclVAT); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(CustAmount - LMtPortHt); // METTRE SUR 10 CARACTERE
        Ligne += '          '; // 10 ESPACES
        Ligne += FormatDecimal(VATAmount); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(LMtPortHt);
        Ligne += FORMAT(LEnteteAvoir."Due Date", 0, '<Year4><Month,2><Day,2>');
        Ligne += '   '; // 3 ESPACES
        Ligne += FormatDecimal(LMtPortTTC - LMtPortHt);
        //Ligne += '............................';


        FichierEdi.WRITE(PurgeAccents(Ligne));

        LEnteteAvoir."EDI Exportation" := TRUE;
        LEnteteAvoir."EDI Exportation Date" := TODAY;
        LEnteteAvoir."EDI Exportation Time" := TIME;
        LEnteteAvoir."EDI Exportation User" := USERID;
        LEnteteAvoir."EDI Exportation Code" := LCodeExportation;
        LEnteteAvoir.MODIFY()
    end;
}

