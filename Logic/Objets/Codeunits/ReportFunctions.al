codeunit 50063 ReportFunctions
{
    // GetCorrespontName : donne la chaine a afficher comme correspondant (mettre '' en responsable si pas de responsable a afficher)


    trigger OnRun()
    begin
    end;

    procedure GetCorrespondantName(UserName: Text; ResponsableName: Text) Correspondant: Text[250]
    var
        User: Record User;
        "User Setup": Record "User Setup";
    begin

        Correspondant := UserName;
        // Recherche Info Utilisateur
        CLEAR(User);
        User.SETRANGE("User Name", UserName);
        IF User.FINDLAST() THEN;
        Correspondant := User."Full Name";

        IF Correspondant = '' THEN
            Correspondant := UserName;

        // Recherche info Responsable
        IF ResponsableName <> '' THEN BEGIN
            CLEAR("User Setup");
            IF "User Setup".GET(ResponsableName) THEN;
            BEGIN
                IF "User Setup"."E-Mail" <> '' THEN
                    Correspondant += ' - ' + "User Setup"."E-Mail";
                IF "User Setup"."Phone No." <> '' THEN
                    Correspondant += ' - ' + "User Setup"."Phone No.";
            END;
        END;
    end;
}

