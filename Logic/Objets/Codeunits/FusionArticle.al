codeunit 50014 "Fusion Article"
{
    trigger OnRun()
    begin
        FusionnerConso('SP000489', 'SP000492', TRUE);
    end;

    var
        ESK001Err: Label 'L''article %1 ne doit être dans un kit !', Comment = '%1 = Article Origine No';
        ESK002Err: Label 'Les stocks, attendus, réservés de l''article %1 doivent être à 0 !' , Comment = '%1 = Article No';
        ESK003Err: Label 'Les réceptions de l''article %1 doivent être facturée !', Comment = '%1 = Article  No';
        ESK004Err: Label 'L''article %1 a des équivalences !' , Comment = '%1 = Article Origine No';
        ESK005Err: Label 'L''article %1 ne doit être un kit !' , Comment = '%1 = Article Origine No';
        ESK006Lbl: Label 'Attention, l''article d''origine est réceptionné mais non facturé. Voulez-vous tout de même fusionner ?';

    procedure FusionnerArticle(_pCodeArticleOrigine: Code[20]; _pCodeArticleDestination: Code[20])
    var
        ItemOrigine: Record Item;
        ItemDestination: Record Item;
        ProdBOMLine: Record "Production BOM Line";
        PurchRcptLine: Record "Purch. Rcpt. Line";
        BinContent: Record "Bin Content";
        ItemJnlLine: Record "Item Journal Line";
        LCommentLine: Record "Comment Line";
        LBonComponent: Record "BOM Component";
        ItemSubstitutionDest: Record "Item Substitution";
        ItemSubstitutionOrigine: Record "Item Substitution";
        WhseJnlLine: Record "Warehouse Journal Line";
        MultiRefOrigine: Record "Item Reference";
        LItemVendor: Record "Item Vendor";
        MultiRefDest: Record "Item Reference";
        ItemJnlPostLine: Codeunit "Item Jnl.-Post Line";
        CduLFunctions: Codeunit "Codeunits Functions";
        WMSMngt: Codeunit "WMS Management";
        WhseJnlPostLine: Codeunit "Whse. Jnl.-Register Line";
        AncienneActive: Code[20];
        CodeEmplacementDest: Code[10];
        CodeZoneDest: Code[10];
        LAnciennePubliable: Boolean;
        ESK000Err: Label 'Fusion impossible car quantité en surstock';

    begin
        ItemOrigine.GET(_pCodeArticleOrigine);
        ItemDestination.GET(_pCodeArticleDestination);


        // AD Le 04-12-2012
        TesterStockMiniload(ItemOrigine."No.");

        // AD Le 12-01-2016 => MIG2015
        ItemOrigine.TESTFIELD("Production BOM No.", '');
        CLEAR(LBonComponent);
        LBonComponent.SETRANGE("Parent Item No.", ItemOrigine."No.");
        IF NOT LBonComponent.ISEMPTY THEN ERROR(ESK005Err, ItemOrigine."No.");

        // Modification de la fiche article //
        AncienneActive := ItemOrigine."No. 2";

        // CFR le 28/09/2023 - R‚gie : Fusion article > suivi [No. 2] = [Ref active]
        ItemOrigine."No. 2" := COPYSTR('FUS' + ItemOrigine."No. 2", 1, MAXSTRLEN(ItemOrigine."No. 2"));
        CLEAR(LItemVendor);
        LItemVendor.SETCURRENTKEY("Item No.", "Variant Code", "Vendor No.");
        LItemVendor.SETRANGE("Item No.", ItemOrigine."No.");
        LItemVendor.MODIFYALL("Ref. Active", ItemOrigine."No. 2");
        // FIN CFR le 28/09/2023

        ItemOrigine.Stocké := FALSE;
        // MCO Le 07-06-2017 => Régie
        LAnciennePubliable := (ItemOrigine.Publiable <> ItemOrigine.Publiable::Non);
        ItemOrigine.Publiable := ItemOrigine.Publiable::Non;
        // FIN MCO Le 07-06-2017 => Régie
        ItemOrigine.MODIFY();

        // AD Le 06-02-2015
        CLEAR(LCommentLine);
        LCommentLine.SETRANGE("Table Name", LCommentLine."Table Name"::Item);
        LCommentLine.SETRANGE("No.", ItemOrigine."No.");
        IF LCommentLine.FINDLAST() THEN
            LCommentLine."Line No." += 10000
        ELSE
            LCommentLine."Line No." := 10000;

        LCommentLine."Table Name" := LCommentLine."Table Name"::Item;
        LCommentLine."No." := ItemOrigine."No.";

        LCommentLine.Date := WORKDATE();
        LCommentLine.Comment := 'Fusionner sur ' + ItemDestination."No." + ' par ' + USERID;
        LCommentLine.Insert();
        // FIN AD Le 06-02-2015

        // Gestion des dates de dernière entrée. => REGLE SYMTA
        ItemOrigine.CALCFIELDS(Inventory);
        ItemDestination.CALCFIELDS(Inventory);

        IF (ItemOrigine.Inventory = 0) AND (ItemDestination.Inventory <> 0) THEN; // On laisse date de B
        IF (ItemOrigine.Inventory <> 0) AND (ItemDestination.Inventory = 0) THEN  // On met date de A
          BEGIN
            ItemDestination."Date dernière entrée" := ItemOrigine."Date dernière entrée";
            ItemDestination.MODIFY();
        END;
        IF (ItemOrigine.Inventory <> 0) AND (ItemDestination.Inventory <> 0) THEN  // On met la date la + récente
          BEGIN
            IF ItemOrigine."Date dernière entrée" > ItemDestination."Date dernière entrée" THEN BEGIN
                ItemDestination."Date dernière entrée" := ItemOrigine."Date dernière entrée";
                ItemDestination.MODIFY();
            END;
        END;


        // MCO Le 07-06-2017 => Incompréhension entre nous et le client
        /*
        //GB LE 12-04-2017 : Ticket 22256: L'article fusion ne doit pas être publiable
        ItemDestination.Publiable:=ItemDestination.Publiable::Non;
        ItemDestination.MODIFY();
        //FIN GB Le 12-04-2017
        */
        // FIN MCO Le 07-06-2017 => Incompréhension entre nous et le client


        // Demande de LM (Nouveauté par rapport à DBX
        ItemSubstitutionOrigine.RESET();
        ItemSubstitutionOrigine.SETRANGE(Type, ItemSubstitutionOrigine.Type::Item);
        ItemSubstitutionOrigine.SETRANGE("No.", ItemOrigine."No.");
        IF ItemSubstitutionOrigine.FINDFIRST() THEN
            ERROR(ESK004Err, ItemOrigine."No.");

        // Vérification si l'article est dans un kit //
        ProdBOMLine.RESET();
        ProdBOMLine.SETRANGE(Type, ProdBOMLine.Type::Item);
        ProdBOMLine.SETRANGE("No.", ItemOrigine."No.");
        IF ProdBOMLine.FINDFIRST() THEN
            ERROR(ESK001Err, ItemOrigine."No.");

        // AD Le 12-01-2016 => MIG2015
        CLEAR(LBonComponent);
        LBonComponent.RESET();
        LBonComponent.SETRANGE(Type, LBonComponent.Type::Item);
        LBonComponent.SETRANGE("No.", ItemOrigine."No.");
        IF LBonComponent.FINDFIRST() THEN
            ERROR(ESK001Err, ItemOrigine."No.");


        // Vérification si 0 //
        ItemOrigine.CALCFIELDS(ItemOrigine."Qty. on Purch. Order", ItemOrigine."Qty. on Sales Order");
        IF (ItemOrigine."Qty. on Purch. Order" <> 0) OR
           (ItemOrigine."Qty. on Sales Order" <> 0) THEN
            ERROR(ESK002Err, ItemOrigine."No.");

        // Vérification si pas de reception à facturer //
        PurchRcptLine.RESET();
        PurchRcptLine.SETRANGE(Type, PurchRcptLine.Type::Item);
        PurchRcptLine.SETRANGE("No.", ItemOrigine."No.");
        PurchRcptLine.SETFILTER("Qty. Rcd. Not Invoiced", '<>%1', 0);
        IF PurchRcptLine.FINDFIRST() THEN
            // MCO Le 14-02-2018 => Régie : Demande de Nathalie
            IF NOT CONFIRM(ESK006Lbl) THEN
                ERROR(ESK003Err, ItemOrigine."No.");

        // Vérification de l'article de destination //
        ItemDestination.TESTFIELD("Production BOM No.", '');
        ItemDestination.TESTFIELD("Sans Mouvements de Stock", FALSE);


        // Transfert du stock //


        // MCO Le 20-11-2018 => Demande de SYMTA
        BinContent.RESET();
        BinContent.SETRANGE("Item No.", ItemOrigine."No.");
        BinContent.SETRANGE(Default, FALSE);
        IF BinContent.FINDSET() THEN
            REPEAT
                BinContent.CALCFIELDS(Quantity);
                IF BinContent.Quantity <> 0 THEN
                    ERROR(ESK000Err);
            UNTIL BinContent.NEXT() = 0;
        // FIN MCO Le 20-11-2018

        ItemOrigine.CALCFIELDS(Inventory);
        IF ItemOrigine.Inventory <> 0 THEN BEGIN
            BinContent.RESET();
            BinContent.SETRANGE("Item No.", ItemOrigine."No.");
            IF BinContent.FINDFIRST() THEN
                REPEAT
                    BinContent.CALCFIELDS(Quantity);
                    IF BinContent.Quantity <> 0 THEN BEGIN
                        /* // Demande de Laurent
                          BinContentDest.RESET();
                          BinContentDest.SETRANGE("Location Code", BinContent."Location Code");
                          BinContentDest.SETRANGE("Bin Code", BinContent."Bin Code");
                          BinContentDest.SETRANGE("Item No.", BinContent."Item No.");
                          BinContentDest.SETRANGE("Unit of Measure Code", BinContent."Unit of Measure Code");
                          IF NOT BinContentDest.FINDFIRST () THEN
                            BEGIN
                              BinContentDest.SETRANGE("Bin Code");
                              BinContent.SETRANGE(Default, TRUE);
                              Defaut := NOT BinContentDest.FINDFIRST();
                              BinContentDest := BinContent;
                              BinContentDest.VALIDATE("Item No.", ItemDestination."No.");
                              BinContentDest.VALIDATE("Unit of Measure Code", ItemDestination."Base Unit of Measure");
                              BinContentDest.VALIDATE(Default, Defaut);
                              BinContentDest.Insert();
                            END;
                        */
                        // Lecture de l'emplacement
                        CduLFunctions.ESK_GetDefaultBin(ItemDestination."No.", '', 'SP', CodeEmplacementDest, CodeZoneDest);


                        CLEAR(ItemJnlLine);
                        ItemJnlLine.VALIDATE("Entry Type", ItemJnlLine."Entry Type"::"Positive Adjmt.");
                        ItemJnlLine.VALIDATE("Document No.", 'FUS' + ItemOrigine."No.");
                        ItemJnlLine.VALIDATE("Item No.", ItemDestination."No.");
                        ItemJnlLine.VALIDATE("Posting Date", WORKDATE());
                        ItemJnlLine.VALIDATE("Location Code", BinContent."Location Code");
                        ItemJnlLine.VALIDATE(Quantity, BinContent.Quantity);
                        ItemJnlLine.VALIDATE("Bin Code", CodeEmplacementDest);
                        ItemJnlLine.VALIDATE("Unit Cost", ItemOrigine."Unit Cost");
                        ItemJnlLine.Description := STRSUBSTNO('Fusion de l''article %1 !', ItemOrigine."No.");

                        ItemJnlPostLine.RunWithCheck(ItemJnlLine);

                        WhseJnlLine.INIT();
                        CLEAR(WMSMngt);
                        WMSMngt.CreateWhseJnlLine(ItemJnlLine, 0, WhseJnlLine, FALSE);
                        CLEAR(WhseJnlPostLine);
                        WhseJnlPostLine.RUN(WhseJnlLine);


                        CLEAR(ItemJnlLine);
                        ItemJnlLine.VALIDATE("Entry Type", ItemJnlLine."Entry Type"::"Negative Adjmt.");
                        ItemJnlLine.VALIDATE("Document No.", 'FUS' + ItemDestination."No.");
                        ItemJnlLine.VALIDATE("Item No.", ItemOrigine."No.");
                        ItemJnlLine.VALIDATE("Posting Date", WORKDATE());
                        ItemJnlLine.VALIDATE("Location Code", BinContent."Location Code");
                        ItemJnlLine.VALIDATE(Quantity, BinContent.Quantity);
                        ItemJnlLine.VALIDATE("Bin Code", BinContent."Bin Code");
                        ItemJnlLine.Description := STRSUBSTNO('Fusion de l''article %1 !', ItemDestination."No.");
                        ItemJnlPostLine.RunWithCheck(ItemJnlLine);

                        WhseJnlLine.INIT();
                        CLEAR(WMSMngt);
                        WMSMngt.CreateWhseJnlLine(ItemJnlLine, 0, WhseJnlLine, FALSE);
                        CLEAR(WhseJnlPostLine);
                        WhseJnlPostLine.RUN(WhseJnlLine);

                    END;

                UNTIL BinContent.NEXT() = 0;
        END;

        // Transfert des consommations //
        FusionnerConso(_pCodeArticleOrigine, _pCodeArticleDestination, FALSE);

        // Transfert des multi-références //
        MultiRefOrigine.RESET();
        MultiRefOrigine.SETRANGE("Item No.", ItemOrigine."No.");
        IF MultiRefOrigine.FINDSET() THEN
            REPEAT
                MultiRefDest := MultiRefOrigine;
                MultiRefOrigine.DELETE(TRUE);
                MultiRefDest.VALIDATE("Item No.", ItemDestination."No.");
                MultiRefDest."Reference Type" := MultiRefDest."Reference Type"::"Ste Fusion";
                MultiRefDest."Reference Type No." := ItemOrigine."No.";

                MultiRefDest.INSERT(TRUE);
            UNTIL MultiRefOrigine.NEXT() = 0;

        CLEAR(MultiRefDest);
        MultiRefDest.VALIDATE("Item No.", ItemDestination."No.");
        MultiRefDest.VALIDATE("Reference Type", MultiRefDest."Reference Type"::"Ancienne Active");
        ;
        MultiRefDest.VALIDATE("Reference No.", 'FUS' + ItemOrigine."No.");
        MultiRefDest.INSERT(TRUE);

        CLEAR(MultiRefDest);
        MultiRefDest.VALIDATE("Item No.", ItemDestination."No.");
        MultiRefDest.VALIDATE("Reference Type", MultiRefDest."Reference Type"::"Ancienne Active");
        ;
        MultiRefDest.VALIDATE("Reference No.", AncienneActive);
        // MCO Le 07-06-2017 => Régie
        MultiRefDest.VALIDATE("publiable web", LAnciennePubliable);
        // FIN MCO Le 07-06-2017 => Régie
        IF MultiRefDest.INSERT(TRUE) THEN;



        // Création des équivalences //
        ItemSubstitutionOrigine.RESET();
        ItemSubstitutionOrigine.SETRANGE(Type, ItemSubstitutionOrigine.Type::Item);
        ItemSubstitutionOrigine.SETRANGE("No.", ItemOrigine."No.");
        ItemSubstitutionOrigine.SETFILTER("Substitute No.", '<>%1', ItemDestination."No.");
        IF ItemSubstitutionOrigine.FINDFIRST() THEN
            REPEAT
                ItemSubstitutionDest.RESET();
                ItemSubstitutionDest.VALIDATE(Type, ItemSubstitutionDest.Type::Item);
                ItemSubstitutionDest.VALIDATE("No.", ItemDestination."No.");
                ItemSubstitutionDest.VALIDATE("Substitute Type", ItemSubstitutionDest."Substitute Type"::Item);
                ItemSubstitutionDest.VALIDATE("Substitute No.", ItemSubstitutionOrigine."Substitute No.");
                ItemSubstitutionDest.VALIDATE(Interchangeable, FALSE);
                IF ItemSubstitutionDest.INSERT(TRUE) THEN;

                ItemSubstitutionOrigine.DELETE();
            UNTIL ItemSubstitutionOrigine.NEXT() = 0;


        ItemSubstitutionDest.RESET();
        ItemSubstitutionDest.VALIDATE(Type, ItemSubstitutionDest.Type::Item);
        ItemSubstitutionDest.VALIDATE("No.", ItemOrigine."No.");
        ItemSubstitutionDest.VALIDATE("Substitute Type", ItemSubstitutionDest."Substitute Type"::Item);
        ItemSubstitutionDest.VALIDATE("Substitute No.", ItemDestination."No.");
        ItemSubstitutionDest.VALIDATE(Interchangeable, FALSE);
        IF ItemSubstitutionDest.INSERT(TRUE) THEN;

    end;

    procedure FusionnerConso(_pArticleOrigine: Code[20]; _pArticleDestination: Code[20]; _SupprimerAncienneConso: Boolean)
    var
        LStatsConsoOrigine: Record "Stats Conso";
        LStatsConsoDestination: Record "Stats Conso";
        LCommentLineFrom: Record "Comment Line";
        LCommentLineTo: Record "Comment Line";
    begin

        LStatsConsoOrigine.RESET();
        LStatsConsoOrigine.SETRANGE("Item No.", _pArticleOrigine);

        //LM le 21-05-2013 =>ajout des consommations en plus des ventes
        //LStatsConsoOrigine.SETRANGE("Item Ledger Entry Type", LStatsConsoOrigine."Item Ledger Entry Type"::Sale);
        LStatsConsoOrigine.SETFILTER("Item Ledger Entry Type", '%1|%2',
        LStatsConsoOrigine."Item Ledger Entry Type"::Consumption,
        LStatsConsoOrigine."Item Ledger Entry Type"::Sale);

        IF LStatsConsoOrigine.FINDFIRST() THEN
            REPEAT
                IF LStatsConsoDestination.GET(_pArticleDestination, LStatsConsoOrigine."Posting Date",
                        LStatsConsoOrigine."Item Ledger Entry Type", LStatsConsoOrigine."Location Code") THEN BEGIN
                    LStatsConsoDestination."Valued Quantity" += LStatsConsoOrigine."Valued Quantity";
                    LStatsConsoDestination.MODIFY();
                END
                ELSE BEGIN
                    LStatsConsoDestination := LStatsConsoOrigine;
                    LStatsConsoDestination.VALIDATE("Item No.", _pArticleDestination);
                    LStatsConsoDestination.Insert();
                END;

                IF _SupprimerAncienneConso THEN
                    LStatsConsoOrigine.DELETE();

            UNTIL LStatsConsoOrigine.NEXT() = 0;

        // CFR le 27/09/2023 - R‚gie : ajout commentaire copie [Stats conso]
        CLEAR(LCommentLineFrom);
        LCommentLineFrom.SETRANGE("Table Name", LCommentLineFrom."Table Name"::Item);
        LCommentLineFrom.SETRANGE("No.", _pArticleOrigine);
        IF LCommentLineFrom.FINDLAST() THEN
            LCommentLineFrom."Line No." += 10000
        ELSE
            LCommentLineFrom."Line No." := 10000;

        LCommentLineFrom."Table Name" := LCommentLineFrom."Table Name"::Item;
        LCommentLineFrom."No." := _pArticleOrigine;
        LCommentLineFrom.Date := WORKDATE();
        LCommentLineFrom.Comment := 'Copie [Stats Conso] sur ' + _pArticleDestination + ' par ' + USERID;
        LCommentLineFrom.Insert();
        IF _SupprimerAncienneConso THEN BEGIN
            LCommentLineFrom."Line No." += 10000;
            LCommentLineFrom.Comment := 'Suppression [Stats Conso] par ' + USERID;
            LCommentLineFrom.INSERT();
        END;

        // CFR le 25/10/2023 - R‚gie : Correction gestion des messages
        LCommentLineTo.SETRANGE("Table Name", LCommentLineTo."Table Name"::Item);
        LCommentLineTo.SETRANGE("No.", _pArticleDestination);
        IF LCommentLineTo.FINDLAST() THEN
            LCommentLineTo."Line No." += 10000
        ELSE
            LCommentLineTo."Line No." := 10000;

        LCommentLineTo."Table Name" := LCommentLineTo."Table Name"::Item;
        LCommentLineTo."No." := _pArticleDestination;
        LCommentLineTo.Date := WORKDATE();
        LCommentLineTo.Comment := 'Copie [Stats Conso] à partir de ' + _pArticleOrigine + ' par ' + USERID;
        LCommentLineTo.INSERT();

        // FIN CFR le 27/09/2023   
    end;

    procedure TesterStockMiniload(_pItemNo: Code[20])
    var
        LInventorySetup: Record "Inventory Setup";
        LBinContent: Record "Bin Content";
    begin
        // Recup de l'emplacement Miniload
        LInventorySetup.GET();

        LBinContent.RESET();
        LBinContent.SETRANGE("Item No.", _pItemNo);
        //LBinContent.SETRANGE("Bin Code", LInventorySetup."Code emplacement MiniLoad");
        LBinContent.SETRANGE(LBinContent.Default, FALSE);
        IF LBinContent.FINDFIRST() THEN
            REPEAT
                LBinContent.CALCFIELDS(Quantity);
                IF LBinContent.Quantity <> 0 THEN
                    ERROR('STOCK sur DEPOT, Fusion impossible');

            UNTIL LBinContent.NEXT() = 0;
    end;

    // CFR le 27/09/2023 - R‚gie : ajout commentaire copie [Stats conso]
    // CFR le 28/09/2023 - R‚gie : Fusion article > suivi [No. 2] = [Ref active]
    // CFR le 25/10/2023 - R‚gie : Correction gestion des messages
}

