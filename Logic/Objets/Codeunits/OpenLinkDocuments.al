codeunit 50005 "Open Link Documents"
{

    trigger OnRun()
    begin
    end;

    var
     Fichiers: Record File;
        FrmFichiers: Page "Liste Fichiers / Répertoires";
        Lien: Text[1024];
        TextPhotoTxt: Label '\\MACGRAPH\partages\photos_emails\Photos 2009\BD\';
        Filtre: Text[1024];
       

    procedure OuvrePhotoProduit(item: Record Item)
    begin
        // Photo de l'article

        Lien := TextPhotoTxt;
        Filtre := item."No." + '*' + '-dt.sjp';

        Fichiers.RESET();
        Fichiers.SETRANGE("Is a file", TRUE);
        Fichiers.SETRANGE(Path, Lien);
        Fichiers.SETFILTER(Name, '%1', Filtre);

        FrmFichiers.SETTABLEVIEW(Fichiers);
        FrmFichiers.RUNMODAL();
    end;
}

