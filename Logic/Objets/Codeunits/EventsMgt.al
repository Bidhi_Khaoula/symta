codeunit 59999 "Events Mgt."
{
    [EventSubscriber(ObjectType::Table, Database::Customer, OnAfterOnInsert, '', false, false)]
    local procedure T18_OnAfterOnInsert(var Customer: Record Customer; xCustomer: Record Customer)
    var
        Param: Record "Generals Parameters";
    begin
        //CFR le 22/09/2021 => R‚gie : date de cr‚ation
        Customer."Create Date" := TODAY();
        // AD Le 28-10-2009 => FACTURATION -> Type de facturation par défaut
        Param.RESET();
        Param.SETRANGE(Type, 'CLI_T_FAC');
        Param.SETRANGE(Default, TRUE);
        IF Param.FINDFIRST() THEN
            Customer.VALIDATE("Invoice Type", Param.Code);
        // FIN AD Le 07-09-2009

        // CJ Le 05-05-2010 => Conditions de relance STD par défaut
        Param.RESET();
        Param.SETRANGE(Type, 'CLI_T_REL');
        Param.SETRANGE(Default, TRUE);
        IF Param.FINDFIRST() THEN
            Customer.VALIDATE("Reminder Terms Code", Param.Code);
        //FIN CJ


        // FIN Le 28-10-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeIsContactUpdateNeeded', '', false, false)]
    local procedure T18_OnBeforeIsContactUpdateNeeded(Customer: Record Customer; xCustomer: Record Customer; ForceUpdateContact: Boolean; var UpdateNeeded: Boolean)
    var
        CustContUpdate: Codeunit "CustCont-Update";
    begin
        UpdateNeeded := UpdateNeeded OR
                  //  (Customer."Centrale Active" <> xCustomer."Centrale Active") OR
                  (Customer.Supprimé <> xCustomer.Supprimé)
                  // MCO Le 23-03-2016 => Gestion du mobile
                  OR (Customer."Mobile Phone No." <> xCustomer."Mobile Phone No.");
        // FIN MCO Le 23-03-2016 => Gestion du mobile
        if not UpdateNeeded and not Customer.IsTemporary then
            UpdateNeeded := CustContUpdate.ContactNameIsBlank(Customer."No.");

        if ForceUpdateContact then
            UpdateNeeded := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeOnDelete', '', false, false)]
    local procedure T18_OnBeforeOnDelete(var Customer: Record Customer)
    var
        recL_OtherCust: Record Customer;
        ESK001Err: Label 'C''est un client %1  pour la fiche %2. Veuillez d''abord le retirer.', Comment = '%1 = Billed or Invoiced or Central ;  %2 = Client No';
    begin
        // MCO Le 23-03-2016 => Contr“les sur suppression
        recL_OtherCust.RESET();

        // Contr“le client comptable
        recL_OtherCust.SETRANGE("Bill-to Customer No.", Customer."No.");
        IF recL_OtherCust.FINDFIRST() THEN
            ERROR(ESK001Err, recL_OtherCust.FIELDCAPTION("Bill-to Customer No."), recL_OtherCust."No.");

        // Contr“le client facturé
        recL_OtherCust.SETRANGE("Bill-to Customer No.");
        recL_OtherCust.SETRANGE("Invoice Customer No.", Customer."No.");
        IF recL_OtherCust.FINDFIRST() THEN
            ERROR(ESK001Err, recL_OtherCust.FIELDCAPTION("Invoice Customer No."), recL_OtherCust."No.");

        // Contr“le centrale
        recL_OtherCust.SETRANGE("Invoice Customer No.");
        recL_OtherCust.SETRANGE("Centrale Active", Customer."No.");
        IF recL_OtherCust.FINDFIRST() THEN
            ERROR(ESK001Err, recL_OtherCust.FIELDCAPTION("Centrale Active"), recL_OtherCust."No.");
        // FIN MCO Le 23-03-2016
    end;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeValidateEvent', 'Address', false, false)]
    local procedure T18_OnBeforeValidateEvent(var Rec: Record Customer; var xRec: Record Customer; CurrFieldNo: Integer)
    begin
        // MC Le 29-04-2011 => SYMTA -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xRec.Address = Rec."Adresse Expédition" THEN
            Rec.VALIDATE("Adresse Expédition", Rec.Address);
        // FIN MC Le 29-04-2011
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeValidateEvent', 'Address 2', false, false)]
    local procedure T18_OnBeforeValidateEvent_add2(var Rec: Record Customer; var xRec: Record Customer; CurrFieldNo: Integer)
    begin
        // MC Le 29-04-2011 => SYMTA -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xRec."Address 2" = Rec."Adresse 2 Expédition" THEN
            Rec.VALIDATE("Adresse 2 Expédition", Rec."Address 2");
        // FIN MC Le 29-04-2011
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnAfterValidateCity, '', false, false)]
    local procedure T18_OnAfterValidateCity(var Customer: Record Customer; xCustomer: Record Customer)
    begin
        // MC Le 29-04-2011 => SYMTA -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xCustomer.City = Customer."Ville Expédition" THEN
            Customer.VALIDATE("Ville Expédition", Customer.City);
        // FIN MC Le 29-04-2011
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnBeforeValidateEvent, 'Credit Limit (LCY)', false, false)]
    local procedure T18_OnBeforeValidateEvent_CreditLimitLCY(var Rec: Record Customer; var xRec: Record Customer)
    begin
        // AD Le 28-08-2009 => CREDIT -> Historisation des modifications sur le credit
        rec.SaveCreditUpdate();
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnBeforeValidateEvent, 'Country/Region Code', false, false)]
    local procedure T18_OnBeforeValidateEvent_CountryRegionCode(var Rec: Record Customer; var xRec: Record Customer)
    begin
        // GR Le 29-07-2015 => MIG2015 -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xRec."Country/Region Code" = Rec."Country/Region Code Expédition" THEN
            Rec.VALIDATE("Country/Region Code Expédition", Rec."Country/Region Code");
        // FIN GR Le 29-07-2015
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnAfterValidateEvent, 'Post Code', false, false)]
    local procedure T18_OnAfterValidateEvent_PostCode(var Rec: Record Customer; var xRec: Record Customer)
    begin
        // LM Le 20-03-2013 => SYMTA -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xRec."Post Code" = rec."Code postal Expédition" THEN
            rec.VALIDATE("Code postal Expédition", rec."Post Code");
        // FIN LM Le 20-03-2013
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnBeforeValidateEvent, 'County', false, false)]
    local procedure T18_OnBeforeValidateEvent_County(var Rec: Record Customer; var xRec: Record Customer)
    begin
        // GR Le 29-07-2015 => MIG2015 -> MAJ des champs d'adresses expéditions en fonction des champs adresses standards.
        IF xRec.County = rec."County Expédition" THEN
            rec.VALIDATE("County Expédition", rec.County);
        // FIN GR Le 29-07-2015
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnBeforeCheckBlockedCust, '', false, false)]
    local procedure T18_OnBeforeCheckBlockedCust(Customer: Record Customer; Source: Option Journal,Document; DocType: Option; Shipment: Boolean; Transaction: Boolean; var IsHandled: Boolean)
    var
        Esk000Msg: Label 'Le client %1 est bloqué. Vous pouvez faire que des devis pour ce client. Penser à facturer le port.', Comment = '%1 = No Client';
    begin
        if Source = Source::Journal then
            exit;
        // DZ Le 11/06/2012 => Impossible d'enregistrer un retour pour les clients bloqués
        // Ancien Code
        //IF (DocType<>DocType::Order) AND (DocType<>DocType::Quote) THEN
        // EXIT;
        // Fin Ancien Code
        if Transaction then begin
            IsHandled := true;
            exit;
        end;
        if CduGSingleInstance.FctGetSkipFunctionValue() then begin
            CduGSingleInstance.FctsetSkipFunctionValue(false);
            exit;
        end;
        IF (DocType <> 1) AND (DocType <> 0) AND (DocType <> 5) THEN
            exit;
        // Fin DZ Le 11/06/2012
        if Customer."Privacy Blocked" then
            Customer.CustPrivacyBlockedErrorMessage(Customer, Transaction);

        if ((Customer.Blocked = Customer.Blocked::All) or
            ((Customer.Blocked = Customer.Blocked::Invoice) and
             // (DocType in [DocType::Quote, DocType::Order, DocType::Invoice, DocType::"Blanket Order"])) or
             // ((Customer.Blocked = Customer.Blocked::Ship) and (DocType in [DocType::Quote, DocType::Order, DocType::"Blanket Order"]) and
             // AD Le 13-06-2012 + MIG2017
             (DocType IN [0, 1, 2, 4, 5])) OR
            ((Customer.Blocked = Customer.Blocked::Ship) AND (DocType IN [0, 1, 4, 5]) AND
             (not Transaction)) or
            ((Customer.Blocked = Customer.Blocked::Ship) and (DocType in [0, 1, 2, 4]) and
             Shipment and Transaction))
        then
          // MC Le 25-10-2011 => Si le type de document est un devis on autorise mˆme si le client est bloqu‚
          // Ancien Code
          //CustBlockedErrorMessage(Customer,Transaction);
          // Nouveau Code
          BEGIN
            IF (DocType = 0) THEN BEGIN
                IF GUIALLOWED THEN
                    MESSAGE(Esk000Msg, Customer."No.")
            END
            ELSE
                Customer.CustBlockedErrorMessage(Customer, Transaction);
        END;
        // Fin MC Le 25-10-2011
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, OnBeforeCheckBlockedCust, '', false, false)]
    local procedure T18_OnBeforeCheckBlockedCust2(Customer: Record Customer; Source: Option Journal,Document; DocType: Option; Shipment: Boolean; Transaction: Boolean; var IsHandled: Boolean)
    begin
        IF Source = Source::Document then
            exit;
        IsHandled := true;
        if CduGSingleInstance.FctGetSkipFunctionValue() then begin
            CduGSingleInstance.FctsetSkipFunctionValue(false);
            exit;
        end;
        if Customer."Privacy Blocked" then
            Customer.CustPrivacyBlockedErrorMessage(Customer, Transaction);

        // MCO Le 23-03-2016 => Pouvoir lettrer un client bloqu‚ tous
        // Ancien code
        //IF (Blocked = Blocked::All) OR
        // Nouveau code
        IF ((Customer.Blocked = Customer.Blocked::All) AND (DocType <> 1)) OR
        // FIN MCO Le 23-03-2016 => Pouvoir lettrer un client bloqu‚ tous
           ((Customer.Blocked = Customer.Blocked::Invoice) and (DocType in [2, 0]))
        then
            Customer.CustBlockedErrorMessage(Customer, Transaction)
    END;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeGetTotalAmountLCYUI', '', false, false)]
    local procedure T18_OnBeforeGetTotalAmountLCYUI(var Customer: Record Customer)
    begin
        Customer.SetAutoCalcFields(Customer."Payment in progress (LCY)");
    end;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnBeforeGetTotalAmountLCYCommon', '', false, false)]
    local procedure T18_OnBeforeGetTotalAmountLCYCommon(var Customer: Record Customer; var AdditionalAmountLCY: Decimal; var IsHandled: Boolean)
    var
        [SecurityFiltering(SecurityFilter::Filtered)]
        SalesLine: Record "Sales Line";
        [SecurityFiltering(SecurityFilter::Filtered)]
        ServiceLine: Record "Service Line";
        LSetup: Record "Sales & Receivables Setup";
        LPaymentLine: Record "Payment Line";
        SalesOutstandingAmountFromShipment: Decimal;
        ServOutstandingAmountFromShipment: Decimal;
        InvoicedPrepmtAmountLCY: Decimal;
        RetRcdNotInvAmountLCY: Decimal;
        LMtRisque: Decimal;
        LDate_inf: Date;
    begin
        IsHandled := true;
        SalesOutstandingAmountFromShipment := SalesLine.OutstandingInvoiceAmountFromShipment(Customer."No.");
        ServOutstandingAmountFromShipment := ServiceLine.OutstandingInvoiceAmountFromShipment(Customer."No.");
        InvoicedPrepmtAmountLCY := Customer.GetInvoicedPrepmtAmountLCY();
        RetRcdNotInvAmountLCY := Customer.GetReturnRcdNotInvAmountLCY();
        // FB le 02/11/2009 Calcul du montant risque
        LMtRisque := 0;
        LMtRisque := Customer."Payment in progress (LCY)";

        LSetup.GET();
        LPaymentLine.SETCURRENTKEY("Account Type", "Account No.", "Due Date", "Payment Class");
        LPaymentLine.SETRANGE("Account Type", LPaymentLine."Account Type"::Customer);
        LPaymentLine.SETRANGE("Account No.", Customer."No.");
        LPaymentLine.SETRANGE("Payment Class", 'CLIENT EFF');
        LDate_inf := CALCDATE(LSetup."Delai de securité (Paiement)", TODAY);
        LPaymentLine.SETRANGE("Due Date", LDate_inf, 99991231D);
        LPaymentLine.SETRANGE("Payment in Progress", FALSE);
        LPaymentLine.CALCSUMS(Amount);

        LMtRisque += LPaymentLine.Amount;

        // FIN FB
        AdditionalAmountLCY := Customer."Balance (LCY)" + Customer."Outstanding Orders (LCY)" + Customer."Shipped Not Invoiced (LCY)" + Customer."Outstanding Invoices (LCY)" +
          Customer."Outstanding Serv. Orders (LCY)" + Customer."Serv Shipped Not Invoiced(LCY)" + Customer."Outstanding Serv.Invoices(LCY)" -
          SalesOutstandingAmountFromShipment - ServOutstandingAmountFromShipment - InvoicedPrepmtAmountLCY - RetRcdNotInvAmountLCY +
          AdditionalAmountLCY + LMtRisque;
    end;

    [EventSubscriber(ObjectType::Table, Database::Vendor, OnAfterValidateEvent, 'Lead Time Calculation', false, false)]
    local procedure T23_OnAfterValidateEvent(var Rec: Record Vendor; var xRec: Record Vendor; CurrFieldNo: Integer)
    var
        Text012: Label 'Mise à jour du délai de réapprovisionnement des articles associés ?';
    begin
        IF CONFIRM(Text012) THEN
            Rec.UpdateLeadTime(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnAfterInsertEvent, '', false, false)]
    local procedure T27_OnAfterInsertEvent(var Rec: Record Item; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        rec."OnInsert.Eskape"();
        rec.SetLastDateTimeModified();
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnAfterModifyEvent, '', false, false)]
    local procedure T27_OnAfterModifyEvent(var Rec: Record Item; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        rec."OnModify.Eskape"();
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnBeforeValidateEvent, 'No. 2', false, false)]
    local procedure T27_OnBeforeValidateEvent(var Rec: Record Item; var xRec: Record Item; CurrFieldNo: Integer)
    VAR
        LItem: Record Item;
        LMultiref: Record "Item Reference";
        LItemVendor: Record "Item Vendor";
        ItemCrossReference: Record "Item Reference";
        Text003Qst: Label 'Do you want to change %1?', Comment = '%1 =No 2 Lbl ';
        Text7381Err: Label 'Cancelled.';
        ESK005Err: Label 'Cette référence existe déjà en référence externe pour l''aritcle %1', Comment = '%1 = Article No';
    BEGIN
        // AD Le 01-12-2011 => SYMTA
        IF NOT CONFIRM(Text003Qst, FALSE, Rec.FIELDCAPTION("No. 2")) THEN
            ERROR(Text7381Err);

        LItem.RESET();
        LItem.SETRANGE("No. 2", Rec."No. 2");
        IF LItem.FINDSET() THEN
            ERROR('Référence active existante sur l''article %1', LItem."No.");

        // MCO Le 27-09-2016 => FE20160905 - SYMTA - Gestion des références externes
        ItemCrossReference.RESET();
        ItemCrossReference.SETRANGE("Reference No.", Rec."No. 2");
        IF ItemCrossReference.FINDFIRST() THEN
            ERROR(ESK005Err, ItemCrossReference."Item No.");
        // MCO Le 27-09-2016 => FE20160905 - SYMTA - Gestion des références externes



        IF Rec."No. 2" <> xRec."No. 2" THEN BEGIN
            // MCO Le 27-09-2016 => Régie
            IF xRec."No. 2" <> '' THEN BEGIN
                // MCO Le 27-09-2016 => Régie
                CLEAR(LMultiref);
                LMultiref.SetHideControl(TRUE);
                LMultiref.VALIDATE("Item No.", Rec."No.");
                LMultiref.VALIDATE("Variant Code", '');
                LMultiref.VALIDATE("Reference Type", LMultiref."Reference Type"::"Ancienne Active");
                LMultiref.VALIDATE("Reference No.", xRec."No. 2");
                // MCO Le 16-07-2018 =>  FE20180612 - SYMTA Í Affichage WEB
                IF xRec.Publiable = xRec.Publiable::Web THEN
                    LMultiref.VALIDATE("publiable web", TRUE);

                // FIN MCO Le 16-07-2018 => FE20180616 - SYMTA Í Affichage WEB
                LMultiref.INSERT(TRUE);
            END;
            // AD Le 09-03-2016 =>
            CLEAR(LItemVendor);
            LItemVendor.SETCURRENTKEY("Item No.", "Variant Code", "Vendor No.");
            LItemVendor.SETRANGE("Item No.", Rec."No.");
            LItemVendor.MODIFYALL("Ref. Active", Rec."No. 2");
            // AD Le 09-03-2016
        END;

        // Modification MiniLoad … coder
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnAfterValidateEvent, 'Description', false, false)]
    local procedure T27_OnAfterValidateEvent(var Rec: Record Item; var xRec: Record Item; CurrFieldNo: Integer)
    VAR
        BOMComponent: Record "BOM Component";
        lItemSubstitution: Record "Item Substitution";
    begin
        //FBRUN LE 07/11/2017 -> mise a jours dans les nomenclatures
        BOMComponent.SETRANGE(Type, BOMComponent.Type::Item);
        BOMComponent.SETRANGE("No.", Rec."No.");
        IF NOT BOMComponent.ISEMPTY THEN
            BOMComponent.MODIFYALL(Description, Rec.Description);
        //FIn FBRUN
        // CFR le 28/09/2023 => R‚gie : Mise … jour de la description dans substitution
        lItemSubstitution.SETRANGE("Substitute Type", lItemSubstitution."Substitute Type"::Item);
        lItemSubstitution.SETRANGE("Substitute No.", Rec."No.");
        IF lItemSubstitution.FINDSET() THEN
            lItemSubstitution.MODIFYALL(Description, Rec.Description);
        // FIN CFR le 28/09/2023
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnBeforeTestNoOpenEntriesExist, '', false, false)]
    local procedure T27_OnBeforeTestNoOpenEntriesExist(Item: Record Item; CurrentFieldName: Text[100]; var IsHandled: Boolean; var ItemLedgerEntry: Record "Item Ledger Entry")
    begin
        if CurrentFieldName = item.FieldCaption("Base Unit of Measure") then
            IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnAfterValidateEvent, "Base Unit of Measure", false, false)]
    local procedure T27_OnAfterValidateEvent_BaseUnitOfMeasure(var Rec: Record Item; var xRec: Record Item; CurrFieldNo: Integer)
    var
        ItemUnitOfMeasure: Record "Item Unit of Measure";
    begin
        if Rec."Base Unit of Measure" <> '' then begin
            ItemUnitOfMeasure.Get(Rec."No.", Rec."Base Unit of Measure");
            ItemUnitOfMeasure.SetLoadFields(ItemUnitOfMeasure.Weight, ItemUnitOfMeasure.Cubage);
            // AD Le 27-10-2009 => FARGROUP -> Mise a jour des infos de la fiche articles en fonction de l'unit²
            Rec."Gross Weight" := ItemUnitOfMeasure.Weight;
            Rec."Net Weight" := ItemUnitOfMeasure.Weight;
            Rec."Unit Volume" := ItemUnitOfMeasure.Cubage;
            // FIN AD 27-10-2009
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnAfterValidateEvent, "vendor No.", false, false)]
    local procedure T27_OnAfterValidateEvent_VendorNo(var Rec: Record Item; var xRec: Record Item; CurrFieldNo: Integer)
    var
        Vend: Record Vendor;
    begin
        Rec."Code Appro" := '';
        if (xRec."Vendor No." <> Rec."Vendor No.") and
               (Rec."Vendor No." <> '')
            then
            IF Vend.GET(Rec."Vendor No.") THEN
                // ANI Le 16-06-2016 FE20150424
                Rec."Code Appro" := Vend."Code Appro";
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, OnBeforeValidateEvent, "Manufacturer Code", false, false)]
    local procedure T27_OnBeforeValidateEvent_ManufacturerCode(var Rec: Record Item; var xRec: Record Item; CurrFieldNo: Integer)
    var
        LManufacturer: Record Manufacturer;
    BEGIN
        // AD Le 26-04-2011 => Mis en commentaire pour les imports de données
        //IF "Manufacturer Code" <> xRec."Manufacturer Code" THEN
        //  IF NOT CONFIRM(Text50013, FALSE) THEN
        //    "Manufacturer Code" := xRec."Manufacturer Code";

        // ANI Le 14-04-2015 => FE20150402
        IF (rec."Manufacturer Code" <> xRec."Manufacturer Code") AND (rec."Manufacturer Code" <> '') THEN BEGIN
            LManufacturer.GET(rec."Manufacturer Code");
            //IF LManufacturer."Gen. Prod. Posting Group" <> '' THEN
            rec.VALIDATE("Gen. Prod. Posting Group", LManufacturer."Gen. Prod. Posting Group");
            //IF LManufacturer."Tariff No." <> '' THEN
            // MCO Le 08-09-2017 => ON n'écrase plus la nomenclature par rapport … la marque
            //VALIDATE("Tariff No.", LManufacturer."Tariff No.");
            // FIN MCO Le 08-09-2017
        END;
    END;

    [EventSubscriber(ObjectType::Table, Database::Item, 'OnAfterIsAssemblyItem', '', false, false)]
    local procedure T27_OnAfterIsAssemblyItem(Item: Record Item; var Result: Boolean)
    begin
        Result := true;
    end;

    [EventSubscriber(ObjectType::table, database::"Accounting Period", 'OnCheckOpenFiscalYearsOnBeforeError', '', false, false)]
    local procedure T50_OnCheckOpenFiscalYearsOnBeforeError(var AccountingPeriod: Record "Accounting Period"; var IsHandled: Boolean)
    var

        AccountingPeriod2: Record "Accounting Period";
        Text10801Err: Label 'It is not allowed to have more than two open fiscal years. Please fiscally close the oldest open fiscal year first.';
        NoOfOpenFiscalYears: Integer;
    begin
        IsHandled := true;
        AccountingPeriod2.Reset();
        AccountingPeriod2.SetRange("New Fiscal Year", true);
        AccountingPeriod2.SetRange("Fiscally Closed", false);
        NoOfOpenFiscalYears := AccountingPeriod2.Count();
        if AccountingPeriod2.FindFirst() then;

        // check last period of previous fiscal year
        AccountingPeriod2.SetRange("New Fiscal Year");
        AccountingPeriod2.SetRange("Fiscally Closed");
        if AccountingPeriod2.Find('<') then
            if not AccountingPeriod2."Fiscally Closed" then
                NoOfOpenFiscalYears := NoOfOpenFiscalYears + 1;
        if NoOfOpenFiscalYears > 3 then
            Error(Text10801Err);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Report Selections", 'OnBeforeGetEmailBodyCustomer', '', false, false)]
    local procedure T77_OnBeforeGetEmailBodyCustomer(CustNo: Code[20]; RecordVariant: Variant; ReportUsage: Integer; var CustEmailAddress: Text[250]; var EmailBodyText: Text; var IsHandled: Boolean; var Result: Boolean; var TempBodyReportSelections: Record "Report Selections" temporary)
    begin
        // MIG 2017 : appel … selection de contact
        //CustEmailAddress := GetCustEmailAddress(CustNo);
        CustEmailAddress := TempBodyReportSelections.GetEmailAddressFromVariant(RecordVariant);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Report Selections", 'OnAfterGetEmailBodyCustomer', '', false, false)]
    local procedure T77_OnAfterGetEmailBodyCustomer(var CustomerEmailAddress: Text[250]; ServerEmailBodyFilePath: Text[250]; RecordVariant: Variant; var Result: Boolean; var IsHandled: Boolean)
    var
        RecLReportSelections: record "Report Selections";
    begin
        RecLReportSelections.GetEmailAddressFromVariant(RecordVariant);
    end;

    [EventSubscriber(ObjectType::Table, database::"Report Selections", 'OnBeforeSendEmailDirectly', '', false, false)]
    local procedure T77_OnBeforeSendEmailDirectly(FoundAttachment: Boolean; FoundBody: Boolean; RecordVariant: Variant; ReportUsage: Enum "Report Selection Usage"; ServerEmailBodyFilePath: Text[250]; ShowDialog: Boolean; var AllEmailsWereSuccessful: Boolean; var CustomReportSelection: Record "Custom Report Selection"; var DefaultEmailAddress: Text[250]; var DocName: Text[150]; var DocNo: Code[20]; var IsHandled: Boolean; var ReportSelections: Record "Report Selections"; var SourceIDs: List of [Guid]; var SourceRelationTypes: List of [Integer]; var SourceTableIDs: List of [Integer]; var TempAttachReportSelections: Record "Report Selections" temporary)
    var
        cdu_DataTypeMgt: Codeunit "Data Type Management";
        CduLsingleInstance: Codeunit SingleInstance;
        RecRef: RecordRef;
        FieldRef: FieldRef;
    begin
        ReportSelections.ShowNoBodyNoAttachmentError(ReportUsage, FoundBody, FoundAttachment);
        // MCO Le 15-01-2018 => Le sujet du mail doit ˆtre le nom du donneur d'ordre
        IF cdu_DataTypeMgt.GetRecordRef(RecordVariant, RecRef) THEN
            IF cdu_DataTypeMgt.FindFieldByName(RecRef, FieldRef, 'Sell-to Customer Name') THEN
                CduLsingleInstance.SetRecipientName(FORMAT(FieldRef.VALUE));


        // FIN MCO Le 15-01-2018
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnBeforeValidateEvent', "Document Type", false, false)]
    local procedure T81_OnBeforeValidateEvent(var Rec: Record "Gen. Journal Line"; var xRec: Record "Gen. Journal Line"; CurrFieldNo: Integer)
    begin
        CduGSingleInstance.FctSetSkipFunctionValue(true);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnGetCustomerAccountOnAfterCustGet', '', false, false)]
    local procedure T81_OnGetCustomerAccountOnAfterCustGet(var GenJournalLine: Record "Gen. Journal Line"; CallingFieldNo: Integer; sender: Record "Gen. Journal Line"; var Customer: Record Customer)
    begin
        CduGSingleInstance.FctSetSkipFunctionValue(true);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Gen. Journal Line", 'OnGetCustomerBalAccountOnAfterCustGet', '', false, false)]
    local procedure T81_OnGetCustomerBalAccountOnAfterCustGet(var GenJournalLine: Record "Gen. Journal Line"; CallingFieldNo: Integer; sender: Record "Gen. Journal Line"; var Customer: Record Customer)
    begin
        CduGSingleInstance.FctSetSkipFunctionValue(true);
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Shipment Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T110_OnBeforeDeleteEvent(var Rec: Record "Sales Shipment Header"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Shipment Header", 'OnBeforePrintRecords', '', false, false)]
    local procedure T110_OnBeforePrintRecords(var SalesShipmentHeader: Record "Sales Shipment Header"; ShowDialog: Boolean; var IsHandled: Boolean)
    begin
        IsHandled := true;
        SalesShipmentHeader.SendRecords(ShowDialog, FALSE);
    end;

    [EventSubscriber(ObjectType::Table, database::"No. Series", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T308_OnBeforeDeleteEvent(var Rec: Record "No. Series"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR('interdit');
    end;

    [EventSubscriber(ObjectType::Table, database::"Requisition Line", 'OnBeforeCopyFromItem', '', false, false)]
    local procedure T246_OnBeforeCopyFromItem(var RequisitionLine: Record "Requisition Line"; Item: Record Item; xRequisitionLine: Record "Requisition Line"; FieldNo: Integer; var TempPlanningErrorLog: Record "Planning Error Log" temporary; PlanningResiliency: Boolean)
    begin
        // AD le 27-01-2012
        RequisitionLine."Ref. Active" := Item."No. 2";
        // FIN AD le 27-01-2012
    end;

    [EventSubscriber(ObjectType::Table, database::"Requisition Line", 'OnAfterValidateEvent', "No.", false, false)]
    local procedure T246_OnAfterCopyFromItem(var Rec: Record "Requisition Line"; var xRec: Record "Requisition Line"; CurrFieldNo: Integer)
    begin
        // AD Le 19-11-2009 => Traitements ESKAPE
        Rec."ValidateNo.Eskape"();
    end;

    [EventSubscriber(ObjectType::table, database::"Standard Customer Sales Code", 'OnApplyStdCodesToSalesLinesOnBeforeRoundUnitPrice', '', false, false)]
    local procedure T172_OnApplyStdCodesToSalesLinesOnBeforeRoundUnitPrice(StandardSalesLine: Record "Standard Sales Line"; var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        GestionMultiref: Codeunit "Gestion Multi-référence";
    begin
        // AD Le 03-11-2011 => SYMTA
        SalesLine."Recherche référence" := GestionMultiref.RechercheRefActive(SalesLine."No.");
        // FIN AD Le 03-11-2011
    end;

    [EventSubscriber(ObjectType::table, database::"Standard Customer Sales Code", 'OnApplyStdCodesToSalesLinesOnAfterSetSalesLineLineNo', '', false, false)]
    local procedure T172_OnApplyStdCodesToSalesLinesOnAfterSetSalesLineLineNo(var SalesLine: Record "Sales Line"; var StdCustSalesCode: Record "Standard Customer Sales Code"; var IsHandled: Boolean)
    var
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.get(SalesLine."Document Type", SalesLine."Document No.");
        //MC Le 12-08-2010 -> BUG pour les prets
        SalesLine.Loans := SalesHeader.Loans;
        //FIN MC
    end;

    [EventSubscriber(ObjectType::Table, database::"No. Series", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T124_OnBeforeDeleteEvent(var Rec: Record "No. Series"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, database::"Purch. Inv. Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T122_OnBeforeDeleteEvent(var Rec: Record "Purch. Inv. Header"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, database::"Purch. Rcpt. Line", 'OnBeforeInsertInvLineFromRcptLine', '', false, false)]
    local procedure T121_OnBeforeInsertInvLineFromRcptLine(var PurchLine: Record "Purchase Line"; PurchOrderLine: Record "Purchase Line"; var IsHandled: Boolean; var PurchRcptLine: Record "Purch. Rcpt. Line")
    var

    begin
        PurchLine."Recherche référence" := PurchRcptLine."Recherche référence";

    end;

    [EventSubscriber(ObjectType::Table, database::"Purch. Rcpt. Line", 'OnAfterInitFromPurchLine', '', false, false)]
    local procedure T121_OnAfterInitFromPurchLine(var PurchRcptLine: Record "Purch. Rcpt. Line"; PurchLine: Record "Purchase Line"; PurchRcptHeader: Record "Purch. Rcpt. Header")
    begin
        // AD Le 20-01-2014
        PurchRcptLine."Vendor Shipment No." := PurchRcptHeader."Vendor Shipment No.";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purch. Rcpt. Line", 'OnInsertInvLineFromRcptLineOnBeforePurchLineUpdatePrePaymentAmounts', '', false, false)]
    local procedure T121_OnInsertInvLineFromRcptLineOnBeforePurchLineUpdatePrePaymentAmounts(var PurchaseLine: Record "Purchase Line"; PurchOrderLine: Record "Purchase Line")
    var
        PurchOrderHeader: Record "Purchase Header";
    begin
        // ESK Le ??-??-????
        PurchaseLine."Discount1 %" := PurchOrderLine."Discount1 %";
        PurchaseLine."Discount2 %" := PurchOrderLine."Discount2 %";
        // AD Le 21-03-2013
        PurchOrderLine."Qte Sur Facture Frn" := PurchaseLine."Qte Sur Facture Frn";
        // FIN AD Le 21-03-2013
        // MCO Le 27-09-2016 => Suivi des champs
        if PurchOrderHeader.GET(PurchOrderLine."Document Type"::Order, PurchaseLine."Order No.") then begin
            PurchaseLine."Type de commande" := PurchOrderHeader."Type de commande";
            PurchaseLine."Vendor Order No." := PurchOrderHeader."Vendor Order No.";
        end;
        // FIN MCO Le 27-09-2016 => Suivi des champs
    end;

    [EventSubscriber(ObjectType::Table, database::"Purch. Rcpt. Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T120_OnBeforeDeleteEvent(var Rec: Record "Purch. Rcpt. Header"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Cr.Memo Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T114_OnBeforeDeleteEvent(var Rec: Record "Sales Cr.Memo Header"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Invoice Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T112_OnBeforeDeleteEvent(var Rec: Record "Sales Invoice Header"; RunTrigger: Boolean)
    var
        Text5000Err: Label 'Impossible de supprimer';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 29-19-2009 => Interdiction de supprimer
        ERROR(Text5000Err);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Job Queue Log Entry", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T474_OnBeforeModifyEvent(var Rec: Record "Job Queue Log Entry"; var xRec: Record "Job Queue Log Entry"; RunTrigger: Boolean)
    VAR
        t: Record "Temp Texte 250";
    begin
        if Rec.IsTemporary then
            exit;
        if Not RunTrigger then
            exit;
        Rec.SendEmail();
        t.Tmp := FORMAT(CURRENTDATETIME);
        t.Insert();
    END;

    [EventSubscriber(ObjectType::Table, database::"Item Substitution", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T5715_OnBeforeInsertEvent(var Rec: Record "Item Substitution"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // ESKAPE => Tra‡abilité des enregistrements
        IF STRLEN(USERID) <= MAXSTRLEN(Rec."Create User ID") THEN
            Rec."Create User ID" := USERID;
        Rec."Create Date" := TODAY;
        Rec."Create Time" := TIME;
        // FIN ESKAPE => Tra‡abilité des enregistrements
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Substitution", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T5715_OnBeforeModifyEvent(var Rec: Record "Item Substitution"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // ESKAPE => Tra‡abilité des enregistrements
        IF STRLEN(USERID) <= MAXSTRLEN(Rec."Create User ID") THEN
            Rec."Modify User ID" := USERID;
        Rec."Modify Date" := TODAY;
        Rec."Modify Time" := TIME;
        // FIN ESKAPE => Tra‡abilité des enregistrements
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnAfterInsertEvent', '', false, false)]
    local procedure T5777_OnAfterInsertEvent(var Rec: Record "Item Reference"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        CduGSingleInstance.FctGetItemReferenceTypeValue(Rec."Reference Type");
        rec.Rename(rec."Reference Type");
        // AD Le 03-03-2010 => GDI -> Verification multiref
        rec.VerifRefExiste('I');

        // MCO Le 24-03-2016 => Tra‡abilité
        rec."OnInsert.Eskape"();
        // FIN MCO Le 24-03-2016
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T5777_OnBeforeModifyEvent(var Rec: Record "Item Reference"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // MCO Le 24-03-2016 => Tra‡abilité
        rec."OnModify.Eskape"();
        // FIN MCO Le 24-03-2016
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeRenameEvent', '', false, false)]
    local procedure T5777_OnBeforeRenameEvent(var Rec: Record "Item Reference"; RunTrigger: Boolean)
    var
        Text000Err: Label 'You cannot enter a %1 for a blank Cross-Reference Type.', Comment = '%1 = Ref Type';
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        IF (Rec."Reference Type No." <> '') AND
        (Rec."Reference Type" = Rec."Reference Type"::"Non Qualifié")
     THEN
            ERROR(Text000Err, Rec.FIELDCAPTION("Reference Type No."));
        if Rec."Reference Type" = Rec."Reference Type"::" " then begin
            CduGSingleInstance.FctSaveItemReferenceTypeValue(Rec."Reference Type");
            Rec."Reference Type" := Rec."Reference Type"::"Non Qualifié"
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnAfterRenameEvent', '', false, false)]
    local procedure T5777_OnAfterRenameEvent(var Rec: Record "Item Reference"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        CduGSingleInstance.FctGetItemReferenceTypeValue(Rec."Reference Type");
        // AD Le 03-03-2010 => GDI -> Verification multiref
        rec.VerifRefExiste('R');
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T5777_OnBeforeInsertEvent(var Rec: Record "Item Reference"; RunTrigger: Boolean)
    var
        Text000Err: Label 'You cannot enter a %1 for a blank Cross-Reference Type.', Comment = '%1 = Ref Type';
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        IF (Rec."Reference Type No." <> '') AND
          (Rec."Reference Type" = Rec."Reference Type"::"Non Qualifié")
       THEN
            ERROR(Text000Err, Rec.FIELDCAPTION("Reference Type No."));
        if Rec."Reference Type" = Rec."Reference Type"::" " then begin
            CduGSingleInstance.FctSaveItemReferenceTypeValue(Rec."Reference Type");
            Rec."Reference Type" := Rec."Reference Type"::"Non Qualifié"
        end;

    end;

    [EventSubscriber(ObjectType::Page, Page::"Sales Statistics", 'OnAfterCalculateTotals', '', false, false)]
    local procedure P160_OnAfterCalculateTotals(sender: Page "Sales Statistics"; var SalesHeader: Record "Sales Header"; var TempVATAmountLine: Record "VAT Amount Line" temporary; var TotalAmt1: Decimal; var TotalAmt2: Decimal; var TotalSalesLine: Record "Sales Line"; var TotalSalesLineLCY: Record "Sales Line")
    begin
        // MC Le 25-10-2011 => Gestion du total Brut
        sender.FctSetTotalAmountBrut(TotalSalesLine.Amount + TotalSalesLine."Line Discount Amount");
        // FIN MC Le 25-10-2011
    end;

    [EventSubscriber(ObjectType::Page, Page::"Item References", 'OnNewRecordEvent', '', false, false)]
    local procedure P5736_OnNewRecordEvent(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; BelowxRec: Boolean)
    begin
        rec."Reference Type" := rec."Reference Type"::Vendor;
    end;

    [EventSubscriber(ObjectType::Page, Page::"Item Reference Entries", 'OnNewRecordEvent', '', false, false)]
    local procedure P5737_OnNewRecordEvent(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; BelowxRec: Boolean)
    begin
        rec."Reference Type" := rec."Reference Type"::Vendor;
    end;

    [EventSubscriber(ObjectType::Page, Page::"Item Reference List", 'OnNewRecordEvent', '', false, false)]
    local procedure P5735_OnNewRecordEvent(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; BelowxRec: Boolean)
    begin
        rec."Reference Type" := rec."Reference Type"::Vendor;
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeValidateEvent', "Reference Type", false, false)]
    local procedure T5777_OnBeforeValidateEvent(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; CurrFieldNo: Integer)
    begin
        IF (Rec."Reference Type" <> xRec."Reference Type") AND
          (xRec."Reference Type" <> xRec."Reference Type"::"Non Qualifié") OR
          (Rec."Reference Type" = Rec."Reference Type"::"Bar Code") THEN
            Rec."Reference Type No." := '';
        if Rec."Reference Type" = Rec."Reference Type"::" " then begin
            CduGSingleInstance.FctSaveItemReferenceTypeValue(Rec."Reference Type");
            Rec."Reference Type" := Rec."Reference Type"::"Non Qualifié"
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnAfterValidateEvent', "Reference Type", false, false)]
    local procedure T5777_OnAfterValidateEvent_RefType(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; CurrFieldNo: Integer)
    begin
        CduGSingleInstance.FctGetItemReferenceTypeValue(Rec."Reference Type");
    end;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnAfterValidateEvent', 'Reference No.', false, false)]
    local procedure T5777_OnAfterValidateEvent(var Rec: Record "Item Reference"; var xRec: Record "Item Reference"; CurrFieldNo: Integer)
    VAR
        Item: Record item;
        ESK5010Err: Label 'Cette référence existe déjà en référence active pour l''article %1', Comment = '%1 = Article No';
    BEGIN
        // MCO Le 27-09-2016 => FE20160905 - SYMTA - Gestion des références externes
        IF NOT rec.GETHideControl() THEN BEGIN
            Item.SETRANGE("No. 2", rec."Reference No.");
            IF Item.FINDFIRST() THEN
                ERROR(ESK5010Err, Item."No.");
        END;
        // MCO Le 27-09-2016 => FE20160905 - SYMTA - Gestion des références externes
    END;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeCreateItemVendorProcedure', '', false, false)]
    local procedure T5777_OnBeforeCreateItemVendorProcedure(var ItemReference: Record "Item Reference"; var ItemVendor: Record "Item Vendor"; var IsHandled: Boolean)
    BEGIN
        IsHandled := true;
    END;

    [EventSubscriber(ObjectType::Table, database::"Item Reference", 'OnBeforeDeleteItemVendor', '', false, false)]
    local procedure T5777_OnBeforeDeleteItemVendor(var ItemReference: Record "Item Reference"; var IsHandled: Boolean)
    BEGIN
        IsHandled := true;
    END;

    [EventSubscriber(ObjectType::Table, database::"Value Entry", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T5802_OnBeforeInsertEvent(var Rec: Record "Value Entry"; RunTrigger: Boolean)
    begin
        if Rec.IsTemporary then
            exit;
        if Not RunTrigger then
            exit;
        // AD Le 22-07-2011 => SYMTA
        Rec."N° article Pour Stats" := Rec."Item No.";
        // FIN AD Le 22-07-2011    
    end;

    [EventSubscriber(ObjectType::Table, database::"Value Entry", 'OnSumCostsTillValuationDateOnAfterSetFilters', '', false, false)]
    local procedure T5802_OnSumCostsTillValuationDateOnAfterSetFilters(var ValueEntry: Record "Value Entry"; var Item: Record Item; var ValueEntryRec: Record "Value Entry")
    BEGIN
        // AD Le 23-09-2009 => Non Stocké
        ValueEntryRec.SETRANGE("Sans mouvement de stock", FALSE);
        // FIN AD Le 23-09-2009
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Header", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T6660_OnBeforeInsertEvent(var Rec: Record "Return Receipt Header"; RunTrigger: Boolean)
    BEGIN
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // CFR le 18/11/2020 : Régie > Code utilisateur modification
        Rec.VALIDATE("Create User ID", USERID);
        Rec.VALIDATE("Create Date", Today);
        Rec.VALIDATE("Create Time", TIME);

        Rec.VALIDATE("Modify User ID", USERID);
        Rec.VALIDATE("Modify Date", Today);
        Rec.VALIDATE("Modify Time", TIME);
        // FIN CFR le 18/11/2020 : Régie > Code utilisateur modification
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Header", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T6660_OnBeforeModifyEvent(var Rec: Record "Return Receipt Header"; RunTrigger: Boolean)
    BEGIN
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // CFR le 18/11/2020 : Régie > Code utilisateur modification
        Rec.VALIDATE("Modify User ID", USERID);
        Rec.VALIDATE("Modify Date", Today);
        Rec.VALIDATE("Modify Time", TIME);
        // FIN CFR le 18/11/2020 : Régie > Code utilisateur modification
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Header", 'OnBeforePrintRecords', '', false, false)]
    local procedure T6660_OnBeforePrintRecords(var ReturnRcptHeader: Record "Return Receipt Header"; ShowDialog: Boolean; var IsHandled: Boolean)
    BEGIN
        ReturnRcptHeader.SendRecords(ShowDialog, FALSE);
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnInsertInvLineFromRetRcptLineOnAfterCalcUnitPrice', '', false, false)]
    local procedure T6661_OnInsertInvLineFromRetRcptLineOnAfterCalcUnitPrice(var ReturnReceiptLine: Record "Return Receipt Line"; SalesHeader: Record "Sales Header"; SalesHeader2: Record "Sales Header"; var SalesLine: Record "Sales Line"; var SalesOrderLine: Record "Sales Line"; Currency: Record Currency)
    begin
        CduGSingleInstance.FctSaveRecords(Currency, SalesOrderLine);
        //DZ - 15/12/2011 - Regrouper les réceptions retour
        SalesLine.SetHideValidationDialog(TRUE);
        // Fin DZ
    end;


    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnAfterCopyFieldsFromReturnReceiptLine', '', false, false)]
    local procedure T6661_OnAfterCopyFieldsFromReturnReceiptLine(var ReturnReceiptLine: Record "Return Receipt Line"; var SalesLine: Record "Sales Line")
    Var
        ReLSalesOrderLine: Record "Sales Line";
        RecLCurrency: Record Currency;
    begin
        CduGSingleInstance.FctGetSavedRecords(RecLCurrency, ReLSalesOrderLine);
        IF (SalesLine.Type <> SalesLine.Type::" ") THEN BEGIN
            SalesLine.VALIDATE(Quantity, ReturnReceiptLine.Quantity - ReturnReceiptLine."Quantity Invoiced");
            SalesLine.Validate("Unit Price", ReLSalesOrderLine."Unit Price");
            SalesLine."Allow Line Disc." := ReLSalesOrderLine."Allow Line Disc.";
            SalesLine."Allow Invoice Disc." := ReLSalesOrderLine."Allow Invoice Disc.";
            SalesLine.Validate("Line Discount %", ReLSalesOrderLine."Line Discount %");
            // DZ - 15/12/2011 - Regrouper les r‚ceptions retour
            SalesLine."Discount1 %" := ReLSalesOrderLine."Discount1 %";
            SalesLine."Discount2 %" := ReLSalesOrderLine."Discount2 %";
            SalesLine."Net Unit Price" := ReLSalesOrderLine."Net Unit Price";
            //message('1 - %1 - %2 - %3', SalesLine.VALIDATE("Unit Price"
            IF (ReLSalesOrderLine."Internal Line Type" = ReLSalesOrderLine."Internal Line Type"::"Discount Header") OR
            (ReLSalesOrderLine."Internal Line Type" = ReLSalesOrderLine."Internal Line Type"::"Discount Line") THEN BEGIN
                SalesLine.VALIDATE("Unit Price", 0);
                SalesLine.VALIDATE("Line Discount %", 0);
                SalesLine."Internal Line Type" := ReLSalesOrderLine."Internal Line Type"::" ";
            END;
            // Fin DZ
            if ReLSalesOrderLine.Quantity = 0 then
                SalesLine.Validate("Inv. Discount Amount", 0)
            else
                SalesLine.Validate(
                  "Inv. Discount Amount",
                  Round(
                    ReLSalesOrderLine."Inv. Discount Amount" * SalesLine.Quantity / ReLSalesOrderLine.Quantity,
                    RecLCurrency."Amount Rounding Precision"));
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnInsertInvLineFromRetRcptLine', '', false, false)]
    local procedure T6661_OnInsertInvLineFromRetRcptLine(var SalesLine: Record "Sales Line"; var ReturnReceiptLine: Record "Return Receipt Line"; var IsHandled: Boolean)
    BEGIN
        ReturnReceiptLine.SetRange("Document No.", ReturnReceiptLine."Document No.");
        //DZ - 15/12/2011 - Regrouper les réceptions retour
        IF ReturnReceiptLine."Attached to Line No." <> 0 THEN
            IsHandled := true;
        // Fin DZ
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnInsertInvLineFromRetRcptLineOnAfterSalesHeaderGet', '', false, false)]
    local procedure T6661_OnInsertInvLineFromRetRcptLineOnAfterSalesHeaderGet(var ReturnReceiptLine: Record "Return Receipt Line"; var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line")
    var
        NextLineNo: integer;
        ESK001Txt: Label 'Client : %1', Comment = '%1 = Client No';
    BEGIN
        if SalesLine.Find('+') then
            NextLineNo := SalesLine."Line No." + 10000
        else
            NextLineNo := 10000;
        //DZ - 15/12/2011 - Regrouper les réceptions retour
        IF SalesLine."Sell-to Customer No." <> ReturnReceiptLine."Sell-to Customer No." THEN BEGIN
            SalesLine.INIT();
            SalesLine."Line No." := NextLineNo;
            SalesLine."Document Type" := SalesLine."Document Type";
            SalesLine."Document No." := SalesLine."Document No.";
            SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015
            SalesLine.Description := STRSUBSTNO(ESK001Txt, ReturnReceiptLine."Sell-to Customer No.");
            SalesLine."Description 2" := ReturnReceiptLine."Sell-to Customer No.";
            SalesLine."N° client Livré" := ReturnReceiptLine."Sell-to Customer No.";
            SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureClientLivré;
            SalesLine.Insert();
            NextLineNo := NextLineNo + 10000;
        END;
        // Fin DZ
    END;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnBeforeInsertInvLineFromRetRcptLineBeforeInsertTextLine', '', false, false)]
    local procedure T6661_OnBeforeInsertInvLineFromRetRcptLineBeforeInsertTextLine(var ReturnReceiptLine: Record "Return Receipt Line"; var SalesLine: Record "Sales Line"; var IsHandled: Boolean; var NextLineNo: Integer)
    var
        ReturnRcptHeader: Record "Return Receipt Header";
        ESK50000Msg: Label 'N° Retour : %1', Comment = '%1 = Document Retour No';
        ESK50001Msg: Label 'Du : %1', Comment = '%1 = Posting Dat';
        ESK50002Msg: Label 'Cde : %1', Comment = '%1 = Return Order No';
    //ESK001: Label 'Client : %1';

    begin
        if SalesLine.Find('+') then
            NextLineNo := SalesLine."Line No." + 10000
        else
            NextLineNo := 10000;
        // DZ - 15/12/2011 - Regrouper les réceptions retour
        // SalesLine.Description := STRSUBSTNO(Text000,"Document No."); => CODE D'ORIGINE
        SalesLine."Line No." := NextLineNo;
        SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015
        SalesLine.Description := STRSUBSTNO(ESK50000Msg, ReturnReceiptLine."Document No.");
        SalesLine."Description 2" := STRSUBSTNO(ESK50001Msg, ReturnReceiptLine."Posting Date");
        SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureBl;

        ReturnRcptHeader.GET(ReturnReceiptLine."Document No.");
        SalesLine."N° client Livré" := ReturnRcptHeader."Sell-to Customer No.";

        SalesLine.Insert();
        SalesLine."Line No." := SalesLine."Line No." + 1;

        // Cde : ... / ...
        SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015
        SalesLine.Description := STRSUBSTNO(ESK50002Msg, ReturnRcptHeader."Return Order No.");
        IF ReturnRcptHeader."External Document No." <> '' THEN
            SalesLine."Description 2" := '/' + ReturnRcptHeader."External Document No."
        ELSE
            SalesLine."Description 2" := '';
        SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureBl;
        SalesLine."N° client Livré" := ReturnRcptHeader."Sell-to Customer No.";
        // Fin DZ
    end;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnInsertInvLineFromRetRcptLineOnBeforeValidateSalesLineQuantity', '', false, false)]
    local procedure T6661_OnInsertInvLineFromRetRcptLineOnBeforeValidateSalesLineQuantity(var ReturnReceiptLine: Record "Return Receipt Line"; var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnBeforeCopySalesLinePriceAndDiscountFromSalesOrderLine', '', false, false)]
    local procedure T6661_OnBeforeCopySalesLinePriceAndDiscountFromSalesOrderLine(SalesOrderLine: Record "Sales Line"; var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, database::"Return Receipt Line", 'OnBeforeInsertInvLineFromRetRcptLine', '', false, false)]
    local procedure T6661_OnBeforeInsertInvLineFromRetRcptLine(SalesOrderLine: Record "Sales Line"; var SalesLine: Record "Sales Line"; var ReturnReceiptLine: Record "Return Receipt Line"; var IsHandled: Boolean)
    var
        ReturnRcptHeader: Record "Return Receipt Header";
        Currency: Record Currency;
    begin
        if ReturnReceiptLine."Currency Code" <> '' then
            Currency.get(ReturnReceiptLine."Currency Code");
        // DZ - 15/12/2011 - Regrouper les r‚ceptions retour
        // ANCIEN CODE  //IF NOT ExtTextLine THEN BEGIN
        IF (SalesLine.Type.AsInteger() <> 0) THEN BEGIN
            // Fin DZ

            SalesLine.VALIDATE(Quantity, ReturnReceiptLine.Quantity - ReturnReceiptLine."Quantity Invoiced");
            // AD Le 28-12-2011 => Rustine a cause d'un bug sur les abatements
            //SalesLine.VALIDATE("Unit Price",SalesOrderLine."Unit Price");
            SalesLine.VALIDATE("Unit Price", ROUND(SalesOrderLine."Unit Price", 0.01));
            // FIN AD Le 28-12-2011
            SalesLine."Allow Line Disc." := SalesOrderLine."Allow Line Disc.";
            SalesLine."Allow Invoice Disc." := SalesOrderLine."Allow Invoice Disc.";
            SalesLine.VALIDATE("Line Discount %", SalesOrderLine."Line Discount %");
            // DZ - 15/12/2011 - Regrouper les r‚ceptions retour
            SalesLine."Discount1 %" := SalesOrderLine."Discount1 %";
            SalesLine."Discount2 %" := SalesOrderLine."Discount2 %";
            SalesLine."Net Unit Price" := SalesOrderLine."Net Unit Price";
            //message('1 - %1 - %2 - %3', SalesLine.VALIDATE("Unit Price"
            IF (SalesOrderLine."Internal Line Type" = SalesOrderLine."Internal Line Type"::"Discount Header") OR
            (SalesOrderLine."Internal Line Type" = SalesOrderLine."Internal Line Type"::"Discount Line") THEN BEGIN
                SalesLine.VALIDATE("Unit Price", 0);
                SalesLine.VALIDATE("Line Discount %", 0);
                SalesLine."Internal Line Type" := SalesOrderLine."Internal Line Type"::" ";
            END;
            if SalesOrderLine.Quantity = 0 then
                SalesLine.Validate("Inv. Discount Amount", 0)
            else
                SalesLine.Validate(
                  "Inv. Discount Amount",
                  Round(
                    SalesOrderLine."Inv. Discount Amount" * SalesLine.Quantity / SalesOrderLine.Quantity,
                    Currency."Amount Rounding Precision"));
            // Fin DZ
        end;
        // DZ - 15/12/2011 - Regrouper les réceptions retour
        ReturnRcptHeader.GET(ReturnReceiptLine."Document No.");
        SalesLine."N° client Livré" := ReturnRcptHeader."Sell-to Customer No.";
        // Fin DZ
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Price", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7002_OnAfterInsertEvent(var Rec: Record "Sales Price"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // AD Le 16-04-2012 =>
        rec.TESTFIELD("Unit of Measure Code");
        // FIN AD Le 16-04-2012
        rec."user maj prix" := USERID;// MC Le 18-01-2016 => Demande de Nathalie
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Price", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T7002_OnBeforeModifyEvent(var Rec: Record "Sales Price"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        rec."user maj prix" := USERID// AD Le 23-01-2014 => Demande de Philippe
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Price", 'OnBeforeValidateEvent', "Unit Price", false, false)]
    local procedure T7002_OnBeforeValidateEvent(var Rec: Record "Sales Price"; var xRec: Record "Sales Price"; CurrFieldNo: Integer)
    begin
        // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs.
        IF Rec."Prix catalogue" <> 0 THEN
            Rec.Coefficient := Rec."Unit Price" / Rec."Prix catalogue";
        // FIN MC Le 19-04-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T10866_OnBeforeInsertEvent(var Rec: Record "Payment Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        rec."Acceptation Code" := rec."Acceptation Code"::LCR;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnAfterValidateEvent', 'Account No.', false, false)]
    local procedure T10866_OnAfterValidateEvent(var Rec: Record "Payment Line"; var xRec: Record "Payment Line"; CurrFieldNo: Integer)
    VAR
        rec_client: Record Customer;
        rec_vendor: Record Vendor;
        rec_compte: Record "G/L Account";
    BEGIN
        CASE Rec."Account Type" OF
            Rec."Account Type"::Customer:
                BEGIN
                    rec_client.GET(Rec."Account No.");
                    Rec.Désignation := rec_client.Name;
                END;
            Rec."Account Type"::Vendor:
                BEGIN
                    rec_vendor.GET(Rec."Account No.");
                    Rec.Désignation := rec_vendor.Name;
                END;
            Rec."Account Type"::"G/L Account":
                BEGIN
                    rec_compte.GET(Rec."Account No.");
                    Rec.Désignation := rec_compte.Name;
                END;

        END;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnBeforeValidateEvent', 'Debit Amount', false, false)]
    local procedure T10866_OnBeforeValidateEvent(var Rec: Record "Payment Line"; var xRec: Record "Payment Line"; CurrFieldNo: Integer)
    VAR
        lPaymentHeader: Record "Payment Header";
        lPaymentClass: Record "Payment Class";
    BEGIN
        //CFR le 22/09/2021 => Régie : bloquer débit/crédit
        IF lPaymentHeader.GET(Rec."No.") THEN
            IF lPaymentClass.GET(lPaymentHeader."Payment Class") THEN
                IF lPaymentClass.Sign = lPaymentClass.Sign::Crédit THEN
                    ERROR('Vous devez saisir les montants au crédit');
        //FIN CFR le 21/09/2021
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnBeforeValidateEvent', 'Credit Amount', false, false)]
    local procedure T10866_OnBeforeValidateEvent_CreditAmount(var Rec: Record "Payment Line"; var xRec: Record "Payment Line"; CurrFieldNo: Integer)
    VAR
        lPaymentHeader: Record "Payment Header";
        lPaymentClass: Record "Payment Class";
    BEGIN
        //CFR le 22/09/2021 => Régie : bloquer débit/crédit
        IF lPaymentHeader.GET(rec."No.") THEN
            IF lPaymentClass.GET(lPaymentHeader."Payment Class") THEN
                IF lPaymentClass.Sign = lPaymentClass.Sign::Débit THEN
                    ERROR('Vous devez saisir les montants au débit');
        //FIN CFR le 21/09/2021
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Line", 'OnBeforeCompareShipAndPickQty', '', false, false)]
    local procedure T7321_OnBeforeCompareShipAndPickQty(WarehouseShipmentLine: Record "Warehouse Shipment Line"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        Location: Record Location;
        Text002Lbl: Label 'must not be greater than %1 units', Comment = '%1 = Picked - Shipped';

    begin
        IsHandled := true;
        Location.get(WarehouseShipmentLine."Location Code");
        // AD Le 26-04-2007 => Pour pouvoir utiliser les kits sans PrélŠvements.
        //(Location."Require Pick AND "NOT "Assemble to Order"")
        IF (WarehouseShipmentLine."Qty. to Ship" > WarehouseShipmentLine."Qty. Picked" - WarehouseShipmentLine."Qty. Shipped") AND (Location."Require Pick")
                                                     // FIN AD Le 26-04-2007

                                                     THEN
            WarehouseShipmentLine.FIELDERROR(WarehouseShipmentLine."Qty. to Ship",
              STRSUBSTNO(Text002Lbl, WarehouseShipmentLine."Qty. Picked" - WarehouseShipmentLine."Qty. Shipped"));
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Header", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7320_OnAfterInsertEvent(var Rec: Record "Warehouse Shipment Header"; RunTrigger: Boolean)
    var
        WhseSetup: Record "Warehouse Setup";
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        WhseSetup.Get();
        //SID  AD Le 28-02-2008 => Tri par emplacement par def
        rec."Sorting Method" := rec."Sorting Method"::"Shelf or Bin";
        //FIN SID

        // AD Le 31-03-2015
        rec."Date Création" := TODAY;
        rec."Heure Création" := TIME;
        rec."Utilisateur Création" := COPYSTR(USERID, 1, 20);
        // FIN AD Le 31-03-2015

        // AD Le 01-12-2011
        IF rec."Code Emballage" = '' THEN
            rec.VALIDATE("Code Emballage", WhseSetup."Code Emballage par def");
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T7320_OnBeforeDeleteEvent(var Rec: Record "Warehouse Shipment Header"; RunTrigger: Boolean)
    var
        WhseShptLine: Record "Warehouse Shipment Line";
        SalesHeader: Record "Sales Header";
        OldNo: Code[20];
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        Rec.TESTFIELD(Status, Rec.Status::Open);

        // AD Le 19-09-2009 => On remet la commande en preparation
        WhseShptLine.RESET();
        WhseShptLine.SETRANGE("No.", Rec."No.");
        IF WhseShptLine.FINDFIRST() THEN
            REPEAT
                IF WhseShptLine."No." <> OldNo THEN
                    IF WhseShptLine."Source Document" = WhseShptLine."Source Document"::"Sales Order" THEN
                        IF SalesHeader.GET(SalesHeader."Document Type"::Order, WhseShptLine."Source No.") THEN BEGIN
                            SalesHeader.Status := SalesHeader.Status::Preparate;
                            SalesHeader.MODIFY();
                        END;

                OldNo := WhseShptLine."No.";
            UNTIL WhseShptLine.NEXT() = 0;
        // FIN AD Le 19-09-2009
    end;

    [EventSubscriber(ObjectType::Table, database::"Warehouse Shipment Header", 'OnBeforeValidateEvent', 'Shipping Agent Code', false, false)]
    local procedure T7320_OnBeforeValidateEvent(var Rec: Record "Warehouse Shipment Header"; var xRec: Record "Warehouse Shipment Header"; CurrFieldNo: Integer)
    VAR
        rec_SalesOrder: Record "Sales Header";
        Text50000Err: Label 'Transporteur "%1" impératif pour ce client !', Comment = '%1 = Transporteur Code';
    BEGIN
        IF xRec."Shipping Agent Code" = Rec."Shipping Agent Code" THEN
            EXIT;

        // MC Le 02-05-2011 => SYMTA -> Gestion de la préparation.
        // Gestion d'un transporteur impératif : Si le transporteur est impératif alors on ne peux modifier le transporteur dans cette table
        IF rec_SalesOrder.GET(Rec."Source Document", Rec."Source No.") THEN BEGIN
            IF rec_SalesOrder."Transporteur imperatif" THEN
                IF xRec."Shipping Agent Code" <> '' THEN
                    ERROR(STRSUBSTNO(Text50000Err, Rec."Shipping Agent Code"));
        END;
        // FIN MC Le 02-05-2011.

        // MCO Le 04-10-2018 => Régie
        //"CheckWeigth&Box"();
        // FIN MCO Le 04-10-2018
    end;

    [EventSubscriber(ObjectType::Table, database::"Warehouse Shipment Header", 'OnAfterDeleteEvent', '', false, false)]
    local procedure T7320_OnAfterDeleteEvent(var Rec: Record "Warehouse Shipment Header"; RunTrigger: Boolean)
    var
        lPICSCancelation: Record "PICS Cancelation";
    begin
        // CFR le 12/04/2022 - R‚gie => Annulation de PICS
        lPICSCancelation.Create(Rec."No.");
        // FIN CFR le 12/04/2022
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Header", 'OnBeforeValidateEvent', 'No.', false, false)]
    local procedure T7320_OnBeforeValidateEventNo(var Rec: Record "Warehouse Shipment Header"; var xRec: Record "Warehouse Shipment Header"; CurrFieldNo: Integer)
    begin
        Rec.TestField(Blocked, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Header", 'OnBeforeValidateEvent', 'Bin Code', false, false)]
    local procedure T7320_OnBeforeValidateEventBinCode(var Rec: Record "Warehouse Shipment Header"; var xRec: Record "Warehouse Shipment Header"; CurrFieldNo: Integer)
    begin
        Rec.TestField(Blocked, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Header", 'OnBeforeValidateEvent', 'Zone Code', false, false)]
    local procedure T7320_OnBeforeValidateEventZoneCode(var Rec: Record "Warehouse Shipment Header"; var xRec: Record "Warehouse Shipment Header"; CurrFieldNo: Integer)
    begin
        Rec.TestField(Blocked, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnAfterInsertEvent', '', false, false)]
    local procedure T99_OnAfterInsertEvent(var Rec: Record "Item Vendor"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // CFR le 23/03/2023 => R‚gie : initialisation de l'unit‚ d'achat … 1 par d‚faut
        IF (Rec."Purch. Unit of Measure" = '') THEN begin
            Rec."Purch. Unit of Measure" := '1';
            rec.Modify();
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnBeforeValidateEvent', "Vendor Item No.", false, false)]
    local procedure T99_OnBeforeValidateEvent(var Rec: Record "Item Vendor"; var xRec: Record "Item Vendor")
    var
        lPurchaseHeader: Record "Purchase Header";
    BEGIN
        BEGIN
            // AD Le 05-12-2011 => SYMTA
            IF Rec."Vendor Item No." <> xRec."Vendor Item No." THEN BEGIN
                // CFR le 16/04/2024 => R‚gie : ModifierCde(Question) >> ModifierAchats(Type, Question)
                Rec.ModifierAchats(lPurchaseHeader."Document Type"::Order, TRUE);
                Rec.ModifierAchats(lPurchaseHeader."Document Type"::"Return Order", TRUE);
                Rec.ModifierAchats(lPurchaseHeader."Document Type"::Quote, TRUE);
            END;
        END;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnBeforeInsertItemReference', '', false, false)]
    local procedure T99_OnBeforeInsertItemReference(var ItemVendor: Record "Item Vendor"; xItemVendor: Record "Item Vendor"; var IsHandled: Boolean)
    var
        Item: Record Item;
    BEGIN
        // MCO Le 01-04-2015 => Régie
        Item.GET(ItemVendor."Item No.");
        ItemVendor."Ref. Active" := Item."No. 2";
        // MCO Le 01-04-2015 => Régie
    END;

    [EventSubscriber(ObjectType::Table, Database::"Item Vendor", 'OnBeforeUpdateItemReference', '', false, false)]
    local procedure T99_OnBeforeUpdateItemReference(var ItemVendor: Record "Item Vendor"; var xItemVendor: Record "Item Vendor"; var IsHandled: Boolean)
    var
        Item: Record Item;
    BEGIN
        // MCO Le 01-04-2015 => Régie
        Item.GET(ItemVendor."Item No.");
        ItemVendor."Ref. Active" := Item."No. 2";
        // MCO Le 01-04-2015 => Régie
    END;

    [EventSubscriber(ObjectType::Table, Database::"Comment Line", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T97_OnBeforeInsertEvent(var Rec: Record "Comment Line"; RunTrigger: Boolean)
    VAR
        rec_Comment: Record "Comment Line";
        Text000Err: Label 'Deux lignes existent déjà pour le code %1.', Comment = '%1 = Ligne COmmentaire';
    BEGIN
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // MC Le 21-01-2014 => Commentaires TVA, restreindre certaines options
        IF rec."Table Name" = rec."Table Name"::"VAT Business Posting Group" THEN BEGIN
            rec_Comment.RESET();
            rec_Comment.SETRANGE("Table Name", rec."Table Name"::"VAT Business Posting Group");
            rec_Comment.SETRANGE("No.", rec."No.");
            IF rec_Comment.COUNT > 1 THEN
                ERROR(STRSUBSTNO(Text000Err, rec."No."));
        END;
        // FIN MC Le 21-01-2014
    END;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterInsertEvent', '', false, false)]
    local procedure T38_OnAfterInsertEvent(var Rec: Record "Purchase Header"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        Rec."Order Create User" := USERID;
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T38_OnBeforeDeleteEvent(var Rec: Record "Purchase Header"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // MCO Le 27-09-2016 => Régie : Ne pas pouvoir supprimer retour si au moins une ligne est réceptionnée
        Rec.CheckDelete();
        // FIN MCO Le 27-09-2016 => Régie : Ne pas pouvoir supprimer retour si au moins une ligne est réceptionnée
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterValidateEvent', 'Buy-from Vendor No.', false, false)]
    local procedure T38_OnAfterValidateEvent(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    var
        Vend: Record Vendor;
    begin
        Vend.get(rec."Buy-from Vendor No.");
        // AD Le 29-10-2009 => Validation ESKAPE
        Rec.ValidateBuyVendorNo();

        //FBRUN LE 21/02/2013 modification du libéllé comptable
        IF Rec."Document Type" = Rec."Document Type"::Invoice THEN
            Rec."Posting Description" := 'F.' + Rec."Pay-to Name"
        ELSE
            IF Rec."Document Type" = Rec."Document Type"::"Credit Memo" THEN
                Rec."Posting Description" := 'A.' + Rec."Pay-to Name"
            ELSE
                Rec."Posting Description" := Rec."Pay-to Name";
        //FIN

        // CFR le 03/09/2021 - SFD20210201 Incoterm 2020
        Rec."Incoterm City" := Vend."Incoterm City";
        // FIN CFR le 03/09/2021
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterValidateEvent', 'Order Date', false, false)]
    local procedure T38_OnAfterValidateEvent_OrderDate(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        // MCO Le 01-04-2015 => Régie
        rec.UpdatePurchLines(rec.FIELDCAPTION("Order Date"), FALSE);
        // FIN MCO Le 01-04-2015 => Régie
        // CFR le 10/05/2022 => R‚gie : UpdateLineDirectUnitCost()
        rec.UpdateLineDirectUnitCost();
        // FIN CFR le 10/05/2022
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeValidateDocumentDateWithPostingDate', '', false, false)]
    local procedure T38_OnBeforeValidateDocumentDateWithPostingDate(var PurchaseHeader: Record "Purchase Header"; xPurchaseHeader: Record "Purchase Header"; CallingFieldNo: Integer; var IsHandled: Boolean)
    begin
        IsHandled := true;
        // MCO Le 01-04-2015 => Régie
        IF (PurchaseHeader."Document Type" <> PurchaseHeader."Document Type"::Invoice) AND (PurchaseHeader."Document Type" <> PurchaseHeader."Document Type"::"Credit Memo") THEN
            IF PurchaseHeader."Document Date" <> PurchaseHeader."Posting Date" THEN
                // MCO Le 01-04-2015 => Régie
                PurchaseHeader.VALIDATE("Document Date", PurchaseHeader."Posting Date");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeValidateEvent', 'Vendor Order No.', false, false)]
    local procedure T38_OnBeforeValidateEvent(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        // MCO Le 27-09-2016 => Régie
        rec.UpdatePurchLines(rec.FIELDCAPTION("Vendor Order No."), FALSE);
        // FIN MCO Le 27-09-2016
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeValidateEvent', 'Vendor Invoice No.', false, false)]
    local procedure T38_OnBeforeValidateEvent_VendorInvoiceNo(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        // AD Le 03-02-2016 => MIG2015 -> En 2009 c'était codé dans la form. Je passe dans la table
        rec.VerifNoFactureFournisseur();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeValidateEvent', 'Vendor Cr. Memo No.', false, false)]
    local procedure T38_OnBeforeValidateEvent_VendorCrMemoNo(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        // AD Le 03-02-2016 => MIG2015 -> En 2009 c'était codé dans la form. Je passe dans la table
        Rec.VerifNoAvoirFournisseur();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeValidateEvent', 'Document Date', false, false)]
    local procedure T38_OnBeforeValidateEvent_DocumentDate(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        // MCO Le 01-04-2015 => Régie
        IF (rec."Document Type" = rec."Document Type"::Invoice) OR (rec."Document Type" = rec."Document Type"::"Credit Memo") THEN
            rec.VALIDATE("Posting Date", rec."Document Date");
        // FIN MCO Le 01-04-2015
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeGetNextArchiveDocOccurrenceNo', '', false, false)]
    local procedure T38_OnBeforeGetNextArchiveDocOccurrenceNo(var PurchaseHeader: Record "Purchase Header"; var IsHandled: Boolean)
    begin
        // MC Le 24-10-2011 => Informations de création
        PurchaseHeader."Create User ID" := COPYSTR(USERID, 1, MAXSTRLEN(PurchaseHeader."Create User ID"));
        // FIN MC Le 24-10-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeInitPostingDescription', '', false, false)]
    local procedure T38_OnBeforeInitPostingDescription(var PurchaseHeader: Record "Purchase Header"; var IsHandled: Boolean)
    begin
        IsHandled := true;
        //"Posting Description" := FORMAT("Document Type") + ' ' + "No.";

        //FBRUN LE 21/02/2013 modification du libéllé comptable
        IF PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Invoice THEN
            PurchaseHeader."Posting Description" := 'F.' + PurchaseHeader."Pay-to Name"
        ELSE
            IF PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::"Credit Memo" THEN
                PurchaseHeader."Posting Description" := 'A.' + PurchaseHeader."Pay-to Name"
            ELSE
                PurchaseHeader."Posting Description" := PurchaseHeader."Pay-to Name";
        //FIN
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterTransferSavedFields', '', false, false)]
    local procedure T38_OnAfterTransferSavedFields(var DestinationPurchaseLine: Record "Purchase Line"; SourcePurchaseLine: Record "Purchase Line")
    begin
        // MC le 10-11-2011 => SYMTA -> Conservation des Infos !
        DestinationPurchaseLine."Recherche référence" := SourcePurchaseLine."Recherche référence";
        DestinationPurchaseLine."Vendor Order No." := SourcePurchaseLine."Vendor Order No.";
        // FIN MC Le 10-11-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnBeforeTransferSavedFieldsDropShipment', '', false, false)]
    local procedure T38_OnBeforeTransferSavedFieldsDropShipment(var SourcePurchaseLine: Record "Purchase Line"; var DestinationPurchaseLine: Record "Purchase Line"; var IsHandled: Boolean)
    var
        SalesLine: Record "Sales Line";
        CopyDocMgt: Codeunit "Copy Document Mgt.";
    begin
        IsHandled := true;
        SalesLine.Get(
          SalesLine."Document Type"::Order,
          SourcePurchaseLine."Sales Order No.",
          SourcePurchaseLine."Sales Order Line No.");
        CopyDocMgt.TransfldsFromSalesToPurchLine(SalesLine, DestinationPurchaseLine);
        DestinationPurchaseLine."Drop Shipment" := SourcePurchaseLine."Drop Shipment";
        DestinationPurchaseLine."Purchasing Code" := SalesLine."Purchasing Code";
        DestinationPurchaseLine."Sales Order No." := SourcePurchaseLine."Sales Order No.";
        DestinationPurchaseLine."Sales Order Line No." := SourcePurchaseLine."Sales Order Line No.";
        Evaluate(DestinationPurchaseLine."Inbound Whse. Handling Time", '<0D>');
        DestinationPurchaseLine.Validate("Inbound Whse. Handling Time");
        SalesLine.Validate("Unit Cost (LCY)", DestinationPurchaseLine."Unit Cost (LCY)");
        SalesLine."Purchase Order No." := DestinationPurchaseLine."Document No.";
        SalesLine."Purch. Order Line No." := DestinationPurchaseLine."Line No.";
        // MC le 10-11-2011 => SYMTA -> Conservation des Infos !
        DestinationPurchaseLine."Recherche référence" := SourcePurchaseLine."Recherche référence";
        DestinationPurchaseLine."Vendor Order No." := SourcePurchaseLine."Vendor Order No.";
        // FIN MC Le 10-11-2011
        SalesLine.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnTransferSavedFieldsSpecialOrderOnBeforeSalesLineModify', '', false, false)]
    local procedure T38_OnTransferSavedFieldsSpecialOrderOnBeforeSalesLineModify(var SourcePurchaseLine: Record "Purchase Line"; var DestinationPurchaseLine: Record "Purchase Line"; var SalesLine: Record "Sales Line")
    begin
        // MC le 10-11-2011 => SYMTA -> Conservation des Infos !
        DestinationPurchaseLine."Recherche référence" := SourcePurchaseLine."Recherche référence";
        DestinationPurchaseLine."Vendor Order No." := SourcePurchaseLine."Vendor Order No.";
        // FIN MC Le 10-11-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnUpdatePurchLinesByChangedFieldName', '', false, false)]
    local procedure T38_OnUpdatePurchLinesByChangedFieldName(xPurchaseHeader: Record "Purchase Header"; ChangedFieldName: Text[100]; ChangedFieldNo: Integer; PurchHeader: Record "Purchase Header"; var PurchLine: Record "Purchase Line")
    begin
        // MCO Le 01-04-2015 => Régie
        if ChangedFieldNo = PurchHeader.FieldNo("Order Date") then
            IF (PurchLine."No." <> '') THEN
                PurchLine.VALIDATE("Order Date", PurchHeader."Order Date");
        // FIN MCO Le 01-04-2015 => Régie
        // MCO Le 27-09-2016 => Régie
        if ChangedFieldNo = PurchHeader.FieldNo("Vendor Order No.") then
            IF (PurchLine."No." <> '') THEN
                PurchLine.VALIDATE("Vendor Order No.", PurchHeader."Vendor Order No.");
        // FIN MCO Le 27-09-2016 => Régie
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnValidatePaymentTermsCodeOnBeforeValidateDueDate', '', false, false)]
    local procedure t38_OnValidatePaymentTermsCodeOnBeforeValidateDueDate(CurrentFieldNo: Integer; var IsHandled: Boolean; var PurchaseHeader: Record "Purchase Header"; xPurchaseHeader: Record "Purchase Header")
    begin
        if PurchaseHeader."Document Type" <> PurchaseHeader."Document Type"::Order then
            exit;
        IsHandled := true;
        IF PurchaseHeader."Date de dépat estimée (ETD)" <> 0D THEN
            PurchaseHeader.VALIDATE("Due Date", PurchaseHeader."Date de dépat estimée (ETD)");
    end;
    //>>khaoula
    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnValidatePaymentTermsCodeOnBeforeCalcDueDate', '', false, false)]
    local procedure t38_OnValidatePaymentTermsCodeOnBeforeCalcDueDate(var PurchaseHeader: Record "Purchase Header"; var xPurchaseHeader: Record "Purchase Header"; CalledByFieldNo: Integer; CallingFieldNo: Integer; var IsHandled: Boolean)
    var
        PaymentTerms: Record "Payment Terms";
    begin
        if PurchaseHeader."Document Type" <> PurchaseHeader."Document Type"::Order then
            exit;
        IsHandled := true;
        PaymentTerms.GET(PurchaseHeader."Payment Terms Code");
        IF PurchaseHeader."Date de dépat estimée (ETD)" <> 0D THEN
            PurchaseHeader."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", PurchaseHeader."Date de dépat estimée (ETD)");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnValidatePaymentTermsCodeOnBeforeValidateDueDateWhenBlank', '', false, false)]
    local procedure t38_OnValidatePaymentTermsCodeOnBeforeValidateDueDateWhenBlank(CurrentFieldNo: Integer; var IsHandled: Boolean; var PurchaseHeader: Record "Purchase Header"; xPurchaseHeader: Record "Purchase Header")
    var
        PaymentTerms: Record "Payment Terms";
    begin
        // if (PurchaseHeader."Payment Terms Code" <> '') AND (PurchaseHeader."Date de dépat estimée (ETD)" <> 0D)
        // then begin
        //     IsHandled := true;
        //     PaymentTerms.GET(PurchaseHeader."Payment Terms Code");

        //     IF PurchaseHeader.IsCreditDocType AND NOT PaymentTerms."Calc. Pmt. Disc. on Cr. Memos" THEN BEGIN
        //         //WF le 29-04-2010 => Analyse compl‚mentaire Avril 2010 õ3.4
        //         //    VALIDATE("Due Date","Document Date");
        //         IF PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Order THEN BEGIN
        //             IF PurchaseHeader."Date de dépat estimée (ETD)" <> 0D THEN
        //                 PurchaseHeader.VALIDATE("Due Date", PurchaseHeader."Date de dépat estimée (ETD)");
        //         END
        //         ELSE
        //             //FIN WF le 29-04-2010
        //             PurchaseHeader.VALIDATE("Due Date", PurchaseHeader."Document Date");

        //         PurchaseHeader.VALIDATE("Pmt. Discount Date", 0D);
        //         PurchaseHeader.VALIDATE("Payment Discount %", 0);
        //     END ELSE BEGIN
        //         //WF le 29-04-2010 => Analyse compl‚mentaire Avril 2010 õ3.4
        //         //    "Due Date" := CALCDATE(PaymentTerms."Due Date Calculation","Document Date");
        //         IF PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Order THEN BEGIN
        //             IF PurchaseHeader."Date de dépat estimée (ETD)" <> 0D THEN
        //                 PurchaseHeader."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", PurchaseHeader."Date de dépat estimée (ETD)");
        //         END
        //         ELSE
        //             PurchaseHeader."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", PurchaseHeader."Document Date");
        //         //FIN WF le 29-04-2010
        //         PurchaseHeader."Pmt. Discount Date" := CALCDATE(PaymentTerms."Discount Date Calculation", PurchaseHeader."Document Date");
        //         IF NOT UpdateDocumentDate THEN
        //             PurchaseHeader.VALIDATE("Payment Discount %", PaymentTerms."Discount %")
        //     END;
        // END
        // else
        if PurchaseHeader."Document Type" = PurchaseHeader."Document Type"::Order then BEGIN
            IsHandled := true;
            IF PurchaseHeader."Date de dépat estimée (ETD)" <> 0D THEN
                PurchaseHeader.VALIDATE("Due Date", PurchaseHeader."Date de dépat estimée (ETD)");
        END
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Header", 'OnAfterValidateEvent', 'Payment Terms Code', false, false)]
    local procedure t38_OnAfterValidateEventPaymentTermsCode(CurrFieldNo: Integer; var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header")
    var
        PaymentTerms: Record "Payment Terms";
    begin
        if (Rec."Payment Terms Code" <> '') AND (Rec."Date de dépat estimée (ETD)" <> 0D)
               then begin
            PaymentTerms.GET(Rec."Payment Terms Code");

            IF Rec.IsCreditDocType() AND NOT PaymentTerms."Calc. Pmt. Disc. on Cr. Memos" THEN BEGIN
                //WF le 29-04-2010 => Analyse compl‚mentaire Avril 2010 õ3.4
                //    VALIDATE("Due Date","Document Date");
                IF Rec."Document Type" = Rec."Document Type"::Order THEN BEGIN
                    IF Rec."Date de dépat estimée (ETD)" <> 0D THEN
                        Rec.VALIDATE("Due Date", Rec."Date de dépat estimée (ETD)");
                END
                ELSE
                    //FIN WF le 29-04-2010
                    Rec.VALIDATE("Due Date", Rec."Document Date");

                Rec.VALIDATE("Pmt. Discount Date", 0D);
                Rec.VALIDATE("Payment Discount %", 0);
            END ELSE BEGIN
                //WF le 29-04-2010 => Analyse compl‚mentaire Avril 2010 õ3.4
                //    "Due Date" := CALCDATE(PaymentTerms."Due Date Calculation","Document Date");
                IF Rec."Document Type" = Rec."Document Type"::Order THEN BEGIN
                    IF Rec."Date de dépat estimée (ETD)" <> 0D THEN
                        Rec."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", Rec."Date de dépat estimée (ETD)");
                END
                ELSE
                    Rec."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", Rec."Document Date");
                //FIN WF le 29-04-2010
                Rec."Pmt. Discount Date" := CALCDATE(PaymentTerms."Discount Date Calculation", Rec."Document Date");
                //  IF NOT UpdateDocumentDate THEN
                Rec.VALIDATE("Payment Discount %", PaymentTerms."Discount %")
            END;
            if xRec."Payment Terms Code" = Rec."Prepmt. Payment Terms Code" then
                Rec.Validate("Prepmt. Payment Terms Code", Rec."Payment Terms Code");
        END
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnValidateLineDiscountPercentOnAfterTestStatusOpen', '', false, false)]
    local procedure T39_OnValidateLineDiscountPercentOnAfterTestStatusOpen(var PurchaseLine: Record "Purchase Line"; var xPurchaseLine: Record "Purchase Line"; CallingFieldNo: Integer; var IsHandled: Boolean)
    var
        PurchaseHeader: Record "Purchase Header";
        Currency: Record Currency;
    begin

        if CallingFieldNo = PurchaseLine.FieldNo("Line Discount %") then begin
            IsHandled := true;
            PurchaseLine.GetPurchHeader(PurchaseHeader, Currency);
            PurchaseLine."Line Discount Amount" :=
              Round(
                Round(PurchaseLine.Quantity * PurchaseLine."Direct Unit Cost", Currency."Amount Rounding Precision") *
                PurchaseLine."Line Discount %" / 100,
                Currency."Amount Rounding Precision");

            // // MCO Le 24-03-2016 => Gestion des prix/Remise ESKAPE
            // {  CODE D'ORIGINE
            // "Line Discount Amount" :=
            //   ROUND(
            //     ROUND(Quantity * "Unit Price", Currency."Amount Rounding Precision") *
            //     "Line Discount %" / 100, Currency."Amount Rounding Precision");

            // }
            // Application de la remise sur l'unitaire + sans arrondi … cause des petits montants.
            PurchaseLine."Line Discount Amount" :=
            (PurchaseLine."Direct Unit Cost" * PurchaseLine.Quantity -
            (PurchaseLine."Direct Unit Cost" - (PurchaseLine."Direct Unit Cost" * PurchaseLine."Line Discount %") / 100) * PurchaseLine.Quantity);
            // FIN MCO Le 24-03-2016 => Gestion des prix/Remise ESKAPE
            if CduGSingleInstance.GetDropInvoiceDiscountAmountValue() then begin
                PurchaseLine."Inv. Discount Amount" := 0;
                PurchaseLine."Inv. Disc. Amount to Invoice" := 0;
            end;
            PurchaseLine.UpdateAmounts();
            PurchaseLine.UpdateUnitCost();

            // MC Le 15-09-2011 => Import des r‚ceptions, stocke la valeur par d‚faut
            IF NOT PurchaseLine.GetHideValidationFromFac() THEN
                PurchaseLine."Line Discount % Ctrl Fac" := PurchaseLine."Line Discount %"
            // FIN MC Le 15-09-2011
        end;
    end;

    [EventSubscriber(ObjectType::table, database::"Purchase Line", 'OnValidateQuantityOnBeforePurchaseLineVerifyChange', '', false, false)]
    local procedure T39_OnValidateQuantityOnBeforePurchaseLineVerifyChange(var PurchaseLine: Record "Purchase Line"; var xPurchaseLine: Record "Purchase Line"; StatusCheckSuspended: Boolean; var IsHandled: Boolean)
    var
        WhseValidateSourceLine: Codeunit "Purchases Warehouse Mgt.";
    begin
        // AD Le 28-08-2009 => Possibilit‚ de solder la ligne d'origine
        // WhseValidateSourceLine.PurchaseLineVerifyChange(Rec,xRec); => Code d'origine
        IF NOT PurchaseLine.GetShortCloseLine() THEN
            WhseValidateSourceLine.PurchaseLineVerifyChange(PurchaseLine, xPurchaseLine);
        // AD Le 28-08-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeValidateLineDiscountPercent', '', false, false)]
    local procedure T39_OnBeforeValidateLineDiscountPercent(var PurchaseLine: Record "Purchase Line"; DropInvoiceDiscountAmount: Boolean; var IsHandled: Boolean)
    begin
        CduGSingleInstance.SetDropInvoiceDiscountAmountValue(DropInvoiceDiscountAmount);
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Header", 'OnBeforeSendToPosting', '', false, false)]
    local procedure T38_OnBeforeSendToPosting(var PurchaseHeader: Record "Purchase Header"; var IsSuccess: Boolean; var IsHandled: Boolean)
    var
        ESK002Qst: Label 'La date de document est supérieure ou égale à la date de comptabilisation. Continuez ?';
        ESK003Err: Label 'Erreur pour respecter l''alerte !';
        ESK004Qst: Label 'La date de document est supérieure ou égale à la date du jour. Continuez ?';
    begin
        // AD Le 24-03-2016 =>
        IF (PurchaseHeader."Document Date" > PurchaseHeader."Posting Date") THEN
            IF NOT CONFIRM(ESK002Qst) THEN ERROR(ESK003Err);
        // FIN AD Le 24-03-2016

        // AD Le 24-03-2016 =>
        IF (PurchaseHeader."Document Date" > WORKDATE()) THEN
            IF NOT CONFIRM(ESK004Qst) THEN ERROR(ESK003Err);
        // FIN AD Le 24-03-2016
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnAfterInsertEvent', '', false, false)]
    local procedure T39_OnAfterInsertEvent(var Rec: Record "Purchase Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // MC Le 27-10-2011 => Suivi des commentaires de l'article
        rec."Insert.Eskape"();
        // FIN MC Le 27-10-2011
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnDeleteOnBeforeTestStatusOpen', '', false, false)]
    local procedure T39_OnDeleteOnBeforeTestStatusOpen(var PurchaseLine: Record "Purchase Line"; var IsHandled: Boolean)
    var
        ESK002Err: Label 'Impossible car la ligne est réceptionnée.';
        ESK003Err: Label 'Impossible car la ligne est expediée.';
    begin
        IsHandled := true;
        PurchaseLine.TestStatusOpen();
        // MCO Le 09-02-2017 => Régie : Ne pas supprimer si la ligne est livrée mˆme facturée
        IF PurchaseLine."Quantity Received" <> 0 THEN
            ERROR(ESK002Err);
        // MCO Le 07-06-2017 => Régie
        IF PurchaseLine."Return Qty. Shipped" <> 0 THEN
            ERROR(ESK003Err);
        // MCO Le 07-06-2017 => Régie
        // MCO Le 09-02-2017 => Régie : Ne pas supprimer si la ligne est livrée mˆme facturée

        // FIN MCO Le 09-02-2017 => Régie : Ne pas supprimer si la ligne est livrée mˆme facturée

    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Quantity', false, false)]
    local procedure T39_OnAfterValidateEvent_Quantity(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    var
        LItemVendor: Record "Item Vendor";
        LAjuster: Boolean;
        ESK50117Msg: Label 'Vous devez ajuster la quantité : Unité d''achat fournisseur %1 - Multiple d''achat %2 - Mini d''achat %3 -Référence %4', Comment = '%1 = Unité Achat ; %2 = Multiple d''achat ; %3 = Min ; %4 = Ref';
    begin
        IF (Rec."Document Type" = Rec."Document Type"::Invoice) AND (Rec."Prepayment %" <> 0) THEN
            // MC Le 15-09-2011 => Import des r‚ceptions, stocke la valeur par d‚faut
            IF NOT Rec.GetHideValidationFromFac() THEN
                Rec."Initial Quantity ctrl fac" := Rec.Quantity;
        // FIN MC Le 15-09-2011



        // AD Le 22-11-2011
        IF Rec.Type = Rec.Type::Item THEN BEGIN
            LAjuster := FALSE;
            IF LItemVendor.GET(Rec."Buy-from Vendor No.", Rec."No.", Rec."Variant Code") THEN BEGIN
                IF LItemVendor."Purch. Unit of Measure" <> Rec."Unit of Measure Code" THEN
                    LAjuster := TRUE;

                IF Rec."Quantity (Base)" < LItemVendor."Minimum Order Quantity" THEN
                    LAjuster := TRUE;

                // Arrondi au par combien
                IF (LItemVendor."Purch. Multiple" <> 0) AND (Rec."Quantity (Base)" <> 0) THEN
                    IF (Rec."Quantity (Base)" MOD LItemVendor."Purch. Multiple") <> 0 THEN
                        LAjuster := TRUE;



                IF Rec."Document Type" <> Rec."Document Type"::Invoice THEN
                    IF (Rec."Quantity (Base)" <> 0) AND LAjuster THEN
                        MESSAGE(ESK50117MSg,
                        LItemVendor."Purch. Unit of Measure",
                        LItemVendor."Purch. Multiple",
                        LItemVendor."Minimum Order Quantity",
                        LItemVendor."Item No.");
            END
            ELSE
                MESSAGE('Article inexistant pour le fournisseur %1', Rec."Buy-from Vendor No.");
        END;
        // AD Le 22-11-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Direct Unit Cost', false, false)]
    local procedure T39_OnAfterValidateEvent_DirectUnitCost(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        // MC Le 15-09-2011 => Import des r‚ceptions, stocke la valeur par d‚faut
        IF NOT rec.GetHideValidationFromFac() THEN
            rec."Direct Unit Cost Ctrl Fac" := rec."Direct Unit Cost";
        // FIN MC Le 15-09-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeUpdateOrderDateFromPlannedReceiptDate', '', false, false)]
    local procedure T39_OnBeforeUpdateOrderDateFromPlannedReceiptDate(var PurchaseLine: Record "Purchase Line"; CustomCalendarChange: array[2] of Record "Customized Calendar Change"; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeUpdateDirectUnitCost', '', false, false)]
    local procedure T39_OnBeforeUpdateDirectUnitCost(var PurchLine: Record "Purchase Line"; xPurchLine: Record "Purchase Line"; var Handled: Boolean; CalledByFieldNo: Integer; CurrFieldNo: Integer)
    begin
        if (CurrFieldNo <> 0) and (PurchLine."Prod. Order No." <> '') then
            PurchLine.UpdateAmounts();

        if ((CalledByFieldNo <> CurrFieldNo) and (CurrFieldNo <> 0)) or
           (PurchLine."Prod. Order No." <> '')
        then
            Handled := true;
        // CFR le 13/05/2022 => R‚gie : UpdateDirectUnitCost() Pas de mise … jour de tarif pour une ligne r‚ceptionn‚e non factur‚e
        IF (PurchLine."Quantity Received" > PurchLine."Quantity Invoiced") THEN BEGIN
            MESSAGE('Attention,\Le tarif n''est pas actualisé car il y a des quantités réceptionnées mais non facturées.');
            Handled := true;
        END;
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnDeleteOnAfterCalcShouldModifySalesOrderLine', '', false, false)]
    local procedure T39_OnDeleteOnAfterCalcShouldModifySalesOrderLine(var PurchaseLine: Record "Purchase Line"; var ShouldModifySalesOrderLine: Boolean)
    begin
        // MCO Le 15-01-2018 => Reprise des developpements perdu NAv 2009->2015  FR20150608
        PurchaseLine.CancelMvtEmplReturn(PurchaseLine);
        // FIN MCO Le 15-01-2018
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnBeforeCheckAssosiatedSalesOrder', '', false, false)]
    local procedure T39_OnBeforeCheckAssosiatedSalesOrder(var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        ESK50113Qst: Label 'Un article existe déjà voulez vous le remplacer ?';
        ESK50114Err: Label 'Vous avez choisit de ne pas modifier l''article.';
    begin
        if CurrentFieldNo = PurchaseLine.FieldNo(Type) then
            exit;
        // MCO Le 15-01-2018 => Migration et finalisation du dev sur les retours
        PurchaseLine.CheckUpdateQtyOnReturn();
        // FIN MCO Le 15-01-2018

        // MC Le 15-09-2011 => Demande de symta -> Demander la confirmation avant de modifier un article.

        IF (PurchaseLine."No." <> xPurchaseLine."No.") AND (PurchaseLine.ItemExists(xPurchaseLine."No.")) AND (PurchaseLine.Type = PurchaseLine.Type::Item) THEN
            IF NOT CONFIRM(ESK50113Qst) THEN
                ERROR(ESK50114Err);
        // FIN MC Le 15-09-2011
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnValidateNoOnCopyFromTempPurchLine', '', false, false)]
    local procedure T39_OnValidateNoOnCopyFromTempPurchLine(var PurchLine: Record "Purchase Line"; xPurchLine: Record "Purchase Line"; TempPurchaseLine: Record "Purchase Line" temporary)
    begin
        // AD Le 14-12-2009 => GDI -> Multireference -> Conservation du no saisi
        PurchLine."Recherche référence" := TempPurchaseLine."Recherche référence";
        // FIN AD Le 14-12-2009
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnInitHeaderDefaultsOnBeforeSetVATBusPostingGroup', '', false, false)]
    local procedure T39_OnInitHeaderDefaultsOnBeforeSetVATBusPostingGroup(var PurchaseLine: Record "Purchase Line"; var IsHandled: Boolean)
    var
        PurchHeader: Record "Purchase Header";
    begin
        PurchHeader.get(PurchaseLine."Document Type", PurchaseLine."Document No.");
        // MCO Le 27-09-2016 => Suivi des champs
        PurchaseLine."Type de commande" := PurchHeader."Type de commande";
        PurchaseLine."Vendor Order No." := PurchHeader."Vendor Order No.";
        // FIN MCO Le 27-09-2016 => Suivi des champs
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnValidateNoOnBeforeJobTaskIsSet', '', false, false)]
    local procedure T39_OnValidateNoOnBeforeJobTaskIsSet(PurchaseHeader: Record "Purchase Header"; var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; TempPurchaseLine: Record "Purchase Line" temporary; IsHandled: Boolean)
    begin
        // AD Le 03-06-2008 => Procedure Eskape
        PurchaseLine."ValidateNo.Eskape"();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnValidateQtyToReceiveOnAfterInitQty', '', false, false)]
    local procedure T39_OnValidateQtyToReceiveOnAfterInitQty(var PurchaseLine: Record "Purchase Line"; var xPurchaseLine: Record "Purchase Line"; CallingFieldNo: Integer; var IsHandled: Boolean)
    var
        CannotReceiveErrorInfo: ErrorInfo;
        QtyReceiveNotValidTitleLbl: Label 'Qty. to Receive isn''t valid';
        Text008: label 'Vous ne pouvez pas recevoir plus de %1 unité(s) pour l''article %2.';
        QtyReceiveActionLbl: Label 'Set value to %1', comment = '%1=Qty. to Receive';
        Text009: Label 'You cannot receive more than %1 base units.';
    begin
        IsHandled := true;
        if (((PurchaseLine."Qty. to Receive" < 0) xor (PurchaseLine.Quantity < 0)) and (PurchaseLine.Quantity <> 0) and (PurchaseLine."Qty. to Receive" <> 0)) or
                           (Abs(PurchaseLine."Qty. to Receive") > Abs(PurchaseLine."Outstanding Quantity")) or
                           (((PurchaseLine.Quantity < 0) xor (PurchaseLine."Outstanding Quantity" < 0)) and (PurchaseLine.Quantity <> 0) and (PurchaseLine."Outstanding Quantity" <> 0))
                        then begin
            CannotReceiveErrorInfo.Title := QtyReceiveNotValidTitleLbl;
            CannotReceiveErrorInfo.Message := StrSubstNo(Text008, PurchaseLine."Outstanding Quantity", PurchaseLine."No.");
            CannotReceiveErrorInfo.RecordId := PurchaseLine.RecordId;
            CannotReceiveErrorInfo.AddAction(StrSubstNo(QtyReceiveActionLbl, PurchaseLine."Outstanding Quantity"), Codeunit::"Purchase Line - Price", 'SetPurchaseReceiveQty');
            Error(CannotReceiveErrorInfo);
        end;
        if (((PurchaseLine."Qty. to Receive (Base)" < 0) xor (PurchaseLine."Quantity (Base)" < 0)) and (PurchaseLine."Quantity (Base)" <> 0) and (PurchaseLine."Qty. to Receive (Base)" <> 0)) or
           (Abs(PurchaseLine."Qty. to Receive (Base)") > Abs(PurchaseLine."Outstanding Qty. (Base)")) or
           (((PurchaseLine."Quantity (Base)" < 0) xor (PurchaseLine."Outstanding Qty. (Base)" < 0)) and (PurchaseLine."Quantity (Base)" <> 0) and (PurchaseLine."Outstanding Qty. (Base)" <> 0))
        then
            Error(Text009, PurchaseLine."Outstanding Qty. (Base)");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnLineDiscountAmountOnValidateOnBeforeUpdateAmounts', '', false, false)]
    local procedure T39_OnLineDiscountAmountOnValidateOnBeforeUpdateAmounts(var PurchaseLine: Record "Purchase Line")
    begin
        // MCO Le 27-09-2016 => Régie => On vide les remises 1 et 2 sinin incohérence
        PurchaseLine."Discount1 %" := 0;
        PurchaseLine."Discount2 %" := 0;
        // MCO Le 27-09-2016 => Régie => On vide les remises 1 et 2 sinin incohérence
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeValidateRequestedReceiptDate', '', false, false)]
    local procedure T39_OnBeforeValidateRequestedReceiptDate(var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; CurrFieldNo: Integer; var CustomCalendarChange: array[2] of Record "Customized Calendar Change"; var IsHandled: Boolean)
    var
        Text023Err: Label 'You cannot change the %1 when the %2 has been filled in.', Comment = '%1 = Date de Récéption requise ; %2 =  Date de Récéption promise  ';
    begin
        IsHandled := true;
        if (CurrFieldNo <> 0) and
                  (PurchaseLine."Promised Receipt Date" <> 0D)
               then
            Error(
              Text023Err,
              PurchaseLine.FieldCaption("Requested Receipt Date"),
              PurchaseLine.FieldCaption("Promised Receipt Date"));
        IF NOT (PurchaseLine."Requested Receipt Date" <> 0D) THEN
            IF PurchaseLine."Requested Receipt Date" <> xPurchaseLine."Requested Receipt Date" THEN begin
                PurchaseLine.GetUpdateBasicDates();
                // CFR le 12/04/2022 => R‚gie : Champ 10 [date de rangement] = [Expected Receipt Date] initialis‚e … date du jour par d‚faut pour les retours achats
                IF (PurchaseLine."Document Type" = PurchaseLine."Document Type"::"Return Order") THEN
                    PurchaseLine.VALIDATE("Expected Receipt Date", TODAY());
                // FIN CFR le 12/04/2022
            end;
        // FIN AD Le 02-11-2009

        // AD Le 05-11-2009 => Date de rangement = date de reception demandée
        PurchaseLine."Expected Receipt Date" := PurchaseLine."Requested Receipt Date";
        // FIN AD Le 05-11-2009

    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', "Lead Time Calculation", false, false)]
    local procedure T39_OnAfterValidateEvent_LeadTimeCalculation(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        if Rec."Requested Receipt Date" = 0D then begin
            // CFR le 12/04/2022 => R‚gie : Champ 10 [date de rangement] = [Expected Receipt Date] initialis‚e … date du jour par d‚faut pour les retours achats
            IF (Rec."Document Type" = Rec."Document Type"::"Return Order") THEN
                Rec.VALIDATE("Expected Receipt Date", TODAY());
            // FIN CFR le 12/04/2022
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Planned Receipt Date', false, false)]
    local procedure T39_OnAfterValidateEvent_PlannedReceiptDate(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        if Rec."Promised Receipt Date" = 0D then
            if Rec."Planned Receipt Date" = 0D then
                if Rec."Requested Receipt Date" = 0D then Begin
                    // CFR le 12/04/2022 => R‚gie : Champ 10 [date de rangement] = [Expected Receipt Date] initialis‚e … date du jour par d‚faut pour les retours achats
                    IF (Rec."Document Type" = Rec."Document Type"::"Return Order") THEN
                        Rec.VALIDATE("Expected Receipt Date", TODAY());
                    // FIN CFR le 12/04/2022
                end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterUpdateDates', '', false, false)]
    local procedure T39_OnAfterUpdateDates(var PurchaseLine: Record "Purchase Line")
    begin
        if PurchaseLine."Promised Receipt Date" = 0D then
            if PurchaseLine."Requested Receipt Date" = 0D then Begin
                // CFR le 12/04/2022 => R‚gie : Champ 10 [date de rangement] = [Expected Receipt Date] initialis‚e … date du jour par d‚faut pour les retours achats
                IF (PurchaseLine."Document Type" = PurchaseLine."Document Type"::"Return Order") THEN
                    PurchaseLine.VALIDATE("Expected Receipt Date", TODAY());
                // FIN CFR le 12/04/2022
            end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Promised Receipt Date', false, false)]
    local procedure T39_OnAfterValidateEvent(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    begin
        // AD Le 05-11-2009 => Date de rangement = date de reception confirmé si existe
        Rec."Expected Receipt Date" := Rec."Promised Receipt Date";
        // FIN AD Le 05-11-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnAfterValidateEvent', 'Work Center No.', false, false)]
    local procedure T39_OnAfterValidateEvent_WorkCenterNo(var Rec: Record "Purchase Line"; var xRec: Record "Purchase Line"; CurrFieldNo: Integer)
    var
        WorkCenter: record "Work Center";
    begin
        WorkCenter.GET(Rec."Work Center No.");
        // AD Le 22-09-2009 => FARGROUP -> Cout
        Rec."Frais Généraux Article" := WorkCenter."Overhead Rate";
        // FIN AD Le 22-09-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeUpdateAmounts', '', false, false)]
    local procedure T39_OnBeforeUpdateAmounts(var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        PurchaseHeader: Record "Purchase Header";
        Currency: Record Currency;
    begin
        PurchaseLine.GetPurchHeader(PurchaseHeader, Currency);
        // MC Le 15-09-2011 => Gestion des prix/Remise ESKAPE
        IF PurchaseLine."Line Discount %" <> 0 THEN
            PurchaseLine."Net Unit Cost" := ROUND(PurchaseLine."Direct Unit Cost" - (PurchaseLine."Direct Unit Cost" * PurchaseLine."Line Discount %") / 100, Currency."Amount Rounding Precision")
        ELSE
            PurchaseLine."Net Unit Cost" := ROUND(PurchaseLine."Direct Unit Cost", Currency."Amount Rounding Precision");

        IF NOT PurchaseLine.GetHideValidationFromFac() THEN
            PurchaseLine."Net Unit Cost ctrl fac" := PurchaseLine."Net Unit Cost";

        // FIN MC Le 15-09-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Line", 'OnBeforeGetDefaultBin', '', false, false)]
    local procedure T39_OnBeforeGetDefaultBin(var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; var IsHandled: Boolean)
    var
        Location: Record Location;
        WMSManagement: Codeunit "WMS Management";
        WhseIntegrationMgt: Codeunit "Whse. Integration Management";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        IsHandled := true;
        if (PurchaseLine.Type <> PurchaseLine.Type::Item) or PurchaseLine.IsNonInventoriableItem() then
            exit;
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        PurchaseLine."Zone Code" := '';
        // FIN MC Le 27-04-2011

        PurchaseLine."Bin Code" := '';
        if PurchaseLine."Drop Shipment" then
            exit;

        if (PurchaseLine."Location Code" <> '') and (PurchaseLine."No." <> '') then begin
            Location.Get(PurchaseLine."Location Code");
            if Location."Bin Mandatory" and not Location."Directed Put-away and Pick" then begin
                // WMSManagement.GetDefaultBin(PurchaseLine."No.", PurchaseLine."Variant Code", PurchaseLine."Location Code", PurchaseLine."Bin Code");
                // Nouveau code.
                CduLFunctions.ESK_GetDefaultBin(PurchaseLine."No.", PurchaseLine."Variant Code", PurchaseLine."Location Code", PurchaseLine."Bin Code", PurchaseLine."Zone Code");
                // FIN MC Le 27-04-2011
                if not PurchaseLine.IsInbound() and (PurchaseLine."Quantity (Base)" <> 0) then
                    WhseIntegrationMgt.CheckIfBinDedicatedOnSrcDoc(PurchaseLine."Location Code", PurchaseLine."Bin Code", false);

            end;
        end;

    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Item Reference Management", 'OnPurchaseReferenceNoLookUpOnAfterSetFilters', '', false, false)]
    local procedure CU5720_OnPurchaseReferenceNoLookUpOnAfterSetFilters(var ItemReference: Record "Item Reference"; PurchaseLine: Record "Purchase Line")
    begin
        ItemReference.SetFilter("Reference Type", '%1|%2', ItemReference."Reference Type"::Vendor, ItemReference."Reference Type"::"Non Qualifié");
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnBeforeInitType', '', false, false)]
    local procedure T39_OnBeforeInitType(var PurchaseLine: Record "Purchase Line"; xPurchaseLine: Record "Purchase Line"; var PurchHeader: Record "Purchase Header"; var IsHandled: Boolean)
    begin
        IsHandled := true;

        if PurchaseLine."Document No." <> '' then begin
            if not PurchHeader.Get(PurchaseLine."Document Type", PurchaseLine."Document No.") then
                exit;
            if (PurchHeader.Status = PurchHeader.Status::Released) and
               (xPurchaseLine.Type in [xPurchaseLine.Type::Item, xPurchaseLine.Type::"Fixed Asset"])
            then
                PurchaseLine.Type := PurchaseLine.Type::" "
            else
                // AD Le 21-10-2009 => Article par defaut pour simplifier la saisie
                // Type := xRec.Type;
                IF (xPurchaseLine.Type IN [xPurchaseLine.Type::" ", xPurchaseLine.Type::Item]) AND (NOT xPurchaseLine.ISEMPTY) THEN
                    PurchaseLine.Type := xPurchaseLine.Type
                ELSE
                    PurchaseLine.Type := PurchaseLine.Type::Item;
            // FIN AD Le 21-10-2009
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Purchase Line", 'OnBeforeValidateQuantity', '', false, false)]
    local procedure T39_OnBeforeValidateQuantity(var PurchaseLine: Record "Purchase Line"; var xPurchaseLine: Record "Purchase Line"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        ItemUnitOf: Record "Item Unit of Measure";
        Item: Record Item;
        Multiple: Decimal;
        ESK50116Msg: Label 'La quantité unitaire n''est pas un multiple d''achat (%1) ! ', Comment = '%1 = Multiple';
    begin
        with PurchaseLine do begin
            TestStatusOpen();

            // AD 30-08-2011 => Verif du multiple d'achat avec ajustement si besoin (si le message doit bloquant mettre dans le validateQte)
            IF (Type = Type::Item) THEN //AND (NOT HideValidationDialog)  THEN
              BEGIN
                Item := GetItem();
                //   { // AD Le 18-11-2011 => On travail sur les fiches fournisseurs
                //Multiple := Item."Purch. Multiple";
                // IF Multiple = 0 THEN BEGIN
                //     IF ItemVendor.GET("Buy-from Vendor No.", "No.", "Variant Code") THEN
                //         IF ItemVendor."Purch. Multiple" <> 0 THEN
                //             Multiple := ItemVendor."Purch. Multiple";
                // END;
                // IF (Multiple <> 0) AND (Quantity <> 0) THEN
                //     IF (Quantity MOD Multiple) <> 0 THEN BEGIN
                //         QteProposée := ROUND(Quantity, Multiple, '>');
                //         IF CONFIRM(ESK50112, FALSE, Multiple, QteProposée) THEN
                //             Quantity := QteProposée;
                //     END;
                //                                                       }
                // MC Le 26-10-2011 => Vérife du Quantité par de l'unité de mesure avec ajustement si besoin.
                IF ItemUnitOf.GET(Item."No.", "Unit of Measure Code") THEN BEGIN
                    Multiple := ItemUnitOf."Qty. per Unit of Measure";
                    IF (Multiple <> 0) AND ("Quantity (Base)" <> 0) THEN
                        IF ("Quantity (Base)" MOD Multiple) <> 0 THEN BEGIN
                            // CFR le 18/01/2024 - R‚gie : masquer message [...multiple d'achat...] inutile
                            IF NOT FctGetArrondirQteValue() THEN
                                MESSAGE(ESK50116Msg, Multiple);
                        END;
                END;
                // FIN MC Le 26-10-2011
            END;
            // FIN AD Le 21-10-2009

        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterInsertEvent', '', false, false)]
    local procedure T36_OnAfterInsertEvent(var Rec: Record "Sales Header"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        //WF le 30-04-2010 => FraGroup Analyse complémentaire Avril 2010 õ2.11
        IF Rec."Document Type" = Rec."Document Type"::"Credit Memo" THEN begin
            Rec."Sans mouvement de stock" := TRUE;
            rec.Modify();
        end;
        //FIN WF le 30-04-2010
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterModifyEvent', '', false, false)]
    local procedure T36_OnAfterModifyEvent(var Rec: Record "Sales Header"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // CFR le 18/11/2020 : Régie > Code utilisateur modification
        rec.VALIDATE("Modify User ID", USERID);
        rec.VALIDATE("Modify Date", Today);
        rec.VALIDATE("Modify Time", TIME);
        // FIN CFR le 18/11/2020 : Régie > Code utilisateur modification
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T36_OnBeforeDeleteEvent(var Rec: Record "Sales Header"; RunTrigger: Boolean)
    var
        rec_salesline: Record "sales Line";
        _qte_exp: Decimal;
        lm_txt1Err: label 'Suppression impossible, commande déjà livrée';
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        //LM le 03-06-2013  =>verrouille la suppression de commande si déja livré
        _qte_exp := 0;
        rec_salesline.RESET();
        rec_salesline.SETRANGE("Document Type", rec."Document Type"); // AD Le 20-01-2014
        rec_salesline.SETRANGE(rec_salesline."Document No.", rec."No.");
        IF rec_salesline.FindSet() THEN
            REPEAT
                _qte_exp += rec_salesline."Quantity Shipped";
            UNTIL rec_salesline.NEXT() = 0;
        IF _qte_exp > 0 THEN ERROR(lm_txt1Err);
        //fin LM le 03-06-2013
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnDeleteOnBeforeArchiveSalesDocument', '', false, false)]
    local procedure T36_OnDeleteOnBeforeArchiveSalesDocument(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header")
    begin
        // AD Le 04-12-2012
        IF salesheader."Document Type" IN [salesheader."Document Type"::Quote, salesheader."Document Type"::"Return Order"] THEN
            salesheader.TESTFIELD(Status, salesheader.Status::Open);
        // FIN AD Le 04-12-2012
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnValidateSellToCustomerNoOnBeforeCheckBlockedCustOnDocs', '', false, false)]
    local procedure T36_OnValidateSellToCustomerNoOnBeforeCheckBlockedCustOnDocs(var SalesHeader: Record "Sales Header"; var Cust: Record Customer; var IsHandled: Boolean)
    var
        ESK000Err: Label 'Ce client ne peut pas être utilisé comme donneur d''ordre en devis/commande : %1', Comment = '%1 = Client Bloqué ou Suprrime';
    begin
        // MCO Le 21-11-2018 => Régie
        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::Quote) OR (SalesHeader."Document Type" = SalesHeader."Document Type"::Order) THEN BEGIN
            IF Cust."Bloquer en commande" THEN ERROR(ESK000Err, Cust.FIELDCAPTION("Bloquer en commande"));
            IF Cust.Supprimé THEN ERROR(ESK000Err, Cust.FIELDCAPTION(Supprimé));
        END;
        // FIN MCO Le 21-11-2018
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterValidateEvent', 'Sell-to Customer No.', false, false)]
    local procedure T36_OnAfterValidateEvent_SellToCustomerNo(var Rec: Record "Sales Header"; var xRec: Record "Sales Header")
    var
        Cust: Record Customer;
    begin
        Cust.get(rec."Sell-to Customer No.");
        // CFR le 06/04/2023 - R‚gie : Incoterm ICC 2020 Ventes
        IF (Cust."Salesperson Code" = '19') THEN BEGIN
            Rec."Incoterm City" := Cust."Incoterm City";
            Rec."Incoterm Code" := Cust."Incoterm Code";
        END;
        // FIN CFR le 06/04/2023
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterCheckSellToCust', '', false, false)]
    local procedure T36_OnAfterCheckSellToCust(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; Customer: Record Customer; CurrentFieldNo: Integer)
    begin
        // AD Le 12-11-2009 => On prend le vendeur du client livré
        // FBO le 21-04-2017 => Correction alimentation Vendeur
        // "Salesperson Code" := Cust."Salesperson Code";
        SalesHeader.VALIDATE("Salesperson Code", Customer."Salesperson Code");
        // FIN FBO le 21-04-2017
        // FIN AD Le 12-11-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnCopySelltoCustomerAddressFieldsFromCustomerOnBeforeAssignRespCenter', '', false, false)]
    local procedure T36_OnCopySelltoCustomerAddressFieldsFromCustomerOnBeforeAssignRespCenter(var SalesHeader: Record "Sales Header"; var SellToCustomer: Record Customer)
    begin
        IF SellToCustomer.Entete <> '' THEN
            SalesHeader."Sell-to Customer Name" := SellToCustomer.Entete + ' ' + SellToCustomer.Name
        ELSE
            SalesHeader."Sell-to Customer Name" := SellToCustomer.Name;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnValidateSellToCustomerNoOnBeforeUpdateSellToCont', '', false, false)]
    local procedure T36_OnValidateSellToCustomerNoOnBeforeUpdateSellToCont(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; SellToCustomer: Record Customer; var SkipSellToContact: Boolean)
    begin
        // MC Le 29-04-2011 => SYMTA -> Mise … jour de l'adresse Destinataire.
        IF (SellToCustomer."Adresse Expédition" <> '') OR (SellToCustomer."Adresse 2 Expédition" <> '') OR (SellToCustomer."Ville Expédition" <> '') OR
           (SellToCustomer."Code postal Expédition" <> '') THEN BEGIN

            SalesHeader."Ship-to Address" := SellToCustomer."Adresse Expédition";
            SalesHeader."Ship-to Address 2" := SellToCustomer."Adresse 2 Expédition";
            SalesHeader."Ship-to City" := SellToCustomer."Ville Expédition";
            SalesHeader."Ship-to Post Code" := SellToCustomer."Code postal Expédition";
        END;
        // FIN MC Le 29-04-2011 => SYMTA -> Mise … jour de l'adresse Destinataire.
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnBeforeValidateEvent', 'Ship-to City', false, false)]
    local procedure T36_OnBeforeValidateEvent_ShipToCity(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        rec.SetCentraleActiveGroupementPayeur();
        // FIN CFR le 24/09/2021
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnValidateBillToCustomerNoOnBeforeCheckBlockedCustOnDocs', '', false, false)]
    local procedure T36_OnValidateBillToCustomerNoOnBeforeCheckBlockedCustOnDocs(var SalesHeader: Record "Sales Header"; var Cust: Record Customer; var IsHandled: Boolean)
    begin
        // AD Le 07-09-2009 => FARGROUP -> Client Facturé
        //GetCust("Bill-to Customer No."); => Code d'origine
        SalesHeader.GetCust(SalesHeader."Invoice Customer No.");
        // FIN AD Le 07-09-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnSetBillToCustomerAddressFieldsFromCustomerOnAfterAssignBillToCustomerAddress', '', false, false)]
    local procedure T36_OnSetBillToCustomerAddressFieldsFromCustomerOnAfterAssignBillToCustomerAddress(var SalesHeader: Record "Sales Header"; Customer: Record Customer)
    begin
        IF Customer.Entete <> '' THEN
            SalesHeader."Bill-to Name" := Customer.Entete + ' ' + Customer.Name
        ELSE
            SalesHeader."Bill-to Name" := Customer.Name;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnAfterSetFieldsBilltoCustomer', '', false, false)]
    local procedure T36_OnAfterSetFieldsBilltoCustomer(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; Customer: Record Customer; CUrrentFieldNo: Integer; SkipBillToContact: Boolean)
    var
        LCentrale: Record Customer;
    begin
        // FBRUN le 02/05/2011 => SYMTA suivi type facturation
        SalesHeader."Regroupement Facturation" := Customer."Regroupement Facturation";


        // AD Le 12-11-2009 => On prend le vendeur du client livré
        //"Salesperson Code" := Cust."Salesperson Code";
        // FIN AD Le 12-11-2009

        // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs suivi des champs.
        SalesHeader."Sous Groupe Tarifs" := Customer."Sous Groupe Tarifs";
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        SalesHeader.SetCentraleActiveGroupementPayeur();
        //"Groupement payeur" := Cust."Groupement payeur";
        // FIN CFR le 24/09/2021
        // FIN MC Le 19-04-2011

        // MCO Le 23-03-2016 => Prise de l'information sur la base
        // Mise en commentaire
        // {
        // // GR Le 05-10-2015 => SYMTA affichage code zone
        // "Service Zone Code" := Cust."Service Zone Code";
        // }
        // FIN MCO Le 23-03-2016

    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnBeforeValidateEvent', 'Order Date', false, false)]
    local procedure T36_OnBeforeValidateEvent_OrderDate(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        Rec.SetCentraleActiveGroupementPayeur();
        // FIN CFR le 24/09/2021
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnSetShipToCustomerAddressFieldsFromShipToAddrOnAfterCalcShouldCopyLocationCode', '', false, false)]
    local procedure T36_OnSetShipToCustomerAddressFieldsFromShipToAddrOnAfterCalcShouldCopyLocationCode(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; ShipToAddress: Record "Ship-to Address"; var ShouldCopyLocationCode: Boolean)
    begin
        // MC Le 23-11-2011 => SYMTA -> Gestion des conditions de livraisons
        SalesHeader.ModifShipmentMethodFromOrderTy();
        // FIN MC Le 23-11-2011
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnAfterCopyShipToCustomerAddressFieldsFromShipToAddr', '', false, false)]
    local procedure T36_OnAfterCopyShipToCustomerAddressFieldsFromShipToAddr(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; ShipToAddress: Record "Ship-to Address")
    begin
        // MCO Le 23-03-2016 => Prise de l'information sur la base
        SalesHeader."Service Zone Code" := ShipToAddress."Service Zone Code";
        // FIN MCO Le 23-03-2016
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnCopyShipToCustomerAddressFieldsFromCustOnBeforeValidateShippingAgentFields', '', false, false)]
    local procedure T36_OnCopyShipToCustomerAddressFieldsFromCustOnBeforeValidateShippingAgentFields(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; SellToCustomer: Record Customer; var IsHandled: Boolean)
    begin
        IF SellToCustomer.Entete <> '' THEN
            SalesHeader."Ship-to Name" := SellToCustomer.Entete + ' ' + SellToCustomer.Name
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeGetShipmentMethodCode', '', false, false)]
    local procedure T36_OnBeforeGetShipmentMethodCode(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    var
        ShipToAddress: Record "Ship-to Address";
        IsShipmentMethodCodeAssigned: Boolean;
        Customer: Record Customer;
    begin
        IsHandled := true;
        if SalesHeader."Ship-to Code" <> '' then begin
            ShipToAddress.SetLoadFields("Shipment Method Code");
            ShipToAddress.Get(SalesHeader."Sell-to Customer No.", SalesHeader."Ship-to Code");
            if ShipToAddress."Shipment Method Code" <> '' then begin
                SalesHeader.Validate("Shipment Method Code", ShipToAddress."Shipment Method Code");
                IsShipmentMethodCodeAssigned := true;
            end;
        end;

        if (not IsShipmentMethodCodeAssigned) and (SalesHeader."Sell-to Customer No." <> '') then begin
            SalesHeader.GetCust(SalesHeader."Sell-to Customer No.");
            SalesHeader.Validate("Shipment Method Code", Customer."Shipment Method Code");
        end;
        // MC Le 23-11-2011 => SYMTA -> Gestion des conditions de livraisons
        SalesHeader.ModifShipmentMethodFromOrderTy();
        // FIN MC Le 23-11-2011
        // AD Le 15-09-2011
        SalesHeader."Mode d'expédition" := Customer."Mode d'expédition";
        // FIN
        // MCO Le 23-03-2016 => Prise de l'information sur la base
        SalesHeader."Service Zone Code" := Customer."Service Zone Code";
        // FIN MCO Le 23-03-2016
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnValidatePaymentTermsCodeOnBeforeValidateDueDate', '', false, false)]
    local procedure T36_OnValidatePaymentTermsCodeOnBeforeValidateDueDate(var SalesHeader: Record "Sales Header"; xSalesHeader: Record "Sales Header"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        PaymentTerms: Record "Payment Terms";
    begin
        IsHandled := true;
        PaymentTerms.GET(SalesHeader."Payment Terms Code");
        SalesHeader."Due Date" := CALCDATE(PaymentTerms."Due Date Calculation", SalesHeader."Document Date");
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeMessageIfSalesLinesExist', '', false, false)]
    local procedure T36_OnBeforeMessageIfSalesLinesExist(SalesHeader: Record "Sales Header"; ChangedFieldCaption: Text; var IsHandled: Boolean)
    begin
        if ChangedFieldCaption = SalesHeader.FieldCaption("Customer Disc. Group") then begin
            IsHandled := true;
            // MC Le 08-11-2011 => Recalcule de la remise
            // Ancien code
            //MessageIfSalesLinesExist(FIELDCAPTION("Customer Disc. Group"));
            // Nouveau code
            SalesHeader.RecreateSalesLines(SalesHeader.FIELDCAPTION("Customer Disc. Group"));
            // FIN MC Le 08-11-2011
        end;
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnAfterValidateEvent', "Salesperson Code", false, false)]
    local procedure T36_OnAfterValidateEvent(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    var
        LrecSalesPerson: Record "Salesperson/Purchaser";
    begin
        // FBO le 05-04-2017 => FE20170324
        IF LrecSalesPerson.GET(Rec."Salesperson Code") THEN
            Rec.VALIDATE("Export Salesperson", LrecSalesPerson."Export Salesperson");
        // FIN FBO le 05-04-2017
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnAfterValidateEvent', "Document Date", false, false)]
    local procedure T36_OnAfterValidateEvent_DocumentDate(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        //FBRUN le 21/04/2011 =>FBRUN ECHEANCE FRACTIONNEES
        Rec.VALIDATE("Payment Terms Code 2");
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeValidateEvent', "Document Date", false, false)]
    local procedure T36_OnBeforeValidateEvent(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        // AD Le 22-06-2009 => SPECIF ESKAPE
        Rec.ControlExternalNo();
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeValidatePaymentMethodCode', '', false, false)]
    local procedure T36_OnBeforeValidatePaymentMethodCode(var SalesHeader: Record "Sales Header"; PaymentMethod: Record "Payment Method"; var IsHandled: Boolean)
    var
        SEPADirectDebitMandate: Record "SEPA Direct Debit Mandate";
    begin
        IsHandled := true;
        PaymentMethod.Init();
        if SalesHeader."Payment Method Code" <> '' then
            PaymentMethod.Get(SalesHeader."Payment Method Code");
        if PaymentMethod."Direct Debit" then begin
            SalesHeader."Direct Debit Mandate ID" := SEPADirectDebitMandate.GetDefaultMandate(SalesHeader."Bill-to Customer No.", SalesHeader."Due Date");
            if SalesHeader."Payment Terms Code" = '' then
                SalesHeader."Payment Terms Code" := PaymentMethod."Direct Debit Pmt. Terms Code";
        end else
            SalesHeader."Direct Debit Mandate ID" := '';
        SalesHeader."Bal. Account Type" := PaymentMethod."Bal. Account Type";
        SalesHeader."Bal. Account No." := PaymentMethod."Bal. Account No.";
        //FBRUN le 20/04/2011 => Gestion Bordereau
        SalesHeader."Scénario paiement" := PaymentMethod."Scénario paiement";

        IF (SalesHeader."Document Type" = SalesHeader."Document Type"::"Return Order") OR (SalesHeader."Document Type" = SalesHeader."Document Type"::"Credit Memo") THEN BEGIN
            SalesHeader."Bal. Account No." := '';
            //FBRUN le 20/04/2011 => Gestion Bordereau
            SalesHeader."Scénario paiement" := '';
        END;
        if SalesHeader."Bal. Account No." <> '' then begin
            SalesHeader.TestField("Applies-to Doc. No.", '');
            SalesHeader.TestField("Applies-to ID", '');
            Clear(SalesHeader."Payment Service Set ID");
        end;
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeValidateShippingAgentCode', '', false, false)]
    local procedure T36_OnBeforeValidateShippingAgentCode(var SalesHeader: Record "Sales Header"; var xSalesHeader: Record "Sales Header"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        rec_Cust: Record Customer;
        rec_Transporteur: Record "Shipping Agent";
        Text50004Qst: Label 'Transporteur %1 impératif pour ce client ! Continuer ?', Comment = '%1 = Nom Transporteur';
        Text50005Err: Label 'Traitement interrompu pour respecter l''alerte.';
    begin
        SalesHeader.TESTFIELD(Status, SalesHeader.Status::Open);
        IF xSalesHeader."Shipping Agent Code" = SalesHeader."Shipping Agent Code" THEN
            IsHandled := true;
        // MC Le 27-04-2011 => SYMTA -> Transport.
        // Si le champ [Transporteur impératif] est … VRAI alors si changement de transporteur : Avertissement.
        IF SalesHeader."Transporteur imperatif" THEN BEGIN
            // On récupŠre le transporteur du client pour afficher le message.
            IF rec_Cust.GET(SalesHeader."Sell-to Customer No.") THEN;
            IF rec_Transporteur.GET(rec_Cust."Shipping Agent Code") THEN;
            // Ouverture de la boite de dialogue.
            IF NOT CONFIRM(STRSUBSTNO(Text50004Qst, rec_Transporteur.Name)) THEN
                ERROR(Text50005Err);
        END;
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeValidateEvent', "Campaign No.", false, false)]
    local procedure T36_OnBeforeValidateEvent_CampaignNo(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        // AD Le 05-07-2010 => Interdiction de modifier la campagne si des lignes
        // AD Le 17-12-2010 => On autorise la modif.
        //IF UPPERCASE(USERID) <> 'ESKAPE' THEN
        //  BEGIN
        //    IF SalesLinesExist THEN
        //      IF xRec."Campaign No." <> "Campaign No." THEN
        //         ERROR(Text50003, FIELDNAME("Campaign No."));
        //  END
        //ELSE
        BEGIN
            IF Rec.SalesLinesExist() THEN
                Rec.RecreateSalesLines(Rec.FIELDCAPTION("Campaign No."))
        END;
        // FIN AD Le 05-07-2010
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnAfterInitFromBillToCustTemplate', '', false, false)]
    local procedure T36_OnAfterInitFromBillToCustTemplate(var SalesHeader: Record "Sales Header"; BillToCustTemplate: Record "Customer Templ.")
    begin
        // MC Le 23-11-2011 => SYMTA -> Gestion des conditions de livraisons
        SalesHeader.ModifShipmentMethodFromOrderTy();
        // FIN MC Le 23-11-2011
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnInitRecordOnBeforeAssignOrderDate', '', false, false)]
    local procedure T36_OnInitRecordOnBeforeAssignOrderDate(var SalesHeader: Record "Sales Header"; var NewOrderDate: Date)
    begin
        // AD Le 24-05-2011 => SYMTA
        SalesHeader."Requested Delivery Date" := WORKDATE();
        // FIN AD Le 24-05-2011
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnInitRecordOnBeforeGetNextArchiveDocOccurrenceNo', '', false, false)]
    local procedure T36_OnInitRecordOnBeforeGetNextArchiveDocOccurrenceNo(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    var
        TypeParam: Code[20];
        Param: Record "Generals Parameters";
        SalesSetup: Record "Sales & Receivables Setup";
    begin
        // AD Le 07-09-2009 => FARGROUP -> Gestion du type de document
        CASE SalesHeader."Document Type" OF
            SalesHeader."Document Type"::Order:
                TypeParam := 'VTE-DOC-CDE';
            SalesHeader."Document Type"::"Return Order":
                TypeParam := 'VTE-DOC-AVO';
            SalesHeader."Document Type"::Quote:
                TypeParam := 'VTE-DOC-DEV';
            SalesHeader."Document Type"::"Credit Memo":
                TypeParam := 'VTE-DOC-AVO';
            SalesHeader."Document Type"::Invoice:
                TypeParam := 'VTE-DOC-FAC';
        END;
        Param.RESET();
        Param.SETRANGE(Type, TypeParam);
        Param.SETRANGE(Default, TRUE);
        IF Param.FINDFIRST() THEN
            SalesHeader.VALIDATE("Source Document Type", Param.Code);
        // FIN AD Le 07-09-2009

        // AD Le 27-10-2009 => Initialisation champ
        SalesHeader.VALIDATE("Create User ID", USERID);
        SalesHeader.VALIDATE("Order Create User", USERID);
        SalesHeader.VALIDATE("Create Date", Today);
        SalesHeader.VALIDATE("Create Time", TIME);
        //VALIDATE("Order Create User", USERID);
        // FIN AD Le 27-10-2009

        // CFR le 18/11/2020 : Régie > Code utilisateur modification
        SalesHeader.VALIDATE("Modify User ID", USERID);
        SalesHeader.VALIDATE("Modify Date", Today);
        SalesHeader.VALIDATE("Modify Time", TIME);
        // FIN CFR le 18/11/2020 : Régie > Code utilisateur modification  
        // CFR le 04/10/2023 - R‚gie : Date de validit‚ des devis champ 50090
        SalesHeader."Quote Validity Date" := TODAY();
        SalesSetup.GET();
        IF (FORMAT(SalesSetup."Quote Default Validity Period") <> '') THEN
            SalesHeader."Quote Validity Date" := CALCDATE(SalesSetup."Quote Default Validity Period", SalesHeader."Quote Validity Date");
        // FIN CFR le 04/10/2023
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnUpdateSalesLinesByFieldNoOnBeforeSalesLineModify', '', false, false)]
    local procedure T36_OnUpdateSalesLinesByFieldNoOnBeforeSalesLineModify(var SalesLine: Record "Sales Line"; ChangedFieldNo: Integer; CurrentFieldNo: Integer)
    var
        SalesHeader: Record "Sales Header";
        lGroupementMarque: Record "Groupement/Marque";
        lItem: Record Item;
    begin
        SalesHeader.get(SalesLine."Document Type", SalesLine."Document No.");
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        if ChangedFieldNo = SalesHeader.FieldNo("Centrale Active") then BEGIN
            SalesLine."Gerer par groupement" := FALSE;
            IF (SalesHeader."Centrale Active" <> '') AND (SalesLine.Type = SalesLine.Type::Item) AND (SalesLine."No." <> '') AND lItem.GET(SalesLine."No.") THEN BEGIN
                IF lGroupementMarque.GET(lGroupementMarque.Type::Marque, SalesHeader."Centrale Active", lItem."Manufacturer Code") THEN
                    SalesLine."Gerer par groupement" := TRUE;
            END;
        END;
        // FIN CFR le 24/09/2021
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterUpdateAmountsDone', '', false, false)]
    local procedure T37_OnAfterUpdateAmountsDone(var SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; CurrentFieldNo: Integer)
    begin
        // CFR le 26/10/2023 - R‚gie correction du calcul de marge
        SalesLine.CalcMarge();
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeConfirmDeletion', '', false, false)]
    local procedure T36_OnBeforeConfirmDeletion(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean; var Result: Boolean)
    var
        Error001Err: Label 'Impossible de supprimer une commande dont le Statut n''est pas ouvert et donc le Statut livraison n''est pas à livrer';
    begin
        // DZ Le 01/06/2012 => Erreur si Commande statut <> "ouvert" ou statut livraison <> "a livrer"
        IF SalesHeader."Document Type" = SalesHeader."Document Type"::Order THEN BEGIN
            IF (SalesHeader.Status <> SalesHeader.Status::Open) OR (SalesHeader."Shipment Status" <> SalesHeader."Shipment Status"::"A Livrer") THEN BEGIN
                ERROR(Error001Err);
            END;
        END;
        // Fin DZ Le 01/06/2012
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnRecreateSalesLinesOnAfterSetSalesLineFilters', '', false, false)]
    local procedure T36_OnRecreateSalesLinesOnAfterSetSalesLineFilters(var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line")
    begin
        // MC Le 23-11-2011 => Pour eviter le bug sur le formulaire des commentaires liés
        SalesLine.SetHideValidationDialog(TRUE);
        // FIN MC Le 23-11-2011
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeCheckCreditMaxBeforeInsert', '', false, false)]
    local procedure T36_OnBeforeCheckCreditMaxBeforeInsert(var SalesHeader: Record "Sales Header"; FilterContNo: Code[20]; FilterCustNo: Code[20]; HideCreditCheckDialogue: Boolean; var IsHandled: Boolean)
    var
        SalesHeader2: Record "Sales Header";
        ContBusinessRelation: Record "Contact Business Relation";
        Cont: Record Contact;
        Customer: Record Customer;
        CustCheckCreditLimit: Codeunit "Cust-Check Cr. Limit";
    begin
        IsHandled := true;
        if HideCreditCheckDialogue then
            exit;

        if (SalesHeader.GetFilterCustNo() <> '') or (SalesHeader."Sell-to Customer No." <> '') then begin
            if SalesHeader."Sell-to Customer No." <> '' then
                Customer.Get(SalesHeader."Sell-to Customer No.")
            else
                Customer.Get(SalesHeader.GetFilterCustNo());
            if Customer."Bill-to Customer No." <> '' then
                SalesHeader2."Bill-to Customer No." := Customer."Bill-to Customer No."
            else
                SalesHeader2."Bill-to Customer No." := Customer."No.";
            IF NOT SalesHeader.GetHideValidationDialog() THEN
                CustCheckCreditLimit.SalesHeaderCheck(SalesHeader);
        end else
            if SalesHeader.GetFilterContNo() <> '' then begin
                Cont.Get(SalesHeader.GetFilterContNo());
                if ContBusinessRelation.FindByContact(ContBusinessRelation."Link to Table"::Customer, Cont."Company No.") then begin
                    Customer.Get(ContBusinessRelation."No.");
                    if Customer."Bill-to Customer No." <> '' then
                        SalesHeader2."Bill-to Customer No." := Customer."Bill-to Customer No."
                    else
                        SalesHeader2."Bill-to Customer No." := Customer."No.";
                    IF NOT SalesHeader.GetHideValidationDialog() THEN
                        CustCheckCreditLimit.SalesHeaderCheck(SalesHeader);
                end;
            end;
        //MC Le 19-05-2010 => Impossible de passer le devis en commande … cause d'un runmodal Appel TLM

    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeCheckCreditLimit', '', false, false)]
    local procedure T36_OnBeforeCheckCreditLimit(var SalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    begin
        //MC Le 19-05-2010 => Impossible de passer le devis en commande … cause d'un runmodal Appel TLM
        IF SalesHeader.getHideValidationDialog() THEN
            IsHandled := true;
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnRecreateReservEntryReqLineOnAfterLoop', '', false, false)]
    local procedure T36_OnRecreateReservEntryReqLineOnAfterLoop(var SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line")
    begin
        // MCO Le 09-02-2017 => Régie : On garde les commentaires
        SalesHeader.RecreateCommentLine(SalesLine, 0, TRUE);
        // FIN MCO Le 09-02-2017 => Régie
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeSalesLineInsert', '', false, false)]
    local procedure T36_OnBeforeSalesLineInsert(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var TempSalesLine: Record "Sales Line" temporary)

    begin
        // AD 17-12-2010 => EDI -> Conservation des Infos !
        SalesLine."EDI Document Line" := TempSalesLine."EDI Document Line";
        SalesLine."EDI Line No." := TempSalesLine."EDI Line No.";
        SalesLine."EDI Document No." := TempSalesLine."EDI Document No.";
        SalesLine."EDI Quantity" := TempSalesLine."EDI Quantity";
        SalesLine."EDI Net Price" := TempSalesLine."EDI Net Price";
        // AD 17-12-2010
        // MC le 10-11-2011 => SYMTA -> Conservation des Infos !
        SalesLine."Recherche référence" := TempSalesLine."Recherche référence";
        SalesLine."Référence saisie" := TempSalesLine."Référence saisie";
        // FIN MC Le 10-11-2011
        // AD Le 05-02-2015 => SYMTA -> Conservation des Infos !
        SalesLine."Originally Ordered No." := TempSalesLine."Originally Ordered No.";
        SalesLine."Originally Ordered Var. Code" := TempSalesLine."Originally Ordered Var. Code";
        SalesLine."Qty. to Ship" := TempSalesLine."Qty. to Ship";
        SalesLine."Qty. to Ship (Base)" := TempSalesLine."Qty. to Ship (Base)";
        // FIN AD Le 05-02-2015
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnAfterCreateSalesLine', '', false, false)]
    local procedure T36_OnAfterCreateSalesLine(var SalesLine: Record "Sales Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // MC Le 15-06-2012 => Les lignes de commentaires ne suivent pas car il n'y a pas de Insert(TRUE) donc appel fonction ESKA
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        SalesLine.SetNotCreateComment(TRUE);
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        SalesLine."Insert.Eskape"();
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        SalesLine.SetNotCreateComment(FALSE);
        // MCO Le 09-02-2017 => Régie : Les commentaires doivent suivrent de la ligne
        // FIN MC LE 15-06-2012

        // MCO Le 31-03-2015 => Régie
        SalesLine.VALIDATE("Line type", TempSalesLine."Line type");
        SalesLine.MODIFY();
        // FIN MCO Le 31-03-2015

        // AD 17-12-2010 => DEEE -> Insertion des charges associées
        IF TransferItemCharge.SalesCheckIfAnyLink(SalesLine) THEN BEGIN
            TransferItemCharge.InsertSalesLink(SalesLine);
            IF TransferItemCharge.MakeUpdate() THEN BEGIN
                TransferItemCharge.UpdateQtySalesLink(SalesLine);
                SalesLine."Line No." := SalesLine."Line No." + 10000;
            END;
        END;
        // FIN AD 17-12-2010
    end;

    [EventSubscriber(ObjectType::table, database::"Sales Header", 'OnBeforeInitFromSalesHeader', '', false, false)]
    local procedure T36_OnBeforeInitFromSalesHeader(var SalesHeader: Record "Sales Header"; SourceSalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    begin
        IsHandled := true;
        SalesHeader."Document Date" := SourceSalesHeader."Document Date";
        SalesHeader."Shipment Date" := SourceSalesHeader."Shipment Date";
        SalesHeader."Shortcut Dimension 1 Code" := SourceSalesHeader."Shortcut Dimension 1 Code";
        SalesHeader."Shortcut Dimension 2 Code" := SourceSalesHeader."Shortcut Dimension 2 Code";
        SalesHeader."Dimension Set ID" := SourceSalesHeader."Dimension Set ID";
        SalesHeader."Location Code" := SourceSalesHeader."Location Code";
        // AD Le 30-11-2009 => Initialisation des dates … la date du jour
        SalesHeader."Order Date" := WORKDATE();
        SalesHeader."Posting Date" := WORKDATE();
        SalesHeader."Document Date" := WORKDATE();
        // MCO Le 31-03-2015 => Régie
        IF SalesHeader."Requested Delivery Date" < WORKDATE() THEN BEGIN
            SalesHeader.SetHideQuoteToOrder(TRUE);
            SalesHeader.VALIDATE("Requested Delivery Date", WORKDATE());
            SalesHeader.MODIFY();
        END;
        // MCO Le 31-03-2015 => Régie
        // FIN AD Le 30-11-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnValidateSellToCustomerNoOnBeforeValidateLocationCode', '', false, false)]
    local procedure T36_OnValidateSellToCustomerNoOnBeforeValidateLocationCode(var SalesHeader: Record "Sales Header"; var Cust: Record Customer; var IsHandled: Boolean)
    begin
        // AD Le 22-06-2009 => VALIDATE ESKAPE
        SalesHeader.ValidateSellCustNo();

        // AD Le 07-09-2009 => FARGROUP -> Client Facturé
        IF Cust."Invoice Customer No." <> '' THEN
            SalesHeader."Invoice Customer No." := Cust."Invoice Customer No."
        ELSE BEGIN
            IF SalesHeader."Invoice Customer No." = SalesHeader."Sell-to Customer No." THEN
                SalesHeader.SetSkipBillToContact(TRUE);
            SalesHeader."Invoice Customer No." := SalesHeader."Sell-to Customer No.";
            SalesHeader.SetSkipBillToContact(FALSE);
        END;
        // FIN AD Le 07-09-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Header", 'OnBeforeSynchronizeForReservations', '', false, false)]
    local procedure T36_OnBeforeSynchronizeForReservations(var SalesHeader: Record "Sales Header"; var NewSalesLine: Record "Sales Line"; OldSalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        // MCO Le 09-02-2017 => R‚gie -> Cr‚ation des commentaires
        SalesHeader.RecreateCommentLine(OldSalesLine, NewSalesLine."Line No.", FALSE);
        // FIN MCO Le 09-02-2017 => R‚gie
    end;

    [EventSubscriber(ObjectType::Table, Database::"VAT Registration No. Format", 'OnBeforeTest', '', false, false)]
    local procedure T381_OnBeforeTest(VATRegNo: Text[20]; CountryCode: Code[10]; Number: Code[20]; TableID: Option; Check: Boolean; var IsHandled: Boolean)
    var
        VatRegistrationNoFormat: Record "VAT Registration No. Format";
        CompanyInfo: Record "Company Information";
        Finish: Boolean;
        TextString: Text;
        Text000Msg: Label 'The entered VAT Registration number is not in agreement with the format specified for Country/Region Code %1.\', Comment = '%1 = Country/Region code ';
        Text001Msg: Label 'The following formats are acceptable: %1', Comment = '%1 =format list';
    begin
        IsHandled := true;

        if CountryCode = '' then begin
            if not CompanyInfo.Get() then
                exit;
            VatRegistrationNoFormat.SetRange("Country/Region Code", CompanyInfo."Country/Region Code");
        end else
            VatRegistrationNoFormat.SetRange("Country/Region Code", CountryCode);
        VatRegistrationNoFormat.SetFilter(Format, '<> %1', '');
        if VatRegistrationNoFormat.FindSet() then
            repeat
                AppendString(TextString, Finish, VatRegistrationNoFormat.Format);
                Check := VatRegistrationNoFormat.Compare(VATRegNo, VatRegistrationNoFormat.Format);
            until Check or (VatRegistrationNoFormat.Next() = 0);

        if not Check then
            // AD Le 02-11-2009 => Demande de Catalina -> Pas bloquant
            //Error('%1%2', StrSubstNo(Text000, VatRegistrationNoFormat."Country/Region Code"), StrSubstNo(Text001, TextString));
            Message('%1%2', StrSubstNo(Text000Msg, VatRegistrationNoFormat."Country/Region Code"), StrSubstNo(Text001MSg, TextString));
        // FIN AD Le 02-11-2009

        case TableID of
            DATABASE::Customer:
                CheckCust(VATRegNo, Number);
            DATABASE::Vendor:
                CheckVendor(VATRegNo, Number);
            DATABASE::Contact:
                CheckContact(VATRegNo, Number);
        end;
    end;

    local procedure AppendString(var String: Text; var Finish: Boolean; AppendText: Text)
    begin
        case true of
            Finish:
                exit;
            String = '':
                String := AppendText;
            StrLen(String) + StrLen(AppendText) + 5 <= 250:
                String += ', ' + AppendText;
            else begin
                String += '...';
                Finish := true;
            end;
        end;
    end;

    local procedure CheckCust(VATRegNo: Text[20]; Number: Code[20])
    var
        Cust: Record Customer;
        Check: Boolean;
        Finish: Boolean;
        TextString: Text;
        CustomerIdentification: Text[100];
        Text002Msg: Label 'This VAT registration number has already been entered for the following customers:\ %1', Comment = '%1 = Client identifi';
    begin
        Check := true;
        TextString := '';
        Cust.SetCurrentKey("VAT Registration No.");
        Cust.SetRange("VAT Registration No.", VATRegNo);
        Cust.SetFilter("No.", '<>%1', Number);
        if Cust.FindSet() then begin
            Check := false;
            Finish := false;
            repeat
                CustomerIdentification := Cust."No.";
                AppendString(TextString, Finish, CustomerIdentification);
            until (Cust.Next() = 0) or Finish;
        end;
        if not Check then
            Message(StrSubstNo(Text002Msg, TextString));
    end;

    local procedure CheckVendor(VATRegNo: Text[20]; Number: Code[20])
    var
        Vend: Record Vendor;
        Check: Boolean;
        Finish: Boolean;
        TextString: Text;
        Text003Lbl: Label 'This VAT registration number has already been entered for the following vendors:\ %1', Comment = '%1 = Liste Fournisseurs';
    begin
        Check := true;
        TextString := '';
        Vend.SetCurrentKey("VAT Registration No.");
        Vend.SetRange("VAT Registration No.", VATRegNo);
        Vend.SetFilter("No.", '<>%1', Number);
        if Vend.FindSet() then begin
            Check := false;
            Finish := false;
            repeat
                AppendString(TextString, Finish, Vend."No.");
            until (Vend.Next() = 0) or Finish;
        end;
        if not Check then
            Message(StrSubstNo(Text003Lbl, TextString));
    end;

    local procedure CheckContact(VATRegNo: Text[20]; Number: Code[20])
    var
        Cont: Record Contact;
        Check: Boolean;
        Finish: Boolean;
        TextString: Text;
        Text004Lbl: Label 'This VAT registration number has already been entered for the following contacts:\ %1', Comment = '%1 = Contacts';
    begin
        Check := true;
        TextString := '';
        Cont.SetCurrentKey("VAT Registration No.");
        Cont.SetRange("VAT Registration No.", VATRegNo);
        Cont.SetFilter("No.", '<>%1', Number);
        if Cont.FindSet() then begin
            Check := false;
            Finish := false;
            repeat
                AppendString(TextString, Finish, Cont."No.");
            until (Cont.Next() = 0) or Finish;
        end;
        if not Check then
            Message(StrSubstNo(Text004Lbl, TextString));
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T7317_OnBeforeInsertEvent(var Rec: Record "Warehouse Receipt Line"; RunTrigger: Boolean)
    var
        WhseReceiptHeader: Record "Warehouse Receipt Header";
    BEGIN
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 16-05-2011 => SYMTA -> Récéption Entrepot
        WhseReceiptHeader.GET(Rec."No.");
        Rec."N° Fusion Réception" := WhseReceiptHeader."N° Fusion Réception";
        // FIN AD Le 16-05-2011

    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnAfterDeleteEvent', '', false, false)]
    local procedure T7317_OnAfterDeleteEvent(var Rec: Record "Warehouse Receipt Line"; RunTrigger: Boolean)
    var
        WhseReceiptHeader: Record "Warehouse Receipt Header";
        FactureFour: Record "Import facture tempo. fourn.";
        FactureFourHeader: Record "Import facture tempo. fourn.";
        NoFact: Code[20];
        FactureFour2: Record "Import facture tempo. fourn.";
    BEGIN
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        IF Rec."N° sequence facture fourn." <> 0 THEN BEGIN
            // IF FactureFour.GET("N° sequence facture fourn.",FactureFour."Type ligne"::Ligne) THEN
            //  BEGIN

            // Vu que maintenant deux lignes de factures fourn peuvent ˆtre egale … une ligne de réception il faut chercher
            FactureFour.RESET();
            FactureFour.SETRANGE("No de reception", Rec."No.");
            FactureFour.SETRANGE("No ligne reception", Rec."Line No.");
            IF FactureFour.FINDSET() THEN
                REPEAT
                    FactureFour."date intégration" := 0D;
                    FactureFour."heure intégration" := 0T;
                    FactureFour."No de reception" := '';
                    FactureFour."No ligne reception" := 0;
                    FactureFour.MODIFY();
                UNTIL FactureFour.NEXT() = 0;



            FactureFourHeader.RESET();
            FactureFourHeader.SETRANGE("Code fournisseur", FactureFour."Code fournisseur");
            FactureFourHeader.SETRANGE("Type ligne", FactureFour."Type ligne"::Entete);
            FactureFourHeader.SETRANGE("No Facture", FactureFour."No Facture");
            IF FactureFourHeader.FINDFIRST() THEN BEGIN
                FactureFourHeader."date intégration" := 0D;
                FactureFourHeader."heure intégration" := 0T;
                FactureFourHeader."No de reception" := '';
                FactureFourHeader."No ligne reception" := 0;
                FactureFourHeader.MODIFY();
            END;
        END;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnBeforeValidateEvent', "Qty. to Receive", false, false)]
    local procedure T7317_OnBeforeValidateEvent(var Rec: Record "Warehouse Receipt Line"; var xRec: Record "Warehouse Receipt Line"; CurrFieldNo: Integer)
    var
        rec_PurchLine: Record "Purchase Line";
        rec_WhseRcptLine: Record "Warehouse Receipt Line";
        _QteSurReception: Decimal;
        Text60000Qst: Label 'Etes-vous sûr de vouloir réceptionner plus que la quantité commandée?';
        AdditionalReceiptManagement: Codeunit "Warehouse Receipt Management";

    BEGIN
        // ESKVN1 => Réceptionné plus que la commande initiale.
        IF ((rec."Source Document" = rec."Source Document"::"Purchase Order") OR (rec."Source Document" = rec."Source Document"::"Inbound Transfer"))
        AND ((rec."Qty. to Receive" + rec."Qty. Received") > rec.Quantity) THEN BEGIN
            IF CONFIRM(Text60000Qst) THEN BEGIN
                AdditionalReceiptManagement.AdditionalReceipt(rec);
                rec."Qty. to Receive" := (rec.Quantity - rec."Qty. Received");
            END
            ELSE
                rec.VALIDATE("Qty. to Receive", xRec."Qty. to Receive");
        END;
        //FIN ESKVN1
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnBeforeValidateQtyToReceive', '', false, false)]
    local procedure T7317_OnBeforeValidateQtyToReceive(var WarehouseReceiptLine: Record "Warehouse Receipt Line"; CurrentFieldNo: Integer; var IsHandled: Boolean)
    var
        Text002Err: Label 'Vous ne pouvez pas traiter plus que les %1 unités restantes, article %2.', Comment = '%1 = Qty Restantes ; %2 = No Article';
    begin
        IsHandled := true;
        IF WarehouseReceiptLine."Qty. to Receive" > WarehouseReceiptLine."Qty. Outstanding" THEN
            ERROR(Text002Err, WarehouseReceiptLine."Qty. Outstanding", WarehouseReceiptLine."Item No.");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Posted Whse. Receipt Line", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T7319_OnBeforeInsertEvent(var Rec: Record "Posted Whse. Receipt Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [50060] et [50061]
        Rec.SetPartner();
    end;

    [EventSubscriber(ObjectType::Page, Page::"Bin Content Creation Worksheet", 'OnBeforeActionEvent', 'CreateBinContent', false, false)]
    local procedure P7371_OnBeforeActionEvent(var Rec: Record "Bin Creation Worksheet Line")
    var
        BinCreateLine: Record "Bin Creation Worksheet Line";
        RecLLocation: Record Location;
    begin
        BinCreateLine.copy(Rec);
        IF BinCreateLine.FIND('-') THEN BEGIN
            RecLLocation.get(BinCreateLine."Location Code");
            REPEAT
                IF Not RecLLocation."Directed Put-away and Pick" THEN
                    // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
                    IF NOT RecLLocation."Autoriser emplacement" THEN
                        // FIN MC Le 27-04-2011
                        BinCreateLine.TESTFIELD("Zone Code", '');
            UNTIL BinCreateLine.NEXT() = 0;
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Posted Whse. Receipt Line", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T7319_OnBeforeModifyEvent(var Rec: Record "Posted Whse. Receipt Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [50060] et [50061]
        Rec.SetPartner();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Posted Whse. Receipt Line", 'OnBeforeValidateEvent', 'Posted Source No.', false, false)]
    local procedure T7319_OnBeforeValidateEvent(var Rec: Record "Posted Whse. Receipt Line"; var xRec: Record "Posted Whse. Receipt Line"; CurrFieldNo: Integer)
    begin
        //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [50060] et [50061]
        Rec.SetPartner();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7335_OnAfterInsertEvent(var Rec: Record "Warehouse Receipt Line"; RunTrigger: Boolean)
    var
        location: Record location;
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;

        IF not Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Rec.TESTFIELD("Zone Code", '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Line", 'OnAfterModifyEvent', '', false, false)]
    local procedure T7335_OnAfterModifyEvent(var Rec: Record "Warehouse Receipt Line"; RunTrigger: Boolean)
    var
        location: Record location;
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        location.get(Rec."Location Code");
        IF not Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Rec.TESTFIELD("Zone Code", '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Header", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7316_OnAfterInsertEvent(var Rec: Record "Warehouse Receipt Header"; RunTrigger: Boolean)
    var
        PurchSetup: Record "Purchases & Payables Setup";
        Utilisateur: Record "Warehouse Employee";
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // AD Le 22-09-2009 => FARGROUP -> Cout -> Recherche des infos par defaut
        PurchSetup.GET();
        Rec.VALIDATE("% Facture HK", PurchSetup."% Facture HK");
        Rec.VALIDATE("% Assurance", PurchSetup."% Assurance");
        Rec.VALIDATE("% Coef Réception", PurchSetup."% Coef Réception");
        // FIN AD Le 22-09-2009

        // MC Le 15-09-2011 => SYMTA -> On ramŠne en automatique le magasin
        IF Rec."Location Code" = '' THEN BEGIN
            Utilisateur.RESET();
            Utilisateur.SETRANGE(Utilisateur."User ID", USERID);
            Utilisateur.SETRANGE(Default, TRUE);
            IF Utilisateur.FINDFIRST() THEN
                Rec.VALIDATE("Location Code", Utilisateur."Location Code");
        END;

        // AD Le 12-11-2015
        Rec.VALIDATE("Assigned User ID", USERID);
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Receipt Header", 'OnBeforeDeleteWhseRcptRelatedLines', '', false, false)]
    local procedure T7316_OnBeforeDeleteWhseRcptRelatedLines(var WhseRcptLine: Record "Warehouse Receipt Line"; var SkipConfirm: Boolean)
    var
        RecLWarehouseReceiptHeader: record "Warehouse Receipt Header";
    begin
        // MC Le 16-11-2011 => D‚safectation des lignes de factures
        RecLWarehouseReceiptHeader.DeleteInfoOnFacture();
        // FIN MC Le 16-12-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin Content", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T7302_OnBeforeInsertEvent(var Rec: Record "Bin Content"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        Rec.Fixed := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin Content", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7302_OnAfterInsertEvent(var Rec: Record "Bin Content"; RunTrigger: Boolean)
    var
        location: Record location;
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        location.get(Rec."Location Code");
        IF not Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Rec.TESTFIELD("Zone Code", '');
        // ----------------------------------------------------
        // Mise … jour de la date de création (PMA le 21-02-19)
        // ----------------------------------------------------
        Rec."Bin Content Creation Date" := TODAY;
        rec.Modify();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin Content", 'OnAfterModifyEvent', '', false, false)]
    local procedure T7302_OnAfterModifyEvent(var Rec: Record "Bin Content"; RunTrigger: Boolean)
    var
        location: Record location;
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        location.get(Rec."Location Code");
        IF not Location."Directed Put-away and Pick" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Rec.TESTFIELD("Zone Code", '');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Price", 'OnAfterInsertEvent', '', false, false)]
    local procedure T7012_OnAfterInsertEvent(var Rec: Record "Purchase Price"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if Rec.IsTemporary then
            exit;
        // AD Le 16-04-2012 =>
        Rec.TESTFIELD("Unit of Measure Code");
        // FIN AD Le 16-04-2012
    end;

    [EventSubscriber(ObjectType::Table, Database::"Purchase Price", 'OnAfterValidateEvent', 'Ending Date', false, false)]
    local procedure T7012_OnAfterValidateEvent(var Rec: Record "Purchase Price"; var xRec: Record "Purchase Price"; CurrFieldNo: Integer)
    begin
        // AD Le 06-02-2015 =>
        Rec.EnregPrixNetCoture()
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line Discount", 'OnBeforeInsertEvent', '', false, false)]
    local procedure T7004_OnBeforeInsertEvent(var Rec: Record "Sales Line Discount"; RunTrigger: Boolean)
    begin
        if rec.IsTemporary then
            exit;
        if Not RunTrigger then
            exit;
        rec.Type := rec.type::"All Item";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line Discount", 'OnAfterValidateEvent', 'Code', false, false)]
    local procedure T7004_OnAfterValidateEvent(var Rec: Record "Sales Line Discount"; var xRec: Record "Sales Line Discount"; CurrFieldNo: Integer)
    var
        Text001Err: Label '%1 must be blank.', Comment = '%1 = Ligne Remise Vente Code';
    begin
        // AD Le 15-09-2009 => FARGROUP -> Possibilité de mettre une remise pour tous les articles
        IF rec.Code <> '' THEN BEGIN
            CASE rec.Type OF
                rec.Type::"All Item":
                    ERROR(Text001Err, rec.FIELDCAPTION(Code));
            END;
        END;
        // FIN AD Le 15-09-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnInsertOnAfterCheckInventoryConflict', '', false, false)]
    local procedure T37_OnInsertOnAfterCheckInventoryConflict(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; var SalesLine2: Record "Sales Line")
    begin
        // MC Le 27-10-2011 =W Gestion du OnInsert ESKAPE.
        SalesLine."Insert.Eskape"();
        // FIN MC Le 27-10-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"sales Line", 'OnAfterModifyEvent', '', false, false)]
    local procedure T37_OnAfterModifyEvent(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        // MCO Le 08-02-2017 => Ticket 17567
        IF (rec.Type = rec.Type::Item) AND (rec."No." <> xRec."No.") THEN
            rec.SuiviCommentaireArticle()
        // FIN MCO Le 08-02-2017 => Ticket 17567
    end;

    [EventSubscriber(ObjectType::Table, Database::"sales Line", 'OnDeleteOnBeforeTestStatusOpen', '', false, false)]
    local procedure T37_OnDeleteOnBeforeTestStatusOpen(var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        ESK003Err: Label 'Vous ne pouvez pas suprimer la ligne car celle-ci a été reçue.';
        ESK002Err: Label 'Vous ne pouvez pas suprimer la ligne car celle-ci a été livrée.';
    begin

        // MCO Le 09-02-2017 => Régie : Ne pas supprimer si la ligne est livrée mˆme facturée
        IF salesline."Quantity Shipped" <> 0 THEN
            ERROR(ESK002Err);
        IF salesline."Return Qty. Received" <> 0 THEN
            ERROR(ESK003Err);
        // MCO Le 09-02-2017 => Régie : Ne pas supprimer si la ligne est livrée mˆme facturée

    end;


    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'onbeforeInsertEvent', '', false, false)]
    local procedure T37_OnBeforeInsertEvent(var Rec: Record "Sales Line"; RunTrigger: Boolean)
    begin
        if Not RunTrigger then
            exit;
        if rec.IsTemporary then
            exit;
        Rec.Type := Rec.Type::item;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnValidateNoOnCopyFromTempSalesLine', '', false, false)]
    local procedure T37_OnValidateNoOnCopyFromTempSalesLine(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; CurrentFieldNo: Integer; var TempSalesLine: Record "Sales Line" temporary)
    begin
        // MC Le 24-09-2012 => Pour ne pas recalculer l'abattement en copydocument
        SalesLine."Ne pas recalculer abattement" := TempSalesLine."Ne pas recalculer abattement";
        // AD Le 14-12-2009 => GDI -> Multireference -> Conservation du no saisi
        SalesLine."Recherche référence" := TempSalesLine."Recherche référence";
        // FIN AD Le 14-12-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnValidateNoOnAfterCalcShouldStopValidation', '', false, false)]
    local procedure T37_OnValidateNoOnAfterCalcShouldStopValidation(var SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; CallingFieldNo: Integer; var ShouldStopValidation: Boolean)
    begin
        if ShouldStopValidation then
            exit;
        //FBRUN le 26/04/2011 =>Gestion des centrales
        SalesLine."Gerer par groupement" := FALSE;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeCopyFromItem', '', false, false)]
    local procedure T37_OnBeforeCopyFromItem(var SalesLine: Record "Sales Line"; Item: Record Item; var IsHandled: Boolean)
    var
        SalesBlockedErr: Label 'You cannot sell this %1 because the Sales Blocked check box is selected on the %1 card.', Comment = '%1 - Table name';
        Functions: Codeunit Functions;
    begin
        IsHandled := true;
        // AD Le 26-08-2009 => Gestion avancé du bloque
        // Item.TESTFIELD(Blocked,FALSE);
        // FIN AD Le 26-08-2009
        Item.TESTFIELD("Inventory Posting Group");
        Item.TestField("Gen. Prod. Posting Group");
        if Item."Sales Blocked" then
            if SalesLine.IsCreditDocType() then
                Functions.SendBlockedItemNotification(SalesLine)
            else
                Error(SalesBlockedErr, Item.TableCaption());
        if Item.Type = Item.Type::Inventory then begin
            Item.TestField("Inventory Posting Group");
            SalesLine."Posting Group" := Item."Inventory Posting Group";
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnCopyFromItemOnAfterCheck', '', false, false)]
    local procedure T37_OnCopyFromItemOnAfterCheck(var SalesLine: Record "Sales Line"; Item: Record Item)
    begin
        // CFR le 16/12/2020 - Régie : Description 3 > Informations vente (50120)
        SalesLine."Description 3" := Item."Description 3";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterCopyFromItem', '', false, false)]
    local procedure T37_OnAfterCopyFromItem(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; Item: Record Item; CurrentFieldNo: Integer)
    var
        rec_groupement_marque: Record "Groupement/Marque";
        SalesHeader: Record "Sales Header";
    begin
        SalesHeader.get(SalesLine."Document Type", SalesLine."Document No.");
        //FBRUN le 26/04/2011 =>Gestion des centrales
        SalesLine."Gerer par groupement" := FALSE;
        IF rec_groupement_marque.GET(rec_groupement_marque.Type::Marque, SalesHeader."Centrale Active", Item."Manufacturer Code") THEN
            SalesLine."Gerer par groupement" := TRUE;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterAssignItemValues', '', false, false)]
    local procedure T37_OnAfterAssignItemValues(var SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; SalesHeader: Record "Sales Header"; Item: Record Item; CurrentFieldNo: Integer)
    begin
        // AD Le 03-06-2008 => Procedure Eskape
        SalesLine."ValidateNo.Eskape"();
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeCheckShipmentDateBeforeWorkDate', '', false, false)]
    local procedure T37_OnBeforeCheckShipmentDateBeforeWorkDate(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnValidateQuantityOnBeforeCheckAssocPurchOrder', '', false, false)]
    local procedure T37_OnValidateQuantityOnBeforeCheckAssocPurchOrder(var SalesLine: Record "Sales Line"; CurrentFieldNo: Integer)
    var
        QteProposée: Decimal;
        Item: Record Item;
        ESK50112Qst: Label 'La quantité saisie n''est pas un multiple du conditionnement (%1). Voulez vous ajuster à %2.', Comment = '%1 =Vente Multiple , %2 = Qty Proposée';
    begin
        // AD Le 21-10-2009 => Verif du multiple de vente avec ajustement si besoin (si le message doit bloquant mettre dans le validateQte)
        IF (SalesLine.Type = SalesLine.Type::Item) AND (NOT SalesLine.GetHideValidationDialog()) AND GUIALLOWED THEN BEGIN
            Item := SalesLine.GetItem();
            IF (Item."Sales multiple" <> 0) AND (SalesLine.Quantity <> 0) THEN
                IF (SalesLine.Quantity MOD Item."Sales multiple") <> 0 THEN BEGIN
                    QteProposée := ROUND(SalesLine.Quantity, Item."Sales multiple", '>');
                    IF CONFIRM(ESK50112Qst, FALSE, Item."Sales multiple", QteProposée) THEN
                        SalesLine.Quantity := QteProposée;
                END;
        END;
        // FIN AD Le 21-10-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnValidateQuantityOnBeforeSalesLineVerifyChange', '', false, false)]
    local procedure T37_OnValidateQuantityOnBeforeSalesLineVerifyChange(var SalesLine: Record "Sales Line"; StatusCheckSuspended: Boolean; var IsHandled: Boolean)
    begin
        // AD Le 28-08-2009 => Possibilité de solder la ligne d'origine
        //WhseValidateSourceLine.SalesLineVerifyChange(Rec,xRec); => Code D'origine
        IF SalesLine.GetShortCloseLine() THEN
            IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeUpdateUnitPrice', '', false, false)]
    local procedure T37_OnBeforeUpdateUnitPrice(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; CalledByFieldNo: Integer; CurrFieldNo: Integer; var Handled: Boolean)
    begin
        if CalledByFieldNo = SalesLine.FieldNo(Quantity) then
            IF SalesLine.GetShortCloseLine() THEN
                Handled := true;
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterValidateEvent', 'Quantity', false, false)]
    local procedure T37_OnAfterValidateEvent(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        // AD Le 16-10-2009 => Validate ESKAPE
        Rec."ValidateQte.Eskape"();
        // FIN AD Le 16-10-2009
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterValidateEvent', 'Unit Cost (LCY)', false, false)]
    local procedure T37_OnAfterValidateEvent_UnitCostLCY(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        // AD Le 16-10-2009 => Validate ESKAPE
        Rec."ValidateQte.Eskape"();
        // FIN AD Le 16-10-2009
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnValidateLineDiscountPercentOnBeforeUpdateAmounts', '', false, false)]
    local procedure T37_OnValidateLineDiscountPercentOnBeforeUpdateAmounts(var SalesLine: Record "Sales Line"; CurrFieldNo: Integer)
    var
        Currency: Record Currency;
    begin
        Currency.get(SalesLine."Currency Code");
        SalesLine."Line Discount Amount" := (ROUND(SalesLine."Unit Price", Currency."Unit-Amount Rounding Precision") * SalesLine.Quantity -
                                           ROUND(SalesLine."Unit Price" - (SalesLine."Unit Price" * SalesLine."Line Discount %") / 100, Currency."Amount Rounding Precision") * SalesLine.Quantity);
        // FIN AD Le 28-09-2007 
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterValidateEvent', 'Requested Delivery Date', false, false)]
    local procedure T37_OnAfterValidateEvent_RequestedDeliveryDate(var Rec: Record "Sales Line"; var xRec: Record "Sales Line"; CurrFieldNo: Integer)
    begin
        // AD Le 06-12-2012
        Rec.VALIDATE("Qty. to Ship", Rec.CalcQteLivrableSYMPA(5790));
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterSetSalesHeader', '', false, false)]
    local procedure T37_OnAfterSetSalesHeader(var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var Currency: Record Currency)
    begin
        IF SalesHeader."Currency Code" = '' THEN begin
            Currency."Unit-Amount Rounding Precision" := 0.01;
            Currency."Amount Rounding Precision" := 0.01;
        end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterGetSalesHeader', '', false, false)]
    local procedure T37_OnAfterGetSalesHeader(var SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line"; var Currency: Record Currency)
    begin
        if (SalesLine."Document Type" <> SalesHeader."Document Type") or (SalesLine."Document No." <> SalesHeader."No.") then
            if SalesHeader.Get(SalesLine."Document Type", SalesLine."Document No.") then
                if SalesHeader."Currency Code" = '' then begin
                    Currency."Unit-Amount Rounding Precision" := 0.01;
                    Currency."Amount Rounding Precision" := 0.01;
                end;
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnBeforeUpdateUnitPriceProcedure', '', false, false)]
    local procedure T37_OnBeforeUpdateUnitPriceProcedure(var SalesLine: Record "Sales Line"; CalledByFieldNo: Integer; var IsHandled: Boolean)
    begin
        // AD Le 28-08-2009 => Possibilité de solder la ligne d'origine (sans changer le prix)
        IF SalesLine.GetShortCloseLine() THEN
            EXIT;
        // AD Le 05-02-2015 => Si nous sommes sur un retour et que l'on rapproches d'une ligne, on garde les conditions initiales
        IF (SalesLine."Document Type" = SalesLine."Document Type"::"Return Order") AND (SalesLine."Appl.-from Item Entry" <> 0) THEN BEGIN
            SalesLine.VALIDATE("Unit Price");
            EXIT;
        END;
        // FIN AD Le 05-02-2015
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnAfterUpdateUnitPrice', '', false, false)]
    local procedure T37_OnAfterUpdateUnitPrice(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; CalledByFieldNo: Integer; CurrFieldNo: Integer)
    begin
        // AD Le 27-09-2016 => REGIE -> Message si changement du prix
        //IF STRPOS(USERID, 'ADAGOIS') <> 0 THEN
        IF (SalesLine."Net Unit Price" <> xSalesLine."Net Unit Price") AND
           (xSalesLine.Quantity <> 0) AND (CalledByFieldNo = 15) THEN
            MESSAGE('Attention ! Modification du prix ! Ancien Prix Net : %1', xSalesLine."Net Unit Price");
        // AD Le 27-09-2016
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnUpdateAmountsOnBeforeCheckLineAmount', '', false, false)]
    local procedure T37_OnUpdateAmountsOnBeforeCheckLineAmount(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        Currency: Record Currency;
        SalesHeader: Record "Sales Header";
        CurrExchRate: Record "Currency Exchange Rate";
    begin
        Currency.get(SalesLine."Currency Code");
        SalesHeader.get(SalesLine."Document Type", SalesLine."Document No.");
        // AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE
        IF SalesLine."Line Discount %" <> 0 THEN
            SalesLine."Net Unit Price" := ROUND(SalesLine."Unit Price" - (SalesLine."Unit Price" * SalesLine."Line Discount %") / 100, Currency."Amount Rounding Precision")
        ELSE
            SalesLine."Net Unit Price" := ROUND(SalesLine."Unit Price", Currency."Amount Rounding Precision");
        // FIN AD Le 15-09-2009

        // AD Le 21-10-2009 => Demande de Pierre Bertrand -> Pour les ligne de compte, le cout = le prix
        IF SalesLine.Type = SalesLine.Type::"G/L Account" THEN
            SalesLine."Unit Cost (LCY)" := SalesLine."Net Unit Price";


        IF SalesHeader."Currency Code" <> '' THEN BEGIN
            Currency.TESTFIELD("Unit-Amount Rounding Precision");
            SalesLine."Unit Cost" :=
              ROUND(
                CurrExchRate.ExchangeAmtLCYToFCY(
                  SalesLine.GetDate(), SalesHeader."Currency Code",
                  SalesLine."Unit Cost (LCY)", SalesHeader."Currency Factor"),
                Currency."Unit-Amount Rounding Precision")
        END ELSE
            SalesLine."Unit Cost" := SalesLine."Unit Cost (LCY)";
        // FIN AD Le 21-10-2009
    end;

    [EventSubscriber(ObjectType::Table, database::"Sales Line", 'OnBeforeGetDefaultBin', '', false, false)]
    local procedure T37_OnBeforeGetDefaultBin(var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        Location: Record Location;
        Functions: Codeunit Functions;
        WMSManagement: Codeunit "WMS Management";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        IsHandled := true;

        if (SalesLine.Type <> SalesLine.Type::Item) or SalesLine.IsNonInventoriableItem() then
            exit;
        SalesLine."Bin Code" := '';
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        SalesLine."Zone Code" := '';
        // FIN MC Le 27-04-2011
        if SalesLine."Drop Shipment" then
            exit;

        if (SalesLine."Location Code" <> '') and (SalesLine."No." <> '') then begin
            Location.get(SalesLine."Location Code");
            if Location."Bin Mandatory" and not Location."Directed Put-away and Pick" then begin
                if (SalesLine."Qty. to Assemble to Order" > 0) or SalesLine.IsAsmToOrderRequired() then
                    if SalesLine.GetATOBin(Location, SalesLine."Bin Code") then
                        exit;

                if not Functions.IsShipmentBinOverridesDefaultBin(Location) then begin
                    // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
                    // Ancien code.
                    //WMSManagement.GetDefaultBin("No.","Variant Code","Location Code","Bin Code");
                    // Nouveau code.
                    CduLFunctions.ESK_GetDefaultBin(SalesLine."No.", SalesLine."Variant Code", SalesLine."Location Code", SalesLine."Bin Code", SalesLine."Zone Code");
                    // FIN MC Le 27-04-2011 
                    Functions.HandleDedicatedBin(SalesLine, false);
                end;
            end;
        end;

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales Info-Pane Management", 'OnBeforeLookupItem', '', false, false)]
    local procedure CU7171_OnBeforeLookupItem(Item: Record Item; var IsHandled: Boolean; var SalesLine: Record "Sales Line")
    var
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        IsHandled := true;
        SalesLine.TestField(Type, SalesLine.Type::Item);
        SalesLine.TestField("No.");
        CduLFunctions.GetItem(Item, SalesLine);
        // AD Le 06-10-2009 => FARGROUP -> Pour le pas filtrer les dates
        Item.SETRANGE("Date Filter");
        // FIN AD Le 06-10-2009

        PAGE.RunModal(PAGE::"Item Card", Item);
    end;

    [EventSubscriber(ObjectType::Codeunit, codeunit::"Item Reference Management", 'OnSalesReferenceNoLookupOnAfterSetFilters', '', false, false)]
    local procedure CU5720_OnSalesReferenceNoLookupOnAfterSetFilters(SalesLine: Record "Sales Line"; SalesHeader: Record "Sales Header"; var ItemReference: Record "Item Reference")
    var
        ToDate: Date;
    begin
        ItemReference.Reset();
        ItemReference.SetCurrentKey("Reference Type", "Reference Type No.");
        ItemReference.SetFilter("Reference Type", '%1|%2', ItemReference."Reference Type"::Customer, ItemReference."Reference Type"::"Non Qualifié");
        ItemReference.SetFilter("Reference Type No.", '%1|%2', SalesHeader."Sell-to Customer No.", '');
        ToDate := SalesLine.GetDateForCalculations();
        if ToDate <> 0D then begin
            ItemReference.SetFilter("Starting Date", '<=%1', ToDate);
            ItemReference.SetFilter("Ending Date", '>=%1|%2', ToDate, 0D);
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeCheckApplFromItemLedgEntry', '', false, false)]
    local procedure T37_OnBeforeCheckApplFromItemLedgEntry(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; var ItemLedgerEntry: Record "Item Ledger Entry"; var IsHandled: Boolean)
    begin
        if (SalesLine."Appl.-from Item Entry" = 0) or (SalesLine."Shipment No." <> '') or (SalesLine."Return Receipt No." <> '') then
            IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterGetLineAmountToHandle', '', false, false)]
    local procedure T37_OnAfterGetLineAmountToHandle(SalesLine: Record "Sales Line"; QtyToHandle: Decimal; var LineAmount: Decimal; var LineDiscAmount: Decimal)
    var
        Currency: Record Currency;
    begin
        Currency.get(SalesLine."Currency Code");
        // MIG 2017 : reprise code de Nav 2015, car celui de 2017 bug ...
        LineDiscAmount := ROUND(SalesLine."Line Discount Amount" * QtyToHandle / SalesLine.Quantity, Currency."Amount Rounding Precision");
        //LineDiscAmount :=
        //  ROUND(
        //    LineAmount * "Line Discount %" / 100,Currency."Amount Rounding Precision");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeIsAsmToOrderRequired', '', false, false)]
    local procedure T37_OnBeforeIsAsmToOrderRequired(SalesLine: Record "Sales Line"; var IsHandled: Boolean; var Result: Boolean)
    begin
        IsHandled := true;
        Result := false;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeInitType', '', false, false)]
    local procedure T37_OnBeforeInitType(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; var IsHandled: Boolean; var SalesHeader: Record "Sales Header")
    begin
        IsHandled := true;
        SalesLine.Type := SalesLine.Type::Item; // AD Le 02-11-2015
        if SalesLine."Document No." <> '' then begin
            if not SalesHeader.Get(SalesLine."Document Type", SalesLine."Document No.") then
                exit;
            if (SalesHeader.Status = SalesHeader.Status::Released) and
               (xSalesLine.Type in [xSalesLine.Type::Item, xSalesLine.Type::"Fixed Asset"])
            then
                SalesLine.Type := SalesLine.Type::" "
            else
                // AD Le 21-10-2009 => Article par defaut pour simplifier la saisie
                // Type := xRec.Type;
                IF xSalesLine.Type IN [xSalesLine.Type::" ", xSalesLine.Type::Item] THEN
                    SalesLine.Type := xSalesLine.Type
                ELSE
                    SalesLine.Type := SalesLine.Type::Item;
            // FIN AD Le 21-10-2009
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnLookUpICPartnerReferenceTypeCaseElse', '', false, false)]
    local procedure T37_OnLookUpICPartnerReferenceTypeCaseElse(sender: Record "Sales Line")
    var
        ItemReference: Record "Item Reference";
    begin
        if sender."IC Partner Ref. Type" = sender."IC Partner Ref. Type"::"Cross Reference" then begin
            ItemReference.RESET();
            ItemReference.SETCURRENTKEY("Reference Type", "Reference Type No.");
            ItemReference.SETFILTER(
              "Reference Type", '%1|%2',
              ItemReference."Reference Type"::Customer,
              ItemReference."Reference Type"::"Non Qualifié");
            ItemReference.SETFILTER("Reference Type No.", '%1|%2', sender."Sell-to Customer No.", '');
            IF PAGE.RUNMODAL(PAGE::"Item Reference List", ItemReference) = ACTION::LookupOK THEN
                sender.VALIDATE("IC Partner Reference", ItemReference."Reference No.");
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnAfterUpdateAmounts', '', false, false)]
    local procedure T37_OnAfterUpdateAmounts(var SalesLine: Record "Sales Line"; var xSalesLine: Record "Sales Line"; CurrentFieldNo: Integer)
    begin
        if SalesLine.Type = SalesLine.Type::"Charge (Item)" then
            CduGSingleInstance.FctSetIfFunctionUpdateAmountsExecuted(true);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnBeforeUpdateItemChargeAssgnt', '', false, false)]
    local procedure T37_OnBeforeUpdateItemChargeAssgnt(var SalesLine: Record "Sales Line"; var InHandled: Boolean)
    begin
        if CduGSingleInstance.FctIfUpdateAmountsIsExecuted() then begin
            InHandled := SalesLine.Quantity = 0;
            CduGSingleInstance.FctSetIfFunctionUpdateAmountsExecuted(false);
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnBeforeValidateEvent', "Item No.", false, false)]
    local procedure T83_OnBeforeValidateEvent(var Rec: Record "Item Journal Line"; var xRec: Record "Item Journal Line"; CurrFieldNo: Integer)
    begin
        IF rec."Item No." <> xRec."Item No." THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            rec."Zone Code" := '';
        // FIN MC Le 27-04-2011
    end;


    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnValidateItemNoOnBeforeSetDescription', '', false, false)]
    local procedure T83_OnValidateItemNoOnBeforeSetDescription(var ItemJournalLine: Record "Item Journal Line"; Item: Record Item)
    begin
        //FBRUN REFERENCE ACTIVE
        ItemJournalLine."Recherche référence" := Item."No. 2";
        // AD TEMP
        ItemJournalLine.Ref := ItemJournalLine."Item No.";
        ItemJournalLine.Produit := Item."Product Type";
        ItemJournalLine.marque := Item."Manufacturer Code";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnSetUpNewLineOnBeforeSetDefaultPriceCalculationMethod', '', false, false)]
    local procedure T83_OnSetUpNewLineOnBeforeSetDefaultPriceCalculationMethod(ItemJnlBatch: Record "Item Journal Batch"; var DimMgt: Codeunit DimensionManagement; var ItemJournalLine: Record "Item Journal Line")
    begin
        // MC Le 04-06-2012 => Faire suivre l'information MiniLoad
        ItemJournalLine.MiniLoad := ItemJnlBatch.MiniLoad;
        // FIN MC Le 04-06-2012
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromSalesHeader', '', false, false)]
    local procedure T83_OnAfterCopyItemJnlLineFromSalesHeader(var ItemJnlLine: Record "Item Journal Line"; SalesHeader: Record "Sales Header")
    var
        LCust: Record Customer;
    begin
        // MCO Le 04-10-2017 => MIgration
        ItemJnlLine."Document Code" := SalesHeader."Source Document Type";
        LCust.GET(SalesHeader."Sell-to Customer No.");
        ItemJnlLine."Source Responsable" := LCust.Responsable;
        ItemJnlLine."Invoice Customer No." := SalesHeader."Invoice Customer No.";
        // MCO Le 08-02-2018 => Ticket 29107
        //IF SalesHeader."Sans mouvement de stock" THEN
        IF SalesHeader."Not stored" THEN
            ItemJnlLine."Sans mouvement de stock" := TRUE;
        // FIN MCO Le 04-10-2017
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromSalesLine', '', false, false)]
    local procedure T83_OnAfterCopyItemJnlLineFromSalesLine(var ItemJnlLine: Record "Item Journal Line"; SalesLine: Record "Sales Line")
    var
        LItem: Record Item;
    begin
        // MCO Le 04-10-2017 => MIgration
        LItem.GET(SalesLine."No.");
        ItemJnlLine."Manufacturer Code" := LItem."Manufacturer Code";
        ItemJnlLine."Item Category Code" := LItem."Item Category Code";
        ItemJnlLine."Item Category Code" := LItem."Item Category Code";
        ItemJnlLine."Campaign No." := SalesLine."Campaign No.";
        ItemJnlLine."Sales Line type" := SalesLine."Line type";
        ItemJnlLine."Opération ADV" := SalesLine."Opération ADV";
        ItemJnlLine."Commission %" := SalesLine."Commission %";
        ItemJnlLine."Exclure RFA" := SalesLine."Exclure RFA";
        ItemJnlLine."Requested Delivery Date" := SalesLine."Requested Delivery Date";
        // MC Le 31-01-2013 => Stats conso retour … la date de l'écriture d'origine
        ItemJnlLine."Return Appl.-from Item Entry" := SalesLine."Appl.-from Item Entry";
        // FIN MC Le 31-01-2013
        IF SalesLine."Sans mouvement de stock" THEN
            ItemJnlLine."Sans mouvement de stock" := TRUE;
        // FIN MCO Le 04-10-2017

        // ANI Le 27-11-2017 FE20171019
        ItemJnlLine."Source Document No." := SalesLine."Document No.";

        //fbrun le 10/03/20 FE20200728
        ItemJnlLine."Référence saisie" := SalesLine."Référence saisie";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromPurchHeader', '', false, false)]
    local procedure T83_OnAfterCopyItemJnlLineFromPurchHeader(var ItemJnlLine: Record "Item Journal Line"; PurchHeader: Record "Purchase Header")
    begin
        // AD Le 18-11-2009 => Suivi des champs
        ItemJnlLine."Currency Code" := PurchHeader."Currency Code";
        ItemJnlLine."Currency Factor" := PurchHeader."Currency Factor";
        // FIN AD Le 18-11-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterCopyItemJnlLineFromPurchLine', '', false, false)]
    local procedure T83_OnAfterCopyItemJnlLineFromPurchLine(var ItemJnlLine: Record "Item Journal Line"; PurchLine: Record "Purchase Line")
    begin
        // AD Le 24-03-2015 => No de BL fournisseur … la ligne de réception
        ItemJnlLine."External Document No. 2" := PurchLine."Vendor Shipment No.";
        // ItemJnlLine.TESTFIELD("External Document No. 2");  // A VOIR SI UTILE -> LE 26-03-=> Non car plante la facturation
        // FIN AD Le 24-03-2015

        ItemJnlLine."Date Arrivage Marchandise" := PurchLine."Date Arrivage Marchandise"; // AD Le 30-01-2020 => REGIE

        // ANI Le 27-11-2017 FE20171019
        ItemJnlLine."Source Document No." := PurchLine."Document No.";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnBeforeValidateEvent', "Item No.", false, false)]
    local procedure T83_OnBeforeValidateEvent_ItemNo(var Rec: Record "Item Journal Line"; var xRec: Record "Item Journal Line"; CurrFieldNo: Integer)
    begin
        CduGSingleInstance.FctSaveItemJournalLineValues(Rec, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnBeforeValidateEvent', "Variant Code", false, false)]
    local procedure T83_OnBeforeValidateEvent_VariantCode(var Rec: Record "Item Journal Line"; var xRec: Record "Item Journal Line"; CurrFieldNo: Integer)
    begin
        CduGSingleInstance.FctSaveItemJournalLineValues(Rec, false);
        IF Rec."Variant Code" <> xRec."Variant Code" THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            Rec."Zone Code" := '';
        // FIN MC Le 27-04-2011
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnBeforeValidateEvent', "Location Code", false, false)]
    local procedure T83_OnBeforeValidateEvent_LocationCode(var Rec: Record "Item Journal Line"; var xRec: Record "Item Journal Line"; CurrFieldNo: Integer)
    begin
        IF rec."Item No." <> xRec."Item No." THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            Rec."Zone Code" := '';
        // FIN MC Le 27-04-2011
        CduGSingleInstance.FctSaveItemJournalLineValues(Rec, false);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnAfterIsDefaultBin', '', false, false)]
    local procedure T83_OnAfterIsDefaultBin(Location: Record Location; var Result: Boolean)
    var
        CduLFunctions: Codeunit "Codeunits Functions";
        RecLItemJournalLine: Record "Item Journal Line";
        IsNewLocationCode: Boolean;
    begin
        Result := false;
        CduGSingleInstance.FctGetItemJournalLineValues(RecLItemJournalLine, IsNewLocationCode);
        if IsNewLocationCode then
            CduLFunctions.ESK_GetDefaultBin(RecLItemJournalLine."Item No.", RecLItemJournalLine."Variant Code", RecLItemJournalLine."New Location Code", RecLItemJournalLine."New Bin Code", RecLItemJournalLine."New Zone Code")
        else
            CduLFunctions.ESK_GetDefaultBin(RecLItemJournalLine."Item No.", RecLItemJournalLine."Variant Code", RecLItemJournalLine."Location Code", RecLItemJournalLine."Bin Code", RecLItemJournalLine."Zone Code");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Journal Line", 'OnBeforeValidateEvent', "New Location Code", false, false)]
    local procedure T83_OnBeforeValidateEvent_NewLocationCode(var Rec: Record "Item Journal Line"; var xRec: Record "Item Journal Line"; CurrFieldNo: Integer)
    begin
        IF rec."Item No." <> xRec."Item No." THEN
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
            Rec."New Zone Code" := '';
        // FIN MC Le 27-04-2011
        CduGSingleInstance.FctSaveItemJournalLineValues(Rec, true);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Unit of Measure", 'OnBeforeValidateEvent', 'Cubage', false, false)]
    local procedure T5404_OnBeforeValidateEvent(var Rec: Record "Item Unit of Measure"; var xRec: Record "Item Unit of Measure"; CurrFieldNo: Integer)
    begin
        // AD Le 27-10-2009 => FARGROUP -> Mise a jour des infos de la fiche articles
        Rec.UpdateCaracteristiqueArticle();

        // FIN AD Le 27-10-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Unit of Measure", 'OnBeforeValidateEvent', 'Weight', false, false)]
    local procedure T5404_OnBeforeValidateEvent_Weight(var Rec: Record "Item Unit of Measure"; var xRec: Record "Item Unit of Measure"; CurrFieldNo: Integer)
    begin
        // AD Le 27-10-2009 => FARGROUP -> Mise a jour des infos de la fiche articles
        Rec.UpdateCaracteristiqueArticle();
        // FIN AD Le 27-10-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Unit of Measure", 'OnAfterCalcCubage', '', false, false)]
    local procedure T5404_OnAfterCalcCubage(var ItemUnitOfMeasure: Record "Item Unit of Measure")
    begin
        // AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3
        ItemUnitOfMeasure.Cubage := ItemUnitOfMeasure.Cubage / 1000000;
        // AD Le 27-10-2009 => FARGROUP -> Mise a jour des infos de la fiche articles
        ItemUnitOfMeasure.UpdateCaracteristiqueArticle();
        // FIN AD Le 27-10-2009
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin", 'OnBeforeOnInsert', '', false, false)]
    local procedure T7354_OnBeforeOnInsert(var Bin: Record Bin; var IsHandled: Boolean)
    var
        Location: Record Location;
    begin
        IsHandled := true;
        Bin.TestField("Location Code");
        Bin.TestField(Code);
        Location.Get(Bin."Location Code");
        if Location."Directed Put-away and Pick" then begin
            Bin.TestField("Zone Code");
            Bin.TestField("Bin Type Code");
        end else begin
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Bin.TESTFIELD("Zone Code", '');
            Bin.TestField("Bin Type Code", '');
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin", 'OnBeforeOnModify', '', false, false)]
    local procedure T7354_OnBeforeOnModify(var Bin: Record Bin; var IsHandled: Boolean; var xBin: Record Bin)
    var
        Location: Record Location;
    begin
        IsHandled := true;
        Location.Get(Bin."Location Code");
        if Location."Directed Put-away and Pick" then begin
            Bin.TestField("Zone Code");
            Bin.TestField("Bin Type Code");
        end else begin
            // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement.
            IF NOT Location."Autoriser emplacement" THEN
                // FIN MC Le 27-04-2011
                Bin.TESTFIELD("Zone Code", '');
            Bin.TestField("Bin Type Code", '');
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin", 'OnBeforeCheckEmptyBin', '', false, false)]
    local procedure T7354_OnBeforeCheckEmptyBin(var Bin: Record Bin; ErrorText: Text[250]; var IsHandled: Boolean)
    var
        WarehouseEntry: Record "Warehouse Entry";
        WarehouseJnl: Record "Warehouse Journal Line";
        WhseActivLine: Record "Warehouse Activity Line";
        WhseRcptLine: Record "Warehouse Receipt Line";
        WhseShptLine: Record "Warehouse Shipment Line";
        Text000Err: Label 'You cannot %1 the %2 with %3 = %4, %5 = %6, because the %2 contains items.';
        Text001Err: Label 'You cannot %1 the %2 with %3 = %4, %5 = %6, because one or more %7 exists for this %2.';
    begin

        WarehouseEntry.SetCurrentKey("Bin Code", "Location Code");
        WarehouseEntry.SetRange("Bin Code", Bin.Code);
        WarehouseEntry.SetRange("Location Code", Bin."Location Code");
        WarehouseEntry.CalcSums("Qty. (Base)");
        if WarehouseEntry."Qty. (Base)" <> 0 then
            Error(
              Text000Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"),
              Bin."Location Code", Bin.FieldCaption(Code), Bin.Code);

        WhseActivLine.SetRange("Bin Code", Bin.Code);
        WhseActivLine.SetRange("Location Code", Bin."Location Code");
        WhseActivLine.SetRange("Activity Type", WhseActivLine."Activity Type"::Movement);
        if not WhseActivLine.IsEmpty() then
            Error(
              Text001Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"), Bin."Location Code",
              Bin.FieldCaption(Code), Bin.Code, WhseActivLine.TableCaption());

        WarehouseJnl.SetRange("Location Code", Bin."Location Code");
        WarehouseJnl.SetRange("From Bin Code", Bin.Code);
        if not WarehouseJnl.IsEmpty() then
            Error(
              Text001Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"), Bin."Location Code",
              Bin.FieldCaption(Bin.Code), Bin.Code, WarehouseJnl.TableCaption());

        WarehouseJnl.Reset();
        WarehouseJnl.SetRange("To Bin Code", Bin.Code);
        WarehouseJnl.SetRange("Location Code", Bin."Location Code");
        if not WarehouseJnl.IsEmpty() then
            Error(
              Text001Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"), Bin."Location Code",
              Bin.FieldCaption(Code), Bin.Code, WarehouseJnl.TableCaption());

        WhseRcptLine.SetRange("Bin Code", Bin.Code);
        WhseRcptLine.SetRange("Location Code", Bin."Location Code");
        if not WhseRcptLine.IsEmpty() then
            Error(
              Text001Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"), Bin."Location Code",
              Bin.FieldCaption(Code), Bin.Code, WhseRcptLine.TableCaption());

        WhseShptLine.SetRange("Bin Code", Bin.Code);
        WhseShptLine.SetRange("Location Code", Bin."Location Code");
        if not WhseShptLine.IsEmpty() then
            Error(
              Text001Err,
              ErrorText, Bin.TableCaption(), Bin.FieldCaption("Location Code"), Bin."Location Code",
              Bin.FieldCaption(Code), Bin.Code, WhseShptLine.TableCaption());



        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Bin", 'OnAfterValidateEvent', 'Zone Code', false, false)]
    local procedure T7354_OnAfterValidateEvent_ZoneCode(var Rec: record Bin; xRec: record Bin)
    var
        Functions: Codeunit Functions;
    begin
        // ANI Le 18-05-2016 FE20160427

        // il doit y avoir changement de zone
        IF xRec."Zone Code" = Rec."Zone Code" THEN EXIT;
        Functions.ChangeZoneCode(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Shipment Line", 'OnBeforeCodeInsertInvLineFromShptLine', '', false, false)]
    local procedure T111_OnBeforeCodeInsertInvLineFromShptLine(var SalesShipmentLine: Record "Sales Shipment Line"; var SalesLine: Record "Sales Line"; var IsHandled: Boolean)
    var
        SalesInvHeader: Record "Sales Header";
        SalesOrderHeader: Record "Sales Header";
        SalesOrderLine: Record "Sales Line";
        TempSalesLine: Record "Sales Line" temporary;
        TransferOldExtLines: Codeunit "Transfer Old Ext. Text Lines";
        Currency: Record Currency;
        ItemTrackingMgt: Codeunit "Item Tracking Management";
        TranslationHelper: Codeunit "Translation Helper";
        PrepaymentMgt: Codeunit "Prepayment Mgt.";
        ExtTextLine: Boolean;
        NextLineNo: Integer;
        ShipHeader: Record "Sales Shipment Header";
        ESK001Txt: Label 'Client : %1';
        Text000: Label 'Shipment No. %1:';
        Text001: Label 'The program cannot find this Sales line.';
        ESK50000Txt: Label 'N° BL : %1';
        ESK50001Txt: Label 'Du : %1';
        ESK50002Txt: Label 'Cde : %1';
    begin
        IsHandled := true;

        SalesShipmentLine.SETRANGE("Document No.", SalesShipmentLine."Document No.");
        IF SalesShipmentLine."Attached to Line No." <> 0 THEN
            EXIT;
        TempSalesLine := SalesLine;
        if SalesLine.Find('+') then
            NextLineNo := SalesLine."Line No." + 10000
        else
            NextLineNo := 10000;

        if SalesInvHeader."No." <> TempSalesLine."Document No." then
            SalesInvHeader.Get(TempSalesLine."Document Type", TempSalesLine."Document No.");
        // AD Le 15-09-2011
        IF SalesLine."Sell-to Customer No." <> SalesShipmentLine."Sell-to Customer No." THEN BEGIN
            SalesLine.INIT();
            SalesLine."Line No." := NextLineNo;
            SalesLine."Document Type" := TempSalesLine."Document Type";
            SalesLine."Document No." := TempSalesLine."Document No.";
            SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015 => Migration
            SalesLine.Description := STRSUBSTNO(ESK001Txt, SalesShipmentLine."Sell-to Customer No.");
            SalesLine."Description 2" := SalesShipmentLine."Sell-to Customer No.";
            SalesLine."N° client Livré" := SalesShipmentLine."Sell-to Customer No.";
            SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureClientLivré;
            SalesLine.Insert();
            NextLineNo := NextLineNo + 10000;
        END;
        if SalesLine."Shipment No." <> SalesShipmentLine."Document No." then begin
            SalesLine.Init();
            SalesLine."Line No." := NextLineNo;
            SalesLine."Document Type" := TempSalesLine."Document Type";
            SalesLine."Document No." := TempSalesLine."Document No.";
            TranslationHelper.SetGlobalLanguageByCode(SalesInvHeader."Language Code");
            SalesLine.Description := StrSubstNo(Text000, SalesShipmentLine."Document No.");
            TranslationHelper.RestoreGlobalLanguage();
            // AD Le 11-03-2010 => Modification de l'édition de regrp de facture
            // SalesLine.Description := STRSUBSTNO(Text000,"Document No."); => CODE D'ORIGINE
            // BL N° : ... Du : ...
            SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015 => Migration
            SalesLine.Description := STRSUBSTNO(ESK50000Txt, SalesShipmentLine."Document No.");
            SalesLine."Description 2" := STRSUBSTNO(ESK50001Txt, SalesShipmentLine."Posting Date");
            SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureBl;

            //FBRUN AJOUT CLIENT LIVRE
            ShipHeader.GET(SalesShipmentLine."Document No.");
            SalesLine."N° client Livré" := ShipHeader."Sell-to Customer No.";
            //FIN FB

            SalesLine.Insert();
            SalesLine."Line No." := SalesLine."Line No." + 1;

            // Cde : ... / ...
            ShipHeader.GET(SalesShipmentLine."Document No.");
            SalesLine.Type := SalesLine.Type::" "; // AD Le 12-11-2015 => Migration
            SalesLine.Description := STRSUBSTNO(ESK50002Txt, ShipHeader."Order No.");
            IF ShipHeader."External Document No." <> '' THEN
                SalesLine."Description 2" := '/' + ShipHeader."External Document No."
            ELSE
                SalesLine."Description 2" := '';
            SalesLine."Internal Line Type" := SalesLine."Internal Line Type"::RuptureBl;
            // FIN AD Le 11-03-2010
            //FBRUN AJOUT CLIENT LIVRE
            SalesLine."N° client Livré" := ShipHeader."Sell-to Customer No.";
            //FIN FB
            SalesLine.Insert();
            NextLineNo := NextLineNo + 10000;
        end;

        TransferOldExtLines.ClearLineNumbers();

        repeat
            ExtTextLine := (SalesShipmentLine.Type = SalesShipmentLine.Type::" ") and (SalesShipmentLine."Attached to Line No." <> 0) and (SalesShipmentLine.Quantity = 0);
            if ExtTextLine then
                TransferOldExtLines.GetNewLineNumber(SalesShipmentLine."Attached to Line No.")
            else
                SalesShipmentLine."Attached to Line No." := 0;

            if (SalesShipmentLine.Type <> SalesShipmentLine.Type::" ") and SalesOrderLine.Get(SalesOrderLine."Document Type"::Order, SalesShipmentLine."Order No.", SalesShipmentLine."Order Line No.")
            then begin
                if (SalesOrderHeader."Document Type" <> SalesOrderLine."Document Type"::Order) or
                   (SalesOrderHeader."No." <> SalesOrderLine."Document No.")
                then
                    SalesOrderHeader.Get(SalesOrderLine."Document Type"::Order, SalesShipmentLine."Order No.");

                PrepaymentMgt.TestSalesOrderLineForGetShptLines(SalesOrderLine);
                Currency.get(SalesShipmentLine."Currency Code");

                if SalesInvHeader."Prices Including VAT" then begin
                    if not SalesOrderHeader."Prices Including VAT" then
                        SalesOrderLine."Unit Price" :=
                          Round(
                            SalesOrderLine."Unit Price" * (1 + SalesOrderLine."VAT %" / 100),
                            Currency."Unit-Amount Rounding Precision");
                end else
                    if SalesOrderHeader."Prices Including VAT" then
                        SalesOrderLine."Unit Price" :=
                          Round(
                            SalesOrderLine."Unit Price" / (1 + SalesOrderLine."VAT %" / 100),
                            Currency."Unit-Amount Rounding Precision");
            end else begin
                SalesOrderHeader.Init();
                if ExtTextLine or (SalesShipmentLine.Type = SalesShipmentLine.Type::" ") then begin
                    SalesOrderLine.Init();
                    SalesOrderLine."Line No." := SalesShipmentLine."Order Line No.";
                    SalesOrderLine.Description := SalesShipmentLine.Description;
                    SalesOrderLine."Description 2" := SalesShipmentLine."Description 2";
                end else
                    Error(Text001);
            end;

            SalesLine := SalesOrderLine;
            // AD Le 30-11-2009 => Masque certains message
            SalesLine.SetHideValidationDialog(TRUE);
            SalesLine."Line No." := NextLineNo;
            SalesLine."Document Type" := TempSalesLine."Document Type";
            SalesLine."Document No." := TempSalesLine."Document No.";
            SalesLine."Variant Code" := SalesShipmentLine."Variant Code";
            SalesLine."Location Code" := SalesShipmentLine."Location Code";
            SalesLine."Drop Shipment" := SalesShipmentLine."Drop Shipment";
            SalesLine."Shipment No." := SalesShipmentLine."Document No.";
            SalesLine."Shipment Line No." := SalesShipmentLine."Line No.";
            SalesShipmentLine.ClearSalesLineValues(SalesLine);
            // AD Le 30-11-2009 => TEST
            //IF NOT ExtTextLine AND (SalesLine.Type <> 0) THEN BEGIN
            IF (SalesLine.Type <> SalesLine.Type::" ") THEN BEGIN
                if SalesLine."Deferral Code" <> '' then
                    SalesLine.Validate("Deferral Code");
                if not IsHandled then
                    SalesLine.Validate(Quantity, SalesShipmentLine.Quantity - SalesShipmentLine."Quantity Invoiced");
                SalesShipmentLine.CalcBaseQuantities(SalesLine, SalesShipmentLine."Quantity (Base)" / SalesShipmentLine.Quantity);


                SalesLine.Validate("Unit Price", SalesOrderLine."Unit Price");
                SalesLine."Allow Line Disc." := SalesOrderLine."Allow Line Disc.";
                SalesLine."Allow Invoice Disc." := SalesOrderLine."Allow Invoice Disc.";
                SalesOrderLine."Line Discount Amount" :=
                  Round(
                    SalesOrderLine."Line Discount Amount" * SalesLine.Quantity / SalesOrderLine.Quantity,
                    Currency."Amount Rounding Precision");
                if SalesInvHeader."Prices Including VAT" then begin
                    if not SalesOrderHeader."Prices Including VAT" then
                        SalesOrderLine."Line Discount Amount" :=
                          Round(
                            SalesOrderLine."Line Discount Amount" *
                            (1 + SalesOrderLine."VAT %" / 100), Currency."Amount Rounding Precision");
                end else
                    if SalesOrderHeader."Prices Including VAT" then
                        SalesOrderLine."Line Discount Amount" :=
                          Round(
                            SalesOrderLine."Line Discount Amount" /
                            (1 + SalesOrderLine."VAT %" / 100), Currency."Amount Rounding Precision");
                SalesLine.Validate("Line Discount Amount", SalesOrderLine."Line Discount Amount");
                SalesLine."Line Discount %" := SalesOrderLine."Line Discount %";
                // ESK Le ??-??-????
                SalesLine."Discount1 %" := SalesOrderLine."Discount1 %";
                SalesLine."Discount2 %" := SalesOrderLine."Discount2 %";
                SalesLine."Net Unit Price" := SalesOrderLine."Net Unit Price";

                //FBRUN LE 22/09/09 GESTION DES REMISES SUPPLEMENTAIRE
                IF (SalesOrderLine."Internal Line Type" = SalesOrderLine."Internal Line Type"::"Discount Header") OR
                   (SalesOrderLine."Internal Line Type" = SalesOrderLine."Internal Line Type"::"Discount Line") THEN BEGIN
                    SalesLine.VALIDATE("Unit Price", 0);
                    SalesLine.VALIDATE("Line Discount %", 0);
                    SalesLine."Internal Line Type" := SalesOrderLine."Internal Line Type"::" ";
                END;
                //FIN FBRUN GESTION DES REMISES
                SalesLine.UpdatePrePaymentAmounts();

                if SalesOrderLine.Quantity = 0 then
                    SalesLine.Validate("Inv. Discount Amount", 0)
                else
                    SalesLine.Validate(
                      "Inv. Discount Amount",
                      Round(
                        SalesOrderLine."Inv. Discount Amount" * SalesLine.Quantity / SalesOrderLine.Quantity,
                        Currency."Amount Rounding Precision"));

            end;

            SalesLine."Attached to Line No." :=
              TransferOldExtLines.TransferExtendedText(
                SalesOrderLine."Line No.",
                NextLineNo,
                SalesShipmentLine."Attached to Line No.");
            SalesLine."Shortcut Dimension 1 Code" := SalesShipmentLine."Shortcut Dimension 1 Code";
            SalesLine."Shortcut Dimension 2 Code" := SalesShipmentLine."Shortcut Dimension 2 Code";
            SalesLine."Dimension Set ID" := SalesShipmentLine."Dimension Set ID";
            //FBRUN AJOUT CLIENT LIVRE
            ShipHeader.GET(SalesShipmentLine."Document No.");
            SalesLine."N° client Livré" := ShipHeader."Sell-to Customer No.";
            //FIN FB
            SalesLine.Insert();

            ItemTrackingMgt.CopyHandledItemTrkgToInvLine(SalesOrderLine, SalesLine);

            NextLineNo := NextLineNo + 10000;
            if SalesShipmentLine."Attached to Line No." = 0 then begin
                SalesShipmentLine.SetRange("Attached to Line No.", SalesShipmentLine."Line No.");
                SalesShipmentLine.SetRange(Type, SalesShipmentLine.Type::" ");
            end;
        until (SalesShipmentLine.Next() = 0) or (SalesShipmentLine."Attached to Line No." = 0);

        if SalesOrderHeader.Get(SalesOrderHeader."Document Type"::Order, SalesShipmentLine."Order No.") then begin
            SalesOrderHeader."Get Shipment Used" := true;
            SalesOrderHeader.Modify();
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Sales Shipment Line", 'OnAfterInitFromSalesLine', '', false, false)]
    local procedure T111_OnAfterInitFromSalesLine(SalesShptHeader: Record "Sales Shipment Header"; var SalesShptLine: Record "Sales Shipment Line"; SalesLine: Record "Sales Line")
    begin
        // AD Le 17-04-2012 => En standard le champ ne suit pas
        SalesShptLine."Originally Ordered No. Pour BL" := SalesLine."Originally Ordered No.";
        // FIN AD Le 17-04-2012
    end;

    [EventSubscriber(ObjectType::Table, Database::"Contact", 'OnBeforeOnDelete', '', false, false)]
    local procedure T5050_OnBeforeOnDelete(var Contact: Record Contact; xContact: Record Contact; var IsHandled: Boolean)
    var
        Task: Record "To-do";
        SegLine: Record "Segment Line";
        ContIndustGrp: Record "Contact Industry Group";
        ContactWebSource: Record "Contact Web Source";
        ContJobResp: Record "Contact Job Responsibility";
        ContMailingGrp: Record "Contact Mailing Group";
        ContProfileAnswer: Record "Contact Profile Answer";
        RMCommentLine: Record "Rlshp. Mgt. Comment Line";
        ContAltAddr: Record "Contact Alt. Address";
        ContAltAddrDateRange: Record "Contact Alt. Addr. Date Range";
        InteractLogEntry: Record "Interaction Log Entry";
        ContBusRel: Record "Contact Business Relation";
        Opp: Record Opportunity;
        Cont: Record Contact;
        DuplMgt: Codeunit DuplicateManagement;
        Text001: Label 'You cannot delete the %2 record of the %1 because the contact is assigned one or more unlogged segments.';
        Text002: Label 'You cannot delete the %2 record of the %1 because one or more opportunities are in not started or progress.';
        ESK000Qst: Label 'Be carefull, the contact is linked to customer. Are you sure ?';
        ESK001Err: Label 'Cancel by user.';
        CannotDeleteWithOpenTasksErr: Label 'You cannot delete contact %1 because there are one or more tasks open.', Comment = '%1 = Contact No.';

#if not CLEAN22
        IntrastatSetup: Record "Intrastat Setup";
#endif
        Customer: Record Customer;
        Vendor: Record Vendor;
        CampaignTargetGrMgt: Codeunit "Campaign Target Group Mgt";
        VATRegistrationLogMgt: Codeunit "VAT Registration Log Mgt.";
    begin
        IsHandled := true;
        Task.SetCurrentKey("Contact Company No.", "Contact No.", Closed, Date);
        Task.SetRange("Contact Company No.", Contact."Company No.");
        Task.SetRange("Contact No.", Contact."No.");
        Task.SetRange(Closed, false);
        if Task.Find('-') then
            Error(CannotDeleteWithOpenTasksErr, Contact."No.");

        SegLine.SetRange("Contact No.", Contact."No.");
        if not SegLine.IsEmpty() then
            Error(Text001, Contact.TableCaption(), Contact."No.");

        Opp.SetCurrentKey("Contact Company No.", "Contact No.");
        Opp.SetRange("Contact Company No.", Contact."Company No.");
        Opp.SetRange("Contact No.", Contact."No.");
        Opp.SetRange(Status, Opp.Status::"Not Started", Opp.Status::"In Progress");
        if Opp.Find('-') then
            Error(Text002, Contact.TableCaption(), Contact."No.");

        ContBusRel.SetRange("Contact No.", Contact."No.");
        ContBusRel.DeleteAll();

        if not Contact.Find() then;

        case Contact.Type of
            Contact.Type::Company:
                begin
                    // MCO Le 23-03-2016 => Demande confirmation avant suppression
                    IF NOT CONFIRM(ESK000Qst) THEN
                        ERROR(ESK001Err);
                    ContIndustGrp.SetRange("Contact No.", Contact."No.");
                    ContIndustGrp.DeleteAll();
                    ContactWebSource.SetRange("Contact No.", Contact."No.");
                    ContactWebSource.DeleteAll();
                    DuplMgt.RemoveContIndex(Contact, false);
                    InteractLogEntry.SetCurrentKey("Contact Company No.");
                    InteractLogEntry.SetRange("Contact Company No.", Contact."No.");
                    if InteractLogEntry.Find('-') then
                        repeat
                            CampaignTargetGrMgt.DeleteContfromTargetGr(InteractLogEntry);
                            Clear(InteractLogEntry."Contact Company No.");
                            Clear(InteractLogEntry."Contact No.");
                            InteractLogEntry.Modify();
                        until InteractLogEntry.Next() = 0;

                    Cont.Reset();
                    Cont.SetCurrentKey("Company No.");
                    Cont.SetRange("Company No.", Contact."No.");
                    Cont.SetRange(Type, Contact.Type::Person);
                    if Cont.Find('-') then
                        repeat
                            Cont.Delete(true);
                        until Cont.Next() = 0;

                    Opp.Reset();
                    Opp.SetCurrentKey("Contact Company No.", "Contact No.");
                    Opp.SetRange("Contact Company No.", Contact."Company No.");
                    Opp.SetRange("Contact No.", Contact."No.");
                    if Opp.Find('-') then
                        repeat
                            Clear(Opp."Contact No.");
                            Clear(Opp."Contact Company No.");
                            Opp.Modify();
                        until Opp.Next() = 0;

                    Task.Reset();
                    Task.SetCurrentKey("Contact Company No.");
                    Task.SetRange("Contact Company No.", Contact."Company No.");
                    if Task.Find('-') then
                        repeat
                            Clear(Task."Contact No.");
                            Clear(Task."Contact Company No.");
                            Task.Modify();
                        until Task.Next() = 0;
                end;
            Contact.Type::Person:
                begin
                    ContJobResp.SetRange("Contact No.", Contact."No.");
                    if not ContJobResp.IsEmpty() then
                        ContJobResp.DeleteAll();

                    InteractLogEntry.SetCurrentKey("Contact Company No.", "Contact No.");
                    InteractLogEntry.SetRange("Contact Company No.", Contact."Company No.");
                    InteractLogEntry.SetRange("Contact No.", Contact."No.");
                    if not InteractLogEntry.IsEmpty() then
                        InteractLogEntry.ModifyAll("Contact No.", Contact."Company No.");

                    Opp.Reset();
                    Opp.SetCurrentKey("Contact Company No.", "Contact No.");
                    Opp.SetRange("Contact Company No.", Contact."Company No.");
                    Opp.SetRange("Contact No.", Contact."No.");
                    if not Opp.IsEmpty() then
                        Opp.ModifyAll("Contact No.", Contact."Company No.");

                    Task.Reset();
                    Task.SetCurrentKey("Contact Company No.", "Contact No.");
                    Task.SetRange("Contact Company No.", Contact."Company No.");
                    Task.SetRange("Contact No.", Contact."No.");
                    if not Task.IsEmpty() then
                        Task.ModifyAll("Contact No.", Contact."Company No.");
                end;
        end;

        ContMailingGrp.SetRange("Contact No.", Contact."No.");
        if not ContMailingGrp.IsEmpty() then
            ContMailingGrp.DeleteAll();

        ContProfileAnswer.SetRange("Contact No.", Contact."No.");
        if not ContProfileAnswer.IsEmpty() then
            ContProfileAnswer.DeleteAll();

        RMCommentLine.SetRange("Table Name", RMCommentLine."Table Name"::Contact);
        RMCommentLine.SetRange("No.", Contact."No.");
        RMCommentLine.SetRange("Sub No.", 0);
        if not RMCommentLine.IsEmpty() then
            RMCommentLine.DeleteAll();

        ContAltAddr.SetRange("Contact No.", Contact."No.");
        if not ContAltAddr.IsEmpty() then
            ContAltAddr.DeleteAll();

        ContAltAddrDateRange.SetRange("Contact No.", Contact."No.");
        if not ContAltAddrDateRange.IsEmpty() then
            ContAltAddrDateRange.DeleteAll();

        VATRegistrationLogMgt.DeleteContactLog(Contact);
        IntrastatSetup.CheckDeleteIntrastatContact(IntrastatSetup."Intrastat Contact Type"::Contact, Contact."No.");
        Customer.SetRange("Primary Contact No.", Contact."No.");
        Customer.ModifyAll(Contact, '');
        Customer.ModifyAll("Primary Contact No.", '');

        Vendor.SetRange("Primary Contact No.", Contact."No.");
        Vendor.ModifyAll(Contact, '');
        Vendor.ModifyAll("Primary Contact No.", '');
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnBeforeValidateEvent', 'address', false, false)]
    local procedure T5050_OnBeforeValidateEvent(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        Rec.ChangeCoordinate();
        // FIN MCO Le 23-03-2013
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnBeforeValidateEvent', 'address 2', false, false)]
    local procedure T5050_OnBeforeValidateEvent_Address2(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        Rec.ChangeCoordinate();
        // FIN MCO Le 23-03-2013
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure T5050_OnAfterValidateEvent(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        Rec.ChangeCoordinate();
        // FIN MCO Le 23-03-2013
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure T5050_OnAfterValidateEvent_PostCode(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        Rec.ChangeCoordinate();
        // FIN MCO Le 23-03-2013
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnBeforeIsUpdateNeeded', '', false, false)]
    local procedure T5050_OnBeforeIsUpdateNeeded(var Contact: Record Contact; xContact: Record Contact; var UpdateNeeded: Boolean)
    begin
        UpdateNeeded := UpdateNeeded // MCO Le 23-03-2016 => Gestion du mobile
           OR (Contact."Mobile Phone No." <> xContact."Mobile Phone No.");
        // FIN MCO Le 23-03-2016 => Gestion du mobile
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        IF Contact.Type = Contact.Type::Company THEN
            IF (Contact.Name <> xContact.Name) OR
               (Contact.Address <> xContact.Address) OR
               (Contact."Address 2" <> xContact."Address 2") OR
               (Contact.City <> xContact.City) OR
               (Contact."Post Code" <> xContact."Post Code")
            THEN
                Contact.ChangeCoordinate();
        // FIN MCO Le 23-03-2016 => Recalcul des coordin‚es
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnCreateCustomerFromTemplateOnBeforeCustomerInsert', '', false, false)]
    local procedure T5050_OnCreateCustomerFromTemplateOnBeforeCustomerInsert(var Contact: Record Contact; CustomerTemplate: Code[20]; var Cust: Record Customer)
    begin

        // AD Le 19-10-2010 => Mˆme num‚ro
        Cust."No." := Contact."No.";
        // FIN AD Le 19-10-2010
    end;

    [EventSubscriber(ObjectType::Table, Database::Contact, 'OnTypeChangeOnAfterCheckInteractionLog', '', false, false)]
    local procedure T5050_OnTypeChangeOnAfterCheckInteractionLog(var Contact: Record Contact; xContact: Record Contact)
    begin
        // MCO Le 23-03-2016 => Recalcul des coordin‚es
        Contact.ChangeCoordinate();
        // FIN MCO Le 23-03-2013
    end;

    [EventSubscriber(ObjectType::Page, Page::"Check Credit Limit", 'OnBeforeCalcCreditLimitLCY', '', false, false)]
    local procedure P343_OnAfterCalcCreditLimitLCYProcedure(var Customer: Record Customer; var OutstandingRetOrdersLCY: Decimal; var RcdNotInvdRetOrdersLCY: Decimal; var NewOrderAmountLCY: Decimal; var OrderAmountTotalLCY: Decimal; var OrderAmountThisOrderLCY: Decimal; var ShippedRetRcdNotIndLCY: Decimal; var CustCreditAmountLCY: Decimal; var CustNo: Code[20]; var ExtensionAmountsDic: Dictionary of [Guid, Decimal]; var IsHandled: Boolean; DeltaAmount: Decimal)
    var
        Functions: Codeunit Functions;
    begin
        IsHandled := true;
        if Customer.GetFilter("Date Filter") = '' then
            Customer.SetFilter("Date Filter", '..%1', WorkDate());
        Customer.CalcFields("Balance (LCY)", "Shipped Not Invoiced (LCY)", "Serv Shipped Not Invoiced(LCY)");
        Functions.CalcReturnAmounts(Customer, OutstandingRetOrdersLCY, RcdNotInvdRetOrdersLCY);

        OrderAmountTotalLCY := Functions.CalcTotalOutstandingAmt(Customer) - OutstandingRetOrdersLCY + DeltaAmount;
        ShippedRetRcdNotIndLCY := Customer."Shipped Not Invoiced (LCY)" + Customer."Serv Shipped Not Invoiced(LCY)" - RcdNotInvdRetOrdersLCY;
        if Customer."No." = CustNo then
            OrderAmountThisOrderLCY := NewOrderAmountLCY
        else
            OrderAmountThisOrderLCY := 0;

        CustCreditAmountLCY :=
          Customer."Balance (LCY)" + Customer."Shipped Not Invoiced (LCY)" + Customer."Serv Shipped Not Invoiced(LCY)" - RcdNotInvdRetOrdersLCY +
          OrderAmountTotalLCY - Customer.GetInvoicedPrepmtAmountLCY();

        // AD Le 28-09-2016 => REGIE -> Demande de Philippe, oon ne prend que le livr‚ non factur‚
        CustCreditAmountLCY := Customer."Shipped Not Invoiced (LCY)" + OrderAmountTotalLCY + DeltaAmount;

    end;

    [EventSubscriber(ObjectType::Page, page::"Apply Bank Acc. Ledger Entries", 'OnBeforeGetSelectedRecords', '', false, false)]
    local procedure P381_OnBeforeGetSelectedRecords(var BankAccountLedgerEntry: Record "Bank Account Ledger Entry"; var TempBankAccountLedgerEntry: Record "Bank Account Ledger Entry" temporary; var IsHandled: Boolean)
    var
        BankAccountLedgerEntry2: Record "Bank Account Ledger Entry";
    begin
        IsHandled := true;

        BankAccountLedgerEntry2.COPY(BankAccountLedgerEntry);
        BankAccountLedgerEntry2.SETRANGE(Select, TRUE);
        // CurrPage.SetSelectionFilter(BankAccountLedgerEntry);
        if BankAccountLedgerEntry2.FindSet() then
            repeat
                TempBankAccountLedgerEntry := BankAccountLedgerEntry2;
                TempBankAccountLedgerEntry.Insert();
            until BankAccountLedgerEntry2.Next() = 0;
    end;

    [EventSubscriber(ObjectType::Page, Page::"Curr. Exch. Rate Service Card", 'OnBeforeGenerateXMLStructure', '', false, false)]
    local procedure P1651_OnBeforeGenerateXMLStructure(var CurrExchRateUpdateSetup: Record "Curr. Exch. Rate Update Setup"; var IsHandled: Boolean)
    var
        ServiceURL: Text;
        TempXMLBuffer: Record "XML Buffer" temporary;
        PagLDataExchSetupSubform: page "Data Exch. Setup Subform";
    begin
        IsHandled := true;
        TempXMLBuffer.Reset();
        TempXMLBuffer.DeleteAll();
        CurrExchRateUpdateSetup.GetWebServiceURL(ServiceURL);
        if CurrExchRateUpdateSetup.GetXMLStructure(TempXMLBuffer, ServiceURL) then begin
            TempXMLBuffer.Reset();
            PagLDataExchSetupSubform.SetXMLDefinition(TempXMLBuffer);
        end else
            CurrExchRateUpdateSetup.ShowHttpError();
    end;

    [EventSubscriber(ObjectType::Page, Page::"Posted Sales Document Lines", 'OnCopyLineToDocOnBeforeMessage', '', false, false)]
    local procedure P5850_OnCopyLineToDocOnBeforeMessage(ToSalesHeader: Record "Sales Header"; var IsHandled: Boolean)
    var
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        // AD Le 10-10-2011 => Application de l'abattement
        CduLFunctions.ApplicationAbattement(ToSalesHeader);
        // FIN AD Le 10-10-2011
    end;

    [EventSubscriber(ObjectType::Page, Page::"Sales Order Subform", 'OnAfterNoOnAfterValidate', '', false, false)]
    local procedure P46_OnAfterNoOnAfterValidate(var SalesLine: Record "Sales Line"; xSalesLine: Record "Sales Line"; sender: Page "Sales Order Subform")
    begin
        sender.SAVERECORD();
        // AD Le 10-11-2015
    end;

    [EventSubscriber(ObjectType::Page, Page::"Sales Order", 'OnBeforePostSalesOrder', '', false, false)]
    local procedure Pag42_OnBeforePostSalesOrder(var SalesHeader: Record "Sales Header"; PostingCodeunitID: Integer; Navigate: Enum "Navigate After Posting")
    begin
        // MCO Le 21-11-2018 => Pour les livraisons et factures directs on force la date du jour
        SalesHeader.VALIDATE("Posting Date", WORKDATE());
        SalesHeader.MODIFY();
        //FIN MCO
    end;





    //>>Mig Rep296
    [EventSubscriber(ObjectType::Report, Report::"Batch Post Sales Orders", 'OnBeforeSalesBatchPostMgt', '', false, false)]
    local procedure Rep296_OnBeforeSalesBatchPostMgt(var SalesHeader: Record "Sales Header"; var ShipReq: Boolean; var InvReq: Boolean; var SalesBatchPostMgt: Codeunit "Sales Batch Post Mgt."; var IsHandled: Boolean)
    var
        TypeFacturation: Code[10];
        ESK1001Err: Label 'Le type de facturation est obligatoire !';
    begin
        // AD Le 21-10-2009 => Le type de facturation est obligatoire
        IF TypeFacturation = '' THEN
            ERROR(ESK1001Err);

        SalesHeader.SETRANGE("Invoice Type", TypeFacturation);

        // AD Le 27-10-2009 => On prend les commandes sans regroupement
        SalesHeader.SETRANGE("Combine Shipments", FALSE);
        // FIN AD Le 27-10-2009
    end;
    //<<Mig Rep296
    //>>Mig Rep5195
    [EventSubscriber(ObjectType::Report, Report::"Create Conts. from Customers", 'OnBeforeSetSkipDefaults', '', false, false)]
    local procedure Rep5195_OnBeforeSetSkipDefaults(var Customer: Record Customer; var Contact: Record Contact)
    begin
        // AD Le 20-02-2013
        Contact."No." := Customer."No.";
        // FIN AD Le 20-02-2013
    end;
    //<<Mig Rep5195
    //>>Mig Rep1497
    [EventSubscriber(ObjectType::Report, Report::"Trans. Bank Rec. to Gen. Jnl.", 'OnBeforeGenJnlLineInsert', '', false, false)]
    local procedure Rep1497_OnBeforeGenJnlLineInsert(var GenJournalLine: Record "Gen. Journal Line"; BankAccReconciliationLine: Record "Bank Acc. Reconciliation Line")
    var
        GenJnlBatch: Record "Gen. Journal Batch";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        BankAccRecon: Record "Bank Acc. Reconciliation";
        TMP_DocNo: Code[20];

    begin
        if (GenJnlBatch.get(GenJournalLine."Journal Template Name", GenJournalLine."Journal Batch Name")) and
        (BankAccRecon.get(BankAccReconciliationLine."Statement Type", BankAccReconciliationLine."Bank Account No.", BankAccReconciliationLine."Statement No."))
         then Begin
            if GenJournalLine."Document No." <> BankAccReconciliationLine."Document No." then begin
                //Nouveau code

                // IF "Document No." <> '' THEN
                //     GenJnlLine."Document No." := "Document No."
                // ELSE
                IF TMP_DocNo <> '' THEN
                    GenJournalLine."Document No." := INCSTR(TMP_DocNo)
                ELSE
                    IF GenJnlBatch."No. Series" <> '' THEN
                        GenJournalLine."Document No." := NoSeriesMgt.GetNextNo(
                           GenJnlBatch."No. Series", BankAccReconciliationLine."Transaction Date", FALSE);
                //Fin FBrun
            end;
            TMP_DocNo := GenJournalLine."Document No.";
            GenJournalLine."Posting No. Series" := GenJnlBatch."Posting No. Series";

            if (GenJnlBatch."Bal. Account No." <> '')
                    // MCO Le 16-01-2018 => Comportement diff‚rent de 2015 et ne convient pas aux utilisateurs
                    /* 
                 and
         ((GenJnlBatch."Bal. Account Type" <> GenJnlBatch."Bal. Account Type"::"Bank Account") or
          (GenJnlBatch."Bal. Account No." <> "Bank Acc. Reconciliation"."Bank Account No."))*/
                    then begin
                GenJournalLine.Validate("Bal. Account Type", GenJnlBatch."Bal. Account Type");
                GenJournalLine.Validate("Bal. Account No.", GenJnlBatch."Bal. Account No.");
                GenJournalLine.Validate(Amount, -BankAccReconciliationLine.Difference);
            end else begin
                GenJournalLine.Validate("Bal. Account Type", GenJournalLine."Account Type"::"Bank Account");
                GenJournalLine.Validate("Bal. Account No.", BankAccRecon."Bank Account No.");
                GenJournalLine.Validate(Amount, BankAccReconciliationLine.Difference);
            end;
        end;
    end;
    //<<Mig Rep1497
    //>>Mig Rep5194
    [EventSubscriber(ObjectType::Report, Report::"Create Conts. from Vendors", 'OnBeforeSetSkipDefaults', '', false, false)]
    local procedure Rep5194_OnBeforeSetSkipDefaults(var Vendor: Record Vendor; var Contact: Record Contact)
    begin
        // AD Le 20-02-2013
        Contact."No." := Vendor."No.";
        // FIN AD Le 20-02-2013  
    end;
    //<<Mig Rep5194
    //<<Mig Rep7391
    [EventSubscriber(ObjectType::Report, Report::"Whse. Get Bin Content", 'OnInsertItemJournalLineOnBeforeInsert', '', false, false)]
    local procedure Rep7391_OnInsertItemJournalLineOnBeforeInsert(var ItemJournalLine: Record "Item Journal Line"; BinContent: Record "Bin Content")
    var
        CopierOrigineDest: Boolean;
    begin
        // AD Le 02-07-2014
        IF CopierOrigineDest THEN
            ItemJournalLine.VALIDATE("New Bin Code", BinContent."Bin Code");
        // FIN AD Le 02-07-2014
        ItemJournalLine."Posting No. Series" := '';
    end;
    //<<Mig Rep7391
    //>> Mig Rep 208
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep208_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Sales - Shipment" then
            NewReportId := Report::"Sales - Shipment SYM";
    end;
    //<< Mig Rep 208
    //>> Mig Rep 10864
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep10864_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Suggest Customer Payments" then
            NewReportId := Report::"Suggest Customer Payments SYM";
    end;
    //<< Mig Rep 10864
    //>> Mig Rep 6653
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep6653_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Combine Return Receipts" then
            NewReportId := Report::"Combine Return Receipts SYM";
    end;
    //<< Mig Rep 6653
    //>> Mig Rep 295
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep295_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Combine Shipments" then
            NewReportId := Report::"Combine Shipments SYM";
    end;
    //<< Mig Rep 295
    //>> Mig Rep 405
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep405_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::Order then
            NewReportId := Report::"Order SYM";
    end;
    //<< Mig Rep 405
    //>> Mig Rep 790
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep790_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Calculate Inventory" then
            NewReportId := Report::"Calculate Inventory SYM";
    end;
    //<< Mig Rep 790
    //>> Mig Rep 5753
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep5753_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Get Source Documents" then
            NewReportId := Report::"Get Source Documents SYM";
    end;
    //<< Mig Rep 5753
    //>> Mig Rep 10808
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep10808_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Vendor Detail Trial Balance FR" then
            NewReportId := Report::"Vend. Detail Trial Bal. FR SYM";
    end;
    //<< Mig Rep 10808
    //>> Mig Rep 10806
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep10806_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Customer Detail Trial Balance" then
            NewReportId := Report::"Customer Detail Trial Bal. SYM";
    end;
    //<< Mig Rep 10806
    //>> Mig Rep 7391
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep7391_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Whse. Get Bin Content" then
            NewReportId := Report::"Whse. Get Bin Content SYM";
    end;
    //<< Mig Rep 7391
    //>> Mig Rep 6661
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep6661_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Delete Invd Purch. Ret. Orders" then
            NewReportId := Report::"Delet Invd Prch. Ret. Ord. SYM";
    end;
    //<< Mig Rep 6661
    //>> Mig Rep 108
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep108_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Customer - Order Detail" then
            NewReportId := Report::"Customer - Order Detail SYM";
    end;
    //<< Mig Rep 108
    //>> Mig Rep 6646
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep6646_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Sales - Return Receipt" then
            NewReportId := Report::"Sales - Return Receipt SYM";
    end;
    //<< Mig Rep 6646
    //>> Mig Rep 6631
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep6631_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Return Order Confirmation" then
            NewReportId := Report::"Return Order Confirmation SYM";
    end;
    //<< Mig Rep 6631
    //>> Mig Rep 321
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep321_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Vendor - Balance to Date" then
            NewReportId := Report::"Vendor - Balance to Date SYM";
    end;
    //<< Mig Rep 321
    //>> Mig Rep 207
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep207_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Standard Sales - Credit Memo" then
            NewReportId := Report::"Sales - Credit Memo SYM";
    end;
    //<< Mig Rep 207
    //>> Mig Rep 204
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep204_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Standard Sales - Quote" then
            NewReportId := Report::"Sales - Quote SYM";
    end;
    //<< Mig Rep 204
    //>> Mig Rep 205
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep205_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Standard Sales - Order Conf." then
            NewReportId := Report::"Order Confirmation SYM";
    end;
    //<< Mig Rep 205
    //>> Mig Rep 206
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep206_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Standard Sales - Invoice" then
            NewReportId := Report::"Sales - Invoice SYM";
    end;
    //<< Mig Rep 206
    //>> Mig Rep 6641
    [EventSubscriber(ObjectType::Codeunit, Codeunit::ReportManagement, 'OnAfterSubstituteReport', '', false, false)]
    local procedure Rep6641_OnAfterSubstituteReport(ReportId: Integer; RunMode: Option Normal,ParametersOnly,Execute,Print,SaveAs,RunModal; RequestPageXml: Text; RecordRef: RecordRef; var NewReportId: Integer)
    begin
        if ReportId = Report::"Return Order" then
            NewReportId := Report::"Return Order SYM";
    end;
    //<< Mig Rep 6641
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Format Address", 'OnBeforeSetLineNos', '', false, false)]
    local procedure CU365_OnBeforeSetLineNos(Country: Record "Country/Region"; var NameLineNo: Integer; var Name2LineNo: Integer; var AddrLineNo: Integer; var Addr2LineNo: Integer; var ContLineNo: Integer; var PostCodeCityLineNo: Integer; var CountyLineNo: Integer; var CountryLineNo: Integer; var IsHandled: Boolean)
    begin
        case Country."Contact Address Format Spec" of
            Country."Contact Address Format Spec"::First:
                begin
                    NameLineNo := 2;
                    Name2LineNo := 3;
                    ContLineNo := 1;
                    AddrLineNo := 4;
                    Addr2LineNo := 5;
                    PostCodeCityLineNo := 6;
                    CountyLineNo := 7;
                    CountryLineNo := 8;
                end;
            Country."Contact Address Format Spec"::"After Company Name":
                begin
                    NameLineNo := 1;
                    Name2LineNo := 2;
                    ContLineNo := 3;
                    AddrLineNo := 4;
                    Addr2LineNo := 5;
                    PostCodeCityLineNo := 6;
                    CountyLineNo := 7;
                    CountryLineNo := 8;
                end;
            Country."Contact Address Format Spec"::Last:
                begin
                    NameLineNo := 1;
                    Name2LineNo := 2;
                    ContLineNo := 8;
                    AddrLineNo := 3;
                    Addr2LineNo := 4;
                    PostCodeCityLineNo := 5;
                    CountyLineNo := 6;
                    CountryLineNo := 7;
                end;
            // AD Le 21-10-2009 => FARGROUP -> Possibilit‚ de ne pas mettre le contact
            Country."Contact Address Format Spec"::"Ne pas afficher":
                BEGIN
                    NameLineNo := 1;
                    Name2LineNo := 2;
                    ContLineNo := 8;
                    AddrLineNo := 3;
                    Addr2LineNo := 4;
                    PostCodeCityLineNo := 5;
                    CountyLineNo := 6;
                    CountryLineNo := 7;
                    // Contact := '';
                END;
        // FIN AD Le 21-10-2009
        end;
        IsHandled := true;
    end;

    [EventSubscriber(ObjectType::Table, Database::"Warehouse Shipment Line", 'OnBeforeAutofillQtyToHandle', '', false, false)]
    local procedure T7321_OnBeforeAutofillQtyToHandle(var WarehouseShipmentLine: Record "Warehouse Shipment Line"; var HideValidationDialog: Boolean; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;
    //>>Mig Rep 393
    [EventSubscriber(ObjectType::Report, Report::"Suggest Vendor Payments", 'OnBeforeGetMessageToRecipient', '', false, false)]
    local procedure R393_OnBeforeGetMessageToRecipient(SummarizePerVend: Boolean; TempVendorPaymentBuffer: Record "Vendor Payment Buffer" temporary; var IsHandled: Boolean; var Message: Text[140])
    var
        CompanyInformation: Record "Company Information";
        MessageToRecipientMsg: Label 'Payment of %1 %2 ', Comment = '%1 document type, %2 Document No.';
    begin
        IsHandled := true;
        CompanyInformation.Get();
        if SummarizePerVend then begin
            Message := CompanyInformation.Name;
            exit;
        end;
        Message :=
          StrSubstNo(
            MessageToRecipientMsg,
            TempVendorPaymentBuffer."Vendor Ledg. Entry Doc. Type",
            TempVendorPaymentBuffer."Applies-to Ext. Doc. No.");
    end;
    //<<Mig Rep 393
    [EventSubscriber(ObjectType::page, page::"Bank Acc. Reconciliation", 'OnActionSuggestLinesOnBeforeSuggestBankAccReconLines', '', false, false)]
    local procedure Pa379_OnActionSuggestLinesOnBeforeSuggestBankAccReconLines(var BankAccReconciliation: Record "Bank Acc. Reconciliation"; var IsHandled: Boolean)
    var
        RepLSuggestBankAccReconLines: Report "Suggest Bank Acc. Recon. Lns";
    begin
        IsHandled := true;
        RepLSuggestBankAccReconLines.SetStmt(BankAccReconciliation);
        RepLSuggestBankAccReconLines.RunModal();
        Clear(RepLSuggestBankAccReconLines);
    end;
    //>>Mi
    [EventSubscriber(ObjectType::Table, Database::"Sales Line", 'OnCheckWarehouseOnBeforeShowDialog', '', false, false)]
    local procedure T37_OnCheckWarehouseOnBeforeShowDialog(var SalesLine: Record "Sales Line"; Location: Record Location; var ShowDialog: Option " ",Message,Error; var DialogText: Text[50])
    begin
        if ShowDialog <> ShowDialog::Error then
            ShowDialog := ShowDialog::" ";
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Header", 'OnBeforeDeleteEvent', '', false, false)]
    local procedure T10865_OnBeforeDeleteEvent(var Rec: Record "Payment Header"; RunTrigger: Boolean)
    begin
        if not RunTrigger then exit;
        if Rec.IsTemporary then exit;
        if Rec.PaymentLinesExist() then
            Error('Vous ne pouvez pas supprimer l''entête bordereau puisqu''il existe de lignes paiements');
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnBeforeModifyEvent', '', false, false)]
    local procedure T10866_OnBeforeModifyEvent(var Rec: Record "Payment Line"; var xRec: Record "Payment Line"; RunTrigger: Boolean)
    begin
        CduGSingleInstance.FctSetPosted(Rec.Posted);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Payment Line", 'OnAfterModifyEvent', '', false, false)]
    local procedure T10866_OnAfterModifyEvent(var Rec: Record "Payment Line"; var xRec: Record "Payment Line"; RunTrigger: Boolean)
    begin
        Rec.Posted := CduGSingleInstance.FctGetPosted();
        Rec.Modify();
    end;

    [EventSubscriber(ObjectType::Page, Page::"Apply G/L Entries", 'OnBeforePostApplication', '', false, false)]
    local procedure P10842_OnBeforePostApplication(var GLEntry: Record "G/L Entry"; var IsHandled: Boolean)
    var
        GLEntryApplication: Codeunit "G/L Entry Application Symta";
    begin
        IsHandled := true;
        GLEntryApplication.Validate(GLEntry);
    end;

    [EventSubscriber(ObjectType::Report, Report::"Calculate Inventory Value", 'OnInsertItemJnlLineOnCaseCalcBaseOnStandardCostAssemblyOrManufacturing', '', false, false)]
    local procedure Rep5899_OnInsertItemJnlLineOnCaseCalcBaseOnStandardCostAssemblyOrManufacturing(var ItemJournalLine: Record "Item Journal Line"; var TempNewStdCostItem: Record Item temporary)
    begin
        CduGSingleInstance.FctInsertSKU(ItemJournalLine);
    end;

    [EventSubscriber(ObjectType::Report, Report::"Calculate Inventory Value", 'OnAfterInsertItemJnlLine', '', false, false)]
    local procedure Rep5899_OnAfterInsertItemJnlLine(VariantCode2: Code[10]; Amount2: Decimal; AppliedAmount: Decimal; ApplyToEntry2: Integer; CalcBase: Enum "Inventory Value Calc. Base"; EntryType2: Enum "Item Ledger Entry Type"; ItemNo2: Code[20]; LocationCode2: Code[10]; Quantity2: Decimal; var ItemJournalLine: Record "Item Journal Line")
    begin
        CduGSingleInstance.FctDeleteSKU();
    end;

    var

        CduGSingleInstance: codeunit SingleInstance;
}