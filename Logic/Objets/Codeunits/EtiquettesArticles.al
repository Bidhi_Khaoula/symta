#pragma warning disable AA0215
codeunit 50000 "Etiquettes Articles"
#pragma warning restore AA0215
{

    trigger OnRun()
    begin
    end;

    var
#pragma warning disable AA0074
        Text50000: Label 'Voulez vous éditer les étiquettes produits client?';
#pragma warning restore AA0074

    procedure EditLabelShipment(ShipmentHeader: Record "Sales Shipment Header"; LineNo: Integer)
    var
        ItemCrossRef: Record "Item Reference";
        ShipmentLine: Record "Sales Shipment Line";
         item: Record Item;
        Cust: Record Customer;
        Edit: Boolean;
    begin
        // AD Le 29-05-2007 => Recherche si il y a des étiquettes articles à editer
        Edit := FALSE;

        ShipmentLine.RESET();
        ShipmentLine.SETRANGE("Document No.", ShipmentHeader."No.");
        ShipmentLine.SETRANGE(Type, ShipmentLine.Type::Item);
        ShipmentLine.SETFILTER(Quantity, '<>0');

        // AD Le 12-05-2010 -> Re edition par une ligne
        IF LineNo <> 0 THEN
            ShipmentLine.SETRANGE("Line No.", LineNo);

#pragma warning disable AA0233
        IF ShipmentLine.FINDFIRST() THEN
#pragma warning restore AA0233
            REPEAT
                ItemCrossRef.RESET();
                ItemCrossRef.SETRANGE("Item No.", ShipmentLine."No.");
                ItemCrossRef.SETRANGE("Variant Code", ShipmentLine."Variant Code");
                ItemCrossRef.SETRANGE("Reference Type", ItemCrossRef."Reference Type"::Customer);

                // AD Le 08-07-2010 => Ajout du champ lien client
                //ItemCrossRef.SETRANGE("Reference Type No.", ShipmentLine."Bill-to Customer No.");
                Cust.GET(ShipmentHeader."Sell-to Customer No.");
                ItemCrossRef.SETRANGE("Reference Type No.", Cust."APE Code");
                // FIN AD Le 08-07-2010

                ItemCrossRef.SETFILTER("Shipment Label Print", '<>%1', ItemCrossRef."Shipment Label Print"::Aucune);
                ItemCrossRef.SETFILTER("Code Format Etiquette", '<>%1', '');
                IF ItemCrossRef.FINDFIRST() THEN
                    Edit := TRUE;
            UNTIL ShipmentLine.NEXT() = 0;

        // Lance l'édition
        IF Edit = TRUE THEN
            IF CONFIRM(Text50000) THEN
                IF ShipmentLine.FindSet() THEN
                    REPEAT
                        ItemCrossRef.RESET();
                        ItemCrossRef.SETRANGE("Item No.", ShipmentLine."No.");
                        ItemCrossRef.SETRANGE("Variant Code", ShipmentLine."Variant Code");
                        ItemCrossRef.SETRANGE("Reference Type", ItemCrossRef."Reference Type"::Customer);
                        ItemCrossRef.SETRANGE("Reference Type No.", ShipmentLine."Bill-to Customer No.");
                        // AD Le 08-07-2010 => Ajout du champ lien client
                        //ItemCrossRef.SETRANGE("Reference Type No.", ShipmentLine."Bill-to Customer No.");
                        Cust.GET(ShipmentHeader."Sell-to Customer No.");
                        ItemCrossRef.SETRANGE("Reference Type No.", Cust."APE Code");
                        // FIN AD Le 08-07-2010

                        ItemCrossRef.SETFILTER("Shipment Label Print", '<>%1', ItemCrossRef."Shipment Label Print"::Aucune);
                        ItemCrossRef.SETFILTER("Code Format Etiquette", '<>%1', '');
                        IF ItemCrossRef.FINDFIRST() THEN BEGIN
                            CASE ItemCrossRef."Shipment Label Print" OF
                                ItemCrossRef."Shipment Label Print"::"1 par article":
                                    EditLabel(ItemCrossRef, ShipmentLine.Quantity, 1);
                                ItemCrossRef."Shipment Label Print"::"1 par ligne":
                                    EditLabel(ItemCrossRef, 1, ShipmentLine.Quantity);
                                ItemCrossRef."Shipment Label Print"::Aucune:
                                    ;
                                ItemCrossRef."Shipment Label Print"::"Par multiple d'achat":
                                    BEGIN
                                        item.GET(ShipmentLine."No.");
                                        IF item."Purch. Multiple" = 0 THEN
                                            EditLabel(ItemCrossRef, 1, item."Purch. Multiple")
                                        ELSE
                                            EditLabel(ItemCrossRef, ROUND(ShipmentLine.Quantity / item."Purch. Multiple", 1, '>'),
                                                    item."Purch. Multiple");
                                    END;
                                ItemCrossRef."Shipment Label Print"::"Par multiple de vente":
                                    BEGIN
                                        item.GET(ShipmentLine."No.");
                                        IF item."Sales multiple" = 0 THEN
                                            EditLabel(ItemCrossRef, 1, item."Sales multiple")
                                        ELSE
                                            EditLabel(ItemCrossRef, ROUND(ShipmentLine.Quantity / item."Sales multiple", 1, '>'),
                                                      item."Sales multiple");
                                    END;

                            END;
                        END;
                    UNTIL ShipmentLine.NEXT() = 0;
    end;

    procedure EditLabel(ItemCrossRef: Record "Item Reference"; Qte: Integer; NbPieces: Integer)
    var
    item: Record Item;
        Cust: Record Customer;
        FrmEtq: Record "Format Etiquette Client";
        FichierEtq: File;
        MonFichierEtq: Text[250];
        cr: Char;
        ctl_b: Char;
           ascii_10: Char;
          DesignationArticle: Text[50];
    begin
        item.GET(ItemCrossRef."Item No.");
        Cust.GET(ItemCrossRef."Reference Type No.");
        FrmEtq.GET(ItemCrossRef."Code Format Etiquette");

        DesignationArticle := item.Description;

        FrmEtq.TESTFIELD("Repertoire Etiquette");

        MonFichierEtq := STRSUBSTNO('%1\%2-%3.TXT',
          FrmEtq."Repertoire Etiquette",
         ItemCrossRef."Reference No.",
          FORMAT(TIME, 0, '<Hours24,2><Filler Character,0><Minutes,2><Seconds,2><Second dec.>'));

        FichierEtq.CREATE(MonFichierEtq);
        FichierEtq.TEXTMODE(TRUE);

        cr := 13;
        ctl_b := 2;
        ascii_10 := 10;

        FichierEtq.WRITE(FORMAT(ctl_b) + 'n' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'e' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'c0000' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'RN' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'f215' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'V0' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'O0215' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'M2080' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'qC' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'L' + FORMAT(cr));
        FichierEtq.WRITE('A2' + FORMAT(cr));
        FichierEtq.WRITE('D11' + FORMAT(cr));
        FichierEtq.WRITE('z' + FORMAT(cr));
        FichierEtq.WRITE('PG' + FORMAT(cr));
        FichierEtq.WRITE('SG' + FORMAT(cr));
        FichierEtq.WRITE('H22' + FORMAT(cr));

        IF FrmEtq."Imprimer Zone Libre 1" = FrmEtq."Imprimer Zone Libre 1"::Oui THEN
            FichierEtq.WRITE('442200002860050' + ItemCrossRef."Zone Libre 1" + FORMAT(cr));
        IF FrmEtq."Imprimer Zone Libre 2" = FrmEtq."Imprimer Zone Libre 2"::Oui THEN
            FichierEtq.WRITE('442200002880388' + ItemCrossRef."Zone Libre 2" + FORMAT(cr));
        IF FrmEtq."Imprimer Nom Client" = FrmEtq."Imprimer Nom Client"::Oui THEN
            FichierEtq.WRITE('461100001470093' + Cust.Name + FORMAT(cr));

        IF FrmEtq."Imprimer N° Article" = FrmEtq."Imprimer N° Article"::Oui THEN BEGIN
            FichierEtq.WRITE('451100001400144' + ItemCrossRef."Item No." + FORMAT(cr));
            FichierEtq.WRITE('451100000720143REF :' + FORMAT(cr));
        END;

        IF FrmEtq."Imprimer Référence Externe" = FrmEtq."Imprimer Référence Externe"::Oui THEN BEGIN
            FichierEtq.WRITE('451100003660144REF CLIENT :' + FORMAT(cr));
            FichierEtq.WRITE('451100005050143' + ItemCrossRef."Reference No." + FORMAT(cr));
        END;

        IF FrmEtq."Imprimer Designatio," = FrmEtq."Imprimer Designatio,"::Oui THEN
            FichierEtq.WRITE('441100000140187' + DesignationArticle);
        IF FrmEtq."Imprimer Zone Libre 3" = FrmEtq."Imprimer Zone Libre 3"::Oui THEN
            FichierEtq.WRITE('442200000500334' + ItemCrossRef."Zone Libre 3" + FORMAT(cr));
        IF FrmEtq."Imprimer Zone Libre 4" = FrmEtq."Imprimer Zone Libre 4"::Oui THEN
            FichierEtq.WRITE('442200002870334' + ItemCrossRef."Zone Libre 4" + FORMAT(cr));
        IF FrmEtq."Imprimer Gencode" = FrmEtq."Imprimer Gencode"::Oui THEN
            FichierEtq.WRITE('4F4408506050311' + item."Gencod EAN13" + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + '451100000240047' + FORMAT(NbPieces) + FORMAT(cr));
        FichierEtq.WRITE('451100000710047PCS' + FORMAT(cr));
        FichierEtq.WRITE('^01' + FORMAT(cr));
        FichierEtq.WRITE('Q' + FORMAT(Qte, 0, '<Integer,4><Filler Character,0>') + FORMAT(cr));
        FichierEtq.WRITE('E' + FORMAT(cr));

        FichierEtq.CLOSE()
    end;

    procedure EditItemLabel(ItemNo: Code[20]; TypeFormat: Code[20]; Qte: Decimal)
    var
    item: Record Item;
        FrmETQ: Record "Format Etiquette Client";
        FichierEtq: File;
        MonFichierEtq: Text[250];
        cr: Char;
        ctl_b: Char;
       // ligne: Text[250];
        ascii_10: Char;
        
    begin
        CASE TypeFormat OF
            'PETIT':
                ;
            'GRAND':
                ;
            ELSE
                ERROR('Format inconnu');
        END;

        item.GET(ItemNo);

        FrmETQ.GET('ETQ_ART');

        MonFichierEtq := STRSUBSTNO('%1\%2-%3.TXT',
          FrmETQ."Repertoire Etiquette",
          item."No.",
          FORMAT(TIME, 0, '<Hours24,2><Filler Character,0><Minutes,2><Seconds,2><Second dec.>'));

        FichierEtq.CREATE(MonFichierEtq);
        FichierEtq.TEXTMODE(TRUE);

        cr := 13;
        ctl_b := 2;
        ascii_10 := 10;

        FichierEtq.WRITE('');

        FichierEtq.WRITE(FORMAT(ctl_b) + 'n' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'e' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'c0000' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'RN' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'f215' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'V0' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'O0215' + FORMAT(cr));
        CASE TypeFormat OF
            'GRAND':
                FichierEtq.WRITE(FORMAT(ctl_b) + 'M0700' + FORMAT(cr));
            'PETIT':
                FichierEtq.WRITE(FORMAT(ctl_b) + 'M0500' + FORMAT(cr));
        END;


        FichierEtq.WRITE(FORMAT(ctl_b) + 'qC' + FORMAT(cr));
        FichierEtq.WRITE(FORMAT(ctl_b) + 'L' + FORMAT(cr));
        FichierEtq.WRITE('A2' + FORMAT(cr));
        FichierEtq.WRITE('D11' + FORMAT(cr));
        FichierEtq.WRITE('z' + FORMAT(cr));
        FichierEtq.WRITE('PG' + FORMAT(cr));
        FichierEtq.WRITE('SG' + FORMAT(cr));
        FichierEtq.WRITE('H22' + FORMAT(cr));

        CASE TypeFormat OF
            'PETIT':
                BEGIN
                    FichierEtq.WRITE('141100001040034ART :' + FORMAT(cr));
                    FichierEtq.WRITE('141100001040231ART :' + FORMAT(cr));
                    FichierEtq.WRITE('141100001040090' + item."No." + FORMAT(cr));
                    FichierEtq.WRITE('141100001040287' + item."No." + FORMAT(cr));
                    FichierEtq.WRITE('1F3306000120022' + item."Gencod EAN13" + FORMAT(cr));
                    FichierEtq.WRITE('1F3306000120219' + item."Gencod EAN13" + FORMAT(cr));
                END;
            'GRAND':
                BEGIN
                    FichierEtq.WRITE('451100000360038ART :' + FORMAT(cr));
                    FichierEtq.WRITE('451100000360231ART :' + FORMAT(cr));
                    FichierEtq.WRITE('461100001010043' + item."No." + FORMAT(cr));
                    FichierEtq.WRITE('461100001010236' + item."No." + FORMAT(cr));
                    FichierEtq.WRITE('4F4411200230191' + item."Gencod EAN13" + FORMAT(cr));
                    FichierEtq.WRITE('4F4411200230384' + item."Gencod EAN13" + FORMAT(cr));
                END;
        END;

        FichierEtq.WRITE('^01' + FORMAT(cr));
        FichierEtq.WRITE('Q' + FORMAT(Qte, 0, '<Integer,4><Filler Character,0>') + FORMAT(cr));
        FichierEtq.WRITE('E' + FORMAT(cr));


        FichierEtq.CLOSE();
    end;

    procedure EditionEtiquetteUniquNiceLabel(_pItem: Record Item; _pQteCondit: Integer; _pTexte: Code[10]; _pNoDocument: Code[20]; _pEmplacement: Code[20]; _pTabComment: array[3] of Text[30]; _pCodeBarre: Code[30]; _pQteEtiquette: Integer; _pModele: Code[20]; _pImprimante: Code[20])
    var
        LMonFichier: File;
        LNomFic: Text[250];
    begin

        LNomFic := _pItem."No." + '_' + _pModele + '_' + _pImprimante;
        OuvrirEtiquetteNiceLabel(LNomFic, LMonFichier);
        EdittEtiqetteNiceLabel(LMonFichier, _pItem, _pQteCondit, _pTexte, _pNoDocument, _pEmplacement, _pTabComment,
              _pCodeBarre, _pQteEtiquette, _pModele, _pImprimante);
        FermeEtiquetteNiceLabel(LNomFic, LMonFichier);
    end;

    procedure OuvrirEtiquetteNiceLabel(_pNomFichier: Text[50]; var _pFichierEtq: File)
   
    begin
        //tmp_chemin := TEMPORARYPATH + '/' + _pNomFichier + '.txt';
        _pFichierEtq.CREATE(TEMPORARYPATH + '/' + _pNomFichier + '.txt');
        _pFichierEtq.TEXTMODE(TRUE);
    end;

    procedure EdittEtiqetteNiceLabel(var _pFichier: File; _pItem: Record Item; _pQteCondit: Integer; _pTexte: Code[10]; _pNoDocument: Code[20]; _pEmplacement: Code[20]; _pTabComment: array[3] of Text[30]; _pCodeBarre: Code[30]; _pQteEtiquette: Integer; _pModele: Code[20]; _pImprimante: Code[20])
    var
        
        LParam: Record "Generals Parameters";
        LWhseSetup: Record "Warehouse Setup";
        LGestionMultiRef: Codeunit "Gestion Multi-référence";
        LRefArticle: Code[20];
        LLigne: Text[1024];
    begin


        LLigne := '';

        // AD Le 06-02-2015 => Demande de Vincent
        //IF EVALUATE(LEntier, _pItem."No. 2") THEN
        //  LRefArticle := FORMAT(LEntier, 0, '<Integer><1000Character, >')
        //ELSE
        LRefArticle := LGestionMultiRef.RechercheRefActive(_pItem."No.");

        LLigne += _pItem."No." + ';';
        LLigne += LRefArticle + ';';
        LLigne += _pItem.Description + ';';
        LLigne += _pItem."Description 2" + ';';

        IF LParam.GET('NICELBL_TXT', _pTexte) THEN
            LLigne += LParam.LongDescription;
        LLigne += ';';
        LLigne += FORMAT(_pQteCondit) + ';';
        LLigne += _pNoDocument + ';';
        LLigne += _pEmplacement + ';';
        LLigne += _pCodeBarre + ';';

        LLigne += FORMAT(_pQteEtiquette) + ';';

        // On place le nom du format dans le fichier
        LWhseSetup.GET();
        LParam.GET('NICELBL_FRM', _pModele);
        LLigne += LWhseSetup."Chemin Frm Etiquette NiceLabel" + '\' + LParam.LongDescription2 + ';';


        // on place le nom windows de l'imprimante dans le fichier
        IF NOT LParam.GET('NICELBL_IMP', _pImprimante) THEN
            IF NOT LParam.GET('IMP_ETQ_RECEPT', _pImprimante) THEN;
        LLigne += LParam.LongDescription2 + ';';

        // Commentaires
        LLigne += _pTabComment[1] + ';';
        LLigne += _pTabComment[2] + ';';
        LLigne += _pTabComment[3] + ';';


        _pFichier.WRITE(LLigne);
    end;

    procedure FermeEtiquetteNiceLabel(_pNomFichier: Text[50]; var _pFichierEtq: File)
    var
        LWhseSetup: Record "Warehouse Setup";
    begin
        _pFichierEtq.CLOSE();
        LWhseSetup.GET();
        RENAME(TEMPORARYPATH + '/' + _pNomFichier + '.txt', LWhseSetup."Chemin Etiquette NiceLabel" + '/' + _pNomFichier + '.txt');
        //IF GUIALLOWED THEN MESSAGE('ad changer le répertoire dans cu 50000 !');
        //RENAME(TEMPORARYPATH + '/' + _pNomFichier + '.txt', '\\Srv-avimp\etiquettes\NICELABEL\TESTS_SORTIES\' + '/' + _pNomFichier + '.txt');
    end;

    procedure purgeUserId(): Code[50]
    var
        LUser: Code[50];
        LPos: Integer;
    begin

        LUser := USERID;
        LPos := STRPOS(LUser, '\');
        IF LPos <> 0 THEN
            LUser := COPYSTR(LUser, LPos);

        EXIT(LUser);
    end;
}

