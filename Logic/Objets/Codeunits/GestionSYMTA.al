codeunit 50009 "Gestion SYMTA"
{
    Permissions = TableData 110 = rm;


    trigger OnRun()
    begin
    end;

    procedure CompleteNoClient(var Text: Text[20])
    var
        PosTiret: Integer;
        ChaineAvantTiret: Code[20];
    begin
        PosTiret := STRPOS(Text, '-');
        IF PosTiret = 0 THEN
            EXIT;

        ChaineAvantTiret := COPYSTR(Text, 1, PosTiret - 1);

        WHILE STRLEN(ChaineAvantTiret) < 6 DO
            ChaineAvantTiret := '0' + ChaineAvantTiret;

        Text := ChaineAvantTiret + COPYSTR(Text, PosTiret, STRLEN(Text) - PosTiret + 1);
    end;

    procedure GetCommentaireReception(var recL_TmpCommentLine: Record "Comment Line" temporary; NoArticleInterne: Code[20]; NoFusion: Code[20]; ShowFourn: Boolean)
    var
        LCommentLine: Record "Comment Line";
        PuchCommentLine: Record "Purch. Comment Line";
        WareReceiptLine: Record "Warehouse Receipt Line";       
          rec_PrePointage: Record "Saisis Réception Magasin";
        recL_WareReceiptHeader: Record "Warehouse Receipt Header";
        decL_NumLigne: Decimal;
    begin
        // MCO Le 24-03-2016
        // Nouveau code
        // On charge dans une temporaire les 3 sortes de commentaires en excluant les doublons
        recL_TmpCommentLine.RESET();
        decL_NumLigne := 0;
        // Commentaire Articles
        LCommentLine.RESET();
        LCommentLine.SETRANGE("Table Name", LCommentLine."Table Name"::Item);
        LCommentLine.SETRANGE("No.", NoArticleInterne);
        LCommentLine.SETRANGE("Display Purch. Receipt", TRUE);
        IF LCommentLine.FINDSET() THEN
            REPEAT
                recL_TmpCommentLine := LCommentLine;
                decL_NumLigne += 1;
                recL_TmpCommentLine."Line No." := decL_NumLigne;
                recL_TmpCommentLine.Code := 'ART';
                recL_TmpCommentLine.Insert();
            UNTIL LCommentLine.NEXT() = 0;

        //commentaire commande achat
        WareReceiptLine.RESET();
        WareReceiptLine.SETRANGE(WareReceiptLine."N° Fusion Réception", NoFusion);
        WareReceiptLine.SETRANGE(WareReceiptLine."Item No.", NoArticleInterne);
        IF WareReceiptLine.FINDFIRST() THEN
            REPEAT
                PuchCommentLine.RESET();
                PuchCommentLine.SETRANGE(PuchCommentLine."No.", WareReceiptLine."Source No.");
                PuchCommentLine.SETRANGE(PuchCommentLine."Document Line No.", WareReceiptLine."Source Line No.");
                PuchCommentLine.SETRANGE(PuchCommentLine."Display Receipt", TRUE);
                IF PuchCommentLine.FINDFIRST() THEN
                    REPEAT
                        recL_TmpCommentLine.RESET();
                        recL_TmpCommentLine.SETRANGE(Comment, PuchCommentLine.Comment);
                        IF recL_TmpCommentLine.ISEMPTY THEN BEGIN
                            recL_TmpCommentLine."Table Name" := LCommentLine."Table Name"::Item;
                            recL_TmpCommentLine."No." := NoArticleInterne;
                            decL_NumLigne += 1;
                            recL_TmpCommentLine."Line No." := decL_NumLigne;

                            recL_TmpCommentLine.Date := PuchCommentLine.Date;
                            recL_TmpCommentLine.Comment := PuchCommentLine.Comment;
                            recL_TmpCommentLine."Display Purch. Receipt" := PuchCommentLine."Display Receipt";
                            recL_TmpCommentLine.Code := 'CDE';
                            recL_TmpCommentLine.Insert();
                        END;
                    UNTIL PuchCommentLine.NEXT() = 0;

                // MCO Le 08-09-2017 => Régie
                // Récupération des commentaires en pré pointages
                recL_WareReceiptHeader.GET(WareReceiptLine."No.");
                rec_PrePointage.RESET();
                rec_PrePointage.SETRANGE("No.", recL_WareReceiptHeader."N° Fusion Réception");
                rec_PrePointage.SETRANGE("Item No.", WareReceiptLine."Item No.");
                rec_PrePointage.SETFILTER(Commentaire, '<>%1', '');
                IF rec_PrePointage.FINDSET() THEN
                    REPEAT
                        recL_TmpCommentLine.RESET();
                        recL_TmpCommentLine.SETRANGE(Comment, rec_PrePointage.Commentaire);
                        IF recL_TmpCommentLine.ISEMPTY THEN BEGIN
                            recL_TmpCommentLine."Table Name" := LCommentLine."Table Name"::Item;
                            recL_TmpCommentLine."No." := WareReceiptLine."Item No.";
                            decL_NumLigne += 1;
                            recL_TmpCommentLine."Line No." := decL_NumLigne;

                            recL_TmpCommentLine.Date := rec_PrePointage."Date Modif";
                            recL_TmpCommentLine.Comment := rec_PrePointage.Commentaire;
                            recL_TmpCommentLine.Code := 'PRC';
                            recL_TmpCommentLine.Insert();
                        END;
                    UNTIL rec_PrePointage.NEXT() = 0;


            UNTIL WareReceiptLine.NEXT() = 0;

        // Commentaire fournisseurs
        IF ShowFourn THEN BEGIN
            WareReceiptLine.RESET();
            WareReceiptLine.SETRANGE(WareReceiptLine."N° Fusion Réception", NoFusion);
            WareReceiptLine.SETRANGE(WareReceiptLine."Item No.", NoArticleInterne);
            IF WareReceiptLine.FINDFIRST() THEN BEGIN
                LCommentLine.RESET();
                LCommentLine.SETRANGE("Table Name", LCommentLine."Table Name"::Vendor);
                LCommentLine.SETRANGE("No.", WareReceiptLine.GetInfoLigneCode('FOURN'));
                LCommentLine.SETRANGE("Display Purch. Receipt", TRUE);
                IF LCommentLine.FINDSET() THEN
                    REPEAT
                        recL_TmpCommentLine := LCommentLine;
                        decL_NumLigne += 1;
                        recL_TmpCommentLine."Line No." := decL_NumLigne;
                        recL_TmpCommentLine.Code := 'FOURN';
                        recL_TmpCommentLine.Insert();
                    UNTIL LCommentLine.NEXT() = 0;
            END;
        END;
        recL_TmpCommentLine.RESET();
        // MCO Le 24-03-2016 => Nouvelle gestion des commentaires
    end;

    PROCEDURE T110_EntierementFacture(pNo: Code[20]);
    VAR
        lSalesShipmentHeader: Record "Sales Shipment Header";
    BEGIN
        // CFR le 05/10/2023 - R‚gie : ajout Fonction T110_EntierementFacture() pour corriger pb ponctuel
        // Cas des lignes de BL n‚gatives !!
        IF NOT lSalesShipmentHeader.GET(pNo) THEN EXIT;

        IF (lSalesShipmentHeader."Entierement facturé") THEN
            ERROR('Ce BL est déjà coché entièrement facturé');

        lSalesShipmentHeader.CALCFIELDS("Somme Qté livrée non facturée");
        IF (lSalesShipmentHeader."Somme Qté livrée non facturée" > 0) THEN
            ERROR('Ce BL n''est pas entièrement facturé');
        IF CONFIRM('Voulez vous cocher ce BL comme entièrement facturé ?') THEN BEGIN
            lSalesShipmentHeader."Entierement facturé" := TRUE;
            lSalesShipmentHeader.VALIDATE("En attente de facturation", FALSE);
            lSalesShipmentHeader.MODIFY(FALSE);
        END;
    END;

    // CFR le 05/10/2023 - R‚gie : ajout Fonction T110_EntierementFacture() pour corriger pb ponctuel
}

