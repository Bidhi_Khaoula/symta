codeunit 50024 "Export Factures SCAR"
{
    Permissions = TableData "Sales Invoice Header" = rm,
                  TableData "Sales Cr.Memo Header" = rm;

    trigger OnRun()
   
    begin
    end;

    var
       
        
        InfoComptabilite: Record "General Ledger Setup";
        "InfoSociété": Record "Company Information";
          gPurchaseCentralHistory: Record "Purchase Central History";
        
         Text001Qst: Label 'Voulez vous exporter les factures EDI ?';
        FichierEdi: File;
        
        sep: Code[1];
        
        Ligne: Text[1024];
        Text002Err: Label 'La date de début doit être > à la date de fin !';
        Text003Err: Label 'Les date des début et fin doivent être renseignées !';
        Text004Err: Label 'Code Exportation Obligatoire !';
     
        Window: Dialog;
       
        Text100Lbl: Label 'Type facturation EDI ################1#\\' , Comment = '%1';
        Text101Lbl: Label 'Code Client               ################2#\\' ,Comment = '%1';
        Text102Lbl: Label 'No Facture                ################3#\\' ,Comment = '%1';
      

    procedure ExportInvoiceEDI(TypeExportation: Option "Génération","Re-Génération",Maintenance; "DateDébut": Date; DateFin: Date; CodeExportation: Code[10])
    var
        Param: Record "Generals Parameters";
        EnteteFacture: Record "Sales Invoice Header";
        EnteteAvoir: Record "Sales Cr.Memo Header";
        Customer: Record Customer;
        LMois: Code[6];
    begin
        sep := ';';


        // AD LE 29-04-2020 => J'ai tout regroupé dans le cu5022
        ERROR('Contacter ESKAPE !');

        // Lecture des informations générales
        // ---------------------------------- Unreachable Code
     /*    InfoComptabilite.GET();
        InfoSociété.GET();

        // Test des paramétres
        // -------------------
        CASE TypeExportation OF
            TypeExportation::Génération:
                BEGIN
                END;
            TypeExportation::"Re-Génération":
                BEGIN
                    IF (DateDébut = 0D) OR (DateFin = 0D) THEN
                        ERROR(Text003Err);

                    IF (DateDébut > DateFin) THEN
                        ERROR(Text002Err);

                    IF (CodeExportation = '') THEN
                        ERROR(Text004Err);
                END;
            TypeExportation::Maintenance:
                               ERROR('Pas codé');
                        END;

        IF NOT CONFIRM(Text001Qst, TRUE) THEN
            EXIT;

        Window.OPEN(Text100Lbl + Text101Lbl + Text102Lbl);

        LMois := FORMAT(DateDébut, 0, '<Year4><Month,2>');

        CASE TypeExportation OF
            // #################################################
            // Génrétaion de toutes les factures avoirs non émis
            // #################################################
            TypeExportation::Génération:
                BEGIN
                    // Lecture des <> paramètres
                    // -------------------------
                    Param.RESET();
                    Param.SETRANGE(Type, 'FIC_EDI');
                    Param.SETRANGE(Code, 'FAC_SCAR');
                    IF Param.FIND('-') THEN
                        REPEAT
                            Window.UPDATE(1, Param.Code);

                            // Rechcerche si des clients avec ce no
                            // ------------------------------------
                            Customer.RESET();
                            Customer.SETCURRENTKEY("Code Facturation EDI");
                            Customer.SETRANGE("Code Facturation EDI", Param.Code);
                            IF Customer.FIND('-') THEN
                                REPEAT

                                    Window.UPDATE(2, Customer."No.");

                                    // Pose des filtres
                                    // ----------------

                                    Customer.TESTFIELD("Date Début Export Facture EDI");
                                    EnteteFacture.RESET();
                                    EnteteFacture.SETFILTER("Posting Date", '%1..', Customer."Date Début Export Facture EDI");
                                    EnteteFacture.SETRANGE("Sell-to Customer No.", Customer."No.");
                                    EnteteFacture.SETRANGE("EDI Exportation", FALSE);


                                    EnteteAvoir.RESET();
                                    EnteteAvoir.SETFILTER("Posting Date", '%1..', Customer."Date Début Export Facture EDI");
                                    EnteteAvoir.SETRANGE("Sell-to Customer No.", Customer."No.");
                                    EnteteAvoir.SETRANGE("EDI Exportation", FALSE);

                                    // Exportation des données pour les filtres
                                    // ----------------------------------------
                                    RechercheFactureAvoir(Customer."Code Facturation EDI", EnteteFacture, EnteteAvoir, Customer."No.", LMois);
                                UNTIL Customer.NEXT() = 0;
                        UNTIL Param.NEXT() = 0;


                END;
            // ##########################################
            // Re-Génrétaion des factures avoirs demandés
            // ##########################################
            TypeExportation::"Re-Génération":
                BEGIN
                    // Pose des filtres
                    // ----------------
                    EnteteFacture.RESET();
                    EnteteFacture.SETRANGE("Posting Date", DateDébut, DateFin);
                    //EnteteFacture.SETRANGE("Code Export EDI", CodeExportation);
                    //EnteteFacture.SETRANGE("Export EDI", TRUE);

                    EnteteAvoir.RESET();
                    EnteteAvoir.SETRANGE("Posting Date", DateDébut, DateFin);
                    //EnteteAvoir.SETRANGE("Code Export EDI", CodeExportation);
                    //EnteteAvoir.SETRANGE("Export EDI", TRUE);

                    // Exportation des données pour les filtres
                    // ----------------------------------------
                    RechercheFactureAvoir(CodeExportation, EnteteFacture, EnteteAvoir, Customer."No.", LMois);

                END;
            // ########################################
            // Re-Génrétaion des factures selectionnées
            // ########################################
            TypeExportation::Maintenance:
                BEGIN
                END;
        END; */
    end;

    procedure OuvrirFichierEdi(CodeFic: Code[10]; _pCodeClient: Code[10]; _pMois: Code[10])
    var
        param: Record "Generals Parameters";
         Cust: Record Customer;
         NomFichier: Text[254];
    begin
        // Ouverture du fichier pour ce client
        // -----------------------------------
        FichierEdi.TEXTMODE(TRUE);
        FichierEdi.WRITEMODE(TRUE);

        Cust.GET(_pCodeClient);

        InfoSociété.GET();

        param.GET('FIC_EDI', CodeFic);
        param.TESTFIELD(param.LongDescription2);

        NomFichier := InfoSociété."Export EDI Folder" + '\' + Cust.Name + '-' + _pMois + '.txt';

        IF FILE.EXISTS(NomFichier) THEN
            FichierEdi.OPEN(NomFichier)
        ELSE
            FichierEdi.CREATE(NomFichier);

        FichierEdi.SEEK(FichierEdi.LEN);
    end;

    procedure RechercheFactureAvoir(LCodeExportation: Code[10]; var LEnteteFacture: Record "Sales Invoice Header"; var LEnteteAvoir: Record "Sales Cr.Memo Header"; _pNoClient: Code[10]; _pMois: Code[6])
    begin

        // Aucune facture ni avoir à exporter
        // ----------------------------------
        IF (LEnteteFacture.COUNT = 0) AND (LEnteteAvoir.COUNT = 0) THEN
            EXIT;

        // Ouverture du fichier EDI
        // ------------------------
        OuvrirFichierEdi(LCodeExportation, _pNoClient, _pMois);

        // Export des données
        // ------------------
        IF LEnteteFacture.FINDSET() THEN
            REPEAT
                Window.UPDATE(2, LEnteteFacture."Sell-to Customer No.");
                Window.UPDATE(3, LEnteteFacture."No.");
                ExportFactureSCAR(LEnteteFacture, LCodeExportation, _pMois);
            UNTIL LEnteteFacture.NEXT() = 0;



        IF LEnteteAvoir.FINDSET() THEN
            REPEAT
                Window.UPDATE(2, LEnteteAvoir."Sell-to Customer No.");
                Window.UPDATE(3, LEnteteAvoir."No.");
                ExportAvoirSCAR(LEnteteAvoir, LCodeExportation, _pMois);
            UNTIL LEnteteAvoir.NEXT() = 0;


        // Fermeture du fichier EDI
        // ------------------------
        FichierEdi.CLOSE();
    end;

    procedure ExportFactureSCAR(LEnteteFacture: Record "Sales Invoice Header"; LCodeExportation: Code[10]; _pMois: Code[6])
    var
        LClient: Record Customer;
        LSalesInvLine: Record "Sales Invoice Line";
        CliLiv: Record Customer;
        CustAmount: Decimal;
        AmountInclVAT: Decimal;
        LMtPortHt: Decimal;
        LMtPortTTC: Decimal;
        VATAmount: Decimal;
        
    begin

        LClient.GET(LEnteteFacture."Sell-to Customer No.");
        LEnteteFacture.CALCFIELDS("Amount Including VAT", Amount);

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        LSalesInvLine.RESET();
        LSalesInvLine.SETRANGE("Document No.", LEnteteFacture."No.");
        LSalesInvLine.SETFILTER(Type, '>0');

        IF LSalesInvLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + LSalesInvLine.Amount;
                AmountInclVAT := AmountInclVAT + LSalesInvLine."Amount Including VAT";
            UNTIL LSalesInvLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        // ----------------------------
        // Recherche du montant du port
        // ----------------------------
        LSalesInvLine.RESET();
        LSalesInvLine.SETRANGE("Document No.", LEnteteFacture."No.");
        LSalesInvLine.SETRANGE("Internal Line Type", LSalesInvLine."Internal Line Type"::Shipment);
        IF LSalesInvLine.FINDset() THEN
            REPEAT
                LMtPortHt += LSalesInvLine.Amount;
                LMtPortTTC += LSalesInvLine."Amount Including VAT";
            UNTIL LSalesInvLine.NEXT() = 0;
        // ----------------------------
        // FIN Recherche du montant du port
        // ----------------------------

        //----------------------------------------------------------------------------------------------------------------------------------
        // WF le 06/03/2012 => Bug si le client n'existe pas, on ajout le test et initialise "Libre 3" à vide
        //CliLiv.GET(LSalesInvLine."N° client Livré");
        IF NOT CliLiv.GET(LSalesInvLine."N° client Livré") THEN CliLiv."Libre 3" := '';
        //Fin WF le 06/03/2012
        //----------------------------------------------------------------------------------------------------------------------------------

        Ligne := '';
        Ligne += '000206';
        Ligne += _pMois;
        Ligne += 'FV' + COPYSTR(LEnteteFacture."No.", 5, 8); // METTRE SUR 10 CARACTERES
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        //Ligne += PADSTR(CliLiv."Libre 3", 6);
        Ligne += PADSTR(gPurchaseCentralHistory.GetAdherentFromCustomerCentralDate(LSalesInvLine."N° client Livré", LClient."No.", LEnteteFacture."Document Date"), 6);
        // FIN CFR le 24/09/2021        
        Ligne += 'F';
        Ligne += FormatDecimal(AmountInclVAT); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(CustAmount - LMtPortHt); // METTRE SUR 10 CARACTERE
        Ligne += '          '; // 10 ESPACES
        Ligne += FormatDecimal(VATAmount); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(LMtPortHt);
        Ligne += FORMAT(LEnteteFacture."Due Date", 0, '<Year4><Month,2><Day,2>');
        Ligne += '   '; // 3 ESPACES
        Ligne += FormatDecimal(LMtPortTTC - LMtPortHt);
        //Ligne += '............................';


        FichierEdi.WRITE(PurgeAccents(Ligne));



        LEnteteFacture."EDI Exportation" := TRUE;
        LEnteteFacture."EDI Exportation Date" := TODAY;
        LEnteteFacture."EDI Exportation Time" := TIME;
        LEnteteFacture."EDI Exportation User" := USERID;
        LEnteteFacture."EDI Exportation Code" := LCodeExportation;
        LEnteteFacture.MODIFY()
    end;

    procedure ExportAvoirSCAR(LEnteteAvoir: Record "Sales Cr.Memo Header"; LCodeExportation: Code[20]; _pMois: Code[6])
    var
        LSalesCreLine: Record "Sales Cr.Memo Line";
         LClient: Record Customer;
         CliLiv: Record Customer;
        CustAmount: Decimal;
        AmountInclVAT: Decimal;     
        LMtPortHt: Decimal;
        LMtPortTTC: Decimal;
        VATAmount: Decimal;
        
    begin
        Ligne := '';

        LClient.GET(LEnteteAvoir."Sell-to Customer No.");
        LEnteteAvoir.CALCFIELDS("Amount Including VAT", Amount);

        // ----------------------------------------------------------------------------
        // Calcul des montants TVA - TTC - HT (Basé sur le F9 des facture enregistrées)
        // ----------------------------------------------------------------------------
        LSalesCreLine.RESET();
        LSalesCreLine.SETRANGE("Document No.", LEnteteAvoir."No.");
        LSalesCreLine.SETFILTER(Type, '>0');
        IF LSalesCreLine.FIND('-') THEN
            REPEAT
                CustAmount := CustAmount + LSalesCreLine.Amount;
                AmountInclVAT := AmountInclVAT + LSalesCreLine."Amount Including VAT";
            UNTIL LSalesCreLine.NEXT() = 0;
        VATAmount := AmountInclVAT - CustAmount;
        // --------------------------
        // FIN du calcul des montants
        // --------------------------

        // ----------------------------
        // Recherche du montant du port
        // ----------------------------
        LSalesCreLine.RESET();
        LSalesCreLine.SETRANGE("Document No.", LEnteteAvoir."No.");
        LSalesCreLine.SETRANGE("Internal Line Type", LSalesCreLine."Internal Line Type"::Shipment);
        IF LSalesCreLine.FindSet() THEN
            REPEAT
                LMtPortHt += LSalesCreLine.Amount;
                LMtPortTTC += LSalesCreLine."Amount Including VAT";
            UNTIL LSalesCreLine.NEXT() = 0;
        // ----------------------------
        // FIN Recherche du montant du port
        // ----------------------------

        CliLiv.GET(LSalesCreLine."N° client Livré");

        Ligne := '';
        Ligne += '000206';
        Ligne += _pMois;
        Ligne += 'AV' + COPYSTR(LEnteteAvoir."No.", 5, 8); // METTRE SUR 10 CARACTERES
        // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
        //Ligne += PADSTR(CliLiv."Libre 3", 6);
        Ligne += PADSTR(gPurchaseCentralHistory.GetAdherentFromCustomerCentralDate(LSalesCreLine."N° client Livré", LClient."No.", LEnteteAvoir."Document Date"), 6);
        // FIN CFR le 24/09/2021

        Ligne += 'A';
        Ligne += FormatDecimal(AmountInclVAT); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(CustAmount - LMtPortHt); // METTRE SUR 10 CARACTERE
        Ligne += '          '; // 10 ESPACES
        Ligne += FormatDecimal(VATAmount); // METTRE SUR 10 CARACTERE
        Ligne += FormatDecimal(LMtPortHt);
        Ligne += FORMAT(LEnteteAvoir."Due Date", 0, '<Year4><Month,2><Day,2>');
        Ligne += '   '; // 3 ESPACES
        Ligne += FormatDecimal(LMtPortTTC - LMtPortHt);
        //Ligne += '............................';


        FichierEdi.WRITE(PurgeAccents(Ligne));

        LEnteteAvoir."EDI Exportation" := TRUE;
        LEnteteAvoir."EDI Exportation Date" := TODAY;
        LEnteteAvoir."EDI Exportation Time" := TIME;
        LEnteteAvoir."EDI Exportation User" := USERID;
        LEnteteAvoir."EDI Exportation Code" := LCodeExportation;
        LEnteteAvoir.MODIFY()
    end;

    procedure "--- Formatage des données ---"()
    begin
    end;

    procedure FormatDecimal(Value: Decimal): Text[50]
    begin
        EXIT(FORMAT(Value * 100, 0, '<Integer,10><Filler Character,0>'));
    end;

    procedure "--- Divers ---"()
    begin
    end;

    procedure PurgeAccents(chaine: Text[1024]): Text[1024]
    var
        NewChaine: Text[1024];
    begin
        NewChaine := CONVERTSTR(chaine, 'àäéèêëïîöô', 'aaeeeeiioo');
        EXIT(NewChaine);
    end;

    // CFR le 24/09/2021 - SFD20210201 historique Centrale Active
}

