codeunit 50021 Globals
{
    // Gestion de variables globales (via SingleInstance)

    SingleInstance = true;

    trigger OnRun()
    begin
    end;

    var
        fromTherefore: Boolean;

    procedure setFromTherefore(value: Boolean)
    begin
        fromTherefore := value;
    end;

    procedure getFromTherefore(): Boolean
    begin
        EXIT(fromTherefore);
    end;
}

