codeunit 50002 "ANSI  <->  ASCII converter"
{

    trigger OnRun()
    var
       
    begin
    end;

    var
        AsciiStr: Text[250];
        AnsiStr: Text[250];
        CharVar: array[32] of Char;
        car_apostropheLbl: Label '''';
        car_tiretLbl: Label '-';
        car_pointLbl: Label '.';
        car_virguleLbl: Label ',';
        car_numeroLbl: Label '°';

    procedure Ansi2Ascii(_Text: Text[250]): Text[250]
    begin
        MakeVars();
        EXIT(CONVERTSTR(_Text, AnsiStr, AsciiStr));
    end;

    procedure Ascii2Ansi(_Text: Text[250]): Text[250]
    begin
        MakeVars();
        EXIT(CONVERTSTR(_Text, AsciiStr, AnsiStr));
    end;

    procedure MakeVars()
    begin
        AsciiStr := 'ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®¬½¼¡«»¦¦¦¦¦ÁÂÀ©¦¦++¢¥++--+-+ãÃ++--¦-+';
        AsciiStr := AsciiStr + '¤ðÐÊËÈiÍÎÏ++¦_¦Ì¯ÓßÔÒõÕµþÞÚÛÙýÝ¯´­±=¾¶§÷¸°¨·¹³²¦ ';
        CharVar[1] := 196;
        CharVar[2] := 197;
        CharVar[3] := 201;
        CharVar[4] := 242;
        CharVar[5] := 220;
        CharVar[6] := 186;
        CharVar[7] := 191;
        CharVar[8] := 188;
        CharVar[9] := 187;
        CharVar[10] := 193;
        CharVar[11] := 194;
        CharVar[12] := 192;
        CharVar[13] := 195;
        CharVar[14] := 202;
        CharVar[15] := 203;
        CharVar[16] := 200;
        CharVar[17] := 205;
        CharVar[18] := 206;
        CharVar[19] := 204;
        CharVar[20] := 175;
        CharVar[21] := 223;
        CharVar[22] := 213;
        CharVar[23] := 254;
        CharVar[24] := 218;
        CharVar[25] := 219;
        CharVar[26] := 217;
        CharVar[27] := 180;
        CharVar[28] := 177;
        CharVar[29] := 176;
        CharVar[30] := 185;
        CharVar[31] := 179;
        CharVar[32] := 178;
        AnsiStr := 'Ã³ÚÔõÓÕþÛÙÞ´¯ý' + FORMAT(CharVar[1]) + FORMAT(CharVar[2]) + FORMAT(CharVar[3]) + 'µã¶÷' + FORMAT(CharVar[4]);
        AnsiStr := AnsiStr + '¹¨ Í' + FORMAT(CharVar[5]) + '°úÏÎâßÝ¾·±Ð¬' + FORMAT(CharVar[6]) + FORMAT(CharVar[7]);
        AnsiStr := AnsiStr + '«¼¢' + FORMAT(CharVar[8]) + 'í½' + FORMAT(CharVar[9]) + '___ªª' + FORMAT(CharVar[10]) + FORMAT(CharVar[11]);
        AnsiStr := AnsiStr + FORMAT(CharVar[12]) + '®ªª++óÑ++--+-+Ò' + FORMAT(CharVar[13]) + '++--ª-+ñ­ð';
        AnsiStr := AnsiStr + FORMAT(CharVar[14]) + FORMAT(CharVar[15]) + FORMAT(CharVar[16]) + 'i' + FORMAT(CharVar[17]) + FORMAT(CharVar[18]);
        AnsiStr := AnsiStr + '¤++__ª' + FORMAT(CharVar[19]) + FORMAT(CharVar[20]) + 'Ë' + FORMAT(CharVar[21]) + 'ÈÊ§';
        AnsiStr := AnsiStr + FORMAT(CharVar[22]) + 'Á' + FORMAT(CharVar[23]) + 'Ì' + FORMAT(CharVar[24]) + FORMAT(CharVar[25]);
        AnsiStr := AnsiStr + FORMAT(CharVar[26]) + '²¦»' + FORMAT(CharVar[27]) + '¡' + FORMAT(CharVar[28]) + '=¥Âº¸©' + FORMAT(CharVar[29]);
        AnsiStr := AnsiStr + '¿À' + FORMAT(CharVar[30]) + FORMAT(CharVar[31]) + FORMAT(CharVar[32]) + '_ ';
    end;

    procedure modifie_accent(var texte: Text[150])
    var
        caractere: Char;
        tmp_texte: Text[1];
        i: Integer;
    begin
        texte := CONVERTSTR(texte, 'é', 'e');
        texte := CONVERTSTR(texte, 'è', 'e');
        texte := CONVERTSTR(texte, 'ê', 'e');
        texte := CONVERTSTR(texte, 'à', 'a');
        texte := CONVERTSTR(texte, 'ë', 'e');
        texte := CONVERTSTR(texte, 'ç', 'c');
        texte := CONVERTSTR(texte, 'ô', 'o');
        texte := CONVERTSTR(texte, 'â', 'a');
        //texte:=CONVERTSTR(texte,'Y',' ');
        //texte:=CONVERTSTR(texte,'Y',' ');

        texte := CONVERTSTR(texte, car_numeroLbl, '-');
        texte := CONVERTSTR(texte, car_apostropheLbl, ' ');
        texte := DELCHR(texte, '=', car_virguleLbl);
        texte := DELCHR(texte, '=', '(');
        texte := DELCHR(texte, '=', ')');

        IF STRPOS(texte, '@') = 0 THEN BEGIN
            texte := DELCHR(texte, '=', car_pointLbl);
            texte := DELCHR(texte, '=', car_tiretLbl);
        END;

        caractere := 255;
        tmp_texte[1] := caractere;
        texte := DELCHR(texte, '=', tmp_texte);

        FOR i := 1 TO 31 DO BEGIN
            caractere := i;
            tmp_texte[1] := caractere;
            texte := DELCHR(texte, '=', tmp_texte);
        END;

        texte := DELCHR(texte, '<', ' ');

        IF STRPOS(texte, '  ') <> 0 THEN
            REPEAT
                texte := DELSTR(texte, STRPOS(texte, '  '), 1);
            UNTIL STRPOS(texte, '  ') = 0;
    end;
}

