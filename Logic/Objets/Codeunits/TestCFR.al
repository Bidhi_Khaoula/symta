codeunit 50075 "Test CFR"
{
    Permissions = TableData "Warehouse Entry" = rm;
    trigger OnRun();
    var
       // lPurchaseHeader: Record "Purchase Header";
       // lTest: Text[50];
      //  lInteger: Integer;
      //  lDecimal: Decimal;
       // lShippingAgent: Record "Shipping Agent";
    begin
        //MESSAGE(FORMAT(INCSTR('TOTO187')));
        /*
        IF gSalesHeader.GET(gSalesHeader."Document Type"::"Return Order", 'RV-20000421') THEN
          BEGIN
            MESSAGE('retour n°%1 : %2',gSalesHeader."No.", gSalesHeader.Status);
            "gE-Commerce".ReleaseReturn(gSalesHeader);
            MESSAGE('retour n°%1 : %2',gSalesHeader."No.", gSalesHeader.Status);
          END;
        */
        /*
        IF gPICSCancelation.Create('EX20002490') THEN
          MESSAGE('Insertion PICS Cancellation OK')
        ELSE
          MESSAGE('Insertion PICS Cancellation KO');
        */
        //UpdateContactReclaculateGPS()
        //CalculateLongueur();
        //CalculateLargeur();
        //IF lPurchaseHeader.GET(lPurchaseHeader."Document Type"::Order, 'CA-12007711') THEN
        //MESSAGE('Fin : %1', lPurchaseHeader.GetOutstandingAmount());
        //lShippingAgent.GET('20CH');
        //lShippingAgent.ShowComment('', '', TRUE);
        //updatePurchaseLineQty();

        //UpdateMargeSalesLine();

        //Update_ItemVendor_RefActive();
        //Update_7312_from_39();

        //"Update_7354.50020"();
        Init_Retour_InfoTransport();
        Init_ShipmentMethodCode_7320();

        MESSAGE('Traitement terminé');

    end;

    LOCAL PROCEDURE Init_ShipmentMethodCode_7320();
    VAR
        lWarehouseShipmentHeader: Record 7320;
        lSalesHeader: Record 36;
    BEGIN

        IF lWarehouseShipmentHeader.FINDSET() THEN BEGIN
            IF (CONFIRM(STRSUBSTNO('MAJ %1 PICS ?', lWarehouseShipmentHeader.COUNT()), FALSE)) THEN BEGIN
                REPEAT
                    IF lSalesHeader.GET(lSalesHeader."Document Type"::Order, lWarehouseShipmentHeader."Source No.") THEN BEGIN
                        lWarehouseShipmentHeader."Shipment Method Code" := lSalesHeader."Shipment Method Code";
                        lWarehouseShipmentHeader.MODIFY(FALSE);
                    END;
                UNTIL lWarehouseShipmentHeader.NEXT() = 0;
            END;
        END;
    END;

    LOCAL PROCEDURE Init_Retour_InfoTransport();
    VAR
        lSalesHeader: Record 36;
    BEGIN

        lSalesHeader.SETRANGE("Document Type", lSalesHeader."Document Type"::"Return Order");
        lSalesHeader.SETFILTER("External Document No.", '<>%1', '');
        IF lSalesHeader.FINDSET() THEN BEGIN
            IF (CONFIRM(STRSUBSTNO('MAJ %1 retours ?', lSalesHeader.COUNT()), FALSE)) THEN BEGIN
                REPEAT
                    lSalesHeader."Transport Information" := lSalesHeader."External Document No.";
                    lSalesHeader."External Document No." := '';
                    lSalesHeader.MODIFY(FALSE);
                UNTIL lSalesHeader.NEXT() = 0;
            END;
        END;
    END;

    LOCAL PROCEDURE "Update_7354.50020"();
    VAR
        lBin: Record 7354;
        lVendor: Record 23;
        lCompteur: Integer;
    BEGIN
        lCompteur := lBin.COUNT();

        lBin.SETFILTER(Code, '%1', 'RET-*');
        IF lBin.FINDSET() THEN BEGIN
            IF (CONFIRM(STRSUBSTNO('MAJ %1/%2 emplacements ?', lBin.COUNT(), lCompteur), FALSE)) THEN BEGIN
                lCompteur := 0;
                REPEAT
                    IF lVendor.GET(COPYSTR(lBin.Code, 5)) THEN BEGIN
                        lBin.VALIDATE("Return Vendor Name", lVendor.Name);
                        lBin.MODIFY(FALSE);
                        lCompteur += 1;
                    END;
                UNTIL lBin.NEXT() = 0;
                MESSAGE('%1 emplacement maj', lCompteur);
            END;
        END;
    END;

    LOCAL PROCEDURE Update_7312_from_39();
    VAR
        lPurchaseLine: Record 39;
        lWarehouseEntry: Record 7312;
    BEGIN
        lPurchaseLine.SETRANGE("Document Type", lPurchaseLine."Document Type"::"Return Order");
        lPurchaseLine.SETFILTER("Bin Code", '%1', 'RET*');
        IF lPurchaseLine.FINDSET() THEN
            REPEAT
                lWarehouseEntry.SETRANGE("Journal Template Name", 'RECLASS');
                lWarehouseEntry.SETRANGE("Journal Batch Name", 'DEFAUT');
                lWarehouseEntry.SETRANGE("Source No.", lPurchaseLine."Document No.");
                lWarehouseEntry.SETRANGE("Location Code", lPurchaseLine."Location Code");
                lWarehouseEntry.SETRANGE("Bin Code", lPurchaseLine."Bin Code");
                lWarehouseEntry.SETRANGE("Item No.", lPurchaseLine."No.");
                lWarehouseEntry.SETRANGE("Variant Code", lPurchaseLine."Variant Code");
                lWarehouseEntry.SETRANGE("Unit of Measure Code", lPurchaseLine."Unit of Measure Code");
                lWarehouseEntry.SETRANGE(Quantity, lPurchaseLine."Quantity (Base)");
                IF lWarehouseEntry.COUNT() = 1 THEN BEGIN
                    IF lWarehouseEntry.FINDFIRST() THEN BEGIN
                        lWarehouseEntry."Source Line No." := lPurchaseLine."Line No.";
                        lWarehouseEntry.Commentaire := STRSUBSTNO('Mvt gestion retour achat : %1', FORMAT(lPurchaseLine."Demande retour"));
                        lWarehouseEntry.MODIFY();
                    END;
                END;

            UNTIL lPurchaseLine.NEXT() = 0;
    END;

    LOCAL PROCEDURE Update_ItemVendor_RefActive();
    VAR
        lItem: Record 27;
        lItemVendor: Record 99;
    BEGIN
        // CFR - R‚gie le 17/01/2024 : mise … jour catalogue fournisseur
        //lItem.SETRANGE("No.", 'SP058787');
        IF lItem.FINDSET() THEN
            REPEAT
                CLEAR(lItemVendor);
                lItemVendor.SETCURRENTKEY("Item No.", "Variant Code", "Vendor No.");
                lItemVendor.SETRANGE("Item No.", lItem."No.");
                lItemVendor.MODIFYALL("Ref. Active", lItem."No. 2");
            UNTIL lItem.NEXT() = 0;
    END;

    var
       // gSalesHeader: Record "Sales Header";
       // "gE-Commerce": Codeunit "E-Commerce";
      //  gPICSCancelation: Record "PICS Cancelation";

    local procedure UpdateMargeSalesLine();
    var
        lSalesLine: Record "Sales Line";
    begin
        lSalesLine.SETFILTER("Document No.", '%1', '*-23*');
        lSalesLine.SETRANGE(Type, lSalesLine.Type::Item);
        lSalesLine.SETFILTER("Document Type", '%1|%2', lSalesLine."Document Type"::Order, lSalesLine."Document Type"::Quote);
        IF NOT CONFIRM(STRSUBSTNO('Calculer la marge sur %1 lignes ?\%2', lSalesLine.COUNT, lSalesLine.GETFILTERS), FALSE) THEN EXIT;
        IF lSalesLine.FINDSET() THEN
            REPEAT
                lSalesLine.CalcMarge();
                lSalesLine.MODIFY(FALSE);
                COMMIT();
            UNTIL lSalesLine.NEXT() = 0;
    end;

    local procedure updatePurchaseLineQty();
    var
        lPurchaseLine: Record "Purchase Line";
           begin
        IF lPurchaseLine.GET(lPurchaseLine."Document Type"::Order, 'CA-22001888', 100000) THEN BEGIN
            lPurchaseLine.Quantity := 999;
            lPurchaseLine."Quantity (Base)" := 999;
            lPurchaseLine.InitOutstanding();
            lPurchaseLine.InitQtyToReceive();
            lPurchaseLine.MODIFY(FALSE);
            MESSAGE('END');
        END;
    end;

    local procedure UpdateContactReclaculateGPS();
    var
        lContact: Record Contact;
    begin
        MESSAGE('Nombre de contacts total : %1', lContact.COUNT);
        lContact.SETRANGE("Change Coordinate", TRUE);
        MESSAGE('Nombre de contacts concernés : %1', lContact.COUNT);
        lContact.MODIFYALL("Change Coordinate", FALSE);
        MESSAGE('Nombre de contacts restant : %1', lContact.COUNT);
        ERROR('Annulation');
    end;

    local procedure CalculateLongueur();
    var
        lItem: Record Item;
        lUnitofMeasure: Record "Item Unit of Measure";
        lCompteurAvecBUOM: Integer;
        lCompteurAvecEcart: Integer;
        lCompteurDe27Vers5404: Integer;
        lCompteurDe5404Vers27: Integer;
    begin
        lItem.RESET();
        lCompteurAvecBUOM := 0;
        lCompteurAvecEcart := 0;
        lCompteurDe27Vers5404 := 0;
        lCompteurDe5404Vers27 := 0;
        IF lItem.FINDSET() THEN
            REPEAT
                IF lUnitofMeasure.GET(lItem."No.", lItem."Base Unit of Measure") THEN BEGIN
                    lCompteurAvecBUOM += 1;
                    IF (lItem.Longueur <> lUnitofMeasure.Length) AND (lItem.Longueur <> 0) AND (lUnitofMeasure.Length <> 0) THEN BEGIN
                        lCompteurAvecEcart += 1;
                        lUnitofMeasure.VALIDATE(Length, lItem.Longueur);
                        //lUnitofMeasure.MODIFY(TRUE);
                    END;
                    IF (lItem.Longueur <> 0) AND (lUnitofMeasure.Length = 0) THEN BEGIN
                        lCompteurDe27Vers5404 += 1;
                        lUnitofMeasure.VALIDATE(Length, lItem.Longueur);
                        //lUnitofMeasure.MODIFY(TRUE);
                    END;
                    IF (lItem.Longueur = 0) AND (lUnitofMeasure.Length <> 0) THEN BEGIN
                        lCompteurDe5404Vers27 += 1;
                        lItem.VALIDATE(Longueur, lUnitofMeasure.Length);
                        //lItem.MODIFY(TRUE);
                    END;
                END;
            UNTIL lItem.NEXT() = 0;

        MESSAGE('Nombre d''article : %1\ - avec BUOM : %2\ - avec écart : %3\ - de 27 > 5404 : %4\ - de 5404 > 27 : %5',
                lItem.COUNT(), lCompteurAvecBUOM, lCompteurAvecEcart, lCompteurDe27Vers5404, lCompteurDe5404Vers27);
    end;

    local procedure CalculateLargeur();
    var
        lItem: Record Item;
        lUnitofMeasure: Record "Item Unit of Measure";
        lCompteurAvecBUOM: Integer;
        lCompteurAvecEcart: Integer;
        lCompteurDe27Vers5404: Integer;
        lCompteurDe5404Vers27: Integer;
    begin
        lItem.RESET();
        lCompteurAvecBUOM := 0;
        lCompteurAvecEcart := 0;
        lCompteurDe27Vers5404 := 0;
        lCompteurDe5404Vers27 := 0;
        IF lItem.FINDSET() THEN
            REPEAT
                IF lUnitofMeasure.GET(lItem."No.", lItem."Base Unit of Measure") THEN BEGIN
                    lCompteurAvecBUOM += 1;
                    IF (lItem.Largeur <> lUnitofMeasure.Width) AND (lItem.Largeur <> 0) AND (lUnitofMeasure.Width <> 0) THEN BEGIN
                        lCompteurAvecEcart += 1;
                        lUnitofMeasure.VALIDATE(Width, lItem.Largeur);
                        //lUnitofMeasure.MODIFY(TRUE);
                    END;
                    IF (lItem.Largeur <> 0) AND (lUnitofMeasure.Width = 0) THEN BEGIN
                        lCompteurDe27Vers5404 += 1;
                        lUnitofMeasure.VALIDATE(Width, lItem.Largeur);
                        //lUnitofMeasure.MODIFY(TRUE);
                    END;
                    IF (lItem.Largeur = 0) AND (lUnitofMeasure.Width <> 0) THEN BEGIN
                        lCompteurDe5404Vers27 += 1;
                        lItem.VALIDATE(Largeur, lUnitofMeasure.Width);
                        //lItem.MODIFY(TRUE);
                    END;
                END;
            UNTIL lItem.NEXT() = 0;

        MESSAGE('Nombre d''article : %1\ - avec BUOM : %2\ - avec écart : %3\ - de 27 > 5404 : %4\ - de 5404 > 27 : %5',
                lItem.COUNT(), lCompteurAvecBUOM, lCompteurAvecEcart, lCompteurDe27Vers5404, lCompteurDe5404Vers27);
    end;
}

