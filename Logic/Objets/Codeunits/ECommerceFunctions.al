codeunit 50058 "ECommerce Functions"
{
    trigger OnRun()
    begin
    end;

    procedure GetNbOfWebQuote(varExport: Boolean) int_rtn: Integer
    var
        recL_SalesQuote: Record "Sales Header";
    begin
        // MCO Le 31-03-2015 => Régie
        int_rtn := 0;

        recL_SalesQuote.RESET();
        // FBO le 05-04-2017 => FE20170324
        recL_SalesQuote.SETRANGE(recL_SalesQuote."Export Salesperson", varExport);
        // FIN FBO le 05-04-2017

        recL_SalesQuote.SETRANGE(recL_SalesQuote."Document Type", recL_SalesQuote."Document Type"::Quote);
        recL_SalesQuote.SETRANGE("Devis Web", TRUE);
        int_rtn := recL_SalesQuote.COUNT;
    end;
}

