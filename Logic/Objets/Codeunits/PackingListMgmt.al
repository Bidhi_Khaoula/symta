codeunit 50049 "Packing List Mgmt"
{
    trigger OnRun()
    begin
        IF GUIALLOWED THEN
            MESSAGE('Hello World');
    end;

    var
      
        gText05Lbl: Label 'Souhaitez vous réellement effacer toutes les informations de la liste de colisage ?';
        gText06Lbl: Label 'L''impression n''''est pas possible tant que l''expédition vente enregistrée n''est pas créée';
        gText07Lbl: Label 'Souhaitez vous réellement supprimer l''affectation à la liste de colisage ?';
        gText08Lbl: Label 'La liste de colisage %1 est vide. \Souhaitez vous la supprimer ?' , Comment = '%1 = Liste de Package ';

    procedure RazPackingList(var pPackingListHeader: Record "Packing List Header")
    var
        lPackingListLine: Record "Packing List Line";
        lPackingListPackage: Record "Packing List Package";
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        // Fonction V2
        pPackingListHeader.CheckHeaderStatusOpen();

        // CFR > GUIALLOWED pour utilisation via WebServices WIIO
        IF GUIALLOWED THEN 
            IF NOT CONFIRM(gText05Lbl) THEN
                EXIT;   

        // suppression des lignes
        lPackingListLine.SETRANGE("Packing List No.", pPackingListHeader."No.");
        IF lPackingListLine.FINDSET() THEN
            lPackingListLine.DELETEALL(TRUE);
        // suppression des colis
        lPackingListPackage.SETRANGE("Packing List No.", pPackingListHeader."No.");
        IF lPackingListPackage.FINDSET() THEN
            lPackingListPackage.DELETEALL(TRUE);

        // Création à partir des expéditions entrepôt
        lWarehouseShipmentHeader.SETRANGE("Packing List No.", pPackingListHeader."No.");
        IF lWarehouseShipmentHeader.FINDSET() THEN
            REPEAT
                InitPackingListFromWhseShipment(pPackingListHeader, lWarehouseShipmentHeader);
            UNTIL lWarehouseShipmentHeader.NEXT() = 0;
    end;

    procedure RefreshPackingList(var pPackingListHeader: Record "Packing List Header")
    var
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
    begin
        // Fonction V2
        // Création à partir des expéditions entrepôt
        lWarehouseShipmentHeader.SETRANGE("Packing List No.", pPackingListHeader."No.");
        IF lWarehouseShipmentHeader.FINDSET() THEN
            REPEAT
                InitPackingListFromWhseShipment(pPackingListHeader, lWarehouseShipmentHeader);
            UNTIL lWarehouseShipmentHeader.NEXT() = 0;
    end;

    procedure getPackingListForWhseShipment(var pPackingListHeader: Record "Packing List Header"; pWhseShipmentNo: Code[20]): Boolean
    var
        lWhseShipmentHeader: Record "Warehouse Shipment Header";
        lPackingListSelection: Record "Packing List Header";
          DemandeTransporteur: Record "Demande transporteur";
        Rec_WhseShipmentLine: Record "Warehouse Shipment Line";
        SalesHeader: Record "Sales Header";
        lPageSelection: Page "Packing List List";
        lPackingListQst: Label 'Créer nouveau document,Sélectionner document existant (%1)';
        lSelection: Integer;
        lDefaultNumber: Integer;
      
    begin
        // Fonction V2
        // L'expédition entrepôt doit exister...
        CLEAR(lWhseShipmentHeader);
        IF NOT lWhseShipmentHeader.GET(pWhseShipmentNo) THEN
            EXIT(FALSE);
        // ... et doit être à l'origine d'une demande client
        IF lWhseShipmentHeader."Destination Type" <> lWhseShipmentHeader."Destination Type"::Customer THEN
            ERROR('Packing List impossible pour de type de document');

        // CFR > GUIALLOWED pour utilisation via WebServices WIIO
        IF NOT GUIALLOWED THEN 
            ERROR('CU50049 - getPackingListForWhseShipment() appel impossible');
              // Création de la Packing List si elle n'existe pas...
        IF NOT pPackingListHeader.GET(lWhseShipmentHeader."Packing List No.") THEN BEGIN
            // ici question : voulez vous créer ou sélectionner ?

            //CFR le 21/09/2021 => Régie : optimisation création/sélection PL = déplacement du code
            lPackingListSelection.FILTERGROUP(2);
            // Que les PL non enregistrées
            lPackingListSelection.SETFILTER("Header Status", '<>%1', lPackingListSelection."Header Status"::Enregistré);
            // Que les PL dont le client est le même
            lPackingListSelection.SETFILTER("Sell-to Customer No.", '%1|%2', '', lWhseShipmentHeader."Destination No.");
            lPackingListSelection.FILTERGROUP(0);
            IF lPackingListSelection.ISEMPTY() THEN
                lDefaultNumber := 1
            ELSE
                lDefaultNumber := 2;
            //FIN CFR le 21/09/2021

            lSelection := STRMENU(STRSUBSTNO(lPackingListQst, lPackingListSelection.COUNT()), lDefaultNumber);
            IF lSelection = 0 THEN
                EXIT(FALSE);
            IF lSelection = 1 THEN BEGIN
                pPackingListHeader.INSERT(TRUE);
                // GRI le 31/03/2023
                IF CONFIRM('Voulez-vous créer une demande de transport ?') THEN BEGIN
                    DemandeTransporteur.INIT();
                    DemandeTransporteur.VALIDATE(Type, DemandeTransporteur.Type::Expédition);
                    DemandeTransporteur.VALIDATE("Packing List No.", pPackingListHeader."No.");
                    Rec_WhseShipmentLine.SETRANGE("No.", lWhseShipmentHeader."No.");
                    IF Rec_WhseShipmentLine.FINDFIRST() THEN
                        // on r‚cupŠre les infos de la commande
                        IF SalesHeader.GET(Rec_WhseShipmentLine."Source Subtype", Rec_WhseShipmentLine."Source No.") THEN BEGIN
                            DemandeTransporteur.VALIDATE("Customer No.", SalesHeader."Sell-to Customer No.");
                            DemandeTransporteur."Ship-to Name" := SalesHeader."Ship-to Name";
                            DemandeTransporteur."Ship-to Name 2" := SalesHeader."Ship-to Name 2";
                            DemandeTransporteur."Ship-to Address" := SalesHeader."Ship-to Address";
                            DemandeTransporteur."Ship-to Address 2" := SalesHeader."Ship-to Address 2";
                            DemandeTransporteur."Ship-to City" := SalesHeader."Ship-to City";
                            DemandeTransporteur."Ship-to Post Code" := SalesHeader."Ship-to Post Code";
                            DemandeTransporteur."Ship-to Country/Region Code" := SalesHeader."Ship-to Country/Region Code";
                        END;
                    DemandeTransporteur.INSERT(TRUE);
                END;
            END;
            IF lSelection = 2 THEN BEGIN
                CLEAR(lPageSelection);
                lPageSelection.SETTABLEVIEW(lPackingListSelection);
                lPageSelection.LOOKUPMODE := TRUE;
                IF NOT (lPageSelection.RUNMODAL() = ACTION::LookupOK) THEN 
                    EXIT(FALSE);
                lPageSelection.GETRECORD(lPackingListSelection);
                IF NOT pPackingListHeader.GET(lPackingListSelection."No.") THEN
                    EXIT(FALSE);
            END;

            //pPackingListHeader.insert(true);
            // ... et mise à jour de l'expédition entrepôt
            lWhseShipmentHeader."Packing List No." := pPackingListHeader."No.";
            lWhseShipmentHeader.MODIFY(FALSE);
            // on est en création : on ajoute tout
            InitPackingListFromWhseShipment(pPackingListHeader, lWhseShipmentHeader);
        END;
        EXIT(TRUE);
    end;

    procedure InitPackingListFromWhseShipment(var pPackingListHeader: Record "Packing List Header"; pWhseShipmentHeader: Record "Warehouse Shipment Header")
    var
        lWhseShipmentLine: Record "Warehouse Shipment Line";
        lPackingListLine: Record "Packing List Line";
          begin
        // Fonction V2
        IF pWhseShipmentHeader."Destination Type" = pWhseShipmentHeader."Destination Type"::Customer THEN BEGIN
            pPackingListHeader."Sell-to Customer No." := pWhseShipmentHeader."Destination No.";
            pPackingListHeader.MODIFY(FALSE);
        END;
        // Recherche de toutes les lignes d'expéditions
        CLEAR(lWhseShipmentLine);
        lWhseShipmentLine.SETCURRENTKEY("No.", "Sorting Sequence No.");
        lWhseShipmentLine.SETRANGE("No.", pWhseShipmentHeader."No.");
        //lWhseShipmentLine.SETFILTER("Qty. to Ship", '>%1', 0);
        IF lWhseShipmentLine.FINDSET() THEN
            REPEAT
                CLEAR(lPackingListLine);
                lPackingListLine.SETRANGE("Packing List No.", pPackingListHeader."No.");
                lPackingListLine.SETRANGE("Whse. Shipment No.", pWhseShipmentHeader."No.");
                lPackingListLine.SETRANGE("Whse. Shipment Line No.", lWhseShipmentLine."Line No.");
                // la ligne a déja été insérée... et peut avoir été dupliquée
                IF (lPackingListLine.FINDSET()) THEN BEGIN
                    REPEAT
                        // MAJ des quantités à expédier si besoin (oubli de la préparation par exemple...)
                        lPackingListLine.VALIDATE("Quantity to Ship", lWhseShipmentLine."Qty. to Ship");
                        lPackingListLine.MODIFY(TRUE);
                    UNTIL lPackingListLine.NEXT() = 0;
                END
                ELSE BEGIN
                    // c'est une nouvelle ligne que j'insère
                    lPackingListLine.VALIDATE("Packing List No.", pPackingListHeader."No.");
                    lPackingListLine.VALIDATE("Item No.", lWhseShipmentLine."Item No.");
                    lPackingListLine.VALIDATE(Description, lWhseShipmentLine.Description);
                    lPackingListLine.VALIDATE("Description 2", lWhseShipmentLine."Description 2");
                    lPackingListLine.VALIDATE("Unit Weight", lWhseShipmentLine.Weight);
                    //pas de validate à l'insert
                    lPackingListLine."Quantity to Ship" := lWhseShipmentLine."Qty. to Ship";
                    lPackingListLine."Line Quantity" := lWhseShipmentLine."Qty. to Ship";

                    lPackingListLine.VALIDATE("Whse. Shipment No.", pWhseShipmentHeader."No.");
                    lPackingListLine.VALIDATE("Whse. Shipment Line No.", lWhseShipmentLine."Line No.");
                    lPackingListLine.VALIDATE("WSL Sorting Sequence No.", lWhseShipmentLine."Sorting Sequence No.");

                    lPackingListLine.VALIDATE("Source Type", lWhseShipmentLine."Source Type");
                    lPackingListLine.VALIDATE("Source Subtype", lWhseShipmentLine."Source Subtype");
                    lPackingListLine.VALIDATE("Source No.", lWhseShipmentLine."Source No.");
                    lPackingListLine.VALIDATE("Source Line No.", lWhseShipmentLine."Source Line No.");
                    lPackingListLine.VALIDATE("Source Document", lWhseShipmentLine."Source Document");
                    //>>SFD20201005
                    lPackingListLine."Bin Code" := lWhseShipmentLine."Bin Code";
                    lPackingListLine."Zone Code" := lWhseShipmentLine."Zone Code";
                    //<<SFD20201005
                    IF lPackingListLine.INSERT(TRUE) THEN;
                END;
            UNTIL lWhseShipmentLine.NEXT() = 0
    end;

    procedure LineDuplicate(pPackingListLine: Record "Packing List Line")
    var
        lPackingListLineTmp: Record "Packing List Line";
        lWhseShipmentLine: Record "Warehouse Shipment Line";
        lQuantiteReste: Decimal;
        lNumeroLigne: Integer;
    begin
        // Fonction V2
        // Duplique ssi il y a quelque chose à dupliquer
        lPackingListLineTmp.SETRANGE("Packing List No.", pPackingListLine."Packing List No.");
        lPackingListLineTmp.SETRANGE("Whse. Shipment No.", pPackingListLine."Whse. Shipment No.");
        lPackingListLineTmp.SETRANGE("Whse. Shipment Line No.", pPackingListLine."Whse. Shipment Line No.");
        IF lPackingListLineTmp.ISEMPTY() THEN EXIT;

        // Calcule la quantité restant à saisir
        lPackingListLineTmp.CALCSUMS("Line Quantity");
        lWhseShipmentLine.GET(pPackingListLine."Whse. Shipment No.", pPackingListLine."Whse. Shipment Line No.");
        lQuantiteReste := lWhseShipmentLine."Qty. to Ship" - lPackingListLineTmp."Line Quantity";

        // Si la quantité restant à saisir est nulle : pas de ligne
        IF (lQuantiteReste <= 0) THEN
            //lQuantiteReste := 0;//Error(gText03Lbl);
            EXIT;

        // Calcule le nouveau n° de ligne
        CLEAR(lPackingListLineTmp);
        lPackingListLineTmp.SETRANGE("Packing List No.", pPackingListLine."Packing List No.");
        lPackingListLineTmp := pPackingListLine;
        IF (lPackingListLineTmp.NEXT() <> 0) THEN
            lNumeroLigne := (lPackingListLineTmp."Line No." - pPackingListLine."Line No.") / 2
        ELSE
            lNumeroLigne := 10000;
        lNumeroLigne := pPackingListLine."Line No." + ROUND(lNumeroLigne, 1, '=');

        //insère une nouvelle ligne
        lPackingListLineTmp := pPackingListLine;
        lPackingListLineTmp."Line No." := lNumeroLigne;
        lPackingListLineTmp."Package No." := '';
        lPackingListLineTmp.VALIDATE("Line Quantity", lQuantiteReste);
        lPackingListLineTmp.INSERT(TRUE);
    end;

    procedure PrintPackingListItem(pPackingListHeaderNo: Code[20]; pRequestWindows: Boolean; pType: Code[10])
    var
        lPackingListHeader: Record "Packing List Header";
        lSalesShipmentHeader: Record "Sales Shipment Header";
        lWarehouseSetup: Record "Warehouse Setup";
        lIDReport: Integer;
    begin
        // Fonction V2
        IF NOT lPackingListHeader.GET(pPackingListHeaderNo) THEN
            EXIT;

        lWarehouseSetup.GET();
        IF pType = 'ARTICLE' THEN BEGIN
            IF lWarehouseSetup."Item PL Report No." > 0 THEN
                lIDReport := lWarehouseSetup."Item PL Report No."
            ELSE
                lIDReport := 50030;
        END;
        IF pType = 'COLIS' THEN BEGIN
            IF lWarehouseSetup."Package PL Report No." > 0 THEN
                lIDReport := lWarehouseSetup."Package PL Report No."
            ELSE
                lIDReport := 50031;
        END;

        lPackingListHeader.CALCFIELDS("First Posted Shipment No.");
        // Attention :SetRange() obligatoire et non pas GET()
        lSalesShipmentHeader.SETRANGE("No.", lPackingListHeader."First Posted Shipment No.");
        IF lSalesShipmentHeader.FINDSET() THEN
            REPORT.RUN(lIDReport, pRequestWindows, TRUE, lSalesShipmentHeader)
        ELSE
            MESSAGE(gText06Lbl);
    end;

    procedure PackingListPostHeader(pWhseShipmentHeaderNo: Code[20]; pPackingListeHeaderNo: Code[20])
    var
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
        lPackingListHeader: Record "Packing List Header";
    begin
        // --------------
        // Statut Packing List et vide expédition entrepôt
        // --------------
        IF lPackingListHeader.GET(pPackingListeHeaderNo) THEN
            lPackingListHeader.RefreshHeaderStatus();

        IF lWarehouseShipmentHeader.GET(pWhseShipmentHeaderNo) THEN BEGIN
            // Vide la référence de l'expédition entrepôt
            lWarehouseShipmentHeader.VALIDATE("Packing List No.", '');
            lWarehouseShipmentHeader.MODIFY();
        END;
    end;

    procedure PackingListPostLine(pPostedWhseShipmentLine: Record "Posted Whse. Shipment Line"; pWarehouseShipmentLine: Record "Warehouse Shipment Line"; pSalesShipmentHeaderNo: Code[20])
    var
        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
        lPackinglistLine: Record "Packing List Line";
    begin
        // --------------
        // Lignes de la Packing List
        // --------------
        IF NOT lWarehouseShipmentHeader.GET(pWarehouseShipmentLine."No.") THEN
            EXIT;

        lPackinglistLine.RESET();
        lPackinglistLine.SETRANGE("Packing List No.", lWarehouseShipmentHeader."Packing List No.");
        lPackinglistLine.SETRANGE("Whse. Shipment No.", pWarehouseShipmentLine."No.");
        lPackinglistLine.SETRANGE("Whse. Shipment Line No.", pWarehouseShipmentLine."Line No.");
        IF lPackinglistLine.FINDSET() THEN
            // mise à jour des lignes
            REPEAT
                // met à jour les infos liées à l'expédition entrepôt enregistrée
                lPackinglistLine.VALIDATE("Posted Whse. Shipment No.", pPostedWhseShipmentLine."No.");
                lPackinglistLine.VALIDATE("Posted Whse. Shipment Line No.", pPostedWhseShipmentLine."Line No.");

                // met à jour les infos liées à l'expédition vente enregistrée
                lPackinglistLine.VALIDATE("Posted Source No.", pSalesShipmentHeaderNo);
                lPackinglistLine.VALIDATE("Posted Source Document", lPackinglistLine."Posted Source Document"::"Posted Shipment");

                // cloture chaque ligne
                lPackinglistLine.VALIDATE("Line Status", lPackinglistLine."Line Status"::Close);
                lPackinglistLine.MODIFY(TRUE);

            UNTIL lPackinglistLine.NEXT() = 0;
    end;

    procedure PrintPackingListTemporary(pPackingListHeaderNo: Code[20]; pRequestWindows: Boolean)
    var
        lPackingListHeader: Record "Packing List Header";
          lIDReport: Integer;
    begin
        // Fonction V2
        IF NOT lPackingListHeader.GET(pPackingListHeaderNo) THEN
            EXIT;

        lIDReport := 50032;
        lPackingListHeader.SETRANGE("No.", pPackingListHeaderNo);
        REPORT.RUN(lIDReport, pRequestWindows, TRUE, lPackingListHeader);
    end;

    procedure delWhseShipmentFromPackingList(pWhseShipmentNo: Code[20]): Boolean
    var
        lWhseShipmentHeader: Record "Warehouse Shipment Header";
        lPackingListHeader: Record "Packing List Header";
        lPackingListLine: Record "Packing List Line";
    begin
        // Fonction V2
        IF NOT lWhseShipmentHeader.GET(pWhseShipmentNo) THEN
            EXIT(FALSE);
        IF NOT lPackingListHeader.GET(lWhseShipmentHeader."Packing List No.") THEN
            EXIT(FALSE);
        lPackingListHeader.CheckHeaderStatusOpen();

        // CFR > GUIALLOWED pour utilisation via WebServices WIIO
        IF GUIALLOWED THEN BEGIN
            IF NOT CONFIRM(gText07Lbl) THEN
                EXIT(FALSE);
        END;

        // suppression des lignes
        lPackingListLine.SETRANGE("Packing List No.", lPackingListHeader."No.");
        lPackingListLine.SETRANGE("Whse. Shipment No.", lWhseShipmentHeader."No.");
        IF lPackingListLine.FINDSET() THEN
            lPackingListLine.DELETEALL(TRUE);

        //CFR le 21/09/2021 => Régie : Correction bug suppression : inutile s'est fait dans le DELETEALL précédent
        // maj de l'en-tête
        //lWhseShipmentHeader."Packing List No." := '';
        //lWhseShipmentHeader.MODIFY(TRUE);
        // FIN CFR le 21/09/2021

        // reste t-il des lignes ?
        lPackingListLine.SETRANGE("Whse. Shipment No.", '');
        IF NOT lPackingListLine.FINDSET() THEN BEGIN
            IF GUIALLOWED THEN BEGIN
                IF NOT CONFIRM(STRSUBSTNO(gText08Lbl, lPackingListHeader."No.")) THEN
                    EXIT(TRUE);
            END;
            lPackingListHeader.DELETE(TRUE);
        END;

    end;
}

