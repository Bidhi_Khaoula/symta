codeunit 50019 "Génération AR Edi"
{

    trigger OnRun()
    begin
    end;

    procedure GenerateEDIOrders(SalesHeader: Record "Sales Header")
    var
        EdiHeader: Record "EDI Ordes Header";
        SalesLine: Record "Sales Line";
        Cust: Record Customer;
        Item: Record Item;
        Param: Record "Generals Parameters";
        FichierEdi: File;
        Ligne: Text[1024];
    begin
        IF NOT Cust.GET(SalesHeader."Sell-to Customer No.") THEN
            EXIT;

        IF (Cust."Code AR EDI" = '') OR (Cust."Date Début Export AR EDI" > WORKDATE()) THEN
            EXIT;

        Param.GET('FIC_EDI', Cust."Code AR EDI");
        Param.TESTFIELD(Param.LongDescription);

        FichierEdi.TEXTMODE(TRUE);
        FichierEdi.WRITEMODE(TRUE);

        IF FILE.EXISTS(Param.LongDescription) THEN
            FichierEdi.OPEN(Param.LongDescription)
        ELSE
            FichierEdi.CREATE(Param.LongDescription);

        FichierEdi.SEEK(FichierEdi.LEN);

        IF NOT FILE.EXISTS(Param.LongDescription) THEN
            FichierEdi.CREATE(Param.LongDescription)
        ELSE
            EdiHeader.GET('ORDERS', SalesHeader."EDI Document No.");


        Ligne := 'ENR;';
        Ligne += SalesHeader."No." + ';';
        Ligne += FormatDate(SalesHeader."Order Date") + ';';
        Ligne += Cust."Customer EDI Code" + ';';
        Ligne += SalesHeader."EDI Document No." + ';';
        Ligne += FormatDate(EdiHeader."EDI Document Date") + ';';
        IF SalesHeader."Requested Delivery Date" <> 0D THEN
            Ligne += FormatDate(SalesHeader."Requested Delivery Date") + ';'
        ELSE
            Ligne += FormatDate(SalesHeader."Order Date") + ';';

        FichierEdi.WRITE(Ligne);

        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
        SalesLine.SETRANGE("Document No.", SalesHeader."No.");
        SalesLine.SETRANGE(Type, SalesLine.Type::Item);
        IF SalesLine.FindSet() THEN
            REPEAT

                CLEAR(Item);
                IF NOT Item.GET(SalesLine."No.") THEN;

                Ligne := 'LIG;';
                Ligne += FormatInteger(SalesLine."Line No.") + ';';
                Ligne += Item."Gencod EAN13" + ';';
                IF SalesLine."Requested Delivery Date" <> 0D THEN
                    Ligne += FormatDate(SalesLine."Requested Delivery Date") + ';'
                ELSE
                    Ligne += FormatDate(SalesHeader."Order Date") + ';';
                Ligne += FormatDecimal(SalesLine."Net Unit Price") + ';';
                Ligne += '' + ';';
                Ligne += FormatDecimal(SalesLine.Quantity) + ';';

                FichierEdi.WRITE(Ligne);
            UNTIL SalesLine.NEXT() = 0;

        FichierEdi.CLOSE();

        SalesHeader.VALIDATE("EDI AR Generate", TRUE);
        SalesHeader.VALIDATE("EDI AR Date", WORKDATE());
        SalesHeader.VALIDATE("EDI AR Time", TIME);
        SalesHeader.VALIDATE("EDI AR User", USERID);
        SalesHeader.MODIFY();
    end;

    procedure FormatDate(Value: Date): Text[10]
    begin
        EXIT(FORMAT(Value, 0, '<Year4>-<Month,2>-<Day,2>'));
    end;

    procedure FormatDecimal(Value: Decimal): Text[30]
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals><Comma,,>'));
    end;

    procedure FormatInteger(Value: Integer): Text[30]
    begin
        EXIT(FORMAT(Value));
    end;
}

