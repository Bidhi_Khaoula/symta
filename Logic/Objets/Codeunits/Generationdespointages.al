codeunit 50033 "Generation des pointages"
{

    trigger OnRun()
    begin
    end;

    procedure CreationFeuilleReclassement(_pNoFusion: Code[20])
    var
        LigneFeuille: Record "Item Journal Line";
        FeuilleReclassement: Record "Item Journal Batch";
        CurrTemplateName: Code[20];
        CurrWorksheetName: Code[20];
        LigneFusion: Record "Saisis Réception Magasin";
    begin
        CurrTemplateName := 'RECLASS';

        CurrWorksheetName := _pNoFusion;

        IF NOT FeuilleReclassement.GET(CurrTemplateName, CurrWorksheetName) THEN BEGIN
            CLEAR(FeuilleReclassement);
            FeuilleReclassement.VALIDATE("Journal Template Name", CurrTemplateName);
            FeuilleReclassement.VALIDATE(Name, CurrWorksheetName);
            FeuilleReclassement.VALIDATE(Description, 'Rangement : ' + CurrWorksheetName);
            FeuilleReclassement.VALIDATE("Template Type", FeuilleReclassement."Template Type"::Item);
            FeuilleReclassement.INSERT(TRUE);
        END;

        LigneFusion.RESET();
        LigneFusion.SETRANGE("No.", _pNoFusion);
        IF LigneFusion.FINDFIRST() THEN
            REPEAT
                CLEAR(LigneFeuille);
                LigneFeuille.VALIDATE("Journal Template Name", FeuilleReclassement."Journal Template Name");
                LigneFeuille.VALIDATE("Journal Batch Name", FeuilleReclassement.Name);
                LigneFeuille.VALIDATE("Entry Type", LigneFeuille."Entry Type"::Transfer);
                LigneFeuille.VALIDATE("Document No.", _pNoFusion);
                LigneFeuille.VALIDATE("Line No.", LigneFusion."Line No.");
                LigneFeuille.VALIDATE("Item No.", LigneFusion."Item No.");
                LigneFeuille.VALIDATE("Posting Date", WORKDATE());
                LigneFeuille.VALIDATE("Location Code", 'SP');
                LigneFeuille.VALIDATE("New Location Code", 'SP');
                LigneFeuille.VALIDATE("New Bin Code", LigneFusion."Bin Code");
                LigneFeuille.VALIDATE(Quantity, LigneFusion.Quantity);

                IF LigneFeuille."New Bin Code" <> LigneFeuille."Bin Code" THEN
                    LigneFeuille.INSERT(TRUE);

            UNTIL LigneFusion.NEXT() = 0;
    end;
}

