codeunit 50030 CalculConso
{

    trigger OnRun()
    begin
        CalculConso('SP026613', 20110101D, 'OOOOOOOOOOOO', 4, TRUE);
    end;

    var
        RecDate: Record Date;
        gEventsAppro: Codeunit "Events Appro";

        ESK001Msg: Label 'Article : %1\Mois Deb : %2\Conso\%3\Total : %4\Moyenne X %5  -  Moyenne Y : %6\Variance X %7  -  Variance Y %8\CoVaiance %9\Coef Directeur %10\', Comment = '%1 = Mois Deb ; %2 = Conso ;%3 = Total;  %4 = Moyenne ;%5 = Moyenne Y ; %6 = Variance X ; %7 = Variance Y; %8 = CoVaiance; %9 = Coef Directeur ; %10 =nCov/VarX';
        ESK002Msg: Label 'Ord Origine %1\Projection %2\Ecart Type %3\ Rap %4   Seuil %5\ RESULTAT %6', Comment = '%1 = Ord Origine ; %2 = Projection ;%3 =Ecart Type ; %4 = Rap ; %5 = Seuil;  %6 = RESULTAT';

    procedure CalculConso(_pRefArticle: Code[20]; _pDateFin: Date; _pMoisRef: Code[12]; _pNbAnnee: Integer; _pDebug: Boolean): Decimal
    var
        // ValueEntry: record "Value Entry";
        Seuil: Integer;
        DateDeb: Date;
        i: Integer;
        j: Integer;

        ConsoMensuelle: array[48] of Decimal;
        ConsoAnnuelle: array[4] of Decimal;
        MoyX: Decimal;
        MoyY: Decimal;
        VarX: Decimal;
        VarY: Decimal;
        Cov: Decimal;
        A: Decimal;
        B: Decimal;
        Pro: Decimal;
        Ety: Decimal;
        Rap: Decimal;
        txt: Text[1024];
        Resultat: Decimal;
        x: Integer;
    begin
        // Fonction récupéré de DBX

        Seuil := 90;
        DateDeb := CALCDATE('<-4A+FM+1J>', _pDateFin);


        // ---------------------------------------------------
        // Remplissage du tableau avec les conso mois par mois
        // ---------------------------------------------------
        CLEAR(ConsoMensuelle);
        i := 0;
        RecDate.RESET();
        RecDate.SETRANGE("Period Type", RecDate."Period Type"::Month);
        RecDate.SETRANGE("Period Start", DateDeb, _pDateFin);
        RecDate.FINDFIRST();
        REPEAT
            i += 1;
            // Si le mois est a prendre en compte, on calcul les conso.
            IF COPYSTR(_pMoisRef, RecDate."Period No.", 1) = 'O' THEN 
                //i := (DATE2DMY(RecDate."Period Start", 3) - DATE2DMY(DateDeb, 3)) * 12 +
                //     (DATE2DMY(RecDate."Period Start", 2) - DATE2DMY(DateDeb, 2)) + 1;
                ConsoMensuelle[i] := RechecherConsommation(_pRefArticle, RecDate."Period Start", RecDate."Period End");
            UNTIL RecDate.NEXT() = 0;


        // ---------------------------------------
        // Somme les valeurs sur 12 mois glissants
        // ---------------------------------------
        CLEAR(ConsoAnnuelle);

        FOR i := 1 TO 4 DO
            FOR j := 1 TO 12 DO BEGIN
                x := 12 * i - 12 + j;
                ConsoAnnuelle[i] += ConsoMensuelle[x]; // ????????
            END;

        // Calcul de la projection
        CASE _pNbAnnee OF
            1:
                BEGIN
                    MoyX := 1;
                    MoyY := ConsoAnnuelle[4];
                    VarX := 0;
                    VarY := 0;
                    Cov := 0;
                    A := 0;
                    B := MoyY;
                    Pro := ConsoAnnuelle[4];
                END;
            2:
                BEGIN
                    MoyX := (1 + 2) / 2;
                    MoyY := ROUND((ConsoAnnuelle[3] + ConsoAnnuelle[4]) / 2, 0.1);
                    VarX := ROUND((Carre(1 - MoyX) + Carre(2 - MoyX)) / 2);
                    VarY := ROUND((Carre(ConsoAnnuelle[3] - MoyY) + Carre(ConsoAnnuelle[4] - MoyY)) / 2);
                    Cov := ((1 * ConsoAnnuelle[3] + 2 * ConsoAnnuelle[4]) / 2) - MoyX * MoyY;
                    A := Cov / VarX;
                    B := MoyY - A * MoyX;
                    Pro := A * 3 + B;
                END;
            3:
                BEGIN
                    MoyX := (1 + 2 + 3) / 3;
                    MoyY := (ConsoAnnuelle[2] + ConsoAnnuelle[3] + ConsoAnnuelle[4]) / 3;
                    VarX := (Carre(1 - MoyX) + Carre(2 - MoyX) + Carre(3 - MoyX)) / 3;
                    VarY := (Carre(ConsoAnnuelle[2] - MoyY) + Carre(ConsoAnnuelle[3] - MoyY) + Carre(ConsoAnnuelle[4] - MoyY)) / 3;
                    Cov := ((1 * ConsoAnnuelle[2] + 2 * ConsoAnnuelle[3] + 3 * ConsoAnnuelle[4]) / 3) - MoyX * MoyY;
                    A := Cov / VarX;
                    B := MoyY - A * MoyX;
                    Pro := A * 4 + B;
                END;
            4:
                BEGIN
                    MoyX := (1 + 2 + 3 + 4) / 4;
                    MoyY := (ConsoAnnuelle[1] + ConsoAnnuelle[2] + ConsoAnnuelle[3] + ConsoAnnuelle[4]) / 4;
                    VarX := (Carre(1 - MoyX) + Carre(2 - MoyX) + Carre(3 - MoyX) + Carre(4 - MoyX)) / 4;
                    VarY := (Carre(ConsoAnnuelle[1] - MoyY) + Carre(ConsoAnnuelle[2] - MoyY) + Carre(ConsoAnnuelle[3] - MoyY)
                              + Carre(ConsoAnnuelle[4] - MoyY)) / 4;
                    Cov := ((1 * ConsoAnnuelle[1] + 2 * ConsoAnnuelle[2] + 3 * ConsoAnnuelle[3] + 4 * ConsoAnnuelle[4]) / 4) - MoyX * MoyY;
                    A := Cov / VarX;
                    B := MoyY - A * MoyX;
                    Pro := A * 5 + B;
                END;
            0:
                BEGIN  // On recupere la plus grande des valeurs
                    Pro := -99999;
                    FOR i := 1 TO 4 DO
                        IF ConsoAnnuelle[i] > Pro THEN
                            Pro := ConsoAnnuelle[i];
                END;
        END;

        // AD Le 15-11-2011 => Pro est un decimal (10,0) dans dbx
        Pro := ROUND(Pro, 1);

        Ety := RacineCarre(VarY);
        IF MoyY <> 0 THEN Rap := 100 * Ety / MoyY;
        IF (Rap > Seuil) AND (Seuil <> 0) THEN
            Resultat := MoyY
        ELSE
            Resultat := Pro;

        Resultat := ROUND(Resultat, 1);


        FOR i := 1 TO 12 DO
            txt += FORMAT(ConsoMensuelle[i]) + ' - ' + FORMAT(ConsoMensuelle[i + 12]) + ' - ' + FORMAT(ConsoMensuelle[i + 24]) + ' - ' +
                   FORMAT(ConsoMensuelle[i + 36]) + '\\';

        IF _pDebug THEN BEGIN

            MESSAGE(ESK001Msg, _pRefArticle, DateDeb, txt, FORMAT(ConsoAnnuelle[1]) + ' - ' + FORMAT(ConsoAnnuelle[2]) + ' - ' +
                  FORMAT(ConsoAnnuelle[3]) + ' - ' + FORMAT(ConsoAnnuelle[4]), MoyX, MoyY, VarX, VarY, Cov, A);

            MESSAGE(ESK002Msg, B, Pro, Ety, Rap, Seuil,
                  Resultat);
        END;

        EXIT(Resultat);
    end;

    procedure RechecherConsommation(_pRefArticle: Code[20]; _pDateDeb: Date; _pDateFin: Date): Decimal
    var
        LStatConso: Record "Stats Conso";
        LVentesFictives: Record "Ventes Manuelles";
    begin

        /*
        ValueEntry.RESET();
        ValueEntry.SETCURRENTKEY("Item No.","Posting Date","Item Ledger Entry Type","Entry Type",
           "Variance Type","Source No.","Récupération Historiques","Location Code", "Document No.");
        ValueEntry.SETRANGE("Item No.", _pRefArticle);
        ValueEntry.SETRANGE("Posting Date", _pDateDeb, _pDateFin);
        ValueEntry.SETFILTER("Item Ledger Entry Type",'%1|%2', ValueEntry."Item Ledger Entry Type"::Sale,
              ValueEntry."Item Ledger Entry Type"::"Negative Adjmt.");
        // AD A VERIFIER
        //ValueEntry.SETFILTER("Document No.", '<>%1', 'RECUP DBX P');
        //ValueEntry.SETFILTER("Document No.", '<>%1', 'RECUP DBX*');
        ValueEntry.CALCSUMS("Valued Quantity");
        
        
        EXIT (ValueEntry."Valued Quantity" * -1);
        */

        LStatConso.RESET();
        LStatConso.SETCURRENTKEY("Item No.", "Posting Date", "Item Ledger Entry Type", "Location Code");
        LStatConso.SETRANGE("Item No.", _pRefArticle);
        LStatConso.SETRANGE("Posting Date", _pDateDeb, _pDateFin);
        LStatConso.SETFILTER("Item Ledger Entry Type", '%1|%2|%3', LStatConso."Item Ledger Entry Type"::Sale,
            LStatConso."Item Ledger Entry Type"::Consumption,
            LStatConso."Item Ledger Entry Type"::"Assembly Consumption");


        LStatConso.CALCSUMS("Valued Quantity");


        LVentesFictives.RESET();
        LVentesFictives.SETCURRENTKEY("Item No.", "Posting Date", "Item Ledger Entry Type", "Location Code");
        LVentesFictives.SETRANGE("Item No.", _pRefArticle);
        LVentesFictives.SETRANGE("Posting Date", _pDateDeb, _pDateFin);
        LVentesFictives.SETFILTER("Item Ledger Entry Type", '%1|%2', LVentesFictives."Item Ledger Entry Type"::Sale,
            LVentesFictives."Item Ledger Entry Type"::Consumption);


        LVentesFictives.CALCSUMS("Valued Quantity");




        EXIT((LStatConso."Valued Quantity" + LVentesFictives."Valued Quantity") * -1);

    end;

    procedure CalculerConsoSur4ans(_pRefArticle: Code[20]; var _pTabConso: array[5, 12] of Decimal; var _pTabTotalConso: array[5] of Decimal; _pDateBase: Date)
    var
        recDate: Record Date;
        dateDebut: Date;
        dateFin: Date;
        i: Integer;
        j: Integer;

    begin

        dateDebut := DMY2DATE(1, 1, DATE2DMY(_pDateBase, 3) - 4);
        dateFin := _pDateBase;


        CLEAR(_pTabConso);
        i := 0;
        recDate.RESET();
        recDate.SETRANGE("Period Type", recDate."Period Type"::Month);
        recDate.SETRANGE("Period Start", dateDebut, dateFin);
        recDate.FindSet();
        REPEAT
            i := DATE2DMY(WORKDATE(), 3) - DATE2DMY(recDate."Period Start", 3) + 1;
            j := DATE2DMY(recDate."Period Start", 2);
            _pTabConso[i] [j] :=
               RechecherConsommation(_pRefArticle, recDate."Period Start", recDate."Period End");
        UNTIL recDate.NEXT() = 0;

        CLEAR(_pTabTotalConso);
        FOR i := 1 TO 5 DO
            FOR j := 1 TO 12 DO
                _pTabTotalConso[i] += _pTabConso[i] [j];
    end;

    procedure RacineCarre(_pValeur: Decimal): Decimal
    begin
        IF _pValeur <= 0 THEN EXIT(0);
        EXIT(POWER(_pValeur, 0.5));
    end;

    local procedure Carre(_pValeur: Decimal): Decimal
    begin
        EXIT(_pValeur * _pValeur);
    end;

    procedure "CréerLigneAppro"(_pItem: Record Item; _pQteReappro: Decimal; var _pReqLine: Record "Requisition Line")
    var
        ReqLine2: Record "Requisition Line";
        temp_Buffer: Record "Vendor Price Buffer" temporary;
        temp_Buffer2: Record "Vendor Price Buffer" temporary;
    begin
        // -----------------------------------------
        // Génération de la ligne de demande d'achat
        // -----------------------------------------
        // Recherche si l'article existe sur une feuille (si oui suppression pour eviter les erreurs)
        /*
        ReqLine2.RESET();
        ReqLine2.SETRANGE(Type, ReqLine2.Type::Item);
        ReqLine2.SETRANGE("No.", _pItem."No.");
        IF ReqLine2.FINDSET(FALSE, FALSE) THEN
          REPEAT
            ReqLine2.DELETE;
          UNTIL ReqLine2.NEXT() = 0;
        */


        ReqLine2 := _pReqLine;

        // Création de la ligne de réappro
        _pReqLine.INIT();
        _pReqLine.SetReapproAuto(TRUE);
        _pReqLine.VALIDATE(Type, _pReqLine.Type::Item);
        _pReqLine.VALIDATE("No.", _pItem."No.");
        _pReqLine.VALIDATE("Location Code", _pItem.GETFILTER("Location Filter"));
        _pReqLine.VALIDATE("Action Message", _pReqLine."Action Message"::New);
        _pReqLine.VALIDATE("Original Quantity", _pQteReappro);
        _pReqLine.VALIDATE(Quantity, _pQteReappro);

        _pReqLine.VALIDATE("Stock Calculé", _pItem.Inventory);
        _pReqLine.VALIDATE("Fusion en attente sur", _pItem."Fusion en attente sur");

        // MC Le 08-11-2011 => Nouveau champs provenant de la fiche article.
        _pReqLine.VALIDATE("Date création fiche", _pItem."Last Date Modified");
        _pReqLine.VALIDATE(Stocke, _pItem.Stocké);
        //ReqLine.VALIDATE("Date dernère sortie",


        _pReqLine.VALIDATE("Accept Action Message", TRUE);

        _pReqLine.VALIDATE("Reservé Calculé", ReqLine2."Reservé Calculé");
        _pReqLine.VALIDATE("Attendu Calculé", ReqLine2."Attendu Calculé");
        _pReqLine.VALIDATE("Projection Mini", ReqLine2."Projection Mini");
        _pReqLine.VALIDATE("Projection Maxi", ReqLine2."Projection Maxi");
        _pReqLine.VALIDATE("Conso Mois En Cours", ReqLine2."Conso Mois En Cours");
        _pReqLine.VALIDATE("Mini Encours", ReqLine2."Mini Encours");
        _pReqLine.VALIDATE(Formule, ReqLine2.Formule);
        _pReqLine.VALIDATE("Chaine Mois", ReqLine2."Chaine Mois");
        _pReqLine.VALIDATE("Type Comparaison", ReqLine2."Type Comparaison");
        _pReqLine.VALIDATE("Type Projection", ReqLine2."Type Projection");
        _pReqLine.VALIDATE("Type de commande", ReqLine2."Type de commande");

        //CFR le 18/10/2022 => R‚gie : ajout [FiltreFournisseurAExclure] [TriEcartMarge]
        _pReqLine.VALIDATE(FiltreFournisseurAExclure, ReqLine2.FiltreFournisseurAExclure);
        _pReqLine.VALIDATE(TriEcartMarge, ReqLine2.TriEcartMarge);
        // FIN CFR le 18/10/2022

        _pReqLine.VALIDATE("Location Code", 'SP');

        // AD Le 16-04-2012
        //_pReqLine.VALIDATE("Vendor No.", _pItem."Vendor No."); // CODE D'ORIGINE
        _pReqLine.VALIDATE("Vendor No.", '');
        // FIN AD Le 16-04-2012


        temp_Buffer.DELETEALL();
        temp_Buffer2.DELETEALL();
        temp_Buffer2.SetRecords(_pReqLine, temp_Buffer);
        temp_Buffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
        IF temp_Buffer.FINDSET() THEN BEGIN
            _pReqLine.VALIDATE("Vendor No.", temp_Buffer."Vendor No.");
            // MC Le 25-10-2012 => Corrige prix d'achat pour la demande
            _pReqLine.VALIDATE("Direct Unit Cost", temp_Buffer."Net Unit Price");
            _pReqLine."Price Qty" := temp_Buffer."Minimum Qty" // MCO Le 04-10-2018
                                                               // FIN MC Le 25-10-2012
                                                               //_pReqLine.MODIFY(TRUE);
        END;

        // AD Le 16-04-2012 =>
        IF _pItem."Fusion en attente sur" <> '' THEN
            _pReqLine.VALIDATE("Vendor No.", '');
        // FIN AD Le 16-04-2012


        _pReqLine.VALIDATE("Fournisseur Calculé", _pReqLine."Vendor No.");


        // MC Le 25-10-2012 => Corrige prix d'achat pour la demande
        // Code mis en commentaire
        /*
        // LM le 27-08-2012 => corrige meilleur prix d'achat dans la demande
        MeilleurFournisseur := CalcPurchasePrice.GetBestPurchPrice(_pItem."No.", 10000000, ReqLine2."Type de commande", px, '');
        _pReqLine.VALIDATE(_pReqLine."Vendor No.",MeilleurFournisseur);
        _pReqLine.VALIDATE(_pReqLine."Direct Unit Cost",px);
        // fin LM le 27-08-2012
        */
        // FIN MC Le 25-10-2012

        _pReqLine.Insert();

        /*
        tmp_Buffer.DELETEALL();
        tmp_Buffer2.DELETEALL();
        tmp_Buffer2.SetRecords(_pReqLine, tmp_Buffer);
        tmp_Buffer.SETCURRENTKEY("Worksheet Template Name","Journal Batch Name","Line No.","Net Unit Price");
        IF tmp_Buffer.FINDSET(TRUE, FALSE) THEN
          BEGIN
            _pReqLine.VALIDATE("Vendor No.", tmp_Buffer."Vendor No.");
            _pReqLine.MODIFY(TRUE);
          END;
        */
        //CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e par rapport … la quantit‚ minimum du tarif achat
        gEventsAppro.SetSelectedByMinimumOrder(_pReqLine, temp_Buffer);
        //FIN CFR Le 13/04/2022
    end;

    //CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e par rapport … la quantit‚ minimum du tarif achat
    //CFR le 18/10/2022 => R‚gie : ajout [FiltreFournisseurAExclure] [TriEcartMarge]
}

