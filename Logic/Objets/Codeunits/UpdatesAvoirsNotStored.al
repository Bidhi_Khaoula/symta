codeunit 50042 "Updates Avoirs NotStored"
{
    Permissions = TableData "Item Ledger Entry" = rimd,
                  TableData "Value Entry" = rimd,
                  TableData "Return Receipt Line" = rimd;

    trigger OnRun()
    var
       // pointentree: Record "Avg. Cost Adjmt. Entry Point";
    begin
        // pointentree.RESET();
        // pointentree.SETFILTER("Valuation Date", '<%1', 010118D);
        // pointentree.SETRANGE("Cost Is Adjusted", FALSE);
        // pointentree.FINDSET;
        // REPEAT
        // pointentree."Cost Is Adjusted" := TRUE;
        // pointentree.Date_Modif := TODAY;
        // pointentree.MODIFY();
        // UNTIL pointentree.NEXT=0;

        // Récupere les avoirs potentiellement concernés par le problème.
        EnteteAvoir.RESET();
        //EnteteAvoir.SETFILTER("No.", '<>%1', 'AVE-18010020');// => Filtre sur seul avoir
        EnteteAvoir.SETRANGE("No.", 'AVE-18010224');// => Filtre sur seul avoir
        EnteteAvoir.SETFILTER("Posting Date", '>=%1&<=%2', 20180115D, 20180210D);
        EnteteAvoir.SETRANGE("Sans mouvement de stock", TRUE);
        IF EnteteAvoir.FINDSET() THEN
            REPEAT
                LigneAvoir.RESET();
                LigneAvoir.SETRANGE("Document No.", EnteteAvoir."No.");
                LigneAvoir.SETRANGE(Type, LigneAvoir.Type::Item);
                LigneAvoir.SETFILTER("Return Receipt No.", '<>%1', '');
                IF LigneAvoir.FINDSET() THEN
                    REPEAT
                        // On récupère la ligne réception retour enregistrée correspondant à la ligne de l'avoir
                        IF LigneReceptionRetour.GET(LigneAvoir."Return Receipt No.", LigneAvoir."Return Receipt Line No.") THEN BEGIN


                            // Récupère l'écriture comptable article liée à la ligne de réception
                            EcritureArticle.RESET();
                            EcritureArticle.SETRANGE("Document No.", LigneReceptionRetour."Document No.");
                            EcritureArticle.SETRANGE("Document Line No.", LigneReceptionRetour."Line No.");
                            EcritureArticle.FINDFIRST();
                            IF LigneReceptionRetour."Quantity Invoiced" <> EcritureArticle."Invoiced Quantity" THEN BEGIN // Si le retour n'est pas facturé alors il ya le probleme
                                                                                                                          // MAJ de la ligne de réception
                                                                                                                          //UpdateReturnReceiptLine(); // Les lignes de réceptions sont déjà bonnes
                                                                                                                          // Récupère l'écriture valeur liée à la ligne d'avoir
                                EcritureValeur.RESET();
                                EcritureValeur.SETRANGE("Document No.", LigneAvoir."Document No.");
                                EcritureValeur.SETRANGE("Document Line No.", LigneAvoir."Line No.");
                                EcritureValeur.SETRANGE("Posting Date", LigneAvoir."Posting Date");
                                EcritureValeur.FINDFIRST();

                                // Mise à jour de l'écriture comptable à partir d'une fonction du cu80 retouchée
                                IF UpdateItemLedgEntry(EcritureValeur, EcritureArticle) THEN
                                    EcritureArticle.MODIFY();

                                EcritureArticle.CALCFIELDS("Cost Amount (Expected)", "Sales Amount (Expected)");
                                // Mise à jour de l'écriture valeur
                                EcritureValeur."Item Ledger Entry No." := EcritureArticle."Entry No.";
                                EcritureValeur."Sans mouvement de stock" := FALSE;
                                EcritureValeur."Location Code" := EcritureArticle."Location Code";
                                EcritureValeur."Valuation Date" := EcritureArticle."Posting Date";
                                EcritureValeur."Cost Amount (Expected)" := -EcritureArticle."Cost Amount (Expected)";
                                EcritureValeur."Sales Amount (Expected)" := -EcritureArticle."Sales Amount (Expected)";

                                EcritureValeur.MODIFY();

                            END;
                        END;
                    UNTIL LigneAvoir.NEXT() = 0;
            UNTIL EnteteAvoir.NEXT() = 0;

        MESSAGE('Fini');
    end;

    var
        EnteteAvoir: Record "Sales Cr.Memo Header";
        LigneAvoir: Record "Sales Cr.Memo Line";
        LigneReceptionRetour: Record "Return Receipt Line";
        EcritureArticle: Record "Item Ledger Entry";
        EcritureValeur: record "Value Entry";

    local procedure UpdateReturnReceiptLine()
    begin
        LigneReceptionRetour."Quantity Invoiced" += LigneAvoir.Quantity;
        LigneReceptionRetour."Qty. Invoiced (Base)" += LigneAvoir."Quantity (Base)";
        LigneReceptionRetour."Return Qty. Rcd. Not Invd." := LigneReceptionRetour.Quantity - LigneReceptionRetour."Quantity Invoiced";
        // Nouveau champ pour marquer la ligne comme traitée
        LigneReceptionRetour.MODIFY();
    end;

    local procedure UpdateValueEntry()
    begin
    end;

    local procedure UpdateItemLedgEntry(ValueEntry: record "Value Entry"; var ItemLedgEntry: Record "Item Ledger Entry") ModifyEntry: Boolean
    begin
        WITH ItemLedgEntry DO

            IF NOT (ValueEntry."Entry Type" IN
                    [ValueEntry."Entry Type"::Variance,
                     ValueEntry."Entry Type"::"Indirect Cost",
                     ValueEntry."Entry Type"::Rounding])
            THEN BEGIN
                IF ValueEntry.Inventoriable AND (NOT ValueEntry.Adjustment OR ("Entry Type" = "Entry Type"::"Assembly Output")) THEN
                    UpdateAvgCostAdjmtEntryPoint(ItemLedgEntry, ValueEntry."Valuation Date");

                IF (Positive OR "Job Purchase") AND
                   (Quantity <> "Remaining Quantity") AND NOT "Applied Entry to Adjust" AND
                   //(Item.Type = Item.Type::Inventory) AND
                   (NOT TRUE OR AppliedEntriesToReadjust(ItemLedgEntry))
                THEN BEGIN
                    "Applied Entry to Adjust" := TRUE;
                    ModifyEntry := TRUE;
                END;

                IF (ValueEntry."Entry Type" = ValueEntry."Entry Type"::"Direct Cost") AND
                   //(ItemJnlLine."Item Charge No." = '') AND
                   //(ValueEntry.Quantity = 0)
                   (ValueEntry."Invoiced Quantity" <> 0)
                THEN BEGIN
                    IF ValueEntry."Invoiced Quantity" <> 0 THEN BEGIN
                        "Invoiced Quantity" := "Invoiced Quantity" + ValueEntry."Invoiced Quantity";
                        IF ABS("Invoiced Quantity") > ABS(Quantity) THEN
                            ERROR('ici');
                        //VerifyInvoicedQty(ItemLedgEntry);
                        ModifyEntry := TRUE;
                    END;

                    IF ("Entry Type" <> "Entry Type"::Output) AND
                       ("Invoiced Quantity" = Quantity) AND
                       NOT "Completely Invoiced"
                    THEN BEGIN
                        "Completely Invoiced" := TRUE;
                        ModifyEntry := TRUE;
                    END;

                    IF "Last Invoice Date" < ValueEntry."Posting Date" THEN BEGIN
                        "Last Invoice Date" := ValueEntry."Posting Date";
                        ModifyEntry := TRUE;
                    END;
                END;

                //IF ItemJnlLine."Applies-from Entry" <> 0 THEN
                //  UpdateOutboundItemLedgEntry(ItemJnlLine."Applies-from Entry");
            END;

        EXIT(ModifyEntry);
    end;

    local procedure UpdateAvgCostAdjmtEntryPoint(OldItemLedgEntry: Record "Item Ledger Entry"; ValuationDate: Date)
    var
       // AvgCostAdjmtEntryPoint: Record "Avg. Cost Adjmt. Entry Point";
      //  ValueEntry: record "Value Entry";
    begin
        // MCO Attention a ce code pertinent ou non ???
        /*
        WITH AvgCostAdjmtEntryPoint DO BEGIN
          ValueEntry.INIT();
          ValueEntry."Item No." := OldItemLedgEntry."Item No.";
          ValueEntry."Valuation Date" := ValuationDate;
          ValueEntry."Location Code" := OldItemLedgEntry."Location Code";
          ValueEntry."Variant Code" := OldItemLedgEntry."Variant Code";
        
          LOCKTABLE;
          UpdateValuationDate(ValueEntry);
        END;
        */

    end;

    local procedure AppliedEntriesToReadjust(ItemLedgEntry: Record "Item Ledger Entry"): Boolean
    begin
        WITH ItemLedgEntry DO
            EXIT("Entry Type" IN ["Entry Type"::Output, "Entry Type"::"Assembly Output"]);
    end;
}

