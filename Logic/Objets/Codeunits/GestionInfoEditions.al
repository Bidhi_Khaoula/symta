codeunit 50052 "Gestion Info Editions"
{

    trigger OnRun()
    begin
    end;

    var
        Address3Txt: Label 'Téléphone %1 - Fax %2' , Comment = '%1 = Tel , %2 = Fax';
        Address4Txt: Label 'e-mail : %1', Comment = '%1 e-Mail';
        Address6Txt: Label '%1 au capital de %2 - SIRET %3' , Comment = '%1 = Form Legal ; %2 = Stock Capital ; %3 = Num Registration';
        Address6_2Txt: label '%1 au capital de %2 - SIREN : %3' , Comment = '%1 = Form Legal ; %2 = Stock Capital ; %3 = Num Registration';
        Address7Txt: Label 'APE %1 -  R.C. %2  La Roche sur Yon' , Comment = '%1 = APE , %2 = Regsitre COmmerce';
        Address7_2Txt: label 'R.C. %1  La Roche sur Yon' , Comment = '%1 = Registre Commerce';
        Address8Txt: Label 'N° d''identification T.V.A.: %1' , Comment = '%1 = Num TVA socié';
        Address8_2Txt: label 'TVA : %1 - EORI : %2' , Comment = '%1 = Num TVA socié; %2 = Num EORI Socié..' ;
        PiedPage1Txt: Label 'PIECES ADAPTABLES POUR TRACTEURS MOISSONNEUSES BATTEUSES ET MATERIEL AGRICOLE.';
        PiedPage2Txt: Label 'LES REFERENCES CONSTRUCTEURS SONT MENTIONNEES A TITRE INDICATIF. CONDITIONS DE VENTE AU VERSO.';
        //PiedPage3Txt: Label 'NotUsed';

    procedure AfficheInformationSymta(bln_PrintInfo: Boolean; var tab_InfoSymta: array[8] of Text[60]; var CompanyInfoForpicture: Record "Company Information")
    begin
        // Impression des informations concernant SYMTA ou non.
        IF bln_PrintInfo THEN BEGIN
            // Gestion du logo
            CompanyInfoForpicture.GET();
            CompanyInfoForpicture.CALCFIELDS(Picture);
            // Remplit le tableau d'information de symta.
            SetSymtaInfo(tab_InfoSymta);
        END
        ELSE BEGIN
            // Gestion du logo
            CLEAR(CompanyInfoForpicture);
            // Vide le tableau d'information de symta.
            RazSymtaInfo(tab_InfoSymta);
        END;
    end;

    procedure RazSymtaInfo(var tab_InfoSymta: array[8] of Text[60])
    var
        i: Integer;
    begin
        //** Cette fonction permet de vider tous les champs du tableau d'information symta.
        FOR i := 1 TO 8 DO
            tab_InfoSymta[i] := '';
    end;

    procedure SetSymtaInfo(var tab_InfoSymta: array[8] of Text[60])
    var
        rec_CompanyInformation: Record "Company Information";
        rec_Pays: Record "Country/Region";
    begin
        //** Cette fonction permet de remplir le tableau d'information SYMTA.

        // Initialisation des informations à afficher
        rec_CompanyInformation.GET();
        // Récupération du pays
        IF rec_CompanyInformation."Country/Region Code" <> '' THEN
            rec_Pays.GET(rec_CompanyInformation."Country/Region Code");


        // Première Ligne : Adresse - Adresse 2
        tab_InfoSymta[1] := rec_CompanyInformation.Address + '-' + rec_CompanyInformation."Address 2";
        // Seconde Ligne  : Code Postal Ville - Pays
        tab_InfoSymta[2] := rec_CompanyInformation."Post Code" + ' ' + rec_CompanyInformation.City + '-' + rec_Pays.Name;
        // Troisième Ligne : Téléphone - Fax
        tab_InfoSymta[3] := STRSUBSTNO(Address3Txt, rec_CompanyInformation."Phone No.", rec_CompanyInformation."Fax No.");
        // Quatrième Ligne : Email
        tab_InfoSymta[4] := STRSUBSTNO(Address4Txt, rec_CompanyInformation."E-Mail");
        // Cinquième Ligne : Site
        tab_InfoSymta[5] := rec_CompanyInformation."Home Page";

        // Sixième Ligne : Forme Juridique et capital
        //tab_InfoSymta[6] := STRSUBSTNO(txtAddress6, rec_CompanyInformation."Legal Form", rec_CompanyInformation."Stock Capital");
        // Septième Ligne : SIRET - APE - RC
        //tab_InfoSymta[7] := STRSUBSTNO(txtAddress7, rec_CompanyInformation."Registration No.", rec_CompanyInformation."APE Code",
        //                    rec_CompanyInformation."Trade Register");

        // Cj 16-12-2013 modif pour mettre la ville du RC ligne 7
        // Sixième Ligne : Forme Juridique et capital
        tab_InfoSymta[6] := STRSUBSTNO(Address6Txt, rec_CompanyInformation."Legal Form", rec_CompanyInformation."Stock Capital",
                              rec_CompanyInformation."Registration No.");
        // Septième Ligne : SIRET - APE - RC
        tab_InfoSymta[7] := STRSUBSTNO(Address7Txt, rec_CompanyInformation."APE Code",
                            rec_CompanyInformation."Trade Register");




        // Huitième Ligne : Identifiant Intracommunautaire.
        tab_InfoSymta[8] := STRSUBSTNO(Address8Txt, rec_CompanyInformation."VAT Registration No.");
    end;

    procedure GetInfoPiedPage(_AfficheInfoSymta: Boolean; var textSIEGSOC: array[5] of Text[250])
    var
        rec_CompanyInformation: Record "Company Information";
    begin
        CLEAR(textSIEGSOC);
        IF NOT _AfficheInfoSymta THEN
            EXIT;

        textSIEGSOC[1] := PiedPage1Txt;
        textSIEGSOC[2] := PiedPage2Txt;

        rec_CompanyInformation.GET();
        //textSIEGSOC[3] := txtPiedPage3;
        // dernière ligne pied page : information juridique société
        // CFR le 06/04/2023 - Régie : Modification pied de page documents commerciaux
        /*textSIEGSOC[3] := STRSUBSTNO(txtAddress6, rec_CompanyInformation."Legal Form", rec_CompanyInformation."Stock Capital",
                              rec_CompanyInformation."Registration No.") + ' - ' +
                          STRSUBSTNO(txtAddress7, rec_CompanyInformation."APE Code",
                              rec_CompanyInformation."Trade Register") + ' - ' +
                          STRSUBSTNO(txtAddress8, rec_CompanyInformation."VAT Registration No.");
        */
        textSIEGSOC[3] := STRSUBSTNO(Address6_2Txt, rec_CompanyInformation."Legal Form",
                                                  rec_CompanyInformation."Stock Capital",
                                                  COPYSTR(rec_CompanyInformation."Registration No.", 1, 11))
                        + ' - ' +
                        STRSUBSTNO(Address7_2Txt, rec_CompanyInformation."Trade Register")
                        + ' - ' +
                        STRSUBSTNO(Address8_2Txt, rec_CompanyInformation."VAT Registration No.",
                                                  rec_CompanyInformation.EORI);
        // FIN CFR le 06/04/2023
    end;

    PROCEDURE GetSalesInfoIncotermICC2020(pIncotermCode: Code[10]; pIncotermCity: Text[50]) rText: Text;
    BEGIN
        // CFR le 06/04/2023 - R‚gie : Incoterm ICC 2020 Ventes
        rText := '';
        IF (pIncotermCode <> '') AND (pIncotermCity <> '') THEN
            rText := STRSUBSTNO('Incoterm ICC 2020 : %1 %2', pIncotermCode, pIncotermCity);
        EXIT(rText);
    END;

    // CFR le 06/04/2023 - R‚gie : Modification pied de page documents commerciaux
    // CFR le 06/04/2023 - R‚gie : Incoterm ICC 2020 Ventes
}

