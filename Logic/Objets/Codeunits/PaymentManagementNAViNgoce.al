codeunit 50090 "PaymentManagementNAViNégoce"
{
    // // MCO Le 23-03-2016 => Non prise en compte des codes acceptation différent de LCR


    trigger OnRun()
    begin
    end;

    procedure AffichageChamps(): Boolean
    begin
        EXIT(TRUE);
    end;

    procedure SelectionBordereau(var PaymentLine: Record "Payment Line")
    var
        PageSelection: Page "Séléctions Bordreaux";
        tmp_montant: Decimal;
        tmp_montant2: Decimal;
        delta_av: Decimal;
        delta_ap: Decimal;
        TEXT001Err: Label 'le Montant minimum saisi est superieur au montant maximun saisi';
        TEXT002Err: Label 'le Montant minimum saisi est superieur au montant maximun saisi';
        TEXT003Qst: Label 'Montant du dernier Effet: %1 %3 pour un montant total de %2 %3 le prendre ?', Comment = '%1 = Montant Crédit ; %2 = Montant 2 ; %3 = Devise )';
        TEXT004Err: Label 'Montant minimun de la selection trop petite';
        TEXT005Lbl: Label 'Montant Total: %1', Comment = '%1 =Montant';
        select_min: Decimal;
        select_max: Decimal;
        montant_min: Decimal;
        montant_max: Decimal;
        date_dech: Date;
    begin



        CLEAR(PageSelection);
        PageSelection.LOOKUPMODE := TRUE;
        IF PageSelection.RUNMODAL() <> ACTION::LookupOK THEN
            EXIT;

        PageSelection.Recup_variable(select_min, select_max, montant_min, montant_max, date_dech);

        PaymentLine.MARKEDONLY := FALSE;

        PaymentLine.MODIFYALL(Selection, FALSE);

        WITH PaymentLine DO BEGIN
            tmp_montant := 0;
            tmp_montant2 := 0;
            IF select_min > select_max THEN ERROR(TEXT001Err);
            IF montant_min > montant_max THEN ERROR(TEXT002Err);


            IF FINDFIRST() THEN
                REPEAT
                    MARK := FALSE;
                UNTIL NEXT() = 0;

            FILTERGROUP(2);

            // MCO Le 23-03-2016 => Non prise en compte des codes acceptation différent de LCR
            SETRANGE("Acceptation Code", "Acceptation Code"::LCR);
            // FIN MCO Le 23-03-2016
            SETFILTER("Credit Amount", '>=%1 & <=%2', montant_min, montant_max);
            SETFILTER("Due Date", '..%1', date_dech);

            SETCURRENTKEY("Due Date", "Credit Amount");
            IF FIND('-') THEN BEGIN
                REPEAT
                    delta_av := select_max - tmp_montant;
                    tmp_montant := tmp_montant + "Credit Amount";
                    IF tmp_montant < select_max THEN BEGIN
                        MARK := TRUE;

                        tmp_montant2 := tmp_montant2 + "Credit Amount";
                    END ELSE BEGIN
                        delta_ap := tmp_montant - select_max;
                        IF delta_ap < delta_av THEN BEGIN
                            MARK := TRUE;
                            tmp_montant2 := tmp_montant2 + "Credit Amount";

                        END;
                    END;
                UNTIL (NEXT() = 0) OR (tmp_montant > select_max);


                IF MARK() = FALSE THEN
                    IF CONFIRM(TEXT003Qst,
                       FALSE, "Credit Amount", tmp_montant2, 'EUR') THEN BEGIN
                        //ICI
                        MARK := TRUE;
                        tmp_montant2 := tmp_montant2 + "Credit Amount";
                    END

            END;
            IF tmp_montant2 < select_min THEN
                ERROR(TEXT004Err);

            MESSAGE(TEXT005Lbl, tmp_montant2);
        END;
        PaymentLine.MARKEDONLY := TRUE;
        PaymentLine.MODIFYALL(Selection, TRUE);
        PaymentLine.MODIFYALL(Selection, TRUE);
        PaymentLine.MARKEDONLY := FALSE;
        PaymentLine.SETRANGE(Selection, TRUE);
        PaymentLine.FILTERGROUP(2);
    end;
}

