// codeunit 50004 "Sales-Quote to Credit"
// {

//     TableNo = "Sales Header";

//     trigger OnRun()
//     var
//         OldSalesCommentLine: Record "Sales Comment Line";
//         Opp: Record Opportunity;
//         OpportunityEntry: Record "Opportunity Entry";
//         TempOpportunityEntry: Record "Opportunity Entry" temporary;
//         Cust: Record Customer;
//         SalesLineAff: Record "Sales Line";
//         TransferItemCharge: Codeunit "Transfer Item Charge";
//     begin
//         rec.TESTFIELD("Document Type", rec."Document Type"::Quote);
//         Cust.GET(rec."Sell-to Customer No.");
//         Cust.CheckBlockedCustOnDocs(Cust, rec."Document Type"::"Credit Memo", TRUE, FALSE);
//         rec.CALCFIELDS("Amount Including VAT");
//         SalesCreditHeader := Rec;
//         IF GUIALLOWED AND NOT HideValidationDialog THEN
//             CustCheckCreditLimit.SalesHeaderCheck(SalesCreditHeader);
//         SalesCreditHeader."Document Type" := SalesCreditHeader."Document Type"::"Credit Memo";

//         SalesQuoteLine.SETRANGE("Document Type", rec."Document Type");
//         SalesQuoteLine.SETRANGE("Document No.", rec."No.");
//         SalesQuoteLine.SETRANGE(Type, SalesQuoteLine.Type::Item);
//         SalesQuoteLine.SETFILTER("No.", '<>%1', '');
//         IF SalesQuoteLine.FINDSET() THEN
//             REPEAT
//                 IF (SalesQuoteLine."Outstanding Quantity" > 0) THEN BEGIN
//                     SalesLine := SalesQuoteLine;
//                     SalesLine.VALIDATE("Reserved Qty. (Base)", 0);
//                     SalesLine."Line No." := 0;

//                     IF SalesQuoteLine."Build Kit" THEN BEGIN
//                         KitSalesQuoteLine.SETRANGE("Document Type", rec."Document Type");
//                         KitSalesQuoteLine.SETRANGE("Document No.", rec."No.");
//                         KitSalesQuoteLine.SETRANGE("Document Line No.", SalesQuoteLine."Line No.");
//                         KitSalesQuoteLine.SETRANGE(Type, KitSalesQuoteLine.Type::Item);
//                         KitSalesQuoteLine.SETFILTER("No.", '<>%1', '');
//                         IF KitSalesQuoteLine.FINDSET() THEN
//                             REPEAT
//                                 SalesLine."No." := KitSalesQuoteLine."No.";
//                                 SalesLine."Variant Code" := KitSalesQuoteLine."Variant Code";
//                                 SalesLine."Unit of Measure Code" := KitSalesQuoteLine."Unit of Measure Code";
//                                 SalesLine."Qty. per Unit of Measure" := KitSalesQuoteLine."Qty. per Unit of Measure";
//                                 SalesLine."Outstanding Quantity" := ROUND(SalesLine."Quantity (Base)" * KitSalesQuoteLine."Quantity per", 0.00001);
//                                 SalesLine."Build Kit" := FALSE;
//                                 ItemCheckAvail.SalesLineCheck(SalesLine);
//                             UNTIL KitSalesQuoteLine.NEXT() = 0;
//                     END ELSE
//                         ItemCheckAvail.SalesLineCheck(SalesLine);

//                 END;
//             UNTIL SalesQuoteLine.NEXT() = 0;


//         SalesCreditHeader."No. Printed" := 0;
//         SalesCreditHeader.Status := SalesCreditHeader.Status::Open;
//         SalesCreditHeader."No." := '';
//         SalesCreditHeader."Quote No." := rec."No.";
//         SalesCreditLine.LOCKTABLE;
//         SalesCreditHeader.INSERT(TRUE);

//         FromDocDim.SETRANGE("Table ID", DATABASE::"Sales Header");
//         FromDocDim.SETRANGE("Document Type", rec."Document Type");
//         FromDocDim.SETRANGE("Document No.", rec."No.");

//         ToDocDim.SETRANGE("Table ID", DATABASE::"Sales Header");
//         ToDocDim.SETRANGE("Document Type", SalesCreditHeader."Document Type");
//         ToDocDim.SETRANGE("Document No.", SalesCreditHeader."No.");
//         ToDocDim.DELETEALL();

//         DocDim.MoveDocDimToDocDim(
//           FromDocDim,
//           DATABASE::"Sales Header",
//           SalesCreditHeader."No.",
//           SalesCreditHeader."Document Type",
//           0);

//         SalesCreditHeader."Order Date" := rec."Order Date";
//         IF rec."Posting Date" <> 0D THEN
//             SalesCreditHeader."Posting Date" := rec."Posting Date";
//         SalesCreditHeader."Document Date" := rec."Document Date";

//         // AD Le 30-11-2009 => Initialisation des dates à la date du jour
//         SalesCreditHeader."Order Date" := WORKDATE;
//         SalesCreditHeader."Posting Date" := WORKDATE;
//         SalesCreditHeader."Document Date" := WORKDATE;
//         // FIN AD Le 30-11-2009


//         SalesCreditHeader."Shipment Date" := rec."Shipment Date";
//         SalesCreditHeader."Shortcut Dimension 1 Code" := rec."Shortcut Dimension 1 Code";
//         SalesCreditHeader."Shortcut Dimension 2 Code" := rec."Shortcut Dimension 2 Code";
//         SalesCreditHeader."Date Sent" := 0D;
//         SalesCreditHeader."Time Sent" := 0T;

//         SalesCreditHeader."Location Code" := rec."Location Code";
//         SalesCreditHeader."Outbound Whse. Handling Time" := rec."Outbound Whse. Handling Time";
//         SalesCreditHeader."Ship-to Name" := rec."Ship-to Name";
//         SalesCreditHeader."Ship-to Name 2" := rec."Ship-to Name 2";
//         SalesCreditHeader."Ship-to Address" := rec."Ship-to Address";
//         SalesCreditHeader."Ship-to Address 2" := rec."Ship-to Address 2";
//         SalesCreditHeader."Ship-to City" := rec."Ship-to City";
//         SalesCreditHeader."Ship-to Post Code" := rec."Ship-to Post Code";
//         SalesCreditHeader."Ship-to County" := rec."Ship-to County";
//         SalesCreditHeader."Ship-to Country/Region Code" := rec."Ship-to Country/Region Code";
//         SalesCreditHeader."Ship-to Contact" := rec."Ship-to Contact";

//         SalesCreditHeader."Prepayment %" := Cust."Prepayment %";
//         IF SalesCreditHeader."Posting Date" = 0D THEN
//             SalesCreditHeader."Posting Date" := WORKDATE;

//         // AD Le 21-10-2009 => Spécificiter pour les avoirs
//         SalesCreditHeader.VALIDATE("Payment Terms Code");
//         // FIN AD Le 21-10-2009

//         SalesCreditHeader.MODIFY();

//         SalesQuoteLine.RESET();
//         SalesQuoteLine.SETRANGE("Document Type", rec."Document Type");
//         SalesQuoteLine.SETRANGE("Document No.", rec."No.");

//         FromDocDim.SETRANGE("Table ID", DATABASE::"Sales Line");
//         ToDocDim.SETRANGE("Table ID", DATABASE::"Sales Line");

//         IF SalesQuoteLine.FINDSET() THEN
//             REPEAT
//                 SalesCreditLine := SalesQuoteLine;
//                 SalesCreditLine."Document Type" := SalesCreditHeader."Document Type";
//                 SalesCreditLine."Document No." := SalesCreditHeader."No.";
//                 //ReserveSalesLine.TransferSaleLineToSalesLine(
//                 //  SalesQuoteLine,SalesCreditLine,SalesQuoteLine."Outstanding Qty. (Base)");
//                 SalesCreditLine."Shortcut Dimension 1 Code" := SalesQuoteLine."Shortcut Dimension 1 Code";
//                 SalesCreditLine."Shortcut Dimension 2 Code" := SalesQuoteLine."Shortcut Dimension 2 Code";

//                 IF Cust."Prepayment %" <> 0 THEN
//                     SalesCreditLine."Prepayment %" := Cust."Prepayment %";
//                 PrepmtMgt.SetSalesPrepaymentPct(SalesCreditLine, SalesCreditHeader."Posting Date");
//                 SalesCreditLine.VALIDATE("Prepayment %");

//                 // AD Le 21-10-2009 => Spécificiter pour les avoirs
//                 SalesCreditLine."Qty. to Ship" := 0;
//                 SalesCreditLine."Qty. to Ship (Base)" := 0;
//                 SalesCreditLine.InitQtyToReceive;
//                 // FIN AD Le 21-10-2009



//                 SalesCreditLine.Insert();

//                 //ReserveSalesLine.VerifyQuantity(SalesCreditLine,SalesQuoteLine);

//                 IF SalesQuoteLine."Build Kit" THEN BEGIN
//                     ERROR('PAS DE GESTION DES KITS POUR LES AVOIRS !!! CONTACTER ESKAPE !!!');
//                     /*
//                     KitSalesQuoteLine.RESET();
//                     KitSalesQuoteLine.SETRANGE("Document Type","Document Type");
//                     KitSalesQuoteLine.SETRANGE("Document No.","No.");
//                     KitSalesQuoteLine.SETRANGE("Document Line No.",SalesQuoteLine."Line No.");
//                     IF KitSalesQuoteLine.FINDSET() THEN
//                       REPEAT
//                         KitSalesCreditLine := KitSalesQuoteLine;
//                         KitSalesCreditLine."Document Type" := SalesCreditHeader."Document Type";
//                         KitSalesCreditLine."Document No." := SalesCreditHeader."No.";
//                         IF (KitSalesQuoteLine.Type = KitSalesQuoteLine.Type::Item) AND
//                            (KitSalesQuoteLine."No." <> '')
//                         THEN
//                           ReserveKitSalesLine.TransfKitSalLineToKitSalesLine(
//                             KitSalesQuoteLine,KitSalesCreditLine,KitSalesQuoteLine."Outstanding Qty. (Base)");
//                         KitSalesCreditLine."Shortcut Dimension 1 Code" := KitSalesQuoteLine."Shortcut Dimension 1 Code";
//                         KitSalesCreditLine."Shortcut Dimension 2 Code" := KitSalesQuoteLine."Shortcut Dimension 2 Code";
//                         KitSalesCreditLine.Insert();

//                         IF KitSalesCreditLine.Reserve = KitSalesCreditLine.Reserve::Always THEN
//                           KitSalesCreditLine.AutoReserve;
//                       UNTIL KitSalesQuoteLine.NEXT() = 0;
//                     */
//                 END;

//                 IF SalesCreditLine.Reserve = SalesCreditLine.Reserve::Always THEN BEGIN
//                     SalesCreditLine.AutoReserve;
//                 END;

//                 FromDocDim.SETRANGE("Line No.", SalesQuoteLine."Line No.");

//                 ToDocDim.SETRANGE("Line No.", SalesCreditLine."Line No.");
//                 ToDocDim.DELETEALL();
//                 DocDim.MoveDocDimToDocDim(
//                   FromDocDim,
//                   DATABASE::"Sales Line",
//                   SalesCreditHeader."No.",
//                   SalesCreditHeader."Document Type",
//                   SalesCreditLine."Line No.");
//             UNTIL SalesQuoteLine.NEXT() = 0;

//         SalesSetup.GET();
//         IF SalesSetup."Archive Quotes and Orders" THEN
//             ArchiveManagement.ArchSalesDocumentNoConfirm(Rec);

//         IF SalesSetup."Default Posting Date" = SalesSetup."Default Posting Date"::"No Date" THEN BEGIN
//             SalesCreditHeader."Posting Date" := 0D;
//             SalesCreditHeader.MODIFY();
//         END;

//         SalesCommentLine.SETRANGE("Document Type", rec."Document Type");
//         SalesCommentLine.SETRANGE("No.", rec."No.");
//         IF NOT SalesCommentLine.ISEMPTY THEN BEGIN
//             SalesCommentLine.LOCKTABLE;
//             IF SalesCommentLine.FINDSET() THEN
//                 REPEAT
//                     OldSalesCommentLine := SalesCommentLine;
//                     SalesCommentLine.DELETE;
//                     SalesCommentLine."Document Type" := SalesCreditHeader."Document Type";
//                     SalesCommentLine."No." := SalesCreditHeader."No.";
//                     SalesCommentLine.Insert();
//                     SalesCommentLine := OldSalesCommentLine;
//                 UNTIL SalesCommentLine.NEXT() = 0;
//         END;
//         SalesCreditHeader.COPYLINKS(Rec);

//         ItemChargeAssgntSales.RESET();
//         ItemChargeAssgntSales.SETRANGE("Document Type", rec."Document Type");
//         ItemChargeAssgntSales.SETRANGE("Document No.", rec."No.");
//         WHILE ItemChargeAssgntSales.FINDFIRST DO BEGIN
//             ItemChargeAssgntSales.DELETE;
//             ItemChargeAssgntSales."Document Type" := SalesCreditHeader."Document Type";
//             ItemChargeAssgntSales."Document No." := SalesCreditHeader."No.";
//             IF NOT (ItemChargeAssgntSales."Applies-to Doc. Type" IN
//                     [ItemChargeAssgntSales."Applies-to Doc. Type"::Shipment,
//                      ItemChargeAssgntSales."Applies-to Doc. Type"::"Return Receipt"])
//             THEN BEGIN
//                 ItemChargeAssgntSales."Applies-to Doc. Type" := SalesCreditHeader."Document Type";
//                 ItemChargeAssgntSales."Applies-to Doc. No." := SalesCreditHeader."No.";
//             END;
//             ItemChargeAssgntSales.Insert();
//         END;

//         // AD Le 23-10-2009 => DEEE -> Affectation des frais
//         SalesLineAff.RESET();
//         SalesLineAff.SETRANGE("Document Type", SalesCreditHeader."Document Type");
//         SalesLineAff.SETRANGE("Document No.", SalesCreditHeader."No.");
//         IF SalesLineAff.FINDFIRST () THEN
//             REPEAT
//                 TransferItemCharge.UpdateQtySalesLink(SalesLineAff);
//             UNTIL SalesLineAff.NEXT() = 0;

//         // FIN AD Le 23-10-2009


//         rec.DELETELINKS;
//         rec.DELETE;
//         SalesQuoteLine.DELETEALL();
//         FromDocDim.SETFILTER("Table ID", '%1|%2', DATABASE::"Sales Header", DATABASE::"Sales Line");
//         FromDocDim.SETRANGE("Line No.");
//         FromDocDim.DELETEALL();

//         KitSalesQuoteLine.RESET();
//         KitSalesQuoteLine.SETRANGE("Document Type", rec."Document Type");
//         KitSalesQuoteLine.SETRANGE("Document No.", rec."No.");
//         KitSalesQuoteLine.DELETEALL();
//         COMMIT;
//         CLEAR(CustCheckCreditLimit);
//         CLEAR(ItemCheckAvail);

//     end;

//     var
//         Text000: Label 'An Open Opportunity is linked to this quote.\';
//         Text001: Label 'It has to be closed before an Order can be made.\';
//         Text002: Label 'Do you wish to close this Opportunity now?';
//         Text003: Label 'Wizard Aborted';
//         SalesQuoteLine: Record "Sales Line";
//         SalesLine: Record "Sales Line";
//         SalesCreditHeader: Record "Sales Header";
//         SalesCreditLine: Record "Sales Line";
//         SalesCommentLine: Record "Sales Comment Line";
//         ItemChargeAssgntSales: Record "Item Charge Assignment (Sales)";
//         CustCheckCreditLimit: Codeunit "Cust-Check Cr. Limit";
//         ItemCheckAvail: Codeunit "311";
//         DocDim: Codeunit DimensionManagement;
//         PrepmtMgt: Codeunit "441";
//         HideValidationDialog: Boolean;
//         Text004: Label 'The Opportunity has not been closed. The program has aborted making the Order.';
//         SalesDocLineComment: Record "Sales Comment Line";
//         SalesSetup: Record "Sales & Receivables Setup";
//         ArchiveManagement: Codeunit ArchiveManagement;

//     procedure GetSalesOrderHeader(var SalesHeader2: Record "Sales Header")
//     begin
//         SalesHeader2 := SalesCreditHeader;
//     end;

//     procedure SetHideValidationDialog(NewHideValidationDialog: Boolean)
//     begin
//         HideValidationDialog := NewHideValidationDialog;
//     end;
// }

