codeunit 50060 "Export XML - eCommerce"
{

    trigger OnRun()
    begin

        ExportItem();
        ExportFamily();
        ExportCustomer();
    end;

    var
        ExportPath: Text[200];

    procedure ExportItem()
    var
        XmlFile: File;
        XmlOutStream: OutStream;
    begin
        ExportPath := '\\192.168.100.200\mistic\htdocs\FichierXmlNavision\';
        XmlFile.CREATE(ExportPath + 'article.xml');
        XmlFile.CREATEOUTSTREAM(XmlOutStream);
        //TODO XMLPORT.EXPORT(XMLPORT::"Item - eCommerce", XmlOutStream);
    end;

    procedure ExportFamily()
    var
        XmlFile: File;
        XmlOutStream: OutStream;
    begin
        ExportPath := '\\192.168.100.200\mistic\htdocs\FichierXmlNavision\';
        XmlFile.CREATE(ExportPath + 'famille.xml');
        XmlFile.CREATEOUTSTREAM(XmlOutStream);
        XMLPORT.EXPORT(XMLPORT::"Family - eCommerce", XmlOutStream);
    end;

    procedure ExportCustomer()
    var
        XmlFile: File;
        XmlOutStream: OutStream;
    begin
        ExportPath := '\\192.168.100.200\mistic\htdocs\FichierXmlNavision\';
        XmlFile.CREATE(ExportPath + 'client.xml');
        XmlFile.CREATEOUTSTREAM(XmlOutStream);
        XMLPORT.EXPORT(XMLPORT::"Customer - eCommerce", XmlOutStream);
    end;
}

