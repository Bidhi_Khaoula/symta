codeunit 50069 "Events Appro"
{

    trigger OnRun()
    begin
    end;

    [EventSubscriber(ObjectType::Table, 246, 'OnAfterValidateEvent', 'Quantity', false, false)]
    local procedure Tab246_OnAfterValidateQuantity(var Rec: Record "Requisition Line"; var xRec: Record "Requisition Line"; CurrFieldNo: Integer)
    begin
        // AD Le 30-01-2020 => REGIE -> Si 0 A lors enlever ke message d'action
        IF CurrFieldNo = Rec.FIELDNO(Quantity) THEN
            IF Rec.Quantity = 0 THEN begin
                Rec.VALIDATE("Accept Action Message", FALSE);
                Rec.Modify();
            end;
    end;

    PROCEDURE SetSelectedByMinimumOrder(VAR pRequisitionLine: Record "Requisition Line"; VAR pVendorPriceBuffer: Record "Vendor Price Buffer" TEMPORARY): Boolean;
    VAR
   
        lNetUnitPrice: Decimal;
        lBreak: Boolean;
          
    BEGIN
        //CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e par rapport … la quantit‚ minimum du tarif achat
        IF (pRequisitionLine.Quantity = 0) THEN
            EXIT;

        //CFR le 20/12/2022 => Correction bug : ne pas proposer de fournisseur pour les articles [Fusion en attente] = True
        IF (pRequisitionLine."Fusion en attente sur" <> '') THEN
            EXIT;
        //CFR le 20/12/2022

        lNetUnitPrice := 0;
        pVendorPriceBuffer.RESET();
        pVendorPriceBuffer.SETRANGE("Worksheet Template Name", pRequisitionLine."Worksheet Template Name");
        pVendorPriceBuffer.SETRANGE("Journal Batch Name", pRequisitionLine."Journal Batch Name");
        pVendorPriceBuffer.SETRANGE("Line No.", pRequisitionLine."Line No.");
        pVendorPriceBuffer.SETRANGE("Item No.", pRequisitionLine."No.");
        pVendorPriceBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");

        IF pVendorPriceBuffer.FINDSET() THEN BEGIN
            // #1# Toutes les lignes d‚s‚lectionn‚es :
            pVendorPriceBuffer.MODIFYALL(Selected, FALSE);
            pVendorPriceBuffer.MODIFYALL(Quantity, 0);
            lBreak := FALSE;

            REPEAT
                IF (lNetUnitPrice = 0) OR (pVendorPriceBuffer."Net Unit Price" < lNetUnitPrice) THEN BEGIN
                    IF (pRequisitionLine.Quantity >= pVendorPriceBuffer."Minimum order") AND
                       (pVendorPriceBuffer."Minimum Qty" <= pRequisitionLine.Quantity) THEN BEGIN
                        // #2# ligne … s‚lectionner
                        pVendorPriceBuffer.Selected := TRUE;
                        pVendorPriceBuffer.Quantity := pRequisitionLine.Quantity;
                        pVendorPriceBuffer.MODIFY();
                        lNetUnitPrice := pVendorPriceBuffer."Net Unit Price";
                        lBreak := TRUE;
                    END;
                    //MESSAGE('lNetUnitPrice:%1 - pVendorPriceBuffer."Net Unit Price":%2 - Minimum Qty:%3',lNetUnitPrice,pVendorPriceBuffer."Net Unit Price", pVendorPriceBuffer."Minimum Qty");
                END
            UNTIL (pVendorPriceBuffer.NEXT() = 0) OR (lBreak);

            pVendorPriceBuffer.SETRANGE(Selected, TRUE);
            IF pVendorPriceBuffer.FINDFIRST() THEN BEGIN
                // #3# demande d'achat
                pRequisitionLine.VALIDATE("Vendor No.", pVendorPriceBuffer."Vendor No.");
                pRequisitionLine.VALIDATE("Direct Unit Cost", pVendorPriceBuffer."Net Unit Price");
                pRequisitionLine."Buffer Price Record ID" := pVendorPriceBuffer.RECORDID;
                pRequisitionLine."Price Qty" := pVendorPriceBuffer."Minimum Qty";
                pRequisitionLine."Price Selectionned" := TRUE;
                pRequisitionLine.MODIFY(FALSE);
            END;
        END;

        pVendorPriceBuffer.RESET();
        pVendorPriceBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
        pVendorPriceBuffer.SETASCENDING("Net Unit Price", TRUE);

        //CFR Le 18/10/2022 => R‚gie : S‚lectionner par d‚faut le tarif du 2Šme meilleur fournisseur
        SetSelectedBySecondVendor(pRequisitionLine, pVendorPriceBuffer);
    END;

    LOCAL PROCEDURE SetSelectedBySecondVendor(VAR pRequisitionLine: Record "Requisition Line"; VAR pVendorPriceBuffer: Record "Vendor Price Buffer" TEMPORARY): Boolean;
    VAR
        lBestNetUnitPrice: Decimal;
        lBreak: Boolean;
        lEcartSecondPrice: Decimal;
    BEGIN
        //CFR Le 18/10/2022 => R‚gie : S‚lectionner par d‚faut le tarif du 2Šme meilleur fournisseur
        // A appeler apr‚s SetSelectedByMinimumOrder() donc fonction locale
        IF (pRequisitionLine.TriEcartMarge = 0) THEN
            EXIT;

        pVendorPriceBuffer.RESET();
        pVendorPriceBuffer.SETRANGE("Worksheet Template Name", pRequisitionLine."Worksheet Template Name");
        pVendorPriceBuffer.SETRANGE("Journal Batch Name", pRequisitionLine."Journal Batch Name");
        pVendorPriceBuffer.SETRANGE("Line No.", pRequisitionLine."Line No.");
        pVendorPriceBuffer.SETRANGE("Item No.", pRequisitionLine."No.");
        pVendorPriceBuffer.SETRANGE(Selected, TRUE);

        IF pVendorPriceBuffer.FINDFIRST() THEN BEGIN
            // #1# le meilleur tarif
            pVendorPriceBuffer.MARK(TRUE);
            lBestNetUnitPrice := pVendorPriceBuffer."Net Unit Price";

            pVendorPriceBuffer.SETRANGE(Selected, FALSE);
            pVendorPriceBuffer.SETFILTER("Vendor No.", '<>%1', pVendorPriceBuffer."Vendor No.");
            pVendorPriceBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
            IF pVendorPriceBuffer.FINDSET() THEN BEGIN
                lBreak := FALSE;
                REPEAT
                    lEcartSecondPrice := (pVendorPriceBuffer."Net Unit Price" - lBestNetUnitPrice) / lBestNetUnitPrice * 100;
                    IF (lEcartSecondPrice < pRequisitionLine.TriEcartMarge) THEN BEGIN
                        IF (pRequisitionLine.Quantity >= pVendorPriceBuffer."Minimum order") AND
                           (pVendorPriceBuffer."Minimum Qty" <= pRequisitionLine.Quantity) THEN BEGIN
                            // #2# 2Šme meilleur prix
                            pVendorPriceBuffer.Selected := TRUE;
                            pVendorPriceBuffer.Quantity := pRequisitionLine.Quantity;
                            pVendorPriceBuffer.MODIFY();
                            lBreak := TRUE;
                            // #3# Efface meilleur prix
                            pVendorPriceBuffer.SETRANGE(Selected);
                            pVendorPriceBuffer.SETRANGE("Vendor No.");
                            pVendorPriceBuffer.MARKEDONLY(TRUE);
                            IF pVendorPriceBuffer.FINDFIRST() THEN BEGIN
                                pVendorPriceBuffer.Selected := FALSE;
                                pVendorPriceBuffer.Quantity := 0;
                                pVendorPriceBuffer.MODIFY();
                                pVendorPriceBuffer.MARK(FALSE);
                            END;
                        END;
                        //MESSAGE('lBestNetUnitPrice:%1\pVendorPriceBuffer."Net Unit Price":%2\lEcartSecondPrice:%3\Minimum Qty:%4',lBestNetUnitPrice,pVendorPriceBuffer."Net Unit Price", lEcartSecondPrice,pVendorPriceBuffer."Minimum Qty");
                    END
                UNTIL (pVendorPriceBuffer.NEXT() = 0) OR (lBreak);

                pVendorPriceBuffer.SETRANGE(Selected, TRUE);
                pVendorPriceBuffer.MARKEDONLY(FALSE);
                IF pVendorPriceBuffer.FINDFIRST() THEN BEGIN
                    // #3# demande d'achat
                    pRequisitionLine.VALIDATE("Vendor No.", pVendorPriceBuffer."Vendor No.");
                    pRequisitionLine.VALIDATE("Direct Unit Cost", pVendorPriceBuffer."Net Unit Price");
                    pRequisitionLine."Buffer Price Record ID" := pVendorPriceBuffer.RECORDID;
                    pRequisitionLine."Price Qty" := pVendorPriceBuffer."Minimum Qty";
                    pRequisitionLine."Price Selectionned" := TRUE;
                    pRequisitionLine.MODIFY(FALSE);
                END;

            END;
        END;

        pVendorPriceBuffer.RESET();
        pVendorPriceBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
        pVendorPriceBuffer.SETASCENDING("Net Unit Price", TRUE);
    END;

    //CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e par rapport … la quantit‚ minimum du tarif achat
    //CFR Le 18/10/2022 => R‚gie : S‚lectionner par d‚faut le tarif du 2Šme meilleur fournisseur
    //CFR le 20/12/2022 => Correction bug : ne pas proposer de fournisseur pour les articles [Fusion en attente] = True
}

