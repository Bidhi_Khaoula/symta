#pragma warning disable AA0215
codeunit 50062 "Gestion Magasin"
#pragma warning restore AA0215
{

    trigger OnRun()
    begin
    end;

    procedure GetCasierDefaut(_pItemNo: Code[20]): Code[20]
    var
        LBinContent: Record "Bin Content";
    begin
        CLEAR(LBinContent);
        LBinContent.SETRANGE("Item No.", _pItemNo);
        LBinContent.SETRANGE(Default, TRUE);
        IF LBinContent.FINDFIRST() THEN
            EXIT(LBinContent."Bin Code");
    end;
}

