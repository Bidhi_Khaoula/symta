// codeunit 50018 "Insertion Fichiers EDI"
// {


//     trigger OnRun()
//     var
//         CompanyInfo: Record "Company Information";
//         FicEntete: Text[1024];
//         FicLigne: Text[1024];
//     begin
//         CompanyInfo.GET();
//         FicEntete := CompanyInfo."Import EDI Folder" + '\ENT_CDE.TXT';
//         FicLigne := CompanyInfo."Import EDI Folder" + '\LIG_CDE.TXT';

//         // AD Le 31-03-2010 => LOG car certains messages disparaises
//         FichierLog.TEXTMODE(TRUE);
//         FichierLog.WRITEMODE(TRUE);
//         IF FILE.EXISTS('\\edi\MESSAGES-EDI\logedi.txt') THEN
//             FichierLog.OPEN('\\edi\MESSAGES-EDI\logedi.txt')
//         ELSE
//             FichierLog.CREATE('\\edi\MESSAGES-EDI\logedi.txt');
//         FichierLog.SEEK(FichierLog.LEN);
//         FichierLog.WRITE('CU Intégration' + FORMAT(TODAY) + '--' + FORMAT(TIME));
//         // FIN AD Le 31-03-2010



//         IF FILE.EXISTS(FicEntete) THEN
//             InsertHeader(FicEntete)
//         ELSE BEGIN
//             IF GUIALLOWED THEN
//                 MESSAGE(Text003);
//             FichierLog.WRITE('Aucun fichier');
//         END;
//         IF FILE.EXISTS(FicLigne) THEN
//             InsertLine(FicLigne);

//         // AD Le 31-03-2010 => LOG car certains messages disparaises
//         FichierLog.CLOSE;
//         // FIN AD Le 31-03-2010
//     end;

//     var
//         "CU-EDI": Codeunit "Mise à jour code appro";
//         Text001: Label 'No de document %1-%2 déja existant.';
//         Text002: Label 'Ligne %3 du No de document %1-%2 déja existante.';
//         Text003: Label 'Aucun fichier à intégrer !';
//         FichierLog: File;

//     local procedure InsertHeader(NomFic: Text[1024])
//     var
//         TabEnrg: array[800, 50] of Text[1024];
//         EdiHeader: Record "EDI Ordes Header";
//         EnteteEx: Record "EDI Ordes Header";
//         NbEnrg: Integer;
//         j: Integer;
//     begin

//         CLEAR(TabEnrg);
//         LoadFile(NomFic, TabEnrg, NbEnrg);

//         FOR j := 1 TO NbEnrg DO BEGIN
//             FichierLog.WRITE(TabEnrg[j] [1]);
//             CLEAR(EdiHeader);
//             EVALUATE(EdiHeader."EDI Document Type", 'ORDERS');
//             EVALUATE(EdiHeader."EDI Document No", TabEnrg[j] [1]);
//             EVALUATE(EdiHeader."EDI Document Date", TabEnrg[j] [2]);
//             EVALUATE(EdiHeader."Sell-To Customer EDI Code", TabEnrg[j] [3]);
//             EVALUATE(EdiHeader."Ship-To Customer EDI Code", TabEnrg[j] [4]);
//             EVALUATE(EdiHeader."Delivery Date", TabEnrg[j] [5]);
//             EVALUATE(EdiHeader."Delivery instructions 1", TabEnrg[j] [6]);
//             //EVALUATE(EdiHeader."Delivery instructions 2", TabEnrg[j][8]);
//             //EVALUATE(EdiHeader."Delivery instructions 3", TabEnrg[j][9]);
//             //EVALUATE(EdiHeader."Delivery instructions 4", TabEnrg[j][10]);
//             //EVALUATE(EdiHeader."Delivery instructions 5", TabEnrg[j][11]);
//             //EVALUATE(EdiHeader."Delivery instructions 6", TabEnrg[j][12]);
//             //EVALUATE(EdiHeader."Shipping Agent Code", TabEnrg[j][13]);
//             //EVALUATE(EdiHeader."Shipping Agent Name", TabEnrg[j][14]);
//             //EVALUATE(EdiHeader."Delivery Term 1", TabEnrg[j][17]);
//             //EVALUATE(EdiHeader."Delivery Term 2", TabEnrg[j][18]);
//             //EVALUATE(EdiHeader."Currency Code", TabEnrg[j][19]);

//             //IF TabEnrg[j][21] = '' THEN TabEnrg[j][21] := '0';
//             //EVALUATE(EdiHeader."Discount Amount", TabEnrg[j][21]);

//             //IF TabEnrg[j][22] = '' THEN TabEnrg[j][22] := '0';
//             //EVALUATE(EdiHeader."Discount %", TabEnrg[j][22]);

//             //IF TabEnrg[j][24] = '' THEN TabEnrg[j][24] := '0';
//             //EVALUATE(EdiHeader.Amount, TabEnrg[j][24]);

//             EVALUATE(EdiHeader."Comment 1", TabEnrg[j] [7]);
//             EVALUATE(EdiHeader."Comment 2", TabEnrg[j] [8]);
//             EVALUATE(EdiHeader."Comment 3", TabEnrg[j] [9]);
//             EVALUATE(EdiHeader."Comment 4", TabEnrg[j] [10]);
//             //EVALUATE(EdiHeader."Comment 5", TabEnrg[j][11]);

//             IF EnteteEx.GET(EdiHeader."EDI Document Type", EdiHeader."EDI Document No") THEN
//                 "CU-EDI".LogErr(EdiHeader."EDI Document Type", EdiHeader."EDI Document No", STRSUBSTNO(Text001,
//                                 EdiHeader."EDI Document Type", EdiHeader."EDI Document No"))
//             ELSE
//                 EdiHeader.Insert();
//         END;
//     end;

//     local procedure InsertLine(NomFic: Text[1024])
//     var
//         TabEnrg: array[800, 50] of Text[1024];
//         EdiLine: Record "EDI Orders Line";
//         LigneEx: Record "EDI Orders Line";
//         NbEnrg: Integer;
//         j: Integer;
//     begin
//         CLEAR(TabEnrg);
//         LoadFile(NomFic, TabEnrg, NbEnrg);

//         FOR j := 1 TO NbEnrg DO BEGIN
//             CLEAR(EdiLine);
//             EVALUATE(EdiLine."EDI Document Type", 'ORDERS');

//             //EVALUATE(EdiLine."EDI Document Line No.", TabEnrg[j][2]);
//             EVALUATE(EdiLine."EDI Document Line No.", FORMAT(j));

//             EVALUATE(EdiLine."EDI Document No", TabEnrg[j] [1]);
//             EVALUATE(EdiLine."Item ean", TabEnrg[j] [2]);
//             EVALUATE(EdiLine."Item No", TabEnrg[j] [3]);
//             EVALUATE(EdiLine."Description 1", TabEnrg[j] [4]);
//             EVALUATE(EdiLine."Description 2", TabEnrg[j] [5]);
//             //EVALUATE(EdiLine.Vat, TabEnrg[j][10]);

//             //IF TabEnrg[j][11] = '' THEN TabEnrg[j][11] := '0';
//             //EVALUATE(EdiLine."Discount 1", TabEnrg[j][11]);

//             IF TabEnrg[j] [7] = '' THEN TabEnrg[j] [7] := '0';
//             EVALUATE(EdiLine.Quantity, TabEnrg[j] [7]);

//             IF DELCHR(FORMAT(TabEnrg[j] [9]), '=', ' ') = '' THEN TabEnrg[j] [9] := '0';
//             // AD Le 02-10-2009 => Pour gérer tous les format (. ou ;)
//             IF NOT EVALUATE(EdiLine."Net price", TabEnrg[j] [9]) THEN BEGIN
//                 TabEnrg[j] [9] := CONVERTSTR(TabEnrg[j] [9], '.', ',');
//                 IF NOT EVALUATE(EdiLine."Net price", TabEnrg[j] [9]) THEN BEGIN
//                     TabEnrg[j] [9] := CONVERTSTR(TabEnrg[j] [9], ',', '.');
//                     EVALUATE(EdiLine."Net price", TabEnrg[j] [9]);
//                 END;
//             END;

//             EVALUATE(EdiLine."Comment 1", TabEnrg[j] [10]);
//             EVALUATE(EdiLine."Comment 2", TabEnrg[j] [11]);
//             EVALUATE(EdiLine."Comment 3", TabEnrg[j] [12]);
//             EVALUATE(EdiLine."Comment 4", TabEnrg[j] [13]);
//             //EVALUATE(EdiLine."Comment 5", TabEnrg[j][14]);

//             IF LigneEx.GET(EdiLine."EDI Document Type", EdiLine."EDI Document No", EdiLine."EDI Document Line No.") THEN
//                 "CU-EDI".LogErr(EdiLine."EDI Document Type", EdiLine."EDI Document No",
//                                 STRSUBSTNO(Text002, EdiLine."EDI Document Type", EdiLine."EDI Document No", EdiLine."EDI Document Line No.")
//          )
//             ELSE
//                 EdiLine.Insert();

//         END;
//     end;

//     local procedure LoadFile(NomFic: Text[1024]; var TabVar: array[800, 50] of Text[1024]; var Enrg: Integer)
//     var
//         HeaderFile: File;
//         LineFile: BigText;
//         i: Integer;
//         Champ: Integer;
//         Char: Text[1];
//         Lstream: InStream;
//         Char13: Char;
//         Char10: Char;
//         Separateur: Text[1];
//     begin
//         Separateur := '|';
//         Char13 := 13;
//         Char10 := 10;
//         Enrg := 1;
//         Champ := 1;
//         CLEAR(TabVar);

//         HeaderFile.TEXTMODE(TRUE);
//         HeaderFile.OPEN(NomFic);

//         HeaderFile.CREATEINSTREAM(Lstream);
//         LineFile.READ(Lstream);

//         IF LineFile.LENGTH <> 0 THEN BEGIN
//             FOR i := 1 TO LineFile.LENGTH DO BEGIN
//                 LineFile.GETSUBTEXT(Char, i, 1);
//                 IF (Char <> FORMAT(Char13)) AND (Char <> FORMAT(Char10)) THEN BEGIN
//                     IF Char = Separateur THEN
//                         Champ += 1
//                     ELSE
//                         TabVar[Enrg] [Champ] += Char
//                 END
//                 ELSE
//                     IF (Char = FORMAT(Char13)) THEN BEGIN
//                         Champ := 1;
//                         Enrg += 1;
//                     END;
//             END;
//         END;

//         Enrg -= 1;
//     end;

//     procedure ArchiveFichier()
//     var
//         DateFic: Text[50];
//         CompanyInfo: Record "Company Information";
//     begin
//         DateFic := '-' + FORMAT(TODAY, 0, '<day,2>-<month,2>-<Year4>') + '_' + FORMAT(TIME, 0, '<Hours24>-<Minutes,2>');
//         CompanyInfo.GET();

//         IF EXISTS(CompanyInfo."Import EDI Folder" + '\ENT_CDE.TXT') THEN
//             RENAME(CompanyInfo."Import EDI Folder" + '\ENT_CDE.TXT', CompanyInfo."Import EDI Folder" + '\SAVE\ENT_CDE' + DateFic + '.TXT');

//         IF EXISTS(CompanyInfo."Import EDI Folder" + '\LIG_CDE.TXT') THEN
//             RENAME(CompanyInfo."Import EDI Folder" + '\LIG_CDE.TXT', CompanyInfo."Import EDI Folder" + '\SAVE\LIG_CDE' + DateFic + '.TXT');
//     end;
// }

