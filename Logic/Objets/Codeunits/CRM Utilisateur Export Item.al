// codeunit 50086 "CRM Utilisateur Export Item"
// {

//     trigger OnRun()
//     var
//         LSalesHeader: Record "Sales Header";
//     begin
//         //ExportMarketing('XML', 'P:\Navision\Sidamo\export.xml', TRUE);
//         //ExportMarketing('CSV', TEMPORARYPATH + '\export.csv', TRUE); // Fichier dans le dossier temporaires système
//         //ExportMarketing('XLS', '', TRUE); // Pas de nom de fichier nécessaire
//         //LSalesHeader.GET(LSalesHeader."Document Type"::Order, 'C1100209');
//         //ExportFromSalesHeader(LSalesHeader, 'XLS', '', FALSE, FALSE);
//     end;

//     var
//         Export: Codeunit 50085;
//         DIALOG_INFO: Label 'Format : #1####\Article : #2####################';
//         TXT_NOVALUE: Label 'Inconnu';
//         PrevSuperFamilyCode: Code[20];
//         PrevFamilyCode: Code[20];
//         PrevSubFamilyCode: Code[20];
//         MarketingSetup: Record "Marketing Setup";
//         OutputFormat: Code[10];
//         CR: Char;
//         LF: Char;
//         CrLf: Text[2];
//         ESKGarantie: Label 'Garantie ';
//         SimplifiedOn: Boolean;
//         DIALOG_INFO_SALES: Label 'Format : #1####\Document : #2###############\Article : #3####################';
//         ImagePathOn: Boolean;

//     procedure Init(pOutputFormat: Text[5]; pOutputFilename: Text[1024]; pSimplifiedOn: Boolean; pImagePathOn: Boolean)
//     begin
//         Export.InitGlobals(pOutputFormat, pOutputFilename, 'Article');
//         //Export.SetForceCData(TRUE);
//         CLEAR(PrevSuperFamilyCode);
//         CLEAR(PrevFamilyCode);
//         CLEAR(PrevSubFamilyCode);

//         MarketingSetup.GET();
//         OutputFormat := pOutputFormat;
//         SimplifiedOn := pSimplifiedOn; // TRUE pour passer en export simplifié
//         ImagePathOn := pImagePathOn; // TRUE pour inclure les chemins d'images

//         IF Export.GetOutputFormat() = 'XML' THEN
//             SimplifiedOn := TRUE;

//         CR := 13;
//         LF := 10;

//         CrLf[1] := CR;
//         CrLf[2] := LF;
//     end;

//     procedure ExportStart()
//     begin
//         // Démarrage de l'export (ouverture du fichier)
//         Export.ExportStart();
//     end;

//     procedure ExportEnd()
//     begin
//         // Fin de l'export (fermeture du fichier)
//         Export.ExportEnd();
//     end;

//     procedure ExportRecord(pItem: Record Item; cpt: Integer; _pCustomerCode: Code[20])
//     var
//         c: Integer;
//         Value: Text[250];
//         BigTextValue: BigText;
//         TempSalesPrice: Record "Sales Price" temporary;
//         "Sales Price Calc. Mgt.": Codeunit "Sales Price Calc. Mgt.";
//         Price: Decimal;
//         Discount: Decimal;
//         Criteria: Record Equipment;
//         ItemCriteria: Record "Article Critere";
//         ItemSubstitution: Record "Item Substitution";
//         ItemCrossReference: Record "Item Reference";
//         ItemUnitofMeasure: Record "Item Unit of Measure";
//         RecordRef: RecordRef;
//         AddHeaderCriteriaOn: Boolean;
//     begin

//         // Doit-on ajouter une ligne d'entête des critères
//         AddHeaderCriteriaOn := FALSE;
//         // On n'ajoute pas de ligne additionnelle des critères pour le premier article
//         IF (cpt > 1) THEN BEGIN
//             // Un changement de famille implique un ajout d'une ligne d'entête des critères
//             AddHeaderCriteriaOn := NOT ((PrevFamilyCode = pItem."Item Category Code") AND
//                                         (PrevSubFamilyCode = pItem."Item Category Code"))
//                                    // ANI Le 27/11/2014 - Export simplifié
//                                    AND SimplifiedOn;
//         END;

//         // Export de la ligne article
//         Export.InitNewRow;
//         IF (AddHeaderCriteriaOn) THEN BEGIN
//             Export.ForceNewHeaderOn();
//             Export.SetAllowFieldHeader(FALSE);
//         END;
//         // Groupe "Tableau"
//         Export.AddGroupBegin('Tableau');
//         Export.AddField('CodeArticle', pItem."No.");
//         Export.AddField('Désignation', pItem.Description + ' ' + pItem."Description 2");
//         Export.AddField('PrixPublic', Export.FormatDecimal(Price));
//         Export.SetForceText(TRUE);
//         Export.AddField('Gendcode', pItem."Gencod EAN13");
//         Export.AddField('CodeFamilleMarketing', pItem."Item Category Code");
//         Export.AddField('CodeSousFamilleMarketing', pItem."Item Category Code");
//         Export.AddField('CodeMarketing', pItem."Code Marketing");
//         Export.AddField('LibelleMarketing', pItem."Libelle Marketing");
//         Export.AddField('PaysFabrication', pItem."Country/Region Purchased Code");
//         Export.AddField('PoidsNetkg', pItem."Net Weight");
//         Export.AddField('PoidsBrut', pItem."Gross Weight");
//         Export.AddField('CodeDouanier', pItem."Tariff No.");
//         Export.AddField('DateCreation', pItem."Create Date");
//         // Articles de remplacement
//         Value := '';
//         ItemSubstitution.SETRANGE("No.", pItem."No.");
//         IF ItemSubstitution.FINDFIRST () THEN BEGIN
//             REPEAT
//                 Export.StrAddValue(Value, ItemSubstitution."Sub. Item No.");
//             UNTIL ItemSubstitution.NEXT <= 0;
//         END;
//         Export.AddField('ArticleRemplacement', Value);
//         // [FIN] Articles de remplacement
//         // Références externes
//         Value := '';
//         ItemCrossReference.SETRANGE("Item No.", pItem."No.");
//         IF _pCustomerCode = '' THEN
//             ItemCrossReference.SETFILTER("Reference Type", '<>%1',
//                   ItemCrossReference."Reference Type"::Customer)
//         ELSE BEGIN
//             ItemCrossReference.SETRANGE("Reference Type", ItemCrossReference."Reference Type"::Customer);
//             ItemCrossReference.SETRANGE("Reference Type No.", _pCustomerCode);
//         END;

//         IF ItemCrossReference.FINDFIRST () THEN BEGIN
//             REPEAT
//                 Export.StrAddValue(Value, ItemCrossReference."Reference No.");
//             UNTIL ItemCrossReference.NEXT <= 0;
//         END;
//         Export.AddField('RefExernes', Value);
//         // [FIN] Références externes
//         Export.AddField('GroupeTVA', pItem."VAT Prod. Posting Group");

//         // Critères[X]
//         IF SimplifiedOn THEN BEGIN
//             // version simplifiée : colonne par critère pour chaque ligne
//             ItemCriteria.SETCURRENTKEY("Code Article", ItemCriteria.Priorité);
//             ItemCriteria.SETRANGE("Code Article", pItem."No.");
//             c := 0;
//             Export.SetAllowFieldHeader(TRUE);
//             IF ItemCriteria.FINDFIRST () THEN BEGIN
//                 REPEAT
//                     c := c + 1;

//                     IF Criteria.GET(ItemCriteria."Code Critere") THEN BEGIN
//                         IF (Export.GetOutputFormat() <> 'XML') THEN BEGIN
//                             // On ajoute les critères de la première ligne comme si c'était une ligne normale (entête + valeur)
//                             Export.AddField(Criteria.Nom, GetCriteriaValue(Criteria, ItemCriteria)); // + ' ' + Criteria.Unité);
//                         END ELSE BEGIN
//                             Export.AddFieldWithIdx('NomCritere', Criteria.Nom, c);
//                             Export.AddFieldWithIdx('ValeurCritere', GetCriteriaValue(Criteria, ItemCriteria), c);
//                             Export.AddFieldWithIdx('UniteCritere', Criteria.Model, c); // AD Le 15-09-2014 => POB -> Mis en commentaire
//                         END;
//                     END;
//                 UNTIL (ItemCriteria.NEXT() = 0);
//             END;
//         END ELSE BEGIN
//             // ANI Le 27/11/2014 - Export simplifié
//             // version complète : tous les critères, puis valeur dans la colonne qui va bien
//             IF (Export.GetOutputFormat() <> 'XML') THEN BEGIN
//                 Criteria.RESET();
//                 Criteria.SETCURRENTKEY(Nom); // Tri aplhanumérique
//                 REPEAT
//                     ItemCriteria.RESET();
//                     ItemCriteria.SETCURRENTKEY("Code Article", ItemCriteria.Priorité);
//                     ItemCriteria.SETRANGE("Code Article", pItem."No.");
//                     ItemCriteria.SETRANGE("Code Critere", Format(Criteria."Equipment No."));

//                     IF ItemCriteria.FINDFIRST () THEN BEGIN
//                         Export.AddField(Criteria.Nom, GetCriteriaValue(Criteria, ItemCriteria));
//                     END ELSE BEGIN
//                         Export.AddField(Criteria.Nom, '');
//                     END;
//                 UNTIL Criteria.NEXT() = 0;
//             END ELSE BEGIN

//             END;

//         END;
//         // [FIN] Critères

//         // Fin du groupe "Tableau"
//         Export.AddGroupEnd('Tableau');

//         // Fin de ligne
//         Export.AddEnd;

//         PrevFamilyCode := pItem."Item Category Code";
//         PrevSubFamilyCode := pItem."Product Group Code";
//     end;

//     procedure GetFamilyDescr("Code": Code[10]; Type: Option) Result: Text[1024]
//     var
//         FamilyHierachy: Record "Item Category";
//     begin
//         Result := TXT_NOVALUE;
//         IF (Code <> '') THEN BEGIN
//             IF (FamilyHierachy.GET(Code, Type)) THEN BEGIN
//                 Result := FamilyHierachy.Description;
//             END;
//         END;
//     end;

//     procedure GetParamDescr("Code": Code[10]; Type: Code[10]) Result: Text[1024]
//     var
//         Parameters: Record "Liste Fichiers / Répertoires";
//     begin
//         Result := GetParamDescrWithDefault(Code, Type, TXT_NOVALUE);
//     end;

//     procedure GetParamDescrWithDefault("Code": Code[10]; Type: Code[10]; Default: Text[1024]) Result: Text[1024]
//     var
//         Parameters: Record "Generals Parameters";
//     begin
//         Result := TXT_NOVALUE;
//         IF Parameters.GET(Type, Code) THEN BEGIN
//             Result := Parameters.ShortDescription;
//         END;
//     end;

//     procedure GetCriteriaValue(Criteria: Record Equipment; ItemCriteria: Record "Article Critere"): Text[1024]
//     var
//         CriteriaValue: Variant;
//     begin

//         CASE Criteria.Manufacturer OF
//             Criteria.Manufacturer::"0":
//                 BEGIN
//                     CriteriaValue := FORMAT(ItemCriteria.Booléen);
//                 END;
//             Criteria.Manufacturer::"1":
//                 BEGIN
//                     CriteriaValue := ItemCriteria.Text;
//                 END;
//             Criteria.Manufacturer::"2":
//                 BEGIN
//                     CriteriaValue := ItemCriteria.Décimal;
//                 END;
//             Criteria.Manufacturer::"3":
//                 BEGIN
//                     CriteriaValue := ItemCriteria.Entier;
//                 END;
//             ELSE BEGIN
//                 CriteriaValue := '';
//             END;
//         END;

//         EXIT(FORMAT(CriteriaValue));
//     end;

//     procedure "-- Export globaux --"()
//     begin
//     end;

//     procedure ExportMarketing(pOutputFormat: Text[5]; pOutputFilename: Text[1024]; pSimplifiedOn: Boolean; pImagePathOn: Boolean)
//     var
//         Window: Dialog;
//         Item: Record Item;
//         cpt: Integer;
//     begin

//         Init(pOutputFormat, pOutputFilename, pSimplifiedOn, pImagePathOn);

//         // Démarrage de l'export (ouverture du fichier)
//         ExportStart;

//         // Export des données
//         Window.OPEN(DIALOG_INFO);
//         Window.UPDATE(1, pOutputFormat);

//         // Bouclage sur les articles
//         Item.RESET();
//         //Item.SETRANGE("No.", '21300015');
//         IF (Item.FINDFIRST) THEN BEGIN
//             cpt := 0;
//             REPEAT
//                 cpt := cpt + 1;
//                 ExportRecord(Item, cpt, '');

//                 // Avancement
//                 Window.UPDATE(2, Item.Description);

//             UNTIL Item.NEXT() = 0;
//         END;

//         // Fin de l'export (fermeture du fichier)
//         ExportEnd;
//     end;

//     procedure ExportFromSalesHeader(pSalesHeader: Record "Sales Header"; pOutputFormat: Text[5]; pOutputFilename: Text[1024]; pSimplifiedOn: Boolean; pImagePathOn: Boolean)
//     var
//         LSalesLine: Record "Sales Line";
//         LItem: Record Item;
//         Window: Dialog;
//         cpt: Integer;
//     begin
//         Init(pOutputFormat, pOutputFilename, pSimplifiedOn, pImagePathOn);

//         // Démarrage de l'export (ouverture du fichier)
//         ExportStart;

//         // Export des données
//         Window.OPEN(DIALOG_INFO_SALES);
//         Window.UPDATE(1, pOutputFormat);
//         Window.UPDATE(2, pSalesHeader."No.");

//         // Bouclage sur les lignes
//         LSalesLine.RESET();
//         LSalesLine.SETRANGE("Document Type", pSalesHeader."Document Type");
//         LSalesLine.SETRANGE("Document No.", pSalesHeader."No.");
//         LSalesLine.SETRANGE(Type, LSalesLine.Type::Item); // uniquement les lignes article

//         IF LSalesLine.FINDFIRST () THEN BEGIN
//             cpt := 0;
//             REPEAT
//                 cpt := cpt + 1;
//                 CLEAR(LItem);
//                 IF LItem.GET(LSalesLine."No.") THEN BEGIN
//                     ExportRecord(LItem, cpt, '');

//                     // Avancement
//                     Window.UPDATE(3, LItem.Description);
//                 END;
//             UNTIL LSalesLine.NEXT() = 0;

//         END;

//         // Fin de l'export (fermeture du fichier)
//         ExportEnd;
//     end;
// }

