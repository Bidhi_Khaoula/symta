codeunit 50012 "Edition Etiquette LOGIFLUX"
{
    trigger OnRun()
    var
        lShippingLabel: Record "Shipping Label";
    begin

        IF lShippingLabel.GET(4670676) THEN 
            EditionEtiquetteLogiflux(lShippingLabel, 'M');
         MESSAGE('OK');
    end;

    var
        LigneTitre: Text[1024];

    procedure EditionEtiquetteLogiflux("Shipping Label": Record "Shipping Label"; CodeAction: Code[1])
    var
        LCust: Record Customer;     
        Transporteur: Record "Shipping Agent";
        CompanyInfo: Record "Company Information";
        Trafic: Record "Trafic Transporteur";
        WhseEmployee: Record "Warehouse Employee";
        lTeliaeUMDetail: Record "Teliae UM Detail";
         LSalesShptHeader: Record "Sales Shipment Header";
            FichierEtq: File;
        MonFichierEtq: Text[100];
        sep: Code[1];
        Ligne: Text[1024];
        _DatePch: Date;
        "_CodeExpéditeur": Code[10];
        "_NomExpéditeur": Code[30];
        "_AdresseExpéditeur": Text[30];
        "_Adresse2Expéditeur": Text[30];
        "_CodePostalExpéditeur": Code[10];
        "_VilleExpéditeur": Code[30];
        "_TelExpéditeur": Code[30];
           "_RefExpédition": Code[30];
        _CodeDestinataire: Code[10];
        _ContreRemboursement: Code[10];
        _ValeurDeclare: Code[10];
        _InstructionLivraison: Text[150];
        _NomDestinataire: Text[50];
        _AdresseDestinataire: Text[50];
        _Adresse2Destinataire: Text[50];
        _VilleDestinataire: Text[50];
        _CodePostalDestinataire: Code[10];
        _PaysDestinataire: Text[30];
        _TelDestinataire: Code[30];
        _NbColis: Text[10];
        _NbPalette: Text[10];
        _Poids: Text[10];
        _TypeMarchandise: Code[30];
         _Transporteur: Code[10];
        _PrestationTransport: Code[10];
        _NbColisTotal: Code[10];
        _CodeDevise: Code[10];
        "_Préparateur": Text[30];
        _CodePort: Code[10];
        _SoumisTva: Code[10];
        _DateLivrImperative: Date;
        _NbEtiquette: Code[10];
        _NbColisTotalAssure: Code[10];
        _TypeUM: Code[1];
    begin
        IF CodeAction = '' THEN
            ERROR('Code Action ne doit pas être vide');

        CompanyInfo.GET();

        Transporteur.GET("Shipping Label"."Shipping Agent Code");

        IF UPPERCASE(USERID) = 'SYMTA\ESKAPE' THEN 
            MonFichierEtq := STRSUBSTNO('%1\%2.CSV', '\\srv-avimp\ETIQUETTES\TESTESKAPE\',
            "Shipping Label"."Entry No.")
        ELSE
            MonFichierEtq := STRSUBSTNO('%1\%2.CSV', Transporteur."Label Folder",
            "Shipping Label"."Entry No.");
        FichierEtq.CREATE(MonFichierEtq, TEXTENCODING::Windows);
        FichierEtq.TEXTMODE(TRUE);

        sep := ';';

        // --------------------------
        _NomExpéditeur := CompanyInfo."Ship-to Name";
        _AdresseExpéditeur := CompanyInfo.Address;
        _Adresse2Expéditeur := CompanyInfo."Address 2";
        _CodePostalExpéditeur := CompanyInfo."Post Code";
        _VilleExpéditeur := CompanyInfo.City;
        _TelExpéditeur := CompanyInfo."Phone No.";
        // -----------------------------

        _InstructionLivraison := COPYSTR("Shipping Label"."Instruction of Delivery", 1, 32);
        _TypeMarchandise := 'PIECES MACHINES AGRICOLES';
        _RefExpédition := "Shipping Label"."BL No.";

        IF STRLEN(_RefExpédition) > 10 THEN
            _RefExpédition := COPYSTR(_RefExpédition, 4, STRLEN(_RefExpédition));

        _DatePch := "Shipping Label"."Date of  bl";
        _NbColis := FORMAT("Shipping Label"."Nb Of Box Vrac", 0, '<integer,3><Filler Character,0>');
        _NbPalette := FORMAT("Shipping Label"."Nb Of Pallets", 0, '<integer,3><Filler Character,0>');
        _NbColisTotal := FORMAT("Shipping Label"."Nb Of Box Vrac" + "Shipping Label"."Nb Of Box"
        , 0, '<integer,3><Filler Character,0>');

        _Poids := FORMAT("Shipping Label".Weight, 0, '<Precision,2:2><Standard Format,1>');

        // AD Le 29-09-2010 => Pour avoir les décimals
        //_ContreRemboursement := FORMAT("Shipping Label"."Cash on delivery", 0, '<integer,5><Filler Character,0><Precision,2:2>');


        IF "Shipping Label"."Cash on delivery" = 0 THEN
            IF "Shipping Label"."Mode d'expédition" IN [
            "Shipping Label"."Mode d'expédition"::"Contre Rembourssement Express",
            "Shipping Label"."Mode d'expédition"::"Contre Rembourssement Normal",
            "Shipping Label"."Mode d'expédition"::"Contre Rembourssement Samedi"] THEN
                IF LSalesShptHeader.GET("Shipping Label"."BL No.") THEN
                    "Shipping Label"."Cash on delivery" := LSalesShptHeader.GetAmount() * 1.2; // A VOIR


        _ContreRemboursement := FORMAT("Shipping Label"."Cash on delivery", 0,
                       '<Precision,2:2><Filler Character,0><Integer,5><Filler Character,0><Decimals,3><Comma,.>');
        // FIN AD Le 29-09-2010
        _ValeurDeclare := '0';
        _CodeDevise := 'EUR';

        IF "Shipping Label"."Assured Box" THEN BEGIN
            _NbColisTotalAssure := _NbColisTotal;
            IF LSalesShptHeader.GET("Shipping Label"."BL No.") THEN
                _ValeurDeclare := FORMAT(LSalesShptHeader.GetAmount() - LSalesShptHeader.GetPortAmount());
        END;

        _NbEtiquette := FORMAT("Shipping Label"."Nombre d'étiquette");
        IF _NbEtiquette = '0' THEN
            _NbEtiquette := _NbColisTotal;

        WhseEmployee.GET(USERID, CompanyInfo."Location Code");
        _Préparateur := FORMAT(WhseEmployee."Code Remettant Logiflux");


        _CodeExpéditeur := DELCHR("Shipping Label"."Network Code", '<', '0');
        IF _CodeExpéditeur = '' THEN
            _CodeExpéditeur := 'SP';

        _CodeDestinataire := "Shipping Label"."Recipient Code";
        _NomDestinataire := "Shipping Label"."Recipient Name";
        _AdresseDestinataire := "Shipping Label"."Recipient Address";
        _Adresse2Destinataire := "Shipping Label"."Recipient Address 2";
        _CodePostalDestinataire := "Shipping Label"."Recipient Post Code";
        _VilleDestinataire := "Shipping Label"."Recipient City";
        _PaysDestinataire := "Shipping Label"."Recipient Country Code";
        IF _PaysDestinataire = '' THEN
            _PaysDestinataire := 'FR';
        //_ContactDestinataire := "Shipping Label"."Recipient Person"; => Demande de franck -> Pas de contact
        _TelDestinataire := "Shipping Label"."Recipient Phone No.";

        IF _TelDestinataire = '' THEN BEGIN
            LCust.GET("Shipping Label"."Recipient Code");
            _TelDestinataire := LCust."Phone No.";

        END;

        IF NOT Trafic.GET('', "Shipping Label"."Mode d'expédition".asinteger() - 1, "Shipping Label"."Shipping Agent Code",
               "Shipping Label"."Recipient Country Code") THEN
            Trafic.GET('', "Shipping Label"."Mode d'expédition".asinteger() - 1, "Shipping Label"."Shipping Agent Code", '');


        _Transporteur := Trafic."Code Transporteur Logiflux";
        _PrestationTransport := FORMAT(Trafic."Code Prestation Logiflux");
        _DateLivrImperative := "Shipping Label"."Imperative date of Delivery";


        // MCO Le 27-07-2015 => FE20150721 -> SYMTA
        IF (DATE2DWY("Shipping Label"."Imperative date of Delivery", 1) <> 6) AND (Transporteur."Date imperative Saturday only") THEN
            _DateLivrImperative := 0D;
        // MCO Le 27-07-2015 => FE20150721 -> SYMTA

        IF TRUE THEN BEGIN
            FctLigneTitre();
            FichierEtq.WRITE(LigneTitre);
        END;

        // --- Ligne étiquette --- //
        Ligne := '';
        // --- FE20190930 Détail Colis >> ajout préfixe BL--- //
        Ligne := 'BL' + sep;
        // --- Fin FE20190930 Détail Colis --- //
        Ligne += _Transporteur + sep;
        Ligne += _PrestationTransport + sep;// Code prestation
        Ligne += _Préparateur + sep;
        Ligne += _RefExpédition + sep;
        Ligne += FORMAT(_DatePch, 0, '<Day,2>') + sep;
        Ligne += FORMAT(_DatePch, 0, '<Month,2>') + sep;
        Ligne += FORMAT(_DatePch, 0, '<Year,2>') + sep;
        Ligne += _CodeDestinataire + sep;
        Ligne += _NomDestinataire + sep;
        Ligne += _AdresseDestinataire + sep;
        Ligne += _Adresse2Destinataire + sep;
        Ligne += _CodePostalDestinataire + sep;
        Ligne += _VilleDestinataire + sep;
        Ligne += _PaysDestinataire + sep;        // 10
        Ligne += _TypeMarchandise + sep;       // 10
        Ligne += _TelDestinataire + sep;
        Ligne += _NbColisTotal + sep;
        Ligne += _Poids + sep;
        Ligne += _NbColisTotalAssure + sep;         // --- ATTENTION ---  // COLIS ASSURE
        Ligne += _CodePort + sep;
        Ligne += _SoumisTva + sep;
        Ligne += _ContreRemboursement + sep;
        Ligne += _CodeDevise + sep;
        Ligne += _ValeurDeclare + sep;
        Ligne += _CodeDevise + sep;
        Ligne += FORMAT(_DateLivrImperative, 0, '<Day,2>') + sep;
        Ligne += FORMAT(_DateLivrImperative, 0, '<Month,2>') + sep;
        Ligne += FORMAT(_DateLivrImperative, 0, '<Year,2>') + sep;
        Ligne += _NomExpéditeur + sep;
        Ligne += _AdresseExpéditeur + sep;
        Ligne += _Adresse2Expéditeur + sep;
        Ligne += _CodePostalExpéditeur + sep;
        Ligne += _VilleExpéditeur + sep;
        Ligne += 'FR' + sep;        // 10
        Ligne += _NbEtiquette + sep;
        Ligne += _CodeExpéditeur + sep;

        FichierEtq.WRITE(Ligne);

        // --- FE20190930 Détail Colis --- //
        lTeliaeUMDetail.SETRANGE("No. BP", "Shipping Label"."Preparation No.");
        IF lTeliaeUMDetail.FINDSET() THEN
            REPEAT

                IF lTeliaeUMDetail."Type UM" = lTeliaeUMDetail."Type UM"::Cardboard THEN _TypeUM := '0';
                IF lTeliaeUMDetail."Type UM" = lTeliaeUMDetail."Type UM"::Pallet THEN _TypeUM := '1';

                Ligne := '';
                Ligne += 'UM' + sep;
                Ligne += '' + sep;  // Code barre transporteur
                Ligne += lTeliaeUMDetail."Reference UM" + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Weight UM (Kg)", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Volume UM (m3)", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Length UM (cm)", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Width UM (cm)", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Height UM (cm)", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += FORMAT(lTeliaeUMDetail."Information UM", 0, '<Precision,2:2><Standard Format,1>') + sep;
                Ligne += '' + sep; // Code barre client
                Ligne += '' + sep; // Code barre de tri
                Ligne += 'A' + sep; //Code action : [A]=ajout - [M]=Modification
                Ligne += _RefExpédition + sep;
                Ligne += '' + sep; // Numero de récépissé
                Ligne += _TypeUM + sep; // Type UM : [0]=Colis - [1]=PAL1
                Ligne += '' + sep; // Texte1
                Ligne += '' + sep; // Texte2
                Ligne += '' + sep; // Texte3
                Ligne += '' + sep; // Texte4
                Ligne += '' + sep; // Texte5
                Ligne += _Préparateur + sep;
                FichierEtq.WRITE(Ligne);
            UNTIL lTeliaeUMDetail.NEXT() = 0;
        // --- Fin FE20190930 Détail Colis --- //

        FichierEtq.CLOSE();
    end;

    procedure FctLigneTitre()
       begin

        //Suppression de la ligne d'en-tête VD le 03/02/2019 -- Changement de logiciel station-chargeur
        //Passage de Eticup à Teliflux
        // sep := ';';
        // LigneTitre += 'Code Transporteur' + sep;
        // LigneTitre += 'Code Produit' + sep;
        // LigneTitre += 'Preparateur' + sep;
        // LigneTitre += 'No BL' + sep;
        // LigneTitre += 'Jour date bl' + sep;
        // LigneTitre += 'Mois date_bl' + sep;
        // LigneTitre += 'Année date_bl' + sep;
        // LigneTitre += 'Destinataire code' + sep;
        // LigneTitre += 'Destinataire nom' + sep;
        // LigneTitre += 'Destinataire adresse1' + sep;
        // LigneTitre += 'Destinataire adresse2' + sep;
        // LigneTitre += 'Destinataire code postal' + sep;
        // LigneTitre += 'Destinataire ville' + sep;
        // LigneTitre += 'Destinataire code pays' + sep;
        // LigneTitre += 'Nature Marchandise' + sep;
        // LigneTitre += 'Destinataire telephone' + sep;
        // LigneTitre += 'nb_colis_vrac' + sep;
        // LigneTitre += 'poids' + sep;
        // LigneTitre += 'nb_colis_assures' + sep;
        // LigneTitre += 'code port' + sep;
        // LigneTitre += 'soumis tva' + sep;
        // LigneTitre += 'mont_contre_remb' + sep;
        // LigneTitre += 'dev_contre_remb' + sep;
        // LigneTitre += 'mont_valeur_dec' + sep;
        // LigneTitre += 'dev_valeur_dec' + sep;
        // LigneTitre += 'Date date_liv_imper' + sep;
        // LigneTitre += 'Mois date_liv_imper' + sep;
        // LigneTitre += 'Année date_liv_imper' + sep;
        // LigneTitre += 'cheq_nom' + sep;
        // LigneTitre += 'cheq_adresse1' + sep;
        // LigneTitre += 'cheq_adresse2' + sep;
        // LigneTitre += 'cheq_c_postal' + sep;
        // LigneTitre += 'cheq_ville' + sep;
        // LigneTitre += 'cheq_c_pays' + sep;
        // LigneTitre += 'Nb étiquette' + sep;
        // LigneTitre += 'c_remettant' + sep;
    end;
}

