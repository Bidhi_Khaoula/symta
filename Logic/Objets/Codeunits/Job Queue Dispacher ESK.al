// codeunit 50031 "Job Queue Dispacher ESK"
// {
//     SingleInstance = true;

//     trigger OnRun()
//     begin
//         HandleRequest;
//     end;

//     var
//         JobQueueProcess: Record "Job Queue Category";
//         SessionID: Integer;
//         LastCleanupTime: DateTime;

//     local procedure HandleRequest()
//     var
//         JobQueueSetup: Record "Scheduled Task";
//         JobQueueEntry: Record "Job Queue Entry";
//         JobQueueResponse: Record "473";
//         JobLogEntryNo: Integer;
//         WasSuccess: Boolean;
//         ThisSessionIsActive: Boolean;
//         MoreRequests: Boolean;
//     begin
//         JobQueueSetup.GET();
//         IF NOT JobQueueSetup."Job Queue Active" THEN
//             EXIT;

//         ThisSessionIsActive := UpdateJobQueueSession(JobQueueEntry, FALSE);
//         COMMIT;
//         IF ThisSessionIsActive THEN
//             MoreRequests := GetNextRequest(JobQueueEntry);  // locks table

//         WHILE ThisSessionIsActive AND MoreRequests DO BEGIN
//             JobLogEntryNo := InsertLogEntry(JobQueueEntry);
//             ThisSessionIsActive := UpdateJobQueueSession(JobQueueEntry, TRUE);
//             COMMIT;
//             WasSuccess := CODEUNIT.RUN(CODEUNIT::"Job Queue Start Codeunit", JobQueueEntry);
//             IF WasSuccess THEN BEGIN
//                 IF JobQueueEntry."Recurring Job" THEN BEGIN
//                     JobQueueEntry."No. of Attempts to Run" := 0;
//                     JobQueueEntry.Status := JobQueueEntry.Status::Ready;
//                     IF NOT JobQueueEntry.MODIFY THEN
//                         JobQueueEntry.Insert();
//                 END;
//             END ELSE BEGIN
//                 IF JobQueueEntry."Maximum No. of Attempts to Run" > JobQueueEntry."No. of Attempts to Run" THEN BEGIN
//                     JobQueueEntry."No. of Attempts to Run" := JobQueueEntry."No. of Attempts to Run" + 1;
//                     JobQueueEntry.Status := JobQueueEntry.Status::Ready;
//                     IF NOT JobQueueEntry.INSERT THEN
//                         JobQueueEntry.MODIFY();
//                 END;
//             END;
//             InsertResponse(JobQueueEntry, JobQueueResponse, WasSuccess);
//             UpdateLogEntry(JobLogEntryNo, WasSuccess);
//             CLEARLASTERROR;
//             ThisSessionIsActive := UpdateJobQueueSession(JobQueueEntry, FALSE);
//             COMMIT;
//             IF ThisSessionIsActive THEN
//                 MoreRequests := GetNextRequest(JobQueueEntry);  // locks table
//         END;
//         COMMIT;
//         CleanUpJobQueue;
//         COMMIT;
//     end;

//     local procedure GetNextRequest(var JobQueueEntry: Record "Job Queue Entry"): Boolean
//     var
//         Found: Boolean;
//     begin
//         JobQueueEntry.LOCKTABLE;
//         JobQueueEntry.SETFILTER("Expiration Date/Time", '>%1|%2', CURRENTDATETIME, CREATEDATETIME(0D, 0T));
//         JobQueueEntry.SETFILTER("Earliest Start Date/Time", '<=%1', CURRENTDATETIME);
//         IF JobQueueProcess."Maximum Job Queue Priority" <> 0 THEN
//             JobQueueEntry.SETRANGE(Priority, JobQueueProcess."Minimum Job Queue Priority", JobQueueProcess."Maximum Job Queue Priority")
//         ELSE
//             IF JobQueueProcess."Minimum Job Queue Priority" <> 0 THEN
//                 JobQueueEntry.SETFILTER(Priority, '>=%1', JobQueueProcess."Maximum Job Queue Priority")
//             ELSE
//                 JobQueueEntry.SETRANGE(Priority);
//         JobQueueEntry.SETRANGE(Status, JobQueueEntry.Status::Ready);
//         JobQueueEntry.SETCURRENTKEY(Priority);
//         Found := JobQueueEntry.FINDFIRST();
//         IF Found THEN BEGIN
//             JobQueueEntry.CALCFIELDS(XML);
//             IF JobQueueEntry."Recurring Job" THEN BEGIN
//                 JobQueueEntry."Earliest Start Date/Time" := CalcNextRunTime(JobQueueEntry);
//                 JobQueueEntry.Status := JobQueueEntry.Status::"In Process";
//                 JobQueueEntry.MODIFY();
//             END ELSE
//                 JobQueueEntry.DELETE;
//         END;
//         EXIT(Found);
//     end;

//     local procedure InsertLogEntry(var JobQueueEntry: Record "Job Queue Entry"): Integer
//     var
//         JobQueueLogEntry: Record "Job Queue Log Entry";
//     begin
//         JobQueueLogEntry.INIT();
//         JobQueueLogEntry.ID := JobQueueEntry.ID;
//         JobQueueLogEntry."User ID" := JobQueueEntry."User ID";
//         JobQueueLogEntry."Start Date/Time" := CURRENTDATETIME;
//         JobQueueLogEntry."Object Type to Run" := JobQueueEntry."Object Type to Run";
//         JobQueueLogEntry."Object ID to Run" := JobQueueEntry."Object ID to Run";
//         JobQueueLogEntry.INSERT(TRUE);
//         EXIT(JobQueueLogEntry."Entry No.");
//     end;

//     local procedure UpdateLogEntry(LogEntryNo: Integer; WasSuccess: Boolean)
//     var
//         JobQueueLogEntry: Record "Job Queue Log Entry";
//     begin
//         JobQueueLogEntry.GET(LogEntryNo);
//         JobQueueLogEntry."End Date/Time" := CURRENTDATETIME;
//         IF WasSuccess THEN
//             JobQueueLogEntry.Status := JobQueueLogEntry.Status::Success
//         ELSE BEGIN
//             JobQueueLogEntry.Status := JobQueueLogEntry.Status::Error;
//             JobQueueLogEntry.SetErrorMessage(COPYSTR(GETLASTERRORTEXT, 1, 1000));
//         END;
//         JobQueueLogEntry.MODIFY();
//     end;

//     local procedure InsertResponse(var JobQueueEntry: Record "Job Queue Entry"; var JobQueueResponse: Record "473"; WasSuccess: Boolean)
//     begin
//         JobQueueResponse.INIT();
//         JobQueueResponse.ID := JobQueueEntry.ID;
//         JobQueueResponse."User ID" := JobQueueEntry."User ID";
//         JobQueueResponse.XML := JobQueueEntry.XML;
//         JobQueueResponse."Creation Date/Time" := CURRENTDATETIME;
//         IF WasSuccess THEN
//             JobQueueResponse.Status := JobQueueResponse.Status::Success
//         ELSE BEGIN
//             JobQueueResponse.Status := JobQueueResponse.Status::Error;
//             JobQueueResponse.SetErrorMessage(COPYSTR(GETLASTERRORTEXT, 1, 1000));
//         END;
//         IF NOT JobQueueResponse.INSERT THEN
//             JobQueueResponse.MODIFY();
//     end;

//     local procedure CalcNextRunTime(var JobQueueEntry: Record "Job Queue Entry"): DateTime
//     var
//         NewRunTime: Time;
//         NewRunDateTime: DateTime;
//         RunOnDate: array[7] of Boolean;
//         CurrWeekDay: Integer;
//         NoOfDays: Integer;
//         Found: Boolean;
//     begin
//         RunOnDate[7] := JobQueueEntry."Run on Sundays";
//         RunOnDate[1] := JobQueueEntry."Run on Mondays";
//         RunOnDate[2] := JobQueueEntry."Run on Tuesdays";
//         RunOnDate[3] := JobQueueEntry."Run on Wednesdays";
//         RunOnDate[4] := JobQueueEntry."Run on Thursdays";
//         RunOnDate[5] := JobQueueEntry."Run on Fridays";
//         RunOnDate[6] := JobQueueEntry."Run on Saturdays";

//         IF JobQueueEntry."No. of Minutes between Runs" > 0 THEN BEGIN
//             NewRunTime := TIME + 60000 * JobQueueEntry."No. of Minutes between Runs";
//             IF (JobQueueEntry."Ending Time" = 0T) AND (JobQueueEntry."No. of Minutes between Runs" * 60000 > 235900T - TIME) OR
//               (JobQueueEntry."Ending Time" <> 0T) AND
//                 (JobQueueEntry."No. of Minutes between Runs" * 60000 > JobQueueEntry."Ending Time" - TIME)
//             THEN BEGIN
//                 NewRunTime := JobQueueEntry."Starting Time";
//                 NoOfDays := 1;
//             END;
//         END ELSE BEGIN
//             NewRunTime := DT2TIME(JobQueueEntry."Earliest Start Date/Time");
//             NoOfDays := 1;
//         END;
//         ;

//         CurrWeekDay := DATE2DWY(TODAY, 1);
//         Found := RunOnDate[(CurrWeekDay - 1 + NoOfDays) MOD 7 + 1];
//         WHILE NOT Found AND (NoOfDays < 7) DO BEGIN
//             NoOfDays := NoOfDays + 1;
//             Found := RunOnDate[(CurrWeekDay - 1 + NoOfDays) MOD 7 + 1];
//         END;
//         IF NOT Found THEN
//             NoOfDays := 10000;

//         NewRunDateTime := CREATEDATETIME(TODAY + NoOfDays, NewRunTime);
//         EXIT(NewRunDateTime);
//     end;

//     local procedure InsertJobQueueSession()
//     var
//         Session: Record "2000000009";
//         RecRef: RecordRef;
//         FieldRef: FieldRef;
//         Found: Boolean;
//         i: Integer;
//     begin
//         RecRef.OPEN(DATABASE::Session);
//         WHILE (i < RecRef.FIELDCOUNT) AND NOT Found DO BEGIN
//             i := i + 1;
//             FieldRef := RecRef.FIELDINDEX(i);
//             Found := FieldRef.NAME = Session.FIELDNAME("My Session");
//         END;
//         IF NOT Found THEN
//             EXIT;
//         FieldRef.SETRANGE(TRUE);
//         IF RecRef.FINDFIRST () THEN BEGIN
//             FieldRef := RecRef.FIELDINDEX(1); // sessionID
//             SessionID := FieldRef.VALUE;
//             IF JobQueueProcess.GET(SessionID) THEN
//                 JobQueueProcess.DELETE;
//             JobQueueProcess.INIT();
//             JobQueueProcess."Session ID" := SessionID;
//             JobQueueProcess."User ID" := USERID;
//             JobQueueProcess."Last Job Processed On" := CURRENTDATETIME;
//             JobQueueProcess.Active := TRUE;
//             JobQueueProcess.Insert();
//         END;
//         COMMIT;
//     end;

//     procedure UpdateJobQueueSession(var JobQueueEntry: Record "Job Queue Entry"; IsStartOfJob: Boolean): Boolean
//     begin
//         IF IsStartOfJob OR (CURRENTDATETIME > JobQueueProcess."Last Job Processed On" + 10000) THEN BEGIN
//             IF NOT JobQueueProcess.GET(SessionID) THEN
//                 JobQueueProcess.Insert();
//             JobQueueProcess."Last Job Processed On" := CURRENTDATETIME;
//             IF IsStartOfJob THEN BEGIN
//                 JobQueueProcess."Object Type Running" := JobQueueEntry."Object Type to Run";
//                 JobQueueProcess."Object ID Running" := JobQueueEntry."Object ID to Run";
//             END ELSE BEGIN
//                 JobQueueProcess."Object Type Running" := 0;
//                 JobQueueProcess."Object ID Running" := 0;
//             END;
//             JobQueueProcess.MODIFY();
//         END;
//         EXIT(JobQueueProcess.Active);
//     end;

//     local procedure CleanUpJobQueue()
//     var
//         JobQueueProcess2: Record "Job Queue Category";
//         JobQueueResponse2: Record 473;
//         StartDateTime: DateTime;
//     begin
//         IF LastCleanupTime + 60000 > CURRENTDATETIME THEN
//             EXIT;
//         LastCleanupTime := CURRENTDATETIME;

//         JobQueueProcess2.SETFILTER("Session ID", '<>%1', SessionID);
//         IF NOT JobQueueProcess2.ISEMPTY THEN BEGIN
//             JobQueueProcess2.LOCKTABLE;
//             IF JobQueueProcess2.FINDSET() THEN
//                 REPEAT
//                     IF NOT JobQueueProcess2.ProcessIsActive THEN
//                         JobQueueProcess2.DELETE;
//                 UNTIL JobQueueProcess2.NEXT() = 0;
//         END;

//         StartDateTime := CREATEDATETIME(TODAY - 365, 0T);
//         JobQueueResponse2.SETCURRENTKEY("Expiration Date/Time");
//         JobQueueResponse2.SETRANGE("Expiration Date/Time", StartDateTime, CURRENTDATETIME);
//         IF NOT JobQueueResponse2.ISEMPTY THEN BEGIN
//             JobQueueResponse2.LOCKTABLE;
//             JobQueueResponse2.DELETEALL();
//         END;
//     end;
// }

