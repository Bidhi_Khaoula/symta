codeunit 50015 "Sales Statment-Printed"
{
    Permissions = TableData "Sales Invoice Header" = rimd;
    TableNo = "Sales Statment Header";

    trigger OnRun()
    begin
        rec.FIND();
        rec."No. Printed" := rec."No. Printed" + 1;
        rec.MODIFY();
        COMMIT();
    end;
}

