codeunit 50016 "Cust. Invoice Disc. Sup."
{
    trigger OnRun()
    begin
    end;

    procedure Calc_Disc_line(SaleHeader: Record "Sales Header"; TypeCalcul: Option global,facture,Livraison)
    var
        CusInvSup: Record "Cust. Invoice Disc. Sup.";
        SaleLine: Record "Sales Line";
        Montant: Decimal;
        Montant_facture: Decimal;
        montant_livrer: Decimal;
        "N° de ligne": Integer;
    begin
        //Verifier si des remises existent
        CusInvSup.SETRANGE(CusInvSup."Customer No.", SaleHeader."Bill-to Customer No.");
        CusInvSup.SETRANGE("Ligne Type", CusInvSup."Ligne Type"::Ligne);
        IF CusInvSup.ISEMPTY THEN
            EXIT;

        //calcul du montant HT
        SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
        SaleLine.SETRANGE("Document No.", SaleHeader."No.");
        SaleLine.SETRANGE(Type, SaleLine.Type::Item);
        IF SaleLine.FINDFIRST() THEN
            REPEAT
                Montant += SaleLine.Quantity * SaleLine."Net Unit Price";

                IF SaleHeader."Document Type" = SaleHeader."Document Type"::Invoice THEN
                    Montant_facture += SaleLine.Quantity * SaleLine."Net Unit Price"
                ELSE
                    Montant_facture += SaleLine."Qty. to Invoice" * SaleLine."Net Unit Price";

                montant_livrer += SaleLine."Qty. to Ship" * SaleLine."Net Unit Price";
            UNTIL SaleLine.NEXT() = 0
        ELSE
            EXIT;

        //recherche de la derniere ligne de commande
        CLEAR(SaleLine);
        SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
        SaleLine.SETRANGE("Document No.", SaleHeader."No.");
        SaleLine.FINDLAST();
        "N° de ligne" := SaleLine."Line No.";


        CusInvSup.FINDFIRST();
        REPEAT
            //vérification que la ligne n'existe pas deja;
            SaleLine.RESET();
            SaleLine.SetHideValidationDialog(TRUE);
            SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
            SaleLine.SETRANGE("Document No.", SaleHeader."No.");
            SaleLine.SETRANGE(Type, SaleLine.Type::"G/L Account");
            SaleLine.SETRANGE("Internal Line Type", SaleLine."Internal Line Type"::"Discount Line");
            SaleLine.SETRANGE("No.", CusInvSup."Gl Account No.");
            IF SaleLine.FINDFIRST() THEN BEGIN
                //Mise a jours du montant
                CASE TypeCalcul OF
                    TypeCalcul::global:
                        BEGIN
                            //FB 21-12-2010  ne pas modifier montant si < deja livre
                            IF Montant >= SaleLine."Quantity Shipped" THEN
                                SaleLine.VALIDATE(Quantity, ROUND(Montant, 0.01, '='));
                        END;

                    TypeCalcul::facture:
                        BEGIN
                            IF ROUND(Montant_facture, 0.01, '=') > SaleLine.Quantity THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";

                            IF ROUND(Montant_facture, 0.01, '=') > (SaleLine.Quantity - SaleLine."Quantity Invoiced") THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";


                            IF ABS(ROUND(Montant_facture, 0.01, '=') - (SaleLine.Quantity - SaleLine."Quantity Invoiced")) <= 0.02 THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";


                            SaleLine.VALIDATE(SaleLine."Qty. to Invoice", ROUND(Montant_facture, 0.01, '='));

                        END;

                    TypeCalcul::Livraison:
                        BEGIN
                            IF ROUND(montant_livrer, 0.01, '=') > SaleLine."Outstanding Quantity" THEN
                                montant_livrer := SaleLine."Outstanding Quantity";

                            IF ABS((ROUND(montant_livrer, 0.01, '=') + SaleLine.Quantity) - SaleLine."Outstanding Quantity") <= 0.02 THEN
                                montant_livrer := SaleLine."Outstanding Quantity";

                            SaleLine.VALIDATE(SaleLine."Qty. to Ship", ROUND(montant_livrer, 0.01, '='));

                        END;
                END;
                SaleLine.MODIFY();
            END
            ELSE BEGIN
                //création de la ligne de remise
                "N° de ligne" += 10000;

                SaleLine.RESET();
                SaleLine.INIT();
                SaleLine.SetHideValidationDialog(TRUE);
                SaleLine."Document Type" := SaleHeader."Document Type";
                SaleLine."Document No." := SaleHeader."No.";
                SaleLine."Line No." := "N° de ligne";
                SaleLine.Type := SaleLine.Type::"G/L Account";
                SaleLine.VALIDATE("No.", CusInvSup."Gl Account No.");
                SaleLine.VALIDATE(Description, CusInvSup.Description);
                SaleLine."Internal Line Type" := SaleLine."Internal Line Type"::"Discount Line";

                SaleLine.VALIDATE("Unit Price", ROUND(-CusInvSup."Pourcent Disc." / 100, 0.01, '='));
                CASE TypeCalcul OF
                    TypeCalcul::global:
                        SaleLine.VALIDATE(Quantity, ROUND(Montant, 0.01, '='));
                    TypeCalcul::facture:
                        SaleLine.VALIDATE(Quantity, ROUND(Montant_facture, 0.01, '='));
                    TypeCalcul::Livraison:
                        BEGIN
                            SaleLine.VALIDATE(Quantity, ROUND(montant_livrer, 0.01, '='));
                            SaleLine.VALIDATE("Qty. to Ship", SaleLine.Quantity);
                        END;
                END;

                SaleLine."Allow Invoice Disc." := TRUE;

                SaleLine.Insert();
            END;
        UNTIL CusInvSup.NEXT() = 0;
    end;

    procedure Calc_Disc_Header(SaleHeader: Record "Sales Header"; TypeCalcul: Option global,facture,livraison)
    var
        CusInvSup: Record "Cust. Invoice Disc. Sup.";
        SaleLine: Record "Sales Line";
        Montant: Decimal;
        Montant_facture: Decimal;
        montant_livrer: Decimal;
        "N° de ligne": Integer;
    begin
        //Verifier si des remises existent
        CusInvSup.SETRANGE(CusInvSup."Customer No.", SaleHeader."Bill-to Customer No.");
        CusInvSup.SETRANGE("Ligne Type", CusInvSup."Ligne Type"::Entete);
        IF CusInvSup.ISEMPTY THEN
            EXIT;

        //calcul du montant HT
        SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
        SaleLine.SETRANGE("Document No.", SaleHeader."No.");
        SaleLine.SETFILTER("Internal Line Type", '<>%1', SaleLine."Internal Line Type"::"Discount Header");

        IF SaleLine.FINDFIRST() THEN
            REPEAT
                Montant += SaleLine.Quantity * SaleLine."Net Unit Price";
                IF SaleHeader."Document Type" = SaleHeader."Document Type"::Invoice THEN
                    Montant_facture += SaleLine.Quantity * SaleLine."Net Unit Price"
                ELSE
                    Montant_facture += SaleLine."Qty. to Invoice" * SaleLine."Net Unit Price";

            UNTIL SaleLine.NEXT() = 0
        ELSE
            EXIT;

        //recherche de la derniere ligne de commande
        CLEAR(SaleLine);
        SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
        SaleLine.SETRANGE("Document No.", SaleHeader."No.");
        SaleLine.FINDLAST();
        "N° de ligne" := SaleLine."Line No.";


        CusInvSup.FINDFIRST();
        REPEAT
            //vérification que la ligne n'existe pas deja;
            SaleLine.RESET();
            SaleLine.SetHideValidationDialog(TRUE);
            SaleLine.SETRANGE("Document Type", SaleHeader."Document Type");
            SaleLine.SETRANGE("Document No.", SaleHeader."No.");
            SaleLine.SETRANGE(Type, SaleLine.Type::"G/L Account");
            SaleLine.SETRANGE("Internal Line Type", SaleLine."Internal Line Type"::"Discount Header");
            SaleLine.SETRANGE("No.", CusInvSup."Gl Account No.");

            IF SaleLine.FINDFIRST() THEN BEGIN
                //Mise a jours du montant
                CASE TypeCalcul OF
                    TypeCalcul::global:
                        BEGIN
                            SaleLine.VALIDATE(Quantity, ROUND(Montant, 0.01, '='));
                        END;

                    TypeCalcul::facture:
                        BEGIN
                            IF ROUND(Montant_facture, 0.01, '=') > SaleLine.Quantity THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";

                            IF ABS(ROUND(Montant_facture, 0.01, '=') - (SaleLine.Quantity - SaleLine."Quantity Invoiced")) <= 0.02 THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";

                            IF ROUND(Montant_facture, 0.01, '=') > (SaleLine.Quantity - SaleLine."Quantity Invoiced") THEN
                                Montant_facture := SaleLine.Quantity - SaleLine."Quantity Invoiced";


                            SaleLine.VALIDATE(SaleLine."Qty. to Invoice", ROUND(Montant_facture, 0.01, '='));

                        END;

                    TypeCalcul::livraison:
                        BEGIN
                            IF ROUND(montant_livrer, 0.01, '=') > SaleLine."Outstanding Quantity" THEN
                                montant_livrer := SaleLine."Outstanding Quantity";

                            IF ABS((ROUND(montant_livrer, 0.01, '=') + SaleLine.Quantity) - SaleLine."Outstanding Quantity") <= 0.02 THEN
                                montant_livrer := SaleLine."Outstanding Quantity";

                            SaleLine.VALIDATE(SaleLine."Qty. to Ship", ROUND(montant_livrer, 0.01, '='));

                        END;

                END;


                SaleLine.MODIFY();
            END
            ELSE BEGIN
                //création de la ligne de remise
                "N° de ligne" += 10000;

                SaleLine.RESET();
                SaleLine.INIT();
                SaleLine.SetHideValidationDialog(TRUE);
                SaleLine."Document Type" := SaleHeader."Document Type";
                SaleLine."Document No." := SaleHeader."No.";
                SaleLine."Line No." := "N° de ligne";
                SaleLine.Type := SaleLine.Type::"G/L Account";
                SaleLine.VALIDATE("No.", CusInvSup."Gl Account No.");
                SaleLine.VALIDATE(Description, CusInvSup.Description);
                SaleLine."Internal Line Type" := SaleLine."Internal Line Type"::"Discount Header";
                SaleLine.VALIDATE("Unit Price", ROUND(-CusInvSup."Pourcent Disc." / 100, 0.01, '='));
                CASE TypeCalcul OF
                    TypeCalcul::global:
                        SaleLine.VALIDATE(Quantity, ROUND(Montant, 0.01, '='));
                    TypeCalcul::facture:
                        SaleLine.VALIDATE(Quantity, ROUND(Montant_facture, 0.01, '='));
                    TypeCalcul::livraison:
                        BEGIN
                            SaleLine.VALIDATE(Quantity, ROUND(montant_livrer, 0.01, '='));
                            SaleLine.VALIDATE("Qty. to Ship", SaleLine.Quantity);
                        END;

                END;
                SaleLine.Insert();
            END;
        UNTIL CusInvSup.NEXT() = 0;
    end;
}

