codeunit 50001 "Transfer Item Charge"
{
    trigger OnRun()
    begin
    end;

    var
        Text000Err: Label 'There is not enough space to insert extended text lines.';
        NextLineNo: Integer;
        LineSpacing: Integer;
        MakeUpdateRequired: Boolean;

    procedure SalesCheckIfAnyLink(var SalesLine: Record "Sales Line"): Boolean
    var
        SalesHeader: Record "Sales Header";
        Item: Record Item;
        ItemCharge: Record Resource;
        customer: Record Customer;
    begin
        MakeUpdateRequired := FALSE;

        IF SalesLine."Line No." <> 0 THEN
            MakeUpdateRequired := DeleteSalesLines(SalesLine);

        IF SalesLine.Type <> SalesLine.Type::Item THEN
            EXIT(FALSE);


        SalesLine.TESTFIELD("Document No.");
        SalesHeader.GET(SalesLine."Document Type", SalesLine."Document No.");

        // AD Le 23-01-2007 => Ajout du IF et renvoie de false pour les devis sur de prospect.
        IF NOT customer.GET(SalesHeader."Sell-to Customer No.") THEN
            EXIT(FALSE);

        IF customer."DEEE Tax" = FALSE THEN
            EXIT(FALSE);

        // AD Le 25-09-2009 => FARGROUP -> Dependant du type de ligne
        CASE SalesLine."Line type" OF
            SalesLine."Line type"::Réparation:
                EXIT(FALSE);
            SalesLine."Line type"::Echange:
                EXIT(FALSE);
        // SalesLine."Line type"::SAV : EXIT(FALSE); AD Le 27-10-2009 => Demande de pierre => DEEE sur le SAV
        END;

        Item.GET(SalesLine."No.");
        IF ItemCharge.GET(Item."Item Charge DEEE") THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure InsertSalesLink(var SalesLine: Record "Sales Line")
    var
     Currency: Record "Currency Exchange Rate";
        SalesHeader: Record "Sales Header";
        ToSalesLine: Record "Sales Line";
        Item: Record Item;
        NbLines: Integer;
        LineNo: Integer;
       
    begin
        SalesHeader.RESET();
        SalesHeader.GET(SalesLine."Document Type", SalesLine."Document No.");


        NbLines := 1; // Nb de frais annexe à ajouter
        LineNo := 0;

        ToSalesLine.RESET();
        ToSalesLine.SETRANGE("Document Type", SalesLine."Document Type");
        ToSalesLine.SETRANGE("Document No.", SalesLine."Document No.");
        ToSalesLine.SETRANGE(ToSalesLine."Attached to Line No.", SalesLine."Line No.");
        IF ToSalesLine.FIND('+') THEN
            LineNo := ToSalesLine."Line No."
        ELSE
            LineNo := SalesLine."Line No.";

        ToSalesLine.RESET();
        ToSalesLine.SETRANGE("Document Type", SalesLine."Document Type");
        ToSalesLine.SETRANGE("Document No.", SalesLine."Document No.");
        ToSalesLine := SalesLine;

        //ToSalesLine.SETRANGE(ToSalesLine."Attached to Line No.");
        IF ToSalesLine.FIND('>') THEN BEGIN
            LineSpacing :=
              (ToSalesLine."Line No." - SalesLine."Line No.") DIV
              (1 + NbLines);
            IF LineSpacing = 0 THEN
                ERROR(Text000Err);
        END ELSE
            LineSpacing := 10000;

        NextLineNo := LineNo + LineSpacing;

        IF Item.GET(SalesLine."No.") THEN BEGIN
            ToSalesLine.INIT();
            ToSalesLine."Document Type" := SalesLine."Document Type";
            ToSalesLine."Document No." := SalesLine."Document No.";
            ToSalesLine."Line No." := NextLineNo;
            NextLineNo := NextLineNo + LineSpacing;

            // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
            // ToSalesLine.VALIDATE(Type, ToSalesLine.Type::"Charge (Item)");
            ToSalesLine.VALIDATE(Type, ToSalesLine.Type::Resource);
            // FIN AD Le 30-11-2009

            ToSalesLine.VALIDATE("No.", Item."Item Charge DEEE");
            ToSalesLine.VALIDATE(Quantity, SalesLine.Quantity);
            ToSalesLine.VALIDATE("Unit Cost (LCY)", Item."Item Charge DEEE Amount");
            ToSalesLine.VALIDATE("Unit Price",
                 Currency.ExchangeAmtLCYToFCY(WORKDATE(), SalesHeader."Currency Code", Item."Item Charge DEEE Amount",
                         SalesHeader."Currency Factor"));
            ToSalesLine."Attached to Line No." := SalesLine."Line No.";
            // AD Le 25-09-2009 => Appliquer les remises d'entete (Escompte) sur la DEEE
            ToSalesLine."Allow Line Disc." := SalesHeader."Allow Line Disc.";
            // FIN AD Le 25-09-2009
            // MC Le 29-09-2010 =>> Suivi du champ prèt pour la facturation des prèts.
            ToSalesLine.Loans := SalesLine.Loans;
            //FIN MC Le 29-09-2010

            IF NOT ToSalesLine.INSERT() THEN ERROR('fdf');
            MakeUpdateRequired := TRUE;
        END
        ELSE
            MakeUpdateRequired := FALSE;
    end;

    procedure DeleteSalesLines(var SalesLine: Record "Sales Line"): Boolean
    var
        SalesLine2: Record "Sales Line";
    begin
        SalesLine2.SETRANGE("Document Type", SalesLine."Document Type");
        SalesLine2.SETRANGE("Document No.", SalesLine."Document No.");
        SalesLine2.SETRANGE("Attached to Line No.", SalesLine."Line No.");
        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        //SalesLine2.SETFILTER(Type,'%1',SalesLine2.Type::"Charge (Item)");
        SalesLine2.SETFILTER(Type, '%1', SalesLine2.Type::Resource);
        // FIN AD Le 30-11-2009

        SalesLine2 := SalesLine;
        IF SalesLine2.FIND('>') THEN BEGIN
            REPEAT
                SalesLine2.DELETE(TRUE);
            UNTIL SalesLine2.NEXT() = 0;
            EXIT(TRUE);
        END;
    end;

    procedure MakeUpdate(): Boolean
    begin
        EXIT(MakeUpdateRequired);
    end;

    procedure UpdateQtySalesLink(SalesLine: Record "Sales Line"): Boolean
    var
        SalesLineCharge: Record "Sales Line";
    begin
        // AD Le 30-10-2006 => DEEE : Modification des quatités de frais annexes.
        SalesLineCharge.RESET();
        SalesLineCharge.SETRANGE("Document Type", SalesLine."Document Type");
        SalesLineCharge.SETRANGE("Document No.", SalesLine."Document No.");
        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        //SalesLineCharge.SETRANGE(Type, SalesLineCharge.Type::"Charge (Item)");
        SalesLineCharge.SETRANGE(Type, SalesLineCharge.Type::Resource);
        // FIN AD Le 30-11-2009
        SalesLineCharge.SETRANGE("Attached to Line No.", SalesLine."Line No.");
        IF SalesLineCharge.FIND('-') THEN BEGIN
            REPEAT
                SalesLineCharge.VALIDATE(Quantity, SalesLine.Quantity);
                //MC Le 29-09-2010 => Gestion des prêts.
                SalesLineCharge.VALIDATE("Qty. to Invoice", SalesLine."Qty. to Invoice");
                //FIN MC Le 29-09-2010
                SalesLineCharge.MODIFY();
            //MC Le 07-01-2010 Suite au passage en ressource ce code ne sert plus a rien
            // AssignChargeItemLink(SalesLineCharge);
            //COMMIT;
            // MC Le 07-01-2010
            UNTIL SalesLineCharge.NEXT() = 0;
            MakeUpdateRequired := TRUE;
        END
        ELSE
            MakeUpdateRequired := FALSE;
    end;

    procedure AssignChargeItemLink(ChargeLineLink: Record "Sales Line")
    var
/*         ItemChargeAssgntSales: Record "Item Charge Assignment (Sales)";
        SalesHeader: Record "Sales Header";
        Currency: Record Currency;
        FromSalesLine: Record "Sales Line";
        ItemChargeAssgnt: Codeunit "Item Charge Assgnt. (Sales)"; */
    begin
        // AD Le 30-11-2009 => DEEE -> Passage sur des ressources
        EXIT;
        // FIN AD Le 30-11-2009

        /*******************Non Utilisé********************************** 
        ChargeLineLink.TESTFIELD(Type, ChargeLineLink.Type::"Charge (Item)");
        ChargeLineLink.TESTFIELD("No.");
        ChargeLineLink.TESTFIELD(Quantity);

        FromSalesLine.GET(ChargeLineLink."Document Type", ChargeLineLink."Document No.", ChargeLineLink."Attached to Line No.");

        ItemChargeAssgntSales.RESET();
        ItemChargeAssgntSales.SETRANGE("Document Type", ChargeLineLink."Document Type");
        ItemChargeAssgntSales.SETRANGE("Document No.", ChargeLineLink."Document No.");
        ItemChargeAssgntSales.SETRANGE("Document Line No.", ChargeLineLink."Line No.");
        ItemChargeAssgntSales.SETRANGE("Item Charge No.", ChargeLineLink."No.");
        ItemChargeAssgntSales.SETRANGE("Applies-to Doc. Line No.", ChargeLineLink."Attached to Line No.");

        IF NOT ItemChargeAssgntSales.FIND('+') THEN BEGIN
            ItemChargeAssgntSales."Document Type" := ChargeLineLink."Document Type";
            ItemChargeAssgntSales."Document No." := ChargeLineLink."Document No.";
            ItemChargeAssgntSales."Document Line No." := ChargeLineLink."Line No.";
            ItemChargeAssgntSales."Item Charge No." := ChargeLineLink."No.";

            SalesHeader.GET(ChargeLineLink."Document Type", ChargeLineLink."Document No.");
            IF SalesHeader."Currency Code" = '' THEN
                Currency.InitRoundingPrecision()
            ELSE BEGIN
                SalesHeader.TESTFIELD("Currency Factor");
                Currency.GET(SalesHeader."Currency Code");
                Currency.TESTFIELD("Amount Rounding Precision");
            END;


            IF (ChargeLineLink."Inv. Discount Amount" = 0) AND (NOT SalesHeader."Prices Including VAT") THEN
                ItemChargeAssgntSales."Unit Cost" := ChargeLineLink."Unit Price"
            ELSE
                IF SalesHeader."Prices Including VAT" THEN
                    ItemChargeAssgntSales."Unit Cost" :=
                      ROUND(
                        (ChargeLineLink."Line Amount" - ChargeLineLink."Inv. Discount Amount") / ChargeLineLink.Quantity /
                          (1 + ChargeLineLink."VAT %" / 100),
                          Currency."Unit-Amount Rounding Precision")
                ELSE
                    ItemChargeAssgntSales."Unit Cost" :=
                      ROUND(
                        (ChargeLineLink."Line Amount" - ChargeLineLink."Inv. Discount Amount") / ChargeLineLink.Quantity,
                        Currency."Unit-Amount Rounding Precision");

            NextLineNo := 0;
            ItemChargeAssgnt.InsertItemChargeAssignment(
                  ItemChargeAssgntSales, FromSalesLine."Document Type",
                  FromSalesLine."Document No.", FromSalesLine."Line No.",
                  FromSalesLine."No.", FromSalesLine.Description, NextLineNo);
            // MESSAGE('Insertion Frais OK');
        END;

        // SI Facture ou avoir direct on met directement les quatité a affecter.
        IF FromSalesLine."Document Type" IN [FromSalesLine."Document Type"::Invoice, FromSalesLine."Document Type"::"Credit Memo"] THEN BEGIN
            ItemChargeAssgntSales.RESET();
            ItemChargeAssgntSales.SETRANGE("Document Type", ChargeLineLink."Document Type");
            ItemChargeAssgntSales.SETRANGE("Document No.", ChargeLineLink."Document No.");
            ItemChargeAssgntSales.SETRANGE("Document Line No.", ChargeLineLink."Line No.");
            ItemChargeAssgntSales.SETRANGE("Item Charge No.", ChargeLineLink."No.");
            ItemChargeAssgntSales.SETRANGE("Applies-to Doc. Line No.", ChargeLineLink."Attached to Line No.");
            IF ItemChargeAssgntSales.FIND('-') THEN BEGIN
                ItemChargeAssgntSales."Qty. to Assign" := FromSalesLine.Quantity;
                ItemChargeAssgntSales.MODIFY();
            END;
        END;


        ChargeLineLink.CALCFIELDS("Qty. to Assign");
         *******************Non Utilisé********************************** */
    end;

    procedure "--- FARGROUP ---"()
    begin
    end;

    procedure UpdateLineType(var SalesLine: Record "Sales Line")
    begin
        IF SalesLine."Line type" IN [SalesLine."Line type"::Réparation, SalesLine."Line type"::Echange] THEN
            IF DeleteSalesLines(SalesLine) THEN
                MakeUpdateRequired := TRUE;
    end;
}

