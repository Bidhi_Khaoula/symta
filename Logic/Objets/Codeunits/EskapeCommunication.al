codeunit 50050 "Eskape Communication"
{
    SingleInstance = true;

    trigger OnRun()

    begin
    end;

    var
        CuFile: Codeunit "File Management";
        Text0002: Label 'Impossible d''identifier l''état associé à la table %1 ! \\ L''enregistrement à imprimer est : %2.';
        optPrinter: Option ptrEmail,ptrFax;
        ImpressionMail: Boolean;
        bln_SendCGV: Boolean;
        ObjetEmail: Text[50];
        ESK001Txt: Label 'accusé de réception de commande';
        ESK003Txt: Label 'offre';
        ESK004Txt: Label 'commande';
       // ESK005: Label 'demande de fabrication';
        _ForcerImpressionPdf: Boolean;
        ESK006Txt: Label 'facture';
        ESK007Txt: Label 'bon de livraison';
        ESK008Txt: Label 'avoir';
        ESK009Txt: Label 'bon de retour';
        ESK010Txt: Label 'bon de retour';
        ESK011Txt: Label 'demande de garantie';
        ESK012Txt: Label 'retour';

        strFullFilePath: Text[255];

    procedure ReportToEmailPDF(pRecordRef: RecordRef; pReportId: Integer; pFileName: Text[255])
    begin
        ReportToPrint(optPrinter::ptrEmail, pRecordRef, pReportId, pFileName);
    end;

    procedure ReportToFax(pRecordRef: RecordRef; pReportId: Integer; pFileName: Text[255])
    begin
        ReportToPrint(optPrinter::ptrFax, pRecordRef, pReportId, pFileName);
    end;

    procedure ReportToPrint(pPrinter: Option ptrEmail,ptrFax; pRecordRef: RecordRef; pReportId: Integer; pFileName: Text[255])
    var
        AllObjWithCaption: Record AllObjWithCaption;
        cuSendMail: Codeunit "Eskape Communication : Email";
        strBCC: Text[255];

        noDoc: Text[20];
        NomDestinataire: Text[60];
        MailBody: Text[500];
        strFilePath: Text[255];
        strFileName: Text[255];

        strRecipient: Text[255];

        intReportId: Integer;
    begin
        // 1ère Etape : Choix du destinataire  et initialisation des variables
        ObjetEmail := '';
        bln_SendCGV := FALSE;
        strBCC := '';
        strRecipient := GetRecipient(pPrinter, pRecordRef);

        IF strRecipient = '' THEN EXIT;

        // AD Le 01-04-2011




        //----------------------------------------------------------------------------------------------------------------------------------
        // 2ère Etape : Identifie l'état à imprimer
        IF pReportId = 0 THEN
            intReportId := GetReportId(pRecordRef, pFileName)

        ELSE BEGIN
            intReportId := pReportId;
            // Récupère le nom du document.

            // On génére les noms en fonction du report à imprimer.
            AllObjWithCaption.SETRANGE("Object ID", intReportId);
            AllObjWithCaption.SETRANGE("Object Type", AllObjWithCaption."Object Type"::Report);
            IF AllObjWithCaption.FINDFIRST() THEN BEGIN
                pFileName := AllObjWithCaption."Object Caption";
                ObjetEmail := AllObjWithCaption."Object Caption";
            END;

        END;

        noDoc := GetNoDoc(pRecordRef, NomDestinataire);
        pFileName := pFileName + '-' + noDoc;
        MailBody := GetObjet(pRecordRef, noDoc);


        //----------------------------------------------------------------------------------------------------------------------------------
        // 3ème Etape : Construction du nom (chemin et nom) du fichier PDF à générer
        //Fixe le chemin du fichier à imprimer
        strFilePath := TEMPORARYPATH;
        //strFilePath     := CuFile.GetDirectoryName(CuFile.ClientTempFileName('')) + '\'; // MIG 015

        //Fixe le nom du fichier à imprimer

        // AD Le 20-07-2010 =>
        //strFileName     := pFileName + ' -- ' + USERID + ' -- ' +
        //                FORMAT(TODAY, 0, '<Year,2>') + '-' + FORMAT(TODAY, 0, '<Month,2>') + '-' + FORMAT(TODAY, 0, '<Day,2>') + ' '+
        //                 FORMAT(TIME, 0, '<Hours24,2>') + 'h' + FORMAT(TIME, 0, '<Minutes,2>') + 'm' + FORMAT(TIME, 0, '<Seconds,2>');

        strFileName := pFileName + '-' + COMPANYNAME + '-' +
                             FORMAT(TODAY, 0, '<Day,2>') + '-' + FORMAT(TODAY, 0, '<Month,2>') + '-' + FORMAT(TODAY, 0, '<Year,2>');
        // FIN AD Le 20-07-2010
        strFileName := PurgeCaractereFichier(strFileName);
        //Construit le nom complet (Chemin + nom + extension)
        strFullFilePath := strFilePath + strFileName + '.pdf';

        //Si le fichier Existe déjà, on le supprime
        IF EXISTS(strFullFilePath) THEN ERASE(strFullFilePath);

        //----------------------------------------------------------------------------------------------------------------------------------
        // 4ème Etape : Paramétrage de l'imprimante PDF
        /*    //Instancie un objet PDF Creator
            IF ISCLEAR(objPDFCreator)       THEN CREATE(objPDFCreator, FALSE, TRUE);
            //Instancie un objet PDF Creator Option
            IF ISCLEAR(objPDFCreatorOption) THEN CREATE(objPDFCreatorOption, FALSE, TRUE);
        
            //Démarre l'imprimante PDF
            objPDFCreator.cStart;
        
            //Paramètre l'imprimante PDF
                //Récupère le paramétrage courant
                objPDFCreatorOption                       :=objPDFCreator.cOptions;
                //Détermine le répertoire d'enregistrement
                objPDFCreatorOption.AutosaveDirectory     := strFilePath;
                //Détermine le nom du fichier de sortie (le .pdf est ajouté automatiquement)
                objPDFCreatorOption.AutosaveFilename      := strFileName;
                //Indique à PDF d'enregistrer le fichier automatiquement
                objPDFCreatorOption.UseAutosave           := 1;
                objPDFCreatorOption.UseAutosaveDirectory  := 1;
                objPDFCreatorOption.AutosaveFormat        := 0;
                //Réaffecte les options à PDF Creator
                objPDFCreator.cOptions                    := objPDFCreatorOption;*/

        //----------------------------------------------------------------------------------------------------------------------------------
        // 5ème Etape
        _ForcerImpressionPdf := TRUE;


        //----------------------------------------------------------------------------------------------------------------------------------
        // 6ème Etape : Lance l'impression de l'état
        PrintReport(pPrinter, intReportId, pRecordRef, strBCC);

        //----------------------------------------------------------------------------------------------------------------------------------
        // 7ème Etape :
        _ForcerImpressionPdf := FALSE;

        //----------------------------------------------------------------------------------------------------------------------------------
        // 8ème Etape : Arrete l'imprimante PDF
        /*objPDFCreator.cPrinterStop(FALSE);
        
        //----------------------------------------------------------------------------------------------------------------------------------
        // 9ème Etape : Attend et vérifie la création du fichier PDF
            //Récupère la date et l'heure courante pour surveiller le temps qui passe
            sngTime := CURRENTDATETIME;
            //Boucle jusqu'à ce que le fichier soit trouvé ou jusqu'à ce que l'on attend plus de 30 secondes
            REPEAT UNTIL ((CURRENTDATETIME-sngTime) >= 30000) OR (CuFile.ClientFileExists(strFullFilePath));
            //Vérifie que l'on est pas sortie en timeOut
            IF NOT CuFile.ClientFileExists(strFullFilePath) THEN ERROR(Text0001, strFullFilePath);
        
        
        //----------------------------------------------------------------------------------------------------------------------------------
        // 10ème Etape : Destruction de l'imprimante PDF
        objPDFCreator.cClose;
        CLEAR(objPDFCreator);*/



        //----------------------------------------------------------------------------------------------------------------------------------
        // 11ème Etape : Envois l'email ou le Fax avec le client de messagerie par défaut ou l'imprimante Fax
        CASE pPrinter OF
            //------------------------------------------------------------------------------------------------------------------------------
            //Gestion de l'envois d'email
            pPrinter::ptrEmail:
                BEGIN
                    //Envoi l'Email
                    ObjetEmail := COMPANYNAME + '-' + ObjetEmail + '-' +
                      GetNoDoc(pRecordRef, NomDestinataire); // AD Le 09-01-2012 => DAMAC ->
                    cuSendMail.SendMail(strFullFilePath, ObjetEmail, strRecipient, '', MailBody);
                END;

            //------------------------------------------------------------------------------------------------------------------------------
            //Gestion de l'envois de Fax
            pPrinter::ptrFax:

                //Envois d'un Fax
                SendFaxFAXBIS(strFullFilePath, strRecipient, strFileName + '.pdf', NomDestinataire);
        END;

    end;

    procedure GetReportId(pRecordRef: RecordRef; var pReportName: Text[255]) intReturn_ReportId: Integer
    var
        recSalesHeader: Record "Sales Header";
        recPurchaseHeader: Record "Purchase Header";
        recSalesInvoiceHeader: Record "Sales Invoice Header";
        recSalesShipmentHeader: Record "Sales Shipment Header";
        recReportSelection: Record "Report Selections";
              recSalesCreditHeader: Record "Sales Cr.Memo Header";
        recReturnReceiptHeader: Record "Return Receipt Header";
           noDoc: Code[20];
    begin
        //Initialise le retour de la fonction
        intReturn_ReportId := 0;
        pReportName := '';

        //Réinitilise tous les filtre sur la table des Etats
        recReportSelection.RESET();

        //Identifie le type d'enregistrement à imprimer
        CASE pRecordRef.NUMBER OF

            //Vente
            DATABASE::"Sales Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesHeader.RESET();
                    recSalesHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesHeader.FINDFIRST() THEN BEGIN
                        //Identifie le type du document pour lancer le bon état
                        CASE recSalesHeader."Document Type" OF
                            recSalesHeader."Document Type"::Quote:             //Vente devis
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Quote");
                            recSalesHeader."Document Type"::"Blanket Order":
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Blanket");
                            recSalesHeader."Document Type"::Order:             //Vente commande
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Order");
                            recSalesHeader."Document Type"::"Return Order":
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Return");
                            ELSE
                                EXIT;
                        END;
                        // MC Le 17-01-2011 => Damac -> Gestion des AR.
                        IF recSalesHeader."Document Type" IN [recSalesHeader."Document Type"::Quote,
                                                              recSalesHeader."Document Type"::Order] THEN
                            bln_SendCGV := TRUE;
                        // FIN MC Le 17-01-2011 => Damac -> Gestion des AR.
                        noDoc := recSalesHeader."No.";

                    END;
                END;

            //Achat
            DATABASE::"Purchase Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recPurchaseHeader.RESET();
                    recPurchaseHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recPurchaseHeader.FINDFIRST() THEN BEGIN
                        //Identifie le type du document pour lancer le bon état
                        CASE recPurchaseHeader."Document Type" OF
                            recPurchaseHeader."Document Type"::Quote:    //achat devis
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"P.Quote");
                            recPurchaseHeader."Document Type"::"Blanket Order":
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"P.Blanket");
                            recPurchaseHeader."Document Type"::Order:    //achat commande
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"P.Order");
                            recPurchaseHeader."Document Type"::"Return Order":
                                recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"P.Return");
                            ELSE
                                EXIT;
                        END;
                        noDoc := recPurchaseHeader."No.";
                    END;
                END;

            //Facture vente enregistrée
            DATABASE::"Sales Invoice Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesInvoiceHeader.RESET();
                    recSalesInvoiceHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesInvoiceHeader.FINDFIRST() THEN 
                        //Identifie le type du document pour lancer le bon état
                        //IF recSalesInvoiceHeader."Service Mgt. Document" THEN
                        //    recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"SM.Invoice")
                        //ELSE
                        recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Invoice");
                    
                END;

            // Expédition vente enregistrée
            DATABASE::"Sales Shipment Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesShipmentHeader.RESET();
                    recSalesShipmentHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesShipmentHeader.FINDFIRST() THEN 
                        recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Shipment");
                    
                END;

            // Avoir enregistrée
            DATABASE::"Sales Cr.Memo Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesCreditHeader.RESET();
                    recSalesCreditHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesCreditHeader.FINDFIRST() THEN 
                        recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Cr.Memo");
                    
                END;

            // bon de livraison retour vente
            DATABASE::"Return Receipt Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recReturnReceiptHeader.RESET();
                    recReturnReceiptHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recReturnReceiptHeader.FINDFIRST() THEN 
                        recReportSelection.SETRANGE(Usage, recReportSelection.Usage::"S.Ret.Rcpt.");
                    
                END;


            //Enregistrement non géré
            ELSE
                EXIT;
        END;

        //Retourne l'état à imprimer
        recReportSelection.SETFILTER("Report ID", '<>0');
        IF recReportSelection.FINDFIRST() THEN BEGIN
            //Calcul le nom de l'état
            recReportSelection.CALCFIELDS("Report Caption");
            //Récupère le numéro de l'état
            intReturn_ReportId := recReportSelection."Report ID";
            pReportName := recReportSelection."Report Caption";
            ObjetEmail := recReportSelection."Report Caption";
        END
        ELSE
            //Erreur, Etat inconnu
            ERROR(Text0002, pRecordRef.NUMBER, pRecordRef.GETVIEW());
    end;

    procedure PrintReport(pPrinter: Option ptrEmail,ptrFax; pReportId: Integer; pRecordRef: RecordRef; var pBCC: Text[255]) strReturn_Recipient: Text[255]
    var
        recSalesHeader: Record "Sales Header";
        recPurchaseHeader: Record "Purchase Header";
        recSalesInvoiceHeader: Record "Sales Invoice Header";
        recSalesShipmentHeader: Record "Sales Shipment Header";
        recCustomer: Record Customer;
        recSalesPerson: Record "Salesperson/Purchaser";
        recReturnReceiptHeader: Record "Return Receipt Header";
        recSalesCreditHeader: Record "Sales Cr.Memo Header";
    begin
        //Imprime un état avec l'enregistrement passé
        ImpressionMail := TRUE;
        pBCC := '';
        //Identifie le type d'enregistrement à imprimer
        CASE pRecordRef.NUMBER OF

            //Vente
            DATABASE::"Sales Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesHeader.RESET();
                    recSalesHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on imprime l'état correspondant
                    IF recSalesHeader.FINDFIRST() THEN BEGIN
                        // MC Le 17-01-2011 => Damac -> Gestion des AR.
                        IF recSalesHeader."Document Type" IN [recSalesHeader."Document Type"::Quote,
                                                              recSalesHeader."Document Type"::Order] THEN
                            bln_SendCGV := TRUE;
                        IF recSalesPerson.GET(recSalesHeader."Salesperson Code") THEN
                            IF recSalesPerson."E-Mail" <> '' THEN
                                pBCC := recSalesPerson."E-Mail";
                        // FIN MC Le 17-01-2011 => Damac -> Gestion des AR.

                        //Lance l'impression de l'état
                        ///REPORT.RUN(pReportId, FALSE, FALSE, recSalesHeader);
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recSalesHeader);
                    END;
                END;

            //Achat
            DATABASE::"Purchase Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recPurchaseHeader.RESET();
                    recPurchaseHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesHeader.FINDFIRST() THEN
                        ///REPORT.RUN(pReportId, FALSE, FALSE, recPurchaseHeader)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recPurchaseHeader);
                END;

            //Facture vente enregistrée
            DATABASE::"Sales Invoice Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesInvoiceHeader.RESET();
                    recSalesInvoiceHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesInvoiceHeader.FINDFIRST() THEN
                        //REPORT.RUN(pReportId, FALSE, FALSE, recSalesInvoiceHeader)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recSalesInvoiceHeader);
                END;
            //OF
            // GR MIG2015
            //DATABASE::"BOM Journal Batch":
            //    BEGIN
            //        //Récupère l'enregistrement à imprimer
            //       recBOMJnlBatch.RESET();
            //        recBOMJnlBatch.SETVIEW(pRecordRef.GETVIEW);
            //        IF recBOMJnlBatch.FINDFIRST () THEN
            //          REPORT.RUN(pReportId, FALSE, FALSE, recBOMJnlBatch)
            //    END;

            // Expédition vente enregistrée
            DATABASE::"Sales Shipment Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesShipmentHeader.RESET();
                    recSalesShipmentHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesShipmentHeader.FINDFIRST() THEN
                        //REPORT.RUN(pReportId, FALSE, FALSE, recSalesShipmentHeader)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recSalesShipmentHeader);
                END;

            // Avoir vente enregistrée
            DATABASE::"Sales Cr.Memo Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesCreditHeader.RESET();
                    recSalesCreditHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesCreditHeader.FINDFIRST() THEN
                        //REPORT.RUN(pReportId, FALSE, FALSE, recSalesCreditHeader)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recSalesCreditHeader);
                END;

            // Retour vente enregistrée
            DATABASE::"Return Receipt Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recReturnReceiptHeader.RESET();
                    recReturnReceiptHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recReturnReceiptHeader.FINDFIRST() THEN
                        //REPORT.RUN(pReportId, FALSE, FALSE, recReturnReceiptHeader)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recReturnReceiptHeader);
                END;

            // Customer
            DATABASE::Customer:
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recCustomer.RESET();
                    recCustomer.SETVIEW(pRecordRef.GETVIEW());
                    IF recCustomer.FINDFIRST() THEN
                        //REPORT.RUN(pReportId, FALSE, FALSE, recCustomer)
                        REPORT.SAVEASPDF(pReportId, strFullFilePath, recCustomer);
                END;

            ELSE
                ERROR('Type non géré dans ESKAPE Communication / PrintReport');

        END;
        ImpressionMail := FALSE;
    end;

    procedure GetRecipient(pPrinter: Option ptrEmail,ptrFax; pRecordRef: RecordRef) strReturn_Recipient: Text[255]
    var
        FrmEskapeCommunication: Page "Eskape Communication";
    begin
        //Initialise le retour de la fonction
        strReturn_Recipient := '';

        CLEAR(FrmEskapeCommunication);

        //Initialise l'écran avec l'enregistrement
        FrmEskapeCommunication.InitForm(pPrinter, pRecordRef);

        //Si le formulaire a été validé, on récupère la valeur sélectionnée
        FrmEskapeCommunication.LOOKUPMODE(TRUE);
        CASE FrmEskapeCommunication.RUNMODAL() OF
            ACTION::LookupOK:
                strReturn_Recipient := FrmEskapeCommunication.GetReturnValue();

            ACTION::LookupCancel:
                ERROR('Annulation');
        END;
    end;

    procedure GetNoDoc(pRecordRef: RecordRef; var _pNom_rtn: Text[60]) txt_rtn: Text[20]
    var
        recSalesHeader: Record "Sales Header";
        recPurchaseHeader: Record "Purchase Header";
        recSalesInvoiceHeader: Record "Sales Invoice Header";
        recSalesShipmentHeader: Record "Sales Shipment Header";
        recReturnReceiptHeader: Record "Return Receipt Header";
        recSalesCreditHeader: Record "Sales Cr.Memo Header";
    begin
        //Initialise le retour de la fonction
        txt_rtn := '';

        //Identifie le type d'enregistrement à imprimer
        CASE pRecordRef.NUMBER OF

            //Vente
            DATABASE::"Sales Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesHeader.RESET();
                    recSalesHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesHeader.FINDFIRST() THEN BEGIN
                        txt_rtn := recSalesHeader."No.";
                        _pNom_rtn := recSalesHeader."Sell-to Customer Name";
                    END;
                END;

            //Achat
            DATABASE::"Purchase Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recPurchaseHeader.RESET();
                    recPurchaseHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recPurchaseHeader.FINDFIRST() THEN BEGIN
                        txt_rtn := recPurchaseHeader."No.";
                        _pNom_rtn := recPurchaseHeader."Pay-to Name";
                    END;
                END;

            //Facture vente enregistrée
            DATABASE::"Sales Invoice Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesInvoiceHeader.RESET();
                    recSalesInvoiceHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesInvoiceHeader.FINDFIRST() THEN BEGIN
                        txt_rtn := recSalesInvoiceHeader."No.";
                        _pNom_rtn := recSalesInvoiceHeader."Sell-to Customer Name";
                    END;
                END;

            //BL vente enregistrée
            DATABASE::"Sales Shipment Header":
                BEGIN
                    recSalesShipmentHeader.RESET();
                    recSalesShipmentHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesShipmentHeader.FINDFIRST() THEN
                        EXIT(recSalesShipmentHeader."No.");
                END;

            //Retour enregistrée
            DATABASE::"Return Receipt Header":
                BEGIN
                    recReturnReceiptHeader.RESET();
                    recReturnReceiptHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recReturnReceiptHeader.FINDFIRST() THEN
                        EXIT(recReturnReceiptHeader."No.");
                END;

            // Avoir vente enregistrée
            DATABASE::"Sales Cr.Memo Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesCreditHeader.RESET();
                    recSalesCreditHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé, on récupère le numéro de l'impression
                    IF recSalesCreditHeader.FINDFIRST() THEN BEGIN
                        txt_rtn := recSalesCreditHeader."No.";
                        _pNom_rtn := recSalesCreditHeader."Sell-to Customer Name";
                    END;
                END;



            //Enregistrement non géré
            ELSE
                EXIT;
        END;
    end;

    procedure SendFax(pFullPathFile: Text[255]; pFaxNumber: Text[30]; pFileName: Text[100]; _pDestinataire: Text[60])
    var
    LUserID: Record "User Setup";
        LCompanyInfo: Record "Company Information";
        LEmplFichierCommande: Text[500];
        LFichierCommande: File;
        LCheminDossierFax: Text[500];
        LEmailAR: Text[50];
        
    begin
        //Affecte le numéro de Fax du destinataire
        IF pFaxNumber <> '' THEN BEGIN

            IF LUserID.GET(USERID) THEN
                LEmailAR := LUserID."E-Mail";

            IF LEmailAR = '' THEN BEGIN
                LCompanyInfo.GET();
                LEmailAR := LCompanyInfo."E-Mail";
            END;

            // GESTION FAX RTE SYMTA
            //LCheminDossierFax := '\\rtefaxhp\c$\Documents and Settings\All Users\Application Data\RTE\RTE FAX\Fax Gateway\Group_1\';
            //LCheminDossierFax := '\\alexia\Group_1\'; // AD Le 26-01-2012 => Changement de répertoire
            LCheminDossierFax := 'X:\'; // AD Le 20-11-2014 => Changement de répertoire
                                        //LCheminDossierFax := 'C:\TmpFAX\';
            LEmplFichierCommande := pFullPathFile + '.tmp';
            LFichierCommande.TEXTMODE(TRUE);
            LFichierCommande.WRITEMODE(TRUE);

            IF FILE.EXISTS(LEmplFichierCommande) THEN
                LFichierCommande.OPEN(LEmplFichierCommande)
            ELSE
                LFichierCommande.CREATE(LEmplFichierCommande);

            LFichierCommande.SEEK(LFichierCommande.LEN);
            LFichierCommande.WRITE(';; Paramètres de l''expéditeur');
            LFichierCommande.WRITE('[Sender]');
            LFichierCommande.WRITE('Name=SYMTA PIECES');
            LFichierCommande.WRITE('Company=SYMTA PIECES');
            LFichierCommande.WRITE('Fax=0251372449');
            LFichierCommande.WRITE('Phone=0251373046');
            LFichierCommande.WRITE('Email=' + LEmailAR);
            LFichierCommande.WRITE('');
            LFichierCommande.WRITE(';; Paramètres de par défaut d''envoi du fax');
            LFichierCommande.WRITE('[FAX]');
            LFichierCommande.WRITE('Type=RTEFAXTYPE_TEXT_0001');
            LFichierCommande.WRITE('FaxID=Exemple_Id');
            LFichierCommande.WRITE('Cover=');
            LFichierCommande.WRITE('Account=');
            LFichierCommande.WRITE('DateTime=');
            LFichierCommande.WRITE('SendMode=');
            LFichierCommande.WRITE('Resolution=');
            LFichierCommande.WRITE('NoCover=1');
            LFichierCommande.WRITE('Priority=');
            LFichierCommande.WRITE('MaxRetries=');
            LFichierCommande.WRITE('Subject=Fax NAVSION');
            LFichierCommande.WRITE('Message=');
            LFichierCommande.WRITE('Dest=1');
            LFichierCommande.WRITE('');
            LFichierCommande.WRITE(';;Destinataires');
            LFichierCommande.WRITE('[Dest_1]');
            LFichierCommande.WRITE('Fax=' + pFaxNumber);
            LFichierCommande.WRITE('Name=');
            LFichierCommande.WRITE('Company=' + _pDestinataire);
            LFichierCommande.WRITE('Phone=');
            LFichierCommande.WRITE('FaxID=ID_FAX');
            LFichierCommande.WRITE('Cover=');
            LFichierCommande.WRITE('Account=');
            LFichierCommande.WRITE('DateTime=');
            LFichierCommande.WRITE('SendMode=0');
            LFichierCommande.WRITE('Resolution=');
            LFichierCommande.WRITE('NoCover=1');
            LFichierCommande.WRITE('Priority=');
            LFichierCommande.WRITE('MaxRetries=');
            LFichierCommande.WRITE('');
            LFichierCommande.WRITE(';;Fichier attachés');
            LFichierCommande.WRITE('[Document]');
            LFichierCommande.WRITE('Delete_Files=1');
            LFichierCommande.WRITE('File_1=' + '\\alexia\Group_1\' + pFileName); // AD Le 20-11-2014 => En dur
            LFichierCommande.WRITE('Name=');
            LFichierCommande.WRITE('Company=' + _pDestinataire);
            LFichierCommande.WRITE('Phone=');
            LFichierCommande.WRITE('FaxID=ID_FAX');
            LFichierCommande.WRITE('Cover=');
            LFichierCommande.WRITE('Account=');
            LFichierCommande.WRITE('DateTime=');
            LFichierCommande.WRITE('SendMode=0');
            LFichierCommande.WRITE('Resolution=');
            LFichierCommande.WRITE('NoCover=1');
            LFichierCommande.WRITE('Priority=');
            LFichierCommande.WRITE('MaxRetries=');
            LFichierCommande.WRITE('');
            LFichierCommande.WRITE(';;Fichier attachés');
            LFichierCommande.WRITE('[Document]');
            LFichierCommande.WRITE('Delete_Files=1');
            LFichierCommande.CLOSE();

            RENAME(pFullPathFile, LCheminDossierFax + pFileName);
            RENAME(LEmplFichierCommande, LCheminDossierFax + '\Out\' + pFileName + '.ini');



            //Averti l'utilisateur Fax envoyé
            MESSAGE('Le Fax est dans la boite d''envois');
        END
        ELSE
            MESSAGE('Pas de numéro de Fax');
        end;

    procedure GetObjet(pRecordRef: RecordRef; pNoDoc: Code[20]) txt_rtn: Text[1024]
    var
        recSalesHeader: Record "Sales Header";
        recPurchaseHeader: Record "Purchase Header";
          recSalesShipmentHeader: Record "Sales Shipment Header";
        recReturnReceiptHeader: Record "Return Receipt Header";
        recSalesCreditHeader: Record "Sales Cr.Memo Header";
        recSalesInvoiceHeader: Record "Sales Invoice Header";
         txt_NomDoc: Text[50];
        strCr: Code[10];
    begin
        //Initialise le retour de la fonction
        txt_rtn := '';

        //Identifie le type d'enregistrement à imprimer
        CASE pRecordRef.NUMBER OF

            //Vente
            DATABASE::"Sales Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesHeader.RESET();
                    recSalesHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesHeader.FINDFIRST() THEN BEGIN
                        CASE recSalesHeader."Document Type" OF
                            recSalesHeader."Document Type"::Order:
                                txt_NomDoc := ESK001Txt;
                            recSalesHeader."Document Type"::Quote:
                                txt_NomDoc := ESK003Txt;
                            recSalesHeader."Document Type"::"Return Order":
                                IF recSalesHeader."Type retour" = recSalesHeader."Type retour"::Garantie THEN
                                    txt_NomDoc := ESK011Txt
                                ELSE
                                    txt_NomDoc := ESK009Txt;
                        END;
                    END;
                END;

            //Achat
            DATABASE::"Purchase Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recPurchaseHeader.RESET();
                    recPurchaseHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recPurchaseHeader.FINDFIRST() THEN BEGIN
                        CASE recPurchaseHeader."Document Type" OF
                            recPurchaseHeader."Document Type"::Order:
                                txt_NomDoc := ESK004Txt;
                            recPurchaseHeader."Document Type"::"Return Order":
                                txt_NomDoc := ESK012Txt;
                        END;
                    END;

                    //LM le 15-05-2013 =>commentaire specifique pour les commande achat
                    strCr := '<br />';
                    txt_rtn :=

                   'Bonjour,' + strCr + strCr +
                   'Vous trouverez ci-joint notre ' + txt_NomDoc + ' N° ' + pNoDoc + '.' + strCr + strCr +
                   'Merci de bien vouloir nous confirmer les quantités, prix et délai de livraison de chacune de ces pièces.' + strCr + strCr +
                   'Dans l''attente de votre réponse.' + strCr + strCr +
                   'Cordialement' + strCr;
                END;

            //OF
            // GR MIG2015
            //DATABASE::"BOM Journal Batch":
            //    BEGIN
            //        //Récupère l'enregistrement à imprimer
            //        BomJnlBatch.RESET();
            //        BomJnlBatch.SETVIEW(pRecordRef.GETVIEW);
            //        IF BomJnlBatch.FINDFIRST () THEN
            //          txt_NomDoc := ESK005;
            //    END;

            // Facture Vente
            DATABASE::"Sales Invoice Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesInvoiceHeader.RESET();
                    recSalesInvoiceHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesInvoiceHeader.FINDFIRST() THEN
                        txt_NomDoc := ESK006Txt;
                END;

            // BL Vente
            DATABASE::"Sales Shipment Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesShipmentHeader.RESET();
                    recSalesShipmentHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesShipmentHeader.FINDFIRST() THEN
                        txt_NomDoc := ESK007Txt;
                END;

            // Facture Vente
            DATABASE::"Sales Cr.Memo Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recSalesCreditHeader.RESET();
                    recSalesCreditHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recSalesCreditHeader.FINDFIRST() THEN
                        txt_NomDoc := ESK008Txt;
                END;

            // Bon de retour
            DATABASE::"Return Receipt Header":
                BEGIN
                    //Récupère l'enregistrement à imprimer
                    recReturnReceiptHeader.RESET();
                    recReturnReceiptHeader.SETVIEW(pRecordRef.GETVIEW());
                    IF recReturnReceiptHeader.FINDFIRST() THEN
                        txt_NomDoc := ESK010Txt;
                END;



            //Enregistrement non géré
            ELSE
                EXIT;
        END;

        //LM le 15-05-2013 =>commentaire specifique pour les commande achat
        IF txt_rtn = '' THEN BEGIN
            strCr := '<br />';
            txt_rtn :=
                   'Nous vous prions de bien vouloir trouver ci-joint notre ' + txt_NomDoc + ' N° ' + pNoDoc + '.' + strCr + strCr +
        'Nous restons à votre entière disposition et nous vous prions d''agréer l''expression de nos sentiments dévoués.'
         + strCr;
        END;
    end;

    procedure GetImpressionPdf(): Boolean
    begin
        EXIT(_ForcerImpressionPdf);
    end;

    procedure SendFaxFAXBIS(pFullPathFile: Text[255]; pFaxNumber: Text[30]; pFileName: Text[100]; _pDestinataire: Text[60])
    var
     LUserID: Record "User Setup";
        LCompanyInfo: Record "Company Information";
        LEmplFichierCommande: Text[500];
        LFichierCommande: File;
        LCheminDossierFax: Text[500];
        LEmailAR: Text[50];
       
    begin
        // AD Le 05-02-2015 => Réécrit pour le nouveau prestataire de fax FAXBIS

        //Affecte le numéro de Fax du destinataire
        IF pFaxNumber <> '' THEN BEGIN

            IF LUserID.GET(USERID) THEN
                LEmailAR := LUserID."E-Mail";

            IF LEmailAR = '' THEN BEGIN
                LCompanyInfo.GET();
                LEmailAR := LCompanyInfo."E-Mail";
            END;

            // GESTION FAX RTE SYMTA
            LCheminDossierFax := '\\faxbis-box\trafic$\Dep\In\';
            ///LCheminDossierFax := '\\SRV-DC3\public\TMP\';

            // LEmplFichierCommande := TEMPORARYPATH + pFileName ;
            // MCO => NAV2017 si on laisse cela alors tout le pdf est dans le fichier est le fax ne marche plus
            LEmplFichierCommande := TEMPORARYPATH + pFileName + '000';
            // FIN MCO NAV2017
            LFichierCommande.TEXTMODE(TRUE);
            LFichierCommande.WRITEMODE(TRUE);

            IF FILE.EXISTS(LEmplFichierCommande) THEN
                //LFichierCommande.OPEN(LEmplFichierCommande)
                LFichierCommande.CREATE(LEmplFichierCommande)
            ELSE
                LFichierCommande.CREATE(LEmplFichierCommande);

            LFichierCommande.SEEK(LFichierCommande.LEN);

            LFichierCommande.WRITE('/F=' + LCheminDossierFax + pFileName);
            LFichierCommande.WRITE('/S=smtp:' + LEmailAR);
            LFichierCommande.WRITE('/company=' + _pDestinataire);
            LFichierCommande.WRITE('/SUJET=');
            LFichierCommande.WRITE('/U=smtp:' + LEmailAR);
            LFichierCommande.WRITE('/D=' + _pDestinataire);
            LFichierCommande.WRITE('/P=' + pFaxNumber);

            LFichierCommande.CLOSE();

            CuFile.CopyServerFile(LEmplFichierCommande, LCheminDossierFax + 'HSP' + CONVERTSTR(DELCHR(USERID, '=', '/: .'), '\', '_') + '_' + DELCHR(FORMAT(CURRENTDATETIME), '=', '/: '), TRUE);
            //CuFile.DeleteServerFile(LEmplFichierCommande);

            CuFile.CopyServerFile(pFullPathFile, LCheminDossierFax + pFileName, TRUE);
            CuFile.DeleteServerFile(pFullPathFile);
            //CuFile.MoveFile(pFullPathFile, LCheminDossierFax + pFileName);
            //       ERROR('ad %1', LCheminDossierFax +'/HSP'+  DELCHR(USERID, '=', '\/: ') +'_' + DELCHR(FORMAT(CURRENTDATETIME), '=', '/: '));
            //        CuFile.MoveFile(LEmplFichierCommande, LCheminDossierFax +'/HSP'+  DELCHR(USERID, '=', '/: ') +'_' + DELCHR(FORMAT(CURRENTDATETIME), '=', '/: '));


            //Averti l'utilisateur Fax envoyé
            MESSAGE('Le Fax est dans la boite d''envois');
        END
        ELSE 
            MESSAGE('Pas de numéro de Fax');
         end;

    local procedure PurgeCaractereFichier(_pParam: Text): Text
    begin
        _pParam := DELCHR(_pParam, '=', '/: .');
        _pParam := CONVERTSTR(_pParam, '\', '_');
        _pParam := CONVERTSTR(_pParam, 'àäéèêëïîöô', 'aaeeeeiioo');
        EXIT(_pParam);
    end;
}

