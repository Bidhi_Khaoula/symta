codeunit 50073 "WIIO - Fonction"
{
    // // CFR 08/07/2020 - Visibilité des fonctions WebService : Locale = No > Yes
    // // CFR le 18/11/2020 : WIIO2 > Commentaire CocherligneReclassement
    // // CFR le 18/11/2020 : WIIO2 > SupprimerLigneReclassement
    // // CFR le 10/03/2021 Régie : CreateMvtEmpl() bloquer si qté=0 et surstock (sinon il a le changement de casier par défaut à traiter)
    // CFR le 15/09/2021 => R‚gie : ModifierColisPackingList() ajout Type colis
    // CFR le 22/09/2021 => R‚gie - Faire suivre le code zone dans les lignes ventes
    // CFR le 18/03/2022 => SFD20210929 > Gestion des inventaires
    // CFR le 15/06/2022 => SFD20210929 > CalculerQuantiteInventaire() : ajout paramŠtre pZeroQty
    // CFR le 09/09/2022 => SFD20210929 > UpdateFeuilleInventaire()
    // CFR le 20/10/2022 : conversion des d‚cimaux
    // CFR le 06/04/2023 - R‚gie : CreateMvtEmpl() Mise … jour du Code Zone
    // CFR le 02/05/2023 - R‚gie : demande du 13/03/2023 Forcer l'emplacement d'origine
    // CFR le 06/07/2023 - Support UpdateFeuilleInventaire()
    // CFR le 05/10/2023 => R‚gie - Blocage mouvement inter-zone si PICS en cours

    trigger OnRun()
    var
    /* var1: Code[20];
    var2: Code[20];
    var3: Code[20];
    dec1: Decimal;
    var4: Code[20];
    dec2: Decimal;
    int1: Integer;
    int2: Integer;
    char1: Char;
    char2: Char; */
    begin
        //EnregistrerLigneInventaire('ESKAPE', '30000|60000','9.1|9,2',FALSE,'');
        //CalculerQuantiteInventaire('SP', '', 'ESKAPE', '310A', '310A9', TRUE, TRUE, 'SYMTA\ESKAPE', TRUE, FALSE);
        //UpdateFeuilleInventaire('SP', '', 'ESKAPE', '310A', '310A9', TRUE, TRUE, 'SYMTA\ESKAPE', FALSE);
        //MESSAGE('FIN');
        //IF CreerColisPackList('EX20008779',var1, 'ZEBRA_PIC_2') THEN MESSAGE(var1) ELSE MESSAGE('KO');
        //IF ModifierColisPackList('PL000036', 'A', 99, 98, 97, 9.99, 'CFR', 'CARTON') THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //IF AffectColisPackList('PL000004',10000, 'SP015247', 1, 'EX20008771/A') THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //IF DesAffectColisPackList('PL000004', 'EX20008771/A', 'SP015247') THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //IF RefreshLignesPackingList('PL000004', 'EX20008771') THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //IF CopieLignePackingList('PL000016', 115000) THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //MESSAGE('n°'+FORMAT(AjouterLigneReclassement('REAP-1PT_O', 'SP002567', 999, 'M01-28-X1')));
        //MESSAGE('nb ligne : '+FORMAT(ModifierBacMiniload('REAP-MINIL', '10000', 'SmallBox')));
        //IF CocherLigneReclassement('REAP-MINIL',10000, FALSE) THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //EnregistrerLigneReclassement('RP-_O', '120000', '50');
        //PrintEtqPrepa('EX20008780', 0, '');
        //PrintEtqColisPackList('PL000009', 'A', 'ZEBRA-QTE');
        //IF CreateNewBin('1PT', 'SP', '43762','SP035675') THEN MESSAGE('OK') ELSE MESSAGE('KO');
        //CreerFeuilleReclassement('SURSTOCK');


        /*//ModifierColisPackList('EX20005813', 'B', 20, 30, 40, 3.99, 'COMMEN');
        //MajQuantitePrepa('EX20005813', 30000, 'TR7544002900', 1, TRUE); //
        //PrintEtqPrepa('EX20005813', 30000, 'ZEBRA_BAT3');
        //CreerFeuilleReclassement('ENTREE MINILOAD');
        //MajCapaciteEmplacement('MC-CA642', '', '13125', 70);

        //CreateNewBin('1PTZE', '', 'ADAGOIS','');
        //MESSAGE('ad %1', CreateNewBin('1PT', '', 'ADAGOIS',''));
        //var3 := 'MC-CA642ds';
        //ValidateItem(var3, var1, var2);

        var1 := 'MC-CA642';
        //ValidateEmpl('', '13125',var1, var2, dec1, var3);

        var1 := 'MC-CA642';
        var2 := 'MLD';
        //ValidateEmplSrc('',var1, var2,dec1, var3,dec2,var4);

        CreateMvtEmpl('TTESTAD', '',var1,'99002','PSMID', '13125', 3, 1, 'TESTAD020620');
        CreateMvtEmpl('', 'SP','SP035656','TAMPON', 'WIIOSUPPLY', '54216', 5, 'DEFAUT', 'Commentaire');

        message('AD %1 - %2 - %3 - %6 / %4 - %5', var1, var2, var3, dec1, dec2, var4);
        */
        EnregistrerLigneReclassement('RP-1PT_O', '10000', '50', TRUE, '217F04');
        MESSAGE('Terminé');

        //CreateMvtEmpl(pDocNo : Code[20];pLocation : Code[20];pItemNo : Code[20];pEmpl : Code[20];pPsmId : Code[20];pEmplSrc : Code[20];pQty : Decimal;pType : Text[50];pComment : Text[40]) : Boolean

    end;

    var
        Company: Record "Company Information";
        CduGFunctions: Codeunit "Codeunits Functions";
        ESK004Err: Label 'Rangement dans le même casier !';
        ESK006Err: Label 'Type empl. obligatoire';
        Text001Err: Label 'Veuillez créer une feuille de reclassement %1.', Comment = '%1 = Article';
        ESK005Err: Label 'La quantité ne peut pas être nulle !';



    procedure Login(pUser: Text[50]; _pCodeMagasin: Code[20]): Boolean
    var
        LWhseEmployee: Record "Warehouse Employee";
    begin
        /// Authentification ==> Fonction reprise et adaptée du CU 50070
        MagasinParDefaut(_pCodeMagasin); // Lecture magasin

        WITH LWhseEmployee DO BEGIN
            IF GET(pUser, _pCodeMagasin) OR GET(STRSUBSTNO('SYMTA\%1', pUser), _pCodeMagasin) THEN
                EXIT(TRUE);
        END;

        EXIT(FALSE);
    end;

    procedure ValidateEmplSrc(_pCodeMagasin: Code[20]; _pCodeArticle: Code[20]; var pBinCode: Code[20]; var pCapacity: Decimal; var pNewBinCode: Code[20]; var pQty: Decimal; var pCodeZone: Code[10]): Boolean
    var
        LItem: Record Item;
        lBinContent: Record "Bin Content";
        LWMSMngt: Codeunit "WMS Management";
    begin
        // ==> Fonction reprise et adaptée du CU 50070
        pCapacity := 0;
        pQty := 0;

        MagasinParDefaut(_pCodeMagasin);      // Lecture magasin
        LectureArticle(_pCodeArticle, LItem); // Lecture Article
        _pCodeArticle := LItem."No.";

        // Si on passe pas l'emplacement, on charge celui par defaut
        //pNewBinCode := pBinCode;
        IF pNewBinCode = '' THEN
            LWMSMngt.GetDefaultBin(LItem."No.", '', _pCodeMagasin, pNewBinCode);

        // On cherche le contenu emplacement
        CLEAR(lBinContent);
        lBinContent.SETRANGE("Location Code", _pCodeMagasin);
        lBinContent.SETRANGE("Bin Code", pBinCode);
        lBinContent.SETRANGE("Item No.", LItem."No.");
        lBinContent.SETRANGE("Unit of Measure Code", LItem."Base Unit of Measure");
        IF lBinContent.FINDFIRST() THEN BEGIN
            // On retourne les élements
            pCodeZone := lBinContent."Zone Code";
            lBinContent.CALCFIELDS(Quantity);
            pQty := lBinContent.Quantity;
            pCapacity := lBinContent.capacité;
            EXIT(TRUE);
        END;

        EXIT(FALSE);
    end;

    procedure ValidateEmpl(pLocation: Code[20]; pBinCode: Code[20]; pItemNo: Code[20]; var pCodeZone: Code[10]; var pCapacity: Decimal; var pConfirm: Text[250]): Boolean
    var
        LItem: Record Item;
        lBinContent: Record "Bin Content";
    begin
        // ==> Fonction reprise et adaptée du CU 50070
        pCapacity := 0;

        MagasinParDefaut(pLocation);    // Lecture magasin
        LectureArticle(pItemNo, LItem); // Lecture Article

        // On cherche le contenu emplacement
        CLEAR(lBinContent);
        lBinContent.SETRANGE("Location Code", pLocation);
        lBinContent.SETRANGE("Bin Code", pBinCode);
        lBinContent.SETRANGE("Item No.", LItem."No.");
        lBinContent.SETRANGE("Unit of Measure Code", LItem."Base Unit of Measure");
        IF lBinContent.FINDFIRST() THEN BEGIN
            // On retourne les élements
            pCodeZone := lBinContent."Zone Code";
            pCapacity := lBinContent.capacité;
            EXIT(TRUE);
        END;

        EXIT(FALSE);
    end;

    procedure ValidateItem(var pItemNo: Code[20]; var pItemNo2: Code[20]; var Description: Text[30]): Boolean
    var
        LItem: Record Item;
    begin
        // Validation de l'existance d'un article avec retour de la designation
        LectureArticle(pItemNo, LItem); // Lecture Article

        // Affectation des infos
        pItemNo := LItem."No.";
        pItemNo2 := LItem."No. 2";
        Description := LItem.Description;
        EXIT(TRUE);
    end;

    procedure CreateMvtEmpl(pDocNo: Code[20]; pLocation: Code[20]; pItemNo: Code[20]; pEmpl: Code[20]; pPsmId: Code[20]; pEmplSrc: Code[20]; pQty: Decimal; pType: Text[50]; pComment: Text[40]): Boolean
    var

        LItemJnlLine: Record "Item Journal Line";
        LWhseJnlLine: Record "Warehouse Journal Line";
        WhseJnlPostLine: Codeunit "Whse. Jnl.-Register Line";
        LSourceCodeSetup: Record "Source Code Setup";
        LBinSrc: Record Bin;
        LBinDest: Record Bin;
        WMSMngt: Codeunit "WMS Management";
        LBinContent: Record "Bin Content";
        LOldDefaultBinContent: Boolean;
        LLigneVente: Record "Sales Line";
        LLigneBP: Record "Warehouse Shipment Line";
        LLigneAchat: Record "Purchase Line";
        LLigneBR: Record "Warehouse Receipt Line";
        ItemJnlPost: Codeunit "Item Jnl.-Post";
        JnlSelected: Boolean;
        "lCapacité": Decimal;
        lUnit: Code[10];
        LQty: Integer;
        lItemJnlBatch: Record "Item Journal Batch";
        lBinCont: Record "Bin Content";
        lSeuil: Decimal;
        LItemJnlMgt: Codeunit ItemJnlManagement;
        LCurrentJnlBatchName: Code[20];
        LItem: Record Item;
    begin
        // ######################################################################
        // ATTENTION voir PA_50077.Valider() : fonction identique … partir de Nav
        // ######################################################################
        // Création d'un mouvement magasin ==> Fonction reprise et adaptée du CU 50070
        MagasinParDefaut(pLocation);    // Lecture magasin
        LectureArticle(pItemNo, LItem); // Lecture Article
        pItemNo := LItem."No.";

        LSourceCodeSetup.GET();
        IF (pEmpl = pEmplSrc) THEN
            ERROR(ESK004Err);

        // CFR le 10/03/2021 Régie : bloquer si qté=0 et surstock (sinon il a le changement de casier par défaut à traiter)
        IF (pQty = 0) AND (UPPERCASE(pType) = 'SURSTOCK') THEN
            ERROR(ESK005Err);

        CASE UPPERCASE(pType) OF
            'DEFAUT':
                ;
            'SURSTOCK':
                ;
            ELSE
                ERROR(ESK006Err);
        END;

        LItemJnlMgt.TemplateSelection(PAGE::"Item Reclass. Journal", 1, FALSE, LItemJnlLine, JnlSelected);
        IF NOT JnlSelected THEN ERROR('');
        LCurrentJnlBatchName := pPsmId;
        IF NOT lItemJnlBatch.GET('RECLASS', LCurrentJnlBatchName) THEN BEGIN
            lItemJnlBatch.INIT();
            lItemJnlBatch."Journal Template Name" := 'RECLASS';
            lItemJnlBatch.SetupNewBatch();
            lItemJnlBatch.PSM := TRUE;
            lItemJnlBatch.Name := LCurrentJnlBatchName;
            lItemJnlBatch.Description := '';
            lItemJnlBatch.INSERT(TRUE);
            COMMIT();
        END;

        LCurrentJnlBatchName := lItemJnlBatch.Name;
        LItemJnlMgt.OpenJnl(LCurrentJnlBatchName, LItemJnlLine);
        IF LCurrentJnlBatchName <> pPsmId THEN
            ERROR(Text001Err, pPsmId);

        IF UPPERCASE(pType) = 'DEFAUT' THEN BEGIN
            WITH LBinContent DO BEGIN
                // On regarde si le casier source etait le casier par defaut de l'article
                CLEAR(LBinContent);
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE("Bin Code", pEmplSrc);
                FINDFIRST();
                LOldDefaultBinContent := Default;

                // recherche du casier par défaut actuel
                SETRANGE("Bin Code");
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE(Default, TRUE);
                IF FINDFIRST() THEN BEGIN
                    Fixed := TRUE;
                    Default := FALSE;
                    lCapacité := capacité;
                    lSeuil := "% Seuil Capacité Réap. Pick."; // MCO Le 04-10-2018 => Régie
                    lUnit := "Unit of Measure Code";
                    MODIFY();
                END;

                //affectation du casier par defaut
                RESET();
                SETRANGE("Location Code", pLocation);
                SETRANGE("Item No.", pItemNo);
                SETRANGE("Bin Code", pEmpl);
                IF FINDFIRST() THEN BEGIN
                    Fixed := TRUE;
                    Default := TRUE;
                    // MCO Le 16-01-2018
                    IF LOldDefaultBinContent THEN BEGIN
                        // FIN MCO Le 16-01-2018
                        capacité := lCapacité;

                        "% Seuil Capacité Réap. Pick." := lSeuil; // MCO Le 04-10-2018 => Régie
                    END;
                    MODIFY();
                END ELSE BEGIN
                    RESET();
                    INIT();
                    VALIDATE("Location Code", pLocation);
                    VALIDATE("Item No.", pItemNo);
                    VALIDATE("Unit of Measure Code", lUnit);
                    VALIDATE("Bin Code", pEmpl);

                    LBinDest.GET(pLocation, pEmpl);

                    VALIDATE("Zone Code", LBinDest."Zone Code");
                    Fixed := TRUE;
                    Default := TRUE;
                    // MCO Le 16-01-2018
                    IF LOldDefaultBinContent THEN BEGIN
                        // FIN MCO Le 16-01-2018
                        capacité := lCapacité;
                        "% Seuil Capacité Réap. Pick." := lSeuil; // MCO Le 04-10-2018 => Régie
                    END;
                    INSERT(TRUE);
                END;
            END;

            // Si le casier source était le casier par defaut, on va le modifier sur les transfert
            IF LOldDefaultBinContent THEN BEGIN
                CLEAR(LItemJnlLine);
                LItemJnlLine.SETRANGE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
                LItemJnlLine.SETRANGE("Item No.", pItemNo);
                LItemJnlLine.SETRANGE("New Location Code", pLocation);
                LItemJnlLine.SETRANGE("New Bin Code", pEmplSrc);
                LItemJnlLine.MODIFYALL("New Bin Code", pEmpl);
            END;

            // CFR le 06/04/2023 - R‚gie : CreateMvtEmpl() Mise … jour du Code Zone
            LBinSrc.GET(pLocation, pEmplSrc);
            LBinDest.GET(pLocation, pEmpl);

            LLigneVente.SETRANGE(Type, LLigneVente.Type::Item);
            LLigneVente.SETRANGE("No.", pItemNo);
            LLigneVente.SETRANGE("Bin Code", pEmplSrc);
            // CFR le 22/09/2021 => R‚gie - Faire suivre le code zone dans les lignes ventes
            LLigneVente.MODIFYALL("Zone Code", LBinDest."Zone Code");
            LLigneVente.MODIFYALL("Bin Code", pEmpl);

            LLigneBP.SETRANGE("Item No.", pItemNo);
            LLigneBP.SETRANGE("Bin Code", pEmplSrc);
            // CFR le 05/10/2023 => R‚gie - Blocage mouvement inter-zone si PICS en cours car les PICS sont g‚n‚r‚s par zone
            IF (LBinDest."Zone Code" <> LBinSrc."Zone Code") THEN
                IF LLigneBP.FINDFIRST() THEN
                    ERROR('Mouvement impossible.\Il existe au moins 1 ligne de PICS (nø%1) en cours de pr‚paration.', LLigneBP."No.");
            // FIN CFR le 05/10/2023

            LLigneBP.MODIFYALL("Bin Code", pEmpl);

            // achat
            LLigneAchat.SETRANGE(Type, LLigneAchat.Type::Item);
            LLigneAchat.SETRANGE("No.", pItemNo);
            LLigneAchat.SETRANGE("Bin Code", pEmplSrc);
            LLigneAchat.MODIFYALL("Bin Code", pEmpl);

            LLigneBR.SETRANGE("Item No.", pItemNo);
            LLigneBR.SETRANGE("Bin Code", pEmplSrc);
            LLigneBR.MODIFYALL("Bin Code", pEmpl);

        END;

        IF pQty <> 0 THEN BEGIN // ANI Le 07-09-2017 Ticket 24541 Régie
            // section code avec reclassement par transfert
            WITH LItemJnlLine DO BEGIN
                CLEAR(LItemJnlLine);
                INIT();
                VALIDATE("Journal Template Name", 'RECLASS');
                VALIDATE("Entry Type", "Entry Type"::Transfer);
                VALIDATE("Journal Batch Name", LCurrentJnlBatchName);

                VALIDATE("Posting Date", WORKDATE());
                VALIDATE("Document No.", USERID);
                VALIDATE("A Valider", TRUE);   // ???
                VALIDATE("Item No.", pItemNo);

                // emplacement source
                VALIDATE("Location Code", pLocation);
                VALIDATE("Bin Code", pEmplSrc);
                // emplacement de destination
                VALIDATE("New Location Code", pLocation);
                VALIDATE("New Bin Code", pEmpl);

                LBinDest.GET("Location Code", "Bin Code");

                VALIDATE(Quantity, pQty);
                VALIDATE(Commentaire, pComment);
                INSERT(TRUE);
            END;
            CduGFunctions.SetHideValidationDialog(TRUE);
            ItemJnlPost.RUN(LItemJnlLine);

        END;

        EXIT(TRUE);
    end;

    procedure CreateNewBin(pCodeZone: Code[10]; pCodeLocation: Code[10]; pCodeEmpl: Code[20]; pCodeItem: Code[10]): Boolean
    var
        lbin: Record Bin;
        LItem: Record Item;
    begin
        // Création d'un nouvel emplacement ==> Fonction reprise et adaptée du CU 50070
        MagasinParDefaut(pCodeLocation); // Lecture magasin

        IF (pCodeItem <> '') THEN BEGIN
            LectureArticle(pCodeItem, LItem); // Lecture Article
            pCodeItem := LItem."No.";
        END;

        IF pCodeEmpl = '' THEN
            ERROR('Veuillez saisir un code emplacement');

        IF NOT (CheckZoneExists(pCodeZone, pCodeLocation)) THEN
            ERROR('Code zone inconnu');

        IF NOT lbin.GET(pCodeLocation, pCodeEmpl) THEN BEGIN
            lbin.INIT();
            lbin.VALIDATE("Location Code", pCodeLocation);
            lbin.VALIDATE("Zone Code", pCodeZone);
            lbin.VALIDATE(Code, pCodeEmpl);
            // CFR le 26/08/2020
            //paramètre inutile : pBinTypeCode : Code[10]
            //lbin.VALIDATE("Bin Type Code", pBinTypeCode);
            // FIN CFR le 26/08/2020
            IF (pCodeItem <> '') THEN
                lbin.VALIDATE("Item Filter", pCodeItem);
            lbin.INSERT(TRUE);
            EXIT(TRUE);
        END
        ELSE
            EXIT(FALSE);
    end;

    procedure MajCapaciteEmplacement(_pCodeArticle: Code[20]; _pCodeMagasin: Code[20]; _pCodeEmplacement: Code[20]; "_pNouvelleCapacité": Decimal): Boolean
    var
        LItem: Record Item;
        LBinContent: Record "Bin Content";
    begin
        // MAJ de la capacité d'un contenu emplacement
        MagasinParDefaut(_pCodeMagasin); // Lecture magasin
        LectureArticle(_pCodeArticle, LItem); // Lecture Article

        // Lecture du contenu emplacement (on ne gère pas la variante)
        LBinContent.GET(_pCodeMagasin, _pCodeEmplacement, LItem."No.", '', LItem."Base Unit of Measure");

        // Modification de la capacité
        LBinContent.VALIDATE(capacité, _pNouvelleCapacité);
        LBinContent.MODIFY(TRUE);

        EXIT(TRUE);
    end;

    local procedure "--- Fonctions Prepa commande ---"()
    begin
    end;

    procedure MajQuantitePrepa(_pNoBp: Code[20]; _pNoLigneBP: Integer; _pCodeArticle: Code[20]; _pQteAExpedier: Decimal; _pPreleve: Boolean): Boolean
    var
        LWhseShipLine: Record "Warehouse Shipment Line";
        LItem: Record Item;
    begin
        // MAJ de la quantité préparée et du booleen spécfiant que la ligne est préparée

        // Lecture de la ligne de BP
        LWhseShipLine.GET(_pNoBp, _pNoLigneBP);

        LectureArticle(_pCodeArticle, LItem); // Lecture Article pour vérif code
        LWhseShipLine.TESTFIELD("Item No.", LItem."No.");

        // Mise à jour de la ligne
        LWhseShipLine.VALIDATE("Qty. to Ship", _pQteAExpedier);
        LWhseShipLine.VALIDATE("WIIO Préparation", _pPreleve);
        LWhseShipLine.MODIFY(TRUE);

        EXIT(TRUE);
    end;

    procedure CreerColisPackList(pNoBp: Code[20]; var pNoColis: Code[20]; pCodeImprimante: Code[20]): Boolean
    var
        LWhseShipHeader: Record "Warehouse Shipment Header";
        LPackingListPack: Record "Packing List Package";
        LPackingListHeader: Record "Packing List Header";
        LNoColis: Code[20];
        LIndice: Integer;
        LIndiceLetter: Char;
        LPartieEntiere: Integer;
        LIndiceLetter2: Char;
    begin
        // Création d'un colis pour un BP pour une packingList

        // Vérification des infos
        LWhseShipHeader.GET(pNoBp);
        LWhseShipHeader.TESTFIELD("Packing List No.");

        LPackingListHeader.GET(LWhseShipHeader."Packing List No.");
        // Recherche du dernier colis
        LPackingListHeader.GetNextColisNoIndice(LNoColis, LIndice);

        // Création du colis
        CLEAR(LPackingListPack);
        LPackingListPack.VALIDATE("Packing List No.", LWhseShipHeader."Packing List No.");
        LPackingListPack.VALIDATE("Package No.", LNoColis);
        LPackingListPack.VALIDATE("Package Type", LPackingListPack."Package Type"::Carton);
        LPackingListPack.VALIDATE("Indice Colis", LIndice);
        LPackingListPack.INSERT(TRUE);

        pNoColis := LPackingListPack."Package No.";

        // Reste à gérer l'imprimante
        //PrintEtqColisPackList(LPackingListPack."Packing List No.", LPackingListPack."Package No.", pCodeImprimante);

        EXIT(TRUE);
    end;

    PROCEDURE ModifierColisPackList(pNoPackingList: Code[20]; pNoColis: Code[20]; pLongueur: Decimal; pLargeur: Decimal; pHauteur: Decimal; pPoids: Decimal; pCommentaire: Text[50]; pTypeColis: Text[50]): Boolean;
    var
        lWhseShipHeader: Record "Warehouse Shipment Header";
        lPackingListPack: Record "Packing List Package";
        lNoColis: Code[20];
    begin

        IF lPackingListPack.GET(pNoPackingList, pNoColis) THEN BEGIN
            // Modification d'un colis pour un BP pour une packingList
            lPackingListPack.VALIDATE("Package Weight", pPoids);
            lPackingListPack.VALIDATE(Length, pLongueur);
            lPackingListPack.VALIDATE(Width, pLargeur);
            lPackingListPack.VALIDATE(Height, pHauteur);
            lPackingListPack.VALIDATE("Comment 1", pCommentaire);
            // CFR le 15/09/2021 => R‚gie : ModifierColisPackingList() ajout Type colis
            IF pTypeColis = 'CARTON' THEN
                lPackingListPack.VALIDATE("Package Type", lPackingListPack."Package Type"::Carton);
            IF pTypeColis = 'PALETTE' THEN
                lPackingListPack.VALIDATE("Package Type", lPackingListPack."Package Type"::Palette);
            // FIN CFR le 15/09/2021
            lPackingListPack.MODIFY(TRUE);
            EXIT(TRUE);
        END;

        EXIT(FALSE);
    end;

    procedure RefreshLignesPackingList(pNoPackingList: Code[20]; pNoBP: Code[20]): Boolean
    var
        lWhseShipHeader: Record "Warehouse Shipment Header";
        lPackingListHeader: Record "Packing List Header";
        lPackingListLine: Record "Packing List Line";
        lPackingListMgmt: Codeunit "Packing List Mgmt";
    begin

        // Vérification des paramètres
        IF NOT lPackingListHeader.GET(pNoPackingList) THEN
            ERROR('N° Packing List inconnu : %1', pNoPackingList);
        IF NOT lWhseShipHeader.GET(pNoBP) THEN
            ERROR('N° Expédition Entrepôt inconnu : %1', pNoBP);
        IF lWhseShipHeader."Packing List No." <> pNoPackingList THEN
            ERROR('Expédition Entrepôt n°%1 n''est pas associée à Packing List n°%2', pNoBP, pNoPackingList);

        // Initialisation des lignes
        lPackingListMgmt.InitPackingListFromWhseShipment(lPackingListHeader, lWhseShipHeader);

        // Valeur de retour
        lPackingListLine.SETRANGE("Packing List No.", pNoPackingList);
        lPackingListLine.SETRANGE("Whse. Shipment No.", pNoBP);
        IF lPackingListLine.COUNT() = 0 THEN
            EXIT(FALSE)
        ELSE
            EXIT(TRUE);
    end;

    procedure CopieLignePackingList(pNoPackingList: Code[20]; pNoPackingListLine: Integer): Boolean
    var
        lPackingListLine: Record "Packing List Line";
        LItem: Record Item;
        lPackingListMgmt: Codeunit "Packing List Mgmt";
    begin
        // Lecture Article
        //LectureArticle(pCodeArticle, LItem);
        //pCodeArticle := LItem."No.";

        // Vérification des paramètres
        IF NOT lPackingListLine.GET(pNoPackingList, pNoPackingListLine) THEN
            ERROR('Ligne Packing List inexistante : %1 - %2', pNoPackingList, pNoPackingListLine);

        // Copie de la ligne
        lPackingListMgmt.LineDuplicate(lPackingListLine);

        EXIT(TRUE);
    end;

    procedure AffectColisPackList(pNoPackingList: Code[20]; pNoPackingListLine: Integer; pCodeArticle: Code[20]; pQte: Decimal; pNoColis: Code[20]): Boolean
    var
        lPackingListLine: Record "Packing List Line";
        LItem: Record Item;
        lPackingListPack: Record "Packing List Package";
    begin
        // Lecture Article
        LectureArticle(pCodeArticle, LItem);
        pCodeArticle := LItem."No.";

        // Vérification des paramètres
        IF NOT lPackingListLine.GET(pNoPackingList, pNoPackingListLine) THEN
            ERROR('Ligne Packing List inexistante : %1 - %2', pNoPackingList, pNoPackingListLine);
        IF lPackingListLine."Item No." <> LItem."No." THEN
            ERROR('Article %1 inexistant pour ligne Packing List %2 - %3', lPackingListLine."Item No.", pNoPackingList, pNoPackingListLine);

        lPackingListLine.VALIDATE("Package No.", pNoColis);
        lPackingListLine.VALIDATE("Line Quantity", pQte);
        lPackingListLine.MODIFY(TRUE);

        EXIT(TRUE);
    end;

    procedure DesAffectColisPackList(pNoPackingList: Code[20]; pNoPackingListLine: Integer; pNoColis: Code[20]; pCodeArticle: Code[20]): Boolean
    var
        lPackingListLine: Record "Packing List Line";
        LItem: Record Item;
    begin
        // Lecture Article
        LectureArticle(pCodeArticle, LItem);
        pCodeArticle := LItem."No.";

        IF NOT lPackingListLine.GET(pNoPackingList, pNoPackingListLine) THEN
            ERROR('Ligne Packing List inexistante : %1 - %2', pNoPackingList, pNoPackingListLine);

        lPackingListLine.VALIDATE("Package No.", '');
        lPackingListLine.MODIFY(TRUE);

        EXIT(TRUE);
    end;

    local procedure "--- Fonction étiquettes ---"()
    begin
    end;

    procedure PrintEtqPrepa(_pNoBp: Code[20]; _pNoLigneBP: Integer; _pCodeImprimante: Code[20]): Boolean
    var
        LWhseShipHeader: Record "Warehouse Shipment Header";
        LWhseShipLine: Record "Warehouse Shipment Line";
        TmpWhseShipHeader: Record "Warehouse Shipment Header" temporary;
        GestionBP: Codeunit "Gestion Bon Préparation";
    begin
        // Imprime les étiquettes préparation d'un BP (si n° ligne = 0, toutes les lignes)

        // Vérif des infos
        LWhseShipHeader.GET(_pNoBp);
        IF _pNoLigneBP <> 0 THEN LWhseShipLine.GET(_pNoBp, _pNoLigneBP);

        // Impression [RESTE A GERER L'IMPRIMANTE --- A VOIR AVEC VINCENT COMMENT FAIRE]
        // >> Vu avec VDO le 01/09/2020 : c'est le CU50013 GestionBP qui fixe automatiquement l'imprimante en fonction de la zone.
        TmpWhseShipHeader := LWhseShipHeader;
        TmpWhseShipHeader.Insert();
        GestionBP.ImprimeEtiquettePrepa(TmpWhseShipHeader, _pNoLigneBP);

        EXIT(TRUE);
    end;

    procedure PrintEtqArticle(_pCodeArticle: Code[20]; _Quantite: Integer; _pCodeImprimante: Code[20]; _pCodeFormat: Code[10]; _pQteEtiquette: Integer; _pComment1Palette: Text): Boolean
    var
        LItem: Record Item;
        LGestionMagasin: Codeunit "Gestion Magasin";
        LTabComment: array[3] of Text;
        LCuEtq: Codeunit "Etiquettes Articles";
    begin
        // Imprime les étiquettes article
        LectureArticle(_pCodeArticle, LItem); // Lecture Article pour vérif code

        // Vérif des infos
        IF _Quantite < 1 THEN _Quantite := 1;
        IF _pQteEtiquette < 1 THEN _pQteEtiquette := 1;

        // Champ [commentaire 1 / palette] idem page 50097
        LTabComment[1] := _pComment1Palette;

        // Impression [RESTE A GERER L'IMPRIMANTE --- A VOIR AVEC VINCENT COMMENT FAIRE]
        LCuEtq.EditionEtiquetteUniquNiceLabel(LItem, _Quantite, '', '', LGestionMagasin.GetCasierDefaut(LItem."No."),
          LTabComment, '', _pQteEtiquette, _pCodeFormat, _pCodeImprimante);

        EXIT(TRUE);
    end;

    procedure PrintEtqColisPackList(_pNoPackingList: Code[20]; _pNoColis: Code[20]; _pCodeImprimante: Code[20])
    var
        LPackingListColis: Record "Packing List Package";
        GestionBP: Codeunit "Gestion Bon Préparation";
    begin
        // Imprime une étiquette colis de packingList

        // Lecture du colis .
        LPackingListColis.GET(_pNoPackingList, _pNoColis);

        // Impression
        GestionBP.ImprimerEtiquetteColis(LPackingListColis."Packing List No.", LPackingListColis."Package No.", _pCodeImprimante);
    end;

    local procedure "--- Fonctions réappro ---"()
    begin
    end;

    procedure CreerFeuilleReclassement(pTypeFeuille: Text[50]): Code[10]
    var
        lItemJournalBatch: Record "Item Journal Batch";
        LNomFeuille: Code[10];
        SuffixeIn: Code[4];
        SuffixeOut: Code[4];
    begin

        SuffixeOut := '_O';
        SuffixeIn := '_I';

        // Créer une feuille de reclassement - Nom = [WIJJMM_X]
        LNomFeuille := 'WI' + FORMAT(DATE2DMY(WORKDATE(), 1), 0, '<Integer,2><Filler Character,0>') + FORMAT(DATE2DMY(WORKDATE(), 2), 0, '<Integer,2><Filler Character,0>') + '_';

        // Cherche la dernière feuille WIIO
        CLEAR(lItemJournalBatch);
        lItemJournalBatch.SETRANGE("Journal Template Name", 'RECLASS');
        lItemJournalBatch.SETFILTER(Name, LNomFeuille + '*');
        IF lItemJournalBatch.FINDLAST() THEN
            LNomFeuille := INCSTR(lItemJournalBatch.Name)
        ELSE
            LNomFeuille := LNomFeuille + '1';

        CLEAR(lItemJournalBatch);
        lItemJournalBatch.VALIDATE("Journal Template Name", 'RECLASS');
        lItemJournalBatch.SetupNewBatch();
        lItemJournalBatch."Posting No. Series" := '';

        CASE UPPERCASE(pTypeFeuille) OF
            /*
            'SURSTOCK'        :
              BEGIN
                // PICKING et SURSTOCK sont créées en même temps...
                // 1 - le surstock
                lItemJournalBatch.VALIDATE(Name, LNomFeuille+SuffixeOut);
                lItemJournalBatch.VALIDATE("Type Feuille Reclassement WIIO", lItemJournalBatch."Type Feuille Reclassement WIIO"::Surstock);
                lItemJournalBatch.VALIDATE(MiniLoad, FALSE);
                lItemJournalBatch.VALIDATE(Description, 'WIIO Surstock'+SuffixeOut);
                lItemJournalBatch.INSERT(TRUE);
                // 2 - le picking
                lItemJournalBatch.VALIDATE(Name, LNomFeuille+SuffixeIn);
                lItemJournalBatch.VALIDATE("Type Feuille Reclassement WIIO", lItemJournalBatch."Type Feuille Reclassement WIIO"::Picking);
                lItemJournalBatch.VALIDATE(MiniLoad, FALSE);
                lItemJournalBatch.VALIDATE(Description, 'WIIO Surstock'+SuffixeIn);
                lItemJournalBatch.INSERT(TRUE);
                // la valeur de retour = le surstock
                LNomFeuille += SuffixeOut;
              END;
            */
            'ENTREE MINILOAD':
                BEGIN
                    lItemJournalBatch.VALIDATE(Name, LNomFeuille);
                    lItemJournalBatch.VALIDATE("Type Feuille Reclassement WIIO", lItemJournalBatch."Type Feuille Reclassement WIIO"::"Entrée Miniload");
                    lItemJournalBatch.VALIDATE(MiniLoad, TRUE);
                    lItemJournalBatch.VALIDATE(Description, 'WIIO Entrée Miniload');
                    lItemJournalBatch.INSERT(TRUE);
                END;
            'SORTIE MINILOAD':
                BEGIN
                    lItemJournalBatch.VALIDATE(Name, LNomFeuille);
                    lItemJournalBatch.VALIDATE("Type Feuille Reclassement WIIO", lItemJournalBatch."Type Feuille Reclassement WIIO"::"Sortie Miniload");
                    lItemJournalBatch.VALIDATE(MiniLoad, TRUE);
                    lItemJournalBatch.VALIDATE(Description, 'WIIO Sortie Miniload');
                    lItemJournalBatch.INSERT(TRUE);
                END;
            ELSE
                ERROR('Type Feuille Reclassement non géré : %1', pTypeFeuille);
        END;

        EXIT(LNomFeuille);

    end;

    PROCEDURE EnregistrerLigneReclassement(_pNomFeuille: Code[10]; _ListeLigne: Text; _ListeQte: Text; _ValiderON: Boolean; _ListeFromBin: Text): Boolean;
    var
        lItemJournalBatch: Record "Item Journal Batch";
        i: Integer;
        j: Integer;
        LNbLigne: Integer;
        LTabLigne: array[1000] of Text;
        LTabQte: array[1000] of Text;
        LNbQte: Integer;
        lItemJournalLine: Record "Item Journal Line";
        LItemJnlPost: Codeunit "Item Jnl.-Post";
        NoLigne: Integer;
        LNbFromBin: Integer;
        LTabFromBin: ARRAY[1000] OF Text;
        lBinCodeTmp: Code[20];
    begin
        // Cherche la  feuille WIIO
        IF NOT lItemJournalBatch.GET('RECLASS', _pNomFeuille) THEN
            ERROR('Feuille reclassement inexistante %1', _pNomFeuille);

        // On Charge les tableaux, c'est … dire du texte s‚par‚ par des "|"
        j := 1;
        FOR i := 1 TO STRLEN(_ListeLigne) DO BEGIN
            IF _ListeLigne[i] = '|' THEN
                j += 1
            ELSE
                LTabLigne[j] += FORMAT(_ListeLigne[i]);
        END;
        LNbLigne := j;
        j := 1;
        FOR i := 1 TO STRLEN(_ListeQte) DO BEGIN
            IF _ListeQte[i] = '|' THEN
                j += 1
            ELSE
                LTabQte[j] += FORMAT(_ListeQte[i]);
        END;
        LNbQte := j;

        // CFR le 02/05/2023 - R‚gie : demande du 13/03/2023 Forcer l'emplacement d'origine
        j := 1;
        FOR i := 1 TO STRLEN(_ListeFromBin) DO BEGIN
            IF _ListeFromBin[i] = '|' THEN
                j += 1
            ELSE
                LTabFromBin[j] += FORMAT(_ListeFromBin[i]);
        END;
        LNbFromBin := j;
        // FIN CFR le 02/05/2023

        IF (LNbLigne <> LNbQte) OR (LNbLigne <> LNbFromBin) THEN
            ERROR('Les listes ne sont pas de même longueur (%1 <> %2 <> %3) !', LNbLigne, LNbQte, LNbFromBin);

        // On met à jour les lignes : RAZ du champ [A valider]
        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        lItemJournalLine.MODIFYALL("A Valider", FALSE);

        // Mise a jour en fonction des tableaux remplis
        CLEAR(lItemJournalLine);
        FOR i := 1 TO LNbLigne DO BEGIN
            EVALUATE(NoLigne, LTabLigne[i]);
            lItemJournalLine.GET(lItemJournalBatch."Journal Template Name", lItemJournalBatch.Name, NoLigne);
            EVALUATE(lItemJournalLine.Quantity, LTabQte[i]);
            lItemJournalLine.VALIDATE(Quantity);
            lItemJournalLine.VALIDATE("A Valider", TRUE);

            // CFR le 02/05/2023 - R‚gie : demande du 13/03/2023 Forcer l'emplacement d'origine
            IF lItemJournalBatch."Type Feuille Reclassement WIIO" = lItemJournalBatch."Type Feuille Reclassement WIIO"::Surstock THEN BEGIN
                IF (LTabFromBin[i] <> '') THEN BEGIN
                    lBinCodeTmp := lItemJournalLine."New Bin Code";
                    lItemJournalLine.VALIDATE("Bin Code", LTabFromBin[i]);
                    lItemJournalLine.VALIDATE("New Bin Code", lBinCodeTmp);
                END;
            END;
            // FIN CFR le 02/05/2023

            lItemJournalLine.MODIFY();
        END;

        // Validation des lignes [A valider]
        IF (_ValiderON) THEN BEGIN
            CLEAR(lItemJournalLine);
            lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
            lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
            lItemJournalLine.SETRANGE("A Valider", TRUE);
            lItemJournalLine.FINDSET();
            CduGFunctions.SetHideValidationDialog(TRUE);
            LItemJnlPost.RUN(lItemJournalLine);
        END;

        EXIT(TRUE);
    end;

    procedure AjouterLigneReclassement(_pNomFeuille: Code[10]; _pCodeArticle: Code[20]; _pQuantity: Decimal; _pBinCode: Code[20]): Integer
    var
        lItemJournalBatch: Record "Item Journal Batch";
        lItemJournalLine: Record "Item Journal Line";
        LNoLigne: Integer;
        LItem: Record Item;
        LLocation: Record Location;
        LEmplacementParDefaut: Code[20];
        LEmplacementMiniload: Code[20];
        LWMSMngt: Codeunit "WMS Management";
        LInventorySetup: Record "Inventory Setup";
        LNomFeuilleIN: Code[10];
        lItemJournalBatchIN: Record "Item Journal Batch";
    begin
        // Ajoute une ligne dans une feuille de reclassement

        // Vérification / lecture des données
        LectureArticle(_pCodeArticle, LItem);
        MagasinParDefaut(lItemJournalLine."Location Code");
        LLocation.GET(lItemJournalLine."Location Code");
        LLocation.TESTFIELD("Emplacement Tampon WIIO");

        lItemJournalBatch.GET('RECLASS', _pNomFeuille);
        LWMSMngt.GetDefaultBin(LItem."No.", '', LLocation.Code, LEmplacementParDefaut);
        LInventorySetup.GET();
        LInventorySetup.TESTFIELD("Code emplacement MiniLoad");
        LEmplacementMiniload := LInventorySetup."Code emplacement MiniLoad";

        // Cherche no ligne
        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        IF lItemJournalLine.FINDLAST() THEN
            LNoLigne := lItemJournalLine."Line No." + 1000
        ELSE
            LNoLigne := 1000;

        // Creation de la ligne
        CLEAR(lItemJournalLine);

        lItemJournalLine.VALIDATE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.VALIDATE("Journal Batch Name", lItemJournalBatch.Name);
        lItemJournalLine.VALIDATE("Line No.", LNoLigne);
        lItemJournalLine.VALIDATE("Entry Type", lItemJournalLine."Entry Type"::Transfer);
        lItemJournalLine.VALIDATE("Posting Date", WORKDATE());
        lItemJournalLine.VALIDATE("Item No.", LItem."No.");
        lItemJournalLine.VALIDATE("Document No.", lItemJournalBatch.Name);
        lItemJournalLine.VALIDATE("Location Code", LLocation.Code);
        lItemJournalLine.VALIDATE("New Location Code", LLocation.Code);
        lItemJournalLine.VALIDATE(Quantity, _pQuantity);

        CASE lItemJournalBatch."Type Feuille Reclassement WIIO" OF
            lItemJournalBatch."Type Feuille Reclassement WIIO"::Surstock:
                BEGIN
                    lItemJournalLine.VALIDATE("Bin Code", _pBinCode);
                    lItemJournalLine.VALIDATE("New Bin Code", LLocation."Emplacement Tampon WIIO");
                END;
            lItemJournalBatch."Type Feuille Reclassement WIIO"::Picking:
                BEGIN
                    lItemJournalLine.VALIDATE("Bin Code", LLocation."Emplacement Tampon WIIO");
                    lItemJournalLine.VALIDATE("New Bin Code", LEmplacementParDefaut);
                END;
            lItemJournalBatch."Type Feuille Reclassement WIIO"::"Entrée Miniload":
                BEGIN
                    lItemJournalLine.VALIDATE("Bin Code", LEmplacementParDefaut);
                    lItemJournalLine.VALIDATE("New Bin Code", LEmplacementMiniload);
                END;
            lItemJournalBatch."Type Feuille Reclassement WIIO"::"Sortie Miniload":
                BEGIN
                    lItemJournalLine.VALIDATE("Bin Code", LEmplacementMiniload);
                    lItemJournalLine.VALIDATE("New Bin Code", LEmplacementParDefaut);
                END;
        END;

        lItemJournalLine.Insert();

        EXIT(LNoLigne);
    end;

    procedure CocherLigneReclassement(pNomFeuille: Code[10]; pNoLigne: Integer; pCocher: Boolean; pCommentaire: Text[40]): Boolean
    var
        lItemJournalBatch: Record "Item Journal Batch";
        i: Integer;
        j: Integer;
        LNbLigne: Integer;
        LTabLigne: array[1000] of Text;
        LTabQte: array[1000] of Text;
        LNbQte: Integer;
        lItemJournalLine: Record "Item Journal Line";
        LItemJnlPost: Codeunit "Item Jnl.-Post";
        NoLigne: Integer;
    begin
        // Cherche la  feuille WIIO
        IF NOT lItemJournalBatch.GET('RECLASS', pNomFeuille) THEN
            ERROR('Feuille reclassement inexistante %1', pNomFeuille);

        IF NOT lItemJournalLine.GET(lItemJournalBatch."Journal Template Name", lItemJournalBatch.Name, pNoLigne) THEN
            ERROR('Ligne feuille reclassement inexistante %1 - %2', pNomFeuille, pNoLigne);

        lItemJournalLine.VALIDATE("A Valider", pCocher);
        // CFR le 18/11/2020 : WIIO2
        lItemJournalLine.VALIDATE(Commentaire, pCommentaire);
        lItemJournalLine.MODIFY();

        EXIT(TRUE);
    end;

    procedure SupprimerLigneReclassement(pNomFeuille: Code[10]; pNoLigne: Integer): Boolean
    var
        lItemJournalBatch: Record "Item Journal Batch";
        lItemJournalLine: Record "Item Journal Line";
    begin
        // Cherche la  feuille WIIO
        IF NOT lItemJournalBatch.GET('RECLASS', pNomFeuille) THEN
            ERROR('Feuille reclassement inexistante %1', pNomFeuille);

        IF NOT lItemJournalLine.GET(lItemJournalBatch."Journal Template Name", lItemJournalBatch.Name, pNoLigne) THEN
            ERROR('Ligne feuille reclassement inexistante %1 - %2', pNomFeuille, pNoLigne);

        IF lItemJournalLine.DELETE(TRUE) THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure ModifierBacMiniload(_pNomFeuille: Code[10]; _ListeLigne: Text; _ListeBac: Text): Integer
    var
        lItemJournalBatch: Record "Item Journal Batch";
        i: Integer;
        j: Integer;
        LNbLigne: Integer;
        LTabLigne: array[1000] of Text;
        LTabBac: array[1000] of Text;
        LNbBac: Integer;
        lItemJournalLine: Record "Item Journal Line";
        NoLigne: Integer;
        LItem: Record Item;
        Lcu_MiniLoad: Codeunit "Gestion MINILOAD";
        lNbModification: Integer;
    begin
        // Cherche la  feuille WIIO
        IF NOT lItemJournalBatch.GET('RECLASS', _pNomFeuille) THEN
            ERROR('Feuille reclassement inexistante %1', _pNomFeuille);

        // On Charge les tableaux
        j := 1;
        FOR i := 1 TO STRLEN(_ListeLigne) DO BEGIN
            IF _ListeLigne[i] = '|' THEN
                j += 1
            ELSE
                LTabLigne[j] += FORMAT(_ListeLigne[i]);
        END;
        LNbLigne := j;
        j := 1;
        FOR i := 1 TO STRLEN(_ListeBac) DO BEGIN
            IF _ListeBac[i] = '|' THEN
                j += 1
            ELSE
                LTabBac[j] += FORMAT(_ListeBac[i]);
        END;
        LNbBac := j;

        IF LNbLigne <> LNbBac THEN
            ERROR('Les listes ne sont pas de même longueur (%1 <> %2) !', LNbLigne, LNbBac);

        // On met à jour les lignes : RAZ du champ [A valider]
        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        lItemJournalLine.MODIFYALL("A Valider", FALSE);

        // Mise a jour en fonction des tableaux remplis
        lNbModification := 0;
        CLEAR(lItemJournalLine);
        FOR i := 1 TO LNbLigne DO BEGIN
            EVALUATE(NoLigne, LTabLigne[i]);
            lItemJournalLine.GET(lItemJournalBatch."Journal Template Name", lItemJournalBatch.Name, NoLigne);

            // Recherche de l'article
            CLEAR(LItem);
            LItem.RESET();
            IF LItem.GET(lItemJournalLine."Item No.") THEN BEGIN
                CASE UPPERCASE(LTabBac[i]) OF
                    'SMALLBOX':
                        BEGIN
                            IF LItem."Box Type" = LItem."Box Type"::BigBox THEN BEGIN
                                LItem.VALIDATE("Box Type", LItem."Box Type"::SmallBox);
                                LItem.MODIFY(TRUE);
                                LItem.SETRECFILTER();
                                CLEAR(Lcu_MiniLoad);
                                Lcu_MiniLoad.Article_MsgPRO(LItem);
                                lNbModification += 1;
                            END;
                        END;
                    'BIGBOX':
                        BEGIN
                            IF LItem."Box Type" = LItem."Box Type"::SmallBox THEN BEGIN
                                LItem.VALIDATE("Box Type", LItem."Box Type"::BigBox);
                                LItem.MODIFY(TRUE);
                                LItem.SETRECFILTER();
                                CLEAR(Lcu_MiniLoad);
                                Lcu_MiniLoad.Article_MsgPRO(LItem);
                                lNbModification += 1;
                            END;
                        END;
                    ELSE
                        ERROR('Type Bac non géré : %1', LTabBac[i]);
                END;
            END;
        END;

        EXIT(lNbModification);
    end;

    LOCAL PROCEDURE "--- Fonctions inventaire ---"();
    BEGIN
    END;

    PROCEDURE CalculerQuantiteInventaire(pLocationCode: Code[10]; pZoneCode: Code[10]; pNomFeuille: Code[10]; pFromBin: Code[20]; pToBin: Code[20]; pPickingInventory: Boolean; pStaticInventory: Boolean; pMagasinier: Code[50]; pDeleteBefore: Boolean; pZeroQty: Boolean): Boolean;
    VAR
        lItemJournalBatch: Record "Item Journal Batch";
        lItemJournalLine: Record "Item Journal Line";
        lCalcQtyOnHand2: Report "Calculate Inventory Par Emplac";
    BEGIN
        // CFR le 18/03/2022 => SFD20210929 > Gestion des inventaires

        MagasinParDefaut(pLocationCode); // Lecture magasin et affectation par d‚faut si besoin

        IF (pFromBin = '') OR (pToBin = '') THEN
            ERROR('Codes emplacements obligatoires');
        IF (NOT pPickingInventory) AND (NOT pStaticInventory) THEN
            ERROR('Type d''emplacement obligatoire');
        IF (pMagasinier = '') THEN
            ERROR('Magasinier obligatoire');

        IF (pZoneCode <> '') THEN
            IF NOT (CheckZoneExists(pZoneCode, pLocationCode)) THEN
                ERROR('Code zone inconnu');

        // Cherche la  feuille
        IF NOT lItemJournalBatch.GET('INV. PHYS.', pNomFeuille) THEN
            ERROR('Feuille inventaire inexistante %1', pNomFeuille);

        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        IF lItemJournalLine.FINDSET() THEN BEGIN
            // vide la feuille si besoin (recalcul)
            IF pDeleteBefore THEN
                lItemJournalLine.DELETEALL();
        END;

        lItemJournalLine.RESET();
        lItemJournalLine."Journal Template Name" := lItemJournalBatch."Journal Template Name";
        lItemJournalLine."Journal Batch Name" := lItemJournalBatch.Name;

        // Initialise le report
        lCalcQtyOnHand2.SetItemJnlLine(lItemJournalLine);
        lCalcQtyOnHand2.InitializeForWS(pLocationCode, pZoneCode, pFromBin, pToBin, pPickingInventory, pStaticInventory, pZeroQty, FALSE, pMagasinier, '');
        lCalcQtyOnHand2.SetHideValidationDialog(TRUE);
        lCalcQtyOnHand2.USEREQUESTPAGE(FALSE);
        lCalcQtyOnHand2.RUNMODAL();
        CLEAR(lCalcQtyOnHand2);

        // Fin de traitement
        EXIT(TRUE);
    END;

    PROCEDURE UpdateFeuilleInventaire(pLocationCode: Code[10]; pZoneCode: Code[10]; pNomFeuille: Code[10]; pFromBin: Code[20]; pToBin: Code[20]; pPickingInventory: Boolean; pStaticInventory: Boolean; pMagasinier: Code[50]; pZeroQty: Boolean): Boolean;
    VAR
        lItemJournalBatch: Record "Item Journal Batch";
        lItemJournalLine: Record "Item Journal Line";
        lCalcQtyOnHand2: Report "Calculate Inventory Par Emplac";
        lItemJournalLineTmp: Record "Item Journal Line" TEMPORARY;
        lFromBin: Code[20];
        lToBin: Code[20];
        lItemNo: Code[20];
    BEGIN
        // CFR le 09/09/2022 => SFD20210929 > UpdateFeuilleInventaire()

        MagasinParDefaut(pLocationCode); // Lecture magasin et affectation par d‚faut si besoin

        IF (pFromBin = '') OR (pToBin = '') THEN
            ERROR('Codes emplacements obligatoires');
        IF (NOT pPickingInventory) AND (NOT pStaticInventory) THEN
            ERROR('Type d''emplacement obligatoire');
        IF (pMagasinier = '') THEN
            ERROR('Magasinier obligatoire');

        IF (pZoneCode <> '') THEN
            IF NOT (CheckZoneExists(pZoneCode, pLocationCode)) THEN
                ERROR('Code zone inconnu');

        // Cherche la  feuille
        IF NOT lItemJournalBatch.GET('INV. PHYS.', pNomFeuille) THEN
            ERROR('Feuille inventaire inexistante %1', pNomFeuille);

        lItemJournalLineTmp.DELETEALL();

        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        IF lItemJournalLine.FINDSET() THEN BEGIN
            // 1- Charge la table temporaire
            REPEAT
                lItemJournalLineTmp.INIT();
                lItemJournalLineTmp := lItemJournalLine;
                lItemJournalLineTmp.INSERT();
            UNTIL lItemJournalLine.NEXT() = 0;
            // 2- Vide la feuille
            lItemJournalLine.DELETEALL();
        END;

        // CFR le 06/07/2023 - Support UpdateFeuilleInventaire()
        lItemJournalLineTmp.SETFILTER("Bin Code", '<>%1', '');
        lItemJournalLineTmp.SETFILTER("Item No.", '<>%1', '');
        // FIN CFR le 06/07/2023
        IF lItemJournalLineTmp.FINDSET() THEN BEGIN
            lItemJournalLine.RESET();
            // 3- Recalcule chaque ligne
            REPEAT
                lItemJournalLine.INIT();
                lItemJournalLine."Journal Template Name" := lItemJournalBatch."Journal Template Name";
                lItemJournalLine."Journal Batch Name" := lItemJournalBatch.Name;

                // Initialise le report
                lFromBin := lItemJournalLineTmp."Bin Code";
                lToBin := lItemJournalLineTmp."Bin Code";
                lItemNo := lItemJournalLineTmp."Item No.";
                lCalcQtyOnHand2.SetItemJnlLine(lItemJournalLine);
                lCalcQtyOnHand2.InitializeForWS(pLocationCode, pZoneCode, lFromBin, lToBin, pPickingInventory, pStaticInventory, pZeroQty, FALSE, pMagasinier, lItemNo);
                lCalcQtyOnHand2.SetHideValidationDialog(TRUE);
                lCalcQtyOnHand2.USEREQUESTPAGE(FALSE);
                lCalcQtyOnHand2.RUNMODAL();
                CLEAR(lCalcQtyOnHand2);

            UNTIL lItemJournalLineTmp.NEXT() = 0;
        END;

        // Fin de traitement
        EXIT(TRUE);
    END;

    PROCEDURE EnregistrerLigneInventaire(_pNomFeuille: Code[10]; _ListeLigne: Text; _ListeQte: Text; _ValiderON: Boolean; _pMagasinier: Code[50]): Boolean;
    VAR
        lItemJournalBatch: Record "Item Journal Batch";
        i: Integer;
        j: Integer;
        LNbLigne: Integer;
        LTabLigne: ARRAY[1000] OF Text;
        LTabQte: ARRAY[1000] OF Text;
        LNbQte: Integer;
        lItemJournalLine: Record "Item Journal Line";
        LItemJnlPost: Codeunit "Item Jnl.-Post";
        NoLigne: Integer;
    BEGIN
        // CFR le 18/03/2022 => SFD20210929 > Gestion des inventaires
        // Cherche la  feuille
        IF NOT lItemJournalBatch.GET('INV. PHYS.', _pNomFeuille) THEN
            ERROR('Feuille inventaire inexistante %1', _pNomFeuille);

        // On Charge les tableaux, c'est … dire du texte s‚par‚ par des "|"
        j := 1;
        FOR i := 1 TO STRLEN(_ListeLigne) DO BEGIN
            IF _ListeLigne[i] = '|' THEN
                j += 1
            ELSE
                LTabLigne[j] += FORMAT(_ListeLigne[i]);
        END;
        LNbLigne := j;
        j := 1;
        FOR i := 1 TO STRLEN(_ListeQte) DO BEGIN
            IF _ListeQte[i] = '|' THEN
                j += 1
            ELSE
                LTabQte[j] += FORMAT(_ListeQte[i]);
        END;
        LNbQte := j;

        IF LNbLigne <> LNbQte THEN
            ERROR('Les listes ne sont pas de même longueur (%1 <> %2) !', LNbLigne, LNbQte);

        // On met … jour les lignes : RAZ du champ [A valider]
        CLEAR(lItemJournalLine);
        lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
        lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
        lItemJournalLine.MODIFYALL("A Valider", FALSE);

        // Mise a jour en fonction des tableaux remplis
        CLEAR(lItemJournalLine);
        FOR i := 1 TO LNbLigne DO BEGIN
            EVALUATE(NoLigne, LTabLigne[i]);
            lItemJournalLine.GET(lItemJournalBatch."Journal Template Name", lItemJournalBatch.Name, NoLigne);
            // CFR le 20/10/2022 : conversion des d‚cimaux
            LTabQte[i] := CONVERTSTR(LTabQte[i], '.', ',');
            IF NOT EVALUATE(lItemJournalLine."Qty. (Phys. Inventory)", LTabQte[i]) THEN BEGIN
                LTabQte[i] := CONVERTSTR(LTabQte[i], ',', '.');
                EVALUATE(lItemJournalLine."Qty. (Phys. Inventory)", LTabQte[i])
            END;
            // FIN CFR le 20/10/2022 : conversion des d‚cimaux
            lItemJournalLine.VALIDATE("Qty. (Phys. Inventory)");
            lItemJournalLine.VALIDATE("A Valider", TRUE);
            lItemJournalLine.VALIDATE("Warehouse User ID", _pMagasinier);
            lItemJournalLine.MODIFY();
        END;

        // Validation des lignes [A valider]
        IF (_ValiderON) THEN BEGIN
            CLEAR(lItemJournalLine);
            lItemJournalLine.SETRANGE("Journal Template Name", lItemJournalBatch."Journal Template Name");
            lItemJournalLine.SETRANGE("Journal Batch Name", lItemJournalBatch.Name);
            lItemJournalLine.SETRANGE("A Valider", TRUE);
            lItemJournalLine.FINDSET();
            CduGFunctions.SetHideValidationDialog(TRUE);
            LItemJnlPost.RUN(lItemJournalLine);
        END;

        EXIT(TRUE);
    END;

    LOCAL PROCEDURE "-- Réception--"();
    BEGIN
    END;

    PROCEDURE ValiderLigneReception(NoFusion: Code[20]; NoArticleInterne: Code[20]; QteReçue: Decimal; EmplacementRangement: Code[10]; ImprimerEtiquetteParArticle: Boolean; ImprimerRéférenceSP: Boolean; ImprimanteEtiquette: Code[20]; CodeUtilisateur: Code[20]; YourComment: Text[80]);
    VAR
        "Pré-reception": Record "Saisis Réception Magasin";
        NoLigne: Integer;
        rec_Whse: Record "Warehouse Receipt Header";
    BEGIN
        "Pré-reception".RESET();
        "Pré-reception".SETRANGE("No.", NoFusion);
        IF "Pré-reception".FINDLAST() THEN
            NoLigne := "Pré-reception"."Line No." + 1000
        ELSE
            NoLigne := 10000;

        CLEAR("Pré-reception");
        "Pré-reception".VALIDATE("No.", NoFusion);
        "Pré-reception".VALIDATE("Line No.", NoLigne);
        "Pré-reception".VALIDATE("Bin Code", EmplacementRangement);
        "Pré-reception".VALIDATE("Item No.", NoArticleInterne);
        "Pré-reception".VALIDATE(Quantity, QteReçue);
        "Pré-reception".VALIDATE("Utilisateur Modif", CodeUtilisateur);
        "Pré-reception".VALIDATE("Date Modif", TODAY);
        "Pré-reception".VALIDATE("Heure Modif", TIME);
        "Pré-reception".Commentaire := YourComment;

        IF NoFusion <> '' THEN
            "Pré-reception".INSERT(TRUE);

        IF ImprimanteEtiquette <> '' THEN
            ReceptionEditionEtiquetteNiceLabel("Pré-reception", ImprimerEtiquetteParArticle, ImprimerRéférenceSP, ImprimanteEtiquette, ImprimerEtiquetteParArticle);
    END;

    LOCAL PROCEDURE ReceptionEditionEtiquetteNiceLabel(_pPréReception: Record "Saisis Réception Magasin"; _pImprimerEtiquetteParArticle: Boolean; _pImprimerRéférenceSP: Boolean; _pImprimante: Code[20]; ImprimerEtiquetteParArticle: Boolean);
    VAR
        NomFichier: Text[250];
        Fichier: File;
        Entier: Integer;
        _RefArticle: Code[20];
        _CBarre: Code[20];
        _Designation: Text[50];
        _NoReception: Code[20];
        _NoReceptionNeutre: Code[20];
        _CodeArticle: Code[20];
        _AControler: Text[20];
        _AMarquer: Text[10];
        _Note: Code[10];
        _Qte: Code[10];
        _Emplacement: Code[10];
        LItem: Record Item;
        Imprimante: Record "Generals Parameters";
        txtMessage: Text[80];
        "---": Integer;
        LCuEtiquette: Codeunit "Etiquettes Articles";
        LTabComment: ARRAY[3] OF Text[30];
        LCondit: Integer;
        GestionMultiRef: Codeunit "Gestion Multi-référence";
    BEGIN

        Imprimante.GET('IMP_ETQ_RECEPT', _pImprimante);
        NomFichier := LCuEtiquette.purgeUserId() + '_' + _pPréReception."No." + '-' + FORMAT(_pPréReception."Line No.") + '.TXT';

        LCuEtiquette.OuvrirEtiquetteNiceLabel(NomFichier, Fichier);

        LItem.GET(_pPréReception."Item No.");

        // Remplissage des variables
        _RefArticle := GestionMultiRef.RechercheRefActive(LItem."No.");
        _CBarre := _RefArticle;

        IF EVALUATE(Entier, _RefArticle) THEN
            _RefArticle := FORMAT(Entier, 0, '<Integer><1000Character, >')
        ELSE
            _RefArticle := GestionMultiRef.RechercheRefActive(LItem."No.");

        _Designation := LItem.Description;
        _NoReception := _pPréReception."No.";
        IF (COPYSTR(_pPréReception."No.", 1, 2) = 'RM') THEN
            _NoReceptionNeutre := COPYSTR(_pPréReception."No.", 3)
        ELSE
            _NoReceptionNeutre := _pPréReception."No.";

        _CodeArticle := _pPréReception."Item No.";
        _AControler := '';
        IF ReceptionVerifSiRefAControler(_pPréReception."Item No.", _pPréReception."No.", txtMessage) THEN
            _AControler := 'A CONTROLER';

        _AMarquer := '';
        IF LItem."Marquage réception" THEN
            _AMarquer := 'A***M';

        IF ReceptionRécupèreCommentaire(_pPréReception."Item No.", _pPréReception."No.") <> '' THEN
            _Note := 'VOIR NOTE';

        _Emplacement := _pPréReception."Bin Code";


        LTabComment[1] := _Note;
        LTabComment[2] := _AControler;
        LTabComment[3] := _AMarquer;


        LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, _pPréReception.Quantity, '', _NoReception, _Emplacement, LTabComment, '', 1
              , '45X45RECEP', _pImprimante);


        CLEAR(LTabComment);
        IF ImprimerEtiquetteParArticle THEN BEGIN
            LCondit := 1;
            LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, LCondit, '', _NoReceptionNeutre, '', LTabComment, '', _pPréReception.Quantity,
                '45X45PIEC', _pImprimante);
        END;
        LCuEtiquette.FermeEtiquetteNiceLabel(NomFichier, Fichier);
    END;

    LOCAL PROCEDURE ReceptionVerifSiRefAControler(_pItemNo: Code[20]; _pFusionNo: Code[20]; VAR _pTxtMsg: Text[80]): Boolean;
    VAR
        WhseReceiptLine: Record "Warehouse Receipt Line";
        PurchaseLine: Record "Purchase Line";
        ItemVendor: Record "Item Vendor";
    BEGIN
        _pTxtMsg := '';

        // Verif pour la reception principale
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("No.", _pFusionNo);
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Verif pour les autres receptions
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pFusionNo);  // La modif est ici
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;
    END;

    LOCAL PROCEDURE ReceptionRécupèreCommentaire(_pNoFusion: Code[20]; _pNoArticle: Code[20]): Text[60];
    VAR
        rec_Comment: Record "Warehouse Comment Line";
    BEGIN
        // R‚cupŠre le commentaire s'il existe.
        rec_Comment.SETRANGE("Table Name", rec_Comment."Table Name"::"Whse. Receipt");
        rec_Comment.SETRANGE(Type, rec_Comment.Type::"Pre-Receipt");
        rec_Comment.SETRANGE("No.", _pNoFusion);
        rec_Comment.SETRANGE("Item No.", _pNoArticle);
        IF rec_Comment.FINDFIRST() THEN
            EXIT(rec_Comment.Comment)
        ELSE
            EXIT('');
    END;

    local procedure "--- Fonctions locales ---"()
    begin
    end;

    local procedure MagasinParDefaut(var _pCodeMagasin: Code[20])
    begin
        // Si le code en paramètre est vide, retourne le magasin par défaut
        IF _pCodeMagasin <> '' THEN
            EXIT;

        Company.GET();
        Company.TESTFIELD("Location Code");
        _pCodeMagasin := Company."Location Code";
    end;

    local procedure LectureArticle(_pCodeArticle: Code[20]; var _pArticle: Record Item)
    var
        lGestionMultiReference: Codeunit "Gestion Multi-référence";
    begin
        // Cherche l'article avec la multiRéférence et charge un record par référence
        _pCodeArticle := lGestionMultiReference.RechercheMultiReference(_pCodeArticle);

        IF _pCodeArticle = 'RIENTROUVE' THEN ERROR('Article %1 Introuvable', _pCodeArticle);

        _pArticle.GET(_pCodeArticle);
    end;

    local procedure LettreSuivante(t: Code[1]): Code[1]
    begin
        IF INCSTR(t) = '' THEN BEGIN
            t[STRLEN(t)] := t[STRLEN(t)] + 1;
        END ELSE
            t := INCSTR(t);
        EXIT(t);
    end;

    local procedure CheckZoneExists(pCodeZone: Code[10]; pCodeLocation: Code[10]): Boolean
    var
        lRecordZone: Record Zone;
    begin
        // Verification de l'existance de la zone

        lRecordZone.RESET();

        lRecordZone.SETRANGE("Location Code", pCodeLocation);
        lRecordZone.SETRANGE(Code, pCodeZone);


        IF lRecordZone.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;
}

