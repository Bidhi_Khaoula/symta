codeunit 50003 "Codeunits Functions 2"
{
    //>> Mig CU 5705
    Permissions = tabledata 111 = m;
    PROCEDURE UpdateSalesShipLine(TransRcptLine: Record "Transfer Receipt Line");
    VAR
        SalesLine: Record "Sales Line";
        SalesShipmentLine: Record "Sales Shipment Line";
        TransRcptHeader: Record "Transfer Receipt Header";
        SalesShipmentHeader: Record "Sales Shipment Header";
        UndoQty: Decimal;
    BEGIN
        //PRET mise … jour du BL
        UndoQty := TransRcptLine.Quantity;
        TransRcptHeader.GET(TransRcptLine."Document No.");
        SalesLine.GET(SalesLine."Document Type"::Order, TransRcptHeader."Loans No.", TransRcptLine."Line No.");
        SalesShipmentHeader.RESET();
        SalesShipmentHeader.SETCURRENTKEY("Order No.");
        SalesShipmentHeader.SETRANGE("Order No.", SalesLine."Document No.");
        SalesShipmentHeader.FINDFIRST();
        REPEAT
            SalesShipmentLine.RESET();
            SalesShipmentLine.SETRANGE("Document No.", SalesShipmentHeader."No.");
            SalesShipmentLine.SETRANGE("Order Line No.", SalesLine."Line No.");
            SalesShipmentLine.SETFILTER("Qty. Shipped Not Invoiced", '<>0');
            IF SalesShipmentLine.FINDSET() THEN
                REPEAT
                    IF SalesShipmentLine."Qty. Shipped Not Invoiced" > UndoQty THEN BEGIN
                        SalesShipmentLine."Qty. Shipped Not Invoiced" := SalesShipmentLine."Qty. Shipped Not Invoiced" - UndoQty;
                        SalesShipmentLine.MODIFY();
                        UpdateAttachedShipLine(SalesShipmentLine."Document No.", UndoQty, SalesShipmentLine."Line No.", FALSE);
                        UndoQty := 0;
                    END
                    ELSE BEGIN
                        UndoQty -= SalesShipmentLine."Qty. Shipped Not Invoiced";
                        SalesShipmentLine."Qty. Shipped Not Invoiced" := 0;
                        SalesShipmentLine.MODIFY();
                        UpdateAttachedShipLine(SalesShipmentLine."Document No.", UndoQty, SalesShipmentLine."Line No.", TRUE);
                    END;



                UNTIL (SalesShipmentLine.NEXT() = 0) OR (UndoQty = 0);
        UNTIL (SalesShipmentHeader.NEXT() = 0) OR (UndoQty = 0);
    END;

    PROCEDURE UpdateAttachedShipLine(HeaderNo: Code[20]; Qty: Decimal; Line: Decimal; RAZ: Boolean);
    VAR
        SalesAttachedLine: Record "Sales Shipment Line";
    BEGIN
        //On r‚cupŠre les lignes attach‚es :
        SalesAttachedLine.RESET();
        SalesAttachedLine.SETRANGE("Document No.", HeaderNo);
        SalesAttachedLine.SETRANGE("Attached to Line No.", Line);
        IF SalesAttachedLine.FINDSET() THEN
            REPEAT
                IF (SalesAttachedLine.Type.AsInteger() > 0) THEN
                    //mettre bonne quantit‚ re‡u PRET
                    IF NOT RAZ THEN
                        SalesAttachedLine."Qty. Shipped Not Invoiced" := SalesAttachedLine."Qty. Shipped Not Invoiced" - Qty
                    ELSE
                        SalesAttachedLine."Qty. Shipped Not Invoiced" := 0;
                SalesAttachedLine.MODIFY();

            UNTIL SalesAttachedLine.NEXT() = 0;
    END;

    //<< Mig CU 5705
}