codeunit 50096 "Fix Phy Ledger Entry"
{
    Permissions = TableData 281 = rm;
    trigger OnRun()
    var
       BinContent: Record "Bin Content";
        PhysEntry: Record "Phys. Inventory Ledger Entry";
        Item: Record Item;
        lGestionMultireference: Codeunit "Gestion Multi-référence";
        row: Integer;
        nb: Integer;
         lDialog: Dialog;
                
    begin
        PhysEntry.SETRANGE("Manufacturer Code", '');
        nb := PhysEntry.COUNT;
        PhysEntry.FINDSET();
        lDialog.OPEN('@1@@@@@@@@@@@@@@@@@@@@');
        REPEAT
            row += 1;

            IF Item.GET(PhysEntry."Item No.") THEN BEGIN
                IF PhysEntry.Description = '' THEN PhysEntry.Description := Item.Description;
                PhysEntry."Product Type" := Item."Product Type";
                PhysEntry."Manufacturer Code" := Item."Manufacturer Code";
                PhysEntry."Item Category Code" := Item."Item Category Code";
                IF PhysEntry."No. 2" = '' THEN PhysEntry."No. 2" := lGestionMultireference.RechercheRefActive(PhysEntry."Item No.");
                IF PhysEntry."Bin Code" = '' THEN BEGIN
                    BinContent.SETRANGE("Item No.", PhysEntry."Item No.");
                    BinContent.SETRANGE(Default, TRUE);
                    IF BinContent.FINDFIRST() THEN
                        PhysEntry."Bin Code" := BinContent."Bin Code";
                END;
                PhysEntry.MODIFY();
            END;

            IF (row MOD 20) = 0 THEN
                lDialog.UPDATE(1, (row / nb * 10000) DIV 1);
            IF (row MOD 1000) = 0 THEN COMMIT();
        UNTIL PhysEntry.NEXT() = 0;
        lDialog.CLOSE();
    END;

  
}