codeunit 50051 "Eskape Communication : Email"
{
    // //MC Le 17-01-2011 => DAMAC -> Gestion des AR, possibilité d'envoyer le fichier des conditions générales de vente//


    trigger OnRun()
    begin
        SendMail('', 'Mon email', 'adagoisy@eskape.fr', '', '');
    end;

    var
       // cuMail: Codeunit Mail;

    procedure GetStandardEmailClient() strReturn_EmailClient: Text[50]
    var
    //TODO objWSH: Automation ;
    begin
        //----------------------------------------------------------------------------------------------------------------------------------
        //Initialisation du retour de la fonction
        strReturn_EmailClient := '';

        //----------------------------------------------------------------------------------------------------------------------------------
        //Instanciation de l'objet permettant de récupérer les infos dans la base de registre
        //TODO IF ISCLEAR(objWSH) THEN CREATE(objWSH,FALSE, TRUE);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Lecture du nom du client de messagerie par défaut dans la base de registre (section current User)
        //strReturn_EmailClient := objWSH.RegRead('HKEY_CURRENT_USER\Software\Clients\Mail\');
        strReturn_EmailClient := 'MICROSOFT OUTLOOK';
        //----------------------------------------------------------------------------------------------------------------------------------
        //Vérifie qu'un nom de client est bien défini sinon on va lire la base de registre (section local machine)
        IF strReturn_EmailClient = '' THEN BEGIN
            //TODO strReturn_EmailClient := objWSH.RegRead('HKEY_LOCAL_MACHINE\SOFTWARE\Clients\Mail\');
        END;

        //----------------------------------------------------------------------------------------------------------------------------------
        //Met le nom du client en majuscule
        strReturn_EmailClient := UPPERCASE(strReturn_EmailClient);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Détruit l'instance de l'objet
        //TODOD CLEAR(objWSH);
    end;

    procedure SendMail(pFileName: Text[255]; pSubject: Text[500]; pRecipient: Text[255]; pBCCRecipient: Text[255]; pBody: Text[1024])
    begin
        //----------------------------------------------------------------------------------------------------------------------------------
        //Récupère le nom du client de messagerie par défaut
        CASE GetStandardEmailClient() OF
            'MICROSOFT OUTLOOK':
               
                    OutlookMail(pFileName, pSubject, pRecipient, pBCCRecipient, pBody);
                

            'OUTLOOK EXPRESS', 'MOZILLA THUNDERBIRD', 'MOZILLA':
                
                    MapiMail(pFileName, pSubject, pRecipient);
                
        END;
    end;

    procedure MapiMail(pFileName: Text[255]; pSubject: Text[500]; pRecipient: Text[255])
    var
    //TODO objMAPI_Session: OCX "{20C62CA0-15DA-101B-B9A8-444553540000}:''";
    //TODO objMAPI_Message: OCX "{20C62CAB-15DA-101B-B9A8-444553540000}:''";
    begin
        //----------------------------------------------------------------------------------------------------------------------------------
        //Création d'une instance de l'objet MAPI
        /* //TODO
        CLEAR(objMAPI_Session);
        CLEAR(objMAPI_Message);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Paramétrage de la session MAPI
        objMAPI_Session.DownLoadMail := FALSE;
        objMAPI_Session.LogonUI      := TRUE;

        //----------------------------------------------------------------------------------------------------------------------------------
        //Ouverture d'une session
        objMAPI_Session.SignOn;
        objMAPI_Session.NewSession := TRUE;
        
        //----------------------------------------------------------------------------------------------------------------------------------
        //Création de l'email
        //Initialisation
        objMAPI_Message.SessionID := objMAPI_Session.SessionID;
        objMAPI_Message.Compose;

        //Destinataire
        IF pRecipient <> '' THEN objMAPI_Message.RecipAddress := pRecipient;

        //Sujet
        IF pSubject <> '' THEN objMAPI_Message.MsgSubject     := pSubject;

        //Pièce jointe
        IF (pFileName <> '') AND EXISTS(pFileName) THEN 
        BEGIN
            objMAPI_Message.AttachmentIndex    := 0;
            objMAPI_Message.AttachmentPathName := pFileName;
            objMAPI_Message.AttachmentName     := pFileName;
        END;

        //----------------------------------------------------------------------------------------------------------------------------------
        //Affichage du nouvel email
        objMAPI_Message.Send(TRUE);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Fermeture de la session
        objMAPI_Session.SignOff;

        //----------------------------------------------------------------------------------------------------------------------------------
        //Destruction des objets
        CLEAR(objMAPI_Message);
        CLEAR(objMAPI_Session);
        */
    end;

    procedure OutlookMail(pFileName: Text[255]; pSubject: Text[500]; pRecipient: Text[255]; pBCCRecipient: Text[255]; pBody: Text[1024])
    var
    //TODO objOutlook_Application: Automation "{00062FFF-0000-0000-C000-000000000046} 9.3:{0006F03A-0000-0000-C000-000000000046}:Unknown Automation Server.Application";
    //TODO objOutlook_Item: Automation "{00062FFF-0000-0000-C000-000000000046} 9.3:{00061033-0000-0000-C000-000000000046}:Unknown Automation Server.MailItem";
    begin
        //----------------------------------------------------------------------------------------------------------------------------------
        //Création d'une instance de l'objet Outlook
        /* TODO IF ISCLEAR(objOutlook_Application) THEN CREATE(objOutlook_Application, FALSE, TRUE);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Création d'un nouvel email
        objOutlook_Item := objOutlook_Application.CreateItem(0);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Remplissage des informations de l'email
        //Destinataire Principal
        IF pRecipient <> '' THEN objOutlook_Item."To"      := pRecipient;

        //Sujet
        IF pSubject   <> '' THEN objOutlook_Item.Subject   := pSubject;

        IF pBody <> '' THEN objOutlook_Item.HTMLBody := pBody;

        //Pièce jointe
        IF (pFileName  <> '') AND EXISTS(pFileName) THEN objOutlook_Item.Attachments.Add(pFileName);

        //----------------------------------------------------------------------------------------------------------------------------------
        //Affichage du nouvel email
        objOutlook_Item.Display;

        //----------------------------------------------------------------------------------------------------------------------------------
        //Destruction des objets
        CLEAR(objOutlook_Item);
        CLEAR(objOutlook_Application);*/
    end;
}

