codeunit 50010 "Calcul Taux Service"
{

    trigger OnRun()
    begin
        MESSAGE('%1', CalculTauxService(20091103D, 20091103D, '', ''));
    end;

    var
        Text001_Err: Label 'Les dates de début et de fin doivent être remplies !';

    procedure CalculTauxService(DateDeb: Date; DateFin: Date; CodeClient: Code[20]; CodeArticle: Code[20]): Decimal
    var
        Client: Record Customer;
        Article: Record Item;
        PostedWhseShipHeader: Record "Posted Whse. Shipment Header";
        PostedWhseShipLine: Record "Posted Whse. Shipment Line";
        NbLigneBP: Integer;
        NbLigneBPDefaut: Integer;
    begin
        // Verification des données
        // -------------------------
        IF (DateDeb = 0D) OR (DateFin = 0D) THEN
            ERROR(Text001_Err);

        IF CodeClient <> '' THEN
            Client.GET(CodeClient);

        IF CodeArticle <> '' THEN
            Article.GET(CodeArticle);

        // Filtre des entêtes
        // ------------------
        PostedWhseShipHeader.RESET();
        PostedWhseShipHeader.SETRANGE("Posting Date", DateDeb, DateFin);
        IF PostedWhseShipHeader.FINDFIRST() THEN
            REPEAT
                // Filtre les lignes
                // -----------------
                PostedWhseShipLine.SETRANGE("No.", PostedWhseShipHeader."No.");
                IF CodeClient <> '' THEN
                    PostedWhseShipLine.SETRANGE("Posted Source No.", Client."No.");

                IF CodeArticle <> '' THEN
                    PostedWhseShipLine.SETRANGE("Item No.", Article."No.");

                IF PostedWhseShipLine.FindSet() THEN
                    REPEAT

                        NbLigneBP += 1;

                        IF (PostedWhseShipLine."Qté Ouverte avant validation" <> 0)
                         AND (PostedWhseShipLine.Quantity <> PostedWhseShipLine."Qté Ouverte avant validation")
                           THEN
                            NbLigneBPDefaut += 1;

                    UNTIL PostedWhseShipLine.NEXT() = 0;

            UNTIL PostedWhseShipHeader.NEXT() = 0;

        // Retourne le résultat
        // --------------------

        IF NbLigneBP = 0 THEN
            EXIT(0);

        EXIT((NbLigneBP - NbLigneBPDefaut) / NbLigneBP * 100);
    end;
}

