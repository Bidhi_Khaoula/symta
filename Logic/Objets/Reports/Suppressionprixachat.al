report 50099 "Suppression prix achat"
{
    Caption = 'Purchase Price Deletion';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Purchase Price"; "Purchase Price")
        {
            RequestFilterFields = "Item No.", "Vendor No.", "Référence Active", "Vendor Item No.";

            trigger OnAfterGetRecord();
            begin

                // Traitement différencié en fonction de l'option choisie
                CASE gEtape OF
                    gEtape::"Effacer le marquage":
                        BEGIN
                            gNombreTarifsTraites := gNombreTarifsTraites + 1;
                            "Purchase Price"."Marked For Deletion" := FALSE;
                            "Purchase Price".MODIFY();
                        END;
                    gEtape::"Marquer les tarifs à supprimer":
                        BEGIN
                            // CAS des tarifs HORS TARCAS* & TARCNH
                            IF (STRPOS("Purchase Price"."Vendor No.", 'TARCAS') = 0) AND (STRPOS("Purchase Price"."Vendor No.", 'TARCNH') = 0)
                              THEN
                                FctMarquer_tarif(FALSE);
                            // CAS des tarifs TARCAS*
                            IF (STRPOS("Purchase Price"."Vendor No.", 'TARCAS') > 0) THEN BEGIN
                                // CFR 04/10/2019 - Optimisation tarifs TARCAS* & TARCNH* avec ref fournisseur différente
                                //IF (STRPOS("Purchase Price"."Vendor No.", '001') > 0) THEN BEGIN
                                FctMarquer_tarif(TRUE);
                                FctMarquer_tarifTAR('TARCAS');
                                //  END;
                            END;
                            // CAS des tarifs TARCNH*
                            IF (STRPOS("Purchase Price"."Vendor No.", 'TARCNH') > 0) THEN BEGIN
                                // CFR 04/10/2019 - Optimisation tarifs TARCAS* & TARCNH* avec ref fournisseur différente
                                //IF (STRPOS("Purchase Price"."Vendor No.", '001') > 0) THEN BEGIN
                                FctMarquer_tarif(TRUE);
                                FctMarquer_tarifTAR('TARCNH');
                                //  END;
                            END;
                        END;
                    gEtape::"Supprimer les tarifs marqués":

                        IF ("Purchase Price"."Marked For Deletion") THEN BEGIN
                            gNombreTarifsTraites := gNombreTarifsTraites + 1;
                            "Purchase Price".DELETE();
                        END;
                END;
            end;

            trigger OnPreDataItem();
            var
                lPurchasePrice: Record "Purchase Price";
            begin
                // Gestion du tri selon clef primaire : tri par vendeur obligatoire pour les TARCAS-*** et TARCNH-***
                "Purchase Price".SETCURRENTKEY("Item No.", "Vendor No.", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity");
                "Purchase Price".SETASCENDING("Vendor No.", TRUE);
                // Pose les filtres en fonction de l'option choisie
                // You can use this trigger to add additional filtering
                CASE gEtape OF
                    gEtape::"Effacer le marquage":
                        BEGIN
                            // Filtre les tarifs marqués
                            "Purchase Price".SETRANGE("Marked For Deletion", TRUE);
                            IF NOT CONFIRM(STRSUBSTNO(ESK0003Qst, "Purchase Price".COUNT))
                              THEN BEGIN
                                MESSAGE(ESK0005Msg);
                                CurrReport.QUIT();
                            END;
                        END;
                    gEtape::"Marquer les tarifs à supprimer":
                        BEGIN
                            // Demande de Symta : on efface tous les marquages existant pour éviter la confusion
                            lPurchasePrice.SETRANGE("Marked For Deletion", TRUE);
                            lPurchasePrice.MODIFYALL("Marked For Deletion", FALSE);
                            // Filtre les tarifs en cours
                            gToday := TODAY;
                            "Purchase Price".SETRANGE("Starting Date", 0D, gToday);
                            "Purchase Price".SETFILTER("Ending Date", '%1|>%2', 0D, gToday);
                        END;
                    gEtape::"Supprimer les tarifs marqués":
                        BEGIN
                            // Filtre les tarifs marqués
                            "Purchase Price".SETRANGE("Marked For Deletion", TRUE);
                            IF NOT CONFIRM(STRSUBSTNO(ESK0002Qst, "Purchase Price".COUNT))
                              THEN BEGIN
                                MESSAGE(ESK0005Msg);
                                CurrReport.QUIT();
                            END;
                        END;
                END;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Parameters)
                {
                    Caption = 'Parameters';
                    field(gEtapeName; gEtape)
                    {
                        Caption = 'Step';
                        ToolTip = 'Choisissez le traitement à réaliser : Marquer les tarifs à supprimer, Effacer le marquage ou Supprimer les tarifs marqués';
                        ApplicationArea = All;
                    }
                    field(gNombreAConserverName; gNombreAConserver)
                    {
                        Caption = 'Closed Purchase Price To keep';
                        ToolTip = 'Préciser le nombre de tarifs cloturés à conserver en plus des tarifs en cours. Ce champ n''est fonctionnel QUE pour l''action de marquage. Il n''est pas repris à la suppression.';
                        ApplicationArea = All;
                    }
                }

            }
        }
    }

    trigger OnInitReport();
    begin
        gNombreTarifsTraites := 0;
        gNombreAConserver := 0;
    end;

    trigger OnPostReport();
    begin
        CASE gEtape OF
            gEtape::"Effacer le marquage":

                MESSAGE(ESK0004Msg, 'effacement du marquage', gNombreTarifsTraites);

            gEtape::"Marquer les tarifs à supprimer":

                MESSAGE(ESK0004Msg, 'marquage', gNombreTarifsTraites);

            gEtape::"Supprimer les tarifs marqués":

                MESSAGE(ESK0004Msg, 'suppression', gNombreTarifsTraites);

        END;
    end;

    var
        gEtape: Option "Marquer les tarifs à supprimer","Effacer le marquage","Supprimer les tarifs marqués";
        ESK0002Qst: Label 'Souhaitez-vous supprimer les %1 tarifs marqués ?', Comment = '%1';
        ESK0003Qst: Label 'Souhaitez-vous effacer le marquage de tous les tarifs marqués (%1) ?', Comment = '%1';
        ESK0004Msg: Label 'Nombre de tarifs traités pour %1 : %2', Comment = '%1;%2';
        gNombreAConserver: Integer;
        gNombreTarifsTraites: Integer;
        ESK0005Msg: Label 'Traitement interrompu.';
        gToday: Date;

    local procedure FctMarquer_tarif(pSupprimeSiRefFournisseurVide: Boolean);
    var
        l_PurchasePrice: Record "Purchase Price";
        l_compteur: Integer;
    begin
        // Champs calculés
        "Purchase Price".CALCFIELDS("Vendor Item No.");

        CLEAR(l_PurchasePrice);

        // Gestion du tri selon clef primaire : date décroissante
        l_PurchasePrice.SETCURRENTKEY("Item No.", "Vendor No.", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity");
        l_PurchasePrice.SETASCENDING("Starting Date", FALSE);

        // Recherche les tarifs terminés
        l_PurchasePrice.SETRANGE("Starting Date", 0D, gToday);
        l_PurchasePrice.SETFILTER("Ending Date", '<=%1&<>%2', gToday, 0D);
        // Selection des tarifs identiques
        l_PurchasePrice.SETRANGE("Item No.", "Purchase Price"."Item No.");
        l_PurchasePrice.SETRANGE("Vendor No.", "Purchase Price"."Vendor No.");
        l_PurchasePrice.SETRANGE("Currency Code", "Purchase Price"."Currency Code");
        l_PurchasePrice.SETRANGE("Variant Code", "Purchase Price"."Variant Code");
        l_PurchasePrice.SETRANGE("Unit of Measure Code", "Purchase Price"."Unit of Measure Code");
        l_PurchasePrice.SETRANGE("Minimum Quantity", "Purchase Price"."Minimum Quantity");
        l_PurchasePrice.SETRANGE("Marked For Deletion", FALSE);
        IF l_PurchasePrice.FINDSET() THEN BEGIN
            l_compteur := 1;
            REPEAT
                IF (l_compteur > gNombreAConserver) THEN BEGIN
                    l_PurchasePrice."Marked For Deletion" := TRUE;
                    l_PurchasePrice.MODIFY();
                    gNombreTarifsTraites := gNombreTarifsTraites + 1;

                    //MESSAGE('1) %1 - %2',l_PurchasePrice."Vendor No.",l_PurchasePrice."Starting Date");
                END;
                l_compteur := l_compteur + 1;
            UNTIL l_PurchasePrice.NEXT() = 0;

            // CFR 04/10/2019
            // Cas des tarifs à supprimer si la référence fournisseur est vide (TARCAS* et TARCNH*)
            IF (pSupprimeSiRefFournisseurVide) AND ("Purchase Price"."Vendor Item No." = '') THEN BEGIN
                l_PurchasePrice.SETRANGE("Vendor Item No.", "Purchase Price"."Vendor Item No.");
                l_PurchasePrice.MODIFYALL("Marked For Deletion", TRUE);
            END;
            // FIN CFR 04/10/2019
        END;
    end;

    local procedure FctMarquer_tarifTAR(pFournisseur: Code[10]);
    var
        l_PurchasePrice: Record "Purchase Price";
    begin

        // Champs calculés
        "Purchase Price".CALCFIELDS("Vendor Item No.");

        CLEAR(l_PurchasePrice);

        // Gestion du tri selon clef primaire : tri par vendeur obligatoire pour les TARCAS-*** et TARCNH-***
        l_PurchasePrice.SETCURRENTKEY("Item No.", "Vendor No.", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity");
        "Purchase Price".SETASCENDING("Vendor No.", TRUE);

        // Selection des tarifs identiques
        l_PurchasePrice.SETRANGE("Item No.", "Purchase Price"."Item No.");
        l_PurchasePrice.SETRANGE("Currency Code", "Purchase Price"."Currency Code");
        l_PurchasePrice.SETRANGE("Variant Code", "Purchase Price"."Variant Code");
        l_PurchasePrice.SETRANGE("Unit of Measure Code", "Purchase Price"."Unit of Measure Code");
        l_PurchasePrice.SETRANGE("Minimum Quantity", "Purchase Price"."Minimum Quantity");
        l_PurchasePrice.SETRANGE("Vendor Item No.", "Purchase Price"."Vendor Item No.");

        // CFR 04/10/2019 - Optimisation tarifs TARCAS* & TARCNH* avec ref fournisseur différente
        l_PurchasePrice.SETRANGE("Marked For Deletion", FALSE);
        l_PurchasePrice.SETFILTER("Vendor No.", '%1&>%2', pFournisseur + '-*', "Purchase Price"."Vendor No.");

        IF l_PurchasePrice.FINDSET() THEN
            REPEAT
                l_PurchasePrice."Marked For Deletion" := TRUE;
                l_PurchasePrice.MODIFY();
                gNombreTarifsTraites := gNombreTarifsTraites + 1;

            //MESSAGE('2) %1 - %2',l_PurchasePrice."Vendor No.",l_PurchasePrice."Starting Date");
            UNTIL l_PurchasePrice.NEXT() = 0;
    END;
}

