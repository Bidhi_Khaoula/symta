report 50079 "Export Factures Avoirs"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Export Factures Avoirs.rdlc';
    Caption = 'Export Factures Avoirs';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = all;
    dataset
    {
        dataitem(Customer; Customer)
        {
            RequestFilterFields = "No.", "Centrale Active", "Date Filter";
            dataitem("Sales Invoice Header"; "Sales Invoice Header")
            {
                DataItemLink = "Sell-to Customer No." = FIELD("No.");
                DataItemTableView = SORTING("No.")
                                    ORDER(Ascending);
                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {
                    DataItemLink = "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE(Type = FILTER(<> ' '));

                    trigger OnAfterGetRecord();
                    var
                        LCli: Record Customer;
                    begin

                        // Écritures des lignes

                        str_ligne := FORMAT("Sales Invoice Header"."No.") + str_sep; // no_facture
                        str_ligne += FORMAT("Sales Invoice Header"."Posting Date") + str_sep; // date_facture
                        str_ligne += FORMAT("Sales Invoice Header"."Payment Terms Code") + str_sep; // cond_reglement
                        str_ligne += FORMAT("Sales Invoice Header"."Due Date") + str_sep; // echeance
                        str_ligne += FORMAT('') + str_sep; // echeance 1
                        str_ligne += FORMAT("Sales Invoice Header"."Due Date 2") + str_sep; // echeance 2
                        str_ligne += FORMAT('') + str_sep; // echeance3
                        str_ligne += FORMAT("Sales Invoice Header"."Bill-to Customer No.") + str_sep; // c_client_fact
                        str_ligne += FORMAT("Sales Invoice Line"."N° client Livré") + str_sep; // c_client
                        str_ligne += FORMAT('') + str_sep; // c_etab
                        LCli.GET("Sales Invoice Line"."N° client Livré");
                        str_ligne += FORMAT(LCli.Name) + str_sep; // nom_client
                        str_ligne += FORMAT("Sales Invoice Header"."Payment Method Code") + str_sep; // mode_reglement
                        str_ligne += FORMAT("Sales Invoice Line"."Line No.") + str_sep; // no_ligne
                        str_ligne += FORMAT("Sales Invoice Line"."Shipment No.") + str_sep; // no_bl
                        IF Type <> Type::Item THEN
                            str_ligne += FORMAT("Sales Invoice Line"."No.") + str_sep // c_article
                        ELSE
                            str_ligne += FORMAT(CuMultiref.RechercheRefActive("Sales Invoice Line"."No.")) + str_sep; // c_article

                        str_ligne += FormatDecimal("Sales Invoice Line".Quantity) + str_sep; // qte
                        str_ligne += FormatDecimal("Sales Invoice Line"."Unit Price") + str_sep; // pu_brut
                        str_ligne += FormatDecimal("Sales Invoice Line"."Discount1 %") + str_sep; // remise1
                        str_ligne += FormatDecimal("Sales Invoice Line"."Discount2 %") + str_sep; // remise2
                        str_ligne += FormatDecimal("Sales Invoice Line"."Net Unit Price") + str_sep; // pu_net
                        str_ligne += FormatDecimal("Sales Invoice Line".Amount) + str_sep; // montant_ht
                        str_ligne += FormatDecimal(dec_tauxtva1) + str_sep; // taux_tva1

                        // Calcul des Flowfields
                        "Sales Invoice Header".CALCFIELDS(Amount, "Amount Including VAT");

                        str_ligne += FormatDecimal("Sales Invoice Header".Amount) + str_sep; // total_brut
                        str_ligne += FORMAT(0) + str_sep; // mont_remise
                        str_ligne += FormatDecimal("Sales Invoice Header".Amount) + str_sep; // total_ht
                        str_ligne += FormatDecimal(dec_montanttva1) + str_sep; // montant_tva1
                        str_ligne += FormatDecimal("Sales Invoice Header"."Amount Including VAT") + str_sep; // net_a_payer
                        str_ligne += FORMAT(dec_PortHt) + str_sep; // port_ht
                        str_ligne += FORMAT('0') + str_sep; // port_ttc
                        str_ligne += FormatDecimal(dec_tauxtva2) + str_sep; // taux_tva2
                        str_ligne += FormatDecimal(dec_montanttva2) + str_sep; // montant_tva2

                        fil_fichiercsv.WRITE(str_ligne);
                    end;

                    trigger OnPreDataItem();
                    begin

                        // Récupération de taux_tva1, montant_tva1, taux_tva2, montant_tva2 à partir des VAT Amount Line
                        dec_tauxtva1 := 0;
                        dec_montanttva1 := 0;
                        dec_tauxtva2 := 0;
                        dec_montanttva2 := 0;

                        CLEAR(rec_SalesInvLine);
                        rec_SalesInvLine.CalcVATAmountLines("Sales Invoice Header", temp_rec_VATAmountLine);
                        // taux_tva1 et montant_tva1
                        IF temp_rec_VATAmountLine.FINDFIRST() THEN BEGIN
                            ;
                            dec_tauxtva1 := temp_rec_VATAmountLine."VAT %";
                            dec_montanttva1 := temp_rec_VATAmountLine."VAT Amount";
                            // taux_tva2 et montant_tva2
                            IF temp_rec_VATAmountLine.NEXT() <> 0 THEN BEGIN
                                dec_tauxtva2 := temp_rec_VATAmountLine."VAT %";
                                dec_montanttva2 := temp_rec_VATAmountLine."VAT Amount";
                            END;
                            IF temp_rec_VATAmountLine.NEXT() <> 0 THEN
                                ERROR(Format(VATErr + "Sales Invoice Header"."No."));
                        END;

                        // ----------------------------
                        // Recherche du montant du port
                        // ----------------------------
                        dec_PortHt := 0;
                        rec_SalesInvLine.RESET();
                        rec_SalesInvLine.SETRANGE("Document No.", "Sales Invoice Header"."No.");
                        rec_SalesInvLine.SETRANGE("Internal Line Type", rec_SalesInvLine."Internal Line Type"::Shipment);
                        IF rec_SalesInvLine.FINDFIRST() THEN
                            REPEAT
                                dec_PortHt += rec_SalesInvLine.Amount;
                            UNTIL rec_SalesInvLine.NEXT() = 0;
                        // ----------------------------
                        // FIN Recherche du montant du port
                        // ----------------------------
                    end;
                }

                trigger OnPreDataItem();
                begin
                    "Sales Invoice Header".SETFILTER("Posting Date", Customer.GETFILTER("Date Filter"));
                end;
            }
            dataitem("Sales Cr.Memo Header"; "Sales Cr.Memo Header")
            {
                DataItemLink = "Sell-to Customer No." = FIELD("No.");
                DataItemTableView = SORTING("No.")
                                    ORDER(Ascending);
                dataitem("Sales Cr.Memo Line"; "Sales Cr.Memo Line")
                {
                    DataItemLink = "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE(Type = FILTER(<> ' '));

                    trigger OnAfterGetRecord();
                    var
                        LCli: Record Customer;
                    begin

                        // Écritures des lignes

                        str_ligne := FORMAT("Sales Cr.Memo Header"."No.") + str_sep; // no_facture
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Posting Date") + str_sep; // date_facture
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Payment Terms Code") + str_sep; // cond_reglement
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Due Date") + str_sep; // echeance
                        str_ligne += FORMAT('') + str_sep; // echeance 1
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Due Date 2") + str_sep; // echeance 2
                        str_ligne += FORMAT('') + str_sep; // echeance3
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Bill-to Customer No.") + str_sep; // c_client_fact
                        str_ligne += FORMAT("Sales Cr.Memo Line"."N° client Livré") + str_sep; // c_client
                        IF NOT LCli.GET("Sales Cr.Memo Line"."N° client Livré") THEN
                            LCli.GET("Sales Cr.Memo Header"."Bill-to Customer No.");

                        str_ligne += FORMAT('') + str_sep; // c_etab
                        str_ligne += FORMAT(LCli.Name) + str_sep; // nom_client
                        str_ligne += FORMAT("Sales Cr.Memo Header"."Payment Method Code") + str_sep; // mode_reglement
                        str_ligne += FORMAT("Sales Cr.Memo Line"."Line No.") + str_sep; // no_ligne
                        str_ligne += FORMAT("Sales Cr.Memo Line"."Return Receipt No.") + str_sep; // no_bl
                        str_ligne += FORMAT("Sales Cr.Memo Line"."No.") + str_sep; // c_article
                        str_ligne += FormatDecimal("Sales Cr.Memo Line".Quantity) + str_sep; // qte
                        str_ligne += FormatDecimal("Sales Cr.Memo Line"."Unit Price") + str_sep; // pu_brut
                        str_ligne += FormatDecimal("Sales Cr.Memo Line"."Discount1 %") + str_sep; // remise1
                        str_ligne += FormatDecimal("Sales Cr.Memo Line"."Discount2 %") + str_sep; // remise2

                        str_ligne += FormatDecimal("Sales Cr.Memo Line"."Net Unit Price") + str_sep; // pu_net
                        str_ligne += FormatDecimal("Sales Cr.Memo Line".Amount) + str_sep; // montant_ht


                        str_ligne += FormatDecimal(dec_tauxtva1) + str_sep; // taux_tva1

                        // Calcul des Flowfields
                        "Sales Cr.Memo Header".CALCFIELDS(Amount, "Amount Including VAT");

                        str_ligne += FormatDecimal("Sales Cr.Memo Header".Amount) + str_sep; // total_brut
                        str_ligne += FORMAT(0) + str_sep; // mont_remise
                        str_ligne += FormatDecimal("Sales Cr.Memo Header".Amount) + str_sep; // total_ht
                        str_ligne += FormatDecimal(dec_montanttva1) + str_sep; // montant_tva1
                        str_ligne += FormatDecimal("Sales Cr.Memo Header"."Amount Including VAT") + str_sep; // net_a_payer
                        str_ligne += FORMAT(dec_PortHt) + str_sep; // port_ht
                        str_ligne += FORMAT('0') + str_sep; // port_ttc
                        str_ligne += FormatDecimal(dec_tauxtva2) + str_sep; // taux_tva2
                        str_ligne += FormatDecimal(dec_montanttva2) + str_sep; // montant_tva2

                        fil_fichiercsv.WRITE(str_ligne);
                    end;

                    trigger OnPreDataItem();
                    begin

                        // Récupération de taux_tva1, montant_tva1, taux_tva2, montant_tva2 à partir des VAT Amount Line
                        dec_tauxtva1 := 0;
                        dec_montanttva1 := 0;
                        dec_tauxtva2 := 0;
                        dec_montanttva2 := 0;


                        rec_SalesCrMemoLine.CalcVATAmountLines("Sales Cr.Memo Header", temp_rec_VATAmountLine);
                        // taux_tva1 et montant_tva1
                        IF temp_rec_VATAmountLine.FINDFIRST() THEN BEGIN
                            dec_tauxtva1 := temp_rec_VATAmountLine."VAT %";
                            dec_montanttva1 := temp_rec_VATAmountLine."VAT Amount";
                            // taux_tva2 et montant_tva2
                            IF temp_rec_VATAmountLine.NEXT() <> 0 THEN BEGIN
                                dec_tauxtva2 := temp_rec_VATAmountLine."VAT %";
                                dec_montanttva2 := temp_rec_VATAmountLine."VAT Amount";
                            END;
                            IF temp_rec_VATAmountLine.NEXT() <> 0 THEN
                                ERROR(Format(VATErr + "Sales Cr.Memo Header"."No."));
                        END;

                        // ----------------------------
                        // Recherche du montant du port
                        // ----------------------------
                        dec_PortHt := 0;
                        rec_SalesCrMemoLine.RESET();
                        rec_SalesCrMemoLine.SETRANGE("Document No.", "Sales Cr.Memo Header"."No.");
                        rec_SalesCrMemoLine.SETRANGE("Internal Line Type", rec_SalesCrMemoLine."Internal Line Type"::Shipment);
                        IF rec_SalesCrMemoLine.FINDFIRST() THEN
                            REPEAT
                                dec_PortHt += rec_SalesCrMemoLine.Amount;
                            UNTIL rec_SalesCrMemoLine.NEXT() = 0;
                        // ----------------------------
                        // FIN Recherche du montant du port
                        // ----------------------------
                    end;
                }

                trigger OnAfterGetRecord();
                begin
                    "Sales Cr.Memo Header".SETFILTER("Posting Date", Customer.GETFILTER("Date Filter"));
                end;
            }

            trigger OnPreDataItem();
            begin

                IF Customer.GETFILTER("Date Filter") = '' THEN
                    ERROR('Filtre date obligatoire');
            end;
        }
    }
    trigger OnPostReport();
    begin

        // Fermeture du fichier
        fil_fichiercsv.CLOSE();
        MESSAGE('Fichier %1 généré !', str_nomfichier);
    end;

    trigger OnPreReport();
    begin

        // Séparateur
        str_sep := ';';

        // Définition du mode d'écriture (append)
        fil_fichiercsv.TEXTMODE(TRUE);
        fil_fichiercsv.WRITEMODE(TRUE);

        // Récupération du répertoire par défaut des exports.
        rec_infosce.GET();

        // Définition du nom de fichier
        str_nomfichier := rec_infosce."Export EDI Folder" + '\' +
                          'Exports_factures_avoirs-' +
                          CONVERTSTR(FORMAT(TODAY), '/', '-') + '-' + CONVERTSTR(FORMAT(TIME), ':', '-') + '.csv';

        // Définition de l'entête titres des champs csv
        str_entete := 'no_facture' + str_sep;
        str_entete += 'date_facture' + str_sep;
        str_entete += 'cond_reglement' + str_sep;
        str_entete += 'echeance' + str_sep;
        str_entete += 'echeance1' + str_sep;
        str_entete += 'echeance2' + str_sep;
        str_entete += 'echeance3' + str_sep;
        str_entete += 'c_client_fact' + str_sep;
        str_entete += 'c_client' + str_sep;
        str_entete += 'c_etab' + str_sep;
        str_entete += 'nom_client' + str_sep;
        str_entete += 'mode_reglement' + str_sep;
        str_entete += 'no_ligne' + str_sep;
        str_entete += 'no_bl' + str_sep;
        str_entete += 'c_article' + str_sep;
        str_entete += 'qte' + str_sep;
        str_entete += 'pu_brut' + str_sep;
        str_entete += 'remise1' + str_sep;
        str_entete += 'remise2' + str_sep;
        str_entete += 'pu_net' + str_sep;
        str_entete += 'montant_ht' + str_sep;
        str_entete += 'taux_tva1' + str_sep;
        str_entete += 'total_brut' + str_sep;
        str_entete += 'mont_remise' + str_sep;
        str_entete += 'total_ht' + str_sep;
        str_entete += 'montant_tva1' + str_sep;
        str_entete += 'net_a_payer' + str_sep;
        str_entete += 'port_ht' + str_sep;
        str_entete += 'port_ttc' + str_sep;
        str_entete += 'taux_tva2' + str_sep;
        str_entete += 'montant_tva2' + str_sep;

        IF FILE.EXISTS(str_nomfichier) THEN BEGIN
            fil_fichiercsv.OPEN(str_nomfichier);
            fil_fichiercsv.SEEK(fil_fichiercsv.LEN);
        END ELSE BEGIN
            // Si le fichier n'existe pas, on le créé avec l'entête des champs
            fil_fichiercsv.CREATE(str_nomfichier);
            fil_fichiercsv.WRITE(str_entete);
        END;
    end;

    var
        rec_infosce: Record "Company Information";
        temp_rec_VATAmountLine: Record "VAT Amount Line" temporary;
        rec_SalesInvLine: Record "Sales Invoice Line";
        rec_SalesCrMemoLine: Record "Sales Cr.Memo Line";
        CuMultiref: Codeunit "Gestion Multi-référence";
        fil_fichiercsv: File;
        str_sep: Text[5];
        str_entete: Text[500];
        str_ligne: Text[500];
        str_nomfichier: Text[300];
        dec_PortHt: Decimal;
        dec_tauxtva1: Decimal;
        dec_montanttva1: Decimal;
        dec_tauxtva2: Decimal;
        dec_montanttva2: Decimal;

        VATErr: Label 'Il existe plus de 2 taux tva différents pour la facture ';

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:2><Sign><Integer><Decimals><Comma,,>'));
    end;
}

