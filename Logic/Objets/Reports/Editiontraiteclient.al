report 50011 "Edition traite client"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Edition traite client.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending);
            RequestFilterFields = "Posting Date", "Payment Method Code";
            column(No; "No.")
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(CustAdress1; CustAdress[1])
            {
            }
            column(CustAdress2; CustAdress[2])
            {
            }
            column(CustAdress3; CustAdress[3])
            {
            }
            column(CustAdress4; CustAdress[4])
            {
            }
            column(CustAdress5; CustAdress[5])
            {
            }
            column(CustAdress6; CustAdress[6])
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(Modereglement; Modereglement.Description)
            {
            }
            column(AmountIncludingVAT; "Amount Including VAT")
            {
            }
            column(netAPayer; netAPayer)
            {
            }
            column(CompanyName; Company.Name)
            {
            }
            column(CompanyAddress; Company.Address)
            {
            }
            column(CompanyPostCode; Company."Post Code")
            {
            }
            column(CompanyCity; Company.City)
            {
            }
            column(ShowAmount; ShowAmount)
            {
            }
            column(BilltoCustomerNo; "Bill-to Customer No.")
            {
            }
            column(BankInfo1; BankInfo[1])
            {
            }
            column(BankInfo2; BankInfo[2])
            {
            }
            column(BankInfo3; BankInfo[3])
            {
            }
            column(BankInfo4; BankInfo[4])
            {
            }
            column(BankInfo5; BankInfo[5])
            {
            }
            column(BankInfo6; BankInfo[6])
            {
            }

            trigger OnAfterGetRecord();
            begin

                IF "Sales Invoice Header"."Amount Including VAT" = 0 THEN CurrReport.SKIP();

                //IF Cust.GET("Sales Invoice Header"."Sell-to Customer No.") THEN
                IF Cust.GET("Sales Invoice Header"."Invoice Customer No.") THEN
                    FormatAdress.Customer(CustAdress, Cust);

                IF "Sales Invoice Header"."Currency Code" = '' THEN BEGIN
                    ParametreCompta.GET();
                    Codedevise := ParametreCompta."LCY Code";
                END
                ELSE
                    Codedevise := "Sales Invoice Header"."Currency Code";



                IF Modereglement.GET("Sales Invoice Header"."Payment Method Code") THEN;





                IF CustAccountbank.GET(Cust."No.", Cust."Preferred Bank Account Code") THEN BEGIN
                    BankInfo[1] := CustAccountbank."Bank Branch No.";
                    BankInfo[2] := CustAccountbank."Agency Code";
                    BankInfo[3] := CustAccountbank."Bank Account No.";
                    BankInfo[4] := FORMAT(CustAccountbank."RIB Key");
                    BankInfo[5] := CustAccountbank.Name;
                    BankInfo[6] := CustAccountbank.City;

                END
                ELSE BEGIN
                    BankInfo[1] := '*****';
                    BankInfo[2] := '*****';
                    BankInfo[3] := '***********';
                    BankInfo[4] := '**';
                    BankInfo[5] := '******************';
                    BankInfo[6] := '******************';



                END;
                Company.GET();
                NumLine := 0;

                ShowAmount := "Sales Invoice Header"."Amount Including VAT";
                netAPayer := STRSUBSTNO('%1 %2', FORMAT(ShowAmount, 0, '<Precision,2:2><Standard Format,0> '), Codedevise);
            end;
        }
    }

    labels
    {
        Against_this_BILL_noted_as_NO_CHARGES_please_pay_the_indicated_sum_below_for_order_ofCaption = 'Against this BILL noted as NO CHARGES Please pay the indicated sum below for order of :';
    }

    var
        Cust: Record Customer;
        CustAccountbank: Record "Customer Bank Account";
        ParametreCompta: Record "General Ledger Setup";
        Modereglement: Record "Payment Method";
        Company: Record "Company Information";
        FormatAdress: Codeunit "Format Address";
        CustAdress: array[8] of Text[80];
        BankInfo: array[6] of Text[50];
        NumLine: Integer;
        ShowAmount: Decimal;
        netAPayer: Text[20];
        Codedevise: Code[10];
}

