report 50153 "Reset PCC MPL absent TAB_TAR"
{
    Caption = 'M.A.J. PCC MPL des articles absents de TAB_TAR';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Vendor; Vendor)
        {
            RequestFilterFields = "No.";
            dataitem("Item Vendor"; "Item Vendor")
            {
                DataItemLink = "Vendor No." = FIELD("No.");
                RequestFilterFields = "Item No.", "Vendor Item No.", "Ref. Active";

                trigger OnAfterGetRecord();
                var
                    lTab_tar_tmp: Record Tab_tar_tmp;
                    lItem: Record Item;
                begin
                    lCompteurTotal += 1;

                    lTab_tar_tmp.RESET();
                    lTab_tar_tmp.SETCURRENTKEY(Societe, Status, c_fournisseur, ref_active); // AD Le 09-03-2016
                    lTab_tar_tmp.SETFILTER(Societe, '%1|%2', 'SP', '');
                    lTab_tar_tmp.SETRANGE(c_fournisseur, Vendor."Groupe Tarif Fournisseur");
                    lTab_tar_tmp.SETRANGE(ref_fourni, "Item Vendor"."Vendor Item No.");
                    IF NOT lTab_tar_tmp.FINDSET() THEN
                        IF lItem.GET("Item Vendor"."Item No.") AND
                            (lItem."Manufacturer Code" IN ['TRA', 'NH']) THEN BEGIN
                            lCompteurAbsent += 1;
                            lItem.VALIDATE("Comodity Code", '');
                            lItem.VALIDATE("Code Concurrence", '');
                            lItem.MODIFY();
                        END
                end;
            }
        }
    }

    trigger OnInitReport();
    begin
        lCompteurTotal := 0;
        lCompteurAbsent := 0;
    end;

    trigger OnPostReport();
    begin
        MESSAGE('Traitement terminé.\  %1 lignes de catalogue fournisseur.\  %2 fiche articles réinitialisées.', lCompteurTotal, lCompteurAbsent);
    end;

    var
        lCompteurTotal: Integer;
        lCompteurAbsent: Integer;
}

