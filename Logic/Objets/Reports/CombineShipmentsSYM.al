report 50007 "Combine Shipments SYM"
{
    ApplicationArea = All;
    UsageCategory = None;
    Caption = 'Combine Shipments';
    ProcessingOnly = true;

    dataset
    {
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING("Entierement facturé", "Payment Method Code", "Payment Terms Code", "Bill-to Customer No.", "Regroupement Centrale", "Multi echéance", "Echéances fractionnées", "Invoice Customer No.", "Sell-to Customer No.", "Regroupement Facturation")
                                WHERE("Entierement facturé" = CONST(false),
                                      "En attente de facturation" = CONST(false));
            RequestFilterFields = "Posting Date", "Payment Method Code";
            RequestFilterHeading = 'Posted Sales Shipment';
            dataitem("Sales Shipment Line"; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = FIELD("No.");
                DataItemTableView = SORTING("Document No.", "Line No.");

                trigger OnAfterGetRecord()
                begin
                    IF Type.AsInteger() = 0 THEN
                        IF (NOT CopyTextLines) OR ("Attached to Line No." <> 0) THEN
                            CurrReport.SKIP();



                    // AD Le 30-11-2009 => DEEE
                    IF ("Sales Shipment Line"."Attached to Line No." <> 0) THEN
                        CurrReport.SKIP();
                    // FIN AD Le 30-11-2009

                    IF "Authorized for Credit Card" THEN
                        CurrReport.SKIP();

                    IF ("Qty. Shipped Not Invoiced" <> 0) OR (Type.AsInteger() = 0) THEN BEGIN
                        IF ("Bill-to Customer No." <> Cust."No.") AND
                           ("Sell-to Customer No." <> '')
                        THEN
                            Cust.GET("Bill-to Customer No.");
                        //  IF NOT (Cust.Blocked IN [Cust.Blocked::All,Cust.Blocked::Invoice]) THEN BEGIN
                        IF ("Sales Shipment Header"."Bill-to Customer No." <> SalesHeader."Bill-to Customer No.") OR
                           ("Sales Shipment Header"."Currency Code" <> SalesHeader."Currency Code") OR
                           ("Sales Shipment Header"."EU 3-Party Trade" <> SalesHeader."EU 3-Party Trade") OR
                           ("Sales Shipment Header"."Dimension Set ID" <> SalesHeader."Dimension Set ID")

                           //FBRUN le 21/04/2011 =>nouvelle rupture
                           OR
                           ("Sales Shipment Header"."Multi echéance" <> SalesHeader."Multi echéance") OR
                           ("Sales Shipment Header"."Echéances fractionnées" <> SalesHeader."Echéances fractionnées")
                           OR
                           ("Sales Shipment Header"."Regroupement Facturation" <> SalesHeader."Regroupement Facturation")
                           // AD
                           OR (
                             ("Sales Shipment Header"."Invoice Customer No." <> XInvtoCustomerNo)

                           )// FIN AD
                           OR
                           (
                            ("Sell-to Customer No." <> XSelltoCustomerNo)
                            AND ("Sales Shipment Header"."Regroupement Facturation"
                                = "Sales Shipment Header"."Regroupement Facturation"::"1 Facture pas donneur d'ordre"
                                )
                           )
                           OR
                           ("Sales Shipment Header"."Regroupement Facturation"
                             = "Sales Shipment Header"."Regroupement Facturation"::Aucun)
                           OR
                           ("Sales Shipment Header"."Payment Method Code" <> SalesHeader."Payment Method Code")
                           OR
                           ("Sales Shipment Header"."Payment Terms Code" <> SalesHeader."Payment Terms Code")
                           OR
                           ("Sales Shipment Header"."Regroupement Centrale" <> SalesHeader."Regroupement Centrale")
                        //FIN

                        THEN BEGIN
                            IF SalesHeader."No." <> '' THEN
                                FinalizeSalesInvHeader();
                            InsertSalesInvHeader();
                            SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                            SalesLine.SETRANGE("Document No.", SalesHeader."No.");
                            SalesLine."Document Type" := SalesHeader."Document Type";
                            SalesLine."Document No." := SalesHeader."No.";
                        END;
                        SalesShptLine := "Sales Shipment Line";
                        HasAmount := HasAmount OR ("Qty. Shipped Not Invoiced" <> 0);
                        SalesShptLine.InsertInvLineFromShptLine(SalesLine);
                        // ESK
                        XSelltoCustomerNo := "Sales Shipment Header"."Sell-to Customer No.";
                        XInvtoCustomerNo := "Sales Shipment Header"."Invoice Customer No.";
                        // FIN ESK
                    END ELSE
                        NoOfSalesInvErrors := NoOfSalesInvErrors + 1;
                    //END;
                end;

                trigger OnPostDataItem()
                var
                    SalesShipmentLine: Record "Sales Shipment Line";
                    SalesLineInvoice: Record "Sales Line";
                    SalesGetShpt: Codeunit "Sales-Get Shipment";
                begin
                    SalesShipmentLine.SETRANGE("Document No.", "Document No.");
                    SalesShipmentLine.SETRANGE(Type, Type::"Charge (Item)");
                    IF SalesShipmentLine.FINDSET() THEN
                        REPEAT
                            SalesLineInvoice.SETRANGE("Document Type", SalesLineInvoice."Document Type"::Invoice);
                            SalesLineInvoice.SETRANGE("Document No.", SalesHeader."No.");
                            SalesLineInvoice.SETRANGE("Shipment Line No.", SalesShipmentLine."Line No.");
                            IF SalesLineInvoice.FINDFIRST() THEN
                                SalesGetShpt.GetItemChargeAssgnt(SalesShipmentLine, SalesLineInvoice."Qty. to Invoice");
                        UNTIL SalesShipmentLine.NEXT() = 0;
                end;
            }

            trigger OnAfterGetRecord()
            var
                DueDate: Date;
                PmtDiscDate: Date;
                PmtDiscPct: Decimal;
            begin

                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");

                Window.UPDATE(1, "Bill-to Customer No.");
                Window.UPDATE(2, "Sales Shipment Header"."Order No.");


                Window.UPDATE(3, "No.");

                /* AD Le 13-11-2015 => Pourquoi ?
                IF IsCompletlyInvoiced THEN
                  CurrReport.SKIP;
                */

                IF OnlyStdPmtTerms THEN BEGIN
                    Cust.GET("Bill-to Customer No.");
                    PmtTerms.GET(Cust."Payment Terms Code");
                    IF PmtTerms.Code = "Payment Terms Code" THEN BEGIN
                        DueDate := CALCDATE(PmtTerms."Due Date Calculation", "Document Date");
                        PmtDiscDate := CALCDATE(PmtTerms."Discount Date Calculation", "Document Date");
                        PmtDiscPct := PmtTerms."Discount %";
                        IF (DueDate <> "Due Date") OR
                           (PmtDiscDate <> "Pmt. Discount Date") OR
                           (PmtDiscPct <> "Payment Discount %")
                        THEN BEGIN
                            NoOfskippedShiment := NoOfskippedShiment + 1;
                            CurrReport.SKIP();
                        END;
                    END ELSE BEGIN
                        NoOfskippedShiment := NoOfskippedShiment + 1;
                        CurrReport.SKIP();
                    END;
                END;

                IF XSelltoCustomerNo = '' THEN XSelltoCustomerNo := "Sales Shipment Header"."Sell-to Customer No.";  // AD Le 13-11-2015 => MIG 2015

            end;

            trigger OnPostDataItem()
            begin
                CurrReport.LANGUAGE := GLOBALLANGUAGE;
                Window.CLOSE();
                // AD Le 13-11-2015 => MIG2015
                //IF "Sales Shipment Header"."No." <> '' THEN BEGIN // Not the first time
                IF ("Sales Shipment Header"."No." <> '') AND (SalesHeader."No." <> '') THEN BEGIN // Not the first time
                                                                                                  // FIN AD Le 13-11-2015
                    FinalizeSalesInvHeader();
                    IF (NoOfSalesInvErrors = 0) AND NOT HideDialog THEN BEGIN
                        IF NoOfskippedShiment > 0 THEN
                            MESSAGE(
                              Text011Msg,
                              NoOfSalesInv, NoOfskippedShiment)
                        ELSE
                            MESSAGE(
                              Text010Msg,
                              NoOfSalesInv);
                    END ELSE
                        IF NOT HideDialog THEN
                            IF PostInv THEN
                                MESSAGE(
                                  Text007Msg,
                                  NoOfSalesInvErrors)
                            ELSE
                                MESSAGE(
                                  NotAllInvoicesCreatedMsg,
                                  NoOfSalesInvErrors)
                END ELSE
                    IF NOT HideDialog THEN
                        MESSAGE(Text008Msg);
            end;

            trigger OnPreDataItem()
            begin

                IF PostingDateReq = 0D THEN
                    ERROR(Text000Err);
                IF DocDateReq = 0D THEN
                    ERROR(Text001Err);


                Window.OPEN(
                  Text002Lbl +
                  Text003Lbl +
                  Text004Lbl +
                  Text005Lbl);

                // MCO Le 04-10-2018 => On exclut les clients comptoirs.
                // Le dateitemcustomer n'existe plus
                //"Sales Shipment Header".SETFILTER("Bill-to Customer No.",'<>%1', '5990*');
                // FIN MCO Le 04-10-2018
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(PostingDateName; PostingDateReq)
                    {
                        Caption = 'Posting Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Posting Date field.';
                    }
                    field(DocDateReqName; DocDateReq)
                    {
                        Caption = 'Document Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Document Date field.';
                    }
                    field(CalcInvDiscName; CalcInvDisc)
                    {
                        Caption = 'Calc. Inv. Discount';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Calc. Inv. Discount field.';

                        trigger OnValidate()
                        begin
                            SalesSetup.GET();
                            SalesSetup.TESTFIELD("Calc. Inv. Discount", FALSE);
                        end;
                    }
                    field(PostInvName; PostInv)
                    {
                        Caption = 'Post Invoices';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Post Invoices field.';
                    }
                    field(OnlyStdPmtTermsName; OnlyStdPmtTerms)
                    {
                        Caption = 'Only Std. Payment Terms';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Only Std. Payment Terms field.';
                    }
                    field(CopyTextLinesName; CopyTextLines)
                    {
                        Caption = 'Copy Text Lines';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Copy Text Lines field.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnOpenPage()
        begin
            IF PostingDateReq = 0D THEN
                PostingDateReq := WORKDATE();
            IF DocDateReq = 0D THEN
                DocDateReq := WORKDATE();

            // MCO Le 04-10-2018 => Régie
            PostingDateReq := WORKDATE();
            DocDateReq := WORKDATE();
            // FIN MCO Le 04-10-2018 => Régie
            SalesSetup.GET();
            CalcInvDisc := SalesSetup."Calc. Inv. Discount";
        end;
    }

    labels
    {
    }

    var
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        SalesShptLine: Record "Sales Shipment Line";
        SalesSetup: Record "Sales & Receivables Setup";
        Cust: Record Customer;
        PmtTerms: Record "Payment Terms";
        Language_G: Codeunit Language;
        SalesCalcDisc: Codeunit "Sales-Calc. Discount";
        CustInvoicDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
        SalesPost: Codeunit "Sales-Post";
        Text000Err: Label 'Enter the posting date.';
        Text001Err: Label 'Enter the document date.';
        Text002Lbl: Label 'Combining shipments...\\';
        Text003Lbl: Label 'Customer No.    #1##########\', Comment = '#1 =Customer No.';
        Text004Lbl: Label 'Order No.       #2##########\', Comment = '#2 =Order No.';
        Text005Lbl: Label 'Shipment No.    #3##########', Comment = '#3 = Shipment No.';
        Text007Msg: Label 'Not all the invoices were posted. A total of %1 invoices were not posted.', Comment = '%1 = No Of Sales Invoice Errors';
        Text008Msg: Label 'There is nothing to combine.';
        Text010Msg: Label 'The shipments are now combined and the number of invoices created is %1.', Comment = '%1 = No Of Sales Invoice';
        Window: Dialog;
        PostingDateReq: Date;
        DocDateReq: Date;
        CalcInvDisc: Boolean;
        PostInv: Boolean;
        OnlyStdPmtTerms: Boolean;
        HasAmount: Boolean;
        HideDialog: Boolean;
        NoOfSalesInvErrors: Integer;
        NoOfSalesInv: Integer;
        Text011Msg: Label 'The shipments are now combined, and the number of invoices created is %1.\%2 Shipments with nonstandard payment terms have not been combined.', Comment = '%1-Number of invoices,%2-Number Of shipments';
        NoOfskippedShiment: Integer;
        CopyTextLines: Boolean;
        NotAllInvoicesCreatedMsg: Label 'Not all the invoices were created. A total of %1 invoices were not created.', Comment = '%1 = No Of Sales Invoice Errors';
        XSelltoCustomerNo: Code[20];
        XInvtoCustomerNo: Code[20];


    local procedure FinalizeSalesInvHeader()
    begin
        WITH SalesHeader DO BEGIN
            IF NOT HasAmount THEN BEGIN
                DELETE(TRUE);
                EXIT;
            END;
            IF CalcInvDisc THEN
                SalesCalcDisc.RUN(SalesLine);
            //FBRUN GESTION DES REMISES SUPPLEMENTAIRE
            CLEAR(CustInvoicDiscSup);
            CustInvoicDiscSup.Calc_Disc_line(SalesHeader, 0);
            CustInvoicDiscSup.Calc_Disc_Header(SalesHeader, 0);
            //FIN FBRUN
            FIND();
            COMMIT();
            CLEAR(SalesCalcDisc);
            CLEAR(SalesPost);
            NoOfSalesInv := NoOfSalesInv + 1;
            IF PostInv THEN BEGIN
                CLEAR(SalesPost);
                IF NOT SalesPost.RUN(SalesHeader) THEN
                    NoOfSalesInvErrors := NoOfSalesInvErrors + 1;
            END;
        END;
    end;

    local procedure InsertSalesInvHeader()
    begin
        CLEAR(SalesHeader);
        WITH SalesHeader DO BEGIN
            INIT();
            "Document Type" := "Document Type"::Invoice;
            "No." := '';
            INSERT(TRUE);
            VALIDATE("Sell-to Customer No.", "Sales Shipment Header"."Bill-to Customer No.");
            IF "Bill-to Customer No." <> "Sell-to Customer No." THEN
                VALIDATE("Bill-to Customer No.", "Sales Shipment Header"."Bill-to Customer No.");
            VALIDATE("Posting Date", PostingDateReq);
            VALIDATE("Document Date", DocDateReq);
            VALIDATE("Currency Code", "Sales Shipment Header"."Currency Code");
            VALIDATE("EU 3-Party Trade", "Sales Shipment Header"."EU 3-Party Trade");
            "Salesperson Code" := "Sales Shipment Header"."Salesperson Code";
            "Shortcut Dimension 1 Code" := "Sales Shipment Header"."Shortcut Dimension 1 Code";
            "Shortcut Dimension 2 Code" := "Sales Shipment Header"."Shortcut Dimension 2 Code";

            // AD Le 07-06-2010 => FARGROUP -> On met la date de facture dans le no doc externe
            VALIDATE("External Document No.", FORMAT("Posting Date"));
            // FIN AD Le 07-06-2010


            //FBRUN le 21/04/2011 =>nouvelle rupture
            "Multi echéance" := "Sales Shipment Header"."Multi echéance";
            "Echéances fractionnées" := "Sales Shipment Header"."Echéances fractionnées";
            "Regroupement Facturation" := "Sales Shipment Header"."Regroupement Facturation";
            // IF "Multi echéance" OR  "Echéances fractionnées" THEN
            VALIDATE("Payment Method Code", "Sales Shipment Header"."Payment Method Code");
            VALIDATE("Payment Terms Code", "Sales Shipment Header"."Payment Terms Code");
            "Regroupement Centrale" := "Sales Shipment Header"."Regroupement Centrale";

            IF "Echéances fractionnées" THEN BEGIN
                VALIDATE("Payment Terms Code 2", "Sales Shipment Header"."Payment Terms Code 2");
                VALIDATE("Taux Premiere Fraction", "Sales Shipment Header"."Taux Premiere Fraction");
            END;
            //FIN


            "Dimension Set ID" := "Sales Shipment Header"."Dimension Set ID";
            MODIFY();
            COMMIT();
            HasAmount := FALSE;
        END;
    end;

    procedure InitializeRequest(NewPostingDate: Date; NewDocDate: Date; NewCalcInvDisc: Boolean; NewPostInv: Boolean; NewOnlyStdPmtTerms: Boolean; NewCopyTextLines: Boolean)
    begin
        PostingDateReq := NewPostingDate;
        DocDateReq := NewDocDate;
        CalcInvDisc := NewCalcInvDisc;
        PostInv := NewPostInv;
        OnlyStdPmtTerms := NewOnlyStdPmtTerms;
        CopyTextLines := NewCopyTextLines;
    end;

    procedure SetHideDialog(NewHideDialog: Boolean)
    begin
        HideDialog := NewHideDialog;
    end;
}

