report 50053 "Remise de Cheque"
{
    caption = 'Remise de Cheque';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Remise de Cheque.rdlc';

    dataset
    {
        dataitem(LOGO; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);

                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
            end;
        }
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("No.", "Due Date");
            RequestFilterFields = "No.";
            column(No; "No.")
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(nb_cheque; nb_cheque)
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column(Designation; "Payment Line".Désignation)
            {
            }
            column(CreditAmount; "Credit Amount")
            {
            }
            column(CustAddr1; CustAddr[1])
            {
            }
            column(CustAddr2; CustAddr[2])
            {
            }
            column(CustAddr3; CustAddr[3])
            {
            }
            column(CustAddr4; CustAddr[4])
            {
            }
            column(CustAddr5; CustAddr[5])
            {
            }

            trigger OnAfterGetRecord();
            begin

                IF "Payment Line"."Credit Amount" <= 0 THEN
                    ERROR('Toutes les lignes doivent etres créditeur');

                nb_cheque += 1;
            end;

            trigger OnPreDataItem();
            begin

                LastFieldNo := FIELDNO("No.");
                "tmp_ lig".COPY("Payment Line");
                IF "tmp_ lig".FIND('-') THEN;
                nb_cheque := "tmp_ lig".COUNT;
                entete.GET("tmp_ lig"."No.");

                bank.GET(entete."Account No.");
                FormatAddrCodeunit.BankAcc(CustAddr, bank);
                CompanyInfo.GET();
                FormatAddrCodeunit.Company(CompanyAddr, CompanyInfo);
                CompanyInfo.CALCFIELDS(Picture);
                nb_cheque := 0;
            end;
        }
    }

    var
        CompanyInfo: Record "Company Information";
        bank: Record "Bank Account";
        entete: Record "Payment Header";
        "tmp_ lig": Record "Payment Line";
        FormatAddrCodeunit: Codeunit "Format Address";
        nb_cheque: Integer;
        LastFieldNo: Integer;
        CustAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];

}

