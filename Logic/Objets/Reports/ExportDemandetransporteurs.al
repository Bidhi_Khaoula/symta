report 50082 "Export Demande transporteurs"
{
    Caption = 'Export Excel des BL';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Demande transporteur"; "Demande transporteur")
        {

            trigger OnAfterGetRecord();
            begin
                MakeExcelDataBody();
            end;
        }
    }
    trigger OnPostReport();
    begin
        CreateExcelbook();
    end;

    trigger OnPreReport();
    begin
        MakeExcelInfo();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        Excel002Lbl: Label 'Data';

    procedure MakeExcelInfo();
    begin
        MakeExcelDataHeader()
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn('N°', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Statut', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Type', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Packing List', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Crée le', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Crée par', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date Départ', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date livraison', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Code livraison', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Transporteur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° récipissé', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Facturé', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Client', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Code postal', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Ville', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pays', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Valeur vente', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Valeur achat', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Cout Transport', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Frais douane', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Frais annexe', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Frais emballage', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Cout Global', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Valeur transport vendu', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn("Demande transporteur"."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur".Statut, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur".Type, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Packing List No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Created At", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Created By", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Date Départ", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Expected Delivery Date", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Shipment Method Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Shipping Vendor No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Package Tracking No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur".Facturé, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);

        TempExcelBuf.AddColumn("Demande transporteur"."Customer No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Vendor No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Ship-to Post Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Ship-to City", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Demande transporteur"."Ship-to Country/Region Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);


        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur".GetValueVente()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur".GetValueAchat()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Cout Transport"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Frais douane"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Frais annexe"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Frais emballage"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Cout Global"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Demande transporteur"."Valeur transport vendu"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, 'Liste des demandes transporteurs', COMPANYNAME, USERID)
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;
}

