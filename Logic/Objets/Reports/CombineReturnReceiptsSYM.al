report 50003 "Combine Return Receipts SYM"//6653
{
    Caption = 'Combine Return Receipts';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = None;
    dataset
    {
        dataitem("Return Receipt Header"; "Return Receipt Header")
        {
            DataItemTableView = SORTING("Entierement facturé", "Payment Method Code", "Payment Terms Code", "Bill-to Customer No.", "Regroupement Centrale", "Multi echéance", "Echéances fractionnées", "Invoice Customer No.", "Sell-to Customer No.", "Regroupement Facturation")
                                WHERE("Entierement facturé" = CONST(false),
                                      "En attente de facturation" = CONST(false));
            RequestFilterFields = "Posting Date", "Sell-to Customer No.", "Bill-to Customer No.", "Payment Method Code";
            RequestFilterHeading = 'Posted Return Receipts';
            dataitem("Return Receipt Line"; "Return Receipt Line")
            {
                DataItemLink = "Document No." = FIELD("No.");
                DataItemTableView = SORTING("Document No.", "Line No.")
                                    WHERE("Return Qty. Rcd. Not Invd." = FILTER(<> 0));

                trigger OnAfterGetRecord()
                var
                    SalesGetReturnReceipts: Codeunit "Sales-Get Return Receipts";
                begin

                    // DZ - Le 15/12/2011 - Regroupement des retours
                    IF ("Return Receipt Line"."Attached to Line No." <> 0) THEN
                        CurrReport.SKIP();
                    // Fin DZ


                    IF "Return Qty. Rcd. Not Invd." <> 0 THEN BEGIN
                        IF "Bill-to Customer No." <> Cust."No." THEN
                            Cust.GET("Bill-to Customer No.");

                        // DZ - Le 15/12/2011 - Regroupement des retours

                        // IF NOT (Cust.Blocked IN [Cust.Blocked::All,Cust.Blocked::Invoice]) THEN BEGIN // AD Le 05-02-2015 => Demande de Marie Laure
                        IF ("Return Receipt Header"."Bill-to Customer No." <> SalesHeader."Bill-to Customer No.") OR
                           ("Return Receipt Header"."Currency Code" <> SalesHeader."Currency Code") OR
                           ("Return Receipt Header"."Multi echéance" <> SalesHeader."Multi echéance") OR
                           ("Return Receipt Header"."Echéances fractionnées" <> SalesHeader."Echéances fractionnées")
                          OR
                          ("Return Receipt Header"."Regroupement Facturation" <> SalesHeader."Regroupement Facturation")
                          OR (
                          ("Return Receipt Header"."Invoice Customer No." <> XInvtoCustomerNo)
                          )
                          OR
                          (
                          ("Sell-to Customer No." <> XSelltoCustomerNo)
                          AND ("Return Receipt Header"."Regroupement Facturation"
                          = "Return Receipt Header"."Regroupement Facturation"::"1 Facture pas donneur d'ordre"
                          )
                          )
                          OR
                          ("Return Receipt Header"."Regroupement Facturation"
                          = "Return Receipt Header"."Regroupement Facturation"::Aucun)
                          OR
                          ("Return Receipt Header"."Payment Method Code" <> SalesHeader."Payment Method Code")
                          OR
                          ("Return Receipt Header"."Payment Terms Code" <> SalesHeader."Payment Terms Code")
                          OR
                          ("Return Receipt Header"."Regroupement Centrale" <> SalesHeader."Regroupement Centrale")
                        //OR
                        //(NOT VerifyDimAnalogy)
                        THEN BEGIN
                            // Fin DZ

                            IF SalesHeader."No." <> '' THEN
                                FinalizeSalesInvHeader();
                            InsertSalesInvHeader();
                            SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                            SalesLine.SETRANGE("Document No.", SalesHeader."No.");
                            SalesLine."Document Type" := SalesHeader."Document Type";
                            SalesLine."Document No." := SalesHeader."No.";
                        END;
                        ReturnRcptLine := "Return Receipt Line";
                        ReturnRcptLine.InsertInvLineFromRetRcptLine(SalesLine);

                        // DZ - Le 15/12/2011 - Regroupement des retours
                        XSelltoCustomerNo := "Return Receipt Header"."Sell-to Customer No.";
                        XInvtoCustomerNo := "Return Receipt Header"."Invoice Customer No.";
                        //Fin DZ

                        IF Type = Type::"Charge (Item)" THEN
                            SalesGetReturnReceipts.GetItemChargeAssgnt("Return Receipt Line", SalesLine."Qty. to Invoice");

                        // AD Le 05-02-2015 => Demande de Marie Laure
                        // END ELSE
                        //  NoOfSalesInvErrors := NoOfSalesInvErrors + 1;
                        // FIN AD Le 05-02-2015

                    END;
                end;
            }

            trigger OnAfterGetRecord()
            begin

                // DZ - Le 15/12/2011 - Regroupement des retours
                CurrReport.LANGUAGE := LanguageG.GetLanguageID("Language Code");

                Window.UPDATE(1, "Bill-to Customer No.");
                Window.UPDATE(2, "Return Receipt Header"."Return Order No.");
                // Fin DZ

                Window.UPDATE(3, "No.");

                // DZ - Le 15/12/2011 - Regroupement des retours
                IF XSelltoCustomerNo = '' THEN XSelltoCustomerNo := "Return Receipt Header"."Sell-to Customer No.";
                // Fin DZ
            end;

            trigger OnPostDataItem()
            begin

                // DZ - Le 15/12/2011 - Regroupement des retours
                CurrReport.LANGUAGE := GLOBALLANGUAGE;

                IF "Return Receipt Header"."No." <> '' THEN BEGIN // Not the first time
                    FinalizeSalesInvHeader();
                    IF (NoOfSalesInvErrors = 0) AND NOT HideDialog THEN
                        IF NoOfskippedShiment > 0 THEN
                            MESSAGE(
                          Text010Msg + Text011Msg,
                          NoOfSalesInv, NoOfskippedShiment)
                        ELSE
                            MESSAGE(
                          Text010Msg,
                          NoOfSalesInv)
                    ELSE
                        IF NOT HideDialog THEN
                            MESSAGE(
                          Text007Msg,
                          NoOfSalesInvErrors)
                END ELSE
                    IF NOT HideDialog THEN
                        MESSAGE(Text008Msg);
                // Fin DZ
            end;

            trigger OnPreDataItem()
            begin

                // DZ - Le 15/12/2011 - Regroupement des retours
                IF PostingDateReq = 0D THEN
                    ERROR(Text000Err);
                IF DocDateReq = 0D THEN
                    ERROR(Text001Err);

                Window.OPEN(
                Text002Lbl +
                Text003Lbl +
                Text004Lbl +
                Text005Lbl);
                // Fin DZ
            end;
        }
    }

    requestpage
    {
        SaveValues = false;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(PostingDateReqName; PostingDateReq)
                    {
                        Caption = 'Posting Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Posting Date field.';
                    }
                    field(DocDateReqName; DocDateReq)
                    {
                        Caption = 'Document Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Document Date field.';
                    }
                    field(CalcInvDiscName; CalcInvDisc)
                    {
                        Caption = 'Calc. Inv. Discount';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Calc. Inv. Discount field.';

                        trigger OnValidate()
                        begin
                            SalesSetup.GET();
                            SalesSetup.TESTFIELD("Calc. Inv. Discount", FALSE);
                        end;
                    }
                    field(PostInvName; PostInv)
                    {
                        Caption = 'Post Credit Memos';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Post Credit Memos field.';
                    }
                }
            }
        }

        trigger OnOpenPage()
        begin
            IF PostingDateReq = 0D THEN
                PostingDateReq := WORKDATE();
            IF DocDateReq = 0D THEN
                DocDateReq := WORKDATE();
            // MCO Le 04-10-2018 => Régie
            PostingDateReq := WORKDATE();
            DocDateReq := WORKDATE();
            // FIN MCO Le 04-10-2018
            SalesSetup.GET();
            CalcInvDisc := SalesSetup."Calc. Inv. Discount";
        end;
    }

    labels
    {
    }

    var
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        ReturnRcptLine: Record "Return Receipt Line";
        SalesSetup: Record "Sales & Receivables Setup";
        Cust: Record Customer;
        LanguageG: Codeunit Language;
        SalesCalcDisc: Codeunit "Sales-Calc. Discount";
        SalesPost: Codeunit "Sales-Post";
        CustInvoicDiscSup: Codeunit "Cust. Invoice Disc. Sup.";
        Window: Dialog;
        PostingDateReq: Date;
        DocDateReq: Date;
        CalcInvDisc: Boolean;
        PostInv: Boolean;
        NoOfSalesInvErrors: Integer;
        NoOfSalesInv: Integer;
        XSelltoCustomerNo: Code[20];
        XInvtoCustomerNo: Code[20];
        HideDialog: Boolean;
        NoOfskippedShiment: Integer;
        Text000Err: Label 'Enter the posting date.';
        Text001Err: Label 'Enter the document date.';
        Text002Lbl: Label 'Combining return receipts...\\';
        Text003Lbl: Label 'Customer No.        #1##########\', Comment = '#1';
        Text004Lbl: Label 'Return Order No.    #2##########\', Comment = '#2';
        Text005Lbl: Label 'Return Receipt No.  #3##########', Comment = '#3';
        Text007Msg: Label 'Not all the credit memos were posted. A total of %1 credit memos were not posted.', Comment = '%1=No Of Sales Invoice Errors';
        Text008Msg: Label 'There is nothing to combine.';
        Text010Msg: Label 'The return receipts are now combined and the number of credit memos created is %1.', Comment = '%1=No of sales invoice';

        Text011Msg: Label 'and the number of credit memo(s) created is %1.', Comment = '%1 =No Of Skipped Shipment';

    local procedure FinalizeSalesInvHeader()
    begin
        WITH SalesHeader DO BEGIN
            IF CalcInvDisc THEN
                SalesCalcDisc.RUN(SalesLine);
            // DZ - Le 15/12/2011 - Regroupement des retours
            CLEAR(CustInvoicDiscSup);
            CustInvoicDiscSup.Calc_Disc_line(SalesHeader, 0);
            CustInvoicDiscSup.Calc_Disc_Header(SalesHeader, 0);
            // Fin DZ
            IF FIND() THEN;

            COMMIT();
            CLEAR(SalesCalcDisc);
            CLEAR(SalesPost);
            NoOfSalesInv := NoOfSalesInv + 1;
            IF PostInv THEN BEGIN
                CLEAR(SalesPost);
                IF NOT SalesPost.RUN(SalesHeader) THEN
                    NoOfSalesInvErrors := NoOfSalesInvErrors + 1;
            END;
        END;
    end;

    local procedure InsertSalesInvHeader()
    begin
        WITH SalesHeader DO BEGIN
            INIT();
            "Document Type" := "Document Type"::"Credit Memo";
            "No." := '';
            INSERT(TRUE);


            // DZ - Le 15/12/2011 - Regroupement des retours
            SalesHeader.SetHideValidationDialog(TRUE);
            // ANCIEN CODE
            //VALIDATE("Sell-to Customer No.",SalesOrderHeader."Bill-to Customer No.");
            //IF "Bill-to Customer No." <> "Sell-to Customer No." THEN
            //  VALIDATE("Bill-to Customer No.",SalesOrderHeader."Bill-to Customer No.");
            VALIDATE("Sell-to Customer No.", "Return Receipt Header"."Bill-to Customer No.");
            IF "Bill-to Customer No." <> "Sell-to Customer No." THEN
                VALIDATE("Bill-to Customer No.", "Return Receipt Header"."Bill-to Customer No.");
            // Fin DZ

            VALIDATE("Posting Date", PostingDateReq);
            VALIDATE("Document Date", DocDateReq);


            // DZ - Le 15/12/2011 - Regroupement des retours
            // ANCIEN CODE
            //VALIDATE("Currency Code",SalesOrderHeader."Currency Code");
            //"Shortcut Dimension 1 Code" := SalesOrderHeader."Shortcut Dimension 1 Code";
            //"Shortcut Dimension 2 Code" := SalesOrderHeader."Shortcut Dimension 2 Code";

            VALIDATE("Currency Code", "Return Receipt Header"."Currency Code");
            "Shortcut Dimension 1 Code" := "Return Receipt Header"."Shortcut Dimension 1 Code";
            "Shortcut Dimension 2 Code" := "Return Receipt Header"."Shortcut Dimension 2 Code";
            VALIDATE("External Document No.", FORMAT("Posting Date"));

            "Multi echéance" := "Return Receipt Header"."Multi echéance";
            "Echéances fractionnées" := "Return Receipt Header"."Echéances fractionnées";
            "Regroupement Facturation" := "Return Receipt Header"."Regroupement Facturation";

            VALIDATE("Payment Method Code", "Return Receipt Header"."Payment Method Code");
            VALIDATE("Payment Terms Code", "Return Receipt Header"."Payment Terms Code");
            "Regroupement Centrale" := "Return Receipt Header"."Regroupement Centrale";

            IF "Echéances fractionnées" THEN BEGIN
                VALIDATE("Payment Terms Code 2", "Return Receipt Header"."Payment Terms Code 2");
                VALIDATE("Taux Premiere Fraction", "Return Receipt Header"."Taux Premiere Fraction");
            END;
            // Fin DZ

            "Dimension Set ID" := "Return Receipt Header"."Dimension Set ID";
            MODIFY();
            COMMIT();
        END;
    end;

    procedure InitializeRequest(NewPostingDate: Date; NewDocumentDate: Date; NewCalcInvDisc: Boolean; NewPostCreditMemo: Boolean)
    begin
        PostingDateReq := NewPostingDate;
        DocDateReq := NewDocumentDate;
        CalcInvDisc := NewCalcInvDisc;
        PostInv := NewPostCreditMemo;
    end;
}

