report 50167 "Import XLS Affectation Critère"
{
    ProcessingOnly = true;
    Caption = 'Import XLS Affectation Critère';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Integer; Integer)
        {

            trigger OnAfterGetRecord();
            var
                "LAffCritères": Record "Famille Critere";
                Rec_Item: Record Item;
                Rec_ListeCritereArticle: Record "Article Critere";
                LVariante: Record "Item Variant";
            begin

                i := i + 1;
                Fenetre.UPDATE(1, ROUND(i / nb * 10000, 1));


                //Initialisation de la table
                CLEAR(LAffCritères);

                //Code Super Famille
                IF NOT GetCell('A') THEN
                    ERROR(ESK001Err, i, LAffCritères.FIELDCAPTION("Super Famille"));
                Createfamille('', 0, TempExcelBuf."Cell Value as Text");

                LAffCritères.VALIDATE("Super Famille", TempExcelBuf."Cell Value as Text");


                //Code Famille
                //IF NOT GetCell('B') THEN
                //ERROR(ESK001, i, LAffCritères.FIELDCAPTION(Famille)); Modification VD 03/02/2017 Car il peu y avoir qu'un super Famille
                IF GetCell('B') THEN BEGIN
                    Createfamille(LAffCritères."Super Famille", 1, TempExcelBuf."Cell Value as Text");
                    LAffCritères.VALIDATE(Famille, TempExcelBuf."Cell Value as Text");
                END;

                //Code sous Famille
                IF GetCell('C') THEN BEGIN
                    Createfamille(LAffCritères.Famille, 2, TempExcelBuf."Cell Value as Text");
                    LAffCritères.VALIDATE("Sous Famille", TempExcelBuf."Cell Value as Text");
                END;

                // Code Critère
                IF NOT GetCell('D') THEN
                    ERROR(ESK001Err, i, LAffCritères.FIELDCAPTION("Code Critere"));
                LAffCritères.VALIDATE("Code Critere", TempExcelBuf."Cell Value as Text");

                // Priorité
                IF NOT GetCell('E') THEN
                    LAffCritères.VALIDATE(Priorité, 99)
                ELSE
                    IF NOT EVALUATE(LAffCritères.Priorité, TempExcelBuf."Cell Value as Text") THEN
                        ERROR(ESK002Err,LAffCritères."Code Critere", i)
                    ELSE
                        LAffCritères.VALIDATE(Priorité, LAffCritères.Priorité * 10);



                IF NOT LAffCritères.INSERT(FALSE) THEN;

                Rec_Item.RESET();
                Rec_Item.SETRANGE("Super Famille Marketing", LAffCritères."Super Famille");
                Rec_Item.SETRANGE(Rec_Item."Famille Marketing", LAffCritères.Famille);
                IF LAffCritères."Sous Famille" <> '' THEN
                    Rec_Item.SETRANGE("Sous Famille Marketing", LAffCritères."Sous Famille");
                IF Rec_Item.FindSet() THEN
                    REPEAT
                        Rec_ListeCritereArticle.INIT();
                        Rec_ListeCritereArticle.VALIDATE("Code Article", Rec_Item."No.");
                        Rec_ListeCritereArticle.VALIDATE("Code Critere", LAffCritères."Code Critere");
                        Rec_ListeCritereArticle.VALIDATE(Priorité, LAffCritères.Priorité);
                        Rec_ListeCritereArticle.VALIDATE("Variant Code", '');
                        Rec_ListeCritereArticle.Insert();

                        // AD Le 01-03-2013 => On ajoute aussi pour les variantes
                        LVariante.RESET();
                        LVariante.SETRANGE("Item No.", Rec_Item."No.");
                        IF LVariante.FindSet() THEN
                            REPEAT
                                Rec_ListeCritereArticle.VALIDATE("Variant Code", LVariante.Code);
                                Rec_ListeCritereArticle.Insert();
                            UNTIL LVariante.NEXT() = 0;
                    // FIN AD Le 01-03-2013

                    UNTIL Rec_Item.NEXT() = 0
            end;

            trigger OnPreDataItem();
            begin


                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 1, TempExcelBuf."Row No." - 1);
                //On se positionne sur la 1ere ligne des données (entete=1)
                No_ligne := 1;
                //MESSAGE(Integer.GETFILTERS);
                nb := TempExcelBuf."Row No.";
                i := 1;
                Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');
            end;
        }
    }

    requestpage
    {

        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', Locked = true;
        begin
            IF CloseAction = ACTION::OK THEN BEGIN
                Evaluate(FileName, FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok));
                IF FileName = '' THEN
                    EXIT(FALSE);

                SheetName := TempExcelBuf.SelectSheetsName(FileName);
                IF SheetName = '' THEN
                    EXIT(FALSE);
            END;
        end;
    }
    trigger OnInitReport();
    begin
        ERROR('plus util');
    end;

    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin
        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var

        TempExcelBuf: Record "Excel Buffer" temporary;
        // Text015: Label 'Choisir le fichier';
        ESK001Err: Label 'Code Critère obligatoire ! Ligne %1,%2', Comment = '%1 ; %2';
        ESK002Err: Label 'Le critère %1 existe déjà ! Ligne %2', Comment = '%1 ; %2';
        // ESK003: Label 'Le type de critère %1 est incorrect ! Ligne %2';
        FileName: Text[250];
        // UploadedFileName: Text[1024];
        SheetName: Text[250];
        No_ligne: Integer;
        nb: Integer;
        i: Integer;
        Fenetre: Dialog;

    // local procedure ReadExcelSheet();
    // begin
    //     TempExcelBuf.OpenBook(FileName, SheetName);
    //     TempExcelBuf.ReadSheet();
    // end;

    procedure GetCell(Row: Text[5]): Boolean;
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.SETRANGE("Row No.", i);
        TempExcelBuf.SETRANGE(xlColID, Row);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    local procedure Createfamille(_pNivSup: Code[10]; _pType: Integer; _pCode: Code[10]);
    var
        LFamille: Record "Famille Marketing";
    begin

        CLEAR(LFamille);
        IF LFamille.GET(_pNivSup, _pType, _pCode) THEN EXIT;

        LFamille.VALIDATE(Type, _pType);
        LFamille.VALIDATE("Code Niveau supérieur", _pNivSup);
        LFamille.VALIDATE(Code, _pCode);
        LFamille.VALIDATE(Description, _pCode);
        LFamille.INSERT(TRUE);
    end;
}

