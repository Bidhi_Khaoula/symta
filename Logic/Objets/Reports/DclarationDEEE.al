report 50040 "Déclaration DEEE"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Déclaration DEEE.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Sales Invoice Line"; "Sales Invoice Line")
        {
            DataItemTableView = SORTING("Posting Date", "No.")
                                ORDER(Ascending)
                                WHERE(Type = FILTER('Charge (Item)' | Resource),
                                      "No." = CONST('DEEE'),
                                      Quantity = FILTER(<> 0));

            trigger OnAfterGetRecord();
            begin

                tmpSalesInvoiceLine.SETCURRENTKEY("Document No.", "Line No.");
                tmpSalesInvoiceLine.SETRANGE("Document No.", "Sales Invoice Line"."Document No.");
                tmpSalesInvoiceLine.SETRANGE("Line No.", "Sales Invoice Line"."Attached to Line No.");
                IF tmpSalesInvoiceLine.FINDFIRST() THEN BEGIN
                    IF (tmpItem.GET(tmpSalesInvoiceLine."No.")) THEN;
                    flagDataItem := 0;
                    IF PrintExcel THEN
                        MakeExcelDataBody();
                END;
            end;

            trigger OnPreDataItem();
            begin

                SETRANGE("Posting Date", dateComptaDeb, dateComptaFin);
            end;
        }
        dataitem("Sales Cr.Memo Line"; "Sales Cr.Memo Line")
        {
            DataItemTableView = SORTING("Posting Date", "No.")
                                ORDER(Ascending)
                                WHERE(Type = FILTER('Charge (Item)' | Resource),
                                      "No." = CONST('DEEE'),
                                      Quantity = FILTER(<> 0));

            trigger OnAfterGetRecord();
            begin

                tmpSalesCrMemoLine.SETCURRENTKEY("Document No.", "Line No.");
                tmpSalesCrMemoLine.SETRANGE("Document No.", "Sales Cr.Memo Line"."Document No.");
                tmpSalesCrMemoLine.SETRANGE("Line No.", "Sales Cr.Memo Line"."Attached to Line No.");
                IF (tmpSalesCrMemoLine.FINDFIRST()) THEN BEGIN
                    IF (tmpItem.GET(tmpSalesInvoiceLine."No.")) THEN;
                    flagDataItem := 1;
                    IF PrintExcel THEN
                        MakeExcelDataBody();
                END;
            end;

            trigger OnPreDataItem();
            begin
                SETRANGE("Posting Date", dateComptaDeb, dateComptaFin);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(dateComptaDebName; dateComptaDeb)
                {
                    Caption = 'Date début comptabilisation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date début comptabilisation field.';
                }
                field(dateComptaFinName; dateComptaFin)
                {
                    Caption = 'Date fin comptabilisation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date fin comptabilisation field.';
                }
            }
        }
    }
    trigger OnInitReport();
    begin
        PrintExcel := TRUE;
    end;

    trigger OnPostReport();
    begin

        IF PrintExcel THEN
            CreateExcelbook();
    end;

    trigger OnPreReport();
    begin

        IF (dateComptaDeb = 0D) THEN ERROR('Date début obligatoire.');
        IF (dateComptaFin = 0D) THEN ERROR('Date fin obligatoire.');

        IF PrintExcel THEN BEGIN
            MakeExcelInfo();
            MakeExcelDataHeader();
        END;
    end;

    var
        tmpSalesInvoiceLine: Record "Sales Invoice Line";
        tmpSalesCrMemoLine: Record "Sales Cr.Memo Line";
        tmpItem: Record Item;
        TempExcelBuf: Record "Excel Buffer" temporary;
        dateComptaDeb: Date;
        dateComptaFin: Date;
        flagDataItem: Integer;
        PrintExcel: Boolean;
        Excel002Lbl: Label 'Data';
        Excel003Lbl: Label 'Déclaration DEEE';
        Excel004Lbl: Label 'Société';
        Excel005Lbl: Label 'N° Etat';
        Excel006Lbl: Label 'Nom Etat';
        Excel007Lbl: Label 'Code utilisateur';
        Excel008Lbl: Label 'Date';
        Excel010Lbl: Label 'Filtres';

    procedure MakeExcelDataHeader();
    begin
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn('Type document', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° document', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date compta', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° Frais annexe', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° Article', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Désignation 1', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Désignation 2', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Super famille', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Famille', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Sous famille', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Quantité', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Montant Vente', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        TempExcelBuf.NewRow();

        IF (flagDataItem = 0) THEN BEGIN
            TempExcelBuf.AddColumn('Facture', FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Invoice Line"."Document No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Invoice Line"."Posting Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Invoice Line"."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesInvoiceLine."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesInvoiceLine.Description, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesInvoiceLine."Description 2", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpItem."Manufacturer Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpItem."Item Category Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            //TODOTempExcelBuf.AddColumn(tmpItem."Product Group Code",          FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Invoice Line".Quantity, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Invoice Line"."Line Amount", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        END;
        IF (flagDataItem = 1) THEN BEGIN
            TempExcelBuf.AddColumn('Avoir', FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Cr.Memo Line"."Document No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Cr.Memo Line"."Posting Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Cr.Memo Line"."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesCrMemoLine."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesCrMemoLine.Description, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpSalesCrMemoLine."Description 2", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpItem."Manufacturer Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(tmpItem."Item Category Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            //TODOTempExcelBuf.AddColumn(tmpItem."Product Group Code",          FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Cr.Memo Line".Quantity * -1, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn("Sales Cr.Memo Line"."Line Amount" * -1, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        END;
    end;

    procedure MakeExcelInfo();
    begin
        // Export Excel
        /////TempExcelBuf.SetUseInfoSheed;
        TempExcelBuf.AddInfoColumn(FORMAT(Excel004Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(COMPANYNAME, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel006Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(FORMAT(Excel003Lbl), FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel005Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(REPORT::"Déclaration DEEE", FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel007Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(USERID, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel008Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(TODAY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel010Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn("Sales Invoice Line".GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.ClearNewRow();
        MakeExcelDataHeader();
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBook('', Excel002Lbl);
        TempExcelBuf.WriteSheet(Excel003Lbl, COMPANYNAME, USERID);
        //TempExcelBuf.GiveUserControl;
        ERROR('');
    end;
}

