report 50016 "Calculate Inventory Par Emplac"
{
    Caption = 'Calculate Inventory';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Bin Content"; "Bin Content")
        {
            DataItemTableView = SORTING("Location Code", "Bin Code", "Item No.", "Variant Code", "Unit of Measure Code")
                                ORDER(Ascending);
            RequestFilterFields = "Location Code", "Bin Code";
            dataitem(Item; Item)
            {
                DataItemLink = "No." = FIELD("Item No.");
                DataItemTableView = SORTING("No.")
                                    WHERE(Type = CONST(Inventory));
                dataitem("Item Ledger Entry"; "Item Ledger Entry")
                {
                    DataItemLink = "Item No." = FIELD("No."),
                                   "Variant Code" = FIELD("Variant Filter"),
                                   "Location Code" = FIELD("Location Filter"),
                                   "Global Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                   "Global Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                   "Posting Date" = FIELD("Date Filter");
                    DataItemTableView = SORTING("Item No.", "Entry Type", "Variant Code", "Drop Shipment", "Location Code", "Posting Date");

                    trigger OnAfterGetRecord();
                    var
                        ItemVariant: Record "Item Variant";
                        ByBin: Boolean;
                        ExecuteLoop: Boolean;
                        InsertTempSKU: Boolean;
                    begin
                        IF ColumnDim <> '' THEN
                            TransferDim("Dimension Set ID");
                        IF NOT "Drop Shipment" THEN BEGIN
                            GetLocation("Location Code");
                            ByBin := Location."Bin Mandatory" AND NOT Location."Directed Put-away and Pick";
                        END;
                        IF NOT SkipCycleSKU("Location Code", "Item No.", "Variant Code") THEN
                            IF ByBin THEN BEGIN
                                IF NOT TempSKU.GET("Location Code", "Item No.", "Variant Code") THEN BEGIN
                                    InsertTempSKU := FALSE;
                                    IF "Variant Code" = '' THEN
                                        InsertTempSKU := TRUE
                                    ELSE
                                        IF ItemVariant.GET("Item No.", "Variant Code") THEN
                                            InsertTempSKU := TRUE;
                                    IF InsertTempSKU THEN BEGIN
                                        TempSKU."Item No." := "Item No.";
                                        TempSKU."Variant Code" := "Variant Code";
                                        TempSKU."Location Code" := "Location Code";
                                        TempSKU.Insert();
                                        ExecuteLoop := TRUE;
                                    END;
                                END;
                                IF ExecuteLoop THEN BEGIN
                                    WhseEntry.SETRANGE("Item No.", "Item No.");
                                    WhseEntry.SETRANGE("Location Code", "Location Code");
                                    WhseEntry.SETRANGE("Variant Code", "Variant Code");
                                    IF WhseEntry.FIND('-') THEN
                                        IF WhseEntry."Entry No." <> OldWhseEntry."Entry No." THEN BEGIN
                                            OldWhseEntry := WhseEntry;
                                            REPEAT
                                                WhseEntry.SETRANGE("Bin Code", WhseEntry."Bin Code");
                                                IF NOT ItemBinLocationIsCalculated(WhseEntry."Bin Code") THEN BEGIN
                                                    WhseEntry.CALCSUMS("Qty. (Base)");
                                                    UpdateBuffer(WhseEntry."Bin Code", WhseEntry."Qty. (Base)");
                                                END;
                                                WhseEntry.FIND('+');
                                                Item.COPYFILTER("Bin Filter", WhseEntry."Bin Code");
                                            UNTIL WhseEntry.NEXT() = 0;
                                        END;
                                END;
                            END ELSE
                                UpdateBuffer('', Quantity);
                    end;

                    trigger OnPreDataItem();
                    begin
                        WhseEntry.SETCURRENTKEY("Item No.", "Bin Code", "Location Code", "Variant Code");
                        Item.COPYFILTER("Bin Filter", WhseEntry."Bin Code");

                        IF ColumnDim = '' THEN
                            TempDimBufIn.SETRANGE("Table ID", DATABASE::Item)
                        ELSE
                            TempDimBufIn.SETRANGE("Table ID", DATABASE::"Item Ledger Entry");
                        TempDimBufIn.SETRANGE("Entry No.");
                        TempDimBufIn.DELETEALL();
                    end;
                }
                dataitem("Warehouse Entry"; "Warehouse Entry")
                {
                    DataItemLink = "Item No." = FIELD("No."),
                                   "Variant Code" = FIELD("Variant Filter"),
                                   "Location Code" = FIELD("Location Filter");

                    trigger OnAfterGetRecord();
                    begin
                        IF NOT "Item Ledger Entry".ISEMPTY THEN
                            CurrReport.SKIP();   // Skip if item has any record in Item Ledger Entry.
                        CLEAR(TempQuantityOnHandBuffer);
                        TempQuantityOnHandBuffer."Item No." := "Item No.";
                        TempQuantityOnHandBuffer."Location Code" := "Location Code";
                        TempQuantityOnHandBuffer."Variant Code" := "Variant Code";

                        GetLocation("Location Code");
                        IF Location."Bin Mandatory" AND NOT Location."Directed Put-away and Pick" THEN
                            TempQuantityOnHandBuffer."Bin Code" := "Bin Code";
                        IF NOT TempQuantityOnHandBuffer.FIND() THEN
                            TempQuantityOnHandBuffer.Insert();   // Insert a zero quantity line.
                    end;
                }

                trigger OnAfterGetRecord();
                begin
                    IF NOT HideValidationDialog THEN
                        Window.UPDATE();
                    TempSKU.DELETEALL();
                end;

                trigger OnPostDataItem();
                begin
                    IF IncludeItemWithNoTransaction THEN
                        UpdateQuantityOnHandBuffer("No.");
                    CalcPhysInvQtyAndInsertItemJnlLine();
                end;

                trigger OnPreDataItem();
                begin

                    // AD Le 30-07-2012
                    IF "Bin Content".GETFILTER("Bin Code") <> '' THEN
                        Item.SETFILTER("Bin Filter", "Bin Content".GETFILTER("Bin Code"));

                    IF "Bin Content".GETFILTER("Location Code") <> '' THEN
                        Item.SETFILTER("Location Filter", "Bin Content".GETFILTER("Location Code"));

                    //ERROR('%1',getfilters);
                end;
            }

            trigger OnPreDataItem();
            var
                ItemJnlTemplate: Record "Item Journal Template";
                ItemJnlBatch: Record "Item Journal Batch";
            begin
                IF PostingDate = 0D THEN
                    ERROR(Text000Lbl);

                ItemJnlTemplate.GET(ItemJnlLine."Journal Template Name");
                ItemJnlBatch.GET(ItemJnlLine."Journal Template Name", ItemJnlLine."Journal Batch Name");
                IF NextDocNo = '' THEN BEGIN
                    IF ItemJnlBatch."No. Series" <> '' THEN BEGIN
                        ItemJnlLine.SETRANGE("Journal Template Name", ItemJnlLine."Journal Template Name");
                        ItemJnlLine.SETRANGE("Journal Batch Name", ItemJnlLine."Journal Batch Name");
                        IF NOT ItemJnlLine.FINDFIRST() THEN
                            NextDocNo := NoSeriesMgt.GetNextNo(ItemJnlBatch."No. Series", PostingDate, FALSE);
                        ItemJnlLine.INIT();
                    END;
                    IF NextDocNo = '' THEN
                        ERROR(Text001Lbl);
                END;

                NextLineNo := 0;

                IF NOT HideValidationDialog THEN
                    Window.OPEN(Text002Lbl, Item."No.");

                IF NOT SkipDim THEN
                    SelectedDim.GetSelectedDim(Format(USERID), 3, REPORT::"Calculate Inventory", '', TempSelectedDim);

                TempQuantityOnHandBuffer.RESET();
                TempQuantityOnHandBuffer.DELETEALL();

                // CFR le 16/03/2022 - SFD20210929 : Inventaires >> variables depuis le WebService
                IF (gLocationCode <> '') THEN
                    "Bin Content".SETRANGE("Location Code", gLocationCode);
                IF (gZoneCode <> '') THEN
                    "Bin Content".SETRANGE("Zone Code", gZoneCode);
                IF (gFromBin <> '') AND (gToBin <> '') THEN
                    "Bin Content".SETRANGE("Bin Code", gFromBin, gToBin);
                // CFR le 09/09/2022 - SFD20210929 : Inventaires > Ajout filtre article
                IF (gItemNo <> '') THEN
                    "Bin Content".SETRANGE("Item No.", gItemNo);

                SetNextDocNo();
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(PostingDateName; PostingDate)
                    {
                        Caption = 'Posting Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Posting Date field.';

                        trigger OnValidate();
                        begin
                            ValidatePostingDate();
                        end;
                    }
                    field(MagasinierName; gMagasinier)
                    {
                        Lookup = true;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the gMagasinier field.';

                        trigger OnLookup(var Text: Text): Boolean;
                        var
                            // lWarehouseEmployees: Page "Warehouse Employees";
                            lWarehouseEmployee: Record "Warehouse Employee";
                        begin
                            /*lWarehouseEmployee.RESET();
                            CLEAR(lWarehouseEmployees);
                            lWarehouseEmployees.SETTABLEVIEW(lWarehouseEmployee);
                            lWarehouseEmployees.LOOKUPMODE  := TRUE;
                            lWarehouseEmployees.EDITABLE    := FALSE;
                            IF lWarehouseEmployees.RUNMODAL = ACTION::LookupOK THEN BEGIN
                              lWarehouseEmployees.GETRECORD(lWarehouseEmployee);
                              Text :=  lWarehouseEmployee."User ID";
                              END
                            ELSE
                              EXIT(FALSE);*/
                            lWarehouseEmployee.RESET();
                            IF PAGE.RUNMODAL(0, lWarehouseEmployee) = ACTION::LookupOK THEN
                                //lWarehouseEmployees.GETRECORD(lWarehouseEmployee);
                                Text := lWarehouseEmployee."User ID"

                            ELSE
                                EXIT(FALSE);

                            RequestOptionsPage.UPDATE(FALSE);
                            EXIT(TRUE);

                        end;

                        trigger OnValidate();
                        begin
                            SetNextDocNo();
                        end;
                    }
                    field(DocumentNo; NextDocNo)
                    {
                        Caption = 'Document No.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Document No. field.';
                    }
                    field(ItemsNotOnInventory; ZeroQty)
                    {
                        Caption = 'Items Not on Inventory.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Items Not on Inventory. field.';

                        trigger OnValidate();
                        begin
                            IF NOT ZeroQty THEN
                                IncludeItemWithNoTransaction := FALSE;
                        end;
                    }
                    field(IncludeItemWithNoTransactionName; IncludeItemWithNoTransaction)
                    {
                        Caption = 'Include Item without Transactions';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Include Item without Transactions field.';

                        trigger OnValidate();
                        begin
                            IF NOT IncludeItemWithNoTransaction THEN
                                EXIT;
                            IF NOT ZeroQty THEN
                                ERROR(ItemNotOnInventoryErr);
                        end;
                    }
                    field(ByDimensions; ColumnDim)
                    {
                        Caption = 'By Dimensions';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the By Dimensions field.';

                        trigger OnAssistEdit();
                        begin
                            DimSelectionBuf.SetDimSelectionMultiple(3, REPORT::"Calculate Inventory", ColumnDim);
                        end;
                    }
                    field(TousLesEmplacementsName; TousLesEmplacements)
                    {
                        Caption = 'Tous les Emplacement';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Tous les Emplacement field.';
                    }
                    field(CtrlPickingInventory; gPickingInventory)
                    {
                        Caption = 'Inventaire picking (défaut)';
                        ToolTip = 'Inventaire sur tous les emplacements où le champ [Défaut] = OUI';
                        ApplicationArea = All;

                        trigger OnValidate();
                        begin
                            // CFR le 16/03/2022 - SFD20210929 : Inventaires >> type emplacement
                            IF (NOT gPickingInventory) THEN
                                gStaticInventory := TRUE;
                            SetCheckBox();
                        end;
                    }
                    field(CtrlStaticInventory; gStaticInventory)
                    {
                        Caption = 'Inventaire sur stock (statique)';
                        ToolTip = 'Inventaire sur tous les emplacements où le champ [Défaut] = NON';
                        ApplicationArea = All;

                        trigger OnValidate();
                        begin
                            // CFR le 16/03/2022 - SFD20210929 : Inventaires >> type emplacement
                            IF (NOT gStaticInventory) THEN
                                gPickingInventory := TRUE;
                            SetCheckBox();
                        end;
                    }
                }
            }
        }

        trigger OnOpenPage();
        begin
            //IF PostingDate = 0D THEN
            PostingDate := WORKDATE();
            ValidatePostingDate();
            ColumnDim := DimSelectionBuf.GetDimSelectionText(3, REPORT::"Calculate Inventory", '');

            // CFR le 16/03/2022 - SFD20210929 : Inventaires >> type emplacement
            gPickingInventory := TRUE;
            gStaticInventory := FALSE;
            Evaluate(gMagasinier, COPYSTR(USERID, STRPOS(USERID, '\') + 1));
            SetNextDocNo();
            // FIN CFR le 16/03/2022 - SFD20210929
        end;
    }


    trigger OnPreReport();
    begin
        IF SkipDim THEN
            ColumnDim := ''
        ELSE
            DimSelectionBuf.CompareDimText(3, REPORT::"Calculate Inventory", '', ColumnDim, Text003Lbl);
        ZeroQtySave := ZeroQty;
    end;

    var
        ItemJnlBatch: Record "Item Journal Batch";
        ItemJnlLine: Record "Item Journal Line";
        WhseEntry: Record "Warehouse Entry";
        TempQuantityOnHandBuffer: Record "Inventory Buffer" temporary;
        SourceCodeSetup: Record "Source Code Setup";
        DimSetEntry: Record "Dimension Set Entry";
        TempDimSetEntry: Record "Dimension Set Entry" temporary;
        SelectedDim: Record "Selected Dimension";
        TempSelectedDim: Record "Selected Dimension" temporary;
        TempDimBufIn: Record "Dimension Buffer" temporary;
        TempDimBufOut: Record "Dimension Buffer" temporary;
        DimSelectionBuf: Record "Dimension Selection Buffer";
        Location: Record Location;
        OldWhseEntry: Record "Warehouse Entry";
        TempSKU: Record "Stockkeeping Unit" temporary;
        NoSeriesMgt: Codeunit NoSeriesManagement;
        DimBufMgt: Codeunit "Dimension Buffer Management";
        Window: Dialog;
        PostingDate: Date;
        CycleSourceType: Option " ",Item,SKU;
        PhysInvtCountCode: Code[10];
        NextDocNo: Code[20];
        NextLineNo: Integer;
        ZeroQty: Boolean;
        ZeroQtySave: Boolean;
        IncludeItemWithNoTransaction: Boolean;
        HideValidationDialog: Boolean;
        AdjustPosQty: Boolean;
        ItemTrackingSplit: Boolean;
        SkipDim: Boolean;
        ColumnDim: Text[250];
        PosQty: Decimal;
        NegQty: Decimal;
        Text004Err: Label 'You must not filter on dimensions if you calculate locations with %1 is %2.', Comment = '%1 ;%2';
        ItemNotOnInventoryErr: Label 'Items Not on Inventory.';
        TousLesEmplacements: Boolean;
        gPickingInventory: Boolean;
        gStaticInventory: Boolean;
        gLocationCode: Code[10];
        gZoneCode: Code[10];
        gFromBin: Code[20];
        gToBin: Code[20];
        gMagasinier: Text[50];
        gItemNo: Code[20];
        Text000Lbl: Label 'Enter the posting date.';
        Text001Lbl: Label 'Enter the document no.';
        Text002Lbl: Label 'Processing items    #1##########', Comment = '#1';
        Text003Lbl: Label 'Retain Dimensions';

    procedure SetItemJnlLine(var NewItemJnlLine: Record "Item Journal Line");
    begin
        ItemJnlLine := NewItemJnlLine;
    end;

    local procedure ValidatePostingDate();
    begin
        ItemJnlBatch.GET(ItemJnlLine."Journal Template Name", ItemJnlLine."Journal Batch Name");
        IF ItemJnlBatch."No. Series" = '' THEN
            NextDocNo := ''
        ELSE BEGIN
            NextDocNo := NoSeriesMgt.GetNextNo(ItemJnlBatch."No. Series", PostingDate, FALSE);
            CLEAR(NoSeriesMgt);
        END;
    end;

    procedure InsertItemJnlLine(ItemNo: Code[20]; VariantCode2: Code[10]; DimEntryNo2: Integer; BinCode2: Code[20]; Quantity2: Decimal; PhysInvQuantity: Decimal);
    var
        ItemLedgEntry: Record "Item Ledger Entry";
        ReservEntry: Record "Reservation Entry";
        WhseEntry: Record "Warehouse Entry";
        WhseEntry2: Record "Warehouse Entry";
        Bin: Record Bin;
        DimValue: Record "Dimension Value";
        BinContent: Record "Bin Content";
        VerificationLigneexistante: Record "Item Journal Line";
        CreateReservEntry: Codeunit "Create Reserv. Entry";
        DimMgt: Codeunit DimensionManagement;
        EntryType: Option "Negative Adjmt.","Positive Adjmt.";
        NoBinExist: Boolean;
        OrderLineNo: Integer;
        lNoBinContentExist: Boolean;
        lIsBinContentDefault: Boolean;
    begin
        WITH ItemJnlLine DO BEGIN
            IF NextLineNo = 0 THEN BEGIN
                LOCKTABLE();
                SETRANGE("Journal Template Name", "Journal Template Name");
                SETRANGE("Journal Batch Name", "Journal Batch Name");
                IF FINDLAST() THEN
                    NextLineNo := "Line No.";



                SourceCodeSetup.GET();
            END;
            NextLineNo := NextLineNo + 10000;

            IF (Quantity2 <> 0) OR ZeroQty THEN BEGIN
                IF (Quantity2 = 0) AND Location."Bin Mandatory" AND NOT Location."Directed Put-away and Pick"
                THEN
                    IF NOT Bin.GET(Location.Code, BinCode2) THEN
                        NoBinExist := TRUE;

                // CFR le 06/04/2022 - SFD20210929 : Inventaires, cas des écritures entrepôt sur emplacement supprimé et stock vide
                IF (NoBinExist) AND (PhysInvQuantity = 0) THEN
                    EXIT;
                // FIN CFR le 06/04/2022 - SFD20210929

                // CFR le 16/03/2022 - SFD20210929 : Inventaires
                lNoBinContentExist := FALSE;
                lIsBinContentDefault := FALSE;
                IF NOT NoBinExist THEN
                    IF BinCode2 <> '' THEN BEGIN
                        BinContent.RESET();
                        BinContent.SETCURRENTKEY("Location Code", "Bin Code", "Item No.", "Variant Code", "Unit of Measure Code");
                        BinContent.SETRANGE("Location Code", Location.Code);
                        BinContent.SETRANGE("Bin Code", BinCode2);
                        BinContent.SETRANGE("Item No.", ItemNo);
                        IF BinContent.FINDFIRST() THEN
                            lIsBinContentDefault := BinContent.Default;
                    END
                    ELSE
                        lNoBinContentExist := TRUE;


                IF (NoBinExist) OR                                   //Bin n'est pas connu           : on propose la ligne
                   (lNoBinContentExist) OR                           //BinContent n'est pas connu    : on propose la ligne
                   (gPickingInventory AND lIsBinContentDefault) OR   //BinContent est Default        : on propose la ligne car on a sélectionné Emplacement Picking
                   (gStaticInventory AND NOT lIsBinContentDefault)   //BinContent n'est pas Default  : on propose la ligne car on a sélectionné Emplacement Statique
                   THEN BEGIN

                    /*
                    // MC Le 18-09-2012 => On ne veut pas de lignes pour les emplacements statiques
                    bln_NoBinFictif := FALSE;
                    IF NOT NoBinExist THEN BEGIN
                      IF BinCode2 <> '' THEN BEGIN
                      BinContent.RESET();
                      BinContent.SETCURRENTKEY("Location Code","Bin Code","Item No.","Variant Code","Unit of Measure Code");
                      BinContent.SETRANGE("Location Code", Location.Code);
                      BinContent.SETRANGE("Bin Code", BinCode2);
                      BinContent.SETRANGE("Item No.", ItemNo);
                      IF BinContent.FINDFIRST () THEN
                        bln_NoBinFictif := BinContent.Default;
                      END
                      ELSE
                        bln_NoBinFictif := TRUE;
                    END;

                   IF TousLesEmplacements THEN bln_NoBinFictif := TRUE; // AD Le 29-01-2020 => REGIE -> Pour Avoir les emplacements qui ne sont pas par defaut

                   IF (bln_NoBinFictif) OR (NoBinExist)  THEN BEGIN
                   // FIN MC Le 18-09-2012

                   */
                    // FIN CFR le 16/03/2022 - SFD20210929

                    INIT();
                    "Line No." := NextLineNo;
                    VALIDATE("Posting Date", PostingDate);
                    IF PhysInvQuantity >= Quantity2 THEN
                        VALIDATE("Entry Type", "Entry Type"::"Positive Adjmt.")
                    ELSE
                        VALIDATE("Entry Type", "Entry Type"::"Negative Adjmt.");
                    VALIDATE("Document No.", NextDocNo);

                    // CFR le 05/04/2022 - SFD20210929 : Inventaires ajout [Code magasinier] et suivi du paramétrage WIIO
                    "Warehouse User ID" := gMagasinier;
                    WIIO_LastPickingInventory := gPickingInventory;
                    WIIO_LastStaticInventory := gStaticInventory;
                    // FIN CFR le 16/03/2022 - SFD20210929

                    VALIDATE("Item No.", ItemNo);
                    VALIDATE("Variant Code", VariantCode2);
                    VALIDATE("Location Code", Location.Code);
                    IF NOT NoBinExist THEN
                        VALIDATE("Bin Code", BinCode2)
                    ELSE
                        VALIDATE("Bin Code", '');
                    VALIDATE("Source Code", SourceCodeSetup."Phys. Inventory Journal");
                    "Qty. (Phys. Inventory)" := PhysInvQuantity;
                    "Phys. Inventory" := TRUE;
                    VALIDATE("Qty. (Calculated)", Quantity2);
                    "Posting No. Series" := ItemJnlBatch."Posting No. Series";
                    "Reason Code" := ItemJnlBatch."Reason Code";

                    "Phys Invt Counting Period Code" := PhysInvtCountCode;
                    "Phys Invt Counting Period Type" := CycleSourceType;

                    IF Location."Bin Mandatory" THEN
                        "Dimension Set ID" := 0;
                    "Shortcut Dimension 1 Code" := '';
                    "Shortcut Dimension 2 Code" := '';

                    ItemLedgEntry.RESET();
                    ItemLedgEntry.SETCURRENTKEY("Item No.");
                    ItemLedgEntry.SETRANGE("Item No.", ItemNo);
                    IF ItemLedgEntry.FINDLAST() THEN
                        "Last Item Ledger Entry No." := ItemLedgEntry."Entry No."
                    ELSE
                        "Last Item Ledger Entry No." := 0;
                    //FBRUN LE 31/07/20 ticket 53234
                    VerificationLigneexistante.SETRANGE("Journal Template Name", "Journal Template Name");
                    VerificationLigneexistante.SETRANGE("Journal Batch Name", "Journal Batch Name");
                    VerificationLigneexistante.SETRANGE("Item No.", ItemNo);
                    VerificationLigneexistante.SETRANGE("Location Code", Location.Code);
                    VerificationLigneexistante.SETRANGE("Bin Code", BinCode2);
                    IF VerificationLigneexistante.ISEMPTY THEN
                        //FIN FBRUN
                        INSERT(TRUE);

                    IF Location.Code <> '' THEN
                        IF Location."Directed Put-away and Pick" THEN BEGIN
                            WhseEntry.SETCURRENTKEY(
                              "Item No.", "Bin Code", "Location Code", "Variant Code", "Unit of Measure Code",
                              "Lot No.", "Serial No.", "Entry Type");
                            WhseEntry.SETRANGE("Item No.", "Item No.");
                            WhseEntry.SETRANGE("Bin Code", Location."Adjustment Bin Code");
                            WhseEntry.SETRANGE("Location Code", "Location Code");
                            WhseEntry.SETRANGE("Variant Code", "Variant Code");
                            IF "Entry Type" = "Entry Type"::"Positive Adjmt." THEN
                                EntryType := EntryType::"Negative Adjmt.";
                            IF "Entry Type" = "Entry Type"::"Negative Adjmt." THEN
                                EntryType := EntryType::"Positive Adjmt.";
                            WhseEntry.SETRANGE("Entry Type", EntryType);
                            IF WhseEntry.FIND('-') THEN
                                REPEAT
                                    WhseEntry.SETRANGE("Lot No.", WhseEntry."Lot No.");
                                    WhseEntry.SETRANGE("Serial No.", WhseEntry."Serial No.");
                                    WhseEntry.CALCSUMS("Qty. (Base)");

                                    WhseEntry2.SETCURRENTKEY(
                                      "Item No.", "Bin Code", "Location Code", "Variant Code", "Unit of Measure Code",
                                      "Lot No.", "Serial No.", "Entry Type");
                                    WhseEntry2.COPYFILTERS(WhseEntry);
                                    CASE EntryType OF
                                        EntryType::"Positive Adjmt.":
                                            WhseEntry2.SETRANGE("Entry Type", WhseEntry2."Entry Type"::"Negative Adjmt.");
                                        EntryType::"Negative Adjmt.":
                                            WhseEntry2.SETRANGE("Entry Type", WhseEntry2."Entry Type"::"Positive Adjmt.");
                                    END;
                                    WhseEntry2.CALCSUMS("Qty. (Base)");
                                    IF ABS(WhseEntry2."Qty. (Base)") > ABS(WhseEntry."Qty. (Base)") THEN
                                        WhseEntry."Qty. (Base)" := 0
                                    ELSE
                                        WhseEntry."Qty. (Base)" := WhseEntry."Qty. (Base)" + WhseEntry2."Qty. (Base)";

                                    IF WhseEntry."Qty. (Base)" <> 0 THEN BEGIN
                                        IF "Order Type" = "Order Type"::Production THEN
                                            OrderLineNo := "Order Line No.";
                                        CreateReservEntry.CreateReservEntryFor(
                                        DATABASE::"Item Journal Line",
                                          "Entry Type".AsInteger(),
                                          "Journal Template Name",
                                          "Journal Batch Name",
                                          OrderLineNo,
                                          "Line No.",
                                          "Qty. per Unit of Measure",
                                          ABS(WhseEntry.Quantity),
                                          ABS(WhseEntry."Qty. (Base)"),
                                          ReservEntry);
                                        IF WhseEntry."Qty. (Base)" < 0 THEN             // only Date on positive adjustments
                                            CreateReservEntry.SetDates(WhseEntry."Warranty Date", WhseEntry."Expiration Date");
                                        CreateReservEntry.CreateEntry(
                                          "Item No.",
                                          "Variant Code",
                                          "Location Code",
                                          Description,
                                          0D,
                                          0D,
                                          0,
                                          ReservEntry."Reservation Status"::Prospect);
                                    END;
                                    WhseEntry.FIND('+');
                                    WhseEntry.SETRANGE("Lot No.");
                                    WhseEntry.SETRANGE("Serial No.");
                                UNTIL WhseEntry.NEXT() = 0;
                        END;

                    IF ColumnDim = '' THEN
                        DimEntryNo2 := CreateDimFromItemDefault();

                    IF DimBufMgt.GetDimensions(DimEntryNo2, TempDimBufOut) THEN BEGIN
                        TempDimSetEntry.RESET();
                        TempDimSetEntry.DELETEALL();
                        IF TempDimBufOut.FIND('-') THEN BEGIN
                            REPEAT
                                DimValue.GET(TempDimBufOut."Dimension Code", TempDimBufOut."Dimension Value Code");
                                TempDimSetEntry."Dimension Code" := TempDimBufOut."Dimension Code";
                                TempDimSetEntry."Dimension Value Code" := TempDimBufOut."Dimension Value Code";
                                TempDimSetEntry."Dimension Value ID" := DimValue."Dimension Value ID";
                                IF TempDimSetEntry.INSERT() THEN;
                                "Dimension Set ID" := DimMgt.GetDimensionSetID(TempDimSetEntry);
                                DimMgt.UpdateGlobalDimFromDimSetID("Dimension Set ID",
                                  "Shortcut Dimension 1 Code", "Shortcut Dimension 2 Code");
                                MODIFY();
                            UNTIL TempDimBufOut.NEXT() = 0;
                            TempDimBufOut.DELETEALL();
                        END;
                    END;
                END;
            END;
        END; // FIN DU BEGIN MC

    end;

    procedure InitializeRequest(NewPostingDate: Date; DocNo: Code[20]; ItemsNotOnInvt: Boolean);
    begin
        PostingDate := NewPostingDate;
        NextDocNo := DocNo;
        ZeroQty := ItemsNotOnInvt;
        IF NOT SkipDim THEN
            ColumnDim := DimSelectionBuf.GetDimSelectionText(3, REPORT::"Calculate Inventory", '');
    end;

    procedure TransferDim(DimSetID: Integer);
    begin
        DimSetEntry.SETRANGE("Dimension Set ID", DimSetID);
        IF DimSetEntry.FIND('-') THEN
            REPEAT
                IF TempSelectedDim.GET(
                     USERID, 3, REPORT::"Calculate Inventory", '', DimSetEntry."Dimension Code")
                THEN
                    InsertDim(DATABASE::"Item Ledger Entry", DimSetID, DimSetEntry."Dimension Code", DimSetEntry."Dimension Value Code");
            UNTIL DimSetEntry.NEXT() = 0;

    end;

    local procedure CalcWhseQty(AdjmtBin: Code[20]; var PosQuantity: Decimal; var NegQuantity: Decimal);
    var
        WhseEntry: Record "Warehouse Entry";
        WhseEntry2: Record "Warehouse Entry";
        ItemTrackingMgt: Codeunit "Item Tracking Management";
        WhseQuantity: Decimal;
        WhseSNRequired: Boolean;
        WhseLNRequired: Boolean;
        NoWhseEntry: Boolean;
        NoWhseEntry2: Boolean;
    begin
        AdjustPosQty := FALSE;
        WITH TempQuantityOnHandBuffer DO BEGIN
            ItemTrackingMgt.CheckWhseItemTrkgSetup("Item No.");
            ItemTrackingSplit := WhseSNRequired OR WhseLNRequired;
            WhseEntry.SETCURRENTKEY(
              "Item No.", "Bin Code", "Location Code", "Variant Code", "Unit of Measure Code",
              "Lot No.", "Serial No.", "Entry Type");

            WhseEntry.SETRANGE("Item No.", "Item No.");
            WhseEntry.SETRANGE("Location Code", "Location Code");
            WhseEntry.SETRANGE("Variant Code", "Variant Code");
            WhseEntry.CALCSUMS("Qty. (Base)");
            WhseQuantity := WhseEntry."Qty. (Base)";
            WhseEntry.SETRANGE("Bin Code", AdjmtBin);

            IF WhseSNRequired THEN BEGIN
                WhseEntry.SETRANGE("Entry Type", WhseEntry."Entry Type"::"Positive Adjmt.");
                WhseEntry.CALCSUMS("Qty. (Base)");
                PosQuantity := WhseQuantity - WhseEntry."Qty. (Base)";
                WhseEntry.SETRANGE("Entry Type", WhseEntry."Entry Type"::"Negative Adjmt.");
                WhseEntry.CALCSUMS("Qty. (Base)");
                NegQuantity := WhseQuantity - WhseEntry."Qty. (Base)";
                WhseEntry.SETRANGE("Entry Type", WhseEntry."Entry Type"::Movement);
                WhseEntry.CALCSUMS("Qty. (Base)");
                IF WhseEntry."Qty. (Base)" <> 0 THEN
                    IF WhseEntry."Qty. (Base)" > 0 THEN
                        PosQuantity := PosQuantity + WhseQuantity - WhseEntry."Qty. (Base)"
                    ELSE
                        NegQuantity := NegQuantity - WhseQuantity - WhseEntry."Qty. (Base)";


                WhseEntry.SETRANGE("Entry Type", WhseEntry."Entry Type"::"Positive Adjmt.");
                IF WhseEntry.FIND('-') THEN
                    REPEAT
                        WhseEntry.SETRANGE("Serial No.", WhseEntry."Serial No.");

                        WhseEntry2.RESET();
                        WhseEntry2.SETCURRENTKEY(
                          "Item No.", "Bin Code", "Location Code", "Variant Code",
                          "Unit of Measure Code", "Lot No.", "Serial No.", "Entry Type");

                        WhseEntry2.COPYFILTERS(WhseEntry);
                        WhseEntry2.SETRANGE("Entry Type", WhseEntry2."Entry Type"::"Negative Adjmt.");
                        WhseEntry2.SETRANGE("Serial No.", WhseEntry."Serial No.");
                        IF WhseEntry2.FIND('-') THEN
                            REPEAT
                                PosQuantity := PosQuantity + 1;
                                NegQuantity := NegQuantity - 1;
                                NoWhseEntry := WhseEntry.NEXT() = 0;
                                NoWhseEntry2 := WhseEntry2.NEXT() = 0;
                            UNTIL NoWhseEntry2 OR NoWhseEntry
                        ELSE
                            AdjustPosQty := TRUE;

                        IF NOT NoWhseEntry AND NoWhseEntry2 THEN
                            AdjustPosQty := TRUE;

                        WhseEntry.FIND('+');
                        WhseEntry.SETRANGE("Serial No.");
                    UNTIL WhseEntry.NEXT() = 0;

            END ELSE BEGIN
                IF WhseEntry.FIND('-') THEN
                    REPEAT
                        WhseEntry.SETRANGE("Lot No.", WhseEntry."Lot No.");
                        WhseEntry.CALCSUMS("Qty. (Base)");
                        IF WhseEntry."Qty. (Base)" <> 0 THEN
                            IF WhseEntry."Qty. (Base)" > 0 THEN
                                NegQuantity := NegQuantity - WhseEntry."Qty. (Base)"
                            ELSE
                                PosQuantity := PosQuantity + WhseEntry."Qty. (Base)";

                        WhseEntry.FIND('+');
                        WhseEntry.SETRANGE("Lot No.");
                    UNTIL WhseEntry.NEXT() = 0;
                IF PosQuantity <> WhseQuantity THEN
                    PosQuantity := WhseQuantity - PosQuantity;
                IF NegQuantity <> -WhseQuantity THEN
                    NegQuantity := WhseQuantity + NegQuantity;
            END;
        END;
    end;

    procedure SetHideValidationDialog(NewHideValidationDialog: Boolean);
    begin
        HideValidationDialog := NewHideValidationDialog;
    end;

    procedure InitializePhysInvtCount(PhysInvtCountCode2: Code[10]; CountSourceType2: Option " ",Item,SKU);
    begin
        PhysInvtCountCode := PhysInvtCountCode2;
        CycleSourceType := CountSourceType2;
    end;

    local procedure SkipCycleSKU(LocationCode: Code[10]; ItemNo: Code[20]; VariantCode: Code[10]): Boolean;
    var
        SKU: Record "Stockkeeping Unit";
    begin
        IF CycleSourceType = CycleSourceType::Item THEN
            IF SKU.READPERMISSION THEN
                IF SKU.GET(LocationCode, ItemNo, VariantCode) THEN
                    EXIT(TRUE);
        EXIT(FALSE);
    end;

    local procedure GetLocation(LocationCode: Code[10]);
    begin
        IF LocationCode = '' THEN
            CLEAR(Location)
        ELSE
            IF Location.Code <> LocationCode THEN
                IF Location.GET(LocationCode) THEN
                    IF Location."Bin Mandatory" AND NOT Location."Directed Put-away and Pick" THEN
                        IF (Item.GETFILTER("Global Dimension 1 Code") <> '') OR
                           (Item.GETFILTER("Global Dimension 2 Code") <> '') OR
                           TempDimBufIn.FINDFIRST()
                        THEN
                            ERROR(Text004Err, Location.FIELDCAPTION("Bin Mandatory"), Location."Bin Mandatory");

    end;

    local procedure UpdateBuffer(BinCode: Code[20]; NewQuantity: Decimal);
    var
        DimEntryNo: Integer;
    begin
        WITH TempQuantityOnHandBuffer DO BEGIN
            IF NOT HasNewQuantity(NewQuantity) THEN
                EXIT;
            IF BinCode = '' THEN BEGIN
                IF ColumnDim <> '' THEN
                    TempDimBufIn.SETRANGE("Entry No.", "Item Ledger Entry"."Dimension Set ID");
                DimEntryNo := DimBufMgt.FindDimensions(TempDimBufIn);
                IF DimEntryNo = 0 THEN
                    DimEntryNo := DimBufMgt.InsertDimensions(TempDimBufIn);
            END;
            IF RetrieveBuffer(BinCode, DimEntryNo) THEN BEGIN
                Quantity := Quantity + NewQuantity;
                MODIFY();
            END ELSE BEGIN
                Quantity := NewQuantity;
                INSERT();
            END;
        END;
    end;

    procedure RetrieveBuffer(BinCode: Code[20]; DimEntryNo: Integer): Boolean;
    begin
        WITH TempQuantityOnHandBuffer DO BEGIN
            RESET();
            "Item No." := "Item Ledger Entry"."Item No.";
            "Variant Code" := "Item Ledger Entry"."Variant Code";
            "Location Code" := "Item Ledger Entry"."Location Code";
            "Dimension Entry No." := DimEntryNo;
            "Bin Code" := BinCode;
            EXIT(FIND());
        END;
    end;

    procedure HasNewQuantity(NewQuantity: Decimal): Boolean;
    begin
        EXIT((NewQuantity <> 0) OR ZeroQty);
    end;

    procedure ItemBinLocationIsCalculated(BinCode: Code[20]): Boolean;
    begin
        WITH TempQuantityOnHandBuffer DO BEGIN
            RESET();
            SETRANGE("Item No.", "Item Ledger Entry"."Item No.");
            SETRANGE("Variant Code", "Item Ledger Entry"."Variant Code");
            SETRANGE("Location Code", "Item Ledger Entry"."Location Code");
            SETRANGE("Bin Code", BinCode);
            EXIT(FIND('-'));
        END;
    end;

    procedure SetSkipDim(NewSkipDim: Boolean);
    begin
        SkipDim := NewSkipDim;
    end;

    local procedure UpdateQuantityOnHandBuffer(No: Code[20]);
    begin
        TempQuantityOnHandBuffer.SETRANGE("Item No.", No);
        IF TempQuantityOnHandBuffer.ISEMPTY THEN BEGIN
            TempQuantityOnHandBuffer.INIT(); // ANI Le 20-09-2017 ticket 26340
            TempQuantityOnHandBuffer."Item No." := No;
            TempQuantityOnHandBuffer.Insert();
        END;
    end;

    local procedure CalcPhysInvQtyAndInsertItemJnlLine();
    begin

        WITH TempQuantityOnHandBuffer DO BEGIN

            RESET();
            IF FINDSET() THEN BEGIN
                REPEAT
                    PosQty := 0;
                    NegQty := 0;

                    GetLocation("Location Code");
                    IF Location."Directed Put-away and Pick" THEN
                        CalcWhseQty(Location."Adjustment Bin Code", PosQty, NegQty);

                    IF (NegQty - Quantity <> Quantity - PosQty) OR ItemTrackingSplit THEN BEGIN
                        IF PosQty = Quantity THEN
                            PosQty := 0;
                        IF (PosQty <> 0) OR AdjustPosQty THEN
                            InsertItemJnlLine(
                              "Item No.", "Variant Code", "Dimension Entry No.",
                              "Bin Code", Quantity, PosQty);

                        IF NegQty = Quantity THEN
                            NegQty := 0;
                        IF NegQty <> 0 THEN BEGIN
                            IF ((PosQty <> 0) OR AdjustPosQty) AND NOT ItemTrackingSplit THEN BEGIN
                                NegQty := NegQty - Quantity;
                                Quantity := 0;
                                ZeroQty := TRUE;
                            END;
                            IF NegQty = -Quantity THEN BEGIN
                                NegQty := 0;
                                AdjustPosQty := TRUE;
                            END;
                            InsertItemJnlLine(
                              "Item No.", "Variant Code", "Dimension Entry No.",
                              "Bin Code", Quantity, NegQty);

                            ZeroQty := ZeroQtySave;
                        END;
                    END ELSE BEGIN
                        PosQty := 0;
                        NegQty := 0;
                    END;

                    IF (PosQty = 0) AND (NegQty = 0) AND NOT AdjustPosQty AND ("Item No." <> '') THEN
                        InsertItemJnlLine(
                          "Item No.", "Variant Code", "Dimension Entry No.",
                          "Bin Code", Quantity, Quantity);
                UNTIL NEXT() = 0;
                DELETEALL();
            END;
        END;
    end;

    local procedure CreateDimFromItemDefault() DimEntryNo: Integer;
    var
        DefaultDimension: Record "Default Dimension";
    begin
        WITH DefaultDimension DO BEGIN
            SETRANGE("No.", TempQuantityOnHandBuffer."Item No.");
            SETRANGE("Table ID", DATABASE::Item);
            IF FINDSET() THEN
                REPEAT
                    InsertDim(DATABASE::Item, 0, "Dimension Code", "Dimension Value Code");
                UNTIL NEXT() = 0;
        END;

        DimEntryNo := DimBufMgt.InsertDimensions(TempDimBufIn);
        TempDimBufIn.SETRANGE("Table ID", DATABASE::Item);
        TempDimBufIn.DELETEALL();
    end;

    local procedure InsertDim(TableID: Integer; EntryNo: Integer; DimCode: Code[20]; DimValueCode: Code[20]);
    begin
        WITH TempDimBufIn DO BEGIN
            INIT();
            "Table ID" := TableID;
            "Entry No." := EntryNo;
            "Dimension Code" := DimCode;
            "Dimension Value Code" := DimValueCode;
            IF INSERT() THEN;
        END;
    end;

    local procedure SetCheckBox();
    begin
        // CFR le 16/03/2022 - SFD20210929 : Inventaires >> type emplacement
        IF gPickingInventory AND gStaticInventory THEN
            TousLesEmplacements := TRUE
        ELSE
            TousLesEmplacements := FALSE;
    end;

    local procedure SetNextDocNo();
    begin
        NextDocNo := '';
        NextDocNo += COPYSTR(gMagasinier, STRPOS(gMagasinier, '\') + 1, 11);
        NextDocNo += '-';
        NextDocNo += FORMAT(TIME(), 0, '<Hours24,2><Filler Character,0>:<Minutes,2>:<Seconds,2>');
    end;

    procedure InitializeForWS(pLocationCode: Code[10]; pZoneCode: Code[10]; pFromBin: Code[20]; pToBin: Code[20]; pPickingInventory: Boolean; pStaticInventory: Boolean; pItemsNotOnInventory: Boolean; pIncludeItemWithNoTransaction: Boolean; pMagasinier: Code[50]; pItemNo: Code[20]);
    begin
        // CFR le 16/03/2022 - SFD20210929 : Inventaires >> type emplacement
        PostingDate := WORKDATE();
        ValidatePostingDate();
        ColumnDim := DimSelectionBuf.GetDimSelectionText(3, REPORT::"Calculate Inventory", '');

        // CFR le 16/03/2022 - SFD20210929 : Inventaires >> variables depuis le WebService
        gPickingInventory := pPickingInventory;
        gStaticInventory := pStaticInventory;

        gLocationCode := pLocationCode;
        gZoneCode := pZoneCode;
        gFromBin := pFromBin;
        gToBin := pToBin;
        gMagasinier := pMagasinier;
        // CFR le 09/09/2022 - SFD20210929 : Inventaires > Ajout filtre article
        gItemNo := pItemNo;

        ZeroQty := pItemsNotOnInventory;
        IncludeItemWithNoTransaction := pIncludeItemWithNoTransaction;

        SetNextDocNo();

    end;
}

