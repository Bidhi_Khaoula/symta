report 50027 "Statistiques ventes articles"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Statistiques ventes articles.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Statistiques ventes articles';
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("Item Category Code") ORDER(Ascending);
            RequestFilterFields = "Item Category Code",
            "No.", "Date Filter";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(ItemFilter; ItemFilter)
            {
            }
            column(TotauxUniquement; TotauxUniquement)
            {
            }
            column(PeriodText; PeriodText)
            {
            }
            column(PeriodTextAN_m1; PeriodTextAN_m1)
            {
            }
            column(ItemCategoryCode; "Item Category Code")
            {
            }
            //TODOcolumn(ProductGroupCode; "Product Group Code") {}
            column(No2; "No. 2")
            {
            }
            column(Description; Description + ' ' + "Description 2")
            {
            }
            column(NetWeight; "Net Weight")
            {
            }
            column(Mois_SalesQty; Mois_SalesQty)
            {
            }
            column(Mois_SalesAmount; Mois_SalesAmount)
            {
            }
            column(Mois_ItemProfit; Mois_ItemProfit)
            {
            }
            column(Mois_ItemProfitPct; Mois_ItemProfitPct)
            {
            }
            column(ANM1_SalesQty; ANM1_SalesQty)
            {
            }
            column(ANM1_SalesAmount; ANM1_SalesAmount)
            {
            }
            column(ANM1_ItemProfit; ANM1_ItemProfit)
            {
            }
            column(ANM1_ItemProfitPct; ANM1_ItemProfitPct)
            {
            }

            trigger OnAfterGetRecord();
            begin

                // CurrReport.CREATETOTALS(Mois_ItemProfit);
                // CurrReport.CREATETOTALS(AN_ItemProfit);
                // CurrReport.CREATETOTALS(ANM1_ItemProfit);
                Calculate();

                IF (Mois_SalesQty = 0) AND (AN_SalesQty = 0) AND (ANM1_SalesQty = 0) THEN
                    CurrReport.SKIP();
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(FiltreClientName; FiltreClient)
                {
                    Caption = 'FiltreClient';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FiltreClient field.';
                }
                field(TotauxUniquementName; TotauxUniquement)
                {
                    Caption = 'Totaux Uniquement';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Totaux Uniquement field.';
                }
            }
        }
    }


    trigger OnPreReport();
    begin

        Evaluate(ItemFilter, Item.GETFILTERS);

        Evaluate(PeriodText, Item.GETFILTER("Date Filter"));

        Mois := DATE2DMY(WORKDATE(), 2);
        Ann := DATE2DMY(WORKDATE(), 3);

        DebMois := DMY2DATE(1, Mois, Ann);
        FinMois := CALCDATE('<+1M>', DebMois);
        DebAnnee := DMY2DATE(1, 1, Ann);
        FinAnnee := DMY2DATE(31, 12, Ann);
        DebAnnee_M1 := DMY2DATE(1, 1, Ann - 1);
        FinAnnee_M1 := DMY2DATE(31, 12, Ann - 1);

        IF PeriodText = '' THEN
            PeriodText := FORMAT(DebMois) + '..' + FORMAT(FinMois);

        PeriodTextAN := FORMAT(DebAnnee) + '..' + FORMAT(FinAnnee);



        DebAnnee_M1 := Item.GETRANGEMIN("Date Filter");
        DebAnnee_M1 := CALCDATE('<-1A>', DebAnnee_M1);

        FinAnnee_M1 := Item.GETRANGEMAX("Date Filter");
        FinAnnee_M1 := CALCDATE('<-1A>', FinAnnee_M1);

        PeriodTextAN_m1 := STRSUBSTNO('%1..%2', DebAnnee_M1, FinAnnee_M1);
    end;

    var
        ItemStatisticsBuf: Record "Item Statistics Buffer";
        ItemFilter: Text[250];
        PeriodText: Text[30];
        PeriodTextAN: Text[30];
        PeriodTextAN_m1: Text[30];
        Mois_SalesQty: Decimal;
        Mois_SalesAmount: Decimal;
        Mois_COGSAmount: Decimal;
        Mois_ItemProfit: Decimal;
        Mois: Integer;
        Ann: Integer;
        DebMois: Date;
        FinMois: Date;
        DebAnnee: Date;
        FinAnnee: Date;
        DebAnnee_M1: Date;
        FinAnnee_M1: Date;
        AN_SalesQty: Decimal;
        // AN_SalesAmount: Decimal;
        // AN_COGSAmount: Decimal;
        // AN_ItemProfit: Decimal;
        ANM1_SalesQty: Decimal;
        ANM1_SalesAmount: Decimal;
        ANM1_COGSAmount: Decimal;
        ANM1_ItemProfit: Decimal;
        Mois_ItemProfitPct: Decimal;
        // AN_ItemProfitPct: Decimal;
        ANM1_ItemProfitPct: Decimal;
        FiltreClient: Code[10];
        TotauxUniquement: Boolean;

    local procedure SetFilters(periodFilter: Text[30]);
    var
    begin
        ItemStatisticsBuf.SETFILTER("Date Filter", periodFilter);

        IF Item.GETFILTER("Location Filter") <> '' THEN
            ItemStatisticsBuf.SETFILTER("Location Filter", Item.GETFILTER("Location Filter"));

        ItemStatisticsBuf.SETRANGE("Item Filter", Item."No.");
        ItemStatisticsBuf.SETRANGE("Item Ledger Entry Type Filter", ItemStatisticsBuf."Item Ledger Entry Type Filter"::Sale);
        ItemStatisticsBuf.SETFILTER("Entry Type Filter", '<>%1', ItemStatisticsBuf."Entry Type Filter"::Revaluation);

        // AD Le 09-12-2015 => MIG2015
        IF FiltreClient <> '' THEN BEGIN
            ItemStatisticsBuf.SETRANGE("Source Type Filter", ItemStatisticsBuf."Source Type Filter"::Customer);
            ItemStatisticsBuf.SETRANGE("Source No. Filter", FiltreClient);
        END;
    end;

    procedure Calculate();
    begin

        SetFilters(PeriodText);
        // Calcul sur le mois en cours
        //------------------------------
        Mois_SalesQty := -ROUND(CalcInvoicedQty(), 0.01);
        Mois_SalesAmount := ROUND(CalcSalesAmount(), 0.01);
        //Mois_SalesAmount := CalcSalesAmount;
        Mois_COGSAmount := ROUND(CalcCostAmount() + CalcCostAmountNonInvnt(), 0.01);
        Mois_ItemProfit := ROUND(Mois_SalesAmount + Mois_COGSAmount, 0.01);


        IF Mois_SalesAmount <> 0 THEN
            Mois_ItemProfitPct := ROUND(100 * Mois_ItemProfit / Mois_SalesAmount, 0.01)
        ELSE
            Mois_ItemProfitPct := 0;

        /* Plus utilise le 24/03/05
          Demande patrice venault
          Fait par AD
        SetFilters(PeriodTextAN);
        // Calcul sur l'annee en cours
        //------------------------------
        AN_SalesQty    := -ROUND(CalcInvoicedQty,0.01);
        AN_SalesAmount :=  ROUND( CalcSalesAmount,0.01);
        AN_COGSAmount  :=  ROUND(CalcCostAmount + CalcCostAmountNonInvnt,0.01);
        AN_ItemProfit  :=  ROUND(AN_SalesAmount + AN_COGSAmount,0.01);
        
        IF AN_SalesAmount <> 0 THEN
          AN_ItemProfitPct := ROUND(100 * AN_ItemProfit / AN_SalesAmount,0.01)
        ELSE
          AN_ItemProfitPct := 0;
        
        */


        SetFilters(PeriodTextAN_m1);
        // Calcul sur l'annee N-1
        //------------------------------
        ANM1_SalesQty := -CalcInvoicedQty();
        ANM1_SalesAmount := CalcSalesAmount();
        ANM1_COGSAmount := CalcCostAmount() + CalcCostAmountNonInvnt();
        ANM1_ItemProfit := ANM1_SalesAmount + ANM1_COGSAmount;

        IF ANM1_SalesAmount <> 0 THEN
            ANM1_ItemProfitPct := ROUND(100 * ANM1_ItemProfit / ANM1_SalesAmount, 0.01)
        ELSE
            ANM1_ItemProfitPct := 0;

    end;

    local procedure CalcSalesAmount(): Decimal;
    begin

        ItemStatisticsBuf.CALCFIELDS("Sales Amount (Actual)");
        EXIT(ItemStatisticsBuf."Sales Amount (Actual)");
    end;

    local procedure CalcCostAmount(): Decimal;
    begin

        ItemStatisticsBuf.CALCFIELDS("Cost Amount (Actual)");
        EXIT(ItemStatisticsBuf."Cost Amount (Actual)");
    end;

    local procedure CalcInvoicedQty(): Decimal;
    begin

        ItemStatisticsBuf.SETRANGE("Entry Type Filter");
        ItemStatisticsBuf.CALCFIELDS("Invoiced Quantity");
        EXIT(ItemStatisticsBuf."Invoiced Quantity");
    end;

    local procedure CalcCostAmountNonInvnt(): Decimal;
    begin
        WITH ItemStatisticsBuf DO BEGIN
            SETRANGE("Item Ledger Entry Type Filter");
            CALCFIELDS("Cost Amount (Non-Invtbl.)");
            EXIT(ItemStatisticsBuf."Cost Amount (Non-Invtbl.)");
        END;
    end;
}

