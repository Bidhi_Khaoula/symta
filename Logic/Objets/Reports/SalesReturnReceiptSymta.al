report 50041 "Sales - Return Receipt - Symta"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Sales - Return Receipt - Symta.rdlc';
    Caption = 'Sales - Return Receipt';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Return Receipt Header"; "Return Receipt Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeading = 'Posted Return Receipt';
            column(No_ReturnRcptHeader; "No.")
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(Title_ReturnRcptHeader; _TITRELbl)
                    {
                    }
                    column(SubTitle_ReturnRcptHeader; STRSUBSTNO(ESK001Lbl, "Return Receipt Header"."No.", FORMAT("Return Receipt Header"."Posting Date", 0, 4)))
                    {
                    }
                    column(CompanyInfo1Picture; CompanyInfo.Picture)
                    {
                    }
                    column(CompanyInfoPictureAddress; CompanyInfo."Picture Address")
                    {
                    }
                    column(TxtPied1; textSIEGSOC[1])
                    {
                    }
                    column(TxtPied2; textSIEGSOC[2])
                    {
                    }
                    column(TxtPied3; textSIEGSOC[3])
                    {
                    }
                    column(SalesReturnRcptCopyText; STRSUBSTNO(Text002Lbl, CopyText))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(ShipToAddr1; ShipToAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(ShipToAddr2; ShipToAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(ShipToAddr3; ShipToAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(ShipToAddr4; ShipToAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(ShipToAddr5; ShipToAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(ShipToAddr6; ShipToAddr[6])
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(SellCustNo_ReturnRcptHdr; "Return Receipt Header"."Sell-to Customer No.")
                    {
                    }
                    column(SellCustNo_ReturnRcptHdrCaption; "Return Receipt Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(DocDate_ReturnRcptHeader; FORMAT("Return Receipt Header"."Document Date", 0, 4))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(No1_ReturnRcptHeader; "Return Receipt Header"."No.")
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourRef_ReturnRcptHeader; "Return Receipt Header"."Your Reference")
                    {
                    }
                    column(ShipToAddr7; ShipToAddr[7])
                    {
                    }
                    column(ShipToAddr8; ShipToAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(ShptDt_ReturnRcptHeader; FORMAT("Return Receipt Header"."Shipment Date"))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(Correspondant; Correspondant)
                    {
                    }
                    column(UserId_ReturnRcptHeader; _UtilisateurRVE)
                    {
                    }
                    column(TypeRetour_ReturnRcptHeader; "Return Receipt Header"."Type retour")
                    {
                    }
                    column(ReturnOrderNo_ReturnRcptHeader; "Return Receipt Header"."Return Order No.")
                    {
                    }
                    column(ExternelDocumentNo_ReturnRcptHeader; "Return Receipt Header"."External Document No.")
                    {
                    }
                    column(TauxAbattement_ReturnRcptHeader; "Return Receipt Header"."Taux Abattement")
                    {
                    }
                    column(EnlevementChargeClient_ReturnRcptHeader; "Return Receipt Header"."Enlevement Charge Client")
                    {
                    }
                    column(PortChargeClient_ReturnRcptHeader; "Return Receipt Header"."Port Charge Client")
                    {
                    }
                    column(PageCaption; STRSUBSTNO(Text003Lbl, ''))
                    {
                    }
                    column(CompanyInfoPhoneNoCaption; CompanyInfoPhoneNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoVATRegNoCptn; CompanyInfoVATRegNoCptnLbl)
                    {
                    }
                    column(CompanyInfoGiroNoCaption; CompanyInfoGiroNoCaptionLbl)
                    {
                    }
                    column(CompanyInfoBankNameCptn; CompanyInfoBankNameCptnLbl)
                    {
                    }
                    column(CompanyInfoBankAccNoCptn; CompanyInfoBankAccNoCptnLbl)
                    {
                    }
                    column(ReturnReceiptHeaderNoCptn; ReturnReceiptHeaderNoCptnLbl)
                    {
                    }
                    column(ReturnRcptHdrShptDtCptn; ReturnRcptHdrShptDtCptnLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(HomePageCaption; HomePageCaptionLbl)
                    {
                    }
                    column(EmailCaption; EmailCaptionLbl)
                    {
                    }
                    column(ChiffrageBL; "Return Receipt Header"."Chiffrage BL")
                    {
                    }
                    dataitem("Sales Comment Line"; "Sales Comment Line")
                    {
                        DataItemLink = "No." = FIELD("No.");
                        DataItemLinkReference = "Return Receipt Header";
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            WHERE("Document Type" = CONST("Posted Return Receipt"),
                                                  "Print Shipment" = CONST(true),
                                                  "Document Line No." = CONST(0));
                        column(CommentLine_ReturnRcptHeader; Comment)
                        {
                        }
                    }
                    dataitem(DimensionLoop1; Integer)
                    {
                        DataItemLinkReference = "Return Receipt Header";
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1Number; Number)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        var
                            ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                        begin
                            IF Number = 1 THEN BEGIN
                                IF NOT DimSetEntry1.FINDSET() THEN
                                    CurrReport.BREAK();
                            END ELSE
                                IF NOT Continue THEN
                                    CurrReport.BREAK();

                            CLEAR(DimText);
                            Continue := FALSE;
                            REPEAT
                                OldDimText := DimText;
                                IF DimText = '' THEN
                                    DimText := STRSUBSTNO(Param2Txt, DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                ELSE
                                    DimText :=
                                      STRSUBSTNO(
                                        ParamTxt, DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                    DimText := OldDimText;
                                    Continue := TRUE;
                                    EXIT;
                                END;
                            UNTIL DimSetEntry1.NEXT() = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowInternalInfo THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem("Return Receipt Line"; "Return Receipt Line")
                    {
                        DataItemLink = "Document No." = FIELD("No.");
                        DataItemLinkReference = "Return Receipt Header";
                        DataItemTableView = SORTING("Document No.", "Line No.");
                        column(Show_InternalInfo; ShowInternalInfo)
                        {
                        }
                        column(TypeInt; TypeInt)
                        {
                        }
                        column(Desc_ReturnReceiptLine; Description)
                        {
                        }
                        column(DocNo_ReturnReceiptLine; "Document No.")
                        {
                        }
                        column(UOM_ReturnReceiptLine; "Unit of Measure")
                        {
                        }
                        column(Qty_ReturnReceiptLine; Quantity)
                        {
                        }
                        column(LineAmount_ReturnReceiptLine; _MtLigne)
                        {
                        }
                        column(Remise_ReturnReceiptLine; _Remise)
                        {
                        }
                        column(No_ReturnReceiptLine; "No.")
                        {
                        }
                        column(NetUnitPrice_ReturnReceiptLine; "Net Unit Price")
                        {
                        }
                        column(UnitPrice_ReturnReceiptLine; "Unit Price")
                        {
                            AutoFormatExpression = "Return Receipt Header"."Currency Code";
                            AutoFormatType = 2;
                        }
                        column(LineDisc_ReturnReceiptLine; "Line Discount %")
                        {
                        }
                        column(AllowInvDisc_ReturnReceiptLine; "Allow Invoice Disc.")
                        {
                        }
                        column(AllowInvDiscYesNo_ReturnReceiptLine; FORMAT("Allow Invoice Disc."))
                        {
                        }
                        column(Ligne_ReturnReceiptLine; NoLigne)
                        {
                        }
                        column(Reference_ReturnReceiptLine; _Reference1)
                        {
                        }
                        column(UOM_ReturnReceiptLineCaption; FIELDCAPTION("Unit of Measure"))
                        {
                        }
                        column(Qty_ReturnReceiptLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(Desc_ReturnReceiptLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(No_ReturnReceiptLineCaption; FIELDCAPTION("No."))
                        {
                        }
                        column(LineNo_ReturnReceiptLine; "Line No.")
                        {
                        }
                        dataitem(ReturnReceiptCommentLine; "Sales Comment Line")
                        {
                            DataItemLink = "No." = FIELD("Document No."),
                                           "Document Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Return Receipt Line";
                            DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                                WHERE("Document Type" = CONST("Posted Return Receipt"),
                                                      Comment = FILTER(<> ''),
                                                      "Print Shipment" = CONST(true));
                            column(CommentLine_SalesCommentLine; Comment)
                            {
                            }
                        }
                        dataitem(DimensionLoop2; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = FILTER(1 ..));
                            column(DimText1; DimText)
                            {
                            }
                            column(DimensionLoop2Number; Number)
                            {
                            }
                            column(LineDimensionsCaption; LineDimensionsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            var
                                ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                            begin
                                IF Number = 1 THEN BEGIN
                                    IF NOT DimSetEntry2.FINDSET() THEN
                                        CurrReport.BREAK();
                                END ELSE
                                    IF NOT Continue THEN
                                        CurrReport.BREAK();

                                CLEAR(DimText);
                                Continue := FALSE;
                                REPEAT
                                    OldDimText := DimText;
                                    IF DimText = '' THEN
                                        DimText := STRSUBSTNO(Param2Txt, DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    ELSE
                                        DimText :=
                                          STRSUBSTNO(
                                            ParamTxt, DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                        DimText := OldDimText;
                                        Continue := TRUE;
                                        EXIT;
                                    END;
                                UNTIL DimSetEntry2.NEXT() = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                IF NOT ShowInternalInfo THEN
                                    CurrReport.BREAK();
                            end;
                        }

                        trigger OnAfterGetRecord();
                        var
                            taux: Integer;
                        begin
                            IF (NOT ShowCorrectionLines) AND Correction THEN
                                CurrReport.SKIP();

                            IF (Type <> Type::" ") AND (Quantity = 0) THEN
                                CurrReport.SKIP();

                            DimSetEntry2.SETRANGE("Dimension Set ID", "Dimension Set ID");
                            TypeInt := Type.AsInteger();


                            // GR - MIG2015
                            _Reference1 := GestionMulti.RechercheRefActive("Return Receipt Line"."No.");
                            NoLigne += 1;
                            _MtLigne := "Return Receipt Line"."Net Unit Price" * "Return Receipt Line".Quantity * -1;

                            _Remise := '';
                            IF ("Return Receipt Line"."Discount1 %" <> 0) OR ("Return Receipt Line"."Discount2 %" <> 0) THEN BEGIN
                                IF ("Return Receipt Line"."Discount1 %" <> 0) THEN
                                    _Remise := FORMAT("Return Receipt Line"."Discount1 %") + '%';

                                IF ("Return Receipt Line"."Discount2 %" <> 0) THEN BEGIN
                                    IF ("Return Receipt Line"."Discount1 %" <> 0) THEN
                                        _Remise += '+';
                                    _Remise += FORMAT("Return Receipt Line"."Discount2 %") + '%';
                                END;

                            END;
                            TempVATAmountLine.DELETEALL();
                            TempVATAmountLine.INIT();
                            //TempVATAmountLine."VAT Identifier" := "VAT Identifier";
                            TempVATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                            TempVATAmountLine."Tax Group Code" := "Tax Group Code";
                            TempVATAmountLine."VAT %" := "VAT %";
                            TempVATAmountLine."VAT Base" := "Return Receipt Line"."Net Unit Price" * "Return Receipt Line".Quantity;
                            TempVATAmountLine."Amount Including VAT" := TempVATAmountLine."VAT Base" * (1 + TempVATAmountLine."VAT %" / 100);
                            TempVATAmountLine."Line Amount" := "Return Receipt Line"."Net Unit Price" * "Return Receipt Line".Quantity;
                            IF "Allow Invoice Disc." THEN
                                TempVATAmountLine."Inv. Disc. Base Amount" := "Return Receipt Line"."Net Unit Price" * "Return Receipt Line".Quantity;
                            //TempVATAmountLine."Invoice Discount Amount" := "Inv. Discount Amount";
                            TempVATAmountLine.InsertLine();

                            // CurrReport.CREATETOTALS(
                            //   TempVATAmountLine."Line Amount", TempVATAmountLine."Inv. Disc. Base Amount",
                            //   TempVATAmountLine."Invoice Discount Amount", TempVATAmountLine."VAT Base", TempVATAmountLine."VAT Amount");



                            IF ((taux_tva[2] = TempVATAmountLine."VAT Identifier") OR (taux_tva[2] = '')) THEN
                                taux := 2
                            ELSE
                                taux := 3;


                            taux_tva[taux] := TempVATAmountLine."VAT Identifier";
                            ////tva_ht[taux] := TempVATAmountLine."VAT %";
                            total_ht[taux] -= TempVATAmountLine."VAT Base";
                            total_tva[taux] -= TempVATAmountLine."VAT Amount";
                            total_ht_net[taux] -= TempVATAmountLine."Line Amount";
                            total_ttc[taux] -= TempVATAmountLine."Amount Including VAT";
                            total_discount[taux] -= TempVATAmountLine."Invoice Discount Amount";
                            taux_tva[taux] := TempVATAmountLine."VAT Identifier";

                            total_ht[1] -= TempVATAmountLine."VAT Base";
                            total_tva[1] -= TempVATAmountLine."VAT Amount";
                            total_ht_net[1] -= TempVATAmountLine."Line Amount";
                            total_ttc[1] -= TempVATAmountLine."Amount Including VAT";
                            total_discount[1] -= TempVATAmountLine."Invoice Discount Amount";
                        end;

                        trigger OnPreDataItem();
                        begin
                            MoreLines := FIND('+');
                            WHILE MoreLines AND (Description = '') AND ("No." = '') AND (Quantity = 0) DO
                                MoreLines := NEXT(-1) <> 0;
                            IF NOT MoreLines THEN
                                CurrReport.BREAK();
                            SETRANGE("Line No.", 0, "Line No.");

                            FOR i := 1 TO 3 DO BEGIN
                                total_ht[i] := 0;
                                total_tva[i] := 0;
                                total_ht_net[i] := 0;
                                total_ttc[i] := 0;
                                total_discount[i] := 0;
                                taux_tva[i] := '';
                            END;
                        end;
                    }
                    dataitem(Total; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(TotalHt; total_ht[1])
                        {
                        }
                        column(TotalHt1; total_ht[2])
                        {
                        }
                        column(TotalHt2; total_ht[3])
                        {
                        }
                        column(TotalHtNet; total_ht_net[1])
                        {
                        }
                        column(TotalHtNet1; total_ht_net[2])
                        {
                        }
                        column(TotalHtNet2; total_ht_net[3])
                        {
                        }
                        column(TotalTva; total_tva[1])
                        {
                        }
                        column(TotalTva1; total_tva[2])
                        {
                        }
                        column(TotalTva2; total_tva[3])
                        {
                        }
                        column(TauxTVA_1; taux_tva[2])
                        {
                        }
                        column(TauxTVA_2; taux_tva[3])
                        {
                        }
                        column(TotalTtc; total_ttc[1])
                        {
                        }
                        column(TotalTtc1; total_ttc[2])
                        {
                        }
                        column(TotalTtc2; total_ttc[3])
                        {
                        }
                        column(TotalDiscount; total_discount[1])
                        {
                        }
                        column(TotalDiscount1; total_discount[2])
                        {
                        }
                        column(TotalDiscount2; total_discount[3])
                        {
                        }
                        column(BilltoCustNo_ReturnRcptHdr; "Return Receipt Header"."Bill-to Customer No.")
                        {
                        }
                        column(BilltoAddressCaption; BilltoAddressCaptionLbl)
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowCustAddr THEN
                                CurrReport.BREAK();
                        end;
                    }
                }

                trigger OnAfterGetRecord();
                begin
                    IF Number > 1 THEN BEGIN
                        CopyText := Text001Lbl;
                        OutputNo += 1;
                    END;
                end;

                trigger OnPostDataItem();
                begin
                    IF NOT CurrReport.PREVIEW THEN
                        RcptCountPrinted.RUN("Return Receipt Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := 1 + ABS(NoOfCopies);
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                Language: codeunit Language;
            begin
                CurrReport.LANGUAGE := Language.GetLanguageID("Language Code");

                ReportLineNumber += 1;
                IF ReportLineNumber = 2 THEN BEGIN
                    CLEAR(CompanyInfo);
                    CompanyInfo.GET();
                END;

                IF RespCenter.GET("Responsibility Center") THEN BEGIN
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                END ELSE
                    FormatAddr.Company(CompanyAddr, CompanyInfo);

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF "Salesperson Code" = '' THEN BEGIN
                    SalesPurchPerson.INIT();
                    SalesPersonText := '';
                END ELSE BEGIN
                    SalesPurchPerson.GET("Salesperson Code");
                    SalesPersonText := Text000Lbl;
                END;
                IF "Your Reference" = '' THEN
                    ReferenceText := ''
                ELSE
                    Evaluate(ReferenceText, FIELDCAPTION("Your Reference"));
                FormatAddr.SalesRcptShipTo(ShipToAddr, "Return Receipt Header");

                // AD Le 09-12-2015 => Demande de Marie Laure
                //FormatAddr.SalesRcptBillTo(CustAddr,"Return Receipt Header");
                FormatAddr.SalesRcptSellTo(CustAddr, "Return Receipt Header");
                // FIN AD Le 09-12-2015

                ShowCustAddr := "Bill-to Customer No." <> "Sell-to Customer No.";
                FOR i := 1 TO ARRAYLEN(CustAddr) DO
                    IF CustAddr[i] <> ShipToAddr[i] THEN
                        ShowCustAddr := TRUE;
                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN
                        SegManagement.LogDocument(
                          20, "No.", 0, 0, DATABASE::Customer, "Bill-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');

                _UtilisateurRVE := '';

                Correspondant := ReportUtils.GetCorrespondantName("Create User ID", '');
                _UtilisateurRVE := ReportUtils.GetCorrespondantName("User ID", '');
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(No_OfCopies; NoOfCopies)
                    {
                        Caption = 'No. of Copies';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. of Copies field.';
                    }
                    field(Show_InternalInfo; ShowInternalInfo)
                    {
                        Caption = 'Show Internal Information';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Internal Information field.';
                    }
                    field(Show_CorrectionLines; ShowCorrectionLines)
                    {
                        Caption = 'Show Correction Lines';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Correction Lines field.';
                    }
                    field(Log_Interaction; LogInteraction)
                    {
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Log Interaction field.';
                    }
                    field("Imprimer Logo société"; _ImprimerLogo)
                    {
                        Caption = 'Imprimer Logo société';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Imprimer Logo société field.';
                    }
                }
            }
        }

        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;
        end;
    }
    trigger OnInitReport();
    begin
        SalesSetup.GET();

        ReportLineNumber := 0;
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();

        CompanyInfo.GET();

        IF EskapeCommunication.GetImpressionPdf() THEN
            _ImprimerLogo := TRUE;
        //MODIFICATION VINCENT DOUSSET EDITION PAPIER EN-TETE Le 23/10/2019
        IF _ImprimerLogo THEN BEGIN
            CompanyInfo.CALCFIELDS(Picture);
            CompanyInfo.CALCFIELDS("Picture Address");
            GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
        END
        ELSE
            GestionEdition.GetInfoPiedPage(FALSE, textSIEGSOC);
    end;

    var

        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        RespCenter: Record "Responsibility Center";
        SalesSetup: Record "Sales & Receivables Setup";
        ReportUtils: Codeunit ReportFunctions;
        RcptCountPrinted: Codeunit "Return Receipt - Printed";
        GestionEdition: Codeunit "Gestion Info Editions";
        FormatAddr: Codeunit "Format Address";
        GestionMulti: Codeunit "Gestion Multi-référence";
        EskapeCommunication: Codeunit "Eskape Communication";
        SegManagement: Codeunit SegManagement;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text[80];
        CopyText: Text[30];
        DimText: Text[120];
        OldDimText: Text[120];
        MoreLines: Boolean;
        ShowCustAddr: Boolean;
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        i: Integer;
        ShowCorrectionLines: Boolean;
        LogInteraction: Boolean;
        OutputNo: Integer;
        TypeInt: Integer;
        LogInteractionEnable: Boolean;
        Text000Lbl: Label 'Salesperson';
        Text001Lbl: Label 'COPY';
        Text002Lbl: Label 'Sales - Return Receipt %1', Comment = '%1 = Retour No';
        Text003Lbl: Label 'Page %1', Comment = '%1 = PAge Nb';
        Param2Txt: Label '%1 - %2', Comment = '%1; %2';
        CompanyInfoPhoneNoCaptionLbl: Label 'Phone No.';
        CompanyInfoVATRegNoCptnLbl: Label 'VAT Reg. No.';
        CompanyInfoGiroNoCaptionLbl: Label 'Giro No.';
        CompanyInfoBankNameCptnLbl: Label 'Bank';
        CompanyInfoBankAccNoCptnLbl: Label 'Account No.';
        ReturnReceiptHeaderNoCptnLbl: Label 'Receipt No.';
        ReturnRcptHdrShptDtCptnLbl: Label 'Shipment Date';
        DocumentDateCaptionLbl: Label 'Document Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'E-Mail';
        HeaderDimensionsCaptionLbl: Label 'Header Dimensions';
        LineDimensionsCaptionLbl: Label 'Line Dimensions';
        BilltoAddressCaptionLbl: Label 'Bill-to Address';
        /* Text50002: Label 'Code douanier : %1';
        Text50003: Label 'Cdt : %1';
        Text50004: Label 'Origine : %1';
        Text50005: Label 'Delivery note submitted to our general terms & conditions of sale (see on the back of the invoice), in particular regarding the title-retention clause and clause granting jurisdiction to the Tribunal de Commerce de Tours.';
        Text50000: Label 'Edition des étiquettes clients ?'; */
        ESK001Lbl: Label 'No %1 du %2', COmment = '%1 , %2';
        /*   ESK002: Label '%1 en %2  -  %3';
          ESK003: Label 'Vos Références : %1 %2';
          ESK004: Label 'Enregistrée par : %1';
          ESK005: Label 'Nombre de colis : %1  -  Poids %2';
          ESK006: Label 'Commande No : %1 du %2';
          ESK007: Label 'Préparé par : %1';
          ESK008: Label 'Notre commande : %1';
   */
        _TITRELbl: Label 'BON DE LIVRAISON';
        /*  "-- Eskape--": Integer;
         _NoLigne: Integer; */
        _Reference1: Text[52];
        NoLigne: Integer;




        Correspondant: Text;
        _MtLigne: Decimal;
        textSIEGSOC: array[5] of Text[250];


        _Remise: Text[13];
        total_ht: array[3] of Decimal;
        total_tva: array[3] of Decimal;
        total_ht_net: array[3] of Decimal;
        total_discount: array[3] of Decimal;
        total_ttc: array[3] of Decimal;
        taux_tva: array[3] of Text;

        _UtilisateurRVE: Text[250];
        ReportLineNumber: Integer;
        _ImprimerLogo: Boolean;

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(20)) <> '';
    end;
}

