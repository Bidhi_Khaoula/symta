report 50173 "Import Ligne Document Vte XLS"
{
    Caption = 'Import Ligne Document Vte XLS';
    ProcessingOnly = true;
    ShowPrintStatus = false;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                LSalesLine: Record "Sales Line";
                LSalesCommentLine: Record "Sales Comment Line";
                LRefSP: Code[20];
                LRefActive: Code[20];
                LRefSaisie: Code[20];
                LQte: Decimal;
            begin


                NoLigne := NoLigne + 1;
                Fenetre.UPDATE(1, ROUND(NoLigne / NbLigne * 10000, 1));

                // Vide les variables
                LRefSP := '';
                LRefActive := '';
                LRefSaisie := '';
                CLEAR(LSalesLine);
                LQte := 0;

                // LEcture & vérif des données obligatoires
                // Lecture Ref SP
                IF GetCell('A', NoLigne) THEN
                    LRefSP := TempExcelBuf."Cell Value as Text";

                // Lecture RefActive
                IF GetCell('B', NoLigne) THEN
                    LRefActive := TempExcelBuf."Cell Value as Text";

                // Lecture RefSaisie
                IF GetCell('C', NoLigne) THEN
                    LRefSaisie := TempExcelBuf."Cell Value as Text";

                IF (STRLEN(LRefSP) + STRLEN(LRefActive) + STRLEN(LRefSaisie)) = 0 THEN ERROR('Référence obligatoire sur ligne %1', NoLigne);

                // Lecture qte
                IF NOT GetCell('D', NoLigne) THEN
                    ERROR('Quantité obligatoire sur ligne %1', NoLigne)
                ELSE
                    IF NOT EVALUATE(LQte, TempExcelBuf."Cell Value as Text") THEN
                        ERROR('Problème de quantité sur ligne %1', NoLigne);

                // Lecture Prix
                IF GetCell('F', NoLigne) THEN
                    IF NOT EVALUATE(LSalesLine."Unit Price", TempExcelBuf."Cell Value as Text") THEN
                        ERROR('Problème de prix sur ligne %1', NoLigne);

                // Fin du controle des données

                // Créaation de la ligne
                NoLigneCde += 10000;
                CLEAR(LSalesLine);
                LSalesLine.VALIDATE("Document Type", SalesHeader."Document Type");
                LSalesLine.VALIDATE("Document No.", SalesHeader."No.");
                LSalesLine."Line No." := NoLigneCde;
                LSalesLine.INSERT(TRUE);

                // Affectation de l'article
                LSalesLine.VALIDATE(Type, LSalesLine.Type::Item);
                IF LRefSP <> '' THEN
                    LSalesLine.VALIDATE("No.", LRefSP)
                ELSE
                    IF LRefActive <> '' THEN
                        LSalesLine.VALIDATE("No.", CuMultiRef.RechercheArticleByActive(LRefActive))
                    ELSE BEGIN
                        LRefSP := CuMultiRef.RechercheMultiReference(LRefSaisie);
                        IF LRefSP <> 'RIENTROUVE' THEN
                            LSalesLine.VALIDATE("No.", LRefSP)
                        ELSE
                            ERROR('Article introuvable pour la ligne %1', NoLigne);
                    END;

                // Affecation DEBUGGER la refSaisie
                IF LRefSaisie <> '' THEN
                    LSalesLine."Référence saisie" := LRefSaisie
                ELSE
                    IF LRefActive <> '' THEN
                        LSalesLine."Référence saisie" := LRefActive
                    ELSE
                        LSalesLine."Référence saisie" := LRefSP;

                // Qte
                LSalesLine.VALIDATE(Quantity, LQte);

                // Prix
                IF GetCell('F', NoLigne) THEN BEGIN
                    EVALUATE(LSalesLine."Unit Price", TempExcelBuf."Cell Value as Text");
                    LSalesLine.VALIDATE("Unit Price");
                    LSalesLine.VALIDATE("Discount1 %", 0);
                    LSalesLine.VALIDATE("Discount2 %", 0);
                END;
                LSalesLine.MODIFY(TRUE);

                IF GetCell('E', NoLigne) THEN BEGIN
                    CLEAR(LSalesCommentLine);
                    LSalesCommentLine.VALIDATE("Document Type", LSalesLine."Document Type");
                    LSalesCommentLine.VALIDATE("No.", LSalesLine."Document No.");
                    LSalesCommentLine.VALIDATE("Document Line No.", LSalesLine."Line No.");
                    LSalesCommentLine.VALIDATE("Line No.", 10000);
                    LSalesCommentLine.VALIDATE(Date, WORKDATE());
                    LSalesCommentLine.VALIDATE(Code, 'XLS');
                    LSalesCommentLine.VALIDATE(Comment, TempExcelBuf."Cell Value as Text");
                    LSalesCommentLine.VALIDATE("Commentaires Web", TRUE);
                    LSalesCommentLine.VALIDATE("Display Order", TRUE);
                    LSalesCommentLine.VALIDATE("Print Wharehouse Shipment", TRUE);
                    LSalesCommentLine.VALIDATE("Print Shipment", TRUE);
                    LSalesCommentLine.INSERT(TRUE);
                END;
            end;

            trigger OnPostDataItem();
            begin
                MESSAGE('Import terminé !');
            end;

            trigger OnPreDataItem();
            var
                LSalesLine: Record "Sales Line";
            begin

                // Pour être sur que l'entête est initialisée
                SalesHeader.GET(SalesHeader."Document Type", SalesHeader."No.");

                // No Ligne Cde
                LSalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
                LSalesLine.SETRANGE("Document No.", SalesHeader."No.");
                IF LSalesLine.FINDLAST() THEN NoLigneCde := LSalesLine."Line No.";

                // Lecture Excel
                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 1, TempExcelBuf."Row No." - 1);
                //On se positionne sur la 1ere ligne des données (entete=1)
                //MESSAGE(Integer.GETFILTERS);
                NbLigne := TempExcelBuf."Row No.";
                NoLigne := 1;
                Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');
            end;
        }
    }
    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin
        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        SalesHeader: Record "Sales Header";
        CuMultiRef: Codeunit "Gestion Multi-référence";
        FileName: Text[250];
        SheetName: Text[250];
        Fenetre: Dialog;
        NbLigne: Integer;
        NoLigne: Integer;
        NoLigneCde: Integer;
    //************************Non utiliser*******************************************************
    // local procedure ReadExcelSheet();
    // begin
    //     TempExcelBuf.OpenBook(FileName, SheetName);
    //     TempExcelBuf.ReadSheet();
    // end;
    //************************Non utiliser*******************************************************
    local procedure GetCell(_pRow: Text[5]; _pLigne: Integer): Boolean;
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.SETRANGE("Row No.", _pLigne);
        TempExcelBuf.SETRANGE(xlColID, _pRow);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure SetSalesOrders(_SalesHeader: Record "Sales Header");
    begin
        SalesHeader := _SalesHeader;
    end;
}

