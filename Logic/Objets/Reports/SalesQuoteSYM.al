report 50046 "Sales - Quote SYM" //204
{
    DefaultLayout = RDLC;
    RDLCLayout = './Objets/Reports/Layouts/Sales - Quote.rdlc';
    Caption = 'Order Confirmation';
    PreviewMode = Normal;
    ApplicationArea = All;
    UsageCategory = None;
    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            DataItemTableView = SORTING("Document Type", "No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeading = 'Sales Order';
            column(DocType_SalesHeader; "Document Type")
            {
            }
            column(No_SalesHeader; "No.")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }
            column(NotPrintRefSaisi; _NotPrintRefSaisi)
            {
            }
            column(refnondisposeul; _refnondisposeul)
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(PhoneNoCaption; PhoneNoCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(PaymentTermsCaption; PaymentTermsCaptionLbl)
            {
            }
            column(PaymentMethodCaption; PaymentMethodCaptionLbl)
            {
            }
            column(ShipmentMethodCaption; ShipmentMethodCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(OrderDateCaption; OrderDateCaptionLbl)
            {
            }
            column(AllowInvDiscCaption; AllowInvDiscCaptionLbl)
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(DocCaptCopyText; DocumentCaption())
                    {
                    }
                    column(CompanyInfoPicture; CompanyInfo.Picture)
                    {
                    }
                    column(CompanyInfoPictureAddress; CompanyInfo."Picture Address")
                    {
                    }
                    column(OrderConfirmCopyCaption; STRSUBSTNO(Text004Lbl, CopyText))
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhNo; CompanyInfo."Phone No.")
                    {
                        IncludeCaption = false;
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(Refnondisposeul_SalesLine; _refnondisposeul)
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoBankAccNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(BilltoCustNo_SalesHeader; "Sales Header"."Bill-to Customer No.")
                    {
                    }
                    column(DocDate_SalesHeader; FORMAT("Sales Header"."Document Date"))
                    {
                    }
                    column(OrderDate_SalesHeader; FORMAT("Sales Header"."Order Date"))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesHeader; "Sales Header"."VAT Registration No.")
                    {
                    }
                    column(ShptDate_SalesHeader; FORMAT("Sales Header"."Shipment Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(SalesPurchPersonCode; SalesPurchPerson.Code)
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(SalesOrderReference_SalesHeader; "Sales Header"."Your Reference")
                    {
                    }
                    column(DocExterne; "Sales Header"."External Document No.")
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(SellToAddr1; SellToAddr[1])
                    {
                    }
                    column(SellToAddr2; SellToAddr[2])
                    {
                    }
                    column(SellToAddr3; SellToAddr[3])
                    {
                    }
                    column(SellToAddr4; SellToAddr[4])
                    {
                    }
                    column(SellToAddr5; SellToAddr[5])
                    {
                    }
                    column(SellToAddr6; SellToAddr[6])
                    {
                    }
                    column(SellToAddr7; SellToAddr[7])
                    {
                    }
                    column(SellToAddr8; SellToAddr[8])
                    {
                    }
                    column(PricesInclVAT_SalesHeader; "Sales Header"."Prices Including VAT")
                    {
                    }
                    column(PageCaption; PageCaptionCapLbl)
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PrepmtPmntTermsDesc; PrepmtPaymentTerms.Description)
                    {
                    }
                    column(PmntTermsDesc; PaymentTerms.Description)
                    {
                    }
                    column(ShptMethodDesc; ShipmentMethod.Description)
                    {
                    }
                    column(PmntMethodDesc; PaymentMethod.Description)
                    {
                    }
                    column(ShippingAgentDesc; ShippingAgent.Name)
                    {
                    }
                    column(PricesInclVATYesNo_SalesHeader; FORMAT("Sales Header"."Prices Including VAT"))
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(GiroNoCaption; GiroNoCaptionLbl)
                    {
                    }
                    column(BankCaption; BankCaptionLbl)
                    {
                    }
                    column(AccountNoCaption; AccountNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(OrderNoCaption; OrderNoCaptionLbl)
                    {
                    }
                    column(HomePageCaption; HomePageCaptionCapLbl)
                    {
                    }
                    column(EmailCaption; EmailCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Bill-to Customer No."))
                    {
                    }
                    column(PricesInclVAT_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(NoClient; "Sales Header"."Sell-to Customer No.")
                    {
                    }
                    column(PhraseSignature; TEXT50004Lbl)
                    {
                    }
                    column(ShipToAddr8; ShipToAddr[8])
                    {
                    }
                    column(ShipToAddr7; ShipToAddr[7])
                    {
                    }
                    column(ShipToAddr6; ShipToAddr[6])
                    {
                    }
                    column(ShipToAddr5; ShipToAddr[5])
                    {
                    }
                    column(ShipToAddr4; ShipToAddr[4])
                    {
                    }
                    column(ShipToAddr3; ShipToAddr[3])
                    {
                    }
                    column(ShipToAddr2; ShipToAddr[2])
                    {
                    }
                    column(ShipToAddr1; ShipToAddr[1])
                    {
                    }
                    column(ShiptoAddrCaption; ShiptoAddrCaptionLbl)
                    {
                    }
                    column(BilltoAddrCaption; BilltoAddrCaptionLbl)
                    {
                    }
                    column(DateLivrDemandee; DateLivrDemandee)
                    {
                    }
                    column(Correspondant; Correspondant)
                    {
                    }
                    column(TextPaiement; TextPaiement)
                    {
                    }
                    column(TextPaiement2; TextPaiement2)
                    {
                    }
                    column(TxtEch1; TxtEch1)
                    {
                    }
                    column(TxtEch2; TxtEch2)
                    {
                    }
                    column(TxtEch3; TxtEch3)
                    {
                    }
                    column(ModeExpedition; "Sales Header"."Mode d'expédition")
                    {
                    }
                    column(SourceDocumentType_SalesHeader; "Sales Header"."Source Document Type")
                    {
                    }
                    dataitem(DimensionLoop1; Integer)
                    {
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(DimensionLoop1Number; Number)
                        {
                        }
                        column(HeaderDimCaption; HeaderDimCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        Var
                            ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                        begin
                            IF Number = 1 THEN BEGIN
                                IF NOT DimSetEntry1.FIND('-') THEN
                                    CurrReport.BREAK();
                            END ELSE
                                IF NOT Continue THEN
                                    CurrReport.BREAK();

                            CLEAR(DimText);
                            Continue := FALSE;
                            REPEAT
                                OldDimText := DimText;
                                IF DimText = '' THEN
                                    DimText := STRSUBSTNO(Param1Txt, DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                ELSE
                                    DimText :=
                                      STRSUBSTNO(
                                        ParamTxt, DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                    DimText := OldDimText;
                                    Continue := TRUE;
                                    EXIT;
                                END;
                            UNTIL DimSetEntry1.NEXT() = 0;
                        end;

                        trigger OnPreDataItem()
                        begin
                            IF NOT ShowInternalInfo THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem("Sales Comment Line"; "Sales Comment Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"),
                                       "No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            WHERE("Document Line No." = CONST(0));
                        column(Comment_SalesHeader; Comment)
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            IF "Sales Header"."Document Type" = "Sales Header"."Document Type"::Quote THEN
                                SETRANGE("Print Quote", TRUE)
                            ELSE
                                SETRANGE("Print Order", TRUE);
                        end;
                    }
                    dataitem("Sales Line"; "Sales Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Document Type"),
                                       "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                        trigger OnPreDataItem()
                        begin
                            CurrReport.BREAK();
                        end;
                    }
                    dataitem(RoundLoop; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(SalesLineAmt; TempSalesLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(Desc_SalesLine; "Sales Line".Description)
                        {
                        }
                        column(Desc2_SalesLine; "Sales Line"."Description 2")
                        {
                        }
                        column(NNCSalesLineLineAmt; NNCSalesLineLineAmt)
                        {
                        }
                        column(NNCSalesLineInvDiscAmt; NNCSalesLineInvDiscAmt)
                        {
                        }
                        column(NNCTotalLCY; NNCTotalLCY)
                        {
                        }
                        column(NNCTotalExclVAT; NNCTotalExclVAT)
                        {
                        }
                        column(NNCVATAmt; NNCVATAmt)
                        {
                        }
                        column(NNCTotalInclVAT; NNCTotalInclVAT)
                        {
                        }
                        column(NNCPmtDiscOnVAT; NNCPmtDiscOnVAT)
                        {
                        }
                        column(NNCTotalInclVAT2; NNCTotalInclVAT2)
                        {
                        }
                        column(NNCVATAmt2; NNCVATAmt2)
                        {
                        }
                        column(NNCTotalExclVAT2; NNCTotalExclVAT2)
                        {
                        }
                        column(VATBaseDisc_SalesHeader; "Sales Header"."VAT Base Discount %")
                        {
                        }
                        column(DisplayAssemblyInfo; DisplayAssemblyInformation)
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(No2_SalesLine; ItemNo)
                        {
                        }
                        column(Qty_SalesLine; "Sales Line".Quantity)
                        {
                        }
                        column(QtyToShip_SalesLine; "Sales Line"."Qty. to Ship")
                        {
                        }
                        column(UOM_SalesLine; "Sales Line"."Unit of Measure")
                        {
                        }
                        column(UnitPrice_SalesLine; "Sales Line"."Unit Price")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 2;
                            IncludeCaption = false;
                        }
                        column(Reference1_SalesLine; _Reference1)
                        {
                        }
                        column(Reference2_SalesLine; _Reference2)
                        {
                        }
                        column(Reference3_SalesLine; _Reference3)
                        {
                        }
                        column(LineDisc_SalesLine; _Remise)
                        {
                        }
                        column(CodeSubstitution_SalesLine; CodeSubstitution)
                        {
                        }
                        column(ReferenceSaisie_SalesLine; ReferenceSaisie)
                        {
                        }
                        column(LineDiscExists_SalesLine; "Sales Line".LineDiscExists())
                        {
                        }
                        column(LineAmt_SalesLine; "Sales Line"."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(AllowInvDisc_SalesLine; "Sales Line"."Allow Invoice Disc.")
                        {
                        }
                        column(VATIdentifier_SalesLine; "Sales Line"."VAT Identifier")
                        {
                        }
                        column(NetUnitPrice_SalesLine; "Sales Line"."Net Unit Price")
                        {
                        }
                        column(Type_SalesLine; FORMAT("Sales Line".Type))
                        {
                        }
                        column(No_SalesLine; _NoLigne)
                        {
                        }
                        column(AllowInvDiscountYesNo_SalesLine; FORMAT("Sales Line"."Allow Invoice Disc."))
                        {
                        }
                        column(AsmInfoExistsForLine; AsmInfoExistsForLine)
                        {
                        }
                        column(SalesLineInvDiscAmt; TempSalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(SalsLinAmtExclLineDiscAmt; TempSalesLine."Line Amount" - TempSalesLine."Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(VATAmtLineVATAmtText3; TempVATAmountLine.VATAmountText())
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(VATAmount; VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(SalesLineAmtExclLineDisc; TempSalesLine."Line Amount" - TempSalesLine."Inv. Discount Amount" + VATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATDiscountAmount; VATDiscountAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseAmount; VATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmountInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(DiscountPercentCaption; DiscountPercentCaptionLbl)
                        {
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(PaymentDiscountVATCaption; PaymentDiscountVATCaptionLbl)
                        {
                        }
                        column(Desc_SalesLineCaption; "Sales Line".FIELDCAPTION(Description))
                        {
                        }
                        column(No2_SalesLineCaption; "Sales Line".FIELDCAPTION("No."))
                        {
                        }
                        column(Qty_SalesLineCaption; "Sales Line".FIELDCAPTION(Quantity))
                        {
                        }
                        column(UOM_SalesLineCaption; "Sales Line".FIELDCAPTION("Unit of Measure"))
                        {
                        }
                        column(VATIdentifier_SalesLineCaption; "Sales Line".FIELDCAPTION("VAT Identifier"))
                        {
                        }
                        column(PromDelDate_SalesLineCaption; "Sales Line".FIELDCAPTION("Promised Delivery Date"))
                        {
                        }
                        column(PromDelDate_SalesLine; FORMAT("Sales Line"."Promised Delivery Date"))
                        {
                        }
                        column(Delai_SalesLine; _delai)
                        {
                        }
                        column(QteReste_SalesLine; _qtereste)
                        {
                        }
                        column("DateDisponibilité"; _DateDisponibilité)
                        {
                        }
                        column(ReferenceActive; ReferenceActive)
                        {
                        }
                        dataitem(SalesCommentLine; "Sales Comment Line")
                        {
                            DataItemLink = "No." = FIELD("Document No."),
                                           "Document Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Line";
                            DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                                WHERE("Document Line No." = FILTER(<> 0));
                            column(CommentLine_SalesCommentLine; Comment)
                            {
                            }

                            trigger OnPreDataItem()
                            begin
                                SETRANGE("Document Type", "Sales Header"."Document Type");
                                IF "Sales Header"."Document Type" = "Sales Header"."Document Type"::Quote THEN
                                    SETRANGE("Print Quote", TRUE)
                                ELSE
                                    SETRANGE("Print Order", TRUE);
                            end;
                        }
                        dataitem(DimensionLoop2; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = FILTER(1 ..));
                            column(DimText2; DimText)
                            {
                            }
                            column(LineDimCaption; LineDimCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord()

                            var
                                ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                            begin
                                IF Number = 1 THEN BEGIN
                                    IF NOT DimSetEntry2.FINDSET() THEN
                                        CurrReport.BREAK();
                                END ELSE
                                    IF NOT Continue THEN
                                        CurrReport.BREAK();

                                CLEAR(DimText);
                                Continue := FALSE;
                                REPEAT
                                    OldDimText := DimText;
                                    IF DimText = '' THEN
                                        DimText := STRSUBSTNO(Param1Txt, DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    ELSE
                                        DimText :=
                                          STRSUBSTNO(
                                            ParamTxt, DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                        DimText := OldDimText;
                                        Continue := TRUE;
                                        EXIT;
                                    END;
                                UNTIL DimSetEntry2.NEXT() = 0;
                            end;

                            trigger OnPreDataItem()
                            begin
                                IF NOT ShowInternalInfo THEN
                                    CurrReport.BREAK();

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(AsmLoop; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(AsmLineType; AsmLine.Type)
                            {
                            }
                            column(AsmLineNo; BlanksForIndent() + AsmLine."No." + 'toto')
                            {
                            }
                            column(AsmLineDescription; BlanksForIndent() + AsmLine.Description)
                            {
                            }
                            column(AsmLineQuantity; AsmLine.Quantity)
                            {
                            }
                            column(AsmLineUOMText; GetUnitOfMeasureDescr(AsmLine."Unit of Measure Code"))
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin
                                IF Number = 1 THEN
                                    AsmLine.FINDSET()
                                ELSE
                                    AsmLine.NEXT();
                            end;

                            trigger OnPreDataItem()
                            begin
                                IF NOT DisplayAssemblyInformation THEN
                                    CurrReport.BREAK();
                                IF NOT AsmInfoExistsForLine THEN
                                    CurrReport.BREAK();
                                AsmLine.SETRANGE("Document Type", AsmHeader."Document Type");
                                AsmLine.SETRANGE("Document No.", AsmHeader."No.");
                                SETRANGE(Number, 1, AsmLine.COUNT);
                            end;
                        }

                        trigger OnAfterGetRecord()
                        var
                            LItem: Record Item;
                        begin
                            IF Number = 1 THEN
                                TempSalesLine.FIND('-')
                            ELSE
                                TempSalesLine.NEXT();
                            "Sales Line" := TempSalesLine;


                            //LM le 13-05-2013
                            IF _refnondisposeul AND (TempSalesLine.Quantity - TempSalesLine."Qty. to Ship" - TempSalesLine."Quantity Shipped" = 0) THEN CurrReport.SKIP();

                            IF DisplayAssemblyInformation THEN
                                AsmInfoExistsForLine := TempSalesLine.AsmToOrderExists(AsmHeader);

                            // ESKVN1.0 => Gestion de l'impression des commentaires
                            IF TempSalesLine.Type = TempSalesLine.Type::" " THEN
                                CASE TempSalesLine."Document Type" OF
                                    TempSalesLine."Document Type"::Quote:
                                        IF NOT TempSalesLine."Print Quote" THEN
                                            CurrReport.SKIP();
                                    TempSalesLine."Document Type"::Order:
                                        IF NOT TempSalesLine."Print Order" THEN
                                            CurrReport.SKIP();
                                END;

                            // FIN ESKVN1.0

                            IF NOT "Sales Header"."Prices Including VAT" AND
                               (TempSalesLine."VAT Calculation Type" = TempSalesLine."VAT Calculation Type"::"Full VAT")
                            THEN
                                TempSalesLine."Line Amount" := 0;

                            IF (TempSalesLine.Type = TempSalesLine.Type::"G/L Account") AND (NOT ShowInternalInfo) THEN
                                "Sales Line"."No." := '';

                            ReferenceActive := GestionMulti.RechercheRefActive("Sales Line"."No.");





                            ItemNo := "Sales Line"."No.";
                            IF (STRLEN(ItemNo) = 7) THEN
                                ItemNo := format(COPYSTR(ItemNo, 3, STRLEN(ItemNo) - 2));

                            CLEAR(PrixTaille);



                            NNCSalesLineLineAmt += TempSalesLine."Line Amount";
                            NNCSalesLineInvDiscAmt += TempSalesLine."Inv. Discount Amount";

                            NNCTotalLCY := NNCSalesLineLineAmt - NNCSalesLineInvDiscAmt;

                            NNCTotalExclVAT := NNCTotalLCY;
                            NNCVATAmt := VATAmount;
                            NNCTotalInclVAT := NNCTotalLCY - NNCVATAmt;

                            NNCPmtDiscOnVAT := -VATDiscountAmount;

                            NNCTotalInclVAT2 := TotalAmountInclVAT;

                            NNCVATAmt2 := VATAmount;
                            NNCTotalExclVAT2 := VATBaseAmount;



                            //// GR MIG2015 - Reprise 2009

                            //LM le 13-05-2013
                            IF _refnondisposeul AND (TempSalesLine.Quantity - TempSalesLine."Qty. to Ship" - TempSalesLine."Quantity Shipped" = 0) THEN CurrReport.SKIP();


                            //Calcul du total des remises de type entête
                            PourcentageRemiseEntete := '';
                            IF TempSalesLine."Internal Line Type" = TempSalesLine."Internal Line Type"::"Discount Header" THEN BEGIN
                                RemiseEnteteTotal := TempSalesLine."Line Amount";
                                PourcentageRemiseEntete := FORMAT(ABS(ROUND(TempSalesLine."Net Unit Price" * 100, 0.01))) + ' %';
                            END;


                            _Remise := '';
                            _Reference1 := ' ';
                            _Reference2 := ' ';

                            NoLigne += 1;

                            //_NoLigne := NoLigne;
                            // LM Le 15-02-2013 => On prend le no de la cde d'origine
                            IF NOT EVALUATE(_NoLigne, FORMAT("Sales Line"."Line No." / 10000)) THEN
                                _NoLigne := 0;


                            IF "Sales Line".Type = "Sales Line".Type::Item THEN BEGIN
                                //IF "Sales Line"."Zone Code" = '3PU' THEN
                                //BEGIN
                                // MC Le 07-11-2011 => Possibilité de ne pas imprimer la référence saisie
                                IF NOT _NotPrintRefSaisi THEN
                                    // FIN MC Le 07-11-2011
                                    _Reference1 := GestionMulti.RechercheRefActive("Sales Line"."No.");
                                _Reference2 := "Sales Line"."No.";
                                _Reference2 := format("Sales Line"."Référence saisie");// AD Le 18--11-2015 => MIG2015
                                IF _Reference1 = _Reference2 THEN _Reference2 := '';
                                //END
                                //ELSE
                                //BEGIN
                                //_Reference2 := GestionMulti.RechercheRefActive("Sales Line"."No.");
                                //_Reference1 := "Sales Line"."No.";
                                //END;
                            END;


                            IF _refnondisposeul THEN BEGIN
                                _qtereste := FORMAT(TempSalesLine.Quantity - TempSalesLine."Qty. to Ship" - TempSalesLine."Quantity Shipped");
                                _delai := FORMAT(TempSalesLine."Requested Delivery Date");
                            END
                            ELSE BEGIN
                                _qtereste := '';
                                _delai := '';
                            END;


                            _Remise := '';


                            IF ("Sales Line"."Discount1 %" <> 0) OR ("Sales Line"."Discount2 %" <> 0) THEN BEGIN
                                IF ("Sales Line"."Discount1 %" <> 0) THEN
                                    _Remise := FORMAT("Sales Line"."Discount1 %") + '%';


                                IF ("Sales Line"."Discount2 %" <> 0) THEN BEGIN
                                    IF ("Sales Line"."Discount1 %" <> 0) THEN
                                        _Remise += '+';
                                    _Remise += FORMAT("Sales Line"."Discount2 %") + '%';
                                END;

                            END;

                            // MC Le 07-11-2011 => Possibilité de ne pas imprimer la référence saisie
                            IF NOT _NotPrintRefSaisi THEN
                                // FIN MC Le 07-11-2011
                                _Reference3 := "Sales Line"."Référence saisie";

                            _DateDisponibilité := '';
                            IF "Sales Line"."Promised Delivery Date" <> 0D THEN
                                _DateDisponibilité := STRSUBSTNO(ESK005Lbl, "Sales Line"."Promised Delivery Date");

                            IF RecCustomer.GET("Sales Header"."Sell-to Customer No.") THEN
                                IF RecCustomer."Masquer remise sur édition" THEN BEGIN
                                    _Remise := '';
                                    _PuBrut := '';
                                END;
                            //Calcul de la remise facture
                            CalcRemiseFacture += "Sales Line"."Inv. Discount Amount";

                            //Calcul du total brut des lignes (hors remise de type entête)
                            IF TempSalesLine."Internal Line Type" <> TempSalesLine."Internal Line Type"::"Discount Header" THEN
                                TotalBrutLigne += TempSalesLine."Line Amount";


                            IF ((TempSalesLine."Originally Ordered No." <> '')
                              // MCO Le 31-03-2015 => Régie
                              AND (NOT _NotPrintRefSaisi))
                             // FIN MCO Le 31-03-2015
                             THEN BEGIN
                                LItem.GET(TempSalesLine."Originally Ordered No.");
                                CodeSubstitution := STRSUBSTNO(ESK010Lbl, LItem."No. 2");
                            END
                            ELSE
                                CodeSubstitution := '';


                            IF ((TempSalesLine.Type = TempSalesLine.Type::Item) AND (TempSalesLine."Recherche référence" <> TempSalesLine."Référence saisie") AND (TempSalesLine."Référence saisie" <> '')) THEN
                                ReferenceSaisie := STRSUBSTNO(ESK007Lbl, TempSalesLine."Référence saisie")
                            ELSE
                                ReferenceSaisie := '';
                        end;

                        trigger OnPostDataItem()
                        begin
                            TempSalesLine.DELETEALL();
                        end;

                        trigger OnPreDataItem()
                        begin
                            MoreLines := TempSalesLine.FIND('+');
                            WHILE MoreLines AND (TempSalesLine.Description = '') AND (TempSalesLine."Description 2" = '') AND
                                  (TempSalesLine."No." = '') AND (TempSalesLine.Quantity = 0) AND
                                  (TempSalesLine.Amount = 0)
                            DO
                                MoreLines := TempSalesLine.NEXT(-1) <> 0;
                            IF NOT MoreLines THEN
                                CurrReport.BREAK();
                            TempSalesLine.SETRANGE("Line No.", 0, TempSalesLine."Line No.");
                            SETRANGE(Number, 1, TempSalesLine.COUNT);
                        end;
                    }
                    dataitem(LigneBlanche; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(LigneBlancheNumber; LigneBlanche.Number)
                        {
                        }

                        trigger OnPostDataItem()
                        begin
                            NombreDeLigneImprimer := 0;
                        end;

                        trigger OnPreDataItem()
                        var
                            CommentLine: Record "Sales Comment Line";
                            NombreDeSaut: Integer;
                        begin

                            NombredelignepossibleparPagePleine := 22;
                            NombredelignepossibleparPage := 22;

                            // AD Le 06-03-2014
                            NombredelignepossibleparPage := 22;
                            NombredelignepossibleparPagePleine := 30;


                            NombreDeLigneImprimer := "Sales Line".COUNT + TempBufferPaiment.COUNT;
                            NombreDeLigneImprimer += 3; //ligne de total;
                            IF TempBufferPaiment.COUNT > 0 THEN
                                NombreDeLigneImprimer += 5;

                            IF ShowShippingAddr THEN
                                //NombredelignepossibleparPage-=8 ;
                                NombreDeLigneImprimer += 8;

                            CommentLine.SETRANGE("Document Type", "Sales Header"."Document Type");
                            CommentLine.SETRANGE("No.", "Sales Header"."No.");
                            CommentLine.SETRANGE("Print Order", TRUE);
                            NombredelignepossibleparPage := NombredelignepossibleparPage - CommentLine.COUNT;
                            //IF UPPERCASE(USERID) =UPPERCASE('capalliance\eskape') THEN
                            //MESSAGE('NombredelignepossibleparPage %1 _ NombreDeLigneImprimer %2',NombredelignepossibleparPage,NombreDeLigneImprimer);

                            NombreDeSaut := NombredelignepossibleparPage - NombreDeLigneImprimer;
                            IF NombreDeSaut < 0 THEN
                                REPEAT
                                    NombreDeLigneImprimer := NombreDeLigneImprimer - NombredelignepossibleparPagePleine;
                                    NombreDeSaut := NombredelignepossibleparPage - NombreDeLigneImprimer;
                                UNTIL NombreDeSaut >= 0;

                            //IF UPPERCASE(USERID) =UPPERCASE('capalliance\eskape') THEN

                            //LIGNE  BLANCHE RETIRER POUR LE MOMENT
                            NombreDeSaut := 0;

                            SETRANGE(Number, 1, NombreDeSaut);
                        end;
                    }
                    dataitem(LigneTotal; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(LigneTotalNum; FORMAT(LigneTotal.Number))
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            LigneTotal.SETRANGE(Number, 1, 1)
                        end;
                    }
                    dataitem(Commentaire; "Sales Comment Line")
                    {
                        DataItemLink = "No." = FIELD("No.");
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            WHERE("Document Type" = CONST(Order),
                                                  "Document Line No." = FILTER(= 0));
                        column(Comment_Commentaire; Commentaire.Comment)
                        {
                        }
                    }
                    dataitem(SautLigneCommentaire; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            ORDER(Ascending)
                                            WHERE(Number = FILTER(1));
                        column(SautLigneCommentaireNumber; SautLigneCommentaire.Number)
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            IF "Sales Comment Line".COUNT = 0 THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem(VATCounter; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmtLineTTCAmt; TempVATAmountLine."VAT Base" + TempVATAmountLine."VAT Amount")
                        {
                        }
                        column(VATAmountLineVATBase; TempVATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmt; TempVATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineLineAmt; TempVATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; TempVATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscAmt; TempVATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercentage; TempVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; TempVATAmountLine."VAT Identifier")
                        {
                        }
                        column(InvDiscBaseAmtCaption; InvDiscBaseAmtCaptionLbl)
                        {
                        }
                        column(VATIdentifierCaption; VATIdentifierCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempVATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem()
                        begin
                            IF VATAmount = 0 THEN
                                CurrReport.BREAK();
                            SETRANGE(Number, 1, TempVATAmountLine.COUNT);
                        end;
                    }
                    dataitem(VATCounterLCY; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALVATTTCLCY; VALVATBaseLCY + VALVATAmountLCY)
                        {
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATAmountLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATPercentage2; TempVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier2; TempVATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempVATAmountLine.GetLine(Number);
                            VALVATBaseLCY :=
                              TempVATAmountLine.GetBaseLCY(
                                "Sales Header"."Posting Date", "Sales Header"."Currency Code", "Sales Header"."Currency Factor");
                            VALVATAmountLCY :=
                              TempVATAmountLine.GetAmountLCY(
                                "Sales Header"."Posting Date", "Sales Header"."Currency Code", "Sales Header"."Currency Factor");
                        end;

                        trigger OnPreDataItem()
                        begin
                            IF (NOT GLSetup."Print VAT specification in LCY") OR
                               ("Sales Header"."Currency Code" = '') OR
                               (TempVATAmountLine.GetTotalVATAmount() = 0)
                            THEN
                                CurrReport.BREAK();

                            SETRANGE(Number, 1, TempVATAmountLine.COUNT);

                            IF GLSetup."LCY Code" = '' THEN
                                VALSpecLCYHeader := Text007Lbl + Text008Lbl
                            ELSE
                                VALSpecLCYHeader := Text007Lbl + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Header"."Posting Date", "Sales Header"."Currency Code", 1);
                            VALExchRate := STRSUBSTNO(Text009Lbl, CurrExchRate."Relational Exch. Rate Amount", CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(PaiementMultiEch; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(PaiementDateEch; FORMAT(TempBufferPaiment."Due Date"))
                        {
                        }
                        column(PaiementPaiement; TempBufferPaiment.Amount)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            ////TempBufferPaiment.GetLine(Number);
                        end;

                        trigger OnPreDataItem()
                        begin
                            SETRANGE(Number, 1, TempBufferPaiment.COUNT);
                        end;
                    }
                    dataitem(CommentairesPieds; "Sales Comment Line")
                    {
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            ORDER(Ascending);
                        column(Comment_Pied; Comment)
                        {
                        }

                        trigger OnPreDataItem()
                        begin


                            CASE "Sales Header"."Document Type" OF
                                "Sales Header"."Document Type"::Order:
                                    CommentairesPieds.SETRANGE("Print Order", TRUE);
                                "Sales Header"."Document Type"::Quote:
                                    CommentairesPieds.SETRANGE("Print Quote", TRUE);
                            END;
                            CommentairesPieds.SETRANGE(Date, 0D, WORKDATE());
                            CommentairesPieds.SETFILTER("End Date", '>%1', WORKDATE());
                        end;
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesHeader; "Sales Header"."Sell-to Customer No.")
                        {
                        }
                        column(SelltoCustNo_SalesHeaderCaption; "Sales Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            IF NOT ShowShippingAddr THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem(PrepmtLoop; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = FILTER(1 ..));
                        column(PrepmtLineAmount; PrepmtLineAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtInvBufDesc; TempPrepmtInvBuf.Description)
                        {
                        }
                        column(PrepmtInvBufGLAccNo; TempPrepmtInvBuf."G/L Account No.")
                        {
                        }
                        column(TotalExclVATText2; TotalExclVATText)
                        {
                        }
                        column(PrepmtVATAmtLineVATAmtTxt; TempPrepmtVATAmountLine.VATAmountText())
                        {
                        }
                        column(TotalInclVATText2; TotalInclVATText)
                        {
                        }
                        column(PrepmtInvAmount; TempPrepmtInvBuf.Amount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmount; PrepmtVATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtInvAmtInclVATAmt; TempPrepmtInvBuf.Amount + PrepmtVATAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmtText2; TempVATAmountLine.VATAmountText())
                        {
                        }
                        column(PrepmtTotalAmountInclVAT; PrepmtTotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATBaseAmount; PrepmtVATBaseAmount)
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtLoopNumber; Number)
                        {
                        }
                        column(DescriptionCaption; DescriptionCaptionLbl)
                        {
                        }
                        column(GLAccountNoCaption; GLAccountNoCaptionLbl)
                        {
                        }
                        column(PrepaymentSpecCaption; PrepaymentSpecCaptionLbl)
                        {
                        }
                        dataitem(PrepmtDimLoop; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = FILTER(1 ..));
                            column(DimText3; DimText)
                            {
                            }

                            trigger OnAfterGetRecord()
                            var
                                ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                            begin
                                IF Number = 1 THEN BEGIN
                                    IF NOT TempPrepmtDimSetEntry.FIND('-') THEN
                                        CurrReport.BREAK();
                                END ELSE
                                    IF NOT Continue THEN
                                        CurrReport.BREAK();

                                CLEAR(DimText);
                                Continue := FALSE;
                                REPEAT
                                    OldDimText := DimText;
                                    IF DimText = '' THEN
                                        DimText :=
                                          STRSUBSTNO(Param1Txt, TempPrepmtDimSetEntry."Dimension Code", TempPrepmtDimSetEntry."Dimension Value Code")
                                    ELSE
                                        DimText :=
                                          STRSUBSTNO(
                                            ParamTxt, DimText,
                                            TempPrepmtDimSetEntry."Dimension Code", TempPrepmtDimSetEntry."Dimension Value Code");
                                    IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                        DimText := OldDimText;
                                        Continue := TRUE;
                                        EXIT;
                                    END;
                                UNTIL TempPrepmtDimSetEntry.NEXT() = 0;
                            end;
                        }

                        trigger OnAfterGetRecord()
                        begin
                            IF Number = 1 THEN BEGIN
                                IF NOT TempPrepmtInvBuf.FIND('-') THEN
                                    CurrReport.BREAK();
                            END ELSE
                                IF TempPrepmtInvBuf.NEXT() = 0 THEN
                                    CurrReport.BREAK();

                            IF ShowInternalInfo THEN
                                DimMgt.GetDimensionSet(TempPrepmtDimSetEntry, TempPrepmtInvBuf."Dimension Set ID");

                            IF "Sales Header"."Prices Including VAT" THEN
                                PrepmtLineAmount := TempPrepmtInvBuf."Amount Incl. VAT"
                            ELSE
                                PrepmtLineAmount := TempPrepmtInvBuf.Amount;
                        end;

                        trigger OnPreDataItem()
                        begin
                        end;
                    }
                    dataitem(PrepmtVATCounter; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(PrepmtVATAmtLineVATAmt; TempPrepmtVATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineVATBase; TempPrepmtVATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineLineAmt; TempPrepmtVATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(PrepmtVATAmtLineVATPerc; TempPrepmtVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(PrepmtVATAmtLineVATIdent; TempPrepmtVATAmountLine."VAT Identifier")
                        {
                        }
                        column(PrepmtVATCounterNumber; Number)
                        {
                        }
                        column(PrepaymentVATAmtSpecCap; PrepaymentVATAmtSpecCapLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempPrepmtVATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem()
                        begin
                            SETRANGE(Number, 1, TempPrepmtVATAmountLine.COUNT);
                        end;
                    }
                    dataitem(PrepmtTotal; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(PrepmtPmtTermsDesc; PrepmtPaymentTerms.Description)
                        {
                        }
                        column(PrepmtPmtTermsDescCaption; PrepmtPmtTermsDescCaptionLbl)
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            IF NOT TempPrepmtInvBuf.FIND('-') THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem(TexteCentrale; "Comment Line")
                    {
                        DataItemLinkReference = "Sales Header";
                        DataItemTableView = SORTING("Table Name", "No.", "Line No.")
                                            WHERE("Table Name" = CONST("VAT Business Posting Group"));

                        trigger OnPreDataItem()
                        begin
                            CLEAR(RecCustomer);
                            ////IF RecCustomer.GET("Sales Header"."Sell-to Customer No.") THEN BEGIN
                            ////  SETRANGE("No.", RecCustomer."Centrale Code");
                            ////END ELSE BEGIN
                            CurrReport.BREAK();
                            ////END;
                        end;
                    }
                }

                trigger OnAfterGetRecord()
                var
                    TempPrepmtSalesLine: Record "Sales Line" temporary;
                    TempSalesLine: Record "Sales Line" temporary;
                    SalesPost: Codeunit "Sales-Post";
                begin
                    CLEAR(TempSalesLine);
                    CLEAR(SalesPost);
                    TempVATAmountLine.DELETEALL();
                    TempSalesLine.DELETEALL();
                    SalesPost.GetSalesLines("Sales Header", TempSalesLine, 0);
                    // AD Le 27-09-2016
                    IF _refnondisposeul THEN
                        TempSalesLine.CalcVATAmountLines(3, "Sales Header", TempSalesLine, TempVATAmountLine)
                    ELSE
                        // FIN AD Le 27-09-2016
                        TempSalesLine.CalcVATAmountLines(0, "Sales Header", TempSalesLine, TempVATAmountLine);
                    TempSalesLine.UpdateVATOnLines(0, "Sales Header", TempSalesLine, TempVATAmountLine);
                    VATAmount := TempVATAmountLine.GetTotalVATAmount();
                    VATBaseAmount := TempVATAmountLine.GetTotalVATBase();
                    VATDiscountAmount :=
                      TempVATAmountLine.GetTotalVATDiscount("Sales Header"."Currency Code", "Sales Header"."Prices Including VAT");
                    TotalAmountInclVAT := TempVATAmountLine.GetTotalAmountInclVAT();

                    TempPrepmtInvBuf.DELETEALL();
                    SalesPostPrepmt.GetSalesLines("Sales Header", 0, TempPrepmtSalesLine);

                    IF NOT TempPrepmtSalesLine.ISEMPTY THEN BEGIN
                        SalesPostPrepmt.GetSalesLinesToDeduct("Sales Header", TempSalesLine);
                        IF NOT TempSalesLine.ISEMPTY THEN
                            SalesPostPrepmt.CalcVATAmountLines("Sales Header", TempSalesLine, TempPrepmtVATAmountLineDeduct, 1);
                    END;
                    SalesPostPrepmt.CalcVATAmountLines("Sales Header", TempPrepmtSalesLine, TempPrepmtVATAmountLine, 0);
                    IF TempPrepmtVATAmountLine.FINDSET() THEN
                        REPEAT
                            TempPrepmtVATAmountLineDeduct := TempPrepmtVATAmountLine;
                            IF TempPrepmtVATAmountLineDeduct.FIND() THEN BEGIN
                                TempPrepmtVATAmountLine."VAT Base" := TempPrepmtVATAmountLine."VAT Base" - TempPrepmtVATAmountLineDeduct."VAT Base";
                                TempPrepmtVATAmountLine."VAT Amount" := TempPrepmtVATAmountLine."VAT Amount" - TempPrepmtVATAmountLineDeduct."VAT Amount";
                                TempPrepmtVATAmountLine."Amount Including VAT" := TempPrepmtVATAmountLine."Amount Including VAT" -
                                  TempPrepmtVATAmountLineDeduct."Amount Including VAT";
                                TempPrepmtVATAmountLine."Line Amount" := TempPrepmtVATAmountLine."Line Amount" - TempPrepmtVATAmountLineDeduct."Line Amount";
                                TempPrepmtVATAmountLine."Inv. Disc. Base Amount" := TempPrepmtVATAmountLine."Inv. Disc. Base Amount" -
                                  TempPrepmtVATAmountLineDeduct."Inv. Disc. Base Amount";
                                TempPrepmtVATAmountLine."Invoice Discount Amount" := TempPrepmtVATAmountLine."Invoice Discount Amount" -
                                  TempPrepmtVATAmountLineDeduct."Invoice Discount Amount";
                                TempPrepmtVATAmountLine."Calculated VAT Amount" := TempPrepmtVATAmountLine."Calculated VAT Amount" -
                                  TempPrepmtVATAmountLineDeduct."Calculated VAT Amount";
                                TempPrepmtVATAmountLine.MODIFY();
                            END;
                        UNTIL TempPrepmtVATAmountLine.NEXT() = 0;

                    SalesPostPrepmt.UpdateVATOnLines("Sales Header", TempPrepmtSalesLine, TempPrepmtVATAmountLine, 0);
                    SalesPostPrepmt.BuildInvLineBuffer("Sales Header", TempPrepmtSalesLine, 0, TempPrepmtInvBuf);
                    PrepmtVATAmount := TempPrepmtVATAmountLine.GetTotalVATAmount();
                    PrepmtVATBaseAmount := TempPrepmtVATAmountLine.GetTotalVATBase();
                    PrepmtTotalAmountInclVAT := TempPrepmtVATAmountLine.GetTotalAmountInclVAT();

                    IF Number > 1 THEN BEGIN
                        CopyText := Text003Lbl;
                        OutputNo += 1;
                    END;

                    NNCTotalLCY := 0;
                    NNCTotalExclVAT := 0;
                    NNCVATAmt := 0;
                    NNCTotalInclVAT := 0;
                    NNCPmtDiscOnVAT := 0;
                    NNCTotalInclVAT2 := 0;
                    NNCVATAmt2 := 0;
                    NNCTotalExclVAT2 := 0;
                    NNCSalesLineLineAmt := 0;
                    NNCSalesLineInvDiscAmt := 0;
                end;

                trigger OnPostDataItem()
                begin
                    //IF Print THEN
                    //  SalesCountPrinted.RUN("Sales Header");
                end;

                trigger OnPreDataItem()
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord()
            begin

                ReportLineNumber += 1;
                IF ReportLineNumber = 2 THEN BEGIN
                    CLEAR(CompanyInfo);
                    CompanyInfo.GET();
                END;


                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");


                // AD Le 28-10-2009 => Pour que tous les contrôle soient fait
                IF "Sales Header".Status = "Sales Header".Status::Open THEN
                    ERROR(Text50006Lbl);


                // FIN AD Le 28-10-2009


                IF RespCenter.GET("Responsibility Center") THEN BEGIN
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                END ELSE
                    FormatAddr.Company(CompanyAddr, CompanyInfo);

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF "Salesperson Code" = '' THEN BEGIN
                    CLEAR(SalesPurchPerson);
                    SalesPersonText := '';
                END ELSE BEGIN
                    SalesPurchPerson.GET("Salesperson Code");
                    SalesPersonText := Text000Lbl;
                END;
                IF "Your Reference" = '' THEN
                    ReferenceText := ''
                ELSE
                    Evaluate(ReferenceText, FIELDCAPTION("Your Reference"));
                IF "VAT Registration No." = '' THEN
                    Evaluate(VATNoText, FIELDCAPTION("VAT Registration No."))//VATNoText := ''
                ELSE
                    Evaluate(VATNoText, FIELDCAPTION("VAT Registration No."));
                IF "Currency Code" = '' THEN BEGIN
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001Lbl, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002Lbl, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006Lbl, GLSetup."LCY Code");
                END ELSE BEGIN
                    TotalText := STRSUBSTNO(Text001Lbl, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002Lbl, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006Lbl, "Currency Code");
                END;
                FormatAddr.SalesHeaderBillTo(CustAddr, "Sales Header");
                FormatAddr.SalesHeaderSellTo(SellToAddr, "Sales Header");

                IF "Payment Method Code" = '' THEN
                    PaymentMethod.INIT()
                ELSE
                    PaymentMethod.GET("Payment Method Code");

                IF "Payment Terms Code" = '' THEN
                    PaymentTerms.INIT()
                ELSE BEGIN
                    PaymentTerms.GET("Payment Terms Code");
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                END;
                IF "Prepmt. Payment Terms Code" = '' THEN
                    PrepmtPaymentTerms.INIT()
                ELSE BEGIN
                    PrepmtPaymentTerms.GET("Prepmt. Payment Terms Code");
                    PrepmtPaymentTerms.TranslateDescription(PrepmtPaymentTerms, "Language Code");
                END;
                IF "Prepmt. Payment Terms Code" = '' THEN
                    PrepmtPaymentTerms.INIT()
                ELSE BEGIN
                    PrepmtPaymentTerms.GET("Prepmt. Payment Terms Code");
                    PrepmtPaymentTerms.TranslateDescription(PrepmtPaymentTerms, "Language Code");
                END;
                IF "Shipment Method Code" = '' THEN
                    ShipmentMethod.INIT()
                ELSE BEGIN
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                END;
                IF "Shipping Agent Code" = '' THEN
                    ShippingAgent.INIT()
                ELSE
                    ShippingAgent.GET("Shipping Agent Code");
                ///ShippingAgent.TranslateDescription(ShippingAgent,"Language Code");

                FormatAddr.SalesHeaderShipTo(ShipToAddr, ShipToAddr, "Sales Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                    IF ShipToAddr[i] <> CustAddr[i] THEN
                        ShowShippingAddr := TRUE;
                IF Print THEN BEGIN
                    IF ArchiveDocument THEN
                        ArchiveManagement.StoreSalesDocument("Sales Header", LogInteraction);
                    IF LogInteraction THEN
                        CALCFIELDS("No. of Archived Versions");
                    /*
                    IF "Bill-to Contact No." <> '' THEN
                      SegManagement.LogDocument(
                        3,"No.","Doc. No. Occurrence",
                        "No. of Archived Versions",DATABASE::Contact,"Bill-to Contact No."
                        ,"Salesperson Code","Campaign No.","Posting Description","Opportunity No.")
                    ELSE
                      SegManagement.LogDocument(
                        3,"No.","Doc. No. Occurrence",
                        "No. of Archived Versions",DATABASE::Customer,"Bill-to Customer No.",
                        "Salesperson Code","Campaign No.","Posting Description","Opportunity No.");
                    */
                END;

                //GESTION DU CORRESPONDANT (paramètres utilisateurs)
                Correspondant := ReportUtils.GetCorrespondantName("Sales Header"."Order Create User", "Sales Header".Responsable);
                //Correspondant := 'BULTEAU PATRICE';

                TextPaiement2 := '';
                IF NOT "Sales Header"."Echéances fractionnées" THEN
                    Evaluate(TextPaiement, PaymentMethod.Description + ' ' + PaymentTerms.Description)
                ELSE BEGIN
                    Evaluate(TextPaiement, FORMAT("Sales Header"."Taux Premiere Fraction") + '% ' + PaymentMethod.Description + ' ' + PaymentTerms.Description);
                    PaymentTerms.GET("Sales Header"."Payment Terms Code 2");
                    Evaluate(TextPaiement2, FORMAT(100 - "Sales Header"."Taux Premiere Fraction") + '% ' +
                                    PaymentMethod.Description + ' ' + PaymentTerms.Description);
                END;
                //CPH 12/03/09 - DEBUT
                IF RemiseFacture.GET("Sales Header"."Invoice Disc. Code") THEN
                    TxtRemiseFacture := FORMAT(RemiseFacture."Discount %") + ' %'
                ELSE
                    TxtRemiseFacture := '';
                //CPH 12/03/09 - FIN


                TxtEch1 := STRSUBSTNO(ESK006Lbl, SalesSetup."Pourcentage échéance 1", SalesSetup."Due Date 1");
                TxtEch2 := STRSUBSTNO(ESK006Lbl, SalesSetup."Pourcentage échéance 2", SalesSetup."Due Date 2");
                TxtEch3 := STRSUBSTNO(ESK006Lbl, 100 - SalesSetup."Pourcentage échéance 2" - SalesSetup."Pourcentage échéance 1",
                       SalesSetup."Due Date 3");


                IF "Sales Header"."Order Date" = "Sales Header"."Requested Delivery Date" THEN
                    DateLivrDemandee := ''
                ELSE
                    DateLivrDemandee := STRSUBSTNO(ESK004Lbl, "Sales Header"."Requested Delivery Date");

            end;

            trigger OnPreDataItem()
            begin
                IF NOT IsRequestPage THEN _refnondisposeul := FALSE; // MCO Le 09-12-2015 => Envoi par mail
                //IF NOT IsRequestPage THEN   _ImprimerLogo := TRUE; // MCO Le 09-12-2015 => Envoi par mail
                Print := Print OR NOT CurrReport.PREVIEW;
                AsmInfoExistsForLine := FALSE;
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOf_CopiesName; NoOfCopies)
                    {
                        Caption = 'No. of Copies';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. of Copies field.';
                    }
                    field(Show_InternalInfoF; ShowInternalInfo)
                    {
                        Caption = 'Show Internal Information';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Internal Information field.';
                    }
                    field(Archive_DocumentName; ArchiveDocument)
                    {
                        Caption = 'Archive Document';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Archive Document field.';

                        trigger OnValidate()
                        begin
                            IF NOT ArchiveDocument THEN
                                LogInteraction := FALSE;
                        end;
                    }
                    field(LogI_nteractionName; LogInteraction)
                    {
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Log Interaction field.';

                        trigger OnValidate()
                        begin
                            IF LogInteraction THEN
                                ArchiveDocument := ArchiveDocumentEnable;
                        end;
                    }
                    field(Show_AssemblyComponents; DisplayAssemblyInformation)
                    {
                        Caption = 'Show Assembly Components';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Assembly Components field.';
                    }
                    field(_NotPrint_RefSaisiNAme; _NotPrintRefSaisi)
                    {
                        Caption = 'Ne pas imprimer ref. active';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Ne pas imprimer ref. active field.';
                    }
                    field(_refnondisposeulNAme; _refnondisposeul)
                    {
                        Caption = 'Ref. Non Dispo seule';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Ref. Non Dispo seule field.';
                    }
                    field("Imprimer Logo"; _ImprimerLogo)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the _ImprimerLogo field.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit()
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage()
        begin
            IsRequestPage := TRUE; // MCO Le 09-12-2015 => Permet de passer outre le save values en envoit par mail

            ArchiveDocument := SalesSetup."Archive Quotes" <> SalesSetup."Archive Quotes"::Never;
            LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(3)) <> '';

            LogInteractionEnable := LogInteraction;

            // _refnondisposeul :=FALSE; // AD Le 08-12-2015   AD Le 23-03-2016 => Plus utile
        end;
    }

    labels
    {
        Dateéchéance = 'date d''échéance';
        VotreReference = 'Votre référence';
        LabelNoClient = 'N° client';
    }

    trigger OnInitReport()
    begin
        GLSetup.GET();
        SalesSetup.GET();

        _refnondisposeul := FALSE; // AD Le 08-12-2015
        ReportLineNumber := 0;
        Test := TRUE;

        // GR le 19-10-2017
        IF Globals.getFromTherefore() THEN
            _NotPrintRefSaisi := TRUE;

    end;

    trigger OnPreReport()
    begin
        _ImprimerLogo := TRUE;// AD Le 07-12-2015 => On force le logo

        CompanyInfo.GET();
        IF _ImprimerLogo THEN
            CompanyInfo.CALCFIELDS(Picture);
        CompanyInfo.CALCFIELDS("Picture Address");
        GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
    end;

    var
        GLSetup: Record "General Ledger Setup";

        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        CurrExchRate: Record "Currency Exchange Rate";
        PaymentMethod: Record "Payment Method";
        PrepmtPaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";

        SalesSetup: Record "Sales & Receivables Setup";
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        TempPrepmtVATAmountLine: Record "VAT Amount Line" temporary;
        TempPrepmtVATAmountLineDeduct: Record "VAT Amount Line" temporary;
        TempSalesLine: Record "Sales Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";

        TempPrepmtDimSetEntry: Record "Dimension Set Entry" temporary;
        TempPrepmtInvBuf: Record "Prepayment Inv. Line Buffer" temporary;
        RespCenter: Record "Responsibility Center";
        RecCustomer: Record Customer;
        AsmHeader: Record "Assembly Header";
        TempBufferPaiment: Record "Payment Post. Buffer" temporary;
        AsmLine: Record "Assembly Line";
        ShippingAgent: Record "Shipping Agent";

        RemiseFacture: Record "Cust. Invoice Disc.";

        GestionEdition: Codeunit "Gestion Info Editions";
        GestionMulti: Codeunit "Gestion Multi-référence";
        Globals: Codeunit Globals;

        FormatAddr: Codeunit "Format Address";
        Language_G: Codeunit Language;
        ReportUtils: Codeunit ReportFunctions;
        SegManagement: Codeunit SegManagement;
        ArchiveManagement: Codeunit ArchiveManagement;
        SalesPostPrepmt: Codeunit "Sales-Post Prepayments";
        DimMgt: Codeunit DimensionManagement;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        SellToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        DimText: Text[120];
        OldDimText: Text[120];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        ArchiveDocument: Boolean;
        LogInteraction: Boolean;
        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        VALSpecLCYHeader: Text[80];
        Text007Lbl: Label 'VAT Amount Specification in ';
        Text008Lbl: Label 'Local Currency';
        Text009Lbl: Label 'Exchange rate: %1/%2', Comment = '%1 =  CalculatedExchRate ; %2 = Exchange Rate Amount';
        VALExchRate: Text[50];
        PrepmtVATAmount: Decimal;
        PrepmtVATBaseAmount: Decimal;
        PrepmtTotalAmountInclVAT: Decimal;
        PrepmtLineAmount: Decimal;
        OutputNo: Integer;
        NNCTotalLCY: Decimal;
        NNCTotalExclVAT: Decimal;
        NNCVATAmt: Decimal;
        NNCTotalInclVAT: Decimal;
        NNCPmtDiscOnVAT: Decimal;
        NNCTotalInclVAT2: Decimal;
        NNCVATAmt2: Decimal;
        NNCTotalExclVAT2: Decimal;
        NNCSalesLineLineAmt: Decimal;
        NNCSalesLineInvDiscAmt: Decimal;
        Print: Boolean;
        ArchiveDocumentEnable: Boolean;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmInfoExistsForLine: Boolean;
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        VATRegNoCaptionLbl: Label 'VAT Registration No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankCaptionLbl: Label 'Bank';
        AccountNoCaptionLbl: Label 'Account No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        OrderNoCaptionLbl: Label 'Order No.';
        HomePageCaptionCapLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'E-Mail';
        HeaderDimCaptionLbl: Label 'Header Dimensions';
        Text000Lbl: Label 'Salesperson';
        Text001Lbl: Label 'Total %1', Comment = '%1 = Currency';
        Text002Lbl: Label 'Total %1 Incl. VAT', Comment = '%1 = Currency';
        Text003Lbl: Label 'COPY';
        Text004Lbl: Label 'Order Confirmation %1', Comment = '%1 = No Ordre';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = PAge Nb ; %2 = TotalNB of Pages';
        Text006Lbl: Label 'Total %1 Excl. VAT', Comment = '%1 = Currency';

        DiscountPercentCaptionLbl: Label 'Discount %';
        SubtotalCaptionLbl: Label 'Subtotal';
        PaymentDiscountVATCaptionLbl: Label 'Payment Discount on VAT';
        LineDimCaptionLbl: Label 'Line Dimensions';
        InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
        Param1Txt: Label '%1 %2', Comment = '%1; %2';
        VATIdentifierCaptionLbl: Label 'VAT Identifier';
        ShiptoAddrCaptionLbl: Label 'Ship-to Address';
        BilltoAddrCaptionLbl: Label 'Ship-to Address';
        DescriptionCaptionLbl: Label 'Description';
        GLAccountNoCaptionLbl: Label 'G/L Account No.';
        PrepaymentSpecCaptionLbl: Label 'Prepayment Specification';
        PrepaymentVATAmtSpecCapLbl: Label 'Prepayment VAT Amount Specification';
        PrepmtPmtTermsDescCaptionLbl: Label 'Prepmt. Payment Terms';
        PhoneNoCaptionLbl: Label 'Phone No.';
        AmountCaptionLbl: Label 'Amount';
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        LineAmtCaptionLbl: Label 'Line Amount';
        TotalCaptionLbl: Label 'Total';
        UnitPriceCaptionLbl: Label 'Unit Price';
        PaymentTermsCaptionLbl: Label 'Payment Terms';
        PaymentMethodCaptionLbl: Label 'Mode de réglement';
        ShipmentMethodCaptionLbl: Label 'Shipment Method';
        DocumentDateCaptionLbl: Label 'Document Date';
        OrderDateCaptionLbl: Label 'Date de commande';
        AllowInvDiscCaptionLbl: Label 'Allow Invoice Discount';
        NombredelignepossibleparPage: Integer;
        NombreDeLigneImprimer: Integer;
        NombredelignepossibleparPagePleine: Integer;

        TEXT50004Lbl: Label 'Merci de nous renvoyer ce document signé.';
        ItemNo: Code[20];

        PrixTaille: array[5] of Decimal;


        Correspondant: Text[250];

        RemiseEnteteTotal: Decimal;
        //PrintForFax: Boolean;
        textSIEGSOC: array[5] of Text[250];
        // MasquerRemise: Boolean;
        // TxtCodeDouanier: Text[30];

        // TxtConditionnement: Text[20];
        // TxtpaysOrigine: Text[30];

        TxtRemiseFacture: Text[7];
        // PrintGencod: Boolean;
        // txtGencod: Text[30];
        // PourcentageRemise: Text[30];
        PourcentageRemiseEntete: Text[30];
        // Escompte: Decimal;
        CalcRemiseFacture: Decimal;

        tab_InfoSymta: array[8] of Text[60];

        TotalBrutLigne: Decimal;

        _NoLigne: Integer;
        ReferenceActive: Text[52];

        _PuBrut: Text[13];
        _Remise: Text[13];

        "_DateDisponibilité": Text[30];
        NoLigne: Integer;

        _Titre: Text[50];

        _NotPrintRefSaisi: Boolean;
        TxtEch1: Text[50];
        TxtEch2: Text[50];
        TxtEch3: Text[50];
        TextPaiement: Text[100];
        TextPaiement2: Text[100];
        CodeSubstitution: Text[100];
        _refnondisposeul: Boolean;
        _qtereste: Text[10];
        _delai: Text[10];
        _Reference1: Text[52];
        _Reference2: Text[30];
        _Reference3: Text[30];
        // ESK001Lbl: Label 'No %1 du %2';
        ESK002Lbl: Label 'Order Confirmation %1', Comment = '%1 = Order No';
        ESK003Lbl: Label 'PROFORMA / DEVIS';
        ESK004Lbl: Label 'Livraison demandée le : %1', Comment = '%1 = Date ';
        ESK005Lbl: Label 'Dispo. Le : %1', Comment = '%1 = Dispo';
        ESK006Lbl: Label '%1% Le %2', Comment = '%1% Le %2';
        ESK007Lbl: Label 'Référence commandée : %1', Comment = '%1 = Ref';
        ESK010Lbl: Label 'En remplacement de : %1', Comment = '%1 =Artice';


        ReferenceSaisie: Text;
        DateLivrDemandee: Text;
        Text50006Lbl: Label 'La commande ne doit pas être ouverte !';
        Test: Boolean;
        ReportLineNumber: Integer;
        _ImprimerLogo: Boolean;
        IsRequestPage: Boolean;


    procedure InitializeRequest(NoOfCopiesFrom: Integer; ShowInternalInfoFrom: Boolean; ArchiveDocumentFrom: Boolean; LogInteractionFrom: Boolean; PrintFrom: Boolean; DisplayAsmInfo: Boolean)
    begin
        NoOfCopies := NoOfCopiesFrom;
        ShowInternalInfo := ShowInternalInfoFrom;
        ArchiveDocument := ArchiveDocumentFrom;
        LogInteraction := LogInteractionFrom;
        Print := PrintFrom;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[50]
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        IF NOT UnitOfMeasure.GET(UOMCode) THEN
            EXIT(UOMCode);
        EXIT(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10]
    begin
        EXIT(PADSTR('', 2, ' '));
    end;

    procedure FormatRemise() retour: Text[20]
    begin
        IF "Sales Line"."Line Discount %" = 0 THEN
            EXIT('');

        ////IF "Sales Line"."Line Discount 1 %"<>0 THEN
        //// retour:=FORMAT("Sales Line"."Line Discount 1 %");

        ////IF "Sales Line"."Line Discount 2 %"<>0 THEN
        //// BEGIN
        ////  IF  retour <> '' THEN
        ////   retour+='+';
        //// retour+=FORMAT("Sales Line"."Line Discount 2 %");
        //// END;

        ////IF "Sales Line"."Line Discount 3 %"<>0 THEN
        //// BEGIN
        ////  IF  retour <> '' THEN
        ////   retour+='+';
        //// retour+=FORMAT("Sales Line"."Line Discount 3 %");
        //// END;
    end;

    local procedure DocumentCaption() _Result: Text[250]
    var
        Text50001Lbl: Label '%1 N° %2 du %3', Comment = '%1 = Titre Document ; %2 = Num Docu ; %3 = Order Date';
    begin
        //_Result:=STRSUBSTNO(Text50001,"Sales Header"."Document Type","Sales Header"."No.");


        CASE "Sales Header"."Document Type" OF
            "Sales Header"."Document Type"::Order:
                _Titre := ESK002Lbl;
            "Sales Header"."Document Type"::Quote:
                _Titre := ESK003Lbl;
            ELSE
                _Titre := FORMAT("Sales Header"."Document Type");
        END;

        _Result := STRSUBSTNO(Text50001Lbl, _Titre, "Sales Header"."No.", "Sales Header"."Order Date");


        EXIT(_Result);
    end;
}