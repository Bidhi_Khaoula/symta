report 50064 "Print Message Miniload"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Print Message Miniload.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Print Message Miniload';
    dataset
    {
        dataitem("Message receive from  MINILOAD"; "Message receive from  MINILOAD")
        {
            DataItemTableView = SORTING("Entry No.");
            RequestFilterFields = "Message No.";
            column(MessageNo; "Message No.")
            {
            }
            column(Date; Date)
            {
            }
            column(Heure; Heure)
            {
            }
            column("Code"; Code)
            {
            }
            column(Description; Description)
            {
            }
        }
    }
}

