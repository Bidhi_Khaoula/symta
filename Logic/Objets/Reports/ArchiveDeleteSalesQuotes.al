report 50002 "Archive Delete Sales Quotes"
{
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Archiver & supprimer devis vente';
    ProcessingOnly = true;

    dataset
    {
        dataitem(dtSalesHeader; "Sales Header")
        {
            DataItemTableView = SORTING("Document Type", "No.")
                                ORDER(Ascending)
                                WHERE("Document Type" = CONST(Quote));
            RequestFilterFields = "Document Date";

            trigger OnAfterGetRecord();
            begin
                dtSalesHeader.CALCFIELDS(Amount);

                // Masque les messages
                dtSalesHeader.SetHideValidationDialog(TRUE);

                //Archivage silencieux
                IF (gArchiveBoolean) THEN
                    gArchiveManagement.ArchSalesDocumentNoConfirm(dtSalesHeader);

                //Suppression
                IF (gDeleteBoolean) THEN
                    DeleteSalesHeader();
            end;

            trigger OnPreDataItem();
            begin
                // Montant à 0
                IF gZeroBoolean THEN
                    dtSalesHeader.SETRANGE(Amount, 0)
                ELSE
                    dtSalesHeader.SETRANGE(Amount);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(gArchive_BooleanName; gArchiveBoolean)
                    {
                        Caption = 'Archiver les devis';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Archiver les devis field.';
                    }
                    field(gDelete_BooleanName; gDeleteBoolean)
                    {
                        Caption = 'Supprimer les devis';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Supprimer les devis field.';
                    }
                    field(gZero_BooleanName; gZeroBoolean)
                    {
                        Caption = 'Filtrer les montants à 0';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Filtrer les montants à 0 field.';
                    }
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        // Initialisation des variables
        gArchiveBoolean := TRUE;
        gDeleteBoolean := TRUE;
        gZeroBoolean := TRUE;
    end;

    trigger OnPostReport();
    begin
        MESSAGE(gMessageEndMsg);
    end;

    trigger OnPreReport();
    begin
        // Option(s) obligatoire(s)
        IF (NOT gArchiveBoolean) AND (NOT gDeleteBoolean) THEN
            ERROR(gSelectErr);

        // Filtre date obligatoire
        IF (dtSalesHeader.GETFILTER("Document Date") = '') THEN
            ERROR(gErrorDocumentDateErr);

        // Montant à 0
        IF gZeroBoolean THEN
            dtSalesHeader.SETRANGE(Amount, 0)
        ELSE
            dtSalesHeader.SETRANGE(Amount);

        // Confirmation traitement par l'utilisateur
        IF gArchiveBoolean THEN
            IF gDeleteBoolean THEN
                gConfirmTxt := gConfirmArchiveDeleteQst
            ELSE
                gConfirmTxt := gConfirmArchiveQst
        ELSE
            gConfirmTxt := gConfirmDeleteQst;

        IF NOT CONFIRM(STRSUBSTNO(gConfirmTxt, dtSalesHeader.COUNT()), FALSE) THEN
            ERROR(gCancelErr);
    end;

    var
        gArchiveManagement: Codeunit ArchiveManagement;
        gArchiveBoolean: Boolean;
        gDeleteBoolean: Boolean;
        gZeroBoolean: Boolean;
        gConfirmArchiveQst: Label 'Voulez vous archiver les devis (Nombre : %1) ?', Comment = '%1=Nombre';
        gConfirmDeleteQst: Label 'Voulez vous supprimer les devis (Nombre : %1) ?', Comment = '%1=Nombre';
        gConfirmArchiveDeleteQst: Label 'Voulez vous archiver puis supprimer les devis (Nombre : %1) ?', Comment = '%1=Nombre';
        gSelectErr: Label 'Vous devez choisir au moins une option [Archiver] et/ou [Supprimer].', Comment = '%1=Nombre';
        gCancelErr: Label 'Traitement annulé par l''utilisateur.';
        gConfirmTxt: Text;
        gErrorDocumentDateErr: Label 'Vous devez préciser un filtre sur le champ [Date de document].';
        gMessageEndMsg: Label 'Traitement terminé.';

    local procedure DeleteSalesHeader();
    var
        lSalesLine: Record "Sales Line";
        lSalesCommentLine: Record "Sales Comment Line";
    //lThereForce_ProcessingJournal: Record "52101147";
    begin
        // voir T36.OnDelete()

        lSalesLine.SETRANGE("Document Type", dtSalesHeader."Document Type");
        lSalesLine.SETRANGE("Document No.", dtSalesHeader."No.");
        lSalesLine.SETRANGE(Type, lSalesLine.Type::"Charge (Item)");
        DeleteSalesLines(lSalesLine);
        lSalesLine.SETRANGE(Type);
        DeleteSalesLines(lSalesLine);

        lSalesCommentLine.SETRANGE("Document Type", dtSalesHeader."Document Type");
        lSalesCommentLine.SETRANGE("No.", dtSalesHeader."No.");
        lSalesCommentLine.DELETEALL();

        // MCO Le 08-02-2017 => Régie
        // Supprime le job planifié dans thereforce car si document n'existe plus alors plantage
        /*lThereForce_ProcessingJournal.RESET();
        lThereForce_ProcessingJournal.SETRANGE("Table ID", 36);
        lThereForce_ProcessingJournal.SETRANGE("Document Type", dtSalesHeader."Document Type");
        lThereForce_ProcessingJournal.SETRANGE("Document No.", dtSalesHeader."No.");
        IF NOT lThereForce_ProcessingJournal.ISEMPTY() THEN
            lThereForce_ProcessingJournal.DELETEALL();
        // FIN MCO Le 08-02-2017 => Régie*/

        // Ne pas rejouer le OnDelete()
        dtSalesHeader.DELETE(FALSE);
    end;

    local procedure DeleteSalesLines(var pSalesLine: Record "Sales Line");
    var
        lReservationManagement: Codeunit "Reservation Management";
    begin
        IF pSalesLine.FINDSET() THEN BEGIN
            lReservationManagement.DeleteDocumentReservation(DATABASE::"Sales Line", dtSalesHeader."Document Type".AsInteger(), dtSalesHeader."No.", TRUE);
            REPEAT
                pSalesLine.SuspendStatusCheck(TRUE);
                pSalesLine.DELETE(TRUE);
            UNTIL pSalesLine.NEXT() = 0;
        END;
    end;
}

