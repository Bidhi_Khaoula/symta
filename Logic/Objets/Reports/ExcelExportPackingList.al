report 50042 "Excel Export Packing List"
{

    Caption = 'Sales - Shipment';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeading = 'Posted Sales Shipment';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(PageCaption; PageCaptionCapLbl)
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalText)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(NetUnitPriceCaption; NetUnitPriceCaptionLbl)
            {
            }
            column(no_fax; no_fax)
            {
            }
            column(Objet_fax; Objet_fax)
            {
            }
            column(Attention_fax; Attention_fax)
            {
            }
            column(nompdf; nompdf)
            {
            }
            column(CompanyInfo1Picture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPictureFooter; CompanyInfo."Bottom Picture")
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(SalesShptCopyText; GetDocumentCaption())
                    {
                    }
                    column(RightAddr1; ShipToAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(RightAddr2; ShipToAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(RightAddr3; ShipToAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(RightAddr4; ShipToAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(RightAddr5; ShipToAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(RightAddr6; ShipToAddr[6])
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoVATRegtnNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(SelltoCustNo_SalesShptHeader; "Sales Shipment Header"."Sell-to Customer No.")
                    {
                    }
                    column(DocDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Document Date", 0, DateFormatLbl))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourRef_SalesShptHeader; "Sales Shipment Header"."Your Reference")
                    {
                    }
                    column(RightAddr7; ShipToAddr[7])
                    {
                    }
                    column(RightAddr8; ShipToAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(ShptDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Shipment Date", 0, DateFormatLbl))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
                    {
                    }
                    column(PhoneNoCaption; PhoneNoCaptionLbl)
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(GiroNoCaption; GiroNoCaptionLbl)
                    {
                    }
                    column(BankNameCaption; BankNameCaptionLbl)
                    {
                    }
                    column(BankAccNoCaption; BankAccNoCaptionLbl)
                    {
                    }
                    column(ShipmentNoCaption; ShipmentNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(HomePageCaption; HomePageCaptionLbl)
                    {
                    }
                    column(EmailCaption; EmailCaptionLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(SelltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(LeftAddr1; SellToAddr[1])
                    {
                    }
                    column(LeftAddr2; SellToAddr[2])
                    {
                    }
                    column(LeftAddr3; SellToAddr[3])
                    {
                    }
                    column(LeftAddr4; SellToAddr[4])
                    {
                    }
                    column(LeftAddr5; SellToAddr[5])
                    {
                    }
                    column(LeftAddr6; SellToAddr[6])
                    {
                    }
                    column(LeftAddr7; SellToAddr[7])
                    {
                    }
                    column(LeftAddr8; SellToAddr[8])
                    {
                    }
                    column(LeftAddrCaption; SelltoAddrCaptionLbl)
                    {
                    }
                    column(MiddleAddrCaption; BilltoAddrCaptionLbl)
                    {
                    }
                    column(HeaderField01Caption; LocationCaptionLbl)
                    {
                    }
                    column(HeaderField02Caption; RequestedDeliveryDateCaptionLbl)
                    {
                    }
                    column(HeaderField03Caption; "Sales Shipment Header".FIELDCAPTION("Order No."))
                    {
                    }
                    column(HeaderField04Caption; ShippingAgentCaptionLbl)
                    {
                    }
                    column(HeaderField05Caption; ExternalDocNoCaptionLbl)
                    {
                    }
                    column(HeaderField06Caption; ShippingAgentSrvCaptionLbl)
                    {
                    }
                    column(HeaderField07Caption; SalesPersonText)
                    {
                    }
                    column(HeaderField08Caption; "Sales Shipment Header".FIELDCAPTION("Shipment Method Code"))
                    {
                    }
                    column(HeaderField09Caption; '')
                    {
                    }
                    column(HeaderField10Caption; '')
                    {
                    }
                    column(HeaderField01; "Sales Shipment Header"."Location Code")
                    {
                    }
                    column(HeaderField02; FORMAT("Sales Shipment Header"."Requested Delivery Date", 0, DateFormatLbl))
                    {
                    }
                    column(HeaderField03; "Sales Shipment Header"."Order No.")
                    {
                    }
                    column(HeaderField04; ShippingAgent.Name)
                    {
                    }
                    column(HeaderField05; "Sales Shipment Header"."External Document No.")
                    {
                    }
                    column(HeaderField06; ShippingAgentServices.Description)
                    {
                    }
                    column(HeaderField07; SalesPurchPerson.Name)
                    {
                    }
                    column(HeaderField08; "Sales Shipment Header"."Shipment Method Code")
                    {
                    }
                    column(HeaderField09; '')
                    {
                    }
                    column(HeaderField10; '')
                    {
                    }
                    column(MiddleAddr1; CustAddr[1])
                    {
                    }
                    column(MiddleAddr2; CustAddr[2])
                    {
                    }
                    column(MiddleAddr3; CustAddr[3])
                    {
                    }
                    column(MiddleAddr4; CustAddr[4])
                    {
                    }
                    column(MiddleAddr5; CustAddr[5])
                    {
                    }
                    column(MiddleAddr6; CustAddr[6])
                    {
                    }
                    column(MiddleAddr7; CustAddr[7])
                    {
                    }
                    column(MiddleAddr8; CustAddr[8])
                    {
                    }
                    column(PricesInclVAT_SalesShptHeader; "Sales Shipment Header"."Prices Including VAT")
                    {
                    }
                    column(SalesOrdersCaption; SalesOrdersCaptionLbl)
                    {
                    }
                    column(SalesShipmentsCaption; SalesShipmentsCaptionLbl)
                    {
                    }
                    column(SalesInvociesCaption; SalesInvoicesCaptionLbl)
                    {
                    }
                    column(ActiveRefCaption; ActiveRefCaptionLbl)
                    {
                    }
                    column(TotalGrossWeightCaption; TotalGrossWeightCaptionLbl)
                    {
                    }
                    column(TotalNetWeightCaption; TotalNetWeightCaptionLbl)
                    {
                    }
                    dataitem(PackingListLine2; "Packing List Line")
                    {
                        DataItemLink = "Packing List No." = FIELD("Packing List No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                            WHERE("Line Quantity" = FILTER(<> 0));
                        dataitem("Posted Whse. Shipment Header"; "Posted Whse. Shipment Header")
                        {
                            DataItemLink = "Packing List No." = FIELD("Packing List No.");
                            DataItemTableView = SORTING("No.")
                                                WHERE("Packing List No." = FILTER(<> ''));
                            dataitem("Posted Whse. Shipment Line"; "Posted Whse. Shipment Line")
                            {
                                DataItemLink = "No." = FIELD("No.");
                                DataItemTableView = SORTING("No.", "Line No.")
                                                    WHERE("Posted Source Document" = CONST("Posted Shipment"));
                                dataitem("Sales Header"; "Sales Header")
                                {
                                    DataItemLink = "No." = FIELD("Source No.");
                                    DataItemTableView = SORTING("Document Type", "No.")
                                                        WHERE("Document Type" = CONST(Order));
                                    column(No_SalesHeader; "Sales Header"."No.")
                                    {
                                    }

                                    trigger OnAfterGetRecord();
                                    begin
                                        //>> SFD20201005
                                        FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                            IF "Sales Header"."No." = gDocNo[gY] THEN
                                                CurrReport.SKIP();
                                        gI := COMPRESSARRAY(gDocNo) + 1;
                                        gDocNo[gI] := "Sales Header"."No.";
                                        MakeExcelBodyDetail(1);
                                        //<< SFD20201005
                                    end;

                                    trigger OnPreDataItem();
                                    begin
                                        //>> SFD20201005
                                        IF NOT gSalesOrderHeaderPrinted THEN BEGIN
                                            MakeExcelBodyHeader(1);
                                            gSalesOrderHeaderPrinted := TRUE;
                                        END;
                                        //<< SFD20201005
                                    end;
                                }
                                dataitem(SalesShipmentHeader2; "Sales Shipment Header")
                                {
                                    DataItemLink = "No." = FIELD("Posted Source No.");
                                    DataItemTableView = SORTING("No.");
                                    column(No_SalesShipmentHeader2; SalesShipmentHeader2."No.")
                                    {
                                    }

                                    trigger OnAfterGetRecord();
                                    begin
                                        //>> SFD20201005
                                        FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                            IF SalesShipmentHeader2."No." = gDocNo[gY] THEN
                                                CurrReport.SKIP();
                                        gI := COMPRESSARRAY(gDocNo) + 1;
                                        gDocNo[gI] := SalesShipmentHeader2."No.";
                                        MakeExcelBodyDetail(2);
                                        //<< SFD20201005
                                    end;

                                    trigger OnPreDataItem();
                                    begin
                                        //>> SFD20201005
                                        IF NOT gSalesShipmentHeaderPrinted THEN BEGIN
                                            MakeExcelBodyHeader(2);
                                            gSalesShipmentHeaderPrinted := TRUE;
                                        END;
                                        //<< SFD20201005
                                    end;
                                }
                                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                                {
                                    DataItemLink = "Shipment No." = FIELD("No.");
                                    DataItemLinkReference = SalesShipmentHeader2;
                                    DataItemTableView = SORTING("Document No.", "N° client Livré", "Shipment No.")
                                                        WHERE(Type = CONST(Item),
                                                              "Shipment No." = FILTER(<> ''));
                                    column(DocumentNo_SalesInvoiceLine; "Sales Invoice Line"."Document No.")
                                    {
                                    }

                                    trigger OnAfterGetRecord();
                                    begin
                                        //>> SFD20201005
                                        FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                            IF "Sales Invoice Line"."Document No." = gDocNo[gY] THEN
                                                CurrReport.SKIP();
                                        gI := COMPRESSARRAY(gDocNo) + 1;
                                        gDocNo[gI] := "Sales Invoice Line"."Document No.";
                                        MakeExcelBodyDetail(3);
                                        //<< SFD20201005
                                    end;

                                    trigger OnPreDataItem();
                                    begin
                                        //>> SFD20201005
                                        IF NOT gSalesInvoiceHeaderPrinted THEN BEGIN
                                            MakeExcelBodyHeader(3);
                                            gSalesInvoiceHeaderPrinted := TRUE;
                                        END;
                                        //<< SFD20201005
                                    end;
                                }
                            }
                        }

                        trigger OnAfterGetRecord();
                        begin
                            //>> SFD20201005
                            CalcTotals(PackingListLine2);
                            //<< SFD20201005
                        end;

                        trigger OnPostDataItem();
                        begin
                            //>> SFD20201005
                            MakeExcelBodyDetail(5);
                            TempExcelBuf.OnlyCreateBook('Entête', 'Master List', Format(COMPANYNAME), USERID, FALSE);
                            TempExcelBuf.DELETEALL();
                            //<< SFD20201005
                        end;

                        trigger OnPreDataItem();
                        begin
                            gI := 0;
                        end;
                    }
                    dataitem("Packing List Line"; "Packing List Line")
                    {
                        CalcFields = "Ref. Active";
                        DataItemLink = "Packing List No." = FIELD("Packing List No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                            WHERE("Line Quantity" = FILTER(<> 0));
                        column(NoExp_PackingList; "Packing List Line"."Whse. Shipment No.")
                        {
                        }
                        column(NumColis_PackingList; "Packing List Line"."Package No.")
                        {
                        }
                        column(NumTypeColis_PackingList; STRSUBSTNO('%2 : %1', "Package No.", DetailColis."Package Type"))
                        {
                        }
                        column(CommentColis; CommentColis)
                        {
                        }
                        column(Comment2Colis; Comment2Colis)
                        {
                        }
                        column(TxtDetailColis; TxtDétailColis)
                        {
                        }
                        column(TxtPoidsBrutColis; STRSUBSTNO(Text50001lBL, DetailColis."Package Weight"))
                        {
                        }
                        column(TxtCodeDouanier; TxtCodeDouanier)
                        {
                        }
                        column(NoArticle_PackingList; "Packing List Line"."Item No.")
                        {
                        }
                        column(TxtDescription; TxtDescription)
                        {
                        }
                        column("QtéColis_PackingList"; "Packing List Line"."Line Quantity")
                        {
                        }
                        column(CodeCaption; CodeCaptionLbl)
                        {
                        }
                        column(DescCaption; DescCaptionLbl)
                        {
                        }
                        column(QtyCaption; QtyCaptionLbl)
                        {
                        }
                        column(RefActive_PackingListLine; "Packing List Line"."Ref. Active")
                        {
                        }
                        column(PackageWeight_PackingListLine; DetailColis."Package Weight")
                        {
                        }
                        column(NetWeight_PackingListLine; DetailColis."Net Weight")
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            IF DetailColis.GET("Packing List Line"."Packing List No.", "Packing List Line"."Package No.") THEN BEGIN
                                DetailColis.CALCFIELDS("Package Quantity", "Package Weight Auto");
                                TxtDétailColis := STRSUBSTNO(Text50000Lbl, DetailColis.Length, DetailColis.Width, DetailColis.Height, DetailColis."Package Quantity");
                                CommentColis := DetailColis."Comment 1";
                                Comment2Colis := DetailColis."Comment 2";

                                // AD Le 02-07-2015 =>
                                IF DetailColis."Outside Of EC" THEN
                                    Evaluate(TxtDétailColis, TxtDétailColis + ' ' + ESK002Lbl)
                                ELSE
                                    Evaluate(TxtDétailColis, TxtDétailColis + ' ' + ESK003Lbl);
                                // FIN AD Le 02-07-2015
                            END
                            ELSE BEGIN
                                TxtDétailColis := '';
                                CommentColis := '';
                                Comment2Colis := '';
                            END;



                            // AD Le 06-07-2007 => Gestion designation1 + designation2
                            item.GET("Packing List Line"."Item No.");
                            Evaluate(TxtDescription, "Packing List Line".Description + ' ' + "Packing List Line"."Description 2");


                            // AD Le 04-04-2013 => Idem BL
                            //TxtCodeDouanier := CU_GestionEdition.GetPoidsInfoDouanière("Sales Shipment Header"."Sell-to Customer No.",
                            //  "Packing List Line"."Item No.");

                            IF NOT ((arExport) AND (item."Tariff No." <> '')) THEN TxtCodeDouanier := '';
                            MakeExcelBodyDetail(4);
                        end;

                        trigger OnPostDataItem();
                        begin
                            TempExcelBuf.OnlyCreateBook('Lignes', 'Master List', Format(COMPANYNAME), USERID, TRUE);
                        end;

                        trigger OnPreDataItem();
                        begin
                            "Nb colis" := 0;
                            //>> SFD20201005
                            IF NOT gPackingLineHeaderPrinted THEN BEGIN
                                MakeExcelBodyHeader(4);
                                gPackingLineHeaderPrinted := TRUE;
                            END;
                            //<< SFD20201005
                        end;
                    }
                    dataitem(Total; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(BilltoCustNo_SalesShptHeader; "Sales Shipment Header"."Bill-to Customer No.")
                        {
                        }
                        column(BilltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Bill-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowCustAddr THEN
                                CurrReport.BREAK();
                        end;
                    }

                    trigger OnPreDataItem();
                    begin
                        // Item Tracking:
                        IF ShowLotSN THEN BEGIN
                            TrackingSpecCount := 0;
                            OldRefNo := 0;
                            ShowGroup := FALSE;
                        END;
                        //>> SFD20201005
                        IF NOT gReportHeaderPrinted THEN BEGIN
                            MakeExcelHeader();
                            gReportHeaderPrinted := TRUE;
                        END;
                        //<< SFD20201005
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    LOldSalesShptLine: Record "Sales Shipment Line";
                begin
                    // ANI ESKVN1.0 => BL Chiffré / TVA - ShowPrices
                    CLEAR(TempSalesShptLine);
                    TempVATAmountLine.DELETEALL();
                    TempSalesShptLine.DELETEALL();

                    // GetSalesShptLines
                    LOldSalesShptLine.SETRANGE("Document No.", "Sales Shipment Header"."No.");
                    IF LOldSalesShptLine.FINDSET() THEN
                        REPEAT
                            TempSalesShptLine := LOldSalesShptLine;
                            TempSalesShptLine.Insert();
                        UNTIL LOldSalesShptLine.NEXT() = 0;

                    VATAmount := TempVATAmountLine.GetTotalVATAmount();
                    VATBaseAmount := TempVATAmountLine.GetTotalVATBase();
                    VATDiscountAmount :=
                      TempVATAmountLine.GetTotalVATDiscount("Sales Shipment Header"."Currency Code", "Sales Shipment Header"."Prices Including VAT");
                    TotalAmountInclVAT := TempVATAmountLine.GetTotalAmountInclVAT();
                    // FIN ANI ESKVN1.0

                    IF Number > 1 THEN BEGIN
                        //CopyText := FormatDocument.GetCOPYText;
                        CopyText := Text001Lbl;
                        OutputNo += 1;
                    END;
                    TotalQty := 0;           // Item Tracking

                    // ANI ESKVN1.0 => BL Chiffré / TVA - ShowPrices
                    NNCTotalExclVAT := 0;
                    NNCVATAmt := 0;
                    NNCSalesLineLineAmt := 0;
                    // FIN ANI ESKVN1.0

                    WhseEmplName := GetWhseEmployeeName("Sales Shipment Header");

                    // AD Le 11-10-2012 => Gestion des types de colis => Vue avec Virginie + Eric on limite a 4 type
                    //GetColis("Sales Shipment Header"."No.");
                end;

                trigger OnPostDataItem();
                begin
                    IF NOT CurrReport.PREVIEW THEN
                        CODEUNIT.RUN(CODEUNIT::"Sales Shpt.-Printed", "Sales Shipment Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := 1 + ABS(NoOfCopies);
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            begin
                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");

                ShowPrices := "Sales Shipment Header"."Abandon remainder";

                FormatAddressFields("Sales Shipment Header");
                FormatDocumentFields("Sales Shipment Header");

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN
                        SegManagement.LogDocument(
                          5, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');



                IF (PrintForFax) THEN
                    CU_GestionEdition."GetInfoFax-Pdf"('BL', "Sales Shipment Header"."No.", no_fax, Objet_fax, Attention_fax, nompdf);

                IF (PrintForFax) AND (no_fax = '') THEN
                    ERROR('Le no de fax est vide');

                // AD Le 25-03-2007 => Possibilité d'imprimer avec un logo sans faxer
                IF PrintLogo AND (NOT PrintForFax) THEN BEGIN
                    no_fax := '';
                    Objet_fax := '';
                    nompdf := '';
                END;


                //AD on regarde si export pour les code douanier
                IF "Sales Shipment Header"."VAT Bus. Posting Group" = 'SANS TVA' THEN
                    arExport := TRUE
                ELSE
                    arExport := FALSE;
            end;

        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOfCopiesName; NoOfCopies)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'No. of Copies';
                        ToolTip = 'Specifies how many copies of the document to print.';
                        Visible = false;
                    }
                    field(ShowInternalInfoName; ShowInternalInfo)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Internal Information';
                        ToolTip = 'Specifies if the document shows internal information.';
                        Visible = false;
                    }
                    field(LogInteractionName; LogInteraction)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ToolTip = 'Specifies if you want to record the reports that you print as interactions.';
                        Visible = false;
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Correction Lines';
                        ToolTip = 'Specifies if the correction lines of an undoing of quantity posting will be shown on the report.';
                        Visible = false;
                    }
                    field(ShowLotSNName; ShowLotSN)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Serial/Lot Number Appendix';
                        ToolTip = 'Specifies if you want to print an appendix to the sales shipment report showing the lot and serial numbers in the shipment.';
                        Visible = false;
                    }
                    field(DisplayAsmInfo; DisplayAssemblyInformation)
                    {
                        ApplicationArea = Assembly;
                        Caption = 'Show Assembly Components';
                        ToolTip = 'Specifies if you want the report to include information about components that were used in linked assembly orders that supplied the item(s) being sold.';
                        Visible = false;
                    }
                    field(PrintLogoName; PrintLogo)
                    {
                        Caption = 'Print logo';
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Print logo field.';
                    }
                    field(ShowPricesName; ShowPrices)
                    {
                        Caption = 'Show prices';
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show prices field.';
                    }
                    group(Fax)
                    {
                        Caption = 'Fax';
                        Visible = false;
                        field(PrintForFaxName; PrintForFax)
                        {
                            Caption = 'Impression fax';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Impression fax field.';
                        }
                        field(no_faxName; no_fax)
                        {
                            Caption = 'N°';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° field.';
                        }
                        field(Attention_faxName; Attention_fax)
                        {
                            Caption = 'A l''attention de';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the A l''attention de field.';
                        }
                        field(nompdfName; nompdf)
                        {
                            Caption = 'Nom du fichier';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Nom du fichier field.';
                        }
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;

            InitPageOption();
        end;
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET();
        SalesSetup.GET();
        //FormatDocument.SetLogoPosition(SalesSetup."Logo Position on Documents",CompanyInfo1,CompanyInfo2,CompanyInfo3);
        //>> SFD20201005
        gSalesOrderHeaderPrinted := FALSE;
        gSalesInvoiceHeaderPrinted := FALSE;
        gSalesShipmentHeaderPrinted := FALSE;
        gPackingLineHeaderPrinted := FALSE;
        gReportHeaderPrinted := FALSE;
        gCountOrder := 18;
        gCountShipment := 18;
        gCountInvoice := 18;
        //<< SFD20201005
    end;

    trigger OnPostReport();
    begin
        //>> SFD20201005
        TempExcelBuf.SetFriendlyFilename('\Packing List_' + "Sales Shipment Header"."No." + '_' + FORMAT(CURRENTDATETIME, 0, '<Day>_<Month>_<Year4>_<Hours24,2>_<Minutes,2>_<Seconds,2>'));
        TempExcelBuf.OnlyOpenExcel();
        //<< SFD20201005
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
        AsmHeaderExists := FALSE;
        //>> SFD20201005
        TempExcelBuf.DELETEALL();
        // ANI ESKVN1.0 => logo entête et pied de page
        CompanyInfo.GET();
        IF PrintLogo THEN BEGIN
            CompanyInfo.CALCFIELDS(Picture);
            //CompanyInfo.CALCFIELDS("Picture Footer");
            CompanyInfo.CALCFIELDS("Header Picture", "Bottom Picture");
        END;
        GLSetup.GET();
        // FIN ANI ESKVN1.0
        //<< SFD20201005
    end;

    var
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        DimSetEntry1: Record "Dimension Set Entry";
        TempSalesShptLine: Record "Sales Shipment Line" temporary;
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        ShippingAgent: Record "Shipping Agent";
        ShippingAgentServices: Record "Shipping Agent Services";
        ShipmentMethod: Record "Shipment Method";
        GLSetup: Record "General Ledger Setup";
        SalesHeader: Record "Sales Header";
        item: Record Item;
        DetailColis: Record "Packing List Package";
        RespCenter: Record "Responsibility Center";
        TempExcelBuf: Record "Excel Buffer" temporary;
        TempPackingListPackage: Record "Packing List Package" temporary;
        Language_G: Codeunit Language;
        FormatAddr: Codeunit "Format Address";
        FormatDocument: Codeunit "Format Document";
        CU_GestionEdition: Codeunit "Gestion Editions / fax / Pdf";
        SegManagement: Codeunit SegManagement;
        Text001Lbl: Label 'COPY';
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[50];
        ReferenceText: Text[80];
        NoOfCopies: Integer;
        OutputNo: Integer;
        NoOfLoops: Integer;
        TrackingSpecCount: Integer;
        OldRefNo: Integer;
        CopyText: Text[30];
        ShowCustAddr: Boolean;
        ShowInternalInfo: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        ShowGroup: Boolean;
        TotalQty: Decimal;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        Text003Lbl: Label 'Total %1', Comment = '%1';
        Text004Lbl: Label 'Total %1 Incl. VAT', Comment = '%1';
        Text005Lbl: Label 'Total %1 Excl. VAT', Comment = '%1';
        ItemTrackingAppendixCaptionLbl: Label 'Item Tracking - Appendix';
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Reg. No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        ShipmentNoCaptionLbl: Label 'Shipment No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'Email';
        DocumentDateCaptionLbl: Label 'Document Date';
        BilltoAddrCaptionLbl: Label 'Bill-to Address';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 %2';
        PrintLogo: Boolean;
        SellToAddr: array[8] of Text[50];
        SelltoAddrCaptionLbl: Label 'Ship-to Address';
        // CustomerNoCaptionLbl: Label 'Customer No.';
        ExternalDocNoCaptionLbl: Label 'External document No';
        // UOMCaptionLbl: Label 'Unit';
        UnitPriceCaptionLbl: Label 'Unit Price';
        NetUnitPriceCaptionLbl: Label 'Net Price';
        // VATAmtLineTTCAmtCaptionLbl: Label 'Incl. VAT';
        ShowPrices: Boolean;

        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        // InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
        // VATIdentifierCaptionLbl: Label 'VAT Identifier';
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        LineAmtCaptionLbl: Label 'Line Amount';
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        TotalText: Text[50];
        TotalAmountInclVAT: Decimal;
        AmountCaptionLbl: Label 'Amount';
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        NNCTotalExclVAT: Decimal;
        NNCVATAmt: Decimal;
        NNCSalesLineLineAmt: Decimal;
        ShippingAgentCaptionLbl: Label 'Shipping Agent';
        ShippingAgentSrvCaptionLbl: Label 'Shipping Agent Services';
        CodeUtilisateurCreation: Code[20];
        DateFormatLbl: Label '<day,2>/<month,2>/<Year4>';
        LocationCaptionLbl: Label 'Location';
        RequestedDeliveryDateCaptionLbl: Label 'Requested Delivery Date';
        WhseEmplName: Text;

        no_fax: Text[30];
        nompdf: Text[80];
        Objet_fax: Text[250];
        Attention_fax: Text[50];
        PrintForFax: Boolean;
        TxtCodeDouanier: Text;
        "Nb colis": Integer;
        "TxtDétailColis": Text[100];
        CommentColis: Text[60];
        Comment2Colis: Text[60];

        TxtDescription: Text[61];
        Text50000Lbl: Label 'L : %1 cm - l : %2 cm - h : %3 cm. %4 Article(s).', Comment = '%1 %2 %3 %4';
        Text50001Lbl: Label 'Packing Gross Weight : %1 Kg', Comment = '%1';
        ESK002Lbl: Label 'Palette Hors CE';
        ESK003Lbl: Label 'Palette CE';
        arExport: Boolean;
        CodeCaptionLbl: Label 'Code';
        DescCaptionLbl: Label 'Désignation';
        QtyCaptionLbl: Label 'Qté livrée';
        // VATRegNoHdrCaptionLbl: Label 'V.A.T. Number  %1';

        gSalesOrderHeaderPrinted: Boolean;
        gSalesShipmentHeaderPrinted: Boolean;
        gSalesInvoiceHeaderPrinted: Boolean;
        gPackingLineHeaderPrinted: Boolean;
        gReportHeaderPrinted: Boolean;
        gI: Integer;
        gY: Integer;
        gDocNo: array[128] of Code[20];
        SalesOrdersCaptionLbl: Label 'Sales Orders';
        SalesInvoicesCaptionLbl: Label 'Sales Invoices';
        SalesShipmentsCaptionLbl: Label 'Sales Shipments';
        ActiveRefCaptionLbl: Label 'Active Reference';
        PackingNoCaptionLbl: Label 'Packing No.';
        TotalGrossWeightCaptionLbl: Label 'Total Gross Weight';
        TotalNetWeightCaptionLbl: Label 'Total Net Weight';
        gCountOrder: Integer;
        gCountShipment: Integer;
        gCountInvoice: Integer;


    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(5)) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; PDisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := PDisplayAsmInfo;
    end;

    local procedure FormatAddressFields(SalesShipmentHeader: Record "Sales Shipment Header");
    var
        lCustomer: Record Customer;
    begin
        FormatAddr.GetCompanyAddr(SalesShipmentHeader."Responsibility Center", RespCenter, CompanyInfo, CompanyAddr);
        FormatAddr.SalesShptShipTo(ShipToAddr, SalesShipmentHeader);
        ShowCustAddr := FormatAddr.SalesShptBillTo(CustAddr, ShipToAddr, SalesShipmentHeader);

        // ANI ESKVN1.0
        FormatAddr.SalesShptSellTo(SellToAddr, "Sales Shipment Header");
        //IF NOT ShowCustAddr THEN CLEAR(CustAddr);
        // FIN ANI ESKVN1.0
        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Invoice Customer No.") THEN
            lCustomer.INIT();
        IF CustAddr[7] = '' THEN CustAddr[7] := SalesShipmentHeader."Invoice Customer No.";
        //IF CustAddr[8] = '' THEN CustAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");

        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Sell-to Customer No.") THEN
            lCustomer.INIT();
        IF SellToAddr[7] = '' THEN SellToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF SellToAddr[8] = '' THEN SellToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
        IF ShipToAddr[7] = '' THEN ShipToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF ShipToAddr[8] = '' THEN ShipToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
    end;

    local procedure FormatDocumentFields(SalesShipmentHeader: Record "Sales Shipment Header");
    begin
        WITH SalesShipmentHeader DO BEGIN
            FormatDocument.SetSalesPerson(SalesPurchPerson, "Salesperson Code", SalesPersonText);
            ReferenceText := FormatDocument.SetText("Your Reference" <> '', Format(FIELDCAPTION("Your Reference")));

            // ANI ESKVN1.0
            // BL Chiffré / TVA - ShowPrices
            IF "Currency Code" = '' THEN BEGIN
                GLSetup.TESTFIELD("LCY Code");
                TotalText := STRSUBSTNO(Text003Lbl, GLSetup."LCY Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, GLSetup."LCY Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, GLSetup."LCY Code");
            END ELSE BEGIN
                TotalText := STRSUBSTNO(Text003Lbl, "Currency Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, "Currency Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, "Currency Code");
            END;

            FormatDocument.SetShipmentMethod(ShipmentMethod, "Shipment Method Code", SalesShipmentHeader."Language Code");
            SetShippingAgent(ShippingAgent, "Shipping Agent Code");
            SetShippingAgentServices(ShippingAgentServices, "Shipping Agent Code", "Shipping Agent Service Code");

            SalesHeader.RESET();
            CodeUtilisateurCreation := '';
            /*
            IF SalesHeader.GET(SalesHeader."Document Type"::Order, "Order No.") THEN
              CodeUtilisateurCreation := SalesHeader."Code utilisateur création";
            */
            // FIN ANI ESKVN1.0


        END;

    end;

    local procedure GetDocumentCaption() Result: Text[250];
    var
        LSalesShptHeader: Record "Sales Shipment Header";
        Txt50001Lbl: Label 'Shipment No. %1 - %2', Comment = '%1=Packing List No.; %2=Date Format';
        Text50002Lbl: Label 'COPY Shipment No. %1 - %2', Comment = '%1=Packing List No.; %2=Date Format';
    begin
        // ANI ESKVN1.0
        IF LSalesShptHeader.GET("Sales Shipment Header"."No.") THEN;

        IF LSalesShptHeader."No. Printed" = 0 THEN
            Result := STRSUBSTNO(Txt50001Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormatLbl))
        ELSE
            Result := STRSUBSTNO(Text50002Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormatLbl));

        EXIT(Result);
        // FIN ANI ESKVN1.0
    end;

    procedure SetShippingAgent(var pShipmentAgent: Record "Shipping Agent"; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgent.INIT()
        ELSE
            pShipmentAgent.GET(Code);
    end;

    procedure SetShippingAgentServices(var pShipmentAgentServices: Record "Shipping Agent Services"; ShippingAgentCode: Code[10]; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgentServices.INIT()
        ELSE
            pShipmentAgentServices.GET(ShippingAgentCode, Code);
    end;

    procedure GetWhseEmployeeName(SalesShipmentHeader: Record "Sales Shipment Header") WhseEmplName: Text;
    var
        lWhseEmpl: Record "Warehouse Employee";
    begin
        WhseEmplName := '';
        IF lWhseEmpl.GET(SalesShipmentHeader.Preparateur, SalesShipmentHeader."Location Code") THEN
            WhseEmplName := SalesShipmentHeader.Preparateur;
    end;

    local procedure InitPageOption();
    begin
        DisplayAssemblyInformation := TRUE; // coché par défaut Redmine #688
        PrintLogo := TRUE;
        PrintForFax := FALSE;
        no_fax := '';
        Attention_fax := '';
        nompdf := '';
        Objet_fax := '';
    end;

    local procedure MakeExcelHeader();
    var
        lRowNo: Integer;
        lColNo: Integer;
    begin
        lRowNo := 1;
        lColNo := 1;
        EnterCell(lRowNo, lColNo, Format(COMPANYNAME), TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text); //1/1
        EnterCell(lRowNo + 1, lColNo, SelltoAddrCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo, SellToAddr[1], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo, SellToAddr[2], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo, SellToAddr[3], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo, SellToAddr[4], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 6, lColNo, SellToAddr[5], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 7, lColNo, SellToAddr[6], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 8, lColNo, SellToAddr[7], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 9, lColNo, SellToAddr[8], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);//10/1
        lRowNo := 2;
        lColNo := 3;
        EnterCell(lRowNo, lColNo, BilltoAddrCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);//1/5
        EnterCell(lRowNo + 1, lColNo, CustAddr[1], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo, CustAddr[2], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo, CustAddr[3], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo, CustAddr[4], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo, CustAddr[5], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 6, lColNo, CustAddr[6], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 7, lColNo, CustAddr[7], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 8, lColNo, CustAddr[8], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);//10/5
        lRowNo := 2;
        lColNo := 5;
        EnterCell(lRowNo, lColNo, ShipToAddr[1], TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);//1/10
        EnterCell(lRowNo + 1, lColNo, ShipToAddr[2], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo, ShipToAddr[3], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo, ShipToAddr[4], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo, ShipToAddr[5], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo, ShipToAddr[6], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 6, lColNo, ShipToAddr[7], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 7, lColNo, ShipToAddr[8], FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text); //8/10
        lRowNo := 11;
        lColNo := 1;
        EnterCell(lRowNo, lColNo, Format(nompdf + '  ' + no_fax + '  ' + Objet_fax), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text); //11/1
        EnterCell(lRowNo + 1, lColNo, GetDocumentCaption(), TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo, LocationCaptionLbl, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo + 1, "Sales Shipment Header"."Location Code", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo + 2, RequestedDeliveryDateCaptionLbl, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 2, lColNo + 3, FORMAT("Sales Shipment Header"."Requested Delivery Date", 0, DateFormatLbl), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        lColNo := 1;
        EnterCell(lRowNo + 3, lColNo, Format("Sales Shipment Header".FIELDCAPTION("Order No.")), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo + 1, "Sales Shipment Header"."Order No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo + 2, ShippingAgentCaptionLbl, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 3, lColNo + 3, ShippingAgent.Name, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        lColNo := 1;
        EnterCell(lRowNo + 4, lColNo, ExternalDocNoCaptionLbl, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo + 1, "Sales Shipment Header"."External Document No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo + 2, ShippingAgentSrvCaptionLbl, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 4, lColNo + 3, ShippingAgentServices.Description, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        lColNo := 1;
        EnterCell(lRowNo + 5, lColNo, SalesPersonText, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo + 1, SalesPurchPerson.Name, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo + 2, Format("Sales Shipment Header".FIELDCAPTION("Shipment Method Code")), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        EnterCell(lRowNo + 5, lColNo + 3, "Sales Shipment Header"."Shipment Method Code", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
    end;

    local procedure MakeExcelBodyHeader(pHeaderFor: Integer);
    var
        lRowNo: Integer;
        lColNo: Integer;
    begin
        CASE pHeaderFor OF
            1: // Commande
                BEGIN
                    lRowNo := 18;
                    lColNo := 1;
                    EnterCell(lRowNo, lColNo, SalesOrdersCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    TempExcelBuf.SetCurrent(lRowNo, lColNo);
                END;
            2: // Expeditions ventes
                BEGIN
                    lRowNo := 18;
                    lColNo := 2;
                    EnterCell(lRowNo, lColNo, SalesShipmentsCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                END;
            3: // Factures ventes
                BEGIN
                    lRowNo := 18;
                    lColNo := 3;
                    EnterCell(lRowNo, lColNo, SalesInvoicesCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                END;
            4: // Details
                BEGIN
                    lRowNo := 1;
                    lColNo := 1;
                    EnterCell(lRowNo, lColNo, PackingNoCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    EnterCell(lRowNo, lColNo + 1, CodeCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    EnterCell(lRowNo, lColNo + 2, ActiveRefCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    EnterCell(lRowNo, lColNo + 3, Format("Packing List Line".FIELDCAPTION(Description)), TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    EnterCell(lRowNo, lColNo + 4, QtyCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    TempExcelBuf.SetCurrent(lRowNo, lColNo);
                END;
        END;
    end;

    local procedure MakeExcelBodyDetail(pMakeBodyFor: Integer);
    var
        lRowNo: Integer;
        lColNo: Integer;
        Value_L: Variant;
    begin
        CASE pMakeBodyFor OF
            1: // Commandes ventes
                BEGIN
                    gCountOrder += 1;
                    lColNo := 1;
                    EnterCell(gCountOrder, lColNo, "Sales Header"."No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    TempExcelBuf.SetCurrent(gCountOrder, lColNo);
                END;
            2: // Expeditions ventes
                BEGIN
                    gCountShipment += 1;
                    lColNo := 2;
                    EnterCell(gCountShipment, lColNo, SalesShipmentHeader2."No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    TempExcelBuf.SetCurrent(gCountShipment, lColNo);
                END;
            3: // Factures ventes
                BEGIN
                    gCountInvoice += 1;
                    lColNo := 3;
                    EnterCell(gCountInvoice, lColNo, "Sales Invoice Line"."Document No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    TempExcelBuf.SetCurrent(gCountInvoice, lColNo);
                END;
            4: // Details
                BEGIN
                    TempExcelBuf.UTgetGlobalValue('CurrentRow', Value_L);
                    Evaluate(lRowNo, Value_L);
                    lRowNo += 1;
                    lColNo := 1;
                    IF TxtDétailColis = '' THEN
                        EnterCell(lRowNo, lColNo, "Packing List Line"."Package No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text)
                    ELSE
                        EnterCell(lRowNo, lColNo, "Packing List Line"."Package No." + ' ' + TxtDétailColis + ' ' + STRSUBSTNO(Text50001Lbl, DetailColis."Package Weight"), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo += 1;
                    EnterCell(lRowNo, lColNo, "Packing List Line"."Item No.", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo += 1;
                    EnterCell(lRowNo, lColNo, "Packing List Line"."Ref. Active", FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo += 1;
                    EnterCell(lRowNo, lColNo, TxtDescription, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo += 1;
                    EnterCell(lRowNo, lColNo, FORMAT("Packing List Line"."Line Quantity"), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
                    TempExcelBuf.SetCurrent(lRowNo, lColNo);
                END;
            5: //Total
                BEGIN
                    IF TempExcelBuf.FINDLAST() THEN;
                    IF TempPackingListPackage.FINDSET() THEN
                        TempPackingListPackage.CALCSUMS("Package Weight", "Net Weight");
                    lRowNo := TempExcelBuf."Row No." + 2;
                    lColNo := 1;
                    EnterCell(lRowNo, lColNo, TotalGrossWeightCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo := 2;
                    EnterCell(lRowNo, lColNo, FORMAT(TempPackingListPackage."Package Weight"), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
                    lRowNo += 1;
                    lColNo := 1;
                    EnterCell(lRowNo, lColNo, TotalNetWeightCaptionLbl, TRUE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                    lColNo := 2;
                    EnterCell(lRowNo, lColNo, FORMAT(TempPackingListPackage."Net Weight"), FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
                    TempExcelBuf.SetCurrent(lRowNo, lColNo);
                END;
        END;
    end;

    local procedure EnterCell(RowNo: Integer; ColumnNo: Integer; CellValue: Text[250]; Bold: Boolean; UnderLine: Boolean; NumberFormat: Text[30]; CellType: Option);
    begin
        TempExcelBuf.INIT();
        TempExcelBuf.VALIDATE("Row No.", RowNo);
        TempExcelBuf.VALIDATE("Column No.", ColumnNo);
        TempExcelBuf."Cell Value as Text" := CellValue;
        TempExcelBuf.Formula := '';
        TempExcelBuf.Bold := Bold;
        TempExcelBuf.Underline := UnderLine;
        TempExcelBuf.NumberFormat := NumberFormat;
        TempExcelBuf."Cell Type" := CellType;
        TempExcelBuf.Insert();
    end;

    local procedure CalcTotals(pPackingListLine: Record "Packing List Line");
    var
        lPackingListPackage: Record "Packing List Package";
    begin
        IF lPackingListPackage.GET(pPackingListLine."Packing List No.", pPackingListLine."Package No.") THEN
            IF NOT TempPackingListPackage.GET(lPackingListPackage."Packing List No.", lPackingListPackage."Package No.") THEN BEGIN
                TempPackingListPackage.INIT();
                TempPackingListPackage.TRANSFERFIELDS(lPackingListPackage);
                TempPackingListPackage.Insert();
            END;
    end;
}

