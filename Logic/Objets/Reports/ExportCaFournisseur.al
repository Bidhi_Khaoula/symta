Report 50154 "Export Ca Fournisseur"
{
    ProcessingOnly = true;
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    Caption = 'Export Ca Fournisseur';
    dataset
    {
        dataitem(Vendor; Vendor)
        {
            RequestFilterFields = "No.";

            dataitem("Vendor Ledger Entry"; "Vendor Ledger Entry")
            {
                DataItemLink = "Vendor No." = FIELD("No.");
                Trigger OnPreDataItem()
                BEGIN
                    // Ecritures fournisseur de type factures et avoirs … partir de la date saisie
                    "Vendor Ledger Entry".SETFILTER("Document Type", '%1|%2', "Vendor Ledger Entry"."Document Type"::Invoice, "Vendor Ledger Entry"."Document Type"::"Credit Memo");
                    "Vendor Ledger Entry".SETRANGE("Document Date", gDateFrom, TODAY());
                END;

                Trigger OnAfterGetRecord()
                BEGIN
                    MakeExcelDataBody25();
                END;

            }
            dataitem("Item Ledger Entry"; "Item Ledger Entry")
            {
                DataItemLink = "Source No." = FIELD("No.");
                Trigger OnPreDataItem()
                BEGIN
                    // Ecritures articles de type achat du magasin SP avec un cout total prévu <> 0 et en fonction de certains types
                    "Item Ledger Entry".SETRANGE("Location Code", 'SP');
                    "Item Ledger Entry".SETRANGE("Entry Type", "Item Ledger Entry"."Entry Type"::Purchase);
                    "Item Ledger Entry".SETFILTER("Cost Amount (Expected)", '<>%1', 0);
                    "Item Ledger Entry".SETFILTER("Return Purchase Request", '<>%1', "Item Ledger Entry"."Return Purchase Request"::Soldé);
                    "Item Ledger Entry".SETFILTER("Warehouse Receipt Request", '<>%1', "Item Ledger Entry"."Warehouse Receipt Request"::Ended);
                END;

                Trigger OnAfterGetRecord()
                BEGIN
                    MakeExcelDataBody32();
                END;

            }

            DataItem("Purchase Line"; "Purchase Line")
            {
                DataItemLink = "Buy-from Vendor No." = FIELD("No.");
                Trigger OnPreDataItem()
                BEGIN
                    // Lignes commande achat en reliquat
                    //"Purchase Line".SETRANGE("Buy-from Vendor No.", Vendor."No.");
                    "Purchase Line".SETRANGE("Document Type", "Purchase Line"."Document Type"::Order);
                    "Purchase Line".SETFILTER("Outstanding Quantity", '>%1', 0);
                END;

                Trigger OnAfterGetRecord()
                BEGIN
                    IF gReliquat THEN
                        MakeExcelDataBody39();
                END;

            }
            Trigger OnPreDataItem()
            BEGIN
                IF (gDateFrom = 0D) THEN
                    ERROR('Date de début obligatoire.');

                IF (Vendor.GETFILTER(Vendor."No.") = '') THEN
                    IF NOT CONFIRM('Voulez vous lancer le traitement SANS filtre fournisseur') THEN
                        ERROR('Traitement interrompu par l''utilisateur');
            END;

        }


    }

    REQUESTPAGE
    {
        layout
        {
            area(content)
            {
                field(DateFrom; gDateFrom)
                {
                    Caption = 'A partir du';
                    ToolTip = 'Specifies the value of the A partir du field.';
                }
                field(Reliquat; gReliquat)
                {
                    ApplicationArea = all;
                    Caption = 'Avec reliquat';
                    ToolTip = 'Specifies the value of the Avec reliquat field.';
                }
            }
        }

    }
    Trigger OnPreReport()
    BEGIN
        MakeExcelInfo();
    END;

    Trigger OnPostReport()
    BEGIN
        CreateExcelbook();
    END;

    VAR
        TempExcelBuf: Record "Excel Buffer" TEMPORARY;
        gDateFrom: Date;
        gReliquat: Boolean;
        Excel002Lbl: Label 'Data';


    PROCEDURE MakeExcelInfo();
    BEGIN
        // Export Excel
        MakeExcelDataHeader();
    END;

    LOCAL PROCEDURE MakeExcelDataHeader();
    BEGIN
        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn('Nø Fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Nom fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Type', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Référence', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date Référence', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Quantité', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Coût unitaire net', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Devise', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Montant', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    END;

    PROCEDURE MakeExcelDataBody25();
    VAR
        lFournisseur: Text[100];
        lType: Text[100];
        lReference: Text[50];
        lDateRef: Text[20];
        lDate: Text[20];
        lMontant: Text[50];
        FourLbl: Label '%1 - %2', Comment = '%1 = No ;%2=Name';
    BEGIN
        // ******************
        // FACTURES ET AVOIRS
        // ******************
        lFournisseur := STRSUBSTNO(FourLbl, Vendor."No.", Vendor.Name);
        lType := 'Factures/Avoirs';//FORMAT("Vendor Ledger Entry"."Document Type");
        lReference := "Vendor Ledger Entry"."Document No.";
        lDateRef := FormatDate("Vendor Ledger Entry"."Document Date");
        lDate := FormatYYYYMM("Vendor Ledger Entry"."Document Date");
        lMontant := FormatDecimal("Vendor Ledger Entry"."Purchase (LCY)" * -1);

        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn(Vendor."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Vendor.Name, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lFournisseur, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lDate, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lType, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lReference, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(lDateRef, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lMontant, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
    END;

    PROCEDURE MakeExcelDataBody32();
    VAR
        lFournisseur: Text[100];
        lType: Text[100];
        lReference: Text[50];
        lDateRef: Text[20];
        lDate: Text[20];
        lMontant: Text[50];
        FourLbl: Label '%1 - %2', Comment = '%1 = No ;%2=Name';
    BEGIN
        // **********
        // A FACTURER
        // **********
        "Item Ledger Entry".CALCFIELDS("Cost Amount (Expected)");
        lFournisseur := STRSUBSTNO(FourLbl, Vendor."No.", Vendor.Name);
        lType := 'Ecriture achat non facturée';
        //STRSUBSTNO('Ecriture achat non facturée - Demande retour achat = %1 - Demande réception magasin = %2',
        //          "Item Ledger Entry"."Return Purchase Request",
        //          "Item Ledger Entry"."Warehouse Receipt Request");
        lReference := "Item Ledger Entry"."Document No.";
        lDateRef := FormatDate("Item Ledger Entry"."Posting Date");
        lDate := FormatYYYYMM(TODAY());
        lMontant := FormatDecimal("Item Ledger Entry"."Cost Amount (Expected)");

        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn(Vendor."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Vendor.Name, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lFournisseur, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lDate, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lType, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lReference, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(lDateRef, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lMontant, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
    END;

    PROCEDURE MakeExcelDataBody39();
    VAR
        lCurrencyExchangeRate: Record "Currency Exchange Rate";
        lFournisseur: Text[100];
        lType: Text[100];
        lReference: Text[50];
        lDateRef: Text[20];
        lDate: Text[20];
        lMontant: Text[50];
        lCoutUnitaire: Text[50];
        lQuantite: Text[50];
        lDevise: Code[10];
        lExchangeRateAmt: Decimal;
        lExchangeRateDate: Date;
        FourLbl: Label '%1 - %2', Comment = '%1 = No ;%2=Name';
    BEGIN
        // ********
        // RELIQUAT
        // ********
        lFournisseur := STRSUBSTNO(FourLbl, Vendor."No.", Vendor.Name);
        lType := 'Reliquat';
        lReference := "Purchase Line"."Document No.";
        lDateRef := FormatDate("Purchase Line"."Order Date");
        lDate := FormatYYYYMM(TODAY());
        lQuantite := FormatDecimal("Purchase Line"."Outstanding Quantity");
        lCoutUnitaire := FormatDecimal("Purchase Line"."Net Unit Cost ctrl fac");
        IF ("Purchase Line"."Currency Code" = '') THEN BEGIN
            lDevise := '';
            lMontant := FormatDecimal("Purchase Line"."Outstanding Quantity" * "Purchase Line"."Net Unit Cost ctrl fac");
        END
        ELSE BEGIN
            lCurrencyExchangeRate.GetLastestExchangeRate("Purchase Line"."Currency Code", lExchangeRateDate, lExchangeRateAmt);
            lDevise := FormatDecimal(lExchangeRateAmt);
            lMontant := FormatDecimal("Purchase Line"."Outstanding Quantity" * "Purchase Line"."Net Unit Cost ctrl fac" * lExchangeRateAmt);
        END;

        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn(Vendor."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Vendor.Name, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lFournisseur, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lDate, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lType, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lReference, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(lDateRef, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lQuantite, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(lCoutUnitaire, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(lDevise, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(lMontant, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
    END;

    PROCEDURE CreateExcelbook();
    BEGIN
        // Export Excel
        TempExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, 'CA Fournisseurs', COMPANYNAME, USERID)
    END;

    PROCEDURE FormatDecimal(Value: Decimal): Text[10];
    BEGIN
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    END;

    LOCAL PROCEDURE FormatDate(pDate: Date): Text[10];
    BEGIN
        EXIT(FORMAT(pDate, 0, '<day,2>/<month,2>/<year4>'));
    END;

    LOCAL PROCEDURE FormatYYYYMM(pDate: Date): Text[10];
    BEGIN
        EXIT(FORMAT(pDate, 0, '<year4>-<month,2>'));
    END;


}

