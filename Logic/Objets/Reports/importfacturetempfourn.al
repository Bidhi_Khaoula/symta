report 50072 "import facture temp. fourn."
{

    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'import facture temp. fourn.';

    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number);

            trigger OnAfterGetRecord();
            var
                RefField: FieldRef;
                RefRec: RecordRef;
                TmpInteger: Integer;
                TmpDecimal: Decimal;
                TmpDate: Date;
            begin

                No_ligne_excel += 1;
                Fenetre.UPDATE(1, ROUND(No_ligne_excel / nb * 10000, 1));
                IF NOT GetCell(tab_lettre[1]) THEN CurrReport.BREAK();

                ligne_table := ligne_table + 1;
                Ligne_import_facture.INIT();
                Ligne_import_facture."Type ligne" := Ligne_import_facture."Type ligne"::Ligne;
                Ligne_import_facture."No séquence" := ligne_table;
                //Champs imformation
                Ligne_import_facture."Date import" := TODAY;
                Ligne_import_facture."heure import" := TIME;
                Evaluate(Ligne_import_facture."utilisateur import", USERID);
                Ligne_import_facture."Document Type" := Ligne_import_facture."Document Type"::Invoice;
                Ligne_import_facture."Code fournisseur" := "Code Fournisseur";
                Ligne_import_facture.Insert();

                RefRec.GETTABLE(Ligne_import_facture);

                FOR i := 1 TO ARRAYLEN(tab_lettre) DO BEGIN
                    IF GetCell(tab_lettre[i]) THEN;

                    RefField := RefRec.FIELD(tab_champ_table[i]);

                    CASE FORMAT(RefField.TYPE) OF
                        'Integer':
                            BEGIN
                                IF TempExcelBuf."Cell Value as Text" <> '' THEN
                                    EVALUATE(TmpInteger, TempExcelBuf."Cell Value as Text")
                                ELSE
                                    TmpInteger := 0;
                                RefField.VALUE := TmpInteger;
                            END;

                        'Decimal':
                            BEGIN
                                IF TempExcelBuf."Cell Value as Text" <> '' THEN
                                    EVALUATE(TmpDecimal, TempExcelBuf."Cell Value as Text")
                                ELSE
                                    TmpDecimal := 0;
                                RefField.VALUE := TmpDecimal;
                            END;
                        'Date':
                            BEGIN
                                IF TempExcelBuf."Cell Value as Text" <> '' THEN
                                    EVALUATE(TmpDate, TempExcelBuf."Cell Value as Text")
                                ELSE
                                    TmpDate := 0D;
                                RefField.VALUE := TmpDate;
                            END;

                        ELSE
                            RefField.VALUE := TempExcelBuf."Cell Value as Text";
                    END;

                    RefRec.MODIFY();
                END;

                Ligne_import_facture.GET(ligne_table, Ligne_import_facture."Type ligne"::Ligne);

                IF factureEnCours <> Ligne_import_facture."No Facture" THEN BEGIN
                    Entete_import_facture.INIT();
                    Entete_import_facture."Document Type" := Entete_import_facture."Document Type"::Invoice;
                    Entete_import_facture."No séquence" := Ligne_import_facture."No séquence";
                    Entete_import_facture."Type ligne" := Entete_import_facture."Type ligne"::Entete;

                    // MC Le 13-09-2011 => Test de l'existence d'une ligne pour la facture
                    //Avant l'insertion on vérifie qu'il n'existe pas déjà des lignes pour cette facture
                    //Sinon on met en erreur car ce n'est pas gérable.
                    Entete_import_factureToVerif.RESET();
                    Entete_import_facture.SETRANGE("Type ligne", Entete_import_facture."Type ligne"::Entete);
                    Entete_import_facture.SETRANGE("Document Type", Entete_import_facture."Document Type"::Invoice);
                    Entete_import_facture.SETFILTER("No séquence", '<>%1', Ligne_import_facture."No séquence");
                    Entete_import_facture.SETRANGE("No Facture", Ligne_import_facture."No Facture");
                    IF Entete_import_facture.FINDFIRST() THEN
                        ERROR(Error001Lbl);
                    // FIN MC Le 13-09-2011


                    Entete_import_facture."No Facture" := Ligne_import_facture."No Facture";
                    //Champs imformation
                    Entete_import_facture."Code fournisseur" := "Code Fournisseur";
                    Entete_import_facture."Date import" := TODAY;
                    Entete_import_facture."heure import" := TIME;
                    Evaluate(Entete_import_facture."utilisateur import", USERID);
                    Entete_import_facture.Insert();
                    factureEnCours := Ligne_import_facture."No Facture"
                END;
            end;

            trigger OnPostDataItem();
            begin
                MESSAGE('Import terminé');
            end;

            trigger OnPreDataItem();
            begin

                TempExcelBuf.FINDLAST();
                nb := TempExcelBuf."Row No.";
                Integer.SETRANGE(Number, 0, TempExcelBuf."Row No.");

                IF "Nom de colonne(premier ligne)" THEN
                    No_ligne_excel := 1
                ELSE
                    No_ligne_excel := 0;

                Fenetre.OPEN('Import des factures @1@@@@@@@@@@');

                lettre := 'A';
                tab_lettre[1] := lettre;
                //en prevision d'un parametrage
                FOR i := 2 TO ARRAYLEN(tab_lettre) DO BEGIN
                    lettre[1] := lettre[1] + 1;
                    tab_lettre[i] := lettre;
                END;

                i := 0;
                fields.SETRANGE(TableNo, DATABASE::"Import facture tempo. fourn.");
                fields.SETRANGE("No.", 100, 254);
                IF fields.FINDFIRST() THEN
                    REPEAT
                        i := i + 1;
                        tab_champ_table[i] := fields."No.";
                    UNTIL fields.NEXT() = 0;

                IF Ligne_import_facture.FINDLAST() THEN;
                ligne_table := Ligne_import_facture."No séquence";
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Code Fournisseur Name"; "Code Fournisseur")
                {
                    Caption = 'Code Fournisseur';
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Fournisseur field.';
                }
                field("Nom de colonne(premier ligne) Name"; "Nom de colonne(premier ligne)")
                {
                    Caption = 'Nom de colonne(premier ligne)';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom de colonne(premier ligne) field.';
                }
            }
        }

        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', Locked = true;
        begin
            IF CloseAction = ACTION::OK THEN BEGIN
                Evaluate(FileName, FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok));
                IF FileName = '' THEN
                    EXIT(FALSE);

                SheetName := TempExcelBuf.SelectSheetsName(FileName);
                IF SheetName = '' THEN
                    EXIT(FALSE);
            END;
        end;
    }

    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin
        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var
        Ligne_import_facture: Record "Import facture tempo. fourn.";
        Entete_import_facture: Record "Import facture tempo. fourn.";
        Entete_import_factureToVerif: Record "Import facture tempo. fourn.";
        TempExcelBuf: Record "Excel Buffer" temporary;
        "fields": Record Field;
        ConvertAsciAnsii: Codeunit "ANSI  <->  ASCII converter";
        FileName: Text[250];
        UploadedFileName: Text[1024];
        No_ligne_excel: Integer;
        SheetName: Text[250];
        nb: Integer;
        Fenetre: Dialog;
        tab_lettre: array[13] of Text[1];
        tab_champ_table: array[13] of Integer;
        lettre: Text[1];
        i: Integer;
        ligne_table: Integer;
        "Code Fournisseur": Code[20];
        factureEnCours: Code[20];
        "Nom de colonne(premier ligne)": Boolean;
        Error001Lbl: Label 'Cette facture a déjà été raprochée ou est en cours de rapprochement.';
    //**********************Non utiliser*************************************************************
    // local procedure ReadExcelSheet();
    // begin
    //     //Lecture de la feuille excel
    //     IF UploadedFileName = '' THEN
    //         UploadFile()
    //     ELSE
    //         FileName := UploadedFileName;

    //     TempExcelBuf.OpenBook(FileName, SheetName);
    //     TempExcelBuf.ReadSheet();
    // end;
    //**********************************************************************************************
    procedure UploadFile();
    var
        CommonDialogMgt: Codeunit "File Management";
        ClientFileName: Text[1024];
        Text015Lbl: Label 'Choisir le fichier';
    begin

        Evaluate(UploadedFileName, CommonDialogMgt.UploadFile(Text015Lbl, ClientFileName));
        Evaluate(FileName, UploadedFileName);
    end;

    procedure GetCell(Row: Text[5]): Boolean
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.INIT();
        TempExcelBuf.SETRANGE("Row No.", No_ligne_excel);
        TempExcelBuf.SETRANGE(xlColID, Row);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure SetDescription(var Desc1: Text[250]; var Desc2: Text[200]);
    var
        Desc: Text[250];
    begin
        Desc := ConvertAsciAnsii.Ansi2Ascii(Desc1);
        IF STRLEN(Desc) > 50 THEN BEGIN
            Desc1 := COPYSTR(Desc, 1, 50);
            Desc2 := COPYSTR(Desc, 50, 50);
        END
        ELSE
            Desc1 := Desc;
    end;
}

