report 50044 "Vendor - Balance to Date SYM" //321
{
    ApplicationArea = All;
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = RDLC;
    RDLCLayout = './Objets/Reports/Layouts/Vendor - Balance to Date.rdlc';
    Caption = 'Vendor - Balance to Date';
    dataset
    {
        dataitem(Vendor; Vendor)
        {
            DataItemTableView = SORTING("Search Name")
                                ORDER(Ascending);
            PrintOnlyIfDetail = true;
            RequestFilterFields = "No.", "Search Name", Blocked, "Date Filter";
            column(StrNoVenGetMaxDtFilter; STRSUBSTNO(Text000Lbl, FORMAT(GETRANGEMAX("Date Filter"))))
            {
            }
            column(CompanyName; COMPANYNAME)
            {
            }
            column(VendFilter; VendFilter)
            {
            }
            column(PrintAmountInLCY; PrintAmountInLCY)
            {
            }
            column(PrintOnePrPage; PrintOnePrPage)
            {
            }
            column(VendorCaption; TABLECAPTION + ': ' + VendFilter)
            {
            }
            column(No_Vendor; "No.")
            {
            }
            column(Name_Vendor; Name)
            {
            }
            column(PhoneNo_Vendor; "Phone No.")
            {
                IncludeCaption = true;
            }
            column(PaymentMethodCode_Vendor; "Payment Method Code")
            {
            }
            column(VendorBalancetoDateCptn; VendorBalancetoDateCptnLbl)
            {
            }
            column(PageNoCaption; PageNoCaptionLbl)
            {
            }
            column(AllamountsareinLCYCaption; AllamountsareinLCYCaptionLbl)
            {
            }
            column(PostingDateCption; PostingDateCptionLbl)
            {
            }
            column(OriginalAmtCaption; OriginalAmtCaptionLbl)
            {
            }
            dataitem(VendLedgEntry3; "Vendor Ledger Entry")
            {
                DataItemTableView = SORTING("Entry No.");
                column(PostDt_VendLedgEntry3; FORMAT("Posting Date"))
                {
                }
                column(DocType_VendLedgEntry3; "Document Type")
                {
                    IncludeCaption = true;
                }
                column(DocNo_VendLedgEntry3; "Document No.")
                {
                    IncludeCaption = true;
                }
                column(ExternalDocNo_VendLedgEntry3; "External Document No.")
                {
                }
                column(Desc_VendLedgEntry3; Description)
                {
                    IncludeCaption = true;
                }
                column(OriginalAmt; OriginalAmt)
                {
                    AutoFormatExpression = CurrencyCode;
                    AutoFormatType = 1;
                }
                column(EntryNo_VendLedgEntry3; "Entry No.")
                {
                    IncludeCaption = true;
                }
                column(CurrencyCode; CurrencyCode)
                {
                }
                column(Date_echeance; FORMAT("Due Date"))
                {
                }
                dataitem("Detailed Vendor Ledg. Entry"; "Detailed Vendor Ledg. Entry")
                {
                    DataItemLink = "Vendor Ledger Entry No." = FIELD("Entry No."),
                                   "Posting Date" = FIELD("Date Filter");
                    DataItemTableView = SORTING("Vendor Ledger Entry No.", "Posting Date")
                                        WHERE("Entry Type" = FILTER(<> "Initial Entry"));
                    column(EntryTp_DtldVendLedgEntry; "Entry Type")
                    {
                    }
                    column(PostDate_DtldVendLedEnt; FORMAT("Posting Date"))
                    {
                    }
                    column(DocType_DtldVendLedEnt; "Document Type")
                    {
                    }
                    column(DocNo_DtldVendLedgEntry; "Document No.")
                    {
                    }
                    column(Amt; Amt)
                    {
                        AutoFormatExpression = CurrencyCode;
                        AutoFormatType = 1;
                    }
                    column(CurrencyCode1; CurrencyCode)
                    {
                    }
                    column(DtldVendtLedgEntryNum; DtldVendtLedgEntryNum)
                    {
                    }
                    column(RemainingAmt; RemainingAmt)
                    {
                        AutoFormatExpression = CurrencyCode;
                        AutoFormatType = 1;
                    }

                    trigger OnAfterGetRecord()
                    begin
                        IF NOT PrintUnappliedEntries THEN
                            IF Unapplied THEN
                                CurrReport.SKIP();
                        IF PrintAmountInLCY THEN BEGIN
                            Amt := "Amount (LCY)";
                            CurrencyCode := '';
                        END ELSE BEGIN
                            Amt := Amount;
                            CurrencyCode := "Currency Code";
                        END;
                        IF Amt = 0 THEN
                            CurrReport.SKIP();

                        DtldVendtLedgEntryNum := DtldVendtLedgEntryNum + 1;
                    end;

                    trigger OnPreDataItem()
                    begin
                        DtldVendtLedgEntryNum := 0;
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    IF PrintAmountInLCY THEN BEGIN
                        CALCFIELDS("Original Amt. (LCY)", "Remaining Amt. (LCY)");
                        OriginalAmt := "Original Amt. (LCY)";
                        RemainingAmt := "Remaining Amt. (LCY)";
                        CurrencyCode := '';
                    END ELSE BEGIN
                        CALCFIELDS("Original Amount", "Remaining Amount");
                        OriginalAmt := "Original Amount";
                        RemainingAmt := "Remaining Amount";
                        CurrencyCode := "Currency Code";
                    END;
                end;

                trigger OnPreDataItem()
                var
                    TempVendorLedgerEntry: Record "Vendor Ledger Entry" temporary;
                    ClosedEntryIncluded: Boolean;
                begin
                    RESET();
                    FilterVendorLedgerEntry(VendLedgEntry3);
                    IF FINDSET() THEN
                        REPEAT
                            IF NOT Open THEN
                                ClosedEntryIncluded := CheckVendEntryIncluded("Entry No.");
                            IF Open OR ClosedEntryIncluded THEN BEGIN
                                MARK(TRUE);
                                TempVendorLedgerEntry := VendLedgEntry3;
                                TempVendorLedgerEntry.Insert();
                            END;
                        UNTIL NEXT() = 0;

                    SETCURRENTKEY("Entry No.");
                    MARKEDONLY(TRUE);

                    AddVendorDimensionFilter(VendLedgEntry3);

                    CalcTotalVendorAmount(TempVendorLedgerEntry);
                end;
            }
            dataitem(Integer2; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    WHERE(Number = FILTER(1 ..));
                column(Name1_Vendor; Vendor.Name)
                {
                }
                column(CurrTotalBufferTotalAmt; TempCurrencyTotalBuffer."Total Amount")
                {
                    AutoFormatExpression = TempCurrencyTotalBuffer."Currency Code";
                    AutoFormatType = 1;
                }
                column(CurrTotalBufferCurrCode; TempCurrencyTotalBuffer."Currency Code")
                {
                }

                trigger OnAfterGetRecord()
                begin
                    IF Number = 1 THEN
                        OK := TempCurrencyTotalBuffer.FIND('-')
                    ELSE
                        OK := TempCurrencyTotalBuffer.NEXT() <> 0;
                    IF NOT OK THEN
                        CurrReport.BREAK();

                    TempCurrencyTotalBuffer2.UpdateTotal(
                      TempCurrencyTotalBuffer."Currency Code",
                      TempCurrencyTotalBuffer."Total Amount",
                      0,
                      Counter1);
                end;

                trigger OnPostDataItem()
                begin
                    TempCurrencyTotalBuffer.DELETEALL();
                end;

                trigger OnPreDataItem()
                begin
                    TempCurrencyTotalBuffer.SETFILTER("Total Amount", '<>0');
                end;
            }

            trigger OnAfterGetRecord()
            begin
                MaxDate := GETRANGEMAX("Date Filter");
                SETRANGE("Date Filter", 0D, MaxDate);
                CALCFIELDS("Net Change (LCY)", "Net Change");

                IF ((PrintAmountInLCY AND ("Net Change (LCY)" = 0)) OR
                    ((NOT PrintAmountInLCY) AND ("Net Change" = 0)))
                THEN
                    CurrReport.SKIP();
            end;

            trigger OnPreDataItem()
            begin
                IF gTriAlpha THEN
                    Vendor.SETCURRENTKEY("Search Name")
                ELSE
                    Vendor.SETCURRENTKEY("No.");
            end;
        }
        dataitem(Integer3; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = FILTER(1 ..));
            column(CurrTotalBuffer2CurrCode; TempCurrencyTotalBuffer2."Currency Code")
            {
            }
            column(CurrTotalBuffer2TotalAmt; TempCurrencyTotalBuffer2."Total Amount")
            {
                AutoFormatExpression = TempCurrencyTotalBuffer2."Currency Code";
                AutoFormatType = 1;
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }

            trigger OnAfterGetRecord()
            begin
                IF Number = 1 THEN
                    OK := TempCurrencyTotalBuffer2.FIND('-')
                ELSE
                    OK := TempCurrencyTotalBuffer2.NEXT() <> 0;
                IF NOT OK THEN
                    CurrReport.BREAK();
            end;

            trigger OnPostDataItem()
            begin
                TempCurrencyTotalBuffer2.DELETEALL();
            end;

            trigger OnPreDataItem()
            begin
                TempCurrencyTotalBuffer2.SETFILTER("Total Amount", '<>0');
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(ShowAmountsInLCY; PrintAmountInLCY)
                    {
                        Caption = 'Show Amounts in LCY';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Amounts in LCY field.';
                    }
                    field(PrintOnePrPageName; PrintOnePrPage)
                    {
                        Caption = 'New Page per Vendor';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the New Page per Vendor field.';
                    }
                    field(PrintUnappliedEntriesName; PrintUnappliedEntries)
                    {
                        Caption = 'Include Unapplied Entries';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Include Unapplied Entries field.';
                    }
                    field(gTriAlphaName; gTriAlpha)
                    {
                        Caption = 'Fournisseur : tri alpha';
                        ToolTip = 'Trier les fournisseurs par ordre alphabétique (non coché = tri par N°)';
                        ApplicationArea = All;
                    }
                    field(gDueDateFilterName; gDueDateFilter)
                    {
                        Caption = 'Filtre date d''échéance';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Filtre date d''échéance field.';
                    }
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPreReport()
    var
        FormatDocument: Codeunit "Format Document";
        VendLbl: Label 'Date d''échéance: ..%1', Comment = '%1 Date';
    begin
        VendFilter := FormatDocument.GetRecordFiltersWithCaptions(Vendor);

        // CFR le 26/10/2023 => Régie : filtre sur [Date échéance]
        IF (gDueDateFilter <> 0D) THEN BEGIN
            IF (VendFilter <> '') THEN VendFilter := VendFilter + ', ';
            VendFilter := VendFilter + STRSUBSTNO(VendLbl, gDueDateFilter);
        END;
        // FIN CFR le 26/10/2023
    end;

    var
        TempCurrencyTotalBuffer: Record "Currency Total Buffer" temporary;
        TempCurrencyTotalBuffer2: Record "Currency Total Buffer" temporary;
        Text000Lbl: Label 'Balance on %1', Comment = '%1 = Date';
        PrintAmountInLCY: Boolean;
        PrintOnePrPage: Boolean;
        VendFilter: Text;
        MaxDate: Date;
        OriginalAmt: Decimal;
        Amt: Decimal;
        RemainingAmt: Decimal;
        Counter1: Integer;
        DtldVendtLedgEntryNum: Integer;
        OK: Boolean;
        CurrencyCode: Code[10];
        PrintUnappliedEntries: Boolean;
        VendorBalancetoDateCptnLbl: Label 'Vendor - Balance to Date';
        PageNoCaptionLbl: Label 'Page';
        AllamountsareinLCYCaptionLbl: Label 'All amounts are in LCY.';
        PostingDateCptionLbl: Label 'Posting Date';
        OriginalAmtCaptionLbl: Label 'Amount';
        TotalCaptionLbl: Label 'Total';
        gTriAlpha: Boolean;
        gDueDateFilter: Date;

    procedure InitializeRequest(NewPrintAmountInLCY: Boolean; NewPrintOnePrPage: Boolean; NewPrintUnappliedEntries: Boolean)
    begin
        PrintAmountInLCY := NewPrintAmountInLCY;
        PrintOnePrPage := NewPrintOnePrPage;
        PrintUnappliedEntries := NewPrintUnappliedEntries;
    end;

    local procedure FilterVendorLedgerEntry(var VendorLedgerEntry: Record "Vendor Ledger Entry")
    begin
        WITH VendorLedgerEntry DO BEGIN
            SETCURRENTKEY("Vendor No.", "Posting Date");
            SETRANGE("Vendor No.", Vendor."No.");
            SETRANGE("Posting Date", 0D, MaxDate);
            // CFR le 26/10/2023 => Régie : filtre sur [Date échéance]
            IF (gDueDateFilter <> 0D) THEN
                SETRANGE("Due Date", 0D, gDueDateFilter);
            // FIN CFR le 26/10/2023

        END;
    end;

    local procedure AddVendorDimensionFilter(var VendorLedgerEntry: Record "Vendor Ledger Entry")
    begin
        WITH VendorLedgerEntry DO BEGIN
            IF Vendor.GETFILTER("Global Dimension 1 Filter") <> '' THEN
                SETRANGE("Global Dimension 1 Code", Vendor.GETFILTER("Global Dimension 1 Filter"));
            IF Vendor.GETFILTER("Global Dimension 2 Filter") <> '' THEN
                SETRANGE("Global Dimension 2 Code", Vendor.GETFILTER("Global Dimension 2 Filter"));
            IF Vendor.GETFILTER("Currency Filter") <> '' THEN
                SETRANGE("Currency Code", Vendor.GETFILTER("Currency Filter"));
        END;
    end;

    local procedure CalcTotalVendorAmount(var TempVendorLedgerEntry: Record "Vendor Ledger Entry" temporary)
    begin
        WITH TempVendorLedgerEntry DO BEGIN
            SETCURRENTKEY("Entry No.");
            SETRANGE("Date Filter", 0D, MaxDate);
            // CFR le 26/10/2023 => Régie : filtre sur [Date échéance]
            IF (gDueDateFilter <> 0D) THEN
                SETRANGE("Due Date", 0D, gDueDateFilter);
            // FIN CFR le 26/10/2023
            AddVendorDimensionFilter(TempVendorLedgerEntry);
            IF FINDSET() THEN
                REPEAT
                    IF PrintAmountInLCY THEN BEGIN
                        CALCFIELDS("Remaining Amt. (LCY)");
                        RemainingAmt := "Remaining Amt. (LCY)";
                        CurrencyCode := '';
                    END ELSE BEGIN
                        CALCFIELDS("Remaining Amount");
                        RemainingAmt := "Remaining Amount";
                        CurrencyCode := "Currency Code";
                    END;
                    IF RemainingAmt <> 0 THEN
                        TempCurrencyTotalBuffer.UpdateTotal(
                          CurrencyCode,
                          RemainingAmt,
                          0,
                          Counter1);
                UNTIL NEXT() = 0;
        END;
    end;

    local procedure CheckVendEntryIncluded(EntryNo: Integer): Boolean
    var
        VendorLedgerEntry: Record "Vendor Ledger Entry";
    begin
        // CFR le 26/10/2023 => Régie : filtre sur [Date échéance]
        IF (gDueDateFilter <> 0D) THEN
            VendorLedgerEntry.SETRANGE("Due Date", 0D, gDueDateFilter);
        // FIN CFR le 26/10/2023

        IF VendorLedgerEntry.GET(EntryNo) AND (VendorLedgerEntry."Posting Date" <= MaxDate) THEN BEGIN
            VendorLedgerEntry.SETRANGE("Date Filter", 0D, MaxDate);
            VendorLedgerEntry.CALCFIELDS("Remaining Amount");
            IF VendorLedgerEntry."Remaining Amount" <> 0 THEN
                EXIT(TRUE);
            IF PrintUnappliedEntries THEN
                EXIT(CheckUnappliedEntryExists(EntryNo));
        END;
        EXIT(FALSE);
    end;

    local procedure CheckUnappliedEntryExists(EntryNo: Integer): Boolean
    var
        DetailedVendorLedgEntry: Record "Detailed Vendor Ledg. Entry";
    begin
        WITH DetailedVendorLedgEntry DO BEGIN
            SETCURRENTKEY("Vendor Ledger Entry No.", "Entry Type", "Posting Date");
            SETRANGE("Vendor Ledger Entry No.", EntryNo);
            SETRANGE("Entry Type", "Entry Type"::Application);
            SETFILTER("Posting Date", '>%1', MaxDate);
            SETRANGE(Unapplied, TRUE);
            EXIT(NOT ISEMPTY);
        END;
    end;
}