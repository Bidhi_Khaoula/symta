report 50067 "MAj prix vente Kit"
{
    Caption = 'MAj prix vente Kit';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    ProcessingOnly = true;

    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "Item Category Code", "Manufacturer Code", "No. 2";

            trigger OnAfterGetRecord();
            begin
                CANCEL := FALSE;
                CurrReport.BREAK();
            end;
        }
    }

    trigger OnInitReport();
    begin
        CANCEL := TRUE;
    end;

    var
        CANCEL: Boolean;

    procedure Result(var _Item: Record Item);
    begin
        IF CANCEL THEN EXIT;

        Item.SETRANGE("Assembly BOM", TRUE);
        IF Item.FINDSET() THEN
            REPEAT
                _Item := Item;
                _Item.Insert();
            UNTIL Item.NEXT() = 0;
    end;
}

