report 50168 "Import XLS Critère"
{
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Import XLS Critère';
    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                "LCritères": Record Critere;
                "TypeCritère": Code[20];
            begin

                i := i + 1;
                Fenetre.UPDATE(1, ROUND(i / nb * 10000, 1));


                //Initialisation de la table
                CLEAR(LCritères);

                //Vérification du code
                IF NOT GetCell('A') THEN
                    ERROR(ESK001Lbl, i);

                IF LCritères.GET(TempExcelBuf."Cell Value as Text") THEN
                    ERROR(ESK002Lbl, TempExcelBuf."Cell Value as Text", i);

                //On remplit le code
                LCritères.VALIDATE(Code, TempExcelBuf."Cell Value as Text");

                IF GetCell('B') THEN
                    LCritères.VALIDATE(Nom, TempExcelBuf."Cell Value as Text");

                // Gestion du type de critère
                IF GetCell('C') THEN
                   Evaluate(TypeCritère , TempExcelBuf."Cell Value as Text")
                ELSE
                    TypeCritère := '';


                CASE UPPERCASE(TypeCritère) OF
                    'TEXTE':
                        LCritères.VALIDATE(Type, LCritères.Type::Texte);
                    'ENTIER':
                        LCritères.VALIDATE(Type, LCritères.Type::Entier);
                    'BOOLEEN':
                        LCritères.VALIDATE(Type, LCritères.Type::Booléen);
                    'DECIMAL':
                        LCritères.VALIDATE(Type, LCritères.Type::Décimal);
                    '':
                        LCritères.VALIDATE(Type, LCritères.Type::Texte);
                    ELSE
                        ERROR(ESK003Lbl, TypeCritère, i);
                END;

                IF GetCell('D') THEN
                    LCritères.VALIDATE(Unité, TempExcelBuf."Cell Value as Text");


                LCritères.INSERT(TRUE);
            end;

            trigger OnPreDataItem();
            begin

                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 1, TempExcelBuf."Row No." - 1);
                //On se positionne sur la 1ere ligne des données (entete=1)
                No_ligne := 1;
                //MESSAGE(Integer.GETFILTERS);
                nb := TempExcelBuf."Row No.";
                i := 1;
                Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');
            end;
        }
    }

    requestpage
    {

        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', locked = true;

        begin
            IF CloseAction = ACTION::OK THEN BEGIN
                FileName := FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok);
                IF FileName = '' THEN
                    EXIT(FALSE);

                SheetName := TempExcelBuf.SelectSheetsName(FileName);
                IF SheetName = '' THEN
                    EXIT(FALSE);
            END;
        end;
    }
    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin
        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        FileName: Text[250];
        //    UploadedFileName: Text[1024];
        SheetName: Text[250];

        No_ligne: Integer;
        nb: Integer;
        i: Integer;
        Fenetre: Dialog;
       // Text015Lbl: Label 'Choisir le fichier';
        ESK001Lbl: Label 'Code Critère obligatoire ! Ligne %1' , Comment ='%1 = No de Ligne';
        ESK002Lbl: Label 'Le critère %1 existe déjà ! Ligne %2' , Comment ='%1 = Critere ; %2 = No de Ligne';
        ESK003Lbl: Label 'Le type de critère %1 est incorrect ! Ligne %2' , Comment ='%1 = Critere ; %2 = No de Ligne';

    local procedure ReadExcelSheet();
    begin
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    procedure GetCell(Row: Text[5]): Boolean;
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.SETRANGE("Row No.", i);
        TempExcelBuf.SETRANGE(xlColID, Row);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;
}

