report 50150 "Export Cde Achat Excel"
{

    ProcessingOnly = true;
    UseRequestPage = false;
    Caption = 'Export Cde Achat Excel';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    dataset
    {
        dataitem("Purchase Line"; "Purchase Line")
        {
            DataItemTableView = SORTING("Document Type", "Document No.", "Line No.")
                                ORDER(Ascending)
                                WHERE("Document Type" = CONST(Order));

            trigger OnAfterGetRecord();
            begin
                MakeExcelDataBody();
            end;
        }
    }
    trigger OnPostReport();
    begin
        CreateExcelbook();
    end;

    trigger OnPreReport();
    begin
        MakeExcelInfo();
    end;

    var
        gPurchaseHeader: Record "Purchase Header";
        TempExcelBuf: Record "Excel Buffer" temporary;
        Excel002Lbl: Label 'Data';


    procedure MakeExcelInfo();
    begin
        // Export Excel
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        IF ("Purchase Line".GETFILTER("Document No.") <> '') THEN BEGIN
            gPurchaseHeader.GET("Purchase Line"."Document Type"::Order, "Purchase Line".GETFILTER("Document No."));
            TempExcelBuf.NewRow();
            TempExcelBuf.AddColumn('N° cde SYMTA', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(gPurchaseHeader."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.NewRow();
            TempExcelBuf.AddColumn('Nom fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(gPurchaseHeader."Buy-from Vendor Name", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.NewRow();
            TempExcelBuf.AddColumn('N° cde fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(gPurchaseHeader."Vendor Order No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.NewRow();
            TempExcelBuf.AddColumn('N° BL fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddColumn(gPurchaseHeader."Vendor Shipment No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.NewRow();
        END;

        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn('N° Ligne ', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° article', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Référence SYMTA', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Référence fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Désignation', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Quantité', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Unité', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pu Brut', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Remise 1', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Remise 2', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pu Net', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Total Net', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Date de réception demandée', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn("Purchase Line"."Line No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line"."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line"."Recherche référence", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line"."Item Reference No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".Description, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line".Quantity), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn("Purchase Line"."Unit of Measure", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Direct Unit Cost"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Discount1 %"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Discount2 %"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Line Amount" / "Purchase Line".Quantity), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Line Amount"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FORMAT("Purchase Line"."Requested Receipt Date"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, 'Commande achat', COMPANYNAME, USERID)
    end;

    procedure FormatDecimal(Value: Decimal): Text[30];
    begin
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    end;
}

