report 50062 "Articles sans emplacement"
{
    caption = 'Articles sans emplacement';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Articles sans emplacement.rdlc';

    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "No.";
            column(No; "No.")
            {
            }
            column(No2; "No. 2")
            {
            }
            column(Description; Description)
            {
            }
            column(Mouvement; Mouvement)
            {
            }
            column(Inventory; Inventory)
            {
            }

            trigger OnAfterGetRecord();
            var
                itemledgerentry: Record "Item Ledger Entry";
            begin

                "Item Bin Contents".SETRANGE("Item No.", "No.");
                // MCO Le 01-04-2015 => Régie
                IF SansEmplacementParDefaut THEN
                    "Item Bin Contents".SETRANGE(Default, TRUE);
                // FIN MCO Le 01-04-2015 => Régie
                IF NOT "Item Bin Contents".ISEMPTY THEN
                    CurrReport.SKIP()
                ELSE
                    IF CreateBinContentOn THEN
                        WITH "Item Bin Contents" DO BEGIN
                            RESET();
                            INIT();
                            VALIDATE("Location Code", 'SP');
                            VALIDATE("Item No.", "No.");
                            VALIDATE("Unit of Measure Code", '1');
                            VALIDATE("Bin Code", 'ZD');
                            VALIDATE("Zone Code", '1PT');
                            Fixed := TRUE;
                            Default := TRUE;
                            _flag := TRUE;
                            IF NOT INSERT(TRUE) THEN MODIFY(TRUE);
                        END;

                itemledgerentry.SETRANGE("Item No.", "No.");
                IF itemledgerentry.ISEMPTY THEN
                    Mouvement := FALSE
                ELSE
                    Mouvement := TRUE;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(SansEmplacementParDefautName; SansEmplacementParDefaut)
                {
                    Caption = 'Sans emplacement par défaut';
                    ToolTip = 'Si non coché alors seul les articles sans emplacement seront recherchés';
                    ApplicationArea = All;
                }
                field(Create_BinContentOnName; CreateBinContentOn)
                {
                    Caption = 'Initialiser si inexistant';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Initialiser si inexistant field.';
                }
            }
        }
    }
    var
        "Item Bin Contents": Record "Bin Content";
        Mouvement: Boolean;
        CreateBinContentOn: Boolean;
        SansEmplacementParDefaut: Boolean;
}

