report 50103 "recap retours frournisseurs"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\recap retours frournisseurs.rdlc';

    Caption = 'Recap retours fournisseurs';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Logo; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPicture2; CompanyInfo."Picture Address")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);
                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
                CompanyInfo.CALCFIELDS("Picture Address");

                GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
            end;
        }
        dataitem(Vendor; Vendor)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending);
            RequestFilterFields = "No.";
            column(VendorNo; "No.")
            {
            }
            column(VendorName; Name)
            {
            }
            column(VendorAddress; Address)
            {
            }
            column(VendorAddress2; "Address 2")
            {
            }
            column(VendorPostCode; "Post Code")
            {
            }
            column(VendorCity; City)
            {
            }
            dataitem("Purchase Header"; "Purchase Header")
            {
                DataItemLink = "Buy-from Vendor No." = FIELD("No.");
                DataItemTableView = SORTING("Document Type", "No.")
                                    WHERE("Document Type" = CONST("Return Order"));
                column(HeaderNo; "No.")
                {
                }
                dataitem("Purchase Line"; "Purchase Line")
                {
                    DataItemLink = "Document Type" = FIELD("Document Type"),
                                   "Document No." = FIELD("No.");
                    DataItemTableView = WHERE("Document Type" = CONST("Return Order"));
                    RequestFilterFields = "Document Type", "Document No.";
                    column(Type; Type)
                    {
                    }
                    column(ExpectedReceiptDate; "Expected Receipt Date")
                    {
                    }
                    column(RefActive; RefActive)
                    {
                    }
                    column(No; "No.")
                    {
                    }
                    column(CrossRefenceNo; "item Reference No.")
                    {
                    }
                    column(Description; Description)
                    {
                    }
                    column(Quantity; Quantity)
                    {
                    }
                    column(UnitOfMeasure; "Unit of Measure")
                    {
                    }
                    column(DirectUnitCost; "Direct Unit Cost")
                    {
                    }
                    column(Discount1; "Discount1 %")
                    {
                    }
                    column(Discount2; "Discount2 %")
                    {
                    }
                    column(LineAmount; "Line Amount")
                    {
                    }
                    column(CodeGroupe; gCodeGroupe)
                    {
                    }
                    column(MontantGroupe; gMontantGroupe)
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin

                        tot_mt += "Purchase Line"."Line Amount";


                        RefActive := "Purchase Line"."Recherche référence";
                        IF Vendor."Masquer Ref. Active" THEN RefActive := ''; // AD Le 01-04-2015;

                        // CFR le 28/09/2023 - Régie gestion des sous-totaux groupes
                        gMontantGroupe += "Purchase Line"."Line Amount";
                        IF ("Purchase Line"."No." = '') AND ("Purchase Line".Description = '') AND (RefActive = '') THEN
                            CurrReport.SKIP();
                        // FIN CFR le 28/09/2023
                    end;
                }

                trigger OnAfterGetRecord();
                begin
                    // CFR le 28/09/2023 - Régie gestion des sous-totaux groupes
                    gPurchaseLine.SETRANGE("Document Type", "Document Type");
                    gPurchaseLine.SETRANGE("Document No.", "No.");
                    IF gPurchaseLine.ISEMPTY() THEN
                        CurrReport.SKIP();
                    // FIN CFR le 28/09/2023
                end;

                trigger OnPreDataItem();
                begin
                    "Purchase Header".SETRANGE("Purchase Header"."Document Type", 5);
                end;
            }

            trigger OnAfterGetRecord();
            begin

                CurrReport.LANGUAGE := LanguageG.GetLanguageID("Language Code");
                tot_mt := 0;

                // CFR le 28/09/2023 - Régie gestion des sous-totaux groupes
                IF gCodeGroupe <> COPYSTR(Vendor."No.", 1, 6) THEN BEGIN
                    gCodeGroupe := COPYSTR(Vendor."No.", 1, 6);
                    gMontantGroupe := 0;
                END;
                // FIN CFR le 28/09/2023
            end;
        }
    }
    trigger OnPreReport();
    begin
        // CFR le 28/09/2023 - Régie gestion des sous-totaux groupes
        gCodeGroupe := '';
        gPurchaseLine.COPYFILTERS("Purchase Line");
    end;

    var
        CompanyInfo: Record "Company Information";
        gPurchaseLine: Record "Purchase Line";
        GestionEdition: Codeunit "Gestion Info Editions";
        LanguageG: codeunit Language;
        tot_mt: Decimal;
        RefActive: Code[40];
        textSIEGSOC: array[3] of Text[250];
        gCodeGroupe: Code[10];
        gMontantGroupe: Decimal;


}

