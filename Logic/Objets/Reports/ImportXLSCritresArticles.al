report 50169 "Import XLS Critères Articles"
{
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                "LCritèresArt": Record "Article Critere";
                LCritere: Record Critere;
            begin


                i := i + 1;
                Fenetre.UPDATE(1, ROUND(i / nb * 10000, 1));


                //Initialisation de la table
                CLEAR(LCritèresArt);

                //Code Article
                IF NOT GetCell('A') THEN
                    ERROR(ESK001Lbl, i, LCritèresArt.FIELDCAPTION("Code Article"));
                LCritèresArt.VALIDATE("Code Article", TempExcelBuf."Cell Value as Text");

                //Code Variante
                IF GetCell('B') THEN
                    LCritèresArt.VALIDATE("Variant Code", TempExcelBuf."Cell Value as Text");

                // Code Critère
                IF NOT GetCell('C') THEN
                    ERROR(ESK001Lbl, i, LCritèresArt.FIELDCAPTION("Code Critere"));
                LCritèresArt.VALIDATE("Code Critere", TempExcelBuf."Cell Value as Text");


                LCritere.GET(LCritèresArt."Code Critere");
                CASE LCritere.Type OF
                    // Format   Changer par VD le 31/01/2017 car les critère peuvent être sans valeur.
                    LCritere.Type::Texte:
                        BEGIN
                            IF NOT GetCell('D') THEN
                                LCritèresArt.VALIDATE(Text, '')
                            ELSE
                                LCritèresArt.VALIDATE(Text, TempExcelBuf."Cell Value as Text");
                        END;

                    LCritere.Type::Entier:
                        BEGIN
                            IF NOT EVALUATE(LCritèresArt.Entier, TempExcelBuf."Cell Value as Text") THEN
                                ERROR(ESK003Lbl, TempExcelBuf."Cell Value as Text", i)
                            ELSE
                                LCritèresArt.VALIDATE(Entier);
                        END;
                    LCritere.Type::Décimal:
                        BEGIN
                            IF NOT EVALUATE(LCritèresArt.Entier, TempExcelBuf."Cell Value as Text") THEN
                                ERROR(ESK003LBl, TempExcelBuf."Cell Value as Text", i)
                            ELSE
                                LCritèresArt.VALIDATE(Décimal);
                        END;
                    LCritere.Type::Booléen:
                        BEGIN
                            IF NOT EVALUATE(LCritèresArt.Entier, TempExcelBuf."Cell Value as Text") THEN
                                ERROR(ESK003Lbl, TempExcelBuf."Cell Value as Text", i)
                            ELSE
                                LCritèresArt.VALIDATE(Booléen);
                        END;

                END;




                IF NOT LCritèresArt.INSERT(TRUE) THEN;
            end;

            trigger OnPreDataItem();
            begin

                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 1, TempExcelBuf."Row No." - 1);
                //On se positionne sur la 1ere ligne des données (entete=1)
                No_ligne := 1;
                //MESSAGE(Integer.GETFILTERS);
                nb := TempExcelBuf."Row No.";
                i := 1;


                Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');
            end;
        }
    }

    requestpage
    {
        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', Locked = true;
        begin
            IF CloseAction = ACTION::OK THEN BEGIN
                FileName := FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok);
                IF FileName = '' THEN
                    EXIT(FALSE);

                SheetName := TempExcelBuf.SelectSheetsName(FileName);
                IF SheetName = '' THEN
                    EXIT(FALSE);
            END;
        end;
    }
    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin

        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        FileName: Text[250];
        //UploadedFileName: Text[1024];
        SheetName: Text[250];

        No_ligne: Integer;
        nb: Integer;
        i: Integer;
        Fenetre: Dialog;
        // Text015Lbl: Label 'Choisir le fichier';
        ESK001Lbl: Label 'Code %2 obligatoire ! Ligne %1', Comment = '%1 = No de Ligne ; %2 = Code';
      //  ESK002Lbl: Label 'La priorité doit être un entier ! Ligne %1', Comment = '%1 = No de Ligne';
        ESK003Lbl: Label 'Le type de critère %1 est incorrect ! Ligne %2', Comment = '%1 = Critère  ; %2 = Ligne';

    local procedure ReadExcelSheet();
    begin
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    procedure GetCell(Row: Text[5]): Boolean;
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.SETRANGE("Row No.", i);
        TempExcelBuf.SETRANGE(xlColID, Row);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;
}

