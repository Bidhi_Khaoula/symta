report 50081 "Contrôle stock emplacements"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Contrôle stock emplacements.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Permissions = TableData "Warehouse Entry" = rimd;

    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "No.", "Location Filter";
            column(No; Item."No. 2")
            {
            }
            column(Stk; Item.Inventory)
            {
            }
            column(StkMag; lec_ecr_mag."Qty. (Base)")
            {
            }

            trigger OnAfterGetRecord();
            begin

                emplacement.SETRANGE("Location Code", Item.GETFILTER("Location Filter"));
                emplacement.SETRANGE("Item No.", "No.");
                emplacement.SETRANGE(Default, TRUE);
                IF NOT emplacement.FINDFIRST() THEN
                    CurrReport.SKIP();

                Item.CALCFIELDS(Item.Inventory);

                lec_ecr_mag.SETCURRENTKEY("Item No.", "Location Code");
                lec_ecr_mag.SETRANGE("Location Code", Item.GETFILTER("Location Filter"));
                lec_ecr_mag.SETRANGE("Item No.", "No.");
                lec_ecr_mag.CALCSUMS("Qty. (Base)");
                IF lec_ecr_mag."Qty. (Base)" = Item.Inventory THEN
                    CurrReport.SKIP()

                // A activer pour MAJ data
                /*
                ELSE
                 BEGIN
                  CLEAR(ecr_ecr_mag);
                
                  IF ecr_ecr_mag.FINDLAST THEN ;
                  mum_ec:=ecr_ecr_mag."Entry No."+1;
                  CLEAR(ecr_ecr_mag);
                  ecr_ecr_mag."Entry No.":=mum_ec;
                  ecr_ecr_mag."Registering Date":=250516D;
                  ecr_ecr_mag."Location Code":= emplacement."Location Code";
                  ecr_ecr_mag."Zone Code" :=emplacement."Zone Code";
                  ecr_ecr_mag."Bin Code" := emplacement."Bin Code" ;
                  ecr_ecr_mag.Description := 'ERREUR STK' ;
                  ecr_ecr_mag."Item No." := "No." ;
                  ecr_ecr_mag.Quantity :=  Item.Inventory -  lec_ecr_mag."Qty. (Base)";
                  ecr_ecr_mag."Qty. (Base)" := Item.Inventory -  lec_ecr_mag."Qty. (Base)";
                  ecr_ecr_mag."Qty. per Unit of Measure" :=1;
                  ecr_ecr_mag."Unit of Measure Code" := Item."Base Unit of Measure" ;
                
                  // Si un seul emplacement alors on valide sinon a faire manuellement
                  emplacement.SETRANGE("Location Code", Item.GETFILTER("Location Filter"));
                  emplacement.SETRANGE("Item No.","No.");
                  emplacement.SETRANGE(Default,TRUE);
                  IF emplacement.COUNT = 1 THEN BEGIN
                    ecr_ecr_mag.Insert();
                    CurrReport.SKIP;
                  END;
                
                
                 END;
                 */

            end;

            trigger OnPreDataItem();
            begin

                IF Item.GETFILTER("Location Filter") = '' THEN
                    ERROR('filtre magasin obligatoire');
            end;
        }
    }
    var
        lec_ecr_mag: Record "Warehouse Entry";
        // ecr_ecr_mag: Record "Warehouse Entry";
        emplacement: Record "Bin Content";
    // mum_ec: Integer;
}

