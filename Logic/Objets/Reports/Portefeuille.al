report 50092 Portefeuille
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Portefeuille.rdlc';
    Caption = 'Portefeuille';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = all;
    dataset
    {
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("Due Date", "Copied To Line", "Payment in Progress", "Acceptation Code")
                                WHERE("Copied To No." = FILTER(''),
                                      "Status No." = CONST(10000),
                                      "Payment Class" = CONST('CLIENT EFF'));
            RequestFilterFields = "Due Date", "Acceptation Code";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column(clientName; client.Name)
            {
            }
            column(AcceptationCode; "Acceptation Code")
            {
            }
            column(BankBranchNo; "Bank Branch No.")
            {
            }
            column(BankAccountNo; "Bank Account No.")
            {
            }
            column(AgencyCode; "Agency Code")
            {
            }
            column(RIBKey; "RIB Key")
            {
            }
            column(RIBChecked; "RIB Checked")
            {
            }
            column(Amount; Amount)
            {
            }

            trigger OnAfterGetRecord();
            begin
                LastFieldNo := FIELDNO("Acceptation Code");
                IF client.GET("Payment Line"."Account No.") THEN;
            end;
        }
    }
    var
        client: Record Customer;
        LastFieldNo: Integer;
}

