report 50132 "Justificatif de solde bancaire"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Justificatif de solde bancaire.rdlc';
    UsageCategory = ReportsAndAnalysis;
    Caption = '';
    ApplicationArea = All;
    dataset
    {
        dataitem("Bank Account"; "Bank Account")
        {
            RequestFilterFields = "No.";
            column(Date_justification; FORMAT("Date de justificatif"))
            {
            }
            column(Banque_nom; "Bank Account".Name)
            {
            }
            column(Banque_NCompte; "Bank Account"."Bank Account No.")
            {
            }
            column(Banque_Solde; "Bank Account"."Net Change")
            {
            }
            column(Banque_Iban; "Bank Account".IBAN)
            {
            }
            column(Banque_Swift; "Bank Account"."SWIFT Code")
            {
            }
            dataitem("Bank Account Ledger Entry"; "Bank Account Ledger Entry")
            {
                CalcFields = "Date de relevé";
                DataItemLink = "Bank Account No." = FIELD("No.");
                DataItemTableView = SORTING("Bank Account No.", "Posting Date");
                column(Ligne_Mark; "Bank Account Ledger Entry"."Bank Account No.")
                {
                }
                column(Ligne_date; FORMAT("Bank Account Ledger Entry"."Posting Date"))
                {
                }
                column(Ligne_document; "Bank Account Ledger Entry"."Document No.")
                {
                }
                column(Ligne_description; "Bank Account Ledger Entry".Description)
                {
                }
                column(ligne_montant; "Bank Account Ledger Entry".Amount)
                {
                }

                trigger OnAfterGetRecord();
                begin


                    totalmontant += "Bank Account Ledger Entry".Amount;
                end;

                trigger OnPreDataItem();
                begin
                    "Bank Account Ledger Entry".SETRANGE("Posting Date", 0D, Statement."Statement Date");
                    "Bank Account Ledger Entry".SETFILTER("Date de relevé", '>%1|%2', Statement."Statement Date", 0D);
                    //CurrReport.CREATETOTALS("Bank Account Ledger Entry".Amount);
                    totalmontant := 0;
                end;
            }
            dataitem(Total; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    WHERE(Number = CONST(1));
                column(Total_Mark; FORMAT(Total.Number))
                {
                }
                column(Total_nom; "Bank Account".Name)
                {
                }
                column("Total_date_de_relevé"; FORMAT(Statement."Statement Date"))
                {
                }
                column(Total_solde_banque; "Bank Account"."Net Change")
                {
                }
                column("Total_solde_relevé"; Statement."Statement Ending Balance")
                {
                }
                column(Total_diff; "Bank Account"."Net Change" - Statement."Statement Ending Balance")
                {
                }
                column(Total_ligne; totalmontant)
                {
                }
            }

            trigger OnAfterGetRecord();
            begin
                Statement.SETCURRENTKEY("Bank Account No.", "Statement Date");
                Statement.SETRANGE("Bank Account No.", "Bank Account"."No.");
                Statement.SETRANGE("Statement Date", 0D, "Date de justificatif");
                IF Statement.FINDLAST() THEN;
                "Bank Account".SETRANGE("Date Filter", 0D, Statement."Statement Date");
                "Bank Account".CALCFIELDS("Net Change");
                Entete := 1;
            end;

            trigger OnPreDataItem();
            begin
                //CurrReport.CREATETOTALS("Bank Account Ledger Entry".Amount);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Date de justificatif_Req"; "Date de justificatif")
                {
                    Caption = 'Date de justificatif';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date de justificatif field.';
                }
            }
        }

    }

    labels
    {
        Nom = 'Nom'; NCompte = 'N° de compte'; Solde = 'Solde'; IBAN = 'IBAN'; SWIFT = 'SWIFT'; Date = 'Date'; NoDoc = 'N° Document'; Description = 'Désignation'; montant = 'Montant'; Montantcompte = 'Montant Compte Banque'; MontantRelevé = 'Montant du relevé'; DateReleve = 'Date du relevé'; Ecart = 'Ecart Cpt-Relevé'; MontantLigne = 'Total Ligne';
    }

    trigger OnInitReport();
    begin
        "Date de justificatif" := TODAY;
    end;

    var
        Statement: Record "Bank Account Statement";
        "Date de justificatif": Date;
        Entete: Decimal;
        totalmontant: Decimal;
}

