report 50031 "Packing List Detail"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Packing List Detail.rdlc';

    Caption = 'Packing List Detail';
    PreviewMode = PrintLayout;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(PreReport; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfo1Picture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPictureFooter; CompanyInfo."Bottom Picture")
            {
            }

            trigger OnAfterGetRecord();
            begin
                // ANI ESKVN1.0 => logo entête et pied de page
                CompanyInfo.GET();
                IF PrintLogo THEN BEGIN
                    CompanyInfo.CALCFIELDS(Picture);
                    //CompanyInfo.CALCFIELDS("Picture Footer");
                    CompanyInfo.CALCFIELDS("Header Picture", "Bottom Picture");
                END;
                GLSetup.GET();
                // FIN ANI ESKVN1.0
            end;
        }
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeading = 'Posted Sales Shipment';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(PageCaption; PageCaptionCapLbl)
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalText)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(NetUnitPriceCaption; NetUnitPriceCaptionLbl)
            {
            }
            column(nofax; no_fax)
            {
            }
            column(Objetfax; Objet_fax)
            {
            }
            column(Attentionfax; Attention_fax)
            {
            }
            column(nompdf; nompdf)
            {
            }
            column(SalesShptCopyText; GetDocumentCaption())
            {
            }
            column(RightAddr1; ShipToAddr[1])
            {
            }
            column(CompanyAddr1; CompanyAddr[1])
            {
            }
            column(RightAddr2; ShipToAddr[2])
            {
            }
            column(CompanyAddr2; CompanyAddr[2])
            {
            }
            column(RightAddr3; ShipToAddr[3])
            {
            }
            column(CompanyAddr3; CompanyAddr[3])
            {
            }
            column(RightAddr4; ShipToAddr[4])
            {
            }
            column(CompanyAddr4; CompanyAddr[4])
            {
            }
            column(RightAddr5; ShipToAddr[5])
            {
            }
            column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
            {
            }
            column(RightAddr6; ShipToAddr[6])
            {
            }
            column(CompanyInfoHomePage; CompanyInfo."Home Page")
            {
            }
            column(CompanyInfoEmail; CompanyInfo."E-Mail")
            {
            }
            column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
            {
            }
            column(CompanyInfoVATRegtnNo; CompanyInfo."VAT Registration No.")
            {
            }
            column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
            {
            }
            column(CompanyInfoBankName; CompanyInfo."Bank Name")
            {
            }
            column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
            {
            }
            column(SelltoCustNo_SalesShptHeader; "Sales Shipment Header"."Sell-to Customer No.")
            {
            }
            column(DocDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Document Date", 0, DateFormatLbl))
            {
            }
            column(SalesPersonText; SalesPersonText)
            {
            }
            column(SalesPurchPersonName; SalesPurchPerson.Name)
            {
            }
            column(ReferenceText; ReferenceText)
            {
            }
            column(YourRef_SalesShptHeader; "Sales Shipment Header"."Your Reference")
            {
            }
            column(RightAddr7; ShipToAddr[7])
            {
            }
            column(RightAddr8; ShipToAddr[8])
            {
            }
            column(CompanyAddr5; CompanyAddr[5])
            {
            }
            column(CompanyAddr6; CompanyAddr[6])
            {
            }
            column(ShptDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Shipment Date", 0, DateFormatLbl))
            {
            }
            column(OutputNo; OutputNo)
            {
            }
            column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
            {
            }
            column(PhoneNoCaption; PhoneNoCaptionLbl)
            {
            }
            column(VATRegNoCaption; VATRegNoCaptionLbl)
            {
            }
            column(GiroNoCaption; GiroNoCaptionLbl)
            {
            }
            column(BankNameCaption; BankNameCaptionLbl)
            {
            }
            column(BankAccNoCaption; BankAccNoCaptionLbl)
            {
            }
            column(ShipmentNoCaption; ShipmentNoCaptionLbl)
            {
            }
            column(ShipmentDateCaption; ShipmentDateCaptionLbl)
            {
            }
            column(HomePageCaption; HomePageCaptionLbl)
            {
            }
            column(EmailCaption; EmailCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(SelltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Sell-to Customer No."))
            {
            }
            column(LeftAddr1; SellToAddr[1])
            {
            }
            column(LeftAddr2; SellToAddr[2])
            {
            }
            column(LeftAddr3; SellToAddr[3])
            {
            }
            column(LeftAddr4; SellToAddr[4])
            {
            }
            column(LeftAddr5; SellToAddr[5])
            {
            }
            column(LeftAddr6; SellToAddr[6])
            {
            }
            column(LeftAddr7; SellToAddr[7])
            {
            }
            column(LeftAddr8; SellToAddr[8])
            {
            }
            column(LeftAddrCaption; SelltoAddrCaptionLbl)
            {
            }
            column(MiddleAddrCaption; BilltoAddrCaptionLbl)
            {
            }
            column(HeaderField01Caption; LocationCaptionLbl)
            {
            }
            column(HeaderField02Caption; ShipmentMethodCaptionLbl)
            {
            }
            column(HeaderField03Caption; "Sales Shipment Header".FIELDCAPTION("Order No."))
            {
            }
            column(HeaderField04Caption; CredocCaption)
            {
            }
            column(HeaderField05Caption; ExternalDocNoCaptionLbl)
            {
            }
            column(HeaderField06Caption; '')
            {
            }
            column(HeaderField07Caption; '')
            {
            }
            column(HeaderField08Caption; '')
            {
            }
            column(HeaderField09Caption; '')
            {
            }
            column(HeaderField10Caption; '')
            {
            }
            column(HeaderField01; "Sales Shipment Header"."Location Code")
            {
            }
            column(HeaderField02; ShipmentMethod.Description)
            {
            }
            column(HeaderField03; "Sales Shipment Header"."Order No.")
            {
            }
            column(HeaderField04; '')
            {
            }
            column(HeaderField05; "Sales Shipment Header"."External Document No.")
            {
            }
            column(HeaderField06; '')
            {
            }
            column(HeaderField07; '')
            {
            }
            column(HeaderField08; '')
            {
            }
            column(HeaderField09; '')
            {
            }
            column(HeaderField10; '')
            {
            }
            column(MiddleAddr1; CustAddr[1])
            {
            }
            column(MiddleAddr2; CustAddr[2])
            {
            }
            column(MiddleAddr3; CustAddr[3])
            {
            }
            column(MiddleAddr4; CustAddr[4])
            {
            }
            column(MiddleAddr5; CustAddr[5])
            {
            }
            column(MiddleAddr6; CustAddr[6])
            {
            }
            column(MiddleAddr7; CustAddr[7])
            {
            }
            column(MiddleAddr8; CustAddr[8])
            {
            }
            column(PricesInclVAT_SalesShptHeader; "Sales Shipment Header"."Prices Including VAT")
            {
            }
            column(NbOfBox_SalesShptHeader; "Nb Of Box")
            {
            }
            column(Weight_SalesShptHeader; Weight)
            {
            }
            column(NbOfBoxCaption; NbOfBoxCaptionLbl)
            {
            }
            column(WeightCaption; WeightCaptionLbl)
            {
            }
            dataitem("Packing List Package"; "Packing List Package")
            {
                DataItemLink = "Packing List No." = FIELD("Packing List No.");
                DataItemTableView = SORTING("Packing List No.", "Package No.")
                                    ORDER(Ascending);
                column(NoExp_DetailPackingList; "Packing List Package"."Packing List No.")
                {
                }
                column(NumColis_DetailPackingList; "Packing List Package"."Package No.")
                {
                }
                column(NumTypeColis_DetailPackingList; NoPalletCaptionLbl)
                {
                }
                column("TxtDétail"; TxtDétail)
                {
                }
                column(Comment1_DetailPackingList; "Comment 1")
                {
                }
                column(Comment2_DetailPackingList; "Comment 2")
                {
                }
                dataitem("Packing List Line"; "Packing List Line")
                {
                    DataItemLink = "Packing List No." = FIELD("Packing List No."),
                                   "Package No." = FIELD("Package No.");
                    DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                        ORDER(Ascending)
                                        WHERE("Line Quantity" = FILTER(<> 0));
                    column(NoArticle_PackingList; "Packing List Line"."Item No.")
                    {
                    }
                    column(Designation_PackingList; "Packing List Line".Description)
                    {
                    }
                    column("QtéColis_PackingList"; "Packing List Line"."Line Quantity")
                    {
                    }

                    trigger OnPreDataItem();
                    begin
                        "Nb colis" := 0;
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    lTexteTypeColis: Text[50];
                    NumTxt: Label 'Numéro de %1 :', Comment = '%1 = Num';
                begin
                    "Packing List Package".CALCFIELDS("Package Quantity", "Package Weight Auto");
                    lTexteTypeColis := FORMAT("Packing List Package"."Package Type");
                    NoPalletCaptionLbl := STRSUBSTNO(NumTxt, lTexteTypeColis);

                    TxtDétail := STRSUBSTNO(Text50000Lbl, "Packing List Package".Length, "Packing List Package".Width, "Packing List Package".Height,
                                  "Packing List Package"."Package Weight", "Packing List Package"."Package Quantity");

                    IF "Packing List Package"."Outside Of EC" THEN
                        TxtDétail := TxtDétail + ' ' + Text50001Lbl
                    // AD Le 02-07-2015 =>
                    ELSE
                        TxtDétail := TxtDétail + ' ' + ESK002Lbl
                    // FIN AD Le 02-07-2015
                end;
            }
            dataitem("Packing List Package 2"; "Packing List Package")
            {
                DataItemLink = "Packing List No." = FIELD("Packing List No.");
                DataItemTableView = SORTING("Packing List No.", "Package No.")
                                    ORDER(Ascending);
                column(text_DetailPackingList2; text_DetailPackingList2)
                {
                }
                column(Comment1_DetailPackingList2; "Comment 1")
                {
                }
                column(Comment2_DetailPackingList2; "Comment 2")
                {
                }

                trigger OnAfterGetRecord();
                begin
                    "Packing List Package 2".CALCFIELDS("Package Quantity", "Package Weight Auto");
                    text_DetailPackingList2 := '1 ' + FORMAT("Packing List Package 2"."Package Type") + ' de ' + FORMAT("Packing List Package 2"."Package Quantity") + ' article(s).';
                end;
            }

            trigger OnAfterGetRecord();
            begin
                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");

                ShowPrices := "Sales Shipment Header"."Abandon remainder";

                FormatAddressFields("Sales Shipment Header");
                FormatDocumentFields("Sales Shipment Header");

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN
                        SegManagement.LogDocument(
                          5, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');



                IF (PrintForFax) THEN
                    CU_GestionEdition."GetInfoFax-Pdf"('BL', "Sales Shipment Header"."No.", no_fax, Objet_fax, Attention_fax, nompdf);

                IF (PrintForFax) AND (no_fax = '') THEN
                    ERROR('Le no de fax est vide');

                // AD Le 25-03-2007 => Possibilité d'imprimer avec un logo sans faxer
                IF PrintLogo AND (NOT PrintForFax) THEN BEGIN
                    no_fax := '';
                    Objet_fax := '';
                    nompdf := '';
                END;


                //AD on regarde si export pour les code douanier
                IF "Sales Shipment Header"."VAT Bus. Posting Group" = 'SANS TVA' THEN
                    arExport := TRUE
                ELSE
                    arExport := FALSE;
                /*
                IF "Sales Shipment Header"."N° Crédit Documentaire" <> '' THEN
                  CredocCaption := CreditDocumentaireCaptionLbl
                ELSE
                  CredocCaption := '';
                */

            end;

            /* trigger OnPreDataItem();
            
            begin
            end; */
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOf_Copies; NoOfCopies)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'No. of Copies';
                        ToolTip = 'Specifies how many copies of the document to print.';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Internal Information';
                        ToolTip = 'Specifies if the document shows internal information.';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ToolTip = 'Specifies if you want to record the reports that you print as interactions.';
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Correction Lines';
                        ToolTip = 'Specifies if the correction lines of an undoing of quantity posting will be shown on the report.';
                    }
                    field(ShowLotSN; ShowLotSN)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Serial/Lot Number Appendix';
                        ToolTip = 'Specifies if you want to print an appendix to the sales shipment report showing the lot and serial numbers in the shipment.';
                    }
                    field(DisplayAsmInfo; DisplayAssemblyInformation)
                    {
                        ApplicationArea = Assembly;
                        Caption = 'Show Assembly Components';
                        ToolTip = 'Specifies if you want the report to include information about components that were used in linked assembly orders that supplied the item(s) being sold.';
                    }
                    field(Print_Logo; PrintLogo)
                    {
                        Caption = 'Print logo';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Print logo field.';
                    }
                    field(Show_Prices; ShowPrices)
                    {
                        Caption = 'Show prices';
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show prices field.';
                    }
                    group(Fax)
                    {
                        Caption = 'Fax';
                        Visible = false;
                        field(PrintForFax; PrintForFax)
                        {
                            Caption = 'Impression fax';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Impression fax field.';
                        }
                        field(no_fax; no_fax)
                        {
                            Caption = 'N°';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° field.';
                        }
                        field(Attention_fax; Attention_fax)
                        {
                            Caption = 'A l''attention de';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the A l''attention de field.';
                        }
                        field(nompdf; nompdf)
                        {
                            Caption = 'Nom du fichier';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Nom du fichier field.';
                        }
                    }
                }
            }
        }
        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;
            InitPageOption();
        end;
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET();
        SalesSetup.GET();
        //FormatDocument.SetLogoPosition(SalesSetup."Logo Position on Documents",CompanyInfo1,CompanyInfo2,CompanyInfo3);
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
        AsmHeaderExists := FALSE;
    end;

    var
        /* Text000: Label 'Salesperson';
        Text001: Label 'COPY';
        Text002: Label 'Packing list'; */
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        /*    CompanyInfo1: Record "Company Information";
           CompanyInfo2: Record "Company Information";
           CompanyInfo3: Record "Company Information"; */
        SalesSetup: Record "Sales & Receivables Setup";
        ShippingAgentServices: Record "Shipping Agent Services";
        DimSetEntry1: Record "Dimension Set Entry";
        ShipmentMethod: Record "Shipment Method";
        ShippingAgent: Record "Shipping Agent";
        GLSetup: Record "General Ledger Setup";
        SalesHeader: Record "Sales Header";
        //  DimSetEntry2: Record "Dimension Set Entry";

        /*   TrackingSpecBuffer: Record "Tracking Specification" temporary;
          PostedAsmHeader: Record "Posted Assembly Header";
          PostedAsmLine: Record "Posted Assembly Line"; */
        RespCenter: Record "Responsibility Center";
        // ItemTrackingAppendix: Report "Item Tracking Appendix";
        FormatAddr: Codeunit "Format Address";
        FormatDocument: Codeunit "Format Document";
        Language_G: Codeunit Language;
        SegManagement: Codeunit SegManagement;
        CU_GestionEdition: Codeunit "Gestion Editions / fax / Pdf";
        // ItemTrackingDocMgt: Codeunit "Item Tracking Doc. Management";
        // Currency: Record Currency;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text;
        //  MoreLines: Boolean;
        NoOfCopies: Integer;
        OutputNo: Integer;
        //  NoOfLoops: Integer;
        //   TrackingSpecCount: Integer;
        //   OldRefNo: Integer;
        //   OldNo: Code[20];
        //   CopyText: Text[30];
        ShowCustAddr: Boolean;
        /*  DimText: Text[120];
         OldDimText: Text[75]; */
        ShowInternalInfo: Boolean;
        //        Continue: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        /*   ShowTotal: Boolean;
          ShowGroup: Boolean;
          TotalQty: Decimal; */
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        //  LinNo: Integer;
        Text003Lbl: Label 'Total %1', Comment = '%1 = Currency';
        Text004Lbl: Label 'Total %1 Incl. VAT', Comment = '%1 = Currency';
        Text005Lbl: Label 'Total %1 Excl. VAT', Comment = '%1 = Currency';
        ItemTrackingAppendixCaptionLbl: Label 'Item Tracking - Appendix';
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Reg. No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        ShipmentNoCaptionLbl: Label 'Shipment No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'Email';
        DocumentDateCaptionLbl: Label 'Document Date';
        //   HeaderDimensionsCaptionLbl: Label 'Header Dimensions';
        //   LineDimensionsCaptionLbl: Label 'Line Dimensions';
        BilltoAddrCaptionLbl: Label 'Bill-to Address';
        //   SerialNoCaptionLbl: Label 'Serial No.';
        //   LotNoCaptionLbl: Label 'Lot No.';
        //   DescriptionCaptionLbl: Label 'Description';
        //   NoCaptionLbl: Label 'No.';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Num de PAge ;%2 = Nb Total de PAge';
        //   Succursale: Record "Responsibility Center";
        PrintLogo: Boolean;
        SellToAddr: array[8] of Text[50];
        //   OrderCreateUser: Record User;
        //   _LineDisc: Text[13];
        ShipmentMethodCaptionLbl: Label 'Shipment Method';
        SelltoAddrCaptionLbl: Label 'Ship-to Address';
        /*    OrderCreateUserCaptionLbl: Label 'Registered by';
           PaymentMethodDescriptionCaptionLbl: Label 'Payment Method';
           CustomerNoCaptionLbl: Label 'Customer No.'; */
        ExternalDocNoCaptionLbl: Label 'External document No';
        //  UOMCaptionLbl: Label 'Unit';
        UnitPriceCaptionLbl: Label 'Unit Price';
        NetUnitPriceCaptionLbl: Label 'Net Price';
        //   VATAmtLineTTCAmtCaptionLbl: Label 'Incl. VAT';
        ShowPrices: Boolean;
        /*     VATAmountLine: Record "VAT Amount Line" temporary;
            VATAmount: Decimal;
            VATBaseAmount: Decimal;
            VATDiscountAmount: Decimal;
            InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
            VATIdentifierCaptionLbl: Label 'VAT Identifier'; */
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        LineAmtCaptionLbl: Label 'Line Amount';
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        TotalText: Text[50];
        /*   TotalAmountInclVAT: Decimal;
          SalesShptLine: Record "Sales Shipment Line" temporary;
          DiscountPercentCaptionLbl: Label 'Disc. %'; */
        AmountCaptionLbl: Label 'Amount';
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        /*   NNCTotalLCY: Decimal;
          NNCTotalExclVAT: Decimal;
          NNCVATAmt: Decimal;
          NNCTotalInclVAT: Decimal;
          NNCSalesLineLineAmt: Decimal;
          NNCSalesLineInvDiscAmt: Decimal;
          _LineAmount: Decimal;
          _LineDiscAmount: Decimal;
          _NetUnitPrice: Decimal;
          _SalsLinAmtExclLineDiscAmt: Decimal; */

        //  ShippingAgentCaptionLbl: Label 'Shipping Agent';

        //  ShippingAgentSrvCaptionLbl: Label 'Shipping Agent Services';

        /*   QtyOrdered: Decimal;
          QtyRemainsToDeliver: Decimal;
          QtyOrderedCaptionLbl: Label 'Ord. Qty';
          QtyRemainsToDeliverCptLbl: Label 'RTD Qty';
          QtyShippedCaptionLbl: Label 'Shp. Qty';
          QuantityCaptionLbl: Label 'Quantity'; */

        CodeUtilisateurCreation: Code[20];
        DateFormatLbl: Label '<day,2>/<month,2>/<Year4>';
        /*  LongText: Text;
         SalesLineDescr: Text; */
        LocationCaptionLbl: Label 'Location';
        /*   RequestedDeliveryDateCaptionLbl: Label 'Requested Delivery Date';
          WhseEmplNameCaptionLbl: Label 'Warehouse Employee';
          WhseEmplName: Text;
          NetWeightCaptionLbl: Label 'Poids net :';
          PackageCountCaptionLbl: Label 'Nombre de colis :';
          ItemCrossRefNoCaptionLbl: Label 'Réf. externe';
          TotalNetWeight: Decimal;
          TabColis: array[5] of Text[30];
          "--- FAX ---": Integer; */

        no_fax: Text[30];
        nompdf: Text[80];
        Objet_fax: Text[100];
        Attention_fax: Text[50];
        PrintForFax: Boolean;
        //  TxtCodeDouanier: Text;
        "Nb colis": Integer;
        /*    DetailColis: Record "Packing List Package";
           "TxtDétailColis": Text[100];
           CommentColis: Text[60];
           Comment2Colis: Text[60];
           item: Record Item;
           TxtDescription: Text[61]; */
        arExport: Boolean;
        //    VATRegNoHdrCaptionLbl: Label 'V.A.T. Number  %1',;
        Text50000Lbl: Label 'L : %1 - l : %2 - h : %3 - Pb : %4 Kg. %5 article(s).', Comment = '%1 = Longeur ; %2 = LArgeur ; %3 = hauteur ; %4 = Poids Paquet; %5 = Qty';
        Text50001Lbl: Label 'Palette Hors CE';
        //  ESK001: Label 'Crédit Documentaire :';
        ESK002Lbl: Label 'Palette CE';
        "TxtDétail": Text;
        WeightCaptionLbl: Label 'Poids brut avec emballage';
        NbOfBoxCaptionLbl: Label 'Nb de Palette';
        //   CreditDocumentaireCaptionLbl: Label 'Documentary credit';
        CredocCaption: Text;
        NoPalletCaptionLbl: Text[50];
        text_DetailPackingList2: Text;

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(5)) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    local procedure FormatAddressFields(SalesShipmentHeader: Record "Sales Shipment Header");
    var
        lCustomer: Record Customer;
    begin
        FormatAddr.GetCompanyAddr(SalesShipmentHeader."Responsibility Center", RespCenter, CompanyInfo, CompanyAddr);
        FormatAddr.SalesShptShipTo(ShipToAddr, SalesShipmentHeader);
        ShowCustAddr := FormatAddr.SalesShptBillTo(CustAddr, ShipToAddr, SalesShipmentHeader);

        // ANI ESKVN1.0
        FormatAddr.SalesShptSellTo(SellToAddr, "Sales Shipment Header");
        //IF NOT ShowCustAddr THEN CLEAR(CustAddr);
        // FIN ANI ESKVN1.0
        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Invoice Customer No.") THEN
            lCustomer.INIT();
        IF CustAddr[7] = '' THEN CustAddr[7] := SalesShipmentHeader."Invoice Customer No.";
        //IF CustAddr[8] = '' THEN CustAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");

        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Sell-to Customer No.") THEN
            lCustomer.INIT();
        IF SellToAddr[7] = '' THEN SellToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF SellToAddr[8] = '' THEN SellToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
        IF ShipToAddr[7] = '' THEN ShipToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF ShipToAddr[8] = '' THEN ShipToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
    end;

    local procedure FormatDocumentFields(SalesShipmentHeader: Record "Sales Shipment Header");
    begin
        WITH SalesShipmentHeader DO BEGIN
            FormatDocument.SetSalesPerson(SalesPurchPerson, "Salesperson Code", SalesPersonText);
            ReferenceText := FormatDocument.SetText("Your Reference" <> '', FIELDCAPTION("Your Reference"));

            // ANI ESKVN1.0
            // BL Chiffré / TVA - ShowPrices
            IF "Currency Code" = '' THEN BEGIN
                GLSetup.TESTFIELD("LCY Code");
                TotalText := STRSUBSTNO(Text003Lbl, GLSetup."LCY Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, GLSetup."LCY Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, GLSetup."LCY Code");
            END ELSE BEGIN
                TotalText := STRSUBSTNO(Text003Lbl, "Currency Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, "Currency Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, "Currency Code");
            END;

            FormatDocument.SetShipmentMethod(ShipmentMethod, "Shipment Method Code", SalesShipmentHeader."Language Code");
            SetShippingAgent(ShippingAgent, "Shipping Agent Code");
            SetShippingAgentServices(ShippingAgentServices, "Shipping Agent Code", "Shipping Agent Service Code");

            SalesHeader.RESET();
            CodeUtilisateurCreation := '';
            /*
            IF SalesHeader.GET(SalesHeader."Document Type"::Order, "Order No.") THEN
              CodeUtilisateurCreation := SalesHeader."Code utilisateur création";
            */
            // FIN ANI ESKVN1.0


        END;

    end;

    local procedure GetDocumentCaption() Result: Text[250];
    var
        LSalesShptHeader: Record "Sales Shipment Header";
        LText50001Lbl: Label 'Shipment No. %1 - %2', Comment = '%1 = Packing List No; %2 = Posting Date';
        Text50002Lbl: Label 'COPY Shipment No. %1 - %2', Comment = '%1 = Packing List No; %2 = Posting Date';

    begin
        // ANI ESKVN1.0
        IF LSalesShptHeader.GET("Sales Shipment Header"."No.") THEN;

        IF LSalesShptHeader."No. Printed" = 0 THEN
            Result := STRSUBSTNO(LText50001Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormatLbl))
        ELSE
            Result := STRSUBSTNO(Text50002Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormatLbl));

        EXIT(Result);
        // FIN ANI ESKVN1.0
    end;

    procedure SetShippingAgent(var pShipmentAgent: Record "Shipping Agent"; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgent.INIT()
        ELSE
            pShipmentAgent.GET(Code);

    end;

    procedure SetShippingAgentServices(var pShipmentAgentServices: Record "Shipping Agent Services"; ShippingAgentCode: Code[10]; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgentServices.INIT()
        ELSE
            pShipmentAgentServices.GET(ShippingAgentCode, Code);

    end;

    procedure GetWhseEmployeeName(SalesShipmentHeader: Record "Sales Shipment Header") WhseEmplName: Text;
    var
        lWhseEmpl: Record "Warehouse Employee";
    begin
        WhseEmplName := '';
        IF lWhseEmpl.GET(SalesShipmentHeader.Preparateur, SalesShipmentHeader."Location Code") THEN
            WhseEmplName := SalesShipmentHeader.Preparateur;

    end;

    local procedure InitPageOption();
    begin
        DisplayAssemblyInformation := TRUE; // coché par défaut Redmine #688
        PrintLogo := TRUE;
        PrintForFax := FALSE;
        no_fax := '';
        Attention_fax := '';
        nompdf := '';
        Objet_fax := '';
    end;
}

