report 50101 "litiges fournisseurs"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\litiges fournisseurs.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'litiges fournisseurs';
    dataset
    {
        dataitem(Logo; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPicture2; CompanyInfo."Picture Address")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);
                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
                CompanyInfo.CALCFIELDS("Picture Address");

                GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
            end;
        }
        dataitem(Vendor; Vendor)
        {
            RequestFilterFields = "No.";
            column(VendorNo; "No.")
            {
            }
            column(VendorName; Name)
            {
            }
            column(VendorAddress; Address)
            {
            }
            column(VendorAddress2; "Address 2")
            {
            }
            column(VendorPostCode; "Post Code")
            {
            }
            column(VendorCity; City)
            {
            }
            dataitem("Gestion Litige"; "Gestion Litige")
            {
                DataItemLink = "Vendor No." = FIELD("No.");
                column(ReceiptNo; "Whse Receipt No.")
                {
                }
                column(ItemNo; "Item No.")
                {
                }
                column(ref_bl_fournisseur; ref_bl_fournisseur)
                {
                }
                column(RefActive; "Ref. Active")
                {
                }
                column(RefFournisseur; "Ref. Fournisseur")
                {
                }
                column(DateLitige; "Date Litige")
                {
                }
                column(QuantiteTheorique; "Quantité Théorique")
                {
                }
                column(ReceiptQuantity; "Receipt Quantity")
                {
                }
                column(PrixButTheorique; "Prix Brut Théorique")
                {
                }
                column(PrixButReceptionne; "Prix Brut Réceptionné")
                {
                }
                column(Remise1Theorique; "Remise 1 Théorique")
                {
                }
                column(Remise1Receptionnee; "Remise 1 Réceptionnée")
                {
                }
                column(Remise2Theorique; "Remise 2 Théorique")
                {
                }
                column(Remise2Receptionnee; "Remise 2 Réceptionnée")
                {
                }
                column(PrixNetUnitaireTheorique; "Prix Brut Théorique" * (100 - "Remise 1 Théorique") * (100 - "Remise 2 Théorique") / 10000)
                {
                }
                column(PrixNetUnitaireReceptionne; "Prix Brut Réceptionné" * (100 - "Remise 1 Réceptionnée") * (100 - "Remise 2 Réceptionnée") / 10000)
                {
                }
                column(TotalNetTheorique; "Total Net Théorique")
                {
                }
                column(TotalNetReceptionne; "Total Net  Réceptionné")
                {
                }
                column(EcartQte; "Receipt Quantity" - "Quantité Théorique")
                {
                }
                column(EcartMt; "Gestion Litige"."Total Net  Réceptionné" - "Total Net Théorique")
                {
                }

                trigger OnAfterGetRecord();
                begin

                    tot_qte += "Receipt Quantity" - "Quantité Théorique";
                    tot_mt += "Gestion Litige"."Total Net  Réceptionné" - "Total Net Théorique";

                    //ref bl fournisseur
                    /*AD Le 24-03-2016 => On va prendre celui de la ligne car Nathalie veut pouvoir le modifier
                    ref_bl_fournisseur:='';
                    IF rec_receipt_header.GET("Whse Receipt No.") THEN
                      ref_bl_fournisseur:=rec_receipt_header."Vendor Shipment No."
                    ELSE
                      BEGIN
                        rec_receipt_header_post.RESET();
                        rec_receipt_header_post.SETRANGE(rec_receipt_header_post."Whse. Receipt No.","Whse Receipt No.");
                        IF rec_receipt_header_post.FINDFIRST () THEN
                          ref_bl_fournisseur:=rec_receipt_header_post."Vendor Shipment No.";
                      END;
                    */
                    ref_bl_fournisseur := "Gestion Litige"."N° BL fournisseur";

                end;
            }

            trigger OnAfterGetRecord();
            begin


                CurrReport.LANGUAGE := LanguageG.GetLanguageID("Language Code");

                tot_qte := 0;
                tot_mt := 0;

                "Gestion Litige".RESET();
                "Gestion Litige".SETRANGE("Gestion Litige"."Vendor No.", Vendor."No.");
                IF "Gestion Litige".COUNT = 0 THEN CurrReport.SKIP();
            end;
        }
    }
    var
        /*  rec_receipt_header: Record "Warehouse Receipt Header";
            rec_receipt_header_post: Record "Posted Whse. Receipt Header"; */
        CompanyInfo: Record "Company Information";
        GestionEdition: Codeunit "Gestion Info Editions";
        LanguageG: codeunit Language;
        /*  _AfficheInfoSymta: Boolean;
         tab_InfoSymta: array[8] of Text[60];      */
        tot_qte: Decimal;
        tot_mt: Decimal;
        ref_bl_fournisseur: Text[35];
        /* PageConstLbl: Label 'Page';
        txtTitleLbl: Label 'RECLAMATIONS / CLAIMS'; */
        textSIEGSOC: array[3] of Text[250];
}

