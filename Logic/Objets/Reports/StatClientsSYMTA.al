report 50039 "Stat Clients SYMTA"
{
    Caption = 'Stat Clients SYMTA';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Customer; Customer)
        {
            RequestFilterFields = "Salesperson Code", "No.", "Date Filter";
            dataitem("Période"; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    ORDER(Ascending)
                                    WHERE(Number = FILTER(1 .. 5));

                trigger OnAfterGetRecord();
                var
                    Exercice: Record "Accounting Period";
                    Libelle: Text[30];
                    DateDeb: Date;
                    DateFin: Date;
                    CAMarqueStats: Decimal;
                    CATotal: Decimal;
                    CAMarque: Decimal;
                begin

                    CASE Période.Number OF
                        1:
                            BEGIN
                                Libelle := '1.PERIODE';
                                DateDeb := Date_deb_saisie;
                                DateFin := Date_fin_saisie;
                            END;
                        2:
                            BEGIN
                                Libelle := '2.ANNEE';
                                DateDeb := DMY2DATE(1, 1, DATE2DMY(Date_deb_saisie, 3));
                                DateFin := Date_fin_saisie;

                                Exercice.RESET();
                                Exercice.SETRANGE("Starting Date", 0D, Date_deb_saisie);
                                Exercice.SETRANGE("New Fiscal Year", TRUE);
                                Exercice.FINDLAST();

                                DateDeb := Exercice."Starting Date";
                                DateFin := Date_fin_saisie;

                            END;
                        3:
                            BEGIN
                                Libelle := '3.ANNEE_N-1';
                                DateDeb := DMY2DATE(1, 1, DATE2DMY(Date_deb_saisie, 3) - 1);
                                DateFin := DMY2DATE(31, 12, DATE2DMY(Date_deb_saisie, 3) - 1);

                                Exercice.RESET();
                                Exercice.SETRANGE("Starting Date", 0D, CALCDATE('<-1A>', Date_deb_saisie));
                                Exercice.SETRANGE("New Fiscal Year", TRUE);
                                Exercice.FINDLAST();

                                DateDeb := Exercice."Starting Date";
                                //ticket ID : 21176
                                //DateFin := CALCDATE('-1A', Date_fin_saisie);
                                DateFin := CALCDATE('<-1A+FM>', Date_fin_saisie);
                            END;
                        4:
                            BEGIN
                                Libelle := '4.PERIODE_N-1';
                                //ticket ID : 21176
                                //Datefin := CALCDATE('-1A', Date_deb_saisie);
                                DateDeb := CALCDATE('<-1A>', Date_deb_saisie);
                                DateFin := CALCDATE('<-1A+FM>', Date_fin_saisie);
                            END;
                    END;

                    // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
                    Customer.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
                    Customer.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
                    Customer.CALCFIELDS("Centrale Active");
                    // FIN CFR le 10/03/2021 - SFD20210201 historique centrale active

                    Ligne := Customer."No." + ';';
                    Ligne += Customer."Centrale Active" + ';';
                    Ligne += Customer.Name + ';';
                    Ligne += Customer.City + ';';
                    Ligne += COPYSTR(Customer."Post Code", 1, 2) + ';';
                    Ligne += Customer."Family Code 1" + ';';
                    Ligne += Customer."Salesperson Code" + ';';
                    Ligne += FormatDecimal(Customer."Rate Commission") + ';';
                    Ligne += Customer."Code Groupement RFA" + ';';
                    Ligne += Libelle + ';';

                    CAMarqueStats := 0;

                    Customer.SETRANGE("Date Filter", DateDeb, DateFin);

                    Marque.RESET();
                    //CFR le 18/11/2020 : Régie > Tri
                    Marque.SETCURRENTKEY("Tri Stats");
                    Marque.SETRANGE("Colonne Stats", TRUE);
                    //Marque.SETRANGE(Code, 'TRA');
                    Marque.FINDFIRST();
                    REPEAT

                        //CAMarque := CalculCA(Customer."No.", Marque.Code, DateDeb, DateFin);
                        Customer.SETRANGE("Filtre Marque", Marque.Code);
                        Customer.CALCFIELDS("Ca Ecritures Valeurs");
                        CAMarque := Customer."Ca Ecritures Valeurs";

                        CAMarqueStats += CAMarque;
                        Ligne += FormatDecimal(CAMarque) + ';';

                    UNTIL Marque.NEXT() = 0;


                    Customer.SETRANGE("Filtre Marque");
                    Customer.CALCFIELDS("Ca Ecritures Valeurs");
                    CATotal := Customer."Ca Ecritures Valeurs";

                    //CATotal := CalculCA(Customer."No.", '*', DateDeb, DateFin);

                    Ligne += FormatDecimal(CATotal - CAMarqueStats) + ';';

                    Ligne += FormatDecimal(CATotal);

                    Fichier.WRITE(Ligne);

                    //MESSAGE(Ligne);
                end;

                trigger OnPreDataItem();
                begin
                    SETRANGE(Number, 1, 5);
                end;
            }

            trigger OnAfterGetRecord();
            begin

                NoENr += 1;
                Window.UPDATE(1, FORMAT(NoENr) + '/' + FORMAT(NbEnr));
                Window.UPDATE(2, ROUND(NoENr / NbEnr * 10000, 1));
            end;

            trigger OnPreDataItem();
            begin

                //Customer.SETRANGE("Date Filter", 011211D, 311211D);

                IF Customer.GETFILTER("Date Filter") = '' THEN
                    ERROR(ESK001Err);

                Date_deb_saisie := Customer.GETRANGEMIN("Date Filter");
                Date_fin_saisie := Customer.GETRANGEMAX("Date Filter");
                //Customer.SETFILTER("No.", '%1', '003239-*');


                Fichier.SEEK(Fichier.LEN);

                Ligne := Customer.FIELDNAME("No.") + ';';
                Ligne += Customer.FIELDNAME("Centrale Active") + ';';
                Ligne += Customer.FIELDNAME(Name) + ';';
                Ligne += Customer.FIELDNAME(City) + ';';
                Ligne += Customer.FIELDNAME("Post Code") + ';';
                Ligne += Customer.FIELDNAME("Family Code 1") + ';';
                Ligne += Customer.FIELDNAME("Salesperson Code") + ';';
                Ligne += Customer.FIELDNAME("Rate Commission") + ';';
                Ligne += Customer.FIELDNAME("Code Groupement RFA") + ';';
                Ligne += 'PERIODE' + ';';

                Marque.RESET();
                //CFR le 18/11/2020 : Régie > Tri
                Marque.SETCURRENTKEY("Tri Stats");
                Marque.SETRANGE("Colonne Stats", TRUE);
                Marque.FINDFIRST();
                REPEAT
                    Ligne += Marque.Code + ';';
                UNTIL Marque.NEXT() = 0;


                Ligne += 'AUTRE' + ';';

                Ligne += 'TOTAL';

                Fichier.WRITE(Ligne);


                CLEAR(Window);
                Window.OPEN(ESK003Lbl);

                NbEnr := Customer.COUNT;


                //MESSAGE(Ligne);
            end;
        }
    }



    trigger OnPostReport();
    begin
        // on fait télécharger le programme au niveau du client
        NomFichier := 'CA STAT.csv';
        Fichier.CLOSE();
        DOWNLOAD(ExportFileName, 'Stat client', '', 'Fichiers csv|*.csv|Tous les fichiers|*.*', NomFichier);
    end;

    trigger OnPreReport();
    var
        FileMgt: Codeunit "File Management";
    begin
        // création d'un fichier temporaire sur le serveur
        Evaluate(ExportFileName, FileMgt.ServerTempFileName(''));
        Fichier.TEXTMODE := TRUE;
        Fichier.WRITEMODE := TRUE;
        Fichier.CREATE(ExportFileName);
    end;

    var
        Marque: Record Manufacturer;
        Date_deb_saisie: Date;
        Date_fin_saisie: Date;
        Fichier: File;
        Ligne: Text[1024];
        NbEnr: Integer;
        NoENr: Integer;
        Window: Dialog;
        ESK001Err: Label 'Filtre Date Obligatoire !';
        ESK003Lbl: Label '" MAJ  :  #1########## \ @2@@@@@@@@@@@@@@"', Comment = '#1 @2';
        ExportFileName: Text[260];
        NomFichier: Text;

    procedure CalculCA(_pCustomer: Code[20]; _pMarque: Code[20]; _pDateDeb: Date; _pDateFin: Date): Decimal;
    var
        "Value Entry": Record "Value Entry";
    begin
        "Value Entry".RESET();
        "Value Entry".SETCURRENTKEY(
        "Posting Date", "Source No.", "Manufacturer Code", "Item Ledger Entry Type");

        IF _pMarque = '*' THEN
            "Value Entry".SETCURRENTKEY(
            "Posting Date", "Source No.", "Item Ledger Entry Type");

        "Value Entry".SETRANGE("Posting Date", _pDateDeb, _pDateFin);
        "Value Entry".SETRANGE("Source No.", Customer."No.");
        "Value Entry".SETRANGE("Value Entry"."Item Ledger Entry Type", "Value Entry"."Item Ledger Entry Type"::Sale);

        IF _pMarque <> '*' THEN
            "Value Entry".SETRANGE("Manufacturer Code", _pMarque);


        "Value Entry".CALCSUMS("Sales Amount (Actual)");
        EXIT("Value Entry"."Sales Amount (Actual)");
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;
}

