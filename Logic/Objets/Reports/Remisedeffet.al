report 50054 "Remise d'effet"
{
    DefaultLayout = RDLC;
    caption = 'Remise d''effet';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    RDLCLayout = 'Objets\Reports\Layouts\Remise d''effet.rdlc';

    dataset
    {
        dataitem(LOGO; integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);

                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
            end;
        }
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("No.", "Due Date");
            RequestFilterFields = "No.";
            column(No; "No.")
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(BankName; bank.Name)
            {
            }
            column(BankAccountNo2; bank."Bank Account No.")
            {
            }
            column(nb_cheque; nb_cheque)
            {
            }
            column(clientName; client.Name)
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column(AcceptationCode; "Acceptation Code")
            {
            }
            column(BankBranchNo; "Bank Branch No.")
            {
            }
            column(AgencyCode; "Agency Code")
            {
            }
            column(BankAccountNo; "Bank Account No.")
            {
            }
            column(RIBKey; "RIB Key")
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(CreditAmount; "Credit Amount")
            {
            }

            trigger OnAfterGetRecord();
            begin

                client.GET("Payment Line"."Account No.");
                nb_cheque += 1;
            end;

            trigger OnPreDataItem();
            begin

                LastFieldNo := FIELDNO("No.");
                "tmp_ lig".COPY("Payment Line");
                IF "tmp_ lig".FIND('-') THEN;
                nb_cheque := "tmp_ lig".COUNT;
                entete.GET("tmp_ lig"."No.");

                bank.GET(entete."Account No.");
                FormatAddrCodeunit.BankAcc(CustAddr, bank);
                CompanyInfo.GET();
                FormatAddrCodeunit.Company(CompanyAddr, CompanyInfo);
                CompanyInfo.CALCFIELDS(Picture);
                CompanyInfo.CALCFIELDS(Picture);
                dessin.GET('compta');
                dessin.CALCFIELDS("Image 1");
            end;
        }
    }
    var

        CompanyInfo: Record "Company Information";
        bank: Record "Bank Account";
        entete: Record "Payment Header";
        "tmp_ lig": Record "Payment Line";
        client: Record Customer;
        dessin: Record "Parametre Edition Contrat";
        FormatAddrCodeunit: Codeunit "Format Address";
        nb_cheque: Integer;
        LastFieldNo: Integer;
        CustAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
}

