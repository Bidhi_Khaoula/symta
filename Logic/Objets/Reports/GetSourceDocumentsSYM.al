report 50006 "Get Source Documents SYM"//5753
{
    Caption = 'Get Source Documents';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = None;
    dataset
    {
        dataitem("Warehouse Request"; "Warehouse Request")
        {
            DataItemTableView = WHERE("Document Status" = CONST(Released),
                                      "Completely Handled" = FILTER(false));
            RequestFilterFields = "Source Document", "Source No.";
            dataitem("Sales Header"; "Sales Header")
            {
                DataItemLink = "Document Type" = FIELD("Source Subtype"),
                               "No." = FIELD("Source No.");
                DataItemTableView = SORTING("Document Type", "No.");
                dataitem("Sales Line"; "Sales Line")
                {
                    DataItemLink = "Document Type" = FIELD("Document Type"),
                                   "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                    trigger OnAfterGetRecord()
                    var
                        Rec_Location: Record Location;
                        CduLSalesWarehouseMgt: Codeunit "Sales Warehouse Mgt.";
                    begin

                        VerifyItemNotBlocked("No.");
                        IF "Location Code" = "Warehouse Request"."Location Code" THEN
                            CASE RequestType OF
                                RequestType::Receive:
                                    IF CduLSalesWarehouseMgt.CheckIfSalesLine2ReceiptLine("Sales Line") THEN BEGIN
                                        IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                            CreateReceiptHeader();
                                        IF NOT CduLSalesWarehouseMgt.SalesLine2ReceiptLine(WhseReceiptHeader, "Sales Line") THEN
                                            ErrorOccured := TRUE;
                                        LineCreated := TRUE;
                                    END;
                                RequestType::Ship:
                                    IF CduLSalesWarehouseMgt.CheckIfFromSalesLine2ShptLine("Sales Line") THEN BEGIN
                                        IF Cust.Blocked <> Cust.Blocked::" " THEN BEGIN
                                            IF NOT SalesHeaderCounted THEN BEGIN
                                                SkippedSourceDoc += 1;
                                                SalesHeaderCounted := TRUE;
                                            END;
                                            CurrReport.SKIP();
                                        END;

                                        // Création d'un BP par zone si le magasin est paramètré.
                                        rec_Location.GET("Location Code");
                                        IF rec_Location."Autoriser emplacement" THEN BEGIN
                                            // Si première fois que l'on passe alors création d'une en-tête de BP.
                                            IF ((LastZoneCode = '') AND (NOT bln_FirstHeaderCreate)) OR (LastZoneCode <> "Zone Code") THEN BEGIN
                                                NewZoneCode := "Zone Code";
                                                CreateShptHeader();
                                                bln_FirstHeaderCreate := TRUE;
                                            END;

                                            CduLSalesWarehouseMgt.FromSalesLine2ShptLine(WhseShptHeader, "Sales Line");
                                            LineCreated := TRUE;
                                            // On met à jour le derniere code zone.
                                            LastZoneCode := "Zone Code";
                                        END
                                        ELSE BEGIN
                                            // FIN ESK

                                            IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                                CreateShptHeader();
                                            IF NOT CduLSalesWarehouseMgt.FromSalesLine2ShptLine(WhseShptHeader, "Sales Line") THEN
                                                ErrorOccured := TRUE;
                                            LineCreated := TRUE;
                                        END; // AD ESK
                                    END;
                            END;
                    end;

                    trigger OnPostDataItem()
                    begin
                        IF WhseHeaderCreated THEN BEGIN
                            UpdateReceiptHeaderStatus();
                            CheckFillQtyToHandle();
                        END;
                    end;

                    trigger OnPreDataItem()
                    begin


                        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
                        SETCURRENTKEY("Zone Code");
                        // FIN MC Le 27-04-2011


                        SETRANGE(Type, Type::Item);
                        IF (("Warehouse Request".Type = "Warehouse Request".Type::Outbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Sales Order")) OR
                           (("Warehouse Request".Type = "Warehouse Request".Type::Inbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Sales Return Order"))
                        THEN
                            SETFILTER("Outstanding Quantity", '>0')
                        ELSE
                            SETFILTER("Outstanding Quantity", '<0');
                        SETRANGE("Drop Shipment", FALSE);
                        SETRANGE("Job No.", '');

                        // AD Le 23-03-2016 => MIG2015
                        IF (("Warehouse Request".Type = "Warehouse Request".Type::Outbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Sales Order")) THEN
                            "Sales Line".SETFILTER("Qty. to Ship", '>0');
                        // FIN AD Le 23-03-2016

                        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
                        IF ZoneCode <> '' THEN
                            SETRANGE("Zone Code", ZoneCode);
                        LastZoneCode := '';
                        // FIN MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    TESTFIELD("Sell-to Customer No.");
                    Cust.GET("Sell-to Customer No.");
                    IF NOT SkipBlockedCustomer THEN
                        Cust.CheckBlockedCustOnDocs(Cust, "Document Type", FALSE, FALSE);
                    SalesHeaderCounted := FALSE;
                end;

                trigger OnPreDataItem()
                begin
                    IF "Warehouse Request"."Source Type" <> DATABASE::"Sales Line" THEN
                        CurrReport.BREAK();
                end;
            }
            dataitem("Purchase Header"; "Purchase Header")
            {
                DataItemLink = "Document Type" = FIELD("Source Subtype"),
                               "No." = FIELD("Source No.");
                DataItemTableView = SORTING("Document Type", "No.");
                dataitem("Purchase Line"; "Purchase Line")
                {
                    DataItemLink = "Document Type" = FIELD("Document Type"),
                                   "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                    trigger OnAfterGetRecord()
                    var
                        CduLFunctions: Codeunit "Codeunits Functions";
                        CduLPurchasesWarehouseMgt: Codeunit "Purchases Warehouse Mgt.";

                    begin
                        VerifyItemNotBlocked("No.");
                        IF "Location Code" = "Warehouse Request"."Location Code" THEN
                            CASE RequestType OF
                                RequestType::Receive:

                                    // MC Le 14-09-2011 => Possibilité de mettre une ligne de commande sur deux réceptions différentes.

                                    IF NOT HideCheckDoc THEN BEGIN
                                        // FIN MC Le 14-09-2011

                                        IF CduLPurchasesWarehouseMgt.CheckIfPurchLine2ReceiptLine("Purchase Line") THEN BEGIN
                                            IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                                CreateReceiptHeader();
                                            IF NOT CduLPurchasesWarehouseMgt.PurchLine2ReceiptLine(WhseReceiptHeader, "Purchase Line") THEN
                                                ErrorOccured := TRUE;
                                            LineCreated := TRUE;
                                            NbLineCreate += 1; // AD Le 16-04-2012
                                        END;

                                        // MC Le 14-09-2011 => Possibilité de mettre une ligne de commande sur deux réceptions différentes.
                                    END
                                    ELSE BEGIN

                                        IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                            CreateReceiptHeader();
                                        // MCO Le 03-10-2018 => Régie : Ne pas mettre deux fois la meme ligne sur meme reception
                                        //MESSAGE('%1', WhseActivityCreate.CheckIfPurchLine2ReceiptHeader(WhseReceiptHeader,"Purchase Line"));
                                        IF NOT CduLFunctions.CheckIfPurchLine2ReceiptHeader(WhseReceiptHeader, "Purchase Line") THEN BEGIN
                                            // FIN MCO
                                            CduLPurchasesWarehouseMgt.PurchLine2ReceiptLine(WhseReceiptHeader, "Purchase Line");
                                            LineCreated := TRUE;
                                            NbLineCreate += 1; // AD Le 16-04-2012
                                        END;
                                    END;
                                // FIN MC Le 14-09-2011

                                RequestType::Ship:
                                    IF CduLPurchasesWarehouseMgt.CheckIfFromPurchLine2ShptLine("Purchase Line") THEN BEGIN
                                        IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                            CreateShptHeader();
                                        IF NOT CduLPurchasesWarehouseMgt.FromPurchLine2ShptLine(WhseShptHeader, "Purchase Line") THEN
                                            ErrorOccured := TRUE;
                                        LineCreated := TRUE;
                                        NbLineCreate += 1; // AD Le 16-04-2012
                                    END;
                            END;
                    end;

                    trigger OnPostDataItem()
                    begin
                        IF WhseHeaderCreated THEN BEGIN
                            UpdateReceiptHeaderStatus();
                            CheckFillQtyToHandle();
                        END;
                    end;

                    trigger OnPreDataItem()
                    begin
                        SETRANGE(Type, Type::Item);
                        IF (("Warehouse Request".Type = "Warehouse Request".Type::Inbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Purchase Order")) OR
                           (("Warehouse Request".Type = "Warehouse Request".Type::Outbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Purchase Return Order"))
                        THEN
                            SETFILTER("Outstanding Quantity", '>0')
                        ELSE
                            SETFILTER("Outstanding Quantity", '<0');
                        SETRANGE("Drop Shipment", FALSE);
                        SETRANGE("Job No.", '');
                    end;
                }

                trigger OnPreDataItem()
                begin
                    IF "Warehouse Request"."Source Type" <> DATABASE::"Purchase Line" THEN
                        CurrReport.BREAK();
                end;
            }
            dataitem("Transfer Header"; "Transfer Header")
            {
                DataItemLink = "No." = FIELD("Source No.");
                DataItemTableView = SORTING("No.");
                dataitem("Transfer Line"; "Transfer Line")
                {
                    DataItemLink = "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document No.", "Line No.");

                    trigger OnAfterGetRecord()
                    var
                        CduLTransferWarehouseMgt: Codeunit "Transfer Warehouse Mgt.";

                    begin
                        CASE RequestType OF
                            RequestType::Receive:
                                IF CduLTransferWarehouseMgt.CheckIfTransLine2ReceiptLine("Transfer Line") THEN BEGIN
                                    IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                        CreateReceiptHeader();
                                    IF NOT CduLTransferWarehouseMgt.TransLine2ReceiptLine(WhseReceiptHeader, "Transfer Line") THEN
                                        ErrorOccured := TRUE;
                                    LineCreated := TRUE;
                                END;
                            RequestType::Ship:
                                IF CduLTransferWarehouseMgt.CheckIfFromTransLine2ShptLine("Transfer Line") THEN BEGIN
                                    IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                        CreateShptHeader();
                                    IF NOT CduLTransferWarehouseMgt.FromTransLine2ShptLine(WhseShptHeader, "Transfer Line") THEN
                                        ErrorOccured := TRUE;
                                    LineCreated := TRUE;
                                END;
                        END;
                    end;

                    trigger OnPostDataItem()
                    begin
                        IF WhseHeaderCreated THEN BEGIN
                            UpdateReceiptHeaderStatus();
                            CheckFillQtyToHandle();
                        END;
                    end;

                    trigger OnPreDataItem()
                    begin
                        CASE "Warehouse Request"."Source Subtype" OF
                            0:
                                SETFILTER("Outstanding Quantity", '>0');
                            1:
                                SETFILTER("Qty. in Transit", '>0');
                        END;
                    end;
                }

                trigger OnPreDataItem()
                begin
                    IF "Warehouse Request"."Source Type" <> DATABASE::"Transfer Line" THEN
                        CurrReport.BREAK();
                end;
            }
            dataitem("Service Header"; "Service Header")
            {
                DataItemLink = "Document Type" = FIELD("Source Subtype"),
                               "No." = FIELD("Source No.");
                DataItemTableView = SORTING("Document Type", "No.");
                dataitem("Service Line"; "Service Line")
                {
                    DataItemLink = "Document Type" = FIELD("Document Type"),
                                   "Document No." = FIELD("No.");
                    DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");

                    trigger OnAfterGetRecord()
                    var
                        CduLServiceWarehouseMgt: Codeunit "Service Warehouse Mgt.";
                    begin
                        IF "Location Code" = "Warehouse Request"."Location Code" THEN
                            CASE RequestType OF
                                RequestType::Ship:
                                    IF CduLServiceWarehouseMgt.CheckIfFromServiceLine2ShptLine("Service Line") THEN BEGIN
                                        IF NOT OneHeaderCreated AND NOT WhseHeaderCreated THEN
                                            CreateShptHeader();
                                        IF NOT CduLServiceWarehouseMgt.FromServiceLine2ShptLine(WhseShptHeader, "Service Line") THEN
                                            ErrorOccured := TRUE;
                                        LineCreated := TRUE;
                                    END;
                            END;
                    end;

                    trigger OnPreDataItem()
                    begin
                        SETRANGE(Type, Type::Item);
                        IF (("Warehouse Request".Type = "Warehouse Request".Type::Outbound) AND
                            ("Warehouse Request"."Source Document" = "Warehouse Request"."Source Document"::"Service Order"))
                        THEN
                            SETFILTER("Outstanding Quantity", '>0')
                        ELSE
                            SETFILTER("Outstanding Quantity", '<0');
                        SETRANGE("Job No.", '');
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    TESTFIELD("Bill-to Customer No.");
                    Cust.GET("Bill-to Customer No.");
                    IF NOT SkipBlockedCustomer THEN
                        Cust.CheckBlockedCustOnDocs(Cust, "Document Type", FALSE, FALSE)
                    ELSE
                        IF Cust.Blocked <> Cust.Blocked::" " THEN
                            CurrReport.SKIP();
                end;

                trigger OnPreDataItem()
                begin
                    IF "Warehouse Request"."Source Type" <> DATABASE::"Service Line" THEN
                        CurrReport.BREAK();
                end;
            }

            trigger OnAfterGetRecord()
            var
                WhseSetup: Record "Warehouse Setup";
            begin
                WhseHeaderCreated := FALSE;
                CASE Type OF
                    Type::Inbound:
                        BEGIN
                            IF NOT Location.RequireReceive("Location Code") THEN BEGIN
                                IF "Location Code" = '' THEN
                                    WhseSetup.TESTFIELD("Require Receive");
                                Location.GET("Location Code");
                                Location.TESTFIELD("Require Receive");
                            END;
                            IF NOT OneHeaderCreated THEN
                                RequestType := RequestType::Receive;
                        END;
                    Type::Outbound:
                        BEGIN
                            IF NOT Location.RequireShipment("Location Code") THEN BEGIN
                                IF "Location Code" = '' THEN
                                    WhseSetup.TESTFIELD("Require Shipment");
                                Location.GET("Location Code");
                                Location.TESTFIELD("Require Shipment");
                            END;
                            IF NOT OneHeaderCreated THEN
                                RequestType := RequestType::Ship;
                        END;
                END;
            end;

            trigger OnPostDataItem()
            begin
                IF WhseHeaderCreated OR OneHeaderCreated THEN BEGIN
                    WhseShptHeader.SortWhseDoc();
                    WhseReceiptHeader.SortWhseDoc();
                END;
            end;

            trigger OnPreDataItem()
            begin
                IF OneHeaderCreated THEN BEGIN
                    CASE RequestType OF
                        RequestType::Receive:
                            Type := Type::Inbound;
                        RequestType::Ship:
                            Type := Type::Outbound;
                    END;
                    SETRANGE(Type, Type);
                END;

                // AD Le 26-05-2011 =>
                TempLstWhseShptHeaderCreate.DELETEALL();
                // FIN AD Le 26-05-2011
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(DoNotFillQtytoHandleName; DoNotFillQtytoHandle)
                    {
                        Caption = 'Do Not Fill Qty. to Handle';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Do Not Fill Qty. to Handle field.';
                    }
                }
            }
        }

    }
    trigger OnPostReport()
    begin
        IF NOT HideDialog THEN
            CASE RequestType OF
                RequestType::Receive:
                    ShowReceiptDialog();
                RequestType::Ship:
                    ShowShipmentDialog();
            END;
        IF SkippedSourceDoc > 0 THEN
            MESSAGE(CustomerIsBlockedMsg, SkippedSourceDoc);
        Completed := TRUE;
    end;

    trigger OnPreReport()
    begin
        ActivitiesCreated := 0;
        LineCreated := FALSE;
    end;

    var
        WhseReceiptHeader: Record "Warehouse Receipt Header";
        WhseReceiptLine: Record "Warehouse Receipt Line";
        WhseShptHeader: Record "Warehouse Shipment Header";
        WhseShptLine: Record "Warehouse Shipment Line";
        Location: Record Location;
        Cust: Record Customer;
        TempLstWhseShptHeaderCreate: Record "Warehouse Receipt Header" temporary;
        Text000Err: Label 'There are no Warehouse Receipt Lines created.';
        Text001Msg: Label '%1 %2 has been created.', Comment = '%1 = Activities Created ; %2= table Name';
        // WhseActivityCreate: Codeunit "Whse.-Create Source Document";
        ActivitiesCreated: Integer;
        OneHeaderCreated: Boolean;
        Completed: Boolean;
        LineCreated: Boolean;
        WhseHeaderCreated: Boolean;
        DoNotFillQtytoHandle: Boolean;
        HideDialog: Boolean;
        SkipBlockedCustomer: Boolean;
        SkipBlockedItem: Boolean;
        RequestType: Option Receive,Ship;
        SalesHeaderCounted: Boolean;
        SkippedSourceDoc: Integer;
        Text002Msg: Label '%1 Warehouse Receipts have been created.', Comment = '%1= Activities Created';
        Text003Msg: Label 'There are no Warehouse Shipment Lines created.';
        Text004Msg: Label '%1 Warehouse Shipments have been created.', Comment = '%1= Activities Created';
        ErrorOccured: Boolean;
        Text005Lbl: Label 'One or more of the lines on this %1 require special warehouse handling. The %2 for such lines has been set to blank.', Comment = '%1 = table Name ; %2=Field Name';
        CustomerIsBlockedMsg: Label '%1 source documents were not included because the customer is blocked.', Comment = '%1=Skipped Source Doc';
        ZoneCode: Code[10];
        LastZoneCode: Code[10];
        NewZoneCode: Code[10];
        bln_FirstHeaderCreate: Boolean;
        HideCheckDoc: Boolean;
        NbLineCreate: Integer;

    procedure SetHideDialog(NewHideDialog: Boolean)
    begin
        HideDialog := NewHideDialog;
    end;

    procedure SetOneCreatedShptHeader(WhseShptHeader2: Record "Warehouse Shipment Header")
    begin
        RequestType := RequestType::Ship;
        WhseShptHeader := WhseShptHeader2;
        IF WhseShptHeader.FIND() THEN
            OneHeaderCreated := TRUE;
    end;

    procedure SetOneCreatedReceiptHeader(WhseReceiptHeader2: Record "Warehouse Receipt Header")
    begin
        RequestType := RequestType::Receive;
        WhseReceiptHeader := WhseReceiptHeader2;
        IF WhseReceiptHeader.FIND() THEN
            OneHeaderCreated := TRUE;
    end;

    procedure SetDoNotFillQtytoHandle(DoNotFillQtytoHandle2: Boolean)
    begin
        DoNotFillQtytoHandle := DoNotFillQtytoHandle2;
    end;

    procedure GetLastShptHeader(var WhseShptHeader2: Record "Warehouse Shipment Header")
    begin
        RequestType := RequestType::Ship;
        WhseShptHeader2 := WhseShptHeader;
    end;

    procedure GetLastReceiptHeader(var WhseReceiptHeader2: Record "Warehouse Receipt Header")
    begin
        RequestType := RequestType::Receive;
        WhseReceiptHeader2 := WhseReceiptHeader;
    end;

    procedure NotCancelled(): Boolean
    begin
        EXIT(Completed);
    end;

    local procedure CreateShptHeader()
    var
        SaleHeader: Record "Sales Header";
    begin
        WhseShptHeader.INIT();
        WhseShptHeader."No." := '';
        WhseShptHeader."Location Code" := "Warehouse Request"."Location Code";
        IF Location.Code = WhseShptHeader."Location Code" THEN
            WhseShptHeader."Bin Code" := Location."Shipment Bin Code";

        // AD Le 19-11-2009 => Information du document d'origine
        WhseShptHeader."Source No." := "Warehouse Request"."Source No.";
        WhseShptHeader."Source Document" := "Warehouse Request"."Source Document".AsInteger();
        WhseShptHeader."Destination Type" := "Warehouse Request"."Destination Type".AsInteger();
        WhseShptHeader."Destination No." := "Warehouse Request"."Destination No.";
        //// FIN AD Le 19-11-2009

        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // Faire suivre le numéro de zone.
        WhseShptHeader."Zone Code" := NewZoneCode;
        // FIN MC Le 27-04-2011

        // MC Le 02-05-2011 => SYMTA -> Suivi des champs du document d'origne.
        WhseShptHeader."Shipping Agent Code" := "Warehouse Request"."Shipping Agent Code";
        IF SaleHeader.GET(SaleHeader."Document Type"::Order, WhseShptHeader."Source No.") THEN BEGIN
            WhseShptHeader."Mode d'expédition" := "Sales Header"."Mode d'expédition";
            WhseShptHeader.Assurance := SaleHeader."Insurance of Delivery";
            WhseShptHeader."Type de commande" := SaleHeader."Type de commande"; // AD Le 01-04-2015
                                                                                // CFR le 18/04/2024 - R‚gie : Suivi des champs du document d'origne
            WhseShptHeader."Shipment Method Code" := SaleHeader."Shipment Method Code";
        END;
        // FIN MC Le 02-05-2011

        WhseShptHeader."External Document No." := "Warehouse Request"."External Document No.";

        WhseShptLine.LOCKTABLE();
        WhseShptHeader.INSERT(TRUE);
        ActivitiesCreated := ActivitiesCreated + 1;
        WhseHeaderCreated := TRUE;

        //COMMIT; // MCO Le 04-10-2017 => Code standard plus utilisé en 2017

        // AD Le 26-05-2011 => On enregistre la liste de BP dans un table pour gérér l'édition après
        CLEAR(TempLstWhseShptHeaderCreate);
        TempLstWhseShptHeaderCreate."No." := WhseShptHeader."No.";
        TempLstWhseShptHeaderCreate.Insert();
        // FIN AD Le 26-05-2011
    end;

    local procedure CreateReceiptHeader()
    begin
        WhseReceiptHeader.INIT();
        WhseReceiptHeader."No." := '';
        WhseReceiptHeader."Location Code" := "Warehouse Request"."Location Code";
        IF Location.Code = WhseReceiptHeader."Location Code" THEN
            WhseReceiptHeader."Bin Code" := Location."Receipt Bin Code";
        WhseReceiptHeader."Vendor Shipment No." := "Warehouse Request"."External Document No.";
        WhseReceiptLine.LOCKTABLE();
        WhseReceiptHeader.INSERT(TRUE);
        ActivitiesCreated := ActivitiesCreated + 1;
        WhseHeaderCreated := TRUE;
        COMMIT();
    end;

    local procedure UpdateReceiptHeaderStatus()
    begin
        WITH WhseReceiptHeader DO BEGIN
            IF "No." = '' THEN
                EXIT;
            VALIDATE("Document Status", GetHeaderStatus(0));
            MODIFY(TRUE);
        END;
    end;

    procedure SetSkipBlocked(Skip: Boolean)
    begin
        SkipBlockedCustomer := Skip;
    end;

    procedure SetSkipBlockedItem(Skip: Boolean)
    begin
        SkipBlockedItem := Skip;
    end;

    local procedure VerifyItemNotBlocked(ItemNo: Code[20])
    var
        Item: Record Item;
    begin
        Item.GET(ItemNo);
        IF SkipBlockedItem AND Item.Blocked THEN
            CurrReport.SKIP();

        Item.TESTFIELD(Blocked, FALSE);
    end;

    procedure SetZoneCode(PNewZoneCode: Code[10])
    begin
        // MC Le 27-04-2011 => SYMTA -> Pouvoir avoir une zone sur un emplacement sans prelevement. -> Versionning : SYM-ZONE.
        // Cette fonction permet de créer le BP juste pour les lignes de la zone séléctionnée. (Selection de commande).
        ZoneCode := PNewZoneCode;
    end;

    procedure GetLstShpHeaderCreate(var _pNoBP: Code[20]): Boolean
    begin
        // AD Le 26-05-2011 => SYMTA -> Retour un no de BP et le supprime de la table

        _pNoBP := '';
        IF NOT TempLstWhseShptHeaderCreate.FINDFIRST() THEN EXIT(FALSE);

        _pNoBP := TempLstWhseShptHeaderCreate."No.";

        TempLstWhseShptHeaderCreate.DELETE();
        EXIT(TRUE);
        // FIN AD Le 26-05-2011
    end;

    procedure HideCheckIfLineInWhseDoc(New_HideCheckDoc: Boolean)
    begin
        // MC Le 14-09-2011 => Possibilité de mettre une ligne de commande sur deux réceptions différentes.
        HideCheckDoc := New_HideCheckDoc;
    end;

    procedure GetNbLineCreate(): Integer
    begin
        // AD Le 16-04-2012
        EXIT(NbLineCreate);
    end;

    procedure ShowReceiptDialog()
    var
        SpecialHandlingMessage: Text[1024];
    begin
        IF NOT LineCreated THEN
            ERROR(Text000Err);

        IF ErrorOccured THEN
            SpecialHandlingMessage :=
              ' ' + STRSUBSTNO(Text005Lbl, WhseReceiptHeader.TABLECAPTION, WhseReceiptLine.FIELDCAPTION("Bin Code"));
        IF (ActivitiesCreated = 0) AND LineCreated AND ErrorOccured THEN
            MESSAGE(SpecialHandlingMessage);
        IF ActivitiesCreated = 1 THEN
            MESSAGE(STRSUBSTNO(Text001Msg, ActivitiesCreated, WhseReceiptHeader.TABLECAPTION) + SpecialHandlingMessage);
        IF ActivitiesCreated > 1 THEN
            MESSAGE(STRSUBSTNO(Text002Msg, ActivitiesCreated) + SpecialHandlingMessage);
    end;

    procedure ShowShipmentDialog()
    var
        SpecialHandlingMessage: Text[1024];
    begin
        IF NOT LineCreated THEN
            ERROR(Text003Msg);

        IF ErrorOccured THEN
            SpecialHandlingMessage :=
              ' ' + STRSUBSTNO(Text005Lbl, WhseShptHeader.TABLECAPTION, WhseShptLine.FIELDCAPTION("Bin Code"));
        IF (ActivitiesCreated = 0) AND LineCreated AND ErrorOccured THEN
            MESSAGE(SpecialHandlingMessage);
        IF ActivitiesCreated = 1 THEN
            MESSAGE(STRSUBSTNO(Text001Msg, ActivitiesCreated, WhseShptHeader.TABLECAPTION) + SpecialHandlingMessage);
        IF ActivitiesCreated > 1 THEN
            MESSAGE(STRSUBSTNO(Text004Msg, ActivitiesCreated) + SpecialHandlingMessage);
    end;

    local procedure CheckFillQtyToHandle()
    begin
        IF DoNotFillQtytoHandle AND (RequestType = RequestType::Receive) THEN BEGIN
            WhseReceiptLine.RESET();
            WhseReceiptLine.SETRANGE("No.", WhseReceiptHeader."No.");
            WhseReceiptLine.DeleteQtyToReceive(WhseReceiptLine);
        END;
    end;
}

