report 50166 "Réappro. Picking"
{
    ProcessingOnly = true;
    Caption = 'Réappro. Picking';
    ApplicationArea = All;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending)
                                WHERE("Nb Contenu Emplacement" = FILTER(> 1),
                                      Inventory = FILTER(> 0));
            RequestFilterFields = "No.";

            trigger OnAfterGetRecord();
            var
                LBinContentPick: Record "Bin Content";
                LBinContentSurStk: Record "Bin Content";
                LPurchLine: Record "Purchase Line";
                LQtePointee: Decimal;
                LQteAReappro: Decimal;
                LSeuil: Decimal;
            begin

                NoEnr += 1;

                IF GUIALLOWED THEN BEGIN
                    Window.UPDATE(1, Item."No. 2");
                    Window.UPDATE(2, ROUND(NoEnr / NbEnr * 10000, 1));
                    Window.UPDATE(3, NbItem);
                END;


                // On recherche l'emplacement Picking
                LBinContentPick.SETRANGE("Item No.", Item."No.");
                LBinContentPick.SETRANGE("Location Code", CompanyInfo."Location Code");
                LBinContentPick.SETRANGE(Fixed, TRUE);
                LBinContentPick.SETRANGE(Default, TRUE);
                // AD Le 28-09-2016 => REGIE
                IF FiltreZone <> '' THEN
                    LBinContentPick.SETRANGE("Zone Code", FiltreZone);
                // FIN AD Le 28-09-2016

                IF NOT LBinContentPick.FINDFIRST() THEN
                    CurrReport.SKIP();

                // Si pas de capacité MAX
                IF LBinContentPick.capacité <= 0 THEN CurrReport.SKIP();

                // Calcul du stock de l'emplacement
                LBinContentPick.CALCFIELDS("Quantity (Base)");

                // Recherche des quantités pointées pour cet article
                LQtePointee := 0;
                CLEAR(LPurchLine);
                LPurchLine.SETRANGE("Document Type", LPurchLine."Document Type"::Order);
                LPurchLine.SETRANGE(Type, LPurchLine.Type::Item);
                LPurchLine.SETRANGE("No.", LBinContentPick."Item No.");
                LPurchLine.SETRANGE("Location Code", LBinContentPick."Location Code");
                LPurchLine.SETFILTER("Outstanding Qty. (Base)", '<>%1', 0);
                IF LPurchLine.FINDFIRST() THEN
                    REPEAT
                        // ANI le 14-02-2018 Régie on ne se base plus sur la qté pointée, mais la quantité réception
                        //IF LPurchLine.GetIfPointage THEN
                        //  LQtePointee += LPurchLine."Outstanding Qty. (Base)";
                        LQtePointee += LPurchLine.GetQtyOnReceipt();
                    // FIN ANI le 14-02-2018
                    UNTIL LPurchLine.NEXT() = 0;

                LSeuil := InventorySetup."% Seuil Capacité Réap. Pick.";
                IF LBinContentPick."% Seuil Capacité Réap. Pick." <> 0 THEN
                    LSeuil := LBinContentPick."% Seuil Capacité Réap. Pick.";

                // Si la quantité du casier + la qté pointée est suppérieure à xx% de la capacité du casier, on passe
                IF (LBinContentPick."Quantity (Base)" + LQtePointee) >
                    ((LBinContentPick.capacité * LSeuil) / 100) THEN
                    CurrReport.SKIP();

                // Calcul de la quantité a réapprovisionner
                LQteAReappro := LBinContentPick.capacité - (LBinContentPick."Quantity (Base)" + LQtePointee);

                // On réapprovisionne en fonction des autres emplacements
                LBinContentSurStk.SETRANGE("Item No.", Item."No.");
                LBinContentSurStk.SETRANGE("Location Code", CompanyInfo."Location Code");
                LBinContentSurStk.SETRANGE(Default, FALSE);
                LBinContentSurStk.SETFILTER("Quantity (Base)", '<>%1', 0);
                // MCO Le 13-02-2018 => Régie : Ne pas prendre en compte les emplacements de retours
                LBinContentSurStk.SETFILTER("Bin Code", '<>%1', 'RET-*');
                // FIN MCO Le 13-02-2018 => Régie : Ne pas prendre en compte les emplacements de retours
                // PMA le 21-02-19
                // On réapprovisionne en fonction de la date de dernière entrée des autres emplacements (FE20190117)
                LBinContentSurStk.SETCURRENTKEY("Last Entry Date");
                // FIN PMA le 21-02-19
                IF NOT LBinContentSurStk.FINDFIRST() THEN
                    CurrReport.SKIP();
                REPEAT
                    LBinContentSurStk.CALCFIELDS("Quantity (Base)");
                    IF LQteAReappro <= LBinContentSurStk."Quantity (Base)" THEN BEGIN
                        LogReappro(LBinContentSurStk."Item No.", LBinContentPick."Bin Code", LBinContentSurStk."Bin Code",
                          LQteAReappro);
                        LQteAReappro := 0;
                    END ELSE BEGIN
                        LogReappro(LBinContentSurStk."Item No.", LBinContentPick."Bin Code", LBinContentSurStk."Bin Code",
                          LBinContentSurStk."Quantity (Base)");
                        LQteAReappro -= LBinContentSurStk."Quantity (Base)";
                    END;
                UNTIL (LBinContentSurStk.NEXT() = 0) OR (LQteAReappro <= 0);
            end;

            trigger OnPostDataItem();
            begin

                GénérerLigneMiniload();
                GénérerLigneHorsMiniload();
            end;

            trigger OnPreDataItem();
            begin

                // Charge les valeurs par défaut
                CompanyInfo.GET();
                InventorySetup.GET();
                InventorySetup.TESTFIELD("% Seuil Capacité Réap. Pick.");
                InventorySetup.TESTFIELD("Mail Réappro. Picking");
                InventorySetup.TESTFIELD("Nom Feuille Réap. Pick. Minil.");
                InventorySetup.TESTFIELD("Nom Feuille Réappro. Picking");
                InventorySetup.TESTFIELD("Nom Feuille Réappro. PU"); // ANI le 14-02-2018 régie

                CompanyInfo.TESTFIELD("Location Code");
                Location.GET(CompanyInfo."Location Code");
                Location.TESTFIELD("Emplacement Tampon WIIO");

                SuffixeOut := '_O';
                SuffixeIn := '_I';

                NbItem := 0;

                NbEnr := Item.COUNT;


                IF GUIALLOWED THEN BEGIN
                    CLEAR(Window);
                    Window.OPEN(ESK003Lbl);
                END;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(Zone; FiltreZone)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FiltreZone field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var
                        LZone: Record Zone;
                        LFrmZone: Page Zones;
                    begin

                        CLEAR(LZone);
                        CompanyInfo.GET();
                        LZone.SETRANGE("Location Code", CompanyInfo."Location Code");
                        LFrmZone.SETTABLEVIEW(LZone);
                        LFrmZone.LOOKUPMODE(TRUE);

                        IF LFrmZone.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            LFrmZone.GETRECORD(LZone);
                            FiltreZone := LZone.Code;

                        END;
                    end;
                }
                field("Extraction PU"; ExtractionPU)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ExtractionPU field.';
                }
            }
        }
    }

    var
        CompanyInfo: Record "Company Information";
        InventorySetup: Record "Inventory Setup";
        Location: Record Location;
        TempItemJnlLine: Record "Item Journal Line" temporary;
        NbItem: Integer;
        NbEnr: Integer;
        NoEnr: Integer;
        Window: Dialog;
        ESK003Lbl: Label '" MAJ  :  #1########## \ @2@@@@@@@@@@@@@@\Réappro.  :  #3########## "', Comment = '%1 = ###; %2 = ### ; %3 = #####';
        ESK001Lbl: Label '"=""%1"""', Comment = '%1 = Bin Code';
        FiltreZone: Code[10];
        ExtractionPU: Boolean;

        SuffixeIn: Code[4];
        SuffixeOut: Code[4];

    procedure LogReappro(_pNoArticle: Code[20]; _pEmplacementDest: Code[20]; _pEmplacementOrigine: Code[20]; _pQte: Decimal);
    begin
        // On met dans une feuille article temporaire les articles à réapprovisionner
        NbItem += 1;
        CLEAR(TempItemJnlLine);
        TempItemJnlLine."Line No." := NbItem;
        TempItemJnlLine."Item No." := _pNoArticle;
        TempItemJnlLine."Bin Code" := _pEmplacementOrigine;
        TempItemJnlLine."New Bin Code" := _pEmplacementDest;
        TempItemJnlLine.Quantity := _pQte;
        TempItemJnlLine.Insert();
    end;

    procedure "GénérerLigneMiniload"();
    var
        LLineNo: Integer;
    begin
        IF ExtractionPU THEN EXIT; // MCO
        LLineNo := 10000;


        // Pour tous ce qui vient du miniload, on met dans une feuille de reclassement miniload
        CLEAR(TempItemJnlLine);
        TempItemJnlLine.SETRANGE("Bin Code", InventorySetup."Code emplacement MiniLoad");
        IF NOT TempItemJnlLine.FINDFIRST() THEN BEGIN
            IF GUIALLOWED THEN MESSAGE('Aucune ligne pour le miniload !');
            EXIT;
        END;

        // Création de la feuille si elle n'existe pas et regarde s'il existe des ligne
        IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réap. Pick. Minil.", TRUE) THEN BEGIN
            IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le miniload !');
            EXIT;
        END;


        // Créer Ligne Reclassement
        REPEAT
            CréerLigneReclassement(InventorySetup."Nom Feuille Réap. Pick. Minil.", LLineNo,
                             TempItemJnlLine."Item No.", TempItemJnlLine."Bin Code",
                             TempItemJnlLine."New Bin Code", TempItemJnlLine.Quantity);

        UNTIL TempItemJnlLine.NEXT() = 0;
    end;

    procedure "GénérerLigneHorsMiniload"();
    var
        LItem: Record Item;
        LFile: File;
        LLine: Text[1024];
        //cu_SMTPMail: Codeunit 400;
        str_Cr: Text[30];
        str_Object: Text[250];
        str_Body: Text[1024];
        LFileName: Text[1024];
        LLineNo: Integer;
        lNomFeuille: Code[10];
        bln_LignePU: Boolean;
    begin
        LLineNo := 10000;

        IF NOT ExtractionPU THEN BEGIN
            ; // MCO
              // Pour tous ce qui ne vient pas du miniload, on met dans un fichier Csv qui sera joint au mail
            CLEAR(TempItemJnlLine);
            TempItemJnlLine.SETFILTER("Bin Code", '<>%1', InventorySetup."Code emplacement MiniLoad");
            IF NOT TempItemJnlLine.FINDFIRST() THEN BEGIN
                IF GUIALLOWED THEN MESSAGE('Aucune ligne pour le surstock Magasin !');
                EXIT;
            END;

            // AD Le 02-06-2020 => WIIO -> On split la feuille en deux
            /*
            // Création de la feuille si elle n'existe pas et regarde si il existe des ligne
            IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. Picking" + FiltreZone, FALSE) THEN BEGIN
              IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le surstock Magasin !');
              EXIT;
            END;
            */
            IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. Picking" + FiltreZone + SuffixeOut, FALSE) THEN BEGIN // Surstock -> Tampon
                IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le surstock vers Tampon Magasin !');
                EXIT;
            END;

            IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. Picking" + FiltreZone + SuffixeIn, FALSE) THEN BEGIN // Tampon -> Picking
                IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le tampon vers Picking Magasin !');
                EXIT;
            END;
            // FIN AD Le 02-06-2020
        END;

        // ANI le 14-02-2018 régie

        IF ExtractionPU THEN BEGIN // MCO
                                   // AD Le 02-06-2020 => WIIO -> On split la feuille en deux
                                   /*
                                   IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. PU" + FiltreZone, FALSE) THEN BEGIN
                                     IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le surstock Magasin PU !');
                                     EXIT;
                                   END;
                                   */
            IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. PU" + FiltreZone + SuffixeOut, FALSE) THEN BEGIN
                IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le surstock vers Tampon Magasin PU !');
                EXIT;
            END;
            IF CréerFeuilleEtVerif(InventorySetup."Nom Feuille Réappro. PU" + FiltreZone + SuffixeIn, FALSE) THEN BEGIN
                IF GUIALLOWED THEN MESSAGE('Déjà des lignes pour le tampon vers Picking Magasin PU !');
                EXIT;
            END;
            // FIN AD Le 02-06-2020

            // FIN ANI le 14-02-2018
        END;
        // On ouvre un fichier Temporaire
        LFileName := TEMPORARYPATH + '/REAPPRO-PICKING.csv';
        IF EXISTS(LFileName) THEN ERASE(LFileName);

        LFile.CREATE(LFileName);
        LFile.TEXTMODE(TRUE);

        // On écrit la ligne de titre
        LFile.WRITE('No Article;Emplacement Source;Emplacement destination;Quantité');

        // On écrit chaque ligne d'article à réapprovisionner
        IF TempItemJnlLine.FINDFIRST() THEN
            REPEAT
                LItem.GET(TempItemJnlLine."Item No.");

                // ANI Le 14-02-2018 Régie séparation des pièces d'usure
                lNomFeuille := InventorySetup."Nom Feuille Réappro. Picking";

                IF STRLEN(TempItemJnlLine."New Bin Code") > 0 THEN
                    IF (COPYSTR(TempItemJnlLine."New Bin Code", 1, 1) = '8') AND (STRLEN(TempItemJnlLine."New Bin Code") >= 3) THEN BEGIN // = filtre 8??* = Pièce d'usure
                        lNomFeuille := InventorySetup."Nom Feuille Réappro. PU";
                        bln_LignePU := TRUE;
                    END
                    ELSE
                        bln_LignePU := FALSE;
                // FIN ANI le 14-02-2018
                // MCO Le 04-10-2018 => Régie
                lNomFeuille += FiltreZone;

                // FIN MCO Le 04-10-2018
                IF ((bln_LignePU) AND (ExtractionPU)) OR
                   (NOT bln_LignePU) AND (NOT ExtractionPU) THEN BEGIN
                    LLine := LItem."No. 2" + ';';
                    LLine += STRSUBSTNO(ESK001Lbl, TempItemJnlLine."Bin Code") + ';';
                    LLine += STRSUBSTNO(ESK001Lbl, TempItemJnlLine."New Bin Code") + ';';
                    LLine += FORMAT(TempItemJnlLine.Quantity) + ';';

                    LFile.WRITE(LLine);

                    // AD Le 02-06-2020 => WIIO -> On split la feuille en deux
                    /*
                    CréerLigneReclassement(lNomFeuille,LLineNo, // ANI Le 14-02-2018 Régie
                                     TempItemJnlLine."Item No.", TempItemJnlLine."Bin Code",
                                     TempItemJnlLine."New Bin Code", TempItemJnlLine.Quantity);
                    */
                    CréerLigneReclassement(lNomFeuille + SuffixeOut, LLineNo, // ANI Le 14-02-2018 Régie
                                  TempItemJnlLine."Item No.", TempItemJnlLine."Bin Code",
                                  Location."Emplacement Tampon WIIO", TempItemJnlLine.Quantity);

                    /* CFR Le 03/09/2020 : on ne remplit pas la feuille Picking : c'est la validation de feuille Surstock qui va le faire
                    CréerLigneReclassement(lNomFeuille + SuffixeIn,LLineNo, // ANI Le 14-02-2018 Régie
                                  TempItemJnlLine."Item No.", Location."Emplacement Tampon WIIO",
                                  TempItemJnlLine."New Bin Code", TempItemJnlLine.Quantity);
                    */
                    // FIN AD Le 02-06-2020

                END;
            UNTIL TempItemJnlLine.NEXT() = 0;

        // Fermeture du fichier
        LFile.CLOSE();

        COMMIT();
        // Création du mail à envoyer à l'utilisateur
        str_Cr := '<br />';

        //Construit le différents textes dee l'email
        str_Object := 'Fichier réappro Picking du ' + FORMAT(WORKDATE());
        str_Body := 'Voici le fichier de réappro picking du ' + FORMAT(WORKDATE()) +
                      str_Cr + str_Cr +
                      'Ce fichier CSV peut être ouvert avec Excel !' +
                      str_Cr;

        //Envois l'email via SMTP NAV
        /*
        CLEAR(cu_SMTPMail);
        cu_SMTPMail.CreateMessage(CompanyInfo.Name, CompanyInfo."E-Mail",
                  InventorySetup."Mail Réappro. Picking", str_Object, str_Body, TRUE);
        
        cu_SMTPMail.AddAttachment(LFileName, LFileName);
        cu_SMTPMail.Send();
        */
        IF EXISTS(LFileName) THEN ERASE(LFileName);

    end;

    procedure "CréerFeuilleEtVerif"(_pNomFeuille: Code[10]; _pMiniload: Boolean): Boolean;
    var
        LItemJournalBatch: Record "Item Journal Batch";
        LItemJnlLine: Record "Item Journal Line";
    begin
        LItemJournalBatch.SETRANGE("Journal Template Name", 'RECLASS');
        LItemJournalBatch.SETRANGE(Name, _pNomFeuille);
        LItemJournalBatch.SETRANGE(MiniLoad, _pMiniload);
        IF NOT LItemJournalBatch.FINDFIRST() THEN BEGIN
            // Création de la feuille si elle n'existe pas
            LItemJournalBatch.VALIDATE("Journal Template Name", 'RECLASS');
            LItemJournalBatch.VALIDATE(Name, _pNomFeuille);
            LItemJournalBatch.VALIDATE(MiniLoad, _pMiniload);
            LItemJournalBatch.SetupNewBatch();
            LItemJournalBatch."Posting No. Series" := '';

            // AD Le 02-06-2020 => WIIO -> On split la feuille en deux
            IF _pMiniload THEN
                LItemJournalBatch."Type Feuille Reclassement WIIO" := LItemJournalBatch."Type Feuille Reclassement WIIO"::"Sortie Miniload"
            ELSE
                CASE TRUE OF
                    STRPOS(_pNomFeuille, SuffixeOut) <> 0:
                        LItemJournalBatch."Type Feuille Reclassement WIIO" := LItemJournalBatch."Type Feuille Reclassement WIIO"::Surstock;
                    STRPOS(_pNomFeuille, SuffixeIn) <> 0:
                        LItemJournalBatch."Type Feuille Reclassement WIIO" := LItemJournalBatch."Type Feuille Reclassement WIIO"::Picking;
                END;
            // FIN AD Le 02-06-2020

            LItemJournalBatch.INSERT(TRUE);
        END;


        //Si la feuille de réappro picking n'est pas vide, on ne fait rien pour pas déclencher 2 fois
        CLEAR(LItemJnlLine);
        LItemJnlLine.SETRANGE("Journal Template Name", LItemJournalBatch."Journal Template Name");
        LItemJnlLine.SETRANGE("Journal Batch Name", LItemJournalBatch.Name);
        IF LItemJnlLine.FINDFIRST() THEN
            EXIT(TRUE);
    end;

    procedure "CréerLigneReclassement"(_pNomFeuille: Code[10]; var _pNoLigne: Integer; _pCodeArticle: Code[20]; _pEmplSource: Code[20]; _pEmplDestination: Code[20]; _pQte: Decimal);
    var
        LItemJnlLine: Record "Item Journal Line";
    begin
        // MCO Le 31-01-2018 => Ticket 28970 => Passe les deux param d'emplacement a 20 caractères.
        // On va créer les lignes de réappro pour le miniload
        // initialisation des données génériques
        CLEAR(LItemJnlLine);
        LItemJnlLine.VALIDATE("Journal Template Name", 'RECLASS');
        LItemJnlLine.VALIDATE("Journal Batch Name", _pNomFeuille);
        LItemJnlLine.VALIDATE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
        LItemJnlLine.VALIDATE("Posting Date", WORKDATE());
        LItemJnlLine.VALIDATE("Document No.", 'RP-' + FORMAT(WORKDATE()));

        LItemJnlLine.VALIDATE("Line No.", _pNoLigne);
        _pNoLigne += 10000;
        LItemJnlLine.VALIDATE("Item No.", _pCodeArticle);
        LItemJnlLine.VALIDATE("Location Code", CompanyInfo."Location Code");
        LItemJnlLine.VALIDATE("Bin Code", _pEmplSource);
        LItemJnlLine.VALIDATE("New Location Code", CompanyInfo."Location Code");
        LItemJnlLine.VALIDATE("New Bin Code", _pEmplDestination);
        LItemJnlLine.VALIDATE(Quantity, _pQte);
        LItemJnlLine.INSERT(TRUE);
    end;
}

