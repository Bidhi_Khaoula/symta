report 50160 "Réappro SYMTA (Licence) v2"
{

    ProcessingOnly = true;
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    Caption = 'Réappro SYMTA (Licence) v2';
    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "Code Appro", "No. 2", "Manufacturer Code", "Item Category Code", "Vendor No.", "Filtre Code Remise Réappro", "Gen. Prod. Posting Group", "No.", "Date Dernière Sortie";
            column(No; "No.")
            {
            }
            column(No2; "No. 2")
            {
            }
            column(Description; Description)
            {
            }

            trigger OnAfterGetRecord();
            var
                ItemVendor: Record "Item Vendor";
                LPurchLine: Record "Purchase Line";
                "ChaineMoisSelectionné": Code[12];
                ReapproMini: Decimal;
                ReapproMaxi: Decimal;
                Reserve: Decimal;
                Attendu: Decimal;
                TmpReserve: Decimal;
                TmpAttendu: Decimal;
                QteComparaison: Decimal;
                ConsoMoisEncours: Decimal;
                MiniEncours: Decimal;
                Formule: Decimal;
                QteReappro: Decimal;
                LQteDepart: Decimal;
                "---": Integer;
                lOKDate: Boolean;
            begin

                NoArticle += 1;
                Window2.UPDATE(1, Item."No.");
                Window2.UPDATE(2, ROUND(NoArticle * 10000 / NbArticle, 1));

                // LM le 02-08-2012 =>on ne reappro par l'article si la date de derniere commande client est inferieure à la date saisie
                IF datdercdecli <> 0D THEN
                    IF lm_fonction.dat_der_commande_client(Item."No.") < datdercdecli THEN
                        CurrReport.SKIP();

                // On ne reappro l'article qui si il est fourni par un des fournisseur selectionné
                IF gFiltreFournisseurArticle <> '' THEN BEGIN
                    ItemVendor.RESET();
                    ItemVendor.SETRANGE("Item No.", Item."No.");
                    ItemVendor.SETFILTER("Vendor No.", gFiltreFournisseurArticle);
                    // AD Le 16-04-2012 => Demande de NG -> Si filtre frn on ne prend que les conforme, non bloqué
                    ItemVendor.SETRANGE("Statut Qualité", ItemVendor."Statut Qualité"::Conforme);
                    ItemVendor.SETRANGE("Statut Approvisionnement", ItemVendor."Statut Approvisionnement"::"Non Bloqué");
                    // FIN AD Le
                    // CFR le 28/09/2023 => Régie - Réappro : filtre Item Vendor sur Code Remise
                    IF (Item.GETFILTER("Filtre Code Remise Réappro") <> '') THEN
                        ItemVendor.SETFILTER("Code Remise", Item.GETFILTER("Filtre Code Remise Réappro"))
                    ELSE
                        ItemVendor.SETRANGE("Code Remise");
                    // FIN CFR le 28/09/2023
                    IF NOT ItemVendor.FINDFIRST() THEN
                        CurrReport.SKIP();
                END;

                // Calcul des reservé et attendus

                Item.CALCFIELDS(Inventory);

                IF DateLimiteReserve <> 0D THEN
                    Item.SETRANGE("Date Filter", 0D, DateLimiteReserve);

                Item.CalcAttenduReserve(Reserve, TmpAttendu);

                IF DateLimiteAttendu <> 0D THEN
                    Item.SETRANGE("Date Filter", 0D, DateLimiteAttendu);

                Item.CalcAttenduReserve(TmpReserve, Attendu);

                // AD Le 01-04-2015 => On va ajouter les qtes départ
                IF (DateLimiteAttendu <> 0D) AND (PrendreQteDepart) THEN BEGIN
                    CLEAR(LPurchLine);
                    LPurchLine.SETRANGE("Document Type", LPurchLine."Document Type"::Order);
                    LPurchLine.SETRANGE(Type, LPurchLine.Type::Item);
                    LPurchLine.SETRANGE("No.", Item."No.");
                    // CFR le 28/09/2023 => Régie : Utilisation de [Promised Receipt Date] par défaut
                    //LPurchLine.SETRANGE("Expected Receipt Date", DateLimiteAttendu, 31123999D);
                    LPurchLine.SETFILTER("Promised Receipt Date", '%1|%2..%3', 0D, DateLimiteAttendu, 39991231D);
                    // FIN CFR le 28/09/2023
                    LQteDepart := 0;
                    IF LPurchLine.FINDFIRST() THEN
                        REPEAT
                            // CFR le 28/09/2023 => Régie : Utilisation de [Promised Receipt Date] par défaut
                            lOKDate := TRUE;
                            IF (LPurchLine."Promised Receipt Date" = 0D) AND (LPurchLine."Requested Receipt Date" < DateLimiteAttendu) THEN
                                lOKDate := FALSE;
                            IF lOKDate THEN
                                // FIN CFR le 28/09/2023
                                LQteDepart += LPurchLine.GetQtyOnDepart();
                        UNTIL LPurchLine.NEXT() = 0;
                    Attendu += LQteDepart;
                END;
                // FIN AD Le 01-04-2015
                // CFR le 18/01/2024 => Régie : Ajout "Qty. on Asm. Component" pour APPRO uniquement
                Item.CALCFIELDS("Qty. on Asm. Component");
                Reserve += Item."Qty. on Asm. Component";
                // FIN CFR le 18/01/2024




                // Si l'article est non stocké, et pas de besoin, on passe l'article
                IF NOT (Item.Stocké) THEN BEGIN
                    IF ((Item.Inventory + Attendu - Reserve) >= 0) THEN
                        CurrReport.SKIP()
                    ELSE
                        QteReappro := -1 * (Item.Inventory + Attendu - Reserve);
                END
                ELSE BEGIN

                    // On construit la chaine qui sera utiliser pour la fonction des appro
                    ChaineMoisSelectionné := '';
                    FOR i := 1 TO 12 DO
                        IF TabMoisSélectionnés[i] THEN
                            ChaineMoisSelectionné := ChaineMoisSelectionné + 'O'
                        ELSE
                            ChaineMoisSelectionné := ChaineMoisSelectionné + 'N';

                    // Calcul des variables utiles aux reppro
                    Item.CALCFIELDS(Inventory);

                    ReapproMini := CUCalculReappro.CalculConso(Item."No.", DateDeBase, ChaineMoisSelectionné, NbreAnnée, FALSE);
                    ReapproMaxi := CUCalculReappro.CalculConso(Item."No.", DateDeBase, 'OOOOOOOOOOOO', NbreAnnée, FALSE);


                    ConsoMoisEncours := CUCalculReappro.RechecherConsommation(Item."No.", DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3)),
                                            CALCDATE('<+FM>', WORKDATE()));

                    CASE TypeComparasion OF
                        TypeComparasion::Disponible:
                            QteComparaison := Item.Inventory - Reserve;
                        TypeComparasion::"A terme":
                            QteComparaison := Item.Inventory - Reserve + Attendu;
                        TypeComparasion::Rien:
                            QteComparaison := Item.Inventory - Reserve + Attendu;
                    END;

                    CASE TypeProjection OF
                        TypeProjection::Mensuelle:
                            MiniEncours := ReapproMini;
                        TypeProjection::"Mensuelle + Encours":
                            MiniEncours := ReapproMini + ConsoMoisEncours;
                    END;

                    IF PondérationProjection <> 0 THEN BEGIN
                        Formule := (ReapproMaxi + ConsoMoisEncours) / PondérationProjection;
                        Formule := ROUND(Formule, 1, '>');
                        IF (MiniEncours > Formule) AND (MiniEncours > QteComparaison) THEN
                            QteReappro := MiniEncours - QteComparaison
                        ELSE
                            IF (Formule > QteComparaison) THEN
                                QteReappro := Formule - QteComparaison;
                    END
                    ELSE
                        QteReappro := MiniEncours - QteComparaison;

                    //MESSAGE('A réapprovisionner : %1', QteReappro);
                END;


                // AD Le 16-04-2012 => Je rajoute le < dans le <ou= sur QteReappro
                IF (QteReappro <= 0) AND (TypeComparasion <> TypeComparasion::Rien) THEN
                    CurrReport.SKIP();


                // -----------------------------------------
                // Génération de la ligne de demande d'achat
                // -----------------------------------------
                LineNo += 10000;
                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                TempGRequisitionLine.VALIDATE("Worksheet Template Name", CurrTemplateName);
                TempGRequisitionLine.VALIDATE("Journal Batch Name", CurrWorksheetName);
                TempGRequisitionLine.VALIDATE("Line No.", LineNo);
                TempGRequisitionLine.VALIDATE("Reservé Calculé", Reserve);
                TempGRequisitionLine.VALIDATE("Attendu Calculé", Attendu);
                TempGRequisitionLine.VALIDATE("Projection Mini", ReapproMini);
                TempGRequisitionLine.VALIDATE("Projection Maxi", ReapproMaxi);
                TempGRequisitionLine.VALIDATE("Conso Mois En Cours", ConsoMoisEncours);
                TempGRequisitionLine.VALIDATE("Mini Encours", MiniEncours);
                TempGRequisitionLine.VALIDATE(Formule, Formule);
                TempGRequisitionLine.VALIDATE("Chaine Mois", ChaineMoisSelectionné);
                TempGRequisitionLine.VALIDATE("Type Comparaison", FORMAT(TypeComparasion));
                TempGRequisitionLine.VALIDATE("Type Projection", FORMAT(TypeProjection));
                TempGRequisitionLine.VALIDATE("Type de commande", TypeCommande);
                // CFR le 18/10/2022 => Régie : ajout [FiltreFournisseurAExclure]
                TempGRequisitionLine.VALIDATE(FiltreFournisseurAExclure, gFiltreFournisseurAExclureTarif);
                TempGRequisitionLine.VALIDATE(TriEcartMarge, gTriEcartMarge);

                CUCalculReappro.CréerLigneAppro(Item, QteReappro, TempGRequisitionLine);


                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                //COMMIT;
            end;

            trigger OnPostDataItem();
            var
                LLigne: Integer;
            begin
                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                IF nb_milli_sec = 0 THEN
                    IF NOT lm_fonction.user_appro_entree() THEN CurrReport.QUIT();

                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                gRequisitionLine.RESET();
                gRequisitionLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
                gRequisitionLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
                gRequisitionLine.DELETEALL();

                LLigne := 0;
                TempGRequisitionLine.RESET();
                TempGRequisitionLine.SETCURRENTKEY("Fournisseur Calculé", "Ref. Active");
                TempGRequisitionLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
                TempGRequisitionLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
                IF TempGRequisitionLine.FINDSET() THEN
                    REPEAT
                        gRequisitionLine.TRANSFERFIELDS(TempGRequisitionLine);
                        LLigne += 10000;
                        gRequisitionLine."Indice Jauge" := LLigne;
                        gRequisitionLine.INSERT();
                    UNTIL TempGRequisitionLine.NEXT() = 0;

                /*{LLigne := 0;
gRequisitionLine.RESET();
gRequisitionLine.SETCURRENTKEY("Fournisseur Calculé", "Ref. Active");
gRequisitionLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
gRequisitionLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
IF gRequisitionLine.FINDFIRST () THEN
    REPEAT
        LLigne += 10000;
        gRequisitionLine."Indice Jauge" := LLigne;
        gRequisitionLine.MODIFY();
    UNTIL gRequisitionLine.NEXT() = 0;
                }*/
                Window2.CLOSE();
                //LM le 31-07-2012: delock le user appro
                lm_fonction.user_appro_sortie();
            end;

            trigger OnPreDataItem();
            var
                "Requisition Wksh. Name": Record "Requisition Wksh. Name";
                LUser: Record User;
            begin

                //Item.SETRANGE("No.", 'SP030232');
                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                /* {
                 //LM le 31-07-2012=> informe si des users sont en cours de calcul  (si traitement non différé)
                 IF nb_milli_sec = 0 THEN
      IF NOT lm_fonction.user_appro_entree() THEN CurrReport.QUIT;
                 }*/
                //LM le 27-07-12 =>temporisation
                SLEEP(nb_milli_sec * 60000);

                Item.SETRANGE("Location Filter", 'SP');



                IF CurrTemplateName = '' THEN
                    CurrTemplateName := 'PROP';


                IF CurrWorksheetName = '' THEN
                    CurrWorksheetName := COPYSTR(USERID, 1, 10);

                IF Item.GETFILTER("Location Filter") = '' THEN
                    ERROR(Text50000);


                "Requisition Wksh. Name".INIT();
                "Requisition Wksh. Name".VALIDATE("Worksheet Template Name", CurrTemplateName);
                "Requisition Wksh. Name".VALIDATE(Name, CurrWorksheetName);
                "Requisition Wksh. Name".VALIDATE(Description, CurrWorksheetName);
                IF "Requisition Wksh. Name".INSERT() THEN;


                // CFR le 18/01/2024 => Régie - Utilisation d'un record temporaire
                /*         {
                         gRequisitionLine.RESET();
                         gRequisitionLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
                         gRequisitionLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
                         IF gRequisitionLine.FINDSET(FALSE, FALSE) THEN
                           REPEAT
                             gRequisitionLine.DELETE;
                           UNTIL gRequisitionLine.NEXT() = 0;
                         }*/

                Item.SETFILTER("Process Blocked", '%1|%2', Item."Process Blocked"::" ", Item."Process Blocked"::Ventes);


                // MCO Le 18-01-2016 => Gestion du dernier numéro traité
                CLEAR(LUser);
                LUser.SETRANGE("User Name", USERID);
                IF LUser.FINDFIRST() THEN
                    //TODOLUser."Dernière Ref. Appro" := '';
                    LUser.MODIFY();
                // FIN MCO Le 18-01-2016 => Gestion du dernier numéro traité

                // On stock le filtre fournisseur car il doit s'appliquer sur item vendor et non article
                gFiltreFournisseurArticle := Item.GETFILTER("Vendor No.");
                Item.SETRANGE("Vendor No.");


                DateDeBase := CALCDATE('<-1M>', DateDeBase);
                //ERROR('%1',DateDeBase);


                NbArticle := Item.COUNT;
                NoArticle := 0;

                CLEAR(Window2);
                Window2.OPEN(Text001);
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group("Paramètres Achat SYMTA")
                {
                    Caption = 'Paramètres Achat SYMTA';
                    field(Curr_WorksheetName; CurrWorksheetName)
                    {
                        Caption = 'Feuille réappro';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Feuille réappro field.';

                        trigger OnLookup(var Text: Text): Boolean;
                        var
                            ReqWkshName: Record "Requisition Wksh. Name";
                        begin
                            ReqWkshName.FILTERGROUP(2);
                            ReqWkshName.SETRANGE("Worksheet Template Name", CurrTemplateName);
                            ReqWkshName.FILTERGROUP(0);

                            IF PAGE.RUNMODAL(0, ReqWkshName) = ACTION::LookupOK THEN
                                CurrWorksheetName := ReqWkshName.Name;
                        end;
                    }
                    field(Date_DeBase; DateDeBase)
                    {
                        Caption = 'Date de base';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date de base field.';
                    }
                    field(Date_LimiteAttendu; DateLimiteAttendu)
                    {
                        Caption = 'Date Limite Attendue';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date Limite Attendue field.';
                    }
                    field(PrendreQteDepart; PrendreQteDepart)
                    {
                        Caption = 'Prendre les quantités (départ)';
                        ToolTip = 'Si le paramètre [Date limite attendue] est renseigné et que [Prendre les quantités] est coché, alors la quantité attendue pour l''article prend en compte toutes les [Quantités en départ] présent sur les lignes de commandes d''achat dont la ([Date de réception confirmée] si elle est renseigné [Date de réception demandée] sinon) est supérieure ou égale au paramètre [Date limite attendue] et inférieure au 31/12/3999 (!).';
                        ApplicationArea = All;
                    }
                    field(Date_LimiteReserve; DateLimiteReserve)
                    {
                        Caption = 'Date Limite Réservés';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date Limite Réservés field.';
                    }
                    field(dat_dercdecli; datdercdecli)
                    {
                        Caption = 'Date dern. cde client';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date dern. cde client field.';
                    }
                    field(Type_Comparasion; TypeComparasion)
                    {
                        Caption = 'Type Comparaison';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Type Comparaison field.';
                    }
                    field(Type_Projection; TypeProjection)
                    {
                        Caption = 'Type Projection';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Type Projection field.';
                    }
                    field("NbreAnnée"; NbreAnnée)
                    {
                        Caption = 'Nombre d''année';
                        ToolTip = 'De 0 à 4 (0 = Plus grande conso sur 4 ans)';
                        ApplicationArea = All;
                    }
                    field("Pondération_Projection"; PondérationProjection)
                    {
                        Caption = 'Pondération';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Pondération field.';
                    }
                    field(Type_Commande; TypeCommande)
                    {
                        Caption = 'Type Commande';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Type Commande field.';
                    }
                    field(nb_milli_sec; nb_milli_sec)
                    {
                        Caption = 'Démarrage dans x minutes';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Démarrage dans x minutes field.';
                    }
                    field("Tab_MoisSélectionnés1"; TabMoisSélectionnés[1])
                    {
                        Caption = 'Janvier';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Janvier field.';
                    }
                    field("Tab_MoisSélectionnés2"; TabMoisSélectionnés[2])
                    {
                        Caption = 'Février';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Février field.';
                    }
                    field("Tab_MoisSélectionnés3"; TabMoisSélectionnés[3])
                    {
                        Caption = 'Mars';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Mars field.';
                    }
                    field("Tab_MoisSélectionnés4"; TabMoisSélectionnés[4])
                    {
                        Caption = 'Avril';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Avril field.';
                    }
                    field("Tab_MoisSélectionnés5"; TabMoisSélectionnés[5])
                    {
                        Caption = 'Mai';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Mai field.';
                    }
                    field("TabMoisSélectionnés6"; TabMoisSélectionnés[6])
                    {
                        Caption = 'Juin';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Juin field.';
                    }
                    field("TabMoisSélectionnés7"; TabMoisSélectionnés[7])
                    {
                        Caption = 'Juillet';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Juillet field.';
                    }
                    field("TabMoisSélectionnés8"; TabMoisSélectionnés[8])
                    {
                        Caption = 'Aout';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Aout field.';
                    }
                    field("TabMoisSélectionnés9"; TabMoisSélectionnés[9])
                    {
                        Caption = 'Septembre';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Septembre field.';
                    }
                    field("TabMoisSélectionnés10"; TabMoisSélectionnés[10])
                    {
                        Caption = 'Octobre';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Octobre field.';
                    }
                    field("TabMoisSélectionnés112"; TabMoisSélectionnés[11])
                    {
                        Caption = 'Novembre';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Novembre field.';
                    }
                    field("TabMoisSélectionnés12"; TabMoisSélectionnés[12])
                    {
                        Caption = 'Décembre';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Décembre field.';
                    }
                    field(FiltreFournisseurAExclureTarif; gFiltreFournisseurAExclureTarif)
                    {
                        Caption = 'Fournisseurs à exclure';
                        ToolTip = 'Liste des fournisseurs à exclure de la liste des prix d''achat';
                        ApplicationArea = All;
                    }
                    field(TriEcartMarge; gTriEcartMarge)
                    {
                        Caption = '% écart marge';
                        ToolTip = '% écart marge pour sélection 2ème meilleur fournisseur';
                        ApplicationArea = All;
                    }
                }
            }
        }


        trigger OnOpenPage();
        begin
            IF CurrTemplateName = '' THEN
                CurrTemplateName := 'PROP';
        end;
    }

    trigger OnInitReport();
    begin

        // Initialisation des variables
        DateDeBase := WORKDATE();
        NbreAnnée := 4;
        PondérationProjection := 1;
    end;

    trigger OnPostReport();
    begin
        // CFR le 01/12/2022 => Régie : Nouvelle Gestion du dernier numéro traité (suite)
        ClearPosition();

        MESSAGE('Traitement terminé');
    end;

    trigger OnPreReport();
    begin
        /*
        IF CurrWorksheetName <> '' THEN
          IF NOT CONFIRM(STRSUBSTNO('Attention êtes,vous certain d''utiliser la feuille %1', CurrWorksheetName)) THEN ERROR('Arret');
        */

    end;

    var
        gRequisitionLine: Record "Requisition Line";
        TempGRequisitionLine: Record "Requisition Line" TEMPORARY;
        lm_fonction: Codeunit lm_fonction;
        CUCalculReappro: Codeunit CalculConso;
        CurrTemplateName: Code[10];
        CurrWorksheetName: Code[10];

        LineNo: Integer;
        DateDeBase: Date;
        DateLimiteAttendu: Date;
        PrendreQteDepart: Boolean;
        DateLimiteReserve: Date;
        TypeComparasion: Option Disponible,"A terme",Rien;
        TypeProjection: Option Mensuelle,"Mensuelle + Encours";
        "TabMoisSélectionnés": array[12] of Boolean;
        "NbreAnnée": Integer;
        "PondérationProjection": Decimal;
        "---": Integer;
        gFiltreFournisseurArticle: Text;
        i: Integer;

        TypeCommande: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        "----": Integer;
        Window2: Dialog;
        NoArticle: Integer;
        NbArticle: Integer;
        gFiltreFournisseurAExclureTarif: Text[250];
        gTriEcartMarge: Decimal;

        nb_milli_sec: Integer;

        datdercdecli: Date;
        Text50000: Label 'Vous devez spécifier un Filtre Magasin !';
        Text001: Label 'Article  :  #1########## \ @2@@@@@@@@@@@@@@';

    procedure SetTemplAndWorksheet(TemplateName: Code[10]; WorksheetName: Code[10]);
    begin
        CurrTemplateName := TemplateName;
        CurrWorksheetName := WorksheetName;
    end;

    local procedure ClearPosition();
    var
        lRequisitionWkshName: Record "Requisition Wksh. Name";
    begin
        // CFR le 01/12/2022 => Régie : Nouvelle Gestion du dernier numéro traité (suite)
        CLEAR(lRequisitionWkshName);
        lRequisitionWkshName.SETRANGE("Worksheet Template Name", CurrTemplateName);
        lRequisitionWkshName.SETRANGE(Name, CurrWorksheetName);
        IF lRequisitionWkshName.FINDFIRST() THEN BEGIN
            lRequisitionWkshName."Last Item No." := '';
            lRequisitionWkshName.MODIFY();
        END;
    end;
}

