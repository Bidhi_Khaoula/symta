report 50025 "Statistique client"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Statistique client.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Value Entry"; "Value Entry")
        {
            DataItemTableView = SORTING("Salespers./Purch. Code", "Territory Code", "Source No.")
                                ORDER(Ascending)
                                WHERE("Item Ledger Entry Type" = CONST(Sale),
                                      "Invoiced Quantity" = FILTER(<> 0));
            RequestFilterFields = "Salespers./Purch. Code", "Territory Code", "Posting Date";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(SalespersonCode; "Salespers./Purch. Code")
            {
            }
            column(SalespersonName; Salesperson.Name)
            {
            }
            column(Edit_filter; Edit_filter)
            {
            }
            column(PeriodMois; PeriodMois)
            {
            }
            column(PeriodAN; PeriodAN)
            {
            }
            column(PeriodANM1; PeriodANM1)
            {
            }
            column(TerritoryCode; "Territory Code")
            {
            }
            column(SourceNo; "Source No.")
            {
            }
            column(CustomerName; Customer.Name)
            {
            }
            column(MontantMois; MontantMois)
            {
            }
            column(MargeMois; MargeMois)
            {
            }
            column(MontantAnnee; MontantAnnee)
            {
            }
            column(MargeAnnee; MargeAnnee)
            {
            }
            column(MontantAnneePrec; MontantAnneePrec)
            {
            }
            column(MargeAnneePrec; MargeAnneePrec)
            {
            }

            trigger OnAfterGetRecord();
            begin

                MontantMois := 0;
                MontantAnnee := 0;
                MontantAnneePrec := 0;
                MargeMois := 0;
                MargeAnnee := 0;
                MargeAnneePrec := 0;

                IF "Value Entry"."Source No." = '' THEN
                    Customer.INIT()
                ELSE
                    IF NOT Customer.GET("Value Entry"."Source No.") THEN ERROR('Client : %1', "Value Entry"."Source No.");

                IF "Value Entry"."Salespers./Purch. Code" = '' THEN
                    Salesperson.INIT()
                ELSE
                    IF Salesperson.GET("Value Entry"."Salespers./Purch. Code") THEN;

                IF (PreviousSalesPerson <> "Value Entry"."Salespers./Purch. Code") THEN
                    CurrReport.NEWPAGE();

                PreviousSalesPerson := "Value Entry"."Salespers./Purch. Code";


                //AD Le 08-11-2005 => utilisation de la période saisie en date_compta
                Mois := DATE2DMY(Date_fin_saisie, 2);
                Annee := DATE2DMY(Date_fin_saisie, 3);
                Date1 := DMY2DATE(1, Mois, Annee);
                Date2 := CALCDATE('<+1M>', Date1);
                Date2 := CALCDATE('<-1J>', Date2);
                //FIN AD Le 08-11-2005

                PeriodMois := FORMAT(Date1) + '..' + FORMAT(Date2);

                IF ("Value Entry"."Posting Date" >= Date1) AND ("Value Entry"."Posting Date" <= Date2) THEN BEGIN
                    MontantMois := "Value Entry"."Sales Amount (Actual)";
                    MargeMois := ("Value Entry"."Sales Amount (Actual)" + "Value Entry"."Cost Amount (Actual)");
                END;

                //AD Le 08-11-2005 => utilisation de la période saisie en date_compta
                Date1 := Date_deb_saisie;
                Date2 := Date_fin_saisie;
                //FIN AD Le 08-11-2005

                PeriodAN := FORMAT(Date1) + '..' + FORMAT(Date2);

                IF ("Value Entry"."Posting Date" >= Date1) AND ("Value Entry"."Posting Date" <= Date2) THEN BEGIN
                    MontantAnnee := "Value Entry"."Sales Amount (Actual)";
                    MargeAnnee := ("Value Entry"."Sales Amount (Actual)" + "Value Entry"."Cost Amount (Actual)");
                END;

                //AD Le 08-11-2005 => utilisation de la période saisie en date_compta
                Date1 := CALCDATE('<-1A>', Date_deb_saisie);
                Date2 := CALCDATE('<-1A>', Date_fin_saisie);
                PeriodANM1 := FORMAT(Date1) + '..' + FORMAT(Date2);
                //FIN AD Le 08-11-2005


                IF ("Value Entry"."Posting Date" >= Date1) AND ("Value Entry"."Posting Date" <= Date2) THEN BEGIN
                    MontantAnneePrec := "Value Entry"."Sales Amount (Actual)";
                    MargeAnneePrec := ("Value Entry"."Sales Amount (Actual)" + "Value Entry"."Cost Amount (Actual)");
                END;

                IF masqMarge THEN BEGIN
                    MargeAnnee := 0;
                    MargeAnneePrec := 0;
                    MargeMois := 0;
                END;
            end;

            trigger OnPreDataItem();
            begin

                "Value Entry".SETRANGE("Value Entry"."Item Ledger Entry Type", "Value Entry"."Item Ledger Entry Type"::Sale);

                IF "Value Entry".GETFILTER("Posting Date") = '' THEN
                    ERROR('Date compta obligatoire dans le filtre');
                Date_deb_saisie := "Value Entry".GETRANGEMIN("Posting Date");
                Date_fin_saisie := "Value Entry".GETRANGEMAX("Posting Date");

                // AD Le 21-08-2006 => Pour prendre les écritures également de N-1
                "Value Entry".SETRANGE("Posting Date", CALCDATE('<-1A>', Date_deb_saisie), Date_fin_saisie);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(masqMargeName; masqMarge)
                {
                    Caption = 'Masquage des champs';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Masquage des champs field.';
                }
            }
        }

    }

    trigger OnPreReport();
    begin
        Evaluate(Edit_filter, "Value Entry".GETFILTERS);
    end;

    var
        Salesperson: Record "Salesperson/Purchaser";
        Customer: Record Customer;
        // Commission: Decimal;
        PreviousSalesPerson: Code[20];
        MontantMois: Decimal;
        MontantAnnee: Decimal;
        MontantAnneePrec: Decimal;
        MargeAnnee: Decimal;
        MargeMois: Decimal;
        MargeAnneePrec: Decimal;
        // CoefAnnee: Decimal;
        Mois: Integer;
        Annee: Integer;
        Date1: Date;
        Date2: Date;
        masqMarge: Boolean;
        PeriodMois: Text[30];
        PeriodAN: Text[30];
        PeriodANM1: Text[30];
        Edit_filter: Text[200];
        Date_deb_saisie: Date;
        Date_fin_saisie: Date;
}

