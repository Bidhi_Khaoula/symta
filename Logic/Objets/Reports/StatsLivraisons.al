report 50035 "Stats Livraisons"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Stats Livraisons.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Stats Livraisons';
    dataset
    {
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING(Preparateur, "Posting Date");
            RequestFilterFields = Preparateur, "Posting Date";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(Preparateur; Preparateur)
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(SelltoCustomerNo; "Sell-to Customer No.")
            {
            }
            column(No; "No.")
            {
            }
            column(PreparationNo; "Preparation No.")
            {
            }
            column(BilltoName; "Bill-to Name")
            {
            }
            column(BilltoPostCode; "Bill-to Post Code")
            {
            }
            column(Mt; Mt)
            {
            }
            column(MtPort; MtPort)
            {
            }
            column(Weight; Weight)
            {
            }
            column(NbColis; NbColis)
            {
            }
            column(NbLigne; NbLigne)
            {
            }

            trigger OnAfterGetRecord();
            var
                LSalesShipLine: Record "Sales Shipment Line";
            begin

                MtPort := "Sales Shipment Header".GetPortAmount();
                Mt := "Sales Shipment Header".GetAmount() - MtPort;


                NbColis := "Sales Shipment Header"."Nb Of Box";

                LSalesShipLine.RESET();
                LSalesShipLine.SETRANGE("Document No.", "Sales Shipment Header"."No.");
                LSalesShipLine.SETFILTER(Quantity, '>%1', 0);
                NbLigne := LSalesShipLine.COUNT;
            end;

            trigger OnPreDataItem();
            begin

                LastFieldNo := FIELDNO(Preparateur);

                // CurrReport.CREATETOTALS(Mt, MtPort, NbLigne, NbColis);
            end;
        }
    }

    trigger OnInitReport();
    begin


        IF (NOT (UPPERCASE(USERID) IN ['SYMTA\DOMINIQUE.P', 'SYMTA\VINCENT.D', 'SYMTA\LAETITIA', 'SYMTA\ALINE', 'SYMTA\PHILIPPE', 'SYMTA\DOMINIQUE.B', 'SYMTA\GWENDOLINE', 'SYMTA\JESSY', 'SYMTA\MURIEL.P'])) THEN
            CurrReport.QUIT();
    end;

    var
        LastFieldNo: Integer;
       // FooterPrinted: Boolean;
        NbLigne: Decimal;
        Mt: Decimal;
        MtPort: Decimal;
        NbColis: Decimal;
}

