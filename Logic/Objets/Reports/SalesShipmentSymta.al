report 50008 "Sales - Shipment - Symta"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Sales - Shipment - Symta.rdlc';

    Caption = 'Sales - Shipment';
    PreviewMode = PrintLayout;
    ApplicationArea = all;
    dataset
    {
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed", "Posting Date";
            RequestFilterHeading = 'Posted Sales Shipment';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }
            column(PageCaption; STRSUBSTNO(ESK001Lbl, "Sales Shipment Header"."No.", FORMAT("Sales Shipment Header"."Document Date", 0, 4)))
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(AllowInvDiscCaption; AllowInvDiscCaptionLbl)
            {
            }
            column(PageNo; PageCaptionCapLbl)
            {
            }
            column(InfoIncoterm; gInfoIncoterm)
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(CompanyInfoPicture; CompanyInfo.Picture)
                    {
                    }
                    column(CompanyInfoPictureAddress; CompanyInfo."Picture Address")
                    {
                    }
                    column(SalesShptCopyText; STRSUBSTNO(Text002Lbl, CopyText))
                    {
                    }
                    column(ShipToAddr1; ShipToAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(ShipToAddr2; ShipToAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(ShipToAddr3; ShipToAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(ShipToAddr4; ShipToAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(ShipToAddr5; ShipToAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(ShipToAddr6; ShipToAddr[6])
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoVATRegtnNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(SelltoCustNo_SalesShptHeader; "Sales Shipment Header"."Sell-to Customer No.")
                    {
                    }
                    column(DocDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Document Date", 0, 4))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourRef_SalesShptHeader; "Sales Shipment Header"."Your Reference")
                    {
                    }
                    column(ShipToAddr7; ShipToAddr[7])
                    {
                    }
                    column(ShipToAddr8; ShipToAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(ShptDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Shipment Date"))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
                    {
                    }
                    column(PhoneNoCaption; PhoneNoCaptionLbl)
                    {
                    }
                    column(FaxNoCaption; FaxNoCaptionLbl)
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(GiroNoCaption; GiroNoCaptionLbl)
                    {
                    }
                    column(BankNameCaption; BankNameCaptionLbl)
                    {
                    }
                    column(BankAccNoCaption; BankAccNoCaptionLbl)
                    {
                    }
                    column(ShipmentNoCaption; ShipmentNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(HomePageCaption; HomePageCaptionLbl)
                    {
                    }
                    column(EmailCaption; EmailCaptionLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(SelltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(NoDocExterne; "Sales Shipment Header"."External Document No.")
                    {
                    }
                    column(NumCommande; "Sales Shipment Header"."Order No.")
                    {
                    }
                    column(ShippingAgentName; ShippingAgent.Name)
                    {
                    }
                    column(ShippingAgentCaption; ShippingAgentText)
                    {
                    }
                    column(BilltoCustomerNo; "Sales Shipment Header"."Bill-to Customer No.")
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(SelltoCustomerNo; "Sales Shipment Header"."Sell-to Customer No.")
                    {
                    }
                    column(SellToAddr1; SellToAddr[1])
                    {
                    }
                    column(SellToAddr2; SellToAddr[2])
                    {
                    }
                    column(SellToAddr3; SellToAddr[3])
                    {
                    }
                    column(SellToAddr4; SellToAddr[4])
                    {
                    }
                    column(SellToAddr5; SellToAddr[5])
                    {
                    }
                    column(SellToAddr6; SellToAddr[6])
                    {
                    }
                    column(SellToAddr7; SellToAddr[7])
                    {
                    }
                    column(SellToAddr8; SellToAddr[8])
                    {
                    }
                    column(NbPalettes; 0)
                    {
                    }
                    column(InstructionsDelivery_SalesHeader; "Sales Shipment Header"."Instructions of Delivery")
                    {
                    }
                    column(PricesInclVAT_SalesHeader; "Sales Shipment Header"."Prices Including VAT")
                    {
                    }
                    column(PricesInclVATYesNo_SalesHeader; FORMAT("Sales Shipment Header"."Prices Including VAT"))
                    {
                    }
                    column(PricesInclVAT_SalesHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column(AfficherTexteCentrale; "Sales Shipment Header"."Source Document Type")
                    {
                    }
                    column(ChiffrageBL; "Sales Shipment Header"."Chiffrage BL")
                    {
                    }
                    column(Preparateur; _Preparateur)
                    {
                    }
                    column(Correspondant; Correspondant)
                    {
                    }
                    column(ModeExpedition; FORMAT("Sales Shipment Header"."Mode d'expédition"))
                    {
                    }
                    column(ShipmentMethod; ShipmentMethod.Description)
                    {
                    }
                    column(NbOfBox; "Sales Shipment Header"."Nb Of Box")
                    {
                    }
                    column(Weight; "Sales Shipment Header".Weight)
                    {
                    }
                    column(TypeDocument; _TypeDocument)
                    {
                    }
                    column(Date_commande; Date_commande)
                    {
                    }
                    column(txtreliquat; txtreliquat)
                    {
                    }
                    column(NoSerieSpecialCaption; NoSerieSpecialCaptionLbl)
                    {
                    }
                    dataitem(DimensionLoop1; Integer)
                    {
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(HeaderDimensionsCaption; HeaderDimensionsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        var
                            ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                        begin
                            IF Number = 1 THEN BEGIN
                                IF NOT DimSetEntry1.FINDSET() THEN
                                    CurrReport.BREAK();
                            END ELSE
                                IF NOT Continue THEN
                                    CurrReport.BREAK();

                            CLEAR(DimText);
                            Continue := FALSE;
                            REPEAT
                                OldDimText := DimText;
                                IF DimText = '' THEN
                                    DimText := STRSUBSTNO('%1 - %2', DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                ELSE
                                    DimText :=
                                      STRSUBSTNO(
                                        ParamTxt, DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                    DimText := OldDimText;
                                    Continue := TRUE;
                                    EXIT;
                                END;
                            UNTIL DimSetEntry1.NEXT() = 0;
                        end;

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowInternalInfo THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem("Sales Shipment Line"; "Sales Shipment Line")
                    {
                        DataItemLink = "Document No." = FIELD("No."),
                                       "Gerer par groupement" = FIELD("Regroupement Centrale");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Document No.", "Line No.")
                                            WHERE(Quantity = FILTER(<> 0));
                        column(NoLigne_SalesShptLine; _NoLigne)
                        {
                        }
                        column(Description_SalesShptLine; Description)
                        {
                        }
                        column(Description2_SalesShptLine; "Description 2")
                        {
                        }
                        column(ShowInternalInfo; ShowInternalInfo)
                        {
                        }
                        column(ShowCorrectionLines; ShowCorrectionLines)
                        {
                        }
                        column(Type_SalesShptLine; FORMAT(Type, 0, 2))
                        {
                        }
                        column(AsmHeaderExists; AsmHeaderExists)
                        {
                        }
                        column(DocumentNo_SalesShptLine; "Document No.")
                        {
                        }
                        column(LinNo; LinNo)
                        {
                        }
                        column(Qty_SalesShptLine; Quantity)
                        {
                        }
                        column(QteCdee_SalesShptLine; QteCdée)
                        {
                        }
                        column(RefActive_SalesShptLine; RefActive)
                        {
                        }
                        column(QteRAL_SalesShptLine; _QteRAL)
                        {
                        }
                        column(MtLigne; _MtLigne)
                        {
                        }
                        column(TotalHT_SalesShptLine; _TotalHt)
                        {
                        }
                        column(UnitePrice_SalesShptLine; "Sales Shipment Line"."Unit Price")
                        {
                        }
                        column(Remise_SalesShptLine; _Remise)
                        {
                        }
                        column(NetUnitePrice_SalesShptLine; "Sales Shipment Line"."Net Unit Price")
                        {
                        }
                        column(UOM_SalesShptLine; "Unit of Measure")
                        {
                        }
                        column(No_SalesShptLine; ItemNo)
                        {
                        }
                        column(LineNo_SalesShptLine; "Line No.")
                        {
                        }
                        column(ReferenceSaisie_SalesShptLine; ReferenceSaisie)
                        {
                        }
                        column(Description_SalesShptLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(Description2_SalesShptLineCaption; FIELDCAPTION("Description 2"))
                        {
                        }
                        column(Qty_SalesShptLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(UOM_SalesShptLineCaption; FIELDCAPTION("Unit of Measure"))
                        {
                        }
                        column(No_SalesShptLineCaption; NoSalesShptLineCaptionLbl)
                        {
                        }
                        column(CodeSubstitution_SalesShptLine; CodeSubstitution)
                        {
                        }
                        column(LineNo_SalesShptLineCaption; LineNoSalesShptLineCaptionLbl)
                        {
                        }
                        dataitem(SalesCommentLine; "Sales Comment Line")
                        {
                            DataItemLink = "No." = FIELD("Document No."),
                                           "Document Line No." = FIELD("Line No.");
                            DataItemLinkReference = "Sales Shipment Line";
                            DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                                WHERE("Document Type" = CONST(Shipment),
                                                      Comment = FILTER(<> ''),
                                                      "Print Shipment" = CONST(true));
                            column(CommentLine_SalesCommentLine; Comment)
                            {
                            }
                        }
                        dataitem(DimensionLoop2; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = FILTER(1 ..));
                            column(DimText1; DimText)
                            {
                            }
                            column(LineDimensionsCaption; LineDimensionsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord();
                            var
                                ParamTxt: Label '%1, %2 %3', Comment = '%1 ; %2 ;%3';
                            begin
                                IF Number = 1 THEN BEGIN
                                    IF NOT DimSetEntry2.FINDSET() THEN
                                        CurrReport.BREAK();
                                END ELSE
                                    IF NOT Continue THEN
                                        CurrReport.BREAK();

                                CLEAR(DimText);
                                Continue := FALSE;
                                REPEAT
                                    OldDimText := DimText;
                                    IF DimText = '' THEN
                                        DimText := STRSUBSTNO('%1 - %2', DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    ELSE
                                        DimText :=
                                          STRSUBSTNO(
                                           ParamTxt, DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                        DimText := OldDimText;
                                        Continue := TRUE;
                                        EXIT;
                                    END;
                                UNTIL DimSetEntry2.NEXT() = 0;
                            end;

                            trigger OnPreDataItem();
                            begin
                                IF NOT ShowInternalInfo THEN
                                    CurrReport.BREAK();
                            end;
                        }
                        dataitem(DisplayAsmInfo; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(PostedAsmLineItemNo; BlanksForIndent() + PostedAsmLine."No.")
                            {
                            }
                            column(PostedAsmLineDescription; BlanksForIndent() + PostedAsmLine.Description)
                            {
                            }
                            column(PostedAsmLineQuantity; PostedAsmLine.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(PostedAsmLineUOMCode; GetUnitOfMeasureDescr(PostedAsmLine."Unit of Measure Code"))
                            {
                                //DecimalPlaces = 0:5;
                            }

                            trigger OnAfterGetRecord();
                            begin
                                IF Number = 1 THEN
                                    PostedAsmLine.FINDSET()
                                ELSE
                                    PostedAsmLine.NEXT();
                            end;

                            trigger OnPreDataItem();
                            begin
                                IF NOT DisplayAssemblyInformation THEN
                                    CurrReport.BREAK();
                                IF NOT AsmHeaderExists THEN
                                    CurrReport.BREAK();

                                PostedAsmLine.SETRANGE("Document No.", PostedAsmHeader."No.");
                                SETRANGE(Number, 1, PostedAsmLine.COUNT);
                            end;
                        }

                        trigger OnAfterGetRecord();
                        var
                            RecItem: Record Item;
                            LItem: Record Item;
                            reliquat: Boolean;
                            taux: Integer;

                        begin
                            LinNo := "Line No.";// / 10000;
                            IF NOT ShowCorrectionLines AND Correction THEN
                                CurrReport.SKIP();

                            IF NOT (((Type = Type::Item) OR (Type = Type::"Fixed Asset") OR ((Type = Type::"G/L Account") AND ShowInternalInfo))) THEN
                                CurrReport.SKIP();

                            // ESKVN1.0 => Gestion de l'impression des commentaires
                            IF "Sales Shipment Line".Type = "Sales Shipment Line".Type::" " THEN
                                IF NOT "Sales Shipment Line"."Print Shipment" THEN
                                    CurrReport.SKIP();
                            // FIN ESKVN1.0


                            IF Quantity = 0 THEN CurrReport.SKIP();  // AD Le 17-12-2014 => Pas le lignes vides

                            DimSetEntry2.SETRANGE("Dimension Set ID", "Dimension Set ID");
                            IF DisplayAssemblyInformation THEN
                                AsmHeaderExists := AsmToShipmentExists(PostedAsmHeader);

                            CLEAR(RecItem);

                            ItemNo := "No.";
                            IF (STRLEN(ItemNo) = 7) THEN
                                ItemNo := Format(COPYSTR(ItemNo, 3, STRLEN(ItemNo) - 2));


                            // recherche de la qte CDE
                            //IF CdeVteLigEnr.GET(CdeVteLigEnr."Document Type"::Order,"Sales Shipment Header"."Order No.",
                            //                    "Sales Shipment Line"."Line No.") THEN
                            LineArchive.RESET();
                            LineArchive.SETRANGE("Document Type", LineArchive."Document Type"::Order);
                            LineArchive.SETRANGE("Document No.", "Sales Shipment Header"."Order No.");
                            LineArchive.SETRANGE("Line No.", "Sales Shipment Line"."Line No.");
                            IF LineArchive.FINDLAST() THEN
                                QteCdée := LineArchive.Quantity
                            ELSE
                                IF CdeVteLig.GET(CdeVteLig."Document Type"::Order, "Sales Shipment Header"."Order No.",
                                               "Sales Shipment Line"."Line No.") THEN
                                    QteCdée := CdeVteLig.Quantity;

                            // Recherche si La cde est une cde reliquat car si oui il peut y avoir des qtes cde
                            // à 0 alors qu'il faut bien imprimer la ligne.
                            //IF CdeVteEnr.GET(CdeVteLigEnr."Document Type"::Order,"Sales Shipment Header"."Order No.") THEN
                            HeaderArchive.RESET();
                            HeaderArchive.SETRANGE("Document Type", HeaderArchive."Document Type"::Order);
                            HeaderArchive.SETRANGE("No.", "Sales Shipment Header"."Order No.");
                            IF HeaderArchive.FINDLAST() THEN
                                reliquat := HeaderArchive."Abandon remainder"
                            ELSE
                                IF CdeVte.GET(CdeVte."Document Type"::Order, "Sales Shipment Header"."Order No.") THEN
                                    reliquat := CdeVte."Abandon remainder";

                            TotalNetWeight += ("Sales Shipment Line".Quantity * "Sales Shipment Line"."Net Weight");

                            _Remise := '';
                            //MC Le 06-07-2010 => Création d'une fonction permettant de ne pas afficher une ligne déjà livrée totalement à la date du bl imprimé
                            IF "Sales Shipment Line".Type = "Sales Shipment Line".Type::Item THEN
                                IF GetLivTot() AND NOT Correction THEN  // ANI le 08-06-2017 ticket 23685 on ne fait pas le test pour les lignes de correction
                                    CurrReport.SKIP();

                            NoLigne += 1;

                            _NoLigne := NoLigne;

                            // AD Le 26-01-2012 => On prend le no de la cde d'origine
                            IF NOT EVALUATE(_NoLigne, FORMAT("Sales Shipment Line"."Line No." / 10000)) THEN
                                _NoLigne := 0;
                            // FIN AD Le 26-01-2012

                            RefActive := GestionMulti.RechercheRefActive("Sales Shipment Line"."No.");

                            //IF Type = Type::Item THEN
                            //  BEGIN
                            //    IF "Sales Shipment Line"."Zone Code" = '3PU' THEN
                            //      _References := PADSTR(GestionMulti.RechercheRefActive("Sales Shipment Line"."No."),20,' ') + ' ' +
                            //                     PADSTR(COPYSTR("Sales Shipment Line".Description, 1, 20),20,' ') + ' ' +
                            //                     "Sales Shipment Line"."No."
                            //    ELSE
                            //      _References := PADSTR("Sales Shipment Line"."No.",10,' ') + ' ' +
                            //                     PADSTR(COPYSTR("Sales Shipment Line".Description, 1, 19),19,' ') + ' ' +
                            //                     GestionMulti.RechercheRefActive("Sales Shipment Line"."No.");
                            //  END
                            //ELSE
                            //  _References := "Sales Shipment Line"."No." + ' ' +
                            //                 COPYSTR("Sales Shipment Line".Description, 1, 20);

                            // MC Le 04-12-2013 => Création de la fonction GetQteDejaLivre() dec_rtn : Decimal pour permettre la gestion du champ [Q. RAL]
                            //_QteRAL := FORMAT(QteCdée - "Sales Shipment Line".Quantity,0,'<sign><Precision,0:2><Integer><Decimals>');
                            _QteRAL := FORMAT(QteCdée - GetQteDejaLivre() - "Sales Shipment Line".Quantity, 0, '<sign><Precision,0:2><Integer><Decimals>');
                            // FIN MC Le 04-12-2013 => Création de la fonction GetQteDejaLivre() dec_rtn : Decimal pour permettre la gestion du champ [Q. RAL]
                            IF "Sales Shipment Header"."Chiffrage BL" THEN BEGIN
                                _PuBrut := FORMAT("Sales Shipment Line"."Unit Price", 0,
                                    '<Precision,2:2><Integer><Decimals>');
                                IF ("Sales Shipment Line"."Discount1 %" <> 0) OR ("Sales Shipment Line"."Discount2 %" <> 0) THEN BEGIN
                                    IF ("Sales Shipment Line"."Discount1 %" <> 0) THEN
                                        _Remise := FORMAT("Sales Shipment Line"."Discount1 %") + '%';

                                    IF ("Sales Shipment Line"."Discount2 %" <> 0) THEN BEGIN
                                        IF ("Sales Shipment Line"."Discount1 %" <> 0) THEN
                                            _Remise += '+';
                                        _Remise += FORMAT("Sales Shipment Line"."Discount2 %") + '%';
                                    END;

                                END;
                                _PuNet := FORMAT("Sales Shipment Line"."Net Unit Price", 0, '<Precision,2:2><Integer><Decimals>');
                                _MtLigne := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                                _TotalHt += _MtLigne;
                            END
                            ELSE BEGIN
                                _PuBrut := '';
                                _PuNet := '';
                                _MtLigne := 0;
                                _TotalHt := 0;
                                _Remise := '';
                            END;


                            IF "Sales Shipment Line"."Originally Ordered No. Pour BL" <> '' THEN BEGIN
                                LItem.GET("Sales Shipment Line"."Originally Ordered No. Pour BL");
                                CodeSubstitution := STRSUBSTNO(ESK010Lbl, LItem."No. 2");
                            END
                            ELSE
                                CodeSubstitution := '';


                            IF (("Sales Shipment Line"."Référence saisie" <> "Sales Shipment Line"."Recherche référence") AND ("Sales Shipment Line"."Référence saisie" <> '')) THEN
                                ReferenceSaisie := STRSUBSTNO(ESK009Lbl, "Sales Shipment Line"."Référence saisie")
                            ELSE
                                ReferenceSaisie := '';



                            TempVATAmountLine.DELETEALL();
                            TempVATAmountLine.INIT();
                            //TempVATAmountLine."VAT Identifier" := "VAT Identifier";
                            TempVATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                            TempVATAmountLine."Tax Group Code" := "Tax Group Code";
                            TempVATAmountLine."VAT %" := "VAT %";
                            TempVATAmountLine."VAT Base" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                            TempVATAmountLine."Amount Including VAT" := TempVATAmountLine."VAT Base" * (1 + TempVATAmountLine."VAT %" / 100);
                            TempVATAmountLine."Line Amount" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                            IF "Allow Invoice Disc." THEN
                                TempVATAmountLine."Inv. Disc. Base Amount" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                            //TempVATAmountLine."Invoice Discount Amount" := "Inv. Discount Amount";
                            TempVATAmountLine.InsertLine();

                            // CurrReport.CREATETOTALS(
                            //   TempVATAmountLine."Line Amount",TempVATAmountLine."Inv. Disc. Base Amount",
                            //   TempVATAmountLine."Invoice Discount Amount",TempVATAmountLine."VAT Base",TempVATAmountLine."VAT Amount");



                            IF ((taux_tva[2] = TempVATAmountLine."VAT Identifier") OR (taux_tva[2] = '')) THEN
                                taux := 2
                            ELSE
                                taux := 3;


                            taux_tva[taux] := TempVATAmountLine."VAT Identifier";
                            ////tva_ht[taux] := TempVATAmountLine."VAT %";
                            total_ht[taux] += TempVATAmountLine."VAT Base";
                            total_tva[taux] += TempVATAmountLine."VAT Amount";
                            total_ht_net[taux] += TempVATAmountLine."Line Amount";
                            total_ttc[taux] += TempVATAmountLine."Amount Including VAT";
                            total_discount[taux] += TempVATAmountLine."Invoice Discount Amount";
                            taux_tva[taux] := TempVATAmountLine."VAT Identifier";

                            total_ht[1] += TempVATAmountLine."VAT Base";
                            total_tva[1] += TempVATAmountLine."VAT Amount";
                            total_ht_net[1] += TempVATAmountLine."Line Amount";
                            total_ttc[1] += TempVATAmountLine."Amount Including VAT";
                            total_discount[1] += TempVATAmountLine."Invoice Discount Amount";
                        end;

                        trigger OnPostDataItem();
                        begin
                            IF ShowLotSN THEN BEGIN
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(TRUE);
                                TrackingSpecCount :=
                                  ItemTrackingDocMgt.RetrieveDocumentItemTracking(TempTrackingSpecBuffer,
                                    "Sales Shipment Header"."No.", DATABASE::"Sales Shipment Header", 0);
                                ItemTrackingDocMgt.SetRetrieveAsmItemTracking(FALSE);
                            END;
                        end;

                        trigger OnPreDataItem();
                        begin


                            // AD Le 15-09-2011
                            IF "Sales Shipment Header"."Zone Code" <> '' THEN
                                SETRANGE("Zone Code", "Sales Shipment Header"."Zone Code");
                            // FIN AD Le 15-09-2011

                            MoreLines := FIND('+');
                            WHILE MoreLines AND (Description = '') AND ("No." = '') AND (Quantity = 0) DO
                                MoreLines := NEXT(-1) <> 0;
                            IF NOT MoreLines THEN
                                CurrReport.BREAK();
                            SETRANGE("Line No.", 0, "Line No.");

                            TempVATAmountLine.DELETEALL();
                            _TotalHt := 0;



                            FOR i := 1 TO 3 DO BEGIN
                                total_ht[i] := 0;
                                total_tva[i] := 0;
                                total_ht_net[i] := 0;
                                total_ttc[i] := 0;
                                total_discount[i] := 0;
                                taux_tva[i] := '';
                            END;
                        end;
                    }
                    dataitem("Sales Comment Line"; "Sales Comment Line")
                    {
                        DataItemLink = "No." = FIELD("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            WHERE("Document Type" = CONST(Shipment),
                                                  "Document Line No." = CONST(0),
                                                  "Print Shipment" = CONST(true));
                        column(Comment_SalesCommentLine; "Sales Comment Line".Comment)
                        {
                        }
                    }
                    dataitem(CommentairesPieds; "Sales Comment Line")
                    {
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            ORDER(Ascending);
                        column(Comment_Pied; Comment)
                        {
                        }

                        trigger OnPreDataItem();
                        begin

                            CommentairesPieds.SETRANGE("Print Shipment", TRUE);
                            CommentairesPieds.SETRANGE(Date, 0D, WORKDATE());
                            CommentairesPieds.SETFILTER("End Date", '>%1', WORKDATE());
                        end;
                    }
                    dataitem(Total; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(TotalHt; total_ht[1])
                        {
                        }
                        column(TotalHt1; total_ht[2])
                        {
                        }
                        column(TotalHt2; total_ht[3])
                        {
                        }
                        column(TotalHtNet; total_ht_net[1])
                        {
                        }
                        column(TotalHtNet1; total_ht_net[2])
                        {
                        }
                        column(TotalHtNet2; total_ht_net[3])
                        {
                        }
                        column(TotalTva; total_tva[1])
                        {
                        }
                        column(TotalTva1; total_tva[2])
                        {
                        }
                        column(TotalTva2; total_tva[3])
                        {
                        }
                        column(TauxTVA_1; taux_tva[2])
                        {
                        }
                        column(TauxTVA_2; taux_tva[3])
                        {
                        }
                        column(TotalTtc; total_ttc[1])
                        {
                        }
                        column(TotalTtc1; total_ttc[2])
                        {
                        }
                        column(TotalTtc2; total_ttc[3])
                        {
                        }
                        column(TotalDiscount; total_discount[1])
                        {
                        }
                        column(TotalDiscount1; total_discount[2])
                        {
                        }
                        column(TotalDiscount2; total_discount[3])
                        {
                        }
                        column(BilltoCustNo_SalesShptHeader; "Sales Shipment Header"."Bill-to Customer No.")
                        {
                        }
                        column(BilltoAddressCaption; BilltoAddressCaptionLbl)
                        {
                        }
                        column(BilltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Bill-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowCustAddr THEN;
                            //  CurrReport.BREAK;
                        end;
                    }
                    dataitem(ItemTrackingLine; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(TrackingSpecBufferNo; TempTrackingSpecBuffer."Item No.")
                        {
                        }
                        column(TrackingSpecBufferDesc; TempTrackingSpecBuffer.Description)
                        {
                        }
                        column(TrackingSpecBufferLotNo; TempTrackingSpecBuffer."Lot No.")
                        {
                        }
                        column(TrackingSpecBufferSerNo; TempTrackingSpecBuffer."Serial No.")
                        {
                        }
                        column(TrackingSpecBufferQty; TempTrackingSpecBuffer."Quantity (Base)")
                        {
                        }
                        column(ShowTotal; ShowTotal)
                        {
                        }
                        column(ShowGroup; ShowGroup)
                        {
                        }
                        column(QuantityCaption; QuantityCaptionLbl)
                        {
                        }
                        column(SerialNoCaption; SerialNoCaptionLbl)
                        {
                        }
                        column(LotNoCaption; LotNoCaptionLbl)
                        {
                        }
                        column(DescriptionCaption; DescriptionCaptionLbl)
                        {
                        }
                        column(NoCaption; NoCaptionLbl)
                        {
                        }
                        dataitem(TotalItemTracking; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = CONST(1));
                            column(Quantity1; TotalQty)
                            {
                            }
                        }

                        trigger OnAfterGetRecord();
                        begin
                            IF Number = 1 THEN
                                TempTrackingSpecBuffer.FINDSET()
                            ELSE
                                TempTrackingSpecBuffer.NEXT();

                            ShowTotal := FALSE;
                            IF ItemTrackingAppendix.IsStartNewGroup(TempTrackingSpecBuffer) THEN
                                ShowTotal := TRUE;

                            ShowGroup := FALSE;
                            IF (TempTrackingSpecBuffer."Source Ref. No." <> OldRefNo) OR
                               (TempTrackingSpecBuffer."Item No." <> OldNo)
                            THEN BEGIN
                                OldRefNo := TempTrackingSpecBuffer."Source Ref. No.";
                                OldNo := TempTrackingSpecBuffer."Item No.";
                                TotalQty := 0;
                            END ELSE
                                ShowGroup := TRUE;
                            TotalQty += TempTrackingSpecBuffer."Quantity (Base)";
                        end;

                        trigger OnPreDataItem();
                        begin
                            IF TrackingSpecCount = 0 THEN
                                CurrReport.BREAK();
                            SETRANGE(Number, 1, TrackingSpecCount);
                            TempTrackingSpecBuffer.SETCURRENTKEY("Source ID", "Source Type", "Source Subtype", "Source Batch Name",
                              "Source Prod. Order Line", "Source Ref. No.");
                        end;
                    }

                    trigger OnPreDataItem();
                    begin
                        // Item Tracking:
                        IF ShowLotSN THEN BEGIN
                            TrackingSpecCount := 0;
                            OldRefNo := 0;
                            ShowGroup := FALSE;
                        END;
                    end;
                }

                trigger OnAfterGetRecord();
                begin



                    IF Number > 1 THEN BEGIN
                        CopyText := Text001Lbl;
                        OutputNo += 1;
                    END;
                    //CurrReport.PAGENO := 1;
                    TotalQty := 0;           // Item Tracking
                    NombreDeLigneImprimer := 0;
                end;

                trigger OnPostDataItem();
                begin
                    IF NOT CurrReport.PREVIEW THEN
                        ShptCountPrinted.RUN("Sales Shipment Header");
                end;

                trigger OnPreDataItem();

                begin
                    NoOfLoops := 1 + ABS(NoOfCopies);

                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                recL_SalesHeaderArchive: Record "Sales Header Archive";
                EditLabel: Codeunit "Etiquettes Articles";
            begin
                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");
                //CLEAR(RecLogo);
                //RecLogo.SETRANGE(Code,"Code Logo Impression");
                //IF RecLogo.FINDFIRST () THEN
                // RecLogo.CALCFIELDS(Logo);
                ReportLineNumber += 1;
                IF ReportLineNumber = 2 THEN BEGIN
                    CLEAR(CompanyInfo);
                    CompanyInfo.GET();
                END;



                IF RespCenter.GET("Responsibility Center") THEN BEGIN
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                END ELSE
                    FormatAddr.Company(CompanyAddr, CompanyInfo);

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF "Salesperson Code" = '' THEN BEGIN
                    SalesPurchPerson.INIT();
                    SalesPersonText := '';
                END ELSE BEGIN
                    SalesPurchPerson.GET("Salesperson Code");
                    SalesPersonText := Text000Lbl;
                END;
                IF "Your Reference" = '' THEN
                    ReferenceText := ''
                ELSE
                    Evaluate(ReferenceText, (FIELDCAPTION("Your Reference")));
                FormatAddr.SalesShptShipTo(ShipToAddr, "Sales Shipment Header");

                FormatAddr.SalesShptBillTo(CustAddr, CustAddr, "Sales Shipment Header");
                ShowCustAddr := "Bill-to Customer No." <> "Sell-to Customer No.";
                FOR i := 1 TO ARRAYLEN(CustAddr) DO
                    IF CustAddr[i] <> ShipToAddr[i] THEN
                        ShowCustAddr := TRUE;

                // adresse de facturation
                FormatAddr.SalesShptSellTo(SellToAddr, "Sales Shipment Header");

                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN
                        SegManagement.LogDocument(
                          5, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');

                // Le transporteur
                IF "Shipping Agent Code" = '' THEN BEGIN
                    ShippingAgent.INIT();
                    ShippingAgentText := '';
                END ELSE BEGIN
                    ShippingAgent.GET("Shipping Agent Code");
                    ShippingAgentText := Text003Lbl;
                END;

                SalesShptLine.RESET();
                SalesShptLine.SETRANGE("Document No.", "No.");

                IF SalesShptLine.FIND('-') THEN
                    REPEAT
                        LineQty := LineQty + SalesShptLine.Quantity;
                        TotalNetWeight := TotalNetWeight + (SalesShptLine.Quantity * SalesShptLine."Net Weight");
                        TotalGrossWeight := TotalGrossWeight + (SalesShptLine.Quantity * SalesShptLine."Gross Weight");
                        TotalVolume := TotalVolume + (SalesShptLine.Quantity * SalesShptLine."Unit Volume");
                        IF SalesShptLine."Units per Parcel" > 0 THEN
                            TotalParcels := TotalParcels + ROUND(SalesShptLine.Quantity / SalesShptLine."Units per Parcel", 1, '>');
                    UNTIL SalesShptLine.NEXT() = 0;


                // AD Le 18-07-2011 => Gestion du type de document
                _TypeDocument := '';
                IF "Sales Shipment Header"."Source Document Type" <> '' THEN BEGIN
                    ParamGeneraux.GET('VTE-DOC-CDE', "Sales Shipment Header"."Source Document Type");
                    _TypeDocument := ParamGeneraux.LongDescription;
                END;
                // FIN AD Le 18-07-2011


                //Conditions d'expédition
                IF "Shipment Method Code" = '' THEN
                    ShipmentMethod.INIT()
                ELSE BEGIN
                    ShipmentMethod.GET("Shipment Method Code");
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Sales Shipment Header"."Language Code");
                END;


                // AD Le 29-11-2007 => On edite les étiquettes
                EditLabel.EditLabelShipment("Sales Shipment Header", 0);

                //LM le 28-11-2012 impression texte reliquat
                txtreliquat := '';

                rec_sales_header.RESET();
                rec_sales_header.SETRANGE(rec_sales_header."No.", "Sales Shipment Header"."Order No.");
                IF rec_sales_header.FINDFIRST() THEN BEGIN
                    IF rec_sales_header."Shipment Status" = "Shipment Status"::Reliquat THEN
                        txtreliquat := 'RELIQUAT';
                    // MCO Le 31-03-2015 => Régie
                    Date_commande := rec_sales_header."Order Date";
                END
                ELSE BEGIN
                    recL_SalesHeaderArchive.RESET();
                    recL_SalesHeaderArchive.SETRANGE(recL_SalesHeaderArchive."Document Type", recL_SalesHeaderArchive."Document Type"::Order);
                    recL_SalesHeaderArchive.SETRANGE("No.", "Sales Shipment Header"."Order No.");
                    IF recL_SalesHeaderArchive.FINDLAST() THEN
                        Date_commande := recL_SalesHeaderArchive."Order Date";
                END;
                // FIN MCO Le 31-03-2015 => Régie



                //GESTION DU CORRESPONDANT (paramètres utilisateurs)
                Correspondant := ReportUtils.GetCorrespondantName("Sales Shipment Header"."Order Create User", '');
                _Preparateur := "Sales Shipment Header".Preparateur;
                IF ParamGeneraux.GET('MAG_EMPLOYES', "Sales Shipment Header".Preparateur) THEN
                    _Preparateur := ParamGeneraux.LongDescription;

                // CFR le 06/04/2023 - Régie : Incoterm ICC 2020 Ventes
                gInfoIncoterm := GestionEdition.GetSalesInfoIncotermICC2020("Sales Shipment Header"."Incoterm Code", "Sales Shipment Header"."Incoterm City");
                // FIN CFR le 06/04/2023
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOf_Copies; NoOfCopies)
                    {
                        Caption = 'No. of Copies';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. of Copies field.';
                    }
                    field(Show_InternalInfo; ShowInternalInfo)
                    {
                        Caption = 'Show Internal Information';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Internal Information field.';
                    }
                    field(Log_Interaction; LogInteraction)
                    {
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Log Interaction field.';
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        Caption = 'Show Correction Lines';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Correction Lines field.';
                    }
                    field(Show_LotSN; ShowLotSN)
                    {
                        Caption = 'Show Serial/Lot Number Appendix';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Serial/Lot Number Appendix field.';
                    }
                    field(Display_AsmInfo; DisplayAssemblyInformation)
                    {
                        Caption = 'Show Assembly Components';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Assembly Components field.';
                    }
                    field("Imprimer Logo société"; _ImprimerLogo)
                    {
                        Caption = 'Imprimer Logo société';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Imprimer Logo société field.';
                    }
                }
            }
        }
        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            IsRequestPage := TRUE; // MCO Le 09-12-2015 => Permet de passer outre le save values en envoit par mail
            _ImprimerLogo := FALSE; // AD Le 07-01-2016 => Demande de Bernard -> Ticket 7106
            ShowInternalInfo := TRUE; // MCO Le 21-11-2017 => Demande de Marie Laure et Muriel

            InitLogInteraction();
            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
        NoCommande = 'N° de commande'; VotreRef = 'Votre Réfèrence'; NoClient = 'N° client'; SellToAddrCaption = 'Adresse du donneur d''ordre'; BillToAddrCaption = 'Adresse de facturation'; NbColisCaption = 'Nb Unité manut.'; PoidsBrutCaption = 'Poids brut'; NbPaletteCaption = 'Nb palettes'; CorrespondantCaption = 'Correspondant'; PreparateurCaption = 'Préparateur';
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET();
        SalesSetup.GET();

        ReportLineNumber := 0;
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
        AsmHeaderExists := FALSE;

        //IF NOT IsRequestPage THEN   _ImprimerLogo := TRUE; // MCO Le 09-12-2015 => Envoi par mail

        IF EskapeCommunication.GetImpressionPdf() THEN
            _ImprimerLogo := TRUE;

        CLEAR(CompanyInfo);
        IF _ImprimerLogo THEN CompanyInfo.CALCFIELDS(Picture);
        CompanyInfo.CALCFIELDS("Picture Address");
        GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
    end;

    var

        SalesPurchPerson: Record "Salesperson/Purchaser";
        ShippingAgent: Record "Shipping Agent";
        // SalesShipmentLine: Record "Sales Shipment Line" temporary;
        CompanyInfo: Record "Company Information";

        SalesSetup: Record "Sales & Receivables Setup";
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        TempTrackingSpecBuffer: Record "Tracking Specification" temporary;
        PostedAsmHeader: Record "Posted Assembly Header";
        PostedAsmLine: Record "Posted Assembly Line";
        SalesShptLine: Record "Sales Shipment Line";
        rec_sales_header: Record "Sales Header";
        RespCenter: Record "Responsibility Center";
        ParamGeneraux: Record "Generals Parameters";
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        HeaderArchive: Record "Sales Header Archive";
        CdeVteLig: Record "Sales Line";
        //    GLSetup: Record "General Ledger Setup";
        ShipmentMethod: Record "Shipment Method";
        CdeVte: Record "Sales Header";
        // SalesHeader: Record "Sales Header";
        LineArchive: Record "Sales Line Archive";
        //  CurrExchRate: Record "Currency Exchange Rate";
        ItemTrackingAppendix: Report "Item Tracking Appendix";
        Language_G: Codeunit Language;
        ReportUtils: Codeunit ReportFunctions;
        GestionEdition: Codeunit "Gestion Info Editions";
        ShptCountPrinted: Codeunit "Sales Shpt.-Printed";
        SegManagement: Codeunit SegManagement;
        // ItemTrackingMgt: Codeunit "Item Tracking Management";
        FormatAddr: Codeunit "Format Address";
        GestionMulti: Codeunit "Gestion Multi-référence";
        ItemTrackingDocMgt: Codeunit "Item Tracking Doc. Management";
        EskapeCommunication: Codeunit "Eskape Communication";
        CustAddr: array[8] of Text[50];
        SellToAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text[80];
        ShippingAgentText: Text[80];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        OutputNo: Integer;
        NoOfLoops: Integer;
        TrackingSpecCount: Integer;
        OldRefNo: Integer;
        OldNo: Code[20];
        CopyText: Text[30];
        ShowCustAddr: Boolean;
        i: Integer;

        DimText: Text[120];
        OldDimText: Text[120];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        ShowTotal: Boolean;
        ShowGroup: Boolean;
        TotalQty: Decimal;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        LinNo: Integer;
        Text000Lbl: Label 'Salesperson';
        Text001Lbl: Label 'COPY';

        Text003Lbl: Label 'Transporteur';
        Text002Lbl: Label 'Sales - Shipment%1', Comment = '%1 = Ship';
        ItemTrackingAppendixCaptionLbl: Label 'Item Tracking - Appendix';
        PhoneNoCaptionLbl: Label 'Phone No.';
        FaxNoCaptionLbl: Label 'Notre N° fax';
        VATRegNoCaptionLbl: Label 'VAT Reg. No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        ShipmentNoCaptionLbl: Label 'Shipment No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'E-Mail';
        DocumentDateCaptionLbl: Label 'Document Date';
        HeaderDimensionsCaptionLbl: Label 'Header Dimensions';
        LineDimensionsCaptionLbl: Label 'Line Dimensions';
        BilltoAddressCaptionLbl: Label 'Bill-to Address';
        QuantityCaptionLbl: Label 'Quantity';
        SerialNoCaptionLbl: Label 'Serial No.';
        LotNoCaptionLbl: Label 'Lot No.';
        DescriptionCaptionLbl: Label 'Description';
        NoCaptionLbl: Label 'No.';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Pge Nb ; %2 = Total PAge NB';
        //        NombredelignepossibleparPage: Integer;
        NombreDeLigneImprimer: Integer;
        //    NombredelignepossibleparPagePleine: Integer;

        LineQty: Decimal;
        TotalNetWeight: Decimal;
        TotalGrossWeight: Decimal;
        TotalVolume: Decimal;
        TotalParcels: Decimal;
        /*         TotalGrossWeightCaptionLbl: Label 'Poids brut';
                LineQtyCaptionLbl: Label 'Quantité'; */
        NoSalesShptLineCaptionLbl: Label 'Référence';
        LineNoSalesShptLineCaptionLbl: Label 'N°';
        ItemNo: Code[20];
        NoSerieSpecialCaptionLbl: Label 'SERIE';
        // "-- Symta --": Integer;
        _Preparateur: Text[50];
        _NoLigne: Integer;
        /*    _References: Text[70];
           _QteCde: Code[9];
           _QteLiv: Code[9] */
        _QteRAL: Code[9];
        _PuBrut: Text[13];
        _Remise: Text[13];
        _PuNet: Text[13];
        _MtLigne: Decimal;
        _TotalHt: Decimal;
        NoLigne: Integer;


        _TypeDocument: Text[50];


        /*  tab_InfoSymta: array[8] of Text[60];
         CompanyInfoForpicture: Record "Company Information";
         blChiffre: Boolean;
         nb_ligne_imprime: Integer;
         imprimeAdresse: Boolean;
         "RecPréparateur": Record "Warehouse Employee";
         nomPreparateur: Text[30];
         BlReliquat: Text[12];
         client: Record Customer;
         PremiereImpression: Boolean;
         ShowSection: Boolean;
         DesignationLivraison: Record "Shipment Method"; */
        "QteCdée": Decimal;

        // DesignationTotale: Text[101];
        txtreliquat: Text[30];


        Date_commande: Date;
        Correspondant: Text[250];


        AmountCaptionLbl: Label 'Amount';
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';

        /*   Text007: Label 'VAT Amount Specification in ';
          Text008: Label 'Local Currency';
          Text009: Label 'Exchange rate: %1/%2';
          */
        LineAmtCaptionLbl: Label 'Line Amount';
        TotalCaptionLbl: Label 'Total';
        UnitPriceCaptionLbl: Label 'Unit Price';
        AllowInvDiscCaptionLbl: Label 'Allow Invoice Discount';
        /*  InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
         VATIdentifierCaptionLbl: Label 'VAT Identifier';
  */
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        /*    TotalText: Text[50];
           TotalExclVATText: Text[50];
           TotalInclVATText: Text[50];
           SubtotalCaptionLbl: Label 'Subtotal';
           PaymentDiscountVATCaptionLbl: Label 'Payment Discount on VAT'; */

        ESK001Lbl: Label 'No %1 du %2', Comment = '%1 ; %2';
        /*  ESK002: Label '%1 en %2  -  %3';
         ESK003: Label 'Vos Références : %1 %2';
         ESK004: Label 'Enregistrée par : %1';
         ESK005: Label 'Nombre de colis : %1  -  Poids %2';
         ESK006: Label 'Commande No : %1 du %2';
         ESK007: Label 'Préparé par : %1';
         ESK008: Label 'Notre commande : %1'; */
        ESK009Lbl: Label 'Référence commandée : %1', Comment = '%1 = Ref';
        ESK010Lbl: Label 'En remplacement de : %1', Comment = '%1 = Article';
        // ESK011: Label 'du : %1';
        total_ht: array[3] of Decimal;
        total_tva: array[3] of Decimal;
        total_ht_net: array[3] of Decimal;
        total_discount: array[3] of Decimal;
        total_ttc: array[3] of Decimal;
        taux_tva: array[3] of Text;
        CodeSubstitution: Text;
        ReferenceSaisie: Text;
        textSIEGSOC: array[5] of Text[250];
        RefActive: Text;
        ReportLineNumber: Integer;

        _ImprimerLogo: Boolean;
        //  OrderCreateUserCaptionLbl: Label 'Order Create User';

        IsRequestPage: Boolean;

        gInfoIncoterm: Text;

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(5)) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    procedure GetUnitOfMeasureDescr(UOMCode: Code[10]): Text[50];
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        IF NOT UnitOfMeasure.GET(UOMCode) THEN
            EXIT(UOMCode);
        EXIT(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10];
    begin
        EXIT(PADSTR('', 2, ' '));
    end;

    local procedure DocumentCaption() _Result: Text[250];
    var
        Text50001Lbl: Label 'BON DE LIVRAISON N° %1', COmment = '%1 = Expédition';
    // Text50002: Label 'DUPLICATA BON DE LIVRAISON  N° %1';
    begin
        IF "Sales Shipment Header"."No. Printed" = 0 THEN
            _Result := STRSUBSTNO(Text50001Lbl, "Sales Shipment Header"."No.")
        ELSE
            _Result := STRSUBSTNO(Text50001Lbl, "Sales Shipment Header"."No.");


        EXIT(_Result);

        /*
        IF "Sales Invoice Header"."Prepayment Invoice" THEN
          EXIT(Text010);
        EXIT(Text004);
         */

    end;

    procedure "--- ESKAPE ---"();
    begin
    end;

    procedure GetLivTot(): Boolean;
    var
        LignesBL: Record "Sales Shipment Line";
        /*  loc_SalesLine: Record "Sales Line";
         loc_QteTot: Decimal; */
        loc_QteLiv: Decimal;
    begin
        loc_QteLiv := 0;
        CLEAR(LignesBL);

        //CJ 14-09-10  modif dernier IF loc_..

        //On recherche les lignes de BL antérieures au BL pour l'article et la commande
        LignesBL.SETRANGE("Order No.", "Sales Shipment Line"."Order No.");
        LignesBL.SETRANGE("Order Line No.", "Sales Shipment Line"."Order Line No.");
        LignesBL.SETFILTER("Posting Date", '<%1', "Sales Shipment Line"."Posting Date");

        //On récupère les quantités déjà livrées
        IF LignesBL.FINDSET() THEN
            REPEAT
                loc_QteLiv += LignesBL.Quantity;
            UNTIL LignesBL.NEXT() = 0;

        //On met à jour le champ Quantité restante à imprimer sur le report
        //_QteDejaLivree := loc_QteLiv;

        //Si la ligne est entièrement livrée on renvoie vrai
        IF loc_QteLiv = QteCdée THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure GetQteDejaLivre() dec_rtn: Decimal;
    var
        LignesBL: Record "Sales Shipment Line";
    begin
        dec_rtn := 0;


        //On recherche les lignes de BL antérieures au BL pour l'article et la commande
        LignesBL.SETRANGE("Order No.", "Sales Shipment Line"."Order No.");
        LignesBL.SETRANGE("Order Line No.", "Sales Shipment Line"."Order Line No.");
        LignesBL.SETFILTER("Document No.", '<%1', "Sales Shipment Line"."Document No.");

        //On récupère les quantités déjà livrées
        IF LignesBL.FINDSET() THEN
            REPEAT
                dec_rtn += LignesBL.Quantity;
            UNTIL LignesBL.NEXT() = 0;
    end;
}

