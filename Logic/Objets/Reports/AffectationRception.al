report 50075 "Affectation Réception"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Affectation Réception.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Affectation Réception';
    dataset
    {
        dataitem("Warehouse Receipt Header"; "Warehouse Receipt Header")
        {
            DataItemTableView = SORTING("No.");
            PrintOnlyIfDetail = true;
            column(WR_Header_PostingDate; "Posting Date")
            {
            }
            column(WR_Header_VendorShipmentNo; "Vendor Shipment No.")
            {
            }
            column(WR_Header_No; "No.")
            {
            }
            column(WR_Header_VendorNo; "Warehouse Receipt Header".GetVendorNo())
            {
            }
            column(WR_Header_VendorName; "Warehouse Receipt Header".GetVendorName())
            {
            }
            dataitem("Warehouse Receipt Line"; "Warehouse Receipt Line")
            {
                DataItemLink = "No." = FIELD("No.");
                DataItemTableView = SORTING("No.", "Line No.")
                                    ORDER(Ascending)
                                    WHERE("Qty. to Receive" = FILTER(> 0));
                column(WR_Line_ItemNo; GestioMultiRef.RechercheRefActive("Item No."))
                {
                }
                column(WR_Line_Description; Description)
                {
                }
                column(WR_Line_Description2; "Description 2")
                {
                }
                column(WR_Line_QtyOutstanding; "Qty. Outstanding")
                {
                }
                column(WR_Line_QtyToReceive; "Qty. to Receive")
                {
                }
                dataitem("Affectation Ligne Achat"; "Affectation Ligne Achat")
                {
                    DataItemLink = "Purch. Document No." = FIELD("Source No."),
                                   "Purch. Line No." = FIELD("Source Line No.");
                    DataItemTableView = SORTING("Purch. Document Type", "Purch. Document No.", "Purch. Line No.", "Sales Document No.")
                                        ORDER(Ascending)
                                        WHERE("Purch. Document Type" = CONST(Order));
                    column(SalesDocumentNo; STRSUBSTNO(ESK001Txt, "Affectation Ligne Achat"."Sales Document No."))
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin

                        IF NoCdeClient <> '' THEN
                            Evaluate(NoCdeClient, NoCdeClient + '/');

                        Evaluate(NoCdeClient, NoCdeClient + "Affectation Ligne Achat"."Sales Document No.");
                    end;

                    trigger OnPreDataItem();
                    begin
                        NoCdeClient := ''
                    end;
                }
                dataitem("Sales Line"; "Sales Line")
                {
                    DataItemLink = "No." = FIELD("Item No.");
                    DataItemTableView = SORTING("Document Type", Type, "No.")
                                        ORDER(Ascending)
                                        WHERE("Document Type" = CONST(Order),
                                              Type = CONST(Item),
                                              "Outstanding Quantity" = FILTER(> 0));
                    column(Sales_Line_SellToCustomerNo; "Sell-to Customer No.")
                    {
                    }
                    column(Sales_Line_ShipToName; SalesHeader."Ship-to Name")
                    {
                    }
                    column(Sales_Line_OrderDate; SalesHeader."Order Date")
                    {
                    }
                    column(Sales_Line_TypeCommande; SalesHeader."Type de commande")
                    {
                    }
                    column(Sales_Line_DocumentNo; "Document No.")
                    {
                    }
                    column(Sales_Line_ExternalDocumentNo; SalesHeader."External Document No.")
                    {
                    }
                    column(Sales_Line_OutstandingQuantity; "Outstanding Quantity")
                    {
                    }
                    column(Sales_Line_RequestedDeliveryDate; "Requested Delivery Date")
                    {
                    }
                    column(Sales_Line_OrderCreateUser; SalesHeader."Order Create User")
                    {
                    }
                    column(Sales_Line_LineDiscount; FORMAT("Line Discount %") + ' %')
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin
                        CLEAR(SalesHeader);
                        IF NOT SalesHeader.GET("Sales Line"."Document Type", "Sales Line"."Document No.") THEN CurrReport.SKIP();
                        IF temp_SalesLine.GET("Sales Line"."Document Type", "Sales Line"."Document No.", "Sales Line"."Line No.") THEN CurrReport.SKIP();

                        IF ExportExcel THEN
                            MakeExcelDataBody();

                        // MCO Le 07-09-2017 => Régie
                        InsertAffectationLine();
                        // FIN MCO Le 07-09-2017
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    LItem: Record Item;
                // LPurchLine: Record "Purchase Line";
                begin

                    //IF UniquementSiDemandeCli THEN
                    IF TRUE THEN BEGIN
                        LItem.GET("Warehouse Receipt Line"."Item No.");
                        ////LItem.CALCFIELDS("Qty. on Sales Order", "Qty. on Kit Sales Lines");
                        ////IF (LItem."Qty. on Sales Order" + LItem."Qty. on Kit Sales Lines") = 0 THEN
                        LItem.CALCFIELDS("Qty. on Sales Order");
                        IF (LItem."Qty. on Sales Order") = 0 THEN
                            CurrReport.SKIP();
                    END;

                    /* AD Le 21-01-2014 => Changement de méthode
                    // AD Le 21-03-2013 =>
                    NoCdeClient := '';
                    IF LPurchLine.GET("Warehouse Receipt Line"."Source Subtype", "Warehouse Receipt Line"."Source No.",
                        "Warehouse Receipt Line"."Source Line No.") THEN
                      NoCdeClient := LPurchLine."Lien Commande client";
                    // FIN AD Le 21-03-2013
                    */

                end;
            }
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                field(Uniquement_SiDemandeCliField; UniquementSiDemandeCli)
                {
                    Caption = 'Uniquement lignes avec besoins';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Uniquement lignes avec besoins field.';
                }
                field(Export_ExcelField; ExportExcel)
                {
                    Caption = 'Exporter vers Excel';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Exporter vers Excel field.';
                }
                field("Création affectation"; CreateAffect)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CreateAffect field.';
                }
            }
        }
    }
    trigger OnPostReport();
    begin
        IF ExportExcel THEN
            CreateExcelbook();
    end;

    trigger OnPreReport();
    begin
        IF ExportExcel THEN
            MakeExcelInfo();


        CompanyInfo.GET();
        CompanyInfo.CALCFIELDS(Picture);

        // MCO Le 07-09-2017
        bln_Urgent := FALSE;
        IF CONFIRM(ESK002Qst) THEN bln_Urgent := TRUE
        // MCO Le 07-09-2017
    end;

    var
        temp_ExcelBuf: Record "Excel Buffer" temporary;
        SalesHeader: Record "Sales Header";
        rec_Item: Record Item;
        temp_SalesLine: Record "Sales Line" temporary;
        CompanyInfo: Record "Company Information";
        GestioMultiRef: Codeunit "Gestion Multi-référence";
        ExportExcel: Boolean;
        UniquementSiDemandeCli: Boolean;
        // rec_Four: Record Vendor;
        NoCdeClient: Text[1024];
        Excel002Lbl: Label 'Data';
        ESK001Txt: Label 'Commande client : %1', comment = '%1 = N° Docuement vente';
        bln_Urgent: Boolean;
        ESK002Qst: Label 'L''affectation est-elle urgente ?';
        CreateAffect: Boolean;

    procedure MakeExcelInfo();
    begin
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        temp_ExcelBuf.NewRow();
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".FIELDCAPTION("Item No."), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        // DZ Le 11/06/2012 => Récupération du N° et Nom du fournisseur
        temp_ExcelBuf.AddColumn(rec_Item.FIELDCAPTION("Vendor No."), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        // Fin DZ Le 11/06/2012
        temp_ExcelBuf.AddColumn('No Cde Client', FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);  // AD Le 21-03-2013
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".FIELDCAPTION(Description), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".FIELDCAPTION("Description 2"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".FIELDCAPTION(Quantity), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".FIELDCAPTION("Qty. to Receive"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Document No."), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Sell-to Customer No."), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Ship-to Name"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Order Date"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Outstanding Quantity"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Requested Delivery Date"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Order Create User"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Type de commande"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("External Document No."), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Line Discount %"), FALSE, '', TRUE, FALSE, TRUE, '', temp_ExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        temp_ExcelBuf.NewRow();

        // DZ Le 11/06/2012 => Récupération du N° et Nom du fournisseur
        rec_Item.GET("Warehouse Receipt Line"."Item No.");
        // MC Le 12-06-2012 => Il faut prendre le fournisseur de la commande et non de l'article
        // Mise en commentaire car fonction existe déjà
        //rec_Four.GET(rec_Item."Vendor No.");
        // FIN MC Le 12-06-2012
        // Fin DZ Le 11/06/2012

        temp_ExcelBuf.AddColumn(GestioMultiRef.RechercheRefActive("Warehouse Receipt Line"."Item No."), FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        // DZ Le 11/06/2012 => Récupération du N° et Nom du fournisseur
        temp_ExcelBuf.AddColumn("Warehouse Receipt Header".GetVendorNo() + ' ' +
                           "Warehouse Receipt Header".GetVendorName(), FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        //temp_ExcelBuf.AddColumn(rec_Item."Vendor No." + ' ' + rec_Four.Name,FALSE,'',FALSE,FALSE,FALSE,'');
        // Fin DZ Le 11/06/2012
        // AD Le 21-03-2013 => Remodifier AD Le 21-01-2014
        IF STRPOS(NoCdeClient, "Sales Line"."Document No.") <> 0 THEN
            temp_ExcelBuf.AddColumn("Sales Line"."Document No.", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text)
        ELSE
            temp_ExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        // FIN AD Le 21-03-2013
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".Description, FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line"."Description 2", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line".Quantity, FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Warehouse Receipt Line"."Qty. to Receive", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line"."Document No.", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line"."Sell-to Customer No.", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader."Ship-to Name", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader."Order Date", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line"."Outstanding Quantity", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line"."Requested Delivery Date", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader."Order Create User", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader."Type de commande", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn(SalesHeader."External Document No.", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
        temp_ExcelBuf.AddColumn("Sales Line"."Line Discount %", FALSE, '', FALSE, FALSE, FALSE, '', temp_ExcelBuf."Cell Type"::Text);
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        temp_ExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, 'Liste des Affectations', COMPANYNAME, USERID)
        //temp_ExcelBuf.CreateBook(Excel002);
        //temp_ExcelBuf.WriteSheet('Liste des Affectations',COMPANYNAME,USERID);
        //temp_ExcelBuf.GiveUserControl;
        //ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;

    local procedure InsertAffectationLine();
    var
        recL_AffectationLine: Record "Affectation Réception";
    begin
        IF CreateAffect THEN BEGIN
            recL_AffectationLine."Receipt No." := "Warehouse Receipt Header"."No.";
            recL_AffectationLine."Receipt Line No." := "Warehouse Receipt Line"."Line No.";
            recL_AffectationLine."Sales Order No." := "Sales Line"."Document No.";
            recL_AffectationLine."Sales Order Line No." := "Sales Line"."Line No.";
            recL_AffectationLine."Item No." := "Sales Line"."No.";
            recL_AffectationLine."Vendor No." := "Warehouse Receipt Header".GetVendorNo();
            recL_AffectationLine.Urgence := bln_Urgent;

            IF STRPOS(NoCdeClient, "Sales Line"."Document No.") <> 0 THEN
                recL_AffectationLine."No Cde Client" := "Sales Line"."Document No."
            ELSE
                recL_AffectationLine."No Cde Client" := '';

            recL_AffectationLine.Quantity := "Warehouse Receipt Line".Quantity;
            recL_AffectationLine."Quantity To Receive" := "Warehouse Receipt Line"."Qty. to Receive";
            IF recL_AffectationLine.INSERT() THEN BEGIN
                temp_SalesLine := "Sales Line";
                temp_SalesLine.Insert();
            END;
        END;
    end;
}

