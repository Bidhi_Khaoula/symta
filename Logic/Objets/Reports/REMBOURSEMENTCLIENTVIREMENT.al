report 50060 "REMBOURSEMENT CLIENT(VIREMENT)"
{
    caption = 'REMBOURSEMENT CLIENT(VIREMENT)';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\REMBOURSEMENT CLIENT(VIREMENT).rdlc';

    dataset
    {
        dataitem(LOGO; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);

                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
            end;
        }
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("Due Date");
            RequestFilterFields = "No.";
            column(No; "No.")
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(BankName; bank.Name)
            {
            }
            column(BankAccountNo2; bank."Bank Account No.")
            {
            }
            column(nb_cheque; nb_cheque)
            {
            }
            column(clientName; client.Name)
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column(AcceptationCode; "Acceptation Code")
            {
            }
            column(BankBranchNo; "Bank Branch No.")
            {
            }
            column(AgencyCode; "Agency Code")
            {
            }
            column(BankAccountNo; "Bank Account No.")
            {
            }
            column(RIBKey; "RIB Key")
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(DebitAmount; "Debit Amount")
            {
            }

            trigger OnAfterGetRecord();
            begin

                client.GET("Payment Line"."Account No.");
                nb_cheque += 1;
            end;

            trigger OnPreDataItem();
            begin

                LastFieldNo := FIELDNO("No.");
                "tmp_ lig".COPY("Payment Line");
                IF "tmp_ lig".FIND('-') THEN;
                nb_cheque := "tmp_ lig".COUNT;
                entete.GET("tmp_ lig"."No.");

                bank.GET(entete."Account No.");
                FormatAddrCodeunit.BankAcc(CustAddr, bank);
                CompanyInfo.GET();
                FormatAddrCodeunit.Company(CompanyAddr, CompanyInfo);
                CompanyInfo.CALCFIELDS(Picture);
                CompanyInfo.CALCFIELDS(Picture);
                IF dessin.GET('compta') THEN
                    dessin.CALCFIELDS("Image 1");


                nb_cheque := 0;
            end;
        }
    }

    var
      bank: Record "Bank Account";
        entete: Record "Payment Header";
        "tmp_ lig": Record "Payment Line";
         client: Record Customer;
        dessin: Record "Parametre Edition Contrat";
         CompanyInfo: Record "Company Information";
             FormatAddrCodeunit: Codeunit "Format Address";    
        //TotalFor: Label '"Total "';
        LastFieldNo: Integer;
        //FooterPrinted: Boolean;
        CustAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];    
      
        nb_cheque: Integer;
       
}

