report 50172 "iNIT FEUILLE tva"
{
    Caption = 'iNIT FEUILLE tva';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("VAT Posting Setup"; "VAT Posting Setup")
        {
            DataItemTableView = SORTING("VAT Bus. Posting Group", "VAT Prod. Posting Group");

            trigger OnAfterGetRecord();
            var
                ParamTxt: Label 'achat %1 %2-%4 a %3%', Comment = '%1 ; %2 ;%3 ;%4';
                Param1Txt: Label 'achat %1 %2-%4 %3', Comment = '%1 ; %2 ;%3 ;%4';
                Param2Txt: Label 'achat %1 %2-%4 a %3%', Comment = '%1 ; %2 ;%3 ;%4';
                Param3Txt: Label 'achat %1 %2 %3', Comment = '%1 ; %2 ;%3';


            begin
                No_ligne := No_ligne + 1000;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA."Line No." := No_ligne;
                TVA."Row No." := FORMAT(No_ligne);
                TVA.Type := TVA.Type::"VAT Entry Totaling";
                TVA."Gen. Posting Type" := TVA."Gen. Posting Type"::Purchase;
                TVA."VAT Bus. Posting Group" := "VAT Bus. Posting Group";
                TVA."VAT Prod. Posting Group" := "VAT Prod. Posting Group";
                TVA."Amount Type" := TVA."Amount Type"::Base;
                TVA.Print := TRUE;
                TVA.Description := STRSUBSTNO(ParamTxt, FORMAT(TVA."Amount Type"),
                                                TVA."VAT Bus. Posting Group",
                                                "VAT Posting Setup"."VAT %",
                                                "VAT Prod. Posting Group");
                IF "VAT Calculation Type" = "VAT Calculation Type"::"Full VAT" THEN
                    TVA.Description := STRSUBSTNO(Param1Txt,
                                                   FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", FORMAT("VAT Calculation Type"),
                                                   "VAT Prod. Posting Group");
                TVA.Insert();


                No_ligne := No_ligne + 1000;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA."Line No." := No_ligne;
                TVA."Row No." := FORMAT(No_ligne);
                TVA.Type := TVA.Type::"VAT Entry Totaling";
                TVA."Gen. Posting Type" := TVA."Gen. Posting Type"::Purchase;
                TVA."VAT Bus. Posting Group" := "VAT Bus. Posting Group";
                TVA."VAT Prod. Posting Group" := "VAT Prod. Posting Group";
                TVA."Amount Type" := TVA."Amount Type"::Amount;
                TVA.Description := STRSUBSTNO(Param2Txt,
                                            FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", "VAT Posting Setup"."VAT %",
                                            "VAT Prod. Posting Group");
                IF "VAT Calculation Type" = "VAT Calculation Type"::"Full VAT" THEN
                    TVA.Description := STRSUBSTNO(Param3Txt, FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", FORMAT("VAT Calculation Type"));

                TVA.Print := TRUE;
                TVA.Insert();


                No_ligne := No_ligne + 1000;
                CLEAR(TVA);
                TVA."Line No." := No_ligne;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA.Insert();
            end;
        }
        dataitem("VAT Posting Setup 2"; "VAT Posting Setup")
        {

            trigger OnAfterGetRecord();
            VAr
                ParamTxt: Label 'achat %1 %2-%4 a %3%', Comment = '%1 ; %2 ;%3 ;%4';
                Param1Txt: Label 'Vente %1 %2-%4 %3', COmment = '%1 ; %2 ;%3 ;%4';
            begin
                No_ligne := No_ligne + 1000;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA."Line No." := No_ligne;
                TVA."Row No." := FORMAT(No_ligne);
                TVA.Type := TVA.Type::"VAT Entry Totaling";
                TVA."Gen. Posting Type" := TVA."Gen. Posting Type"::Sale;
                TVA."VAT Bus. Posting Group" := "VAT Bus. Posting Group";
                TVA."VAT Prod. Posting Group" := "VAT Prod. Posting Group";
                TVA."Amount Type" := TVA."Amount Type"::Base;
                TVA.Print := TRUE;
                TVA.Description := STRSUBSTNO(ParamTxt,
                                              FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", "VAT %",
                                              "VAT Prod. Posting Group");
                IF "VAT Calculation Type" = "VAT Calculation Type"::"Full VAT" THEN
                    TVA.Description := STRSUBSTNO(Param1Txt,
                                             FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", FORMAT("VAT Calculation Type"),
                                              "VAT Prod. Posting Group");

                TVA.Insert();


                No_ligne := No_ligne + 1000;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA."Line No." := No_ligne;
                TVA."Row No." := FORMAT(No_ligne);
                TVA.Type := TVA.Type::"VAT Entry Totaling";
                TVA."Gen. Posting Type" := TVA."Gen. Posting Type"::Sale;
                TVA."VAT Bus. Posting Group" := "VAT Bus. Posting Group";
                TVA."VAT Prod. Posting Group" := "VAT Prod. Posting Group";
                TVA."Amount Type" := TVA."Amount Type"::Amount;
                TVA.Description := STRSUBSTNO(ParamTxt,
                                           FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", "VAT Posting Setup"."VAT %",
                                           "VAT Prod. Posting Group");
                IF "VAT Calculation Type" = "VAT Calculation Type"::"Full VAT" THEN
                    TVA.Description := STRSUBSTNO(Param1Txt,
                                     FORMAT(TVA."Amount Type"), TVA."VAT Bus. Posting Group", FORMAT("VAT Calculation Type"),
                                     "VAT Prod. Posting Group");

                TVA.Print := TRUE;
                TVA.Insert();


                No_ligne := No_ligne + 1000;
                CLEAR(TVA);
                TVA."Line No." := No_ligne;
                TVA."Statement Template Name" := 'TVA';
                TVA."Statement Name" := 'TVA';
                TVA.Insert();
            end;
        }
    }
    var
        TVA: Record "VAT Statement Line";
        No_ligne: Integer;
}

