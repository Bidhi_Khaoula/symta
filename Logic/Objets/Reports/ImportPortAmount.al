report 50073 "Import Port Amount"
{
    Caption = 'Import des lignes de port depuis Excel';
    Permissions = TableData "Sales Header" = rimd,
                  TableData "Sales Line" = rimd,
                  TableData "Sales Shipment Header" = rimd,
                  TableData "Sales Shipment Line" = rimd;
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                rec_BL: Record "Sales Shipment Header";
                rec_LineBL: Record "Sales Shipment Line";
                rec_SalesOrder: Record "Sales Header";
                rec_SalesLineOrder: Record "Sales Line";
                rec_InvoiceShipment: Record "Shipment Invoiced";
                "Cust Post Group": Record "Customer Posting Group";
                NextBLLineNo: Integer;
                NextOrderLineNo: Integer;
            begin

                // Initialisation des variables

                // Incrémentation de la ligne
                No_ligne := No_ligne + 1;

                // Mise à jour de la fenetre de progression
                Fenetre.UPDATE(1, ROUND(No_ligne / nb * 10000, 1));



                //-------------------------------------------------//
                // 1ere Etape : Contrôles des différentes erreurs. //
                //-------------------------------------------------//

                // Cellule Vide
                IF NOT GetCell('A') THEN
                    ERROR(Format(STRSUBSTNO(Error001Lbl, No_ligne)));

                // BL inexistant
                IF NOT rec_BL.GET(TempExcelBuf."Cell Value as Text") THEN
                    ERROR(Format(STRSUBSTNO(Error002Lbl, TempExcelBuf."Cell Value as Text", No_ligne)));

                // BL déjà facturé.
                rec_InvoiceShipment.RESET();
                rec_InvoiceShipment.SETRANGE("Shipment No.", rec_BL."No.");
                IF NOT rec_InvoiceShipment.ISEMPTY THEN
                    ERROR(Format(STRSUBSTNO(Error003Lbl, rec_BL."No.", No_ligne)));

                // Vérification qu'une ligne de port n'existe pas déjà pour le BL
                "Cust Post Group".GET(rec_BL."Customer Posting Group");
                "Cust Post Group".TESTFIELD("Shipment Account");

                rec_LineBL.RESET();
                rec_LineBL.SETRANGE("Document No.", rec_BL."No.");
                rec_LineBL.SETRANGE(Type, rec_LineBL.Type::"G/L Account");
                rec_LineBL.SETRANGE("No.", "Cust Post Group"."Shipment Account");
                rec_LineBL.SETFILTER(Quantity, '<>%1', 0);
                IF NOT rec_LineBL.ISEMPTY THEN
                    IF NOT "Remplacer ligne de port" THEN
                        ERROR(Format(STRSUBSTNO(Error004Lbl, rec_BL."No.", No_ligne)))

                    ELSE BEGIN
                        rec_LineBL.FINDFIRST();
                        rec_SalesLineOrder.RESET();
                        rec_SalesLineOrder.GET(rec_SalesLineOrder."Document Type"::Order, rec_LineBL."Order No.", rec_LineBL."Order Line No.");
                        rec_SalesLineOrder.DELETE();
                        rec_LineBL.DELETE();
                    END;


                // Montant du port à zéro (Attention ce n'est pas une erreur mais un passage à la ligne suivante)
                IF NOT GetCell('B') THEN
                    CurrReport.SKIP();

                IF TempExcelBuf."Cell Value as Text" = '0' THEN
                    CurrReport.SKIP();


                //-------------------------------------------------//
                //                 FIN 1ere Etape                  //
                //-------------------------------------------------//


                //-------------------------------------------------//
                // 2eme Etape : Mise à jour des documents.         //
                //-------------------------------------------------//


                // Insertion de la ligne de port sur la commande d'ou provient le bon de livraison.
                // Récupération de la commande vente pour la r-ovurir.
                IF NOT rec_SalesOrder.GET(rec_SalesLineOrder."Document Type"::Order, rec_BL."Order No.") THEN
                    ERROR(Format(STRSUBSTNO(Error005Lbl, rec_BL."Order No.", rec_BL."No.", No_ligne)));
                rec_SalesOrder.Status := rec_SalesOrder.Status::Open;
                // On récupère la dernière ligne du BL afin de connaitre le prochain N° de ligne
                rec_SalesLineOrder.RESET();
                rec_SalesLineOrder.SETRANGE("Document No.", rec_BL."Order No.");
                rec_SalesLineOrder.SETRANGE("Document Type", rec_SalesLineOrder."Document Type"::Order);
                rec_SalesLineOrder.FINDLAST();
                NextOrderLineNo := rec_SalesLineOrder."Line No." + 10000;

                // Création de la nouvelle ligne de port.
                rec_SalesLineOrder.RESET();
                rec_SalesLineOrder.INIT();
                rec_SalesLineOrder.VALIDATE("Document Type", rec_SalesLineOrder."Document Type"::Order);
                rec_SalesLineOrder.VALIDATE("Document No.", rec_BL."Order No.");
                rec_SalesLineOrder."Line No." := NextOrderLineNo;
                rec_SalesLineOrder.Insert();
                rec_SalesLineOrder.Type := rec_SalesLineOrder.Type::"G/L Account";

                rec_SalesLineOrder."System-Created Entry" := TRUE;// AD Le 18-11-2015 => MIG2015
                rec_SalesLineOrder.VALIDATE("No.", "Cust Post Group"."Shipment Account");
                rec_SalesLineOrder."Internal Line Type" := rec_SalesLineOrder."Internal Line Type"::Shipment;
                rec_SalesLineOrder.VALIDATE(Quantity, 1);
                EVALUATE(rec_SalesLineOrder."Unit Price", TempExcelBuf."Cell Value as Text");
                rec_SalesLineOrder.VALIDATE("Unit Price");
                rec_SalesLineOrder."Quantity Shipped" := 1;
                rec_SalesLineOrder."Qty. to Ship" := 0;
                rec_SalesLineOrder.InitOutstanding();
                rec_SalesLineOrder."System-Created Entry" := FALSE;// AD Le 18-11-2015 => MIG2015
                rec_SalesLineOrder.MODIFY();

                rec_SalesOrder.Status := rec_SalesOrder.Status::Released;
                // Insertion de la ligne de port sur le bon de livraison.
                // On récupère la dernière ligne du BL afin de connaitre le prochain N° de ligne
                rec_LineBL.RESET();
                rec_LineBL.SETRANGE("Document No.", rec_BL."No.");
                rec_LineBL.FINDLAST();
                NextBLLineNo := rec_LineBL."Line No." + 10000;

                // Création de la nouvelle ligne de port.
                rec_LineBL.RESET();
                rec_LineBL.INIT();
                rec_LineBL.TRANSFERFIELDS(rec_SalesLineOrder);
                rec_LineBL."Document No." := rec_BL."No.";
                rec_LineBL.Type := rec_LineBL.Type::"G/L Account";
                rec_LineBL."Line No." := NextBLLineNo;
                rec_LineBL."No." := "Cust Post Group"."Shipment Account";
                rec_LineBL."Internal Line Type" := rec_LineBL."Internal Line Type"::Shipment;
                rec_LineBL."Order No." := rec_SalesLineOrder."Document No.";
                rec_LineBL."Order Line No." := rec_SalesLineOrder."Line No.";
                // Zone pour impression BL
                rec_LineBL."Zone Code" := rec_BL."Zone Code";
                rec_LineBL."Gerer par groupement" := rec_BL."Regroupement Centrale";

                rec_LineBL.Insert();
            end;

            trigger OnPreDataItem();
            begin

                //On se positionne sur la 1ere ligne des données (entete=0)
                IF "Nom de colonne(premier ligne)" THEN
                    No_ligne := 1
                ELSE
                    No_ligne := 0;
                TempExcelBuf.FINDLAST();
                nb := TempExcelBuf."Row No." - No_ligne;
                IF nb < 1 THEN
                    ERROR('Aucune ligne à importer');

                Integer.SETRANGE(Number, 1, nb);

                Fenetre.OPEN('Import des lignes @1@@@@@@@@@@');
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Nom de colonne(premier ligne) Name"; "Nom de colonne(premier ligne)")
                {
                    Caption = 'Nom de colonne(première ligne)';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom de colonne(première ligne) field.';
                }
                field("Remplacer ligne de port Name"; "Remplacer ligne de port")
                {
                    Caption = 'Remplacer ligne de port';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remplacer ligne de port field.';
                }
            }
        }

        actions
        {
        }

        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', Locked = true;
        begin
            IF CloseAction = ACTION::OK THEN BEGIN
                Evaluate(FileName, FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok));
                IF FileName = '' THEN
                    EXIT(FALSE);

                SheetName := TempExcelBuf.SelectSheetsName(FileName);
                IF SheetName = '' THEN
                    EXIT(FALSE);
            END;
        end;
    }

    trigger OnPostReport();
    begin
        TempExcelBuf.DELETEALL();
    end;

    trigger OnPreReport();
    begin
        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        ConvertAsciAnsii: Codeunit "ANSI  <->  ASCII converter";
        FileName: Text[250];
        UploadedFileName: Text[1024];
        SheetName: Text[250];
        No_ligne: Integer;
        // Item: Record Item;
        // Codeart: Text[20];
        // UnitOfMeasure: Record "Item Unit of Measure";
        // TypeVente: Text[30];
        // Customer: Record Customer;
        // CustomerPriceGroup: Record "Customer Price Group";
        // Campaign: Record Campaign;
        // DateFin: Date;
        Fenetre: Dialog;
        nb: Decimal;
        "Nom de colonne(premier ligne)": Boolean;
        "Remplacer ligne de port": Boolean;
        Text015Lbl: Label 'Choisir le fichier';
        Error001Lbl: Label 'N° BL ne doit pas être vide. Colonne : A, Ligne : %1', Comment = '%1=BL; = N° ligne';
        Error002Lbl: Label 'N° BL : %1 inexistant dans la base. Colonne : A, Ligne %2', Comment = '%1=BL; %2 = N° ligne';
        Error003Lbl: Label 'N° BL : %1 déjà facturé dans la base. Colonne : A, Ligne %2', Comment = '%1=BL; %2 = N° ligne';
        Error004Lbl: Label 'N°BL : %1 possède déjà une ligne de port. Colonne : A, Ligne %2', Comment = '%1=BL; %2 = N° ligne';
        Error005Lbl: Label 'La commande : %1 n''existe pas pour le BL %2. Colonne A, Ligne %3', Comment = '%1=N° commande ; %2=BL; %3 = N° ligne';
    //***********************Non utilise************************************************************
    // local procedure ReadExcelSheet();
    // begin
    //     //Lecture de la feuille excel
    //     IF UploadedFileName = '' THEN
    //         UploadFile()
    //     ELSE
    //         FileName := UploadedFileName;

    //     TempExcelBuf.OpenBook(FileName, SheetName);
    //     TempExcelBuf.ReadSheet();
    // end;
    //***********************Non utilise************************************************************
    procedure UploadFile();
    var
        CommonDialogMgt: Codeunit "File Management";
        ClientFileName: Text[1024];
    begin
        Evaluate(UploadedFileName, CommonDialogMgt.UploadFile(Text015Lbl, ClientFileName));
        //UploadedFileName := CommonDialogMgt.OpenFile(Text015,ClientFileName,2,'',0);
        Evaluate(FileName, UploadedFileName);
    end;

    procedure GetCell(Row: Text[5]): Boolean
    begin
        //Cette fonction permet de se positionner sur la celule passé en paramètre//
        TempExcelBuf.INIT();
        TempExcelBuf.SETRANGE("Row No.", No_ligne);
        TempExcelBuf.SETRANGE(xlColID, Row);
        IF TempExcelBuf.FINDFIRST() THEN
            EXIT(TRUE)
        ELSE
            EXIT(FALSE);
    end;

    procedure SetDescription(var Desc1: Text[200]; var Desc2: Text[200]);
    var
        Desc: Text[250];
    begin
        Desc := ConvertAsciAnsii.Ansi2Ascii(Desc1);
        IF STRLEN(Desc) > 50 THEN BEGIN
            Desc1 := COPYSTR(Desc, 1, 50);
            Desc2 := COPYSTR(Desc, 50, 50);
        END
        ELSE
            Desc1 := Desc;
    end;
}

