report 50170 "Fiche étendue Export Item"
{
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No. 2")
                                ORDER(Ascending);
            RequestFilterFields = "No.", "Item Category Code";
            //TODO, "Product Group Code";

            trigger OnAfterGetRecord();
            begin

                cpt := cpt + 1;
                // Avancement
                Window.UPDATE(2, Item.Description);
                //TODOExportItem.ExportRecord(Item, cpt, _CustomerCode);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(TypeExportName; TypeExport)
                {
                    Caption = 'Format d''export';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Format d''export field.';
                }
                field(XmlPathName; XmlPath)
                {
                    Caption = 'Fichier XML';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fichier XML field.';

                    trigger OnAssistEdit();
                    var
                        CommonDialogMgt: Codeunit "File Management";
                    begin
                        //TododXmlPath := CommonDialogMgt.SaveFileDialog('Fichier', XmlPath, 'Fichiers XML(*.xml)|*.xml|Tous les fichiers (*.*)|*.*');
                    end;
                }
                field(SimplifiedOnName; SimplifiedOn)
                {
                    Caption = 'Simplifié';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Simplifié field.';
                }
            }
        }
    }
    trigger OnPostReport();
    begin

        // Fin de l'export (fermeture du fichier)
        //TODO ExportItem.ExportEnd();
    end;

    trigger OnPreReport();
    begin

        /*//TODO CASE TypeExport OF
             TypeExport::XML:
                 BEGIN
                     ExportItem.Init('XML', XmlPath, SimplifiedOn, ImagePathOn);
                 END;
             TypeExport::CSV:
                 BEGIN
                     ExportItem.Init('CSV', TEMPORARYPATH + '\export.csv', SimplifiedOn, ImagePathOn);
                 END;
             TypeExport::Excel:
                 BEGIN
                     ExportItem.Init('XLS', '', SimplifiedOn, ImagePathOn);
                 END;
         END;

         // Démarrage de l'export (ouverture du fichier)
         ExportItem.ExportStart();*/
        cpt := 0;
        // Export des données
        Window.OPEN(DIALOG_INFOLbl);
        Window.UPDATE(1, TypeExport);
    end;

    var
        //TODOExportItem: Codeunit 50086;
        cpt: Integer;
        TypeExport: Option CSV,XML,Excel;
        XmlPath: Text[1024];
        Window: Dialog;
        // _CustomerCode: Code[20];
        SimplifiedOn: Boolean;
        // ImagePathOn: Boolean;
        DIALOG_INFOLbl: Label 'Format : #1####\Article : #2####################', Comment = '#1 #2';
}

