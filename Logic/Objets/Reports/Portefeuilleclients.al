report 50024 "Portefeuille  clients"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Portefeuille  clients.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Sales Header"; "Sales Header")
        {
            DataItemTableView = SORTING("No.", Status, "Sell-to Customer No.", "Document Type")
                                ORDER(Ascending)
                                WHERE("Document Type" = FILTER(Order | "Blanket Order"),
                                      Loans = CONST(false));
            PrintOnlyIfDetail = true;
            RequestFilterFields = "Order Date", "Bill-to Name", "Sell-to Customer No.", "Sell-to Customer Name", "Order Create User", Status, "Shipment Status";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(HeaderFilter; HeaderFilter)
            {
            }
            column(OrderDate; "Order Date")
            {
            }
            column(DirectionCode; "Direction Code")
            {
            }
            column(CustomerName; CustomerName)
            {
            }
            column(Mois; MonthOf + '  ' + FORMAT(YearOf))
            {
            }
            dataitem("Sales Line"; "Sales Line")
            {
                DataItemLink = "Document Type" = FIELD("Document Type"),
                               "Document No." = FIELD("No.");
                DataItemTableView = SORTING("Document Type", "Document No.", "Line No.");
                RequestFilterFields = "No.";
                column(Type; Type)
                {
                }
                column(RequestedDeliveryDate; "Requested Delivery Date")
                {
                }
                column(ShipmentDate; "Shipment Date")
                {
                }
                column(OrderNo; "OrderNo.")
                {
                }
                column(RechercheReference; "Recherche référence")
                {
                }
                column(Description; Description)
                {
                }
                column(OutstandingQuantity; "Outstanding Quantity")
                {
                }
                column(NetUnitPrice; "Net Unit Price")
                {
                }
                column(Amount; "Sales Line"."Outstanding Quantity" * "Sales Line"."Net Unit Price")
                {
                }

                trigger OnAfterGetRecord();
                begin

                    CASE "Sales Line"."Document Type" OF
                        "Sales Line"."Document Type"::Order:
                            MtRestant += "Sales Line"."Outstanding Quantity" * "Sales Line"."Net Unit Price";
                        "Sales Line"."Document Type"::"Blanket Order":
                            MtRestant += "Sales Line"."Blanket Order Quantity Outstan" * "Sales Line"."Net Unit Price";
                    END;
                end;

                trigger OnPostDataItem();
                begin

                    // Export Excel
                    MakeExcelDataBody();
                end;

                trigger OnPreDataItem();
                begin
                    MtRestant := 0;
                    SETFILTER("Outstanding Quantity", '>0');
                    SETRANGE(Type, "Sales Line".Type::Item);
                end;
            }

            trigger OnAfterGetRecord();
            begin

                CurrentMonth := DATE2DMY("Sales Header"."Order Date", 2);

                Saleheader2.COPY("Sales Header");
                Saleheader2.SETCURRENTKEY("Document Type", "Document Date");
                Exist := Saleheader2.FIND('>');

                Evaluate(CustomerName, "Sales Header"."Bill-to Name");
                "OrderNo." := "Sales Header"."No.";


                IF NOT Customer.GET("Sales Header"."Sell-to Customer No.") THEN CLEAR(Customer);

                MonthOf := ReturnDateMonth("Sales Header"."Order Date");
                YearOf := DATE2DMY("Sales Header"."Order Date", 3);
            end;

            trigger OnPreDataItem();
            begin
                HeaderFilter := Format("Sales Header".GETFILTERS);
            end;
        }
    }

    var
        Customer: Record Customer;
        TempExcelBuf: Record "Excel Buffer" temporary;
        Saleheader2: Record "Sales Header";
        // TotalMois: Decimal;
       // TotalAmount: Decimal;
        CurrentMonth: Integer;
        Exist: Boolean;
        CustomerName: Text[80];
        "OrderNo.": Code[20];
        MonthOf: Text[30];
        YearOf: Integer;
        HeaderFilter: Text[250];
        MtRestant: Decimal;
        Excel002Lbl: Label 'Data';
        Excel003Lbl: Label 'Customer - Order Detail';
        Excel004Lbl: Label 'Company Name';
        Excel005Lbl: Label 'Report No.';
        Excel006Lbl: Label 'Report Name';
        Excel007Lbl: Label 'User ID';
        Excel008Lbl: Label 'Date';
        Excel010Lbl: Label 'Sales Order Lines Filters';

    procedure ReturnDateMonth(Date: Date): Text[30];
    var
        Month: Integer;
    begin
        Month := DATE2DMY(Date, 2);

        CASE Month OF
            1:
                EXIT('Janvier');
            2:
                EXIT('Février');
            3:
                EXIT('Mars');
            4:
                EXIT('Avril');
            5:
                EXIT('Mai');
            6:
                EXIT('Juin');
            7:
                EXIT('Juillet');
            8:
                EXIT('Aout');
            9:
                EXIT('Septembre');
            10:
                EXIT('Octobre');
            11:
                EXIT('Novembre');
            12:
                EXIT('Décembre');
        END;
    end;

    procedure "--- Excel ----"();
    begin
    end;

    procedure MakeExcelInfo();
    begin
        // Export Excel
        //TempExcelBuf.SetUseInfoSheed;
        TempExcelBuf.AddInfoColumn(FORMAT(Excel004Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(COMPANYNAME, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel006Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(FORMAT(Excel003Lbl), FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel005Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        ERROR('TODO 1');
        //TempExcelBuf.AddInfoColumn(REPORT::"Taux de service/Client",FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel007Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(USERID, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel008Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(TODAY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel010Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn("Sales Header".GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.ClearNewRow();
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Document Type"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Order Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Direction Code"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Sell-to Customer No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Sell-to Customer Name"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("External Document No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION(Responsable), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Customer.FIELDCAPTION("Direction Code"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION(Status), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Shipment Status"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".FIELDCAPTION("Requested Delivery Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Montant', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Montant Restant', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        "Sales Header".CALCFIELDS(Amount);

        TempExcelBuf.AddColumn(FORMAT("Sales Header"."Document Type"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT("Sales Header"."Order Date"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header"."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header"."Direction Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header"."Sell-to Customer No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header"."Sell-to Customer Name", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header"."External Document No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Header".Responsable, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Customer."Direction Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT("Sales Header".Status), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT("Sales Header"."Shipment Status"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT("Sales Header"."Requested Delivery Date"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Sales Header".Amount), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(MtRestant), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataTotal();
    begin
        // Export Excel
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBook('', Excel002Lbl);
        TempExcelBuf.WriteSheet(Excel003Lbl, COMPANYNAME, USERID);
        //TempExcelBuf.GiveUserControl;
        ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[30];
    begin
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    end;
}

