report 50045 "Sales - Credit Memo SYM" //207
{
    DefaultLayout = RDLC;
    RDLCLayout = './Objets/Reports/Layouts/Sales - Credit Memo.rdlc';

    Caption = 'Sales - Credit Memo';
    Permissions = TableData "Sales Shipment Buffer" = rimd;
    PreviewMode = PrintLayout;

    dataset
    {
        dataitem(LOGO; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfo1Picture; CompanyInfo1.Picture)
            {
            }
            column(CompanyInfo1PictureAddress; CompanyInfo1."Picture Address")
            {
            }

            trigger OnAfterGetRecord()
            begin
                CLEAR(CompanyInfo1.Picture);
                ////CLEAR(RecSocNavi.Picture);

                IF EskapeCommunication.GetImpressionPdf() THEN
                    _ImprimerLogo := TRUE;

                CompanyInfo1.GET();
                IF _ImprimerLogo THEN
                    CompanyInfo1.CALCFIELDS(Picture);
                CompanyInfo1.CALCFIELDS("Picture Address");
                GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
            end;
        }
        dataitem("Sales Cr.Memo Header"; "Sales Cr.Memo Header")
        {
            RequestFilterFields = "No.", "Posting Date", "Salesperson Code", "Sell-to Customer No.", "No. Printed", "Payment Method Code";
            RequestFilterHeading = 'Posted Sales Invoice';
            column(No_SalesInvHdr; "No.")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }
            column(PmtTermsDescCaption; PmtTermsDescCaptionLbl)
            {
            }
            column(ShpMethodDescCaption; ShpMethodDescCaptionLbl)
            {
            }
            column(HomePageCaption; HomePageCaptionCapLbl)
            {
            }
            column(EMailCaption; EMailCaptionLbl)
            {
            }
            column(DocDateCaption; DocDateCaptionLbl)
            {
            }
            column(AfficherTexteCentrale; "Source Document Type")
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(DocumentCaption; DocumentCaption())
                    {
                    }
                    column(LogoClient; '')
                    {
                    }
                    column(CodeLogo; '')
                    {
                    }
                    column(DocCaptCopyText; DocumentCaption())
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(CompanyInfoVATRegistrationNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEMail; CompanyInfo."E-Mail")
                    {
                    }
                    column(BilltoCustNo_SalesInvHdr; "Sales Cr.Memo Header"."Bill-to Customer No.")
                    {
                    }
                    column(PostingDate_SalesInvHdr; FORMAT("Sales Cr.Memo Header"."Posting Date", 0, 4))
                    {
                    }
                    column(VATNoText; VATNoText)
                    {
                    }
                    column(VATRegNo_SalesInvHdr; "Sales Cr.Memo Header"."VAT Registration No.")
                    {
                    }
                    column(DueDate_SalesInvHdr; FORMAT("Sales Cr.Memo Header"."Due Date"))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Code + '-' + SalesPurchPerson.Name)
                    {
                    }
                    column(No1_SalesInvHdr; "Sales Cr.Memo Header"."No.")
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourRef_SalesInvHdr; "Sales Cr.Memo Header"."Your Reference")
                    {
                    }
                    column(OrderNoText; OrderNoText)
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(DocDate_SalesInvHdr; FORMAT("Sales Cr.Memo Header"."Document Date", 0, 4))
                    {
                    }
                    column(PricesInclVAT_SalesInvHdr; "Sales Cr.Memo Header"."Prices Including VAT")
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(PricesInclVAT1_SalesInvHdr; FORMAT("Sales Cr.Memo Header"."Prices Including VAT"))
                    {
                    }
                    column(SellToAddr1; SellToAddr[1])
                    {
                    }
                    column(SellToAddr2; SellToAddr[2])
                    {
                    }
                    column(SellToAddr3; SellToAddr[3])
                    {
                    }
                    column(SellToAddr4; SellToAddr[4])
                    {
                    }
                    column(SellToAddr5; SellToAddr[5])
                    {
                    }
                    column(SellToAddr6; SellToAddr[6])
                    {
                    }
                    column(SellToAddr7; SellToAddr[7])
                    {
                    }
                    column(SellToAddr8; SellToAddr[8])
                    {
                    }
                    column(ShiptoAddrCaption; AddrCaption)
                    {
                    }
                    column(PageCaption; PageCaptionCapLbl)
                    {
                    }
                    column(PaymentTermsDescription; PaymentTerms.Description)
                    {
                    }
                    column(TextPaiement; TextPaiement)
                    {
                    }
                    column(ShipmentMethodDescription; ShipmentMethod.Description)
                    {
                    }
                    column(PhoneNoCaption; PhoneNoCaptionLbl)
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(GiroNoCaption; GiroNoCaptionLbl)
                    {
                    }
                    column(BankNameCaption; BankNameCaptionLbl)
                    {
                    }
                    column(BankAccNoCaption; BankAccNoCaptionLbl)
                    {
                    }
                    column(DueDateCaption; DueDateCaptionLbl)
                    {
                    }
                    column(InvoiceNoCaption; InvoiceNoCaptionLbl)
                    {
                    }
                    column(PostingDateCaption; PostingDateCaptionLbl)
                    {
                    }
                    column(BilltoCustNo_SalesInvHdrCaption; 'N° Client')
                    {
                    }
                    column(PricesInclVAT_SalesInvHdrCaption; "Sales Cr.Memo Header".FIELDCAPTION("Prices Including VAT"))
                    {
                    }
                    column("CoordonnéesBancaires"; CoordonnéesBancaires)
                    {
                    }
                    column(OrderNos; "Sales Cr.Memo Header"."External Document No.")
                    {
                    }
                    column(OrderNoLbl; OrderNoLbl)
                    {
                    }
                    column(BaseLivraison; BaseLivraison)
                    {
                    }
                    column(PaiementTermsMethod; PaiementTermsMethod)
                    {
                    }
                    column(SelltoAddrCaption; SelltoAddrCaptionLbl)
                    {
                    }
                    column(NoSerieSpecialCaption; NoSerieSpecialCaptionLbl)
                    {
                    }
                    column(SellToCustNo; TxtClientLivre)
                    {
                    }
                    column(SellToCustNoCaption; "Sales Cr.Memo Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(ShippingAgentName; RecTransporteur.Name)
                    {
                    }
                    column(ShippingAgentNameCaption; ShippingAgentNameCaptionLbl)
                    {
                    }
                    column(MontantComm; MontantComm)
                    {
                    }
                    column(MontantCommCaption; MontantCommCaptionLbl)
                    {
                    }
                    column(TauxCommFilter; TauxCommFilter)
                    {
                    }
                    column(Texte_Central; Texte_Central)
                    {
                    }
                    column(SalespersonCode; "Sales Cr.Memo Header"."Salesperson Code")
                    {
                    }
                    dataitem(DimensionLoop1; Integer)
                    {
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = FILTER(1 ..));
                        column(DimText; DimText)
                        {
                        }
                        column(Number_DimensionLoop1; DimensionLoop1.Number)
                        {
                        }
                        column(HdrDimsCaption; HdrDimsCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            IF Number = 1 THEN BEGIN
                                IF NOT DimSetEntry1.FINDSET() THEN
                                    CurrReport.BREAK();
                            END ELSE
                                IF NOT Continue THEN
                                    CurrReport.BREAK();

                            CLEAR(DimText);
                            Continue := FALSE;
                            REPEAT
                                OldDimText := DimText;
                                IF DimText = '' THEN
                                    DimText := STRSUBSTNO(Param1Txt, DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code")
                                ELSE
                                    DimText :=
                                      STRSUBSTNO(
                                        ParamTxt, DimText,
                                        DimSetEntry1."Dimension Code", DimSetEntry1."Dimension Value Code");
                                IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                    DimText := OldDimText;
                                    Continue := TRUE;
                                    EXIT;
                                END;
                            UNTIL DimSetEntry1.NEXT() = 0;
                        end;

                        trigger OnPreDataItem()
                        begin
                            IF NOT ShowInternalInfo THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem(CommEntFacture; "Sales Comment Line")
                    {
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            WHERE("Document Type" = CONST("Posted Credit Memo"),
                                                  "Print Invoice" = CONST(true),
                                                  "Document Line No." = CONST(0));
                        column(Commentaire_Facture; Comment)
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            SETRANGE("No.", "Sales Cr.Memo Header"."No.");
                        end;
                    }
                    dataitem("Sales Cr.Memo Line"; "Sales Cr.Memo Line")
                    {
                        DataItemLink = "Document No." = FIELD("No.");
                        DataItemLinkReference = "Sales Cr.Memo Header";
                        DataItemTableView = SORTING("Document No.", "Line No.")
                                            WHERE(Type = FILTER(<> ' '));
                        column(Commentaire_BL; Commentaire_BL)
                        {
                        }
                        column(LineAmt_SalesInvLine; "Line Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(Desc_SalesInvLine; Desc_SalesInvLine)
                        {
                        }
                        column(Desc2_SalesInvLine; "Description 2")
                        {
                        }
                        column(No_SalesInvLine; ItemNo)
                        {
                        }
                        column(Quantity_SalesInvLine; Quantity)
                        {
                        }
                        column(UnitofMeasure_SalesInvLine; "Unit of Measure")
                        {
                        }
                        column(NetUnitPrice_SalesInvLine; "Net Unit Price")
                        {
                        }
                        column(UnitPrice_SalesInvLine; "Unit Price")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 2;
                        }
                        column(LineDisc_SalesInvLine; TxtRemise)
                        {
                        }
                        column(VATIdentifier_SalesInvLine; "VAT Identifier")
                        {
                        }
                        column(PostedShipmentDate; FORMAT(PostedShipmentDate))
                        {
                        }
                        column(Type_SalesInvLine; FORMAT("Sales Cr.Memo Line".Type))
                        {
                        }
                        column(GetTotalLineAmt; GetTotalLineAmount)
                        {
                        }
                        column(GetTotalAmtIncVAT; GetTotalAmountIncVAT)
                        {
                        }
                        column(GetTotalAmtIncVATtexte; FORMAT(GetTotalAmountIncVAT, 0, '<Precision,2:2>'))
                        {
                        }
                        column(InvDiscAmt; -"Inv. Discount Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(TotalSubTotal; TotalSubTotal)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalInvoiceDiscAmt; TotalInvoiceDiscountAmount)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalText; TotalText)
                        {
                        }
                        column(Amt_SalesInvLine; Amount)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(TotalAmt; TotalAmount)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmtTexte; FORMAT(TotalAmount, 0, '<Precision,2:2><Standard Format,2>'))
                        {
                        }
                        column(AmtIncludingVATAmt_SalesInvLine; "Amount Including VAT" - Amount)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(AmtInclVAT_SalesInvLine; "Amount Including VAT")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(VATAmtText_SalesInvLine; TempVATAmountLine.VATAmountText())
                        {
                        }
                        column(TotalExclVATText; TotalExclVATText)
                        {
                        }
                        column(TotalInclVATText; TotalInclVATText)
                        {
                        }
                        column(TotalAmtInclVAT; TotalAmountInclVAT)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(TotalAmtInclVATTexte; FORMAT(TotalAmountInclVAT, 0, '<Precision,2:2><Standard Format,2>'))
                        {
                        }
                        column(TotalAmtVAT; TotalAmountVAT)
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATBaseDisc_SalesInvHdr; "Sales Cr.Memo Header"."VAT Base Discount %")
                        {
                            AutoFormatType = 1;
                        }
                        column(TotalPaymentDiscOnVAT; TotalPaymentDiscountOnVAT)
                        {
                            AutoFormatType = 1;
                        }
                        column(LineNo_SalesInvLine; "Line No.")
                        {
                        }
                        column(UnitPriceCaption; UnitPriceCaptionLbl)
                        {
                        }
                        column(DiscPercentCaption; DiscPercentCaptionLbl)
                        {
                        }
                        column(AmtCaption; AmtCaptionLbl)
                        {
                        }
                        column(PostedShpDateCaption; PostedShpDateCaptionLbl)
                        {
                        }
                        column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
                        {
                        }
                        column(SubtotalCaption; SubtotalCaptionLbl)
                        {
                        }
                        column(PmtDiscVATCaption; PmtDiscVATCaptionLbl)
                        {
                        }
                        column(Desc_SalesInvLineCaption; FIELDCAPTION(Description))
                        {
                        }
                        column(No_SalesInvLineCaption; FIELDCAPTION("No."))
                        {
                        }
                        column(Quantity_SalesInvLineCaption; FIELDCAPTION(Quantity))
                        {
                        }
                        column(UnitofMeasure_SalesInvLineCaption; FIELDCAPTION("Unit of Measure"))
                        {
                        }
                        column(VATIdentifier_SalesInvLineCaption; FIELDCAPTION("VAT Identifier"))
                        {
                        }
                        column(ShipToAddr8; ShipToAddr[8])
                        {
                        }
                        column(ShipToAddr7; ShipToAddr[7])
                        {
                        }
                        column(ShipToAddr6; ShipToAddr[6])
                        {
                        }
                        column(ShipToAddr5; ShipToAddr[5])
                        {
                        }
                        column(ShipToAddr4; ShipToAddr[4])
                        {
                        }
                        column(ShipToAddr3; ShipToAddr[3])
                        {
                        }
                        column(ShipToAddr2; ShipToAddr[2])
                        {
                        }
                        column(ShipToAddr1; ShipToAddr[1])
                        {
                        }
                        column(TauxComm; TauxComm)
                        {
                        }
                        column(TauxCommCaption; TauxCommCaption)
                        {
                        }
                        column(ReferenceActive; ReferenceActive)
                        {
                        }
                        column(NoClientLivre_SalesInvLin; "N° client Livré")
                        {
                        }
                        column(RetourNo_SalesInvLine; Entete_retour."No.")
                        {
                        }
                        column(ExternalDocumentNo_SalesInvLine; Entete_retour."External Document No.")
                        {
                        }
                        column(PostingDate_SalesInvLine; Entete_retour."Posting Date")
                        {
                        }
                        column(OrderNo_SalesInvLine; Entete_retour."Return Order No.")
                        {
                        }
                        column(CumulClientLivre_SalesInvLine; Cumul_client_livré)
                        {
                        }
                        column(TariffNo; TariffNo)
                        {
                        }
                        column(ClientLivreLigne; Code_client_livré)
                        {
                        }
                        dataitem("Sales Shipment Buffer"; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(SalesShipmentBufferPostingDate; FORMAT(TempSalesShipmentBuffer."Posting Date"))
                            {
                            }
                            column(SalesShipmentBufferQty; TempSalesShipmentBuffer.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(ShpCaption; ShpCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin
                                IF Number = 1 THEN
                                    TempSalesShipmentBuffer.FIND('-')
                                ELSE
                                    TempSalesShipmentBuffer.NEXT();
                            end;

                            trigger OnPreDataItem()
                            begin
                                TempSalesShipmentBuffer.SETRANGE("Document No.", "Sales Cr.Memo Line"."Document No.");
                                TempSalesShipmentBuffer.SETRANGE("Line No.", "Sales Cr.Memo Line"."Line No.");

                                SETRANGE(Number, 1, TempSalesShipmentBuffer.COUNT);
                            end;
                        }
                        dataitem(DimensionLoop2; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = FILTER(1 ..));
                            column(DimText1; DimText)
                            {
                            }
                            column(LineDimsCaption; LineDimsCaptionLbl)
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin
                                IF Number = 1 THEN BEGIN
                                    IF NOT DimSetEntry2.FINDSET() THEN
                                        CurrReport.BREAK();
                                END ELSE
                                    IF NOT Continue THEN
                                        CurrReport.BREAK();

                                CLEAR(DimText);
                                Continue := FALSE;
                                REPEAT
                                    OldDimText := DimText;
                                    IF DimText = '' THEN
                                        DimText := STRSUBSTNO(Param1Txt, DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code")
                                    ELSE
                                        DimText :=
                                          STRSUBSTNO(
                                            ParamTxt, DimText,
                                            DimSetEntry2."Dimension Code", DimSetEntry2."Dimension Value Code");
                                    IF STRLEN(DimText) > MAXSTRLEN(OldDimText) THEN BEGIN
                                        DimText := OldDimText;
                                        Continue := TRUE;
                                        EXIT;
                                    END;
                                UNTIL DimSetEntry2.NEXT() = 0;
                            end;

                            trigger OnPreDataItem()
                            begin
                                IF NOT ShowInternalInfo THEN
                                    CurrReport.BREAK();

                                DimSetEntry2.SETRANGE("Dimension Set ID", "Sales Cr.Memo Line"."Dimension Set ID");
                            end;
                        }
                        dataitem(AsmLoop; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(TempPostedAsmLineUOMCode; GetUOMText(TempPostedAsmLine."Unit of Measure Code"))
                            {
                            }
                            column(TempPostedAsmLineQty; TempPostedAsmLine.Quantity)
                            {
                                DecimalPlaces = 0 : 5;
                            }
                            column(TempPostedAsmLineVariantCode; BlanksForIndent() + TempPostedAsmLine."Variant Code")
                            {
                            }
                            column(TempPostedAsmLineDesc; BlanksForIndent() + TempPostedAsmLine.Description)
                            {
                            }
                            column(TempPostedAsmLineNo; BlanksForIndent() + TempPostedAsmLine."No.")
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin
                                IF Number = 1 THEN
                                    TempPostedAsmLine.FINDSET()
                                ELSE
                                    TempPostedAsmLine.NEXT();
                            end;

                            trigger OnPreDataItem()
                            begin
                                CLEAR(TempPostedAsmLine);
                                IF NOT DisplayAssemblyInformation THEN
                                    CurrReport.BREAK();
                                CollectAsmInformation();
                                CLEAR(TempPostedAsmLine);
                                SETRANGE(Number, 1, TempPostedAsmLine.COUNT);
                            end;
                        }
                        dataitem(ItemTrackingLine; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(TrackingSpecBufferNo; TempTrackingSpecBuffer."Item No.")
                            {
                            }
                            column(TrackingSpecBufferDesc; TempTrackingSpecBuffer.Description)
                            {
                            }
                            column(TrackingSpecBufferLotNo; TempTrackingSpecBuffer."Lot No.")
                            {
                            }
                            column(TrackingSpecBufferSerNo; TempTrackingSpecBuffer."Serial No.")
                            {
                            }
                            column(TrackingSpecBufferQty; TempTrackingSpecBuffer.Quantity)
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin

                                IF Number = 1 THEN
                                    TempTrackingSpecBuffer.FINDSET()
                                ELSE
                                    TempTrackingSpecBuffer.NEXT();
                                /*
                                IF UPPERCASE(USERID)=UPPERCASE('CapAlliance\eskape') THEN
                                 MESSAGE(TempTrackingSpecBuffer."Serial No.");
                                */

                                /*
                                ShowTotal := FALSE;
                                IF ItemTrackingAppendix.IsStartNewGroup(TempTrackingSpecBuffer) THEN
                                  ShowTotal := TRUE;
                                
                                ShowGroup := FALSE;
                                IF (TempTrackingSpecBuffer."Source Ref. No." <> OldRefNo) OR
                                   (TempTrackingSpecBuffer."Item No." <> OldNo)
                                THEN BEGIN
                                  OldRefNo := TempTrackingSpecBuffer."Source Ref. No.";
                                  OldNo := TempTrackingSpecBuffer."Item No.";
                                  TotalQty := 0;
                                END ELSE
                                  ShowGroup := TRUE;
                                TotalQty += TempTrackingSpecBuffer."Quantity (Base)";
                                */

                            end;

                            trigger OnPreDataItem()
                            begin
                                TrackingSpecCount := TempTrackingSpecBuffer.COUNT;

                                IF TrackingSpecCount = 0 THEN
                                    CurrReport.BREAK();

                                SETRANGE(Number, 1, TrackingSpecCount);
                            end;
                        }
                        dataitem(ShipmentDatas; Integer)
                        {
                            DataItemTableView = SORTING(Number);
                            column(NoShipmentText; NoShipmentText)
                            {
                            }
                            column(NoShipmentDatas1; NoShipmentDatas[1])
                            {
                            }
                            column(NoShipmentDatas2; NoShipmentDatas[2])
                            {
                            }
                            column(NoShipmentDatas3; NoShipmentDatas[3])
                            {
                            }

                            trigger OnAfterGetRecord()
                            begin
                                CLEAR(NoShipmentDatas);
                                NoShipmentNumLoop := 0;
                                IF ShipmentDatas.Number = 1 THEN BEGIN
                                    ShipmentInvoiced.FIND('-');
                                    NoShipmentText := Text10800Lbl;
                                    REPEAT
                                        NoShipmentNumLoop := NoShipmentNumLoop + 1;
                                        NoShipmentDatas[NoShipmentNumLoop] := ShipmentInvoiced."Shipment No.";
                                    UNTIL (ShipmentInvoiced.NEXT() = 0) OR (NoShipmentNumLoop = 3);
                                END ELSE BEGIN
                                    NoShipmentText := '';
                                    REPEAT
                                        NoShipmentNumLoop := NoShipmentNumLoop + 1;
                                        NoShipmentDatas[NoShipmentNumLoop] := ShipmentInvoiced."Shipment No.";
                                    UNTIL (ShipmentInvoiced.NEXT() = 0) OR (NoShipmentNumLoop = 3);
                                END;
                            end;

                            trigger OnPreDataItem()
                            begin
                                IF NOT IncludeShptNo THEN
                                    CurrReport.BREAK();

                                IF ShipmentInvoiced.COUNT < 1 THEN
                                    CurrReport.BREAK();
                                ShipmentDatas.SETRANGE(Number, 1, ROUND(ShipmentInvoiced.COUNT / 3, 1, '>'));
                            end;
                        }
                        dataitem(SalesCommentLine; "Sales Comment Line")
                        {
                            DataItemLink = "Document Line No." = FIELD("Line No."),
                                           "No." = FIELD("Document No.");
                            DataItemLinkReference = "Sales Cr.Memo Line";
                            DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                                WHERE("Document Type" = CONST("Posted Credit Memo"),
                                                      "Print Invoice" = CONST(true));
                            column(CommentLine_SalesCommentLine; Comment)
                            {
                            }
                        }

                        trigger OnAfterGetRecord()
                        var
                            LItem: Record Item;
                            LLigneAttach: Record "Sales Invoice Line";
                            // "Sales Invoice Line For BL": Record "Sales Invoice Line";
                            "Sales Comment Line For BL": Record "Sales Comment Line";
                            LRecClient: Record Customer;
                            LF: Char;
                            CR: Char;
                        begin
                            PostedShipmentDate := 0D;

                            IF (Type = Type::"G/L Account") AND (NOT ShowInternalInfo) THEN
                                "No." := '';

                            // ESKVN1.0 => Gestion de l'impression des commentaires
                            IF "Sales Cr.Memo Line".Type = "Sales Cr.Memo Line".Type::" " THEN
                                IF NOT "Sales Cr.Memo Line"."Print Invoice" THEN
                                    CurrReport.SKIP();
                            // FIN ESKVN1.0

                            // AD Le 08-01-2015 => Demande de Mme DEPONT
                            IF ("Sales Cr.Memo Line".Type = "Sales Cr.Memo Line".Type::Item) AND ("Sales Cr.Memo Line".Quantity = 0) THEN
                                CurrReport.SKIP();

                            // AD Le 19-01-2015
                            IF ("Sales Cr.Memo Line".Type = "Sales Cr.Memo Line".Type::" ") AND ("Sales Cr.Memo Line"."Attached to Line No." <> 0) THEN BEGIN
                                LLigneAttach.GET("Sales Cr.Memo Line"."Document No.", "Sales Cr.Memo Line"."Attached to Line No.");
                                IF LLigneAttach.Quantity = 0 THEN
                                    CurrReport.SKIP();
                            END;

                            // AD Le 12-02-2014
                            Desc_SalesInvLine := Description;
                            IF Type = Type::Item THEN
                                IF LItem.GET("No.") THEN
                                    IF LItem."Vendor Item No." <> '' THEN
                                        Desc_SalesInvLine := Desc_SalesInvLine + ' (' + LItem."Vendor Item No." + ')';
                            // FIN AD Le 12-02-2014

                            ItemNo := "No.";
                            IF (STRLEN(ItemNo) = 7) THEN
                                ItemNo := COPYSTR(ItemNo, 3, STRLEN(ItemNo) - 2);

                            TempVATAmountLine.INIT();
                            TempVATAmountLine."VAT Identifier" := "VAT Identifier";
                            TempVATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                            TempVATAmountLine."Tax Group Code" := "Tax Group Code";
                            TempVATAmountLine."VAT %" := "VAT %";
                            TempVATAmountLine."VAT Base" := Amount;
                            TempVATAmountLine."Amount Including VAT" := "Amount Including VAT";
                            TempVATAmountLine."Line Amount" := "Line Amount";
                            IF "Allow Invoice Disc." THEN
                                TempVATAmountLine."Inv. Disc. Base Amount" := "Line Amount";
                            TempVATAmountLine."Invoice Discount Amount" := "Inv. Discount Amount";
                            TempVATAmountLine."VAT Clause Code" := "VAT Clause Code";
                            TempVATAmountLine.InsertLine();

                            TotalSubTotal += "Line Amount";
                            TotalInvoiceDiscountAmount -= "Inv. Discount Amount";
                            TotalAmount += Amount;
                            TotalAmountVAT += "Amount Including VAT" - Amount;
                            TotalAmountInclVAT += "Amount Including VAT";
                            TotalPaymentDiscountOnVAT += -("Line Amount" - "Inv. Discount Amount" - "Amount Including VAT");

                            IF IncludeShptNo THEN BEGIN
                                ShipmentInvoiced.RESET();
                                ShipmentInvoiced.SETRANGE("Invoice No.", "Sales Cr.Memo Line"."Document No.");
                                ShipmentInvoiced.SETRANGE("Invoice Line No.", "Sales Cr.Memo Line"."Line No.");
                            END;

                            TempTrackingSpecBuffer.DELETEALL();
                            /////GetItemTrackingLines(TempTrackingSpecBuffer);

                            TauxComm := "VAT Identifier";
                            TauxCommCaption := TvaCaptionLbl;
                            IF TauxCommFilter THEN
                                ////    TauxComm := FORMAT("Sales Cr.Memo Line"."Sales Commission");
                                TauxCommCaption := '';
                            ////    MontantComm += "Sales Cr.Memo Line".Amount * ("Sales Cr.Memo Line"."Sales Commission" / 100);
                            CLEAR(txtEan13Compo);
                            CLEAR(PrixTaille);

                            // nomenclature douanière
                            TariffNo := '';
                            IF Type = Type::Item THEN
                                IF LItem.GET("No.") THEN
                                    IF IsExport THEN
                                        TariffNo := LItem."Tariff No.";

                            ReferenceActive := GestionMulti.RechercheRefActive("Sales Cr.Memo Line"."No.");

                            IF Entete_retour.GET("Return Receipt No.") THEN
                                FormatAddr.SalesRcptSellTo(ShipToAddr, Entete_retour)
                            ELSE
                                FormatAddr.SalesCrMemoSellTo(ShipToAddr, "Sales Cr.Memo Header");


                            Commentaire_BL := '';
                            IF "N° client Livré" <> Code_client_livré THEN BEGIN
                                Cumul_client_livré := 0;
                                Code_client_livré := "N° client Livré";

                                // changement de BL ?
                                IF LastShipmentNo <> "Return Receipt No." THEN BEGIN
                                    LastShipmentNo := "Return Receipt No.";
                                    // recherche des commentaires liés au BL
                                    "Sales Comment Line For BL".SETRANGE("No.", "Return Receipt No.");
                                    "Sales Comment Line For BL".SETRANGE("Document Type", "Sales Comment Line For BL"."Document Type"::"Posted Return Receipt");
                                    "Sales Comment Line For BL".SETRANGE("Print Invoice", TRUE);
                                    "Sales Comment Line For BL".SETRANGE("Document Line No.", 0);
                                    IF "Sales Comment Line For BL".FIND('-') THEN BEGIN
                                        LF := 10;
                                        CR := 13;
                                        REPEAT
                                            // on met à jour un champ avec le commentaire, et le saut de ligne (solution trouvée pour gérer rupture dans report ...)
                                            Commentaire_BL := Commentaire_BL + FORMAT(CR, 0, '<CHAR>') + FORMAT(LF, 0, '<CHAR>') + "Sales Comment Line For BL".Comment;
                                        UNTIL ("Sales Comment Line For BL".NEXT() = 0);
                                    END;
                                END;
                            END;
                            Cumul_client_livré := Cumul_client_livré + "Line Amount";
                            // AD Le 04-12-2015
                            TxtRemise := FormatRemise();
                            LRecClient.GET("Sell-to Customer No.");
                            IF LRecClient."Masquer remise sur édition" THEN BEGIN
                                TxtRemise := '';
                                "Unit Price" := "Net Unit Price";
                            END;

                            // AD Le 20-11-2015 =>
                            //IF Entete_bl."No." = '' THEN BEGIN
                            //  "Shipment No." :=  "Sales Cr.Memo Header"."Order No.";
                            //  "N° client Livré" := "Sales Cr.Memo Header"."Sell-to Customer No.";
                            //  Entete_bl."External Document No." := "Sales Cr.Memo Header"."External Document No.";
                            //  Entete_bl."Posting Date" := WORKDATE;
                            //  Entete_bl."Order No." := "Sales Cr.Memo Header"."Order No.";
                            //END;
                            // FIN AD Le 20-11-2015
                            FormatAddr.SalesCrMemoBillTo(CustAddr, "Sales Cr.Memo Header");

                            IF Cust."Inverser Adresses Factures" THEN BEGIN

                                CLEAR(AddrTMP);
                                FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                                    AddrTMP[i] := ShipToAddr[i];

                                CLEAR(ShipToAddr);
                                FOR i := 1 TO ARRAYLEN(CustAddr) DO
                                    ShipToAddr[i] := CustAddr[i];

                                CLEAR(CustAddr);
                                FOR i := 1 TO ARRAYLEN(AddrTMP) DO
                                    CustAddr[i] := AddrTMP[i];
                            END
                        end;

                        trigger OnPreDataItem()
                        begin

                            TempVATAmountLine.DELETEALL();
                            TempSalesShipmentBuffer.RESET();
                            TempSalesShipmentBuffer.DELETEALL();
                            FirstValueEntryNo := 0;
                            MoreLines := FIND('+');
                            WHILE MoreLines AND (Description = '') AND ("No." = '') AND (Quantity = 0) AND (Amount = 0) DO
                                MoreLines := NEXT(-1) <> 0;
                            IF NOT MoreLines THEN
                                CurrReport.BREAK();
                            SETRANGE("Line No.", 0, "Line No.");

                            GetTotalLineAmount := 0;
                            GetTotalInvDiscAmount := 0;
                            GetTotalAmount := 0;
                            GetTotalAmountIncVAT := 0;


                            Cumul_client_livré := 0;
                            Code_client_livré := '';
                        end;
                    }
                    dataitem(LigneTotal; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            ORDER(Ascending);
                        column(LigneTotalNum; FORMAT(LigneTotal.Number))
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            LigneTotal.SETRANGE(Number, 1, 1)
                        end;
                    }
                    dataitem(VATCounter; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATAmtLineTTC; TempVATAmountLine."VAT Base" + TempVATAmountLine."VAT Amount")
                        {
                        }
                        column(VATAmtLineVATBase; TempVATAmountLine."VAT Base")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Line".GetCurrencyCode();
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVATAmt; TempVATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineLineAmt; TempVATAmountLine."Line Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvDiscBaseAmt; TempVATAmountLine."Inv. Disc. Base Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineInvoiceDiscAmt; TempVATAmountLine."Invoice Discount Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVAT; TempVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier; TempVATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATPercentCaption; VATPercentCaptionLbl)
                        {
                        }
                        column(VATBaseCaption; VATBaseCaptionLbl)
                        {
                        }
                        column(VATAmtCaption; VATAmtCaptionLbl)
                        {
                        }
                        column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
                        {
                        }
                        column(VATIdentCaption; VATIdentCaptionLbl)
                        {
                        }
                        column(InvDiscBaseAmtCaption; InvDiscBaseAmtCaptionLbl)
                        {
                        }
                        column(LineAmtCaption; LineAmtCaptionLbl)
                        {
                        }
                        column(InvDiscAmtCaption1; InvDiscAmtCaption1Lbl)
                        {
                        }
                        column(TotalCaption; TotalCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempVATAmountLine.GetLine(Number);
                        end;

                        trigger OnPreDataItem()
                        begin
                            SETRANGE(Number, 1, TempVATAmountLine.COUNT);
                        end;
                    }
                    dataitem(PaiementMultiEch; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            ORDER(Ascending);
                        column(PaiementDateEch; FORMAT(TempBufferPaiment."Due Date"))
                        {
                        }
                        column(PaiementPaiement; TempBufferPaiment.Amount)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            ////TempBufferPaiment.GetLine(Number);
                        end;

                        trigger OnPreDataItem()
                        begin
                            SETRANGE(Number, 1, TempBufferPaiment.COUNT);
                        end;
                    }
                    dataitem(VATClauseEntryCounter; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(VATClauseVATIdentifier; TempVATAmountLine."VAT Identifier")
                        {
                        }
                        column(VATClauseCode; TempVATAmountLine."VAT Clause Code")
                        {
                        }
                        column(VATClauseDescription; VATClause.Description)
                        {
                        }
                        column(VATClauseDescription2; VATClause."Description 2")
                        {
                        }
                        column(VATClauseAmount; TempVATAmountLine."VAT Amount")
                        {
                            AutoFormatExpression = "Sales Cr.Memo Header"."Currency Code";
                            AutoFormatType = 1;
                        }
                        column(VATClausesCaption; VATClausesCapLbl)
                        {
                        }
                        column(VATClauseVATIdentifierCaption; VATIdentCaptionLbl)
                        {
                        }
                        column(VATClauseVATAmtCaption; VATAmtCaptionLbl)
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempVATAmountLine.GetLine(Number);
                            IF NOT VATClause.GET(TempVATAmountLine."VAT Clause Code") THEN
                                CurrReport.SKIP();
                            VATClause.TranslateDescription("Sales Cr.Memo Header"."Language Code");
                        end;

                        trigger OnPreDataItem()
                        begin
                            CLEAR(VATClause);
                            SETRANGE(Number, 1, TempVATAmountLine.COUNT);
                        end;
                    }
                    dataitem(VatCounterLCY; Integer)
                    {
                        DataItemTableView = SORTING(Number);
                        column(VALSpecLCYHeader; VALSpecLCYHeader)
                        {
                        }
                        column(VALExchRate; VALExchRate)
                        {
                        }
                        column(VALVATBaseLCY; VALVATBaseLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VALVATAmtLCY; VALVATAmountLCY)
                        {
                            AutoFormatType = 1;
                        }
                        column(VATAmtLineVAT1; TempVATAmountLine."VAT %")
                        {
                            DecimalPlaces = 0 : 5;
                        }
                        column(VATAmtLineVATIdentifier1; TempVATAmountLine."VAT Identifier")
                        {
                        }

                        trigger OnAfterGetRecord()
                        begin
                            TempVATAmountLine.GetLine(Number);
                            VALVATBaseLCY :=
                              TempVATAmountLine.GetBaseLCY(
                                "Sales Cr.Memo Header"."Posting Date", "Sales Cr.Memo Header"."Currency Code",
                                "Sales Cr.Memo Header"."Currency Factor");
                            VALVATAmountLCY :=
                              TempVATAmountLine.GetAmountLCY(
                                "Sales Cr.Memo Header"."Posting Date", "Sales Cr.Memo Header"."Currency Code",
                                "Sales Cr.Memo Header"."Currency Factor");
                        end;

                        trigger OnPreDataItem()
                        begin
                            IF (NOT GLSetup."Print VAT specification in LCY") OR
                               ("Sales Cr.Memo Header"."Currency Code" = '')
                            THEN
                                CurrReport.BREAK();

                            SETRANGE(Number, 1, TempVATAmountLine.COUNT);

                            IF GLSetup."LCY Code" = '' THEN
                                VALSpecLCYHeader := Text007Lbl + Text008Lbl
                            ELSE
                                VALSpecLCYHeader := Text007Lbl + FORMAT(GLSetup."LCY Code");

                            CurrExchRate.FindCurrency("Sales Cr.Memo Header"."Posting Date", "Sales Cr.Memo Header"."Currency Code", 1);
                            CalculatedExchRate := ROUND(1 / "Sales Cr.Memo Header"."Currency Factor" * CurrExchRate."Exchange Rate Amount", 0.000001);
                            VALExchRate := STRSUBSTNO(Text009Lbl, CalculatedExchRate, CurrExchRate."Exchange Rate Amount");
                        end;
                    }
                    dataitem(Total; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(SelltoCustNo_SalesInvHdr; "Sales Cr.Memo Header"."Sell-to Customer No.")
                        {
                        }
                        column(SelltoCustNo_SalesInvHdrCaption; "Sales Cr.Memo Header".FIELDCAPTION("Sell-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem()
                        begin
                            IF NOT ShowShippingAddr THEN
                                CurrReport.BREAK();
                        end;
                    }
                    dataitem(RIB; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(BillDocumentCaption; BillDocumentCaption())
                        {
                        }
                        column(DisplayRib; DisplayRib)
                        {
                        }
                        column(CustAdr_6_; CustAddr[6])
                        {
                        }
                        column(CustAdr_7_; CustAddr[7])
                        {
                        }
                        column(CustAdr_5_; CustAddr[5])
                        {
                        }
                        column(CustAdr_4_; CustAddr[4])
                        {
                        }
                        column(CustAdr_2_; CustAddr[2])
                        {
                        }
                        column(CustAdr_3_; CustAddr[3])
                        {
                        }
                        column(CustAdr_1_; CustAddr[1])
                        {
                        }
                        column(Payment_Line__Agency_Code_; CustomerBankAccount."Agency Code")
                        {
                        }
                        column(Payment_Line__Bank_Branch_No__; CustomerBankAccount."Bank Branch No.")
                        {
                        }
                        column(Payment_Line__Bank_Account_No__; CustomerBankAccount."Bank Account No.")
                        {
                        }
                        column(CONVERTSTR_FORMAT__RIB_Key__2_______0__; FORMAT(CustomerBankAccount."RIB Key"))
                        {
                        }
                        column(Payment_Line__Bank_Account_Name_; CustomerBankAccount.Name)
                        {
                        }
                        column(FORMAT_Amount_0___Precision_2___Standard_Format_0___; '***' + FORMAT(TotalAmountInclVAT, 0, '<Precision,2:><Standard Format,0>') + '***')
                        {
                        }
                        column(FORMAT_Amount_0___Precision_2___Standard_Format_0____Control1120051; '***' + FORMAT(TotalAmountInclVAT, 0, '<Precision,2:><Standard Format,0>') + '***')
                        {
                        }
                        column(Payment_Line__Drawee_Reference_; '')
                        {
                        }
                        column(IssueCity; IssueCity)
                        {
                        }
                        column(IssueDate; FORMAT(IssueDate))
                        {
                        }
                        column(Payment_Line__Due_Date_; FORMAT("Sales Cr.Memo Header"."Due Date"))
                        {
                        }
                        column(Payment_Line__Bank_City_; CustomerBankAccount.City)
                        {
                        }
                        column(PostingDate; FORMAT(PostingDate))
                        {
                        }
                        column(AmountText; AmountText)
                        {
                        }
                        column(FORMAT_AmountText_; FORMAT(AmountText))
                        {
                        }
                        column(ACCEPTANCE_or_ENDORSMENTCaption; ACCEPTANCE_or_ENDORSMENTCaptionLbl)
                        {
                        }
                        column(of_DRAWEECaption; of_DRAWEECaptionLbl)
                        {
                        }
                        column(Stamp_Allow_and_SignatureCaption; Stamp_Allow_and_SignatureCaptionLbl)
                        {
                        }
                        column(ADDRESSCaption; ADDRESSCaptionLbl)
                        {
                        }
                        column(NAME_andCaption; NAME_andCaptionLbl)
                        {
                        }
                        column(Value_in__Caption; Value_in__CaptionLbl)
                        {
                        }
                        column(DRAWEE_R_I_B_Caption; DRAWEE_R_I_B_CaptionLbl)
                        {
                        }
                        column(DOMICILIATIONCaption; DOMICILIATIONCaptionLbl)
                        {
                        }
                        column(TOCaption; TOCaptionLbl)
                        {
                        }
                        column(ONCaption; ONCaptionLbl)
                        {
                        }
                        column(AMOUNT_FOR_CONTROLCaption; AMOUNT_FOR_CONTROLCaptionLbl)
                        {
                        }
                        column(CREATION_DATECaption; CREATION_DATECaptionLbl)
                        {
                        }
                        column(DUE_DATECaption; DUE_DATECaptionLbl)
                        {
                        }
                        column(DRAWEE_REF_Caption; DRAWEE_REF_CaptionLbl)
                        {
                        }
                        column(BILLCaption; BILLCaptionLbl)
                        {
                        }
                        column(CODE_ETABLCaption; CODE_ETABLCaptionLbl)
                        {
                        }
                        column(CODE_GUICHETCaption; CODE_GUICHETCaptionLbl)
                        {
                        }
                        column(NO_DE_COMPTECaption; NO_DE_COMPTECaptionLbl)
                        {
                        }
                        column(CLE_R_I_B_Cpation; CLE_R_I_B_CpationLbl)
                        {
                        }
                    }

                    trigger OnAfterGetRecord()
                    begin
                        NombredeNoseerie := 0;
                    end;
                }

                trigger OnAfterGetRecord()
                begin
                    IF Number > 1 THEN BEGIN
                        CopyText := Text003Lbl;
                        OutputNo += 1;
                    END;
                    NombreDeLigneImprimer := 0;

                    TotalSubTotal := 0;
                    TotalInvoiceDiscountAmount := 0;
                    TotalAmount := 0;
                    TotalAmountVAT := 0;
                    TotalAmountInclVAT := 0;
                    TotalPaymentDiscountOnVAT := 0;

                    MontantComm := 0;
                end;

                trigger OnPostDataItem()
                begin
                    IF NOT CurrReport.PREVIEW THEN
                        SalesCountPrinted.RUN("Sales Cr.Memo Header");
                end;

                trigger OnPreDataItem()
                begin
                    NoOfLoops := ABS(NoOfCopies) + Cust."Invoice Copies" + 1;
                    IF NoOfLoops <= 0 THEN
                        NoOfLoops := 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord()
            var
                LPaymentMethod: Record "Payment Method";
                LClientLivre: Record Customer;
                _SalesInvoiceLine: Record "Sales Cr.Memo Line";
                recPaymentMethod: Record "Payment Method";
            begin
                // AD Le 26-05-2014
                _SalesInvoiceLine.SETRANGE("Document No.", "No.");
                IF _SalesInvoiceLine.FINDFIRST() THEN;
                TxtClientLivre := _SalesInvoiceLine."N° client Livré";
                TxtClientfacture := "Sales Cr.Memo Header"."Bill-to Customer No.";
                AddrCaption := ShiptoAddrCaptionLbl;

                IF Cust.GET("Sales Cr.Memo Header"."Bill-to Customer No.") THEN
                    IF Cust."Inverser Adresses Factures" THEN BEGIN
                        TxtClientLivre := "Sales Cr.Memo Header"."Bill-to Customer No.";
                        TxtClientfacture := _SalesInvoiceLine."N° client Livré";
                        AddrCaption := BillToAddrCaptionLbl;

                    END;

                IF TxtClientLivre = '' THEN
                    TxtClientLivre := "Sales Cr.Memo Header"."Sell-to Customer No.";
                // FIN AD Le 26-05-2014


                //pour la memoire


                FormatAddr.SalesCrMemoBillTo(CustAddr, "Sales Cr.Memo Header");

                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");


                IF RespCenter.GET("Responsibility Center") THEN BEGIN
                    FormatAddr.RespCenter(CompanyAddr, RespCenter);
                    CompanyInfo."Phone No." := RespCenter."Phone No.";
                    CompanyInfo."Fax No." := RespCenter."Fax No.";
                    IssueCity := RespCenter.City;
                END ELSE BEGIN
                    FormatAddr.Company(CompanyAddr, CompanyInfo);
                    IssueCity := CompanyInfo.City;
                END;

                // MC Le 23-11-2011 => Si client Comptoir alors adresse de livraison = adresse de facturation
                IF LClientLivre.GET("Sales Cr.Memo Header"."Sell-to Customer No.") THEN
                    IF LClientLivre."Client Comptoir" THEN
                        FormatAddr.SalesCrMemoShipTo(CustAddr, CustAddr, "Sales Cr.Memo Header");
                // FIN MC Le 23-11-2011



                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                //IF "Order No." = '' THEN
                //  OrderNoText := ''
                //ELSE
                //  OrderNoText := FIELDCAPTION("Order No.");
                IF "Salesperson Code" = '' THEN BEGIN
                    SalesPurchPerson.INIT();
                    SalesPersonText := '';
                END ELSE BEGIN
                    IF SalesPurchPerson.GET("Salesperson Code") THEN;
                    SalesPersonText := Text000Lbl;
                END;
                IF "Your Reference" = '' THEN
                    ReferenceText := ''
                ELSE
                    ReferenceText := FIELDCAPTION("Your Reference");
                IF "VAT Registration No." = '' THEN
                    VATNoText := ''
                ELSE
                    VATNoText := FIELDCAPTION("VAT Registration No.");
                IF "Currency Code" = '' THEN BEGIN
                    GLSetup.TESTFIELD("LCY Code");
                    TotalText := STRSUBSTNO(Text001Lbl, GLSetup."LCY Code");
                    TotalInclVATText := STRSUBSTNO(Text002Lbl, GLSetup."LCY Code");
                    TotalExclVATText := STRSUBSTNO(Text006Lbl, GLSetup."LCY Code");
                END ELSE BEGIN
                    TotalText := STRSUBSTNO(Text001Lbl, "Currency Code");
                    TotalInclVATText := STRSUBSTNO(Text002Lbl, "Currency Code");
                    TotalExclVATText := STRSUBSTNO(Text006Lbl, "Currency Code");
                END;



                IF NOT Cust.GET("Bill-to Customer No.") THEN
                    CLEAR(Cust);

                IF "Payment Terms Code" = '' THEN
                    PaymentTerms.INIT()
                ELSE BEGIN
                    IF PaymentTerms.GET("Payment Terms Code") THEN;
                    PaymentTerms.TranslateDescription(PaymentTerms, "Language Code");
                END;
                IF "Shipment Method Code" = '' THEN
                    ShipmentMethod.INIT()
                ELSE BEGIN
                    IF ShipmentMethod.GET("Shipment Method Code") THEN;
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                END;
                FormatAddr.SalesCrMemoShipTo(ShipToAddr, ShipToAddr, "Sales Cr.Memo Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                    IF ShipToAddr[i] <> CustAddr[i] THEN
                        ShowShippingAddr := TRUE;



                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN BEGIN
                        IF "Bill-to Contact No." <> '' THEN
                            SegManagement.LogDocument(
                              4, "No.", 0, 0, DATABASE::Contact, "Bill-to Contact No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '')
                        ELSE
                            SegManagement.LogDocument(
                              4, "No.", 0, 0, DATABASE::Customer, "Bill-to Customer No.", "Salesperson Code",
                              "Campaign No.", "Posting Description", '');
                    END;



                BaseLivraison := STRSUBSTNO(ESK002Lbl, "Sales Cr.Memo Header"."Ship-to Name", "Sales Cr.Memo Header"."Ship-to City");


                CLEAR(LPaymentMethod);
                IF NOT LPaymentMethod.GET("Sales Cr.Memo Header"."Payment Method Code") THEN
                    LPaymentMethod.Description := "Sales Cr.Memo Header"."Payment Method Code";
                PaiementTermsMethod := LPaymentMethod.Description; // STRSUBSTNO(ESK003,LPaymentMethod.Description, PaymentTerms.Description);

                // FIN AD Le 14-01-2014

                IF "Currency Code" = '' THEN
                    AmountText := Text011Lbl + ' €'
                ELSE
                    AmountText := Text011Lbl + ' ' + "Currency Code";

                PostingDate := "Sales Cr.Memo Header"."Posting Date";
                IssueDate := "Sales Cr.Memo Header"."Posting Date";

                IF NOT CustomerBankAccount.GET(Cust."No.", Cust."Preferred Bank Account Code") THEN
                    CLEAR(CustomerBankAccount);

                ////DisplayRib := CustomerBankAccount."LCR NA" AND NOT TauxCommFilter;
                DisplayRib := LPaymentMethod."Editer Traite" AND NOT TauxCommFilter;


                //IF "Sales Cr.Memo Header"."Shipping Agent Code" = '' THEN
                //  RecTransporteur.INIT
                //ELSE
                //  RecTransporteur.GET ("Sales Cr.Memo Header"."Shipping Agent Code");

                IsExport := CompanyInfo."Country/Region Code" <> "Sales Cr.Memo Header"."Bill-to Country/Region Code";



                "Rec_Client-central".INIT();
                Texte_Central := '';
                IF "Rec_Client-central".GET("Sales Cr.Memo Header"."Centrale Active") AND "Regroupement Centrale" THEN
                    Texte_Central := 'Centrale : ' + "Rec_Client-central".Name;


                IF NOT recPaymentMethod.GET("Sales Cr.Memo Header"."Payment Method Code") THEN
                    recPaymentMethod.Description := "Sales Cr.Memo Header"."Payment Method Code";

                TextPaiement := 'REGLEMENT PAR ' + recPaymentMethod.Description + ' au ' + FORMAT("Sales Cr.Memo Header"."Due Date");
            end;

            trigger OnPreDataItem()
            begin
                // AD Le 14-01-2014
                FILTERGROUP(1);
                ////SETRANGE("company code",managementCompany.CodeSociétéEnCours);
                FILTERGROUP(0);
                // FIN AD Le 14-01-2014

                //"Sales Cr.Memo Header".SETCURRENTKEY("Salesperson Code");
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        Caption = 'No. of Copies';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. of Copies field.';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        Caption = 'Show Internal Information';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Internal Information field.';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Log Interaction field.';
                    }
                    field(IncludeShipmentNo; IncludeShptNo)
                    {
                        Caption = 'Include Shipment No.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Include Shipment No. field.';
                    }
                    field(DisplayAsmInformation; DisplayAssemblyInformation)
                    {
                        Caption = 'Show Assembly Components';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Assembly Components field.';
                    }
                    field("Imprimer logo sociétè"; _ImprimerLogo)
                    {
                        Caption = 'Imprimer logo sociétè';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Imprimer logo sociétè field.';
                    }
                    field("Forcer Affichage à 'Facture'"; "Forcer Affichage à 'Facture'")
                    {
                        Caption = 'Forcer Affichage à ''Facture''';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Forcer Affichage à ''Facture'' field.';
                    }
                    field(TauxCommFilter; TauxCommFilter)
                    {
                        Caption = 'Taux de comm.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Taux de comm. field.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnInit()
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage()
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;
        end;
    }

    labels
    {
        Datefacture = 'Date de facture';
        Dateéchéance = 'Date d''échéance';
        phraseMultiPaiment = 'Attention les traites suivantes ne seront pas rapellées dans les relevés.';
        PhraseReglementaire = 'Pas d''escompte pour paiement anticipé. Tva  acquittée sur les débits. Toute somme non payée à l''échéance entraine l''application de pénalités d''un montant égal à 1.5 fois le taux de l''intérêt légal, et s''y ajoute le versement d''une indemnité de frais de recouvrement de 40€ht.';
        NoCommande = 'Commande';
        Interlocuteur = 'Votre interlocuteur';
        Against_this_BILL_noted_as_NO_CHARGES_please_pay_the_indicated_sum_below_for_order_ofCaption = 'Against this BILL noted as NO CHARGES Please pay the indicated sum below for order of :';
        PrixTailleCaption = 'Prix taille';
        TariffNoCaption = 'Nomenclature douanière';
    }

    trigger OnInitReport()
    begin
        GLSetup.GET();
        CompanyInfo.GET();
        SalesSetup.GET();

        NombredelignepossibleparPage := 25;
        NombredelignepossibleparPagePleine := 39;

        _ImprimerLogo := TRUE;
        _ImprimerLogo := FALSE; // AD Le 07-01-2016 => Demande de Bernard -> Ticket 7106
    end;

    trigger OnPreReport()
    begin

        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
    end;

    var
        GLSetup: Record "General Ledger Setup";
        ShipmentMethod: Record "Shipment Method";
        PaymentTerms: Record "Payment Terms";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        CompanyInfo1: Record "Company Information";
        CurrExchRate: Record "Currency Exchange Rate";
        SalesSetup: Record "Sales & Receivables Setup";
        TempTrackingSpecBuffer: Record "Item Ledger Entry" temporary;
        Cust: Record Customer;
        "Rec_Client-central": Record Customer;
        RecTransporteur: Record "Shipping Agent";
        CustomerBankAccount: Record "Customer Bank Account";
        ShipmentInvoiced: Record "Shipment Invoiced";
        [SecurityFiltering(SecurityFilter::Ignored)]
        TempBufferPaiment: Record "Payment Post. Buffer" temporary;
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        DimSetEntry1: Record "Dimension Set Entry";
        DimSetEntry2: Record "Dimension Set Entry";
        RespCenter: Record "Responsibility Center";
        TempSalesShipmentBuffer: Record "Sales Shipment Buffer" temporary;
        Entete_retour: Record "Return Receipt Header";
        TempPostedAsmLine: Record "Posted Assembly Line" temporary;
        VATClause: Record "VAT Clause";
        SalesCountPrinted: Codeunit "Sales Cr. Memo-Printed";
        FormatAddr: Codeunit "Format Address";
        SegManagement: Codeunit SegManagement;
        GestionMulti: Codeunit "Gestion Multi-référence";
        EskapeCommunication: Codeunit "Eskape Communication";
        GestionEdition: Codeunit "Gestion Info Editions";
        Language_G: Codeunit Language;
        PostedShipmentDate: Date;
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        AddrTMP: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        OrderNoText: Text[80];
        SalesPersonText: Text[30];
        VATNoText: Text[80];
        ReferenceText: Text[80];
        TotalText: Text[50];
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        MoreLines: Boolean;
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        ShowShippingAddr: Boolean;
        i: Integer;
        NextEntryNo: Integer;
        FirstValueEntryNo: Integer;
        DimText: Text[120];
        OldDimText: Text[120];
        ShowInternalInfo: Boolean;
        Continue: Boolean;
        LogInteraction: Boolean;
        VALVATBaseLCY: Decimal;
        VALVATAmountLCY: Decimal;
        VALSpecLCYHeader: Text[80];
        Text000Lbl: Label 'Salesperson';
        Text001Lbl: Label 'Total %1', Comment = '%1 = %1 = Currency';
        Text002Lbl: Label 'Total %1 Incl. VAT', Comment = '%1 = Currency';
        Text003Lbl: Label 'COPY';
        //   Text004Lbl: Label 'Sales - Invoice %1' , Comment = '%1 = ';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Page NB ; %2 = Total PAge Nb';
        Text006Lbl: Label 'Total %1 Excl. VAT', Comment = '%1 = Currency';
        Text007Lbl: Label 'VAT Amount Specification in ';
        Text008Lbl: Label 'Local Currency';
        VALExchRate: Text[50];
        Text009Lbl: Label 'Exchange rate: %1/%2', Comment = '%1 = Taux de Change ; %2 = Montant de Taux de change ';
        CalculatedExchRate: Decimal;
        // Text010Lbl: Label 'Sales - Prepayment Invoice %1' , Comment = '' ;
        OutputNo: Integer;
        TotalSubTotal: Decimal;
        TotalAmount: Decimal;
        TotalAmountInclVAT: Decimal;
        TotalAmountVAT: Decimal;
        TotalInvoiceDiscountAmount: Decimal;
        TotalPaymentDiscountOnVAT: Decimal;
        Text10800Lbl: Label 'ShipmentNo';
        NoShipmentNumLoop: Integer;
        NoShipmentDatas: array[3] of Text[20];
        NoShipmentText: Text[30];
        IncludeShptNo: Boolean;
        GetTotalLineAmount: Decimal;
        GetTotalInvDiscAmount: Decimal;
        GetTotalAmount: Decimal;
        GetTotalAmountIncVAT: Decimal;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Registration No.';
        ParamTxt: Label '%1, %2 %3', Comment = '%1; %2; %3';
        Param1Txt: Label '%1 %2', Comment = '%1; %2';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        DueDateCaptionLbl: Label 'Due Date';
        InvoiceNoCaptionLbl: Label 'Invoice No.';
        PostingDateCaptionLbl: Label 'Posting Date';
        HdrDimsCaptionLbl: Label 'Header Dimensions';
        UnitPriceCaptionLbl: Label 'Unit Price';
        DiscPercentCaptionLbl: Label 'Discount %';
        AmtCaptionLbl: Label 'Amount';
        VATClausesCapLbl: Label 'VAT Clause';
        PostedShpDateCaptionLbl: Label 'Posted Shipment Date';
        InvDiscAmtCaptionLbl: Label 'Inv. Discount Amount';
        SubtotalCaptionLbl: Label 'Subtotal';
        PmtDiscVATCaptionLbl: Label 'Payment Discount on VAT';
        ShpCaptionLbl: Label 'Shipment';
        LineDimsCaptionLbl: Label 'Line Dimensions';
        VATPercentCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        VATIdentCaptionLbl: Label 'VAT Identifier';
        InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
        LineAmtCaptionLbl: Label 'Line Amount';
        InvDiscAmtCaption1Lbl: Label 'Invoice Discount Amount';
        TotalCaptionLbl: Label 'Total';
        ShiptoAddrCaptionLbl: Label 'Ship-to Address';
        PmtTermsDescCaptionLbl: Label 'Payment Terms';
        ShpMethodDescCaptionLbl: Label 'Shipment Method';
        HomePageCaptionCapLbl: Label 'Home Page';
        EMailCaptionLbl: Label 'E-Mail';
        DocDateCaptionLbl: Label 'Document Date';
        NombredelignepossibleparPage: Integer;
        NombreDeLigneImprimer: Integer;
        NombredelignepossibleparPagePleine: Integer;

        //ItemTrackingMgt: Codeunit "Item Tracking Management";
        TrackingSpecCount: Integer;
        /*   ShowTotal: Boolean;
          ItemTrackingAppendix: Report "Item Tracking Appendix";
          ShowGroup: Boolean; */
        NombredeNoseerie: Integer;
        "CoordonnéesBancaires": Text[1024];
        //  ESK001: Label 'Nos coordonnées bancaires : %2 -  IBAN : %1 - SWIFT : %3';
        OrderNoLbl: Label 'Commande';
        BaseLivraison: Text[250];
        ESK002Lbl: Label '*** Livraison à : %1 - %2 ***', Comment = '%1 = Ship-to Name; %2 = Ship-to City ';
        PaiementTermsMethod: Text[1024];
        //  ESK003: Label 'Par %1 à %2';
        "Forcer Affichage à 'Facture'": Boolean;
        //  ESK004: Label 'Votre interlocuteur : %1';
        Desc_SalesInvLine: Text[150];
        ItemNo: Code[20];
        //   RecCustomer: Record Customer;
        SellToAddr: array[8] of Text[50];
        SelltoAddrCaptionLbl: Label 'Commandé par';
        Text011Lbl: Label 'Amount';
        ACCEPTANCE_or_ENDORSMENTCaptionLbl: Label 'ACCEPTANCE or ENDORSMENT';
        of_DRAWEECaptionLbl: Label 'of DRAWEE', Comment = 'NAME and ADDRESS of DRAWEE';
        Stamp_Allow_and_SignatureCaptionLbl: Label 'Stamp Allow and Signature';
        ADDRESSCaptionLbl: Label 'ADDRESS', Comment = 'Translate address and uppecase the result';
        NAME_andCaptionLbl: Label 'NAME and';
        Value_in__CaptionLbl: Label 'Value in :';
        DRAWEE_R_I_B_CaptionLbl: Label 'DRAWEE R.I.B.';
        DOMICILIATIONCaptionLbl: Label 'DOMICILIATION', Comment = 'Translate domiciliation and uppecase the result';
        TOCaptionLbl: Label 'TO';
        ONCaptionLbl: Label 'ON';
        AMOUNT_FOR_CONTROLCaptionLbl: Label 'AMOUNT FOR CONTROL';
        CREATION_DATECaptionLbl: Label 'CREATION DATE';
        DUE_DATECaptionLbl: Label 'DUE DATE';
        DRAWEE_REF_CaptionLbl: Label 'DRAWEE REF.';
        BILLCaptionLbl: Label 'BILL';
        IssueCity: Text[30];
        IssueDate: Date;
        AmountText: Text[30];
        //Amount: Decimal;
        PostingDate: Date;
        CODE_ETABLCaptionLbl: Label 'Code Etabl.';
        CODE_GUICHETCaptionLbl: Label 'Code guichet';
        NO_DE_COMPTECaptionLbl: Label 'N° de compte';
        CLE_R_I_B_CpationLbl: Label 'Clé RIB';

        NoSerieSpecialCaptionLbl: Label 'SERIE';

        ShippingAgentNameCaptionLbl: Label 'Transporteur';
        TauxCommFilter: Boolean;
        TauxComm: Text[20];
        DisplayRib: Boolean;
        txtEan13Compo: array[5] of Text[13];
        PrixTaille: array[5] of Decimal;
        // TotalTaille: Decimal;
        TariffNo: Code[20];
        IsExport: Boolean;
        TvaCaptionLbl: Label 'Tva';
        TauxCommCaption: Text[20];
        // ProduitCatalogueLbl: Label 'CATALOG PRODUCT';
        MontantComm: Decimal;
        MontantCommCaptionLbl: Label 'Montant comm.';
        ReferenceActive: Text;

        //ReportUtils: Codeunit ReportFunctions;
        "Cumul_client_livré": Decimal;
        "Code_client_livré": Code[20];
        textSIEGSOC: array[5] of Text[250];

        //  _AfficheInfoSymta: Boolean;

        Commentaire_BL: Text;
        LastShipmentNo: Code[20];
        TxtRemise: Text[20];
        _ImprimerLogo: Boolean;
        TxtClientLivre: Text[50];
        TxtClientfacture: Text[50];

        TextPaiement: Text[150];

        Texte_Central: Text[100];
        BillToAddrCaptionLbl: Label 'Adresse de facturation';
        AddrCaption: Text;

    procedure InitLogInteraction()
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(4)) <> '';
    end;

    procedure GenerateBufferFromValueEntry(SalesInvoiceLine2: Record "Sales Invoice Line")
    var
        ValueEntry: Record "Value Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        TotalQuantity: Decimal;
        Quantity: Decimal;
    begin
        TotalQuantity := SalesInvoiceLine2."Quantity (Base)";
        ValueEntry.SETCURRENTKEY("Document No.");
        ValueEntry.SETRANGE("Document No.", SalesInvoiceLine2."Document No.");
        ValueEntry.SETRANGE("Posting Date", "Sales Cr.Memo Header"."Posting Date");
        ValueEntry.SETRANGE("Item Charge No.", '');
        ValueEntry.SETFILTER("Entry No.", '%1..', FirstValueEntryNo);
        IF ValueEntry.FIND('-') THEN
            REPEAT
                IF ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") THEN BEGIN
                    IF SalesInvoiceLine2."Qty. per Unit of Measure" <> 0 THEN
                        Quantity := ValueEntry."Invoiced Quantity" / SalesInvoiceLine2."Qty. per Unit of Measure"
                    ELSE
                        Quantity := ValueEntry."Invoiced Quantity";
                    AddBufferEntry(
                      SalesInvoiceLine2,
                      -Quantity,
                      ItemLedgerEntry."Posting Date");
                    TotalQuantity := TotalQuantity + ValueEntry."Invoiced Quantity";
                END;
                FirstValueEntryNo := ValueEntry."Entry No." + 1;
            UNTIL (ValueEntry.NEXT() = 0) OR (TotalQuantity = 0);
    end;

    procedure CorrectShipment(var SalesShipmentLine: Record "Sales Shipment Line")
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
    begin
        SalesInvoiceLine.SETCURRENTKEY("Shipment No.", "Shipment Line No.");
        SalesInvoiceLine.SETRANGE("Shipment No.", SalesShipmentLine."Document No.");
        SalesInvoiceLine.SETRANGE("Shipment Line No.", SalesShipmentLine."Line No.");
        IF SalesInvoiceLine.FIND('-') THEN
            REPEAT
                SalesShipmentLine.Quantity := SalesShipmentLine.Quantity - SalesInvoiceLine.Quantity;
            UNTIL SalesInvoiceLine.NEXT() = 0;
    end;

    procedure AddBufferEntry(SalesInvoiceLine: Record "Sales Invoice Line"; QtyOnShipment: Decimal; PostingDate: Date)
    begin
        TempSalesShipmentBuffer.SETRANGE("Document No.", SalesInvoiceLine."Document No.");
        TempSalesShipmentBuffer.SETRANGE("Line No.", SalesInvoiceLine."Line No.");
        TempSalesShipmentBuffer.SETRANGE("Posting Date", PostingDate);
        IF TempSalesShipmentBuffer.FIND('-') THEN BEGIN
            TempSalesShipmentBuffer.Quantity := TempSalesShipmentBuffer.Quantity + QtyOnShipment;
            TempSalesShipmentBuffer.MODIFY();
            EXIT;
        END;

        WITH TempSalesShipmentBuffer DO BEGIN
            "Document No." := SalesInvoiceLine."Document No.";
            "Line No." := SalesInvoiceLine."Line No.";
            "Entry No." := NextEntryNo;
            Type := SalesInvoiceLine.Type;
            "No." := SalesInvoiceLine."No.";
            Quantity := QtyOnShipment;
            "Posting Date" := PostingDate;
            INSERT();
            NextEntryNo := NextEntryNo + 1
        END;
    end;

    local procedure DocumentCaption() _Result: Text[250]
    var
        tmp_SalesInvoiceHeader: Record "Sales Invoice Header";
        Text50001Lbl: Label 'Facture N° %1', Comment = '%1 =Avoir No';
        Text50002Lbl: Label 'DUPLICATA FACTURE N° %1', Comment = '%1 =Avoir No';
    begin
        IF tmp_SalesInvoiceHeader.GET("Sales Cr.Memo Header"."No.") THEN;

        //IF PageLoop.Number>1 THEN
        // tmp_SalesInvoiceHeader."No. Printed":=1;

        //if UPPERCASE(userID) =  UPPERCASE('capalliance\eskape')  then
        //message('%1',  PageLoop.Number);

        // AD Le 05-01-2014 => SAVEBAG -> Demande de Mme Bertrand
        "Forcer Affichage à 'Facture'" := TRUE;


        IF tmp_SalesInvoiceHeader."No. Printed" = 0 THEN
            _Result := STRSUBSTNO(Text50001Lbl, "Sales Cr.Memo Header"."No.")
        ELSE
            _Result := STRSUBSTNO(Text50002Lbl, "Sales Cr.Memo Header"."No.");

        IF "Forcer Affichage à 'Facture'" THEN
            _Result := STRSUBSTNO(Text50001Lbl, "Sales Cr.Memo Header"."No.");

        EXIT(_Result);

        /*
        IF "Sales Cr.Memo Header"."Prepayment Invoice" THEN
          EXIT(Text010);
        EXIT(Text004);
         */

    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; IncludeShptNo: Boolean; DisplAsmInfo: Boolean)
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        IncludeShptNo := IncludeShptNo;
        DisplayAssemblyInformation := DisplAsmInfo;
    end;

    procedure CollectAsmInformation()
    var
        ValueEntry: Record "Value Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        PostedAsmHeader: Record "Posted Assembly Header";
        PostedAsmLine: Record "Posted Assembly Line";
        SalesShipmentLine: Record "Sales Shipment Line";
    begin
        TempPostedAsmLine.DELETEALL();
        IF "Sales Cr.Memo Line".Type <> "Sales Cr.Memo Line".Type::Item THEN
            EXIT;
        WITH ValueEntry DO BEGIN
            SETCURRENTKEY("Document No.");
            SETRANGE("Document No.", "Sales Cr.Memo Line"."Document No.");
            SETRANGE("Document Type", "Document Type"::"Sales Invoice");
            SETRANGE("Document Line No.", "Sales Cr.Memo Line"."Line No.");
            IF NOT FINDSET() THEN
                EXIT;
        END;
        REPEAT
            IF ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") THEN BEGIN
                IF ItemLedgerEntry."Document Type" = ItemLedgerEntry."Document Type"::"Sales Shipment" THEN BEGIN
                    SalesShipmentLine.GET(ItemLedgerEntry."Document No.", ItemLedgerEntry."Document Line No.");
                    IF SalesShipmentLine.AsmToShipmentExists(PostedAsmHeader) THEN BEGIN
                        PostedAsmLine.SETRANGE("Document No.", PostedAsmHeader."No.");
                        IF PostedAsmLine.FINDSET() THEN
                            REPEAT
                                TreatAsmLineBuffer(PostedAsmLine);
                            UNTIL PostedAsmLine.NEXT() = 0;
                    END;
                END;
            END;
        UNTIL ValueEntry.NEXT() = 0;
    end;

    procedure TreatAsmLineBuffer(PostedAsmLine: Record "Posted Assembly Line")
    begin
        CLEAR(TempPostedAsmLine);
        TempPostedAsmLine.SETRANGE(Type, PostedAsmLine.Type);
        TempPostedAsmLine.SETRANGE("No.", PostedAsmLine."No.");
        TempPostedAsmLine.SETRANGE("Variant Code", PostedAsmLine."Variant Code");
        TempPostedAsmLine.SETRANGE(Description, PostedAsmLine.Description);
        TempPostedAsmLine.SETRANGE("Unit of Measure Code", PostedAsmLine."Unit of Measure Code");
        IF TempPostedAsmLine.FINDFIRST() THEN BEGIN
            TempPostedAsmLine.Quantity += PostedAsmLine.Quantity;
            TempPostedAsmLine.MODIFY();
        END ELSE BEGIN
            CLEAR(TempPostedAsmLine);
            TempPostedAsmLine := PostedAsmLine;
            TempPostedAsmLine.Insert();
        END;
    end;

    procedure GetUOMText(UOMCode: Code[10]): Text[50]
    var
        UnitOfMeasure: Record "Unit of Measure";
    begin
        IF NOT UnitOfMeasure.GET(UOMCode) THEN
            EXIT(UOMCode);
        EXIT(UnitOfMeasure.Description);
    end;

    procedure BlanksForIndent(): Text[10]
    begin
        EXIT(PADSTR('', 2, ' '));
    end;

    procedure ChargerMultiEcheance()
    var
    /* NbDAteEch: Integer;
    TabDateEch: array[5] of Date;
    TabMontantEch: array[5] of Decimal;
    Index: Integer;
    "MontantDerniereEchéance": Decimal; */
    begin
        /*
        TempBufferPaiment.DELETEALL();
        IF NOT "Sales Cr.Memo Header"."Multi Echéance" THEN
        EXIT;
        WITH "Sales Cr.Memo Header" DO
        BEGIN
              NbDAteEch:=0;
              TabDateEch[1]:="Due Date";
              TabDateEch[2]:="Due date 2";
              TabDateEch[3]:="Due date 3";
              TabDateEch[4]:="Due date 4";
              TabDateEch[5]:="Due date 5";
        
              TabMontantEch[1] := ROUND("% Due 1" * ABS(TotalAmountInclVAT)/100,0.01);
              TabMontantEch[2] := ROUND("% Due 2" * ABS(TotalAmountInclVAT)/100,0.01);
              TabMontantEch[3] := ROUND("% Due 3" * ABS(TotalAmountInclVAT)/100,0.01);
              TabMontantEch[4] := ROUND("% Due 4" * ABS(TotalAmountInclVAT)/100,0.01);
              TabMontantEch[5] := ROUND("% Due 5" * ABS(TotalAmountInclVAT)/100,0.01);
        
             //Trouver le Nb D'echéance
              FOR Index:=1 TO 5 DO
               IF TabMontantEch[Index]<>0 THEN NbDAteEch:=Index;
        
        
            //faire en sorte que la sommes des échéances soit egal au TTC -> La différence doit ce faire a la derniere echéance
            MontantDerniereEchéance:=ABS(TotalAmountInclVAT);
        
            FOR Index:=1 TO (NbDAteEch-1) DO
               MontantDerniereEchéance:=MontantDerniereEchéance-TabMontantEch[Index];
        
           TabMontantEch[NbDAteEch] := MontantDerniereEchéance;
        
           //Création des lignes
           FOR  Index:=1 TO NbDAteEch DO
             BEGIN
              TempBufferPaiment."Due Date":=TabDateEch[Index];
              TempBufferPaiment.Amount:= TabMontantEch[Index];
              TempBufferPaiment.Insert();
             END;
          END;
        */

    end;

    procedure FormatRemise() retour: Text[20]
    begin
        IF "Sales Cr.Memo Line"."Line Discount %" = 0 THEN
            EXIT('');

        IF "Sales Cr.Memo Line"."Discount1 %" <> 0 THEN
            retour := FORMAT("Sales Cr.Memo Line"."Discount1 %");

        IF "Sales Cr.Memo Line"."Discount2 %" <> 0 THEN BEGIN
            IF retour <> '' THEN
                retour += '+';
            retour += FORMAT("Sales Cr.Memo Line"."Discount2 %");
        END;
    end;

    local procedure BillDocumentCaption() _Result: Text[250]
    var
        tmp_SalesInvoiceHeader: Record "Sales Invoice Header";
        Text50001Lbl: Label 'Traite sur facture %1', Comment = '%1 = Avoir No';
    begin
        IF tmp_SalesInvoiceHeader.GET("Sales Cr.Memo Header"."No.") THEN;

        _Result := STRSUBSTNO(Text50001Lbl, "Sales Cr.Memo Header"."No.");

        EXIT(_Result);
    end;
}