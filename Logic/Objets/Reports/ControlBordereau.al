report 50052 "Control Bordereau"
{
    caption = 'Control Bordereau';
    DefaultLayout = RDLC;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    RDLCLayout = 'Objets\Reports\Layouts\Control Bordereau.rdlc';

    dataset
    {
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("No.", "Due Date");
            RequestFilterFields = "No.";
            column(COMPANYNAME; COMPANYNAME)
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column(DocumentNo; "Document No.")
            {
            }
            column(ClientName; client.Name)
            {
            }
            column(CreditAmount; "Credit Amount")
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(DraweeReference; "Drawee Reference")
            {
            }
            column(BankBranchNo; "Bank Branch No.")
            {
            }
            column(BankAccountNo; "Bank Account No.")
            {
            }
            column(AgencyCode; "Agency Code")
            {
            }
            column(RIBKey; "RIB Key")
            {
            }
            column(AcceptationCode; "Acceptation Code")
            {
            }

            trigger OnAfterGetRecord();
            begin

                client.GET("Payment Line"."Account No.");
            end;

            trigger OnPreDataItem();
            begin
                LastFieldNo := FIELDNO("No.");
            end;
        }
    }
    var
        client: Record Customer;
        LastFieldNo: Integer;
}

