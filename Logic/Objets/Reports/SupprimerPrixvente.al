report 50028 "Supprimer Prix vente"
{
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    caption = 'Supprimer Prix vente';
    dataset
    {
        dataitem("Sales Price"; "Sales Price")
        {

            trigger OnAfterGetRecord();
            begin
                gRow += 1;
                DELETE(TRUE);
                IF (gRow MOD 10) = 0 THEN gDialog.UPDATE(1, gRow);
            end;

            trigger OnPreDataItem();
            begin
                "Sales Price".SETFILTER("Ending Date", '%1..%2', 19000101D, gDateFin);
            end;
        }
        dataitem("Sales Line Discount"; "Sales Line Discount")
        {

            trigger OnAfterGetRecord();
            begin
                gRow += 1;
                DELETE(TRUE);
                IF (gRow MOD 10) = 0 THEN gDialog.UPDATE(1, gRow);
            end;

            trigger OnPreDataItem();
            begin
                "Sales Line Discount".SETFILTER("Ending Date", '%1..%2', 19000101D, gDateFin);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(gDateFinName; gDateFin)
                {
                    Caption = 'Date fin';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date fin field.';
                }
            }
        }


    }



    trigger OnPostReport();
    begin
        gDialog.CLOSE();
    end;

    trigger OnPreReport();
    begin
        IF gDateFin = 0D THEN ERROR('Veuillez saisir une date de fin');
        IF NOT CONFIRM('Confirmer la suppression des tarifs ventes jusqu''au ' + FORMAT(gDateFin)) THEN ERROR('Suppression annulée');
        gDialog.OPEN('#1############ lignes supprimeés');
    end;

    var
        gDateFin: Date;
        gDialog: Dialog;
        gRow: Integer;
}

