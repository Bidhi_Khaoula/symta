report 50061 "Prix D'achat Net"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Prix D''achat Net.rdlc';


    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending);
            RequestFilterFields = "No.", "No. 2";
            column(No; "No.")
            {
            }
            column(No2; "No. 2")
            {
            }
            column(Description; Description + ' ' + "Description 2")
            {
            }
            column(PrixCatalogue; PrixCatalogue)
            {
            }
            dataitem(ItemVendorB1; "Item Vendor")
            {
                DataItemLink = "Item No." = FIELD("No.");
                DataItemTableView = SORTING("Vendor No.", "Item No.", "Variant Code")
                                    ORDER(Ascending);

                trigger OnAfterGetRecord();
                begin

                    IF (ItemVendorB1."Statut Qualité" = ItemVendorB1."Statut Qualité"::Conforme) AND
                       (ItemVendorB1."Statut Approvisionnement" = ItemVendorB1."Statut Approvisionnement"::"Non Bloqué") THEN BEGIN
                        TempStatArt.Amount := GetPrix(ItemVendorB1, 3);
                        TempStatArt."Item No." := ItemVendorB1."Vendor No.";
                        TempStatArt.Insert();
                    END
                    ELSE BEGIN
                        TempLStatArtNonConforme.Amount := GetPrix(ItemVendorB1, 3);
                        TempLStatArtNonConforme."Item No." := ItemVendorB1."Vendor No.";
                        TempLStatArtNonConforme.Insert();
                    END
                end;
            }
            dataitem(Conforme; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    ORDER(Ascending);
                column(VendorNo; ItemVendor."Vendor No.")
                {
                }
                column(VendorName; Vendor.Name)
                {
                }
                column(VendorItemNo; ItemVendor."Vendor Item No.")
                {
                }
                column(VendorStatutQualite; FORMAT(ItemVendor."Statut Qualité"))
                {
                }
                column(VendorStatutApprovisionnement; FORMAT(ItemVendor."Statut Approvisionnement"))
                {
                }
                column(TabPxNet_1; TabPxNet[1])
                {
                }
                column(Marge_1; Marge[1])
                {
                }
                column(TabPxNet_2; TabPxNet[2])
                {
                }
                column(Marge_2; Marge[2])
                {
                }
                column(TabPxNet_3; TabPxNet[3])
                {
                }
                column(Marge_3; Marge[3])
                {
                }
                column(TabPxNet_4; TabPxNet[4])
                {
                }
                column(Marge_4; Marge[4])
                {
                }

                trigger OnAfterGetRecord();
                begin

                    IF Conforme.Number = 1 THEN
                        TempStatArt.FINDFIRST()
                    ELSE
                        TempStatArt.NEXT();


                    ItemVendor.GET(TempStatArt."Item No.", Item."No.", '');
                    Vendor.GET(ItemVendor."Vendor No.");

                    RemplirTableau(ItemVendor);
                end;

                trigger OnPreDataItem();
                begin
                    Conforme.SETRANGE(Number, 1, TempStatArt.COUNT);
                end;
            }
            dataitem(NonConforme; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    ORDER(Ascending);
                column(VendorNo_NC; ItemVendor."Vendor No.")
                {
                }
                column(VendorName_NC; Vendor.Name)
                {
                }
                column(VendorItemNo_NC; ItemVendor."Vendor Item No.")
                {
                }
                column(VendorStatutQualite_NC; FORMAT(ItemVendor."Statut Qualité"))
                {
                }
                column(VendorStatutApprovisionnement_NC; FORMAT(ItemVendor."Statut Approvisionnement"))
                {
                }
                column(TabPxNet_1_NC; TabPxNet[1])
                {
                }
                column(Marge_1_NC; Marge[1])
                {
                }
                column(TabPxNet_2_NC; TabPxNet[2])
                {
                }
                column(Marge_2_NC; Marge[2])
                {
                }
                column(TabPxNet_3_NC; TabPxNet[3])
                {
                }
                column(Marge_3_NC; Marge[3])
                {
                }
                column(TabPxNet_4_NC; TabPxNet[4])
                {
                }
                column(Marge_4_NC; Marge[4])
                {
                }

                trigger OnAfterGetRecord();
                begin

                    IF NonConforme.Number = 1 THEN
                        TempLStatArtNonConforme.FINDFIRST()
                    ELSE
                        TempLStatArtNonConforme.NEXT();


                    ItemVendor.GET(TempLStatArtNonConforme."Item No.", Item."No.", '');
                    Vendor.GET(ItemVendor."Vendor No.");

                    RemplirTableau(ItemVendor);
                end;

                trigger OnPreDataItem();
                begin
                    NonConforme.SETRANGE(Number, 1, TempLStatArtNonConforme.COUNT);
                end;
            }

            trigger OnAfterGetRecord();
            var
                TempSalesPrice: Record "Sales Price" temporary;
                LCodeunitsFunctions: Codeunit "Codeunits Functions";
                LRemise: Decimal;
                LPrix: Decimal;

            begin

                IF LCodeunitsFunctions.GetUnitPrice(
                          TempSalesPrice, '', '', '',
                          '', Item."No.", '', Item."Base Unit of Measure", '', WORKDATE(), FALSE, 9999999, LRemise, LPrix,
                          '', '', 0) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
                THEN
                    PrixCatalogue := TempSalesPrice."Prix catalogue" * TempSalesPrice.Coefficient;
            end;
        }
    }
    var

        TempStatArt: Record "Item Amount" temporary;
        TempLStatArtNonConforme: Record "Item Amount" temporary;
        ItemVendor: Record "Item Vendor";
        Vendor: Record Vendor;
        i: Integer;
        TabPxNet: array[4] of Decimal;
        PrixCatalogue: Decimal;
        Marge: array[4] of Decimal;

    procedure GetPrix(_pItemVendor: Record "Item Vendor"; _pNiveauCde: Integer): Decimal;
    var
        TempLPurchPrice: Record "Purchase Price" temporary;
        TempLPurchLineDisc: Record "Purchase Line Discount" temporary;
        LItemUOM: Record "Item Unit of Measure";
        LCurrExchRate: Record "Currency Exchange Rate";
        LVendor: Record Vendor;
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        LRemise: Decimal;
        LPrix: Decimal;
    begin
        LVendor.GET(_pItemVendor."Vendor No.");

        LCodeunitsFunctions.GetUnitCost(TempLPurchPrice, _pItemVendor."Vendor No.", _pItemVendor."Item No.",
            _pItemVendor."Variant Code", _pItemVendor."Purch. Unit of Measure", LVendor."Currency Code",
            // MCO Le 01-04-2015 => Régie
            // Modif sinon ne filtre pas sur la date de début
            //WORKDATE, TRUE, 9999999999999.0, '', LPrix);
            WORKDATE(), FALSE, 9999999999999.0, '', LPrix);
        // FIN MCO Le 01-04-2015 => Régie

        // AD Le 10-04-2012 => Gestion Devise
        IF LVendor."Currency Code" <> '' THEN BEGIN

            LPrix := ROUND(
                        LCurrExchRate.ExchangeAmtFCYToLCY(WORKDATE(), LVendor."Currency Code",
                        LPrix,
                        LCurrExchRate.ExchangeRate(WORKDATE(), LVendor."Currency Code")))


        END;
        // FIN AD Le 10-04-2012


        LRemise := LCodeunitsFunctions.GetPurchDisc(TempLPurchLineDisc, _pItemVendor."Vendor No.", _pItemVendor."Item No.",
            _pItemVendor."Variant Code", _pItemVendor."Purch. Unit of Measure", '',
            // MCO Le 01-04-2015 => Régie
            // Modif sinon ne filtre pas sur la date de début
            // WORKDATE, TRUE, 9999999999999.0, _pNiveauCde, _pItemVendor."Code Remise", 0);
            WORKDATE(), FALSE, 9999999999999.0, _pNiveauCde, _pItemVendor."Code Remise", 0);
        // FIN MCO Le 01-04-2015 => Régie
        IF LItemUOM.GET(_pItemVendor."Item No.", _pItemVendor."Purch. Unit of Measure") THEN
            LPrix := LPrix / LItemUOM."Qty. per Unit of Measure";


        LPrix := LPrix * (1 - LRemise / 100);

        EXIT(LPrix);
    end;

    procedure RemplirTableau(_pItemVendor: Record "Item Vendor");
    begin
        CLEAR(TabPxNet);
        CLEAR(Marge);

        FOR i := 1 TO 4 DO BEGIN
            TabPxNet[i] := GetPrix(_pItemVendor, i - 1);
            IF PrixCatalogue <> 0 THEN
                Marge[i] := ROUND(
                        100 * (1 - TabPxNet[i] / PrixCatalogue), 0.00001)

        END;
    end;
}

