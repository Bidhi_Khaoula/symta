report 50095 "Sales Statment SYMTA"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Sales Statment SYMTA.rdlc';

    Caption = 'Sales - Invoice';
    PreviewMode = PrintLayout;
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = all;
    dataset
    {
        dataitem("Sales Statment Header"; "Sales Statment Header")
        {
            DataItemTableView = SORTING("Customer No.", "Statement No.")
                                ORDER(Ascending)
                                WHERE("Imprimer Relevé" = CONST(true));
            RequestFilterFields = "Customer No.", "Statement Date";
            RequestFilterHeading = 'Sales Order';
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(No_Header; "Sales Statment Header"."Statement No.")
                    {
                    }
                    column(DueDate_Header; "Sales Statment Header"."Due Date")
                    {
                    }
                    column(CustomerNo_Header; "Sales Statment Header"."Customer No.")
                    {
                    }
                    column(Amount_Header; FORMAT("Sales Statment Header"."Statement Amount", 0, '<Precision,2:><Standard Format,0>'))
                    {
                    }
                    column(SubTitle_Header; STRSUBSTNO(ESK001Lbl, "Sales Statment Header"."Statement No.", FORMAT("Sales Statment Header"."Statement Date", 0, 4)))
                    {
                    }
                    column(TextReglement; TextReglement)
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(CompanyInfoPicture; CompanyInfo.Picture)
                    {
                    }
                    column(CompanyInfoPictureAddress; CompanyInfo."Picture Address")
                    {
                    }
                    column(CustAddr1; CustAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(CustAddr2; CustAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(CustAddr3; CustAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(CustAddr4; CustAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(CustAddr5; CustAddr[5])
                    {
                    }
                    column(CompanyInfoPhNo; CompanyInfo."Phone No.")
                    {
                        IncludeCaption = false;
                    }
                    column(CustAddr6; CustAddr[6])
                    {
                    }
                    column(CustAddr7; CustAddr[7])
                    {
                    }
                    column(CustAddr8; CustAddr[8])
                    {
                    }
                    column(CompanyInfoVATRegNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoBankAccNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(ShiptoAddrCaption; ShiptoAddrCaptionLbl)
                    {
                    }
                    column(BilltoAddrCaption; BilltoAddrCaptionLbl)
                    {
                    }
                    column(EditerTraite; NOT "Payment Method"."Editer Traite")
                    {
                    }
                    column(CustomerNoCaption; CustomerNoCaptionLbl)
                    {
                    }
                    dataitem("Cust. Ledger Entry"; "Cust. Ledger Entry")
                    {
                        DataItemLink = "Statement No." = FIELD("Statement No."),
                                       "Customer No." = FIELD("Customer No.");
                        DataItemLinkReference = "Sales Statment Header";
                        DataItemTableView = SORTING("Customer No.", "Posting Date", "Currency Code")
                                            ORDER(Ascending)
                                            WHERE("Statement No." = FILTER(<> 'DBX'),
                                                  "Statement No." = FILTER(<> ''));
                        column(DocumentNo_Line; "Cust. Ledger Entry"."Document No.")
                        {
                        }
                        column(Amount_Line; FORMAT(Amount))
                        {
                            //DecimalPlaces = 2:2;
                        }
                        column(CustNo_Line; Cust."No.")
                        {
                        }
                        column(CustName_Line; Cust.Name)
                        {
                        }
                        column(CustPostCode_Line; Cust."Post Code")
                        {
                        }
                        column(CustCity_Line; Cust.City)
                        {
                        }
                        column(PageCaption; PageCaptionCapLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        var
                            LInvLine: Record "Sales Invoice Line";
                            LCredLine: Record "Sales Cr.Memo Line";
                        begin

                            CALCFIELDS(Amount);

                            IF Amount = 0 THEN CurrReport.SKIP(); // ANI Le 08-06-2017 Régie on n'affiche pas les lignes à 0

                            Cust.GET("Cust. Ledger Entry"."Sell-to Customer No.");

                            CASE "Cust. Ledger Entry"."Document Type" OF
                                "Cust. Ledger Entry"."Document Type"::Invoice:
                                    BEGIN
                                        LInvLine.RESET();
                                        LInvLine.SETRANGE("Document No.", "Cust. Ledger Entry"."Document No.");
                                        LInvLine.SETRANGE("Internal Line Type", LInvLine."Internal Line Type"::RuptureClientLivré);
                                        IF LInvLine.FINDFIRST() THEN BEGIN
                                            Cust.GET(LInvLine."N° client Livré");
                                            LInvLine.SETFILTER("N° client Livré", '<>%1', Cust."No.");
                                            IF LInvLine.FINDFIRST() THEN
                                                // AD Le 04-07-2012 =>
                                                //Cust.GET("Cust. Ledger Entry"."Sell-to Customer No."); // CODE D'ORIGINE
                                                Cust.GET(Cust."Invoice Customer No.");
                                            // FIN AD Le 04-07-2012
                                        END;
                                    END;

                                "Cust. Ledger Entry"."Document Type"::"Credit Memo":
                                    BEGIN
                                        LCredLine.RESET();
                                        LCredLine.SETRANGE("Document No.", "Cust. Ledger Entry"."Document No.");
                                        LCredLine.SETRANGE("Internal Line Type", LInvLine."Internal Line Type"::RuptureClientLivré);
                                        IF LCredLine.FINDFIRST() THEN BEGIN
                                            Cust.GET(LCredLine."N° client Livré");
                                            LCredLine.SETFILTER("N° client Livré", '<>%1', Cust."No.");
                                            IF LCredLine.FINDFIRST() THEN
                                                // AD Le 04-07-2012 =>
                                                // Cust.GET("Cust. Ledger Entry"."Sell-to Customer No."); // CODE D'ORIGINE
                                                Cust.GET(Cust."Invoice Customer No.");
                                            // FIN AD Le 04-07-2012

                                        END;
                                    END;
                            END;
                        end;

                        trigger OnPreDataItem();
                        begin
                            //CurrReport.BREAK;
                            //CurrReport.CREATETOTALS(Amount);
                        end;
                    }
                    dataitem(RIB; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(CustAdr_6_; CustAddr[6])
                        {
                        }
                        column(CustAdr_7_; CustAddr[7])
                        {
                        }
                        column(CustAdr_5_; CustAddr[5])
                        {
                        }
                        column(CustAdr_4_; CustAddr[4])
                        {
                        }
                        column(CustAdr_2_; CustAddr[2])
                        {
                        }
                        column(CustAdr_3_; CustAddr[3])
                        {
                        }
                        column(CustAdr_1_; CustAddr[1])
                        {
                        }
                        column(Payment_Line__Agency_Code_; CustomerBankAccount."Agency Code")
                        {
                        }
                        column(Payment_Line__Bank_Branch_No__; CustomerBankAccount."Bank Branch No.")
                        {
                        }
                        column(Payment_Line__Bank_Account_No__; CustomerBankAccount."Bank Account No.")
                        {
                        }
                        column(CONVERTSTR_FORMAT__RIB_Key__2_______0__; FORMAT(CustomerBankAccount."RIB Key"))
                        {
                        }
                        column(Payment_Line__Bank_Account_Name_; CustomerBankAccount.Name)
                        {
                        }
                        column(FORMAT_Amount_0___Precision_2___Standard_Format_0___; MontantTraite)
                        {
                        }
                        column(FORMAT_Amount_0___Precision_2___Standard_Format_0____Control1120051; MontantTraite2)
                        {
                        }
                        column(Payment_Line__Drawee_Reference_; '')
                        {
                        }
                        column(IssueCity; IssueCity)
                        {
                        }
                        column(IssueDate; FORMAT(IssueDate))
                        {
                        }
                        column(Payment_Line__Due_Date_; FORMAT(Traite."Due Date"))
                        {
                        }
                        column(Payment_Line__Bank_City_; CustomerBankAccount.City)
                        {
                        }
                        column(PostingDate; FORMAT(Traite."Statement Date"))
                        {
                        }
                        column(AmountText; AmountText)
                        {
                        }
                        column(FORMAT_AmountText_; FORMAT(AmountText))
                        {
                        }
                        column(ACCEPTANCE_or_ENDORSMENTCaption; ACCEPTANCE_or_ENDORSMENTCaptionLbl)
                        {
                        }
                        column(of_DRAWEECaption; of_DRAWEECaptionLbl)
                        {
                        }
                        column(Stamp_Allow_and_SignatureCaption; Stamp_Allow_and_SignatureCaptionLbl)
                        {
                        }
                        column(ADDRESSCaption; ADDRESSCaptionLbl)
                        {
                        }
                        column(NAME_andCaption; NAME_andCaptionLbl)
                        {
                        }
                        column(Value_in__Caption; Value_in__CaptionLbl)
                        {
                        }
                        column(DRAWEE_R_I_B_Caption; DRAWEE_R_I_B_CaptionLbl)
                        {
                        }
                        column(DOMICILIATIONCaption; DOMICILIATIONCaptionLbl)
                        {
                        }
                        column(TOCaption; TOCaptionLbl)
                        {
                        }
                        column(ONCaption; ONCaptionLbl)
                        {
                        }
                        column(AMOUNT_FOR_CONTROLCaption; AMOUNT_FOR_CONTROLCaptionLbl)
                        {
                        }
                        column(CREATION_DATECaption; CREATION_DATECaptionLbl)
                        {
                        }
                        column(DUE_DATECaption; DUE_DATECaptionLbl)
                        {
                        }
                        column(DRAWEE_REF_Caption; DRAWEE_REF_CaptionLbl)
                        {
                        }
                        column(BILLCaption; BILLCaptionLbl)
                        {
                        }
                        column(CODE_ETABLCaption; CODE_ETABLCaptionLbl)
                        {
                        }
                        column(CODE_GUICHETCaption; CODE_GUICHETCaptionLbl)
                        {
                        }
                        column(NO_DE_COMPTECaption; NO_DE_COMPTECaptionLbl)
                        {
                        }
                        column(CLE_R_I_B_Cpation; CLE_R_I_B_CpationLbl)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            //FBRUN le 03/05/2018 -> mauvaise variable pour charger le RIB
                            //IF CustomerBankAccount.GET(Cust."No.",Cust."Preferred Bank Account Code") THEN;
                            IF CustomerBankAccount.GET(Customer."No.", Customer."Preferred Bank Account Code") THEN;

                            Traite := "Sales Statment Header";

                            MontantTraite := '***' + FORMAT("Sales Statment Header"."Statement Amount", 0, '<Precision,2:><Standard Format,0>') + '***';
                            MontantTraite2 := '***' + FORMAT("Sales Statment Header"."Statement Amount", 0, '<Precision,2:><Standard Format,0>') + '***';

                            IF NOT "Payment Method"."Editer Traite" THEN BEGIN
                                Traite."Statement Date" := 0D;
                                Traite."Due Date" := 0D;
                                Traite."Statement Amount" := 0;

                                CustAccountbank."Bank Branch No." := '*****';
                                CustAccountbank."Bank Account No." := '***********';
                                CustAccountbank."RIB Key" := 0;
                                CustAccountbank."Agency Code" := '*****';
                                CustAccountbank.Name := '**************';
                                CustAccountbank.City := '**************';

                                MontantTraite := '**********';
                                MontantTraite2 := '**********';
                                EchéanceTraite := '******';
                                DateTraite := '******';

                            END;
                        end;
                    }
                }

                trigger OnAfterGetRecord();
                var
                /*  PrepmtSalesLine: Record "Sales Line" temporary;
                 SalesPost: Codeunit "Sales-Post";
                 TempSalesLine: Record "Sales Line" temporary; */
                begin
                    IF Number > 1 THEN
                        //  CopyText := Text003;
                        OutputNo += 1;
                    //CurrReport.PAGENO := 1;
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := ABS(NoOfCopies) + 1;
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            var
                BankAccount: Record "Bank Account";
            begin


                ReportLineNumber += 1;
                IF ReportLineNumber = 2 THEN BEGIN
                    CLEAR(CompanyInfo);
                    CompanyInfo.GET();
                END;

                FormatAddr.Company(CompanyAddr, CompanyInfo);
                IssueCity := CompanyInfo.City;

                Customer.GET("Customer No.");
                FormatAddr.Customer(CustAddr, Customer);

                CustAccountbank.RESET();
                CustAccountbank.SETRANGE("Customer No.", "Customer No.");
                IF CustAccountbank.FIND('-') THEN;


                BankAccount.GET(CompanyInfo."Default Bank Account No.");

                IF "Payment Method".GET(Customer."Payment Method Code") THEN;
                IF "Payment Terms".GET(Customer."Payment Terms Code") THEN;

                // CFR le 12/05/2022 => Régie : Ne pas afficher le texte de règlement si le montant est <= 0.
                TextReglement := '';
                IF ("Sales Statment Header"."Statement Amount" > 0) THEN BEGIN
                    TextReglement := STRSUBSTNO("Payment Method"."Commentaires traites",
                                    BankAccount.Name,
                                    BankAccount."Bank Branch No." + ' ' + BankAccount."Agency Code" + ' ' + BankAccount."Bank Account No."
                                    + ' ' + FORMAT(BankAccount."RIB Key"),
                                    CustAccountbank.Name,
                                    CustAccountbank."Bank Branch No." + ' ' + CustAccountbank."Agency Code" + ' ' +
                                    CustAccountbank."Bank Account No." + ' ' + FORMAT(CustAccountbank."RIB Key"));

                    Evaluate(TextReglement, TextReglement + ' à l''échéance du ' + FORMAT("Sales Statment Header"."Due Date"));
                END;
                // FIN CFR le 12/05/2022

                IssueDate := "Sales Statment Header"."Statement Date";


                //Traite := "Sales Statment Header";

                //MontantTraite := '****'+FORMAT(Traite."Statement Amount",0,'<precision,2:2><standard format,0>');
                //MontantTraite2 := '****'+FORMAT(Traite."Statement Amount",0,'<precision,2:2><standard format,0>')+' '+Customer."Currency Code";
                //EchéanceTraite := FORMAT(Traite."Due Date");
                //DateTraite := FORMAT(Traite."Statement Date");

                //IF NOT "Payment Method"."Editer Traite" THEN
                //  BEGIN
                //    Traite."Statement Date" := 0D;
                //    Traite."Due Date" := 0D;
                //    Traite."Statement Amount" := 0;

                //    CustAccountbank."Bank Branch No." := '*****';
                //    CustAccountbank."Bank Account No." := '***********';
                //    CustAccountbank."RIB Key" := 0;
                //    CustAccountbank."Agency Code" := '*****';
                //    CustAccountbank.Name := '**************';
                //    CustAccountbank.City := '**************';

                //    MontantTraite := '**********';
                //    MontantTraite2 := '**********';
                //    EchéanceTraite := '******';
                //    DateTraite := '******';

                //  END;
            end;

            trigger OnPreDataItem();
            begin
                Print := Print OR NOT CurrReport.PREVIEW;
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOf_Copies; NoOfCopies)
                    {
                        Caption = 'No. of Copies';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. of Copies field.';
                    }
                    field(Show_InternalInfo; ShowInternalInfo)
                    {
                        Caption = 'Show Internal Information';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Internal Information field.';
                    }
                }
            }
        }
        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;
    }

    labels
    {
        Dateéchéance = 'date d''échéance';
        VotreReference = 'Votre réfèrence';
        LabelNoClient = 'N° client';
        PrixTailleCaption = 'Prix taille';
        Against_this_BILL_noted_as_NO_CHARGES_please_pay_the_indicated_sum_below_for_order_ofCaption = 'Against this BILL noted as NO CHARGES Please pay the indicated sum below for order of :';
        // FRA = 'Concernant cet LETTRE DE CHANGE stipulé SANS FRAIS, veuillez régler la somme indiquée ci-dessous à l''ordre de :')
    }

    trigger OnInitReport();
    begin
        GLSetup.GET();
        CompanyInfo.GET();
        CompanyInfo.CALCFIELDS(Picture);
        CompanyInfo.CALCFIELDS("Picture Address");
        ReportLineNumber := 0;
    end;

    var
        /*   Text000: Label 'Salesperson';
          Text001: Label 'Total %1';
          Text002: Label 'Total %1 Incl. VAT';
          Text003: Label 'COPY';
          Text004: Label 'Order Confirmation %1'; */

        /*   Text006: Label 'Total %1 Excl. VAT';
          Dateéchéance: Label 'date d''échéance';
          VotreReference: Label 'Votre réfèrence';
          LabelNoClient: Label 'N° client';
          PrixTailleCaption: Label 'Prix taille'; */

        //  Against_this_BILL_noted_as_NO_CHARGES_please_pay_the_indicated_sum_below_for_order_ofCaption: Label 'Against this BILL noted as NO CHARGES Please pay the indicated sum below for order of :';
        GLSetup: Record "General Ledger Setup";
        CompanyInfo: Record "Company Information";
        Cust: Record Customer;
        Traite: Record "Sales Statment Header";
        CustAccountbank: Record "Customer Bank Account";
        "Payment Method": Record "Payment Method";
        "Payment Terms": Record "Payment Terms";
        Customer: Record Customer;
        CustomerBankAccount: Record "Customer Bank Account";
        FormatAddr: Codeunit "Format Address";
        /*  CompanyInfo1: Record "Company Information";
         CompanyInfo2: Record "Company Information";
         MoreLines: Boolean; */
        NoOfCopies: Integer;
        NoOfLoops: Integer;
        CopyText: Text[30];
        /*  Text007: Label 'VAT Amount Specification in ';
         Text008: Label 'Local Currency';
         Text009: Label 'Exchange rate: %1/%2'; */
        Print: Boolean;
        ShowInternalInfo: Boolean;
        /*  ArchiveDocument: Boolean;
         ArchiveDocumentEnable: Boolean;
         LogInteraction: Boolean; */
        LogInteractionEnable: Boolean;
        /*  InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
         VATRegNoCaptionLbl: Label 'VAT Registration No.';
         GiroNoCaptionLbl: Label 'Giro No.';
         BankCaptionLbl: Label 'Bank';
         AccountNoCaptionLbl: Label 'Account No.';
         ShipmentDateCaptionLbl: Label 'Shipment Date';
         OrderNoCaptionLbl: Label 'Order No.';
         HomePageCaptionCap: Label 'Home Page';
         EmailCaptionLbl: Label 'E-Mail';
         HeaderDimCaptionLbl: Label 'Header Dimensions';
         DiscountPercentCaptionLbl: Label 'Discount %';
         SubtotalCaptionLbl: Label 'Subtotal';
         PaymentDiscountVATCaptionLbl: Label 'Payment Discount on VAT';
         LineDimCaptionLbl: Label 'Line Dimensions';
         InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
         VATIdentifierCaptionLbl: Label 'VAT Identifier'; */
        ShiptoAddrCaptionLbl: Label 'Ship-to Address';
        BilltoAddrCaptionLbl: Label 'Ship-to Address';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Page Nb ; %2 = Total PAge NB';
        /*   DescriptionCaptionLbl: Label 'Description';
          GLAccountNoCaptionLbl: Label 'G/L Account No.';
          PrepaymentSpecCaptionLbl: Label 'Prepayment Specification';
          PrepaymentVATAmtSpecCapLbl: Label 'Prepayment VAT Amount Specification';
          PrepmtPmtTermsDescCaptionLbl: Label 'Prepmt. Payment Terms';
          PhoneNoCaptionLbl: Label 'Phone No.';
          AmountCaptionLbl: Label 'Amount';
          VATPercentageCaptionLbl: Label 'VAT %';
          VATBaseCaptionLbl: Label 'VAT Base';
          VATAmtCaptionLbl: Label 'VAT Amount';
          VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
          LineAmtCaptionLbl: Label 'Line Amount';
          TotalCaptionLbl: Label 'Total';
          UnitPriceCaptionLbl: Label 'Unit Price';
          PaymentTermsCaptionLbl: Label 'Payment Terms';
          PaymentMethodCaptionLbl: Label 'Mode de réglement';
          ShipmentMethodCaptionLbl: Label 'Shipment Method';
          DocumentDateCaptionLbl: Label 'Document Date';
          OrderDateCaptionLbl: Label 'Date de commande';
          AllowInvDiscCaptionLbl: Label 'Allow Invoice Discount';
          NombredelignepossibleparPage: Integer;
          NombreDeLigneImprimer: Integer;
          NombredelignepossibleparPagePleine: Integer; */

        /*   ESK50000: Label 'Relevé';
          Text50006: Label 'Mode et conditions de règlement :';
          Text50007: Label 'Echéance : %1'; */
        ESK001Lbl: Label 'No %1 du %2', Comment = '%1 ; %2';
        OutputNo: Integer;
        //   CompanyInfo3: Record "Company Information";
        CustAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        /* EntryLine: Record "Cust. Ledger Entry" temporary;
        "--- Traite ---": Integer; */

        DateTraite: Text[30];
        "EchéanceTraite": Text[30];
        MontantTraite: Text[30];
        MontantTraite2: Text[30];
        TextReglement: Text[1024];



        // ReportUtils: Codeunit ReportFunctions;

        IssueCity: Text[30];
        IssueDate: Date;
        AmountText: Text[30];
        ACCEPTANCE_or_ENDORSMENTCaptionLbl: Label 'ACCEPTANCE or ENDORSMENT';
        of_DRAWEECaptionLbl: Label 'NAME and ADDRESS of DRAWEE';
        Stamp_Allow_and_SignatureCaptionLbl: Label 'Stamp Allow and Signature';
        ADDRESSCaptionLbl: Label 'ADDRESS', Comment = 'Translate address and uppecase the result';
        NAME_andCaptionLbl: Label 'NAME and';
        Value_in__CaptionLbl: Label 'Value in :';
        DRAWEE_R_I_B_CaptionLbl: Label 'DRAWEE R.I.B.';
        DOMICILIATIONCaptionLbl: Label 'DOMICILIATION', Comment = 'Translate domiciliation and uppecase the result';
        TOCaptionLbl: Label 'TO';
        ONCaptionLbl: Label 'ON';
        AMOUNT_FOR_CONTROLCaptionLbl: Label 'AMOUNT FOR CONTROL';
        CREATION_DATECaptionLbl: Label 'CREATION DATE';
        DUE_DATECaptionLbl: Label 'DUE DATE';
        DRAWEE_REF_CaptionLbl: Label 'DRAWEE REF.';
        BILLCaptionLbl: Label 'BILL';
        CODE_ETABLCaptionLbl: Label 'Code Etabl.';
        CODE_GUICHETCaptionLbl: Label 'Code guichet';
        NO_DE_COMPTECaptionLbl: Label 'N° de compte';
        CLE_R_I_B_CpationLbl: Label 'Clé RIB';
        /*  NoSerieSpecialCaptionLbl: Label 'SERIE';
         ShippingAgentNameCaptionLbl: Label 'Transporteur';
         TvaCaptionLbl: Label 'Tva';
         ProduitCatalogueLbl: Label 'CATALOG PRODUCT';
         MontantCommCaptionLbl: Label 'Montant comm.';
         MontantLigne: Text; */
        ReportLineNumber: Integer;
        CustomerNoCaptionLbl: Label 'N° Client';
}

