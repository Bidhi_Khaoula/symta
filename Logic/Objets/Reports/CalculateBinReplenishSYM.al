report 50071 "Calculate Bin Replenish. SYM" //7300
{
    Caption = 'Calculate Bin Replenishment';
    ProcessingOnly = true;

    dataset
    {
        dataitem("Bin Content"; "Bin Content")
        {
            DataItemTableView = SORTING("Location Code", "Item No.", "Variant Code", "Warehouse Class Code", Fixed, "Bin Ranking") ORDER(Descending) WHERE(Fixed = FILTER(true));
            RequestFilterFields = "Bin Code", "Item No.";

            trigger OnAfterGetRecord()
            begin
                Replenishmt.ReplenishBin("Bin Content", AllowBreakbulk);
            end;

            trigger OnPostDataItem()
            begin
                if not Replenishmt.InsertWhseWkshLine() then
                    if not HideDialog then
                        Message(Text000Msg);
            end;

            trigger OnPreDataItem()
            begin
                IF GETFILTER("Location Code") <> '' THEN BEGIN
                    IF GETFILTER("Location Code") <> LocationCode THEN
                        ERROR(Text001Err, FIELDCAPTION("Location Code"), LocationCode);
                END ELSE
                    SETRANGE("Location Code", LocationCode);
                Replenishmt.SetWhseWorksheet(
                  WhseWkshTemplateName, WhseWkshName, LocationCode, DoNotFillQtytoHandle);
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(AllowBreakbulkName; AllowBreakbulk)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Allow Breakbulk';
                        ToolTip = 'Specifies that the bin will be replenished from bin content that is stored in another unit of measure if the item is not found in the original unit of measure.';
                    }
                    field(DoNotFillQtytoHandleName; DoNotFillQtytoHandle)
                    {
                        ApplicationArea = Warehouse;
                        Caption = 'Do Not Fill Qty. to Handle';
                        ToolTip = 'Specifies that the Quantity to Handle field on each worksheet line must be filled manually. ';
                    }
                }
            }

        }
    }

    labels
    {
    }

    var
        Replenishmt: Codeunit Replenishment;
        WhseWkshTemplateName: Code[10];
        WhseWkshName: Code[10];
        LocationCode: Code[10];
        AllowBreakbulk: Boolean;
        HideDialog: Boolean;
        Text000Msg: Label 'There is nothing to replenish.';
        DoNotFillQtytoHandle: Boolean;
        Text001Err: Label 'If you want to set a filter to field %1, it must be %2.', Comment = '%1 = Field Name ;%2 = Location Code';


    procedure InitializeRequest(WhseWkshTemplateName2: Code[10]; WhseWkshName2: Code[10]; LocationCode2: Code[10]; AllowBreakbulk2: Boolean; HideDialog2: Boolean; DoNotFillQtytoHandle2: Boolean)
    begin
        WhseWkshTemplateName := WhseWkshTemplateName2;
        WhseWkshName := WhseWkshName2;
        LocationCode := LocationCode2;
        AllowBreakbulk := AllowBreakbulk2;
        HideDialog := HideDialog2;
        DoNotFillQtytoHandle := DoNotFillQtytoHandle2;
    end;
}

