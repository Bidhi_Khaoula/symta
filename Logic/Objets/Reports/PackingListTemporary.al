report 50032 "Packing List Temporary"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Packing List Temporary.rdlc';

    Caption = 'Packing List Temporaire';
    PreviewMode = PrintLayout;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(PreReport; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfo1Picture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPictureFooter; CompanyInfo."Bottom Picture")
            {
            }

            trigger OnAfterGetRecord();
            begin
                // ANI ESKVN1.0 => logo entête et pied de page
                CompanyInfo.GET();
                IF PrintLogo THEN BEGIN
                    CompanyInfo.CALCFIELDS(Picture);
                    //CompanyInfo.CALCFIELDS("Picture Footer");
                    CompanyInfo.CALCFIELDS("Header Picture", "Bottom Picture");
                END;
                GLSetup.GET();
                // FIN ANI ESKVN1.0
            end;
        }
        dataitem("Packing List Header"; "Packing List Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.";
            RequestFilterHeading = 'Liste de colisage';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(PageCaption; PageCaptionCapLbl)
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalText)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(NetUnitPriceCaption; NetUnitPriceCaptionLbl)
            {
            }
            column(no_fax; no_fax)
            {
            }
            column(Objet_fax; Objet_fax)
            {
            }
            column(Attention_fax; Attention_fax)
            {
            }
            column(nompdf; nompdf)
            {
            }
            column(SalesShptCopyText; GetDocumentCaption())
            {
            }
            column(RightAddr1; ShipToAddr[1])
            {
            }
            column(CompanyAddr1; CompanyAddr[1])
            {
            }
            column(RightAddr2; ShipToAddr[2])
            {
            }
            column(CompanyAddr2; CompanyAddr[2])
            {
            }
            column(RightAddr3; ShipToAddr[3])
            {
            }
            column(CompanyAddr3; CompanyAddr[3])
            {
            }
            column(RightAddr4; ShipToAddr[4])
            {
            }
            column(CompanyAddr4; CompanyAddr[4])
            {
            }
            column(RightAddr5; ShipToAddr[5])
            {
            }
            column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
            {
            }
            column(RightAddr6; ShipToAddr[6])
            {
            }
            column(CompanyInfoHomePage; CompanyInfo."Home Page")
            {
            }
            column(CompanyInfoEmail; CompanyInfo."E-Mail")
            {
            }
            column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
            {
            }
            column(CompanyInfoVATRegtnNo; CompanyInfo."VAT Registration No.")
            {
            }
            column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
            {
            }
            column(CompanyInfoBankName; CompanyInfo."Bank Name")
            {
            }
            column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
            {
            }
            column(SelltoCustNo_SalesShptHeader; "Packing List Header"."Sell-to Customer No.")
            {
            }
            column(DocDate_SalesShptHeader; '')
            {
            }
            column(SalesPersonText; SalesPersonText)
            {
            }
            column(SalesPurchPersonName; SalesPurchPerson.Name)
            {
            }
            column(ReferenceText; ReferenceText)
            {
            }
            column(YourRef_SalesShptHeader; '')
            {
            }
            column(RightAddr7; ShipToAddr[7])
            {
            }
            column(RightAddr8; ShipToAddr[8])
            {
            }
            column(CompanyAddr5; CompanyAddr[5])
            {
            }
            column(CompanyAddr6; CompanyAddr[6])
            {
            }
            column(ShptDate_SalesShptHeader; '')
            {
            }
            column(OutputNo; OutputNo)
            {
            }
            column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
            {
            }
            column(PhoneNoCaption; PhoneNoCaptionLbl)
            {
            }
            column(VATRegNoCaption; VATRegNoCaptionLbl)
            {
            }
            column(GiroNoCaption; GiroNoCaptionLbl)
            {
            }
            column(BankNameCaption; BankNameCaptionLbl)
            {
            }
            column(BankAccNoCaption; BankAccNoCaptionLbl)
            {
            }
            column(ShipmentNoCaption; ShipmentNoCaptionLbl)
            {
            }
            column(ShipmentDateCaption; ShipmentDateCaptionLbl)
            {
            }
            column(HomePageCaption; HomePageCaptionLbl)
            {
            }
            column(EmailCaption; EmailCaptionLbl)
            {
            }
            column(DocumentDateCaption; DocumentDateCaptionLbl)
            {
            }
            column(SelltoCustNo_SalesShptHeaderCaption; '')
            {
            }
            column(LeftAddr1; SellToAddr[1])
            {
            }
            column(LeftAddr2; SellToAddr[2])
            {
            }
            column(LeftAddr3; SellToAddr[3])
            {
            }
            column(LeftAddr4; SellToAddr[4])
            {
            }
            column(LeftAddr5; SellToAddr[5])
            {
            }
            column(LeftAddr6; SellToAddr[6])
            {
            }
            column(LeftAddr7; SellToAddr[7])
            {
            }
            column(LeftAddr8; SellToAddr[8])
            {
            }
            column(LeftAddrCaption; SelltoAddrCaptionLbl)
            {
            }
            column(MiddleAddrCaption; BilltoAddrCaptionLbl)
            {
            }
            column(HeaderField01Caption; SelltoAddrCaptionLbl)
            {
            }
            column(HeaderField02Caption; '')
            {
            }
            column(HeaderField03Caption; 'Commandes')
            {
            }
            column(HeaderField04Caption; '')
            {
            }
            column(HeaderField05Caption; TotalVolumeLbl)
            {
            }
            column(HeaderField06Caption; '')
            {
            }
            column(HeaderField07Caption; '')
            {
            }
            column(HeaderField08Caption; '')
            {
            }
            column(HeaderField09Caption; '')
            {
            }
            column(HeaderField10Caption; '')
            {
            }
            column(HeaderField01; "Packing List Header"."Sell-to Customer No.")
            {
            }
            column(HeaderField02; '')
            {
            }
            column(HeaderField03; test_ListeCommande)
            {
            }
            column(HeaderField04; '')
            {
            }
            column(HeaderField05; "Packing List Header"."Total Volume")
            {
            }
            column(HeaderField06; '')
            {
            }
            column(HeaderField07; '')
            {
            }
            column(HeaderField08; '')
            {
            }
            column(HeaderField09; '')
            {
            }
            column(HeaderField10; '')
            {
            }
            column(MiddleAddr1; CustAddr[1])
            {
            }
            column(MiddleAddr2; CustAddr[2])
            {
            }
            column(MiddleAddr3; CustAddr[3])
            {
            }
            column(MiddleAddr4; CustAddr[4])
            {
            }
            column(MiddleAddr5; CustAddr[5])
            {
            }
            column(MiddleAddr6; CustAddr[6])
            {
            }
            column(MiddleAddr7; CustAddr[7])
            {
            }
            column(MiddleAddr8; CustAddr[8])
            {
            }
            column(PricesInclVAT_SalesShptHeader; '')
            {
            }
            column(NbOfBox_SalesShptHeader; '')
            {
            }
            column(Weight_SalesShptHeader; "Packing List Header"."Calculate Header Weight")
            {
            }
            column(NetWeight_SalesShptHeader; "Packing List Header"."Theoretical Weight")
            {
            }
            column(NbOfBoxCaption; NbOfBoxCaptionLbl)
            {
            }
            column(WeightCaption; WeightCaptionLbl)
            {
            }
            column(NetCaption; NetWeightCaptionLbl)
            {
            }
            dataitem("Packing List Package"; "Packing List Package")
            {
                DataItemLink = "Packing List No." = FIELD("No.");
                DataItemTableView = SORTING("Packing List No.", "Indice Colis")
                                    ORDER(Ascending)
                                    WHERE("Package No." = FILTER(<> ''),
                                          "Package Quantity" = FILTER(> 0));
                column(NoExp_DetailPackingList; "Packing List Package"."Packing List No.")
                {
                }
                column(NumColis_DetailPackingList; "Packing List Package"."Package No.")
                {
                }
                column(NumTypeColis_DetailPackingList; NoPalletCaptionLbl)
                {
                }
                column("TxtDétail"; TxtDétail)
                {
                }
                column(Comment1_DetailPackingList; "Comment 1")
                {
                }
                column(Comment2_DetailPackingList; "Comment 2")
                {
                }
                dataitem("Packing List Line"; "Packing List Line")
                {
                    DataItemLink = "Packing List No." = FIELD("Packing List No."),
                                   "Package No." = FIELD("Package No.");
                    DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                        ORDER(Ascending)
                                        WHERE("Line Quantity" = FILTER(<> 0),
                                              "Package No." = FILTER(<> ''));
                    column(NoArticle_PackingList; "Packing List Line"."Item No.")
                    {
                    }
                    column(Designation_PackingList; "Packing List Line".Description)
                    {
                    }
                    column("QtéColis_PackingList"; "Packing List Line"."Line Quantity")
                    {
                    }
                    column(RefActive; gGestionMultiReference.RechercheRefActive("Packing List Line"."Item No."))
                    {
                    }

                    trigger OnPreDataItem();
                    begin
                        "Nb colis" := 0;
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    lTexteTypeColis: Text[50];
                    ParamTxt: Label 'Numéro de %1 ;', Comment = '%1';
                begin
                    "Packing List Package".CALCFIELDS("Package Quantity", "Package Weight Auto");
                    lTexteTypeColis := FORMAT("Packing List Package"."Package Type");
                    NoPalletCaptionLbl := STRSUBSTNO(ParamTxt, lTexteTypeColis);

                    TxtDétail := STRSUBSTNO(Text50000Lbl, "Packing List Package".Length, "Packing List Package".Width, "Packing List Package".Height,
                                  "Packing List Package"."Package Weight", "Packing List Package"."Package Quantity");

                    IF "Packing List Package"."Outside Of EC" THEN
                        TxtDétail := TxtDétail + ' ' + Text50001Lbl
                    // AD Le 02-07-2015 =>
                    ELSE
                        TxtDétail := TxtDétail + ' ' + ESK002Lbl
                    // FIN AD Le 02-07-2015
                end;
            }
            dataitem("Packing List Package 2"; "Packing List Package")
            {
                DataItemLink = "Packing List No." = FIELD("No.");
                DataItemTableView = SORTING("Packing List No.", "Indice Colis")
                                    ORDER(Ascending)
                                    WHERE("Package No." = FILTER(<> ''),
                                          "Package Quantity" = FILTER(> 0));
                column(text_DetailPackingList2; text_DetailPackingList2)
                {
                }
                column(Comment1_DetailPackingList2; "Comment 1")
                {
                }
                column(Comment2_DetailPackingList2; "Comment 2")
                {
                }

                trigger OnAfterGetRecord();
                begin
                    "Packing List Package 2".CALCFIELDS("Package Quantity", "Package Weight Auto");
                    text_DetailPackingList2 := '1 ' + FORMAT("Packing List Package 2"."Package Type") + ' de ' + FORMAT("Packing List Package 2"."Package Quantity") + ' article(s).';
                end;
            }

            trigger OnAfterGetRecord();
            var
                lPackingListLine: Record "Packing List Line";
                lI: Integer;
                lY: Integer;
                lFind: Boolean;
                lDocNo: array[64] of Code[20];

            begin

                FormatAddressFields("Packing List Header");
                "Packing List Header".CALCFIELDS("Theoretical Weight", "Total Volume");

                //Temporary
                /*
                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");
                
                ShowPrices := "Sales Shipment Header"."Abandon remainder" ;
                
                FormatAddressFields("Sales Shipment Header");
                FormatDocumentFields("Sales Shipment Header");
                
                DimSetEntry1.SETRANGE("Dimension Set ID","Dimension Set ID");
                
                IF LogInteraction THEN
                  IF NOT CurrReport.PREVIEW THEN
                    SegManagement.LogDocument(
                      5,"No.",0,0,DATABASE::Customer,"Sell-to Customer No.","Salesperson Code",
                      "Campaign No.","Posting Description",'');
                
                
                
                  IF (PrintForFax) THEN
                    CU_GestionEdition."GetInfoFax-Pdf"('BL',"Sales Shipment Header"."No.",no_fax,Objet_fax,Attention_fax,nompdf);
                
                  IF (PrintForFax) AND (no_fax = '') THEN
                     ERROR ('Le no de fax est vide');
                
                  // AD Le 25-03-2007 => Possibilité d'imprimer avec un logo sans faxer
                  IF PrintLogo AND (NOT PrintForFax) THEN
                    BEGIN
                      no_fax := '';
                      Objet_fax := '';
                      nompdf := '';
                    END;
                
                
                //AD on regarde si export pour les code douanier
                IF "Sales Shipment Header"."VAT Bus. Posting Group" = 'SANS TVA' THEN  arExport := TRUE
                ELSE arExport := FALSE;
                
                */

                /*
                IF "Sales Shipment Header"."N° Crédit Documentaire" <> '' THEN
                  CredocCaption := CreditDocumentaireCaptionLbl
                ELSE
                  CredocCaption := '';
                */
                //>> CFR le 15/12/2020 : liste des commandes associées
                test_ListeCommande := '';
                lPackingListLine.SETRANGE("Packing List No.", "Packing List Header"."No.");
                IF lPackingListLine.FINDSET() THEN
                    REPEAT
                        lFind := FALSE;
                        IF (lPackingListLine."Source Document" = lPackingListLine."Source Document"::"Sales Order") THEN BEGIN
                            FOR lY := 1 TO ARRAYLEN(lDocNo) DO
                                IF lPackingListLine."Source No." = lDocNo[lY] THEN
                                    lFind := TRUE;
                            IF (NOT lFind) THEN BEGIN
                                lI := COMPRESSARRAY(lDocNo) + 1;
                                lDocNo[lI] := lPackingListLine."Source No.";
                                test_ListeCommande += lPackingListLine."Source No." + ' ';
                            END;
                        END;
                    UNTIL lPackingListLine.NEXT() = 0;
                //>> CFR le 15/12/2020 : liste des commandes associées

            end;

            /*   trigger OnPreDataItem();
              var
                  _Rec: RecordRef;
                  _CodeSoc: Code[20];
              begin
              end; */
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'No. of Copies';
                        ToolTip = 'Specifies how many copies of the document to print.';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Internal Information';
                        ToolTip = 'Specifies if the document shows internal information.';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ToolTip = 'Specifies if you want to record the reports that you print as interactions.';
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Correction Lines';
                        ToolTip = 'Specifies if the correction lines of an undoing of quantity posting will be shown on the report.';
                    }
                    field(ShowLotSN; ShowLotSN)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Serial/Lot Number Appendix';
                        ToolTip = 'Specifies if you want to print an appendix to the sales shipment report showing the lot and serial numbers in the shipment.';
                    }
                    field(DisplayAsmInfo; DisplayAssemblyInformation)
                    {
                        ApplicationArea = Assembly;
                        Caption = 'Show Assembly Components';
                        ToolTip = 'Specifies if you want the report to include information about components that were used in linked assembly orders that supplied the item(s) being sold.';
                    }
                    field(Print_Logo; PrintLogo)
                    {
                        Caption = 'Print logo';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Print logo field.';
                    }
                    field(Show_Prices; ShowPrices)
                    {
                        Caption = 'Show prices';
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show prices field.';
                    }
                    group(Fax)
                    {
                        Caption = 'Fax';
                        Visible = false;
                        field(PrintForFax; PrintForFax)
                        {
                            Caption = 'Impression fax';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Impression fax field.';
                        }
                        field(no_fax; no_fax)
                        {
                            Caption = 'N°';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° field.';
                        }
                        field(Attention_fax; Attention_fax)
                        {
                            Caption = 'A l''attention de';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the A l''attention de field.';
                        }
                        field(nompdf; nompdf)
                        {
                            Caption = 'Nom du fichier';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Nom du fichier field.';
                        }
                    }
                }
            }
        }
        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;
            InitPageOption();
        end;
    }
    trigger OnInitReport();
    begin
        CompanyInfo.GET();
        SalesSetup.GET();
        //FormatDocument.SetLogoPosition(SalesSetup."Logo Position on Documents",CompanyInfo1,CompanyInfo2,CompanyInfo3);
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
        AsmHeaderExists := FALSE;
    end;

    var
        SalesHeader: Record "Sales Header";
        ShippingAgent: Record "Shipping Agent";
        GLSetup: Record "General Ledger Setup";
        ShippingAgentServices: Record "Shipping Agent Services";
        ShipmentMethod: Record "Shipment Method";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        SalesSetup: Record "Sales & Receivables Setup";
        FormatDocument: Codeunit "Format Document";
        SegManagement: Codeunit SegManagement;
        gGestionMultiReference: Codeunit "Gestion Multi-référence";
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text[80];
        NoOfCopies: Integer;
        "Nb colis": Integer;


        OutputNo: Integer;
        ShowInternalInfo: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        Text003Lbl: Label 'Total %1', Comment = '%1 = Currency';
        Text004Lbl: Label 'Total %1 Incl. VAT', Comment = '%1 = Currency';
        Text005Lbl: Label 'Total %1 Excl. VAT', COmment = '%1 = Currency';
        ItemTrackingAppendixCaptionLbl: Label 'Item Tracking - Appendix';
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Reg. No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        ShipmentNoCaptionLbl: Label 'Shipment No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'Email';
        DocumentDateCaptionLbl: Label 'Document Date';
        BilltoAddrCaptionLbl: Label 'Bill-to Address';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Page Nb ; %2 = Totol NB of Page';
        PrintLogo: Boolean;
        SellToAddr: array[8] of Text[50];
        SelltoAddrCaptionLbl: Label 'Ship-to Address';
        UnitPriceCaptionLbl: Label 'Unit Price';
        NetUnitPriceCaptionLbl: Label 'Net Price';
        ShowPrices: Boolean;
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        LineAmtCaptionLbl: Label 'Line Amount';
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        TotalText: Text[50];

        AmountCaptionLbl: Label 'Amount';
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];

        CodeUtilisateurCreation: Code[20];
        DateFormatLbl: Label '<day,2>/<month,2>/<Year4>';

        NetWeightCaptionLbl: Label 'Poids net :';

        no_fax: Text[30];
        nompdf: Text[80];
        Objet_fax: Text[100];
        Attention_fax: Text[50];
        PrintForFax: Boolean;

        Text50000Lbl: Label 'L : %1 - l : %2 - h : %3 - Pb : %4 Kg. %5 article(s).', Comment = '%1 = Longeur ; %2 = LArgeur ; %3 = hauteur ; %4 = Poids Paquet; %5 = Qty';
        Text50001Lbl: Label 'Palette Hors CE';

        ESK002Lbl: Label 'Palette CE';
        "TxtDétail": Text;
        WeightCaptionLbl: Label 'Poids brut avec emballage';
        NbOfBoxCaptionLbl: Label 'Nb de Palette';

        NoPalletCaptionLbl: Text[50];
        text_DetailPackingList2: Text;
        test_ListeCommande: Text[250];
        TotalVolumeLbl: label 'Total Volume';

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(5)) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    local procedure FormatAddressFields(PackingListHeader: Record "Packing List Header");
    var
        lCustomer: Record Customer;
        CodeunitFunctions: Codeunit "Codeunits Functions";
    begin
        //Temporary FormatAddr.SalesShptSellTo(SellToAddr,"Sales Shipment Header");
        IF lCustomer.GET(PackingListHeader."Sell-to Customer No.") THEN;
        //CFR le 21/09/2021 => Régie : afficher l'adresse de livraison du client
        //FormatAddr.Customer(ShipToAddr, lCustomer);
        CodeunitFunctions.CustomerShipping(ShipToAddr, lCustomer);
    end;

    local procedure FormatDocumentFields(SalesShipmentHeader: Record "Sales Shipment Header");
    begin
        WITH SalesShipmentHeader DO BEGIN
            FormatDocument.SetSalesPerson(SalesPurchPerson, "Salesperson Code", SalesPersonText);
            ReferenceText := FormatDocument.SetText("Your Reference" <> '', FIELDCAPTION("Your Reference"));

            // ANI ESKVN1.0
            // BL Chiffré / TVA - ShowPrices
            IF "Currency Code" = '' THEN BEGIN
                GLSetup.TESTFIELD("LCY Code");
                TotalText := STRSUBSTNO(Text003Lbl, GLSetup."LCY Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, GLSetup."LCY Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, GLSetup."LCY Code");
            END ELSE BEGIN
                TotalText := STRSUBSTNO(Text003Lbl, "Currency Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, "Currency Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, "Currency Code");
            END;

            FormatDocument.SetShipmentMethod(ShipmentMethod, "Shipment Method Code", SalesShipmentHeader."Language Code");
            SetShippingAgent(ShippingAgent, "Shipping Agent Code");
            SetShippingAgentServices(ShippingAgentServices, "Shipping Agent Code", "Shipping Agent Service Code");

            SalesHeader.RESET();
            CodeUtilisateurCreation := '';
            /*
            IF SalesHeader.GET(SalesHeader."Document Type"::Order, "Order No.") THEN
              CodeUtilisateurCreation := SalesHeader."Code utilisateur création";
            */
            // FIN ANI ESKVN1.0
        END;
    end;

    local procedure GetDocumentCaption() Result: Text[250];
    var
        Text50001: Label 'Packing List No. %1 - %2', Comment = '%1 = Num Package ; %2 = Date Création';
    begin
        Result := STRSUBSTNO(Text50001, "Packing List Header"."No.", FORMAT("Packing List Header"."Creation Date", 0, DateFormatLbl));
        EXIT(Result);
    end;


    procedure SetShippingAgent(var pShipmentAgent: Record "Shipping Agent"; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgent.INIT()
        ELSE
            pShipmentAgent.GET(Code);
    end;

    procedure SetShippingAgentServices(var pShipmentAgentServices: Record "Shipping Agent Services"; ShippingAgentCode: Code[10]; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgentServices.INIT()
        ELSE
            pShipmentAgentServices.GET(ShippingAgentCode, Code);

    end;

    procedure GetWhseEmployeeName(SalesShipmentHeader: Record "Sales Shipment Header") WhseEmplName: Text;
    var
        lWhseEmpl: Record "Warehouse Employee";
    //lUser: Record User;
    begin
        WhseEmplName := '';
        IF lWhseEmpl.GET(SalesShipmentHeader.Preparateur, SalesShipmentHeader."Location Code") THEN
            WhseEmplName := SalesShipmentHeader.Preparateur;
    end;

    local procedure InitPageOption();
    begin
        DisplayAssemblyInformation := TRUE; // coché par défaut Redmine #688
        PrintLogo := TRUE;
        PrintForFax := FALSE;
        no_fax := '';
        Attention_fax := '';
        nompdf := '';
        Objet_fax := '';
    end;
}

