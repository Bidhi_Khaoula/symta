report 50161 "Réappro Seuil SYMTA (Licence)"
{
    Caption = 'Réappro Seuil SYMTA (Licence)';
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Réappro Seuil SYMTA (Licence).rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = WHERE(Stocké = CONST(true));
            RequestFilterFields = "Code Appro", "No. 2", "Manufacturer Code", "Item Category Code", "Vendor No.", "Gen. Prod. Posting Group", "No.", "Date Dernière Sortie";
            column(VendorNo; Vendor."No.")
            {
            }
            column(ItemNo; "No.")
            {
            }
            column(ItemNo2; "No. 2")
            {
            }
            dataitem("SemaineTestée"; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    ORDER(Ascending);
                column(DelaiEnSemaine; DelaiEnSemaine)
                {
                }
                column(DelaiEnMois; DelaiEnMois)
                {
                }
                column(ChaineMois; ChaineMois)
                {
                }
                column(DateFinAppro; DateFinAppro)
                {
                }
                column(Attendu; Attendu)
                {
                }
                column("QteReservé"; QteReservé)
                {
                }
                column(AQuai; AQuai)
                {
                }
                column(Projection; Projection)
                {
                }
                column(ProjectionSurDelai; ProjectionSurDelai)
                {
                }
                column(Diff1; Diff1)
                {
                }
                column(ATerme; ATerme)
                {
                }
                column(Number; Number)
                {
                }

                trigger OnAfterGetRecord();
                var
                    CalcConso: Codeunit CalculConso;
                    i: Integer;
                    LDate: Date;
                    AttenduTmp: Decimal;
                    "DelaiCdeDepassé": Boolean;
                    ProjectionWilson: Decimal;
                    QteEconomiqueWilson: Decimal;
                    PxNet: Decimal;
                    DateConsommation: Date;
                begin

                    DelaiEnMois := ROUND(SemaineTestée.Number / 4, 1, '>');

                    DateConsommation := CALCDATE('<-1M>', DateSimulation);

                    ChaineMois := 'NNNNNNNNNNNN';
                    FOR i := 1 TO DelaiEnMois DO BEGIN
                        LDate := CALCDATE('+' + FORMAT(i) + 'M', DateConsommation);
                        ChaineMois[DATE2DMY(LDate, 2)] := 'O';
                    END;

                    Projection := CalcConso.CalculConso(Item."No.", DateConsommation, ChaineMois, 4, FALSE);
                    IF Projection < 0 THEN Projection := 0;

                    ProjectionSurDelai := Projection * SemaineTestée.Number / (4 * DelaiEnMois);

                    IF Item.Stocké THEN
                        DateFinAppro := CALCDATE('+' + FORMAT(SemaineTestée.Number) + 'S', DateSimulation)
                    ELSE
                        DateFinAppro := 20901231D;

                    AQuai := AttenduSpécifiqueConso(Item."No.", 20901231D, FALSE, 2, DelaiCdeDepassé);
                    Attendu := AttenduSpécifiqueConso(Item."No.", DateFinAppro, FALSE, 1, DelaiCdeDepassé);
                    Item.SETRANGE("Date Filter", 0D, DateFinAppro);
                    Item.CalcAttenduReserve(QteReservé, AttenduTmp);

                    // Mois en cours supperieur à la projection //
                    IF SemaineTestée.Number = 1 THEN BEGIN
                        Conso_0 := CalcConso.RechecherConsommation(Item."No.", DMY2DATE(1, DATE2DMY(DateSimulation, 2), DATE2DMY(DateSimulation, 3)),
                                                CALCDATE('<+FM>', DMY2DATE(1, DATE2DMY(DateSimulation, 2), DATE2DMY(DateSimulation, 3))));
                        Diff1 := Conso_0 - (Projection / DelaiEnMois);
                        IF Diff1 < 0 THEN Diff1 := 0;
                    END;



                    Item.CALCFIELDS(Inventory);

                    ATerme := Item.Inventory + AQuai + +Attendu - QteReservé;
                    IF Item.Stocké THEN
                        ATerme := ATerme - ProjectionSurDelai - Diff1;


                    IF (ATerme < 0) AND (DateFinAppro >= DateSimulation) THEN
                        // LM le 03-07-2013=>mise en commentaire des 3 lignes suivantes
                        //IF DelaiCdeDepassé THEN
                        //  DelaiCdeDepassé := DelaiCdeDepassé
                        //ELSE
                        BEGIN
                        ProjectionWilson := CalcConso.CalculConso(Item."No.", DateConsommation, 'OOOOOOOOOOOO', 4, FALSE);
                        PxNet := 10; // ???????
                        QteEconomiqueWilson := CalcConso.RacineCarre((2 * ProjectionWilson * CoutCommande) / (PxNet * TauxStockage));
                        IF QteEconomiqueWilson > ProjectionWilson THEN
                            QteEconomiqueWilson := ProjectionWilson;
                        IF QteEconomiqueWilson <= 0 THEN
                            QteEconomiqueWilson := 1;

                        LineNo += 10000;
                        ReqLine.VALIDATE("Worksheet Template Name", CurrTemplateName);
                        ReqLine.VALIDATE("Journal Batch Name", CurrWorksheetName);
                        ReqLine.VALIDATE("Line No.", LineNo);
                        ReqLine.VALIDATE("Reservé Calculé", QteReservé);
                        ReqLine.VALIDATE("Attendu Calculé", Attendu);
                        ReqLine.VALIDATE("Type de commande", TypeCommande);


                        CalcConso.CréerLigneAppro(Item, QteEconomiqueWilson, ReqLine);

                        CurrReport.BREAK();

                    END;
                end;

                trigger OnPreDataItem();
                begin
                    SemaineTestée.SETRANGE(Number, 1, DelaiEnSemaine);
                end;
            }

            trigger OnAfterGetRecord();
            var
                SalesLine: Record "Sales Line";
                SalesHeader: Record "Sales Header";
                RecDate: Record Date;
                //CalcPurchasePrice: Codeunit "Purch. Price Calc. Mgt.";
                LDateDerniereSortie: Date;
                NbCde: Integer;
                MeilleurFournisseur: Code[20];
            // px: Decimal;

            begin

                Item.CalcAttenduReserve(QteReservé, Attendu);

                // Si l'article est non stocké, et pas de besoin, on passe l'article
                IF (NOT Item.Stocké) AND ((Item.Inventory + Attendu - Reserve.AsInteger()) >= 0) THEN //=> A VOIR POURQUOI CETTE LIGNE
                    CurrReport.SKIP();


                LDateDerniereSortie := Item.DateDerniereSortie();

                IF DateDerniereSortieLimite <> 0D THEN
                    IF (LDateDerniereSortie = 0D) OR (LDateDerniereSortie < DateDerniereSortieLimite) THEN BEGIN
                        SalesLine.RESET();
                        SalesLine.SETRANGE("Document Type", SalesLine."Document Type"::Order);
                        SalesLine.SETRANGE(Type, SalesLine.Type::Item);
                        SalesLine.SETRANGE("No.", Item."No.");
                        NbCde := 0;
                        IF SalesLine.FINDFIRST() THEN
                            REPEAT
                                SalesHeader.GET(SalesLine."Document Type", SalesLine."Document No.");
                                IF SalesHeader."Order Date" > DateDerniereSortieLimite THEN
                                    NbCde += 1;
                            UNTIL SalesLine.NEXT() = 0;
                        IF NbCde = 0 THEN
                            CurrReport.SKIP();
                    END;


                //TODOMeilleurFournisseur := CalcPurchasePrice.GetBestPurchPrice("No.", 10000000, TypeCommande, px, '');



                IF NOT Vendor.GET(MeilleurFournisseur) THEN
                    CurrReport.SKIP();
                //ERROR(' Pas de meilleur frn pour l''articel %1', Item."No.");


                // Calcul du délai
                DelaiAppro := Vendor."Lead Time Calculation";  // +++++++++++++++++++++++++ //

                IF CALCDATE(DelaiAppro, DateSimulation) = DateSimulation THEN
                    CurrReport.SKIP();


                RecDate.RESET();
                RecDate.SETRANGE("Period Type", RecDate."Period Type"::Week);
                RecDate.SETRANGE("Period Start", DateSimulation, CALCDATE(DelaiAppro, DateSimulation));
                DelaiEnSemaine := RecDate.COUNT;
            end;

            trigger OnPostDataItem();
            begin

                //LM le 24-07-2012 =>maj jauge
                LLigne := 0;
                ReqLine.RESET();
                ReqLine.SETCURRENTKEY("Fournisseur Calculé", "Ref. Active");
                ReqLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
                ReqLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
                IF ReqLine.FINDFIRST() THEN
                    REPEAT
                        LLigne += 10000;
                        ReqLine."Indice Jauge" := LLigne;
                        ReqLine.MODIFY();
                    UNTIL ReqLine.NEXT() = 0;

                //LM le 31-07-2012: delock le user appro
                lm_fonction.user_appro_sortie();
            end;

            trigger OnPreDataItem();
            var
                "Requisition Wksh. Name": Record "Requisition Wksh. Name";
                LUser: Record User;
            begin

                //Item.SETRANGE("No. 2", '80320607');

                // CFR le 18/01/2024 => R‚gie - Utilisation d'un record temporaire
                /*   {
                   //LM le 31-07-2012=> informe si des users sont en cours de calcul  (si traitement non diff‚r‚)
                   IF nb_milli_sec = 0 THEN
        IF NOT lm_fonction.user_appro_entree() THEN CurrReport.QUIT;
                   }*/
                //LM le 27-07-12 =>temporisation
                SLEEP(nb_milli_sec * 60000);


                Item.SETRANGE(Stocké, TRUE);
                DateDerniereSortieLimite := 0D;

                CoutCommande := 50;
                TauxStockage := 0.03;


                IF CurrTemplateName = '' THEN
                    CurrTemplateName := 'PROP';

                IF CurrWorksheetName = '' THEN
                    CurrWorksheetName := COPYSTR(USERID, 1, 10);

                "Requisition Wksh. Name".INIT();
                "Requisition Wksh. Name".VALIDATE("Worksheet Template Name", CurrTemplateName);
                "Requisition Wksh. Name".VALIDATE(Name, CurrWorksheetName);
                "Requisition Wksh. Name".VALIDATE(Description, CurrWorksheetName);
                IF "Requisition Wksh. Name".INSERT() THEN;


                // MCO Le 18-01-2016 => Gestion du dernier numéro traité
                CLEAR(LUser);
                LUser.SETRANGE("User Name", USERID);
                IF LUser.FINDFIRST() THEN
                    //TODOLUser."Dernière Ref. Appro" := '';
                    LUser.MODIFY();
                // FIN MCO Le 18-01-2016 => Gestion du dernier numéro traité
                ReqLine.RESET();
                ReqLine.SETRANGE("Worksheet Template Name", CurrTemplateName);
                ReqLine.SETRANGE("Journal Batch Name", CurrWorksheetName);
                ReqLine.DELETEALL();
                IF ReqLine.FINDLAST() THEN
                    LineNo := ReqLine."Line No."
                ELSE
                    LineNo := 0;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("<DateSimulation>"; DateSimulation)
                {
                    Caption = 'Date Simulation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Simulation field.';
                }
                field(nb_millisec; nb_milli_sec)
                {
                    Caption = 'Démarrage dans x minutes';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Démarrage dans x minutes field.';
                }
            }
        }

    }
    trigger OnInitReport();
    begin

        DateSimulation := WORKDATE();
        TypeCommande := 3;
    end;

    var
        ReqLine: Record "Requisition Line";
        Vendor: Record Vendor;
        lm_fonction: Codeunit lm_fonction;
        // "--- Paramètres ---": Integer;
        DelaiAppro: DateFormula;
        DateDerniereSortieLimite: Date;
        DateSimulation: Date;
        TypeCommande: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        //"---": Integer;
        DelaiEnMois: Integer;
        DelaiEnSemaine: Integer;
        ChaineMois: Code[12];
        DateFinAppro: Date;
        Projection: Decimal;
        ProjectionSurDelai: Decimal;
        Attendu: Decimal;
        "QteReservé": Decimal;
        AQuai: Decimal;
        Conso_0: Decimal;
        Diff1: Decimal;
        ATerme: Decimal;
        //  "--- Constante Wilson ---": Integer;
        CoutCommande: Decimal;
        TauxStockage: Decimal;
        // "--- Param. Feuille ---": Integer;
        CurrTemplateName: Code[20];
        CurrWorksheetName: Code[20];
        LineNo: Integer;
        LLigne: Integer;
        //  "-----------": Integer;
        nb_milli_sec: Integer;


    procedure "AttenduSpécifiqueConso"(_CodeArticle: Code[20]; _DateLimite: Date; "_AvecDelaiDepassé": Boolean; _TypeRetour: Integer; var "_DelaiCdeDepassé": Boolean): Decimal;
    var
        PurchLine: Record "Purchase Line";
        WhseReceiptLine: Record "Warehouse Receipt Line";
        QteAQuai: Decimal;
        QteAQuaiCde: Decimal;
        QteAttendu: Decimal;
    begin

        // Recherche des lignes de commandes pour cet article
        PurchLine.RESET();
        PurchLine.SETRANGE("Document Type", PurchLine."Document Type"::Order);
        PurchLine.SETRANGE(Type, PurchLine.Type::Item);
        PurchLine.SETRANGE("No.", _CodeArticle);
        PurchLine.SETRANGE("Promised Receipt Date", 0D, _DateLimite);
        IF PurchLine.FINDFIRST() THEN
            REPEAT
                _DelaiCdeDepassé := FALSE;
                IF _AvecDelaiDepassé THEN
                    QteAttendu += PurchLine."Outstanding Qty. (Base)"
                ELSE
                    IF (PurchLine."Promised Receipt Date" > DateSimulation) OR (PurchLine."Promised Receipt Date" = 0D) THEN
                        QteAttendu += PurchLine."Outstanding Qty. (Base)"
                    ELSE
                        _DelaiCdeDepassé := TRUE;  // permet de remttre les choses en reception comme attendu si le délai est dépassé

                // Recherche des qtes à quai
                WhseReceiptLine.RESET();
                WhseReceiptLine.SETCURRENTKEY("Source Type", "Source Subtype", "Source No.", "Source Line No.");
                WhseReceiptLine.SETRANGE("Source Type", 39);
                WhseReceiptLine.SETRANGE("Source Subtype", 1);
                WhseReceiptLine.SETRANGE("Source No.", PurchLine."Document No.");
                WhseReceiptLine.SETRANGE("Source Line No.", PurchLine."Line No.");
                WhseReceiptLine.CALCSUMS("Qty. to Receive (Base)");

                QteAQuaiCde := WhseReceiptLine."Qty. to Receive (Base)";

                QteAQuai += QteAQuaiCde;

                IF _DelaiCdeDepassé THEN
                    QteAttendu += QteAQuaiCde


            UNTIL PurchLine.NEXT() = 0;


        CASE _TypeRetour OF
            // Uniquement les attendus
            0:
                EXIT(QteAttendu);
            // Attendus - Quais
            1:
                EXIT(QteAttendu - QteAQuai);
            // Uniquement les Quais
            2:
                EXIT(QteAQuai);
            ELSE
                ERROR('TypeRetour incorrect !');
        END;
    end;
}

