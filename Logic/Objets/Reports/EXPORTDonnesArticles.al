report 50048 "EXPORT Données Articles"
{

    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'EXPORT Données Articles';
    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "No. 2", "Manufacturer Code";

            trigger OnAfterGetRecord();
            begin

                TempArticle."No." := Item."No.";
                TempArticle."No. 2" := Item."No. 2";
                TempArticle."Shelf No." := 'NAV';
                TempArticle.Insert();
            end;

            trigger OnPreDataItem();
            begin

                IF Item.GETFILTERS = '' THEN
                    CurrReport.BREAK();
            end;
        }
        dataitem(ArticleTemporaire; Integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);
            column(TmpArticleNo; TempArticle."No.")
            {
            }
            column(Number; Number)
            {
            }

            trigger OnAfterGetRecord();
            var
                Litem: Record Item;
                TempVendorBuffer: Record "Vendor Price Buffer" temporary;
                TempVendorBuffer2: Record "Vendor Price Buffer" temporary;
                TempVendorBufferP1: Record "Vendor Price Buffer" temporary;
                TempVendorBufferP2: Record "Vendor Price Buffer" temporary;
                LSalesPrice: Record "Sales Price";
                LSalesPrice2: Record "Sales Price";
                LSalesPrice3: Record "Sales Price";
                LSalesPrice4: Record "Sales Price";
                _TmpNo: Code[40];
                // LTabTar: Record Tab_tar_tmp;
                LSubstTxt: Text;
            // rec_ItemDesc: Record Item;
            begin

                IF ArticleTemporaire.Number = 1 THEN
                    TempArticle.FINDFIRST()
                ELSE
                    TempArticle.NEXT();

                CLEAR(Litem);

                _TmpNo := TempArticle."No. 2"; // AD Le 13-05-2016 => Je passe sur le 2

                NoEnr += 1;
                Window.UPDATE(1, FORMAT(NoEnr) + '/' + FORMAT(NbEnr));


                // On charge l'article soit à partir de la ref soit de la ref active
                IF NOT Litem.GET(COPYSTR(TempArticle."No.", 1, 20)) THEN
                    //IF Litem.GET(GestionMultiref.RechercheArticleByActive(TempArticle."No.")) THEN;
                    IF Litem.GET(GestionMultiref.RechercheMultiReference(_TmpNo)) THEN;

                // Exportation des donnée de la fiche article
                IF NOT VersionSimplifiée THEN BEGIN
                    FichierCsv.WRITE(MakeBody(Litem, TempArticle));

                    IF Litem."No." <> '' THEN BEGIN

                        IF ExportDonnéesAchat THEN BEGIN
                            TempVendorBuffer.DELETEALL();
                            TempVendorBuffer2.SetRecordsByMulti(Litem."No.", 999999, '', TempVendorBuffer, opt_TypeCde, TRUE);

                            TempVendorBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
                            // AD Le 01-04-2015 =>
                            IF txt_VendorNoAchat <> '' THEN
                                TempVendorBuffer.SETRANGE("Vendor No.", txt_VendorNoAchat);
                            // FIN AD Le 01-04-2015
                            IF TempVendorBuffer.FINDFIRST() THEN
                                REPEAT
                                    FichierCsv.WRITE(MakeEntete(Litem) +
                                                       MakePointVirgule(nbChampPrincipaux) +
                                                       MakePurchInfo(Litem, TempVendorBuffer));
                                UNTIL TempVendorBuffer.NEXT() = 0;
                        END;

                        IF ExportDonnéesVentes THEN BEGIN
                            LSalesPrice.RESET();
                            LSalesPrice.SETRANGE("Item No.", Litem."No.");
                            LSalesPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, DateDeBase);
                            LSalesPrice.SETRANGE("Starting Date", 0D, DateDeBase);
                            IF LSalesPrice.FINDFIRST() THEN
                                REPEAT
                                    FichierCsv.WRITE(MakeEntete(Litem) +
                                                       MakePointVirgule(nbChampPrincipaux) +
                                                       MakePointVirgule(nbChampAchats) +
                                                       MakeSalesInfo(Litem, LSalesPrice));
                                UNTIL LSalesPrice.NEXT() = 0;
                        END;

                        // DZ le 12-03-2012
                        IF bln_ShowSubstitution THEN BEGIN

                            rec_ArtSubst.RESET();
                            rec_ArtSubst.SETRANGE("No.", Litem."No.");
                            rec_ArtSubst.SETRANGE(Type, rec_ArtSubst.Type::Item);
                            IF rec_ArtSubst.FINDFIRST() THEN
                                REPEAT
                                    FichierCsv.WRITE(MakeEntete(Litem) +
                                                       MakePointVirgule(nbChampPrincipaux) +
                                                       MakePointVirgule(nbChampAchats) +
                                                       MakePointVirgule(nbChampVentes) +
                                                       MakeSubstInfo(Litem, rec_ArtSubst));
                                UNTIL rec_ArtSubst.NEXT() = 0;

                        END;
                        // Fin DZ le 12-03-2012

                    END;
                END
                ELSE BEGIN
                    TempVendorBuffer.DELETEALL();
                    CLEAR(LSalesPrice);
                    CLEAR(LSalesPrice2);
                    CLEAR(LSalesPrice3);
                    CLEAR(LSalesPrice4);
                    IF Litem."No." <> '' THEN BEGIN

                        TempVendorBuffer2.SetRecordsByMulti(Litem."No.", 999999, '', TempVendorBuffer, opt_TypeCde, FALSE);

                        TempVendorBuffer.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
                        IF TempVendorBuffer.FindSet() THEN;

                        // AD Le 02-07-2014
                        CLEAR(TempVendorBufferP1);
                        CLEAR(TempVendorBufferP2);

                        TempVendorBufferP1 := TempVendorBuffer;
                        IF TempVendorBuffer.NEXT() <> 0 THEN;
                        TempVendorBufferP2 := TempVendorBuffer;
                        // FIN AD Le 02-07-2014

                        LSalesPrice.RESET();
                        LSalesPrice.SETRANGE("Item No.", Litem."No.");
                        LSalesPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, DateDeBase);
                        LSalesPrice.SETRANGE("Starting Date", 0D, DateDeBase);
                        LSalesPrice.SETRANGE("Sales Type", LSalesPrice."Sales Type"::"Customer Price Group");
                        LSalesPrice.SETRANGE("Sales Code", 'EXP');
                        IF LSalesPrice.FINDFIRST() THEN;

                        LSalesPrice2.RESET();
                        LSalesPrice2.SETRANGE("Item No.", Litem."No.");
                        LSalesPrice2.SETFILTER("Ending Date", '%1|>=%2', 0D, DateDeBase);
                        LSalesPrice2.SETRANGE("Starting Date", 0D, DateDeBase);
                        LSalesPrice2.SETRANGE("Sales Type", LSalesPrice2."Sales Type"::"Customer Price Group");
                        LSalesPrice2.SETRANGE("Sales Code", 'EX2');
                        IF LSalesPrice2.FINDFIRST() THEN;

                        LSalesPrice4.RESET();
                        LSalesPrice4.SETRANGE("Item No.", Litem."No.");
                        LSalesPrice4.SETFILTER("Ending Date", '%1|>=%2', 0D, DateDeBase);
                        LSalesPrice4.SETRANGE("Starting Date", 0D, DateDeBase);
                        LSalesPrice4.SETRANGE("Sales Type", LSalesPrice2."Sales Type"::"Customer Price Group");
                        LSalesPrice4.SETRANGE("Sales Code", 'EX3');
                        IF LSalesPrice4.FINDFIRST() THEN;

                        LSalesPrice3.RESET();
                        LSalesPrice3.SETRANGE("Item No.", Litem."No.");
                        LSalesPrice3.SETFILTER("Ending Date", '%1|>=%2', 0D, DateDeBase);
                        LSalesPrice3.SETRANGE("Starting Date", 0D, DateDeBase);
                        LSalesPrice3.SETRANGE("Sales Type", LSalesPrice3."Sales Type"::"All Customers");
                        IF LSalesPrice3.FINDFIRST() THEN;
                    END;

                    // AD Le 24-03-2016 =>
                    LSubstTxt := '';
                    rec_ArtSubst.RESET();
                    IF bln_ShowSubstitution THEN BEGIN
                        rec_ArtSubst.SETRANGE("No.", Litem."No.");
                        rec_ArtSubst.SETRANGE(Type, rec_ArtSubst.Type::Item);
                        IF rec_ArtSubst.FINDFIRST() THEN
                            REPEAT
                                LSubstTxt += MakeSubstInfo(Litem, rec_ArtSubst);
                            UNTIL rec_ArtSubst.NEXT() = 0;
                    END;
                    // Attention ne rien rajouter derière substitut a cause des colonnes


                    FichierCsv.WRITE(
                        MakeBody(Litem, TempArticle)
                      // AD Le 02-07-2014
                      //+ MakePurchInfo(Litem, TempVendorBuffer)
                      + MakePurchInfo(Litem, TempVendorBufferP1)
                      + MakePurchInfo(Litem, TempVendorBufferP2)
                      // FIN AD Le 02-07-2014
                      + MakeSalesInfo(Litem, LSalesPrice3)
                      + MakeSalesInfo(Litem, LSalesPrice)
                      + MakeSalesInfo(Litem, LSalesPrice2)
                      + MakeSalesInfo(Litem, LSalesPrice4)
                      + LSubstTxt
                      );


                END;
            end;

            trigger OnPostDataItem();
            begin
                Window.CLOSE();
            end;

            trigger OnPreDataItem();
            begin

                ArticleTemporaire.SETRANGE(Number, 1, TempArticle.COUNT);

                CLEAR(Window);
                Window.OPEN(ESK003Lbl);

                NbEnr := ArticleTemporaire.COUNT;
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                field(ImportWithExcelName; ImportWithExcel)
                {
                    Caption = 'Import a partir d''un fichier Excel';
                    ToolTip = 'Si cette case n''est pas cochée, le fichier est déposé dans \\srv-fichiers\export cde fou';
                    ApplicationArea = All;
                }
                field(txt_VendorNoName; txt_VendorNo)
                {
                    Caption = 'Nom du fournisseur';
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom du fournisseur field.';
                }
                field(opt_TypeCdeName; opt_TypeCde)
                {
                    Caption = 'Type de commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("ExportDonnéesConsoName"; ExportDonnéesConso)
                {
                    Caption = 'Calcul données Conso';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Calcul données Conso field.';
                }
                field("ExportDonnéesAchatName"; ExportDonnéesAchat)
                {
                    Caption = 'Export données achat';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Export données achat field.';
                }
                field(txt_VendorNoAchatName; txt_VendorNoAchat)
                {
                    Caption = 'Fournisseur (achat)';
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fournisseur (achat) field.';
                }
                field("ExportDonnéesVentesName"; ExportDonnéesVentes)
                {
                    Caption = 'Export données ventes';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Export données ventes field.';
                }
                field(bln_ShowSubstitutionName; bln_ShowSubstitution)
                {
                    Caption = 'Export art. substitution';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Export art. substitution field.';
                }
                field("ExportDonnéesTabTarName"; ExportDonnéesTabTar)
                {
                    Caption = 'Export TabTar';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Export TabTar field.';
                }
                field("VersionSimplifiéeName"; VersionSimplifiée)
                {
                    Caption = 'Version Simplifiée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Version Simplifiée field.';
                }
                field("Fichier Excel"; FileName)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FileName field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var
                        FileMgt: Codeunit "File Management";
                        Text006Lbl: Label 'Import Fichier Excel';
                        ExcelExtensionTok: Label '.xlsx', Locked = true;
                    begin
                        Evaluate(FileName, FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok));


                        //FBRUN le 11/07/2019 -> pour que le fichier soit dispo si fait par le planificateur
                        FileName := MakeExcelcible(FileName);



                        UploadedFileName := FileName;
                    end;
                }
                field("Nom Feuille Excel"; SheetName)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the SheetName field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var
                    begin
                        SheetName := TempExcelBufEntrée.SelectSheetsName(FileName);
                    end;
                }
            }
        }

        trigger OnOpenPage();
        begin
            //FBRUN LE 10/07/2019
            IF GUIALLOWED THEN
                FileName := '';

        end;

        trigger OnQueryClosePage(CloseAction: Action): Boolean;
        var
            FileMgt: Codeunit "File Management";
            Text006Lbl: Label 'Import Fichier Excel';
            ExcelExtensionTok: Label '.xlsx', Locked = true;
        begin
            IF ImportWithExcel THEN
                IF CloseAction = ACTION::OK THEN BEGIN
                    IF FileName = '' THEN
                        Evaluate(FileName, FileMgt.UploadFile(Text006Lbl, ExcelExtensionTok));
                    IF FileName = '' THEN
                        EXIT(FALSE);

                    IF SheetName = '' THEN
                        SheetName := TempExcelBufEntrée.SelectSheetsName(FileName);
                    IF SheetName = '' THEN
                        EXIT(FALSE);
                    IF (FileName <> '') AND (SheetName = '') THEN ERROR('Veuillez choisir une feuille');

                END;

            UploadedFileName := FileName;
        end;
    }


    trigger OnInitReport();
    begin

        //CFR le 15/09/2021 => Régie : ajout champs
        //CFR le 23/03/2023 => Régie : ajout champs global [Description 3]
        nbChampPrincipaux := 49;
        //nbChampPrincipaux := 48;
        //nbChampPrincipaux := 44;
        // CFR le 22/03/2022 => Régie : [Qté mini] > [Qté mini fiche four] & ajout [Qté mini tarif four]
        // CFR le 23/03/2023 => Régie : ajout champs achat [Comment]
        nbChampAchats := 17;
        //nbChampAchats := 16;
        //nbChampAchats := 14;
        nbChampVentes := 6;
        nbChampSubstitution := 2;
        nbChampTabTar := 3;
    end;

    trigger OnPostReport();
    begin
        CreateExcelbook();
        MESSAGE('Traitement terminé');
    end;

    trigger OnPreReport();
    begin

        // MCO Le 09-02-2017


        IF FileName <> '' THEN
            IF UploadedFileName = '' THEN
                UploadedFileName := FileName;
        // FIN
        IF UploadedFileName <> '' THEN BEGIN
            TempExcelBufEntrée.LOCKTABLE();
            TempExcelBufEntrée.RESET();
            TempExcelBufEntrée.DELETEALL();
            ReadExcelSheet();
            COMMIT();

            GetDonneesExcel();
        END;

        MakeExcelInfo();
        DateDeBase := WORKDATE();
    end;

    var
        rec_ArtSubst: Record "Item Substitution";
        TempExcelBufEntrée: Record "Excel Buffer" temporary;
        TempArticle: Record "Tmp Export Article" temporary;
        rec_vendor: Record Vendor;
        GestionMultiref: Codeunit "Gestion Multi-référence";
        FileName: Text[250];
        UploadedFileName: Text[1024];
        SheetName: Text[250];
        "Nom de colonne(premier ligne)": Boolean;
        DateDeBase: Date;
        FichierCsv: File;
        NomFichierCsv: Text[1024];
        NbEnr: Integer;
        NoEnr: Integer;
        Window: Dialog;
        "ExportDonnéesAchat": Boolean;
        "ExportDonnéesVentes": Boolean;
        "ExportDonnéesConso": Boolean;
        txt_kit: Text[1];
        txt_ArtSubst: Text[1];
        bln_ShowSubstitution: Boolean;
        dec_QteMagasin: Decimal;
        dec_QteDepart: Decimal;
        nbChampPrincipaux: Integer;
        nbChampAchats: Integer;
        nbChampVentes: Integer;
        "VersionSimplifiée": Boolean;
        txt_VendorNo: Code[20];
        opt_TypeCde: Option "Niveau 0"," Niveau 1"," Niveau 2"," Niveau 3";
        nbChampSubstitution: Integer;
        nbChampTabTar: Integer;
        "ExportDonnéesTabTar": Boolean;
        txt_VendorNoAchat: Code[20];
        Text015Lbl: Label 'Choisir le fichier';
        ESK003Lbl: Label 'EXPORT  :  #1##########', Comment = '%1';
        ImportWithExcel: Boolean;

    local procedure ReadExcelSheet();
    var
        session: Record "Active Session";
    begin
        //Lecture de la feuille excel
        IF UploadedFileName = '' THEN
            UploadFile()
        ELSE
            Evaluate(FileName, UploadedFileName);

        //FBRUN le 08/07/2019
        IF NOT FILE.EXISTS(FileName) THEN BEGIN
            session.SETRANGE("Session ID", SESSIONID());
            IF session.FINDFIRST() THEN;
            ERROR('(srv %1) Le fichier %2 est introuvable', session."Server Computer Name", FileName);
        END;
        ////fin le 08/07/2019


        TempExcelBufEntrée.OpenBook(FileName, SheetName);
        TempExcelBufEntrée.ReadSheet();
    end;

    procedure UploadFile();
    var
        CommonDialogMgt: Codeunit "File Management";
        ClientFileName: Text[1024];
    begin
        //UploadedFileName := CommonDialogMgt.OpenFile(Text015,ClientFileName,2,'',0);

        Evaluate(UploadedFileName, CommonDialogMgt.UploadFile(Text015Lbl, ClientFileName));
        Evaluate(FileName, UploadedFileName);
    end;

    procedure GetDonneesExcel();
    var
        LInteger: Record Integer;
        LineNo: Integer;
        Excel_A: Code[40];
        Excel_B: Text[50];
        Excel_C: Text[50];
        Excel_D: Text[50];
        ind: Integer;
        ins: Boolean;
    begin

        // Chargement du fichier EXCEL dans la table temporaire

        TempExcelBufEntrée.FINDLAST();
        LInteger.SETRANGE(Number, 2, TempExcelBufEntrée."Row No.");
        LineNo := 1;

        IF "Nom de colonne(premier ligne)" THEN LineNo += 1;

        REPEAT
            // -----------
            // C_Article
            // -----------
            TempExcelBufEntrée.SETRANGE("Row No.", LineNo);
            TempExcelBufEntrée.SETRANGE(xlColID, 'A');
            IF TempExcelBufEntrée.FINDFIRST() THEN
                Evaluate(Excel_A, TempExcelBufEntrée."Cell Value as Text")
            ELSE
                Excel_A := '';

            // -----------
            // B
            // -----------
            TempExcelBufEntrée.SETRANGE("Row No.", LineNo);
            TempExcelBufEntrée.SETRANGE(xlColID, 'B');
            IF TempExcelBufEntrée.FINDFIRST() THEN
                //CFR le 15/09/2021 => Régie : Correction import fichier Excel
                Excel_B := COPYSTR(TempExcelBufEntrée."Cell Value as Text", 1, 50)
            ELSE
                Excel_B := '';
            // -----------
            // C
            // -----------
            TempExcelBufEntrée.SETRANGE("Row No.", LineNo);
            TempExcelBufEntrée.SETRANGE(xlColID, 'C');
            IF TempExcelBufEntrée.FINDFIRST() THEN
                //CFR le 15/09/2021 => Régie : Correction import fichier Excel
                Excel_C := COPYSTR(TempExcelBufEntrée."Cell Value as Text", 1, 50)
            ELSE
                Excel_C := '';
            // -----------
            // D
            // -----------
            TempExcelBufEntrée.SETRANGE("Row No.", LineNo);
            TempExcelBufEntrée.SETRANGE(xlColID, 'D');
            IF TempExcelBufEntrée.FINDFIRST() THEN
                //CFR le 15/09/2021 => Régie : Correction import fichier Excel
                Excel_D := COPYSTR(TempExcelBufEntrée."Cell Value as Text", 1, 50)
            ELSE
                Excel_D := '';


            // On stocke les données pour ré-exporter apres
            IF STRLEN(Excel_A) > 20 THEN
                MESSAGE('Ref %1 non traité car trop longue !! Ligne %2', Excel_A, LineNo)
            ELSE BEGIN
                TempArticle."No." := Excel_A;
                TempArticle."No. 2" := COPYSTR(Excel_A, 1, 40);

                TempArticle."No. 2" := GestionMultiref.RechercheMultiReference(TempArticle."No. 2"); // AD Le 24-03-2016
                IF TempArticle."No. 2" = 'RIENTROUVE' THEN TempArticle."No. 2" := COPYSTR(Excel_A, 1, 20); // AD Le 13-05-2016

                TempArticle.Description := Excel_B;
                TempArticle."Description 2" := Excel_C;
                TempArticle."Libelle Marketing" := Excel_D;
                TempArticle."Shelf No." := 'XLS';
                // LM le 09-08-2012 traitement des doublons
                IF NOT TempArticle.INSERT() THEN BEGIN
                    ind := 0;
                    REPEAT
                        ind := ind + 1;
                        TempArticle."No." := Format(Excel_A + '.' + FORMAT(ind));
                        IF NOT TempArticle.INSERT() THEN ins := FALSE ELSE ins := TRUE;
                        IF ind = 100 THEN ins := TRUE;
                    UNTIL ins = TRUE;
                END;
            END;
            LineNo += 1;

        UNTIL LInteger.NEXT() = 0;
    end;

    procedure "---"();
    begin
    end;

    procedure MakeExcelInfo();
    var
        LcompanyInfo: Record "Company Information";
    begin
        FichierCsv.TEXTMODE(TRUE);
        FichierCsv.WRITEMODE(TRUE);

        LcompanyInfo.GET();

        NomFichierCsv := LcompanyInfo."Export EDI Folder" + '\ExportArticle_' + CONVERTSTR(USERID, '\', '_') + '.csv';
        IF FILE.EXISTS(NomFichierCsv) THEN
            FILE.ERASE(NomFichierCsv);

        FichierCsv.CREATE(NomFichierCsv);

        MakeExcelDataHeader();
    end;

    procedure MakeExcelcible(FichierLocal: Text): Text[250];
    var
        LcompanyInfo: Record "Company Information";
        NomFichierxlsx: Text[250];
    begin

        LcompanyInfo.GET();

        NomFichierxlsx := LcompanyInfo."Export EDI Folder" + '\ImportTMP_' + CONVERTSTR(USERID, '\', '_') + '.xlsx';
        IF FILE.EXISTS(NomFichierxlsx) THEN
            FILE.ERASE(NomFichierxlsx);

        COPY(FichierLocal, NomFichierxlsx);

        EXIT(NomFichierxlsx);
    end;

    local procedure MakeExcelDataHeader();
    begin
        IF NOT VersionSimplifiée THEN
            FichierCsv.WRITE(TitreGlobal() + TitreAchat() + TitreVente() + TitreSubstitution() + TitreTabTar())
        // FichierCsv.WRITE(TitreGlobal + TitreAchat + TitreVente +  'Substitut 1;Substitut 2;Substitut 3;Substitut 4;Substitut 5;Substitut ;Substitut 7;Substitut 8;Substitut 9;Substitut 10')
        ELSE
            FichierCsv.WRITE(TitreGlobal() + TitreAchat() + TitreAchat() + TitreVente() + TitreVente() + TitreVente() + TitreVente() + 'Substitut 1;Substitut 2;Substitut 3;Substitut 4;Substitut 5;Substitut ;Substitut 7;Substitut 8;Substitut 9;Substitut 10');
    end;

    procedure CreateExcelbook();
    begin

        FichierCsv.CLOSE();
    end;

    procedure MakeEntete(_pItem: Record Item): Text[50];
    begin
        EXIT('' + ';' +
              '' + ';' +
              '' + ';' +
              '' + ';' +
              _pItem."No. 2" + ';' +
              _pItem."No." + ';');
    end;

    procedure MakeBody(_pItem: Record Item; _pTempArticle: Record "Tmp Export Article"): Text[1024];
    var
        LTabTar: Record Tab_tar_tmp;
        rec_PurchLine: Record "Purchase Line";
        litem: Record Item;
        recL_BOMComponent: Record "BOM Component";
        CduLFunctions: Codeunit "Codeunits Functions";
        LGestioConso: Codeunit CalculConso;
        "LRéservé": Decimal;
        LAttendu: Decimal;
        LCodeEmplacement: Code[20];
        LCodeZONE: Code[20];
        LConso: Decimal;
        LConsoN: Decimal;
        LConsoN1: Decimal;
        LConsoN2: Decimal;
        LConsoN3: Decimal;
        LConsoN4: Decimal;
        LConsoN5: Decimal;
    // CalcPurchasePrice: Codeunit "Purch. Price Calc. Mgt.";
    // px: Decimal;

    begin
        LTabTar.RESET();
        LTabTar.SETCURRENTKEY(Societe, Status, c_fournisseur, ref_active);
        LTabTar.SETRANGE(c_fournisseur, txt_VendorNo);
        LTabTar.SETRANGE(ref_active, COPYSTR(_pTempArticle."No.", 1, 20));
        // FIN AD Le 19-04-2016

        // MCO Le 08-06-2017 => Si on ne trouve pas on recherche sur la réf fournisseur
        //IF LTabTar.FINDFIRST () THEN;
        IF NOT LTabTar.FINDFIRST() THEN BEGIN
            LTabTar.RESET();
            LTabTar.SETCURRENTKEY(ref_fourni);
            LTabTar.SETRANGE(ref_fourni, COPYSTR(_pTempArticle."No.", 1, 20));
            LTabTar.SETRANGE(c_fournisseur, txt_VendorNo);
            IF LTabTar.FINDFIRST() THEN;
        END;
        // FIN MCO Le 08-06-2017


        // AD Le 21-03-2013
        IF ExportDonnéesTabTar THEN BEGIN
            LTabTar.RESET();
            LTabTar.SETCURRENTKEY(ref_active);
            LTabTar.SETRANGE(ref_active, COPYSTR(_pTempArticle."No. 2", 1, 20));
            IF LTabTar.FINDFIRST() THEN
                REPEAT
                    FichierCsv.WRITE(MakeEntete(litem) +
                                       MakePointVirgule(nbChampPrincipaux) +
                                       MakePointVirgule(nbChampAchats) +
                                       MakePointVirgule(nbChampVentes) +
                                       MakePointVirgule(nbChampSubstitution) +
                                       MakeTabTar(litem, LTabTar));
                UNTIL LTabTar.NEXT() = 0;

            LTabTar.RESET();
            LTabTar.SETCURRENTKEY(ref_fourni);
            LTabTar.SETRANGE(ref_fourni, COPYSTR(_pTempArticle."No. 2", 1, 20));
            IF LTabTar.FINDFIRST() THEN
                REPEAT
                    FichierCsv.WRITE(MakeEntete(litem) +
                                       MakePointVirgule(nbChampPrincipaux) +
                                       MakePointVirgule(nbChampAchats) +
                                       MakePointVirgule(nbChampVentes) +
                                       MakePointVirgule(nbChampSubstitution) +
                                       MakeTabTar(litem, LTabTar));
                UNTIL LTabTar.NEXT() = 0;

        END;
        // FIN AD  Le 21-03-2013



        IF _pItem."No." <> '' THEN BEGIN
            IF ExportDonnéesConso THEN BEGIN
                LConso := LGestioConso.RechecherConsommation(_pItem."No.", CALCDATE('<-1Y>', DateDeBase), DateDeBase); // Conso 12 M glis
                LConsoN := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3)), DateDeBase); // Conso N
                LConsoN1 := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3) - 1),
                                                                             DMY2DATE(31, 12, DATE2DMY(DateDeBase, 3) - 1));
                LConsoN2 := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3) - 2),
                                                                             DMY2DATE(31, 12, DATE2DMY(DateDeBase, 3) - 2));
                LConsoN3 := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3) - 3),
                                                                             DMY2DATE(31, 12, DATE2DMY(DateDeBase, 3) - 3));
                LConsoN4 := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3) - 4),
                                                                             DMY2DATE(31, 12, DATE2DMY(DateDeBase, 3) - 4));
                LConsoN5 := LGestioConso.RechecherConsommation(_pItem."No.", DMY2DATE(1, 1, DATE2DMY(DateDeBase, 3) - 5),
                                                                              DMY2DATE(31, 12, DATE2DMY(DateDeBase, 3) - 5));
            END;

            _pItem.CALCFIELDS(Inventory, "Date Dernière Sortie");
            _pItem.CalcAttenduReserve(LRéservé, LAttendu);

            CduLFunctions.ESK_GetDefaultBin(_pItem."No.", '', 'SP', LCodeEmplacement, LCodeZONE);
        END;

        // DZ le 12-03-2012 => Ajout d'autres champs
        ////IF _pItem."Kit BOM No." <> '' THEN BEGIN
        ////  txt_kit := 'O';
        ////END ELSE BEGIN
        ////  txt_kit := 'N';
        ////END;
        // MCO Le 01-02-2016 => Ticket 7687 :
        recL_BOMComponent.RESET();
        recL_BOMComponent.SETRANGE("Parent Item No.", _pItem."No.");
        IF NOT recL_BOMComponent.ISEMPTY THEN
            txt_kit := 'O'
        ELSE
            txt_kit := 'N';

        // FIN MCO Le 01-02-2016
        CLEAR(rec_ArtSubst);
        rec_ArtSubst.SETRANGE("No.", _pItem."No.");
        rec_ArtSubst.SETRANGE(Type, rec_ArtSubst.Type::Item);
        IF rec_ArtSubst.FINDFIRST() THEN
            txt_ArtSubst := 'O'
        ELSE
            txt_ArtSubst := 'N';


        rec_PurchLine.RESET();
        rec_PurchLine.SETRANGE("No.", _pItem."No.");
        rec_PurchLine.SETRANGE(Type, rec_PurchLine.Type::Item);
        dec_QteMagasin := 0;
        IF rec_PurchLine.FINDFIRST() THEN
            REPEAT
                dec_QteMagasin += rec_PurchLine.GetQtyOnMagasin();
            UNTIL rec_PurchLine.NEXT() = 0;


        // Fin DZ le 12-03-2012

        rec_PurchLine.RESET();
        rec_PurchLine.SETRANGE("Document Type", rec_PurchLine."Document Type"::Order);
        rec_PurchLine.SETRANGE(Type, rec_PurchLine.Type::Item);
        rec_PurchLine.SETRANGE("No.", _pItem."No.");
        rec_PurchLine.SETFILTER("Outstanding Quantity", '<>%1', 0);
        dec_QteDepart := 0;
        IF rec_PurchLine.FINDFIRST() THEN
            REPEAT
                dec_QteDepart += rec_PurchLine.GetQtyOnDepart();
            UNTIL rec_PurchLine.NEXT() = 0;



        rec_vendor.INIT();
        IF rec_vendor.GET(_pItem."Vendor No.") THEN;


        //CalcPurchasePrice.GetBestPurchPrice(_pItem."No.", 10000000, opt_TypeCde, px, ''); // AD Le 02-07-2014



        EXIT(format(
               //CFR le 19/11/2020 : Régie > Export données article
               KeepZero(_pTempArticle."No.") + ';' +
               _pTempArticle.Description + ';' +
               _pTempArticle."Description 2" + ';' +
               _pTempArticle."Libelle Marketing" + ';' +
               //CFR le 19/11/2020 : Régie > Export données article
               KeepZero(_pItem."No. 2") + ';' +
               _pItem."No." + ';' +
               _pItem.Description + ';' +
               _pItem."Description 2" + ';' +
               //CFR le 23/03/2023 => Régie : ajout champs global [Description 3]
               _pItem."Description 3" + ';' +
               //FIN CFR le 23/03/2023
               FormatDecimal(_pItem.Inventory) + ';' +
               FormatDecimal(LAttendu) + ';' +
               FormatDecimal(LRéservé) + ';' +
               //CFR le 15/09/2021 => Régie : remplacement champs
               FormatDecimal(dec_QteMagasin) + ';' + // 12-03-2012
               FormatDecimal(dec_QteDepart) + ';' +
               //FIN CFR le 15/09/2021
               FormatDecimal(LConso) + ';' +
               FormatDecimal(LConsoN) + ';' +
               FormatDecimal(LConsoN1) + ';' +
               FormatDecimal(LConsoN2) + ';' +
               FormatDecimal(LConsoN3) + ';' +
               FormatDecimal(LConsoN4) + ';' +
               FormatDecimal(LConsoN5) + ';' +
               LCodeEmplacement + ';' +
               FORMAT(_pItem.Stocké) + ';' +
               FORMAT(_pItem."date maj stocké") + ';' + //VD 15/03/2016
               _pItem."user maj stocké" + ';' + //VD 15/03/2016
                                                //CFR le 15/09/2021 => Régie : ajout champs
               FORMAT(_pItem."Stock Mort") + ';' +
               FORMAT(_pItem."date maj stock mort") + ';' +
               FORMAT(_pItem.Publiable) + ';' +
               FORMAT(_pItem."Date Modif. Publiable Web") + ';' +
               //FIN CFR le 15/09/2021 => Régie : ajout champs
               FORMAT(_pItem."Controle ADV pour stockage") + ';' + //lm le 02-04-2013
               FormatDecimal(_pItem."Unit Cost") + ';' +
               _pItem."Manufacturer Code" + ';' +
               _pItem."Catégorie produit" + ';' +
               _pItem."Product Type" + ';' +        // 12-03-2012
               _pItem."Code Concurrence" + ';' +         // 12-03-2012
               _pItem."Comodity Code" + ';' +       // 12-03-2012
               _pItem.Nature + ';' +              // 12-03-2012
               _pItem."Fusion en attente sur" + ';' +   // 12-03-2012
               txt_kit + ';' +               // 12-03-2012
               FORMAT(_pItem."Date dernière entrée") + ';' +
               FORMAT(_pItem."Create Date") + ';' +
               FORMAT(_pItem."Date Dernière Sortie") + ';' +
               FormatDecimal(_pItem."Sales multiple") + ';' +
               _pItem."Item Category Code" + ';' +
               //TODO_pItem."Product Group Code" + ';' +
               FORMAT(LTabTar.FINDFIRST()) + ';' +
               FormatDecimal(LTabTar.px_brut) + ';' +                // 12-03-2012
               LTabTar.design_fourni + ';' +         // 12-03-2012
               LTabTar.code_remise + ';' +  // 12-03-2012
               FORMAT(LTabTar.date_tarif) + ';' +  //lm le 17-12-2012
               _pItem."Code Appro" + ';' +
               FORMAT(txt_ArtSubst) + ';' +  // 12-03-2012
               FORMAT(_pItem."Net Weight") + ';' +  //lm le 26-03-2013
               _pItem."Vendor No." + ';' +
               rec_vendor.Name + ';')

        );
    end;

    procedure MakePurchInfo(_pItem: Record Item; _pVendorBuffer: Record "Vendor Price Buffer"): Text[1024];
    var
        LItemVendor: Record "Item Vendor";
    begin
        IF LItemVendor.GET(_pVendorBuffer."Vendor No.", _pVendorBuffer."Item No.", '') THEN;
        EXIT(
                          _pVendorBuffer."Vendor No." + ';' +  // Debut
                          _pVendorBuffer.Name + ';' +
                          FormatDecimal(_pVendorBuffer."Unit Price") + ';' +
                          FormatDecimal(_pVendorBuffer.Discount) + ';' +
                          FormatDecimal4(_pVendorBuffer."Net Unit Price") + ';' + // 4 Decimals
                                                                                  //CFR le 16/12/2020 : Régie > Export données article
                          KeepZero(LItemVendor."Vendor Item No.") + ';' +
                          // MCO Le 08-06-2017 => Régie
                          LItemVendor."Vendor Item Desciption" + ';' +
                          // FIN MCO Le 08-06-2017
                          // CFR le 23/03/2023 => Régie : ajout champs achat [Comment]
                          LItemVendor.Comment + ';' +
                          // FIN CFR le 23/03/2023
                          FormatDecimal(_pVendorBuffer."Minimum order") + ';' +
                          // CFR le 22/03/2022 => Régie : [Qté mini] > [Qté mini fiche four] & ajout [Qté mini tarif four]
                          FormatDecimal(_pVendorBuffer."Minimum Qty") + ';' +
                          // FIN CFR le 22/03/2022
                          FormatDecimal(LItemVendor."Purch. Multiple") + ';' +  // CFR le 23/03/2023 => Régie : change [Order Multiple] en [Purch. Multiple]
                                                                                // AD Le 16-04-2012 => Demande de LM
                                                                                // FORMAT(LItemVendor."Date Mise a Jour") + ';' +
                          FORMAT(_pVendorBuffer."Date début") + ';' +
                          // FIN AD Le 16-04-2012
                          FORMAT(_pVendorBuffer."Date Fin") + ';' +  // AD Le 01-04-2015
                          LItemVendor."Purch. Unit of Measure" + ';' +

                          FORMAT(LItemVendor."Statut Qualité") + ';' +
                          FORMAT(LItemVendor."Statut Approvisionnement") + ';' +
                          FORMAT(LItemVendor."Indirect Cost") + ';'
        );
    end;

    procedure MakeSalesInfo(_pItem: Record Item; _pSalesPrice: Record "Sales Price"): Text[1024];
    begin
        // Export Excel
        IF _pSalesPrice."Sales Type" = _pSalesPrice."Sales Type"::"All Customers" THEN
            _pSalesPrice."Sales Code" := FORMAT(_pSalesPrice."Sales Type");


        EXIT(
              _pSalesPrice."Sales Code" + ';' + // DEBUT VENTES

              FORMAT(_pSalesPrice."Starting Date") + ';' +
              FORMAT(_pSalesPrice."Ending Date") + ';' +
              FormatDecimal(_pSalesPrice."Prix catalogue") + ';' +
              FormatDecimal(_pSalesPrice.Coefficient) + ';' +
              FormatDecimal(_pSalesPrice."Unit Price") + ';'
        );
    end;

    procedure MakeSubstInfo(_pItem: Record Item; _pArtSubst: Record "Item Substitution"): Text[250];
    var
        rec_ItemDesc: Record Item;
    begin

        IF rec_ItemDesc.GET(_pArtSubst."Substitute No.") THEN;

        EXIT(
              GestionMultiref.RechercheRefActive(_pArtSubst."Substitute No.") + ';'  // DEBUT ART SUBST
                                                                                     //   rec_ItemDesc.Description + ';'
          );
    end;

    procedure MakePointVirgule(_pNbPoint: Integer): Text[1024];
    var
        LLigne: Text[150];
        i: Integer;
    begin

        LLigne := '';

        FOR i := 1 TO _pNbPoint DO
            LLigne += ';';

        EXIT(LLigne);
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;

    procedure FormatDecimal4(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,4:><Sign><Integer><Decimals>'));
    end;

    local procedure TitreGlobal(): Text[1024];
    begin
        // Export Excel
        EXIT(
            'C1' + ';' +
            'C2' + ';' +
            'C3' + ';' +
            'C4' + ';' +
            'Ref. Active' + ';' +
            'Ref. SP' + ';' +
            'Description' + ';' +
            'Infos Achat' + ';' +
            //CFR le 23/03/2023 => Régie : ajout champs global [Description 3]
            'Infos Vente' + ';' +
            //FIN CFR le 23/03/2023
            'Physique' + ';' +
            'Attendu' + ';' +
            'Réservé' + ';' +
            //CFR le 15/09/2021 => Régie : remplacement champs
            'Qte Pièces à Quai' + ';' +   // 12-03-2012
            'Depart' + ';' +
            //FIN CFR le 15/09/2021
            'Consommation 12 M Glissants' + ';' +
            'Consommation N' + ';' + // 26-01-2012
            'Consommation N-1' + ';' + // 26-01-2012
            'Consommation N-2' + ';' + // 26-01-2012
            'Consommation N-3' + ';' + // 26-01-2012
            'Consommation N-4' + ';' + // 26-01-2012
            'Consommation N-5' + ';' + // 26-01-2012
            'Casier' + ';' +
            'Stocké' + ';' +
            'Date MAJ Stocké' + ';' + //VD 15/03/2016
            'User MAJ Stocké' + ';' + //VD 15/03/2016
                                      //CFR le 15/09/2021 => Régie : ajout champs
            'Stock Mort' + ';' +
            'Date maj Stock Mort' + ';' +
            'Publiable Web' + ';' +
            'Date Modif. Publiable Web' + ';' +
            //FIN CFR le 15/09/2021 => Régie : ajout champs
            'Controle ADV' + ';' + //02-04-2013
            'Pamp' + ';' +
            'Marque' + ';' +
            'Catégorie' + ';' +
            'Type Produit' + ';' +        // 12-03-2012
                                          //CFR le 22/03/2022 => Régie : [Comodity Code] > [PCC/CNH] & [Code concurrencé] > [MPL - MPC/CNH]
            'MPL - MPC/CNH' + ';' +       //'Code Concur' + ';' +         // 12-03-2012
            'PCC/CNH' + ';' +             //'Comodity Code' + ';' +       // 12-03-2012
                                          //FIN CFR le 22/03/2022
            'Nature' + ';' +              // 12-03-2012
            'Fusion en attente' + ';' +   // 12-03-2012
            'Kit' + ';' +                 // 12-03-2012
            'Date Entrée' + ';' +
            'Date Création' + ';' +        // 26-01-2012
            'Date Dernière Sortie' + ';' + // 26-01-2012
            'Multiple de Vente' + ';' +
            'Famille' + ';' +
            'Sous Famille' + ';' +
            'Connu NH' + ';' +
            'Prix BRUT ' + txt_VendorNo + ';' +                // 12-03-2012
            'Désignation ' + txt_VendorNo + ';' +         // 12-03-2012
            'Code Remise ' + txt_VendorNo + ';' +  // 12-03-2012
            'Date tarif  ' + txt_VendorNo + ';' + //lm le 17-12-2012
            'Code Appro' + ';' + // 26-01-2012
            'Article de substitution' + ';' + // 12-03-2012
            'Poids net' + ';' + //26-03-2013
            'Fournisseur conseillé' + ';' +
            'Nom fournisseur' + ';'
        );
    end;

    local procedure TitreAchat(): Text[500];
    begin
        // Export Excel
        EXIT(
            'Code Fournisseur' + ';' +
            'Nom Fournisseur' + ';' +
            'Prix Brut' + ';' +
            'Remise' + ';' +
            'Prix Net' + ';' +
            'Réf. Fournisseur' + ';' +
            // MCO Le 08-06-2017 => Régie
            'Désignation fournisseur' + ';' +
            // FIN MCO Le 08-06-2017 => Régie
            // CFR le 23/03/2023 => Régie : ajout champs achat [Comment]
            'Commentaire' + ';' +
            // FIN CFR le 23/03/2023
            // CFR le 22/03/2022 => Régie : [Qté mini] > [Qté mini fiche four] & ajout [Qté mini tarif four]
            'Qté Mini fiche four (Qté minimum commande)' + ';' +
            'Qté Mini tarif four (Qté minimum)' + ';' +
            // FIN CFR le 22/03/2022
            'Multiple d''achat' + ';' + // 26-01-2012
            'Date Tarif' + ';' + // 26-01-2012
            'Date Fin Tarif' + ';' +
            'Unité Achat' + ';' +
            'Statut Qualité' + ';' +
            'Statut Appro' + ';' +  // CFR le 10/05/2022 => Régie : [Unité Appro] > [Statut Appro]
            'Coût indirect' + ';'
        );
    end;

    local procedure TitreVente(): Text[250];
    begin
        // Export Excel
        EXIT(
            'Code Tarifs' + ';' +
            'Date début tarif' + ';' +
            'Date fin tarif' + ';' +
            'Prix Catalogue' + ';' +
            'Coéf.' + ';' +
            'Prix Standard' + ';'
        );
    end;

    local procedure TitreSubstitution(): Text[250];
    begin
        // Export Excel
        EXIT(
            'N° Article Substitution' + ';' +                 // 12-03-2012
            'Desc. Article Substitution' + ';'                 // 12-03-2012
        );
    end;

    procedure MakeTabTar(_pItem: Record Item; _pTabTar: Record Tab_tar_tmp): Text[250];
    begin

        EXIT(
              _pTabTar.ref_active + ';' + // DEBUT ART TarbTar
                                          //CFR le 16/12/2020 : Régie > Export données article
              KeepZero(_pTabTar.ref_fourni) + ';' +
              _pTabTar.c_fournisseur + ';'
          );
    end;

    local procedure TitreTabTar(): Text[250];
    begin
        // Export Excel
        EXIT(
            'Ref. Active' + ';' +                 // 12-03-2012
            'Ref. Fournisseur' + ';' +                // 12-03-2012
            'Fournisseur' + ';'
        );
    end;

    local procedure KeepZero(ptext: Text): Text;
    begin
        //CFR le 19/11/2020 : Régie > Export données article
        EXIT('="' + ptext + '"');
    end;
}

