report 50152 "Maj prix de vente / tab_tar"
{

    ProcessingOnly = true;
    Caption = 'Maj prix de vente / tab_tar';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                LtabTar: Record Tab_tar_tmp;
                // LItemUOM: Record "Item Unit of Measure";
                LTarifClient: Record "Sales Price";
            begin

                // Lectue de tab_tar
                LtabTar.RESET();
                LtabTar.SETFILTER(Societe, '%1|%2', 'SP', '');
                LtabTar.SETRANGE(c_fournisseur, Vendor."No.");
                LtabTar.SETRANGE(ref_active, Item."No. 2");
                LtabTar.SETRANGE(Status, 'N');
                IF LtabTar.FINDFIRST() THEN BEGIN
                    Item.VALIDATE(Description, LtabTar.design_fourni);
                    Item.VALIDATE("Comodity Code", LtabTar.comodity_code);
                    // DEMANDE DE LM => Item.VALIDATE("Item Category Code", LtabTar.code_remise);

                    //CFR le 10/05/2022 => Régie : [MPL - MPC/CNH] provient de [Divers 4] = Marketing Product Code
                    Item.VALIDATE("Code Concurrence", LtabTar.divers4);
                    //FIN CFR le 10/05/2022

                    Item.MODIFY(TRUE);

                    /* AD Le 02-12-2011 => Demande de LM
                    LItemUOM.GET(Item."No.", Item."Base Unit of Measure");
                    LItemUOM.VALIDATE(Weight, LtabTar.poids / 100);
                    LItemUOM.MODIFY(TRUE);
                    */

                    LTarifClient.INIT();
                    LTarifClient.VALIDATE("Item No.", Item."No.");
                    LTarifClient.VALIDATE("Type de commande", LTarifClient."Type de commande"::Tous);
                    LTarifClient.VALIDATE("Sales Type", LTarifClient."Sales Type"::"All Customers");
                    LTarifClient.VALIDATE("Prix catalogue", LtabTar.px_brut);
                    LTarifClient.VALIDATE(Coefficient, 1);
                    LTarifClient.VALIDATE("Unit Price", ROUND(LtabTar.px_brut, 0.01));
                    LTarifClient.VALIDATE("Unit of Measure Code", Item."Base Unit of Measure");
                    LTarifClient.VALIDATE("Starting Date", LtabTar.date_tarif);
                    IF LTarifClient."Starting Date" = 0D THEN
                        LTarifClient.VALIDATE("Starting Date", WORKDATE());

                    LTarifClient.VALIDATE("Price Includes VAT", FALSE);
                    LTarifClient.VALIDATE("Allow Invoice Disc.", TRUE);
                    LTarifClient.VALIDATE("Allow Line Disc.", TRUE);
                    LTarifClient.INSERT(TRUE);
                    Item.CloturerAncienPrix(LTarifClient);

                    MESSAGE(ESK001Lbl, Item."No. 2", LtabTar.code_remise);

                END;

            end;

            trigger OnPreDataItem();
            begin

                IF VendorNo = '' THEN
                    ERROR('Fournisseur Obligatoire !');


                Vendor.GET(VendorNo);

                IF NOT CONFIRM('Mise à jour des prix de vente à partir de tab_tar ?') THEN
                    EXIT;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(Fournisseur; VendorNo)
                {
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    Caption = 'Fournisseur';
                    ToolTip = 'Specifies the value of the Fournisseur field.';
                }
            }
        }
    }

    var
        Vendor: Record Vendor;
        ESK001Lbl: Label 'Code Famille Article %1 -> %2', Comment = '%1 = No. 2 Article, %2 = code_remise';
        VendorNo: Code[20];
}

