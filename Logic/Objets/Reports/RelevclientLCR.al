report 50063 "Relevé client->LCR"
{
    Caption = 'Proposer règlements client';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Sales Statment Header"; "Sales Statment Header")
        {
            DataItemTableView = SORTING("Statement No.", "Customer No.")
                                WHERE("Statement Amount" = FILTER(> 0));
            RequestFilterFields = "Statement Date", "Customer No.";
            dataitem(Customer; Customer)
            {
                DataItemLink = "No." = FIELD("Customer No.");
                DataItemTableView = SORTING("No.")
                                    WHERE("Payment Method Code" = FILTER('TRA|RLC|LCR'));

                trigger OnAfterGetRecord();
                begin

                    Window.UPDATE(1, "No.");
                    GetCustLedgEntries(TRUE, FALSE);
                    GetCustLedgEntries(FALSE, FALSE);
                    CheckAmounts(FALSE);
                end;

                trigger OnPostDataItem();
                begin

                    IF UsePaymentDisc THEN BEGIN
                        RESET();
                        COPYFILTERS(Cust2);
                        Window.OPEN(Text007Lbl);
                        IF Customer.FIND('-') THEN
                            REPEAT
                                Window.UPDATE(1, Customer."No.");
                                TempPayableCustLedgEntry.SETRANGE("Vendor No.", Customer."No.");
                                GetCustLedgEntries(TRUE, TRUE);
                                GetCustLedgEntries(FALSE, TRUE);
                                CheckAmounts(TRUE);
                            UNTIL NEXT() = 0;
                    END;

                    GenPayLine.LOCKTABLE();
                    GenPayLine.SETRANGE("No.", GenPayLine."No.");
                    IF GenPayLine.FIND('+') THEN BEGIN
                        FirstLineNo := GenPayLine."Line No.";
                        LastLineNo := GenPayLine."Line No.";
                        GenPayLine.INIT();
                    END;

                    Window.OPEN(Text008Lbl);

                    TempPayableCustLedgEntry.RESET();
                    TempPayableCustLedgEntry.SETRANGE(Priority, 1, 2147483647);
                    MakeGenPayLines();
                    TempPayableCustLedgEntry.RESET();
                    TempPayableCustLedgEntry.SETRANGE(Priority, 0);
                    MakeGenPayLines();
                    TempPayableCustLedgEntry.RESET();
                    TempPayableCustLedgEntry.DELETEALL();

                    //Window.CLOSE;
                    ShowMessage(MessageText);
                end;
            }

            trigger OnPostDataItem();
            begin
                Window.CLOSE();
            end;

            trigger OnPreDataItem();
            begin

                GenPayLineInserted := FALSE;
                SeveralCurrencies := FALSE;
                MessageText := '';

                SummarizePer := SummarizePer::"Due date";

                Cust2.COPYFILTERS(Customer);

                Window.OPEN(Text006Lbl);

                NextEntryNo := 1;
            end;
        }
    }

    var
        Cust2: Record Customer;
        PaymentClass: Record "Payment Class";
        GenPayHead: Record "Payment Header";
        GenPayLine: Record "Payment Line";
        CustLedgEntry: Record "Cust. Ledger Entry";
        TempPayableCustLedgEntry: Record "Payable Vendor Ledger Entry" temporary;
        TempPaymentPostBuffer: Record "Payment Post. Buffer" temporary;
        TempOldPaymentPostBuffer: Record "Payment Post. Buffer" temporary;
        NoSeriesMgt: Codeunit NoSeriesManagement;
        CustEntryEdit: Codeunit "Cust. Entry-Edit";
        Window: Dialog;
        UsePaymentDisc: Boolean;
        PostingDate: Date;

        LastDueDateToPayReq: Date;
        NextDocNo: Code[20];
        SummarizePer: Option " ",Customer,"Due date";
        FirstLineNo: Integer;
        LastLineNo: Integer;
        NextEntryNo: Integer;
        MessageText: Text[250];
        GenPayLineInserted: Boolean;
        SeveralCurrencies: Boolean;

        CurrencyFilter: Code[10];

        Text006Lbl: Label 'Processing customers     #1##########', Comment = '%1 = ########';
        Text007Lbl: Label 'Processing customers for payment discounts #1##########' , Comment = '%1 = ####';
        Text008Lbl: Label 'Inserting payment journal lines #1##########', Comment = '%1 = #####';
        Text016Lbl: Label ' is already applied to %1 %2 for customer %3.', comment = '%1 = Document Type ; %2 ="Document No."; %3 =Customer No';


    procedure SetGenPayLine(NewGenPayLine: Record "Payment Header");
    begin
        GenPayHead := NewGenPayLine;
        GenPayLine."No." := NewGenPayLine."No.";
        PaymentClass.GET(GenPayHead."Payment Class");
        PostingDate := GenPayHead."Posting Date";
        CurrencyFilter := GenPayHead."Currency Code";
    end;

    procedure GetCustLedgEntries(Positive: Boolean; Future: Boolean);
    begin
        CustLedgEntry.RESET();
        CustLedgEntry.SETCURRENTKEY("Customer No.", Open, Positive, "Due Date");
        CustLedgEntry.SETRANGE("Customer No.", Customer."No.");
        CustLedgEntry.SETRANGE(Open, TRUE);
        CustLedgEntry.SETRANGE(Positive, Positive);
        CustLedgEntry.SETRANGE("Currency Code", CurrencyFilter);
        CustLedgEntry.SETRANGE("Applies-to ID", '');

        /*
        IF "Type date"="Type date"::"Date d'echéance" THEN
        BEGIN
          IF Future THEN BEGIN
            CustLedgEntry.SETRANGE("Due Date",LastDueDateToPayReq+1,31129999D);
            CustLedgEntry.SETRANGE("Pmt. Discount Date",PostingDate,LastDueDateToPayReq);
            CustLedgEntry.SETFILTER("Original Pmt. Disc. Possible",'<0');
          END ELSE
            CustLedgEntry.SETRANGE("Due Date",FirstDueDateToPayReq,LastDueDateToPayReq);
        END
         ELSE
         BEGIN
          CustLedgEntry.SETCURRENTKEY("Customer No.",Open,"Posting Date");
          CustLedgEntry.SETRANGE("Posting Date","Date comptabilité début","Date comptabilité fin");
         END;
        
        
        CustLedgEntry.SETRANGE("On Hold",'');
        */

        CustLedgEntry.SETCURRENTKEY("Customer No.", "Statement No.");
        CustLedgEntry.SETRANGE("Statement No.", "Sales Statment Header"."Statement No.");


        IF CustLedgEntry.FIND('-') THEN
            REPEAT
                SaveAmount();
            UNTIL CustLedgEntry.NEXT() = 0;

    end;

    local procedure SaveAmount();
    begin
        WITH GenPayLine DO BEGIN
            "Account Type" := "Account Type"::Customer;
            VALIDATE("Account No.", CustLedgEntry."Customer No.");
            "Posting Date" := CustLedgEntry."Posting Date";
            "Currency Factor" := CustLedgEntry."Adjusted Currency Factor";
            IF "Currency Factor" = 0 THEN
                "Currency Factor" := 1;
            VALIDATE("Currency Code", CustLedgEntry."Currency Code");
            CustLedgEntry.CALCFIELDS("Remaining Amount");
            IF (CustLedgEntry."Document Type" = CustLedgEntry."Document Type"::Invoice) AND
               (PostingDate <= CustLedgEntry."Pmt. Discount Date")
            THEN
                Amount := -(CustLedgEntry."Remaining Amount" - CustLedgEntry."Original Pmt. Disc. Possible")
            ELSE
                Amount := -CustLedgEntry."Remaining Amount";
            VALIDATE(Amount);
        END;

        TempPayableCustLedgEntry."Vendor No." := CustLedgEntry."Customer No.";
        TempPayableCustLedgEntry."Entry No." := NextEntryNo;
        TempPayableCustLedgEntry."Vendor Ledg. Entry No." := CustLedgEntry."Entry No.";
        TempPayableCustLedgEntry.Amount := GenPayLine.Amount;
        TempPayableCustLedgEntry."Amount (LCY)" := GenPayLine."Amount (LCY)";
        TempPayableCustLedgEntry.Positive := (TempPayableCustLedgEntry.Amount > 0);
        TempPayableCustLedgEntry.Future := (CustLedgEntry."Due Date" > LastDueDateToPayReq);
        TempPayableCustLedgEntry."Currency Code" := CustLedgEntry."Currency Code";
        //TempPayableCustLedgEntry."Due Date" := CustLedgEntry."Due Date";
        TempPayableCustLedgEntry."Due Date" := "Sales Statment Header"."Due Date";
        TempPayableCustLedgEntry.Insert();
        NextEntryNo := NextEntryNo + 1;
    end;

    procedure CheckAmounts(Future: Boolean);
    var
        CurrencyBalance: Decimal;
        PrevCurrency: Code[10];
    begin
        TempPayableCustLedgEntry.SETRANGE("Vendor No.", Customer."No.");
        TempPayableCustLedgEntry.SETRANGE(Future, Future);
        IF TempPayableCustLedgEntry.FIND('-') THEN BEGIN
            PrevCurrency := TempPayableCustLedgEntry."Currency Code";
            REPEAT
                IF TempPayableCustLedgEntry."Currency Code" <> PrevCurrency THEN BEGIN
                    IF CurrencyBalance < 0 THEN BEGIN
                        TempPayableCustLedgEntry.SETRANGE("Currency Code", PrevCurrency);
                        TempPayableCustLedgEntry.DELETEALL();
                        TempPayableCustLedgEntry.SETRANGE("Currency Code");
                    END;
                    CurrencyBalance := 0;
                    PrevCurrency := TempPayableCustLedgEntry."Currency Code";
                END;
                CurrencyBalance := CurrencyBalance + TempPayableCustLedgEntry."Amount (LCY)"
            UNTIL TempPayableCustLedgEntry.NEXT() = 0;
            IF CurrencyBalance > 0 THEN BEGIN
                TempPayableCustLedgEntry.SETRANGE("Currency Code", PrevCurrency);
                TempPayableCustLedgEntry.DELETEALL();
                TempPayableCustLedgEntry.SETRANGE("Currency Code");
            END;
        END;
        TempPayableCustLedgEntry.RESET();
    end;

    local procedure MakeGenPayLines();
    var
        GenPayLine3: Record "Gen. Journal Line";
        //EntryNo: Integer;
    begin
        SummarizePer := SummarizePer::"Due date";
        TempPaymentPostBuffer.DELETEALL();

        IF TempPayableCustLedgEntry.FIND('-') THEN
            REPEAT
                TempPayableCustLedgEntry.SETRANGE("Vendor No.", TempPayableCustLedgEntry."Vendor No.");
                TempPayableCustLedgEntry.FIND('-');
                REPEAT
                    CustLedgEntry.GET(TempPayableCustLedgEntry."Vendor Ledg. Entry No.");
                    TempPaymentPostBuffer."Account No." := CustLedgEntry."Customer No.";
                    TempPaymentPostBuffer."Currency Code" := CustLedgEntry."Currency Code";
                    IF SummarizePer = SummarizePer::"Due date" THEN
                        TempPaymentPostBuffer."Due Date" := CustLedgEntry."Due Date";

                    TempPaymentPostBuffer."Dimension Entry No." := 0;
                    TempPaymentPostBuffer."Global Dimension 1 Code" := '';
                    TempPaymentPostBuffer."Global Dimension 2 Code" := '';

                    IF SummarizePer IN [SummarizePer::Customer, SummarizePer::"Due date"] THEN BEGIN
                        TempPaymentPostBuffer."Auxiliary Entry No." := 0;
                        IF TempPaymentPostBuffer.FIND() THEN BEGIN
                            TempPaymentPostBuffer.Amount := TempPaymentPostBuffer.Amount + TempPayableCustLedgEntry.Amount;
                            TempPaymentPostBuffer."Amount (LCY)" := TempPaymentPostBuffer."Amount (LCY)" + TempPayableCustLedgEntry."Amount (LCY)";
                            TempPaymentPostBuffer.MODIFY();
                        END ELSE BEGIN
                            LastLineNo := LastLineNo + 10000;
                            TempPaymentPostBuffer."Payment Line No." := LastLineNo;
                            IF PaymentClass."Line No. Series" = '' THEN
                                Evaluate(NextDocNo , (GenPayHead."No." + '/' + FORMAT(LastLineNo)))
                            ELSE
                                NextDocNo := NoSeriesMgt.GetNextNo(PaymentClass."Line No. Series", PostingDate, FALSE);
                            TempPaymentPostBuffer."Document No." := NextDocNo;
                            NextDocNo := INCSTR(NextDocNo);
                            TempPaymentPostBuffer.Amount := TempPayableCustLedgEntry.Amount;
                            TempPaymentPostBuffer."Amount (LCY)" := TempPayableCustLedgEntry."Amount (LCY)";
                            Window.UPDATE(1, CustLedgEntry."Customer No.");
                            TempPaymentPostBuffer.Insert();
                        END;
                        CustLedgEntry."Applies-to ID" := TempPaymentPostBuffer."Document No.";
                        CustEntryEdit.RUN(CustLedgEntry)
                    END ELSE BEGIN
                        GenPayLine3.RESET();
                        GenPayLine3.SETCURRENTKEY(
                          "Account Type", "Account No.", "Applies-to Doc. Type", "Applies-to Doc. No.");
                        GenPayLine3.SETRANGE("Account Type", GenPayLine3."Account Type"::Customer);
                        GenPayLine3.SETRANGE("Account No.", CustLedgEntry."Customer No.");
                        GenPayLine3.SETRANGE("Applies-to Doc. Type", CustLedgEntry."Document Type");
                        GenPayLine3.SETRANGE("Applies-to Doc. No.", CustLedgEntry."Document No.");
                        IF GenPayLine3.FIND('-') THEN
                            GenPayLine3.FIELDERROR(
                              "Applies-to Doc. No.",
                              STRSUBSTNO(
                                Text016Lbl,
                                CustLedgEntry."Document Type", CustLedgEntry."Document No.",
                                CustLedgEntry."Customer No."));

                        TempPaymentPostBuffer."Applies-to Doc. Type" := CustLedgEntry."Document Type";
                        TempPaymentPostBuffer."Applies-to Doc. No." := CustLedgEntry."Document No.";
                        TempPaymentPostBuffer."Currency Factor" := CustLedgEntry."Adjusted Currency Factor";
                        TempPaymentPostBuffer.Amount := TempPayableCustLedgEntry.Amount;
                        TempPaymentPostBuffer."Amount (LCY)" := TempPayableCustLedgEntry."Amount (LCY)";
                        TempPaymentPostBuffer."Global Dimension 1 Code" := CustLedgEntry."Global Dimension 1 Code";
                        TempPaymentPostBuffer."Global Dimension 2 Code" := CustLedgEntry."Global Dimension 2 Code";
                        TempPaymentPostBuffer."Auxiliary Entry No." := CustLedgEntry."Entry No.";
                        Window.UPDATE(1, CustLedgEntry."Customer No.");
                        TempPaymentPostBuffer.Insert();
                    END;
                    CustLedgEntry.CALCFIELDS(CustLedgEntry."Remaining Amount");
                    CustLedgEntry."Amount to Apply" := CustLedgEntry."Remaining Amount";
                    CustEntryEdit.RUN(CustLedgEntry);
                UNTIL TempPayableCustLedgEntry.NEXT() = 0;
                TempPayableCustLedgEntry.SETFILTER("Vendor No.", '>%1', TempPayableCustLedgEntry."Vendor No.");
            UNTIL NOT TempPayableCustLedgEntry.FIND('-');

        CLEAR(TempOldPaymentPostBuffer);
        TempPaymentPostBuffer.SETCURRENTKEY("Document No.");
        IF TempPaymentPostBuffer.FIND('-') THEN
            REPEAT
                WITH GenPayLine DO BEGIN
                    INIT();
                    Window.UPDATE(1, TempPaymentPostBuffer."Account No.");
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        LastLineNo := LastLineNo + 10000;
                        "Line No." := LastLineNo;
                        IF PaymentClass."Line No. Series" = '' THEN
                            Evaluate(NextDocNo ,(GenPayHead."No." + '/' + FORMAT(GenPayLine."Line No.")))
                        ELSE
                            NextDocNo := NoSeriesMgt.GetNextNo(PaymentClass."Line No. Series", PostingDate, FALSE);
                    END ELSE BEGIN
                        "Line No." := TempPaymentPostBuffer."Payment Line No.";
                        NextDocNo := TempPaymentPostBuffer."Document No.";
                    END;
                    "Document No." := NextDocNo;
                    GenPayLine."Applies-to ID" := "Document No.";
                    TempOldPaymentPostBuffer := TempPaymentPostBuffer;
                    TempOldPaymentPostBuffer."Document No." := "Document No.";
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        CustLedgEntry.GET(TempPaymentPostBuffer."Auxiliary Entry No.");
                        CustLedgEntry."Applies-to ID" := NextDocNo;
                        CustLedgEntry.MODIFY();
                    END;
                    "Account Type" := "Account Type"::Customer;
                    VALIDATE("Account No.", TempPaymentPostBuffer."Account No.");
                    "Currency Code" := TempPaymentPostBuffer."Currency Code";
                    Amount := TempPaymentPostBuffer.Amount;
                    IF Amount > 0 THEN
                        "Debit Amount" := Amount
                    ELSE
                        "Credit Amount" := -Amount;
                    "Amount (LCY)" := TempPaymentPostBuffer."Amount (LCY)";
                    "Currency Factor" := TempPaymentPostBuffer."Currency Factor";
                    IF ("Currency Factor" = 0) AND (Amount <> 0) THEN
                        "Currency Factor" := Amount / "Amount (LCY)";
                    Cust2.GET(GenPayLine."Account No.");
                    VALIDATE(GenPayLine."Bank Account Code", Cust2."Preferred Bank Account Code");
                    "Payment Class" := GenPayHead."Payment Class";
                    VALIDATE("Status No.");
                    "Posting Date" := PostingDate;
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        "Applies-to Doc. Type" := CustLedgEntry."Document Type";
                        "Applies-to Doc. No." := CustLedgEntry."Document No.";
                    END;
                    CASE SummarizePer OF
                        SummarizePer::" ":
                            "Due Date" := CustLedgEntry."Due Date";
                        SummarizePer::Customer:
                            BEGIN
                                TempPayableCustLedgEntry.SETCURRENTKEY("Vendor No.", "Due Date");
                                TempPayableCustLedgEntry.SETRANGE("Vendor No.", TempPaymentPostBuffer."Account No.");
                                TempPayableCustLedgEntry.FIND('+');
                                "Due Date" := TempPayableCustLedgEntry."Due Date";
                                TempPayableCustLedgEntry.DELETEALL();
                            END;
                        SummarizePer::"Due date":
                            "Due Date" := TempPaymentPostBuffer."Due Date";
                    END;

                    IF Cust2."Payment Method Code" = 'TRA' THEN
                        "Acceptation Code" := "Acceptation Code"::No
                    ELSE
                        "Acceptation Code" := "Acceptation Code"::LCR;
                    IF Amount <> 0 THEN
                        INSERT();
                    GenPayLineInserted := TRUE;
                END;
            UNTIL TempPaymentPostBuffer.NEXT() = 0;
    end;

    local procedure ShowMessage(var Text: Text[250]);
    begin
        IF (Text <> '') AND GenPayLineInserted THEN
            MESSAGE(Text);
    end;
}

