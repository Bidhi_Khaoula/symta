report 50030 "Packing List"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Packing List.rdlc';

    Caption = 'Sales - Shipment';
    PreviewMode = PrintLayout;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem(PreReport; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfo1Picture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfoPictureFooter; CompanyInfo."Bottom Picture")
            {
            }

            trigger OnAfterGetRecord();
            begin
                // ANI ESKVN1.0 => logo entête et pied de page
                CompanyInfo.GET();
                IF PrintLogo THEN BEGIN
                    CompanyInfo.CALCFIELDS(Picture);
                    //CompanyInfo.CALCFIELDS("Picture Footer");
                    CompanyInfo.CALCFIELDS("Header Picture", "Bottom Picture");
                END;
                GLSetup.GET();
                // FIN ANI ESKVN1.0
            end;
        }
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Sell-to Customer No.", "No. Printed";
            RequestFilterHeading = 'Posted Sales Shipment';
            column(No_SalesShptHeader; "No.")
            {
            }
            column(PageCaption; PageCaptionCapLbl)
            {
            }
            column(InvDiscAmtCaption; InvDiscAmtCaptionLbl)
            {
            }
            column(AmountCaption; AmountCaptionLbl)
            {
            }
            column(VATPercentageCaption; VATPercentageCaptionLbl)
            {
            }
            column(VATBaseCaption; VATBaseCaptionLbl)
            {
            }
            column(VATAmtCaption; VATAmtCaptionLbl)
            {
            }
            column(VATAmtSpecCaption; VATAmtSpecCaptionLbl)
            {
            }
            column(LineAmtCaption; LineAmtCaptionLbl)
            {
            }
            column(TotalCaption; TotalText)
            {
            }
            column(UnitPriceCaption; UnitPriceCaptionLbl)
            {
            }
            column(NetUnitPriceCaption; NetUnitPriceCaptionLbl)
            {
            }
            column(no_fax; no_fax)
            {
            }
            column(Objet_fax; Objet_fax)
            {
            }
            column(Attention_fax; Attention_fax)
            {
            }
            column(nompdf; nompdf)
            {
            }
            dataitem(CopyLoop; Integer)
            {
                DataItemTableView = SORTING(Number);
                dataitem(PageLoop; Integer)
                {
                    DataItemTableView = SORTING(Number)
                                        WHERE(Number = CONST(1));
                    column(SalesShptCopyText; GetDocumentCaption())
                    {
                    }
                    column(RightAddr1; ShipToAddr[1])
                    {
                    }
                    column(CompanyAddr1; CompanyAddr[1])
                    {
                    }
                    column(RightAddr2; ShipToAddr[2])
                    {
                    }
                    column(CompanyAddr2; CompanyAddr[2])
                    {
                    }
                    column(RightAddr3; ShipToAddr[3])
                    {
                    }
                    column(CompanyAddr3; CompanyAddr[3])
                    {
                    }
                    column(RightAddr4; ShipToAddr[4])
                    {
                    }
                    column(CompanyAddr4; CompanyAddr[4])
                    {
                    }
                    column(RightAddr5; ShipToAddr[5])
                    {
                    }
                    column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
                    {
                    }
                    column(RightAddr6; ShipToAddr[6])
                    {
                    }
                    column(CompanyInfoHomePage; CompanyInfo."Home Page")
                    {
                    }
                    column(CompanyInfoEmail; CompanyInfo."E-Mail")
                    {
                    }
                    column(CompanyInfoFaxNo; CompanyInfo."Fax No.")
                    {
                    }
                    column(CompanyInfoVATRegtnNo; CompanyInfo."VAT Registration No.")
                    {
                    }
                    column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
                    {
                    }
                    column(CompanyInfoBankName; CompanyInfo."Bank Name")
                    {
                    }
                    column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
                    {
                    }
                    column(SelltoCustNo_SalesShptHeader; "Sales Shipment Header"."Sell-to Customer No.")
                    {
                    }
                    column(DocDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Document Date", 0, DateFormat))
                    {
                    }
                    column(SalesPersonText; SalesPersonText)
                    {
                    }
                    column(SalesPurchPersonName; SalesPurchPerson.Name)
                    {
                    }
                    column(ReferenceText; ReferenceText)
                    {
                    }
                    column(YourRef_SalesShptHeader; "Sales Shipment Header"."Your Reference")
                    {
                    }
                    column(RightAddr7; ShipToAddr[7])
                    {
                    }
                    column(RightAddr8; ShipToAddr[8])
                    {
                    }
                    column(CompanyAddr5; CompanyAddr[5])
                    {
                    }
                    column(CompanyAddr6; CompanyAddr[6])
                    {
                    }
                    column(ShptDate_SalesShptHeader; FORMAT("Sales Shipment Header"."Shipment Date", 0, DateFormat))
                    {
                    }
                    column(OutputNo; OutputNo)
                    {
                    }
                    column(ItemTrackingAppendixCaption; ItemTrackingAppendixCaptionLbl)
                    {
                    }
                    column(PhoneNoCaption; PhoneNoCaptionLbl)
                    {
                    }
                    column(VATRegNoCaption; VATRegNoCaptionLbl)
                    {
                    }
                    column(GiroNoCaption; GiroNoCaptionLbl)
                    {
                    }
                    column(BankNameCaption; BankNameCaptionLbl)
                    {
                    }
                    column(BankAccNoCaption; BankAccNoCaptionLbl)
                    {
                    }
                    column(ShipmentNoCaption; ShipmentNoCaptionLbl)
                    {
                    }
                    column(ShipmentDateCaption; ShipmentDateCaptionLbl)
                    {
                    }
                    column(HomePageCaption; HomePageCaptionLbl)
                    {
                    }
                    column(EmailCaption; EmailCaptionLbl)
                    {
                    }
                    column(DocumentDateCaption; DocumentDateCaptionLbl)
                    {
                    }
                    column(SelltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Sell-to Customer No."))
                    {
                    }
                    column(LeftAddr1; SellToAddr[1])
                    {
                    }
                    column(LeftAddr2; SellToAddr[2])
                    {
                    }
                    column(LeftAddr3; SellToAddr[3])
                    {
                    }
                    column(LeftAddr4; SellToAddr[4])
                    {
                    }
                    column(LeftAddr5; SellToAddr[5])
                    {
                    }
                    column(LeftAddr6; SellToAddr[6])
                    {
                    }
                    column(LeftAddr7; SellToAddr[7])
                    {
                    }
                    column(LeftAddr8; SellToAddr[8])
                    {
                    }
                    column(LeftAddrCaption; SelltoAddrCaptionLbl)
                    {
                    }
                    column(MiddleAddrCaption; BilltoAddrCaptionLbl)
                    {
                    }
                    column(HeaderField01Caption; TotalPackageLbl)
                    {
                    }
                    column(HeaderField02Caption; '')
                    {
                    }
                    column(HeaderField03Caption; TotalVolumeLbl)
                    {
                    }
                    column(HeaderField04Caption; '')
                    {
                    }
                    column(HeaderField05Caption; TotalGrossWeightCaptionLbl)
                    {
                    }
                    column(HeaderField06Caption; '')
                    {
                    }
                    column(HeaderField07Caption; TotalNetWeightCaptionLbl)
                    {
                    }
                    column(HeaderField08Caption; '')
                    {
                    }
                    column(HeaderField09Caption; '')
                    {
                    }
                    column(HeaderField10Caption; '')
                    {
                    }
                    column(HeaderField01; gTempPackingList."Package Number")
                    {
                    }
                    column(HeaderField02; '')
                    {
                    }
                    column(HeaderField03; gTempPackingList."Total Volume")
                    {
                    }
                    column(HeaderField04; '')
                    {
                    }
                    column(HeaderField05; gTempPackingList."Calculate Header Weight")
                    {
                    }
                    column(HeaderField06; '')
                    {
                    }
                    column(HeaderField07; gTempPackingList."Theoretical Weight")
                    {
                    }
                    column(HeaderField08; '')
                    {
                    }
                    column(HeaderField09; '')
                    {
                    }
                    column(HeaderField10; '')
                    {
                    }
                    column(MiddleAddr1; CustAddr[1])
                    {
                    }
                    column(MiddleAddr2; CustAddr[2])
                    {
                    }
                    column(MiddleAddr3; CustAddr[3])
                    {
                    }
                    column(MiddleAddr4; CustAddr[4])
                    {
                    }
                    column(MiddleAddr5; CustAddr[5])
                    {
                    }
                    column(MiddleAddr6; CustAddr[6])
                    {
                    }
                    column(MiddleAddr7; CustAddr[7])
                    {
                    }
                    column(MiddleAddr8; CustAddr[8])
                    {
                    }
                    column(PricesInclVAT_SalesShptHeader; "Sales Shipment Header"."Prices Including VAT")
                    {
                    }
                    column(SalesOrdersCaption; SalesOrdersCaptionLbl)
                    {
                    }
                    column(SalesShipmentsCaption; SalesShipmentsCaptionLbl)
                    {
                    }
                    column(SalesInvociesCaption; SalesInvoicesCaptionLbl)
                    {
                    }
                    column(ActiveRefCaption; ActiveRefCaptionLbl)
                    {
                    }
                    column(SpRefeCaption; SpRefeCaptionLbl)
                    {
                    }
                    column(TotalGrossWeightCaption; TotalGrossWeightCaptionLbl)
                    {
                    }
                    column(TotalNetWeightCaption; TotalNetWeightCaptionLbl)
                    {
                    }
                    dataitem(PackingListLine2; "Packing List Line")
                    {
                        DataItemLink = "Packing List No." = FIELD("Packing List No."),
                                       "Posted Source No." = FIELD("No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                            WHERE("Line Quantity" = FILTER(<> 0));
                        dataitem("Posted Whse. Shipment Header"; "Posted Whse. Shipment Header")
                        {
                            DataItemLink = "Packing List No." = FIELD("Packing List No.");
                            DataItemTableView = SORTING("No.")
                                                WHERE("Packing List No." = FILTER(<> ''));
                            dataitem("Posted Whse. Shipment Line"; "Posted Whse. Shipment Line")
                            {
                                DataItemLink = "No." = FIELD("No.");
                                DataItemTableView = SORTING("No.", "Line No.")
                                                    WHERE("Posted Source Document" = CONST("Posted Shipment"));
                                dataitem("Sales Header"; "Sales Header")
                                {
                                    DataItemLink = "No." = FIELD("Source No.");
                                    DataItemTableView = SORTING("Document Type", "No.")
                                                        WHERE("Document Type" = CONST(Order));
                                    column(No_SalesHeader; "Sales Header"."No.")
                                    {
                                    }

                                    trigger OnAfterGetRecord();
                                    begin
                                        //>> SFD20201005
                                        FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                            IF "Sales Header"."No." = gDocNo[gY] THEN
                                                CurrReport.SKIP();
                                        gI := COMPRESSARRAY(gDocNo) + 1;
                                        gDocNo[gI] := "Sales Header"."No.";
                                        //<< SFD20201005
                                    end;
                                }
                                dataitem(SalesShipmentHeader2; "Sales Shipment Header")
                                {
                                    DataItemLink = "No." = FIELD("Posted Source No.");
                                    DataItemTableView = SORTING("No.");
                                    column(No_SalesShipmentHeader2; SalesShipmentHeader2."No.")
                                    {
                                    }
                                    dataitem("Sales Invoice Line"; "Sales Invoice Line")
                                    {
                                        DataItemLink = "Shipment No." = FIELD("No.");
                                        DataItemLinkReference = SalesShipmentHeader2;
                                        DataItemTableView = SORTING("Document No.", "N° client Livré", "Shipment No.")
                                                            WHERE(Type = CONST(Item),
                                                                  "Shipment No." = FILTER(<> ''));
                                        column(DocumentNo_SalesInvoiceLine; "Sales Invoice Line"."Document No.")
                                        {
                                        }

                                        trigger OnAfterGetRecord();
                                        begin
                                            //>> SFD20201005
                                            FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                                IF "Sales Invoice Line"."Document No." = gDocNo[gY] THEN
                                                    CurrReport.SKIP();
                                            gI := COMPRESSARRAY(gDocNo) + 1;
                                            gDocNo[gI] := "Sales Invoice Line"."Document No.";
                                            //<< SFD20201005
                                        end;
                                    }

                                    trigger OnAfterGetRecord();
                                    begin
                                        //>> SFD20201005
                                        FOR gY := 1 TO ARRAYLEN(gDocNo) DO
                                            IF SalesShipmentHeader2."No." = gDocNo[gY] THEN
                                                CurrReport.SKIP();
                                        gI := COMPRESSARRAY(gDocNo) + 1;
                                        gDocNo[gI] := SalesShipmentHeader2."No.";
                                        //<< SFD20201005
                                    end;
                                }
                            }
                        }

                        trigger OnPreDataItem();
                        begin
                            gI := 0;
                        end;
                    }
                    dataitem("Packing List Line"; "Packing List Line")
                    {
                        CalcFields = "Ref. Active";
                        DataItemLink = "Packing List No." = FIELD("Packing List No.");
                        DataItemLinkReference = "Sales Shipment Header";
                        DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                            WHERE("Line Quantity" = FILTER(<> 0));
                        column(NoExp_PackingList; "Packing List Line"."Whse. Shipment No.")
                        {
                        }
                        column(NumColis_PackingList; "Packing List Line"."Package No.")
                        {
                        }
                        column(NumTypeColis_PackingList; STRSUBSTNO('%2 : %1', "Package No.", DetailColis."Package Type"))
                        {
                        }
                        column(CommentColis; CommentColis)
                        {
                        }
                        column(Comment2Colis; Comment2Colis)
                        {
                        }
                        column(TxtDetailColis; TxtDétailColis)
                        {
                        }
                        column(TxtPoidsBrutColis; STRSUBSTNO(Text50001Lbl, DetailColis."Package Weight"))
                        {
                        }
                        column(TxtCodeDouanier; TxtCodeDouanier)
                        {
                        }
                        column(NoArticle_PackingList; "Packing List Line"."Item No.")
                        {
                        }
                        column(TxtDescription; TxtDescription)
                        {
                        }
                        column("QtéColis_PackingList"; "Packing List Line"."Line Quantity")
                        {
                        }
                        column(CodeCaption; CodeCaptionLbl)
                        {
                        }
                        column(DescCaption; DescCaptionLbl)
                        {
                        }
                        column(QtyCaption; QtyCaptionLbl)
                        {
                        }
                        column(RefActive_PackingListLine; "Packing List Line"."Ref. Active")
                        {
                        }
                        column(CodeSP_PackingListLine; CodeSP)
                        {
                        }
                        dataitem(Total; Integer)
                        {
                            DataItemTableView = SORTING(Number)
                                                WHERE(Number = CONST(1));
                            column(PackageWeight_PackingListLine; TempPackingListPackage."Package Weight")
                            {
                            }
                            column(NetWeight_PackingListLine; TempPackingListPackage."Net Weight")
                            {
                            }

                            trigger OnPreDataItem();
                            begin
                                IF TempPackingListPackage.FINDSET() THEN
                                    TempPackingListPackage.CALCSUMS("Package Weight", "Net Weight");
                            end;
                        }

                        trigger OnAfterGetRecord();
                        /* var
                            "GestionMultiréférence": Codeunit "Gestion Multi-référence"; */
                        begin
                            IF DetailColis.GET("Packing List Line"."Packing List No.", "Packing List Line"."Package No.") THEN BEGIN
                                DetailColis.CALCFIELDS("Package Quantity", "Package Weight Auto");
                                TxtDétailColis := STRSUBSTNO(Text50000Lbl, DetailColis.Length, DetailColis.Width, DetailColis.Height, DetailColis."Package Quantity");
                                CommentColis := DetailColis."Comment 1";
                                Comment2Colis := DetailColis."Comment 2";

                                // AD Le 02-07-2015 =>
                                IF DetailColis."Outside Of EC" THEN
                                    TxtDétailColis := TxtDétailColis + ' ' + ESK002Lbl
                                ELSE
                                    TxtDétailColis := TxtDétailColis + ' ' + ESK003Lbl;
                                // FIN AD Le 02-07-2015
                            END
                            ELSE BEGIN
                                TxtDétailColis := '';
                                CommentColis := '';
                                Comment2Colis := '';
                            END;



                            // AD Le 06-07-2007 => Gestion designation1 + designation2
                            item.GET("Packing List Line"."Item No.");
                            TxtDescription := "Packing List Line".Description + ' ' + "Packing List Line"."Description 2";


                            // AD Le 04-04-2013 => Idem BL
                            //TxtCodeDouanier := CU_GestionEdition.GetPoidsInfoDouanière("Sales Shipment Header"."Sell-to Customer No.",
                            //  "Packing List Line"."Item No.");

                            IF NOT ((arExport) AND (item."Tariff No." <> '')) THEN TxtCodeDouanier := '';
                            //>> SFD20201005
                            CalcTotals("Packing List Line");
                            //<< SFD20201005
                        end;

                        trigger OnPreDataItem();
                        begin
                            "Nb colis" := 0;
                        end;
                    }
                    dataitem(Total2; Integer)
                    {
                        DataItemTableView = SORTING(Number)
                                            WHERE(Number = CONST(1));
                        column(BilltoCustNo_SalesShptHeader; "Sales Shipment Header"."Bill-to Customer No.")
                        {
                        }
                        column(BilltoCustNo_SalesShptHeaderCaption; "Sales Shipment Header".FIELDCAPTION("Bill-to Customer No."))
                        {
                        }

                        trigger OnPreDataItem();
                        begin
                            IF NOT ShowCustAddr THEN
                                CurrReport.BREAK();
                        end;
                    }

                    trigger OnPreDataItem();
                    begin
                        // Item Tracking:
                        IF ShowLotSN THEN BEGIN
                            TrackingSpecCount := 0;
                            OldRefNo := 0;
                            ShowGroup := FALSE;
                        END;
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    LOldSalesShptLine: Record "Sales Shipment Line";
                begin
                    // ANI ESKVN1.0 => BL Chiffré / TVA - ShowPrices
                    CLEAR(TempSalesShptLine);
                    TempVATAmountLine.DELETEALL();
                    TempSalesShptLine.DELETEALL();

                    // GetSalesShptLines
                    LOldSalesShptLine.SETRANGE("Document No.", "Sales Shipment Header"."No.");
                    IF LOldSalesShptLine.FINDSET() THEN
                        REPEAT
                            TempSalesShptLine := LOldSalesShptLine;
                            TempSalesShptLine.Insert();
                        UNTIL LOldSalesShptLine.NEXT() = 0;

                    VATAmount := TempVATAmountLine.GetTotalVATAmount();
                    VATBaseAmount := TempVATAmountLine.GetTotalVATBase();
                    VATDiscountAmount :=
                      TempVATAmountLine.GetTotalVATDiscount("Sales Shipment Header"."Currency Code", "Sales Shipment Header"."Prices Including VAT");
                    TotalAmountInclVAT := TempVATAmountLine.GetTotalAmountInclVAT();
                    // FIN ANI ESKVN1.0

                    IF Number > 1 THEN BEGIN
                        //CopyText := FormatDocument.GetCOPYText;
                        CopyText := Text001Lbl;
                        OutputNo += 1;
                    END;
                    TotalQty := 0;           // Item Tracking

                    // ANI ESKVN1.0 => BL Chiffré / TVA - ShowPrices
                    NNCTotalExclVAT := 0;
                    NNCVATAmt := 0;
                    NNCSalesLineLineAmt := 0;
                    // FIN ANI ESKVN1.0

                    WhseEmplName := GetWhseEmployeeName("Sales Shipment Header");

                    // AD Le 11-10-2012 => Gestion des types de colis => Vue avec Virginie + Eric on limite a 4 type
                    //GetColis("Sales Shipment Header"."No.");
                end;

                trigger OnPostDataItem();
                begin
                    IF NOT CurrReport.PREVIEW THEN
                        CODEUNIT.RUN(CODEUNIT::"Sales Shpt.-Printed", "Sales Shipment Header");
                end;

                trigger OnPreDataItem();
                begin
                    NoOfLoops := 1 + ABS(NoOfCopies);
                    CopyText := '';
                    SETRANGE(Number, 1, NoOfLoops);
                    OutputNo := 1;
                end;
            }

            trigger OnAfterGetRecord();
            begin
                IF gTempPackingList.GET("Sales Shipment Header"."Packing List No.") THEN
                    gTempPackingList.CALCFIELDS("Calculate Header Weight", "Net Header Weight", "Theoretical Weight", "Total Volume", "Package Number");

                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");

                ShowPrices := "Sales Shipment Header"."Abandon remainder";

                FormatAddressFields("Sales Shipment Header");
                FormatDocumentFields("Sales Shipment Header");

                DimSetEntry1.SETRANGE("Dimension Set ID", "Dimension Set ID");

                IF LogInteraction THEN
                    IF NOT CurrReport.PREVIEW THEN
                        SegManagement.LogDocument(
                          5, "No.", 0, 0, DATABASE::Customer, "Sell-to Customer No.", "Salesperson Code",
                          "Campaign No.", "Posting Description", '');



                IF (PrintForFax) THEN
                    CU_GestionEdition."GetInfoFax-Pdf"('BL', "Sales Shipment Header"."No.", no_fax, Objet_fax, Attention_fax, nompdf);

                IF (PrintForFax) AND (no_fax = '') THEN
                    ERROR('Le no de fax est vide');

                // AD Le 25-03-2007 => Possibilité d'imprimer avec un logo sans faxer
                IF PrintLogo AND (NOT PrintForFax) THEN BEGIN
                    no_fax := '';
                    Objet_fax := '';
                    nompdf := '';
                END;


                //AD on regarde si export pour les code douanier
                IF "Sales Shipment Header"."VAT Bus. Posting Group" = 'SANS TVA' THEN
                    arExport := TRUE
                ELSE
                    arExport := FALSE;
            end;

            trigger OnPreDataItem();
            // var
            //     _Rec: RecordRef;
            //     _CodeSoc: Code[20];
            begin
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(NoOfCopies; NoOfCopies)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'No. of Copies';
                        ToolTip = 'Specifies how many copies of the document to print.';
                    }
                    field(ShowInternalInfo; ShowInternalInfo)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Internal Information';
                        ToolTip = 'Specifies if the document shows internal information.';
                    }
                    field(LogInteraction; LogInteraction)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Log Interaction';
                        Enabled = LogInteractionEnable;
                        ToolTip = 'Specifies if you want to record the reports that you print as interactions.';
                    }
                    field("Show Correction Lines"; ShowCorrectionLines)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Correction Lines';
                        ToolTip = 'Specifies if the correction lines of an undoing of quantity posting will be shown on the report.';
                    }
                    field(ShowLotSN; ShowLotSN)
                    {
                        ApplicationArea = Advanced;
                        Caption = 'Show Serial/Lot Number Appendix';
                        ToolTip = 'Specifies if you want to print an appendix to the sales shipment report showing the lot and serial numbers in the shipment.';
                    }
                    field(DisplayAsmInfo; DisplayAssemblyInformation)
                    {
                        ApplicationArea = Assembly;
                        Caption = 'Show Assembly Components';
                        ToolTip = 'Specifies if you want the report to include information about components that were used in linked assembly orders that supplied the item(s) being sold.';
                    }
                    field(PrintLogo; PrintLogo)
                    {
                        Caption = 'Print logo';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Print logo field.';
                    }
                    field(ShowPrices; ShowPrices)
                    {
                        Caption = 'Show prices';
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show prices field.';
                    }
                    group(Fax)
                    {
                        Caption = 'Fax';
                        Visible = false;
                        field(PrintForFax; PrintForFax)
                        {
                            Caption = 'Impression fax';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Impression fax field.';
                        }
                        field(no_fax; no_fax)
                        {
                            Caption = 'N°';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° field.';
                        }
                        field(Attention_fax; Attention_fax)
                        {
                            Caption = 'A l''attention de';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the A l''attention de field.';
                        }
                        field(nompdf; nompdf)
                        {
                            Caption = 'Nom du fichier';
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Nom du fichier field.';
                        }
                    }
                }
            }
        }
        trigger OnInit();
        begin
            LogInteractionEnable := TRUE;
        end;

        trigger OnOpenPage();
        begin
            InitLogInteraction();
            LogInteractionEnable := LogInteraction;

            InitPageOption();
        end;
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET();
        SalesSetup.GET();
        //FormatDocument.SetLogoPosition(SalesSetup."Logo Position on Documents",CompanyInfo1,CompanyInfo2,CompanyInfo3);
    end;

    trigger OnPreReport();
    begin
        IF NOT CurrReport.USEREQUESTPAGE THEN
            InitLogInteraction();
        AsmHeaderExists := FALSE;
    end;

    var
        SalesPurchPerson: Record "Salesperson/Purchaser";
        CompanyInfo: Record "Company Information";
        TempVATAmountLine: Record "VAT Amount Line" temporary;
        /*  Currency: Record Currency;
         CompanyInfo1: Record "Company Information";
         CompanyInfo2: Record "Company Information";
         CompanyInfo3: Record "Company Information"; */
        //Succursale: Record "Responsibility Center";
        SalesSetup: Record "Sales & Receivables Setup";
        DimSetEntry1: Record "Dimension Set Entry";
        TempSalesShptLine: Record "Sales Shipment Line" temporary;
        TempPackingListPackage: Record "Packing List Package" temporary;
        /*   DimSetEntry2: Record "Dimension Set Entry";

          TrackingSpecBuffer: Record "Tracking Specification" temporary;
          PostedAsmHeader: Record "Posted Assembly Header";
          PostedAsmLine: Record "Posted Assembly Line";
   */
        RespCenter: Record "Responsibility Center";
        //   ItemTrackingAppendix: Report "Item Tracking Appendix";
        Language_G: Codeunit Language;
        FormatAddr: Codeunit "Format Address";
        FormatDocument: Codeunit "Format Document";
        SegManagement: Codeunit SegManagement;
        // ItemTrackingDocMgt: Codeunit "Item Tracking Doc. Management";

        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SalesPersonText: Text[20];
        ReferenceText: Text[80];
        // MoreLines: Boolean;
        // Text000Lbl: Label 'Salesperson';
        Text001Lbl: Label 'COPY';
        //   Text002: Label 'Packing list';

        NoOfCopies: Integer;
        OutputNo: Integer;
        NoOfLoops: Integer;
        TrackingSpecCount: Integer;
        OldRefNo: Integer;
        // OldNo: Code[20];
        CopyText: Text[30];
        ShowCustAddr: Boolean;
        //   DimText: Text[120];
        //  OldDimText: Text[75];
        ShowInternalInfo: Boolean;
        //  Continue: Boolean;
        LogInteraction: Boolean;
        ShowCorrectionLines: Boolean;
        ShowLotSN: Boolean;
        //   ShowTotal: Boolean;
        ShowGroup: Boolean;
        TotalQty: Decimal;
        LogInteractionEnable: Boolean;
        DisplayAssemblyInformation: Boolean;
        AsmHeaderExists: Boolean;
        //  LinNo: Integer;
        Text003Lbl: Label 'Total %1', Comment = '%1 = Currency';
        Text004Lbl: Label 'Total %1 Incl. VAT', Comment = '%1 = Currency';
        Text005Lbl: Label 'Total %1 Excl. VAT', Comment = '%1 = Currency';
        ItemTrackingAppendixCaptionLbl: Label 'Item Tracking - Appendix';
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Reg. No.';
        GiroNoCaptionLbl: Label 'Giro No.';
        BankNameCaptionLbl: Label 'Bank';
        BankAccNoCaptionLbl: Label 'Account No.';
        ShipmentNoCaptionLbl: Label 'Shipment No.';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        HomePageCaptionLbl: Label 'Home Page';
        EmailCaptionLbl: Label 'Email';
        DocumentDateCaptionLbl: Label 'Document Date';
        //   HeaderDimensionsCaptionLbl: Label 'Header Dimensions';
        //  LineDimensionsCaptionLbl: Label 'Line Dimensions';
        BilltoAddrCaptionLbl: Label 'Bill-to Address';
        // SerialNoCaptionLbl: Label 'Serial No.';
        // LotNoCaptionLbl: Label 'Lot No.';
        // DescriptionCaptionLbl: Label 'Description';
        //  NoCaptionLbl: Label 'No.';
        PageCaptionCapLbl: Label 'Page %1 of %2', Comment = '%1 = Page Num ; %2 = Page Total Nb';

        PrintLogo: Boolean;
        SellToAddr: array[8] of Text[50];
        //   OrderCreateUser: Record User;

        /* _LineDisc: Text[13];
        ShipmentMethodCaptionLbl: Label 'Shipment Method';
 */
        SelltoAddrCaptionLbl: Label 'Ship-to Address';
        /*    OrderCreateUserCaptionLbl: Label 'Registered by';
           PaymentMethodDescriptionCaptionLbl: Label 'Payment Method';
           CustomerNoCaptionLbl: Label 'Customer No.';
           ExternalDocNoCaptionLbl: Label 'External document No';
           UOMCaptionLbl: Label 'Unit'; */
        UnitPriceCaptionLbl: Label 'Unit Price';
        NetUnitPriceCaptionLbl: Label 'Net Price';
        //   VATAmtLineTTCAmtCaptionLbl: Label 'Incl. VAT';
        ShowPrices: Boolean;

        VATAmount: Decimal;
        VATBaseAmount: Decimal;
        VATDiscountAmount: Decimal;
        /*    InvDiscBaseAmtCaptionLbl: Label 'Invoice Discount Base Amount';
           VATIdentifierCaptionLbl: Label 'VAT Identifier'; */
        VATPercentageCaptionLbl: Label 'VAT %';
        VATBaseCaptionLbl: Label 'VAT Base';
        VATAmtCaptionLbl: Label 'VAT Amount';
        VATAmtSpecCaptionLbl: Label 'VAT Amount Specification';
        LineAmtCaptionLbl: Label 'Line Amount';
        InvDiscAmtCaptionLbl: Label 'Invoice Discount Amount';
        TotalText: Text[50];
        TotalAmountInclVAT: Decimal;
        //     DiscountPercentCaptionLbl: Label 'Disc. %';
        AmountCaptionLbl: Label 'Amount';
        TotalExclVATText: Text[50];
        TotalInclVATText: Text[50];
        //  NNCTotalLCY: Decimal;
        NNCTotalExclVAT: Decimal;
        NNCVATAmt: Decimal;
        //    NNCTotalInclVAT: Decimal;
        NNCSalesLineLineAmt: Decimal;
        /*   NNCSalesLineInvDiscAmt: Decimal;
          _LineAmount: Decimal;
          _LineDiscAmount: Decimal;
          _NetUnitPrice: Decimal;
          _SalsLinAmtExclLineDiscAmt: Decimal; */
        ShippingAgent: Record "Shipping Agent";
        //   ShippingAgentCaptionLbl: Label 'Shipping Agent';
        ShippingAgentServices: Record "Shipping Agent Services";
        //    ShippingAgentSrvCaptionLbl: Label 'Shipping Agent Services';
        ShipmentMethod: Record "Shipment Method";
        /*    QtyOrdered: Decimal;
           QtyRemainsToDeliver: Decimal;
           QtyOrderedCaptionLbl: Label 'Ord. Qty';
           QtyRemainsToDeliverCptLbl: Label 'RTD Qty';
           QtyShippedCaptionLbl: Label 'Shp. Qty';
           QuantityCaptionLbl: Label 'Quantity';
    */
        GLSetup: Record "General Ledger Setup";
        SalesHeader: Record "Sales Header";
        CodeUtilisateurCreation: Code[20];
        DateFormat: Label '<day,2>/<month,2>/<Year4>';
        /*   LongText: Text;
          SalesLineDescr: Text;
          LocationCaptionLbl: Label 'Location';
          RequestedDeliveryDateCaptionLbl: Label 'Requested Delivery Date';
          WhseEmplNameCaptionLbl: Label 'Warehouse Employee'; */
        WhseEmplName: Text;
        /*  WeightCaptionLbl: Label 'Poids brut :';
         NetWeightCaptionLbl: Label 'Poids net :';
         PackageCountCaptionLbl: Label 'Nombre de colis :';
         ItemCrossRefNoCaptionLbl: Label 'Réf. externe';
         TotalNetWeight: Decimal;
         TabColis: array[5] of Text[30];
         "--- FAX ---": Integer; */
        CU_GestionEdition: Codeunit "Gestion Editions / fax / Pdf";
        no_fax: Text[30];
        nompdf: Text[80];
        Objet_fax: Text[100];
        Attention_fax: Text[50];
        PrintForFax: Boolean;
        TxtCodeDouanier: Text;
        "Nb colis": Integer;
        DetailColis: Record "Packing List Package";
        "TxtDétailColis": Text[100];
        CommentColis: Text[60];
        Comment2Colis: Text[60];
        item: Record Item;
        TxtDescription: Text[101];
        Text50000Lbl: Label 'L : %1 cm - l : %2 cm - h : %3 cm. %4 Article(s).', Comment = '%1 = longeur ; %2 = Largeur ; %3 = Poids ; %4 = Qty';
        Text50001Lbl: Label 'Packing Gross Weight : %1 Kg', Comment = '%1 = Poids';
        ESK002Lbl: Label 'Palette Hors CE';
        ESK003Lbl: Label 'Palette CE';
        arExport: Boolean;
        CodeCaptionLbl: Label 'Code';
        DescCaptionLbl: Label 'Désignation';
        QtyCaptionLbl: Label 'Qté livrée';
        /*   VATRegNoHdrCaptionLbl: Label 'V.A.T. Number  %1';
          gOldSalesHeaderNo: Code[20];
          gOldSalesShipmentNo: Code[20];
          gOldSalesInvoiceNo: Code[20]; */
        gI: Integer;
        gY: Integer;
        gDocNo: array[64] of Code[20];
        SalesOrdersCaptionLbl: Label 'Sales Orders';
        SalesInvoicesCaptionLbl: Label 'Sales Invoices';
        SalesShipmentsCaptionLbl: Label 'Sales Shipments';
        ActiveRefCaptionLbl: Label 'Active Reference';
        SpRefeCaptionLbl: Label 'SP Reference';
        CodeSP: Code[40];
        TotalGrossWeightCaptionLbl: Label 'Total Gross Weight';
        TotalNetWeightCaptionLbl: Label 'Total Net Weight';

        TotalVolumeLbl: Label 'Total Volume';
        TotalPackageLbl: Label 'Number of Package';
        gTempPackingList: Record "Packing List Header";

    procedure InitLogInteraction();
    begin
        LogInteraction := SegManagement.FindInteractionTemplateCode("Interaction Log Entry Document Type".FromInteger(5)) <> '';
    end;

    procedure InitializeRequest(NewNoOfCopies: Integer; NewShowInternalInfo: Boolean; NewLogInteraction: Boolean; NewShowCorrectionLines: Boolean; NewShowLotSN: Boolean; DisplayAsmInfo: Boolean);
    begin
        NoOfCopies := NewNoOfCopies;
        ShowInternalInfo := NewShowInternalInfo;
        LogInteraction := NewLogInteraction;
        ShowCorrectionLines := NewShowCorrectionLines;
        ShowLotSN := NewShowLotSN;
        DisplayAssemblyInformation := DisplayAsmInfo;
    end;

    local procedure FormatAddressFields(SalesShipmentHeader: Record "Sales Shipment Header");
    var
        lCustomer: Record Customer;
    begin
        FormatAddr.GetCompanyAddr(SalesShipmentHeader."Responsibility Center", RespCenter, CompanyInfo, CompanyAddr);
        FormatAddr.SalesShptShipTo(ShipToAddr, SalesShipmentHeader);
        ShowCustAddr := FormatAddr.SalesShptBillTo(CustAddr, ShipToAddr, SalesShipmentHeader);

        // ANI ESKVN1.0
        FormatAddr.SalesShptSellTo(SellToAddr, "Sales Shipment Header");
        //IF NOT ShowCustAddr THEN CLEAR(CustAddr);
        // FIN ANI ESKVN1.0
        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Invoice Customer No.") THEN
            lCustomer.INIT();
        IF CustAddr[7] = '' THEN CustAddr[7] := SalesShipmentHeader."Invoice Customer No.";
        //IF CustAddr[8] = '' THEN CustAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");

        CLEAR(lCustomer);
        IF NOT lCustomer.GET(SalesShipmentHeader."Sell-to Customer No.") THEN
            lCustomer.INIT();
        IF SellToAddr[7] = '' THEN SellToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF SellToAddr[8] = '' THEN SellToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
        IF ShipToAddr[7] = '' THEN ShipToAddr[7] := SalesShipmentHeader."Sell-to Customer No.";
        //IF ShipToAddr[8] = '' THEN ShipToAddr[8] := STRSUBSTNO(VATRegNoHdrCaptionLbl, lCustomer."VAT Registration No.");
    end;

    local procedure FormatDocumentFields(SalesShipmentHeader: Record "Sales Shipment Header");
    begin
        WITH SalesShipmentHeader DO BEGIN
            FormatDocument.SetSalesPerson(SalesPurchPerson, "Salesperson Code", SalesPersonText);
            ReferenceText := FormatDocument.SetText("Your Reference" <> '', FIELDCAPTION("Your Reference"));

            // ANI ESKVN1.0
            // BL Chiffré / TVA - ShowPrices
            IF "Currency Code" = '' THEN BEGIN
                GLSetup.TESTFIELD("LCY Code");
                TotalText := STRSUBSTNO(Text003Lbl, GLSetup."LCY Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, GLSetup."LCY Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, GLSetup."LCY Code");
            END ELSE BEGIN
                TotalText := STRSUBSTNO(Text003Lbl, "Currency Code");
                TotalInclVATText := STRSUBSTNO(Text004Lbl, "Currency Code");
                TotalExclVATText := STRSUBSTNO(Text005Lbl, "Currency Code");
            END;

            FormatDocument.SetShipmentMethod(ShipmentMethod, "Shipment Method Code", SalesShipmentHeader."Language Code");
            SetShippingAgent(ShippingAgent, "Shipping Agent Code");
            SetShippingAgentServices(ShippingAgentServices, "Shipping Agent Code", "Shipping Agent Service Code");

            SalesHeader.RESET();
            CodeUtilisateurCreation := '';
            /*
            IF SalesHeader.GET(SalesHeader."Document Type"::Order, "Order No.") THEN
              CodeUtilisateurCreation := SalesHeader."Code utilisateur création";
            */
            // FIN ANI ESKVN1.0


        END;

    end;

    local procedure GetDocumentCaption() Result: Text[250];
    var
        LSalesShptHeader: Record "Sales Shipment Header";
        LText50001Lbl: Label 'Shipment No. %1 - %2', Comment = '%1 = Packing List ; %2 = Posting Date';
        Text50002Lbl: Label 'COPY Shipment No. %1 - %2', Comment = '%1 = Packing List ; %2 = Posting Date';
    begin
        // ANI ESKVN1.0
        IF LSalesShptHeader.GET("Sales Shipment Header"."No.") THEN;

        IF LSalesShptHeader."No. Printed" = 0 THEN
            Result := STRSUBSTNO(LText50001Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormat))
        ELSE
            Result := STRSUBSTNO(Text50002Lbl, "Sales Shipment Header"."Packing List No.", FORMAT("Sales Shipment Header"."Posting Date", 0, DateFormat));

        EXIT(Result);
        // FIN ANI ESKVN1.0
    end;

    procedure SetShippingAgent(var pShipmentAgent: Record "Shipping Agent"; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgent.INIT()
        ELSE
            pShipmentAgent.GET(Code);
    end;

    procedure SetShippingAgentServices(var pShipmentAgentServices: Record "Shipping Agent Services"; ShippingAgentCode: Code[10]; "Code": Code[10]);
    begin
        IF Code = '' THEN
            pShipmentAgentServices.INIT()
        ELSE
            pShipmentAgentServices.GET(ShippingAgentCode, Code);

    end;

    procedure GetWhseEmployeeName(SalesShipmentHeader: Record "Sales Shipment Header") WhseEmplName: Text;
    var
        lWhseEmpl: Record "Warehouse Employee";
    begin
        WhseEmplName := '';
        IF lWhseEmpl.GET(SalesShipmentHeader.Preparateur, SalesShipmentHeader."Location Code") THEN
            WhseEmplName := SalesShipmentHeader.Preparateur;
    end;

    local procedure InitPageOption();
    begin
        DisplayAssemblyInformation := TRUE; // coché par défaut Redmine #688
        PrintLogo := TRUE;
        PrintForFax := FALSE;
        no_fax := '';
        Attention_fax := '';
        nompdf := '';
        Objet_fax := '';
    end;

    local procedure CalcTotals(pPackingListLine: Record "Packing List Line");
    var
        lPackingListPackage: Record "Packing List Package";
    begin
        IF lPackingListPackage.GET(pPackingListLine."Packing List No.", pPackingListLine."Package No.") THEN
            IF NOT TempPackingListPackage.GET(lPackingListPackage."Packing List No.", lPackingListPackage."Package No.") THEN BEGIN
                TempPackingListPackage.INIT();
                TempPackingListPackage.TRANSFERFIELDS(lPackingListPackage);
                TempPackingListPackage.Insert();
            END;
    end;
}

