report 50037 "Import Sales Price from Excel"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Import Sales Price from Excel.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Import Sales Price from Excel';
    dataset
    {
        dataitem(Integer; Integer)
        {
            DataItemTableView = SORTING(Number)
                          ORDER(Ascending);

            trigger OnAfterGetRecord()
            begin

                //Initialisaation de la table
                FeuillePrix.RESET();
                FeuillePrix.INIT();

                //Incrémentation de la ligne
                No_ligne := No_ligne + 1;

                //Verification de l'article:
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'A');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    CurrReport.SKIP();

                Codeart := Format(TempExcelBuf."Cell Value as Text");
                IF Item.GET(Codeart) THEN
                    //Insertion de l'article
                    FeuillePrix.VALIDATE("Item No.", Codeart)
                ELSE
                    ERROR(Text001Lbl, No_ligne);


                //Verification du code unité:
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'B');

                //IF NOT TempExcelBuf.FINDFIRST () THEN
                //ERROR(Text002,No_ligne);

                IF UnitOfMeasure.GET(Codeart, TempExcelBuf."Cell Value as Text") THEN
                    //Insertion du code unité
                    FeuillePrix.VALIDATE("Unit of Measure Code", TempExcelBuf."Cell Value as Text");

                //ELSE
                //ERROR(Text003,No_ligne);


                //Verification du type vente
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'C');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    ERROR(Text004Lbl, No_ligne);

                TypeVente := Format(TempExcelBuf."Cell Value as Text");
                CASE TypeVente OF

                    'Client':
                        BEGIN
                            FeuillePrix.VALIDATE("Sales Type", FeuillePrix."Sales Type"::Customer);
                            //Verif du code vente
                            TempExcelBuf.SETRANGE("Row No.", No_ligne);
                            TempExcelBuf.SETRANGE(xlColID, 'D');
                            IF NOT TempExcelBuf.FINDFIRST() THEN
                                ERROR(Text006Lbl, No_ligne);

                            IF Customer.GET(TempExcelBuf."Cell Value as Text") THEN
                                FeuillePrix.VALIDATE("Sales Code", TempExcelBuf."Cell Value as Text")
                            ELSE
                                ERROR(Text007Lbl, No_ligne);
                        END;

                    'Groupe tarifs client':
                        BEGIN
                            FeuillePrix.VALIDATE("Sales Type", FeuillePrix."Sales Type"::"Customer Price Group");
                            //Verif du code vente
                            TempExcelBuf.SETRANGE("Row No.", No_ligne);
                            TempExcelBuf.SETRANGE(xlColID, 'D');
                            IF NOT TempExcelBuf.FINDFIRST() THEN
                                ERROR(Text006Lbl, No_ligne);

                            IF CustomerPriceGroup.GET(TempExcelBuf."Cell Value as Text") THEN
                                FeuillePrix.VALIDATE("Sales Code", TempExcelBuf."Cell Value as Text")
                            ELSE
                                ERROR(Text007Lbl, No_ligne);
                        END;

                    'Tous les clients':
                        FeuillePrix.VALIDATE("Sales Type", FeuillePrix."Sales Type"::"All Customers");

                    'Campagne':
                        BEGIN
                            FeuillePrix.VALIDATE("Sales Type", FeuillePrix."Sales Type"::Campaign);
                            //Verif du code vente
                            TempExcelBuf.SETRANGE("Row No.", No_ligne);
                            TempExcelBuf.SETRANGE(xlColID, 'D');
                            IF NOT TempExcelBuf.FINDFIRST() THEN
                                ERROR(Text006Lbl, No_ligne);

                            IF Campaign.GET(TempExcelBuf."Cell Value as Text") THEN
                                FeuillePrix.VALIDATE("Sales Code", TempExcelBuf."Cell Value as Text")
                            ELSE
                                ERROR(Text007Lbl, No_ligne);
                        END;

                    ELSE
                        ERROR(Text005Lbl, No_ligne);
                END;

                //Vérification de la quantité minimum
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'E');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    ERROR(Text008Lbl, No_ligne);

                EVALUATE(FeuillePrix."Minimum Quantity", TempExcelBuf."Cell Value as Text");
                //Insertion de la qté mini
                FeuillePrix.VALIDATE(FeuillePrix."Minimum Quantity");

                //Vérification de la date de début
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'F');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    ERROR(Text009Lbl, No_ligne);

                EVALUATE(FeuillePrix."Starting Date", TempExcelBuf."Cell Value as Text");
                //Insertion de la date début
                FeuillePrix.VALIDATE(FeuillePrix."Starting Date");

                //Vérification de la date de fin
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'G');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    FeuillePrix."Ending Date" := 0D
                ELSE
                    EVALUATE(FeuillePrix."Ending Date", TempExcelBuf."Cell Value as Text");



                //Insertion de la date fin
                FeuillePrix.VALIDATE(FeuillePrix."Ending Date");


                // Vérification du nouveau prix
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'H');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    ERROR(Text012Lbl, No_ligne);

                EVALUATE(FeuillePrix."New Unit Price", TempExcelBuf."Cell Value as Text");
                //MC 01-02-10 Modif suite au bug des decimaux
                FeuillePrix."New Unit Price" := ROUND(FeuillePrix."New Unit Price", 0.01);
                //FIN MC
                //Insertion du nouveau prix
                FeuillePrix.VALIDATE("New Unit Price");

                //Verification de la remise a la ligne
                TempExcelBuf.SETRANGE("Row No.", No_ligne);
                TempExcelBuf.SETRANGE(xlColID, 'I');
                IF NOT TempExcelBuf.FINDFIRST() THEN
                    ERROR(Text013Lbl, No_ligne);

                CASE UPPERCASE(TempExcelBuf."Cell Value as Text") OF
                    //Insertion de la remise a la ligne
                    'O':
                        FeuillePrix.VALIDATE("Allow Line Disc.", TRUE);

                    'N':
                        FeuillePrix.VALIDATE("Allow Line Disc.", FALSE)
                    ELSE
                        ERROR(Text014Lbl, No_ligne);
                END;


                //Remplissage automatique du ttc et remise facture
                FeuillePrix.VALIDATE(FeuillePrix."Price Includes VAT", FALSE);
                FeuillePrix.VALIDATE(FeuillePrix."Allow Invoice Disc.", TRUE);

                //Insertion de la ligne
                FeuillePrix.INSERT(TRUE);
            end;

            trigger OnPreDataItem();
            begin

                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 0, TempExcelBuf."Row No.");
                //On se positionne sur la 1ere ligne des données (entete=0)
                No_ligne := 1;
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(FileNameName; FileName)
                    {
                        ApplicationArea = All;
                        Caption = 'Nom du classeur';
                        ToolTip = 'Specifies the value of the Nom du classeur field.';
                    }
                    field(SheetNameName; SheetName)
                    {
                        ApplicationArea = All;
                        Caption = 'Nom de la feuille';
                        ToolTip = 'Specifies the value of the Nom de la feuille field.';
                    }
                }
            }
        }
    }
    trigger OnPreReport()
    begin
        ReadExcelSheet();
        //AnalyzeData;
    end;

    var

        TempExcelBuf: Record "Excel Buffer" temporary;
        FeuillePrix: Record "Sales Price Worksheet";
        UnitOfMeasure: Record "Item Unit of Measure";
        Customer: Record Customer;
        CustomerPriceGroup: Record "Customer Price Group";
        Campaign: Record Campaign;
        Item: Record Item;
        No_ligne: Integer;
        Codeart: Text[20];
        TypeVente: Text[30];
        // DateFin: Date;
        FileName: Text[250];
        UploadedFileName: Text[1024];
        SheetName: Text[250];
        Text001Lbl: Label 'Code article Introuvable : Ligne  %1', Comment = '%1';
        // Text002: Label '"Code Unité vide : Ligne %1 "';
        // Text003: Label 'Code Unité inexistant pour l''article : Ligne %1';
        Text004Lbl: Label 'Type Vente vide : Ligne %1', Comment = '%1';
        Text005Lbl: Label 'Type Vente Incorrect : Ligne %1', Comment = '%1';
        Text006Lbl: Label 'Code Vente vide : Ligne %1', Comment = '%1';
        Text007Lbl: Label 'Code Vente Inexistant pour le Type vente : Ligne %1', Comment = '%1';
        Text008Lbl: Label 'Quantité Minimum vide : Ligne %1', Comment = '%1';
        Text009Lbl: Label 'Date début vide : Ligne %1', Comment = '%1';
        // Text010: Label 'Date fin vide : Ligne %1';
        // Text011: Label 'Date Début doit être supérieure à Date Fin : Ligne %1';
        Text012Lbl: Label 'Nouveau Prix vide : Ligne %1', Comment = '%1';
        Text013Lbl: Label 'Autoriser remise ligne vide : Ligne %1', Comment = '%1';
        Text014Lbl: Label 'Autoriser remise ligne incorrect (O ou N) : Ligne %1', Comment = '%1';
        Text015Lbl: Label 'Choisir le fichier';

    LOCAL PROCEDURE ReadExcelSheet();
    BEGIN
        //Lecture de la feuille excel
        IF UploadedFileName = '' THEN
            UploadFile()
        ELSE
            Evaluate(FileName, UploadedFileName);

        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    END;

    PROCEDURE UploadFile();
    VAR
        CommonDialogMgt: Codeunit "File Management";
        ClientFileName: Text[1024];
    BEGIN
        //UploadedFileName := CommonDialogMgt.OpenFile(Text015,ClientFileName,2,'',0);
        Evaluate(UploadedFileName, CommonDialogMgt.UploadFile(Text015Lbl, ClientFileName));
        Evaluate(FileName, UploadedFileName);
    END;
}