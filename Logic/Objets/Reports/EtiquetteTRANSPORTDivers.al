report 50018 "Etiquette TRANSPORT Divers"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Etiquette TRANSPORT Divers.rdlc';
    caption = 'Etiquette TRANSPORT Divers';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Shipping Label"; "Shipping Label")
        {

            trigger OnPostDataItem();
            var
                RecPrestationTransporteur: Record "Shipping Agent Services";
                transporteur: Record "Shipping Agent";
                ShipmentHeader: Record "Sales Shipment Header";
                RecPays: Record "Country/Region";
                MonFichierEtq: Text[100];
                cr: Char;
                ctl_b: Char;
                ligne: array[100] of Text[1024];
                j: Integer;
                i: Integer;
                _DatePch: Date;
                "_NomExpéditeur": Code[30];
                "_AdresseExpéditeur": Text[30];
                "_Adresse2Expéditeur": Text[50];
                "_CodePostalExpéditeur": Code[20];
                "_VilleExpéditeur": Code[30];
                "_TelExpéditeur": Code[30];
                _FaxExpediteur: Code[30];
                _ContactDestinataire: Text[30];
                "_RefExpédition": Code[30];
                _NoBl: Code[20];
                _CodeDestinataire: Code[20];
                _ContreRemboursement: Decimal;
                _InstructionLivraison: Text[150];
                _NomDestinataire: Text[50];
                _AdresseDestinataire: Text[50];
                _Adresse2Destinataire: Text[50];
                _VilleDestinataire: Text[50];
                _CodePostalDestinataire: Code[10];
                _PaysDestinataire: Text[30];
                _TelDestinataire: Code[30];
                _NbColis: Text[10];
                _Poids: Text[10];
                _CodeBarre: Text[30];
                "_PaysExpéditeur": Text[50];
                TxtLbl: Label '%1%2.TXT', Comment = '%1=Label Folder ; %2';
            begin

                GlbNoEtq := "Shipping Label"."No. start label";
                GlbNoColis := 1;

                CompanyInfo.GET();

                IF NOT transporteur.GET("Shipping Label"."Shipping Agent Code") THEN
                    ERROR(Txt50001Lbl, "Shipping Label"."Shipping Agent Code");

                IF NOT RecPrestationTransporteur.GET("Shipping Label"."Shipping Agent Code", "Shipping Label"."Shipping Agent Services") THEN
                    ERROR(Txt50002Lbl, "Shipping Label"."Shipping Agent Services");

                ShipmentHeader.GET("Shipping Label"."BL No.");

                REPEAT


                    Evaluate(MonFichierEtq, STRSUBSTNO(TxtLbl, transporteur."Label Folder", GlbNoEtq));


                    FichierEtq.CREATE(MonFichierEtq);
                    FichierEtq.TEXTMODE(TRUE);

                    cr := 13;
                    ctl_b := 2;


                    // Construction du no d'expédition (BL) sur 8 chiffres en partant de la fin
                    j := STRLEN("Shipping Label"."BL No.");
                    _RefExpédition := '';
                    REPEAT
                        IF j > 0 THEN BEGIN
                            IF NOT EVALUATE(i, FORMAT("Shipping Label"."BL No."[j])) THEN
                                Evaluate(_RefExpédition, '0' + _RefExpédition)
                            ELSE
                                _RefExpédition := FORMAT("Shipping Label"."BL No."[j]) + _RefExpédition;
                        END
                        ELSE
                            Evaluate(_RefExpédition, '0' + _RefExpédition);

                        j -= 1;

                    UNTIL (STRLEN(_RefExpédition) >= 8);
                    // FIN Construction de la chaine


                    IF "Shipping Label"."Recipient Country Code" = '' THEN
                        "Shipping Label"."Recipient Country Code" := 'FR';

                    //MC --> 06/01/2010 Test de l'adresse de la prestation pour les affrètements
                    IF RecPrestationTransporteur.Name <> '' THEN BEGIN
                       Evaluate( _NomExpéditeur , RecPrestationTransporteur.Name);
                        Evaluate(_AdresseExpéditeur , RecPrestationTransporteur.Address);
                        _Adresse2Expéditeur := RecPrestationTransporteur."Address 2";
                        _CodePostalExpéditeur := RecPrestationTransporteur."Post Code";
                        _VilleExpéditeur := RecPrestationTransporteur.City;
                        _PaysExpéditeur := RecPrestationTransporteur."Country/Region Code";
                        _TelExpéditeur := RecPrestationTransporteur."Phone No.";
                        _FaxExpediteur := RecPrestationTransporteur."Fax No.";
                    END
                    ELSE BEGIN
                        Evaluate(_NomExpéditeur , CompanyInfo."Ship-to Name");
                        Evaluate(_AdresseExpéditeur , CompanyInfo.Address);
                        _Adresse2Expéditeur := CompanyInfo."Address 2";
                        _CodePostalExpéditeur := CompanyInfo."Post Code";
                        _VilleExpéditeur := CompanyInfo.City;
                        _PaysExpéditeur := CompanyInfo."Country/Region Code";
                        _TelExpéditeur := CompanyInfo."Phone No.";
                        _FaxExpediteur := CompanyInfo."Fax No.";
                    END;
                    //Fin MC

                    _ContreRemboursement := "Shipping Label"."Cash on delivery";
                    _InstructionLivraison := COPYSTR("Shipping Label"."Instruction of Delivery", 1, 32);

                    _CodeDestinataire := "Shipping Label"."Recipient Code";
                    _NomDestinataire := "Shipping Label"."Recipient Name";
                    _AdresseDestinataire := "Shipping Label"."Recipient Address";
                    _Adresse2Destinataire := "Shipping Label"."Recipient Address 2";
                    _CodePostalDestinataire := "Shipping Label"."Recipient Post Code";
                    _VilleDestinataire := "Shipping Label"."Recipient City";
                    _PaysDestinataire := "Shipping Label"."Recipient Country Code";
                    _TelDestinataire := "Shipping Label"."Recipient Phone No.";
                    _ContactDestinataire := "Shipping Label"."Recipient Person";

                    _NbColis := FORMAT(GlbNoColis, 0, '<integer,3><Filler Character,0>') + '/' +
                              FORMAT("Shipping Label"."Nb Of Box", 0, '<integer,3><Filler Character,0>');
                    _Poids := PADSTR(FORMAT("Shipping Label".Weight, 0, '<Precision,2:2><Standard Format,1> '), 5, ' ');

                    _DatePch := "Shipping Label"."Date of  bl";

                    Evaluate(_CodeBarre, '4' + transporteur."Account No." + '4' +
                                        COPYSTR(_CodePostalDestinataire, 1, 2) +
                                        '000' + '4' +
                                        _RefExpédition +
                                        FORMAT(GlbNoColis, 0, '<integer,2><Filler Character,0>') +
                                        RecPrestationTransporteur."Product Code");

                    _NoBl := "Shipping Label"."BL No.";

                    ligne[1] := FORMAT(ctl_b) + 'n' + FORMAT(cr);
                    ligne[2] := FORMAT(ctl_b) + 'e' + FORMAT(cr);
                    ligne[3] := FORMAT(ctl_b) + 'c0000' + FORMAT(cr);
                    ligne[4] := FORMAT(ctl_b) + 'RN' + FORMAT(cr);
                    ligne[5] := FORMAT(ctl_b) + 'f215' + FORMAT(cr);
                    ligne[6] := FORMAT(ctl_b) + 'V0' + FORMAT(cr);
                    ligne[7] := FORMAT(ctl_b) + 'O0215' + FORMAT(cr);
                    ligne[8] := FORMAT(ctl_b) + 'M2080' + FORMAT(cr);
                    ligne[9] := FORMAT(ctl_b) + 'qC' + FORMAT(cr);
                    ligne[10] := FORMAT(ctl_b) + 'L' + FORMAT(cr);
                    ligne[11] := 'A2' + FORMAT(cr);
                    ligne[12] := 'D11' + FORMAT(cr);
                    ligne[13] := 'z' + FORMAT(cr);
                    ligne[14] := 'PG' + FORMAT(cr);
                    ligne[15] := 'SG' + FORMAT(cr);
                    ligne[16] := 'H18' + FORMAT(cr);



                    ligne[17] := '451100000410047EXPEDITEUR' + FORMAT(cr);
                    ligne[18] := '441100000190093' + _NomExpéditeur + FORMAT(cr);
                    ligne[19] := '431100000100123' + _AdresseExpéditeur + FORMAT(cr);
                    ligne[20] := '421200000190160' + _CodePostalExpéditeur + ' ' + _VilleExpéditeur + FORMAT(cr);

                    //MC => 05/01/2010 On recherche le libellé du pays
                    RecPays.GET(_PaysExpéditeur);
                    ligne[21] := '431100000710195' + RecPays.Name + FORMAT(cr);
                    //FIN MC



                    ligne[22] := '421200000110232Tel : ' + _TelExpéditeur + FORMAT(cr);
                    ligne[23] := '421200000110264Fax : ' + _FaxExpediteur + FORMAT(cr);

                    ligne[24] := '453100002350047LIVRAISON' + FORMAT(cr);
                    ligne[25] := '441100002120093' + _NomDestinataire + FORMAT(cr);
                    ligne[26] := '441100002090138' + _AdresseDestinataire + FORMAT(cr);
                    ligne[27] := '441100002100176' + _Adresse2Destinataire + FORMAT(cr);
                    ligne[28] := '441100002100236' + _CodePostalDestinataire + FORMAT(cr);
                    ligne[29] := '441100002090275' + _VilleDestinataire + FORMAT(cr);
                    ligne[30] := '441100002670335' + _PaysDestinataire + FORMAT(cr);

                    ligne[31] := '451100005980047Donneur d ordre' + FORMAT(cr);
                    ligne[32] := '431100005640134' + ShipmentHeader."Sell-to Address" + FORMAT(cr);
                    ligne[33] := '441100005640092' + ShipmentHeader."Sell-to Customer Name" + FORMAT(cr);
                    ligne[34] := '431100006020332' + ShipmentHeader."Sell-to Country/Region Code" + FORMAT(cr);
                    ligne[35] := '431100005630166' + ShipmentHeader."Sell-to Address 2" + FORMAT(cr);
                    ligne[36] := '431100005630233' + ShipmentHeader."Sell-to Post Code" + FORMAT(cr);
                    ligne[37] := '431100005640271' + ShipmentHeader."Sell-to City" + FORMAT(cr);


                    ligne[38] := '1X1100001910001l03920002' + FORMAT(cr);
                    ligne[39] := '1X1100005430001l03940002' + FORMAT(cr);
                    ligne[40] := '1X1100000120060l00020802' + FORMAT(cr);
                    ligne[41] := '1X1100000000275l00020193' + FORMAT(cr);

                    ligne[42] := '461200000110364' + _NbColis + FORMAT(cr);
                    ligne[43] := '431100006140357' + _CodeDestinataire + FORMAT(cr);
                    ligne[44] := '421100005500357VOTRE REF :' + FORMAT(cr);
                    ligne[45] := '421100005580379  No BL :' + FORMAT(cr);
                    ligne[46] := '431100006100380' + _RefExpédition + FORMAT(cr);
                    ligne[47] := '441100002570376' + transporteur.Name + FORMAT(cr);
                    ligne[48] := '^01' + FORMAT(cr);
                    ligne[49] := 'Q0001' + FORMAT(cr);
                    ligne[50] := 'E' + FORMAT(cr);





                    i := 0;
                    WHILE i <= 50 DO BEGIN
                        i += 1;
                        FichierEtq.WRITE(ligne[i]);
                    END;

                    FichierEtq.CLOSE();

                    GlbNoEtq := INCSTR(GlbNoEtq);
                    GlbNoColis += 1;

                UNTIL GlbNoEtq > "Shipping Label"."No. end label";
            end;
        }
    }



    var
        CompanyInfo: Record "Company Information";
        GlbNoEtq: Code[15];
        FichierEtq: File;
        GlbNoColis: Integer;
        Txt50001Lbl: Label 'Transporteur %1 inexistant.', Comment = '%1';
        Txt50002Lbl: Label 'Prestation transporteur %1 inexistante.', Comment = '%1';
}

