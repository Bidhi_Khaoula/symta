report 50074 "Export Shipment By Date"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Export Shipment By Date.rdlc';
    Caption = 'Export Excel des BL';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Header; "Sales Shipment Header")
        {
            RequestFilterFields = "Posting Date";

            trigger OnAfterGetRecord();
            begin
                MakeExcelDataBody();
            end;

            trigger OnPreDataItem();
            begin

                IF Header.GETFILTER("Posting Date") = '' THEN
                    ERROR('Un filtre de date est obligatoire');
            end;
        }
    }
    trigger OnPostReport();
    begin
        CreateExcelbook();
    end;

    trigger OnPreReport();
    begin
        MakeExcelInfo();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        Excel002Lbl: Label 'Data';

    procedure MakeExcelInfo();
    begin
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn('date_exp', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('no_bl', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('c_client', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('magasinier', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('c_postal', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('port', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('c_transp', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('c_prodexp', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('plate_forme', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('montant_bp', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('poids_Bl', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('poids_logiflux', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('poids Logiflux arrondi', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('port_ht', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('ref_cmde_client', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('cond_reglement', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('samedi', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('c_representant', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('reliquats', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Mode expedition', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn(Header."Posting Date", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Sell-to Customer No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."User ID", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Ship-to Post Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Shipment Method Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Shipping Agent Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Mode d'expédition", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Zone Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(Header.GetAmount()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Header.GetTheoWeight()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Header.Weight), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(ROUND(Header.Weight, 1, '>'), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Header.GetPortAmount()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(Header."External Document No.", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Payment Terms Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Saturday Delivery", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header."Salesperson Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Header.IsReliquat(), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT(Header."Mode d'expédition"), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, 'Liste des BL', COMPANYNAME, USERID)
        //TempExcelBuf.CreateBook(Excel002);
        //TempExcelBuf.WriteSheet('Liste des BL',COMPANYNAME,USERID);
        //TempExcelBuf.GiveUserControl;
        ////ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;
}

