report 50012 "Bon prepa"
{
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Bon prepa.rdlc';

    Caption = 'Whse. - Shipment';

    dataset
    {
        dataitem("Warehouse Shipment Header"; "Warehouse Shipment Header")
        {
            DataItemTableView = SORTING("No.");
            PrintOnlyIfDetail = true;
            RequestFilterFields = "No.";
            column(HeaderNo_WhseShptHeader; "No.")
            {
            }
            dataitem(Integer; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    WHERE(Number = CONST(1));
                column(Title_WhseShptHeader; STRSUBSTNO(ESK001Lbl, shipHeaderTemp."No.", FORMAT(shipHeaderTemp."Posting Date", 0, 4), FORMAT(TIME, 0, '<Hours24>:<Minutes,2>')))
                {
                }
                column(Show1; TRUE)
                {
                }
                column(Show2; TRUE)
                {
                }
                column(AssUid__WhseShptHeader; "Warehouse Shipment Header"."Assigned User ID")
                {
                    IncludeCaption = true;
                }
                column(HrdLocCode_WhseShptHeader; "Warehouse Shipment Header"."Location Code")
                {
                    IncludeCaption = true;
                }
                column(HeaderNo1_WhseShptHeader; "Warehouse Shipment Header"."No.")
                {
                    IncludeCaption = true;
                }
                column(ZoneCode_WhseShptHeader; "Warehouse Shipment Header"."Zone Code")
                {
                }
                column(ModeExpedition_WhseShptHeader; "Warehouse Shipment Header"."Mode d'expédition")
                {
                }
                column(CurrReportPageNoCaption; CurrReportPageNoCaptionLbl)
                {
                }
                column(SalesHeaderSourceDocumentType; "TempSales Header"."Source Document Type")
                {
                }
                column(SalesHeaderExternalDocumentNo; "TempSales Header"."External Document No.")
                {
                }
                column(SalesHeaderTypeDocument; FORMAT("TempSales Header"."Type de commande"))
                {
                }
                column(SalesHeaderOrderCreateUser; "TempSales Header"."Order Create User")
                {
                }
                column(SalesHeaderNo; "TempSales Header"."No.")
                {
                }
                column(SalesHeaderRequestDeliveryDate; "TempSales Header"."Requested Delivery Date")
                {
                }
                column(SalesHeaderInstructionsDelivery; "TempSales Header"."Instructions of Delivery")
                {
                }
                column(SalesHeaderSellCustomerNo; "TempSales Header"."Sell-to Customer No.")
                {
                }
                column(CodeBarreBlob; CodeBarre.Blob)
                {
                }
                column(OrderUserName; OrderUserName)
                {
                }
                column(SalesHeaderTransporteurImperatif; "Sales Header"."Transporteur imperatif")
                {
                }
                column(ShippingAgentName; ShippingAgent.Name)
                {
                }
                column(Expedition_WhseShptHeader; ConstructChaineExpédition("TempSales Header"))
                {
                }
                column("PoidTotalThéorique"; PoidTotalThéorique)
                {
                }
                column(PiecesEncombrantes; PiecesEncombrantes)
                {
                }
                column(ShipToAddr8; ShipToAddr[8])
                {
                }
                column(ShipToAddr7; ShipToAddr[7])
                {
                }
                column(ShipToAddr6; ShipToAddr[6])
                {
                }
                column(ShipToAddr5; ShipToAddr[5])
                {
                }
                column(ShipToAddr4; ShipToAddr[4])
                {
                }
                column(ShipToAddr3; ShipToAddr[3])
                {
                }
                column(ShipToAddr2; ShipToAddr[2])
                {
                }
                column(ShipToAddr1; ShipToAddr[1])
                {
                }
                column(WarehouseShipmentCaption; WarehouseShipmentCaptionLbl)
                {
                }
                dataitem("Commentaire Cde"; "Sales Comment Line")
                {
                    DataItemLinkReference = "Warehouse Shipment Header";
                    DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE("Document Type" = CONST(Order),
                                              "Print Wharehouse Shipment" = CONST(true),
                                              "Document Line No." = CONST(0));
                    column(Commentaire_Cde; Comment)
                    {
                    }

                    trigger OnPreDataItem();
                    begin
                        //Message(VSourceNo);
                        "Commentaire Cde".SETRANGE("No.", VSourceNo);
                    end;
                }
                dataitem("Commentaire OT"; "Inventory Comment Line")
                {
                    DataItemTableView = SORTING("Document Type", "No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE("Document Type" = CONST("Transfer Order"));
                    column(Commentaire_OT; Comment)
                    {
                    }

                    trigger OnPreDataItem();
                    begin
                        "Commentaire OT".SETRANGE("No.", VSourceNo);
                    end;
                }
                dataitem(Comment; "Sales Line")
                {
                    DataItemLinkReference = "Warehouse Shipment Header";
                    DataItemTableView = SORTING("Document Type", "Document No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE("Document Type" = CONST(Order),
                                              Type = CONST(" "),
                                              "Attached to Line No." = CONST(0),
                                              "Print Wharehouse Shipment" = CONST(true));
                    column(Comment_Sales_Line; Description + ' ' + "Description 2")
                    {
                    }

                    trigger OnPreDataItem();
                    begin
                        Comment.SETRANGE("Document No.", VSourceNo);
                    end;
                }
                dataitem("Warehouse Shipment Line"; "Warehouse Shipment Line")
                {
                    DataItemLink = "No." = FIELD("No.");
                    DataItemLinkReference = "Warehouse Shipment Header";
                    DataItemTableView = SORTING("Bin Code", "Location Code");
                    column(ShelfNo_WhseShptLine; "Shelf No.")
                    {
                        IncludeCaption = true;
                    }
                    column(ItemNo_WhseShptLine; "Item No.")
                    {
                        IncludeCaption = true;
                    }
                    column(Desc_WhseShptLine; Description)
                    {
                        IncludeCaption = true;
                    }
                    column(UomCode_WhseShptLine; "Unit of Measure Code")
                    {
                        IncludeCaption = true;
                    }
                    column(LocCode_WhseShptLine; "Location Code")
                    {
                        IncludeCaption = true;
                    }
                    column(Qty_WhseShptLine; "Qty. to Ship (Base)")
                    {
                        IncludeCaption = true;
                    }
                    column(SourceNo_WhseShptLine; "Source No.")
                    {
                        IncludeCaption = true;
                    }
                    column(SourceDoc_WhseShptLine; "Source Document")
                    {
                        IncludeCaption = true;
                    }
                    column(ZoneCode_WhseShptLine; "Zone Code")
                    {
                        IncludeCaption = true;
                    }
                    column(BinCode_WhseShptLine; "Bin Code")
                    {
                        IncludeCaption = true;
                    }
                    column(Reference_WhseShptLine; _Reference)
                    {
                    }
                    column(SourceLineNo; "Source Line No.")
                    {
                    }
                    column(Ligne_WhseShptLine; _Ligne)
                    {
                    }
                    column(Stock_WhseShptLine; _Stock)
                    {
                    }
                    dataitem(Commentaire_Ligne; "Sales Comment Line")
                    {
                        DataItemLink = "Document Type" = FIELD("Source Subtype"),
                                       "No." = FIELD("Source No."),
                                       "Document Line No." = FIELD("Source Line No.");
                        DataItemTableView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                                            ORDER(Ascending)
                                            WHERE("Print Wharehouse Shipment" = CONST(true));
                        column(CommentaireLigne; Comment)
                        {
                        }
                    }

                    trigger OnAfterGetRecord();
                    begin
                        GetLocation("Location Code");


                        //afficher section lots ou aricle

                        //verifier ligne en distrib mag
                        IF Item.GET("Warehouse Shipment Line"."Item No.") THEN BEGIN
                            //AD LE 20-09-2005
                            Livrable := "Warehouse Shipment Line"."Qty. to Ship";

                            IF Item."Sans Mouvements de Stock" = TRUE THEN
                                Livrable := "Warehouse Shipment Line".Quantity;
                        END;

                        DesignationTotal := Format("Warehouse Shipment Line".Description + "Warehouse Shipment Line"."Description 2");


                        Item.CALCFIELDS(Inventory);

                        _Emplacement := "Warehouse Shipment Line"."Bin Code";
                        _Reference := GestionMultiRef.RechercheRefActive("Warehouse Shipment Line"."Item No.");
                        _Qte := Livrable;
                        _Condit := ' ';
                        _Designation := Format("Warehouse Shipment Line".Description);
                        _Stock := Item.Inventory;
                        _Ligne += 1;

                        IF NOT EVALUATE(_Ligne, FORMAT("Warehouse Shipment Line"."Source Line No." / 10000)) THEN
                            _Ligne := 0;


                        IF "Warehouse Shipment Line".Weight = 0 THEN
                            IF Item.GET("Warehouse Shipment Line"."Item No.") THEN
                                "Warehouse Shipment Line".Weight := Item."Net Weight";

                        PoidTotalThéorique += "Warehouse Shipment Line"."Qty. to Ship" * "Warehouse Shipment Line".Weight;
                    end;
                }
                dataitem(SalesLineNegatif; "Sales Line")
                {
                    DataItemLinkReference = "Warehouse Shipment Header";
                    DataItemTableView = SORTING("Document Type", "Document No.", "Line No.")
                                        ORDER(Ascending)
                                        WHERE("Document Type" = CONST(Order),
                                              Type = CONST(Item),
                                              "Outstanding Quantity" = FILTER(< 0));
                    column(SalesLineNegatifCaption; SalesLineNegatifCaptionLbl)
                    {
                    }
                    column(Reference_SalesLine; _Reference)
                    {
                    }
                    column(Qty_SalesLine; "Qty. to Ship")
                    {
                    }
                    column(Desc_SalesLine; Description)
                    {
                    }
                    column(BinCode_SalesLine; "Bin Code")
                    {
                    }
                    column(Ligne_SalesLine; _Ligne)
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin
                        _Reference := GestionMultiRef.RechercheRefActive(SalesLineNegatif."No.");
                        SalesLineNegatifCaptionLbl := '';
                        IF _FirstSalesLineNeg THEN SalesLineNegatifCaptionLbl := ESK004Lbl;
                        _FirstSalesLineNeg := FALSE;
                        _Ligne += 1;
                    end;

                    trigger OnPreDataItem();
                    begin

                        SalesLineNegatif.SETRANGE("Document No.", VSourceNo);
                        SalesLineNegatif.SETRANGE("Zone Code", "Warehouse Shipment Header"."Zone Code");
                        _FirstSalesLineNeg := TRUE;


                        // AD Le 28-09-2016 => REGIE -> Qty_SalesLine avant "outsanding", maintenant qte to ship
                    end;
                }
                dataitem("SL Other"; Integer)
                {
                    DataItemTableView = SORTING(Number);
                    column(WSH_Other_Zone_Code; QuerySL.Zone_Code)
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin
                        IF NOT QuerySL.READ() THEN CurrReport.SKIP();
                    end;

                    trigger OnPostDataItem();
                    begin
                        QuerySL.CLOSE();
                    end;

                    trigger OnPreDataItem();
                    begin
                        "SL Other".SETRANGE(Number, 1, 5);

                        // on cherche toutes les lignes commande (via une query pour faire un distinct !)
                        QuerySL.SETRANGE(QuerySL.Document_No, VSourceNo);
                        // MCO Le 09-02-2017 => Régie : Changement de règle
                        //QuerySL.SETFILTER(QuerySL.Quantity, '>0');
                        QuerySL.SETFILTER(QuerySL.Qty_to_Ship, '>0');
                        // FIN MCO Le 09-02-2017
                        QuerySL.SETFILTER(QuerySL.Zone_Code, '<>%1', "Warehouse Shipment Header"."Zone Code");
                        QuerySL.OPEN();
                    end;
                }
            }

            trigger OnAfterGetRecord();
            var
                Rec_PurchaseHeader: Record "Purchase Header";
                Rec_TransferHeader: Record "Transfer Header";
            begin
                GetLocation("Location Code");


                // JB Le 22-12-08
                VSourceType := 0;
                VSourceNo := '';

                "TempSales Header".RESET();
                CLEAR("TempSales Header");
                "TempSales Header".INIT();

                "Warehouse Shipment Header".GetSourceDoc("Warehouse Shipment Header", VSourceType, VSourceNo);

                CASE VSourceType OF
                    37:
                        BEGIN
                            "Sales Header".RESET();
                            "Sales Header".SETRANGE("No.", VSourceNo);
                            IF "Sales Header".FINDFIRST() THEN
                                WITH "Sales Header" DO BEGIN
                                    //          //IF RespCenter.GET("Responsibility Center") THEN BEGIN
                                    //          //  FormatAddr.RespCenter(CompanyAddr,RespCenter);
                                    //          //  CompanyInfo."Phone No." := RespCenter."Phone No.";
                                    //          //  CompanyInfo."Fax No." := RespCenter."Fax No.";
                                    //          //END ELSE BEGIN
                                    //            CompanyInfo.GET();
                                    //            FormatAddr.Company(CompanyAddr,CompanyInfo);
                                    //          //END;

                                    //          CompanyInfo.GET();
                                    //          FormatAddr.Company(CompanyAddr,CompanyInfo);

                                    IF "Salesperson Code" = '' THEN BEGIN
                                        CLEAR(SalesPurchPerson);
                                        SalesPersonText := '';
                                    END ELSE BEGIN
                                        SalesPurchPerson.GET("Salesperson Code");
                                        SalesPersonText := Text000Lbl;
                                    END;

                                    IF "Your Reference" = '' THEN
                                        ReferenceText := ''
                                    ELSE
                                        ReferenceText := Format(FIELDCAPTION("Your Reference"));

                                    //IF "VAT Registration No." = '' THEN
                                    //  VATNoText := ''
                                    //ELSE
                                    //  VATNoText := FIELDCAPTION("VAT Registration No.");

                                    //          IF "Currency Code" = '' THEN BEGIN
                                    //            GLSetup.TESTFIELD("LCY Code");
                                    //            TotalText := STRSUBSTNO(Text001,GLSetup."LCY Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,GLSetup."LCY Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,GLSetup."LCY Code");
                                    //          END ELSE BEGIN
                                    //            TotalText := STRSUBSTNO(Text001,"Currency Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,"Currency Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,"Currency Code");
                                    //          END;

                                    //          FormatAddr.SalesHeaderBillTo(CustAddr,"Sales Header");


                                    IF "Shipment Method Code" = '' THEN
                                        ShipmentMethod.INIT()
                                    ELSE
                                        ShipmentMethod.GET("Shipment Method Code");


                                    // AD Le 06-06-2011 => SYMTA -> Pas le contact dans l'adresse
                                    "Sales Header"."Ship-to Contact" := '';
                                    FormatAddr.SalesHeaderShipTo(ShipToAddr, ShipToAddr, "Sales Header");


                                    //          ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                                    //          FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                                    //            IF ShipToAddr[i] <> CustAddr[i] THEN
                                    //              ShowShippingAddr := TRUE;

                                    //          IF NOT CurrReport.PREVIEW THEN BEGIN
                                    //            IF LogInteraction THEN BEGIN
                                    //              CALCFIELDS("No. of Archived Versions");
                                    //            END;

                                    //          END;

                                    shipHeaderTemp.GET("Warehouse Shipment Header"."No.");

                                    txtReliquat := '';
                                    IF "Sales Header"."Shipment Status" = "Shipment Status"::Reliquat THEN
                                        txtReliquat := 'RELIQUAT';


                                    "TempSales Header" := "Sales Header";
                                    "TempSales Header".Insert();

                                    //GESTION DU CORRESPONDANT (paramètres utilisateurs)
                                    Correspondant := '';

                                    ShippingAgent.GET("TempSales Header"."Shipping Agent Code");

                                END;
                        END;
                    39:
                        BEGIN
                            // Recherche du retour d'achat
                            Rec_PurchaseHeader.RESET();
                            Rec_PurchaseHeader.SETRANGE("No.", VSourceNo);
                            IF Rec_PurchaseHeader.FINDFIRST() THEN BEGIN
                                // Création d'un Sales Header fictif afin d'afficher correctement l'entête
                                "TempSales Header"."No." := Rec_PurchaseHeader."No.";
                                "TempSales Header"."Responsibility Center" := Rec_PurchaseHeader."Responsibility Center";
                                "TempSales Header"."Your Reference" := Rec_PurchaseHeader."Your Reference";
                                "TempSales Header"."VAT Registration No." := Rec_PurchaseHeader."VAT Registration No.";
                                "TempSales Header"."Currency Code" := Rec_PurchaseHeader."Currency Code";
                                "TempSales Header"."Payment Terms Code" := Rec_PurchaseHeader."Payment Terms Code";
                                "TempSales Header"."Shipment Method Code" := Rec_PurchaseHeader."Shipment Method Code";

                                // AD Le 05-06-09 => Changement du Ship par pay car le ship est SIDAMO sur un retour
                                "TempSales Header"."Ship-to Code" := Rec_PurchaseHeader."Buy-from Vendor No.";
                                "TempSales Header"."Ship-to Name" := Rec_PurchaseHeader."Pay-to Name";
                                "TempSales Header"."Ship-to Address" := Rec_PurchaseHeader."Pay-to Address";
                                "TempSales Header"."Ship-to Address 2" := Rec_PurchaseHeader."Pay-to Address 2";
                                "TempSales Header"."Ship-to Post Code" := Rec_PurchaseHeader."Pay-to Post Code";
                                "TempSales Header"."Ship-to City" := Rec_PurchaseHeader."Pay-to City";
                                "TempSales Header"."Ship-to Contact" := Rec_PurchaseHeader."Pay-to Contact";
                                // FIN AD Le 05-06-09

                                "TempSales Header"."Sell-to Customer No." := Rec_PurchaseHeader."Buy-from Vendor No.";
                                "TempSales Header"."Sell-to Customer Name" := Rec_PurchaseHeader."Buy-from Vendor Name";
                                "TempSales Header"."Sell-to Customer Name 2" := Rec_PurchaseHeader."Buy-from Vendor Name 2";
                                "TempSales Header"."Sell-to Address" := Rec_PurchaseHeader."Buy-from Address";
                                "TempSales Header"."Sell-to Address 2" := Rec_PurchaseHeader."Buy-from Address 2";
                                "TempSales Header"."Sell-to Post Code" := Rec_PurchaseHeader."Buy-from Post Code";
                                "TempSales Header"."Sell-to City" := Rec_PurchaseHeader."Buy-from City";

                                "TempSales Header"."Bill-to Customer No." := Rec_PurchaseHeader."Buy-from Vendor No.";
                                "TempSales Header"."Bill-to Name" := Rec_PurchaseHeader."Buy-from Vendor Name";
                                "TempSales Header"."Bill-to Name 2" := Rec_PurchaseHeader."Buy-from Vendor Name 2";
                                "TempSales Header"."Bill-to Address" := Rec_PurchaseHeader."Buy-from Address";
                                "TempSales Header"."Bill-to Address 2" := Rec_PurchaseHeader."Buy-from Address 2";
                                "TempSales Header"."Bill-to Post Code" := Rec_PurchaseHeader."Buy-from Post Code";
                                "TempSales Header"."Bill-to City" := Rec_PurchaseHeader."Buy-from City";

                                "TempSales Header".Insert();


                                WITH "TempSales Header" DO BEGIN

                                    //          IF RespCenter.GET("Responsibility Center") THEN BEGIN
                                    //            FormatAddr.RespCenter(CompanyAddr,RespCenter);
                                    //            CompanyInfo."Phone No." := RespCenter."Phone No.";
                                    //            CompanyInfo."Fax No." := RespCenter."Fax No.";
                                    //          END ELSE BEGIN
                                    //            CompanyInfo.GET();
                                    //            FormatAddr.Company(CompanyAddr,CompanyInfo);
                                    //          END;

                                    CLEAR(SalesPurchPerson);
                                    SalesPersonText := '';

                                    IF "Your Reference" = '' THEN
                                        ReferenceText := ''
                                    ELSE
                                        ReferenceText := Format(FIELDCAPTION("Your Reference"));

                                    //          IF "VAT Registration No." = '' THEN
                                    //            VATNoText := ''
                                    //          ELSE
                                    //            VATNoText := FIELDCAPTION("VAT Registration No.");

                                    //          IF "Currency Code" = '' THEN BEGIN
                                    //            GLSetup.TESTFIELD("LCY Code");
                                    //            TotalText := STRSUBSTNO(Text001,GLSetup."LCY Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,GLSetup."LCY Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,GLSetup."LCY Code");
                                    //          END ELSE BEGIN
                                    //            TotalText := STRSUBSTNO(Text001,"Currency Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,"Currency Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,"Currency Code");
                                    //          END;

                                    //          FormatAddr.SalesHeaderBillTo(CustAddr,"TempSales Header");

                                    //          IF "Payment Terms Code" = '' THEN
                                    //            PaymentTerms.INIT
                                    //          ELSE
                                    //            PaymentTerms.GET("Payment Terms Code");

                                    IF "Shipment Method Code" = '' THEN
                                        ShipmentMethod.INIT()
                                    ELSE
                                        ShipmentMethod.GET("Shipment Method Code");

                                    FormatAddr.SalesHeaderShipTo(ShipToAddr, ShipToAddr, "TempSales Header");
                                    //          ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                                    //          FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                                    //            IF ShipToAddr[i] <> CustAddr[i] THEN
                                    //              ShowShippingAddr := TRUE;


                                    /* AD Le 04-11-2009 => Un get Direectement sur le BP en cours
                                              shipLineTemp.SETRANGE(shipLineTemp."Source Type",39);
                                              shipLineTemp.SETRANGE(shipLineTemp."Source No.","TempSales Header"."No.");
                                              IF shipLineTemp.FIND('-') THEN
                                                shipHeaderTemp.GET(shipLineTemp."No.");

                                    */
                                    shipHeaderTemp.GET("Warehouse Shipment Header"."No.");
                                    // FIN AD Le 04-11-2009


                                    txtReliquat := '';


                                END;
                            END;
                        END;
                    5741:
                        BEGIN
                            // Recherche de l'ordre de transfert
                            Rec_TransferHeader.RESET();
                            Rec_TransferHeader.SETRANGE("No.", VSourceNo);
                            IF Rec_TransferHeader.FINDFIRST() THEN BEGIN
                                // Création d'un Sales Header fictif afin d'afficher correctement l'entête
                                "TempSales Header"."No." := Rec_TransferHeader."No.";
                                "TempSales Header"."Shipment Method Code" := Rec_TransferHeader."Shipment Method Code";

                                "TempSales Header"."Ship-to Code" := Rec_TransferHeader."Transfer-from Code";
                                "TempSales Header"."Ship-to Name" := Rec_TransferHeader."Transfer-from Name";
                                "TempSales Header"."Ship-to Address" := Rec_TransferHeader."Transfer-from Address";
                                "TempSales Header"."Ship-to Address 2" := Rec_TransferHeader."Transfer-from Address 2";
                                "TempSales Header"."Ship-to Post Code" := Rec_TransferHeader."Transfer-from Post Code";
                                "TempSales Header"."Ship-to City" := Rec_TransferHeader."Transfer-from City";
                                "TempSales Header"."Ship-to Contact" := Rec_TransferHeader."Transfer-from Contact";

                                "TempSales Header"."Bill-to Customer No." := Rec_TransferHeader."Transfer-to Code";
                                "TempSales Header"."Bill-to Name" := Rec_TransferHeader."Transfer-to Name";
                                "TempSales Header"."Bill-to Name 2" := Rec_TransferHeader."Transfer-to Name 2";
                                "TempSales Header"."Bill-to Address" := Rec_TransferHeader."Transfer-to Address";
                                "TempSales Header"."Bill-to Address 2" := Rec_TransferHeader."Transfer-to Address 2";
                                "TempSales Header"."Bill-to Post Code" := Rec_TransferHeader."Transfer-to Post Code";
                                "TempSales Header"."Bill-to City" := Rec_TransferHeader."Transfer-to City";

                                // AD Le 16-04-2009 => Suivi des instr de livraison
                                //"TempSales Header"."Instructions of Delivery" := Rec_TransferHeader."Instruction of Delivery";
                                // FIN AD Le 16-04-2009


                                "TempSales Header".Insert();

                                WITH "TempSales Header" DO BEGIN

                                    //          IF RespCenter.GET("Responsibility Center") THEN BEGIN
                                    //            FormatAddr.RespCenter(CompanyAddr,RespCenter);
                                    //            CompanyInfo."Phone No." := RespCenter."Phone No.";
                                    //            CompanyInfo."Fax No." := RespCenter."Fax No.";
                                    //          END ELSE BEGIN
                                    //            CompanyInfo.GET();
                                    //            FormatAddr.Company(CompanyAddr,CompanyInfo);
                                    //          END;

                                    CLEAR(SalesPurchPerson);
                                    SalesPersonText := '';

                                    IF "Your Reference" = '' THEN
                                        ReferenceText := ''
                                    ELSE
                                        Evaluate(ReferenceText, FIELDCAPTION("Your Reference"));

                                    //          IF "VAT Registration No." = '' THEN
                                    //            VATNoText := ''
                                    //          ELSE
                                    //            VATNoText := FIELDCAPTION("VAT Registration No.");

                                    //          IF "Currency Code" = '' THEN BEGIN
                                    //            GLSetup.TESTFIELD("LCY Code");
                                    //            TotalText := STRSUBSTNO(Text001,GLSetup."LCY Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,GLSetup."LCY Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,GLSetup."LCY Code");
                                    //          END ELSE BEGIN
                                    //            TotalText := STRSUBSTNO(Text001,"Currency Code");
                                    //            TotalInclVATText := STRSUBSTNO(Text002,"Currency Code");
                                    //            TotalExclVATText := STRSUBSTNO(Text006,"Currency Code");
                                    //          END;

                                    //          FormatAddr.SalesHeaderBillTo(CustAddr,"TempSales Header");

                                    //          IF "Payment Terms Code" = '' THEN
                                    //            PaymentTerms.INIT
                                    //          ELSE
                                    //            PaymentTerms.GET("Payment Terms Code");

                                    IF "Shipment Method Code" = '' THEN
                                        ShipmentMethod.INIT()
                                    ELSE
                                        ShipmentMethod.GET("Shipment Method Code");

                                    FormatAddr.SalesHeaderShipTo(ShipToAddr, ShipToAddr, "TempSales Header");

                                    //          ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                                    //          FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                                    //            IF ShipToAddr[i] <> CustAddr[i] THEN
                                    //              ShowShippingAddr := TRUE;


                                    shipHeaderTemp.GET("Warehouse Shipment Header"."No.");
                                    // FIN AD Le 04-11-2009


                                    txtReliquat := '';

                                END;
                            END;
                        END;
                END;

                cduBarcodeMgt.EncodeCode39("Warehouse Shipment Header"."No.", 1, FALSE, FALSE, CodeBarre);

                // AD Le 31-03-2015 =>
                IF "Warehouse Shipment Header"."Date Impression" = 0D THEN BEGIN
                    "Warehouse Shipment Header"."Date Impression" := WORKDATE();
                    "Warehouse Shipment Header"."Heure Impression" := TIME;
                    Evaluate("Warehouse Shipment Header"."Utilisateur Impression", USERID);
                    "Warehouse Shipment Header".MODIFY();
                END;
                // FIN AD Le 31-03-2015

                PoidTotalThéorique := 0;
                PiecesEncombrantes := GetPiècesEncombrantes();

                OrderUserName := ReportUtils.GetCorrespondantName("TempSales Header"."Order Create User", '');

            end;
        }
    }

    requestpage
    {
        Caption = 'Whse. - Posted Shipment';
    }

    var
        Location: Record Location;
        ShipmentMethod: Record "Shipment Method";
        CodeBarre: Record "Upgrade Blob Storage";
        ShippingAgent: Record "Shipping Agent";
        "Sales Header": Record "Sales Header";
        SalesPurchPerson: Record "Salesperson/Purchaser";
        "TempSales Header": Record "Sales Header" temporary;
        Item: Record Item;
        shipHeaderTemp: Record "Warehouse Shipment Header";
        FormatAddr: Codeunit "Format Address";
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        cduBarcodeMgt: Codeunit "Barcode Mgt.";
        ReportUtils: Codeunit ReportFunctions;
        QuerySL: Query "Sales Line Distinct Zone Code";
        CurrReportPageNoCaptionLbl: Label 'Page';
        WarehouseShipmentCaptionLbl: Label 'Warehouse Shipment';
        ESK001Lbl: Label 'PIC No : %1 DU %2 - %3', Comment = '%1 %2 %3';
        ESK002Lbl: Label '%1 - %2', Comment = '%1 %2 ';
        ESK003Lbl: Label 'PIECES EMCOMBRANTES';
        Text000Lbl: Label 'Salesperson';
        // Text001: Label 'Total %1';
        // Text002: Label 'Total %1 Incl. VAT';
        // Text003: Label 'COPY';
        // Text004: Label 'Order Confirmation %1';
        // Text005: Label 'Page %1';
        // Text006: Label 'Total %1 Excl. VAT';
        ESK004Lbl: Label 'REMISE EN STOCK';
        VSourceType: Integer;
        VSourceNo: Code[20];
        Correspondant: Text[200];
        _Emplacement: Code[20];
        _Reference: Code[20];
        _Qte: Decimal;
        _Condit: Code[1];
        _Designation: Text[60];
        _Stock: Decimal;
        _Ligne: Integer;
        DesignationTotal: Text[110];
        "PoidTotalThéorique": Decimal;
        // "PoidsAffiché": Decimal;
        txtReliquat: Text[20];
        SalesPersonText: Text[30];
        ReferenceText: Text[30];
        Livrable: Decimal;
        PiecesEncombrantes: Text;
        OrderUserName: Text;
        ShipToAddr: array[8] of Text[50];
        // ShowShippingAddr: Boolean;
        // i: Integer;
        SalesLineNegatifCaptionLbl: Text;

        _FirstSalesLineNeg: Boolean;

    local procedure GetLocation(LocationCode: Code[10]);
    begin
        IF LocationCode = '' THEN
            Location.INIT()
        ELSE
            IF Location.Code <> LocationCode THEN
                Location.GET(LocationCode);
    end;

    procedure CheckWhLine("DocNo.": Code[20]; NumLine: Integer): Boolean;
    var
        WhShiptLine: Record "Warehouse Shipment Line";
    begin
        WhShiptLine.SETRANGE("Source No.", "DocNo.");
        WhShiptLine.SETRANGE("Source Line No.", NumLine);
        EXIT(Not WhShiptLine.IsEmpty);
    end;

    procedure "ConstructChaineExpédition"(_pSalesHeader: Record "Sales Header"): Text[250];
    var
        // ShipAgent: Record "Shipping Agent";
        ShipMethod: Record "Shipment Method";
    begin
        // Construit la chaine Expédition

        // Lecture transporteur
        IF NOT ShipMethod.GET("TempSales Header"."Shipment Method Code") THEN
            ShipMethod.Description := "TempSales Header"."Shipment Method Code";


        //IF NOT ShipAgent.GET("TempSales Header"."Shipping Agent Code") THEN
        //  ShipAgent.Name := "TempSales Header"."Shipping Agent Code";

        EXIT(STRSUBSTNO(ESK002Lbl, ShipMethod.Description, FORMAT("TempSales Header"."Mode d'expédition")));
    end;

    procedure "GetPiècesEncombrantes"(): Text[30];
    var
        LigneBP: Record "Warehouse Shipment Line";
        WarehouseSetup: Record "Warehouse Setup";
    begin
        // Retourne si il y a des pièces encombrantes dans le BP

        WarehouseSetup.GET();

        LigneBP.RESET();
        LigneBP.SETRANGE("No.", "Warehouse Shipment Header"."No.");
        LigneBP.SETFILTER(LigneBP.Weight, '>=%1', WarehouseSetup."Limite Poids Pièces Encombrant");
        IF LigneBP.COUNT <> 0 THEN
            EXIT(ESK003Lbl);
    end;
}

