report 50151 "Copie Articles"
{
    ProcessingOnly = true;
    Caption = 'Copie Articles';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.");

            trigger OnAfterGetRecord();
            begin

                //Nouvelle référence remplace l'ancienne
                IF NvRefRemplaceAnc = TRUE THEN BEGIN
                    TSubstituttionRemp.INIT();
                    TSubstituttionRemp."No." := Item."No.";
                    TSubstituttionRemp."Substitute No." := NvNo;
                    TSubstituttionRemp.Insert();
                END;

                //Article
                Article.INIT();
                Article.TRANSFERFIELDS(Item, TRUE);
                Article."No." := '';
                Article."No. 2" := NvNo;
                Article."Envoyé MINILOAD" := FALSE;
                Article."Date Envoi MINILOAD" := 0D;
                Article."Heure Envoi MINILOAD" := 0T;

                //DEBUG SL 06/10/06 NSC1.11
                Article."Last Date Modified" := WORKDATE();
                //Fin DEBUG SL 06/10/06 NSC1.11

                Article."Tariff No." := Item."Tariff No.";

                Article."Date dernière entrée" := 0D;

                //GB ESKAPE : FE20170109 Mise à non du publiable dans le cadre de la copie
                Article.Publiable := Article.Publiable::Non;
                //FIN GB

                Article.INSERT(TRUE);
                Evaluate(Article."Create User ID", USERID);
                NvNo := Article."No.";

                //Image
                /*Item.CALCFIELDS(Picture);
                IF Image AND Item.Picture.HASVALUE THEN BEGIN
                 Article.Picture.CREATEOUTSTREAM(OutS);
                 Item.Picture.CREATEINSTREAM(InS);
                 COPYSTREAM(OutS,InS);
                 END;*/
                Article.MODIFY();

                //Unit of Measure

                TUniteMesure.SETRANGE(TUniteMesure."Item No.", Item."No.");
                IF TUniteMesure.FIND('-') THEN
                    REPEAT
                        TUniteMesureNew.INIT();
                        TUniteMesureNew.TRANSFERFIELDS(TUniteMesure, TRUE);
                        TUniteMesureNew."Item No." := NvNo;
                        TUniteMesureNew.INSERT(TRUE);
                    UNTIL TUniteMesure.NEXT() = 0;


                IF ContenuEmplacement THEN BEGIN
                    TBinContent.SETRANGE("Item No.", Item."No.");
                    IF TBinContent.FIND('-') THEN
                        REPEAT
                            TBinContentNew.INIT();
                            TBinContentNew.TRANSFERFIELDS(TBinContent, TRUE);
                            TBinContentNew."Item No." := NvNo;
                            TBinContentNew.Insert();
                        UNTIL TBinContent.NEXT() = 0;
                END;



                // Prix d'achats
                IF PrixAchat = TRUE THEN BEGIN
                    TPrixAchats.SETRANGE(TPrixAchats."Item No.", Item."No.");
                    IF TPrixAchats.FIND('-') THEN
                        REPEAT
                            TPrixAchatsNew.INIT();
                            TPrixAchatsNew.TRANSFERFIELDS(TPrixAchats, TRUE);
                            TPrixAchatsNew."Item No." := NvNo;
                            TPrixAchatsNew.Insert();
                        UNTIL TPrixAchats.NEXT() = 0;
                END;


                //Prix de ventes
                IF PrixVentes = TRUE THEN BEGIN
                    TPrixVentes.SETRANGE(TPrixVentes."Item No.", Item."No.");
                    TPrixVentes.SETFILTER("Ending Date", '>%1|%2', WORKDATE(), 0D);
                    IF TPrixVentes.FIND('-') THEN
                        REPEAT
                            TPrixVentesNew.INIT();
                            TPrixVentesNew.TRANSFERFIELDS(TPrixVentes, TRUE);
                            TPrixVentesNew."Item No." := NvNo;
                            TPrixVentesNew.INSERT(TRUE);
                        UNTIL TPrixVentes.NEXT() = 0;
                END;


                //Remises achats
                IF RemisesAchats = TRUE THEN BEGIN
                    TRemisesAchats.SETRANGE(TRemisesAchats."Item No.", Item."No.");
                    TRemisesAchats.SETFILTER("Ending Date", '>%1|%2', WORKDATE(), 0D);
                    IF TRemisesAchats.FIND('-') THEN
                        REPEAT
                            TRemisesAchatsNew.INIT();
                            TRemisesAchatsNew.TRANSFERFIELDS(TRemisesAchats, TRUE);
                            TRemisesAchatsNew."Item No." := NvNo;
                            TRemisesAchatsNew.INSERT(TRUE);
                        UNTIL TRemisesAchats.NEXT() = 0;
                END;

                //Catalogue Fournisseur
                IF CatalogueFour = TRUE THEN BEGIN
                    TCataFour.SETRANGE(TCataFour."Item No.", Item."No.");
                    //TCataFour.SETRANGE(TCataFour."Vendor No.",Item."Vendor No.");
                    IF TCataFour.FIND('-') THEN
                        REPEAT
                            TCataFourNew.INIT();
                            TCataFourNew.TRANSFERFIELDS(TCataFour, TRUE);
                            //TCataFour."Vendor No.":="VendorNo.";
                            TCataFourNew."Item No." := NvNo;
                            TCataFourNew.INSERT(TRUE);
                        UNTIL TCataFour.NEXT() = 0;
                END;


                //Substitution
                IF Substitution = TRUE THEN BEGIN
                    TSubstituttion.SETRANGE("No.", Item."No.");
                    IF TSubstituttion.FIND('-') THEN
                        REPEAT
                            TSubstituttionNew.INIT();
                            TSubstituttionNew.TRANSFERFIELDS(TSubstituttion, TRUE);
                            TSubstituttionNew."No." := NvNo;
                            //TSubstituttionNew."Substitute No." := '';
                            TSubstituttionNew.Insert();
                        UNTIL TSubstituttion.NEXT() = 0;
                END;


                //Traductions
                IF Traductions = TRUE THEN BEGIN
                    TTraductions.SETRANGE(TTraductions."Item No.", Item."No.");
                    IF TTraductions.FIND('-') THEN
                        REPEAT
                            TTraductionsNew.INIT();
                            TTraductionsNew.TRANSFERFIELDS(TTraductions, TRUE);
                            TTraductionsNew."Item No." := NvNo;
                            TTraductionsNew.INSERT(TRUE);
                        UNTIL TTraductions.NEXT() = 0;
                END;


                //Référence externe
                IF RéférencesEx = TRUE THEN BEGIN
                    TRefExterne.SETRANGE(TRefExterne."Item No.", Item."No.");
                    // AD Le 24-06-2010 => Demande de Christine
                    TRefExterne.SETFILTER("Reference Type", '%1|%2', TRefExterne."Reference Type"::Customer,
                             TRefExterne."Reference Type"::Vendor);
                    // FIN AD Le 24-06-2010
                    IF TRefExterne.FIND('-') THEN
                        REPEAT
                            TRefExterneNew.INIT();
                            TRefExterneNew.TRANSFERFIELDS(TRefExterne, TRUE);
                            TRefExterneNew."Item No." := NvNo;
                            //CPH 31/08/09 - DEBUT
                            //TRefExterneNew.INSERT(TRUE);
                            IF TRefExterneNew.INSERT(TRUE) THEN;
                        //CPH 31/08/09 - FIN
                        UNTIL TRefExterne.NEXT() = 0;
                END;

                //Variantes
                IF Variantes = TRUE THEN BEGIN
                    TVariantes.SETRANGE(TVariantes."Item No.", Item."No.");
                    IF TVariantes.FIND('-') THEN
                        REPEAT
                            TVariantesNew.INIT();
                            TVariantesNew.TRANSFERFIELDS(TVariantes, TRUE);
                            TVariantesNew."Item No." := NvNo;
                            TVariantesNew.Insert();
                        UNTIL TVariantes.NEXT() = 0;
                END;

                //Commentaires
                IF Commentaires = TRUE THEN BEGIN
                    TComment.SETRANGE(TComment."Table Name", TComment."Table Name"::Item);
                    TComment.SETRANGE(TComment."No.", Item."No.");
                    IF TComment.FIND('-') THEN
                        REPEAT
                            TCommentNew.INIT();
                            TCommentNew.TRANSFERFIELDS(TComment, TRUE);
                            TCommentNew."No." := NvNo;
                            TCommentNew.Insert();
                        UNTIL TComment.NEXT() = 0;
                END;

                //texte étendu Général Entete
                IF TextGen = TRUE THEN BEGIN
                    TTextGenEntete.SETRANGE(TTextGenEntete."Table Name", TTextGenEntete."Table Name"::Item);
                    TTextGenEntete.SETRANGE(TTextGenEntete."No.", Item."No.");
                    IF TTextGenEntete.FIND('-') THEN
                        REPEAT
                            TTextGenEnteteNew.INIT();
                            TTextGenEnteteNew.TRANSFERFIELDS(TTextGenEntete, TRUE);
                            TTextGenEnteteNew."No." := NvNo;
                            // AD Le 16-11-2010 => ATTENTION -> Le INSERT fait un +1 sur le texte no
                            TTextGenEnteteNew."Text No." := TTextGenEnteteNew."Text No." - 1;
                            TTextGenEnteteNew.INSERT(TRUE);
                        UNTIL TTextGenEntete.NEXT() = 0;
                END;

                //texte étendu Général Ligne
                IF TextGen = TRUE THEN BEGIN
                    TTextGenLigne.SETRANGE(TTextGenLigne."Table Name", TTextGenLigne."Table Name"::Item);
                    TTextGenLigne.SETRANGE(TTextGenLigne."No.", Item."No.");
                    IF TTextGenLigne.FIND('-') THEN
                        REPEAT
                            TTextGenLigneNew.INIT();
                            TTextGenLigneNew.TRANSFERFIELDS(TTextGenLigne, TRUE);
                            TTextGenLigneNew."No." := NvNo;
                            TTextGenLigneNew.INSERT();
                        UNTIL TTextGenLigne.NEXT() = 0;
                END;



                COMMIT();
                Item2.GET(Article."No.");
                ItemCard.SETRECORD(Item2);
                // MCO Le 13-12-2017
                ItemCard.IsCopieArticle(TRUE);
                ItemCard.RUNMODAL();

                // AD Le 02-12-2011 => SYMTA -> Demande de ND pas besoin
                /*
                ItemUOm.SETRANGE("Item No.",Article."No.");
                ItemUOMForm.SETTABLEVIEW(ItemUOm);
                ItemUOMForm.RUNMODAL();
                */

                // AD Le 06-12-2011 => SYMTA -> Demande de ND
                TBinContent.SETRANGE("Item No.", Article."No.");
                BinContentForm.SETTABLEVIEW(TBinContent);
                BinContentForm.RUNMODAL();




                CommentLine.RESET();
                IF Commentaires = TRUE THEN BEGIN
                    CommentLine.SETRANGE(CommentLine."Table Name", TComment."Table Name"::Item);
                    CommentLine.SETRANGE(CommentLine."No.", Article."No.");
                    IF CommentLine.FINDFIRST() THEN BEGIN
                        FormCommentLIne.SETTABLEVIEW(CommentLine);
                        FormCommentLIne.RUNMODAL();
                    END;
                END;


                NvRefRemplaceAnc := TRUE;
                Substitution := TRUE;
                AxesAnalytiques := TRUE;
                Image := TRUE;

                IF CatalogueFour = TRUE THEN BEGIN
                    Itemvendor.SETRANGE("Item No.", Article."No.");
                    FormItemvendor.SETTABLEVIEW(Itemvendor);
                    FormItemvendor.RUNMODAL();
                END;


                IF PrixVentes THEN BEGIN
                    SalesPrice.SETRANGE("Item No.", Article."No.");
                    //SalesPrice.SETFILTER("Ending Date",'>%1',WORKDATE);
                    FormSalesPrice.SETTABLEVIEW(SalesPrice);
                    FormSalesPrice.RUNMODAL();
                END;

                // AD Le 16-11-2010 => On présente les textes étendus
                BEGIN
                    TTextGenEntete.RESET();
                    TTextGenEntete.SETRANGE("Table Name", TTextGenEntete."Table Name"::Item);
                    TTextGenEntete.SETRANGE("No.", Article."No.");
                    IF TTextGenEntete.FINDFIRST() THEN
                        REPEAT
                            TTextGenEnteteNew.COPYFILTERS(TTextGenEntete);
                            TTextGenEnteteNew.SETRANGE("Text No.", TTextGenEntete."Text No.");
                            TTextGenEnteteNew.SETRANGE("Language Code", TTextGenEntete."Language Code");
                            CLEAR(FormTxtHeader);
                            FormTxtHeader.SETTABLEVIEW(TTextGenEnteteNew);
                            FormTxtHeader.RUNMODAL();
                        UNTIL TTextGenEntete.NEXT() = 0;

                END;
                // FIN AD Le 16-11-2010

                Variantes := TRUE;
                RéférencesEx := TRUE;
                Traductions := TRUE;
                TextGen := TRUE;
                PrixVentes := TRUE;
                RemisesAchats := TRUE;

            end;

            trigger OnPreDataItem();
            begin
                Item.SETFILTER("No.", AncienNo);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field("Article Source"; AncienNo)
                    {
                        TableRelation = Item;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the AncienNo field.';

                        trigger OnValidate();
                        begin

                            IF NOT Article.GET(AncienNo) THEN
                                ERROR(CopieArticleTexte01Err, AncienNo);

                            IF Vend.GET(Article."Vendor No.")
                           THEN
                                "VendorNo." := Vend."No.";
                        end;
                    }
                    field("Article Destination"; NvNo)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the NvNo field.';

                        trigger OnValidate();
                        var
                            lItemCrossReference: Record "Item Reference";
                        begin

                            Article.RESET();
                            Article.SETRANGE("No. 2", NvNo);
                            IF Article.FINDFIRST() THEN
                                ERROR(CopieArticleTexte02Err, NvNo);

                            //CFR le 19/11/2020 : Régie > Fin Ticket 49042 (copie article)
                            lItemCrossReference.SETCURRENTKEY("Reference No.");
                            lItemCrossReference.RESET();
                            lItemCrossReference.SETRANGE("Reference No.", NvNo);
                            IF lItemCrossReference.FINDFIRST() THEN
                                ERROR(CopieArticleTexte03Err, NvNo, lItemCrossReference."Item No.");
                            //FIN CFR le 19/11/2020

                            IF Article.GET(NvNo) THEN
                                ERROR(CopieArticleTexte02Err, NvNo);
                        end;
                    }
                    field("Nv Ref. Remplace l'ancienne_Name"; NvRefRemplaceAnc)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the NvRefRemplaceAnc field.';
                    }
                    field(Substitutions_Name; Substitution)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Substitution field.';
                    }
                    field("Axes Analytiques_Name"; AxesAnalytiques)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the AxesAnalytiques field.';
                    }
                    field(Images_Name; Image)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Image field.';
                    }
                    field(Commentaires_Name; Commentaires)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Commentaires field.';
                    }
                    field(Variantes_Name; Variantes)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Variantes field.';
                    }
                    field("Références Externes"; RéférencesEx)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the RéférencesEx field.';
                    }
                    field(Traductions_Name; Traductions)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Traductions field.';
                    }
                    field("Testes Etendus "; TextGen)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the TextGen field.';
                    }
                    field("Prix ventes"; PrixVentes)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the PrixVentes field.';
                    }
                    field("Fournisseurs / Articles"; CatalogueFour)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the CatalogueFour field.';
                    }
                    field("Prix Achats"; PrixAchat)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the PrixAchat field.';
                    }
                    field("Remises Achats"; RemisesAchats)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the RemisesAchats field.';
                    }
                    field("Contenus Emplacemens"; ContenuEmplacement)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the ContenuEmplacement field.';
                    }
                }
            }
        }

        trigger OnOpenPage();
        begin

            //Substitution:=TRUE;
            //Image:=TRUE;
            //Commentaires:=TRUE;
            //RéférencesEx:=TRUE;
            //Traductions:=TRUE;
            //TextGen:=TRUE;
            PrixVentes := TRUE;
            //RemisesAchats:=TRUE;
            ContenuEmplacement := TRUE;
            CatalogueFour := TRUE;
        end;
    }

    var
        TBinContent: Record "Bin Content";
        TBinContentNew: Record "Bin Content";
        TUniteMesure: Record "Item Unit of Measure";
        TUniteMesureNew: Record "Item Unit of Measure";
        TSubstituttion: Record "Item Substitution";
        TSubstituttionNew: Record "Item Substitution";
        TSubstituttionRemp: Record "Item Substitution";
        TComment: Record "Comment Line";
        TCommentNew: Record "Comment Line";
        TVariantes: Record "Item Variant";
        TVariantesNew: Record "Item Variant";
        TRefExterne: Record "Item Reference";
        TRefExterneNew: Record "Item Reference";
        TTraductions: Record "Item Translation";
        TTraductionsNew: Record "Item Translation";
        TTextGenEntete: Record "Extended Text Header";
        TTextGenEnteteNew: Record "Extended Text Header";
        TTextGenLigne: Record "Extended Text Line";
        TTextGenLigneNew: Record "Extended Text Line";
        TPrixVentes: Record "Sales Price";
        TPrixVentesNew: Record "Sales Price";
        TCataFour: Record "Item Vendor";
        TCataFourNew: Record "Item Vendor";
        TPrixAchats: Record "Purchase Price";
        TPrixAchatsNew: Record "Purchase Price";
        TRemisesAchats: Record "Purchase Line Discount";
        TRemisesAchatsNew: Record "Purchase Line Discount";
        Article: Record Item;
        Item2: Record Item;
        // ItemUOm: Record "Item Unit of Measure";
        CommentLine: Record "Comment Line";
        Itemvendor: Record "Item Vendor";
        SalesPrice: Record "Sales Price";
        Vend: Record Vendor;
        ItemCard: Page "Item Card";
        // ItemUOMForm: Page "Item Units of Measure";
        FormCommentLIne: Page "Fusion Réception";
        FormItemvendor: Page "Item Vendor Catalog";
        FormSalesPrice: Page "Sales Prices";
        FormTxtHeader: Page "Extended Text";
        BinContentForm: Page "Bin Contents";
        AncienNo: Code[20];
        NvNo: Code[20];
        NvRefRemplaceAnc: Boolean;
        Substitution: Boolean;
        AxesAnalytiques: Boolean;
        Image: Boolean;
        Commentaires: Boolean;
        Variantes: Boolean;
        "RéférencesEx": Boolean;
        Traductions: Boolean;
        TextGen: Boolean;
        PrixVentes: Boolean;
        PrixAchat: Boolean;
        RemisesAchats: Boolean;
        CatalogueFour: Boolean;
        ContenuEmplacement: Boolean;

        // InS: InStream;
        // OutS: OutStream;

        "VendorNo.": Code[20];

        CopieArticleTexte01Err: Label 'Numéro d''aticle %1 n''existe pas', Comment = '%1=Numéro d''aticle';
        CopieArticleTexte02Err: Label 'Numéro d''aticle %1 existe déjà', Comment = '%1=Numéro d''aticle';
        CopieArticleTexte03Err: Label 'Référence externe %1 déjà connue pour l''article %2', Comment = '%1=Référence externe ; %2=Numéro d''aticle';
}

