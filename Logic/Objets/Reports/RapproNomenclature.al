report 50038 "Réappro Nomenclature"
{
    Caption = 'Réappro Nomenclature';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending)
                                WHERE("Assembly BOM" = CONST(true),
                                      "Assembly Policy" = CONST("Assemble-to-Stock"),
                                      "Replenishment System" = CONST(Assembly));
            RequestFilterFields = "No. 2", "Location Filter";

            trigger OnAfterGetRecord();
            var
                LBOMHeader: Record "Assembly Header";
                Reserve: Decimal;
                Attendu: Decimal;
                StkATerme: Decimal;
                PtReappro: Decimal;
                QteCde: Decimal;
                QteMiniAchat: Decimal;
                NextDocNo: Code[20];
            begin

                IF Counter MOD 5 = 0 THEN
                    Window.UPDATE(1, "No.");
                Counter := Counter + 1;

                // --------------------------------
                // Calcul des données pour le stock
                // --------------------------------
                CASE TypeRéappro OF
                    TypeRéappro::Mini:
                        PtReappro := Item."Reorder Point";
                    TypeRéappro::Maxi:
                        PtReappro := Item."Maximum Inventory";
                END;

                Item.CalcAttenduReserve(Reserve, Attendu);
                Item.CALCFIELDS(Inventory);

                StkATerme := Inventory + Attendu - Reserve;

                // Si le stock a terme est > au point de reappro on switch l'article.
                IF StkATerme >= PtReappro THEN
                    CurrReport.SKIP();

                // CFR le 11/05/2022 => Régie : l'attendu ne tient pas compte des quantités sur commande d'assemblage.//
                // donc si on lance 2 fois le calcul, il créée 2 fois les même lignes
                Item.CALCFIELDS("Qty. on Assembly Order");
                StkATerme := StkATerme + Item."Qty. on Assembly Order";

                IF StkATerme >= PtReappro THEN
                    CurrReport.SKIP();
                // FIN CFR le 11/05/2022

                // ---------------------------------
                // Calcul de la quantité à commander
                // ---------------------------------

                QteCde := PtReappro - StkATerme;

                QteMiniAchat := Item."Minimum Order Quantity";

                IF QteCde < QteMiniAchat THEN
                    QteCde := QteMiniAchat;

                IF Item."Purch. Multiple" <> 0 THEN
                    QteCde := ROUND(QteCde, Item."Purch. Multiple", '>');

                NextDocNo := NoSeriesMgt.GetNextNo('STK-FAB', TODAY, TRUE);
                CLEAR(LBOMHeader);
                LBOMHeader.VALIDATE("Document Type", LBOMHeader."Document Type"::Order);
                LBOMHeader."No." := NextDocNo;
                LBOMHeader.VALIDATE("No. Series", 'STK-FAB');
                LBOMHeader.INSERT(TRUE);
                LBOMHeader.SetWarningsOff();
                LBOMHeader.VALIDATE("Posting No. Series", LAssemblySetup."Posted Assembly Order Nos.");
                LBOMHeader.VALIDATE("Due Date", WORKDATE());
                LBOMHeader.VALIDATE("Location Code", Item.GETFILTER("Location Filter"));
                LBOMHeader.VALIDATE("Item No.", Item."No.");
                LBOMHeader.VALIDATE(Quantity, QteCde);
                LBOMHeader.MODIFY();
            end;

            trigger OnPreDataItem();
            begin

                IF Item.GETFILTER("Location Filter") = '' THEN
                    ERROR(Text50000Lbl);
                nb := Item.COUNT;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Type Réappro"; TypeRéappro)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TypeRéappro field.';
                }
            }
        }
    }

    labels
    {
    }

    trigger OnPostReport();
    begin
        MESSAGE('Temps : %1 -> %2 = %3 références', deb, TIME, nb);
    end;

    trigger OnPreReport();
    begin

        Counter := 0;

        Window.OPEN(
          Text006Lbl +
          Text007Lbl);



        deb := TIME;

        LAssemblySetup.GET();
    end;

    var
        LAssemblySetup: Record "Assembly Setup";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        Text006Lbl: Label 'Calculating the plan...\\';
        Text007Lbl: Label 'Item No.  #1##################', Comment = '%1 = Article';
        Text50000Lbl: Label 'Vous devez spécifier un Filtre Magasin !';
        //  "--- Critères ---": Integer;
        "TypeRéappro": Option Mini,Maxi;
        //  "---": Integer;
        Window: Dialog;
        Counter: Integer;
        deb: Time;
        nb: Integer;

}

