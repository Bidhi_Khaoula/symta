report 50094 "Calcul des relevés client"
{
    Permissions = TableData "Cust. Ledger Entry" = rm;
    ProcessingOnly = true;
    Caption = 'Calcul des relevés client';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = all;
    dataset
    {
        dataitem(Customer; Customer)
        {
            DataItemTableView = SORTING("No.")
                                ORDER(Ascending);
            RequestFilterFields = "No.", Name;
            dataitem("Cust. Ledger Entry"; "Cust. Ledger Entry")
            {
                DataItemLink = "Customer No." = FIELD("No.");
                DataItemTableView = SORTING("Customer No.", "Applies-to ID", Open, "Due Date")
                                    ORDER(Ascending)
                                    WHERE("Statement Date" = FILTER(''),
                                          "Document Type" = FILTER('Invoice|Credit Memo'));

                trigger OnAfterGetRecord();
                var
                    LFac: Record "Sales Invoice Header";
                    LAvo: Record "Sales Cr.Memo Header";
                begin

                    // AD Le 17-04-2012 =>
                    CASE "Cust. Ledger Entry"."Document Type" OF
                        "Cust. Ledger Entry"."Document Type"::Invoice:
                            BEGIN
                                LFac.GET("Cust. Ledger Entry"."Document No.");
                                IF LFac."Multi echéance" OR LFac."Echéances fractionnées" THEN
                                    CurrReport.SKIP();
                            END;
                        "Cust. Ledger Entry"."Document Type"::"Credit Memo":
                            BEGIN
                                LAvo.GET("Cust. Ledger Entry"."Document No.");
                                IF LAvo."Multi echéance" OR LAvo."Echéances fractionnées" THEN
                                    CurrReport.SKIP();
                            END;
                    END;
                    // FIN AD Le 17-04-2012

                    IF "Cust. Ledger Entry"."Due Date" <> DateEchéance THEN BEGIN
                        IF "Statment Header"."Statement No." <> '' THEN
                            // Insertion du relevé précédent
                            "Statment Header".INSERT(TRUE);
                        // Modification du client
                        Customer."Last Statement No." += 1;
                        Customer.MODIFY();

                        // Creation du releve
                        CounterOK += 1;
                        DateEchéance := "Cust. Ledger Entry"."Due Date";
                        "Statment Header".VALIDATE("Statement Date", StatementDate);
                        "Statment Header".VALIDATE("Statement No.", FORMAT(Customer."Last Statement No."));
                        "Statment Header".VALIDATE("Customer No.", Customer."No.");
                        "Statment Header".VALIDATE("Due Date", DateEchéance);
                        "Statment Header"."Statement Amount" := 0;
                        "Statment Header".VALIDATE("Payment Method Code", Customer."Payment Method Code");
                        "Statment Header".VALIDATE("Imprimer Relevé", Customer."Print Statements");
                    END;

                    Window.UPDATE(2, "Cust. Ledger Entry"."Document No.");

                    "Cust. Ledger Entry"."Statement No." := FORMAT(Customer."Last Statement No.");
                    "Cust. Ledger Entry"."Statement Date" := StatementDate;
                    "Cust. Ledger Entry"."No. Printed Statement" := 0;
                    "Cust. Ledger Entry".MODIFY();

                    "Cust. Ledger Entry".CALCFIELDS(Amount);
                    "Statment Header"."Statement Amount" += "Cust. Ledger Entry".Amount;
                end;

                trigger OnPostDataItem();
                begin

                    // Insertion du relevé précédent
                    IF "Statment Header"."Statement No." <> '' THEN
                        "Statment Header".INSERT(TRUE);
                end;

                trigger OnPreDataItem();
                begin

                    "Cust. Ledger Entry".SETRANGE("Posting Date", "date debut", "date fin");

                    // Initialisation du relevé.
                    "Statment Header".RESET();
                    "Statment Header"."Statement Amount" := 0;
                    DateEchéance := 0D;
                    CLEAR("Statment Header");
                end;
            }

            trigger OnAfterGetRecord();
            begin

                Counter := Counter + 1;
                Window.UPDATE(1, "No.");
                Window.UPDATE(3, ROUND(Counter / CounterTotal * 10000, 1));
            end;

            trigger OnPostDataItem();
            begin

                MESSAGE(Text50003Msg, CounterOK);
            end;

            trigger OnPreDataItem();
            begin

                IF StatementDate = 0D THEN
                    ERROR(Text50001Err);

                IF "date fin" = 0D THEN
                    ERROR(Text50004Err);

                Window.OPEN(Text50002Lbl);
                CounterTotal := COUNT;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("date debut_Name"; "date debut")
                {
                    Caption = 'date debut';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date debut field.';
                }
                field("date fin_Name"; "date fin")
                {
                    Caption = 'date fin';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date fin field.';
                }
                field(StatementDateName; StatementDate)
                {
                    Caption = 'Date relevé';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date relevé field.';
                }
            }
        }
    }


    var
        "Statment Header": Record "Sales Statment Header";
        StatementDate: Date;
        Window: Dialog;
        Counter: Integer;
        CounterOK: Integer;
        CounterTotal: Integer;
        "DateEchéance": Date;
        "date debut": Date;
        "date fin": Date;
        Text50001Err: Label 'Une date de relevé est obligatoire.';
        Text50002Lbl: Label 'Création des relevés #1########## \ #2########## \ @3@@@@@@@@@@@@@', Comment = '#1 ;#2 ;@3';
        Text50003Msg: Label '%1 Relevé créés.', Comment = '%1 counter OK';
        Text50004Err: Label 'Une date de fin est obligatoire.';
}

