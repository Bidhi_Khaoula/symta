report 50049 "EXPORT Tab_tar"
{
    Caption = 'EXPORT Tab_tar';
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\EXPORT Tab_tar.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Tab_tar_tmp; Tab_tar_tmp)
        {
            RequestFilterFields = c_fournisseur, ref_active, ref_fourni;

            trigger OnAfterGetRecord();
            var
                ItemVendor: Record "Item Vendor";
                GestionMulti: Codeunit "Gestion Multi-référence";
            begin

                NoEnr += 1;
                Window.UPDATE(1, FORMAT(NoEnr) + '/' + FORMAT(NbEnr));


                CLEAR(Item);
                CLEAR(SalesPrice);

                IF NOT Item.GET(GestionMulti.RechercheArticleByActive(Tab_tar_tmp.ref_active)) THEN BEGIN
                    ItemVendor.RESET();
                    ItemVendor.SETRANGE("Vendor No.", Tab_tar_tmp.c_fournisseur);
                    ItemVendor.SETRANGE("Vendor Item No.", Tab_tar_tmp.ref_fourni);
                    IF ItemVendor.FINDFIRST() THEN
                        Item.GET(ItemVendor."Item No.")
                    ELSE
                        IF queSiExistant THEN
                            CurrReport.SKIP();
                END;

                IF Item."No." <> '' THEN BEGIN
                    SalesPrice.RESET();
                    SalesPrice.SETRANGE("Item No.", Item."No.");
                    SalesPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
                    SalesPrice.SETRANGE("Starting Date", 0D, WORKDATE());
                    SalesPrice.SETRANGE("Sales Type", SalesPrice."Sales Type"::"All Customers");
                    IF SalesPrice.FINDFIRST() THEN;
                END;

                MakeDataBody();
            end;

            trigger OnPostDataItem();
            begin

                Window.CLOSE();
                Fichier.CLOSE();
                MESSAGE('Génération terminée');
            end;

            trigger OnPreDataItem();
            var
                LcompnyInfo: Record "Company Information";
            begin

                Fichier.TEXTMODE(TRUE);
                Fichier.WRITEMODE(TRUE);

                LcompnyInfo.GET();

                NomFichier := LcompnyInfo."Export EDI Folder" + '\TAB_TAR_' + USERID + '.txt';


                IF FILE.EXISTS(NomFichier) THEN
                    Fichier.OPEN(NomFichier)
                ELSE
                    Fichier.CREATE(NomFichier);


                MakeDataHeader();

                CLEAR(Window);
                Window.OPEN(ESK003Lbl);

                NbEnr := Tab_tar_tmp.COUNT;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(queSiExistantName; queSiExistant)
                {
                    Caption = 'Exporter Ligne que si article existant';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Exporter Ligne que si article existant field.';
                }
            }
        }
    }
    var
        Item: Record Item;
        SalesPrice: Record "Sales Price";
        Ligne: Text[1024];
        NomFichier: Text[250];
        Fichier: File;
        Window: Dialog;
        NbEnr: Integer;
        NoEnr: Integer;
        queSiExistant: Boolean;
        ESK003Lbl: Label 'EXPORT  :  #1##########', Comment = '#1';

    local procedure MakeDataHeader();
    begin
        // Export Excel
        Ligne := '';
        Ligne += Tab_tar_tmp.FIELDNAME(c_fournisseur) + ';';
        Ligne += Tab_tar_tmp.FIELDNAME(ref_active) + ';';
        Ligne += Tab_tar_tmp.FIELDNAME(ref_fourni) + ';';
        Ligne += Tab_tar_tmp.FIELDNAME(design_fourni) + ';';

        Ligne += Item.FIELDNAME("No.") + ';';
        Ligne += Item.FIELDNAME("No. 2") + ';';
        Ligne += Item.FIELDNAME(Description) + ';';
        Ligne += Item.FIELDNAME("Manufacturer Code") + ';';
        Ligne += Item.FIELDNAME("Item Category Code") + ';';

        Ligne += SalesPrice.FIELDNAME("Prix catalogue") + ';';
        Ligne += SalesPrice.FIELDNAME(Coefficient) + ';';
        Ligne += SalesPrice.FIELDNAME("Unit Price") + ';';

        Ligne += Tab_tar_tmp.FIELDNAME(px_brut) + ';';
        Ligne += Tab_tar_tmp.FIELDNAME(code_remise) + ';';

        Ligne += Item.FIELDNAME("Stock Mort") + ';';



        Fichier.WRITE(Ligne);
    end;

    procedure MakeDataBody();
    begin
        Ligne := '';
        Ligne += Tab_tar_tmp.c_fournisseur + ';';
        Ligne += Tab_tar_tmp.ref_active + ';';
        Ligne += Tab_tar_tmp.ref_fourni + ';';
        Ligne += Tab_tar_tmp.design_fourni + ';';

        Ligne += Item."No." + ';';
        Ligne += Item."No. 2" + ';';
        Ligne += Item.Description + ';';
        Ligne += Item."Manufacturer Code" + ';';
        Ligne += Item."Item Category Code" + ';';

        Ligne += FormatDecimal(SalesPrice."Prix catalogue") + ';';
        Ligne += FormatDecimal(SalesPrice.Coefficient) + ';';
        Ligne += FormatDecimal(SalesPrice."Unit Price") + ';';

        Ligne += FormatDecimal(Tab_tar_tmp.px_brut) + ';';
        Ligne += Tab_tar_tmp.code_remise + ';';

        Ligne += FORMAT(Item."Stock Mort") + ';';


        Fichier.WRITE(Ligne);
    end;

    procedure FormatDecimal(Value: Decimal): Text[50];
    begin
        EXIT(FORMAT(Value, 0, '<Precision,2:><Sign><Integer><Decimals>'));
    end;
}

