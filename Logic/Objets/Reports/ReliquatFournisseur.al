report 50017 "Reliquat Fournisseur"
{
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(diVendor; Vendor)
        {
            RequestFilterFields = "Code Appro";
            dataitem("Purchase Line"; "Purchase Line")
            {
                DataItemLink = "Buy-from Vendor No." = FIELD("No.");
                DataItemTableView = SORTING("Document Type", "Document No.", "Line No.")
                                    ORDER(Ascending)
                                    WHERE("Document Type" = CONST(Order),
                                          "Outstanding Quantity" = FILTER(> 0));
                RequestFilterFields = "Buy-from Vendor No.";

                trigger OnAfterGetRecord();
                var
                    lGestioConso: Codeunit CalculConso;
                    lDateBase: Date;
                    lConso12: Decimal;
                    lBODepart: Decimal;
                    lPourcentage: Decimal;
                begin

                    PurchHeader.GET("Purchase Line"."Document Type", "Purchase Line"."Document No.");
                    CLEAR(ItemVendor);
                    IF ItemVendor.GET("Purchase Line"."Buy-from Vendor No.", "Purchase Line"."No.", "Purchase Line"."Variant Code") THEN;
                    Item.GET("Purchase Line"."No.");
                    Item.CALCFIELDS(Inventory);
                    Item.CalcAttenduReserve(Reservé, Attendu);
                    Vendor.GET("Purchase Line"."Buy-from Vendor No.");

                    // CFR le 22/09/2021 => Régie : ajout filtre sur pourcentage prioritaire de travail - On rappatrie les calcul dans le OnAfterRecord
                    //---------------------------------------------------------------------------------
                    // 12  mois glissants
                    lDateBase := WORKDATE();
                    lConso12 := lGestioConso.RechecherConsommation("Purchase Line"."No.", CALCDATE('<-1Y>', lDateBase), lDateBase); // Conso 12 M glis
                    // BO-Depart
                    lBODepart := "Purchase Line"."Outstanding Quantity" - "Purchase Line".GetQtyOnDepart();
                    // Pourcentage prioritaire de travail
                    IF (lConso12 <> 0) THEN
                        lPourcentage := (Item.Inventory + "Purchase Line".GetQtyOnDepart() - Reservé) / lConso12 * 100
                    ELSE
                        lPourcentage := 0;

                    IF (lPourcentage > gMaxPourcentage) AND (gMaxPourcentage <> 0) THEN
                        CurrReport.SKIP();

                    // Export Excel
                    MakeExcelDataBody(lConso12, lBODepart, lPourcentage);
                end;
            }
        }
    }

    requestpage
    {
        Caption = 'Options';

        layout
        {
            area(content)
            {
                field(gMaxPourcentage; gMaxPourcentage)
                {
                    Caption = 'Poucentage maximum';
                    ToolTip = 'Pourcentage de travail maximum';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnPostReport();
    begin
        CreateExcelbook();
    end;

    trigger OnPreReport();
    begin
        MakeExcelInfo();
    end;

    var
        Vendor: Record Vendor;
        PurchHeader: Record "Purchase Header";
        ItemVendor: Record "Item Vendor";
        TempExcelBuf: Record "Excel Buffer" temporary;
        Item: Record Item;
        "Reservé": Decimal;
        Attendu: Decimal;
        gMaxPourcentage: Decimal;
        Excel002Lbl: Label 'Data';
        Excel003Lbl: Label 'Customer - Order Detail';
        Excel004Lbl: Label 'Company Name';
        Excel005Lbl: Label 'Report No.';
        Excel006Lbl: Label 'Report Name';
        Excel007Lbl: Label 'User ID';
        Excel008Lbl: Label 'Date';
        Excel010Lbl: Label 'Sales Order Lines Filters';


    procedure "--- Excel ----"();
    begin
    end;

    procedure MakeExcelInfo();
    begin
        // Export Excel
        //TempExcelBuf.SetUseInfoSheed;
        TempExcelBuf.AddInfoColumn(FORMAT(Excel004Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(COMPANYNAME, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel006Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(FORMAT(Excel003Lbl), FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel005Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        //TempExcelBuf.AddInfoColumn(REPORT::"Taux de service/Client",FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel007Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(USERID, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel008Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(TODAY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel010Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn("Purchase Line".GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.ClearNewRow();
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader();
    var
        lTextDeltaLbl: Label 'Delta réception à la date du %1', Comment = '%1 = Date Reception';
        lIntitule: Text[100];
    begin
        // Export Excel
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Document No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Line No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(PurchHeader.FIELDCAPTION("Order Create User"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Type Cde', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('N° Cde fournisseur', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Buy-from Vendor No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Nom', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(PurchHeader.FIELDCAPTION("Order Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        // CFR le 10/05/2022 => Régie : Date d'envoi commande > Champ [50002]
        TempExcelBuf.AddColumn(PurchHeader.FIELDCAPTION("Order Dispatch Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Requested Receipt Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Promised Receipt Date"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Commentaires', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);

        // CFR le 15/09/2021 => Régie
        lIntitule := STRSUBSTNO(lTextDeltaLbl, TODAY());
        TempExcelBuf.AddColumn(lIntitule, FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        // FIN CFR le 15/09/2021

        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);

        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Recherche référence"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Vendor Item No."), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(ItemVendor.FIELDCAPTION("Vendor Item Desciption"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION(Description), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Unit of Measure Code"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION(Quantity), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Outstanding Quantity"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line".FIELDCAPTION("Net Unit Cost ctrl fac"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Départ', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Physique', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Vendu', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);

        // CFR le 15/09/2021 => Régie
        //TempExcelBuf.AddColumn('Attendu',FALSE,'',TRUE,FALSE,TRUE,'@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Attendu toutes les commandes', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        // FIN CFR le 15/09/2021

        TempExcelBuf.AddColumn('Stocké', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('12 Mois glissants', FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        // CFR le 15/09/2021 => Régie
        TempExcelBuf.AddColumn('BO - Départ', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Pourcentage prioritaire de travail', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        // FIN CFR le 15/09/2021
    end;

    procedure MakeExcelDataBody(pConso12: Decimal; pBODepart: Decimal; pPourcentage: Decimal);
    var
        _vendor: Record Vendor;
        lDelta: Integer;
    begin
        // Export Excel
        TempExcelBuf.NewRow();


        TempExcelBuf.AddColumn("Purchase Line"."Document No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line"."Line No." / 10000, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(PurchHeader."Order Create User", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(PurchHeader."Type de commande", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(PurchHeader."Vendor Order No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Purchase Line"."Buy-from Vendor No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        IF _vendor.GET("Purchase Line"."Buy-from Vendor No.") THEN;
        TempExcelBuf.AddColumn(_vendor.Name, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(PurchHeader."Order Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        // CFR le 10/05/2022 => Régie : Date d'envoi commande > Champ [50002]
        TempExcelBuf.AddColumn(PurchHeader."Order Dispatch Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.AddColumn("Purchase Line"."Requested Receipt Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.AddColumn("Purchase Line"."Promised Receipt Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.AddColumn("Purchase Line".Commentaires, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        // CFR le 15/09/2021 => Régie
        lDelta := 0;
        IF "Purchase Line"."Requested Receipt Date" <> 0D THEN
            lDelta := "Purchase Line"."Requested Receipt Date" - TODAY();
        IF "Purchase Line"."Promised Receipt Date" <> 0D THEN
            lDelta := "Purchase Line"."Promised Receipt Date" - TODAY();
        TempExcelBuf.AddColumn(lDelta, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        // FIN CFR le 15/09/2021

        TempExcelBuf.AddColumn("Purchase Line"."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        IF NOT Vendor."Masquer Ref. Active" THEN
            TempExcelBuf.AddColumn("Purchase Line"."Recherche référence", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text)
        ELSE
            TempExcelBuf.AddColumn('', FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        TempExcelBuf.AddColumn("Purchase Line"."item Reference No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(ItemVendor."Vendor Item Desciption", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        // CFR le 21/09/2021 => Régie : description de la fiche article
        //TempExcelBuf.AddColumn("Purchase Line".Description,FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Item.Description, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        TempExcelBuf.AddColumn("Purchase Line"."Unit of Measure Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line".Quantity), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Outstanding Quantity"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line"."Net Unit Cost ctrl fac"), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal("Purchase Line".GetQtyOnDepart()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Item.Inventory), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Reservé), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(Attendu), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FORMAT(Item.Stocké), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        // CFR le 22/09/2021 => Régie : ajout filtre sur pourcentage prioritaire de travail - On rappatrie les calcul dans le OnAfterRecord
        //---------------------------------------------------------------------------------
        TempExcelBuf.AddColumn(FormatDecimal(pConso12), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(pBODepart), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(FormatDecimal(pPourcentage), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        /*
        // 12  mois glissants
        DateBase := WORKDATE;
        Valeur :=LGestioConso.RechecherConsommation("Purchase Line"."No.", CALCDATE('<-1Y>', DateBase), DateBase); // Conso 12 M glis
        TempExcelBuf.AddColumn(FormatDecimal(Valeur),FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Number);
        
        // CFR le 15/09/2021 => Régie
        lDec := "Purchase Line"."Outstanding Quantity" - "Purchase Line".GetQtyOnDepart();
        TempExcelBuf.AddColumn(FormatDecimal(lDec),FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Number);
        IF (Valeur <> 0) THEN
          lDec := (Item.Inventory + "Purchase Line".GetQtyOnDepart() - Reservé) / Valeur * 100
        ELSE
          lDec := 0;
        TempExcelBuf.AddColumn(FormatDecimal(lDec),FALSE,'',FALSE,FALSE,FALSE,'', TempExcelBuf."Cell Type"::Number);
        // FIN CFR le 15/09/2021
        */

    end;

    procedure MakeExcelDataTotal();
    begin
        // Export Excel
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBookAndOpenExcel('', Excel002Lbl, Excel003Lbl, COMPANYNAME, USERID)
        //TempExcelBuf.CreateBook(Excel002);
        //TempExcelBuf.WriteSheet(Excel003,COMPANYNAME,USERID);
        //TempExcelBuf.GiveUserControl;
        //ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[30];
    begin
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    end;

    procedure CalculerConso(ItemNo: Code[20]; DateDebut: Date; DateFin: Date): Decimal;
    var
        LItem: Record Item;
    begin

        LItem.GET(ItemNo);

        LItem.SETRANGE("Date Filter", DateDebut, DateFin);

        LItem.CALCFIELDS("Consommation Navision", "Consommation Dbx", "Qte Ecritures Conso. Kit");

        EXIT(ROUND(LItem."Consommation Navision" + LItem."Consommation Dbx" + LItem."Qte Ecritures Conso. Kit", 1));
    end;
}

