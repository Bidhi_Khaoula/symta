report 50019 "Extraire Relance Télèphonique"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Extraire Relance Télèphonique.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Customer; Customer)
        {
            DataItemTableView = SORTING("No.")
                                WHERE("Reminder Terms Code" = FILTER(<> ''));
            dataitem("Cust. Ledger Entry"; "Cust. Ledger Entry")
            {
                CalcFields = "Remaining Amount", Amount;
                DataItemLink = "Customer No." = FIELD("No.");
                DataItemTableView = SORTING("Customer No.", Open, "Posting Date")
                                    WHERE(Open = CONST(true));

                trigger OnAfterGetRecord();
                var
                    Niveau_relance: Record "Reminder Level";
                begin
                    relance.Init();
                    relance."N° client" := "Cust. Ledger Entry"."Customer No.";
                    relance."Date compta" := "Cust. Ledger Entry"."Posting Date";
                    relance."Date d'echéance" := "Cust. Ledger Entry"."Due Date";
                    relance.Désignation := "Cust. Ledger Entry".Description;
                    relance."Type document" := FORMAT("Cust. Ledger Entry"."Document Type");
                    relance."Montant initial" := "Cust. Ledger Entry".Amount;
                    relance."Montant restant" := "Cust. Ledger Entry"."Remaining Amount";
                    relance."N° sequence client" := "Cust. Ledger Entry"."Entry No.";
                    relance."Dernier niveau" := "Cust. Ledger Entry"."Last Issued Reminder Level";
                    relance."N° de document" := "Cust. Ledger Entry"."Document No.";
                    IF Niveau_relance.GET(Customer."Reminder Terms Code", "Cust. Ledger Entry"."Last Issued Reminder Level" + 1) THEN
                        relance."Prochain niveau" := "Cust. Ledger Entry"."Last Issued Reminder Level" + 1
                    ELSE
                        relance."Prochain niveau" := "Cust. Ledger Entry"."Last Issued Reminder Level";

                    relance."Code relance" := Customer."Reminder Terms Code";

                    relance."Groupe compta client" := Customer."Customer Posting Group";
                    relance."Mode de reglement client" := Customer."Payment Method Code";
                    relance."Ville client" := Customer.City;

                    IF NOT relance.INSERT() THEN relance.MODIFY();
                end;

                trigger OnPreDataItem();
                begin

                    IF "Date début compta" <> 0D THEN
                        "Cust. Ledger Entry".SETRANGE("Posting Date", "Date début compta", "Date fin compta");

                    IF "Fin Echéance" <> 0D THEN
                        "Cust. Ledger Entry".SETRANGE("Due Date", "debut echéance", "Fin Echéance")
                    ELSE
                        "Cust. Ledger Entry".SETRANGE("Due Date", 0D, TODAY);

                    IF "Montant débiteur seul" THEN
                        "Cust. Ledger Entry".SETFILTER("Debit Amount", '>0');
                end;
            }

            trigger OnAfterGetRecord();
            begin
                window.UPDATE();
            end;

            trigger OnPostDataItem();
            begin
                window.CLOSE();
            end;

            trigger OnPreDataItem();
            begin
                window.OPEN('Client : #1##########', Customer."No.");
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Date début compta Name"; "Date début compta")
                {
                    ApplicationArea = All;
                    Caption = 'Date début compta Name';
                    ToolTip = 'Specifies the value of the Date début compta Name field.';
                }
                field("Date fin compta Name"; "Date fin compta")
                {
                    ApplicationArea = All;
                    Caption = 'Date fin compta Name';
                    ToolTip = 'Specifies the value of the Date fin compta Name field.';
                }
                field("debut echéance Name"; "debut echéance")
                {
                    ApplicationArea = All;
                    Caption = 'debut echéance Name';
                    ToolTip = 'Specifies the value of the debut echéance Name field.';
                }
                field("Fin Echéance Name"; "Fin Echéance")
                {
                    ApplicationArea = All;
                    Caption = 'Fin Echéance Name';
                    ToolTip = 'Specifies the value of the Fin Echéance Name field.';
                }
                field("Montant débiteur seul Name"; "Montant débiteur seul")
                {
                    ApplicationArea = All;
                    Caption = 'Montant débiteur seul Name';
                    ToolTip = 'Specifies the value of the Montant débiteur seul Name field.';
                }
            }
        }


    }


    var
        relance: Record "Relance Telephonique";
        "Date début compta": Date;
        "Date fin compta": Date;
        "debut echéance": Date;
        "Fin Echéance": Date;
        window: Dialog;
        "Montant débiteur seul": Boolean;
}

