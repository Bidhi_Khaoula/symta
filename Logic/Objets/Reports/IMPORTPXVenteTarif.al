report 50105 "IMPORT PX Vente Tarif"
{
    ProcessingOnly = true;
    UseRequestPage = false;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'IMPORT PX Vente Tarif';
    ApplicationArea = All;
    dataset
    {
        dataitem(integer; integer)
        {
            DataItemTableView = SORTING(Number)
                                ORDER(Ascending);

            trigger OnAfterGetRecord();
            var
                Item: Record Item;
                //   CustPriceGroup: Record "Customer Price Group";
                TarifClient: Record "Sales Price";
                CUMultiRef: Codeunit "Gestion Multi-référence";
                DateDeb: Date;
                //  VarDec: Decimal;
                CArticle: Code[20];
                QteMini: Integer;
                Prix: Decimal;
                PrixCatalogue: Decimal;
                CoefCatalogue: Decimal;
                AllowInvoiceDiscount: Boolean;
                PriceFound: Boolean;
                DateFin: Date;
            begin

                LineNo := LineNo + 1;

                GetDonneesExcel();

                // Lecture de l'article
                EVALUATE(CArticle, Excel_CArticle);
                IF NOT Item.GET(CUMultiRef.RechercheArticleByActive(CArticle)) THEN
                    //ERROR('Article %1 introuvable', CArticle);
                    CurrReport.SKIP();

                TarifClient.INIT();

                TarifClient.VALIDATE("Item No.", Item."No.");

                CASE Excel_TypeCommande OF
                    'TOUS':
                        TarifClient.VALIDATE("Type de commande", TarifClient."Type de commande"::Tous);
                    'DEPANNAGE':
                        TarifClient.VALIDATE("Type de commande", TarifClient."Type de commande"::Dépannage);
                    'REAPPRO':
                        TarifClient.VALIDATE("Type de commande", TarifClient."Type de commande"::Réappro);
                    'STOCK':
                        TarifClient.VALIDATE("Type de commande", TarifClient."Type de commande"::Stock);
                    ELSE
                        ERROR('Type de commande doit être %1-%2-%3-%4 pour l''article %5', 'TOUS', 'DEPANNAGE', 'REAPPRO', 'STOCK', CArticle);
                END;

                CASE Excel_TypeTarif OF
                    'TOUSCLIENT':
                        TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::"All Customers");
                    'CLIENT':
                        TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::Customer);
                    'CENTRALE':
                        TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::Centrale);
                    'SOUSGRPTTARIF':
                        TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::"Sous Groupe tarif");
                    // MC Le 02-04-2012 => Je pense que c'est une erreur de code donc commentaire
                    //'GRPTTARIF' : TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::Customer);
                    'GRPTTARIF':
                        TarifClient.VALIDATE("Sales Type", TarifClient."Sales Type"::"Customer Price Group");
                    // FIN MC Le 02-04-2012
                    ELSE
                        ERROR('Type de tarif doit être %1-%2-%3-%4-%5 pour l''article %6', 'TOUSCLIENT', 'CLIENT', 'CENTRALE', 'SOUSGRPTTARIF',
                        'GRPTTARIF', CArticle);
                END;

                IF TarifClient."Sales Type" <> TarifClient."Sales Type"::"All Customers" THEN
                    TarifClient.VALIDATE("Sales Code", Excel_CTarif);

                IF NOT (Excel_PrixNet IN ['O', 'N']) THEN
                    ERROR('Prix Net Doit être O ou N pour l''article %1', CArticle);

                EVALUATE(DateDeb, Excel_DateDebut);
                IF DateDeb = 0D THEN DateDeb := WORKDATE();

                EVALUATE(DateFin, Excel_DateFin);
                IF DateFin = 0D THEN DateFin := 0D;


                EVALUATE(QteMini, Excel_QteMini);
                IF QteMini <= 1 THEN QteMini := 0;

                EVALUATE(Prix, Excel_Prix);
                Prix := ROUND(Prix, 0.01);

                EVALUATE(PrixCatalogue, Excel_PrixCatalogue);
                PrixCatalogue := ROUND(PrixCatalogue, 0.01);

                EVALUATE(CoefCatalogue, Excel_CoefCatalogue);
                CoefCatalogue := ROUND(CoefCatalogue, 0.01);

                TarifClient.VALIDATE("Starting Date", DateDeb);
                TarifClient.VALIDATE("Minimum Quantity", QteMini);

                IF PrixCatalogue <> 0 THEN
                    TarifClient.VALIDATE("Prix catalogue", PrixCatalogue);

                IF CoefCatalogue <> 0 THEN
                    TarifClient.VALIDATE(Coefficient, CoefCatalogue);

                // CFR le 26/10/2025 - Régie : Ne pas importer le prix unitaire qui est recalculé à partir de l'import prix catalogue et coefficient
                //  IF (Prix <> 0) THEN
                //    TarifClient.VALIDATE("Unit Price", Prix);
                // FIN CFR le 26/10/2025

                TarifClient.VALIDATE("Unit of Measure Code", Item."Base Unit of Measure");

                TarifClient.VALIDATE("Price Includes VAT", FALSE);
                TarifClient.VALIDATE("Allow Invoice Disc.", TRUE);
                // MCO Le 08-06-2017 => Régie
                // Nouvelle règle : On prend la valeur de l'ancien tarif, si on ne trouve pas on prend la valeur de la colonne Excel
                PriceFound := FALSE;
                AllowInvoiceDiscount := GetLastPrice(TarifClient, PriceFound);
                IF PriceFound THEN
                    TarifClient.VALIDATE("Allow Line Disc.", AllowInvoiceDiscount)
                ELSE
                    // FIN MCO Le 08-06-2017
                    TarifClient.VALIDATE("Allow Line Disc.", Excel_PrixNet = 'N');

                //VDOUSSET 16/01/2015
                IF Excel_Utilisateur <> '' THEN
                    TarifClient.VALIDATE("user maj prix", Excel_Utilisateur);

                //VDOUSSET 24/01/2019
                TarifClient.VALIDATE("Ending Date", DateFin);

                IF TarifClient.INSERT(TRUE) THEN
                    Item.CloturerAncienPrix(TarifClient);
            end;

            trigger OnPreDataItem();
            begin

                TempExcelBuf.FINDLAST();
                Integer.SETRANGE(Number, 2, TempExcelBuf."Row No.");
                LineNo := 1;
            end;
        }
    }


    trigger OnInitReport();
    var
        FileMgt: Codeunit "File Management";
    begin

        FileName := FileMgt.UploadFile(Text006Lbl, ExcelFileExtensionTok);
        IF FileName = '' THEN
            CurrReport.QUIT();

        SheetName := TempExcelBuf.SelectSheetsName(FileName);
        IF SheetName = '' THEN
            CurrReport.QUIT();
    end;

    trigger OnPostReport();
    begin
        MESSAGE('Mise à jour terminée \Fichier traité : %1', FileName);
    end;

    trigger OnPreReport();
    begin

        // ------------------------------------------------------------
        //
        // ------------------------------------------------------------



        // -------------------------
        // Lecture des données Excel
        // -------------------------

        TempExcelBuf.LOCKTABLE();
        TempExcelBuf.RESET();
        TempExcelBuf.DELETEALL();
        ReadExcelSheet();
        COMMIT();
    end;

    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        FileName: Text[250];
        SheetName: Text[250];
        LineNo: Integer;

        Excel_CArticle: Text[250];
        Excel_TypeCommande: Text[250];
        Excel_TypeTarif: Text[250];
        Excel_CTarif: Text[250];
        Excel_QteMini: Text[250];
        Excel_Prix: Text[250];
        Excel_CoefCatalogue: Text[250];
        Excel_PrixCatalogue: Text[250];
        Excel_DateDebut: Text[250];
        Excel_PrixNet: Text[250];
        Excel_Utilisateur: Text[250];
        // Text001: Label 'Mise à jour du fichier ''%1'' terminée';
        Text006Lbl: Label 'Import Excel File';
        /*   "AVOIR CLIENT": Label 'AVOIC';
          "FACTURE CLIENT": Label 'FACTC'; */
        ExcelFileExtensionTok: Label '.xlsx', Locked = true;
        Excel_DateFin: Text[250];

    local procedure ReadExcelSheet();
    begin
        TempExcelBuf.OpenBook(FileName, SheetName);
        TempExcelBuf.ReadSheet();
    end;

    procedure GetDonneesExcel();
    begin

        // -----------
        // C_Article
        // -----------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'A');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_CArticle := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_CArticle := '';

        // -----------
        // Type Commande
        // -----------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'B');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_TypeCommande := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_TypeCommande := '';

        // -----------
        // Type Tarif
        // -----------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'C');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_TypeTarif := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_TypeTarif := '';


        // -----------
        // Tarif
        // -----------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'D');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_CTarif := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_CTarif := '';

        // ----------------
        // QteMini
        // ----------------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'E');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_QteMini := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_QteMini := '';

        // -------
        // Prix Catalogue
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'F');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_PrixCatalogue := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_PrixCatalogue := '';

        // -------
        // Prix Coef Catalogue
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'G');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_CoefCatalogue := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_CoefCatalogue := '';

        // -------
        // Prix
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'H');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_Prix := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_Prix := '';

        // -------
        // date Debut
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'I');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_DateDebut := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_DateDebut := '';

        // -------
        // Prix Net
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'J');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_PrixNet := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_PrixNet := '';

        // -------
        // UTILISATEUR  //VDOUSSET 16/01/15
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'K');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_Utilisateur := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_Utilisateur := '';


        // -------
        // date FIN //VDOUSSET 24/01/2019
        // -------
        TempExcelBuf.SETRANGE("Row No.", LineNo);
        TempExcelBuf.SETRANGE(xlColID, 'L');
        IF TempExcelBuf.FINDFIRST() THEN
            Excel_DateFin := TempExcelBuf."Cell Value as Text"
        ELSE
            Excel_DateFin := '';
    end;

    local procedure GetLastPrice(NvPrix: Record "Sales Price"; var _pExist: Boolean): Boolean;
    var
        AncPrix: Record "Sales Price";
    begin
        _pExist := FALSE;

        // recherche lde dernier tarif existant pour les critères
        AncPrix.RESET();
        AncPrix.SETRANGE("Item No.", NvPrix."Item No.");
        AncPrix.SETRANGE("Sales Type", NvPrix."Sales Type");
        AncPrix.SETRANGE("Type de commande", NvPrix."Type de commande");
        AncPrix.SETRANGE("Sales Code", NvPrix."Sales Code");
        AncPrix.SETRANGE("Starting Date", 0D, CALCDATE('<-1J>', NvPrix."Starting Date"));
        AncPrix.SETRANGE("Currency Code", NvPrix."Currency Code");
        AncPrix.SETRANGE("Variant Code", NvPrix."Variant Code");
        AncPrix.SETFILTER("Unit of Measure Code", '%1|%2', NvPrix."Unit of Measure Code", '');
        AncPrix.SETRANGE("Minimum Quantity", NvPrix."Minimum Quantity");
        AncPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, NvPrix."Starting Date");
        IF AncPrix.FINDLAST() THEN BEGIN
            _pExist := TRUE;
            EXIT(AncPrix."Allow Line Disc.");
        END;
    end;
}

