report 50159 "Réappro Emplacement Picking"
{
    Caption = 'Réappro Emplacement Picking';
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {
            DataItemTableView = SORTING("No. 2");
            RequestFilterFields = "No. 2", "Manufacturer Code";

            trigger OnAfterGetRecord();
            var
                LBinContent: Record "Bin Content";
                //LWMSMngt: Codeunit "WMS Management";                
                LCalcuConso: Codeunit CalculConso;
                CduLFunctions: Codeunit "Codeunits Functions";
                LConso: Decimal;

            begin

                NoENr += 1;
                Window.UPDATE(1, Item."No. 2");
                Window.UPDATE(2, ROUND(NoENr / NbEnr * 10000, 1));

                CodeEmplacement := '';
                CodeZone := '';
                StockPicking := 0;
                LConso := 0;
                QteReappro := 0;

                // Recherche de l'emplacement par defaut
                CduLFunctions.ESK_GetDefaultBin(Item."No.", '', CompanyInfo."Location Code", CodeEmplacement, CodeZone);

                IF CodeEmplacement <> '' THEN BEGIN
                    // Calcul du stock de l'emplacement par defaut
                    LBinContent.GET(CompanyInfo."Location Code", CodeEmplacement, Item."No.", '', Item."Base Unit of Measure");
                    LBinContent.CALCFIELDS(Quantity);
                    StockPicking := LBinContent.Quantity;
                    capacite := LBinContent.capacité;
                END;

                // Calcul de la conso trimestrielle
                LConso := LCalcuConso.RechecherConsommation(Item."No.", CALCDATE('<-1Y>', DateBase), DateBase);
                LConso := LConso / 4;

                IF StockPicking >= LConso THEN
                    CurrReport.SKIP();

                QteReappro := LConso - StockPicking;

                IF PrintToExcel THEN
                    MakeExcelDataBody();
            end;

            trigger OnPostDataItem();
            begin
                Window.CLOSE();
            end;

            trigger OnPreDataItem();
            begin


                CompanyInfo.GET();
                DateBase := WORKDATE();


                NbEnr := Item.COUNT;
                CLEAR(Window);
                Window.OPEN(ESK003Lbl);
            end;
        }
    }
    var
        TempExcelBuf: Record "Excel Buffer" temporary;
        CompanyInfo: Record "Company Information";
        DateBase: Date;
        QteReappro: Decimal;
        StockPicking: Decimal;
        capacite: Decimal;
    //    "--- Excel ---": Integer;
        CodeEmplacement: Code[10];
        CodeZone: Code[10];
        PrintToExcel: Boolean;

      //  "--- Fenetre ---": Integer;
        NbEnr: Integer;
        NoENr: Integer;
        Window: Dialog;
        Excel002Lbl: Label 'Data';
        Excel003Lbl: Label 'Customer - Order Detail';
        Excel004Lbl: Label 'Company Name';
        Excel005Lbl: Label 'Report No.';
        Excel006Lbl: Label 'Report Name';
        Excel007Lbl: Label 'User ID';
        Excel008Lbl: Label 'Date';
        Excel010Lbl: Label 'Sales Order Lines Filters';
        ESK003Lbl: Label '" MAJ  :  #1########## \ @2@@@@@@@@@@@@@@"' , Comment = '%1 ; %2';

    procedure "--- Excel ----"();
    begin
    end;

    procedure MakeExcelInfo();
    begin
        // Export Excel
        TempExcelBuf.SetUseInfoSheet();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel004Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(COMPANYNAME, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel006Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(FORMAT(Excel003Lbl), FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel005Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(REPORT::"Réappro Emplacement Picking", FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel007Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(USERID, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel008Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(TODAY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Excel010Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(Item.GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.ClearNewRow();
        MakeExcelDataHeader()
    end;

    local procedure MakeExcelDataHeader();
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn(Item.FIELDCAPTION("No. 2"), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Item.FIELDCAPTION(Description), FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Zone', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Empl. Picking', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Stock Picking', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Qté à approvisionner', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Taux', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Capacité', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn('Emplacement ...', FALSE, '', TRUE, FALSE, TRUE, '@', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    var
        LBincontent: Record "Bin Content";
        taux: Decimal;
    begin
        // Export Excel
        TempExcelBuf.NewRow();

        TempExcelBuf.AddColumn(Item."No. 2", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Item.Description, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(CodeZone, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(CodeEmplacement, FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(StockPicking), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(QteReappro), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        IF QteReappro + StockPicking <> 0 THEN
            taux := 100 * QteReappro / (QteReappro + StockPicking)
        ELSE
            taux := 0;
        TempExcelBuf.AddColumn(FormatDecimal(taux), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FormatDecimal(capacite), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);

        LBincontent.RESET();
        LBincontent.SETRANGE("Location Code", CompanyInfo."Location Code");
        LBincontent.SETRANGE("Item No.", Item."No.");
        LBincontent.SETRANGE(Default, FALSE);
        LBincontent.SETFILTER(Quantity, '<>%1', 0);
        IF LBincontent.FINDFIRST() THEN
            REPEAT
                TempExcelBuf.AddColumn(LBincontent."Bin Code", FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);

                TempExcelBuf.AddColumn(FormatDecimal(LBincontent.CalcQtyUOM()), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
                TempExcelBuf.AddColumn(LBincontent.GetCommentaireInfoEntrée('DATE'), FALSE, '', FALSE, FALSE, FALSE, '@', TempExcelBuf."Cell Type"::Text);


            UNTIL LBincontent.NEXT() = 0;
    end;

    procedure CreateExcelbook();
    begin
        // Export Excel
        TempExcelBuf.CreateBook('', Excel002Lbl);
        TempExcelBuf.WriteSheet(Excel003Lbl, COMPANYNAME, USERID);
        //TempExcelBuf.GiveUserControl;
        ERROR('');
    end;

    procedure FormatDecimal(Value: Decimal): Text[30];
    begin
        EXIT(FORMAT(Value, 0, '<Sign><Integer><Decimals><Precision,2:2>')); //<Comma,,>
    end;
}

