report 50033 "Nomenclature Douanière"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Nomenclature Douanière.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Nomenclature Douanière';

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            column(CompanyInfo1Picture; CompanyInfo.Picture)
            {
            }
            column(CompanyInfo1PictureAddress; CompanyInfo."Picture Address")
            {
            }
            column(TitleDoc; TitleDocLbl)
            {
            }
            column(CodeDouanierLbl; CodeDouanierLbl)
            {
            }
            column(PaysOrigineLbl; PaysOrigineLbl)
            {
            }
            column(TotalQteLbl; TotalQteLbl)
            {
            }
            column(TotalPoidsNetLb; TotalPoidsNetLbL)
            {
            }
            column(TotalMontantLbl; TotalMontantLbl)
            {
            }
            column(TotalLbl; TotalLbl)
            {
            }
            column(TotauxLbl; TotauxLbl)
            {
            }
            column(No_SalesInvHdr; "No.")
            {
            }
            column(TxtPied1; textSIEGSOC[1])
            {
            }
            column(TxtPied2; textSIEGSOC[2])
            {
            }
            column(TxtPied3; textSIEGSOC[3])
            {
            }
            column(ShpMethodDescCaption; ShpMethodDescCaptionLbl)
            {
            }
            column(HomePageCaption; HomePageCaptionCapLbl)
            {
            }
            column(EMailCaption; EMailCaptionLbl)
            {
            }
            column(DocDateCaption; DocDateCaptionLbl)
            {
            }
            column(gDisplayDetails; gDisplayDetails)
            {
            }
            column(DocCaptCopyText; DocumentCaption)
            {
            }
            column(CustAddr1; CustAddr[1])
            {
            }
            column(CompanyAddr1; CompanyAddr[1])
            {
            }
            column(CustAddr2; CustAddr[2])
            {
            }
            column(CompanyAddr2; CompanyAddr[2])
            {
            }
            column(CustAddr3; CustAddr[3])
            {
            }
            column(CompanyAddr3; CompanyAddr[3])
            {
            }
            column(CustAddr4; CustAddr[4])
            {
            }
            column(CompanyAddr4; CompanyAddr[4])
            {
            }
            column(CustAddr5; CustAddr[5])
            {
            }
            column(CompanyInfoPhoneNo; CompanyInfo."Phone No.")
            {
            }
            column(CustAddr6; CustAddr[6])
            {
            }
            column(CompanyInfoVATRegistrationNo; CompanyInfo."VAT Registration No.")
            {
            }
            column(CompanyInfoGiroNo; CompanyInfo."Giro No.")
            {
            }
            column(CompanyInfoBankName; CompanyInfo."Bank Name")
            {
            }
            column(CompanyInfoBankAccountNo; CompanyInfo."Bank Account No.")
            {
            }
            column(CompanyInfoHomePage; CompanyInfo."Home Page")
            {
            }
            column(CompanyInfoEMail; CompanyInfo."E-Mail")
            {
            }
            column(BilltoCustNo_SalesInvHdr; "Sales Invoice Header"."Bill-to Customer No.")
            {
            }
            column(PostingDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Posting Date", 0, 4))
            {
            }
            column(VATRegNo_SalesInvHdr; "Sales Invoice Header"."VAT Registration No.")
            {
            }
            column(DueDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Due Date"))
            {
            }
            column(No1_SalesInvHdr; "Sales Invoice Header"."No.")
            {
            }
            column(ReferenceText; ReferenceText)
            {
            }
            column(YourRef_SalesInvHdr; "Sales Invoice Header"."Your Reference")
            {
            }
            column(OrderNoText; OrderNoText)
            {
            }
            column(OrderNo_SalesInvHdr; "Sales Invoice Header"."Order No.")
            {
            }
            column(CustAddr7; CustAddr[7])
            {
            }
            column(CustAddr8; CustAddr[8])
            {
            }
            column(CompanyAddr5; CompanyAddr[5])
            {
            }
            column(CompanyAddr6; CompanyAddr[6])
            {
            }
            column(DocDate_SalesInvHdr; FORMAT("Sales Invoice Header"."Document Date", 0, 4))
            {
            }
            column(PricesInclVAT_SalesInvHdr; "Sales Invoice Header"."Prices Including VAT")
            {
            }
            column(SellToAddr1; SellToAddr[1])
            {
            }
            column(SellToAddr2; SellToAddr[2])
            {
            }
            column(SellToAddr3; SellToAddr[3])
            {
            }
            column(SellToAddr4; SellToAddr[4])
            {
            }
            column(SellToAddr5; SellToAddr[5])
            {
            }
            column(SellToAddr6; SellToAddr[6])
            {
            }
            column(SellToAddr7; SellToAddr[7])
            {
            }
            column(SellToAddr8; SellToAddr[8])
            {
            }
            column(SelltoAddrCaptionLbl; SelltoAddrCaptionLbl)
            {
            }
            column(ShipmentMethodDescription; ShipmentMethod.Description)
            {
            }
            column(PhoneNoCaption; PhoneNoCaptionLbl)
            {
            }
            column(VATRegNoCaption; VATRegNoCaptionLbl)
            {
            }
            column(InvoiceNoCaption; InvoiceNoCaptionLbl)
            {
            }
            column(PostingDateCaption; PostingDateCaptionLbl)
            {
            }
            column(BilltoCustNo_SalesInvHdrCaption; 'N° Client')
            {
            }
            column(PricesInclVAT_SalesInvHdrCaption; "Sales Invoice Header".FIELDCAPTION("Prices Including VAT"))
            {
            }
            column(OrderNos; "Sales Invoice Header"."Order No." + ' / ' + "Sales Invoice Header"."External Document No.")
            {
            }
            column(OrderNoLbl; OrderNoLbl)
            {
            }
            column(SelltoAddrCaption; SelltoAddrCaptionLbl)
            {
            }
            column(SellToCustNoCaption; "Sales Invoice Header".FIELDCAPTION("Sell-to Customer No."))
            {
            }
            column(ShippingAgentName; RecTransporteur.Name)
            {
            }
            column(ShippingAgentNameCaption; ShippingAgentNameCaptionLbl)
            {
            }
            column(ShipToAddr8; ShipToAddr[8])
            {
            }
            column(ShipToAddr7; ShipToAddr[7])
            {
            }
            column(ShipToAddr6; ShipToAddr[6])
            {
            }
            column(ShipToAddr5; ShipToAddr[5])
            {
            }
            column(ShipToAddr4; ShipToAddr[4])
            {
            }
            column(ShipToAddr3; ShipToAddr[3])
            {
            }
            column(ShipToAddr2; ShipToAddr[2])
            {
            }
            column(ShipToAddr1; ShipToAddr[1])
            {
            }
            column(ShiptoAddrCaptionLbl; ShiptoAddrCaptionLbl)
            {
            }
            dataitem("Sales Shipment Header"; "Sales Shipment Header")
            {
                column(BlNo; "Sales Shipment Header"."No.")
                {
                }
                dataitem("Packing List Line"; "Packing List Line")
                {
                    DataItemLink = "Posted Source No." = FIELD("No.");
                    DataItemTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.")
                                        ORDER(Ascending)
                                        WHERE("Line Quantity" = FILTER(<> 0));
                    column(No_PackingList; "Packing List Line"."Packing List No.")
                    {
                    }
                    column(NoArticle_PackingList; "Packing List Line"."Item No.")
                    {
                    }
                    column(Designation_PackingList; "Packing List Line".Description)
                    {
                    }
                    column("QtéColis_PackingList"; "Packing List Line"."Line Quantity")
                    {
                    }
                    column(CodeDouanier; gItem."Tariff No.")
                    {
                    }
                    column(CountryCode; gCountry.Code)
                    {
                    }
                    column(CE; gCE)
                    {
                    }
                    column(PoidsUnitaireNet; "Packing List Line"."Unit Weight")
                    {
                    }
                    column(PoidsTotalNet; "Packing List Line"."Line Weight")
                    {
                    }
                    column(Montant; gAmount)
                    {
                    }
                    column(ItemNo; "Packing List Line"."Item No.")
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin
                        gRow += 1;
                        IF (gRow > 1) THEN CLEAR(CompanyInfo);

                        gItem.GET("Packing List Line"."Item No.");
                        CLEAR(gCountry);
                        IF gItem."Country/Region of Origin Code" <> '' THEN
                            IF gCountry.GET(gItem."Country/Region of Origin Code") THEN;

                        IF (gCountry."EU Country/Region Code" = '') THEN
                            gCE := HorsCELbl
                        ELSE
                            gCE := InCELbl;

                        IF gSalesLine.GET(gSalesLine."Document Type"::Order, "Packing List Line"."Source No.", "Packing List Line"."Source Line No.") THEN;

                        //IF gShipmentLine.GET("Posted Whse. Shipment No.","Posted Whse. Shipment Line No.") THEN ;
                        gAmount := gSalesLine.Amount * ("Packing List Line"."Line Quantity" / gSalesLine.Quantity);
                    end;

                    trigger OnPreDataItem();
                    begin
                        //"Nb colis":=0;
                    end;
                }

                trigger OnAfterGetRecord();
                var
                    lCustomer: Record Customer;
                    i: Integer;
                    lAddrTMP: array[8] of Text[50];
                begin
                    // CFR le 05/04/2023 - Régie : Adresse de livraison identique à celle de la facture
                    FormatAddr.SalesShptShipTo(ShipToAddr, "Sales Shipment Header");
                    IF lCustomer.GET("Sales Invoice Header"."Bill-to Customer No.") THEN
                        IF (lCustomer."Inverser Adresses Factures") THEN BEGIN
                            CLEAR(lAddrTMP);
                            FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                                lAddrTMP[i] := ShipToAddr[i];
                            CLEAR(ShipToAddr);
                            FOR i := 1 TO ARRAYLEN(CustAddr) DO
                                ShipToAddr[i] := CustAddr[i];
                            CLEAR(CustAddr);
                            FOR i := 1 TO ARRAYLEN(lAddrTMP) DO
                                CustAddr[i] := lAddrTMP[i];
                        END;
                    // FIN CFR le 05/04/2023
                end;

                trigger OnPreDataItem();
                begin
                    //MESSAGE(gBlNoFilter);
                    "Sales Shipment Header".SETFILTER("No.", gBlNoFilter);
                end;
            }

            trigger OnAfterGetRecord();
            var
                ShipmentInvoiced: Record "Shipment Invoiced";
                lastNo: Text;
                i: Integer;
            begin
                ShipmentInvoiced.SETRANGE("Invoice No.", "Sales Invoice Header"."No.");
                ShipmentInvoiced.FINDSET();
                REPEAT
                    IF lastNo <> ShipmentInvoiced."Shipment No." THEN BEGIN
                        IF gBlNoFilter <> '' THEN gBlNoFilter += '|';
                        gBlNoFilter += ShipmentInvoiced."Shipment No.";
                        lastNo := ShipmentInvoiced."Shipment No.";
                    END;
                UNTIL ShipmentInvoiced.NEXT() = 0;


                DocumentCaption := STRSUBSTNO(Text50001Lbl, "Sales Invoice Header"."No.");
                FormatAddr.SalesInvBillTo(CustAddr, "Sales Invoice Header");
                CurrReport.LANGUAGE := Language_G.GetLanguageID("Language Code");

                IF "Shipment Method Code" = '' THEN
                    ShipmentMethod.INIT()
                ELSE BEGIN
                    IF ShipmentMethod.GET("Shipment Method Code") THEN;
                    ShipmentMethod.TranslateDescription(ShipmentMethod, "Language Code");
                END;
                FormatAddr.SalesInvShipTo(ShipToAddr, ShipToAddr, "Sales Invoice Header");
                ShowShippingAddr := "Sell-to Customer No." <> "Bill-to Customer No.";
                FOR i := 1 TO ARRAYLEN(ShipToAddr) DO
                    IF ShipToAddr[i] <> CustAddr[i] THEN
                        ShowShippingAddr := TRUE;

                IF "Sales Invoice Header"."Shipping Agent Code" = '' THEN
                    RecTransporteur.INIT()
                ELSE
                    RecTransporteur.GET("Sales Invoice Header"."Shipping Agent Code");
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(Display_Details; gDisplayDetails)
                {
                    Caption = 'Afficher détails';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Afficher détails field.';
                }
            }
        }


    }
    trigger OnPreReport();
    begin
        CompanyInfo.CALCFIELDS(Picture);
        CompanyInfo.CALCFIELDS("Picture Address");
        GestionEdition.GetInfoPiedPage(TRUE, textSIEGSOC);
    end;

    var

        gItem: Record Item;
        gCountry: Record "Country/Region";
        gSalesLine: Record "Sales Line";
        ShipmentMethod: Record "Shipment Method";
        CompanyInfo: Record "Company Information";
        // Cust: Record Customer;
        RecTransporteur: Record "Shipping Agent";
        Language_G: Codeunit Language;
        FormatAddr: Codeunit "Format Address";
        GestionEdition: Codeunit "Gestion Info Editions";
        gCE: Text;
        gBlNoFilter: Text;
        CodeDouanierLbl: Label 'Customs nomenclature';
        PaysOrigineLbl: Label 'Native country';
        TotalQteLbl: Label 'Total quantity';
        TotalPoidsNetLbl: Label 'Total net weight';
        TotalMontantLbl: Label 'Total amount';
        HorsCELbl: Label 'Outside UE';
        InCELbl: Label 'UE';
        TotalLbl: Label 'TOTAL';
        TotauxLbl: Label 'TOTAUX';
        CustAddr: array[8] of Text[50];
        ShipToAddr: array[8] of Text[50];
        CompanyAddr: array[8] of Text[50];
        SellToAddr: array[8] of Text[50];
        textSIEGSOC: array[5] of Text[250];
        // PageCaptionCap: Label 'Page %1 of %2';
        PhoneNoCaptionLbl: Label 'Phone No.';
        VATRegNoCaptionLbl: Label 'VAT Registration No.';
        InvoiceNoCaptionLbl: Label 'Invoice No.';
        ShiptoAddrCaptionLbl: Label 'Ship-to Address';
        // BillToAddrCaptionLbl: Label 'Adresse de facturation';
        ShpMethodDescCaptionLbl: Label 'Shipment Method';
        HomePageCaptionCapLbl: Label 'Home Page';
        EMailCaptionLbl: Label 'E-Mail';
        DocDateCaptionLbl: Label 'Document Date';
        gRow: Integer;
        ShowShippingAddr: Boolean;
        gDisplayDetails: Boolean;
        DocumentCaption: Text;
        //VATNoText: Text[80];
        ReferenceText: Text[80];
        OrderNoText: Text;
        PostingDateCaptionLbl: Label 'Posting Date';
        OrderNoLbl: Label 'Commande';
        SelltoAddrCaptionLbl: Label 'Commandé par';
        ShippingAgentNameCaptionLbl: Label 'Transporteur';
        Text50001Lbl: Label 'Facture N° %1', Comment = '%1 = Nu fac';
        TitleDocLbl: Label 'Custom nomenclature';
        gAmount: Decimal;
}

