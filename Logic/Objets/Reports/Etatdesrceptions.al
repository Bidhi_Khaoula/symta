report 50047 "Etat des réceptions"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Etat des réceptions.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Etat des réceptions';

    dataset
    {
        dataitem(dti_Fusion; "Warehouse Receipt Header")
        {
            DataItemTableView = SORTING("No.")
                                WHERE(Fusion = CONST(false),
                                      "N° Fusion Réception" = FILTER(<> ''));
            RequestFilterFields = "No.";
            column(dti_fusion_no; "No.")
            {
            }
            column(dti_fusion_vendor_name; rec_Vendor.Name)
            {
            }
            dataitem(dti_SousFusion; "Warehouse Receipt Header")
            {
                DataItemLink = "N° Fusion Réception" = FIELD("N° Fusion Réception");
                DataItemTableView = SORTING("No.");
                column(dti_sousfusion_no; "No.")
                {
                }

                trigger OnAfterGetRecord();
                var
                    rec_WhseReceiptLine: Record "Warehouse Receipt Line";
                    "Pré-reception": Record "Saisis Réception Magasin";
                    LPurchline: Record "Purchase Line";
                    dec_QtePointe: Decimal;
                begin
                    // On récupère chaque article de chaque réception de la fusion principale
                    // Et on addition pour chaque article les quantités à réceptionner.
                    rec_WhseReceiptLine.RESET();
                    rec_WhseReceiptLine.SETRANGE("No.", dti_SousFusion."No.");

                    IF rec_WhseReceiptLine.FINDSET() THEN
                        REPEAT
                            // Initialisation de la table gérant les statistiques.
                            TempItemStat.RESET();
                            TempItemStat.SETRANGE("Item No.", rec_WhseReceiptLine."Item No.");
                            // MC Le 03-07-2013 => Il est possible qu'un même article soit sur plusieurs réceptions, mais ne fonctionne pas avec pré récepti
                            //TempItemStat.SETRANGE(Texte, rec_WhseReceiptLine."No.");
                            // FIN MC Le 03-07-2013
                            IF TempItemStat.FINDFIRST() THEN
                                // On utilise le champ [Amount] pour stocker les valeurs des réceptions.
                                TempItemStat.RENAME(TempItemStat.Amount + rec_WhseReceiptLine."Qty. to Receive (Base)",
                                        TempItemStat."Amount 2", TempItemStat."Item No.")

                            ELSE BEGIN
                                TempItemStat.INIT();
                                TempItemStat.VALIDATE("Item No.", rec_WhseReceiptLine."Item No.");
                                TempItemStat.VALIDATE(TempItemStat.Amount, rec_WhseReceiptLine."Qty. to Receive (Base)");
                                // On va chercher les quantités en pré-réceptions.
                                dec_QtePointe := 0;
                                "Pré-reception".RESET();
                                "Pré-reception".SETRANGE("No.", dti_Fusion."No.");
                                "Pré-reception".SETRANGE("Item No.", rec_WhseReceiptLine."Item No.");
                                IF "Pré-reception".FINDSET() THEN
                                    REPEAT
                                        dec_QtePointe += "Pré-reception".Quantity;
                                    UNTIL "Pré-reception".NEXT() = 0;
                                // Met à jour les montants
                                TempItemStat.VALIDATE(TempItemStat."Amount 2", dec_QtePointe);

                                // CFR le 25/10/2023 - Régie : Ajout du n° de ligne RM
                                //TempItemStat.Texte := rec_WhseReceiptLine."No.";
                                Evaluate(TempItemStat.Texte, rec_WhseReceiptLine."No." + ' / ' + FORMAT(rec_WhseReceiptLine."Line No." / 10000));
                                // FIN CFR le 25/10/2023

                                // AD Le 06-02-2015
                                IF (rec_WhseReceiptLine."Source Type" = 39) AND (rec_WhseReceiptLine."Source Subtype" = 1) THEN
                                    IF LPurchline.GET(rec_WhseReceiptLine."Source Subtype", rec_WhseReceiptLine."Source No.",
                                           rec_WhseReceiptLine."Source Line No.") THEN
                                        TempItemStat."Text 2" := LPurchline."item Reference No.";

                                // FIN AD Le 06-02-2015
                                // AD Le 01-04-2015
                                IF rec_WhseReceiptLine."Vendor Shipment No." <> '' THEN
                                    TempItemStat."Text 3" := rec_WhseReceiptLine."Vendor Shipment No."
                                ELSE
                                    TempItemStat."Text 3" := dti_SousFusion."Vendor Shipment No.";
                                // FIN AD Le 01-04-2015
                                TempItemStat.Insert();
                            END;
                        UNTIL rec_WhseReceiptLine.NEXT() = 0;
                end;

                trigger OnPreDataItem();
                begin
                    // Vide la table temporaire permettant de stocker les statistiques par articles.
                    TempItemStat.DELETEALL();
                end;
            }
            dataitem(dti_Comparaison; Integer)
            {
                DataItemTableView = SORTING(Number);
                column(dti_comparaison_itemstat; TempItemStat.Texte)
                {
                }
                column(dti_comparaison_item_no2; rec_Item."No. 2")
                {
                }
                column(dti_comparaison_item_amount; TempItemStat.Amount)
                {
                }
                column(dti_comparaison_item_amount2; TempItemStat."Amount 2")
                {
                }
                column(dti_comparaison_item_description; rec_Item.Description)
                {
                }
                column(dti_comparaison_itemstat2; TempItemStat."Text 2")
                {
                }
                column(dti_comparaison_itemstat3; TempItemStat."Text 3")
                {
                }
                dataitem(dti_PreReception; Integer)
                {
                    DataItemTableView = SORTING(Number);
                    column(dti_prereception_bincode; rec_PrePointage."Bin Code")
                    {
                    }
                    column(dti_prereception_quantity; rec_PrePointage.Quantity)
                    {
                    }
                    column(dti_prereception_showline; FORMAT(rec_PrePointage.Quantity))
                    {
                    }
                    column(dti_prereception_user_modif; rec_PrePointage."Utilisateur Modif")
                    {
                    }
                    column(dti_prereception_Comment; rec_PrePointage.Commentaire)
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin

                        IF Number = 1 THEN BEGIN
                            IF NOT rec_PrePointage.ISEMPTY THEN
                                rec_PrePointage.FINDFIRST();
                        END
                        ELSE
                            rec_PrePointage.NEXT();

                        // MCO Le 08-09-2017
                        IF rec_PrePointage.Commentaire <> '' THEN BEGIN
                            j += 1;
                            tab_Comment[j] := rec_PrePointage.Commentaire;
                        END;
                    end;

                    trigger OnPreDataItem();
                    begin

                        // Récupère le nombre de pointages à imprimer.
                        // On récupère tous les articles pour lesquels il y a des réceptions et qui n'ont pas le même nombre de pré pointages.
                        rec_PrePointage.RESET();
                        rec_PrePointage.SETRANGE("No.", dti_Fusion."No.");
                        rec_PrePointage.SETRANGE("Item No.", TempItemStat."Item No.");

                        // On pose le filtre sur Integer pour boucler sur les pré-réceptions.
                        SETRANGE(Number, 1, rec_PrePointage.COUNT);
                    end;
                }

                trigger OnAfterGetRecord();
                begin

                    IF Number = 1 THEN BEGIN
                        IF NOT TempItemStat.ISEMPTY THEN BEGIN
                            TempItemStat.FINDFIRST();
                            rec_Item.GET(TempItemStat."Item No.");
                        END;
                    END
                    ELSE BEGIN
                        TempItemStat.NEXT();
                        rec_Item.GET(TempItemStat."Item No.");
                    END;


                    IF TempItemStat.Amount = TempItemStat."Amount 2" THEN
                        CurrReport.SKIP();
                end;

                trigger OnPreDataItem();
                begin

                    // Récupère le nombre de pointages à imprimer.
                    // On récupère tous les articles pour lesquels il y a des réceptions et qui n'ont pas le même nombre de pré pointages.
                    TempItemStat.RESET();
                    //TempItemStat.SETFILTER(Amount,'<>%1&<>%2', -1, -1 * TempItemStat."Amount 2"); => AD LE 02-12 PB AVEC CETTE LIGNE

                    // On pose le filtre sur Integer pour boucler sur ces articles.
                    SETRANGE(Number, 1, TempItemStat.COUNT);
                end;
            }
            dataitem(dti_HorsReception; Integer)
            {
                DataItemTableView = SORTING(Number);
                column(dti_horsreception_refactive; RefActive.RechercheRefActive(TempPrePointage."Item No."))
                {
                }
                column(dti_horsreception_bincode; TempPrePointage."Bin Code")
                {
                }
                column(dti_horsreception_quantity; TempPrePointage.Quantity)
                {
                }
                column(dti_horsreception_user_modif; TempPrePointage."Utilisateur Modif")
                {
                }
                column(dti_horsreception_comment; TempPrePointage.Commentaire)
                {
                }
                column(dti_horsreception_description; rec_Item.Description)
                {
                }
                column(dti_horsreception_refFourn; refFourn)
                {
                }

                trigger OnAfterGetRecord();
                var
                    recL_ItemVendor: Record "Item Vendor";
                begin

                    IF Number = 1 THEN BEGIN
                        IF NOT TempPrePointage.ISEMPTY THEN
                            TempPrePointage.FINDFIRST();
                    END
                    ELSE
                        TempPrePointage.NEXT();

                    CLEAR(rec_Item);
                    IF rec_Item.GET(TempPrePointage."Item No.") THEN;
                    // MCO Le 24-03-2016
                    refFourn := '';
                    recL_ItemVendor.RESET();
                    recL_ItemVendor.SETRANGE("Vendor No.", rec_Vendor."No.");
                    recL_ItemVendor.SETRANGE("Item No.", rec_Item."No.");
                    IF recL_ItemVendor.FINDFIRST() THEN
                        refFourn := recL_ItemVendor."Vendor Item No.";
                    // FIN MCO Le 24-03-2016
                end;

                trigger OnPreDataItem();
                begin

                    // Suppression des enregistrements de la table temporaire.
                    TempPrePointage.DELETEALL();
                    // Récupère tous les pointages hors-réception.
                    rec_PrePointage.RESET();
                    rec_PrePointage.SETRANGE("No.", dti_Fusion."No.");
                    IF rec_PrePointage.FINDSET() THEN
                        REPEAT
                            // On va voir si le pointage correspond ou non à une réception.
                            TempItemStat.RESET();
                            TempItemStat.SETRANGE("Item No.", rec_PrePointage."Item No.");
                            // Si c'est hors réception alors on l'insère dans la table temporaire.
                            IF TempItemStat.ISEMPTY THEN BEGIN
                                TempPrePointage := rec_PrePointage;
                                TempPrePointage.Insert();
                            END;
                        UNTIL rec_PrePointage.NEXT() = 0;

                    // On pose le filtre sur Integer pour boucler sur les pré-réceptions.
                    SETRANGE(Number, 1, TempPrePointage.COUNT);
                end;
            }
            dataitem(dti_LigneQualiteSousFUsion; "Warehouse Receipt Line")
            {
                DataItemLink = "No." = FIELD("No.");
                DataItemLinkReference = dti_SousFusion;
                DataItemTableView = SORTING("No.", "Line No.");
                column(dti_lqsf_item_no2; rec_Item."No. 2")
                {
                }
                column(dti_lqsf_item_description; rec_Item.Description)
                {
                }
                column(dti_lqsf_coderemise; rec_ItemVendor."Code Remise")
                {
                }
                column(dti_lqsf_qualite; FORMAT(rec_ItemVendor."Statut Qualité"))
                {
                }
                column(dti_lqsf_quantite; "Qty. to Receive")
                {
                }
                column(dti_lsqf_bl_nos; "Vendor Shipment No.")
                {
                }
                column(dti_lsqf_no_recept; "No.")
                {
                }

                trigger OnAfterGetRecord();
                var
                    rec_PurchLine: Record "Purchase Line";
                    bln_Skip: Boolean;
                begin
                    // On récupère la ligne d'achat correspondant à la réception pour avoir le fournisseur.
                    IF dti_Fusion."N° Fusion Réception" = dti_LigneQualiteSousFUsion."No." THEN
                        CurrReport.SKIP();

                    bln_Skip := TRUE;
                    IF rec_PurchLine.GET("Source Subtype", "Source No.", "Source Line No.") THEN
                        IF rec_ItemVendor.GET(rec_PurchLine."Buy-from Vendor No.", "Item No.", "Variant Code") THEN
                            CASE rec_ItemVendor."Statut Qualité" OF
                                rec_ItemVendor."Statut Qualité"::"Non Conforme",
                              rec_ItemVendor."Statut Qualité"::"A contrôler",
                              rec_ItemVendor."Statut Qualité"::"Contrôle en cours":
                                    bln_Skip := FALSE;
                            END;

                    IF bln_Skip THEN
                        CurrReport.SKIP()
                    ELSE BEGIN
                        rec_Item.GET("Item No.");
                        IsFound := TRUE;
                    END;
                end;

                trigger OnPostDataItem();
                begin
                    IF IsFound THEN
                        QualiteHeaderPrinted := TRUE;
                end;

                trigger OnPreDataItem();
                begin
                    rec_Item.RESET();
                end;
            }
            dataitem(dti_LigneQualiteFusion; "Warehouse Receipt Line")
            {
                DataItemLink = "No." = FIELD("No.");
                column(dti_lqf_item_no2; rec_Item."No. 2")
                {
                }
                column(dti_lqf_item_description; rec_Item.Description)
                {
                }
                column(dti_lqf_coderemise; rec_ItemVendor."Code Remise")
                {
                }
                column(dti_lqf_qualite; FORMAT(rec_ItemVendor."Statut Qualité"))
                {
                }
                column(dti_lqf_quantite; "Qty. to Receive")
                {
                }
                column(dti_lqf_bl_no; "Vendor Shipment No.")
                {
                }
                column(dti_lqf_no_recept; "No.")
                {
                }

                trigger OnAfterGetRecord();
                var
                    rec_PurchLine: Record "Purchase Line";
                    bln_Skip: Boolean;
                begin

                    // On récupère la ligne d'achat correspondant à la réception pour avoir le fournisseur.
                    bln_Skip := TRUE;
                    IF rec_PurchLine.GET("Source Subtype", "Source No.", "Source Line No.") THEN
                        IF rec_ItemVendor.GET(rec_PurchLine."Buy-from Vendor No.", "Item No.", "Variant Code") THEN
                            CASE rec_ItemVendor."Statut Qualité" OF
                                rec_ItemVendor."Statut Qualité"::"Non Conforme",
                              rec_ItemVendor."Statut Qualité"::"A contrôler",
                              rec_ItemVendor."Statut Qualité"::"Contrôle en cours":
                                    bln_Skip := FALSE;
                            END;

                    IF bln_Skip THEN
                        CurrReport.SKIP()
                    ELSE
                        rec_Item.GET("Item No.");
                end;
            }
            dataitem("Warehouse Comment Line"; "Warehouse Comment Line")
            {
                DataItemLink = "No." = FIELD("No.");
                column(wc_line_item_no2; rec_ItemComment."No. 2")
                {
                }
                column(wc_line_comment; "Warehouse Comment Line".Comment)
                {
                }
            }
            dataitem("Warehouse Receipt Header"; "Warehouse Receipt Header")
            {
                DataItemLink = "N° Fusion Réception" = FIELD("N° Fusion Réception");
                DataItemTableView = SORTING("No.")
                                    ORDER(Ascending);
                column(wr_header_comment; 'Commentaires')
                {
                }
                dataitem("Warehouse Receipt Line"; "Warehouse Receipt Line")
                {
                    DataItemLink = "No." = FIELD("No.");
                    dataitem(Integer; Integer)
                    {
                        column(wr_line_no; "Warehouse Receipt Line"."No.")
                        {
                        }
                        column(wr_line_item_no2; rec_Item."No. 2")
                        {
                        }
                        column(pc_line_comment; TempCommentLine.Comment)
                        {
                        }
                        column(pc_Line_Code; TempCommentLine.Code)
                        {
                        }

                        trigger OnAfterGetRecord();
                        begin
                            IF Number = 1 THEN
                                TempCommentLine.FINDFIRST()
                            ELSE
                                TempCommentLine.NEXT();

                            // MCO Le 08-09-2017
                            IF TempCommentLine.Code = 'PRC' THEN
                                FOR i := 1 TO j DO
                                    IF TempCommentLine.Comment = tab_Comment[i] THEN CurrReport.SKIP();
                        end;

                        trigger OnPreDataItem();
                        begin
                            // MCO Le 24-03-2016
                            SETRANGE(Number, 1, TempCommentLine.COUNT);
                        end;
                    }

                    trigger OnAfterGetRecord();
                    var
                        cduL_GestionSYMTA: Codeunit "Gestion SYMTA";
                    begin
                        rec_Item.GET("Item No.");

                        // MCO Le 24-03-2016
                        TempCommentLine.RESET();
                        IF TempCommentLine.COUNT < 250 THEN // MCO Le 24-03-2016 => Verrou par peur (j'ai pas les reins).
                            TempCommentLine.DELETEALL();
                        cduL_GestionSYMTA.GetCommentaireReception(TempCommentLine, "Warehouse Receipt Line"."Item No.", "Warehouse Receipt Line"."N° Fusion Réception", FALSE);
                        // FIN MCO Le 24-03-2016
                    end;
                }
            }
            dataitem("Vendor Comment Line"; "Comment Line")
            {
                DataItemTableView = SORTING("Table Name", "No.", "Line No.")
                                    ORDER(Ascending)
                                    WHERE("Table Name" = CONST(Vendor),
                                          "Display Purch. Receipt" = CONST(true));
                column(VendorCommentLine; Comment)
                {
                }

                trigger OnPreDataItem();
                begin
                    "Vendor Comment Line".SETRANGE("No.", rec_Vendor."No.");
                end;
            }

            trigger OnAfterGetRecord();
            begin
                // Récupère le fournisseur.
                IF rec_Vendor.GET(dti_Fusion.GetVendorNo()) THEN;
            end;
        }
    }

    var
        TempItemStat: Record "Item Amount" temporary;
        rec_PrePointage: Record "Saisis Réception Magasin";
        TempPrePointage: Record "Saisis Réception Magasin" temporary;
        rec_ItemComment: Record Item;
        rec_ItemVendor: Record "Item Vendor";
        TempCommentLine: Record "Comment Line" temporary;
        rec_Item: Record Item;
        rec_Vendor: Record Vendor;
        RefActive: Codeunit "Gestion Multi-référence";
        QualiteHeaderPrinted: Boolean;
        IsFound: Boolean;
        refFourn: Code[50];
        tab_Comment: array[1000] of Text[80];
        i: Integer;
        j: Integer;
}

