report 50000 "Recalcul Mini-Maxi"
{
    Caption = 'Recalcul Mini-Maxi';
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Recalcul Mini-Maxi.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem(Item; Item)
        {

            trigger OnAfterGetRecord();
            var
                "1DernierMois": Integer;
                "3DernierMois": Decimal;
                "12DernierMois": Decimal;
                "3ProchainMois": Decimal;
                 DateDebut: Date;
                DateFin: Date;
               
            begin

                // ---------------------------------
                // CALCUL DES CONSO DU MOIS EN COURS
                // ---------------------------------
                DateDebut := DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3)); // Début du mois en cours
                DateFin := CALCDATE('<+FM>', WORKDATE());

                Item.SETRANGE("Date Filter", DateDebut, DateFin);
                Item.CALCFIELDS("Sales (Qty.)", Item."Hors Norme Sales (Qty.)");

                //MC Le 26-05-2010 =>Analyse complémentaire Etats d'alertes
                Item."Ancien Conso. Normative" := Item."Conso. Normative";
                //FIN MC

                "1DernierMois" := Item."Sales (Qty.)" - Item."Hors Norme Sales (Qty.)";

                // ------------------------------------
                // CALCUL DES CONSO DES 3 DERNIERS MOIS
                // ------------------------------------
                DateDebut := CALCDATE('<-2M>', DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3))); // Début du mois en cours - 2mois
                DateFin := CALCDATE('<+FM>', WORKDATE()); // Fin du mois en cours

                Item.SETRANGE("Date Filter", DateDebut, DateFin);
                Item.CALCFIELDS("Sales (Qty.)", Item."Hors Norme Sales (Qty.)");

                "3DernierMois" := (Item."Sales (Qty.)" - Item."Hors Norme Sales (Qty.)") / 3;

                // ------------------------------------
                // CALCUL DES CONSO DES 12 DERNIERS MOIS
                // ------------------------------------
                DateDebut := CALCDATE('<-11M>', DMY2DATE(1, DATE2DMY(WORKDATE(), 2), DATE2DMY(WORKDATE(), 3))); // Début du mois en cours - 2mois
                DateFin := CALCDATE('<+FM>', WORKDATE()); // Fin du mois en cours

                Item.SETRANGE("Date Filter", DateDebut, DateFin);
                Item.CALCFIELDS("Sales (Qty.)", Item."Hors Norme Sales (Qty.)");

                "12DernierMois" := (Item."Sales (Qty.)" - Item."Hors Norme Sales (Qty.)") / 12;

                // ------------------------------------------------------------------
                // CALCUL DES CONSO DES 3 PROCHAIN MOIS (Basé sur l'année précédente)
                // ------------------------------------------------------------------
                DateDebut := CALCDATE('<-1A+FM+1J>', WORKDATE()); // Le premier jour du mois suivant de l'année précédente
                DateFin := CALCDATE('<+FM+2M>', DateDebut); // 3 mois après le debut

                Item.SETRANGE("Date Filter", DateDebut, DateFin);
                Item.CALCFIELDS("Sales (Qty.)", Item."Hors Norme Sales (Qty.)");

                "3ProchainMois" := (Item."Sales (Qty.)" - Item."Hors Norme Sales (Qty.)") / 3;


                // ----------------------------------------------------------
                // APPLICATION DE LA FORMULE POUR CALCULER LA CONSO NORMATIVE
                // ----------------------------------------------------------
                CASE Item."Formule Stock Normatif" OF
                    Item."Formule Stock Normatif"::"S-(12DernierMois + 3ProchainMois + 3ProchainMois) / 3":
                        Item."Conso. Normative" := ("12DernierMois" + "3ProchainMois" + "3ProchainMois") / 3;
                    Item."Formule Stock Normatif"::"1-Non géré":
                        ERROR('Non géré');
                    Item."Formule Stock Normatif"::"2-Non géré":
                        ERROR('Non géré');
                    Item."Formule Stock Normatif"::"3-Non géré":
                        ERROR('Non géré');
                    Item."Formule Stock Normatif"::"4-(12DernierMois + 3DernierMois) / 2":
                        Item."Conso. Normative" := ("12DernierMois" + "3DernierMois") / 2;
                    Item."Formule Stock Normatif"::"5-3DernierMois":
                        Item."Conso. Normative" := ("3DernierMois");
                    Item."Formule Stock Normatif"::"6-DernierMois":
                        Item."Conso. Normative" := ("1DernierMois");
                END;
                //MESSAGE(' (%1 + %2) / 2 * %3 / 30', "12DernierMois" , "3DernierMois" , nbJourDeStock);
                // ---------------------------------
                // APPLICATION DU COEF DE REFFACTION
                // ---------------------------------
                IF Item."Coef. multiplicateur" <> 0 THEN
                    Item."Conso. Normative" := Item."Conso. Normative" * Item."Coef. multiplicateur";

                // ----------------------------------
                // ARRONDI DE LA VALEUR SUR UN ENTIER
                // ----------------------------------
                Item."Conso. Normative" := ROUND(Item."Conso. Normative", 1);
                IF Item."Conso. Normative" < 0 THEN
                    Item."Conso. Normative" := 0;

                // ----------------------------
                // ENREGISTREMENT DANS LA TABLE
                // ----------------------------
                //Calcul de la variation
                //MC Le 27-05-2010 => Garde un petit historique de la variation
                IF Item."Ancien Conso. Normative" <> 0 THEN
                    Item."Variation Conso. Normative (%)" := ROUND(((Item."Conso. Normative" - Item."Ancien Conso. Normative") /
                    Item."Ancien Conso. Normative" * 100), 0.01);
                //FIN MC
                Item."Date Conso. Normative" := WORKDATE();
                Item.MODIFY();
            end;

            trigger OnPreDataItem();
            begin
                Item.SETRANGE("Sans Mouvements de Stock", FALSE);
            end;
        }
    }

    labels
    {
    }

    trigger OnPostReport();
    begin

        //MC Le 27-05-2010 => Lancement automatique du report récapitulatif (demande de P.Bertrand)
        //ItemReport.SETRANGE("Date Conso. Normative",WORKDATE);
        //:IF ItemReport.FINDSET() THEN
        //  REPORT.RUN(REPORT::"REMBOURSEMENT CLIENT(VIREMENT)",TRUE,TRUE,ItemReport);
        //FIN MC
    end;

    
}

