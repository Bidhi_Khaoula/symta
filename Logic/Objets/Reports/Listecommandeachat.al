report 50162 "Liste commande achat"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Liste commande achat.rdlc';
    Caption = 'Liste commande achat';
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    dataset
    {
        dataitem("Purchase Header"; "Purchase Header")
        {
            RequestFilterFields = "Document Type";
            column(CompanyName; COMPANYNAME)
            {
            }
            column(No; "No.")
            {
            }
            column(BuyfromVendorNo; "Buy-from Vendor No.")
            {
            }
            column(PaytoName; "Pay-to Name")
            {
            }
        }
    }
}

