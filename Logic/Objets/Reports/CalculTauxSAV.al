report 50022 "Calcul Taux SAV"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Calcul Taux SAV.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    Caption = 'Calcul Taux SAV';

    dataset
    {
        dataitem(Customer; Customer)
        {
            RequestFilterFields = "No.";

            trigger OnAfterGetRecord();
            var
                "Value Entry": Record "Value Entry";
                TempSalesPrice: Record "Sales Price" temporary;
                TempSalesLineDisc: Record "Sales Line Discount" temporary;
                HistoSav: Record "Historisation Taux SAV / Porte";
                SalesLine: Record "Sales Line";
                LCodeunitsFunctions: Codeunit "Codeunits Functions";
                Mt: Decimal;
                Remise: Decimal;
            begin

                NoCustomer += 1;
                window.UPDATE(1, Customer."No.");
                window.UPDATE(2, ROUND(NoCustomer * 10000 / NbCustomer, 1));

                // SI PAS DE VENTE SUR L'ANNEE EN COURS ON PASSE AU SUIVANT
                // --------------------------------------------------------
                //Customer.SETRANGE("Date Filter", DateDeb , DateFin);
                //Customer.CALCFIELDS("Sales (LCY)");
                //IF Customer."Sales (LCY)" = 0 THEN
                //  CurrReport.SKIP;

                // CALCUL DES VENTES
                // -----------------
                "Value Entry".RESET();
                "Value Entry".SETCURRENTKEY("Item No.", "Posting Date", "Item Ledger Entry Type", "Entry Type", "Variance Type",
                                 "Source No.", "External Document No.", "Document Code", "Sales Line type");
                "Value Entry".SETRANGE("Posting Date", DateDeb, DateFin);
                "Value Entry".SETRANGE("Source No.", Customer."No.");
                "Value Entry".SETRANGE("Item Ledger Entry Type", "Value Entry"."Item Ledger Entry Type"::Sale);


                VentesNormales := 0;
                VentesSav := 0;
                IF "Value Entry".FindSet() THEN
                    REPEAT

                        Mt := "Value Entry"."Sales Amount (Actual)";

                        // CALCUL DES VENTES SAV + REPERATIONS + ECHANGES
                        IF "Value Entry"."Sales Line type" IN ["Value Entry"."Sales Line type"::Réparation, "Value Entry"."Sales Line type"::Echange,
                                                               "Value Entry"."Sales Line type"::SAV] THEN BEGIN
                            // SI MONTANT, ON VALORISE AU PRIX TARIF EN COURS
                            IF Mt = 0 THEN BEGIN

                                // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
                                Customer.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
                                Customer.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
                                Customer.CALCFIELDS("Centrale Active");
                                // FIN CFR le 10/03/2021 - SFD20210201 historique centrale active

                                Item.GET("Value Entry"."Item No.");
                                Remise := LCodeunitsFunctions.GetSalesDisc(
                                          TempSalesLineDisc, Customer."No.", '', Customer."Customer Disc. Group",
                                          // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs ventes.
                                          //'', Item."No.", '', Item."Base Unit of Measure", '', WORKDATE, FALSE, "Value Entry"."Invoiced Quantity",0);
                                          '', Item."No.", '', Item."Base Unit of Measure", '', WORKDATE(), FALSE, "Value Entry"."Invoiced Quantity", 0,
                                          Customer."Sous Groupe Tarifs", Customer."Centrale Active", 0);
                                // FIN MC Le 19-04-2011 => SYMTA -> Gestion des tarifs ventes.

                                IF NOT LCodeunitsFunctions.GetUnitPrice(
                                          // MC Le 19-04-2011 => SYMTA -> Gestion des tarifs ventes.
                                          TempSalesPrice, Customer."No.", '', Customer."Customer Price Group",
                                          '', Item."No.", '', Item."Base Unit of Measure", '', WORKDATE(), FALSE,
                                          //ABS("Value Entry"."Invoiced Quantity"), Remise, Mt)
                                          ABS("Value Entry"."Invoiced Quantity"), Remise, Mt, Customer."Sous Groupe Tarifs", Customer."Centrale Active", 0)
                                // FIN MC Le 19-04-2011
                                THEN
                                    Mt := Item."Unit Price"
                                ELSE
                                    Mt := TempSalesPrice."Unit Price";

                                Mt := Mt * "Value Entry"."Invoiced Quantity";
                            END;

                            VentesSav += ABS(Mt);
                        END;

                        // CALCUL DES NORMALES
                        IF "Value Entry"."Sales Line type" = "Value Entry"."Sales Line type"::Normal THEN
                            VentesNormales += Mt;

                    UNTIL "Value Entry".NEXT() = 0;

                // On ajoute les devis SAV au VentesSAV
                SalesLine.RESET();
                SalesLine.SETRANGE("Document Type", SalesLine."Document Type"::Quote);
                SalesLine.SETRANGE(Type, SalesLine.Type::Item);
                SalesLine.SETFILTER("Line type", '%1|%2|%3',
                   SalesLine."Line type"::Réparation, SalesLine."Line type"::Echange, SalesLine."Line type"::SAV);
                SalesLine.SETRANGE("Sell-to Customer No.", Customer."No.");
                IF SalesLine.FindSet() THEN
                    REPEAT
                        VentesSav += ABS(SalesLine.Quantity * SalesLine."Net Unit Price");
                    UNTIL SalesLine.NEXT() = 0;



                IF VentesNormales <> 0 THEN BEGIN
                    Customer."Taux S.A.V." := (VentesSav / VentesNormales) * 100;
                    Customer."Date Taux S.A.V." := WORKDATE();
                    Customer.MODIFY();

                    // Historisation
                    HistoSav.INIT();
                    HistoSav.VALIDATE("Type Ligne", HistoSav."Type Ligne"::"SAV Client");
                    HistoSav.VALIDATE("No.", Customer."No.");
                    HistoSav.VALIDATE("Taux Sav", Customer."Taux S.A.V.");
                    HistoSav.VALIDATE(Montant, VentesSav);
                    HistoSav.VALIDATE("Montant Base", VentesNormales);
                    HistoSav.INSERT(TRUE);

                END;
            end;

            trigger OnPostDataItem();
            begin
                window.UPDATE(1, 'FIN');
            end;

            trigger OnPreDataItem();
            begin

                NbCustomer := Customer.COUNT;
                NoCustomer := 0;
            end;
        }
        dataitem(Item; Item)
        {
            RequestFilterFields = "No.";

            trigger OnAfterGetRecord();
            var
                HistoSav: Record "Historisation Taux SAV / Porte";
            begin

                NoArticle += 1;
                window.UPDATE(3, Item."No.");
                window.UPDATE(4, ROUND(NoArticle * 10000 / NbArticle, 1));


                TauxSAVArticle(Item."No.", DateDeb, DateFin, VentesNormales, VentesSav);


                IF VentesNormales <> 0 THEN BEGIN
                    Item."Taux S.A.V." := (VentesSav / VentesNormales) * 100;
                    Item."Date Taux S.A.V." := WORKDATE();
                    Item.MODIFY();

                    // Historisation
                    HistoSav.INIT();
                    HistoSav.VALIDATE("Type Ligne", HistoSav."Type Ligne"::"SAV Article");
                    HistoSav.VALIDATE("No.", Item."No.");
                    HistoSav.VALIDATE("Taux Sav", Item."Taux S.A.V.");
                    HistoSav.VALIDATE(Qte, VentesSav);
                    HistoSav.VALIDATE("Qte Base", VentesNormales);
                    HistoSav.INSERT(TRUE);

                END;
            end;

            trigger OnPostDataItem();
            begin
                window.UPDATE(3, 'FIN');
            end;

            trigger OnPreDataItem();
            begin

                NbArticle := Item.COUNT;
                NoArticle := 0;
            end;
        }
        dataitem(Vendor; Vendor)
        {
            RequestFilterFields = "No.";

            trigger OnAfterGetRecord();
            var
                RecArticle: Record Item;
                HistoSav: Record "Historisation Taux SAV / Porte";
                TauxArt: Decimal;
                NbTauxArt: Integer;
            begin

                NoFournisseur += 1;
                window.UPDATE(5, Vendor."No.");
                window.UPDATE(6, ROUND(NoFournisseur * 10000 / NbFournisseur, 1));

                VentesSav := 0;
                VentesNormales := 0;

                TauxArt := 0;
                NbTauxArt := 0;

                RecArticle.RESET();
                RecArticle.SETRANGE("Vendor No.", Vendor."No.");
                IF RecArticle.FindSet() THEN
                    REPEAT
                        TauxSAVArticle(RecArticle."No.", DateDeb, DateFin, VentesNormales, VentesSav);
                        IF VentesNormales <> 0 THEN BEGIN
                            // AD le 01-02-2010 => Changement de méthode demandé par Pierre
                            /*
                            TauxArt += (VentesSav / VentesNormales) * 100;
                            NbTauxArt += 1;
                            MESSAGE('%1   %2',TauxArt, NbTauxArt);
                           */
                            TauxArt += ((VentesSav / VentesNormales) * 100) * (VentesSav + VentesNormales); // Taux SAV * Qte vendus
                            NbTauxArt += (VentesSav + VentesNormales);
                        END;

                    UNTIL RecArticle.NEXT() = 0;


                IF NbTauxArt <> 0 THEN BEGIN
                    Vendor."Taux S.A.V." := TauxArt / NbTauxArt;
                    Vendor."Date Taux S.A.V." := WORKDATE();
                    Vendor.MODIFY();

                    // Historisation
                    HistoSav.INIT();
                    HistoSav.VALIDATE("Type Ligne", HistoSav."Type Ligne"::"SAV Fournisseur");
                    HistoSav.VALIDATE("No.", Vendor."No.");
                    HistoSav.VALIDATE("Taux Sav", Vendor."Taux S.A.V.");
                    HistoSav.VALIDATE(Qte, TauxArt);
                    HistoSav.VALIDATE("Qte Base", NbTauxArt);
                    HistoSav.INSERT(TRUE);

                END;

            end;

            trigger OnPostDataItem();
            begin
                window.UPDATE(5, 'FIN');
            end;

            trigger OnPreDataItem();
            begin

                NbFournisseur := Vendor.COUNT;
                NoFournisseur := 0;
            end;
        }
    }


    trigger OnPostReport();
    begin

        window.CLOSE();
        MESSAGE('%1', TIME);
    end;

    trigger OnPreReport();
    begin

        DateDeb := CALCDATE('<-1A>', WORKDATE());
        DateFin := WORKDATE();


        CLEAR(window);
        window.OPEN(Text001Lbl);
    end;

    var
        window: Dialog;
        NbCustomer: Integer;
        NoCustomer: Integer;
        NbArticle: Integer;
        NoArticle: Integer;
        NbFournisseur: Integer;
        NoFournisseur: Integer;
        DateDeb: Date;
        DateFin: Date;
        VentesNormales: Decimal;
        VentesSav: Decimal;
        Text001Lbl: Label 'Client  :  #1########## \ @2@@@@@@@@@@@@@@ \Article :  #3########## \  @4@@@@@@@@@@@@@@  \Fournisseur :  #5########## \  @6@@@@@@@@@@@@@@', Comment = '#1 @2 #3 @4 #5 @6';

    procedure TauxSAVArticle(PNoArticle: Code[20]; PDateDeb: Date; PDateFin: Date; var PVentesNormales: Decimal; var PVentesSAV: Decimal);
    var
        "Value Entry": Record "Value Entry";
        SalesLine: Record "Sales Line";
    begin
        // CALCUL DES VENTES
        // -----------------
        "Value Entry".RESET();
        "Value Entry".SETCURRENTKEY("Item No.", "Posting Date", "Item Ledger Entry Type", "Entry Type", "Variance Type",
                         "Source No.", "External Document No.", "Document Code", "Sales Line type");
        "Value Entry".SETRANGE("Posting Date", PDateDeb, PDateFin);
        "Value Entry".SETRANGE("Item No.", PNoArticle);
        "Value Entry".SETRANGE("Item Ledger Entry Type", "Value Entry"."Item Ledger Entry Type"::Sale);


        PVentesNormales := 0;
        PVentesSAV := 0;
        IF "Value Entry".FindSet() THEN
            REPEAT

                // CALCUL DES VENTES SAV + REPERATIONS + ECHANGES
                IF "Value Entry"."Sales Line type" IN ["Value Entry"."Sales Line type"::Réparation, "Value Entry"."Sales Line type"::Echange,
                                                       "Value Entry"."Sales Line type"::SAV] THEN
                    PVentesSAV += ABS("Value Entry"."Invoiced Quantity");


                // CALCUL DES NORMALES
                IF "Value Entry"."Sales Line type" = "Value Entry"."Sales Line type"::Normal THEN
                    PVentesNormales += ("Value Entry"."Invoiced Quantity" * -1);

            UNTIL "Value Entry".NEXT() = 0;


        // On ajoute les devis SAV au VentesSAV
        SalesLine.RESET();
        SalesLine.SETRANGE("Document Type", SalesLine."Document Type"::Quote);
        SalesLine.SETRANGE(Type, SalesLine.Type::Item);
        SalesLine.SETRANGE("No.", PNoArticle);
        SalesLine.SETFILTER("Line type", '%1|%2|%3',
           SalesLine."Line type"::Réparation, SalesLine."Line type"::Echange, SalesLine."Line type"::SAV);
        IF SalesLine.FindSet() THEN
            REPEAT
                PVentesSAV += ABS(SalesLine.Quantity);
            UNTIL SalesLine.NEXT() = 0;
    end;
}

