report 50001 "Suggest Customer Payments SYM"//10864
{
    Caption = 'Suggest Customer Payments';
    Permissions = TableData "Cust. Ledger Entry" = rm;
    ProcessingOnly = true;
    ApplicationArea = all;
    UsageCategory = None;
    dataset
    {
        dataitem(Customer; Customer)
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "No.", "Payment Method Code";

            trigger OnAfterGetRecord()
            begin
                Window.UPDATE(1, "No.");
                GetCustLedgEntries(TRUE, FALSE);
                GetCustLedgEntries(FALSE, FALSE);
                CheckAmounts(FALSE);
            end;

            trigger OnPostDataItem()
            begin
                IF UsePaymentDisc THEN BEGIN
                    RESET();
                    COPYFILTERS(Cust2);
                    Window.OPEN(Text007Lbl);
                    IF FIND('-') THEN
                        REPEAT
                            Window.UPDATE(1, "No.");
                            TempPayableCustLedgEntry.SETRANGE("Vendor No.", "No.");
                            GetCustLedgEntries(TRUE, TRUE);
                            GetCustLedgEntries(FALSE, TRUE);
                            CheckAmounts(TRUE);
                        UNTIL NEXT() = 0;
                END;

                GenPayLine.LOCKTABLE();
                GenPayLine.SETRANGE("No.", GenPayLine."No.");
                IF GenPayLine.FINDLAST() THEN BEGIN
                    LastLineNo := GenPayLine."Line No.";
                    GenPayLine.INIT();
                END;

                Window.OPEN(Text008Lbl);

                TempPayableCustLedgEntry.RESET();
                TempPayableCustLedgEntry.SETRANGE(Priority, 1, 2147483647);
                MakeGenPayLines();
                TempPayableCustLedgEntry.RESET();
                TempPayableCustLedgEntry.SETRANGE(Priority, 0);
                MakeGenPayLines();
                TempPayableCustLedgEntry.RESET();
                TempPayableCustLedgEntry.DELETEALL();
                Window.CLOSE();

                IF GenPayLineInserted AND (Customer.GETFILTER("Partner Type") <> '') THEN BEGIN
                    GenPayHead."Partner Type" := Customer."Partner Type";
                    GenPayHead.MODIFY();
                END;
                ShowMessage(MessageText);
            end;

            trigger OnPreDataItem()
            begin

                //GR 07-10-15 Reprise MIG2015
                IF "Type date" = "Type date"::"Date d'echéance" THEN BEGIN
                    IF LastDueDateToPayReq = 0D THEN
                        ERROR(Text000Lbl);
                END
                ELSE
                    IF ("Date comptabilité début" = 0D) OR ("Date comptabilité fin" = 0D) THEN
                        ERROR('Veuillez entrer des dates de fin et de début');

                //IF LastDueDateToPayReq = 0D THEN
                //  ERROR(Text000);
                IF PostingDate = 0D THEN
                    ERROR(Text001Lbl);

                GenPayLineInserted := FALSE;
                MessageText := '';

                IF UsePaymentDisc AND (LastDueDateToPayReq < WORKDATE()) THEN
                    IF NOT
                       CONFIRM(
                         Text003Lbl +
                         Text004Qst, FALSE,
                         WORKDATE())
                    THEN
                        ERROR(Text005Lbl);

                Cust2.COPYFILTERS(Customer);
                Window.OPEN(Text006Lbl);

                NextEntryNo := 1;
            end;
        }
    }

    requestpage
    {
        SaveValues = false;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(Use_PaymentDisc1; UsePaymentDisc)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Find Payment Discounts';
                        MultiLine = true;
                        ToolTip = 'Specifies the value of the Find Payment Discounts field.';
                    }
                    field(Summarize_PerName; SummarizePer)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Summarize per';
                        OptionCaption = ' ,Customer,Due date';
                        ToolTip = 'Specifies the value of the Summarize per field.';
                    }
                    field(Currency_FilterName; CurrencyFilter)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Currency Filter';
                        Editable = false;
                        TableRelation = Currency;
                        ToolTip = 'Specifies the value of the Currency Filter field.';
                    }
                    field(Type_Date; "Type date")
                    {
                        Caption = 'Type Date';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Type Date field.';
                    }
                    field(First_DueDateToPayReqName; FirstDueDateToPayReq)
                    {
                        Caption = 'Premiere date échéance';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Premiere date échéance field.';
                    }
                    field(Last_PaymentDate; LastDueDateToPayReq)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Last Payment Date';
                        ToolTip = 'Specifies the value of the Last Payment Date field.';
                    }
                    field("Date comptabilité début Name"; "Date comptabilité début")
                    {
                        Caption = 'Date comptabilité début';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date comptabilité début field.';
                    }
                    field("Date comptabilité fin Name"; "Date comptabilité fin")
                    {
                        Caption = 'Date comptabilité fin';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date comptabilité fin field.';
                    }
                    field(UsePaymentDiscName; UsePaymentDisc)
                    {
                        Caption = 'Rechercher les escomptes';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Rechercher les escomptes field.';
                    }
                }
            }
        }

    }



    var
        Cust2: Record Customer;
        GenPayHead: Record "Payment Header";
        GenPayLine: Record "Payment Line";
        CustLedgEntry: Record "Cust. Ledger Entry";
        TempPayableCustLedgEntry: Record "Payable Vendor Ledger Entry" temporary;
        TempPaymentPostBuffer: Record "Payment Post. Buffer" temporary;
        TempOldPaymentPostBuffer: Record "Payment Post. Buffer" temporary;
        PaymentClass: Record "Payment Class";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        Text000Lbl: Label 'Please enter the last payment date.';
        Text001Lbl: Label 'Please enter the posting date.';
        Text003Lbl: Label 'The selected last due date is earlier than %1.\\', Comment = '%1';
        Text004Qst: Label 'Do you still want to run the batch job?';
        Text005Lbl: Label 'The batch job was interrupted.';
        Text006Lbl: Label 'Processing customers     #1##########', Comment = '#1';
        Text007Lbl: Label 'Processing customers for payment discounts #1##########', Comment = '#1';
        Text008Lbl: Label 'Inserting payment journal lines #1##########', Comment = '#1';
        Text016Lbl: Label ' is already applied to %1 %2 for customer %3.', Comment = '%1=Document Type ,%2=Document No. ,%3 = Customer No.';

        Window: Dialog;
        UsePaymentDisc: Boolean;
        PostingDate: Date;
        LastDueDateToPayReq: Date;
        NextDocNo: Code[20];
        SummarizePer: Option " ",Customer,"Due date";
        LastLineNo: Integer;
        NextEntryNo: Integer;
        MessageText: Text[250];
        GenPayLineInserted: Boolean;
        CurrencyFilter: Code[10];
        "Date comptabilité début": Date;
        "Date comptabilité fin": Date;
        "Type date": Option "Date d'echéance","Date comptabilité";
        FirstDueDateToPayReq: Date;

    procedure SetGenPayLine(NewGenPayLine: Record "Payment Header")
    begin
        GenPayHead := NewGenPayLine;
        GenPayLine."No." := NewGenPayLine."No.";
        PaymentClass.GET(GenPayHead."Payment Class");
        PostingDate := GenPayHead."Posting Date";
        CurrencyFilter := GenPayHead."Currency Code";
    end;

    procedure GetCustLedgEntries(Positive: Boolean; Future: Boolean)
    begin
        CustLedgEntry.RESET();
        CustLedgEntry.SETCURRENTKEY("Customer No.", Open, Positive, "Due Date");
        CustLedgEntry.SETRANGE("Customer No.", Customer."No.");
        CustLedgEntry.SETRANGE(Open, TRUE);
        CustLedgEntry.SETRANGE(Positive, Positive);
        CustLedgEntry.SETRANGE("Currency Code", CurrencyFilter);
        CustLedgEntry.SETRANGE("Applies-to ID", '');

        // GR MIG 2015 Reprise
        IF "Type date" = "Type date"::"Date d'echéance" THEN BEGIN
            IF Future THEN BEGIN
                CustLedgEntry.SETRANGE("Due Date", LastDueDateToPayReq + 1, 99991231D);
                CustLedgEntry.SETRANGE("Pmt. Discount Date", PostingDate, LastDueDateToPayReq);
                CustLedgEntry.SETFILTER("Original Pmt. Disc. Possible", '<0');
            END ELSE
                CustLedgEntry.SETRANGE("Due Date", 0D, LastDueDateToPayReq);
        END
        ELSE BEGIN
            CustLedgEntry.SETCURRENTKEY("Customer No.", Open, "Posting Date");
            CustLedgEntry.SETRANGE("Posting Date", "Date comptabilité début", "Date comptabilité fin");
        END;
        // fin modif

        CustLedgEntry.SETRANGE("On Hold", '');
        IF CustLedgEntry.FIND('-') THEN
            REPEAT
                SaveAmount();
            UNTIL CustLedgEntry.NEXT() = 0;
    end;

    local procedure SaveAmount()
    begin
        WITH GenPayLine DO BEGIN
            "Account Type" := "Account Type"::Customer;
            VALIDATE("Account No.", CustLedgEntry."Customer No.");
            "Posting Date" := CustLedgEntry."Posting Date";
            "Currency Factor" := CustLedgEntry."Adjusted Currency Factor";
            IF "Currency Factor" = 0 THEN
                "Currency Factor" := 1;
            VALIDATE("Currency Code", CustLedgEntry."Currency Code");
            CustLedgEntry.CALCFIELDS("Remaining Amount");
            IF ((CustLedgEntry."Document Type" = CustLedgEntry."Document Type"::"Credit Memo") AND
                (CustLedgEntry."Remaining Pmt. Disc. Possible" <> 0) OR
                (CustLedgEntry."Document Type" = CustLedgEntry."Document Type"::Invoice)) AND
               (PostingDate <= CustLedgEntry."Pmt. Discount Date") AND UsePaymentDisc
            THEN
                Amount := -(CustLedgEntry."Remaining Amount" - CustLedgEntry."Original Pmt. Disc. Possible")
            ELSE
                Amount := -CustLedgEntry."Remaining Amount";
            VALIDATE(Amount);
        END;

        TempPayableCustLedgEntry."Vendor No." := CustLedgEntry."Customer No.";
        TempPayableCustLedgEntry."Entry No." := NextEntryNo;
        TempPayableCustLedgEntry."Vendor Ledg. Entry No." := CustLedgEntry."Entry No.";
        TempPayableCustLedgEntry.Amount := GenPayLine.Amount;
        TempPayableCustLedgEntry."Amount (LCY)" := GenPayLine."Amount (LCY)";
        TempPayableCustLedgEntry.Positive := (TempPayableCustLedgEntry.Amount > 0);
        TempPayableCustLedgEntry.Future := (CustLedgEntry."Due Date" > LastDueDateToPayReq);
        TempPayableCustLedgEntry."Currency Code" := CustLedgEntry."Currency Code";
        TempPayableCustLedgEntry."Due Date" := CustLedgEntry."Due Date";
        TempPayableCustLedgEntry.Insert();
        NextEntryNo := NextEntryNo + 1;
    end;

    procedure CheckAmounts(Future: Boolean)
    var
        CurrencyBalance: Decimal;
        PrevCurrency: Code[10];
    begin
        TempPayableCustLedgEntry.SETRANGE("Vendor No.", Customer."No.");
        TempPayableCustLedgEntry.SETRANGE(Future, Future);
        IF TempPayableCustLedgEntry.FIND('-') THEN BEGIN
            PrevCurrency := TempPayableCustLedgEntry."Currency Code";
            REPEAT
                IF TempPayableCustLedgEntry."Currency Code" <> PrevCurrency THEN BEGIN
                    IF CurrencyBalance < 0 THEN BEGIN
                        TempPayableCustLedgEntry.SETRANGE("Currency Code", PrevCurrency);
                        TempPayableCustLedgEntry.DELETEALL();
                        TempPayableCustLedgEntry.SETRANGE("Currency Code");
                    END;
                    CurrencyBalance := 0;
                    PrevCurrency := TempPayableCustLedgEntry."Currency Code";
                END;
                CurrencyBalance := CurrencyBalance + TempPayableCustLedgEntry."Amount (LCY)"
            UNTIL TempPayableCustLedgEntry.NEXT() = 0;
            IF CurrencyBalance > 0 THEN BEGIN
                TempPayableCustLedgEntry.SETRANGE("Currency Code", PrevCurrency);
                TempPayableCustLedgEntry.DELETEALL();
                TempPayableCustLedgEntry.SETRANGE("Currency Code");
            END;
        END;
        TempPayableCustLedgEntry.RESET();
    end;

    local procedure InsertTempPaymentPostBuffer(var PTempPaymentPostBuffer: Record "Payment Post. Buffer" temporary; var PCustLedgEntry: Record "Cust. Ledger Entry")
    begin
        PTempPaymentPostBuffer."Applies-to Doc. Type" := PCustLedgEntry."Document Type";
        PTempPaymentPostBuffer."Applies-to Doc. No." := PCustLedgEntry."Document No.";
        PTempPaymentPostBuffer."Currency Factor" := PCustLedgEntry."Adjusted Currency Factor";
        PTempPaymentPostBuffer.Amount := PCustLedgEntry.Amount;
        PTempPaymentPostBuffer."Amount (LCY)" := PCustLedgEntry."Amount (LCY)";
        PTempPaymentPostBuffer."Global Dimension 1 Code" := PCustLedgEntry."Global Dimension 1 Code";
        PTempPaymentPostBuffer."Global Dimension 2 Code" := PCustLedgEntry."Global Dimension 2 Code";
        PTempPaymentPostBuffer."Auxiliary Entry No." := PCustLedgEntry."Entry No.";
        PTempPaymentPostBuffer.Insert();
    end;

    local procedure MakeGenPayLines()
    var
        SEPADirectDebitMandate: Record "SEPA Direct Debit Mandate";
    begin
        TempPaymentPostBuffer.DELETEALL();

        IF TempPayableCustLedgEntry.FIND('-') THEN
            REPEAT
                TempPayableCustLedgEntry.SETRANGE("Vendor No.", TempPayableCustLedgEntry."Vendor No.");
                TempPayableCustLedgEntry.FIND('-');
                REPEAT
                    CustLedgEntry.GET(TempPayableCustLedgEntry."Vendor Ledg. Entry No.");
                    TempPaymentPostBuffer."Account No." := CustLedgEntry."Customer No.";
                    TempPaymentPostBuffer."Currency Code" := CustLedgEntry."Currency Code";
                    IF SummarizePer = SummarizePer::"Due date" THEN
                        TempPaymentPostBuffer."Due Date" := CustLedgEntry."Due Date";

                    TempPaymentPostBuffer."Dimension Entry No." := 0;
                    TempPaymentPostBuffer."Global Dimension 1 Code" := '';
                    TempPaymentPostBuffer."Global Dimension 2 Code" := '';

                    IF SummarizePer IN [SummarizePer::Customer, SummarizePer::"Due date"] THEN BEGIN
                        TempPaymentPostBuffer."Auxiliary Entry No." := 0;
                        IF TempPaymentPostBuffer.FIND() THEN BEGIN
                            TempPaymentPostBuffer.Amount := TempPaymentPostBuffer.Amount + TempPayableCustLedgEntry.Amount;
                            TempPaymentPostBuffer."Amount (LCY)" := TempPaymentPostBuffer."Amount (LCY)" + TempPayableCustLedgEntry."Amount (LCY)";
                            TempPaymentPostBuffer.MODIFY();
                        END ELSE BEGIN
                            LastLineNo := LastLineNo + 10000;
                            TempPaymentPostBuffer."Payment Line No." := LastLineNo;
                            IF PaymentClass."Line No. Series" = '' THEN
                                Evaluate(NextDocNo, GenPayHead."No." + '/' + FORMAT(LastLineNo))
                            ELSE
                                NextDocNo := NoSeriesMgt.GetNextNo(PaymentClass."Line No. Series", PostingDate, FALSE);
                            TempPaymentPostBuffer."Document No." := NextDocNo;
                            NextDocNo := INCSTR(NextDocNo);
                            TempPaymentPostBuffer.Amount := TempPayableCustLedgEntry.Amount;
                            TempPaymentPostBuffer."Amount (LCY)" := TempPayableCustLedgEntry."Amount (LCY)";
                            Window.UPDATE(1, CustLedgEntry."Customer No.");
                            TempPaymentPostBuffer.Insert();
                        END;
                        CustLedgEntry."Applies-to ID" := TempPaymentPostBuffer."Document No.";
                        CODEUNIT.RUN(CODEUNIT::"Cust. Entry-Edit", CustLedgEntry)
                    END ELSE BEGIN
                        IsNotApplied(CustLedgEntry);
                        InsertTempPaymentPostBuffer(TempPaymentPostBuffer, CustLedgEntry);
                        Window.UPDATE(1, CustLedgEntry."Customer No.");
                    END;
                    CustLedgEntry.CALCFIELDS("Remaining Amount");
                    CustLedgEntry."Amount to Apply" := CustLedgEntry."Remaining Amount";
                    CODEUNIT.RUN(CODEUNIT::"Cust. Entry-Edit", CustLedgEntry)
                UNTIL TempPayableCustLedgEntry.NEXT() = 0;
                TempPayableCustLedgEntry.SETFILTER("Vendor No.", '>%1', TempPayableCustLedgEntry."Vendor No.");
            UNTIL NOT TempPayableCustLedgEntry.FIND('-');

        CLEAR(TempOldPaymentPostBuffer);
        TempPaymentPostBuffer.SETCURRENTKEY("Document No.");
        IF TempPaymentPostBuffer.FINDSET() THEN
            REPEAT
                WITH GenPayLine DO BEGIN
                    INIT();
                    Window.UPDATE(1, TempPaymentPostBuffer."Account No.");
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        LastLineNo := LastLineNo + 10000;
                        "Line No." := LastLineNo;
                        IF PaymentClass."Line No. Series" = '' THEN
                            Evaluate(NextDocNo, GenPayHead."No." + '/' + FORMAT("Line No."))
                        ELSE
                            NextDocNo := NoSeriesMgt.GetNextNo(PaymentClass."Line No. Series", PostingDate, FALSE);
                    END ELSE BEGIN
                        "Line No." := TempPaymentPostBuffer."Payment Line No.";
                        NextDocNo := TempPaymentPostBuffer."Document No.";
                    END;
                    "Document No." := NextDocNo;
                    "Applies-to ID" := "Document No.";
                    TempOldPaymentPostBuffer := TempPaymentPostBuffer;
                    TempOldPaymentPostBuffer."Document No." := "Document No.";
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        CustLedgEntry.GET(TempPaymentPostBuffer."Auxiliary Entry No.");
                        CustLedgEntry."Applies-to ID" := NextDocNo;
                        CustLedgEntry.MODIFY();
                    END;
                    "Account Type" := "Account Type"::Customer;
                    VALIDATE("Account No.", TempPaymentPostBuffer."Account No.");
                    "Currency Code" := TempPaymentPostBuffer."Currency Code";
                    Amount := TempPaymentPostBuffer.Amount;
                    IF Amount > 0 THEN
                        "Debit Amount" := Amount
                    ELSE
                        "Credit Amount" := -Amount;
                    "Amount (LCY)" := TempPaymentPostBuffer."Amount (LCY)";
                    "Currency Factor" := TempPaymentPostBuffer."Currency Factor";
                    IF ("Currency Factor" = 0) AND (Amount <> 0) THEN
                        "Currency Factor" := Amount / "Amount (LCY)";
                    Cust2.GET("Account No.");
                    VALIDATE("Bank Account Code", Cust2."Preferred Bank Account Code");
                    "Payment Class" := GenPayHead."Payment Class";
                    VALIDATE("Status No.");
                    "Posting Date" := PostingDate;
                    IF SummarizePer = SummarizePer::" " THEN BEGIN
                        "Applies-to Doc. Type" := CustLedgEntry."Document Type";
                        "Applies-to Doc. No." := CustLedgEntry."Document No.";
                        "Dimension Set ID" := CustLedgEntry."Dimension Set ID";
                        IF SEPADirectDebitMandate.GET(CustLedgEntry."Direct Debit Mandate ID") THEN
                            VALIDATE("Bank Account Code", SEPADirectDebitMandate."Customer Bank Account Code");
                        "Direct Debit Mandate ID" := CustLedgEntry."Direct Debit Mandate ID";
                    END;
                    CASE SummarizePer OF
                        SummarizePer::" ":
                            "Due Date" := CustLedgEntry."Due Date";
                        SummarizePer::Customer:
                            BEGIN
                                TempPayableCustLedgEntry.SETCURRENTKEY("Vendor No.", "Due Date");
                                TempPayableCustLedgEntry.SETRANGE("Vendor No.", TempPaymentPostBuffer."Account No.");
                                TempPayableCustLedgEntry.FIND('+');
                                "Due Date" := TempPayableCustLedgEntry."Due Date";
                                TempPayableCustLedgEntry.DELETEALL();
                            END;
                        SummarizePer::"Due date":
                            "Due Date" := TempPaymentPostBuffer."Due Date";
                    END;
                    IF Amount <> 0 THEN BEGIN
                        IF "Dimension Set ID" = 0 THEN
                            DimensionSetup(); // per "Vendor", per "Due Date"
                        INSERT();
                    END;
                    GenPayLineInserted := TRUE;
                END;
            UNTIL TempPaymentPostBuffer.NEXT() = 0;
    end;

    local procedure ShowMessage(Text: Text)
    begin
        IF (Text <> '') AND GenPayLineInserted THEN
            MESSAGE(Text);
    end;

    local procedure IsNotApplied(CustLedgerEntry: Record "Cust. Ledger Entry")
    var
        GenJournalLine: Record "Gen. Journal Line";
    begin
        WITH GenJournalLine DO BEGIN
            RESET();
            SETCURRENTKEY(
              "Account Type", "Account No.", "Applies-to Doc. Type", "Applies-to Doc. No.");
            SETRANGE("Account Type", "Account Type"::Customer);
            SETRANGE("Account No.", CustLedgerEntry."Customer No.");
            SETRANGE("Applies-to Doc. Type", CustLedgerEntry."Document Type");
            SETRANGE("Applies-to Doc. No.", CustLedgerEntry."Document No.");
            IF FINDFIRST() THEN
                FIELDERROR(
                  "Applies-to Doc. No.",
                  STRSUBSTNO(
                    Text016Lbl,
                    CustLedgerEntry."Document Type", CustLedgerEntry."Document No.",
                    CustLedgerEntry."Customer No."));
        END;
    end;
}

