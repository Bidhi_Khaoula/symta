report 50021 "Customer - Order Detail SYM" //108
{
    DefaultLayout = RDLC;
    RDLCLayout = './Objets/Reports/Layouts/Customer - Order Detail.rdlc';
    ApplicationArea = All;
    Caption = 'Customer - Order Detail';
    PreviewMode = PrintLayout;
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem(Customer; Customer)
        {
            PrintOnlyIfDetail = true;
            RequestFilterFields = "No.", "Search Name", Priority;
            column(ShipmentPeriodDate; STRSUBSTNO(Text000Lbl, PeriodText))
            {
            }
            column(CompanyName; COMPANYNAME)
            {
            }
            column(PrintAmountsInLCY; PrintAmountsInLCY)
            {
            }
            column(CustTableCapCustFilter; TABLECAPTION + ': ' + CustFilter)
            {
            }
            column(CustFilter; CustFilter)
            {
            }
            column(SalesOrderLineFilter; STRSUBSTNO(Text001Lbl, SalesLineFilter))
            {
            }
            column(SalesLineFilter; SalesLineFilter)
            {
            }
            column(No_Customer; "No.")
            {
                IncludeCaption = true;
            }
            column(Name_Customer; Name)
            {
            }
            column(PageGroupNo; PageGroupNo)
            {
            }
            column(CustOrderDetailCaption; CustOrderDetailCaptionLbl)
            {
            }
            column(PageCaption; PageCaptionLbl)
            {
            }
            column(AllAmtAreInLCYCaption; AllAmtAreInLCYCaptionLbl)
            {
            }
            column(ShipmentDateCaption; ShipmentDateCaptionLbl)
            {
            }
            column(QtyOnBackOrderCaption; QtyOnBackOrderCaptionLbl)
            {
            }
            column(OutstandingOrdersCaption; OutstandingOrdersCaptionLbl)
            {
            }
            dataitem("Sales Line"; "Sales Line")
            {
                DataItemLink = "Bill-to Customer No." = FIELD("No."),
                               "Shortcut Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                               "Shortcut Dimension 2 Code" = FIELD("Global Dimension 2 Filter");
                DataItemTableView = SORTING("Document Type", "Bill-to Customer No.", "Currency Code")
                                    WHERE("Document Type" = CONST(Order),
                                          "Outstanding Quantity" = FILTER(<> 0));
                RequestFilterFields = "Shipment Date";
                RequestFilterHeading = 'Sales Order Line';
                column(SalesHeaderNo; SalesHeader."No.")
                {
                }
                column(SalesHeaderOrderDate; SalesHeader."Order Date")
                {
                }
                column(Description_SalesLine; Description)
                {
                    IncludeCaption = true;
                }
                column(No_SalesLine; "No.")
                {
                    IncludeCaption = true;
                }
                column(ReferenceActive_SalesLine; ReferenceActive)
                {
                }
                column(Type_SalesLine; Type)
                {
                    IncludeCaption = true;
                }
                column(ShipmentDate_SalesLine; FORMAT("Shipment Date"))
                {
                }
                column(Quantity_SalesLine; Quantity)
                {
                    IncludeCaption = true;
                }
                column(OutStandingQty_SalesLine; "Outstanding Quantity")
                {
                    IncludeCaption = true;
                }
                column(BackOrderQty; BackOrderQty)
                {
                    DecimalPlaces = 0 : 5;
                }
                column(UnitPrice_SalesLine; "Unit Price")
                {
                    AutoFormatExpression = "Currency Code";
                    AutoFormatType = 2;
                    IncludeCaption = true;
                }
                column(LineDiscAmt_SalesLine; "Line Discount Amount")
                {
                    IncludeCaption = true;
                }
                column(InvDiscAmt_SalesLine; "Inv. Discount Amount")
                {
                    AutoFormatExpression = "Currency Code";
                    AutoFormatType = 2;
                    IncludeCaption = true;
                }
                column(SalesOrderAmount; SalesOrderAmount)
                {
                    AutoFormatExpression = "Currency Code";
                    AutoFormatType = 1;
                }
                column(SalesHeaderCurrCode; SalesHeader."Currency Code")
                {
                }

                trigger OnAfterGetRecord()
                begin
                    NewOrder := "Document No." <> SalesHeader."No.";
                    IF NewOrder THEN
                        SalesHeader.GET(1, "Document No.");
                    IF "Shipment Date" <= WORKDATE() THEN
                        BackOrderQty := "Outstanding Quantity"
                    ELSE
                        BackOrderQty := 0;
                    Currency.InitRoundingPrecision();
                    IF "VAT Calculation Type" IN ["VAT Calculation Type"::"Normal VAT", "VAT Calculation Type"::"Reverse Charge VAT"] THEN
                        SalesOrderAmount :=
                          ROUND(
                            (Amount + "VAT Base Amount" * "VAT %" / 100) * "Outstanding Quantity" / Quantity / (1 + "VAT %" / 100),
                            Currency."Amount Rounding Precision")
                    ELSE
                        SalesOrderAmount :=
                          ROUND(
                            "Outstanding Amount" / (1 + "VAT %" / 100),
                            Currency."Amount Rounding Precision");
                    SalesOrderAmountLCY := SalesOrderAmount;
                    IF SalesHeader."Currency Code" <> '' THEN BEGIN
                        IF SalesHeader."Currency Factor" <> 0 THEN
                            SalesOrderAmountLCY :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  WORKDATE(), SalesHeader."Currency Code",
                                  SalesOrderAmountLCY, SalesHeader."Currency Factor"));
                        IF PrintAmountsInLCY THEN BEGIN
                            "Unit Price" :=
                              ROUND(
                                CurrExchRate.ExchangeAmtFCYToLCY(
                                  WORKDATE(), SalesHeader."Currency Code",
                                  "Unit Price", SalesHeader."Currency Factor"));
                            SalesOrderAmount := SalesOrderAmountLCY;
                        END;
                    END;
                    IF SalesHeader."Prices Including VAT" THEN BEGIN
                        "Unit Price" := "Unit Price" / (1 + "VAT %" / 100);
                        "Inv. Discount Amount" := "Inv. Discount Amount" / (1 + "VAT %" / 100);
                    END;
                    "Inv. Discount Amount" := "Inv. Discount Amount" * "Outstanding Quantity" / Quantity;
                    CurrencyCode2 := SalesHeader."Currency Code";
                    IF PrintAmountsInLCY THEN
                        CurrencyCode2 := '';
                    TempCurrencyTotalBuffer.UpdateTotal(
                      CurrencyCode2,
                      SalesOrderAmount,
                      Counter1,
                      Counter1);

                    IF PrintToExcel THEN
                        MakeExcelDataBody();


                    ReferenceActive := GestionMulti.RechercheRefActive("Sales Line"."No.");
                end;
            }
            dataitem(Integer; Integer)
            {
                DataItemTableView = SORTING(Number)
                                    WHERE(Number = FILTER(1 ..));
                column(TotalAmt_CurrTotalBuff; TempCurrencyTotalBuffer."Total Amount")
                {
                    AutoFormatExpression = TempCurrencyTotalBuffer."Currency Code";
                    AutoFormatType = 1;
                }
                column(CurrCode_CurrTotalBuff; TempCurrencyTotalBuffer."Currency Code")
                {
                }

                trigger OnAfterGetRecord()
                begin
                    IF Number = 1 THEN
                        OK := TempCurrencyTotalBuffer.FIND('-')
                    ELSE
                        OK := TempCurrencyTotalBuffer.NEXT() <> 0;
                    IF NOT OK THEN
                        CurrReport.BREAK();

                    TempCurrencyTotalBuffer2.UpdateTotal(
                      TempCurrencyTotalBuffer."Currency Code",
                      TempCurrencyTotalBuffer."Total Amount",
                      Counter1,
                      Counter1);
                end;

                trigger OnPostDataItem()
                begin
                    TempCurrencyTotalBuffer.DELETEALL();
                end;
            }

            trigger OnAfterGetRecord()
            begin
                IF PrintOnlyOnePerPage THEN
                    PageGroupNo := PageGroupNo + 1;
            end;

            trigger OnPreDataItem()
            begin
                PageGroupNo := 1;
            end;
        }
        dataitem(Integer2; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = FILTER(1 ..));
            column(TotalAmt_CurrTotalBuff2; TempCurrencyTotalBuffer2."Total Amount")
            {
                AutoFormatExpression = TempCurrencyTotalBuffer2."Currency Code";
                AutoFormatType = 1;
            }
            column(CurrCode_CurrTotalBuff2; TempCurrencyTotalBuffer2."Currency Code")
            {
            }
            column(TotalCaption; TotalCaptionLbl)
            {
            }

            trigger OnAfterGetRecord()
            begin
                IF Number = 1 THEN
                    OK := TempCurrencyTotalBuffer2.FIND('-')
                ELSE
                    OK := TempCurrencyTotalBuffer2.NEXT() <> 0;
                IF NOT OK THEN
                    CurrReport.BREAK();
            end;

            trigger OnPostDataItem()
            begin
                TempCurrencyTotalBuffer2.DELETEALL();
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(ShowAmountsInLCY; PrintAmountsInLCY)
                    {
                        Caption = 'Show Amounts in LCY';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Show Amounts in LCY field.';
                    }
                    field(NewPagePerCustomer; PrintOnlyOnePerPage)
                    {
                        Caption = 'New Page per Customer';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the New Page per Customer field.';
                    }
                    field(PrintToExcelName; PrintToExcel)
                    {
                        Caption = 'Print to Excel';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Print to Excel field.';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnOpenPage()
        begin
            PrintToExcel := FALSE;
        end;
    }

    labels
    {
        OrderNoCaption = 'Order No.';
    }

    trigger OnPostReport()
    begin
        IF PrintToExcel THEN
            CreateExcelbook();
    end;

    trigger OnPreReport()
    begin
        CustFilter := Customer.GETFILTERS;
        SalesLineFilter := "Sales Line".GETFILTERS;
        Evaluate(PeriodText, "Sales Line".GETFILTER("Shipment Date"));

        IF PrintToExcel THEN
            MakeExcelInfo();
    end;

    var
        CurrExchRate: Record "Currency Exchange Rate";
        TempCurrencyTotalBuffer: Record "Currency Total Buffer" temporary;
        TempCurrencyTotalBuffer2: Record "Currency Total Buffer" temporary;
        SalesHeader: Record "Sales Header";
        TempExcelBuf: Record "Excel Buffer" temporary;
        Currency: Record Currency;
        GestionMulti: Codeunit "Gestion Multi-référence";
        Text000Lbl: Label 'Shipment Date: %1', Comment = ' %1=Shipment Date';
        Text001Lbl: Label 'Sales Order Line: %1', Comment = '%1=Sales Line Filter';
        CustFilter: Text;
        SalesLineFilter: Text;
        SalesOrderAmount: Decimal;
        SalesOrderAmountLCY: Decimal;
        PrintAmountsInLCY: Boolean;
        PeriodText: Text[30];
        PrintOnlyOnePerPage: Boolean;
        BackOrderQty: Decimal;
        NewOrder: Boolean;
        OK: Boolean;
        Counter1: Integer;
        CurrencyCode2: Code[10];
        PrintToExcel: Boolean;
        Text002Lbl: Label 'Data';
        Text003Lbl: Label 'Customer - Order Detail';
        Text004Lbl: Label 'Company Name';
        Text005Lbl: Label 'Report No.';
        Text006Lbl: Label 'Report Name';
        Text007Lbl: Label 'User ID';
        Text008Lbl: Label 'Date';
        Text009Lbl: Label 'Customer Filters';
        Text010Lbl: Label 'Sales Order Lines Filters';
        Text011Lbl: Label 'Quantity on Back Order';
        Text012Lbl: Label 'Outstanding Orders';
        Text013Lbl: Label 'All amounts are in LCY';
        Text014Lbl: Label ' ,G/L Account,Item,Resource,Fixed Asset,Charge (Item)';
        Text015Lbl: Label 'Item';
        Text016Lbl: Label 'Order';
        PageGroupNo: Integer;
        CustOrderDetailCaptionLbl: Label 'Customer - Order Detail';
        PageCaptionLbl: Label 'Page';
        AllAmtAreInLCYCaptionLbl: Label 'All amounts are in LCY';
        ShipmentDateCaptionLbl: Label 'Shipment Date';
        QtyOnBackOrderCaptionLbl: Label 'Quantity on Back Order';
        OutstandingOrdersCaptionLbl: Label 'Outstanding Orders';
        TotalCaptionLbl: Label 'Total';
        ReferenceActive: Text[52];

    procedure MakeExcelInfo()
    begin
        TempExcelBuf.SetUseInfoSheet();
        TempExcelBuf.AddInfoColumn(FORMAT(Text004Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(COMPANYNAME, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text006Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(FORMAT(Text003Lbl), FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text005Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(REPORT::"Customer - Order Detail", FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text007Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(USERID, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text008Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(TODAY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text009Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn(Customer.GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.NewRow();
        TempExcelBuf.AddInfoColumn(FORMAT(Text010Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddInfoColumn("Sales Line".GETFILTERS, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        IF PrintAmountsInLCY THEN BEGIN
            TempExcelBuf.NewRow();
            TempExcelBuf.AddInfoColumn(FORMAT(Text013Lbl), FALSE, TRUE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
            TempExcelBuf.AddInfoColumn(PrintAmountsInLCY, FALSE, FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        END;
        TempExcelBuf.ClearNewRow();
        MakeExcelDataHeader();
    end;

    local procedure MakeExcelDataHeader()
    begin
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn(Customer.FIELDCAPTION("No."), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Customer.FIELDCAPTION(Name), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(
          FORMAT(Text016Lbl + '  ' + SalesHeader.FIELDCAPTION("No.")), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Order Date"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Shipment Date"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION(Type), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(
          FORMAT(Text015Lbl + ' ' + "Sales Line".FIELDCAPTION("No.")), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION(Description), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION(Quantity), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Outstanding Quantity"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT(Text011Lbl), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Unit Price"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Line Discount Amount"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".FIELDCAPTION("Inv. Discount Amount"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(FORMAT(Text012Lbl), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
        IF NOT PrintAmountsInLCY THEN
            TempExcelBuf.AddColumn(SalesHeader.FIELDCAPTION("Currency Code"), FALSE, '', TRUE, FALSE, TRUE, '', TempExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody()
    begin
        TempExcelBuf.NewRow();
        TempExcelBuf.AddColumn(Customer."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(Customer.Name, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(SalesHeader."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn(SalesHeader."Order Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.AddColumn("Sales Line"."Shipment Date", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Date);
        TempExcelBuf.AddColumn(FORMAT(SELECTSTR("Sales Line".Type.AsInteger() + 1, Text014Lbl)), FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line"."No.", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".Description, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
        TempExcelBuf.AddColumn("Sales Line".Quantity, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn("Sales Line"."Outstanding Quantity", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(BackOrderQty, FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn("Sales Line"."Unit Price", FALSE, '', FALSE, FALSE, FALSE, '#,##0.00', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn("Sales Line"."Line Discount Amount", FALSE, '', FALSE, FALSE, FALSE, '#,##0.00', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn("Sales Line"."Inv. Discount Amount", FALSE, '', FALSE, FALSE, FALSE, '#,##0.00', TempExcelBuf."Cell Type"::Number);
        TempExcelBuf.AddColumn(SalesOrderAmount, FALSE, '', FALSE, FALSE, FALSE, '#,##0.00', TempExcelBuf."Cell Type"::Number);
        IF NOT PrintAmountsInLCY THEN
            TempExcelBuf.AddColumn(SalesHeader."Currency Code", FALSE, '', FALSE, FALSE, FALSE, '', TempExcelBuf."Cell Type"::Text);
    end;

    procedure CreateExcelbook()
    begin
        TempExcelBuf.CreateBookAndOpenExcel('', Text002Lbl, Text003Lbl, COMPANYNAME, USERID);
        ERROR('');
    end;

    procedure InitializeRequest(ShowAmountInLCY: Boolean; NewPagePerCustomer: Boolean; SetPrintToExcel: Boolean)
    begin
        PrintAmountsInLCY := ShowAmountInLCY;
        PrintOnlyOnePerPage := NewPagePerCustomer;
        PrintToExcel := SetPrintToExcel;
    end;
}