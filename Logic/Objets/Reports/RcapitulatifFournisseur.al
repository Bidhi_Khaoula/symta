report 50051 "Récapitulatif Fournisseur"
{
    DefaultLayout = RDLC;
    RDLCLayout = 'Objets\Reports\Layouts\Récapitulatif Fournisseur.rdlc';
    ApplicationArea = all;
    UsageCategory = ReportsAndAnalysis;
    dataset
    {
        dataitem("Payment Line"; "Payment Line")
        {
            DataItemTableView = SORTING("Account Type", "Account No.", "Copied To Line", "Payment in Progress");
            RequestFilterFields = "Account Type", "Account No.";
            column(EnteteBankName; entete."Bank Name")
            {
            }
            column(EnteteBankAccountNo; entete."Bank Account No.")
            {
            }
            column(EntetePaymentClassName; entete."Payment Class Name")
            {
            }
            column(EntetePostingDate; entete."Posting Date")
            {
            }
            column(TotalMontant; FORMAT(entete."Amount (LCY)"))
            {
            }
            column(AccountNo; "Account No.")
            {
            }
            column("Désignation"; Désignation)
            {
            }
            column(PostingDate; "Posting Date")
            {
            }
            column(DocumentNo; "Document No.")
            {
            }
            column(DueDate; "Due Date")
            {
            }
            column(DebitAmount; "Debit Amount")
            {
            }
            column(BankBranchNo; "Bank Branch No.")
            {
            }
            column(BankAccountNo; "Bank Account No.")
            {
            }
            column(AgencyCode; "Agency Code")
            {
            }
            column(RIBKey; "RIB Key")
            {
            }
            column(LineNo_PaymentLine; "Line No.")
            {
            }
            dataitem("Vendor Ledger Entry"; "Vendor Ledger Entry")
            {
                DataItemLink = "Vendor No." = FIELD("Account No."),
                               "Applies-to ID" = FIELD("Applies-to ID");
                DataItemTableView = SORTING("Vendor No.", "Applies-to ID", Open, Positive, "Due Date");
                column(EntryDocumentNo; "Document No.")
                {
                }
                column(EntryDueDate; "Due Date")
                {
                }
                column(EntryPostingDate; "Posting Date")
                {
                }
                column(AmountNonRapproche; ("Debit Amount" - "Credit Amount") - ("Vendor Ledger Entry"."Credit Amount" - "Vendor Ledger Entry"."Debit Amount"))
                {
                }
                column(DiffAmount; "Debit Amount" - "Credit Amount")
                {
                }

                trigger OnAfterGetRecord();
                begin

                    IF "Payment Line"."Applies-to ID" = '' THEN
                        CurrReport.BREAK();
                end;
            }

            trigger OnPreDataItem();
            begin

                LastFieldNo := FIELDNO("Account No.");
                //CurrReport.CREATETOTALS("Vendor Ledger Entry"."Debit Amount","Vendor Ledger Entry"."Credit Amount");
                company.GET();
                company.CALCFIELDS(Picture);
                //dessin.GET('compta');
                //dessin.CALCFIELDS("Image 1");

                entete.GET("Payment Line".GETFILTER("No."));
                entete.CALCFIELDS("Payment Class Name", entete."Amount (LCY)");
            end;
        }
        dataitem(LOGO; Integer)
        {
            DataItemTableView = SORTING(Number)
                                WHERE(Number = CONST(1));
            column(CompanyInfoPicture; CompanyInfo.Picture)
            {
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(CompanyInfo.Picture);

                CompanyInfo.GET();
                CompanyInfo.CALCFIELDS(Picture);
            end;
        }
    }

    var
        company: Record "Company Information";
        entete: Record "Payment Header";
        CompanyInfo: Record "Company Information";
        LastFieldNo: Integer;
}
