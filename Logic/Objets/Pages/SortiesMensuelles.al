page 50079 "Sorties Mensuelles"
{
    Editable = false;
    PageType = Card;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                group(grp)
                {
                    ShowCaption = false;
                    field("No."; Rec."No.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. field.';
                    }
                    field("No. 2"; Rec."No. 2")
                    {
                        Style = Strong;
                        StyleExpr = TRUE;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. 2 field.';
                    }
                    field(Description; Rec.Description)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Description field.';
                    }
                    field("Description 2"; Rec."Description 2")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Description 2 field.';
                    }
                }
                group(grp1)
                {
                    ShowCaption = false;
                    field(Dispo; Dispo)
                    {
                        Caption = 'Disponible';
                        Editable = false;
                        Style = Strong;
                        StyleExpr = TRUE;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Disponible field.';
                    }
                }
            }
            group(gr)
            {
                ShowCaption = false;
                group(group)
                {
                    ShowCaption = false;
                    fixed(fix)
                    {
                        ShowCaption = false;
                        group(groupe1)
                        {
                            ShowCaption = false;
                            field(TabValeur_1_1; TabValeur[1, 1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Janvier field.';
                            }
                            field(TabValeur_1_2; TabValeur[1, 2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Février field.';
                            }
                            field(TabValeur_1_3; TabValeur[1, 3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mars field.';
                            }
                            field("TRIMESTRE 1"; TabValeur[1, 1] + TabValeur[1, 2] + TabValeur[1, 3])
                            {
                                Caption = '     TRIMESTRE 1';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 1 field.';
                            }
                            field(TabValeur_1_4; TabValeur[1, 4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Avril field.';
                            }
                            field(TabValeur_1_5; TabValeur[1, 5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mai field.';
                            }
                            field(TabValeur_1_6; TabValeur[1, 6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juin field.';
                            }
                            field("TRIMESTRE 2"; TabValeur[1, 4] + TabValeur[1, 5] + TabValeur[1, 6])
                            {
                                Caption = '     TRIMESTRE 2';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 2 field.';
                            }
                            field(TabValeur_1_7; TabValeur[1, 7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juillet field.';
                            }
                            field(TabValeur_1_8; TabValeur[1, 8])
                            {
                                BlankZero = true;
                                Caption = 'Aout';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Aout field.';
                            }
                            field(_1_9; TabValeur[1, 9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Septembre field.';
                            }
                            field("TRIMESTRE 3"; TabValeur[1, 7] + TabValeur[1, 8] + TabValeur[1, 9])
                            {
                                Caption = '     TRIMESTRE 3';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 3 field.';
                            }
                            field(TabValeur_1_10; TabValeur[1, 10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Octobre field.';
                            }
                            field(TabValeur_1_11; TabValeur[1, 11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Novembre field.';
                            }
                            field(TabValeur_1_12; TabValeur[1, 12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Décembre field.';
                            }
                            field("TRIMESTRE 4"; TabValeur[1, 10] + TabValeur[1, 11] + TabValeur[1, 12])
                            {
                                Caption = '     TRIMESTRE 4';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 4 field.';
                            }
                            field(""; '')
                            {
                                ShowCaption = false;
                                ApplicationArea = All;
                            }
                            field("TOTAL"; TabValeur[1, 1] + TabValeur[1, 2] + TabValeur[1, 3] + TabValeur[1, 4] + TabValeur[1, 5] + TabValeur[1, 6] + TabValeur[1, 7] + TabValeur[1, 8] + TabValeur[1, 9] + TabValeur[1, 10] + TabValeur[1, 11] + TabValeur[1, 12])
                            {
                                Caption = 'TOTAL';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Unfavorable;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TOTAL field.';
                            }
                            field(MoyenneAnnée_1; MoyenneAnnée[1])
                            {
                                Caption = 'Moyenne';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Moyenne field.';
                            }
                            field(StkDispo_1; StkDispo[1])
                            {
                                Caption = 'Dispo';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Dispo field.';
                            }
                        }
                        group(grpp3)
                        {
                            ShowCaption = false;
                            field(TabValeur_2_1; TabValeur[2, 1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Janvier field.';
                            }
                            field(TabValeur_2_2; TabValeur[2, 2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Février field.';
                            }
                            field(TabValeur_2_3; TabValeur[2, 3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mars field.';
                            }
                            field("TRIMESTRE 1 "; TabValeur[2, 1] + TabValeur[2, 2] + TabValeur[2, 3])
                            {
                                Caption = 'TRIMESTRE 1';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 1 field.';
                            }
                            field(TabValeur_2_4; TabValeur[2, 4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Avril field.';
                            }
                            field(TabValeur_2_5; TabValeur[2, 5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mai field.';
                            }
                            field(TabValeur_2_6; TabValeur[2, 6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juin field.';
                            }
                            field("TRIMESTRE 2 "; TabValeur[2, 4] + TabValeur[2, 5] + TabValeur[2, 6])
                            {
                                Caption = 'TRIMESTRE 2';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 2 field.';
                            }
                            field(TabValeur_2_7; TabValeur[2, 7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juillet field.';
                            }
                            field(TabValeur_2_8; TabValeur[2, 8])
                            {
                                BlankZero = true;
                                Caption = 'Aout';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Aout field.';
                            }
                            field(TabValeur_2_9; TabValeur[2, 9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Septembre field.';
                            }
                            field("TRIMESTRE 3 "; TabValeur[2, 7] + TabValeur[2, 8] + TabValeur[2, 9])
                            {
                                Caption = 'TRIMESTRE 3';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 3 field.';
                            }
                            field(TabValeur_2_10; TabValeur[2, 10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Octobre field.';
                            }
                            field(TabValeur_2_11; TabValeur[2, 11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Novembre field.';
                            }
                            field(TabValeur_2_12; TabValeur[2, 12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Décembre field.';
                            }
                            field("TRIMESTRE 4 "; TabValeur[2, 10] + TabValeur[2, 11] + TabValeur[2, 12])
                            {
                                Caption = 'TRIMESTRE 4';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 4 field.';
                            }
                            field(" "; '')
                            {
                                ShowCaption = false;
                                ApplicationArea = All;
                            }
                            field("TOTAL "; TabValeur[2, 1] + TabValeur[2, 2] + TabValeur[2, 3] + TabValeur[2, 4] + TabValeur[2, 5] + TabValeur[2, 6] + TabValeur[2, 7] + TabValeur[2, 8] + TabValeur[2, 9] + TabValeur[2, 10] + TabValeur[2, 11] + TabValeur[2, 12])
                            {
                                Caption = 'TOTAL';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Unfavorable;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TOTAL field.';
                            }
                            field(MoyenneAnnée_2; MoyenneAnnée[2])
                            {
                                Caption = 'Moyenne';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Moyenne field.';
                            }
                            field(StkDispo_2; StkDispo[2])
                            {
                                Caption = 'Dispo';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Dispo field.';
                            }
                        }
                        group(gp)
                        {
                            ShowCaption = false;
                            field(TabValeur_3_1; TabValeur[3, 1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Janvier field.';
                            }
                            field(TabValeur_3_2; TabValeur[3, 2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Février field.';
                            }
                            field(TabValeur_3_3; TabValeur[3, 3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mars field.';
                            }
                            field("TRIMESTRE1"; TabValeur[3, 1] + TabValeur[3, 2] + TabValeur[3, 3])
                            {
                                Caption = '     TRIMESTRE 1';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 1 field.';
                            }
                            field(TabValeur_3_4; TabValeur[3, 4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Avril field.';
                            }
                            field(TabValeur_3_5; TabValeur[3, 5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mai field.';
                            }
                            field(TabValeur_3_6; TabValeur[3, 6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juin field.';
                            }
                            field("TRIMESTRE2"; TabValeur[3, 4] + TabValeur[3, 5] + TabValeur[3, 6])
                            {
                                Caption = '     TRIMESTRE 2';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 2 field.';
                            }
                            field(TabValeur_3_7; TabValeur[3, 7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juillet field.';
                            }
                            field(TabValeur_3_8; TabValeur[3, 8])
                            {
                                BlankZero = true;
                                Caption = 'Aout';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Aout field.';
                            }
                            field(TabValeur_3_9; TabValeur[3, 9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Septembre field.';
                            }
                            field("TRIMESTRE3"; TabValeur[3, 7] + TabValeur[3, 8] + TabValeur[3, 9])
                            {
                                Caption = '     TRIMESTRE 3';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 3 field.';
                            }
                            field(TabValeur_3_10; TabValeur[3, 10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Octobre field.';
                            }
                            field(TabValeur_3_11; TabValeur[3, 11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Novembre field.';
                            }
                            field(TabValeur_3_12; TabValeur[3, 12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Décembre field.';
                            }
                            field("TRIMESTRE4"; TabValeur[3, 10] + TabValeur[3, 11] + TabValeur[3, 12])
                            {
                                Caption = '     TRIMESTRE 4';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the      TRIMESTRE 4 field.';
                            }
                            field("   "; '')
                            {
                                ShowCaption = false;
                                ApplicationArea = All;
                            }
                            field("TOTAL  "; TabValeur[3, 1] + TabValeur[3, 2] + TabValeur[3, 3] + TabValeur[3, 4] + TabValeur[3, 5] + TabValeur[3, 6] + TabValeur[3, 7] + TabValeur[3, 8] + TabValeur[3, 9] + TabValeur[3, 10] + TabValeur[3, 11] + TabValeur[3, 12])
                            {
                                Caption = 'TOTAL';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Unfavorable;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TOTAL field.';
                            }
                            field(MoyenneAnnée_3; MoyenneAnnée[3])
                            {
                                Caption = 'Moyenne';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Moyenne field.';
                            }
                            field(StkDispo_3; StkDispo[3])
                            {
                                Caption = 'Dispo';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Dispo field.';
                            }
                        }
                        group(Grp6)
                        {
                            ShowCaption = false;
                            field(TabValeur_4_1; TabValeur[4, 1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Janvier field.';
                            }
                            field(TabValeur_4_2; TabValeur[4, 2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Février field.';
                            }
                            field(TabValeur_4_3; TabValeur[4, 3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mars field.';
                            }
                            field("TRIMESTRE4_1"; TabValeur[4, 1] + TabValeur[4, 2] + TabValeur[4, 3])
                            {
                                Caption = 'TRIMESTRE 1';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 1 field.';
                            }
                            field(TabValeur_4_4; TabValeur[4, 4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Avril field.';
                            }
                            field(TabValeur_4_5; TabValeur[4, 5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mai field.';
                            }
                            field(TabValeur_4_6; TabValeur[4, 6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juin field.';
                            }
                            field("TRIMESTRE4_2"; TabValeur[4, 4] + TabValeur[4, 5] + TabValeur[4, 6])
                            {
                                Caption = 'TRIMESTRE 2';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 2 field.';
                            }
                            field(TabValeur_4_7; TabValeur[4, 7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juillet field.';
                            }
                            field(TabValeur_4_8; TabValeur[4, 8])
                            {
                                BlankZero = true;
                                Caption = 'Aout';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Aout field.';
                            }
                            field(TabValeur_4_9; TabValeur[4, 9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Septembre field.';
                            }
                            field("TRIMESTRE4_3"; TabValeur[4, 7] + TabValeur[4, 8] + TabValeur[4, 9])
                            {
                                Caption = 'TRIMESTRE 3';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 3 field.';
                            }
                            field(TabValeur_4_10; TabValeur[4, 10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Octobre field.';
                            }
                            field(TabValeur_4_11; TabValeur[4, 11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Novembre field.';
                            }
                            field(TabValeur_4_12; TabValeur[4, 12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Décembre field.';
                            }
                            field("TRIMESTRE4_4"; TabValeur[4, 10] + TabValeur[4, 11] + TabValeur[4, 12])
                            {
                                Caption = 'TRIMESTRE 4';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 4 field.';
                            }
                            field("    "; '')
                            {
                                ShowCaption = false;
                                ApplicationArea = All;
                            }
                            field("T O T A L   "; TabValeur[4, 1] + TabValeur[4, 2] + TabValeur[4, 3] + TabValeur[4, 4] + TabValeur[4, 5] + TabValeur[4, 6] + TabValeur[4, 7] + TabValeur[4, 8] + TabValeur[4, 9] + TabValeur[4, 10] + TabValeur[4, 11] + TabValeur[4, 12])
                            {
                                Caption = 'T O T A L ';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Unfavorable;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the T O T A L  field.';
                            }
                            field(MoyenneAnnée_4; MoyenneAnnée[4])
                            {
                                Caption = 'Moyenne';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Moyenne field.';
                            }
                            field(StkDispo_4; StkDispo[4])
                            {
                                Caption = 'Dispo';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Dispo field.';
                            }
                        }
                        group(group66)
                        {
                            ShowCaption = false;
                            field(TabValeur_5_1; TabValeur[5, 1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Janvier field.';
                            }
                            field(TabValeur_5_2; TabValeur[5, 2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Février field.';
                            }
                            field(TabValeur_5_3; TabValeur[5, 3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mars field.';
                            }
                            field("TRIMESTRE5_1"; TabValeur[5, 1] + TabValeur[5, 2] + TabValeur[5, 3])
                            {
                                Caption = 'TRIMESTRE 1';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 1 field.';
                            }
                            field(TabValeur_5_4; TabValeur[5, 4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Avril field.';
                            }
                            field(TabValeur_5_5; TabValeur[5, 5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Mai field.';
                            }
                            field(TabValeur_5_6; TabValeur[5, 6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juin field.';
                            }
                            field("TRIMESTRE5_2"; TabValeur[5, 4] + TabValeur[5, 5] + TabValeur[5, 6])
                            {
                                Caption = 'TRIMESTRE 2';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 2 field.';
                            }
                            field(TabValeur_5_7; TabValeur[5, 7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Juillet field.';
                            }
                            field(TabValeur_5_8; TabValeur[5, 8])
                            {
                                BlankZero = true;
                                Caption = 'Aout';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Aout field.';
                            }
                            field(TabValeur_5_9; TabValeur[5, 9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Septembre field.';
                            }
                            field("TRIMESTRE5_3"; TabValeur[5, 7] + TabValeur[5, 8] + TabValeur[5, 9])
                            {
                                Caption = 'TRIMESTRE 3';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 3 field.';
                            }
                            field(TabValeur_5_10; TabValeur[5, 10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Octobre field.';
                            }
                            field(TabValeur_5_11; TabValeur[5, 11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Novembre field.';
                            }
                            field(TabValeur_5_12; TabValeur[5, 12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Décembre field.';
                            }
                            field("TRIMESTRE5_4"; TabValeur[5, 10] + TabValeur[5, 11] + TabValeur[5, 12])
                            {
                                Caption = 'TRIMESTRE 4';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Strong;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TRIMESTRE 4 field.';
                            }
                            field("     "; '')
                            {
                                ShowCaption = false;
                                ApplicationArea = All;
                            }
                            field("T O T A L "; TabValeur[5, 1] + TabValeur[5, 2] + TabValeur[5, 3] + TabValeur[5, 4] + TabValeur[5, 5] + TabValeur[5, 6] + TabValeur[5, 7] + TabValeur[5, 8] + TabValeur[5, 9] + TabValeur[5, 10] + TabValeur[5, 11] + TabValeur[5, 12])
                            {
                                Caption = 'T O T A L ';
                                DecimalPlaces = 0 : 2;
                                Editable = false;
                                Style = Unfavorable;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the T O T A L  field.';
                            }
                            field(MoyenneAnnée_5; MoyenneAnnée[5])
                            {
                                Caption = 'Moyenne';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Moyenne field.';
                            }
                            field(StkDispo_5; StkDispo[5])
                            {
                                Caption = 'Dispo.';
                                DecimalPlaces = 1 : 1;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Dispo. field.';
                            }
                        }
                    }
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        Calculer(rec."No.", WORKDATE())
    end;

    trigger OnOpenPage()
    begin
        //MESSAGE('ad :%1', CalculerConso('SP093195', 290416D, 290416D));
    end;

    var
        TabValeur: array[5, 12] of Decimal;
        "MoyenneAnnée": array[5] of Decimal;
        StkDispo: array[5] of Decimal;
        Dispo: Decimal;
    // TabValeurTot: array[5] of Decimal;

    procedure Calculer(ItemNo: Code[20]; DateBase: Date)
    var
        CalculConso: Codeunit CalculConso;
        DateDeb: Date;
        "Année": Integer;
        /*  Mois: Integer;
         "NbAnnée": Integer; */
        "NbMoisAnnéeN": Integer;

    begin

        CLEAR(MoyenneAnnée);
        NbMoisAnnéeN := DATE2DMY(DateBase, 2) - 1; // On compte pas le mois en cours

        // On part du 1er janvier de l'année N-5
        DateDeb := DMY2DATE(1, 1, DATE2DMY(DateBase, 3) - 4);

        /* AD Le 11-05-2016 Je crois que ca ne sert a ren car tout est fait dans la fonction d'après
        NbAnnée := 0;
        FOR Année := DATE2DMY(DateDeb, 3) TO DATE2DMY(DateBase, 3) DO
          BEGIN
            NbAnnée += 1;
            FOR Mois := 1 TO 12 DO
              BEGIN
                // MCO Le 01-10-2014 => Prendre en compte le premier jour du mois
                //IF DMY2DATE(1, Mois, Année) < DateBase THEN
                IF DMY2DATE(1, Mois, Année) <= DateBase THEN
                // FIN MCO Le 01-10-2014 => Prendre en compte le premier jour du mois
                  BEGIN
                    TabValeur[NbAnnée, Mois] := CalculerConso(ItemNo, DMY2DATE(1, Mois, Année), CALCDATE('+FM', DMY2DATE(1, Mois, Année)))
        ;
                    IF (NbAnnée = 5) AND (Mois = 4) THEN MESSAGE('ad %1  %2', TabValeur[NbAnnée, Mois],DMY2DATE(1, Mois, Année));
                    IF NOT ((NbAnnée = 5) AND ( Mois > NbMoisAnnéeN)) THEN
                      MoyenneAnnée[NbAnnée] += TabValeur[NbAnnée, Mois];
                  END;
              END;
        
          END;
        */

        CalculConso.CalculerConsoSur4ans(ItemNo, TabValeur, MoyenneAnnée, WORKDATE());


        IF NbMoisAnnéeN <> 0 THEN MoyenneAnnée[1] := MoyenneAnnée[1] / NbMoisAnnéeN;
        MoyenneAnnée[4] := MoyenneAnnée[4] / 12;
        MoyenneAnnée[3] := MoyenneAnnée[3] / 12;
        MoyenneAnnée[2] := MoyenneAnnée[2] / 12;
        MoyenneAnnée[5] := MoyenneAnnée[5] / 12;

        FOR Année := 1 TO 5 DO
            IF MoyenneAnnée[Année] <> 0 THEN
                StkDispo[Année] := Dispo / MoyenneAnnée[Année];

    end;

    procedure CalculerConso(ItemNo: Code[20]; DateDebut: Date; DateFin: Date): Decimal
    var
        Item: Record Item;
    begin

        Item.GET(ItemNo);

        Item.SETRANGE("Date Filter", DateDebut, DateFin);

        Item.CALCFIELDS("Consommation Navision", "Consommation Dbx", "Qte Ecritures Conso. Kit");

        EXIT(ROUND(Item."Consommation Navision" + Item."Consommation Dbx" + Item."Qte Ecritures Conso. Kit", 1));
    end;

    procedure InitDispo(LDispo: Decimal)
    begin
        Dispo := LDispo;
    end;
}


