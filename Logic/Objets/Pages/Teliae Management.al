page 50218 "Teliae Management"
{
    ApplicationArea = All;
    Caption = 'ENU =Teliae Management', comment = 'FRA =Pilotage Teliae';
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    SourceTable = "Shipping Label";
    SourceTableView = SORTING("Date of  bl")
                    ORDER(Descending);
    PageType = List;
    layout
    {
        area(Content)
        {
            group("End Of Day")
            {
                Caption = 'End Of Day';
                field(gEndOfDayOnlyToday; gEndOfDayOnlyToday)
                {
                    Caption = 'Only the current day';
                    ToolTip = 'Check if you want to limit the end of day to the current day';
                    ApplicationArea = All;

                }
            }
            repeater(Group)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the N° de Séquence field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Code Transporteur field.';

                }
                field("Product Code"; Rec."Product Code")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Code Produit field.';
                }
                field("Prepare Code"; Rec."Prepare Code")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Code Préparateur field.';

                }
                field("BL No."; Rec."BL No.")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the N° BL field.';

                }
                field("Date of  bl"; Rec."Date of  bl")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Date du bl field.';

                }
                field("Teliae Deleted"; Rec."Teliae Deleted")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Deleted field.';
                }
                field("Teliae Deleting User"; Rec."Teliae Deleting User")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the User suppression field.';

                }
                field("Teliae Deleting Date"; Rec."Teliae Deleting Date")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Deleting Date field.';

                }
                field("Teliae Deleting Time"; Rec."Teliae Deleting Time")
                {
                    ApplicationArea = All;
                    Editable = false;
                    ToolTip = 'Specifies the value of the Deleting Time field.';

                }
                field("Teliae End Of Day"; Rec."Teliae End Of Day")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the End Of Day field.';
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            group(Computing)
            {
                Caption = 'Computing';
                action(DeleteLabel)
                {
                    Caption = 'Delete Label';
                    ToolTip = 'Delete Label';
                    ApplicationArea = Basic, Suite;
                    Promoted = true;
                    PromotedIsBig = true;
                    Image = DeleteXML;
                    PromotedCategory = Process;
                    trigger OnAction()
                    var
                        lShippingLabel: Record "Shipping Label";
                    begin
                        CurrPage.SETSELECTIONFILTER(lShippingLabel);
                        gCUTeliaeManagement.DeleteLabel(lShippingLabel)
                    end;
                }
                action(EndOfDay)
                {
                    Caption = 'End of day';
                    ToolTip = 'End of day';
                    ApplicationArea = Basic, Suite;
                    Promoted = true;
                    PromotedIsBig = true;
                    Image = CalendarWorkcenter;
                    PromotedCategory = Process;
                    trigger OnAction()
                    begin
                        gCUTeliaeManagement.EndOfDay(gEndOfDayOnlyToday);
                    end;
                }
                action(ReturnImport)
                {
                    Caption = 'Return File Import';
                    ToolTip = 'Return File Import';
                    ApplicationArea = Basic, Suite;
                    Promoted = true;
                    PromotedIsBig = true;
                    Image = ImportCodes;
                    PromotedCategory = Process;
                    trigger OnAction()
                    VAR
                        info01Msg: Label 'Processing completed. Number of files processed : %1', comment = 'FRA=Traitement terminé. Nombre de fichiers traités : %1';
                        lNbFile: Integer;
                    BEGIN
                        lNbFile := gCUTeliaeManagement.ImportReturn();
                        MESSAGE(info01Msg, lNbFile);
                    END;
                }
                action(ReturnFile)
                {
                    Caption = 'Return File';
                    ToolTip = 'Show Return File';
                    ApplicationArea = Basic, Suite;
                    RunObject = Page "Teliae Standard Return";
                    Promoted = true;
                    PromotedIsBig = true;
                    Image = ReturnCustomerBill;
                    PromotedCategory = Process;
                }
                action(DetailColis)
                {
                    Caption = 'Detail package';
                    ApplicationArea = All;
                    RunObject = Page "Teliae UM Detail";
                    ToolTip = 'Executes the Detail package action.';
                }
            }

        }
    }
    var
        gCUTeliaeManagement: Codeunit "Teliae Management";
        gEndOfDayOnlyToday: Boolean;
}