page 50330 "Receipt Packing ListPart"
{
    PageType = ListPart;
    SourceTable = "Receipt Packing List";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(group1)
            {
                ShowCaption = false;
                field(PoidsTotal; PoidsTotal)
                {
                    Caption = 'Poids total';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids total field.';
                }
                field(VolumeTotal; VolumeTotal)
                {
                    Caption = 'Volume total';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Volume total field.';
                }
            }
            repeater(Group)
            {
                field("Demande No."; Rec."Demande No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Demande No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Type colisage"; Rec."Type colisage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type colisage field.';
                    trigger OnValidate()
                    BEGIN
                        SetEditable();
                    END;
                }
                field(Quantité; Rec.Quantité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field(Longeur; Rec.Longeur)
                {
                    ApplicationArea = All;
                    Editable = gDimensionsEditable;
                    HideValue = NOT gDimensionsEditable;
                    ToolTip = 'Specifies the value of the Longeur (m) field.';
                    trigger OnValidate();
                    begin
                        GetPoidsAndVolume();
                    end;
                }
                field(Largeur; Rec.Largeur)
                {
                    ApplicationArea = All;
                    Editable = gDimensionsEditable;
                    HideValue = NOT gDimensionsEditable;
                    ToolTip = 'Specifies the value of the Largeur (m) field.';
                    trigger OnValidate();
                    begin
                        GetPoidsAndVolume();
                    end;
                }
                field(Hauteur; Rec.Hauteur)
                {
                    ApplicationArea = All;
                    Editable = gDimensionsEditable;
                    HideValue = NOT gDimensionsEditable;
                    ToolTip = 'Specifies the value of the Hauteur (m) field.';
                    trigger OnValidate();
                    begin
                        GetPoidsAndVolume();
                    end;
                }
                field(Poids; Rec.Poids)
                {
                    ApplicationArea = All;
                    Editable = gPoidsEditable;
                    HideValue = NOT gPoidsEditable;
                    ToolTip = 'Specifies the value of the Poids field.';
                    trigger OnValidate();
                    begin
                        GetPoidsAndVolume();
                    end;
                }
                field(GetVolume; Rec.GetVolume())
                {
                    Caption = 'Volume (m3)';
                    ApplicationArea = All;
                    HideValue = NOT gDimensionsEditable;
                    ToolTip = 'Specifies the value of the Volume (m3) field.';
                }
            }
        }
    }

    actions
    {
    }
    trigger OnInit()
    begin
        gDimensionsEditable := TRUE;
        gPoidsEditable := TRUE;
    end;

    trigger OnAfterGetRecord();
    begin
        IF gDemandeNo = '' THEN BEGIN
            Rec.FILTERGROUP(4);
            gDemandeNo := Rec.GETFILTER("Demande No.");
            Rec.FILTERGROUP(0);
            GetPoidsAndVolume();
        END;
        SetEditable();
    end;

    var
        PoidsTotal: Decimal;
        VolumeTotal: Decimal;
        gDemandeNo: Code[20];
        gDimensionsEditable: Boolean;
        gPoidsEditable: Boolean;

    local procedure GetPoidsAndVolume();
    var
        ReceiptLine: Record "Receipt Packing List";
    // lDemandeNo: Code[20];
    begin
        CurrPage.UPDATE(TRUE);

        PoidsTotal := 0;
        VolumeTotal := 0;

        ReceiptLine.SETRANGE("Demande No.", gDemandeNo);
        IF ReceiptLine.FINDSET() THEN
            REPEAT
                PoidsTotal += ReceiptLine.Quantité * ReceiptLine.Poids;
                VolumeTotal += ReceiptLine.GetVolume();
            UNTIL ReceiptLine.NEXT() = 0;
    end;

    LOCAL PROCEDURE SetEditable();
    BEGIN
        gDimensionsEditable := (Rec."Type colisage" = Rec."Type colisage"::Palette) OR (Rec."Type colisage" = Rec."Type colisage"::Carton);
        gPoidsEditable := (Rec."Type colisage" <> Rec."Type colisage"::Complet);
    END;
}

