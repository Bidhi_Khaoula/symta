page 50049 "Liste Incident Transport"
{
    ApplicationArea = All;
    CardPageID = "Fiche Incident Transport";
    PageType = List;
    SourceTable = "Incident transport";
    SourceTableView = SORTING("No.")
                    ORDER(Descending);

    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                Editable = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Order No."; Rec."Order No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order No. field.';
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    Caption = 'Nom client';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom client field.';
                }
                field("Traité par"; Rec."Traité par")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traité par field.';
                }
                field("Sales Shipment No."; Rec."Sales Shipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° expédition vente field.';
                }
                field(GetDateBLSalesShipmentNo; Rec.GetDateBL(rec."Sales Shipment No."))
                {
                    Caption = 'Date expédition vente';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date expédition vente field.';
                }
                field(Contact; Rec.Contact)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Contact field.';
                }
                field("Phone No."; Rec."Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Téléphone field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Commentaire Incident"; Rec."Commentaire Incident")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire Incident field.';
                }
                field("Commentaire Traitement"; Rec."Commentaire Traitement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire Traitement field.';
                }
                field(Status; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
            }
        }
    }
    trigger OnOpenPage()
    begin
        // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
        IF Rec.FINDFIRST() THEN;
    end;

    // CFR le 07/02/2022 => FA20211005 : tri par [Clef] D‚croissant
}

