page 50241 "Critere List"
{
    Caption = 'Liste des crières';
    PageType = List;
    SourceTable = Critere;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Nom; Rec.Nom)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(Unité; Rec.Unité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unité field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Traduction)
            {
                Caption = 'Traductions';
                Image = Documents;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Traductions action.';

                trigger OnAction()
                var
                    Rec_Translation: Record "Translation SYMTA";
                    Frm_Translation: Page "Translation List";
                    RecordRef: RecordRef;
                begin
                    //Assigne l'enregistrement à un RecordRed
                    RecordRef.GETTABLE(Rec);
                    //Initialisation de la page et du record
                    CLEAR(Frm_Translation);
                    CLEAR(Rec_Translation);
                    Rec_Translation.SETRANGE(Record_ID, RecordRef.RECORDID);
                    Rec_Translation.SETRANGE("Table No", RecordRef.NUMBER);
                    //Rec_Translation.SETRANGE("Language Code",'ENU');
                    Frm_Translation.SETTABLEVIEW(Rec_Translation);
                    //Ouverture Page
                    Frm_Translation.RUN();
                end;
            }
        }
    }
}

