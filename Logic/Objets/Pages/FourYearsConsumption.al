page 50259 "Four Years Consumption"
{
    Caption = 'Consommation sur 4 ans';
    PageType = CardPart;
    SourceTable = "Requisition Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(group)
            {
                fixed(fixed)
                {
                    group(N)
                    {
                        Caption = 'N';
                        field(Conso_1_1; TabConso[1] [1])
                        {
                            BlankZero = true;
                            Caption = 'Janvier';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_1;
                            ApplicationArea = All;
                        }
                        field(Conso_1_2; TabConso[1] [2])
                        {
                            BlankZero = true;
                            Caption = 'Février';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_2;
                            ApplicationArea = All;
                        }
                        field(Conso_1_3; TabConso[1] [3])
                        {
                            BlankZero = true;
                            Caption = 'Mars';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_3;
                            ApplicationArea = All;
                        }
                        field(Conso_1_4; TabConso[1] [4])
                        {
                            BlankZero = true;
                            Caption = 'Avril';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_4;
                            ApplicationArea = All;
                        }
                        field(Conso_1_5; TabConso[1] [5])
                        {
                            BlankZero = true;
                            Caption = 'Mai';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_5;
                            ApplicationArea = All;
                        }
                        field(Conso_1_6; TabConso[1] [6])
                        {
                            BlankZero = true;
                            Caption = 'Juin';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_6;
                            ApplicationArea = All;
                        }
                        field(Conso_1_7; TabConso[1] [7])
                        {
                            BlankZero = true;
                            Caption = 'Juillet';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_7;
                            ApplicationArea = All;
                        }
                        field(Conso_1_8; TabConso[1] [8])
                        {
                            BlankZero = true;
                            Caption = 'Août';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_8;
                            ApplicationArea = All;
                        }
                        field(Conso_1_9; TabConso[1] [9])
                        {
                            BlankZero = true;
                            Caption = 'Septembre';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_9;
                            ApplicationArea = All;
                        }
                        field(Conso_1_10; TabConso[1] [10])
                        {
                            BlankZero = true;
                            Caption = 'Octobre';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_10;
                            ApplicationArea = All;
                        }
                        field(Conso_1_11; TabConso[1] [11])
                        {
                            BlankZero = true;
                            Caption = 'Novembre';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_11;
                            ApplicationArea = All;
                        }
                        field(Conso_1_12; TabConso[1] [12])
                        {
                            BlankZero = true;
                            Caption = 'Décembre';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            ShowCaption = false;
                            Style = Attention;
                            StyleExpr = StyleConso_1_12;
                            ApplicationArea = All;
                        }
                        field(TabTotalConso_1; TabTotalConso[1])
                        {
                            Caption = 'Total';
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Standard;
                            StyleExpr = TRUE;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Total field.';
                        }
                    }
                    group("N-1")
                    {
                        Caption = 'N-1';
                        field(Conso_2_1; TabConso[2] [1])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_1;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [1] field.';
                        }
                        field(Conso_2_2; TabConso[2] [2])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_2;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [2] field.';
                        }
                        field(Conso_2_3; TabConso[2] [3])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_3;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [3] field.';
                        }
                        field(Conso_2_4; TabConso[2] [4])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_4;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [4] field.';
                        }
                        field(Conso_2_5; TabConso[2] [5])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_5;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [5] field.';
                        }
                        field(Conso_2_6; TabConso[2] [6])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_6;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [6] field.';
                        }
                        field(Conso_2_7; TabConso[2] [7])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_7;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [7] field.';
                        }
                        field(Conso_2_8; TabConso[2] [8])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_8;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [8] field.';
                        }
                        field(Conso_2_9; TabConso[2] [9])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_9;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [9] field.';
                        }
                        field(Conso_2_10; TabConso[2] [10])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_10;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [10] field.';
                        }
                        field(Conso_2_11; TabConso[2] [11])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_11;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [11] field.';
                        }
                        field(Conso_2_12; TabConso[2] [12])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_2_12;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[2] [12] field.';
                        }
                        field(TabTotalConso_2; TabTotalConso[2])
                        {
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Standard;
                            StyleExpr = TRUE;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabTotalConso[2] field.';
                        }
                    }
                    group("N-2")
                    {
                        Caption = 'N-2';
                        field(Conso_3_1; TabConso[3] [1])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_1;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [1] field.';
                        }
                        field(Conso_3_2; TabConso[3] [2])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_2;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [2] field.';
                        }
                        field(Conso_3_3; TabConso[3] [3])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_3;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [3] field.';
                        }
                        field(Conso_3_4; TabConso[3] [4])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_4;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [4] field.';
                        }
                        field(Conso_3_5; TabConso[3] [5])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_5;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [5] field.';
                        }
                        field(Conso_3_6; TabConso[3] [6])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_6;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [6] field.';
                        }
                        field(Conso_3_7; TabConso[3] [7])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_7;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [7] field.';
                        }
                        field(Conso_3_8; TabConso[3] [8])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_8;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [8] field.';
                        }
                        field(Conso_3_9; TabConso[3] [9])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_9;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [9] field.';
                        }
                        field(Conso_3_10; TabConso[3] [10])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_10;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [10] field.';
                        }
                        field(Conso_3_11; TabConso[3] [11])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_11;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [11] field.';
                        }
                        field(Conso_3_12; TabConso[3] [12])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_3_12;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[3] [12] field.';
                        }
                        field(TabTotalConso_3; TabTotalConso[3])
                        {
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Standard;
                            StyleExpr = TRUE;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabTotalConso[3] field.';
                        }
                    }
                    group("N-3")
                    {
                        Caption = 'N-3';
                        field(Conso_4_1; TabConso[4] [1])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_1;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [1] field.';
                        }
                        field(Conso_4_2; TabConso[4] [2])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_2;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [2] field.';
                        }
                        field(Conso_4_3; TabConso[4] [3])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_3;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [3] field.';
                        }
                        field(Conso_4_4; TabConso[4] [4])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_4;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [4] field.';
                        }
                        field(Conso_4_5; TabConso[4] [5])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_5;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [5] field.';
                        }
                        field(Conso_4_6; TabConso[4] [6])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_6;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [6] field.';
                        }
                        field(Conso_4_7; TabConso[4] [7])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_7;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [7] field.';
                        }
                        field(Conso_4_8; TabConso[4] [8])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_8;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [8] field.';
                        }
                        field(Conso_4_9; TabConso[4] [9])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_9;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [9] field.';
                        }
                        field(Conso_4_10; TabConso[4] [10])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_10;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [10] field.';
                        }
                        field(Conso_4_11; TabConso[4] [11])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_11;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [11] field.';
                        }
                        field(Conso_4_12; TabConso[4] [12])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_4_12;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[4] [12] field.';
                        }
                        field(TabTotalConso_4; TabTotalConso[4])
                        {
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Standard;
                            StyleExpr = TRUE;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabTotalConso[4] field.';
                        }
                    }
                    group("N-4")
                    {
                        Caption = 'N-4';
                        field(Conso_5_1; TabConso[5] [1])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_1;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [1] field.';
                        }
                        field(Conso_5_2; TabConso[5] [2])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_2;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [2] field.';
                        }
                        field(Conso_5_3; TabConso[5] [3])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_3;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [3] field.';
                        }
                        field(Conso_5_4; TabConso[5] [4])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_4;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [4] field.';
                        }
                        field(Conso_5_5; TabConso[5] [5])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_5;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [5] field.';
                        }
                        field(Conso_5_6; TabConso[5] [6])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_6;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [6] field.';
                        }
                        field(Conso_5_7; TabConso[5] [7])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_7;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [7] field.';
                        }
                        field(Conso_5_8; TabConso[5] [8])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_8;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [8] field.';
                        }
                        field(Conso_5_9; TabConso[5] [9])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_9;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [9] field.';
                        }
                        field(Conso_5_10; TabConso[5] [10])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_10;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [10] field.';
                        }
                        field(Conso_5_11; TabConso[5] [11])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_11;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [11] field.';
                        }
                        field(Conso_5_12; TabConso[5] [12])
                        {
                            BlankZero = true;
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Attention;
                            StyleExpr = StyleConso_5_12;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabConso[5] [12] field.';
                        }
                        field(TabTotalConso_5; TabTotalConso[5])
                        {
                            DecimalPlaces = 0 : 0;
                            Editable = false;
                            Style = Standard;
                            StyleExpr = TRUE;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the TabTotalConso[5] field.';
                        }
                    }
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetCurrRecord();
    var

    begin
        CalculConso.CalculerConsoSur4ans(Rec."No.", TabConso, TabTotalConso, WORKDATE());
        GetColor2();
    end;

    trigger OnAfterGetRecord();

    begin
        CalculConso.CalculerConsoSur4ans(Rec."No.", TabConso, TabTotalConso, WORKDATE());
        GetColor2();
    end;

    trigger OnOpenPage();

    begin
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean;

    begin
    end;

    var
        CalculConso: Codeunit CalculConso;
        TabConso: array[5, 12] of Decimal;
        TabTotalConso: array[5] of Decimal;

        StyleConso_0_1: Boolean;
        StyleConso_0_2: Boolean;
        StyleConso_0_3: Boolean;
        StyleConso_0_4: Boolean;
        StyleConso_0_5: Boolean;
        StyleConso_0_6: Boolean;
        StyleConso_0_7: Boolean;
        StyleConso_0_8: Boolean;
        StyleConso_0_9: Boolean;
        StyleConso_0_10: Boolean;
        StyleConso_0_11: Boolean;
        StyleConso_0_12: Boolean;
        StyleConso_1_1: Boolean;
        StyleConso_1_2: Boolean;
        StyleConso_1_3: Boolean;
        StyleConso_1_4: Boolean;
        StyleConso_1_5: Boolean;
        StyleConso_1_6: Boolean;
        StyleConso_1_7: Boolean;
        StyleConso_1_8: Boolean;
        StyleConso_1_9: Boolean;
        StyleConso_1_10: Boolean;
        StyleConso_1_11: Boolean;
        StyleConso_1_12: Boolean;
        StyleConso_2_1: Boolean;
        StyleConso_2_2: Boolean;
        StyleConso_2_3: Boolean;
        StyleConso_2_4: Boolean;
        StyleConso_2_5: Boolean;
        StyleConso_2_6: Boolean;
        StyleConso_2_7: Boolean;
        StyleConso_2_8: Boolean;
        StyleConso_2_9: Boolean;
        StyleConso_2_10: Boolean;
        StyleConso_2_11: Boolean;
        StyleConso_2_12: Boolean;
        StyleConso_3_1: Boolean;
        StyleConso_3_2: Boolean;
        StyleConso_3_3: Boolean;
        StyleConso_3_4: Boolean;
        StyleConso_3_5: Boolean;
        StyleConso_3_6: Boolean;
        StyleConso_3_7: Boolean;
        StyleConso_3_8: Boolean;
        StyleConso_3_9: Boolean;
        StyleConso_3_10: Boolean;
        StyleConso_3_11: Boolean;
        StyleConso_3_12: Boolean;
        StyleConso_4_1: Boolean;
        StyleConso_4_2: Boolean;
        StyleConso_4_3: Boolean;
        StyleConso_4_4: Boolean;
        StyleConso_4_5: Boolean;
        StyleConso_4_6: Boolean;
        StyleConso_4_7: Boolean;
        StyleConso_4_8: Boolean;
        StyleConso_4_9: Boolean;
        StyleConso_4_10: Boolean;
        StyleConso_4_11: Boolean;
        StyleConso_4_12: Boolean;
        StyleConso_5_1: Boolean;
        StyleConso_5_2: Boolean;
        StyleConso_5_3: Boolean;
        StyleConso_5_4: Boolean;
        StyleConso_5_5: Boolean;
        StyleConso_5_6: Boolean;
        StyleConso_5_7: Boolean;
        StyleConso_5_8: Boolean;
        StyleConso_5_9: Boolean;
        StyleConso_5_10: Boolean;
        StyleConso_5_11: Boolean;
        StyleConso_5_12: Boolean;

    procedure GetColor(Annee: Integer; Mois: Integer): Integer;
    begin
        IF COPYSTR(Rec."Chaine Mois", Mois, 1) = 'O' THEN
            EXIT(255)
        ELSE
            EXIT(1);
    end;

    local procedure GetColor2();
    begin
        StyleConso_0_1 := FALSE;
        StyleConso_0_2 := FALSE;
        StyleConso_0_3 := FALSE;
        StyleConso_0_4 := FALSE;
        StyleConso_0_5 := FALSE;
        StyleConso_0_6 := FALSE;
        StyleConso_0_7 := FALSE;
        StyleConso_0_8 := FALSE;
        StyleConso_0_9 := FALSE;
        StyleConso_0_10 := FALSE;
        StyleConso_0_11 := FALSE;
        StyleConso_0_12 := FALSE;

        StyleConso_1_1 := FALSE;
        StyleConso_1_2 := FALSE;
        StyleConso_1_3 := FALSE;
        StyleConso_1_4 := FALSE;
        StyleConso_1_5 := FALSE;
        StyleConso_1_6 := FALSE;
        StyleConso_1_7 := FALSE;
        StyleConso_1_8 := FALSE;
        StyleConso_1_9 := FALSE;
        StyleConso_1_10 := FALSE;
        StyleConso_1_11 := FALSE;
        StyleConso_1_12 := FALSE;

        StyleConso_2_1 := FALSE;
        StyleConso_2_2 := FALSE;
        StyleConso_2_3 := FALSE;
        StyleConso_2_4 := FALSE;
        StyleConso_2_5 := FALSE;
        StyleConso_2_6 := FALSE;
        StyleConso_2_7 := FALSE;
        StyleConso_2_8 := FALSE;
        StyleConso_2_9 := FALSE;
        StyleConso_2_10 := FALSE;
        StyleConso_2_11 := FALSE;
        StyleConso_2_12 := FALSE;

        StyleConso_3_1 := FALSE;
        StyleConso_3_2 := FALSE;
        StyleConso_3_3 := FALSE;
        StyleConso_3_4 := FALSE;
        StyleConso_3_5 := FALSE;
        StyleConso_3_6 := FALSE;
        StyleConso_3_7 := FALSE;
        StyleConso_3_8 := FALSE;
        StyleConso_3_9 := FALSE;
        StyleConso_3_10 := FALSE;
        StyleConso_3_11 := FALSE;
        StyleConso_3_12 := FALSE;

        StyleConso_3_1 := FALSE;
        StyleConso_3_2 := FALSE;
        StyleConso_3_3 := FALSE;
        StyleConso_3_4 := FALSE;
        StyleConso_3_5 := FALSE;
        StyleConso_3_6 := FALSE;
        StyleConso_3_7 := FALSE;
        StyleConso_3_8 := FALSE;
        StyleConso_3_9 := FALSE;
        StyleConso_3_10 := FALSE;
        StyleConso_3_11 := FALSE;
        StyleConso_3_12 := FALSE;

        StyleConso_4_1 := FALSE;
        StyleConso_4_2 := FALSE;
        StyleConso_4_3 := FALSE;
        StyleConso_4_4 := FALSE;
        StyleConso_4_5 := FALSE;
        StyleConso_4_6 := FALSE;
        StyleConso_4_7 := FALSE;
        StyleConso_4_8 := FALSE;
        StyleConso_4_9 := FALSE;
        StyleConso_4_10 := FALSE;
        StyleConso_4_11 := FALSE;
        StyleConso_4_12 := FALSE;

        StyleConso_5_1 := FALSE;
        StyleConso_5_2 := FALSE;
        StyleConso_5_3 := FALSE;
        StyleConso_5_4 := FALSE;
        StyleConso_5_5 := FALSE;
        StyleConso_5_6 := FALSE;
        StyleConso_5_7 := FALSE;
        StyleConso_5_8 := FALSE;
        StyleConso_5_9 := FALSE;
        StyleConso_5_10 := FALSE;
        StyleConso_5_11 := FALSE;
        StyleConso_5_12 := FALSE;

        IF COPYSTR(Rec."Chaine Mois", 1, 1) = 'O' THEN BEGIN
            StyleConso_0_1 := TRUE;
            StyleConso_1_1 := TRUE;
            StyleConso_2_1 := TRUE;
            StyleConso_3_1 := TRUE;
            StyleConso_4_1 := TRUE;
            StyleConso_5_1 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 2, 1) = 'O' THEN BEGIN
            StyleConso_0_2 := TRUE;
            StyleConso_1_2 := TRUE;
            StyleConso_2_2 := TRUE;
            StyleConso_3_2 := TRUE;
            StyleConso_4_2 := TRUE;
            StyleConso_5_2 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 3, 1) = 'O' THEN BEGIN
            StyleConso_0_3 := TRUE;
            StyleConso_1_3 := TRUE;
            StyleConso_2_3 := TRUE;
            StyleConso_3_3 := TRUE;
            StyleConso_4_3 := TRUE;
            StyleConso_5_3 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 4, 1) = 'O' THEN BEGIN
            StyleConso_0_4 := TRUE;
            StyleConso_1_4 := TRUE;
            StyleConso_2_4 := TRUE;
            StyleConso_3_4 := TRUE;
            StyleConso_4_4 := TRUE;
            StyleConso_5_4 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 5, 1) = 'O' THEN BEGIN
            StyleConso_0_5 := TRUE;
            StyleConso_1_5 := TRUE;
            StyleConso_2_5 := TRUE;
            StyleConso_3_5 := TRUE;
            StyleConso_4_5 := TRUE;
            StyleConso_5_5 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 6, 1) = 'O' THEN BEGIN
            StyleConso_0_6 := TRUE;
            StyleConso_1_6 := TRUE;
            StyleConso_2_6 := TRUE;
            StyleConso_3_6 := TRUE;
            StyleConso_4_6 := TRUE;
            StyleConso_5_6 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 7, 1) = 'O' THEN BEGIN
            StyleConso_0_7 := TRUE;
            StyleConso_1_7 := TRUE;
            StyleConso_2_7 := TRUE;
            StyleConso_3_7 := TRUE;
            StyleConso_4_7 := TRUE;
            StyleConso_5_7 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 8, 1) = 'O' THEN BEGIN
            StyleConso_0_8 := TRUE;
            StyleConso_1_8 := TRUE;
            StyleConso_2_8 := TRUE;
            StyleConso_3_8 := TRUE;
            StyleConso_4_8 := TRUE;
            StyleConso_5_8 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 9, 1) = 'O' THEN BEGIN
            StyleConso_0_9 := TRUE;
            StyleConso_1_9 := TRUE;
            StyleConso_2_9 := TRUE;
            StyleConso_3_9 := TRUE;
            StyleConso_4_9 := TRUE;
            StyleConso_5_9 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 10, 1) = 'O' THEN BEGIN
            StyleConso_0_10 := TRUE;
            StyleConso_1_10 := TRUE;
            StyleConso_2_10 := TRUE;
            StyleConso_3_10 := TRUE;
            StyleConso_4_10 := TRUE;
            StyleConso_5_10 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 11, 1) = 'O' THEN BEGIN
            StyleConso_0_11 := TRUE;
            StyleConso_1_11 := TRUE;
            StyleConso_2_11 := TRUE;
            StyleConso_3_11 := TRUE;
            StyleConso_4_11 := TRUE;
            StyleConso_5_11 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 12, 1) = 'O' THEN BEGIN
            StyleConso_0_12 := TRUE;
            StyleConso_1_12 := TRUE;
            StyleConso_2_12 := TRUE;
            StyleConso_3_12 := TRUE;
            StyleConso_4_12 := TRUE;
            StyleConso_5_12 := TRUE;
        END;
    end;
}

