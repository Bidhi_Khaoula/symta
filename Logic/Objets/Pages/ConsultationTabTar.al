page 50045 "Consultation TabTar"
{
    CardPageID = "Consultation TabTar";
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = Tab_tar_tmp;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(Status; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field(c_fournisseur; Rec.c_fournisseur)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the c_fournisseur field.';
                }
                field(ref_active; Rec.ref_active)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref_active field.';
                }
                field(ref_fourni; Rec.ref_fourni)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref_fourni field.';
                }
                field(design_fourni; Rec.design_fourni)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the design_fourni field.';
                }
                field(designation; Rec.designation)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the designation field.';
                }
                field(poids; Rec.poids)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the poids field.';
                }
                field(unite_achat; Rec.unite_achat)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the unite_achat field.';
                }
                field(condit_achat; Rec.condit_achat)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the condit_achat field.';
                }
                field(px_brut; Rec.px_brut)
                {
                    DecimalPlaces = 2 :;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the px_brut field.';
                }
                field(px_net; Rec.px_net)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the px_net field.';
                }
                field(date_tarif; Rec.date_tarif)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date_tarif field.';
                }
                field(comodity_code; Rec.comodity_code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the comodity_code field.';
                }
                field(code_remise; Rec.code_remise)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the code_remise field.';
                }
                field(ligne_produit; Rec.ligne_produit)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ligne_produit field.';
                }
                field(code_gestion; Rec.code_gestion)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the code_gestion field.';
                }
                field(divers1; Rec.divers1)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the divers1 field.';
                }
                field(divers2; Rec.divers2)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the divers2 field.';
                }
                field(divers3; Rec.divers3)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the divers3 field.';
                }
                field(divers4; Rec.divers4)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the divers4 field.';
                }
                field(divers5; Rec.divers5)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the divers5 field.';
                }
                field(qte_col; Rec.qte_col)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the qte_col field.';
                }
                field(qte_col1; Rec.qte_col1)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the qte_col1 field.';
                }
                field(prix_brut1; Rec.prix_brut1)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the prix_brut1 field.';
                }
                field(qte_col2; Rec.qte_col2)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the qte_col2 field.';
                }
                field(prix_brut2; Rec.prix_brut2)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the prix_brut2 field.';
                }
                field(qte_col3; Rec.qte_col3)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the qte_col3 field.';
                }
                field(prix_brut3; Rec.prix_brut3)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the prix_brut3 field.';
                }
                field(date_tarif_2; Rec.date_tarif_2)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date_tarif_2 field.';
                }
                field(Date_Traitement; Rec.Date_Traitement)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date_Traitement field.';
                }
            }
        }
    }

    actions
    {
    }
}

