page 50303 "WIIO - Liste Whse Line"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Warehouse Shipment Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Packing List No."; WhseShipHeader."Packing List No.")
                {
                    Caption = 'N° PackingList';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° PackingList field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Qty. Outstanding"; Rec."Qty. Outstanding")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Outstanding field.';
                }
                field("Qty. to Ship"; Rec."Qty. to Ship")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Ship field.';
                }
                field(Inventory; Item.Inventory)
                {
                    Caption = 'Stock';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("WIIO Préparation"; Rec."WIIO Préparation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the WIIO Préparation field.';
                }
                field(Commentaire; Rec.getCommentLigneKit())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the getCommentLigneKit() field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        WhseShipHeader.GET(Rec."No.");

        Item.GET(Rec."Item No.");
        Item.CALCFIELDS(Inventory);
    end;

    var
        WhseShipHeader: Record "Warehouse Shipment Header";
        Item: Record Item;
}

