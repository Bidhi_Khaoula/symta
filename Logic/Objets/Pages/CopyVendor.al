page 50263 "Copy Vendor"
{
    ApplicationArea = all;
    Caption = 'Copier fournisseur';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            field(NouveauCodeFournisseur; gNouveauCodeFournisseur)
            {
                Caption = 'Nouveau code fournisseur';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nouveau code fournisseur field.';

                trigger OnValidate()
                begin
                    IF gVendor.GET(gNouveauCodeFournisseur) THEN
                        ERROR(ERROR001, gNouveauCodeFournisseur);
                end;
            }
        }
    }

    actions
    {
    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        // Pas de nouveau code saisi
        IF (gNouveauCodeFournisseur = '') THEN
            EXIT;

        IF (CloseAction = ACTION::OK) THEN BEGIN
            IF CONFIRM('Voulez vous créer le fournisseur ' + gNouveauCodeFournisseur) THEN BEGIN
                gDestinationVendor := gOrigineVendor;
                gDestinationVendor."No." := gNouveauCodeFournisseur;
                gDestinationVendor.INSERT(TRUE);
            END;
        END;
    end;

    var
        gNouveauCodeFournisseur: Code[20];
        gVendor: Record Vendor;
        ERROR001: Label 'Le fournisseur %1 existe déjà';
        gOrigineVendor: Record Vendor;
        gDestinationVendor: Record Vendor;

    procedure init(pVendor: Record Vendor)
    begin
        pVendor.TESTFIELD("No.");
        gOrigineVendor := pVendor;
    end;

    procedure returnNewVendor(var pVendor: Record Vendor)
    begin
        pVendor := gDestinationVendor;
    end;
}

