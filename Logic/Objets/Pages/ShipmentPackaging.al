page 50009 "Shipment Packaging"
{
    CardPageID = "Shipment Packaging";
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Shipement Packaging";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Packaging Code"; Rec."Packaging Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code embalage field.';
                }
                field(Qte; Rec.Qte)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qte field.';
                }
                field(weight; Rec.weight)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
            }
        }
    }

    actions
    {
    }
}

