page 50072 "Fiche litige"
{
    CardPageID = "Fiche litige";
    InsertAllowed = false;
    PageType = Card;
    SourceTable = "Gestion Litige";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                ShowCaption = false;
                Caption = 'Général';
                field("Whse Receipt No."; Rec."Whse Receipt No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Date de création"; Rec."Date de création")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date de création field.';
                }
                field("N° BL fournisseur"; Rec."N° BL fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° BL fournisseur field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Fournisseur field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field("Ref. Fournisseur"; Rec."Ref. Fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Fournisseur field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Quantité Théorique"; Rec."Quantité Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Receipt Quantity"; Rec."Receipt Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité Réceptionnée field.';
                }
                field("Prix Brut Théorique"; Rec."Prix Brut Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut Théorique field.';
                }
                field("Remise 1 Théorique"; Rec."Remise 1 Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 Théorique field.';
                }
                field("Remise 2 Théorique"; Rec."Remise 2 Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 Théorique field.';
                }
                field("Total Net Théorique"; Rec."Total Net Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net Théorique field.';
                }
                field("Prix Brut Réceptionné"; Rec."Prix Brut Réceptionné")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut Réceptionné field.';
                }
                field("Remise 1 Réceptionnée"; Rec."Remise 1 Réceptionnée")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 Réceptionnée field.';
                }
                field("Remise 2 Réceptionnée"; Rec."Remise 2 Réceptionnée")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 Réceptionnée field.';
                }
                field("Total Net  Réceptionné"; Rec."Total Net  Réceptionné")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net  Réceptionné field.';
                }
                field(Diff_; Diff())
                {
                    Caption = 'Ecart';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ecart field.';
                }
                field("Receipt No."; Rec."Receipt No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Reception field.';
                }
                field("Receipt Line No."; Rec."Receipt Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Reception ligne field.';
                }
                field("Date Litige"; Rec."Date Litige")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Litige field.';
                }
                field("Litige Prix"; Rec."Litige Prix")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Litige Prix field.';
                }
                field("Liitige Quantité"; Rec."Liitige Quantité")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Liitige Quantité field.';
                }
                field(Action; Rec.Action)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Action field.';
                }
                field(Statut; Rec.Statut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Closed Date"; Rec."Closed Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Cloture field.';
                }
                field("document régularisation"; Rec."document régularisation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the document régularisation field.';
                }
                field("référence régularisation"; Rec."référence régularisation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the référence régularisation field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure Diff(): Decimal
    begin
        EXIT(Rec."Total Net  Réceptionné" - Rec."Total Net Théorique");
    end;
}

