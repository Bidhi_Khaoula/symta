page 50090 "Item Ledger Entries_lm"
{
    Caption = 'Item Ledger Entries';
    CardPageID = "Item Ledger Entries_lm";
    DataCaptionFields = "Item No.";
    Editable = false;
    PageType = List;
    SourceTable = "Item Ledger Entry";
    SourceTableView = SORTING("Item No.", "Location Code");
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entry No. field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Date Réelle Ecriture"; Rec."Date Réelle Ecriture")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Réelle Ecriture field.';
                }
                field("Date Arrivage Marchandise"; Rec."Date Arrivage Marchandise")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Arrivage Marchandise field.';
                }
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Entry Type"; Rec."Entry Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entry Type field.';
                }
                field("Document Type"; Rec."Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Type field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Document Line No."; Rec."Document Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Line No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    Caption = 'Item No.';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("<Item No. Bis>"; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(refactive; ref_active)
                {
                    Caption = 'recherche référence';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the recherche référence field.';
                }
                field("Référence saisie"; Rec."Référence saisie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence saisie field.';
                }
                field("Item Card Manufacturer"; Rec."Item Card Manufacturer")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Marque Fiche Article field.';
                }
                field("Item Card Sub Family"; FctGetFamily())
                {
                    Visible = false;
                    ApplicationArea = All;
                    Caption = 'Famille Fiche Article';
                    ToolTip = 'Specifies the value of the Famille Fiche Article field.';
                }
                field("Item Card Family"; Rec."Item Card Family")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille Fiche Article field.';
                }

                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("External Document No. 2"; Rec."External Document No. 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Return Reason Code"; Rec."Return Reason Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Return Reason Code field.';
                }
                field("Global Dimension 1 Code"; Rec."Global Dimension 1 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Global Dimension 1 Code field.';
                }
                field("Global Dimension 2 Code"; Rec."Global Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Global Dimension 2 Code field.';
                }
                field("Expiration Date"; Rec."Expiration Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Expiration Date field.';
                }
                field("Serial No."; Rec."Serial No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Serial No. field.';
                }
                field("Lot No."; Rec."Lot No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Lot No. field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Invoiced Quantity"; Rec."Invoiced Quantity")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Invoiced Quantity field.';
                }
                field("Remaining Quantity"; Rec."Remaining Quantity")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remaining Quantity field.';
                }
                field("Shipped Qty. Not Returned"; Rec."Shipped Qty. Not Returned")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipped Qty. Not Returned field.';
                }
                field("Reserved Quantity"; Rec."Reserved Quantity")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reserved Quantity field.';
                }
                field("Qty. per Unit of Measure"; Rec."Qty. per Unit of Measure")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. per Unit of Measure field.';
                }
                field("Sales Amount (Expected)"; Rec."Sales Amount (Expected)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Amount (Expected) field.';
                }
                field("Sales Amount (Actual)"; Rec."Sales Amount (Actual)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Amount (Actual) field.';
                }
                field(CalcPrixVenteUnitaire_; CalcPrixVenteUnitaire())
                {
                    Caption = 'Px vente unitaire';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Px vente unitaire field.';
                }
                field(CalcUnitCost_; CalcUnitCost())
                {
                    Caption = 'Coût unitaire';
                    DecimalPlaces = 2 : 4;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coût unitaire field.';
                }
                field("Cost Amount (Expected)"; Rec."Cost Amount (Expected)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Expected) field.';
                }
                field("Cost Amount (Actual)"; Rec."Cost Amount (Actual)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Actual) field.';
                }
                field("Cost Amount (Non-Invtbl.)"; Rec."Cost Amount (Non-Invtbl.)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Non-Invtbl.) field.';
                }
                field("Cost Amount (Expected) (ACY)"; Rec."Cost Amount (Expected) (ACY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Expected) (ACY) field.';
                }
                field("Cost Amount (Actual) (ACY)"; Rec."Cost Amount (Actual) (ACY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Actual) (ACY) field.';
                }
                field("Cost Amount (Non-Invtbl.)(ACY)"; Rec."Cost Amount (Non-Invtbl.)(ACY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost Amount (Non-Invtbl.)(ACY) field.';
                }
                field("Completely Invoiced"; Rec."Completely Invoiced")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Completely Invoiced field.';
                }
                field(Open; Rec.Open)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Open field.';
                }
                field("Drop Shipment"; Rec."Drop Shipment")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Drop Shipment field.';
                }
                field("Applied Entry to Adjust"; Rec."Applied Entry to Adjust")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Applied Entry to Adjust field.';
                }
                field("Prod. Order Comp. Line No."; Rec."Prod. Order Comp. Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prod. Order Comp. Line No. field.';
                }
                field("Job No."; Rec."Job No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Project No. field.';
                }
                field("Job Task No."; Rec."Job Task No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Project Task No. field.';
                }
                field("No."; rec_item."No.")
                {
                    Caption = 'ref interne';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref interne field.';
                }
                field("Manufacturer Code"; rec_item."Manufacturer Code")
                {
                    Caption = 'marque';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the marque field.';
                }
                field("Product Type"; rec_item."Product Type")
                {
                    Caption = 'type produit';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the type produit field.';
                }
                field(rec_customerName; rec_customer.Name)
                {
                    Caption = 'nom client';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the nom client field.';
                }
                field(rec_vendorName; rec_vendor.Name)
                {
                    Caption = 'nom fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the nom fournisseur field.';
                }
                field(no_fac_vente; no_fac_vente)
                {
                    Caption = 'no facture vente';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the no facture vente field.';
                }
                field(no_av_vente; no_av_vente)
                {
                    Caption = 'no avoir vente';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the no avoir vente field.';
                }
                field("Montant achat"; Rec."Purchase Amount (Actual)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Purchase Amount (Actual) field.';
                }
                field(px_ach_uni; px_ach_uni)
                {
                    Caption = 'px achat uni';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the px achat uni field.';
                }
                field("Stock Physique"; Rec."Stock Physique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Physique field.';
                }
                field("Ref Active"; Rec."Ref Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref Active field.';
                }
                field("Item Category Code"; Rec."Item Category Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Category Code field.';
                }
                field("Warehouse Document No."; Rec."Warehouse Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° document magasin field.';
                }
                field("Source Document No."; Rec."Source Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° document origine field.';
                }
                field("Return Purchase Adjustment"; Rec."Return Purchase Adjustment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Régularisation Retour Achat field.';
                }
                field("Return Purchase Request"; Rec."Return Purchase Request")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Demande Retour Achat field.';
                }
                field("Warehouse Receipt Adjustment"; Rec."Warehouse Receipt Adjustment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Régularisation Réception Magasin field.';
                }
                field("Warehouse Receipt Request"; Rec."Warehouse Receipt Request")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Demande Réception Magasin field.';
                }
                field("User Entry"; Rec."User Entry")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the User ID field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("Ent&ry")
            {
                Caption = 'Ent&ry';
                action("&Value Entries")
                {
                    Caption = '&Value Entries';
                    Image = ValueLedger;
                    RunObject = Page "Value Entries";
                    RunPageLink = "Item Ledger Entry No." = FIELD("Entry No.");
                    RunPageView = SORTING("Item Ledger Entry No.");
                    ShortCutKey = 'Ctrl+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Value Entries action.';
                }
            }
            group("&Application")
            {
                Caption = '&Application';
                action("Applied E&ntries")
                {
                    Caption = 'Applied E&ntries';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Applied E&ntries action.';

                    trigger OnAction()
                    begin
                        CODEUNIT.RUN(CODEUNIT::"Show Applied Entries", Rec);
                    end;
                }
                action("Reservation Entries")
                {
                    Caption = 'Reservation Entries';
                    Image = ReservationLedger;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Reservation Entries action.';

                    trigger OnAction()
                    begin
                        Rec.ShowReservationEntries(TRUE);
                    end;
                }
                action("Application Worksheet")
                {
                    Caption = 'Application Worksheet';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Application Worksheet action.';

                    trigger OnAction()
                    var
                        Worksheet: Page "Application Worksheet";
                    begin
                        CLEAR(Worksheet);
                        Worksheet.SetRecordToShow(Rec);
                        Worksheet.RUN();
                    end;
                }
            }
        }
        area(processing)
        {
            group("F&unctions")
            {
                Caption = 'F&unctions';
                action("Order &Tracking")
                {
                    Caption = 'Order &Tracking';
                    ApplicationArea = All;
                    Image = OrderTracking;
                    ToolTip = 'Executes the Order &Tracking action.';

                    trigger OnAction()
                    var
                        TrackingForm: Page "Order Tracking";
                    begin
                        TrackingForm.SetItemLedgEntry(Rec);
                        TrackingForm.RUNMODAL();
                    end;
                }
            }
            action("&Navigate")
            {
                Caption = '&Navigate';
                Image = Navigate;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Navigate action.';

                trigger OnAction()
                begin
                    Navigate.SetDoc(rec."Posting Date", rec."Document No.");
                    Navigate.RUN();
                end;
            }
            action("Afficher BL/RVE/RME/RAE")
            {
                Image = Warehouse;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Afficher BL/RVE/RME/RAE action.';

                trigger OnAction()
                var
                    SalesShipmentHeader: Record "Sales Shipment Header";
                    PostedWhseReceipt: Record "Posted Whse. Receipt Header";
                    SalesReturnReceipt: Record "Return Receipt Header";
                    lReturnShipmentHeader: Record "Return Shipment Header";
                begin
                    // MCO Le 04-10-2018 => Régie
                    IF rec."Document Type" = rec."Document Type"::"Purchase Receipt" THEN BEGIN
                        PostedWhseReceipt.SETRANGE("Whse. Receipt No.", rec."Warehouse Document No.");
                        PAGE.RUN(7330, PostedWhseReceipt);
                    END;
                    IF rec."Document Type" = rec."Document Type"::"Sales Shipment" THEN BEGIN
                        SalesShipmentHeader.RESET();
                        SalesShipmentHeader.GET(rec."Document No.");
                        PAGE.RUN(130, SalesShipmentHeader);
                    END;
                    // MCO Le 21-11-2018 => Régie
                    IF rec."Document Type" = rec."Document Type"::"Sales Return Receipt" THEN BEGIN
                        SalesReturnReceipt.RESET();
                        SalesReturnReceipt.GET(rec."Document No.");
                        PAGE.RUN(6660, SalesReturnReceipt);
                    END;
                    // FIN MCO Le 21-11-2018
                    // CFR le 16/04/2024 - R‚gie : accŠs aux retours achats
                    IF Rec."Document Type" = Rec."Document Type"::"Purchase Return Shipment" THEN BEGIN
                        lReturnShipmentHeader.RESET();
                        lReturnShipmentHeader.GET(Rec."Document No.");
                        PAGE.RUN(6650, lReturnShipmentHeader);
                    END;
                    // FIN CFR le 16/04/2024 - R‚gie : accŠs aux retours achats
                END;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin

        rec_item.INIT();
        IF rec_item.GET(rec."Item No.") THEN;

        rec_customer.INIT();
        IF rec_customer.GET(rec."Source No.") THEN;

        rec_vendor.INIT();
        IF rec_vendor.GET(rec."Source No.") THEN;

        rec_sales_inv_line.RESET();
        rec_sales_inv_line.SETRANGE(rec_sales_inv_line."Shipment No.", rec."Document No.");
        IF rec_sales_inv_line.FINDFIRST() THEN
            no_fac_vente := rec_sales_inv_line."Document No."
        ELSE
            no_fac_vente := '';

        // MCO Le 03-10-2018 => On affiche le N° d'avoir
        rec_sales_cr_line.RESET();
        rec_sales_cr_line.SETRANGE("Return Receipt No.", rec."Document No.");
        IF rec_sales_cr_line.FINDFIRST() THEN
            no_av_vente := rec_sales_cr_line."Document No."
        ELSE
            no_av_vente := '';
        // FIN MCO Le 03-10-2018 => On affiche le N° d'avoir

        // lm le 05-05-2013=> affichage prix unitaire
        px_ach_uni := rec."Purchase Amount (Actual)" / rec.Quantity;

        //LM le 14-02-2013 =>affichage ref_active
        ref_active := '';
        IF COPYSTR(rec."Document No.", 1, 2) = 'BL' THEN BEGIN
            IF rec_sales_ship_line.GET(rec."Document No.", rec."Document Line No.") THEN;
            ref_active := rec_sales_ship_line."Recherche référence";
        END
        ELSE
            IF COPYSTR(rec."Document No.", 1, 2) = 'BR' THEN BEGIN
                IF rec_purch_recept_line.GET(rec."Document No.", rec."Document Line No.") THEN;
                ref_active := rec_purch_recept_line."Recherche référence";
            END;
        ItemNoC8OnFormat(FORMAT(rec."Item No."));

        rec."Posting Date" := rec."Posting Date";
    end;

    var
        rec_item: Record Item;
        rec_customer: Record Customer;
        rec_vendor: Record Vendor;
        rec_sales_inv_line: Record "Sales Invoice Line";
        rec_sales_ship_line: Record "Sales Shipment Line";
        rec_purch_recept_line: Record "Purch. Rcpt. Line";
        rec_sales_cr_line: Record "Sales Cr.Memo Line";
        Navigate: Page Navigate;
        CUMultiRef: Codeunit "Gestion Multi-référence";

        no_fac_vente: Text[20];
        ref_active: Text[30];

        px_ach_uni: Decimal;

        no_av_vente: Code[20];


    procedure GetCaption(): Text[250]
    var
        GLSetup: Record "General Ledger Setup";
        ObjTransl: Record "Object Translation";
        Item: Record Item;
        // ProdOrder: Record "Production Order";
        Cust: Record Customer;
        Vend: Record Vendor;
        Dimension: Record Dimension;
        DimValue: Record "Dimension Value";
        SourceTableName: Text[100];
        SourceFilter: Text[200];
        Description: Text[100];
    begin
        Description := '';

        CASE TRUE OF
            rec.GETFILTER("Item No.") <> '':
                BEGIN
                    SourceTableName := ObjTransl.TranslateObject(ObjTransl."Object Type"::Table, 27);
                    SourceFilter := rec.GETFILTER("Item No.");
                    IF MAXSTRLEN(Item."No.") >= STRLEN(SourceFilter) THEN
                        IF Item.GET(SourceFilter) THEN
                            Description := Item.Description;
                END;
            //  GETFILTER("Prod. Order No.") <> '':
            //    BEGIN
            //      SourceTableName := ObjTransl.TranslateObject(ObjTransl."Object Type"::Table,5405);
            //      SourceFilter := GETFILTER("Prod. Order No.");
            //      IF MAXSTRLEN(ProdOrder."No.") >= STRLEN(SourceFilter) THEN
            //        IF ProdOrder.GET(ProdOrder.Status::Released,SourceFilter) OR
            //           ProdOrder.GET(ProdOrder.Status::Finished,SourceFilter)
            //        THEN BEGIN
            //          SourceTableName := STRSUBSTNO('%1 %2',ProdOrder.Status,SourceTableName);
            //          Description := ProdOrder.Description;
            //        END;
            //    END;
            rec.GETFILTER("Source No.") <> '':
                BEGIN
                    CASE rec."Source Type" OF
                        rec."Source Type"::Customer:
                            BEGIN
                                SourceTableName :=
                                  ObjTransl.TranslateObject(ObjTransl."Object Type"::Table, 18);
                                SourceFilter := rec.GETFILTER("Source No.");
                                IF MAXSTRLEN(Cust."No.") >= STRLEN(SourceFilter) THEN
                                    IF Cust.GET(SourceFilter) THEN
                                        Description := Cust.Name;
                            END;
                        rec."Source Type"::Vendor:
                            BEGIN
                                SourceTableName :=
                                  ObjTransl.TranslateObject(ObjTransl."Object Type"::Table, 23);
                                SourceFilter := rec.GETFILTER("Source No.");
                                IF MAXSTRLEN(Vend."No.") >= STRLEN(SourceFilter) THEN
                                    IF Vend.GET(SourceFilter) THEN
                                        Description := Vend.Name;
                            END;
                    END;
                END;
            rec.GETFILTER("Global Dimension 1 Code") <> '':
                BEGIN
                    GLSetup.GET();
                    Dimension.Code := GLSetup."Global Dimension 1 Code";
                    SourceFilter := rec.GETFILTER("Global Dimension 1 Code");
                    SourceTableName := Dimension.GetMLName(GLOBALLANGUAGE);
                    IF MAXSTRLEN(DimValue.Code) >= STRLEN(SourceFilter) THEN
                        IF DimValue.GET(GLSetup."Global Dimension 1 Code", SourceFilter) THEN
                            Description := DimValue.Name;
                END;
            rec.GETFILTER("Global Dimension 2 Code") <> '':
                BEGIN
                    GLSetup.GET();
                    Dimension.Code := GLSetup."Global Dimension 2 Code";
                    SourceFilter := rec.GETFILTER("Global Dimension 2 Code");
                    SourceTableName := Dimension.GetMLName(GLOBALLANGUAGE);
                    IF MAXSTRLEN(DimValue.Code) >= STRLEN(SourceFilter) THEN
                        IF DimValue.GET(GLSetup."Global Dimension 2 Code", SourceFilter) THEN
                            Description := DimValue.Name;
                END;
            rec.GETFILTER("Document Type") <> '':
                BEGIN
                    SourceTableName := rec.GETFILTER("Document Type");
                    SourceFilter := rec.GETFILTER("Document No.");
                    Description := rec.GETFILTER("Document Line No.");
                END;
        END;
        EXIT(STRSUBSTNO('%1 %2 %3', SourceTableName, SourceFilter, Description));
    end;

    local procedure ItemNoC8OnAfterInput(var Text: Text[1024])
    var
        C: Code[20];
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        EVALUATE(C, Text);
        Text := CUMultiRef.RechercheArticleByActive(C);
    end;

    local procedure ItemNoC8OnFormat(Text: Text[1024])
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;

    local procedure CalcUnitCost(): Decimal
    begin
        IF (rec."Invoiced Quantity" <> 0) THEN
            EXIT(rec."Cost Amount (Actual)" / rec."Invoiced Quantity")
        ELSE
            EXIT(rec."Cost Amount (Actual)" / rec.Quantity);
    end;

    local procedure CalcPrixVenteUnitaire(): Decimal
    begin
        //fbrun le 05/12/2019 Ticket 49173
        //AVANT
        //EXIT("Sales Amount (Actual)"/"Invoiced Quantity"+0.0001);

        IF rec."Invoiced Quantity" <> 0 THEN
            EXIT(rec."Sales Amount (Actual)" / rec."Invoiced Quantity");

        IF rec.Quantity <> 0 THEN
            EXIT(rec."Sales Amount (Expected)" / rec.Quantity)
    end;

    local procedure FctGetFamily(): Code[20]
    var
        RecLItem: record Item;
    begin
        RecLItem.get("Item No.");
        exit(RecLItem.GetCodeFamille())
    end;
}

