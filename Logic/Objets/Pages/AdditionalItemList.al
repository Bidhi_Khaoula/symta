page 50074 "Additional Item List"
{
    Caption = 'Liste articles complémentaires';
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Additional Item";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Item No."; Rec."Item No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Item No. 2"; Rec."Item No. 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active article field.';
                }
                field("Item Description"; Rec."Item Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description article field.';
                }
                field("Item Description 2"; Rec."Item Description 2")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 article field.';
                }
                field("Additional Search Reference"; Rec."Additional Search Reference")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence complémentaire field.';
                }
                field("Additional Item No."; Rec."Additional Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Additional Item No. 2"; Rec."Additional Item No. 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active article complémentaire field.';
                }
                field("Additional Item Description"; Rec."Additional Item Description")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description article complémentaire field.';
                }
                field("Additional Item Description 2"; Rec."Additional Item Description 2")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 article complémentaire field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field(Commentaire; Rec.Commentaire)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Create User ID"; Rec."Create User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur Création field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Modify User ID"; Rec."Modify User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur modif. Cde field.';
                }
                field("Modify Date"; Rec."Modify Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date modif. Cde field.';
                }
            }
        }
    }

    actions
    {
    }
}

