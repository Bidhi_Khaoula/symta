page 50030 "Packing List"
{
    CardPageID = "Packing List";
    InsertAllowed = false;
    PageType = List;
    SourceTable = "Packing List";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("N° article"; Rec."N° article")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° article field.';
                }
                field(Désignation; Rec.Désignation)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Quantité à expédié"; Rec."Quantité à expédié")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité à expédié field.';
                }
                field("Quantité Dans le colis"; Rec."Quantité Dans le colis")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité Dans le colis field.';
                }
                field("Numero de Colis"; Rec."Numero de Colis")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero de Colis field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Dé&tail")
            {
                Caption = 'Dé&tail';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Dé&tail action.';

                trigger OnAction()
                var
                    "DétailColis": Record "Détail Packing List";
                begin
                    Rec.TESTFIELD("N° expedition magasin");
                    Rec.TESTFIELD("Numero de Colis");

                    IF NOT DétailColis.GET(Rec."N° expedition magasin", Rec."Numero de Colis") THEN BEGIN
                        DétailColis.INIT();
                        DétailColis."N° expedition magasin" := Rec."N° expedition magasin";
                        DétailColis."Numero de Colis" := Rec."Numero de Colis";
                        DétailColis.Insert();
                    END;

                    // MIG 2015 - GR - FORM
                    //FrmDétailColis.SETRECORD(DétailColis);
                    //FrmDétailColis.RUN();
                end;
            }
            action("&Dupliquer ligne")
            {
                Caption = '&Dupliquer ligne';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Dupliquer ligne action.';

                trigger OnAction()
                begin
                    //calcul quantité saisi
                    "ligne tmp".COPYFILTERS(Rec);
                    "ligne tmp".SETCURRENTKEY("N° expedition magasin", "N° Ligne origine");
                    "ligne tmp".SETRANGE("N° Ligne origine", Rec."N° Ligne origine");
                    "ligne tmp".CALCSUMS("Quantité Dans le colis");

                    CLEAR("Ligne BP");
                    "Ligne BP".GET(Rec."N° expedition magasin", Rec."N° Ligne origine");

                    quantité_tmp := "Ligne BP"."Qty. to Ship" - "ligne tmp"."Quantité Dans le colis";


                    CLEAR("ligne tmp");
                    "ligne tmp" := Rec;
                    IF Rec.FIND('>') THEN
                        num_ligne := (Rec."N° ligne" - "ligne tmp"."N° ligne") / 2
                    ELSE
                        num_ligne := 10000;

                    num_ligne := ROUND(num_ligne, 1, '=');

                    "ligne tmp"."N° ligne" := "ligne tmp"."N° ligne" + num_ligne;
                    "ligne tmp"."Quantité Dans le colis" := quantité_tmp;
                    "ligne tmp"."Numero de Colis" := '';

                    IF "Ligne BP".GET("ligne tmp"."N° expedition magasin", "ligne tmp"."N° ligne") THEN
                        "ligne tmp"."N° ligne" := "ligne tmp"."N° ligne" + 500;

                    "ligne tmp".Insert();

                    IF Rec.FIND('<') THEN;
                end;
            }
            action("re-génerer la paking list")
            {
                Caption = 're-génerer la paking list';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the re-génerer la paking list action.';

                trigger OnAction()
                begin
                    "Créer paking liste"();
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        IF NOT Rec.FIND('-') THEN
            "Créer paking liste"();
    end;

    var
        "Ligne BP": Record "Warehouse Shipment Line";
        "ligne tmp": Record "Packing List";
        num_ligne: Decimal;
        "quantité_tmp": Decimal;

    procedure "Créer paking liste"()
    begin
        CLEAR("Ligne BP");
        "Ligne BP".RESET();
        "Ligne BP".SETCURRENTKEY("No.", "Sorting Sequence No.");
        "Ligne BP".SETRANGE("No.", Rec.GETFILTER("N° expedition magasin"));
        IF "Ligne BP".FIND('-') THEN
            REPEAT
                Rec."N° expedition magasin" := "Ligne BP"."No.";
                // AD Le 04-03-2008 => Pour mettre dans le même ordre que les BP
                //"N° ligne":="Ligne BP"."Line No.";
                Rec."N° ligne" := "Ligne BP"."Sorting Sequence No.";
                Rec."N° Ligne origine" := "Ligne BP"."Line No.";
                Rec."N° article" := "Ligne BP"."Item No.";
                Rec.Désignation := "Ligne BP".Description;
                Rec."Quantité à expédié" := "Ligne BP"."Qty. to Ship";
                Rec."Quantité Dans le colis" := "Ligne BP"."Qty. to Ship";
                Rec."Shelf No." := "Ligne BP"."Shelf No.";
                IF Rec.INSERT() THEN;
            UNTIL "Ligne BP".NEXT() = 0;
    end;
}

