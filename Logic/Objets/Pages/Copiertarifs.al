page 50098 "Copier tarifs"
{
    CardPageID = "Copier tarifs";
    PageType = Card;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(_1; _1)
            {
                Caption = 'Groupe Tarif Client';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Groupe Tarif Client field.';
            }
            field(_2; _2)
            {
                Caption = 'Date Début tarif GDI';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Début tarif GDI field.';
            }
            field(_3; _3)
            {
                Caption = 'Coef. Prix';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Coef. Prix field.';
            }
            field(_4; _4)
            {
                Caption = 'Taux de change';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Taux de change field.';
            }
            field(_5; _5)
            {
                Caption = 'Date Début Application';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Début Application field.';
            }
            field(_6; _6)
            {
                Caption = 'Date Fin Application';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Fin Application field.';
            }
        }
    }

    actions
    {
    }

    var
        _1: Code[10];
        _2: Date;
        _3: Decimal;
        _4: Decimal;
        _5: Date;
        _6: Date;
}

