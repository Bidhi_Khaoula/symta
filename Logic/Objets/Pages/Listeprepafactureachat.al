page 50084 "Liste prepa-facture achat"
{
    CardPageID = "Liste prepa-facture achat";
    Editable = false;
    PageType = List;
    SourceTable = "Import facture tempo. fourn.";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Code fournisseur"; Rec."Code fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code fournisseur field.';
                }
                field("No Facture"; Rec."No Facture")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Facture field.';
                }
                field("Date Facture"; Rec."Date Facture")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Facture field.';
                }
                field("No commande"; Rec."No commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande field.';
                }
                field("ligne commande"; Rec."ligne commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ligne commande field.';
                }
                field("Code article SYMTA"; Rec."Code article SYMTA")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article SYMTA field.';
                }
                field("Code article Fournisseur"; Rec."Code article Fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article Fournisseur field.';
                }
                field(Désignation; Rec.Désignation)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field(Quantité; Rec.Quantité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field("Pu brut"; Rec."Pu brut")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pu brut field.';
                }
                field("Remise 1"; Rec."Remise 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 field.';
                }
                field("Remise 2"; Rec."Remise 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 field.';
                }
                field("Pu net"; Rec."Pu net")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pu net field.';
                }
                field("Total Net"; Rec."Total Net")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net field.';
                }
                field("Date import"; Rec."Date import")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date import field.';
                }
                field("heure import"; Rec."heure import")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the heure import field.';
                }
                field("utilisateur import"; Rec."utilisateur import")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the utilisateur import field.';
                }
                field("libéllé import erreur"; Rec."libéllé import erreur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the libéllé import erreur field.';
                }
                field("date intégration"; Rec."date intégration")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date intégration field.';
                }
                field("heure intégration"; Rec."heure intégration")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the heure intégration field.';
                }
                field("utilisateur integration"; Rec."utilisateur integration")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the utilisateur integration field.';
                }
                field("erreur integration"; Rec."erreur integration")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the erreur integration field.';
                }
                field("No de reception"; Rec."No de reception")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No de reception field.';
                }
                field("No ligne reception"; Rec."No ligne reception")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No ligne reception field.';
                }
                field("No commande affecté"; Rec."No commande affecté")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande affecté field.';
                }
                field("No ligne de commande"; Rec."No ligne de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No ligne de commande field.';
                }
                field("No reception enregistré"; Rec."No reception enregistré")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No reception enregistré field.';
                }
                field("No ligne reception enregistré"; Rec."No ligne reception enregistré")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No ligne reception enregistré field.';
                }
                field("Aucune ligne trouvé"; Rec."Aucune ligne trouvé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aucune ligne trouvé field.';
                }
                field("Erreur côut"; Rec."Erreur côut")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Erreur côut field.';
                }
                field("Erreur Quantité"; Rec."Erreur Quantité")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Erreur Quantité field.';
                }
                field("Plusieur ligne possible"; Rec."Plusieur ligne possible")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Plusieur ligne possible field.';
                }
                field("Erreur traité"; Rec."Erreur traité")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Erreur traité field.';
                }
                field("Code article commande"; Rec."Code article commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article commande field.';
                }
                field("Quantité restante commande"; Rec."Quantité restante commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité restante commande field.';
                }
                field("Pu net commande"; Rec."Pu net commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pu net commande field.';
                }
                field("Code article a utilisé"; Rec."Code article a utilisé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article a utilisé field.';
                }
                field("Nouvelle quantité"; Rec."Nouvelle quantité")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nouvelle quantité field.';
                }
                field("Nouveau cout net"; Rec."Nouveau cout net")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nouveau cout net field.';
                }
                field("Pu brut commande"; Rec."Pu brut commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pu brut commande field.';
                }
                field("Remise 1 commande"; Rec."Remise 1 commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 commande field.';
                }
                field("Remise 2 commande"; Rec."Remise 2 commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 commande field.';
                }
                field("Total net commande"; Rec."Total net commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total net commande field.';
                }
                field("Type erreur"; Rec."Type erreur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type erreur field.';
                }
                field("Nouveau Pu Brut"; Rec."Nouveau Pu Brut")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nouveau Pu Brut field.';
                }
                field("Nouvelle Remise 1"; Rec."Nouvelle Remise 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nouvelle Remise 1 field.';
                }
                field("Nouvelle Remise 2"; Rec."Nouvelle Remise 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nouvelle Remise 2 field.';
                }
            }
        }
    }

    actions
    {
    }
}

