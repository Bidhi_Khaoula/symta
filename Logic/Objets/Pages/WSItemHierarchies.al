page 50224 "WS Item Hierarchies"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Hierarchies";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code Article"; Rec."Code Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Article field.';
                }
                field("Super Famille Marketing"; Rec."Super Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Super Famille Marketing field.';
                }
                field("Famille Marketing"; Rec."Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Famille Marketing field.';
                }
                field("Sous Famille Marketing"; Rec."Sous Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille Marketing field.';
                }
                field("By Default"; Rec."By Default")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hiérarchie pirncipale field.';
                }
            }
        }
    }

    actions
    {
    }
}

