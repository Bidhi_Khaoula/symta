page 50269 "Item Web Info"
{
    Caption = 'Info web article';
    PageType = CardPart;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(Publiable; Rec.Publiable)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Publiable field.';
            }
            field("Stock Maxi Publiable"; Rec."Stock Maxi Publiable")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Stock Maxi Publiable field.';
            }
        }
    }

    actions
    {
    }
}

