page 50089 "Item Journal Batches MiniLoad"
{
    Caption = 'Item Journal Batches';
    CardPageID = "Item Journal Batches MiniLoad";
    DataCaptionExpression = DataCaption();
    InsertAllowed = false;
    PageType = List;
    SourceTable = "Item Journal Batch";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("No. Series"; Rec."No. Series")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. Series field.';
                }
                field("Posting No. Series"; Rec."Posting No. Series")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting No. Series field.';
                }
                field("Reason Code"; Rec."Reason Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reason Code field.';
                }
                field("Station MiniLoad"; Rec."Station MiniLoad")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Station MiniLoad field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Nouveau)
            {
                Caption = 'Nouveau';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                Image = NewItem;
                ToolTip = 'Executes the Nouveau action.';

                trigger OnAction()
                var
                    CduLFunctions: Codeunit "Codeunits Functions";
                begin

                    rec.Name := CduLFunctions.SetNewBatchMiniLoad();
                    rec.MiniLoad := TRUE;
                    // CFR le 17/09/2020 : WIIO >> la feuille par défaut créée par les utilisateurs ne doit pas avoir les paramètres WIIO
                    rec.Description := '';
                    rec."Type Feuille Reclassement WIIO" := rec."Type Feuille Reclassement WIIO"::" ";
                    // FIN CFR LE 17/09/2020
                    rec.SetupNewBatch();
                    rec.Insert();
                end;
            }
            group("P&osting")
            {
                Caption = 'P&osting';
                action("Test Report")
                {
                    Caption = 'Test Report';
                    Ellipsis = true;
                    Image = TestReport;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Test Report action.';

                    trigger OnAction()
                    begin
                        ReportPrint.PrintItemJnlBatch(Rec);
                    end;
                }
                action("P&ost")
                {
                    Caption = 'P&ost';
                    Image = Post;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    RunObject = Codeunit "Item Jnl.-B.Post";
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the P&ost action.';
                }
                action("Post and &Print")
                {
                    Caption = 'Post and &Print';
                    Image = PostPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    RunObject = Codeunit "Item Jnl.-B.Post+Print";
                    ShortCutKey = 'Shift+F11';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Post and &Print action.';
                }
            }
        }
    }

    trigger OnInit()
    begin
        rec.SETRANGE("Journal Template Name");
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        rec.MiniLoad := TRUE;

        rec.SetupNewBatch();
    end;

    trigger OnOpenPage()
    begin
        ItemJnlMgt.OpenJnlBatch(Rec);


        rec.FILTERGROUP(2);
        rec.SETRANGE(MiniLoad, TRUE);
        rec.FILTERGROUP(0);
    end;

    var
        ReportPrint: Codeunit "Test Report-Print";
        ItemJnlMgt: Codeunit ItemJnlManagement;

    local procedure DataCaption(): Text[250]
    var
        ItemJnlTemplate: Record "Item Journal Template";
    begin
        IF NOT CurrPage.LOOKUPMODE THEN
            IF rec.GETFILTER("Journal Template Name") <> '' THEN
                IF rec.GETRANGEMIN("Journal Template Name") = rec.GETRANGEMAX("Journal Template Name") THEN
                    IF ItemJnlTemplate.GET(rec.GETRANGEMIN("Journal Template Name")) THEN
                        EXIT(ItemJnlTemplate.Name + ' ' + ItemJnlTemplate.Description);
    end;
}

