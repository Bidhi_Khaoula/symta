page 50015 "WhseReceipt"
{
    ApplicationArea = all;
    PageType = StandardDialog;
    Caption = 'Input the Whse. Receipt No.';
    layout
    {
        area(Content)
        {
            field(no_rec_rech; no_rec_rech)
            {
                ApplicationArea = All;
                Caption = 'Whse. Receipt No.';
                ToolTip = 'Specifies the value of the Whse. Receipt No. field.';
            }
        }
    }

    procedure FctGetWhseReceiptNo(): Code[20]
    var
        MgtErrLbl: Label 'Whse. Receipt No. must be filled.';
    begin
        if no_rec_rech = '' then
            Error(MgtErrLbl);
        exit(no_rec_rech);
    end;

    var
        no_rec_rech: Code[20];
}