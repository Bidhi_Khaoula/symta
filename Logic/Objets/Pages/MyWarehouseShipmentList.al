page 50217 "My Warehouse Shipment List"
{
    Caption = 'Warehouse Shipment List';
    CardPageID = "Warehouse Shipment SYMTA";
    DataCaptionFields = "No.";
    Editable = false;
    PageType = List;
    SourceTable = "Warehouse Shipment Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ToolTip = 'Specifies the number of the warehouse shipment header that was created.';
                    ApplicationArea = All;
                }
                field("Location Code"; Rec."Location Code")
                {
                    ToolTip = 'Specifies the code of the location from which the items are being shipped.';
                    ApplicationArea = All;
                }
                field("Packing List No."; Rec."Packing List No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
                field("Assigned User ID"; Rec."Assigned User ID")
                {
                    ToolTip = 'Specifies the ID of the user who is responsible for the document.';
                    ApplicationArea = All;
                }
                field(GetDestinationName; Rec.GetDestinationName())
                {
                    Caption = 'Donneur d''ordre';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Donneur d''ordre field.';
                }
                field("Sorting Method"; Rec."Sorting Method")
                {
                    ToolTip = 'Specifies the method by which the shipments are sorted.';
                    ApplicationArea = All;
                }
                field(Status; Rec.Status)
                {
                    ToolTip = 'Specifies the status of the shipment and is filled in by the program.';
                    ApplicationArea = All;
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ToolTip = 'Specifies the code of the zone on this shipment header.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ToolTip = 'Indicates the bin code to place the items that are about to be shipped.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Document Status"; Rec."Document Status")
                {
                    ToolTip = 'Specifies the progress level of warehouse handling on lines in the warehouse shipment.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ToolTip = 'Specifies a posting date. If you enter a date, the posting date of the source documents is updated during posting.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Assignment Date"; Rec."Assignment Date")
                {
                    ToolTip = 'Specifies the date on which the document was assigned to the user.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Shipment Date"; Rec."Shipment Date")
                {
                    ToolTip = 'Specifies the shipment date of the warehouse shipment.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ToolTip = 'Specifies the codes of the shipping agent being used for this warehouse shipment.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Shipping Agent Service Code"; Rec."Shipping Agent Service Code")
                {
                    ToolTip = 'Specifies the code of the shipping agent service that applies to this warehouse shipment.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ToolTip = 'Specifies the code of the shipment method being used for this shipment.';
                    Visible = false;
                    ApplicationArea = All;
                }
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Destination Type"; Rec."Destination Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination Type field.';
                }
                field("Destination No."; Rec."Destination No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination No. field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Date Création"; Rec."Date Création")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Heure Création"; Rec."Heure Création")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Création field.';
                }
                field("Utilisateur Création"; Rec."Utilisateur Création")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur Création field.';
                }
                field("Date Impression"; Rec."Date Impression")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Impression field.';
                }
                field("Heure Impression"; Rec."Heure Impression")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Impression field.';
                }
                field("Date Flashage"; Rec."Date Flashage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Flashage field.';
                }
                field("Heure Flashage"; Rec."Heure Flashage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Flashage field.';
                }
            }
        }
        area(factboxes)
        {
            systempart(Links; Links)
            {
                Visible = false;
                ApplicationArea = All;
            }
            systempart(Notes; Notes)
            {
                Visible = true;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Shipment")
            {
                Caption = '&Shipment';
                Image = Shipment;
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Warehouse Comment Sheet";
                    RunPageLink = "Table Name" = CONST("Whse. Shipment"),
                                  Type = CONST(" "),
                                  "No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';
                }
                action("Pick Lines")
                {
                    Caption = 'Pick Lines';
                    Image = PickLines;
                    RunObject = Page "Warehouse Activity Lines";
                    RunPageLink = "Whse. Document Type" = CONST(Shipment),
                                  "Whse. Document No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Document No.", "Whse. Document Type", "Activity Type")
                                  WHERE("Activity Type" = CONST(Pick));
                    ApplicationArea = All;
                    ToolTip = 'Executes the Pick Lines action.';
                }
                action("Registered P&ick Lines")
                {
                    Caption = 'Registered P&ick Lines';
                    Image = RegisteredDocs;
                    RunObject = Page "Registered Whse. Act.-Lines";
                    RunPageLink = "Whse. Document No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Document Type", "Whse. Document No.", "Whse. Document Line No.")
                                  WHERE("Whse. Document Type" = CONST(Shipment));
                    ApplicationArea = All;
                    ToolTip = 'Executes the Registered P&ick Lines action.';
                }
                action("Posted &Whse. Shipments")
                {
                    Caption = 'Posted &Whse. Shipments';
                    Image = PostedReceipt;
                    RunObject = Page "Posted Whse. Shipment List";
                    RunPageLink = "Whse. Shipment No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Shipment No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Posted &Whse. Shipments action.';
                }
            }
            group("&Line")
            {
                Caption = '&Line';
                Image = Line;
                action(Card)
                {
                    Caption = 'Card';
                    Image = EditLines;
                    ShortCutKey = 'Shift+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Card action.';

                    trigger OnAction()
                    begin
                        PAGE.RUN(PAGE::"Warehouse Shipment", Rec);
                    end;
                }
            }
        }
        area(processing)
        {
            group("F&unctions")
            {
                Caption = 'F&unctions';
                Image = "Action";
                action("Re&lease")
                {
                    Caption = 'Re&lease';
                    Image = ReleaseDoc;
                    ShortCutKey = 'Ctrl+F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&lease action.';

                    trigger OnAction()
                    var
                        ReleaseWhseShptDoc: Codeunit "Whse.-Shipment Release";
                    begin
                        CurrPage.UPDATE(TRUE);
                        IF Rec.Status = Rec.Status::Open THEN
                            ReleaseWhseShptDoc.Release(Rec);
                    end;
                }
                action("Re&open")
                {
                    Caption = 'Re&open';
                    Image = ReOpen;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&open action.';

                    trigger OnAction()
                    var
                        ReleaseWhseShptDoc: Codeunit "Whse.-Shipment Release";
                    begin
                        ReleaseWhseShptDoc.Reopen(Rec);
                    end;
                }
            }
        }
    }

    trigger OnFindRecord(Which: Text): Boolean
    begin
        EXIT(Rec.FindFirstAllowedRec(Which));
    end;

    trigger OnNextRecord(Steps: Integer): Integer
    begin
        EXIT(Rec.FindNextAllowedRec(Steps));
    end;

    trigger OnOpenPage()
    begin
        Rec.ErrorIfUserIsNotWhseEmployee();
        // MCO Le 13-01-2018
        Rec.SETRANGE("Utilisateur Création", USERID);
        // FIN MCO Le 13-01-2018
    end;
}

