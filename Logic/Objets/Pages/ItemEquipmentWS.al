page 50212 "Item Equipment WS"
{
    Caption = 'Item Equipment';
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Modele / Article";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';

                    trigger OnValidate()
                    begin
                        CurrPage.UPDATE();
                    end;
                }
                field("Equipment No."; Rec."Equipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment No. field.';
                }
                field("Trace No."; Rec."Trace No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Trace No. field.';
                }
                field("Equipment Manufacturer"; Rec."Equipment Manufacturer")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment Manufacturer field.';
                }
                field("Equipment Model"; Rec."Equipment Model")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment Model field.';
                }
                field("Equipment Type"; Rec."Equipment Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment Type field.';
                }
                field("Item No.2"; Rec."Item No.2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No.2 field.';
                }
            }
        }
    }

    actions
    {
    }
}

