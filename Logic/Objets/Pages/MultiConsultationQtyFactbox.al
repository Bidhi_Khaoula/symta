page 50091 "Multi Consultation Qty Factbox"
{
    PageType = CardPart;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Stock physique"; Rec."Conso. Normative")
            {
                Caption = 'Conso. Normative';
                Editable = false;
                Enabled = true;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Conso. Normative field.';
            }
            field(Inventory; Rec.Inventory)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Inventory field.';
            }
            field("Qty. on Assembly Order"; Rec."Qty. on Assembly Order")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Assembly Order field.';
            }
            field("Qty. on Asm. Component"; Rec."Qty. on Asm. Component")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Asm. Component field.';
            }
            field("Qty. on Sales Order"; Rec."Qty. on Sales Order")
            {
                Caption = 'Qty. on Sales Order';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Sales Order field.';
            }
            field("Qty. on Blanket Sales Order"; Rec."Qty. on Blanket Sales Order")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Sales Order field.';
            }
            field("Qty. on Purch. Order"; Rec."Qty. on Purch. Order")
            {
                Caption = 'Qty. on Purch. Order';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Purch. Order field.';
            }
            field("Qty. on Purchase Return"; Rec."Qty. on Purchase Return")
            {
                Caption = 'Qty. on Purch. Order';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Purch. Order field.';
            }
            field("Qty. on Sales Return"; Rec."Qty. on Sales Return")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Qty. on Sales Return field.';
            }
            field(Dispo; Dispo)
            {
                Caption = 'Disponible';
                Editable = false;
                Style = Strong;
                StyleExpr = TRUE;
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Disponible field.';
            }
            field(DispoADate; DispoADate)
            {
                Caption = 'Disponible a date';
                Editable = false;
                Style = Strong;
                StyleExpr = TRUE;
                Visible = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Disponible a date field.';
            }
            field("No."; Rec."No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. field.';
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin

        DispoADate := Rec.CalcAvailability();
        Dispo := Rec.Inventory - Rec."Qty. on Sales Order"
             - Rec."Qty. on Component Lines";
    end;

    var
        DispoADate: Decimal;
        Dispo: Decimal;
}

