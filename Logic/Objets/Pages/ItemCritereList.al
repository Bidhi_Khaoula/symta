page 50244 "Item Critere List"
{
    Caption = 'Critères par article';
    PageType = List;
    SourceTable = "Article Critere";
    SourceTableView = SORTING("Code Article", Priorité);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code Article"; Rec."Code Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Article field.';
                }
                field("Code Critere"; Rec."Code Critere")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Critere field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field(Priorité; Rec.Priorité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Priorité field.';
                }
                field(Text; Rec.Text)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Text field.';
                }
                field(Booléen; Rec.Booléen)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Booléen field.';
                }
                field(Entier; Rec.Entier)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entier field.';
                }
                field(Décimal; Rec.Décimal)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décimal field.';
                }
                field("Super Famille Marketing"; Rec."Super Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Super Famille Marketing field.';
                }
                field("Famille Marketing"; Rec."Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Famille Marketing field.';
                }
                field("Sous Famille Marketing"; Rec."Sous Famille Marketing")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille Marketing field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Modify Date"; Rec."Modify Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modification field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Initialiser)
            {
                Image = Delegate;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Initialiser action.';
                trigger OnAction()
                var
                    LItem: Record Item;
                begin

                    LItem.GET(Rec."Code Article");
                    // MCO Le 16-01-2018 => Appel de la fonction ByHierarchy
                    LItem.ReInitCriteriaByHierarchy('', FALSE);
                    CurrPage.UPDATE(FALSE);
                end;
            }
            action("Mettre à jour")
            {
                Image = UpdateDescription;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Mettre à jour action.';

                trigger OnAction()
                var
                    LItem: Record Item;
                begin
                    LItem.GET(Rec."Code Article");
                    // MCO Le 16-01-2018 => Appel de la fonction ByHierarchy
                    LItem.ReInitCriteriaByHierarchy('', TRUE);
                    CurrPage.UPDATE(FALSE);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        IF NOT Item.GET(Rec."Code Article") THEN
            Item.INIT();
    end;

    trigger OnInit()
    begin

        BtnFiltreVariante := TRUE;
        FiltreVariante := Rec.GETFILTER("Variant Code");
    end;

    var
        Item: Record Item;
        FiltreVariante: Text[30];
        BtnFiltreVariante: Boolean;

}

