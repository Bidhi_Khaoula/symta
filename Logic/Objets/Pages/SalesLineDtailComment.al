page 50011 "Sales Line Détail Comment"
{
    CardPageID = "Sales Line Détail Comment";
    PageType = Card;
    SourceTable = "Sales Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(test; test)
            {
                Caption = 'Cocher tous';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Cocher tous field.';

                trigger OnValidate()
                begin
                    testOnPush();
                end;
            }
            field("Print Quote"; Rec."Print Quote")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Impression devis field.';
            }
            field("Print Order"; Rec."Print Order")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Impression commande field.';
            }
            field("Print Wharehouse Shipment"; Rec."Print Wharehouse Shipment")
            {
                Caption = 'Imprimer sur BP';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Imprimer sur BP field.';
            }
            field("Print Shipment"; Rec."Print Shipment")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Impression expédition field.';
            }
            field("Print Invoice"; Rec."Print Invoice")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Impression facture field.';
            }
            field("Attached to Line No."; Rec."Attached to Line No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Attached to Line No. field.';
            }
        }
    }

    trigger OnOpenPage()
    begin
        test := FALSE;
    end;

    var
        test: Boolean;

    local procedure testOnPush()
    begin
        IF test = FALSE THEN BEGIN
            Rec."Print Order" := FALSE;
            Rec."Print Wharehouse Shipment" := FALSE;
            Rec."Print Shipment" := FALSE;
            Rec."Print Invoice" := FALSE;
            Rec."Display Order" := FALSE;
        END ELSE BEGIN
            Rec."Print Order" := TRUE;
            Rec."Print Wharehouse Shipment" := TRUE;
            Rec."Print Shipment" := TRUE;
            Rec."Print Invoice" := TRUE;
            Rec."Display Order" := TRUE;
        END;
        Rec.Modify();
    end;
}

