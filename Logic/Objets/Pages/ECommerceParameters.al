page 50225 "ECommerce Parameters"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Generals Parameters";
    SourceTableView = SORTING(Type, Code)
                      ORDER(Ascending)
                      WHERE(Type = FILTER('VTE_RET_ABATEMENT | VTE-DOC-RET'));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(Code; Rec.Code)
                {
                    Caption = 'Code format';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code format field.';
                }
                field(LongDescription; Rec.LongDescription)
                {
                    Caption = 'Nom format';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom format field.';
                }
                field(Decimal1; Rec.Decimal1)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Decimal1 field.';
                }
            }
        }
    }

    actions
    {
    }
}

