page 50029 "Purchase Lines to attach"
{
    Caption = 'Purchase Lines';
    CardPageID = "Purchase Lines to attach";
    DelayedInsert = true;
    DeleteAllowed = false;
    Editable = true;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = Document;
    SourceTable = "Purchase Line";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Réf.SYMTA"; lgfacFou."Code Article Origine")
            {
                Caption = 'Réf.SYMTA';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Réf.SYMTA field.';
            }
            field("Code article SYMTA"; gestionREF.RechercheRefActive(lgfacFou."Code article SYMTA"))
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the RechercheRefActive(lgfacFou.Code article SYMTA) field.';
            }
            field("Réf.fournisseur"; lgfacFou."Code article Fournisseur")
            {
                Caption = 'Réf.fournisseur';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Réf.fournisseur field.';
            }
            field("Code article Fournisseur"; gestionREF.RechercheRefActive(lgfacFou."Code article Fournisseur"))
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the RechercheRefActive(lgfacFou.Code article Fournisseur) field.';
            }
            field(Quantité; lgfacFou.Quantité)
            {
                Caption = 'Quantité';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Quantité field.';
            }
            field("Pu brut"; lgfacFou."Pu brut")
            {
                Caption = 'Prix brut';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix brut field.';
            }
            field("Remise 1"; lgfacFou."Remise 1")
            {
                Caption = 'Remise 1';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 1 field.';
            }
            field("Remise 2"; lgfacFou."Remise 2")
            {
                Caption = 'Remise 2';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 2 field.';
            }
            field("Pu net"; lgfacFou."Pu net")
            {
                Caption = 'Prix net';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix net field.';
            }
            field("Total Net"; lgfacFou."Total Net")
            {
                Caption = 'Total Net';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total Net field.';
            }
            repeater(Lines)
            {
                ShowCaption = false;
                field("Qty to attach"; Rec."Qty to attach")
                {
                    Caption = 'Qté à pointer';
                    DecimalPlaces = 0 : 2;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté à pointer field.';

                    trigger OnValidate()
                    begin
                        QtytoattachOnAfterValidate();
                    end;
                }
                field("Qté sur facture"; Rec."Qté sur facture")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté sur facture field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    Caption = 'Quantity';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    Caption = 'Outstanding Quantity';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field("Qty On Receipt Order Manual"; Rec."Qty On Receipt Order Manual")
                {
                    Caption = 'Qté/autre récept°';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    LookupPageID = "Stats Conso";
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté/autre récept° field.';
                }
                field("Qty On Fac Receipt"; Rec."Qty On Fac Receipt")
                {
                    Caption = 'Qté en cours de récept';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté en cours de récept field.';
                }
                field("Qté restante à réceptionner"; Rec."Outstanding Quantity" - (Rec."Qty On Fac Receipt" + Rec."Qty On Receipt Order Manual"))
                {
                    Caption = 'Qté restante à réceptionner';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté restante à réceptionner field.';
                }
                field("Direct Unit Cost"; Rec."Direct Unit Cost")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Direct Unit Cost field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise1 field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise2 field.';
                }
                field("Net Unit Cost"; Rec."Net Unit Cost")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cout unitaire net field.';
                }
                field(Amount; Rec.Amount)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field("Vendor Item No."; Rec."Vendor Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Order Date"; Rec."Order Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field("N° Fac"; Rec."N° Fac")
                {
                    Caption = 'N° Facture';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Facture field.';
                }
                field("Document Type"; Rec."Document Type")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Type field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Buy-from Vendor No."; Rec."Buy-from Vendor No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Buy-from Vendor No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field(Type; Rec.Type)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Editable = false;
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(GetStatutMarchandise; rec.GetStatutMarchandise())
                {
                    Caption = 'Etat Marchandise';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Etat Marchandise field.';
                }
                field(GetDateHeader1; rec.GetDateHeader(1))
                {
                    Caption = 'Date ETD';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date ETD field.';
                }
                field(GetDateHeader2; rec.GetDateHeader(2))
                {
                    Caption = 'Date ETA';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date ETA field.';
                }
                field("Promised Receipt Date"; Rec."Promised Receipt Date")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Promised Receipt Date field.';
                }
                field("Reserved Qty. (Base)"; Rec."Reserved Qty. (Base)")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reserved Qty. (Base) field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Indirect Cost %"; Rec."Indirect Cost %")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Indirect Cost % field.';
                }
                field("Unit Cost (LCY)"; Rec."Unit Cost (LCY)")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost (LCY) field.';
                }
                field("Unit Price (LCY)"; Rec."Unit Price (LCY)")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price (LCY) field.';
                }
                field("Line Amount"; Rec."Line Amount")
                {
                    BlankZero = true;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Amount field.';
                }
                field("Job No."; Rec."Job No.")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Project No. field.';
                }
                field("Job Task No."; Rec."Job Task No.")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Project Task No. field.';
                }
                field("Job Line Type"; Rec."Job Line Type")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Project Line Type field.';
                }
                field("Shortcut Dimension 1 Code"; Rec."Shortcut Dimension 1 Code")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 1 Code field.';
                }
                field("Shortcut Dimension 2 Code"; Rec."Shortcut Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 2 Code field.';
                }
                field(ShortcutDimCode3; ShortcutDimCode[3])
                {
                    CaptionClass = '1,2,3';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[3] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(3, ShortcutDimCode[3]);
                    end;
                }
                field(ShortcutDimCode_4; ShortcutDimCode[4])
                {
                    CaptionClass = '1,2,4';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[4] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(4, ShortcutDimCode[4]);
                    end;
                }
                field(ShortcutDimCode_5; ShortcutDimCode[5])
                {
                    CaptionClass = '1,2,5';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[5] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        Rec.LookupShortcutDimCode(5, ShortcutDimCode[5]);
                    end;
                }
                field(ShortcutDimCode_6; ShortcutDimCode[6])
                {
                    CaptionClass = '1,2,6';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[6] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(6, ShortcutDimCode[6]);
                    end;
                }
                field(ShortcutDimCode_7; ShortcutDimCode[7])
                {
                    CaptionClass = '1,2,7';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[7] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(7, ShortcutDimCode[7]);
                    end;
                }
                field(ShortcutDimCode_8; ShortcutDimCode[8])
                {
                    CaptionClass = '1,2,8';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[8] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(8, ShortcutDimCode[8]);
                    end;
                }
                field("Requested Receipt Date"; rec."Requested Receipt Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Receipt Date field.';
                }
                field("Expected Receipt Date"; rec."Expected Receipt Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Expected Receipt Date field.';
                }
                field("Outstanding Amount (LCY)"; rec."Outstanding Amount (LCY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Amount (LCY) field.';
                }
                field("Vendor Shipment No."; PurchHeader."Vendor Shipment No.")
                {
                    Caption = 'N° BL Fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° BL Fournisseur field.';
                }
                field("Vendor Order No."; PurchHeader."Vendor Order No.")
                {
                    Caption = 'N° Commande fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Commande fournisseur field.';
                }
                field("Amt. Rcd. Not Invoiced (LCY)"; rec."Amt. Rcd. Not Invoiced (LCY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amt. Rcd. Not Invoiced (LCY) field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Line")
            {
                Caption = '&Line';
                action("Show Document")
                {
                    Caption = 'Show Document';
                    Image = View;
                    ShortCutKey = 'Shift+F5';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Show Document action.';

                    trigger OnAction()
                    begin
                        PurchHeader.GET(rec."Document Type", rec."Document No.");
                        CASE rec."Document Type" OF
                            rec."Document Type"::Quote:
                                PAGE.RUN(PAGE::"Purchase Quote", PurchHeader);
                            rec."Document Type"::Order:
                                PAGE.RUN(PAGE::"Purchase Order", PurchHeader);
                            rec."Document Type"::Invoice:
                                PAGE.RUN(PAGE::"Purchase Invoice", PurchHeader);
                            rec."Document Type"::"Return Order":
                                PAGE.RUN(PAGE::"Purchase Return Order", PurchHeader);
                            rec."Document Type"::"Credit Memo":
                                PAGE.RUN(PAGE::"Purchase Credit Memo", PurchHeader);
                            rec."Document Type"::"Blanket Order":
                                PAGE.RUN(PAGE::"Blanket Purchase Order", PurchHeader);
                        END;
                    end;
                }
                action("Reservation Entries")
                {
                    Caption = 'Reservation Entries';
                    Image = ReservationLedger;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Reservation Entries action.';

                    trigger OnAction()
                    begin
                        rec.ShowReservationEntries(TRUE);
                    end;
                }
                action("Item &Tracking Lines")
                {
                    Caption = 'Item &Tracking Lines';
                    Image = ItemTrackingLines;
                    ShortCutKey = 'Shift+Ctrl+I';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Item &Tracking Lines action.';

                    trigger OnAction()
                    begin
                        rec.OpenItemTrackingLines();
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        rec.ShowShortcutDimCode(ShortcutDimCode);
        rec.CALCFIELDS("Qty On Fac Receipt", "Qty On Receipt Order Manual");
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        CLEAR(ShortcutDimCode);
    end;

    var
        PurchHeader: Record "Purchase Header";
        lgfacFou: Record "Import facture tempo. fourn.";
        gestionREF: Codeunit "Gestion Multi-référence";
        ShortcutDimCode: array[8] of Code[20];
        //IsAffected: Decimal;
        Text000Err: Label 'Vous ne pouvez affecter de la quantité que sur une ligne.';
        Affected: Boolean;
        QteFac: Decimal;



    procedure SetQteSurFac(NewQte: Decimal)
    begin
        QteFac := NewQte;
    end;

    procedure SetRecords(PurchLine: Record "Purchase Line")
    var
    //PurchLineBuff: Record "Purchase Line";
    begin
        /*
        DELETEALL;
        PurchLineBuff.COPYFILTERS(PurchLine);
        IF PurchLineBuff.FINDFIRST () THEN
         REPEAT
            Rec:=PurchLineBuff;
            "Qty to attach" :=0;
          IF NOT Rec.INSERT THEN Rec.MODIFY();
         UNTIL PurchLineBuff.NEXT=0;
         */

    end;

    procedure InsertBufferRecord(p_ReqLine: Record "Requisition Line")
    begin
    end;

    procedure SetSelection(PurchLine: Record "Purchase Line")
    begin
        CurrPage.SETSELECTIONFILTER(PurchLine);
    end;

    procedure InitTmpfacFou(_lgfacFou: Record "Import facture tempo. fourn."; var _lgachat: Record "Purchase Line")
    begin
        lgfacFou := _lgfacFou;
        IF _lgachat.FINDFIRST() THEN
            REPEAT
                Rec := _lgachat;
                rec.Insert();
            UNTIL _lgachat.NEXT() = 0;
    end;

    local procedure QtytoattachOnAfterValidate()
    begin
        IF xRec."Qty to attach" = 0 THEN BEGIN
            IF Affected THEN BEGIN
                rec."Qty to attach" := 0;
                MESSAGE(Text000Err);
            END
            ELSE
                Affected := TRUE;
        END
        ELSE
            IF rec."Qty to attach" <> 0 THEN
                Affected := TRUE
            ELSE
                Affected := FALSE;
    end;
}
