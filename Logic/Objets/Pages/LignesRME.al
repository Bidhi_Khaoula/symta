page 50248 "Lignes RME"
{
    PageType = List;
    SourceTable = "Posted Whse. Receipt Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Source Type"; Rec."Source Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Type field.';
                }
                field("Source Subtype"; Rec."Source Subtype")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Subtype field.';
                }
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Source Line No."; Rec."Source Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Line No. field.';
                }
                field("Source Document"; Rec."Source Document")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Document field.';
                }
                field("Date Arrivage Marchandise"; Rec."Date Arrivage Marchandise")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Arrivage Marchandise field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Shelf No."; Rec."Shelf No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shelf No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Vendor Shipment No."; Rec."Vendor Shipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
                }
                field("Whse. Receipt No."; Rec."Whse. Receipt No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Whse. Receipt No. field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field("N° Fusion Réception"; Rec."N° Fusion Réception")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Fusion Réception field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Discount1 % field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Discount2 % field.';
                }
                field("Prix Brut Origine"; Rec."Prix Brut Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut Origine field.';
                }
                field("Litige prix"; Rec."Litige prix")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Litige prix field.';
                }
                field("Référence fournisseur"; Rec."Référence fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence fournisseur field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Qty. (Base)"; Rec."Qty. (Base)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Base) field.';
                }
                field("Qty. Put Away"; Rec."Qty. Put Away")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Put Away field.';
                }
                field("Qty. Put Away (Base)"; Rec."Qty. Put Away (Base)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Put Away (Base) field.';
                }
                field("Put-away Qty."; Rec."Put-away Qty.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Put-away Qty. field.';
                }
                field("Put-away Qty. (Base)"; Rec."Put-away Qty. (Base)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Put-away Qty. (Base) field.';
                }
                field("Qty. per Unit of Measure"; Rec."Qty. per Unit of Measure")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. per Unit of Measure field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Due Date"; Rec."Due Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Due Date field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Qty. Cross-Docked"; Rec."Qty. Cross-Docked")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Cross-Docked field.';
                }
                field("Qty. Cross-Docked (Base)"; Rec."Qty. Cross-Docked (Base)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Cross-Docked (Base) field.';
                }
                field("Cross-Dock Zone Code"; Rec."Cross-Dock Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cross-Dock Zone Code field.';
                }
                field("Cross-Dock Bin Code"; Rec."Cross-Dock Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cross-Dock Bin Code field.';
                }
                field("Posted Source Document"; Rec."Posted Source Document")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posted Source Document field.';
                }
                field("Posted Source No."; Rec."Posted Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posted Source No. field.';
                }
                field("Whse Receipt Line No."; Rec."Whse Receipt Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Whse Receipt Line No. field.';
                }
                field(Status; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field("Serial No."; Rec."Serial No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Serial No. field.';
                }
                field("Lot No."; Rec."Lot No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Lot No. field.';
                }
                field("Warranty Date"; Rec."Warranty Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Warranty Date field.';
                }
                field("Expiration Date"; Rec."Expiration Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Expiration Date field.';
                }
                field("N° sequence facture fourn."; Rec."N° sequence facture fourn.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° sequence facture fourn. field.';
                }
                field("Remise 1 Origine"; Rec."Remise 1 Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 Origine field.';
                }
                field("Net 1 Origine"; Rec."Net 1 Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net 1 Origine field.';
                }
                field("Total Net 1 Origine"; Rec."Total Net 1 Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net 1 Origine field.';
                }
                field("Remise 2 Origine"; Rec."Remise 2 Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 Origine field.';
                }
                field("Net 2 Origine"; Rec."Net 2 Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net 2 Origine field.';
                }
                field("Total Origine"; Rec."Total Origine")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Origine field.';
                }
                field("Qte Reçue théorique"; Rec."Qte Reçue théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qte Reçue théorique field.';
                }
                field("Prix Brut Théorique"; Rec."Prix Brut Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut Théorique field.';
                }
                field("Remise 1 Théorique"; Rec."Remise 1 Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 Théorique field.';
                }
                field("Remise 2 Théorique"; Rec."Remise 2 Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 Théorique field.';
                }
                field("Source Partner No."; Rec."Source Partner No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Partner No. field.';
                }
                field("Source Partner Name"; Rec."Source Partner Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Partner Name field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action(Fiche)
            {
                Promoted = true;
                Image = Document;
                PromotedCategory = New;
                RunObject = Page "Posted Whse. Receipt";
                RunPageLink = "No." = FIELD("No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Fiche action.';
            }
            action(Partners)
            {
                ApplicationArea = All;
                Caption = 'Partenaires tous';
                Visible = false;
                Image = ICPartner;
                ToolTip = 'Executes the Partenaires tous action.';
                trigger OnAction()
                VAR
                    lPostedWhseReceiptLine: Record "Posted Whse. Receipt Line";
                BEGIN
                    IF lPostedWhseReceiptLine.FINDSET() THEN
                        REPEAT
                            lPostedWhseReceiptLine.SetPartner();
                            lPostedWhseReceiptLine.MODIFY();
                        UNTIL lPostedWhseReceiptLine.NEXT() = 0;
                    CurrPage.UPDATE(FALSE);
                    MESSAGE('Terminé : %1', CURRENTDATETIME);
                END;
            }
            action(Partner)
            {
                ApplicationArea = All;
                Caption = 'Partenaire';
                Visible = false;
                Image = ICPartner;
                ToolTip = 'Executes the Partenaire action.';
                trigger OnAction()
                BEGIN
                    Rec.SetPartner();
                    Rec.MODIFY();
                    CurrPage.UPDATE(FALSE);
                END;
            }
        }
    }

    //CFR le 27/09/2023 - R‚gie : ajout des r‚f‚rences au partenaires, champ [Source Partner No.] et [Source Partner Name]
}

