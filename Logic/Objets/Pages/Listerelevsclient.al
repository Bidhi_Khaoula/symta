page 50085 "Liste relevés client"
{
    CardPageID = "Liste relevés client";
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Sales Statment Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Statement No."; Rec."Statement No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Relevé field.';
                }
                field("Customer No."; Rec."Customer No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Client field.';
                }
                field("Statement Date"; Rec."Statement Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date relevé field.';
                }
                field("Due Date"; Rec."Due Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date d''échéance field.';
                }
                field("No. Printed"; Rec."No. Printed")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. Printed field.';
                }
                field("Imprimer Relevé"; Rec."Imprimer Relevé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Relevé field.';
                }
                field("Nb Ecritures"; Rec."Nb Ecritures")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nb Ecritures field.';
                }
                field("Statement Amount"; Rec."Statement Amount")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant relevé field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Rendre Imprimable Oui/non")
            {
                Caption = 'Rendre Imprimable Oui/non';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                Image = PrintCheck;
                ToolTip = 'Executes the Rendre Imprimable Oui/non action.';

                trigger OnAction()
                begin
                    rec."Imprimer Relevé" := NOT rec."Imprimer Relevé";
                    rec.MODIFY();
                end;
            }
            action(Edition)
            {
                Caption = 'Edition';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                Image = Edit;
                ToolTip = 'Executes the Edition action.';

                trigger OnAction()
                begin
                    CLEAR(rec_entete);
                    CLEAR(report_releve);
                    rec_entete := Rec;
                    rec_entete.SETRECFILTER();
                    report_releve.SETTABLEVIEW(rec_entete);
                    report_releve.RUN();
                end;
            }
        }
    }

    var
        report_releve: Report "Sales Statment SYMTA";
        rec_entete: Record "Sales Statment Header";
}

