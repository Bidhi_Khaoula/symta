page 50070 "Suppression Factures temporair"
{
    CardPageID = "Suppression Factures temporair";
    PageType = Card;
    SourceTable = "Company Information";

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Grp)
            {
                ShowCaption = false;
                field(Name; STRSUBSTNO('%1 - %2', rec.Name, Text001Lbl))
                {
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name, Text001Lbl) field.';
                }
                field("CtrlDateDébut"; NoDébut)
                {
                    Caption = 'N° début';
                    LookupPageID = "Sales List";
                    TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Invoice));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° début field.';
                }
                field(Toutes; Toutes)
                {
                    Caption = 'Toutes';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Toutes field.';
                }
                field(CtrlDateFin; NoFin)
                {
                    Caption = 'au';
                    LookupPageID = "Sales List";
                    TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Invoice));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the au field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action(Supprimer_)
                {
                    Caption = 'Supprimer';
                    Image = DeleteXML;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Supprimer action.';

                    trigger OnAction()
                    begin
                        Supprimer();


                        MESSAGE('Fin');
                    end;
                }
            }
        }
    }

    var
        "NoDébut": Code[20];
        NoFin: Code[20];
        Text001Lbl: Label 'Suppression des factures temporaires !';
        Toutes: Boolean;
        ESK002Lbl: Label 'les factures du numéro %1 au numéro %2 ?';
        ESK003Lbl: Label 'toutes les factures ?';
        ESK004Qst: Label 'Voulez vous supprimer';
        ESK005Lbl: Label 'Soit %1 facture(s) !';

    procedure Supprimer()
    var
        LFacture: Record "Sales Header";
        Ltxt: Text[250];
    begin

        CLEAR(LFacture);
        LFacture.SETRANGE("Document Type", LFacture."Document Type"::Invoice);
        IF NOT Toutes THEN
            LFacture.SETRANGE("No.", NoDébut, NoFin);

        IF NOT Toutes THEN
            Ltxt := STRSUBSTNO(ESK002Lbl, NoDébut, NoFin)
        ELSE
            Ltxt := ESK003Lbl;

        IF NOT CONFIRM(ESK004Qst + ' ' + Ltxt + ' ' + STRSUBSTNO(ESK005Lbl, LFacture.COUNT)) THEN
            EXIT;

        LFacture.FINDFIRST();
        REPEAT
            LFacture.DELETE(TRUE);
        UNTIL LFacture.NEXT() = 0;
    end;
}

