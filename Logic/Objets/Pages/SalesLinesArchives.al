page 50109 "Sales Lines Archives"
{
    Caption = 'Sales Lines';
    Editable = false;
    LinksAllowed = false;
    PageType = List;
    SourceTable = "Sales Line Archive";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Document Type"; Rec."Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Type field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(Reserve; Rec.Reserve)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reserve field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise1 field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise2 field.';
                }
                field("Net Unit Price"; Rec."Net Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix unitaire net field.';
                }
                field("Line Amount"; Rec."Line Amount")
                {
                    BlankZero = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Amount field.';
                }
                field("Shipment Date"; Rec."Shipment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Date field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field("Requested Delivery Date"; Rec."Requested Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Delivery Date field.';
                }
                field("Outstanding Qty. (Base)"; Rec."Outstanding Qty. (Base)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Qty. (Base) field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("Référence saisie"; Rec."Référence saisie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence saisie field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Line")
            {
                Caption = '&Line';
                Image = Line;
                action("Show Document")
                {
                    Caption = 'Show Document';
                    Image = View;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'Shift+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Show Document action.';

                    trigger OnAction()
                    begin
                        SalesHeader.GET(Rec."Document Type", Rec."Document No.");
                        CASE Rec."Document Type" OF
                            Rec."Document Type"::Quote:
                                PAGE.RUN(PAGE::"Sales Quote", SalesHeader);
                            Rec."Document Type"::Order:
                                PAGE.RUN(PAGE::"Sales Order", SalesHeader);
                            Rec."Document Type"::Invoice:
                                PAGE.RUN(PAGE::"Sales Invoice", SalesHeader);
                            Rec."Document Type"::"Return Order":
                                PAGE.RUN(PAGE::"Sales Return Order", SalesHeader);
                            Rec."Document Type"::"Credit Memo":
                                PAGE.RUN(PAGE::"Sales Credit Memo", SalesHeader);
                            Rec."Document Type"::"Blanket Order":
                                PAGE.RUN(PAGE::"Blanket Sales Order", SalesHeader);
                        END;
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        LItem: Record Item;
    begin

        dec_reserve := 0;
        dec_attendu := 0;
        Dec_Stock := 0;
        // AD Le 02-11-2015
        IF LItem.GET(Rec."No.") THEN BEGIN
            LItem.CalcAttenduReserve(dec_reserve, dec_attendu);
            Dec_Stock := LItem.Inventory;
        END;
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        CLEAR(ShortcutDimCode);
    end;

    var
        SalesHeader: Record "Sales Header";
        ShortcutDimCode: array[8] of Code[20];
        dec_reserve: Decimal;
        dec_attendu: Decimal;
        Dec_Stock: Decimal;
}

