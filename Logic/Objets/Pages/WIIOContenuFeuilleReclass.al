page 50308 "WIIO - Contenu Feuille Reclass"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Journal Line";
    SourceTableView = SORTING("Journal Template Name", "Journal Batch Name", "Line No.")
                      WHERE("Journal Template Name" = CONST('RECLASS'));

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Journal Batch Name"; Rec."Journal Batch Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Journal Batch Name field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Ref Active"; Rec."Ref. Active")
                {
                    Caption = 'Ref Active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref Active field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("New Bin Code"; Rec."New Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Bin Code field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Post Picking"; Feuille."Station MiniLoad")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Station MiniLoad field.';
                }
                field("Poids total"; GetPoidsTotal())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetPoidsTotal() field.';
                }
                field("Box Type"; GetBoxTypee())
                {
                    Caption = 'Type de box';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de box field.';
                }
                field("A Valider"; Rec."A Valider")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the A Valider field.';
                }
                field(Commentaire; Rec.Commentaire)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        Feuille.GET(Rec."Journal Template Name", Rec."Journal Batch Name");
    end;

    var
        Feuille: Record "Item Journal Batch";
        rec_Item: Record Item;

    procedure GetPoidsTotal(): Decimal
    begin
        IF rec_Item.GET(Rec."Item No.") THEN
            EXIT(Rec.Quantity * rec_Item."Net Weight");
    end;

    procedure GetBoxTypee(): Code[20]
    begin
        IF rec_Item.GET(Rec."Item No.") THEN
            EXIT(FORMAT(rec_Item."Box Type"));
    end;
}

