page 50304 "WIIO - Liste Colis PackingList"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Packing List Package";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Packing List No."; Rec."Packing List No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
                field("Package No."; Rec."Package No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° colis field.';
                }
                field(Length; Rec.Length)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Longueur (cm) field.';
                }
                field(Width; Rec.Width)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Largeur (cm) field.';
                }
                field(Height; Rec.Height)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hauteur (cm) field.';
                }
                field("Package Weight"; Rec."Package Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids brut field.';
                }
                field("Comment 1"; Rec."Comment 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire 1 field.';
                }
                field("Package Quantity"; Rec."Package Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité dans colis field.';
                }
                field("Package Type"; Rec."Package Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de colis field.';
                }
            }
        }
    }
}

