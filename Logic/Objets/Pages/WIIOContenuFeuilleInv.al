page 50318 "WIIO - Contenu Feuille Inv."
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Journal Line";
    SourceTableView = SORTING("Journal Template Name", "Journal Batch Name", "Line No.")
                      WHERE("Journal Template Name" = CONST('INV. PHYS.'),
                            "Item No." = FILTER(<> ''));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Journal Batch Name"; Rec."Journal Batch Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Journal Batch Name field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Ref Active"; Rec."Ref. Active")
                {
                    Caption = 'Ref Active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref Active field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Qty. (Calculated)"; Rec."Qty. (Calculated)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Calculated) field.';
                }
                field("Qty. (Phys. Inventory)"; Rec."Qty. (Phys. Inventory)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Phys. Inventory) field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Entry Type"; Rec."Entry Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entry Type field.';
                }
                field("Salespers./Purch. Code"; Rec."Salespers./Purch. Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salespers./Purch. Code field.';
                }
                field("Stock Depot"; gStock - Rec."Qty. (Calculated)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Calculated) field.';
                }
                field("Unit Amount"; Rec."Unit Amount")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Amount field.';
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field(WIIO_LastPickingInventory; Rec.WIIO_LastPickingInventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the WIIO_LastPickingInventory field.';
                }
                field(WIIO_LastStaticInventory; Rec.WIIO_LastStaticInventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the WIIO_LastStaticInventory field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord();
    var
        lItem: Record Item;
    begin
        Feuille.GET(Rec."Journal Template Name", Rec."Journal Batch Name");

        // Calcul d'un FLOWFIELD
        gStock := 0;
        IF lItem.GET(Rec."Item No.") THEN BEGIN
            lItem.CALCFIELDS(Inventory);
            gStock := lItem.Inventory;
        END;
    end;

    var
        Feuille: Record "Item Journal Batch";
        //  rec_Item: Record Item;
        gStock: Decimal;
}

