page 50062 "Sales Quote Subform WEB"
{
    ApplicationArea = all;
    AutoSplitKey = true;
    Caption = 'Sales Quote Subform';
    CardPageID = "Sales Quote Subform WEB";
    DelayedInsert = true;
    LinksAllowed = false;
    MultipleNewLines = true;
    PageType = ListPart;
    SourceTable = "Sales Line";
    SourceTableView = WHERE("Document Type" = FILTER(Quote));

    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Gerer par groupement"; Rec."Gerer par groupement")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gerer par groupement field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';

                    trigger OnValidate()
                    begin
                        TypeOnAfterValidate();
                    end;
                }
                field("Sans mouvement de stock"; Rec."Sans mouvement de stock")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Not stored field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("Référence saisie"; Rec."Référence saisie")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence saisie field.';
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';

                    trigger OnValidate()
                    begin
                        Rec.ShowShortcutDimCode(ShortcutDimCode);
                        NoOnAfterValidate();
                    end;
                }
                field("Line type"; Rec."Line type")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type ligne field.';

                    trigger OnValidate()
                    begin
                        LinetypeOnAfterValidate();
                    end;
                }
                field("Reference No."; Rec."Item Reference No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Reference No. field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        ItemReferenceMgt: Codeunit "Item Reference Management";
                    begin
                        SalesHeader.GET(rec."Document Type", rec."Document No.");
                        ItemReferenceMgt.SalesReferenceNoLookUp(Rec, SalesHeader);
                        InsertExtendedText(FALSE);
                    end;

                    trigger OnValidate()
                    begin
                        CrossReferenceNoOnAfterValidat();
                    end;
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Substitution Available"; Rec."Substitution Available")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Substitution Available field.';
                }
                field(Nonstock; Rec.Nonstock)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Catalog field.';
                }
                field("VAT Prod. Posting Group"; Rec."VAT Prod. Posting Group")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the VAT Prod. Posting Group field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code zone field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    BlankZero = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';

                    trigger OnValidate()
                    begin
                        QuantityOnAfterValidate();
                    end;
                }
                field("Qty. to Ship"; Rec."Qty. to Ship")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Ship field.';
                }
                field("Unit Volume"; Rec."Unit Volume")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Volume field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';

                    trigger OnValidate()
                    begin
                        UnitofMeasureCodeOnAfterValida();
                    end;
                }
                field("Unit of Measure"; Rec."Unit of Measure")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure field.';
                }
                field("Unit Cost (LCY)"; Rec."Unit Cost (LCY)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost (LCY) field.';
                }
                field(PriceExists; rec.PriceExists())
                {
                    Caption = 'Sales Price Exists';
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Price Exists field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    BlankZero = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise1 field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise2 field.';
                }
                field("Net Unit Price"; Rec."Net Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix unitaire net field.';
                }
                field("Line Amount"; Rec."Line Amount")
                {
                    BlankZero = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Amount field.';
                }
                field(LineDiscExists; Rec.LineDiscExists())
                {
                    Caption = 'Sales Line Disc. Exists';
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Line Disc. Exists field.';
                }
                field("Line Discount %"; Rec."Line Discount %")
                {
                    BlankZero = true;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount % field.';
                }
                field("Line Discount Amount"; Rec."Line Discount Amount")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount Amount field.';
                }
                field("Allow Invoice Disc."; Rec."Allow Invoice Disc.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Allow Invoice Disc. field.';
                }
                field("Allow Item Charge Assignment"; Rec."Allow Item Charge Assignment")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Allow Item Charge Assignment field.';
                }
                field("Qty. to Assign"; Rec."Qty. to Assign")
                {
                    BlankZero = true;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Assign field.';

                    trigger OnDrillDown()
                    begin
                        CurrPage.SAVERECORD();
                        Rec.ShowItemChargeAssgnt();
                        UpdateForm(FALSE);
                    end;
                }
                field("Qty. Assigned"; Rec."Qty. Assigned")
                {
                    BlankZero = true;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Assigned field.';

                    trigger OnDrillDown()
                    begin
                        CurrPage.SAVERECORD();
                        Rec.ShowItemChargeAssgnt();
                        UpdateForm(FALSE);
                    end;
                }
                field("Work Type Code"; Rec."Work Type Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Work Type Code field.';
                }
                field("Blanket Order No."; Rec."Blanket Order No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Blanket Order No. field.';
                }
                field("Blanket Order Line No."; Rec."Blanket Order Line No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Blanket Order Line No. field.';
                }
                field("Appl.-to Item Entry"; Rec."Appl.-to Item Entry")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Appl.-to Item Entry field.';
                }
                field("Shortcut Dimension 1 Code"; Rec."Shortcut Dimension 1 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 1 Code field.';
                }
                field("Shortcut Dimension 2 Code"; Rec."Shortcut Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 2 Code field.';
                }
                field(ShortcutDimCode_3; ShortcutDimCode[3])
                {
                    CaptionClass = '1,2,3';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[3] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(3, ShortcutDimCode[3]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(3, ShortcutDimCode[3]);
                    end;
                }
                field(ShortcutDimCode_4; ShortcutDimCode[4])
                {
                    CaptionClass = '1,2,4';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[4] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(4, ShortcutDimCode[4]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(4, ShortcutDimCode[4]);
                    end;
                }
                field(ShortcutDimCode_5; ShortcutDimCode[5])
                {
                    CaptionClass = '1,2,5';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[5] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(5, ShortcutDimCode[5]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(5, ShortcutDimCode[5]);
                    end;
                }
                field(ShortcutDimCode_6; ShortcutDimCode[6])
                {
                    CaptionClass = '1,2,6';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[6] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(6, ShortcutDimCode[6]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(6, ShortcutDimCode[6]);
                    end;
                }
                field(ShortcutDimCode_7; ShortcutDimCode[7])
                {
                    CaptionClass = '1,2,7';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[7] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(7, ShortcutDimCode[7]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(7, ShortcutDimCode[7]);
                    end;
                }
                field(ShortcutDimCode_8; ShortcutDimCode[8])
                {
                    CaptionClass = '1,2,8';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[8] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(8, ShortcutDimCode[8]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(8, ShortcutDimCode[8]);
                    end;
                }
                field(GetGencod_; GetGencod())
                {
                    Caption = 'Gencod';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gencod field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group("&Line")
            {
                Caption = '&Line';
                Image = Line;
                group("Item Availability by")
                {
                    Caption = 'Item Availability by';
                    Image = ItemAvailability;
                    action("Event")
                    {
                        Caption = 'Event';
                        Image = "Event";
                        ApplicationArea = All;
                        ToolTip = 'Executes the Event action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec, ItemAvailFormsMgt.ByEvent())
                        end;
                    }
                    action(Period)
                    {
                        Caption = 'Period';
                        Image = Period;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Period action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec, ItemAvailFormsMgt.ByPeriod())
                        end;
                    }
                    action(Variant)
                    {
                        Caption = 'Variant';
                        Image = ItemVariant;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Variant action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec, ItemAvailFormsMgt.ByVariant())
                        end;
                    }
                    action(Location)
                    {
                        Caption = 'Location';
                        Image = Warehouse;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Location action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec, ItemAvailFormsMgt.ByLocation())
                        end;
                    }
                    action("BOM Level")
                    {
                        Caption = 'BOM Level';
                        Image = BOMLevel;
                        ApplicationArea = All;
                        ToolTip = 'Executes the BOM Level action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec, ItemAvailFormsMgt.ByBOM())
                        end;
                    }
                }
                action("Select Item Substitution")
                {
                    Caption = 'Select Item Substitution';
                    Image = SelectItemSubstitution;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Select Item Substitution action.';

                    trigger OnAction()
                    begin
                        Show_ItemSub();
                    end;
                }
                action(Dimensions)
                {
                    Caption = 'Dimensions';
                    Image = Dimensions;
                    ShortCutKey = 'Shift+Ctrl+D';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Dimensions action.';

                    trigger OnAction()
                    begin
                        rec.ShowDimensions();
                    end;
                }
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';

                    trigger OnAction()
                    begin
                        ShowLineComment();
                    end;
                }
                action("Item Charge &Assignment")
                {
                    Caption = 'Item Charge &Assignment';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Item Charge &Assignment action.';

                    trigger OnAction()
                    begin
                        ItemChargeAssgnt();
                    end;
                }
                action("Item &Tracking Lines")
                {
                    Caption = 'Item &Tracking Lines';
                    Image = ItemTrackingLines;
                    ShortCutKey = 'Shift+Ctrl+I';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Item &Tracking Lines action.';

                    trigger OnAction()
                    var
                        Item: Record Item;
                    begin
                        Item.GET(rec."No.");
                        Item.TESTFIELD("Assembly Policy", Item."Assembly Policy"::"Assemble-to-Stock");
                        rec.TESTFIELD("Qty. to Asm. to Order (Base)", 0);
                        rec.OpenItemTrackingLines();
                    end;
                }
                group("Assemble to Order")
                {
                    Caption = 'Assemble to Order';
                    Image = AssemblyBOM;
                    action("Assemble-to-Order Lines")
                    {
                        Caption = 'Assemble-to-Order Lines';
                        ApplicationArea = All;
                        ToolTip = 'Executes the Assemble-to-Order Lines action.';

                        trigger OnAction()
                        begin
                            rec.ShowAsmToOrderLines();
                        end;
                    }
                    action("Roll Up &Price")
                    {
                        Caption = 'Roll Up &Price';
                        Ellipsis = true;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Roll Up &Price action.';

                        trigger OnAction()
                        begin
                            rec.RollupAsmPrice();
                        end;
                    }
                    action("Roll Up &Cost")
                    {
                        Caption = 'Roll Up &Cost';
                        Ellipsis = true;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Roll Up &Cost action.';

                        trigger OnAction()
                        begin
                            rec.RollUpAsmCost();
                        end;
                    }
                }
            }
            group("F&unctions")
            {
                Caption = 'F&unctions';
                Image = "Action";
                action("Get &Price")
                {
                    Caption = 'Get &Price';
                    Ellipsis = true;
                    Image = Price;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Get &Price action.';

                    trigger OnAction()
                    begin
                        ShowPrices()
                    end;
                }
                action("Get Li&ne Discount")
                {
                    Caption = 'Get Li&ne Discount';
                    Ellipsis = true;
                    Image = LineDiscount;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Get Li&ne Discount action.';

                    trigger OnAction()
                    begin
                        ShowLineDisc()
                    end;
                }
                action("E&xplode BOM")
                {
                    Caption = 'E&xplode BOM';
                    Image = ExplodeBOM;
                    ApplicationArea = All;
                    ToolTip = 'Executes the E&xplode BOM action.';

                    trigger OnAction()
                    begin
                        ExplodeBOM();
                    end;
                }
                action("Insert &Ext. Texts")
                {
                    Caption = 'Insert &Ext. Texts';
                    Image = Text;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Insert &Ext. Texts action.';

                    trigger OnAction()
                    begin
                        InsertExtendedText(TRUE);
                    end;
                }
                action("Nonstoc&k Items")
                {
                    Caption = 'Nonstoc&k Items';
                    Image = NonStockItem;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Nonstoc&k Items action.';

                    trigger OnAction()
                    begin
                        ShowNonstockItems();
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        rec.ShowShortcutDimCode(ShortcutDimCode);
        rec_item.INIT();
        IF rec_item.GET(rec."No.") THEN;
    end;

    trigger OnDeleteRecord(): Boolean
    var
        ReserveSalesLine: Codeunit "Sales Line-Reserve";
    begin
        IF (rec.Quantity <> 0) AND rec.ItemExists(rec."No.") THEN BEGIN
            COMMIT();
            IF NOT ReserveSalesLine.DeleteLineConfirm(Rec) THEN
                EXIT(FALSE);
            ReserveSalesLine.DeleteLine(Rec);
        END;
    end;

    trigger OnInit()
    begin
        ItemPanelVisible := TRUE;
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        // AD Le 21-10-2009 => Article par defaut pour simplifier la saisie
        // Type := xRec.Type;
        IF xRec.Type IN [xRec.Type::" ", xRec.Type::Item] THEN
            rec.Type := xRec.Type
        ELSE
            rec.Type := rec.Type::Item;
        // FIN AD Le 21-10-2009

        rec_item.INIT();

        CLEAR(ShortcutDimCode);
    end;

    var
        SalesHeader: Record "Sales Header";
        rec_item: Record Item;
        TransferExtendedText: Codeunit "Transfer Extended Text";
        SalesPriceCalcMgt: Codeunit "Sales Price Calc. Mgt.";
        ItemAvailFormsMgt: Codeunit "Item Availability Forms Mgt";
        ShortcutDimCode: array[8] of Code[20];

        ItemPanelVisible: Boolean;


    procedure ApproveCalcInvDisc()
    begin
        CODEUNIT.RUN(CODEUNIT::"Sales-Disc. (Yes/No)", Rec);
    end;

    procedure CalcInvDisc()
    begin
        CODEUNIT.RUN(CODEUNIT::"Sales-Calc. Discount", Rec);
    end;

    procedure ExplodeBOM()
    begin
        CODEUNIT.RUN(CODEUNIT::"Sales-Explode BOM", Rec);
    end;

    procedure InsertExtendedText(Unconditionally: Boolean)
    begin
        IF TransferExtendedText.SalesCheckIfAnyExtText(Rec, Unconditionally) THEN BEGIN
            CurrPage.SAVERECORD();
            TransferExtendedText.InsertSalesExtText(Rec);
        END;
        IF TransferExtendedText.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure ItemChargeAssgnt()
    begin
        Rec.ShowItemChargeAssgnt();
    end;

    procedure UpdateForm(SetSaveRecord: Boolean)
    begin
        CurrPage.UPDATE(SetSaveRecord);
    end;

    procedure "--- ESKAPE ---"()
    begin
    end;

    procedure InsertItemCharge()
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Insertion des charges associées
        CurrPage.SAVERECORD();
        IF TransferItemCharge.SalesCheckIfAnyLink(Rec) THEN
            TransferItemCharge.InsertSalesLink(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure UpdateItemCharge()
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Modification des qtes de charges associées

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateQtySalesLink(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure UpdateLineType()
    var
        TransferItemCharge: Codeunit "Transfer Item Charge";
    begin
        // AD Le 30-10-2006 => DEEE -> Modification du type de ligne

        CurrPage.SAVERECORD();
        TransferItemCharge.UpdateLineType(Rec);
        IF TransferItemCharge.MakeUpdate() THEN
            UpdateForm(TRUE);
    end;

    procedure DetailLigne()
    begin
        // AD Le 12-12-2008 => Ouverture de l'écran détaillé

        IF rec.Type = rec.Type::" " THEN
            PAGE.RUNMODAL(PAGE::"Sales Line Détail Comment", Rec)
        ELSE
            PAGE.RUNMODAL(PAGE::"Sales Line Détail", Rec);
    end;

    procedure InsertPort()
    var
        Type: Option Port,Emballage;
    begin
        // AD Le 31-08-2009 => Gestion des frais de port
        rec.InsertPortLine(Type::Port);
    end;

    procedure GetGencod(): Text[13]
    var
        LRec_Item: Record Item;
    begin
        IF LRec_Item.GET(rec."No.") THEN
            EXIT(LRec_Item."Gencod EAN13")
        ELSE
            EXIT('');
    end;

    local procedure TypeOnAfterValidate()
    begin
        ItemPanelVisible := rec.Type = rec.Type::Item;
    end;

    local procedure NoOnAfterValidate()
    begin
        InsertExtendedText(FALSE);

        // AD Le 30-10-2006 => DEEE
        InsertItemCharge();


        IF (rec.Type = rec.Type::"Charge (Item)") AND (rec."No." <> xRec."No.") AND
           (xRec."No." <> '')
        THEN
            CurrPage.SAVERECORD();
    end;

    local procedure LinetypeOnAfterValidate()
    begin
        // AD Le 30-10-2006 => DEEE
        UpdateLineType();
        // FIN AD Le 30-10-2006
    end;

    local procedure CrossReferenceNoOnAfterValidat()
    begin
        InsertExtendedText(FALSE);
    end;

    local procedure BuildKitOnAfterValidate()
    begin
        CurrPage.SAVERECORD();
    end;

    local procedure QuantityOnAfterValidate()
    begin
        // AD Le 30-10-2006 => DEEE
        UpdateItemCharge();


        IF rec.Reserve = rec.Reserve::Always THEN BEGIN
            CurrPage.SAVERECORD();
            rec.AutoReserve();
        END;
    end;

    local procedure UnitofMeasureCodeOnAfterValida()
    begin
        IF rec.Reserve = rec.Reserve::Always THEN BEGIN
            CurrPage.SAVERECORD();
            rec.AutoReserve();
        END;
    end;

    procedure ShowPrices()
    begin
        SalesHeader.GET(rec."Document Type", rec."Document No.");
        CLEAR(SalesPriceCalcMgt);
        SalesPriceCalcMgt.GetSalesLinePrice(SalesHeader, Rec);
    end;

    procedure ShowLineDisc()
    begin
        SalesHeader.GET(rec."Document Type", rec."Document No.");
        CLEAR(SalesPriceCalcMgt);
        SalesPriceCalcMgt.GetSalesLineLineDisc(SalesHeader, Rec);
    end;

    procedure ShowLineComment()
    begin
        Rec.ShowLineComments();
    end;

    procedure Show_ItemSub()
    begin
        Rec.ShowItemSub();
    end;

    procedure ShowNonstockItems()
    begin
        rec.ShowNonstock();
    end;
}

