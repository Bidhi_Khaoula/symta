page 50064 "Export des factures EDI"
{
    CardPageID = "Export des factures EDI";
    PageType = Card;
    SourceTable = "Company Information";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(grp)
            {
                ShowCaption = false;
                field("Name"; STRSUBSTNO('%1 - %2', rec.Name, Text001Lbl))
                {
                    ApplicationArea = All;
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ToolTip = 'Specifies the value of the Name, Text001Lbl) field.';
                }
                field(CtrlTypeExportation; TypeExportation)
                {
                    Caption = 'Type exportation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type exportation field.';

                    trigger OnValidate()
                    begin
                        TypeExportationOnAfterValidate();
                    end;
                }
                field(CtrlCodeExportation; CodeExportation)
                {
                    Caption = 'Code Exportation';
                    LookupPageID = "List Parameters";
                    TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Exportation field.';
                }
                field("CtrlDateDébut"; DateDébut)
                {
                    Caption = 'Date début';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date début field.';
                }
                field(CtrlDateFin; DateFin)
                {
                    Caption = 'au';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the au field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action(SCAR)
                {
                    Caption = 'SCAR';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    Image = Export;
                    ToolTip = 'Executes the SCAR action.';

                    trigger OnAction()
                    var
                        // ExporInvoiceEdiSCAR: Codeunit "Export Factures SCAR";
                        ExporInvoiceEdi: Codeunit "Export Factures EDI";
                    begin
                        // Exportation des factures et des avoirs.
                        //IF UPPERCASE(USERID) = 'MTHOUREL' THEN

                        // AD Le 29-04-2020 => Tous dans le même CU
                        //ExporInvoiceEdiSCAR.ExportInvoiceEDI(TypeExportation,DateDébut,DateFin,CodeExportation);
                        ExporInvoiceEdi.ExportInvoiceEDI(TypeExportation, DateDébut, DateFin, 'FAC_SCAR');

                        MESSAGE('Fin');
                    end;
                }
                action("Générer")
                {
                    Caption = 'Générer';
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    Image = CoupledSalesInvoice;
                    ToolTip = 'Executes the Générer action.';

                    trigger OnAction()
                    var
                        ExporInvoiceEdi: Codeunit "Export Factures EDI";
                    begin

                        // Exportation des factures et des avoirs.
                        //IF UPPERCASE(USERID) = 'MTHOUREL' THEN

                        ExporInvoiceEdi.ExportInvoiceEDI(TypeExportation, DateDébut, DateFin, CodeExportation);

                        MESSAGE('Fin');
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        AffichageChamps();
    end;

    var
        TypeExportation: Option "Génération des factures","Re-générer des factures",Maintenance;
        "DateDébut": Date;
        DateFin: Date;
        CodeExportation: Code[10];
        Text001Lbl: Label 'Génération des factures EDI';

    procedure AffichageChamps()
    begin
        /*
        CurrForm.CtrlDateDébut.EDITABLE := TypeExportation <> TypeExportation::"Génération des factures";
        CurrForm.CtrlDateFin.EDITABLE := TypeExportation <> TypeExportation::"Génération des factures";
        CurrForm.CtrlCodeExportation.EDITABLE := TypeExportation <> TypeExportation::"Génération des factures";
        */

    end;

    local procedure TypeExportationOnAfterValidate()
    begin
        AffichageChamps();
    end;
}

