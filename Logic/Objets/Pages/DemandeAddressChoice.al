page 50331 "Demande Address Choice"
{

    Caption = 'Choix adresse';
    Editable = false;
    LinksAllowed = false;
    PageType = List;
    SourceTable = "Order Address";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Address; Rec.Address)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address field.';
                }
                field("Address 2"; Rec."Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address 2 field.';
                }
                field("Post Code"; Rec."Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Post Code field.';
                }
                field(City; Rec.City)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the City field.';
                }
                field("Country/Region Code"; Rec."Country/Region Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Country/Region Code field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure InitVendor(pVendorNo: Code[20]);
    var
        LRecAddress: Record "Order Address";
        Vendor: Record Vendor;
    begin
        // adresse principale
        Vendor.GET(pVendorNo);
        Rec.INIT();
        Rec.Code := '';
        Rec.Address := Vendor.Address;
        Rec."Address 2" := Vendor."Address 2";
        Rec."Post Code" := Vendor."Post Code";
        Rec.City := Vendor.City;
        Rec."Country/Region Code" := Vendor."Country/Region Code";
        Rec.Insert();

        // adresses d'enlevement
        LRecAddress.SETRANGE("Vendor No.", pVendorNo);
        LRecAddress.FINDSET();
        REPEAT
            Rec.TRANSFERFIELDS(LRecAddress);
            Rec.Insert();
        UNTIL LRecAddress.NEXT() = 0;
    end;
}

