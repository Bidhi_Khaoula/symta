page 50310 "WIIO - MultiConsultation"
{

    ApplicationArea = All;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    SourceTable = Item;
    PageType = List;
    ShowFilter = false;
    layout
    {
        area(Content)
        {

            field(QteDemande; QteDemande)
            {
                Caption = 'QteDemande';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the QteDemande field.';

            }
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Ref_Active; Rec."No. 2")
                {
                    Caption = 'No. 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Code Zone_Emplacement"; CodeZone + ' / ' + CodeEmplacement)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CodeZone + '' / '' + CodeEmplacement field.';
                }
                field("Stock Mort"; Rec."Stock Mort")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Mort field.';
                }
                field(Inventory; Rec.Inventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Inventory field.';
                }
                field("Net Weight"; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
                field("Date Dernière Sortie"; Rec."Date Dernière Sortie")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Dernière Sortie field.';
                }

                field("Date dernière entrée"; Rec."Date dernière entrée")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dernière entrée field.';
                }
                field("Sales multiple"; Rec."Sales multiple")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple de vente field.';
                }
                field("Qty. on Sales Order"; Rec."Qty. on Sales Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. on Sales Order field.';
                }
                field("Qty. on Blanket Sales Order"; Rec."Qty. on Blanket Sales Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. on Sales Order field.';
                }
                field("Qty. on Purch. Order"; Rec."Qty. on Purch. Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. on Purch. Order field.';
                }
                field("Qty. on Purchase Return"; Rec."Qty. on Purchase Return")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. on Purch. Order field.';
                }
                field("Qty. on Sales Return"; Rec."Qty. on Sales Return")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. on Sales Return field.';
                }
                field(Dispo; Dispo)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo field.';
                }
                field(DispoADate; DispoADate)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the DispoADate field.';
                }
                field(Prix; Prix)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;
                ToolTip = 'Executes the ActionName action.';

                trigger OnAction()
                begin

                end;
            }
        }
    }
    trigger OnAfterGetRecord()
    var
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        // Filte sur le magasin par defaut
        CompanyInfo.GET();

        Rec.SETRANGE("Location Filter", CompanyInfo."Location Code");

        // Empalcement par defaut
        CodeEmplacement := '';
        CodeZone := '';
        CduLFunctions.ESK_GetDefaultBin(Rec."No.", '', CompanyInfo."Location Code", CodeEmplacement, CodeZone);

        // Dipsonibles
        DispoADate := Rec.CalcAvailability();
        Dispo := Rec.Inventory - Rec."Qty. on Sales Order"
              - Rec."Qty. on Component Lines";

        calcul_tarif();
    end;

    var
        CompanyInfo: Record "Company Information";
        CodeEmplacement: Code[20];
        CodeZone: Code[10];
        // WMSMngt: Codeunit "WMS Management";
        Dispo: Decimal;
        DispoADate: Decimal;
        QteDemande: Decimal;
        Prix: Decimal;
        remise1: Decimal;
        remise2: Decimal;
        remise: Decimal;
        TypeCdeVente: Integer;

    PROCEDURE calcul_tarif();
    VAR

        TempSalesPrice: Record "Sales Price" TEMPORARY;
        TempSalesLineDisc: Record "Sales Line Discount" TEMPORARY;
        /*  "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
         TempPurchPrice: Record "Purchase Price" TEMPORARY;
         TempPurchLineDisc: Record "Purchase Line Discount" TEMPORARY;
         PurchPrice: Record "Purchase Price" TEMPORARY; */
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
    BEGIN
        // Remplissage des prix de base
        Prix := Rec."Unit Price";
        //prix_four := "Last Direct Cost";
        remise1 := 0;
        remise2 := 0;
        //ERROR('affiche tarif' + client_demande + '*' + FORMAT(prix));
        remise1 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, QteDemande, 1,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        remise2 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, QteDemande, 2,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        IF NOT LCodeunitsFunctions.GetUnitPrice(
                  TempSalesPrice, '', '', '',
                  '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, QteDemande, remise, Prix,
                  '', '', TypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
        THEN BEGIN
            Prix := Rec."Unit Price";
        END
        // MC Le 25-10-2011 => Gestion des prix catalogues
    END;

}