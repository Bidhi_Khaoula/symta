page 50326 "Demande Transporteur List"
{
    Caption = 'Demandes Transporteur';
    CardPageID = "Demande Transporteur Card";
    PageType = List;
    SourceTable = "Demande transporteur";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Demande field.';
                }
                field(Statut; Rec.Statut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("Created At"; Rec."Created At")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Date Départ"; Rec."Date Départ")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date départ prévue field.';
                }
                field("Expected Delivery Date"; Rec."Expected Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison prévue field.';
                }
                field("Delivery Date"; Rec."Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison field.';
                }
                field(gPartnerNo; gPartnerNo)
                {
                    Caption = 'Code partenaire';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code partenaire field.';
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field(Facturé; Rec.Facturé)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Facturé field.';
                }
                field("Shipping Vendor Name"; Rec."Shipping Vendor Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom transporteur field.';
                }
                field(GetCoutGlobal; Rec.GetCoutGlobal())
                {
                    Caption = 'Coût global';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coût global field.';
                }
                field("Ship-to Country/Region Code"; Rec."Ship-to Country/Region Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Country/Region Code field.';
                }
                field("Valeur transport vendu"; Rec."Valeur transport vendu")
                {
                    ApplicationArea = All;
                    Visible = false;
                    ToolTip = 'Specifies the value of the Valeur transport vendu HT field.';
                }
                field(GetValueVente; Rec.GetValueVente())
                {
                    ApplicationArea = All;
                    Visible = false;
                    Caption = 'Valeur Vente HT';
                    ToolTip = 'Specifies the value of the Valeur Vente HT field.';
                }
                field(GetValueAchat; Rec.GetValueAchat())
                {
                    ApplicationArea = All;
                    Visible = false;
                    Caption = 'Valeur Achat HT';
                    ToolTip = 'Specifies the value of the Valeur Achat HT field.';
                }
                field("Nombre Unite"; Rec."Nombre Unite")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre d''unité field.';
                }
                field(GetCalculateWeigh; Rec.GetCalculateWeigh())
                {
                    Caption = 'Poids brut total saisi';
                    ToolTip = 'Issu de la packing list';
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(creation)
        {
            action("Export Excel")
            {
                Image = Excel;
                Promoted = true;
                PromotedCategory = "Report";
                PromotedOnly = true;
                RunObject = Report "Export Demande transporteurs";
                ApplicationArea = All;
                ToolTip = 'Executes the Export Excel action.';
            }
        }
    }

    trigger OnAfterGetRecord();
    begin
        IF Rec.Type = Rec.Type::Expédition THEN gPartnerNo := Rec."Customer No.";
        IF Rec.Type = Rec.Type::Réception THEN gPartnerNo := Rec."Vendor No.";
    end;

    var
        gPartnerNo: Code[20];
}

