page 50052 "Contenus Emplacements SubForm"
{
    ApplicationArea = all;
    CardPageID = "Contenus Emplacements SubForm";
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = ListPart;
    SourceTable = "Bin Content";
    SourceTableView = SORTING(Default, Quantity, "Bin Code", "Location Code")
                    ORDER(Descending);
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Warehouse Class Code"; Rec."Warehouse Class Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Warehouse Class Code field.';
                }
                field(Fixed; Rec.Fixed)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fixed field.';
                }
                field(Default; Rec.Default)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Default field.';
                }
                field("Block Movement"; Rec."Block Movement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Block Movement field.';
                }
                field("Min. Qty."; Rec."Min. Qty.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Min. Qty. field.';
                }
                field("Max. Qty."; Rec."Max. Qty.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Max. Qty. field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure FiltreArticle(_pItemNo: Code[20])
    begin
        Rec.MARKEDONLY(FALSE);
        Rec.SETRANGE("Item No.", _pItemNo);
        Rec.FINDFIRST();
        CurrPage.UPDATE(FALSE);
    end;

    procedure RazListe()
    begin
        Rec.CLEARMARKS();
        Rec.MARKEDONLY(TRUE);
        CurrPage.UPDATE(FALSE);
    end;

    // CFR le 12/04/2022 => R‚gie : Ajout Key [Default] > tri dans page 50052
}

