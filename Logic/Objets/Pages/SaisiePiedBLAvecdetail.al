page 50268 "Saisie Pied BL - Avec detail"
{
    ApplicationArea = all;
    Caption = 'Saisie du pied de BL avec détail colis';
    CardPageID = "Saisie du Pied de BL";
    PageType = Card;
    Permissions = TableData "Sales Shipment Header" = rm,
                  TableData 7322 = rm;
    RefreshOnActivate = true;
    SourceTable = "Sales Shipment Header";

    layout
    {
        area(content)
        {
            field(ztxtBonPrepa; txtBonPrepa)
            {
                Caption = 'Choix du bon de prépa.';
                Style = Standard;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Choix du bon de prépa. field.';

                trigger OnLookup(Var Text: Text): Boolean;
                var
                    rec_BP: Record "Posted Whse. Shipment Header";
                    frm_BP: Page "Posted Whse. Shipment List";
                begin
                    // Initialisation
                    CLEAR(frm_BP);
                    rec_BP.RESET();

                    frm_BP.LOOKUPMODE(TRUE);
                    IF frm_BP.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        frm_BP.GETRECORD(rec_BP);
                        txtBonPrepa := rec_BP."Whse. Shipment No.";
                        txtBonPrepaEnr := rec_BP."No.";
                    END;

                    // On remet à blanc les valeurs.
                    RazValues();

                    IF txtBonPrepa = '' THEN BEGIN
                        // Panel non visible.
                        SetEditable(FALSE);
                        btn_ValiderEnable := FALSE;
                    END
                    ELSE BEGIN
                        // On remplit les champs.
                        SetValues();
                        // Panel visible.
                        SetEditable(TRUE);
                        btn_ValiderEnable := TRUE;
                    END;
                end;

                trigger OnValidate();
                begin
                    txtBonPrepaOnAfterValidate();
                end;
            }
            field(CtlFocus; Focus)
            {
                //The property ControlAddIn is not yet supported. Please convert manually.
                //ControlAddIn = 'Ahead_EXFocus;PublicKeyToken=41567e7a458383d3';
                ShowCaption = false;
                ApplicationArea = All;
            }
            group(pnlPiedBL)
            {
                Caption = 'Saisie du pied de BL';
                field("No."; Rec."No.")
                {
                    Caption = 'Bon de livraison :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bon de livraison : field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    Caption = 'Code client :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code client : field.';
                }
                field("Sell-to Customer Name"; Rec."Sell-to Customer Name")
                {
                    Caption = 'Nom du Client :';
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom du Client : field.';
                }
                field(Preparateur; Rec.Preparateur)
                {
                    Caption = 'Préparateur :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Préparateur : field.';
                }
                field(decPdsTheo; decPdsTheo)
                {
                    Caption = 'Poids Théorique :';
                    Editable = false;
                    ToolTip = 'Somme (qté article x poids théorique article)';
                    ApplicationArea = All;
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    Caption = 'Code Port :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Port : field.';
                }
                field(txtPortNom; txtPortNom)
                {
                    Caption = 'Nom port :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom port : field.';
                }
                field(NbColis; intNbColis)
                {
                    Caption = 'Nombre de colis :';
                    Editable = NbColisEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre de colis : field.';

                    trigger OnValidate();
                    begin
                        intNbColisOnAfterValidate();
                    end;
                }
                field(NbEtiq; intNbEtiq)
                {
                    Caption = 'Nb Eti :';
                    Editable = NbEtiqEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nb Eti : field.';
                }
                field(CdeEmb; txtEmbal)
                {
                    Caption = 'Code emballage :';
                    Editable = CdeEmbEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code emballage : field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var
                        rec_Emballage: Record "Generals Parameters";
                        frm_Emballage: Page "List Parameters";
                    begin
                        // Possibilité de choisir un emballage.

                        // Initialisation
                        CLEAR(frm_Emballage);
                        rec_Emballage.RESET();


                        rec_Emballage.SETRANGE(Type, 'EMBALLAGE');
                        frm_Emballage.SETTABLEVIEW(rec_Emballage);
                        frm_Emballage.LOOKUPMODE(TRUE);
                        IF frm_Emballage.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            frm_Emballage.GETRECORD(rec_Emballage);
                            txtEmbal := rec_Emballage.Code;
                            dec_PoidsUnitEmb := rec_Emballage.Decimal1;
                            decPdsEmbal := decPdsTheo + (intNbColis * dec_PoidsUnitEmb);
                        END;
                    end;
                }
                field(CdeTransp; txtTransporteur)
                {
                    Caption = 'Code Transport :';
                    Editable = CdeTranspEditable;
                    TableRelation = "Shipping Agent";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Transport : field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var
                        rec_Transp: Record "Shipping Agent";
                        frm_Transp: Page "Shipping Agents";
                    begin
                        // Initialisation
                        CLEAR(frm_Transp);
                        CLEAR(rec_Transp);

                        IF rec_Transp.GET(txtTransporteur) THEN
                            frm_Transp.SETRECORD(rec_Transp);
                        frm_Transp.LOOKUPMODE(TRUE);

                        IF frm_Transp.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            frm_Transp.GETRECORD(rec_Transp);
                            // Vérification du transporteur impératif
                            IF txtTransporteur <> rec_Transp.Code THEN BEGIN
                                IF bln_TransporteurImperatif THEN
                                    ERROR(STRSUBSTNO(Error50000Lbl, txtTransporteur));
                                txtTransporteur := rec_Transp.Code;
                                txtTransporteurNom := rec_Transp.Name;
                            END;
                        END;
                    end;

                    trigger OnValidate();
                    begin
                        IF bln_TransporteurImperatif THEN
                            ERROR(STRSUBSTNO(Error50000Lbl, txtTransporteur));
                    end;
                }
                field(txtTransporteurNom; txtTransporteurNom)
                {
                    Caption = 'Nom Transporteur :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Transporteur : field.';
                }
                field(ModeExp; optModeExpe)
                {
                    Caption = 'Mod. Expedition :';
                    Editable = ModeExpEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mod. Expedition : field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    Caption = 'Date livraison :';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison : field.';
                }
                field(decPdsEmbal; decPdsEmbal)
                {
                    Caption = 'Poids avec emballage :';
                    Editable = false;
                    ToolTip = 'Somme (qté article x poids théorique article) + Somme (Qté emballage x poids emballage)';
                    ApplicationArea = All;
                }
                field(Poids; decPdsReel)
                {
                    Caption = 'Poids réel :';
                    Editable = PoidsEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids réel : field.';

                    trigger OnValidate();
                    begin
                        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
                        IF (intNbColis > 1) THEN ERROR('Vous ne pouvez pas saisir directement le poids lorsqu''il y a plusieurs colis');
                        // FIN CFR 28/09/2022
                        decPdsReelOnAfterValidate();
                    end;
                }
                field(decPortHT; decPortHT)
                {
                    Caption = 'Port HT :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Port HT : field.';
                }
                field(blnAssurance; blnAssurance)
                {
                    Caption = 'Assurance :';
                    Editable = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assurance : field.';
                }
                field(blnSamedi; blnSamedi)
                {
                    Caption = 'Samedi :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Samedi : field.';
                }
            }
            part(PageTeliaeUMDetail; 50229)
            {
                Caption = 'Détail des colis (si plus de 1)';
                SubPageLink = "No. BP" = FIELD("Preparation No.");
                UpdatePropagation = Both;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(btn_FctValider)
            {
                Caption = 'Fonction';
                action(btn_Valider)
                {
                    Caption = 'Valider';
                    Image = PostInventoryToGL;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Valider action.';

                    trigger OnAction();
                    var
                        lBLNo: Code[20];
                        lTeliaeUMDetail: Record "Teliae UM Detail";
                    begin
                        // Modification des tables.
                        lBLNo := ValidValues();

                        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
                        /*
                        // CFR 07/12/2021 - FE20190930 : Détail des colis
                        // Création des enregistrements
                        lTeliaeUMDetail.InitRecords(txtBonPrepaEnr, txtBonPrepa, intNbColis, decPdsReel);
                        // Fin CFR 07/12/2021
                        */
                        VerifierColisage();
                        // FIN CFR 28/09/2022

                        // Générations des étiquettes.
                        ShipmentLabel();

                        // Fermeture du form.
                        RazValues();
                        txtBonPrepa := '';
                        txtBonPrepaEnr := '';

                        // ANI Le 17-11-2015 : Gestion du focus
                        Focus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);

                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord();
    begin
        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis - on remet à jour en fonction de ce qui est saisi dans le détail des colis.
        decPdsReel := Rec.Weight + (intNbColis * dec_PoidsUnitEmb);
        intNbColis := Rec."Nb Of Box";
        intNbEtiq := intNbColis;
    end;

    trigger OnInit();
    begin
        NbEtiqEditable := TRUE;
        ModeExpEditable := TRUE;
        CdeTranspEditable := TRUE;
        NbColisEditable := TRUE;
        PoidsEditable := TRUE;
        CdeEmbEditable := TRUE;
        btn_ValiderEnable := TRUE;
    end;

    trigger OnOpenPage();
    begin

        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
        Rec.RESET();
        CLEAR(Rec);
        Rec.SETRANGE("No.", '');
        // FIN CFR 28/09/2022

        // A l'ouverture du formulaire, on rend les variables non éditables;
        SetEditable(FALSE);
        btn_ValiderEnable := FALSE;

        // ANI Le 17-11-2015 : Gestion du focus
        Focus := 'ST1000000002;MD' + FORMAT(TIME, 0, 1);
    end;

    var
        txtBonPrepa: Code[20];
        txtBonPrepaEnr: Code[20];
        //   "-------------------": Integer;
        decPdsTheo: Decimal;
        intNbColis: Integer;
        txtEmbal: Code[20];
        decPdsEmbal: Decimal;
        decPdsReel: Decimal;
        intNbEtiq: Integer;
        txtTransporteur: Code[20];
        txtTransporteurNom: Text[50];
        decPortHT: Decimal;
        blnEtiquetage: Boolean;
        optModeExpe: Enum "Mode Expédition";
        blnAssurance: Boolean;
        blnSamedi: Boolean;
        dec_PoidsUnitEmb: Decimal;
        txtPortNom: Text[50];
        bln_TransporteurImperatif: Boolean;
        Error50000Lbl: Label 'Le transporteur %1 est impératif pour cette livraison.', Comment = '%1 =Transporteur';
        PoidsBP: Decimal;
        btn_ValiderEnable: Boolean;
        CdeEmbEditable: Boolean;
        PoidsEditable: Boolean;
        NbColisEditable: Boolean;
        CdeTranspEditable: Boolean;
        ModeExpEditable: Boolean;
        NbEtiqEditable: Boolean;
        Focus: Text[50];

    procedure RazValues();
    var
        Parametres: Record "Generals Parameters";
    begin
        // ** Cette fonction permet de remettre à vide tout les champs du formulaire ** //

        // Variables textes.
        txtEmbal := '';
        Parametres.RESET();
        Parametres.SETRANGE(Type, 'EMBALLAGE');
        Parametres.SETRANGE(Default, TRUE);
        IF Parametres.FINDFIRST() THEN
            txtEmbal := Parametres.Code;
        txtTransporteur := '';
        txtTransporteurNom := '';
        txtPortNom := '';

        // Variables numériques
        decPdsTheo := 0;
        intNbColis := 1;
        decPdsEmbal := 0;
        intNbEtiq := 1;
        decPortHT := 0;
        dec_PoidsUnitEmb := 0;
        decPdsReel := 0;
        // Variables booléenes
        blnEtiquetage := FALSE;
        blnAssurance := FALSE;
        blnSamedi := FALSE;
    end;

    procedure SetValues();
    var
        rec_Transporteur: Record "Shipping Agent";
        rec_BP: Record "Posted Whse. Shipment Header";
        rec_Emballage: Record "Generals Parameters";
        rec_Port: Record "Shipment Method";
    begin
        // ** Cette fonction permet de remplir tout les champs du formulaire ** //

        // On récupère le bon de livraison correspondant au numéro de BP saisit par l'utilisateur.
        // On prend le BL correspondant au donneur d'ordre et non de la centrale (Spécifique SYMTA).
        Rec.RESET();
        Rec.SETRANGE("Preparation No.", txtBonPrepa);
        IF Rec.COUNT > 1 THEN
            Rec.SETRANGE("Regroupement Centrale", FALSE);
        IF Rec.FINDFIRST() THEN BEGIN
            // Récupération des tables associées.
            // Transporteur
            IF rec_Transporteur.GET(Rec."Shipping Agent Code") THEN;
            // BP enregistré
            rec_BP.RESET();
            rec_BP.SETRANGE("Whse. Shipment No.", txtBonPrepa);
            rec_BP.FINDFIRST();
            // Mode de livraison
            IF rec_Port.GET(Rec."Shipment Method Code") THEN;

            // Variables textes.
            txtPortNom := rec_Port.Description;
            txtTransporteur := Rec."Shipping Agent Code";
            txtTransporteurNom := rec_Transporteur.Name;
            optModeExpe := Rec."Mode d'expédition";

            // Variables numériques
            decPdsTheo := rec_BP.CalcPoids();
            PoidsBP := rec_BP.Poids;
            intNbColis := Rec."Nb Of Box";
            IF intNbColis = 0 THEN
                intNbColis := 1;
            intNbEtiq := intNbColis;
            // Calcul du poids de l'emballage.
            IF rec_Emballage.GET('EMBALLAGE', txtEmbal) THEN
                dec_PoidsUnitEmb := rec_Emballage.Decimal1
            ELSE
                dec_PoidsUnitEmb := 0;
            decPdsEmbal := decPdsTheo + (intNbColis * dec_PoidsUnitEmb);
            decPdsReel := PoidsBP + (intNbColis * dec_PoidsUnitEmb);
            decPortHT := 0;


            // Variables booléenes
            blnEtiquetage := FALSE;
            blnAssurance := Rec."Insurance of Delivery";
            blnSamedi := Rec."Saturday Delivery";
            bln_TransporteurImperatif := Rec."Transporteur imperatif";

        END
        ELSE
            // Si on ne trouve pas le BP.
            BEGIN
            SetEditable(FALSE);
            btn_ValiderEnable := FALSE;

            // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
            Rec.RESET();
            CLEAR(Rec);
            Rec.SETRANGE("No.", '');
            // FIN CFR 28/09/2022
        END;

        CurrPage.UPDATE(FALSE);
    end;

    procedure SetEditable(Editable: Boolean);
    begin
        CdeEmbEditable := Editable;
        PoidsEditable := Editable;
        NbColisEditable := Editable;
        CdeTranspEditable := Editable;
        ModeExpEditable := Editable;
        NbEtiqEditable := Editable;
    end;

    procedure ValidValues() returnBLNo: Code[20];
    var
        rec_BL: Record "Sales Shipment Header";
        rec_BP: Record "Posted Whse. Shipment Header";
        bln_ModifyBP: Boolean;
        bln_ModifyBL: Boolean;
    begin
        // ** Cette fonction permet de valider les valeurs choisit par l'utilisateur **//

        // Elle retourne le BL.No. s'il a été trouvé
        returnBLNo := '';

        bln_ModifyBL := FALSE;
        bln_ModifyBP := FALSE;

        // Récupère les tables à modifier.
        // On récupère le bon de livraison correspondant au numéro de BP saisit par l'utilisateur.
        // On prend le BL correspondant au donneur d'ordre et non de la centrale (Spécifique SYMTA).
        rec_BL.RESET();
        rec_BL.SETRANGE("Preparation No.", txtBonPrepa);
        IF rec_BL.COUNT > 1 THEN
            rec_BL.SETRANGE("Regroupement Centrale", FALSE);

        IF rec_BL.FINDFIRST() THEN BEGIN
            returnBLNo := rec_BL."No.";

            // BP enregistré
            rec_BP.RESET();
            rec_BP.SETRANGE("Whse. Shipment No.", txtBonPrepa);
            rec_BP.FINDFIRST();

            // Vérification des modifications

            // Code emballage
            IF txtEmbal <> rec_BL."Code Emballage" THEN BEGIN
                rec_BL."Code Emballage" := txtEmbal;
                bln_ModifyBL := TRUE;
            END;

            // Poids
            IF decPdsReel <> rec_BP.Poids THEN BEGIN
                rec_BP.Poids := decPdsReel;
                rec_BL.Weight := decPdsReel;
                bln_ModifyBL := TRUE;
                bln_ModifyBP := TRUE;
            END;

            // Nombre de colis.
            IF intNbColis <> rec_BL."Nb Of Box" THEN BEGIN
                rec_BL."Nb Of Box" := intNbColis;
                bln_ModifyBL := TRUE;
            END;

            // Nombre d'étiquettes.
            IF intNbEtiq <> rec_BL."Nb Of Label" THEN BEGIN
                rec_BL."Nb Of Label" := intNbEtiq;
                bln_ModifyBL := TRUE;
            END;


            // Transporteur
            IF txtTransporteur <> rec_BL."Shipping Agent Code" THEN BEGIN
                rec_BL."Shipping Agent Code" := txtTransporteur;
                rec_BP."Shipping Agent Code" := txtTransporteur;
                bln_ModifyBL := TRUE;
                bln_ModifyBP := TRUE;
            END;

            // Mode d'expedition.
            IF optModeExpe <> rec_BL."Mode d'expédition" THEN BEGIN
                rec_BL."Mode d'expédition" := optModeExpe;
                rec_BP."Mode d'expédition" := optModeExpe;
                bln_ModifyBL := TRUE;
                bln_ModifyBP := TRUE;
            END;

            // ASssurance.
            IF blnAssurance <> rec_BL."Insurance of Delivery" THEN BEGIN
                rec_BL."Insurance of Delivery" := blnAssurance;
                rec_BP.Assurance := blnAssurance;
                bln_ModifyBL := TRUE;
                bln_ModifyBP := TRUE;
            END;


            // Modification des enregistrements.
            IF bln_ModifyBL THEN rec_BL.MODIFY();
            IF bln_ModifyBP THEN rec_BP.MODIFY();
        END;

        EXIT(returnBLNo);
    end;

    procedure ShipmentLabel();
    var
        ShipLabel: Record "Shipping Label";
        transporteur: Record "Shipping Agent";
        SalesSetup: Record "Sales & Receivables Setup";
        SalesShptHeader: Record "Sales Shipment Header";
        LCust: Record Customer;
        NoSeriesMgt: Codeunit NoSeriesManagement;

        EtqLogiflux: Codeunit "Edition Etiquette LOGIFLUX";
        LGestionBP: Codeunit "Gestion Bon Préparation";

        "Quantité_collis": Integer;
        bln_Insert: Boolean;
    begin
        // AD Le 07-04-2007 => Gestion des étiquettes transports

        SalesSetup.GET();
        SalesShptHeader.RESET();
        SalesShptHeader.SETRANGE("Preparation No.", txtBonPrepa);
        IF SalesShptHeader.COUNT > 1 THEN
            SalesShptHeader.SETRANGE("Regroupement Centrale", FALSE);

        IF NOT SalesShptHeader.FINDFIRST() THEN EXIT;


        // Vérification qu'un enregistrement n'existe pas déjà daéns la table.
        ShipLabel.RESET();
        ShipLabel.SETRANGE(ShipLabel."BL No.", SalesShptHeader."No.");

        IF ShipLabel.FINDFIRST() THEN
            bln_Insert := FALSE
        ELSE
            bln_Insert := TRUE;

        // Generation de l'enregistrement dans la table.
        // ---------------------------------------------
        IF bln_Insert THEN
            ShipLabel.INIT();

        ShipLabel."Shipping Agent Code" := SalesShptHeader."Shipping Agent Code";
        ShipLabel.VALIDATE("Shipping Agent Services", SalesShptHeader."Shipping Agent Service Code");

        IF bln_Insert THEN
            ShipLabel."Entry No." := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Entry Nos.", WORKDATE(), TRUE);

        ShipLabel."Prepare Code" := SalesShptHeader.Preparateur;

        ShipLabel."BL No." := SalesShptHeader."No.";
        ShipLabel."Preparation No." := SalesShptHeader."Preparation No.";
        ShipLabel."Date of  bl" := SalesShptHeader."Posting Date";

        ShipLabel."Recipient Code" := SalesShptHeader."Sell-to Customer No.";

        // AD Le 05-12-2011
        IF LCust.GET(ShipLabel."Recipient Code") THEN
            ShipLabel."Network Code" := LCust."Code Expéditeur";
        // FIN AD Le 05-12-2011


        //CJ 16/12/09 - DEBUT
        //ShipLabel."Recipient Person"       := SalesShptHeader."Ship-to Contact";
        ShipLabel."Recipient Person" := COPYSTR(SalesShptHeader."Ship-to Contact", 1, 30);
        //CJ 16/12/09 -FIN

        ShipLabel."Recipient Name" := SalesShptHeader."Ship-to Name";
        ShipLabel."Recipient Address" := SalesShptHeader."Ship-to Address";
        ShipLabel."Recipient Address 2" := SalesShptHeader."Ship-to Address 2";
        ShipLabel."Recipient Post Code" := SalesShptHeader."Ship-to Post Code";
        ShipLabel."Recipient City" := SalesShptHeader."Ship-to City";
        ShipLabel."Recipient Country Code" := SalesShptHeader."Ship-to Country/Region Code";

        IF SalesShptHeader."Promised Delivery Date" <> 0D THEN
            //ShipLabel."Imperative date of Delivery" := SalesShptHeader."Promised Delivery Date";
            ShipLabel."Imperative date of Delivery" := SalesShptHeader."Requested Delivery Date";



        // AD Le 05-12-2011 =>
        /*
        ShipLabel."Imperative date of Delivery" := CALCDATE('<+1D>', SalesShptHeader."Posting Date");
        
        IF (DATE2DWY(ShipLabel."Imperative date of Delivery", 1) = 6) AND
         (NOT (SalesShptHeader."Mode d'expédition" IN [SalesShptHeader."Mode d'expédition"::"Contre Rembourssement Samedi",
                  SalesShptHeader."Mode d'expédition"::"Express Samedi"])) THEN
            ShipLabel."Imperative date of Delivery" := CALCDATE('<+2D>', ShipLabel."Imperative date of Delivery");
        
        */
        ShipLabel."Imperative date of Delivery" :=
                LGestionBP."ChercheDateLivr.Logiflux"(SalesShptHeader."Posting Date", SalesShptHeader."Mode d'expédition");

        // FIN AD Le 05-12-2011 =>



        ShipLabel."Instruction of Delivery" := SalesShptHeader."Instructions of Delivery";
        ShipLabel."Cash on delivery" := SalesShptHeader."Cash on Delivery";
        ShipLabel."External Document No." := SalesShptHeader."External Document No.";

        ShipLabel."Nb Of Pallets" := SalesShptHeader."Nb Of Box";
        ShipLabel."Nb Of Box" := SalesShptHeader."Nb Of Box";

        ShipLabel."Assured Box" := SalesShptHeader."Insurance of Delivery";


        ShipLabel.Weight := SalesShptHeader.Weight;
        ShipLabel."Mode d'expédition" := SalesShptHeader."Mode d'expédition";
        IF NOT ShipLabel.INSERT() THEN ShipLabel.MODIFY();

        // Affectation des numéros de colis uniques.
        // -----------------------------------------
        IF ShipLabel."Nb Of Pallets" <> 0 THEN BEGIN
            ShipLabel."No. start label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
            ShipLabel."No. end label" := ShipLabel."No. start label";

            Quantité_collis := 1;
            WHILE Quantité_collis < ShipLabel."Nb Of Pallets" DO BEGIN
                ShipLabel."No. end label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
                Quantité_collis += 1;
            END;

            IF NOT ShipLabel.MODIFY() THEN
                ShipLabel.Insert();
        END;


        // Edition de l'étiquette.
        // -----------------------
        IF ShipLabel."No. start label" <> '' THEN BEGIN
            IF transporteur.GET(ShipLabel."Shipping Agent Code") THEN
                IF transporteur."Label Report ID" <> 0 THEN BEGIN
                    //Modif parce que ne marche pas pour SCHENKER et urgent.
                    ShipLabel.SETRECFILTER();
                    REPORT.RUNMODAL(transporteur."Label Report ID", FALSE, FALSE, ShipLabel);
                    ShipLabel.RESET();
                    //FIN MODIF
                END
                ELSE
                    EtqLogiflux.EditionEtiquetteLogiflux(ShipLabel, 'M');

        END;

    end;

    local procedure txtBonPrepaOnAfterValidate();
    begin
        // Après avoir saisit un bon de préparation, on affiche le panel ou non.

        // On remet à blanc les valeurs.
        RazValues();

        IF txtBonPrepa = '' THEN BEGIN
            // Panel non visible.
            SetEditable(FALSE);
            btn_ValiderEnable := FALSE;
        END
        ELSE BEGIN
            // Panel visible.
            SetEditable(TRUE);
            btn_ValiderEnable := TRUE;

            // On remplit les champs.
            SetValues();
        END;
    end;

    local procedure decPdsReelOnAfterValidate();
    var
        rec_BP: Record "Posted Whse. Shipment Header";
    begin
        // Sur changement de poids réels, on va vérifier l'écart avec le BP enregistré //
        rec_BP.RESET();
        rec_BP.SETRANGE("Whse. Shipment No.", txtBonPrepa);
        rec_BP.FINDFIRST();
        rec_BP.VerifPoids(decPdsReel);
    end;

    local procedure intNbColisOnAfterValidate();
    var
        lTeliaeUMDetail: Record "Teliae UM Detail";
    begin
        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
        // Création des enregistrements
        lTeliaeUMDetail.InitRecords(txtBonPrepaEnr, txtBonPrepa, intNbColis, 0);
        // FIN CFR 28/09/2022

        // Recalcule du poids en fonction du nb de colis.
        decPdsEmbal := decPdsTheo + (intNbColis * dec_PoidsUnitEmb);

        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
        //decPdsReel  := PoidsBP    + (intNbColis * dec_PoidsUnitEmb);
        decPdsReel := CurrPage.PageTeliaeUMDetail.PAGE.CalcSumWeight(txtBonPrepa) + (intNbColis * dec_PoidsUnitEmb);
        // FIN CFR 28/09/2022

        // Met à jour le nombre d'étiquettes.
        intNbEtiq := intNbColis;
    end;

    local procedure VerifierColisage(): Boolean;
    var
        // lWarehouseEmployee: Record "Warehouse Employee";
        lTeliaeUMDetail: Record "Teliae UM Detail";
        //lWarning01: TextConst ENU = 'The sum of the weights (%1 packages) must be equal to %2 kg (actually : %3).', FRA = 'La somme des poids des %1 colis doit être égale à %2 kg (actuellement saisi : %3).';
        lWarning02Err: Label 'You must enter a weight on all packages';
    //FRA = 'Vous devez saisir un poids sur tous les colis.';
    begin
        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
        IF (intNbColis > 1) THEN BEGIN
            lTeliaeUMDetail.SETRANGE("No. BP", txtBonPrepa);
            // V2 : Pas de poids à 0
            lTeliaeUMDetail.SETRANGE("Weight UM (Kg)", 0);
            IF (NOT lTeliaeUMDetail.ISEMPTY()) THEN
                ERROR(lWarning02Err);
        END;
    end;
}

