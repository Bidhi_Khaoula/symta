page 50305 "WIIO - Liste Ligne PackingList"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Packing List Line";

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Packing List No."; Rec."Packing List No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° ligne field.';
                }
                field("Whse. Shipment No."; Rec."Whse. Shipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° expédition entrepôt field.';
                }
                field("Whse. Shipment Line No."; Rec."Whse. Shipment Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° ligne expédition entrepôt field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° article field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Quantity to Ship"; Rec."Quantity to Ship")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité à expédier field.';
                }
                field("Line Quantity"; Rec."Line Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité de la ligne field.';
                }
                field("Package No."; Rec."Package No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° colis field.';
                }
                field("WIIO Préparation"; Rec."WIIO Préparation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the WIIO Préparation field.';
                }
            }
        }
    }

    actions
    {
    }
}

