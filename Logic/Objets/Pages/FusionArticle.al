page 50054 "Fusion Article"
{
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    PageType = Worksheet;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(ArticleOrigine; ArticleOrigine)
            {
                Caption = 'Article origine';
                TableRelation = Item;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Article origine field.';
            }
            field(ArticleDestination; ArticleDestination)
            {
                Caption = 'Article destination';
                TableRelation = Item;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Article destination field.';
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action("Fusionner Articles")
                {
                    Caption = 'Fusionner Articles';
                    ToolTip = 'En fin de fusion, le programme recopie tous les enregistrement [Stat conso] de l''article d''origine dans l''article de destinatation ET conserve tous les enregistrement [Stat conso] de l''article d''origine';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    Image = CopyItem;

                    trigger OnAction()
                    begin
                        IF CONFIRM('Voulez vous fusionner ?') THEN BEGIN
                            Fusion.FusionnerArticle(ArticleOrigine, ArticleDestination);
                            MESSAGE('Traitement terminé');
                        END;
                    end;
                }
                action("Copier Stats Conso AVEC Remise … 0")
                {
                    Caption = 'Copier Stats Conso AVEC Remise … 0';
                    ToolTip = 'Recopier tous les enregistrements [Stat conso] de l''article d''origine dans l''article de destination ET supprimer tous les enregistrements [Stat conso] de l''article d''origine';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = DeleteXML;
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        IF CONFIRM(ESK002Qst) THEN BEGIN
                            Fusion.FusionnerConso(ArticleOrigine, ArticleDestination, TRUE);
                            MESSAGE('Traitement terminé');
                        END;
                    end;
                }
                action("Copier Stats Conso SANS Remise … 0")
                {
                    Caption = 'Copier Stats Conso SANS Remise … 0';
                    ToolTip = 'Recopier tous les enregistrements [Stat conso] de l''article d''origine dans l''article de destination ET conserver tous les enregistrements [Stat conso] de l''article d''origine';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Image = UpdateXML;
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        IF CONFIRM(ESK003Qst) THEN BEGIN
                            Fusion.FusionnerConso(ArticleOrigine, ArticleDestination, FALSE);
                            MESSAGE('Traitement terminé');
                        END;
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        MESSAGE('Effectuer le relevé des stocks dépots et informer le magasin avant la fusion');
    end;

    var
        Fusion: Codeunit "Fusion Article";
        ESK002Qst: Label 'Fusion des consommations AVEC remise à 0 des conso. de l''article d''origine.';
        ESK003Qst: Label 'Fusion des consommations SANS remise à 0 des conso. de l''article d''origine.';
        ArticleOrigine: Code[20];
        ArticleDestination: Code[20];


    // CFR le 27/09/2023 - Ajout tooltip sur actions
}

