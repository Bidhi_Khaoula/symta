page 50257 "Bin Inventory Ledger Entry"
{
    Caption = 'Ecritures inventaire emplacement';
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = Worksheet;
    SourceTable = "Bin Inventory Ledger Entry";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Filtres)
            {
                Caption = 'Filtres';
                field(CtrlPostingDateFilter; gPostingDateFilter)
                {
                    Caption = 'Filtre date compta';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Filtre date compta field.';
                    trigger OnValidate();
                    var
                        FilterTokens: Codeunit "Filter Tokens";
                    begin

                        FilterTokens.MakeDateFilter(gPostingDateFilter);
                        PostingDateFilterOnAfterValid();
                    end;
                }
                field(CtrlLocationCodeFilter; gLocationCodeFilter)
                {
                    Caption = 'Filtre magasin';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Filtre magasin field.';

                    trigger OnLookup(Var Text: Text): Boolean;
                    var
                        lLocationList: Page "Location List";
                    begin
                        lLocationList.LOOKUPMODE := TRUE;
                        IF lLocationList.RUNMODAL() = ACTION::LookupOK THEN
                            Text := lLocationList.GetSelectionFilter()
                        ELSE
                            EXIT(FALSE);

                        EXIT(TRUE);
                    end;

                    trigger OnValidate();
                    begin
                        LocationCodeFilterOnAfterValidate();
                    end;
                }
                field(CtrlBinCodeFilter; gBinCodeFilter)
                {
                    Caption = 'Filtre emplacement';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Filtre emplacement field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        lBinList: Page "Bin List";
                        lBin: Record Bin;
                        CduLFunctions: Codeunit "Codeunits Functions";
                    begin
                        IF Rec.GETFILTER("Location Code") <> '' THEN BEGIN
                            lBin.SETFILTER("Location Code", Rec.GETFILTER("Location Code"));
                            lBinList.SETTABLEVIEW(lBin);
                            lBinList.LOOKUPMODE := TRUE;
                            IF lBinList.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                lBinList.SETSELECTIONFILTER(lBin);
                                //EXIT(SelectionFilterManagement.GetSelectionFilterForLocation(Loc));
                                //Text := lBin.GETFILTER(Code)
                                Text := CduLFunctions.GetSelectionFilterForBin(lBin);
                            END
                            ELSE
                                EXIT(FALSE);

                            EXIT(TRUE);
                        END;
                    end;

                    trigger OnValidate();
                    begin
                        BinCodeFilterOnAfterValidate();
                    end;
                }
            }
            repeater(Group)
            {
                Editable = false;
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Journal Batch Name"; Rec."Journal Batch Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Journal Batch Name field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Salespers./Purch. Code"; Rec."Salespers./Purch. Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salespers./Purch. Code field.';
                }
                field("Number Of Phys. Inv. Entry"; Rec."Number Of Phys. Inv. Entry")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre écriture article field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage();
    var
        lCompanyInformation: Record "Company Information";
    begin
        // filtres par défaut : à partir de la date du jour + Magasin société
        gPostingDateFilter := FORMAT(TODAY()) + '..';
        lCompanyInformation.GET();
        gLocationCodeFilter := lCompanyInformation."Location Code";
        GetRecFilters();
        SetRecFilters();
    end;

    var
        gPostingDateFilter: Text[30];
        gLocationCodeFilter: Text;
        gBinCodeFilter: Text;

    local procedure GetRecFilters();
    begin
        IF Rec.GETFILTERS() <> '' THEN BEGIN
            EVALUATE(gPostingDateFilter, Rec.GETFILTER("Posting Date"));
            gLocationCodeFilter := Rec.GETFILTER("Location Code");
            gBinCodeFilter := Rec.GETFILTER("Bin Code");
        END;
    end;

    procedure SetRecFilters();
    begin
        IF gPostingDateFilter <> '' THEN
            Rec.SETFILTER("Posting Date", gPostingDateFilter)
        ELSE
            Rec.SETRANGE("Posting Date");

        IF gLocationCodeFilter <> '' THEN
            Rec.SETFILTER("Location Code", gLocationCodeFilter)
        ELSE
            Rec.SETRANGE("Location Code");

        IF gBinCodeFilter <> '' THEN
            Rec.SETFILTER("Bin Code", gBinCodeFilter)
        ELSE
            Rec.SETRANGE("Bin Code");

        CurrPage.UPDATE(FALSE);
    end;

    local procedure PostingDateFilterOnAfterValid();
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;

    local procedure LocationCodeFilterOnAfterValidate();
    var
        lLocation: Record Location;
    begin
        IF lLocation.GET(Rec."Location Code") THEN
            CurrPage.SAVERECORD();
        SetRecFilters();
    end;

    local procedure BinCodeFilterOnAfterValidate();
    var
        lBinCode: Record Bin;
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;
}

