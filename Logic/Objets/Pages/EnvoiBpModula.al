page 50047 "Envoi Bp Modula"
{
    InsertAllowed = false;
    DeleteAllowed = false;
    DelayedInsert = true;
    PageType = Worksheet;
    RefreshOnActivate = false;
    SourceTable = "Warehouse Shipment Header";
    ApplicationArea = all;

    layout
    {
        area(content)
        {
            Group(content1)
            {
                ShowCaption = false;
                field(NoFlasher; gNoFlasher)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the gNoFlasher field.';

                    trigger OnValidate()
                    begin
                        NoFlasherOnValidate(gNoFlasher);
                    end;

                    trigger OnLookup(Var Text: Text): Boolean
                    var
                        WhseShipHeader: Record "Warehouse Shipment Header";
                        FrmLstWhseShipHeader: Page "Warehouse Shipment List";
                    begin
                        WhseShipHeader.RESET();
                        FrmLstWhseShipHeader.LOOKUPMODE(TRUE);
                        IF FrmLstWhseShipHeader.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmLstWhseShipHeader.GETRECORD(WhseShipHeader);
                            gNoFlasher := WhseShipHeader."No.";
                            NoFlasherOnValidate(gNoFlasher);
                            gFocus := '1000000002;MD' + FORMAT(TIME, 0, 1);
                        END;
                    end;
                }
                field("Location Code"; Rec."Location Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Destination Type"; Rec."Destination Type")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination Type field.';
                }
                field("Destination No."; Rec."Destination No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination No. field.';
                }
                field(GetName; Get_Name())
                {
                    Caption = 'Nom';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field(CtrlAssignedUserId; rec."Assigned User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field(CtrlFocus; gFocus)
                {
                    ShowCaption = false;
                    //TODOControlAddIn=[Ahead_EXFocus;PublicKeyToken=41567e7a458383d3];
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fonctions)
            {
                Caption = 'Fonctions';
                action("Génération")
                {
                    Caption = 'Génération';
                    Image = ConfirmAndPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Génération action.';

                    trigger OnAction()
                    begin
                        ExportBp();
                    end;
                }
                action(Effacer)
                {
                    caption = 'Effacer';
                    ApplicationArea = All;
                    Promoted = true;
                    PromotedCategory = Process;
                    Image = ClearLog;
                    ToolTip = 'Executes the Effacer action.';
                    trigger OnAction()
                    begin
                        ClearScreen();
                    end;
                }
            }
        }
    }
    var
        gFocus: Text[50];
        gNoFlasher: Code[20];

    trigger OnOpenPage()
    begin
        ClearScreen();
    end;

    procedure Get_Name(): Text[100]
    var
        cust: Record Customer;
    begin
        CASE Rec."Destination Type" OF
            Rec."Destination Type"::Customer:
                IF cust.GET(Rec."Destination No.") THEN
                    EXIT(cust.Name);
        END;
    end;

    procedure ExportBp()
    var
        GestionModula: Codeunit "Gestion Bon Préparation";
    begin
        IF (Rec."Assigned User ID" = '') THEN BEGIN
            // Focus "Assigned User ID"
            MESSAGE('La saisie du [Code utilisateur affect‚] est obligatoire');
            gFocus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);
        END
        ELSE BEGIN
            CurrPage.UPDATE(TRUE);
            GestionModula.ImprimeEtiquettePrepa(Rec, 0); // WIIO => Ajout du 0 en dernier paramŠtre
            ClearScreen();
        END;
    END;

    LOCAL PROCEDURE NoFlasherOnValidate(pCode: Code[20]);
    BEGIN
        Rec.RESET();
        Rec.SETRANGE("No.", gNoFlasher);
        IF NOT Rec.FINDFIRST() THEN BEGIN
            MESSAGE('PICS inconnu !');
            CurrPage.ACTIVATE();
            // Focus NoFlasher
            gFocus := 'ID1000000001;MD' + FORMAT(TIME, 0, 1);
        END
        ELSE BEGIN
            CurrPage.UPDATE(FALSE);
            // Focus "Assigned User ID"
            gFocus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);
        END;
    END;

    PROCEDURE ClearScreen();
    BEGIN
        CLEAR(Rec);
        Rec.SETRANGE("No.", '');
        gNoFlasher := '';
        gFocus := 'ID1000000001;MD' + FORMAT(TIME, 0, 1);
    END;

}

