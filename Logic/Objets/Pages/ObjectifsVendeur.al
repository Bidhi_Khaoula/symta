page 50003 "Objectifs Vendeur"
{
    CardPageID = "Objectifs Vendeur";
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Objectif Representant";
    ApplicationArea = All;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Date référence"; Rec."Date référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date référence field.';
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salesperson Code field.';
                }
                field("Cust. Country/Region Code"; Rec."Cust. Country/Region Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Country/Region Code field.';
                }
                field("Cust. Family Code 1"; Rec."Cust. Family Code 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Famille 1 Client field.';
                }
                field("Item Manufacturer Code"; Rec."Item Manufacturer Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Manufacturer Code field.';
                }
                field("Item Category Code"; Rec."Item Category Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Category Code field.';
                }
                field("Direction Code"; Rec."Direction Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Direction field.';
                }
                field("Objectif CA"; Rec."Objectif CA")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Objectif CA field.';
                }
                field("Objectif Marge"; Rec."Objectif Marge")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Objectif Marge field.';
                }
                field("Objectif CA Promo"; Rec."Objectif CA Promo")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Objectif CA Promo field.';
                }
                field("Objectif Marge Promo"; Rec."Objectif Marge Promo")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Objectif Marge Promo field.';
                }
            }
        }
    }
}

