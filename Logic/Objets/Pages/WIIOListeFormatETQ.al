page 50314 "WIIO - Liste Format ETQ"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Generals Parameters";
    SourceTableView = SORTING(Type, Code)
                      ORDER(Ascending)
                      WHERE(Type = FILTER('NICELBL_FRM'),
                            LongDescription = FILTER(<> ''));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    Caption = 'Code format';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code format field.';
                }
                field(LongDescription; Rec.LongDescription)
                {
                    Caption = 'Nom format';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom format field.';
                }
            }
        }
    }

    actions
    {
    }
}

