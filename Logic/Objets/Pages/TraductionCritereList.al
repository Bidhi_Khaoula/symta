page 50242 "Traduction Critere List"
{
    Caption = 'Traduction Critères';
    PageType = List;
    SourceTable = "Traduction Critere";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code Critere"; Rec."Code Critere")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Critere field.';
                }
                field("Code Langue"; Rec."Code Langue")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Langue field.';
                }
                field(Nom; Rec.Nom)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
            }
        }
    }

    actions
    {
    }
}

