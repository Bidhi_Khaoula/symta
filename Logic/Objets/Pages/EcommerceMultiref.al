page 50236 "Ecommerce Multiref"
{
    PageType = List;
    SourceTable = "Item Reference";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Article; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(MultiRef; Rec."Reference No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reference No. field.';
                }
                field(Reference_Active; _mvarStr_ItemNo2)
                {
                    Caption = 'Référence active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active field.';
                }
                field("publiable web"; Rec."publiable web")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the publiable web field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    var
        rec_Item: Record Item;
    begin
        // Récupère l'article.
        _mvarStr_ItemNo2 := '';
        IF rec_Item.GET(Rec."Item No.") THEN
            _mvarStr_ItemNo2 := rec_Item."No. 2";
    end;

    var
        _mvarStr_ItemNo2: Text[30];
}

