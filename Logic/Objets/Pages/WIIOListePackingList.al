page 50301 "WIIO - Liste PackingList"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Packing List Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° field.';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date création field.';
                }
                field("Whse Shipment Header Quantity"; Rec."Whse Shipment Header Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre d''expédition entrepôt field.';
                }
                field("Destination Name"; Rec.GetDestinationName())
                {
                    Caption = 'Nom Destination';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Destination field.';
                }
                field("Header Status"; Rec."Header Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
            }
        }
    }
}

