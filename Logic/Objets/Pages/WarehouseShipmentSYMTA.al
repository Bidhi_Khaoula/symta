page 50082 "Warehouse Shipment SYMTA"
{

    Caption = 'Warehouse Shipment';
    CardPageID = "Warehouse Shipment SYMTA";
    DeleteAllowed = true;
    InsertAllowed = false;
    PageType = Document;
    PopulateAllFields = true;
    RefreshOnActivate = false;
    SourceTable = "Warehouse Shipment Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General';
                field(txtFlash; NoFlasher)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NoFlasher field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        WhseShipHeader: Record "Warehouse Shipment Header";
                        FrmLstWhseShipHeader: Page "Warehouse Shipment List";
                    begin
                        WhseShipHeader.RESET();
                        FrmLstWhseShipHeader.LOOKUPMODE(TRUE);
                        IF FrmLstWhseShipHeader.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmLstWhseShipHeader.GETRECORD(WhseShipHeader);
                            NoFlasher := WhseShipHeader."No.";
                            GetNoFlasher(NoFlasher);
                        END;

                        // CFR le 19/01/2022 - FE20190930 : On focalise soit sur le poids (ID1000000025) soit sur le nombre de colis (ID1000000008)
                        IF NbColisVisible THEN
                            Focus := 'ID1000000008;MD' + FORMAT(TIME, 0, 1)
                        ELSE
                            Focus := 'ID1000000025;MD' + FORMAT(TIME, 0, 1);
                        // FIN CFR le 19/01/2022                    
                    end;

                    trigger OnValidate()
                    begin
                        NoFlasherOnAfterValidate();
                    end;
                }
                field("No."; Rec."No.")
                {
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';

                    trigger OnAssistEdit()
                    begin
                        IF rec.AssistEdit(xRec) THEN
                            CurrPage.UPDATE();
                    end;
                }
                field("Location Code"; Rec."Location Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        CurrPage.SAVERECORD();
                        rec.LookupLocation(Rec);
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';

                    trigger OnValidate()
                    begin
                        ZoneCodeOnAfterValidate();
                    end;
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Document Status"; Rec."Document Status")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Status field.';
                }
                field(GetDestinationName; rec.GetDestinationName())
                {
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetDestinationName() field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Assigned User ID"; Rec."Assigned User ID")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field("Assignment Date"; Rec."Assignment Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assignment Date field.';
                }
                field("Assignment Time"; Rec."Assignment Time")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assignment Time field.';
                }
                field("Sorting Method"; Rec."Sorting Method")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sorting Method field.';

                    trigger OnValidate()
                    begin
                        SortingMethodOnAfterValidate();
                    end;
                }
                field("Packing List No."; Rec."Packing List No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
            }
            /*//TODOusercontrol(CtlFocus; "Ahead_EXFocus")
            {
                ApplicationArea = All;
            }*/
            part(WhseShptLines; "Whse. Shipment Subform")
            {
                SubPageLink = "No." = FIELD("No.");
                SubPageView = SORTING("No.", "Sorting Sequence No.");
                ApplicationArea = All;
            }
            group(PiedBP)
            {
                Caption = 'Informations';
                field(Transporteur; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Mode d'expédition"; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field(NbColis; Rec."Nb Of Box")
                {
                    Caption = 'Nombre de colis :';
                    Editable = true;
                    Visible = NbColisVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre de colis : field.';
                    trigger OnValidate()
                    VAR
                        lTeliaeUMDetail: Record "Teliae UM Detail";
                    BEGIN
                        // CFR 07/12/2021 - FE20190930 : D‚tail des colis
                        // Cr‚ation des enregistrements
                        lTeliaeUMDetail.InitRecords(Rec."Shipping No.", Rec."No.", Rec."Nb Of Box", 0);
                        //CurrPage.UPDATE(FALSE);
                        Rec.VALIDATE(Poids, 0);

                        ZoneCodeOnAfterValidate();

                    end;
                }
                field(Poids; Rec.Poids)
                {
                    ApplicationArea = All;
                    Editable = PoidsEditable;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
                field("Assigned User ID2"; Rec."Assigned User ID")
                {
                    Caption = 'Assigned User ID';
                    ApplicationArea = All;
                    Editable = PoidsEditable;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field(CdEmb; Rec."Code Emballage")
                {
                    Caption = 'Code emballage :';
                    Visible = CdEmbVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code emballage : field.';
                }
            }
            part(PageTeliaeUMDetail; "Teliae UM Detail")
            {
                Caption = 'Détail des colis (si plus de 1)';
                SubPageLink = "No. BP" = FIELD("No.");
                Visible = NbColisVisible;
                UpdatePropagation = Both;
                ApplicationArea = All;
            }
            group(PiedBP2)
            {
                Caption = 'Secondaires';
                field(txtPort; decPortHT)
                {
                    Caption = 'Port HT :';
                    Editable = false;
                    Visible = txtPortVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Port HT : field.';
                }
                field(BlnSaturday; rec.Get_IsDeliverySaturday())
                {
                    Caption = 'Samedi :';
                    Editable = false;
                    Visible = BlnSaturdayVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Samedi : field.';
                }
                field(blnInsurance; rec.Assurance)
                {
                    Caption = 'Assurance :';
                    Editable = true;
                    Visible = blnInsuranceVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assurance : field.';
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Method Code field.';
                }
            }
            group(Shipping)
            {
                Caption = 'Shipping';
                Visible = false;
                field("External Document No."; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field("Shipment Date"; Rec."Shipment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Date field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = false;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    CurrPage.WhseShptLines.PAGE.MultiConsultation();
                end;
            }
            group("&Shipment")
            {
                Caption = '&Shipment';
                Image = Shipment;
                action(List)
                {
                    Caption = 'List';
                    Image = OpportunitiesList;
                    ShortCutKey = 'Shift+Ctrl+L';
                    ApplicationArea = All;
                    ToolTip = 'Executes the List action.';

                    trigger OnAction()
                    var
                        CduLFunctions: Codeunit Functions;
                    begin
                        CduLFunctions.LookupWhseShptHeader(Rec);
                    end;
                }
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Warehouse Comment Sheet";
                    RunPageLink = "Table Name" = CONST("Whse. Shipment"),
                                  Type = CONST(" "),
                                  "No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';
                }
                action("Pick Lines")
                {
                    Caption = 'Pick Lines';
                    Image = PickLines;
                    RunObject = Page "Warehouse Activity Lines";
                    RunPageLink = "Whse. Document Type" = CONST(Shipment),
                                  "Whse. Document No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Document No.", "Whse. Document Type", "Activity Type")
                                  WHERE("Activity Type" = CONST(Pick));
                    ApplicationArea = All;
                    ToolTip = 'Executes the Pick Lines action.';
                }
                action("Registered P&ick Lines")
                {
                    Caption = 'Registered P&ick Lines';
                    Image = RegisteredDocs;
                    RunObject = Page "Registered Whse. Act.-Lines";
                    RunPageLink = "Whse. Document No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Document Type", "Whse. Document No.", "Whse. Document Line No.")
                                  WHERE("Whse. Document Type" = CONST(Shipment));
                    ApplicationArea = All;
                    ToolTip = 'Executes the Registered P&ick Lines action.';
                }
                action("Posted &Whse. Shipments")
                {
                    Caption = 'Posted &Whse. Shipments';
                    Image = PostedReceipt;
                    RunObject = Page "Posted Whse. Shipment List";
                    RunPageLink = "Whse. Shipment No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Shipment No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Posted &Whse. Shipments action.';
                }
            }
        }
        area(processing)
        {
            group("F&unctions")
            {
                Caption = 'F&unctions';
                Image = "Action";
                action(UseFiltersToGetSrcDocs)
                {
                    Caption = 'Use Filters to Get Src. Docs.';
                    Ellipsis = true;
                    Image = UseFilters;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Use Filters to Get Src. Docs. action.';

                    trigger OnAction()
                    var
                        GetSourceDocOutbound: Codeunit "Get Source Doc. Outbound";
                    begin
                        Rec.TESTFIELD(Status, rec.Status::Open);
                        GetSourceDocOutbound.GetOutboundDocs(Rec);
                    end;
                }
                action("TEST AD")
                {
                    ApplicationArea = All;
                    ToolTip = 'Executes the TEST AD action.';

                    trigger OnAction()
                    begin
                        Focus := 'ID1000000025;MD' + FORMAT(TIME, 0, 1)
                    end;
                }
                action("Get Source Documents")
                {
                    Caption = 'Get Source Documents';
                    Ellipsis = true;
                    Image = GetSourceDoc;
                    Promoted = true;
                    PromotedCategory = Process;
                    ShortCutKey = 'Shift+F11';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Get Source Documents action.';

                    trigger OnAction()
                    var
                        GetSourceDocOutbound: Codeunit "Get Source Doc. Outbound";
                    begin
                        rec.TESTFIELD(Status, rec.Status::Open);
                        GetSourceDocOutbound.GetSingleOutboundDoc(Rec);
                    end;
                }
                separator("Separator")
                {
                }
                action("Re&lease")
                {
                    Caption = 'Re&lease';
                    Image = ReleaseDoc;
                    Promoted = true;
                    PromotedCategory = Process;
                    ShortCutKey = 'Ctrl+F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&lease action.';

                    trigger OnAction()
                    var
                        ReleaseWhseShptDoc: Codeunit "Whse.-Shipment Release";
                    begin
                        CurrPage.UPDATE(TRUE);
                        IF rec.Status = rec.Status::Open THEN
                            ReleaseWhseShptDoc.Release(Rec);
                    end;
                }
                action("Re&open")
                {
                    Caption = 'Re&open';
                    Image = ReOpen;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&open action.';

                    trigger OnAction()
                    var
                        ReleaseWhseShptDoc: Codeunit "Whse.-Shipment Release";
                    begin
                        ReleaseWhseShptDoc.Reopen(Rec);
                    end;
                }
                separator(" ")
                {
                }
                action("Autofill Qty. to Ship")
                {
                    Caption = 'Autofill Qty. to Ship';
                    Image = AutofillQtyToHandle;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Autofill Qty. to Ship action.';

                    trigger OnAction()
                    begin
                        ERROR('Ne pas Utiliser cette fonction !'); // AD Le 23-03-2016 => Car des fois les quantités repassent à la qte expédiée,
                        AutofillQtyToHandle();
                    end;
                }
                action("Delete Qty. to Ship")
                {
                    Caption = 'Delete Qty. to Ship';
                    Image = DeleteQtyToHandle;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Delete Qty. to Ship action.';

                    trigger OnAction()
                    begin
                        DeleteQtyToHandle();
                    end;
                }
                separator("  ")
                {
                }
                action("Create Pick")
                {
                    Caption = 'Create Pick';
                    Ellipsis = true;
                    Image = CreateInventoryPickup;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Create Pick action.';

                    trigger OnAction()
                    begin
                        CurrPage.UPDATE(TRUE);
                        CurrPage.WhseShptLines.PAGE.PickCreate();
                    end;
                }
                separator("---------------")
                {
                    Caption = '---------------';
                }
                action("Packing List")
                {
                    Caption = 'Packing List';
                    Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
                    Image = CreateWarehousePick;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Packing List action.';

                    trigger OnAction()
                    var
                        lPackingListHeader: Record "Packing List Header";
                        lPackingListMgmt: Codeunit "Packing List Mgmt";
                        lPackingListHeaderPage: Page "Packing List Header";
                    begin
                        IF lPackingListMgmt.getPackingListForWhseShipment(lPackingListHeader, rec."No.") THEN BEGIN
                            CLEAR(lPackingListHeaderPage);
                            lPackingListHeader.FILTERGROUP(2);
                            lPackingListHeader.SETRANGE("No.", lPackingListHeader."No.");
                            lPackingListHeader.SETRECFILTER();
                            lPackingListHeader.FILTERGROUP(0);
                            lPackingListHeaderPage.SETRECORD(lPackingListHeader);
                            lPackingListHeaderPage.RUN();
                        END;
                    end;
                }
                action("Packing List ***OLD***")
                {
                    Caption = 'Packing List ***OLD***';
                    Description = 'Ancienne version - non utilisée';
                    RunObject = Page "Packing List";
                    RunPageLink = "N° expedition magasin" = FIELD("No.");
                    RunPageView = SORTING("N° expedition magasin", "N° ligne");
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Packing List ***OLD*** action.';
                }
                separator("   ")
                {
                }
                action("Génération Modula")
                {
                    Caption = 'Génération Modula';
                    Image = CalculateWarehouseAdjustment;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Génération Modula action.';

                    trigger OnAction()
                    begin
                        GénértionBPSYMTA('MODULA');
                    end;
                }
                action("Génération Etiquette")
                {
                    Caption = 'Génération Etiquette';
                    Image = PrintVoucher;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Génération Etiquette action.';

                    trigger OnAction()
                    begin
                        GénértionBPSYMTA('ETIQUETTE');
                    end;
                }
            }
            group("P&osting")
            {
                Caption = 'P&osting';
                Image = Post;
                action("P&ost Shipment")
                {
                    Caption = 'P&ost Shipment';
                    Ellipsis = true;
                    Image = PostOrder;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'Shift+F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the P&ost Shipment action.';

                    trigger OnAction()
                    begin
                        // CFR 07/12/2021 - FE20190930 : D‚tail des colis
                        VerifierColisage();

                        PostShipmentYesNo();
                    end;
                }
                action("Post and &Print")
                {
                    Caption = 'Post and &Print';
                    Ellipsis = true;
                    Image = PostPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Post and &Print action.';

                    trigger OnAction()
                    begin
                        // CFR 07/12/2021 - FE20190930 : D‚tail des colis
                        VerifierColisage();

                        PostShipmentPrintYesNo();
                        VideEcran(); // ESK
                    end;
                }
            }
            action("&Print")
            {
                Caption = '&Print';
                Ellipsis = true;
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Print action.';

                trigger OnAction()
                begin
                    WhseDocPrint.PrintShptHeader(Rec);
                end;
            }
        }
    }

    trigger OnAfterGetCurrRecord()
    var
        WhseEmployee: Record "Warehouse Employee";
    begin
        // MC Le 07-06-2011 => SYMTA -> Possibilité de saisir le pied de BL sur le BP
        IF WhseEmployee.GET(USERID, 'SP') THEN;
        SetHideInfoBL(WhseEmployee."Validation Logistique Direct");
    end;

    trigger OnAfterGetRecord()
    var
        WhseEmployee: Record "Warehouse Employee";
    begin
        //SETRANGE("No.");

        // MC Le 07-06-2011 => SYMTA -> Possibilité de saisir le pied de BL sur le BP
        IF WhseEmployee.GET(USERID, 'SP') THEN;
        SetHideInfoBL(WhseEmployee."Validation Logistique Direct");
    end;

    trigger OnFindRecord(Which: Text): Boolean
    begin
        EXIT(rec.FindFirstAllowedRec(Which));
    end;

    trigger OnInit()
    begin
        txtPortVisible := TRUE;
        BlnSaturdayVisible := TRUE;
        blnInsuranceVisible := TRUE;
        CdEmbVisible := TRUE;
        NbColisVisible := TRUE;

        DetailUMVisible := TRUE;
        PoidsEditable := FALSE;
    end;

    trigger OnNextRecord(Steps: Integer): Integer
    begin
        EXIT(rec.FindNextAllowedRec(Steps));
    end;

    trigger OnOpenPage()
    var
        WhseEmployee: Record "Warehouse Employee";
    /*      LShipLine: Record "Warehouse Shipment Line";
     lUserPersonalization: Record "User Personalization";
       "--- LVArESKAPE ---": Integer; */
    begin
        rec.ErrorIfUserIsNotWhseEmployee();

        // CFR le 15/12/2020 : Gestion des appels multiples
        IF (Rec."No." = '') THEN BEGIN
            // cas de l'appel via Action de la page 9008 (Tableau de bord magasinier >> initialisation à vide)
            CLEAR(Rec);
            rec.SETRANGE("No.", '');
        END
        ELSE BEGIN
            // Cas de l'appel via Action de la Page 7341 (Afficher doc entrepôt >> précise le n° de doc)
            // CFR le 10/03/2021 : cette fonction est appelée dans le OnAction : SetNoFlash(Rec."No.");
            // Cas de l'appel via CardPageID de la Page 50217 (My Warehouse Shipment List)
            // CFR le 10/03/2021 : pas besoin de SetNoFlash() sinon ça bloque la navigation
        END;
        // FIN CFR le 15/12/2020 : Gestion des appels multiples

        // MC Le 26-10-2011 => Initialisation du formulaire.
        IF initialisation THEN
            GetNoFlasher(NoFlasher);

        // MC Le 07-06-2011 => SYMTA -> Possibilité de saisir le pied de BL sur le BP
        WhseEmployee.GET(USERID, 'SP');
        SetHideInfoBL(WhseEmployee."Validation Logistique Direct");

        Focus := 'ST1000000011;MD' + FORMAT(TIME, 0, 1)
    end;

    var
        WhseDocPrint: Codeunit "Warehouse Document-Print";
        decPortHT: Decimal;
        NoFlasher: Code[20];
        initialisation: Boolean;
        NbColisVisible: Boolean;
        CdEmbVisible: Boolean;
        blnInsuranceVisible: Boolean;
        BlnSaturdayVisible: Boolean;
        txtPortVisible: Boolean;
        Focus: Text[50];
        DetailUMVisible: Boolean;
        PoidsEditable: Boolean;

    local procedure AutofillQtyToHandle()
    begin
        CurrPage.WhseShptLines.PAGE.AutofillQtyToHandle();
    end;

    local procedure DeleteQtyToHandle()
    begin
        CurrPage.WhseShptLines.PAGE.DeleteQtyToHandle();
    end;

    local procedure PostShipmentYesNo()
    begin
        CurrPage.WhseShptLines.PAGE.PostShipmentYesNo();
    end;

    local procedure PostShipmentPrintYesNo()
    begin
        CurrPage.WhseShptLines.PAGE.PostShipmentPrintYesNo();
    end;

    procedure "--- FctESKAPE ---"()
    begin
    end;

    procedure "GénértionBPSYMTA"(_Type: Code[20])
    var
        TempWhseShipHeader: Record "Warehouse Shipment Header" temporary;
        GestionBP: Codeunit "Gestion Bon Préparation";
    begin
        TempWhseShipHeader := Rec;
        TempWhseShipHeader.Insert();

        CASE _Type OF
            'MODULA':
                GestionBP.ExportBlsModula(TempWhseShipHeader);
            'ETIQUETTE':
                GestionBP.ImprimeEtiquettePrepa(TempWhseShipHeader, 0); // WIIO => Ajout du 0 en dernier paramètre
        END;
    end;

    procedure SetHideInfoBL(bln_IsVisible: Boolean)
    begin
        // ** Cette fonction permet d'afficher ou non les infos concernant la mise à jour du pied de BL en fonction d'un booléen passé **//
        // MC Le 07-06-2011 => SYMTA -> Possibilité de saisir le pied de BL sur le BP
        NbColisVisible := bln_IsVisible;
        CdEmbVisible := bln_IsVisible;
        blnInsuranceVisible := bln_IsVisible;
        BlnSaturdayVisible := bln_IsVisible;
        txtPortVisible := bln_IsVisible;

        // CFR le 14/01/2021 - FE20190930 : La saisie du poids du BP est d‚sormais fonction de [WhseEmployee."Validation Logistique Direct"]
        DetailUMVisible := bln_IsVisible; //Ne fonctionne pas >> AND (Rec."Nb Of Box" > 1);
        PoidsEditable := (NOT bln_IsVisible) OR (Rec."Nb Of Box" <= 1);
        //MESSAGE('DetailUMVisible : %1 - PoidsEditable : %2', DetailUMVisible, PoidsEditable);
        // FIN CFR le 14/01/2021 - FE20190930

        //CurrPage.UPDATE(FALSE);
    end;

    procedure GetNoFlasher(_pCodeNoFlasher: Code[20])
    begin
        rec.RESET();
        rec.SETRANGE("No.", _pCodeNoFlasher);
        IF NOT rec.FINDFIRST() THEN BEGIN
            MESSAGE('Inconnu !');
            CurrPage.ACTIVATE();
        END
        ELSE
            CurrPage.UPDATE(FALSE);
    end;

    procedure VideEcran()
    begin
        CLEAR(Rec);
        rec.SETRANGE("No.", '');
        NoFlasher := '';
    end;

    procedure SetNoFlash(WhseNo: Code[20])
    begin
        // MC Le 26-10-2011 => Initialisation du formulaire.
        NoFlasher := WhseNo;
        initialisation := TRUE;
    end;

    local procedure SortingMethodOnAfterValidate()
    begin
        CurrPage.UPDATE();
    end;

    local procedure ZoneCodeOnAfterValidate()
    var
        WhseEmployee: Record "Warehouse Employee";
    begin
        // MC Le 07-06-2011 => SYMTA -> Possibilité de saisir le pied de BL sur le BP
        WhseEmployee.GET(USERID, 'SP');
        SetHideInfoBL(WhseEmployee."Validation Logistique Direct");
    end;

    local procedure NoFlasherOnAfterValidate()
    begin
        GetNoFlasher(NoFlasher);

        // CFR le 19/01/2022 - FE20190930 : On focalise soit sur le poids (ID1000000025) soit sur le nombre de colis (ID1000000008)
        IF NbColisVisible THEN
            Focus := 'ID1000000008;MD' + FORMAT(TIME, 0, 1)
        ELSE
            Focus := 'ID1000000025;MD' + FORMAT(TIME, 0, 1);
        // FIN CFR le 19/01/2022
    END;

    LOCAL PROCEDURE VerifierColisage(): Boolean;
    VAR
        lWarehouseEmployee: Record "Warehouse Employee";
        lTeliaeUMDetail: Record "Teliae UM Detail";
        //lWarning01: Label 'The sum of the weights (%1 packages) must be equal to %2 kg (actually : %3).', comment = 'FRA=La somme des poids des %1 colis doit être égale … %2 kg (actuellement saisi : %3).';
        lWarning02Err: Label 'You must enter a weight on all packages', comment = 'FRA=Vous devez saisir un poids sur tous les colis.';
    BEGIN
        // CFR 07/12/2021 - FE20190930 : D‚tail des colis
        IF lWarehouseEmployee.GET(USERID, Rec."Location Code") THEN
            IF lWarehouseEmployee."Validation Logistique Direct" THEN BEGIN
                IF (Rec."Nb Of Box" > 1) THEN BEGIN
                    lTeliaeUMDetail.SETRANGE("No. BP", Rec."No.");
                    // V1 : Poids total
                    /*
                    lTeliaeUMDetail.CALCSUMS("Weight UM (Kg)");
                        IF (Rec.Poids <> lTeliaeUMDetail."Weight UM (Kg)") THEN
                            ERROR(lWarning01, lTeliaeUMDetail.COUNT(), Rec.Poids, lTeliaeUMDetail."Weight UM (Kg)");
                    */
                    // V2 : Pas de poids … 0
                    lTeliaeUMDetail.SETRANGE("Weight UM (Kg)", 0);
                    IF (NOT lTeliaeUMDetail.ISEMPTY()) THEN
                        ERROR(lWarning02Err);
                END;
            END;
    end;

    // CFR 07/12/2021 - FE20190930 : D‚tail des colis
    // CFR le 14/01/2022 - FE20190930 : La saisie du poids du BP est d‚sormais fonction de [WhseEmployee."Validation Logistique Direct"]
    // CFR le 19/01/2022 - FE20190930 : On focalise soit sur le poids (ID1000000025) soit sur le nombre de colis (ID1000000008)
}

