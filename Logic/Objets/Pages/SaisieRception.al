page 50051 "Saisie Réception"
{
    ApplicationArea = all;
    CardPageID = "Saisie Réception";
    PageType = Document;

    layout
    {
        area(content)
        { //TODO 
            // usercontrol(CtlFocus; "Ahead_EXFocus")
            // {
            //     ApplicationArea = All;
            // }
            group("Général")
            {
                Caption = 'Général';
                field(CodeUtilisateur; CodeUtilisateur)
                {
                    Caption = 'Utilisateur';
                    TableRelation = "Generals Parameters".Code WHERE(Type = CONST('MAG_EMPLOYES'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur field.';
                }
                field(NoFusion; NoFusion)
                {
                    Caption = 'N° Réception';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Réception field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        Reception: Record "Warehouse Receipt Header";
                        FrmReception: Page "Warehouse Receipts";
                    begin
                        Reception.RESET();
                        // On ne veut pas les réceptions fusionnées.
                        Reception.SETRANGE(Reception.Fusion, FALSE);
                        CLEAR(FrmReception);
                        FrmReception.SETTABLEVIEW(Reception);
                        FrmReception.LOOKUPMODE(TRUE);
                        IF FrmReception.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmReception.GETRECORD(Reception);
                            NoFusion := Reception."No.";
                            ValidateNoFusion();
                        END
                    end;

                    trigger OnValidate()
                    begin
                        ValidateNoFusion();
                    end;
                }
                field(NomFrn; NomFrn)
                {
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NomFrn field.';
                }
                field(GenImprimerEtiquetteParArticle; GenImprimerEtiquetteParArticle)
                {
                    Caption = 'Imprimer Etiquette Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Etiquette Article field.';
                }
                field(ImprimerRéférenceSP; ImprimerRéférenceSP)
                {
                    Caption = 'Imprimer Réf. SP';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Réf. SP field.';
                }
                field("Compteur"; FORMAT(NbArtRecus) + '   /   ' + FORMAT(NbArtARecevoir))
                {
                    Caption = 'Compteur';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compteur field.';
                }
                field(ImprimanteEtiquette; ImprimanteEtiquette)
                {
                    Caption = 'Imprimante';
                    TableRelation = "Generals Parameters".Code WHERE(Type = CONST('IMP_ETQ_RECEPT'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimante field.';
                }
            }
            group(Article)
            {
                Caption = 'Article';
                field(NoArticle; NoArticle)
                {
                    Caption = 'N° Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        Item: Record Item;
                        FrmItem: Page "Item List";
                    begin
                        Item.RESET();
                        CLEAR(FrmItem);
                        FrmItem.SETTABLEVIEW(Item);
                        FrmItem.LOOKUPMODE(TRUE);
                        IF FrmItem.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmItem.GETRECORD(Item);
                            NoArticle := Item."No. 2";
                            ValidateNoArticle();
                        END
                    end;

                    trigger OnValidate()
                    begin
                        ValidateNoArticle();
                    end;
                }
                field(NoArticleInterne; NoArticleInterne)
                {
                    Caption = 'N° Article Interne';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article Interne field.';
                }
                field(ItemDescription; Item.Description)
                {
                    Caption = 'Désignation';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field(ItemDescription2; Item."Description 2")
                {
                    Caption = 'Désignation 2';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation 2 field.';
                }
                field(CtrlPoidsArticle; PoidsArticle)
                {
                    Caption = 'Poids Net';
                    DecimalPlaces = 2 :;
                    Editable = CtrlPoidsArticleEditable;
                    Enabled = CtrlPoidsArticleEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids Net field.';

                    trigger OnValidate()
                    begin
                        UpdatePoidsArticle()
                    end;
                }
                field(ItemInventory; Item.Inventory)
                {
                    Caption = 'Physique';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Physique field.';
                }
                field(ItemQtyonSalesOrder; Item."Qty. on Sales Order")
                {
                    Caption = 'Qté sur Cde Vente';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté sur Cde Vente field.';

                    trigger OnDrillDown()
                    var
                        LSalesLine: Record "Sales Line";
                        LFrmSalesLine: Page "Sales Lines";
                    begin
                        IF NoArticle <> '' THEN BEGIN
                            LSalesLine.INIT();
                            LSalesLine.RESET();
                            LSalesLine.SETRANGE(LSalesLine."Document Type", LSalesLine."Document Type"::Order);
                            LSalesLine.SETRANGE(LSalesLine.Type, LSalesLine.Type::Item);
                            LSalesLine.SETRANGE(LSalesLine."No.", NoArticleInterne);
                            LSalesLine.SETCURRENTKEY(LSalesLine."Document Type", LSalesLine.Type, LSalesLine."No.");

                            LFrmSalesLine.EDITABLE(FALSE);
                            LFrmSalesLine.SETTABLEVIEW(LSalesLine);
                            LFrmSalesLine.RUNMODAL();
                        END
                    end;
                }
                field(ItemStocké; Item.Stocké)
                {
                    Caption = 'Stocké';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field(Comment; Comment)
                {
                    Caption = 'Commentaires';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires field.';
                }
                field(ImprimerEtiquetteParArticle; ImprimerEtiquetteParArticle)
                {
                    Caption = 'Imprimer Etiquette Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Etiquette Article field.';
                }
                field(NbARecevoir; NbARecevoir)
                {
                    Caption = 'A Réceptionner';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the A Réceptionner field.';
                }
                field(NbRecus; NbRecus)
                {
                    Caption = 'Déjà Réceptionné';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Déjà Réceptionné field.';
                }
                field(QteReçue; QteReçue)
                {
                    Caption = 'Quantité reçue';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité reçue field.';

                    trigger OnValidate()
                    begin
                        IF (QteReçue <> 0) AND (PoidsArticle = 0) THEN
                            ERROR('Le poids est obligatoire');
                    end;
                }
                field(YourComment; YourComment)
                {
                    Caption = 'Votre commentaire';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Votre commentaire field.';
                }
                field(EmplacementRangement; EmplacementRangement)
                {
                    Caption = 'Emplacement';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Emplacement field.';
                }
                field(HorsReception; HorsReception)
                {
                    Editable = false;
                    Enabled = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the HorsReception field.';
                }
                field(capacité; capacité)
                {
                    Caption = 'Capacité casier';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Capacité casier field.';
                }
                field("Sales multiple"; Item."Sales multiple")
                {
                    Caption = 'Multiple de vente';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple de vente field.';
                }
            }
            group(Lignes)
            {
                Caption = 'Lignes';
                part(SubForm; 50052)
                {
                    SubPageView = SORTING(Default, "Location Code", "Item No.", "Variant Code", "Bin Code")
                                  ORDER(Ascending);
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action(Valider)
                {
                    Caption = 'Valider';
                    Image = PostPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Valider action.';

                    trigger OnAction()
                    begin
                        EnregistrerLigne();

                        // AD Le 07-12-2015 => Gestion du focus
                        Focus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);
                    end;
                }
            }
        }
        area(navigation)
        {
            action("Liste articles")
            {
                Caption = 'Liste articles';
                Image = Item;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Liste articles action.';

                trigger OnAction()
                begin
                    ListeArticles(NoFusion);
                end;
            }
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                var
                    FrmQuid: Page "Multi -consultation";
                begin

                    FrmQuid.InitArticle(NoArticle);
                    FrmQuid.RUNMODAL();
                end;
            }
            action("Etat des réceptions")
            {
                Image = ConfirmAndPrint;
                Promoted = true;
                PromotedCategory = "Report";
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Etat des réceptions action.';

                trigger OnAction()
                var
                    LReceipt: Record "Warehouse Receipt Header";
                    Etatdesreceptions: Report "Etat des réceptions";
                begin
                    CLEAR(LReceipt);
                    IF NoFusion <> '' THEN
                        LReceipt.SETRANGE("No.", NoFusion);

                    Etatdesreceptions.SETTABLEVIEW(LReceipt);
                    Etatdesreceptions.RUNMODAL();
                end;
            }
            action("Détail saisie réception")
            {
                Image = GetLines;
                Promoted = true;
                PromotedCategory = "Report";
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Détail saisie réception action.';

                trigger OnAction()
                var
                    recL_DetailSaisie: Record "Saisis Réception Magasin";
                    pgeL_DetailSaisie: Page "Detail Saisie Reception";
                begin
                    recL_DetailSaisie.RESET();
                    recL_DetailSaisie.SETRANGE("No.", NoFusion);
                    pgeL_DetailSaisie.SETTABLEVIEW(recL_DetailSaisie);
                    pgeL_DetailSaisie.RUN();
                end;
            }
        }
    }

    trigger OnInit()
    begin
        CtrlPoidsArticleEditable := TRUE;
    end;

    trigger OnOpenPage()
    begin
        ViderInfoLigne();
        NbArtARecevoir := 0;
        NbArtRecus := 0;
        //CodeUtilisateur := USERID;

        // AD Le 07-12-2015 => Gestion du focus
        Focus := 'ST1000000003;MD' + FORMAT(TIME, 0, 1);
    end;

    var
        Item: Record Item;
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        NoFusion: Code[20];
        NoArticle: Code[40];
        NoArticleInterne: Code[20];
        "QteReçue": Decimal;
        EmplacementRangement: Code[10];
        PoidsArticle: Decimal;

        GenImprimerEtiquetteParArticle: Boolean;
        ImprimerEtiquetteParArticle: Boolean;
        "ImprimerRéférenceSP": Boolean;
        ImprimanteEtiquette: Code[20];
        CodeUtilisateur: Code[20];
        "---": Integer;
        NbArtARecevoir: Decimal;
        NbArtRecus: Decimal;
        ESK001Lbl: Label 'No Réception incorrect !';
        NbARecevoir: Decimal;
        NbRecus: Decimal;
        HorsReception: Code[40];

        ESK002Lbl: Label 'HORS RECEPTION';
        ESK003Qst: Label 'Enregistrer la ligne ?';
        //ESK004: Label 'Veuillez d''abord choisir une réception.';
        ESK005Lbl: Label 'Article %1 n''existe pas.', Comment = '%1 = No Article';
        Comment: Text[80];
        ESK006Qst: Label 'Référence à marquer !!';
        ESK007Lbl: Label 'Référence à controler !! Statut : %1.', Comment = '%1 = Statut';
        ESK008Lbl: Label 'Le multiple d''achat pour l''article est %1.', Comment = '%1 = Multiple d''achat';
        ESK009Err: Label 'Veuillez d''abord choisir un article.';
        ESK010Qst: Label 'Voulez vous modifier le poids de l''article à %1 ?', Comment = '%1 = Poids Article';
        NomFrn: Text[50];
        ESK011Err: Label 'Vous devez choisir une réception !';
        "capacité": Decimal;
        CtrlPoidsArticleEditable: Boolean;
        Focus: Text[50];
        YourComment: Text[80];
        CONFIRM001Qst: Label 'Voulez-vous commencer le pointage de cette reception pour le fournisseur (%1) %2 ?', Comment = '%1 = Fournisseur ; %2 = Récéption';

    procedure ViderInfoLigne()
    begin
        CLEAR(Item);
        NoArticle := '';
        NoArticleInterne := '';

        Jauge(NoFusion, NbArtARecevoir, NbArtRecus);


        NbARecevoir := 0;
        NbRecus := 0;
        QteReçue := 0;
        EmplacementRangement := '';
        HorsReception := '';
        Comment := '';
        PoidsArticle := 0;
        // MCO Le 08-06-2017 => Régie
        YourComment := '';


        CurrPage.SubForm.PAGE.RazListe()
    end;

    procedure ValidateNoFusion()
    var
        Reception: Record "Warehouse Receipt Header";
        LCommentLine: Record "Comment Line";
        LFrmCommentLine: Page "Comment Sheet";
    begin
        IF NoFusion = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            Reception.GET(NoFusion);
            IF (Reception."N° Fusion Réception" <> '') THEN
                IF Reception."N° Fusion Réception" <> Reception."No." THEN BEGIN
                    NoFusion := '';
                    ERROR(ESK001Lbl);
                END;
            // Gestion de la traçabilité
            IF NOT Reception."En réception" THEN BEGIN
                // LM le 05-07-2013
                //IF CONFIRM('Voulez-vous commencer le pointage de cette reception?') THEN
                //CFR le 19/11/2020 : Régie > Sasie réception : nom fournisseur
                IF CONFIRM(STRSUBSTNO(CONFIRM001Qst, Reception.GetVendorNo(), Reception.GetVendorName())) THEN BEGIN
                    Reception."En réception" := TRUE;
                    Reception.MODIFY();
                END;
            END;
            MessageConditionnement();
            Jauge(NoFusion, NbArtARecevoir, NbArtRecus);
            NomFrn := Reception.GetVendorName();
            // MCO Le 24-03-2016
            LCommentLine.RESET();
            LCommentLine.SETRANGE("Table Name", LCommentLine."Table Name"::Vendor);
            LCommentLine.SETRANGE("No.", Reception.GetVendorNo());
            LCommentLine.SETRANGE("Display Purch. Receipt", TRUE);
            IF LCommentLine.COUNT > 0 THEN BEGIN
                LFrmCommentLine.EDITABLE(FALSE);
                LFrmCommentLine.SETTABLEVIEW(LCommentLine);
                LFrmCommentLine.RUN();
            END;
            // FIN MCO Le 24-03-2016
        END;
    end;

    procedure ValidateNoArticle()
    var
        BinContent: Record "Bin Content";
        Reception: Record "Warehouse Receipt Header";
        TempCommentLine: Record "Comment Line" temporary;
        cduL_GestionSYMTA: Codeunit "Gestion SYMTA";
        ok: Boolean;
        txtMessage: Text[80];

    begin

        //IF NoFusion = '' THEN BEGIN
        //  ViderInfoLigne();
        //  ERROR(ESK004);
        //END;

        ImprimerEtiquetteParArticle := GenImprimerEtiquetteParArticle;

        IF NoArticle = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            NoArticleInterne := GestionMultiRef.RechercheMultiReference(NoArticle);
            IF NoArticleInterne <> 'RIENTROUVE' THEN
                Item.GET(NoArticleInterne)
            ELSE BEGIN
                ViderInfoLigne();
                ERROR(STRSUBSTNO(ESK005Lbl, NoArticle));
            END;

            // MC Le 26-10-11 => Gestion de l'unité.
            MessageConditionnement();

            PoidsArticle := Item."Net Weight";
            CtrlPoidsArticleEditable := PoidsArticle = 0;


            ok := FALSE;
            IF Item."Marquage réception" THEN
                WHILE NOT ok DO
                    ok := CONFIRM(ESK006Qst, FALSE);

            Item.CALCFIELDS(Inventory, "Qty. on Sales Order");

            CurrPage.SubForm.PAGE.FiltreArticle(NoArticleInterne);
            QteArticle(NoFusion, NoArticleInterne);

            BinContent.RESET();

            IF NoFusion <> '' THEN BEGIN
                Reception.GET(NoFusion);
                BinContent.SETRANGE("Location Code", Reception."Location Code");
            END;

            BinContent.SETRANGE("Item No.", Item."No.");
            BinContent.SETRANGE(Default, TRUE);
            IF BinContent.FINDFIRST() THEN
                EmplacementRangement := BinContent."Bin Code";
            capacité := BinContent.capacité;

            // Récupération du commentaire
            IF NoFusion <> '' THEN
                Comment := RécupèreCommentaire(NoFusion, Item."No.");

            IF Comment <> '' THEN
                MESSAGE(Comment);

            // Verif si a controler
            ok := FALSE;
            IF VerifSiRefAControler(Item."No.", NoFusion, txtMessage) THEN
                WHILE NOT ok DO
                    ok := CONFIRM(STRSUBSTNO(ESK007Lbl, txtMessage), FALSE);

            // MCO Le 24-03-2016 => Nouvelle gestion des commentaires
            // Ancien code
            /*
           // Commentaire Articles
           LCommentLine.RESET();
           LCommentLine.SETRANGE("Table Name", LCommentLine."Table Name"::Item);
           LCommentLine.SETRANGE("No.", Item."No.");
           LCommentLine.SETRANGE("Display Purch. Receipt",TRUE);
           IF LCommentLine.COUNT >0 THEN
             BEGIN
               LFrmCommentLine.EDITABLE(FALSE);
               LFrmCommentLine.SETTABLEVIEW(LCommentLine);
               LFrmCommentLine.RUNMODAL();
             END;



           //commentaire commande achat
           WareReceiptLine.RESET();
           WareReceiptLine.SETRANGE(WareReceiptLine."N° Fusion Réception", NoFusion);
           WareReceiptLine.SETRANGE(WareReceiptLine."Item No.",NoArticleInterne);
           IF WareReceiptLine.FINDFIRST () THEN
             REPEAT
               PuchCommentLine.RESET();
               PuchCommentLine.SETRANGE(PuchCommentLine."No.",WareReceiptLine."Source No.");
               PuchCommentLine.SETRANGE(PuchCommentLine."Document Line No.",WareReceiptLine."Source Line No.");
               PuchCommentLine.SETRANGE(PuchCommentLine."Display Receipt",TRUE);
               IF PuchCommentLine.FINDFIRST () THEN
                 REPEAT
                   MESSAGE(PuchCommentLine.Comment);
                 UNTIL PuchCommentLine.NEXT=0;
             UNTIL WareReceiptLine.NEXT=0;
       */
            // Nouveau code

            // On charge dans une temporaire les 3 sortes de commentaires en excluant les doublons
            TempCommentLine.RESET();
            cduL_GestionSYMTA.GetCommentaireReception(TempCommentLine, NoArticleInterne, NoFusion, FALSE);
            IF NOT TempCommentLine.ISEMPTY THEN
                PAGE.RUN(PAGE::"Comment Sheet", TempCommentLine);
            // MCO Le 24-03-2016 => Nouvelle gestion des commentaires



        END;

    end;

    procedure MessageConditionnement()
    var
        ItemVendor: Record "Item Vendor";
        WhseReceipt: Record "Warehouse Receipt Header";
        ItemUnitOf: Record "Item Unit of Measure";
    begin
        // Cette fonction permet d'alerter l'utilisateur si l'unité d'achat pour le fournisseur de l'article est différente de 1 //
        IF WhseReceipt.GET(NoFusion) THEN BEGIN
            IF ItemVendor.GET(WhseReceipt.GetVendorNo(), NoArticleInterne, '') THEN BEGIN
                IF ItemUnitOf.GET(NoArticleInterne, ItemVendor."Purch. Unit of Measure") THEN BEGIN
                    IF ItemUnitOf."Qty. per Unit of Measure" <> 1 THEN BEGIN
                        MESSAGE(STRSUBSTNO(ESK008Lbl, ItemUnitOf."Qty. per Unit of Measure"));
                    END;
                END;
            END;
        END;
    end;

    procedure Jauge(_pNoFusion: Code[20]; var _pNbARecevoir: Decimal; var _pNbRecus: Decimal)
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
        TempMontantArticle: Record "Item Amount" temporary;
    begin
        // Cherche combien d'article a recevoir et conbien de recus
        TempMontantArticle.RESET();
        TempMontantArticle.DELETEALL();
        // Cherche le nb à recevoir
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                TempMontantArticle.RESET();
                TempMontantArticle.SETRANGE("Item No.", WhseReceiptLine."Item No.");
                IF TempMontantArticle.FINDFIRST() THEN
                    TempMontantArticle.RENAME(TempMontantArticle.Amount + WhseReceiptLine.Quantity, TempMontantArticle."Amount 2", TempMontantArticle."Item No."
              )
                ELSE BEGIN
                    TempMontantArticle.INIT();
                    TempMontantArticle.Amount := WhseReceiptLine."Qty. Outstanding (Base)";
                    TempMontantArticle."Amount 2" := 0;
                    TempMontantArticle."Item No." := WhseReceiptLine."Item No.";
                    TempMontantArticle.INSERT(TRUE);
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Cherche le nb à de recus complets
        TempMontantArticle.RESET();
        _pNbARecevoir := TempMontantArticle.COUNT;
        _pNbRecus := 0;
        IF TempMontantArticle.FINDFIRST() THEN
            REPEAT
                "Pré-reception".RESET();
                "Pré-reception".SETCURRENTKEY("No.", "Item No.");
                "Pré-reception".SETRANGE("No.", _pNoFusion);
                "Pré-reception".SETRANGE("Item No.", TempMontantArticle."Item No.");
                "Pré-reception".CALCSUMS(Quantity);
                IF "Pré-reception".Quantity >= TempMontantArticle.Amount THEN
                    _pNbRecus += 1;
            UNTIL TempMontantArticle.NEXT() = 0;
    end;

    procedure QteArticle(_pNoFusion: Code[20]; _pItemNo: Code[20])
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
    begin
        HorsReception := '';
        NbARecevoir := 0;
        NbRecus := 0;

        WhseReceiptLine.RESET();
        WhseReceiptLine.SETCURRENTKEY("Item No.", "N° Fusion Réception");
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.COUNT = 0 THEN BEGIN
            HorsReception := ESK002Lbl;
            MESSAGE(ESK002Lbl);
        END;

        // MC Le 07-11-2011 -> Si on utilise le champ "Qty. to receive" il faut le mettre dans le calcsum.
        WhseReceiptLine.CALCSUMS("Qty. Outstanding (Base)", "Qty. to Receive (Base)");
        // FIN MC Le 07-11-2011

        "Pré-reception".RESET();
        "Pré-reception".SETCURRENTKEY("No.", "Item No.");
        "Pré-reception".SETRANGE("No.", _pNoFusion);
        "Pré-reception".SETRANGE("Item No.", _pItemNo);
        "Pré-reception".CALCSUMS(Quantity);

        // AD Le 28-10-2011 => DEMANDE DE SYMTA
        //
        //NbARecevoir := WhseReceiptLine."Qty. Outstanding (Base)";
        NbARecevoir := WhseReceiptLine."Qty. to Receive (Base)";
        // FIN AD Le 28-10-2011
        NbRecus := "Pré-reception".Quantity;
    end;

    procedure UpdatePoidsArticle()
    var
        LItem: Record Item;
        LItemUOM: Record "Item Unit of Measure";
    begin
        IF NoArticle = '' THEN
            ERROR(ESK009Err);

        LItem.GET(NoArticleInterne);

        IF CONFIRM(ESK010QSt, TRUE, PoidsArticle) THEN BEGIN
            LItemUOM.GET(LItem."No.", LItem."Base Unit of Measure");
            LItemUOM.TESTFIELD(Weight, 0);
            LItemUOM.VALIDATE(Weight, PoidsArticle);
            LItemUOM.MODIFY(TRUE);
        END;
    end;

    procedure EnregistrerLigne()
    var
        "Pré-reception": Record "Saisis Réception Magasin";
        NoLigne: Integer;
    // rec_Whse: Record "Warehouse Receipt Header";
    begin
        IF NOT CONFIRM(ESK003QSt, TRUE) THEN
            EXIT;


        "Pré-reception".RESET();
        "Pré-reception".SETRANGE("No.", NoFusion);
        IF "Pré-reception".FINDLAST() THEN
            NoLigne := "Pré-reception"."Line No." + 1000
        ELSE
            NoLigne := 10000;

        CLEAR("Pré-reception");
        "Pré-reception".VALIDATE("No.", NoFusion);
        "Pré-reception".VALIDATE("Line No.", NoLigne);
        "Pré-reception".VALIDATE("Bin Code", EmplacementRangement);
        "Pré-reception".VALIDATE("Item No.", NoArticleInterne);
        "Pré-reception".VALIDATE(Quantity, QteReçue);
        "Pré-reception".VALIDATE("Utilisateur Modif", CodeUtilisateur);
        "Pré-reception".VALIDATE("Date Modif", TODAY);
        "Pré-reception".VALIDATE("Heure Modif", TIME);
        // MCO Le 08-06-2017 => Régie
        "Pré-reception".Commentaire := YourComment;
        // FIN MCO Le 08-06-2017

        IF NoFusion <> '' THEN
            "Pré-reception".INSERT(TRUE);

        // Modification de statut de la récéption.
        /*
        rec_Whse.RESET();
        IF rec_Whse.GET("Pré-reception"."No.") THEN
          BEGIN
            rec_Whse.Position := rec_Whse.Position::"2";
            rec_Whse.MODIFY();
          END;
        */

        IF ImprimanteEtiquette <> '' THEN
            // AD Le 31-03-2015
            //IF CONFIRM('Ancien Format ?', TRUE) THEN
            //  EditionEtiquette("Pré-reception", ImprimerEtiquetteParArticle, ImprimerRéférenceSP, ImprimanteEtiquette)
            //ELSE
            // FIN AD Le 31-03-2015
            EditionEtiquetteNiceLabel("Pré-reception", ImprimerEtiquetteParArticle, ImprimerRéférenceSP, ImprimanteEtiquette);

        ViderInfoLigne();

    end;

    procedure "RécupèreCommentaire"(_pNoFusion: Code[20]; _pNoArticle: Code[20]): Text[80]
    var
        rec_Comment: Record "Warehouse Comment Line";
    begin
        // Récupère le commentaire s'il existe.
        rec_Comment.SETRANGE("Table Name", rec_Comment."Table Name"::"Whse. Receipt");
        rec_Comment.SETRANGE(Type, rec_Comment.Type::"Pre-Receipt");
        rec_Comment.SETRANGE("No.", _pNoFusion);
        rec_Comment.SETRANGE("Item No.", _pNoArticle);
        IF rec_Comment.FINDFIRST() THEN
            EXIT(rec_Comment.Comment)
        ELSE
            EXIT('');
    end;

    procedure VerifSiRefAControler(_pItemNo: Code[20]; _pFusionNo: Code[20]; var _pTxtMsg: Text[80]): Boolean
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        PurchaseLine: Record "Purchase Line";
        ItemVendor: Record "Item Vendor";
    begin
        _pTxtMsg := '';

        // Verif pour la reception principale
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("No.", _pFusionNo);
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Verif pour les autres receptions
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pFusionNo);  // La modif est ici
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;
    end;

    procedure EditionEtiquette("_pPréReception": Record "Saisis Réception Magasin"; _pImprimerEtiquetteParArticle: Boolean; "_pImprimerRéférenceSP": Boolean; _pImprimante: Code[20])
    var
        LItem: Record Item;
        Imprimante: Record "Generals Parameters";
        NomFichier: Text[250];
        NomFichierTmp: Text[250];
        Fichier: File;
        //Ligne: Text[1024];
        Entier: Integer;
        _RefArticle: Code[20];
        _CBarre: Code[20];
        _Designation: Text[50];
        _NoReception: Code[20];
        _CodeArticle: Code[20];
        _AControler: Text[20];
        _AMarquer: Text[10];
        _Note: Code[10];
        _Qte: Code[10];
        _Emplacement: Code[10];
        i: Integer;
        txtMessage: Text[80];
    begin

        Imprimante.GET('IMP_ETQ_RECEPT', _pImprimante);
        NomFichier := Imprimante.LongDescription + '\' + _pPréReception."No." + '-' + FORMAT(_pPréReception."Line No.") + '.TXT';
        NomFichierTmp := TEMPORARYPATH + _pPréReception."No." + '-' + FORMAT(_pPréReception."Line No.") + '.TXT';



        Fichier.CREATE(NomFichierTmp);
        Fichier.TEXTMODE(TRUE);



        LItem.GET(_pPréReception."Item No.");

        // Remplissage des variables
        _RefArticle := GestionMultiRef.RechercheRefActive(Item."No.");
        _CBarre := _RefArticle;
        IF EVALUATE(Entier, _RefArticle) THEN
            _RefArticle := FORMAT(Entier, 0, '<Integer><1000Character, >')
        ELSE
            _RefArticle := GestionMultiRef.RechercheRefActive(Item."No.");

        _Designation := LItem.Description;
        _NoReception := _pPréReception."No.";
        _CodeArticle := _pPréReception."Item No.";
        _AControler := '';
        IF VerifSiRefAControler(_pPréReception."Item No.", _pPréReception."No.", txtMessage) THEN
            _AControler := 'A CONTROLER';

        _AMarquer := '';
        IF Item."Marquage réception" THEN
            _AMarquer := 'A***M';

        IF RécupèreCommentaire(_pPréReception."Item No.", _pPréReception."No.") <> '' THEN
            _Note := 'VOIR NOTE';

        _Qte := FORMAT(_pPréReception.Quantity);
        _Emplacement := _pPréReception."Bin Code";


        Fichier.WRITE('^XA');
        Fichier.WRITE('^LH10,10');
        Fichier.WRITE('^FO20,10^ABN,36,15^FD' + _RefArticle + '^FS');
        Fichier.WRITE('^FO20,50^A0,36,23^FD' + _Designation + '^FS');

        Fichier.WRITE('^FO20,90^A0,36,36^FDRec: ' + _NoReception + '^FS');
        Fichier.WRITE('^FO20,130^A0,36,36^FD' + _CodeArticle + '^FS');
        Fichier.WRITE('^FO20,170^A0,36,36^FD' + _AControler + '^FS');
        Fichier.WRITE('^FO20,210^A0,36,36^FD' + _Note + _AMarquer + '^FS');
        Fichier.WRITE('^FO20,250^A0,36,36^FDQte:' + _Qte + '^FS');
        Fichier.WRITE('^FO20,290^A0,36,36^FDCasier: ' + _Emplacement + '^FS');
        Fichier.WRITE('^XZ');

        IF ImprimerEtiquetteParArticle THEN BEGIN
            FOR i := 1 TO _pPréReception.Quantity DO BEGIN
                Fichier.WRITE('^XA');
                IF ImprimerRéférenceSP THEN
                    Fichier.WRITE('^FO10,20^FB300,1,0,C^A0,36,36^FD' + _CodeArticle + '^FS');

                Fichier.WRITE('^FO10,80^FB300,1,0,C^A0,36,36^FD' + _RefArticle + '^FS');

                IF _Designation <> '' THEN
                    Fichier.WRITE('^FO1,140^FB360,1,0,C^A0,36,23^FD' + _Designation + '^FS');

                Fichier.WRITE('^BCN,50,N,N,N,N');
                Fichier.WRITE('^FO10,200^BY2^FD' + _CBarre + '^XZ');
                Fichier.WRITE('^XZ');
            END;
        END;
        Fichier.CLOSE();
        FILE.RENAME(NomFichierTmp, NomFichier);
        IF UPPERCASE(USERID) = 'ESKAPE' THEN
            MESSAGE('%1--%2', NomFichierTmp, NomFichier);
    end;

    procedure ListeArticles(_pNoFusion: Code[20])
    var
        LWarehouseReceiptLine: Record "Warehouse Receipt Line";
        LFrmReceipt: Page "Whse. Receipt Lines";
    begin
        IF _pNoFusion = '' THEN
            ERROR(ESK011Err);

        LWarehouseReceiptLine.RESET();
        LWarehouseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        LFrmReceipt.EDITABLE(FALSE);
        LFrmReceipt.SETTABLEVIEW(LWarehouseReceiptLine);
        //LFrmReceipt.RUNMODAL();
        //FBRUN LE 07/11/2017
        LFrmReceipt.LOOKUPMODE := TRUE;
        IF LFrmReceipt.RUNMODAL() = ACTION::LookupOK THEN BEGIN
            LFrmReceipt.GETRECORD(LWarehouseReceiptLine);
            NoArticle := LWarehouseReceiptLine."Item No.";
            ValidateNoArticle();
        END;
        //FIN FBRUN LE 07/11/2017
    end;

    procedure EditionEtiquetteNiceLabel("_pPréReception": Record "Saisis Réception Magasin"; _pImprimerEtiquetteParArticle: Boolean; "_pImprimerRéférenceSP": Boolean; _pImprimante: Code[20])
    var
        LItem: Record Item;
        Imprimante: Record "Generals Parameters";
        LCuEtiquette: Codeunit "Etiquettes Articles";
        NomFichier: Text[250];
        Fichier: File;
        Entier: Integer;
        _RefArticle: Code[20];
        _CBarre: Code[20];
        _Designation: Text[50];
        _NoReception: Code[20];
        _NoReceptionNeutre: Code[20];
        _CodeArticle: Code[20];
        _AControler: Text[20];
        _AMarquer: Text[10];
        _Note: Code[10];
        //_Qte: Code[10];
        _Emplacement: Code[10];

        txtMessage: Text[80];
        // "---": Integer;

        LTabComment: array[3] of Text[30];
        LCondit: Integer;
    begin

        Imprimante.GET('IMP_ETQ_RECEPT', _pImprimante);
        NomFichier := LCuEtiquette.purgeUserId() + '_' + _pPréReception."No." + '-' + FORMAT(_pPréReception."Line No.") + '.TXT';

        LCuEtiquette.OuvrirEtiquetteNiceLabel(NomFichier, Fichier);

        LItem.GET(_pPréReception."Item No.");

        // Remplissage des variables
        _RefArticle := GestionMultiRef.RechercheRefActive(Item."No.");
        _CBarre := _RefArticle;

        IF EVALUATE(Entier, _RefArticle) THEN
            _RefArticle := FORMAT(Entier, 0, '<Integer><1000Character, >')
        ELSE
            _RefArticle := GestionMultiRef.RechercheRefActive(Item."No.");

        _Designation := LItem.Description;
        _NoReception := _pPréReception."No.";
        // CFR le 19/11/2020 : Régie > Etiquette Réception [RM] - _NoReceptionNeutre
        IF (COPYSTR(_pPréReception."No.", 1, 2) = 'RM') THEN
            _NoReceptionNeutre := COPYSTR(_pPréReception."No.", 3)
        ELSE
            _NoReceptionNeutre := _pPréReception."No.";
        // FIN CFR le 19/11/2020

        _CodeArticle := _pPréReception."Item No.";
        _AControler := '';
        IF VerifSiRefAControler(_pPréReception."Item No.", _pPréReception."No.", txtMessage) THEN
            _AControler := 'A CONTROLER';

        _AMarquer := '';
        IF Item."Marquage réception" THEN
            _AMarquer := 'A***M';

        IF RécupèreCommentaire(_pPréReception."Item No.", _pPréReception."No.") <> '' THEN
            _Note := 'VOIR NOTE';

        _Emplacement := _pPréReception."Bin Code";


        LTabComment[1] := _Note;
        LTabComment[2] := _AControler;
        LTabComment[3] := _AMarquer;

        //  AD Le 30-01-2020 => REGIE -> Pour certains articles, une étiquette par qté
        //IF NOT LItem."Une Etq Reception Par art" THEN
        LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, _pPréReception.Quantity, '', _NoReception, _Emplacement, LTabComment, '', 1  // AVANT
        , '45X45RECEP', _pImprimante); // AVANT
                                       /*ELSE
                                             LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, 1, '', _NoReception, _Emplacement, LTabComment, '',_pPréReception.Quantity
                                             ,'45X45RECEP', _pImprimante);*/

        CLEAR(LTabComment);
        IF ImprimerEtiquetteParArticle THEN BEGIN
            //CFR le 26/10/2023 : R‚gie > Bug Quantit‚ sur ‚tiquette article toujours 1 … la r‚ception
            //LCondit := LItem."Sales multiple";
            //IF LCondit = 0 THEN LCondit := 1;
            LCondit := 1;
            //FIN CFR le 26/10/2023

            // CFR le 19/11/2020 : Régie > Etiquette Réception [RM] - _NoReceptionNeutre
            //LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, LCondit, '', '', '', LTabComment, '',_pPréReception.Quantity,
            LCuEtiquette.EdittEtiqetteNiceLabel(Fichier, LItem, LCondit, '', _NoReceptionNeutre, '', LTabComment, '', _pPréReception.Quantity,
                '45X45PIEC', _pImprimante);
        END;
        LCuEtiquette.FermeEtiquetteNiceLabel(NomFichier, Fichier);

    end;

    // CFR le 28/09/2023 : R‚gie > Bug sur variable NomFrn Text[40] > Text[50]
    //   CFR le 26/10/2023 : R‚gie > Bug Quantit‚ sur ‚tiquette article toujours 1 … la r‚ception
}

