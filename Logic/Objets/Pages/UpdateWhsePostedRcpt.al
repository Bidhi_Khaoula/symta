page 50219 "Update Whse. Posted Rcpt."
{
    PageType = CardPart;
    SourceTable = "Posted Whse. Receipt Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Mise à jour")
            {
                field("Receipt Adjustment"; Rec."Receipt Adjustment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Receipt Adjustment field.';
                }
                field("Receipt Request"; Rec."Receipt Request")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Receipt Request field.';
                }
            }
        }
    }

    actions
    {
    }
}

