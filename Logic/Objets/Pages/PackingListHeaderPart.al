page 50328 "Packing List Header Part"
{
    InsertAllowed = false;
    PageType = CardPart;
    ShowFilter = false;
    SourceTable = "Packing List Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("No."; Rec."No.")
            {
                ShowMandatory = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° field.';
            }
            field("Header Status"; Rec."Header Status")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Statut field.';
            }
            field("Creation User"; Rec."Creation User")
            {
                Editable = false;
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Utilisateur création field.';
            }
            field("Creation Date"; Rec."Creation Date")
            {
                Editable = false;
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date création field.';
            }
            field("Creation Time"; Rec."Creation Time")
            {
                Editable = false;
                Importance = Additional;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Heure création field.';
            }
            field("Calculate Header Weight"; Rec."Calculate Header Weight")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Poids brut total saisi field.';
            }
            field(GetDestinationName; Rec.GetDestinationName())
            {
                Caption = 'Destination';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Destination field.';
            }
            field(Comment; Rec.Comment)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Comment field.';
            }
            field("Total Volume"; Rec."Total Volume")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Volume total field.';
            }
        }
    }

    actions
    {
        area(processing)
        {
        }
    }

    trigger OnAfterGetRecord();
    begin
        //CurrPage.EDITABLE := (Rec."Header Status" <> Rec."Header Status"::Enregistré);
        gDynamicEditable := CurrPage.EDITABLE;
    end;

    var

        //gPackingListMgmt: Codeunit "Packing List Mgmt";
        gDynamicEditable: Boolean;
}

