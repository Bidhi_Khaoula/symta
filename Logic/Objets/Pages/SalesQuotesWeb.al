page 50069 "Sales Quotes Web"
{
    ApplicationArea = all;
    Caption = 'Sales Quotes';
    CardPageID = "Sales Quote";
    Editable = false;
    PageType = List;
    SourceTable = "Sales Header";
    SourceTableView = WHERE("Document Type" = CONST(Quote),
                            "Devis Web" = CONST(True));

    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    StyleExpr = StyleText;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field("Sell-to Customer Name"; Rec."Sell-to Customer Name")
                {
                    StyleExpr = StyleText;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer Name field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    StyleExpr = StyleText;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Method Code field.';
                }
                field("Customer Disc. Group"; Rec."Customer Disc. Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Customer Disc. Group field.';
                }
                field("External Document No."; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field("Sell-to Post Code"; Rec."Sell-to Post Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Post Code field.';
                }
                field("Sell-to Country/Region Code"; Rec."Sell-to Country/Region Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Country/Region Code field.';
                }
                field("Sell-to Contact"; Rec."Sell-to Contact")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Contact field.';
                }
                field("Bill-to Customer No."; Rec."Bill-to Customer No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
                }
                field("Bill-to Name"; Rec."Bill-to Name")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Name field.';
                }
                field("Bill-to Post Code"; Rec."Bill-to Post Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Post Code field.';
                }
                field("Bill-to Country/Region Code"; Rec."Bill-to Country/Region Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Country/Region Code field.';
                }
                field("Bill-to Contact"; Rec."Bill-to Contact")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Contact field.';
                }
                field("Ship-to Code"; Rec."Ship-to Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Code field.';
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field("Ship-to Post Code"; Rec."Ship-to Post Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Post Code field.';
                }
                field("Ship-to Country/Region Code"; Rec."Ship-to Country/Region Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Country/Region Code field.';
                }
                field("Ship-to Contact"; Rec."Ship-to Contact")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Contact field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Shortcut Dimension 1 Code"; Rec."Shortcut Dimension 1 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 1 Code field.';
                }
                field("Shortcut Dimension 2 Code"; Rec."Shortcut Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 2 Code field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salesperson Code field.';

                    trigger OnValidate()
                    var
                        LrecSalesPerson: Record "Salesperson/Purchaser";
                    begin
                        // FBO le 05-04-2017 => FE20170324
                        IF LrecSalesPerson.GET(rec."Salesperson Code") THEN
                            rec.VALIDATE("Export Salesperson", LrecSalesPerson."Export Salesperson");
                        // FIN FBO le 05-04-2017
                    end;
                }
                field("Assigned User ID"; Rec."Assigned User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field("Currency Code"; Rec."Currency Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Currency Code field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Campaign No."; Rec."Campaign No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Campaign No. field.';
                }
                field("Opportunity No."; Rec."Opportunity No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Opportunity No. field.';
                }
                field(Status; Rec.Status)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Order Create User"; Rec."Order Create User")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
                }
                field("Order Date"; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field("Ship-to City"; Rec."Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to City field.';
                }
                field(Marge; Rec.CalcCost(1))
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CalcCost(1) field.';
                }
                field("% Marge"; Rec.CalcCost(2))
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CalcCost(2) field.';
                }
            }
        }
        area(factboxes)
        {
            part("Nb Devis Web"; 50055)
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
            part("Customer Statistics FactBox"; "Customer Statistics FactBox")
            {
                SubPageLink = "No." = FIELD("Bill-to Customer No."),
                              "Date Filter" = FIELD("Date Filter");
                Visible = true;
                ApplicationArea = All;
            }
            part("Customer Details FactBox"; "Customer Details FactBox")
            {
                SubPageLink = "No." = FIELD("Bill-to Customer No."),
                              "Date Filter" = FIELD("Date Filter");
                Visible = true;
                ApplicationArea = All;
            }
            systempart(Links; Links)
            {
                Visible = false;
                ApplicationArea = All;
            }
            systempart(Notes; Notes)
            {
                Visible = true;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Quote")
            {
                Caption = '&Quote';
                Image = Quote;
                action(Statistics)
                {
                    Caption = 'Statistics';
                    Image = Statistics;
                    Promoted = true;
                    PromotedCategory = Process;
                    ShortCutKey = 'F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Statistics action.';

                    trigger OnAction()
                    begin
                        rec.CalcInvDiscForHeader();
                        COMMIT();
                        PAGE.RUNMODAL(PAGE::"Sales Statistics", Rec);
                    end;
                }
                action("Customer Card")
                {
                    Caption = 'Customer Card';
                    Image = Customer;
                    RunObject = Page "Customer Card";
                    RunPageLink = "No." = FIELD("Sell-to Customer No.");
                    ShortCutKey = 'Shift+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Customer Card action.';
                }
                action("C&ontact Card")
                {
                    Caption = 'C&ontact Card';
                    Image = Card;
                    RunObject = Page "Contact Card";
                    RunPageLink = "No." = FIELD("Sell-to Contact No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the C&ontact Card action.';
                }
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Sales Comment Sheet";
                    RunPageLink = "Document Type" = FIELD("Document Type"),
                                  "No." = FIELD("No."),
                                  "Document Line No." = CONST(0);
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';
                }
                action(Dimensions)
                {
                    AccessByPermission = TableData 348 = R;
                    Caption = 'Dimensions';
                    Image = Dimensions;
                    ShortCutKey = 'Shift+Ctrl+D';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Dimensions action.';

                    trigger OnAction()
                    begin
                        rec.ShowDocDim();
                    end;
                }
                action(Approvals)
                {
                    Caption = 'Approvals';
                    Image = Approvals;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Approvals action.';

                    trigger OnAction()
                    var
                        ApprovalEntries: Page "Approval Entries";
                    begin
                        ApprovalEntries.SetRecordFilters(DATABASE::"Sales Header", rec."Document Type", rec."No.");
                        ApprovalEntries.RUN();
                    end;
                }
            }
        }
        area(processing)
        {
            action("&Print")
            {
                Caption = '&Print';
                Ellipsis = true;
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Print action.';

                trigger OnAction()
                begin
                    DocPrint.PrintSalesHeader(Rec);
                end;
            }
            action(Email)
            {
                Caption = 'Email';
                Image = Email;
                ApplicationArea = All;
                ToolTip = 'Executes the Email action.';

                trigger OnAction()
                begin
                    DocPrint.EmailSalesHeader(Rec);
                end;
            }
            group(Release)
            {
                Caption = 'Release';
                Image = ReleaseDoc;
                action("Re&lease")
                {
                    Caption = 'Re&lease';
                    Image = ReleaseDoc;
                    ShortCutKey = 'Ctrl+F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&lease action.';

                    trigger OnAction()
                    var
                        ReleaseSalesDoc: Codeunit "Release Sales Document";
                    begin
                        ReleaseSalesDoc.PerformManualRelease(Rec);
                    end;
                }
                action("Re&open")
                {
                    Caption = 'Re&open';
                    Image = ReOpen;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Re&open action.';

                    trigger OnAction()
                    var
                        ReleaseSalesDoc: Codeunit "Release Sales Document";
                    begin
                        ReleaseSalesDoc.PerformManualReopen(Rec);
                    end;
                }
                separator(" ")
                {
                }
            }
            group(Create)
            {
                Caption = 'Create';
                action(MakeOrder)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Make &Order';
                    Image = MakeOrder;
                    ToolTip = 'Convert the sales quote to a sales order.';

                    AboutTitle = 'When a quote gets accepted';
                    AboutText = 'When your customer is ready to buy, you can turn quotes into orders. Or, unless you need partial shipments, skip the order and create an invoice instead.';

                    trigger OnAction()
                    var
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin
                        if ApprovalsMgmt.PrePostApprovalCheckSales(Rec) then
                            CODEUNIT.Run(CODEUNIT::"Sales-Quote to Order (Yes/No)", Rec);
                    end;
                }
                action("C&reate Customer")
                {
                    Caption = 'C&reate Customer';
                    Image = NewCustomer;
                    ApplicationArea = All;
                    ToolTip = 'Executes the C&reate Customer action.';

                    trigger OnAction()
                    begin
                        IF rec.CheckCustomerCreated(FALSE) THEN
                            CurrPage.UPDATE(TRUE);
                    end;
                }
                action("Create &To-do")
                {
                    AccessByPermission = TableData Contact = R;
                    Caption = 'Create &To-do';
                    Image = NewToDo;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Create &To-do action.';

                    trigger OnAction()
                    begin
                        rec.CreateTask();
                    end;
                }
                separator("  ")
                {
                }
            }
            group(Approval)
            {
                Caption = 'Approval';
                Image = Approval;
                group("Request Approval")
                {
                    Caption = 'Request Approval';
                    Image = SendApprovalRequest;
                    action(SendApprovalRequest)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Send A&pproval Request';
                        Image = SendApprovalRequest;
                        ToolTip = 'Request approval of the document.';
                        trigger OnAction()
                        var
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                        begin
                            if ApprovalsMgmt.CheckSalesApprovalPossible(Rec) then
                                ApprovalsMgmt.OnSendSalesDocForApproval(Rec);
                        end;
                    }
                    action(CancelApprovalRequest)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Cancel Approval Re&quest';
                        Image = CancelApprovalRequest;
                        ToolTip = 'Cancel the approval request.';

                        trigger OnAction()
                        var
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                            WorkflowWebhookMgt: Codeunit "Workflow Webhook Management";
                        begin
                            ApprovalsMgmt.OnCancelSalesApprovalRequest(Rec);
                            WorkflowWebhookMgt.FindAndCancel(Rec.RecordId);
                        end;
                    }
                    separator("   ")
                    {
                    }
                }
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        //VD mise en couleur de devis Export et du nouveau site 28/04/2017
        StyleText := '';
        IF ((rec."Shipping Agent Code" = 'EXPORT') OR (rec."Salesperson Code" = '19')) THEN
            StyleText := 'Attention'

        ELSE
            IF rec."Source Document Type" = 'WEB' THEN
                StyleText := 'StrongAccent'

            ELSE
                StyleText := 'no style';

    end;

    trigger OnOpenPage()
    begin
        rec.SetSecurityFilterOnRespCenter();
    end;

    var
        DocPrint: Codeunit "Document-Print";
        StyleText: Text[20];
}

