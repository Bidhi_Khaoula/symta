page 50208 "Sales Shipment Header"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Sales Shipment Header";
    SourceTableView = SORTING("Sell-to Customer No.", "No.")
                      ORDER(Descending);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(SearchDocNo; Rec."No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. field.';
            }
            field(SearchClientNo; Rec."Sell-to Customer No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
            }
            field(Searchdate; Rec."Posting Date")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Posting Date field.';
            }
            repeater(Group)
            {
                field(NoBL; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(ClientNo; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(DateBL; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field(MontantTotalTTC; MontantTotalTTC)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MontantTotalTTC field.';
                }
                field(MontantTVA; MontantTVA)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MontantTVA field.';
                }
                field(MontantHT; MontantHT)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MontantHT field.';
                }
                field(MontantPort; MontantPort)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MontantPort field.';
                }
                field(Reference; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Mode expedition"; Rec."Mode d'expédition")
                {
                    Caption = 'Mode expédition';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode expédition field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field("Order No."; Rec."Order No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order No. field.';
                }
                field("Order Date"; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field("Source Document Type"; Rec."Source Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Origine Document field.';
                }
                field("Order Create User"; Rec."Order Create User")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
                }
                field(Preparateur; Rec.Preparateur)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Preparateur field.';
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Method Code field.';
                }
                field("Mode d'expédition"; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field("Nb Of Box"; Rec."Nb Of Box")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nb de colis field.';
                }
                field(Weight; Rec.Weight)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field("Ship-to Post Code"; Rec."Ship-to Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Post Code field.';
                }
                field("Ship-to City"; Rec."Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to City field.';
                }
                field("Bill-to Customer No."; Rec."Bill-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Customer No. field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    var
        "Sales Shipment Line": Record "Sales Shipment Line";
    begin


        MontantPort := 0;

        TempVATAmountLine.DELETEALL();

        "Sales Shipment Line".SETRANGE("Document No.", Rec."No.");
        "Sales Shipment Line".SETFILTER(Quantity, '<>0'); // AD Le 23-01-2014
        IF "Sales Shipment Line".FINDFIRST() THEN
            REPEAT
                TempVATAmountLine.INIT();
                TempVATAmountLine."VAT Calculation Type" := "Sales Shipment Line"."VAT Calculation Type";
                TempVATAmountLine."Tax Group Code" := "Sales Shipment Line"."Tax Group Code";
                TempVATAmountLine."VAT %" := "Sales Shipment Line"."VAT %";
                TempVATAmountLine."VAT Base" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                TempVATAmountLine."Amount Including VAT" := TempVATAmountLine."VAT Base" * (1 + TempVATAmountLine."VAT %" / 100);
                TempVATAmountLine."Line Amount" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                IF "Sales Shipment Line"."Allow Invoice Disc." THEN
                    TempVATAmountLine."Inv. Disc. Base Amount" := "Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity;
                TempVATAmountLine.InsertLine();

                IF "Sales Shipment Line"."Internal Line Type" = "Sales Shipment Line"."Internal Line Type"::Shipment THEN
                    MontantPort += ("Sales Shipment Line"."Net Unit Price" * "Sales Shipment Line".Quantity);

            UNTIL "Sales Shipment Line".NEXT() = 0;

        TempVATAmountLine.CALCSUMS(
                "Line Amount",
                "Inv. Disc. Base Amount",
               "Invoice Discount Amount",
               "VAT Base",
               "VAT Amount");


        MontantTotalTTC := ROUND(TempVATAmountLine."Amount Including VAT", 0.01);
        MontantTVA := ROUND(TempVATAmountLine."VAT Amount", 0.01);
        MontantHT := ROUND(TempVATAmountLine."VAT Base" - MontantPort, 0.01);
    end;

    var

        TempVATAmountLine: Record "VAT Amount Line" temporary;
        MontantTotalTTC: Decimal;
        MontantPort: Decimal;
        MontantTVA: Decimal;
        MontantHT: Decimal;
}

