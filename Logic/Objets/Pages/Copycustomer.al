page 50262 "Copy customer"
{
    PageType = StandardDialog;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Nouveau code client"; "Nouveau code client")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nouveau code client field.';

                trigger OnValidate()
                begin
                    IF Customer.GET("Nouveau code client") THEN ERROR(ERROR001, "Nouveau code client");
                end;
            }
        }
    }

    actions
    {
    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        IF "Nouveau code client" = '' THEN EXIT;


        IF CloseAction = ACTION::OK THEN BEGIN
            IF CONFIRM('Voulez vous créer le client ' + "Nouveau code client") THEN BEGIN
                DestCustomer := OrigineCustomer;
                DestCustomer."No." := "Nouveau code client";
                DestCustomer.INSERT(TRUE);
            END;
        END;
    end;

    var
        "Nouveau code client": Code[20];
        Customer: Record Customer;
        ERROR001: Label 'Le client %1 existe déjà';
        OrigineCustomer: Record Customer;
        DestCustomer: Record Customer;

    procedure init(_Customer: Record Customer)
    begin
        _Customer.TESTFIELD("No.");
        OrigineCustomer := _Customer;
    end;

    procedure returnNewCust(var _Cust: Record Customer)
    begin
        _Cust := DestCustomer;
    end;
}

