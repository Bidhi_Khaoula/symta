page 50351 "WIIO - ListeReceptionEntrepot"
{
    ApplicationArea = all;
    UsageCategory = Lists;
    CardPageID = "Warehouse Receipt";
    DataCaptionFields = "No.";
    Editable = false;
    PageType = List;
    SourceTable = "Warehouse Receipt Header";

    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the warehouse receipt header number, which is generated according to the No. Series specified in the Warehouse Mgt. Setup window.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the code of the location in which the items are being received.';
                }
                field("Assigned User ID"; Rec."Assigned User ID")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the ID of the user who is responsible for the document.';
                }
                field("Sorting Method"; Rec."Sorting Method")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the method by which the receipts are sorted.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the zone in which the items are being received if you are using directed put-away and pick.';
                    Visible = false;
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Indicates the code of the bin in which you will place the items being received.';
                    Visible = false;
                }
                field("Document Status"; Rec."Document Status")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the status of the warehouse receipt.';
                    Visible = false;
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the posting date of the warehouse receipt.';
                    Visible = false;
                }
                field("Assignment Date"; Rec."Assignment Date")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the date on which the document was assigned to the user.';
                    Visible = false;
                }
                field("First Line Origin No."; Rec."First Line Origin No.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the N° Origine Première Ligne field.';
                }
                field(Fournisseur; Rec.GetVendorNo())
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the GetVendorNo() field.';
                }
                field(Nom; Rec.GetVendorName())
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the GetVendorName() field.';
                }
                field("Reçu"; recu)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the recu field.';
                }
                field("A Reçevoir"; a_recevoir)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the a_recevoir field.';
                }
                field("Vendor Shipment No."; Rec."Vendor Shipment No.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Vendor Shipment No. field.';
                }
                field("Vendor Type No."; Rec."Vendor Type No.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Type n° doc. fournisseur field.';
                }
                field(Information; Rec.Information)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Information field.';
                }
                field(Position; Rec.Position)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Position field.';
                }
                field(En_Réception; Rec."En réception")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the En réception field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Receipt")
            {
                Caption = '&Receipt';
                Image = Receipt;
                action("Co&mments")
                {
                    ApplicationArea = all;
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Warehouse Comment Sheet";
                    RunPageLink = "Table Name" = CONST("Whse. Receipt"),
                                  Type = CONST(" "),
                                  "No." = FIELD("No.");
                    ToolTip = 'Executes the Co&mments action.';
                }
                action("Posted &Whse. Receipts")
                {
                    ApplicationArea = all;
                    Caption = 'Posted &Whse. Receipts';
                    Image = PostedReceipts;
                    RunObject = Page "Posted Whse. Receipt List";
                    RunPageLink = "Whse. Receipt No." = FIELD("No.");
                    RunPageView = SORTING("Whse. Receipt No.");
                    ToolTip = 'Executes the Posted &Whse. Receipts action.';
                }
            }
            group("&Line")
            {
                Caption = '&Line';
                Image = Line;
                action(Card)
                {
                    ApplicationArea = all;
                    Caption = 'Card';
                    Image = EditLines;
                    ShortCutKey = 'Shift+F7';
                    ToolTip = 'Executes the Card action.';

                    trigger OnAction();
                    begin
                        PAGE.RUN(PAGE::"Warehouse Receipt", Rec);
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord();
    begin
        jauge(Rec."No.", a_recevoir, recu);
    end;

    var
        //  "--- GVarESKAPE ---": Integer;
        a_recevoir: Decimal;
        recu: Decimal;


    procedure jauge(_pNoFusion: Code[20]; var _pNbARecevoir: Decimal; var _pNbRecus: Decimal);
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
        TempMontantArticle: Record "Item Amount" temporary;
    begin
        // Cherche combien d'article a recevoir et conbien de recus
        TempMontantArticle.RESET();
        TempMontantArticle.DELETEALL();
        // Cherche le nb à recevoir
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                TempMontantArticle.RESET();
                TempMontantArticle.SETRANGE("Item No.", WhseReceiptLine."Item No.");
                IF TempMontantArticle.FINDFIRST() THEN
                    TempMontantArticle.RENAME(TempMontantArticle.Amount + WhseReceiptLine.Quantity, TempMontantArticle."Amount 2", TempMontantArticle."Item No.")
                ELSE BEGIN
                    TempMontantArticle.INIT();
                    TempMontantArticle.Amount := WhseReceiptLine."Qty. Outstanding (Base)";
                    TempMontantArticle."Amount 2" := 0;
                    TempMontantArticle."Item No." := WhseReceiptLine."Item No.";
                    TempMontantArticle.INSERT(TRUE);
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Cherche le nb à de recus complets
        TempMontantArticle.RESET();
        _pNbARecevoir := TempMontantArticle.COUNT;
        _pNbRecus := 0;
        IF TempMontantArticle.FINDFIRST() THEN
            REPEAT
                "Pré-reception".RESET();
                "Pré-reception".SETCURRENTKEY("No.", "Item No.");
                "Pré-reception".SETRANGE("No.", _pNoFusion);
                "Pré-reception".SETRANGE("Item No.", TempMontantArticle."Item No.");
                "Pré-reception".CALCSUMS(Quantity);
                IF "Pré-reception".Quantity >= TempMontantArticle.Amount THEN
                    _pNbRecus += 1;
            UNTIL TempMontantArticle.NEXT() = 0;
    end;
}

