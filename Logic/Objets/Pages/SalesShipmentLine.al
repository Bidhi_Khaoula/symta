page 50209 "Sales Shipment Line"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = ListPart;
    SourceTable = "Sales Shipment Line";
    SourceTableView = WHERE(Quantity = FILTER(<> 0));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(NoBL; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field(CodeArticle; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Designation; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Designation2; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field(Quantite; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field(ReferenceOrigine; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field(donneur_ordre; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(date_preparation; Rec."Shipment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Date field.';
                }
                field("Line Discount %"; Rec."Line Discount %")
                {
                    Caption = 'Line Discount %';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount % field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Net Unit Price"; Rec."Net Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix unitaire net field.';
                }
                field("Unit Cost"; Rec."Unit Cost")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';
                }
                field(ReturnQuantity; GetReturnQuantity())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetReturnQuantity() field.';
                }

            }
        }
    }

    LOCAL PROCEDURE GetReturnQuantity(): Decimal;
    VAR
        lItemLedgerEntry: Record "Item Ledger Entry";
    BEGIN

        lItemLedgerEntry.SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Sale);
        lItemLedgerEntry.SETRANGE("Document Type", lItemLedgerEntry."Document Type"::"Sales Shipment");
        lItemLedgerEntry.SETRANGE("Document No.", Rec."Document No.");
        lItemLedgerEntry.SETRANGE("Document Line No.", Rec."Line No.");
        //MESSAGE('COUNT : %1\Filters : %2', lItemLedgerEntry.COUNT(), lItemLedgerEntry.GETFILTERS());
        IF lItemLedgerEntry.FINDFIRST() THEN BEGIN
            lItemLedgerEntry.CALCFIELDS("Return Quantity");
            EXIT(lItemLedgerEntry."Return Quantity");
        END
        ELSE
            EXIT(10);

    END;

    // CFR le 11/03/2021 - R‚gie : gestion des retour > ajout du nø de ligne pour avoir la clef
    // CFR le 17/12/2021 - FA20211414 : suivi des quantit‚s retourn‚es
}

