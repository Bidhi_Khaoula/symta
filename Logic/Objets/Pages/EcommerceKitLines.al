page 50235 "Ecommerce KitLines"
{
    PageType = List;
    SourceTable = "BOM Component";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Nomenclature; Rec."Parent Item No.")
                {
                    Caption = 'Nomenclature';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nomenclature field.';
                }
                field(Article; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Designation; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Quantite; Rec."Quantity per")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity per field.';
                }
                field(Reference_Active; Rec."Ref. Active")
                {
                    Caption = 'Référence active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active field.';
                }
            }
        }
    }

    actions
    {
    }

}

