page 50040 "Balance AG"
{
    CardPageID = "Balance AG";
    DeleteAllowed = false;
    Editable = true;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = Document;
    SourceTable = customer;
    ApplicationArea = all;
    layout
    {
    }

    actions
    {
    }

    var
        MatrixHeader: Text[50];
        PeriodType: Option Day,Week,Month,Quarter,Year,"Accounting Period";
        DateFilter: Text[30];
        PeriodInitialized: Boolean;
        InternalDateFilter: Text[100];
        // PeriodFormMgt: Codeunit PeriodFormManagement;
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        CustLedgEntry: Record "Cust. Ledger Entry";
        excelBuf: Record "Excel Buffer" temporary;

    local procedure FindPeriod(SearchText: Code[10])
    var
        Cust: Record Customer;
    begin
    end;

    procedure calcul(): Decimal
    var
        DtldCustLedgEntryLoc: Record "Detailed Cust. Ledg. Entry";
    begin
    end;

    procedure Total(DateDebut: Date; DateFin: Date; CodeClient: Code[20]): Decimal
    var
        DtldCustLedgEntryLoc: Record "Detailed Cust. Ledg. Entry";
    begin
    end;

    procedure CalculBallance(DateDebut: Date; DateFin: Date; CodeClient: Code[20]): Decimal
    var
        DtldCustLedgEntryLoc: Record "Detailed Cust. Ledg. Entry";
    begin
    end;

    procedure "----excel"()
    begin
    end;

    procedure MakeExcelInfo()
    begin
    end;

    procedure MakeExcelDataHeader()
    var
        affiche_date: Record Date;
    begin
    end;

    procedure MakeExcelDataBody()
    var
        Affiche_Client: Record Customer;
        affiche_date: Record Date;
        Montant_tmp: Decimal;
        Window: Dialog;
    begin
    end;

    procedure CreateExcelbook()
    begin
    end;

    procedure calcul_excel(DateDeb: Date; DateFin: Date; NoClient: Code[20]): Decimal
    var
        DtldCustLedgEntryLoc: Record "Detailed Cust. Ledg. Entry";
    begin
    end;
}

