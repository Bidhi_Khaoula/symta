// page 50102 "Appro. SYMTA_240712"
// {
//     PageType = Card;
//     SourceTable = "Requisition Line";
//     SourceTableView = SORTING("Fournisseur Calculé", "Ref. Active") ORDER(Ascending);

//     layout
//     {
//         area(content)
//         {
//             group("Général")
//             {
//                 Caption = 'Général';
//                 field("No."; rec."No.")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(RechercheRefActiveNo; CUMultiRef.RechercheRefActive(rec."No."))
//                 {
//                     Caption = 'Ref. Active';
//                     ApplicationArea = All;
//                 }
//                 field(Description; rec.Description)
//                 {
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Description 2"; rec."Description 2")
//                 {
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Type de commande"; rec."Type de commande")
//                 {
//                     Visible = false;
//                     ApplicationArea = All;

//                     trigger OnValidate()
//                     begin
//                         TypedecommandeOnAfterValidate;
//                     end;
//                 }
//                 field("Stock Calculé"; rec."Stock Calculé")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Attendu Calculé"; rec."Attendu Calculé")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Reservé Calculé"; rec."Reservé Calculé")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Projection Mini"; rec."Projection Mini")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Projection Maxi"; rec."Projection Maxi")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Manufacturer Code"; Item."Manufacturer Code")
//                 {
//                     Caption = 'Marque';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("No. 2"; recitem."No. 2")
//                 {
//                     Caption = 'Fusion en attente sur';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Accept Action Message"; rec."Accept Action Message")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Text19015056; '')
//                 {
//                     CaptionClass = Text19015056;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Quantity; Rec.Quantity)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_1; 'J')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_1; TabConso[1] [1])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Réservé Global"; Item.Inventory - _Reservé + _Attendu)
//                 {
//                     Caption = 'Réservé Global';
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Stock; Item.Inventory)
//                 {
//                     Caption = 'Stock';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(_Attendu; _Attendu)
//                 {
//                     Caption = 'Attendu Global';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(_Reservé; _Reservé)
//                 {
//                     Caption = 'Réservé Global';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Stocké; Item.Stocké)
//                 {
//                     Caption = 'Stocké';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Date création fiche"; Item."Create Date")
//                 {
//                     Caption = 'Date création fiche';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Date dernière Sortie"; Item."Date Dernière Sortie")
//                 {
//                     Caption = 'Date dernière Sortie';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Date dernière entrée"; Item."Date dernière entrée")
//                 {
//                     Caption = 'Date dernière Entrée';
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field("Conso Mois En Cours"; rec."Conso Mois En Cours")
//                 {
//                     Visible = false;
//                     ApplicationArea = All;
//                 }
//                 field("Indice Jauge"; Rec."Indice Jauge" / 10000)
//                 {
//                     DecimalPlaces = 0 : 2;
//                     ApplicationArea = All;
//                 }
//                 field("Mini Encours"; Rec."Mini Encours")
//                 {
//                     Visible = false;
//                     ApplicationArea = All;
//                 }
//                 field(Text19034574; '')
//                 {
//                     CaptionClass = Text19034574;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Text19034575; '')
//                 {
//                     CaptionClass = Text19034575;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_1; TabConso[2] [1])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_2; 'F')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_2; TabConso[1] [2])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_2; TabConso[2] [2])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_3; 'M')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_3; TabConso[1] [3])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_3; TabConso[2] [3])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_4; 'A')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_4; TabConso[1] [4])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_4; TabConso[2] [4])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_5; 'M')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_5; TabConso[1] [5])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_5; TabConso[2] [5])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_6; 'J')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_6; TabConso[1] [6])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_6; TabConso[2] [6])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_7; 'J')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_7; TabConso[1] [7])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_7; TabConso[2] [7])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_8; 'A')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_8; TabConso[1] [8])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_8; TabConso[2] [8])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_9; 'S')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_9; TabConso[1] [9])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_9; TabConso[2] [9])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_10; 'O')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_10; TabConso[1] [10])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_10; TabConso[2] [10])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_11; 'N')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_11; TabConso[1] [11])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_11; TabConso[2] [11])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Lib_Mois_12; 'D')
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_1_12; TabConso[1] [12])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(TabTotalConso_1; TabTotalConso[1])
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Text19034576; '')
//                 {
//                     CaptionClass = Text19034576;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_1; TabConso[3] [1])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_1; TabConso[4] [1])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_2; TabConso[3] [2])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_2; TabConso[4] [2])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_3; TabConso[3] [3])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_3; TabConso[4] [3])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_4; TabConso[3] [4])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_4; TabConso[4] [4])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_5; TabConso[3] [5])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_5; TabConso[4] [5])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_6; TabConso[3] [6])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_6; TabConso[4] [6])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_7; TabConso[3] [7])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_7; TabConso[4] [7])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_8; TabConso[3] [8])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_8; TabConso[4] [8])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_9; TabConso[3] [9])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_9; TabConso[4] [9])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_10; TabConso[3] [10])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_10; TabConso[4] [10])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_11; TabConso[3] [11])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_2_12; TabConso[2] [12])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_3_12; TabConso[3] [12])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(TabTotalConso_2; TabTotalConso[2])
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(TabTotalConso_3; TabTotalConso[3])
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(COUNT; Rec.COUNT)
//                 {
//                     Caption = '/';
//                     //DecimalPlaces = 0 : 2;
//                     ApplicationArea = All;
//                 }
//                 field(Text19034569; '')
//                 {
//                     CaptionClass = Text19034569;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_1; TabConso[5] [1])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_2; TabConso[5] [2])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_3; TabConso[5] [3])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_4; TabConso[5] [4])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_5; TabConso[5] [5])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_6; TabConso[5] [6])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_7; TabConso[5] [7])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_8; TabConso[5] [8])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_9; TabConso[5] [9])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_10; TabConso[5] [10])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_11; TabConso[4] [11])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_11; TabConso[5] [11])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_4_12; TabConso[4] [12])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(Conso_5_12; TabConso[5] [12])
//                 {
//                     BlankZero = true;
//                     Editable = false;
//                     ApplicationArea = All;
//                 }
//                 field(TabTotalConso_4; TabTotalConso[4])
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//                 field(TabTotalConso_5; TabTotalConso[5])
//                 {
//                     Editable = false;
//                     Style = Standard;
//                     StyleExpr = TRUE;
//                     ApplicationArea = All;
//                 }
//             }
//             part(frm_BufferPrice; "Equivalence Appro")
//             {
//                 SubPageLink = "No." = FIELD("No.");
//                 ApplicationArea = All;
//             }
//             part(frm_BufferPrice2; "Vendor Price Buffer")
//             {
//                 ApplicationArea = All;
//             }
//         }
//     }

//     actions
//     {
//         area(navigation)
//         {
//             group("Réappro")
//             {
//                 Caption = 'Réappro';
//                 action(Statistiques)
//                 {
//                     Caption = 'Statistiques';
//                     ShortCutKey = 'F7';
//                     ApplicationArea = All;

//                     trigger OnAction()
//                     var
//                         Selection: Integer;
//                         blnLineFilter: Boolean;
//                         dec_LineFilter: Decimal;
//                     begin
//                         Selection := STRMENU(text000, 1);
//                         IF Selection = 0 THEN
//                             EXIT;
//                         blnLineFilter := Selection IN [2];

//                         // Si on filtre sur les lignes alors on se positionne sur l'enregistrement précédent.
//                         IF blnLineFilter THEN BEGIN
//                             dec_LineFilter := Rec."Indice Jauge";
//                         END
//                         ELSE
//                             dec_LineFilter := 0;
//                         // Instancie et ouvre le formulaire des statistiques.
//                         CLEAR(frm_Stat);
//                         frm_Stat.DeleteRecords();
//                         frm_Stat.InsertRecords(Rec."Worksheet Template Name", Rec."Journal Batch Name", dec_LineFilter);
//                         frm_Stat.RUN();
//                     end;
//                 }
//             }
//         }
//         area(processing)
//         {
//             action("Multi-Consultation")
//             {
//                 Caption = 'Multi-Consultation';
//                 Promoted = true;
//                 PromotedCategory = Process;
//                 ApplicationArea = All;

//                 trigger OnAction()
//                 begin
//                     //FrmQuid.InitClient(SalesLine."Sell-to Customer No.");
//                     LFrmQuid.InitArticle(Rec."No.");
//                     LFrmQuid.RUNMODAL();
//                 end;
//             }
//         }
//     }

//     trigger OnAfterGetRecord()
//     var
//         LItemvendor: Record "Item Vendor";
//     begin
//         CalculConso.CalculerConsoSur4ans(Rec."No.", TabConso, TabTotalConso, WORKDATE());

//         CurrPage.frm_BufferPrice2.page.InsertBufferRecord(Rec);

//         Item.GET(Rec."No.");
//         Item.CALCFIELDS("Date Dernière Sortie");

//         Item.CalcAttenduReserve(_Reservé, _Attendu);

//         LItemvendor.RESET();
//         LItemvendor.SETRANGE("Item No.", Rec."No.");
//         LItemvendor.SETRANGE("Statut Qualité", LItemvendor."Statut Qualité"::"Contrôle en cours");
//         IF LItemvendor.FINDFIRST () THEN
//             MESSAGE(ESK001, LItemvendor."Vendor No.");

//         IF NOT Item.Stocké THEN
//             MESSAGE(ESK002);

//         IF (Item."Reorder Point" <> 0) OR (Item."Maximum Inventory" <> 0) THEN
//             MESSAGE(ESK003, Item."Reorder Point", Item."Maximum Inventory");

//         // LM le 23-07-2012 =>ref ctive de fusion SP
//         recitem.INIT();
//         IF recitem.GET(Item."Fusion en attente sur") THEN;
//         // FIN LM le 23-07-2012
//         TabConso11OnFormat;
//         TabConso12OnFormat;
//         TabConso14OnFormat;
//         TabConso13OnFormat;
//         TabConso15OnFormat;
//         TabConso52OnFormat;
//         TabConso42OnFormat;
//         TabConso32OnFormat;
//         TabConso22OnFormat;
//         TabConso21OnFormat;
//         TabConso53OnFormat;
//         TabConso43OnFormat;
//         TabConso33OnFormat;
//         TabConso23OnFormat;
//         TabConso31OnFormat;
//         TabConso56OnFormat;
//         TabConso46OnFormat;
//         TabConso36OnFormat;
//         TabConso26OnFormat;
//         TabConso55OnFormat;
//         TabConso45OnFormat;
//         TabConso35OnFormat;
//         TabConso25OnFormat;
//         TabConso24OnFormat;
//         TabConso44OnFormat;
//         TabConso34OnFormat;
//         TabConso54OnFormat;
//         TabConso41OnFormat;
//         TabConso51OnFormat;
//         TabConso16OnFormat;
//         TabConso410OnFormat;
//         TabConso310OnFormat;
//         TabConso510OnFormat;
//         TabConso59OnFormat;
//         TabConso49OnFormat;
//         TabConso39OnFormat;
//         TabConso29OnFormat;
//         TabConso210OnFormat;
//         TabConso58OnFormat;
//         TabConso48OnFormat;
//         TabConso38OnFormat;
//         TabConso28OnFormat;
//         TabConso18OnFormat;
//         TabConso110OnFormat;
//         TabConso19OnFormat;
//         TabConso17OnFormat;
//         TabConso27OnFormat;
//         TabConso37OnFormat;
//         TabConso47OnFormat;
//         TabConso57OnFormat;
//         TabConso411OnFormat;
//         TabConso311OnFormat;
//         TabConso511OnFormat;
//         TabConso211OnFormat;
//         TabConso111OnFormat;
//         TabConso412OnFormat;
//         TabConso312OnFormat;
//         TabConso512OnFormat;
//         TabConso212OnFormat;
//         TabConso112OnFormat;
//         J39OnFormat;
//         F39OnFormat;
//         A39OnFormat;
//         M39OnFormat;
//         A39OnFormat;
//         J39OnFormat;
//         J39OnFormat;
//         M39OnFormat;
//         D39OnFormat;
//         N39OnFormat;
//         O39OnFormat;
//         S39OnFormat;
//         QuantityOnFormat;
//     end;

//     trigger OnOpenPage()
//     begin
//         Rec.FILTERGROUP(2);
//         Rec.SETRANGE("Journal Batch Name", COPYSTR(USERID, 1, 10));
//         Rec.FILTERGROUP(0);


//         // AD Le 16-04-2012 => Demande NG -> On affiche les fusions en attente
//         Rec.SETRANGE("Fusion en attente sur");
//         // FIN AD Le 16-04-2012
//     end;

//     var
//         CalculConso: Codeunit CalculConso;
//         TabConso: array[5, 12] of Decimal;
//         TabTotalConso: array[5] of Decimal;
//         text000: Label '&Toutes les lignes, &Jusque ligne en cours.';
//         CUMultiRef: Codeunit "Gestion Multi-référence";
//         Item: Record Item;
//         "_Reservé": Decimal;
//         _Attendu: Decimal;
//         ESK001: Label 'Il existe au moins un fournisseur [%1] avec statut qualité à contrôle en cours !';
//         ESK002: Label '-- ATTENTION -- Article Non Stocké';
//         ESK003: Label 'Point de commande %1 \ Stock Maximum %2';
//         recitem: Record Item;
//         Text19015056: Label 'N';
//         Text19034574: Label 'N - 1';
//         Text19034575: Label 'N - 2';
//         Text19034576: Label 'N - 3';
//         Text19034569: Label 'N - 4';

//     procedure GetColor(Annee: Integer; Mois: Integer): Integer
//     begin
//         IF COPYSTR(Rec."Chaine Mois", Mois, 1) = 'O' THEN
//             EXIT(255)
//         ELSE
//             EXIT(1);
//     end;

//     local procedure TypedecommandeOnAfterValidate()
//     begin
//         CurrPage.frm_BufferPrice.FORM.InsertBufferRecord(Rec);
//         CurrPage.UPDATE;
//     end;

//     local procedure TabConso11OnFormat()
//     begin
//         CurrPage.Conso_1_1.UPDATEFORECOLOR := GetColor(1, 1);
//     end;

//     local procedure TabConso12OnFormat()
//     begin
//         CurrPage.Conso_1_2.UPDATEFORECOLOR := GetColor(1, 2);
//     end;

//     local procedure TabConso14OnFormat()
//     begin
//         CurrPage.Conso_1_4.UPDATEFORECOLOR := GetColor(1, 4);
//     end;

//     local procedure TabConso13OnFormat()
//     begin
//         CurrPage.Conso_1_3.UPDATEFORECOLOR := GetColor(1, 3);
//     end;

//     local procedure TabConso15OnFormat()
//     begin
//         CurrPage.Conso_1_5.UPDATEFORECOLOR := GetColor(1, 5);
//     end;

//     local procedure TabConso52OnFormat()
//     begin
//         CurrPage.Conso_5_2.UPDATEFORECOLOR := GetColor(5, 2);
//     end;

//     local procedure TabConso42OnFormat()
//     begin
//         CurrPage.Conso_4_2.UPDATEFORECOLOR := GetColor(4, 2);
//     end;

//     local procedure TabConso32OnFormat()
//     begin
//         CurrPage.Conso_3_2.UPDATEFORECOLOR := GetColor(3, 2);
//     end;

//     local procedure TabConso22OnFormat()
//     begin
//         CurrPage.Conso_2_2.UPDATEFORECOLOR := GetColor(2, 2);
//     end;

//     local procedure TabConso21OnFormat()
//     begin
//         CurrPage.Conso_2_1.UPDATEFORECOLOR := GetColor(2, 1);
//     end;

//     local procedure TabConso53OnFormat()
//     begin
//         CurrPage.Conso_5_3.UPDATEFORECOLOR := GetColor(5, 3);
//     end;

//     local procedure TabConso43OnFormat()
//     begin
//         CurrPage.Conso_4_3.UPDATEFORECOLOR := GetColor(4, 3);
//     end;

//     local procedure TabConso33OnFormat()
//     begin
//         CurrPage.Conso_3_3.UPDATEFORECOLOR := GetColor(3, 3);
//     end;

//     local procedure TabConso23OnFormat()
//     begin
//         CurrPage.Conso_2_3.UPDATEFORECOLOR := GetColor(2, 3);
//     end;

//     local procedure TabConso31OnFormat()
//     begin
//         CurrPage.Conso_3_1.UPDATEFORECOLOR := GetColor(3, 1);
//     end;

//     local procedure TabConso56OnFormat()
//     begin
//         CurrPage.Conso_5_6.UPDATEFORECOLOR := GetColor(5, 6);
//     end;

//     local procedure TabConso46OnFormat()
//     begin
//         CurrPage.Conso_4_6.UPDATEFORECOLOR := GetColor(4, 6);
//     end;

//     local procedure TabConso36OnFormat()
//     begin
//         CurrPage.Conso_3_6.UPDATEFORECOLOR := GetColor(3, 6);
//     end;

//     local procedure TabConso26OnFormat()
//     begin
//         CurrPage.Conso_2_6.UPDATEFORECOLOR := GetColor(2, 6);
//     end;

//     local procedure TabConso55OnFormat()
//     begin
//         CurrPage.Conso_5_5.UPDATEFORECOLOR := GetColor(5, 5);
//     end;

//     local procedure TabConso45OnFormat()
//     begin
//         CurrPage.Conso_4_5.UPDATEFORECOLOR := GetColor(4, 5);
//     end;

//     local procedure TabConso35OnFormat()
//     begin
//         CurrPage.Conso_3_5.UPDATEFORECOLOR := GetColor(3, 5);
//     end;

//     local procedure TabConso25OnFormat()
//     begin
//         CurrPage.Conso_2_5.UPDATEFORECOLOR := GetColor(2, 5);
//     end;

//     local procedure TabConso24OnFormat()
//     begin
//         CurrPage.Conso_2_4.UPDATEFORECOLOR := GetColor(2, 4);
//     end;

//     local procedure TabConso44OnFormat()
//     begin
//         CurrPage.Conso_4_4.UPDATEFORECOLOR := GetColor(4, 4);
//     end;

//     local procedure TabConso34OnFormat()
//     begin
//         CurrPage.Conso_3_4.UPDATEFORECOLOR := GetColor(3, 4);
//     end;

//     local procedure TabConso54OnFormat()
//     begin
//         CurrPage.Conso_5_4.UPDATEFORECOLOR := GetColor(5, 4);
//     end;

//     local procedure TabConso41OnFormat()
//     begin
//         CurrPage.Conso_4_1.UPDATEFORECOLOR := GetColor(4, 1);
//     end;

//     local procedure TabConso51OnFormat()
//     begin
//         CurrPage.Conso_5_1.UPDATEFORECOLOR := GetColor(5, 1);
//     end;

//     local procedure TabConso16OnFormat()
//     begin
//         CurrPage.Conso_1_6.UPDATEFORECOLOR := GetColor(1, 6);
//     end;

//     local procedure TabConso410OnFormat()
//     begin
//         CurrPage.Conso_4_10.UPDATEFORECOLOR := GetColor(4, 10);
//     end;

//     local procedure TabConso310OnFormat()
//     begin
//         CurrPage.Conso_3_10.UPDATEFORECOLOR := GetColor(3, 10);
//     end;

//     local procedure TabConso510OnFormat()
//     begin
//         CurrPage.Conso_5_10.UPDATEFORECOLOR := GetColor(5, 10);
//     end;

//     local procedure TabConso59OnFormat()
//     begin
//         CurrPage.Conso_5_9.UPDATEFORECOLOR := GetColor(5, 9);
//     end;

//     local procedure TabConso49OnFormat()
//     begin
//         CurrPage.Conso_4_9.UPDATEFORECOLOR := GetColor(4, 9);
//     end;

//     local procedure TabConso39OnFormat()
//     begin
//         CurrPage.Conso_3_9.UPDATEFORECOLOR := GetColor(3, 9);
//     end;

//     local procedure TabConso29OnFormat()
//     begin
//         CurrPage.Conso_2_9.UPDATEFORECOLOR := GetColor(2, 9);
//     end;

//     local procedure TabConso210OnFormat()
//     begin
//         CurrPage.Conso_2_10.UPDATEFORECOLOR := GetColor(2, 10);
//     end;

//     local procedure TabConso58OnFormat()
//     begin
//         CurrPage.Conso_5_8.UPDATEFORECOLOR := GetColor(5, 8);
//     end;

//     local procedure TabConso48OnFormat()
//     begin
//         CurrPage.Conso_4_8.UPDATEFORECOLOR := GetColor(4, 8);
//     end;

//     local procedure TabConso38OnFormat()
//     begin
//         CurrPage.Conso_3_8.UPDATEFORECOLOR := GetColor(3, 8);
//     end;

//     local procedure TabConso28OnFormat()
//     begin
//         CurrPage.Conso_2_8.UPDATEFORECOLOR := GetColor(2, 8);
//     end;

//     local procedure TabConso18OnFormat()
//     begin
//         CurrPage.Conso_1_8.UPDATEFORECOLOR := GetColor(1, 8);
//     end;

//     local procedure TabConso110OnFormat()
//     begin
//         CurrPage.Conso_1_10.UPDATEFORECOLOR := GetColor(1, 10);
//     end;

//     local procedure TabConso19OnFormat()
//     begin
//         CurrPage.Conso_1_9.UPDATEFORECOLOR := GetColor(1, 9);
//     end;

//     local procedure TabConso17OnFormat()
//     begin
//         CurrPage.Conso_1_7.UPDATEFORECOLOR := GetColor(1, 7);
//     end;

//     local procedure TabConso27OnFormat()
//     begin
//         CurrPage.Conso_2_7.UPDATEFORECOLOR := GetColor(2, 7);
//     end;

//     local procedure TabConso37OnFormat()
//     begin
//         CurrPage.Conso_3_7.UPDATEFORECOLOR := GetColor(3, 7);
//     end;

//     local procedure TabConso47OnFormat()
//     begin
//         CurrPage.Conso_4_7.UPDATEFORECOLOR := GetColor(4, 7);
//     end;

//     local procedure TabConso57OnFormat()
//     begin
//         CurrPage.Conso_5_7.UPDATEFORECOLOR := GetColor(5, 7);
//     end;

//     local procedure TabConso411OnFormat()
//     begin
//         CurrPage.Conso_4_11.UPDATEFORECOLOR := GetColor(4, 11);
//     end;

//     local procedure TabConso311OnFormat()
//     begin
//         CurrPage.Conso_3_11.UPDATEFORECOLOR := GetColor(3, 11);
//     end;

//     local procedure TabConso511OnFormat()
//     begin
//         CurrPage.Conso_5_11.UPDATEFORECOLOR := GetColor(5, 11);
//     end;

//     local procedure TabConso211OnFormat()
//     begin
//         CurrPage.Conso_2_11.UPDATEFORECOLOR := GetColor(2, 11);
//     end;

//     local procedure TabConso111OnFormat()
//     begin
//         CurrPage.Conso_1_11.UPDATEFORECOLOR := GetColor(1, 11);
//     end;

//     local procedure TabConso412OnFormat()
//     begin
//         CurrPage.Conso_4_12.UPDATEFORECOLOR := GetColor(4, 12);
//     end;

//     local procedure TabConso312OnFormat()
//     begin
//         CurrPage.Conso_3_12.UPDATEFORECOLOR := GetColor(3, 12);
//     end;

//     local procedure TabConso512OnFormat()
//     begin
//         CurrPage.Conso_5_12.UPDATEFORECOLOR := GetColor(5, 12);
//     end;

//     local procedure TabConso212OnFormat()
//     begin
//         CurrPage.Conso_2_12.UPDATEFORECOLOR := GetColor(2, 12);
//     end;

//     local procedure TabConso112OnFormat()
//     begin
//         CurrPage.Conso_1_12.UPDATEFORECOLOR := GetColor(1, 12);
//     end;

//     local procedure J39OnFormat()
//     begin
//         CurrPage.Lib_Mois_1.UPDATEFORECOLOR := GetColor(0, 1);
//     end;

//     local procedure F39OnFormat()
//     begin
//         CurrPage.Lib_Mois_2.UPDATEFORECOLOR := GetColor(0, 2);
//     end;

//     local procedure A39OnFormat()
//     begin
//         CurrPage.Lib_Mois_4.UPDATEFORECOLOR := GetColor(0, 4);
//     end;

//     local procedure M39OnFormat()
//     begin
//         CurrPage.Lib_Mois_3.UPDATEFORECOLOR := GetColor(0, 3);
//     end;

//     local procedure A39OnFormat()
//     begin
//         CurrPage.Lib_Mois_8.UPDATEFORECOLOR := GetColor(0, 8);
//     end;

//     local procedure J39OnFormat()
//     begin
//         CurrPage.Lib_Mois_7.UPDATEFORECOLOR := GetColor(0, 7);
//     end;

//     local procedure J39OnFormat()
//     begin
//         CurrPage.Lib_Mois_6.UPDATEFORECOLOR := GetColor(0, 6);
//     end;

//     local procedure M39OnFormat()
//     begin
//         CurrPage.Lib_Mois_5.UPDATEFORECOLOR := GetColor(0, 5);
//     end;

//     local procedure D39OnFormat()
//     begin
//         CurrPage.Lib_Mois_12.UPDATEFORECOLOR := GetColor(0, 12);
//     end;

//     local procedure N39OnFormat()
//     begin
//         CurrPage.Lib_Mois_11.UPDATEFORECOLOR := GetColor(0, 11);
//     end;

//     local procedure O39OnFormat()
//     begin
//         CurrPage.Lib_Mois_10.UPDATEFORECOLOR := GetColor(0, 10);
//     end;

//     local procedure S39OnFormat()
//     begin
//         CurrPage.Lib_Mois_9.UPDATEFORECOLOR := GetColor(0, 9);
//     end;

//     local procedure QuantityOnFormat()
//     begin
//         IF NOT Item.Stocké THEN
//             CurrPage.Quantity.UPDATEFORECOLOR := 255
//         ELSE
//             ;
//     end;
// }

