page 50230 "ECommerce Customer"
{

    PageType = List;
    SourceTable = customer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code_Remise; Rec."Customer Disc. Group")
                {
                    Caption = 'Customer Disc. Group';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Customer Disc. Group field.';
                }
                field(Code_Client; Rec."No.")
                {
                    Caption = 'No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Nom; Rec.Name)
                {
                    Caption = 'Name';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(Adresse; Rec.Address)
                {
                    Caption = 'Address';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address field.';
                }
                field(Adresse_2; Rec."Address 2")
                {
                    Caption = 'Address 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Address 2 field.';
                }
                field(Code_Postal; Rec."Post Code")
                {
                    Caption = 'Post Code';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Post Code field.';
                }
                field(Ville; Rec.City)
                {
                    Caption = 'City';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the City field.';
                }
                field(Telephone; Rec."Phone No.")
                {
                    Caption = 'Phone No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Phone No. field.';
                }
                field(Email; Rec."E-Mail")
                {
                    Caption = 'E-Mail';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the E-Mail field.';
                }
                field(Fax; Rec."Fax No.")
                {
                    Caption = 'Fax No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fax No. field.';
                }
                field(Contact; Rec.Contact)
                {
                    Caption = 'Contact';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Contact field.';
                }
                field(Droits_Telechargement; Rec."Droits Téléchargement WEB")
                {
                    Caption = 'Droits_Telechargement';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Droits_Telechargement field.';
                }
                field(Groupe_Tarif; Rec."Customer Price Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Customer Price Group field.';
                }
                field(Login_Web; Rec."Login WEB")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Login WEB field.';
                }
                field(MDP_Web; Rec."Mot de passe WEB")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mot de passe WEB field.';
                }
                field("Country/Region Code"; Rec."Country/Region Code")
                {
                    Caption = 'Country/Region Code';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Country/Region Code field.';
                }
                field(Disable_Web; Rec."Disable Web")
                {
                    Caption = 'Disable Web';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Disable Web field.';
                }
                field("Code Groupement RFA"; Rec."Code Groupement RFA")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Groupement RFA field.';
                }
                field("Consult Price Right"; Rec."Consult Price Right")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Consult Price Right field.';
                }
                field("Acces Right"; Rec."Acces Right")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Access Right field.';
                }
                field("Centrale Active"; Rec."Centrale Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Centrale Active field.';
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Salesperson Code field.';
                }
            }
        }
    }

    actions
    {
    }
}

