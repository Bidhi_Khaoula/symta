page 50065 "Item Vendor Card"
{
    Caption = 'Fiche Fournisseur Article (Catalogue)';
    CardPageID = "Item Vendor Card";
    PageType = Document;
    ShowFilter = true;
    SourceTable = "Item Vendor";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("Vendor No."; Rec."Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor No. field.';
                }
                field(VendorName; Vendor.Name)
                {
                    Caption = 'Nom';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field("No. 2"; Item."No. 2")
                {
                    Caption = 'Référence Active';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field(Libellé; Item.Description + ' ' + Item."Description 2")
                {
                    Caption = 'Libellé';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Libellé field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Vendor Item No."; Rec."Vendor Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Vendor Item Desciption"; Rec."Vendor Item Desciption")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Purch. Unit of Measure"; Rec."Purch. Unit of Measure")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Purch. Unit of Measure field.';
                }
                field("Minimum Order Quantity"; Rec."Minimum Order Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Minimum Order Quantity field.';
                }
                field("Purch. Multiple"; Rec."Purch. Multiple")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple d''achat field.';
                }
                field("Lead Time Calculation"; Rec."Lead Time Calculation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Lead Time Calculation field.';
                }
                field("Last Direct Cost"; Rec."Last Direct Cost")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Last Direct Cost field.';
                }
                field("Indirect Cost"; Rec."Indirect Cost")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Indirect Cost % field.';
                }
                field("Code Remise"; Rec."Code Remise")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Remise field.';
                }
                field("Statut Qualité"; Rec."Statut Qualité")
                {
                    Caption = 'Status Qualité';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status Qualité field.';

                    trigger OnValidate()
                    begin
                        // AD Le 12-01-2016 => Déplace dans la table
                        // StatutQualit233OnAfterValidate;
                    end;
                }
                field("date maj statut qualité"; Rec."date maj statut qualité")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date maj statut qualité field.';
                }
                field("user maj statut qualité"; Rec."user maj statut qualité")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the user maj statut qualité field.';
                }
                field("Statut Approvisionnement"; Rec."Statut Approvisionnement")
                {
                    Caption = 'Status Appro.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status Appro. field.';

                    trigger OnValidate()
                    begin
                        // AD Le 12-01-2016 => Déplace dans la table
                        // StatutApprovisionnementOnAfter;
                    end;
                }
                field("date maj statut appro"; Rec."date maj statut appro")
                {
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date maj statut appro field.';
                }
                field("user maj statut appro"; Rec."user maj statut appro")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the user maj statut appro field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
            }
            part("Purchases Price SubForm"; "Purchases Price SubForm")
            {
                SubPageLink = "Item No." = FIELD("Item No."),
                              "Vendor No." = FIELD("Vendor No."),
                              "Variant Code" = FIELD("Variant Code");
                SubPageView = SORTING("Item No.", "Vendor No.", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity")
                              ORDER(Descending);
                ApplicationArea = All;
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        Item.GET(rec."Item No.");
        Vendor.GET(rec."Vendor No.");
    end;

    var
        Item: Record Item;
        Vendor: Record Vendor;
    /* Text19065955: Label 'Date m.a.j.';
    Text19007652: Label 'Utilisateur'; */

    //CFR le 23/03/2023 => R‚gie : Ajout Champ [Comment]
    //CFR le 28/09/2023 => R‚gie : Traduction Caption
}

