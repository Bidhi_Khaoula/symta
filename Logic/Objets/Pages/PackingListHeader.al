page 50321 "Packing List Header"
{
    ApplicationArea = all;
    InsertAllowed = false;
    PageType = Card;
    SourceTable = "Packing List Header";

    layout
    {
        area(content)
        {
            group("Général")
            {
                Editable = gDynamicEditable;
                field("No."; Rec."No.")
                {
                    ShowMandatory = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° field.';
                }
                field("Header Status"; Rec."Header Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Creation User"; Rec."Creation User")
                {
                    Editable = false;
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur création field.';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    Editable = false;
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date création field.';
                }
                field("Creation Time"; Rec."Creation Time")
                {
                    Editable = false;
                    Importance = Additional;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure création field.';
                }
                field("Calculate Header Weight"; Rec."Calculate Header Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids brut total saisi field.';
                }
                field("Theoretical Weight"; Rec."Theoretical Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Theoretical Weight field.';
                }
                field("Whse Shipment Header Quantity"; Rec."Whse Shipment Header Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre d''expédition entrepôt field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(GetDestinationName; Rec.GetDestinationName())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetDestinationName() field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
            }
            part("Packing List Subform1"; "Packing List Subform1")
            {
                Editable = gDynamicEditable;
                SubPageLink = "Packing List No." = FIELD("No.");
                UpdatePropagation = Both;
                ApplicationArea = All;
            }
            part("Packing List Subform2"; "Packing List Subform2")
            {
                Editable = gDynamicEditable;
                SubPageLink = "Packing List No." = FIELD("No.");
                UpdatePropagation = Both;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Process)
            {
                Caption = 'Processing';
                action("Actualiser la liste")
                {
                    Caption = 'Actualiser la liste';
                    Image = RefreshText;
                    ToolTip = 'Mettez à jour la liste de colisage en ajoutant les lignes d''''article manquantes';
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        gPackingListMgmt.RefreshPackingList(Rec);
                    end;
                }
                action("Réinitialiser la liste")
                {
                    Caption = 'Réinitialiser la liste';
                    Image = RefreshText;
                    ToolTip = 'Supprimez la liste de colisage et les informations associées pour la recréer à partir de l''expédition';
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        gPackingListMgmt.RazPackingList(Rec);
                    end;
                }
            }
            group(Reports)
            {
                Caption = 'Reporting';
                action("Imprimer liste par article")
                {
                    Caption = 'Imprimer liste par article';
                    Image = PrintCheck;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer liste par article action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."No.", TRUE, 'ARTICLE');
                    end;
                }
                action("Imprimer liste par colis")
                {
                    Caption = 'Imprimer liste par colis';
                    Image = PrintCover;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer liste par colis action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."No.", TRUE, 'COLIS');
                    end;
                }
                action("Excel Export")
                {
                    Caption = 'Excel Export';
                    Image = Excel;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Excel Export action.';

                    trigger OnAction()
                    var
                        lSalesShipmentHeader: Record "Sales Shipment Header";
                        lPackingListLine: Record "Packing List Line";
                    begin
                        //>> SFD20201005
                        IF Rec."First Posted Shipment No." <> '' THEN
                            lSalesShipmentHeader.SETRANGE("No.", Rec."First Posted Shipment No.")
                        ELSE BEGIN
                            lPackingListLine.SETRANGE("Packing List No.", Rec."No.");
                            IF lPackingListLine.FINDFIRST() THEN
                                lSalesShipmentHeader.SETRANGE("No.", lPackingListLine."Posted Source No.");
                        END;
                        IF lSalesShipmentHeader.FINDSET() THEN;
                        REPORT.RUN(REPORT::"Excel Export Packing List", TRUE, FALSE, lSalesShipmentHeader);
                        //<< SFD20201005
                    end;
                }
                action("Imprimer liste temporaire")
                {
                    Caption = 'Imprimer liste temporaire';
                    Image = PrintCover;
                    Promoted = true;
                    PromotedCategory = "Report";
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer liste temporaire action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.PrintPackingListTemporary(Rec."No.", TRUE);
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        //CurrPage.EDITABLE := (Rec."Header Status" <> Rec."Header Status"::Enregistré);
        gDynamicEditable := CurrPage.EDITABLE;
    end;

    trigger OnOpenPage()
    begin
        gPackingListMgmt.RefreshPackingList(Rec);
    end;

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";
        gDynamicEditable: Boolean;

}

