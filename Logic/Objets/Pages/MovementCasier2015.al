page 50077 "Movement Casier 2015"
{
    Caption = 'Saisie du pied de BL';
    CardPageID = "Movement Casier 2015";
    PageType = Card;
    SourceTableTemporary = true;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            /*//TODOusercontrol(CtlFocus; "Ahead_EXFocus")
            {
                ApplicationArea = All;
            }*/
            field(CurrentJnlBatchName; CurrentJnlBatchName)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the CurrentJnlBatchName field.';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    CurrPage.SAVERECORD();
                    ItemJnlMgt.LookupName(CurrentJnlBatchName, TempItemJnlLine);
                    CurrPage.UPDATE(FALSE);
                end;

                trigger OnValidate()
                begin
                    ItemJnlMgt.CheckName(CurrentJnlBatchName, TempItemJnlLine);
                end;
            }
            group(Mouvement)
            {
                field("Date Comptabilisation"; TempItemJnlLine."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';

                    trigger OnValidate()
                    begin
                        TempItemJnlLine.VALIDATE("Posting Date");
                    end;
                }
                field("Référence"; TempItemJnlLine."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the référence active field.';

                    trigger OnValidate()
                    var
                        LBinCont: Record "Bin Content";
                    begin
                        TempItemJnlLine.VALIDATE("Recherche référence");



                        TempItemJnlLine.VALIDATE("Location Code", CompanyInfo."Location Code");
                        TempItemJnlLine.VALIDATE("New Location Code", CompanyInfo."Location Code");
                        TempItemJnlLine.VALIDATE(Quantity, chargerquantité());
                        NouveauCodeEmplacement := TempItemJnlLine."New Bin Code";

                        CodeZoneNouvelEmplacement := TempItemJnlLine."Zone Code";



                        //LM le 23-05-2013 =>affichage ancienne capacité casier
                        CLEAR(LBinCont);
                        IF LBinCont.GET(TempItemJnlLine."Location Code", TempItemJnlLine."Bin Code", TempItemJnlLine."Item No.", '', TempItemJnlLine."Unit of Measure Code") THEN BEGIN
                            capacitéEmplacement := LBinCont.capacité;
                            Seuil := LBinCont."% Seuil Capacité Réap. Pick." // MCO Le 04-10-2018
                        END;
                    end;
                }
                field("N° Article"; TempItemJnlLine."Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Code Magasin"; TempItemJnlLine."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';

                    trigger OnValidate()
                    begin
                        TempItemJnlLine.VALIDATE("Location Code");
                    end;
                }
                field("Code Emplacement"; TempItemJnlLine."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        LBinContent: Record "Bin Content";
                        LFrmBinContent: Page "Bin Contents List";
                    begin
                        TempItemJnlLine.TESTFIELD("Item No.");

                        CLEAR(LBinContent);
                        LBinContent.SETRANGE("Location Code", TempItemJnlLine."Location Code");
                        LBinContent.SETRANGE("Item No.", TempItemJnlLine."Item No.");
                        LFrmBinContent.SETTABLEVIEW(LBinContent);
                        LFrmBinContent.EDITABLE(FALSE);
                        LFrmBinContent.LOOKUPMODE(TRUE);

                        IF LFrmBinContent.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            LFrmBinContent.GETRECORD(LBinContent);
                            TempItemJnlLine.VALIDATE("Bin Code", LBinContent."Bin Code");
                            OnAfterValidateBinCode();
                        END;
                    end;

                    trigger OnValidate()
                    begin
                        TempItemJnlLine.VALIDATE("Bin Code");
                        OnAfterValidateBinCode()
                    end;
                }
                field("Quantité"; TempItemJnlLine.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';

                    trigger OnValidate()
                    begin
                        TempItemJnlLine.VALIDATE(Quantity);
                    end;
                }
                field("Capacité"; capacitéEmplacement)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the capacitéEmplacement field.';
                }
            }
            group(Emplacement)
            {
                field("Créer Emplacement"; CréerEmplacement)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CréerEmplacement field.';
                }
                field("Zone Nouvel Emplacement"; CodeZoneNouvelEmplacement)
                {
                    Editable = CréerEmplacement;
                    LookupPageID = Zones;
                    TableRelation = Zone.Code;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CodeZoneNouvelEmplacement field.';
                }
                field("Type de casier"; CasierDestinationParDefaut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CasierDestinationParDefaut field.';
                }
                field("Nouveau code magasin"; TempItemJnlLine."New Location Code")
                {
                    TableRelation = Location;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Location Code field.';
                }
                field("Nouvel Emplacement"; NouveauCodeEmplacement)
                {
                    Lookup = true;
                    LookupPageID = "Bin List";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NouveauCodeEmplacement field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var

                        recL_Bin: Record Bin;
                        pgeL_Bin: Page "Bin List";
                    begin
                        CLEAR(pgeL_Bin);
                        recL_Bin.RESET();
                        recL_Bin.SETRANGE("Location Code", TempItemJnlLine."Location Code");
                        recL_Bin.SETRANGE("Zone Code", CodeZoneNouvelEmplacement);
                        pgeL_Bin.LOOKUPMODE(TRUE);
                        pgeL_Bin.SETTABLEVIEW(recL_Bin);
                        IF pgeL_Bin.RUNMODAL() = ACTION::LookupOK THEN
                            pgeL_Bin.GETRECORD(recL_Bin);

                        NouveauCodeEmplacement := recL_Bin.Code;
                        TempItemJnlLine.VALIDATE("New Bin Code", NouveauCodeEmplacement);
                    end;

                    trigger OnValidate()
                    var
                        bin: Record Bin;
                    begin
                        IF CréerEmplacement THEN BEGIN
                            IF NOT bin.GET(TempItemJnlLine."New Location Code", NouveauCodeEmplacement) THEN BEGIN
                                IF CodeZoneNouvelEmplacement = '' THEN ERROR('Veuillez saisir un code zone');
                                bin.VALIDATE("Location Code", TempItemJnlLine."New Location Code");
                                bin.VALIDATE(Code, NouveauCodeEmplacement);
                                bin.VALIDATE("Item Filter", TempItemJnlLine."Item No.");
                                bin.VALIDATE("Zone Code", CodeZoneNouvelEmplacement);
                                bin.INSERT(TRUE);
                            END;

                        END;
                        TempItemJnlLine.VALIDATE("New Bin Code", NouveauCodeEmplacement);
                    end;
                }
                field(Commentaire; TempItemJnlLine.Commentaire)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(btn_FctValider)
            {
                Caption = 'Fonction';
                action(btn_Valider)
                {
                    Caption = 'Valider';
                    Image = PostInventoryToGL;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Valider action.';

                    trigger OnAction()
                    begin
                        IF CONFIRM(Esk004Qst, TRUE) THEN BEGIN
                            Valider();

                            // ANI Le 17-11-2015 : Gestion du focus
                            Focus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);

                        END;
                    end;
                }
                action(Stock)
                {
                    Caption = 'Stock';
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Stock action.';

                    trigger OnAction()
                    var
                        BinContent: Record "Bin Content";
                    begin
                        IF TempItemJnlLine."Item No." <> '' THEN
                            BinContent.ShowBinContents2(TempItemJnlLine."Location Code", TempItemJnlLine."Item No.", '', '');
                    end;
                }
                action("Multi-Consultation")
                {
                    Image = CalculateConsumption;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'Ctrl+M';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Multi-Consultation action.';

                    trigger OnAction()
                    var
                        FrmQuid: Page "Multi -consultation";
                    begin

                        FrmQuid.InitArticle(TempItemJnlLine."Item No.");
                        FrmQuid.RUNMODAL();
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        JnlSelected: Boolean;
    begin
        InitValue();


        ItemJnlMgt.TemplateSelection(PAGE::"Item Reclass. Journal", 1, FALSE, TempItemJnlLine, JnlSelected);
        IF NOT JnlSelected THEN
            ERROR('');

        ItemJnlMgt.OpenJnl(CurrentJnlBatchName, TempItemJnlLine);

        TempItemJnlLine.VALIDATE("Journal Batch Name", CurrentJnlBatchName);
        CompanyInfo.GET();
        InitValue();

        // ANI Le 18-05-2016 FE20160427 Casier par défaut => type de casier
        //CasierDestinationParDefaut := FALSE; // AD Le 23-03-2016
        CasierDestinationParDefaut := CasierDestinationParDefaut::" ";
        // FIN ANI Le 18-05-2016 FE20160427

        // ANI Le 17-11-2015 : Gestion du focus
        Focus := 'ST1000000002;MD' + FORMAT(TIME, 0, 1);
    end;

    var
        CompanyInfo: Record "Company Information";

        TempItemJnlLine: Record "Item Journal Line" temporary;

        ItemJnlMgt: Codeunit ItemJnlManagement;
        CurrentJnlBatchName: Code[10];
        CodeZoneAncienEmplacement: Code[20];
        CodeZoneNouvelEmplacement: Code[20];
        Focus: Text[50];
        NouveauCodeEmplacement: Code[20];
        CasierDestinationParDefaut: Option " ","Défaut",Surstock;
        "capacitéEmplacement": Integer;
        "CréerEmplacement": Boolean;
        Esk001Err: Label 'Le nouvel emplacement doit être différent de l''emplacement actuel !';
        Esk002Err: Label 'Type de casier obligatoire';
        Esk004Qst: Label 'Souhaitez-vous valider ?';
        Seuil: Decimal;

    local procedure InitValue()
    begin
        TempItemJnlLine.VALIDATE("Journal Template Name", 'RECLASS');
        TempItemJnlLine.VALIDATE("Posting Date", WORKDATE());
        TempItemJnlLine.VALIDATE("Entry Type", TempItemJnlLine."Entry Type"::Transfer);
        TempItemJnlLine.VALIDATE("Document No.", USERID);
        // AD Le 23-03-2016 => Géré à l'ouverture pour que ça reste a ce qu'a choisi l'utilisateur
        // CasierDestinationParDefaut := TRUE;
        // FIN AD Le 23-03-2016
    end;

    local procedure PurgeValue()
    begin

        NouveauCodeEmplacement := '';
        CodeZoneNouvelEmplacement := '';
        CréerEmplacement := FALSE;
    end;

    local procedure PurgeAllValue()
    begin

        NouveauCodeEmplacement := '';
        CodeZoneNouvelEmplacement := '';
        CréerEmplacement := FALSE;
        TempItemJnlLine.INIT();
    end;

    procedure "chargerquantité"(): Decimal
    var
        LBin: Record "Bin Content";
    begin
        LBin.SETRANGE("Location Code", TempItemJnlLine."Location Code");
        LBin.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
        LBin.SETRANGE("Item No.", TempItemJnlLine."Item No.");
        LBin.SETRANGE("Qty. per Unit of Measure", TempItemJnlLine."Qty. per Unit of Measure");
        IF LBin.FINDFIRST() THEN BEGIN
            LBin.CALCFIELDS("Quantity (Base)");
            EXIT(LBin."Quantity (Base)");
        END;
        EXIT(0);
    end;

    local procedure OnAfterValidateBinCode()
    begin

        TempItemJnlLine.VALIDATE(Quantity, chargerquantité());
        NouveauCodeEmplacement := TempItemJnlLine."New Bin Code";
    end;

    local procedure Valider()
    var
        LBin: Record Bin;
        LBinSrc: Record Bin;
        LBinCont: Record "Bin Content";
        LLigneVente: Record "Sales Line";
        LLigneBP: Record "Warehouse Shipment Line";
        LLigneAchat: Record "Purchase Line";
        LLigneBR: Record "Warehouse Receipt Line";
        //  ItemJnlPostLine: Codeunit "Item Jnl.-Post Line";
        LItemJnlLine: Record "Item Journal Line";
        ItemJnlPost: Codeunit "Item Jnl.-Post";
        CduLFunctions: Codeunit "Codeunits Functions";
        LAncienCasierParDefaut: Boolean;


    begin

        // ###############################################################################
        // ATTENTION voir CU_50073.CreateMvtEmpl() : fonction identique pour les tablettes
        // ###############################################################################

        TempItemJnlLine.TESTFIELD("New Bin Code");
        TempItemJnlLine.TESTFIELD("Item No.");
        TempItemJnlLine.TESTFIELD("Unit of Measure Code");

        IF TempItemJnlLine."New Bin Code" = TempItemJnlLine."Bin Code" THEN
            ERROR(Esk001Err);

        // ANI Le 18-05-2016 FE20160427 Casier par défaut => type de casier
        IF CasierDestinationParDefaut = CasierDestinationParDefaut::" " THEN
            ERROR(Esk002Err);


        //Emplacement par defaut
        // FIN ANI Le 18-05-2016 FE20160427 Casier par défaut => type de casier
        //IF CasierDestinationParDefaut THEN
        IF CasierDestinationParDefaut = CasierDestinationParDefaut::Défaut THEN
        // FIN ANI Le 18-05-2016 FE20160427
         BEGIN

            // AD Le 28-09-2016 => REGIE -> On regarde si le casier source etait le casier par defaut de l'article
            CLEAR(LBinCont);
            LBinCont.SETRANGE("Location Code", TempItemJnlLine."New Location Code");
            LBinCont.SETRANGE("Item No.", TempItemJnlLine."Item No.");
            LBinCont.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            LBinCont.FINDFIRST();
            LAncienCasierParDefaut := LBinCont.Default;
            // FIN  AD Le 28-09-2016


            //recherche du casier actuel
            LBinCont.SETRANGE("Location Code", TempItemJnlLine."New Location Code");
            LBinCont.SETRANGE("Item No.", TempItemJnlLine."Item No.");
            LBinCont.SETRANGE(Default, TRUE);
            IF LBinCont.FINDFIRST() THEN BEGIN
                LBinCont.Fixed := TRUE;
                LBinCont.Default := FALSE;
                LBinCont.MODIFY();
            END;

            //affectation du casier par defaut
            LBinCont.RESET();
            LBinCont.SETRANGE("Location Code", TempItemJnlLine."New Location Code");
            LBinCont.SETRANGE("Item No.", TempItemJnlLine."Item No.");
            LBinCont.SETRANGE("Bin Code", TempItemJnlLine."New Bin Code");
            IF LBinCont.FINDFIRST() THEN BEGIN
                LBinCont.Fixed := TRUE;
                LBinCont.Default := TRUE;
                LBinCont.capacité := capacitéEmplacement;
                LBinCont."% Seuil Capacité Réap. Pick." := Seuil; // MCO Le 04-10-2018
                LBinCont.MODIFY();
            END
            ELSE BEGIN
                LBinCont.RESET();
                LBinCont.INIT();
                LBinCont.VALIDATE("Location Code", TempItemJnlLine."New Location Code");
                LBinCont.VALIDATE("Item No.", TempItemJnlLine."Item No.");
                LBinCont.VALIDATE("Unit of Measure Code", TempItemJnlLine."Unit of Measure Code");
                LBinCont.VALIDATE("Bin Code", TempItemJnlLine."New Bin Code");

                LBin.SETRANGE("Location Code", TempItemJnlLine."New Location Code");
                LBin.SETRANGE(Code, TempItemJnlLine."New Bin Code");
                LBin.FINDFIRST();

                LBinCont.VALIDATE("Zone Code", LBin."Zone Code");
                LBinCont.Fixed := TRUE;
                LBinCont.Default := TRUE;
                LBinCont.capacité := capacitéEmplacement;
                LBinCont."% Seuil Capacité Réap. Pick." := Seuil; // MCO Le 04-10-2018
                LBinCont.INSERT(TRUE);
            END;


            //on change tout les emplacement sur les documents vente et achat.
            //vente

            // AD Le 28-09-2016 => REGIE -> Si le casier source était le casier par defaut, on va le modifier sur les transfert
            IF LAncienCasierParDefaut THEN BEGIN
                CLEAR(LItemJnlLine);
                LItemJnlLine.SETRANGE("Entry Type", LItemJnlLine."Entry Type"::Transfer);
                LItemJnlLine.SETRANGE("Item No.", TempItemJnlLine."Item No.");
                LItemJnlLine.SETRANGE("New Location Code", TempItemJnlLine."Location Code");
                LItemJnlLine.SETRANGE("New Bin Code", TempItemJnlLine."Bin Code");
                LItemJnlLine.MODIFYALL("New Bin Code", TempItemJnlLine."New Bin Code");
            END;
            // FIN AD Le 28-09-2016

            LLigneVente.SETRANGE(Type, LLigneVente.Type::Item);
            LLigneVente.SETRANGE("No.", TempItemJnlLine."Item No.");
            LLigneVente.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            // CFR le 22/09/2021 => Régie - Faire suivre le code zone dans les lignes ventes
            LLigneVente.MODIFYALL("Zone Code", CodeZoneNouvelEmplacement);
            LLigneVente.MODIFYALL("Bin Code", TempItemJnlLine."New Bin Code");

            LLigneBP.SETRANGE("Item No.", TempItemJnlLine."Item No.");
            LLigneBP.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            // CFR le 05/10/2023 => R‚gie - Blocage mouvement inter-zone si PICS en cours car les PICS sont g‚n‚r‚s par zone
            LBinSrc.GET(TempItemJnlLine."Location Code", TempItemJnlLine."Bin Code");
            CodeZoneAncienEmplacement := LBinSrc."Zone Code";
            IF (CodeZoneAncienEmplacement <> CodeZoneNouvelEmplacement) THEN
                IF LLigneBP.FINDFIRST() THEN
                    ERROR('Mouvement impossible.\Il existe au moins 1 ligne de PICS (nø%1) en cours de pr‚paration.', LLigneBP."No.");
            // FIN CFR le 05/10/2023
            LLigneBP.MODIFYALL("Bin Code", TempItemJnlLine."New Bin Code");

            //achat
            LLigneAchat.SETRANGE(Type, LLigneAchat.Type::Item);
            LLigneAchat.SETRANGE("No.", TempItemJnlLine."Item No.");
            LLigneAchat.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            LLigneAchat.MODIFYALL("Bin Code", TempItemJnlLine."New Bin Code");

            LLigneBR.SETRANGE("Item No.", TempItemJnlLine."Item No.");
            LLigneBR.SETRANGE("Bin Code", TempItemJnlLine."Bin Code");
            LLigneBR.MODIFYALL("Bin Code", TempItemJnlLine."New Bin Code");

        END;
        //on valide

        // AD Le 16-04-2012 => Pour pouvoir faire des changement de casier sans Qté
        // CODEUNIT.RUN(CODEUNIT::"Item Jnl.-Post",Rec); // CODE D'ORIGINE
        CLEAR(LItemJnlLine); // AD Le 28-09-2016 => REGIE
        LItemJnlLine := TempItemJnlLine;
        LItemJnlLine.SETRECFILTER();
        // MCO Le 09-12-2015 => Normalement la ligne ne doit pas exister (N° de ligne = 0 ).
        IF NOT LItemJnlLine.INSERT(TRUE) THEN
            REPEAT
                LItemJnlLine."Line No." += 1;
            UNTIL LItemJnlLine.INSERT(TRUE);
        IF TempItemJnlLine.Quantity <> 0 THEN BEGIN
            //ItemJnlPostLine.RUN(LItemJnlLine);
            CduLFunctions.SetHideValidationDialog(TRUE);
            ItemJnlPost.RUN(LItemJnlLine)
        END
        ELSE
            LItemJnlLine.DELETE(TRUE); // Supprimer la ligne si mouvement ) zéro
        // FIN AD Le 16-04-2012

        // Vider la page
        PurgeAllValue();
        InitValue();
        // ANI Le 17-11-2015 : Gestion du focus
        Focus := 'ID1000000002;MD' + FORMAT(TIME, 0, 1);


        CurrPage.UPDATE(FALSE);
        MESSAGE('Terminé !');
    end;

    // CFR le 05/10/2023 => R‚gie - Blocage mouvement inter-zone si PICS en cours

}

