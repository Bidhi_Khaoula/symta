page 50306 "WIIO - Liste Emplacements"
{
    // version WIIO2SFD20210929

    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = Bin;
    SourceTableView = SORTING("Location Code", Code)
                      ORDER(Ascending);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Block Movement"; Rec."Block Movement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Block Movement field.';
                }
                field(Default; Rec.Default)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Default field.';
                }
            }
        }
    }

    actions
    {
    }
}

