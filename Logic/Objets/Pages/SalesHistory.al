page 50206 "Sales History"
{
    PageType = List;
    SourceTable = "Sales Header Archive";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Customer No"; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(No; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Date; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field(Reference; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field(SalesStatus; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field(ShipmentStatus; Rec."Shipment Status")
                {
                    Caption = 'Statut Livraison';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Livraison field.';
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field(AmountVTA; Rec."Amount Including VAT")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount Including VAT field.';
                }
                field(ShipmentDate; Rec."Shipment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Date field.';
                }
                field(ContactName; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(ContactMail; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(ContactPhone; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }

                part(Lines; "Sales Line History")
                {
                    SubPageLink = "Document No." = FIELD("No."),
                                  "Document Type" = FIELD("Document Type");
                    ApplicationArea = All;
                }

            }
            group(Status)
            {
                field(Description; Status_History)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status_History field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnInit()
    begin
        Status_History := 'Expédiée / facturée';
    end;

    var
        Status_History: Text[50];
}

