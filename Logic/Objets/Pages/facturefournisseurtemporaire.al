page 50042 "facture fournisseur temporaire"
{
    CardPageID = "facture fournisseur temporaire";
    Editable = false;
    PageType = List;
    SourceTable = "Import facture tempo. fourn.";
    SourceTableView = SORTING("No séquence", "Type ligne")
                      WHERE("Type Ligne" = CONST(Entete),
                            "No de reception" = FILTER(''));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Code fournisseur"; Rec."Code fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code fournisseur field.';
                }
                field("Nom Fournisseur"; Rec."Nom Fournisseur"())
                {
                    Caption = 'Nom';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field("No Facture"; Rec."No Facture")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Facture field.';
                }
                field("Date Facture"; Rec."Date Facture")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Facture field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure recup_filtre(var FactureTempoFourn: Record "Import facture tempo. fourn.")
    begin
        CurrPage.SETSELECTIONFILTER(FactureTempoFourn);

        FactureTempoFourn.MARKEDONLY := TRUE;

        IF NOT FactureTempoFourn.FINDFIRST() THEN BEGIN
            CLEAR(FactureTempoFourn);
            FactureTempoFourn := Rec;
            FactureTempoFourn.SETRECFILTER();
        END;
    end;
}

