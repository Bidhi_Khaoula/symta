page 50002 "Pieces detachées / Consommable"
{

    CardPageID = "Pieces detachées / Consommable";
    PageType = List;
    UsageCategory = Lists;
    SourceTable = "Ligne import";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(n_ordre; Rec.n_ordre)
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the n_ordre field.';
                }
                field(lib_cpt; Rec.lib_cpt)
                {
                    Visible = lib_cptVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the lib_cpt field.';
                }
                field(num_ecriture; Rec.num_ecriture)
                {
                    Visible = num_ecritureVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the num_ecriture field.';
                }
                field(mt_euro; Rec.mt_euro)
                {
                    Visible = mt_euroVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the mt_euro field.';
                }
                field(GetItemDescription_; GetItemDescription())
                {
                    Caption = 'Désignation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field(journ; Rec.journ)
                {
                    Visible = journVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the journ field.';
                }
                field(GetPrice_; GetPrice())
                {
                    Caption = 'Prix Tarif';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Tarif field.';
                }
                field(GetDispo_; GetDispo())
                {
                    Caption = 'Disponible';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Disponible field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnInit()
    begin
        journVisible := TRUE;
        mt_euroVisible := TRUE;
        lib_cptVisible := TRUE;
    end;

    trigger OnOpenPage()
    begin
        //WF le 04-05-2010 => FarGroup Analyse Complémentaire Avril 2010 §1.1
        IF rec.GETFILTER(num_ecriture) <> '' THEN BEGIN
            lib_cptVisible := TRUE;
            num_ecritureVisible := FALSE;
            mt_euroVisible := TRUE;
            journVisible := TRUE;
        END
        ELSE BEGIN
            lib_cptVisible := FALSE;
            num_ecritureVisible := TRUE;
            mt_euroVisible := FALSE;
            journVisible := FALSE;
        END;
        //Fin WF le 04-05-2010
    end;

    var
        lib_cptVisible: Boolean;
        num_ecritureVisible: Boolean;
        mt_euroVisible: Boolean;
        journVisible: Boolean;

    procedure GetItemDescription(): Text[70]
    var
        item: Record Item;
    begin
        //WF le 04-05-2010 => FarGroup Analyse Complémentaire Avril 2010 §1.1
        //----------Nouveau code--------------//
        IF Rec.GETFILTER(num_ecriture) <> '' THEN BEGIN
            IF NOT item.GET(rec."mt_euro") THEN EXIT;
        END
        ELSE
            IF NOT item.GET(rec.num_ecriture) THEN EXIT;

        EXIT(item.Description + ' ' + item."Description 2");
        //--------FIN Nouveau code------------//


        //----------Ancien code--------------//
        /*
        IF NOT item.GET("Link Item No.") THEN
          EXIT;
        
        EXIT(item.Description + ' ' + item."Description 2");
        */
        //--------FIN Ancien code------------//

        //Fin WF le 04-05-2010

    end;

    procedure GetDispo(): Decimal
    var
        Item: Record Item;
    begin
        //WF le 04-05-2010 => FarGroup Analyse Complémentaire Avril 2010 ã1.1
        //----------Nouveau code--------------//
        IF rec.GETFILTER(num_ecriture) <> '' THEN BEGIN
            IF Item.GET(Rec.mt_euro) THEN EXIT(Item.CalcAvailability());
        END
        ELSE BEGIN
            IF Item.GET(Rec.num_ecriture) THEN EXIT(Item.CalcAvailability());
        END;
        //--------FIN Ancien code------------//

        //----------Ancien code--------------//
        /*
        IF Item.GET("Link Item No.") THEN
          EXIT(Item.CalcAvailability);
        */
        //--------FIN Ancien code------------//
        //Fin WF le 04-05-2010

    end;

    procedure GetPrice(): Decimal
    var
        Item: Record Item;
    begin
        //WF le 04-05-2010 => FarGroup Analyse Complémentaire Avril 2010 §1.1

        //----------Nouveau code--------------//
        IF Rec.GETFILTER(num_ecriture) <> '' THEN BEGIN
            IF Item.GET(Rec.mt_euro) THEN EXIT(Item.GetSalesPriceAllCustomer());
        END
        ELSE BEGIN
            IF Item.GET(Rec.num_ecriture) THEN EXIT(Item.GetSalesPriceAllCustomer());
        END;
        //--------FIN Nouveau code------------//

        //----------Ancien code--------------//
        /*
        IF Item.GET("Link Item No.") THEN
          EXIT(Item.GetSalesPriceAllCustomer);
        */
        //--------FIN Ancien code------------//

        // FIn WF Le 04-02-2010

    end;
}

