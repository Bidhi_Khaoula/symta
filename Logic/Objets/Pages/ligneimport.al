page 50092 "ligne import"
{
    CardPageID = "ligne import";
    DeleteAllowed = false;
    Editable = true;
    InsertAllowed = false;
    PageType = ListPart;
    SourceTable = "Ligne import";

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(cpt; Rec.cpt)
                {
                    Editable = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the cpt field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        Rec."Charge compte lookup"();
                    end;

                    trigger OnValidate()
                    begin
                        Rec."verif compte"();
                    end;
                }
                field(type_cpt; Rec.type_cpt)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the type_cpt field.';
                }
                field(lib_ecr; Rec.lib_ecr)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the lib_ecr field.';
                }
                field(sens; Rec.sens)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the sens field.';
                }
                field(mont; Rec.mont)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the mont field.';
                }
            }
        }
    }

    actions
    {
    }
}

