page 50032 "Remises Fournisseurs"
{
    CardPageID = "Remises Fournisseurs";
    PageType = List;
    SourceTable = "Groupe Remises Fournisseurs";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Discount Code"; Rec."Discount Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Remise field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
                field("Minimum Quantity"; Rec."Minimum Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Minimum Quantity field.';
                }
                field("Line Discount 1 %"; Rec."Line Discount 1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise ligne 1 field.';
                }
                field("Line Discount 2 %"; Rec."Line Discount 2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise ligne 2 field.';
                }
                field("Vendor Disc. Group"; Rec."Vendor Disc. Group")
                {
                    Caption = '<Groupe remise Fournisseur>';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the <Groupe remise Fournisseur> field.';
                }
            }
        }
    }
}

