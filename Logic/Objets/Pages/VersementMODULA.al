page 50081 "Versement MODULA"
{
    CardPageID = "Versement MODULA";
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = Card;
    SourceTable = Integer;
    SourceTableView = SORTING(Number)
                      ORDER(Ascending)
                      WHERE(Number = CONST(1));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(_CodeArmoire; _CodeArmoire)
            {
                Caption = 'Armoire';
                TableRelation = "Generals Parameters".Code WHERE(Type = CONST('MODULA'));
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Armoire field.';
            }
            field(_Référence; _Référence)
            {
                Caption = 'Référence';
                TableRelation = Item;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Référence field.';

                trigger OnValidate()
                begin
                    ValideArticle()
                end;
            }
            field(_CodeArticle; _CodeArticle)
            {
                Caption = 'Article';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Article field.';
            }
            field(_Designation; _Designation)
            {
                Caption = 'Désignation';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Désignation field.';
            }
            field(_Emplacement; _Emplacement)
            {
                Caption = 'Emplacement';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Emplacement field.';
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action(Valider)
                {
                    Caption = 'Valider';
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Valider action.';

                    trigger OnAction()
                    begin
                        Versement();
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        VideVariables();
    end;

    var
        Item: Record Item;
        ESK001Err: Label 'Code armoire Obligatoire !';
        ESK002Err: Label 'Code Article Obligatoire !';
        ESK003Err: Label 'Emplacement hors MODULA !';
        ESK004Msg: Label 'Versement MODULA dans Armoire %1 effectué !';
        _CodeArmoire: Code[10];
        "_Référence": Code[20];
        _CodeArticle: Code[10];
        _Designation: Text[30];
        _Emplacement: Code[10];


    procedure Versement()
    var
        NomFichier: Text[250];
        NomFichierTmp: Text[250];
        Fichier: File;
        Ligne: Text[250];
        Param: Record "Generals Parameters";
    begin
        IF _CodeArmoire = '' THEN
            ERROR(ESK001Err);
        IF Item."No." = '' THEN
            ERROR(ESK002Err);

        IF COPYSTR(_Emplacement, 1, 3) <> _CodeArmoire THEN
            ERROR(ESK003Err);

        Param.GET('MODULA', _CodeArmoire);

        NomFichierTmp := TEMPORARYPATH + 'Versement-' + _Référence + '.txt';
        NomFichier := Param.LongDescription2 + '\' + 'Versement-' + _Référence + '.txt';

        Fichier.CREATE(NomFichierTmp);
        Fichier.TEXTMODE := TRUE;

        Ligne := _Référence + '|V|' + _Référence;
        Fichier.WRITE(Ligne);

        Ligne := _Référence + '||' + _Référence + '|||||||||' + COPYSTR(_Emplacement, 4, 4) + '|0|' + COPYSTR(_Emplacement, 8, 3) + '|O||'
        ;
        Fichier.WRITE(Ligne);

        Fichier.CLOSE();

        FILE.RENAME(NomFichierTmp, NomFichier);

        VideVariables();

        MESSAGE(ESK004Msg, Param.LongDescription)
    end;

    procedure VideVariables()
    begin
        _CodeArmoire := '';
        _Référence := '';
        _CodeArticle := '';
        _Designation := '';
        _Emplacement := '';
        CLEAR(Item);
    end;

    procedure ValideArticle()
    var
        CuMultiRef: Codeunit "Gestion Multi-référence";
        CduLFunctions: Codeunit "Codeunits Functions";
        CompanyInfo: Record "Company Information";
        _Zone: Code[10];
    begin
        Item.GET(CuMultiRef.RechercheMultiReference(_Référence));
        _Designation := Item.Description;
        _CodeArticle := Item."No.";
        CompanyInfo.GET();
        CduLFunctions.ESK_GetDefaultBin(Item."No.", '', CompanyInfo."Location Code", _Emplacement, _Zone)
    end;
}

