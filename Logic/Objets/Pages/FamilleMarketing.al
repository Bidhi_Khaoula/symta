page 50240 "Famille Marketing"
{
    PageType = List;
    SourceTable = "Famille Marketing";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Code Niveau supérieur"; Rec."Code Niveau supérieur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Niveau supérieur field.';
                }
                field(Priority; Rec.Priority)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Priority field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Traduction)
            {
                Caption = 'Traductions';
                Image = Documents;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Traductions action.';

                trigger OnAction()
                var

                    Rec_Translation: Record "Translation SYMTA";
                    Frm_Translation: Page "Translation List";
                    RecordRef: RecordRef;
                begin
                    //Assigne l'enregistrement à un RecordRed
                    RecordRef.GETTABLE(Rec);
                    //Initialisation de la page et du record
                    CLEAR(Frm_Translation);
                    CLEAR(Rec_Translation);
                    Rec_Translation.SETRANGE(Record_ID, RecordRef.RECORDID);
                    Rec_Translation.SETRANGE("Table No", RecordRef.NUMBER);
                    //Rec_Translation.SETRANGE("Language Code",'ENU');
                    Frm_Translation.SETTABLEVIEW(Rec_Translation);
                    //Ouverture Page
                    Frm_Translation.RUN();
                end;
            }
        }
    }
}

