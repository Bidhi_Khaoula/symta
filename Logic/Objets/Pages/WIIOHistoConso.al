page 50312 "WIIO - Histo. Conso"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Ref_Active; Rec."No. 2")
                {
                    Caption = 'No. 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Disponible; Dispo)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo field.';
                }
                field("Janvier N"; TabValeur[1, 1])
                {
                    BlankZero = true;
                    Caption = 'Janvier N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Janvier N field.';
                }
                field("Février N"; TabValeur[1, 2])
                {
                    BlankZero = true;
                    Caption = 'Février N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Février N field.';
                }
                field("Mars N"; TabValeur[1, 3])
                {
                    BlankZero = true;
                    Caption = 'Mars N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mars N field.';
                }
                field("TRIMESTRE 1 N"; TabValeur[1, 1] + TabValeur[1, 2] + TabValeur[1, 3])
                {
                    Caption = 'TRIMESTRE 1 N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 1 N field.';
                }
                field("Avril N"; TabValeur[1, 4])
                {
                    BlankZero = true;
                    Caption = 'Avril N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avril N field.';
                }
                field("Mai N"; TabValeur[1, 5])
                {
                    BlankZero = true;
                    Caption = 'Mai N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mai N field.';
                }
                field("Juin N"; TabValeur[1, 6])
                {
                    BlankZero = true;
                    Caption = 'Juin N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juin N field.';
                }
                field("TRIMESTRE 2 N"; TabValeur[1, 4] + TabValeur[1, 5] + TabValeur[1, 6])
                {
                    Caption = 'TRIMESTRE 2 N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 2 N field.';
                }
                field("Juillet N"; TabValeur[1, 7])
                {
                    BlankZero = true;
                    Caption = 'Juillet N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juillet N field.';
                }
                field("Aout N"; TabValeur[1, 8])
                {
                    BlankZero = true;
                    Caption = 'Aout N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aout N field.';
                }
                field("Septembre N"; TabValeur[1, 9])
                {
                    BlankZero = true;
                    Caption = 'Septembre N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Septembre N field.';
                }
                field("TRIMESTRE 3 N"; TabValeur[1, 7] + TabValeur[1, 8] + TabValeur[1, 9])
                {
                    Caption = 'TRIMESTRE 3 N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 3 N field.';
                }
                field("Octobre N"; TabValeur[1, 10])
                {
                    BlankZero = true;
                    Caption = 'Octobre N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Octobre N field.';
                }
                field("Novembre N"; TabValeur[1, 11])
                {
                    BlankZero = true;
                    Caption = 'Novembre N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Novembre N field.';
                }
                field("Décembre N"; TabValeur[1, 12])
                {
                    BlankZero = true;
                    Caption = 'Décembre N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décembre N field.';
                }
                field("TRIMESTRE 4 N"; TabValeur[1, 10] + TabValeur[1, 11] + TabValeur[1, 12])
                {
                    Caption = 'TRIMESTRE 4 N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 4 N field.';
                }
                field("TOTAL  N"; TabValeur[1, 1] + TabValeur[1, 2] + TabValeur[1, 3] + TabValeur[1, 4] + TabValeur[1, 5] + TabValeur[1, 6] + TabValeur[1, 7] + TabValeur[1, 8] + TabValeur[1, 9] + TabValeur[1, 10] + TabValeur[1, 11] + TabValeur[1, 12])
                {
                    Caption = 'TOTAL  N';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TOTAL  N field.';
                }
                field("Moyenne  N"; MoyenneAnnée[1])
                {
                    Caption = 'Moyenne  N';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Moyenne  N field.';
                }
                field("Dispo  N"; StkDispo[1])
                {
                    Caption = 'Dispo  N';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo  N field.';
                }
                field("Janvier N-1"; TabValeur[2, 1])
                {
                    BlankZero = true;
                    Caption = 'Janvier N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Janvier N-1 field.';
                }
                field("Février N-1"; TabValeur[2, 2])
                {
                    BlankZero = true;
                    Caption = 'Février N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Février N-1 field.';
                }
                field("Mars N-1"; TabValeur[2, 3])
                {
                    BlankZero = true;
                    Caption = 'Mars N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mars N-1 field.';
                }
                field("TRIMESTRE 1 N-1"; TabValeur[2, 1] + TabValeur[2, 2] + TabValeur[2, 3])
                {
                    Caption = 'TRIMESTRE 1 N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 1 N-1 field.';
                }
                field("Avril N-1"; TabValeur[2, 4])
                {
                    BlankZero = true;
                    Caption = 'Avril N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avril N-1 field.';
                }
                field("Mai N-1"; TabValeur[2, 5])
                {
                    BlankZero = true;
                    Caption = 'Mai N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mai N-1 field.';
                }
                field("Juin N-1"; TabValeur[2, 6])
                {
                    BlankZero = true;
                    Caption = 'Juin N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juin N-1 field.';
                }
                field("TRIMESTRE 2 N-1"; TabValeur[2, 4] + TabValeur[2, 5] + TabValeur[2, 6])
                {
                    Caption = 'TRIMESTRE 2 N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 2 N-1 field.';
                }
                field("Juillet N-1"; TabValeur[2, 7])
                {
                    BlankZero = true;
                    Caption = 'Juillet N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juillet N-1 field.';
                }
                field("Aout N-1"; TabValeur[2, 8])
                {
                    BlankZero = true;
                    Caption = 'Aout N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aout N-1 field.';
                }
                field("Septembre N-1"; TabValeur[2, 9])
                {
                    BlankZero = true;
                    Caption = 'Septembre N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Septembre N-1 field.';
                }
                field("TRIMESTRE 3 N-1"; TabValeur[2, 7] + TabValeur[2, 8] + TabValeur[2, 9])
                {
                    Caption = 'TRIMESTRE 3 N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 3 N-1 field.';
                }
                field("Octobre N-1"; TabValeur[2, 10])
                {
                    BlankZero = true;
                    Caption = 'Octobre N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Octobre N-1 field.';
                }
                field("Novembre N-1"; TabValeur[2, 11])
                {
                    BlankZero = true;
                    Caption = 'Novembre N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Novembre N-1 field.';
                }
                field("Décembre N-1"; TabValeur[2, 12])
                {
                    BlankZero = true;
                    Caption = 'Décembre N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décembre N-1 field.';
                }
                field("TRIMESTRE 4 N-1"; TabValeur[2, 10] + TabValeur[2, 11] + TabValeur[2, 12])
                {
                    Caption = 'TRIMESTRE 4 N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 4 N-1 field.';
                }
                field("TOTAL N-1"; TabValeur[2, 1] + TabValeur[2, 2] + TabValeur[2, 3] + TabValeur[2, 4] + TabValeur[2, 5] + TabValeur[2, 6] + TabValeur[2, 7] + TabValeur[2, 8] + TabValeur[2, 9] + TabValeur[2, 10] + TabValeur[2, 11] + TabValeur[2, 12])
                {
                    Caption = 'TOTAL N-1';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TOTAL N-1 field.';
                }
                field("Moyenne N-1"; MoyenneAnnée[2])
                {
                    Caption = 'Moyenne N-1';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Moyenne N-1 field.';
                }
                field("Dispo N-1"; StkDispo[2])
                {
                    Caption = 'Dispo N-1';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo N-1 field.';
                }
                field("Janvier N-2"; TabValeur[3, 1])
                {
                    BlankZero = true;
                    Caption = 'Janvier N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Janvier N-2 field.';
                }
                field("Février N-2"; TabValeur[3, 2])
                {
                    BlankZero = true;
                    Caption = 'Février N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Février N-2 field.';
                }
                field("Mars N-2"; TabValeur[3, 3])
                {
                    BlankZero = true;
                    Caption = 'Mars N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mars N-2 field.';
                }
                field("TRIMESTRE 1 N-2"; TabValeur[3, 1] + TabValeur[3, 2] + TabValeur[3, 3])
                {
                    Caption = 'TRIMESTRE 1 N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 1 N-2 field.';
                }
                field("Avril N-2"; TabValeur[3, 4])
                {
                    BlankZero = true;
                    Caption = 'Avril N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avril N-2 field.';
                }
                field("Mai N-2"; TabValeur[3, 5])
                {
                    BlankZero = true;
                    Caption = 'Mai N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mai N-2 field.';
                }
                field("Juin N-2"; TabValeur[3, 6])
                {
                    BlankZero = true;
                    Caption = 'Juin N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juin N-2 field.';
                }
                field("TRIMESTRE 2 N-2"; TabValeur[3, 4] + TabValeur[3, 5] + TabValeur[3, 6])
                {
                    Caption = 'TRIMESTRE 2 N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 2 N-2 field.';
                }
                field("Juillet N-2"; TabValeur[3, 7])
                {
                    BlankZero = true;
                    Caption = 'Juillet N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juillet N-2 field.';
                }
                field("Aout N-2"; TabValeur[3, 8])
                {
                    BlankZero = true;
                    Caption = 'Aout N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aout N-2 field.';
                }
                field("Septembre N-2"; TabValeur[3, 9])
                {
                    BlankZero = true;
                    Caption = 'Septembre N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Septembre N-2 field.';
                }
                field("TRIMESTRE 3 N-2"; TabValeur[3, 7] + TabValeur[3, 8] + TabValeur[3, 9])
                {
                    Caption = 'TRIMESTRE 3 N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 3 N-2 field.';
                }
                field("Octobre N-2"; TabValeur[3, 10])
                {
                    BlankZero = true;
                    Caption = 'Octobre N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Octobre N-2 field.';
                }
                field("Novembre N-2"; TabValeur[3, 11])
                {
                    BlankZero = true;
                    Caption = 'Novembre N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Novembre N-2 field.';
                }
                field("Décembre N-2"; TabValeur[3, 12])
                {
                    BlankZero = true;
                    Caption = 'Décembre N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décembre N-2 field.';
                }
                field("TRIMESTRE 4 N-2"; TabValeur[3, 10] + TabValeur[3, 11] + TabValeur[3, 12])
                {
                    Caption = 'TRIMESTRE 4 N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 4 N-2 field.';
                }
                field("TOTAL N-2"; TabValeur[3, 1] + TabValeur[3, 2] + TabValeur[3, 3] + TabValeur[3, 4] + TabValeur[3, 5] + TabValeur[3, 6] + TabValeur[3, 7] + TabValeur[3, 8] + TabValeur[3, 9] + TabValeur[3, 10] + TabValeur[3, 11] + TabValeur[3, 12])
                {
                    Caption = 'TOTAL N-2';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TOTAL N-2 field.';
                }
                field("Moyenne N-2"; MoyenneAnnée[3])
                {
                    Caption = 'Moyenne N-2';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Moyenne N-2 field.';
                }
                field("Dispo N-2"; StkDispo[3])
                {
                    Caption = 'Dispo N-2';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo N-2 field.';
                }
                field("Janvier N-3"; TabValeur[4, 1])
                {
                    BlankZero = true;
                    Caption = 'Janvier N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Janvier N-3 field.';
                }
                field("Février N-3"; TabValeur[4, 2])
                {
                    BlankZero = true;
                    Caption = 'Février N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Février N-3 field.';
                }
                field("Mars N-3"; TabValeur[4, 3])
                {
                    BlankZero = true;
                    Caption = 'Mars N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mars N-3 field.';
                }
                field("TRIMESTRE 1 N-3"; TabValeur[4, 1] + TabValeur[4, 2] + TabValeur[4, 3])
                {
                    Caption = 'TRIMESTRE 1 N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 1 N-3 field.';
                }
                field("Avril N-3"; TabValeur[4, 4])
                {
                    BlankZero = true;
                    Caption = 'Avril N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avril N-3 field.';
                }
                field("Mai N-3"; TabValeur[4, 5])
                {
                    BlankZero = true;
                    Caption = 'Mai N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mai N-3 field.';
                }
                field("Juin N-3"; TabValeur[4, 6])
                {
                    BlankZero = true;
                    Caption = 'Juin N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juin N-3 field.';
                }
                field("TRIMESTRE 2 N-3"; TabValeur[4, 4] + TabValeur[4, 5] + TabValeur[4, 6])
                {
                    Caption = 'TRIMESTRE 2 N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 2 N-3 field.';
                }
                field("Juillet N-3"; TabValeur[4, 7])
                {
                    BlankZero = true;
                    Caption = 'Juillet N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juillet N-3 field.';
                }
                field("Aout N-3"; TabValeur[4, 8])
                {
                    BlankZero = true;
                    Caption = 'Aout N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aout N-3 field.';
                }
                field("Septembre N-3"; TabValeur[4, 9])
                {
                    BlankZero = true;
                    Caption = 'Septembre N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Septembre N-3 field.';
                }
                field("TRIMESTRE 3 N-3"; TabValeur[4, 7] + TabValeur[4, 8] + TabValeur[4, 9])
                {
                    Caption = 'TRIMESTRE 3 N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 3 N-3 field.';
                }
                field("Octobre N-3"; TabValeur[4, 10])
                {
                    BlankZero = true;
                    Caption = 'Octobre N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Octobre N-3 field.';
                }
                field("Novembre N-3"; TabValeur[4, 11])
                {
                    BlankZero = true;
                    Caption = 'Novembre N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Novembre N-3 field.';
                }
                field("Décembre N-3"; TabValeur[4, 12])
                {
                    BlankZero = true;
                    Caption = 'Décembre N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décembre N-3 field.';
                }
                field("TRIMESTRE 4 N-3"; TabValeur[4, 10] + TabValeur[4, 11] + TabValeur[4, 12])
                {
                    Caption = 'TRIMESTRE 4 N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 4 N-3 field.';
                }
                field("T O T A L  N-3"; TabValeur[4, 1] + TabValeur[4, 2] + TabValeur[4, 3] + TabValeur[4, 4] + TabValeur[4, 5] + TabValeur[4, 6] + TabValeur[4, 7] + TabValeur[4, 8] + TabValeur[4, 9] + TabValeur[4, 10] + TabValeur[4, 11] + TabValeur[4, 12])
                {
                    Caption = 'T O T A L  N-3';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the T O T A L  N-3 field.';
                }
                field("Moyenne N-3"; MoyenneAnnée[4])
                {
                    Caption = 'Moyenne N-3';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Moyenne N-3 field.';
                }
                field("Dispo N-3"; StkDispo[4])
                {
                    Caption = 'Dispo N-3';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo N-3 field.';
                }
                field("Janvier N-4"; TabValeur[5, 1])
                {
                    BlankZero = true;
                    Caption = 'Janvier N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Janvier N-4 field.';
                }
                field("Février N-4"; TabValeur[5, 2])
                {
                    BlankZero = true;
                    Caption = 'Février N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Février N-4 field.';
                }
                field("Mars N-4"; TabValeur[5, 3])
                {
                    BlankZero = true;
                    Caption = 'Mars N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mars N-4 field.';
                }
                field("TRIMESTRE 1 N-4"; TabValeur[5, 1] + TabValeur[5, 2] + TabValeur[5, 3])
                {
                    Caption = 'TRIMESTRE 1 N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 1 N-4 field.';
                }
                field("Avril N-4"; TabValeur[5, 4])
                {
                    BlankZero = true;
                    Caption = 'Avril N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avril N-4 field.';
                }
                field("Mai N-4"; TabValeur[5, 5])
                {
                    BlankZero = true;
                    Caption = 'Mai N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mai N-4 field.';
                }
                field("Juin N-4"; TabValeur[5, 6])
                {
                    BlankZero = true;
                    Caption = 'Juin N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juin N-4 field.';
                }
                field("TRIMESTRE 2 N-4"; TabValeur[5, 4] + TabValeur[5, 5] + TabValeur[5, 6])
                {
                    Caption = 'TRIMESTRE 2 N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 2 N-4 field.';
                }
                field("Juillet N-4"; TabValeur[5, 7])
                {
                    BlankZero = true;
                    Caption = 'Juillet N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Juillet N-4 field.';
                }
                field("Aout N-4"; TabValeur[5, 8])
                {
                    BlankZero = true;
                    Caption = 'Aout N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Aout N-4 field.';
                }
                field("Septembre N-4"; TabValeur[5, 9])
                {
                    BlankZero = true;
                    Caption = 'Septembre N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Septembre N-4 field.';
                }
                field("TRIMESTRE 3 N-4"; TabValeur[5, 7] + TabValeur[5, 8] + TabValeur[5, 9])
                {
                    Caption = 'TRIMESTRE 3 N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 3 N-4 field.';
                }
                field("Octobre N-4"; TabValeur[5, 10])
                {
                    BlankZero = true;
                    Caption = 'Octobre N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Octobre N-4 field.';
                }
                field("Novembre N-4"; TabValeur[5, 11])
                {
                    BlankZero = true;
                    Caption = 'Novembre N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Novembre N-4 field.';
                }
                field("Décembre N-4"; TabValeur[5, 12])
                {
                    BlankZero = true;
                    Caption = 'Décembre N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décembre N-4 field.';
                }
                field("TRIMESTRE 4 N-4"; TabValeur[5, 10] + TabValeur[5, 11] + TabValeur[5, 12])
                {
                    Caption = 'TRIMESTRE 4 N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TRIMESTRE 4 N-4 field.';
                }
                field("T O T A L  N-4"; TabValeur[5, 1] + TabValeur[5, 2] + TabValeur[5, 3] + TabValeur[5, 4] + TabValeur[5, 5] + TabValeur[5, 6] + TabValeur[5, 7] + TabValeur[5, 8] + TabValeur[5, 9] + TabValeur[5, 10] + TabValeur[5, 11] + TabValeur[5, 12])
                {
                    Caption = 'T O T A L  N-4';
                    DecimalPlaces = 0 : 2;
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the T O T A L  N-4 field.';
                }
                field("Moyenne N-4"; MoyenneAnnée[5])
                {
                    Caption = 'Moyenne N-4';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Moyenne N-4 field.';
                }
                field("Dispo. N-4"; StkDispo[5])
                {
                    Caption = 'Dispo. N-4';
                    DecimalPlaces = 1 : 1;
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dispo. N-4 field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    var
        "NbMoisAnnéeN": Integer;
        "Année": Integer;
        LReserve: Decimal;
        LAttendu: Decimal;
        LDispo: Decimal;
    begin

        // Calcul du dispo
        Rec.CALCFIELDS(Inventory);
        Rec.CalcAttenduReserve(LReserve, LAttendu);
        LDispo := Rec.Inventory - LReserve;
        Dispo := LDispo;

        CalculConso.CalculerConsoSur4ans(Rec."No.", TabValeur, MoyenneAnnée, WORKDATE());

        NbMoisAnnéeN := DATE2DMY(WORKDATE(), 2) - 1; // On compte pas le mois en cours

        IF NbMoisAnnéeN <> 0 THEN MoyenneAnnée[1] := MoyenneAnnée[1] / NbMoisAnnéeN;
        MoyenneAnnée[4] := MoyenneAnnée[4] / 12;
        MoyenneAnnée[3] := MoyenneAnnée[3] / 12;
        MoyenneAnnée[2] := MoyenneAnnée[2] / 12;
        MoyenneAnnée[5] := MoyenneAnnée[5] / 12;

        FOR Année := 1 TO 5 DO
            IF MoyenneAnnée[Année] <> 0 THEN
                StkDispo[Année] := LDispo / MoyenneAnnée[Année];
    end;

    var
        CalculConso: Codeunit CalculConso;
        TabValeur: array[5, 12] of Decimal;
        "MoyenneAnnée": array[5] of Decimal;
        StkDispo: array[5] of Decimal;
        Dispo: Decimal;
}

