page 50105 "Vendor Price Buffer_lm_270712"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = Card;
    SourceTable = "Vendor Price Buffer";
    SourceTableTemporary = true;
    SourceTableView = SORTING("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price") ORDER(Ascending);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = true;
                field("Vendor No."; Rec."Vendor No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor No. field.';
                }
                field(Name; Rec.Name)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field("Ref.Fournisseur"; ref_fou)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref_fou field.';
                }
                field("Code Remise"; Rec."Code Remise")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Remise field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut field.';
                }
                field(Discount; Rec.Discount)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise field.';
                }
                field("Statut Qualité"; Rec."Statut Qualité")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Qualité field.';
                }
                field("Statut Approvisionnement"; Rec."Statut Approvisionnement")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Approvisionnement field.';
                }
                field("Net Unit Price"; Rec."Net Unit Price")
                {
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Net field.';
                }
                field("Profit %"; Rec."Profit %")
                {
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % de Marge field.';
                }
                field("Unité d'achat"; Rec."Unité d'achat")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unité d''achat field.';
                }
                field(MinimumorderText; MinimumorderText)
                {
                    //BlankNumbers = BlankZero;
                    //BlankZero = true;
                    CaptionClass = Rec.FIELDCAPTION("Minimum order");
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MinimumorderText field.';
                }
                field(ByHowManyText; ByHowManyText)
                {
                    //BlankNumbers = BlankZero;
                    //BlankZero = true;
                    CaptionClass = Rec.FIELDCAPTION("By How Many");
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ByHowManyText field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field(bln_Selected; Rec.Selected)
                {
                    Visible = bln_SelectedVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Séléctionner field.';

                    trigger OnValidate()
                    begin
                        SelectedOnAfterValidate();
                    end;
                }
                field("Référence Fournisseur"; Rec."Référence Fournisseur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Fournisseur field.';
                }
            }
            group(ItemPanel)
            {
                Caption = 'Item Information';
                field(GetNbPrice_; GetNbPrice())
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetNbPrice() field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Purcha&se Prices")
            {
                Caption = 'Purcha&se Prices';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Purcha&se Prices action.';

                trigger OnAction()
                var
                    rec_PurchPrice: Record "Purchase Price";
                begin
                    rec_PurchPrice.RESET();
                    rec_PurchPrice.SETRANGE("Item No.", Rec."Item No.");
                    rec_PurchPrice.SETRANGE("Vendor No.", Rec."Vendor No.");
                    rec_PurchPrice.SETRANGE("Starting Date", 0D, WORKDATE());
                    rec_PurchPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
                    //TODOfrm_PurchPrice.SETTABLEVIEW(rec_PurchPrice);
                    //TODOfrm_PurchPrice.RUN();
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        //LM le 27-07-2012 affichage ref fourni
        rec_itemvendor.INIT();
        rec_itemvendor.SETRANGE(rec_itemvendor."Item No.", Rec."Item No.");
        rec_itemvendor.SETRANGE(rec_itemvendor."Vendor No.", Rec."Vendor No.");
        IF rec_itemvendor.FINDFIRST() THEN
            ref_fou := rec_itemvendor."Vendor Item No.";
        Unit233d39achatOnFormat(FORMAT(Rec."Unité d'achat"));
        MinimumorderText := FORMAT(Rec."Minimum order");
        MinimumorderTextOnFormat(MinimumorderText);
        ByHowManyText := FORMAT(Rec."By How Many");
        ByHowManyTextOnFormat(ByHowManyText);
    end;

    trigger OnInit()
    begin
        bln_SelectedVisible := TRUE;
    end;

    var
        rec_itemvendor: Record "Item Vendor";

        tempBuffer: Record "Vendor Price Buffer" temporary;
        Error001Lbl: Label 'Pour enlever un fournisseur de la selection, il faut en selectionner un autre.';
        ref_fou: Text[30];
        "Unité d'achatEmphasize": Boolean;
        "Minimum orderEmphasize": Boolean;
        MinimumorderText: Text[1024];
        "By How ManyEmphasize": Boolean;
        ByHowManyText: Text[1024];
        bln_SelectedVisible: Boolean;

    procedure InsertBufferRecord(p_ReqLine: Record "Requisition Line")
    begin
        Rec.DELETEALL();
        tempBuffer.DELETEALL();
        Rec.SetRecords(p_ReqLine, tempBuffer);

        IF tempBuffer.FINDFIRST() THEN
            REPEAT
                Rec := tempBuffer;
                IF NOT Rec.INSERT() THEN Rec.MODIFY();
            UNTIL tempBuffer.NEXT() = 0;

        IF Rec.FINDFIRST() THEN;

        CurrPage.UPDATE(FALSE);
    end;

    procedure InsertBufferRecordByMulti(ItemNo: Code[20]; QteDde: Decimal; VendorNo: Code[20]; TypeCde: Integer)
    begin
        // MC Le 19-07-2012 => On veut pouvoir gérer par type de commande donc paramètre en plus
        Rec.DELETEALL();
        tempBuffer.DELETEALL();
        Rec.SetRecordsByMulti(ItemNo, QteDde, VendorNo, tempBuffer, TypeCde, FALSE);

        IF tempBuffer.FINDFIRST() THEN
            REPEAT
                Rec := tempBuffer;
                IF NOT Rec.INSERT() THEN Rec.MODIFY();
            UNTIL tempBuffer.NEXT() = 0;

        IF Rec.FINDFIRST() THEN;

        CurrPage.UPDATE(FALSE);
    end;

    procedure HideSelection(bln_Visible: Boolean)
    begin
        bln_SelectedVisible := bln_Visible;
    end;

    procedure ShowPrices()
    var
    begin
    end;

    procedure ShowLineDisc()
    begin
    end;

    procedure GetNbPrice() rtn_int: Integer
    var
        //PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        EXIT(CduLFunctions.NoOfPurchPriceByItemVendor(Rec."Vendor No.", Rec."Item No.", '', '', '', WORKDATE(), TRUE));
    end;

    local procedure SelectedOnAfterValidate()
    var
        rec_ReqLine: Record "Requisition Line";
    begin
        // Mise à jour la ligne de demande d'achat et de la quantité de la ligne.
        IF Rec.Selected THEN BEGIN
            rec_ReqLine.RESET();
            rec_ReqLine.GET(Rec."Worksheet Template Name", Rec."Journal Batch Name", Rec."Line No.");
            rec_ReqLine.VALIDATE("Vendor No.", Rec."Vendor No.");
            rec_ReqLine.MODIFY(TRUE);
            InsertBufferRecord(rec_ReqLine);
        END
        ELSE BEGIN
            MESSAGE(Error001Lbl);
            Rec.Selected := TRUE;
        END;
    end;

    local procedure Unit233d39achatOnFormat(Text: Text[1024])
    var
    //   Litem: Record Item;
    begin
        /*
        IF Litem.GET("Item No.") THEN
          IF Litem."Base Unit of Measure" <> "Unité d'achat" THEN
            BEGIN
              CurrForm."Unité d'achat".UPDATEFORECOLOR(255);
              CurrForm."Unité d'achat".UPDATEFONTBOLD(TRUE);
            END;
        */

        IF (Text <> '1') AND (Text <> '0') THEN
            "Unité d'achatEmphasize" := TRUE;


    end;

    local procedure MinimumorderTextOnFormat(var Text: Text[1024])
    begin
        IF (Text <> '0.00') AND (Text <> '1.00') AND (Text <> '0,00') AND (Text <> '1,00') THEN
            "Minimum orderEmphasize" := TRUE;

    end;

    local procedure ByHowManyTextOnFormat(var Text: Text[1024])
    begin
        IF (Text <> '0.00') AND (Text <> '1.00') AND (Text <> '0,00') AND (Text <> '1,00') THEN
            "By How ManyEmphasize" := TRUE;
    end;
}

