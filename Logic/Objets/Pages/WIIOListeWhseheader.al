page 50302 "WIIO - Liste Whse header"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Warehouse Shipment Header";

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Packing List No."; Rec."Packing List No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';
                }
                field("Date Création"; Rec."Date Création")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Line Count"; Rec."Line Count")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre de ligne field.';
                }
                field("Destination Name"; Rec.GetDestinationName())
                {
                    Caption = 'Nom Destination';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Destination field.';
                }
                field("Poids Theorique"; Rec.CalcPoids())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CalcPoids() field.';
                }
                field("Destination Country"; Rec.GetDestinationCountry())
                {
                    Caption = 'Pays Destination';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pays Destination field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        //"Destination Name" := LireNomDestination;
    end;

    var


    local procedure LireNomDestination()
    begin
    end;
}

