page 50039 "Appro. SYMTA Stk Factbox"
{
    PageType = CardPart;
    SourceTable = "Requisition Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Stock)
            {
                Caption = 'Stock';
                field("Inventory - _Reservé + _Attendu"; Item.Inventory - _Reservé + _Attendu)
                {
                    Caption = 'Stock A Terme';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock A Terme field.';
                }
                field(Inventory; Item.Inventory)
                {
                    Caption = 'Stock';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock field.';
                }
                field(_Attendu; _Attendu)
                {
                    Caption = 'Attendu Global';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Attendu Global field.';

                    trigger OnDrillDown()
                    var
                        ligne_achat: Record "Purchase Line";
                        frm_achat: Page "Purchase Lines";
                    begin

                        ligne_achat.RESET();
                        ligne_achat.SETRANGE(ligne_achat."No.", Rec."No.");
                        ligne_achat.SETFILTER(ligne_achat."Outstanding Quantity", '>%1', 0);

                        frm_achat.EDITABLE(FALSE);
                        frm_achat.SETTABLEVIEW(ligne_achat);
                        frm_achat.RUNMODAL();
                    end;
                }
                field(_Reservé; _Reservé)
                {
                    Caption = 'Réservé Global';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Réservé Global field.';
                }
                field("Qty. on Purchase Return"; Item."Qty. on Purchase Return")
                {
                    BlankZero = true;
                    Caption = 'Retours Achats';
                    Editable = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Retours Achats field.';
                }
            }
            group(Projection)
            {
                Caption = 'Projection';
                field("Projection Mini"; Rec."Projection Mini")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Projection Mini field.';
                }
                field("Projection Maxi"; Rec."Projection Maxi")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Projection Maxi field.';
                }
            }
            group(Calcul)
            {
                Caption = 'Calcul';
                field("Stock Calculé"; Rec."Stock Calculé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Calculé field.';
                }
                field("Attendu Calculé"; Rec."Attendu Calculé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Attendu Calculé field.';
                }
                field("Reservé Calculé"; Rec."Reservé Calculé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reservé Calculé field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        Item.GET(Rec."No.");
        Item.CalcAttenduReserve(_Reservé, _Attendu);
    end;

    var
        Item: Record Item;
        "_Reservé": Decimal;
        _Attendu: Decimal;
}

