page 50252 "PSM Employé"
{
    PageType = Card;
    SourceTable = "Warehouse Employee";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(General)
            {
                field("User ID"; Rec."User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the User ID field.';
                }
            }
        }
    }

    actions
    {
    }
}

