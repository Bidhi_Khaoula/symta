// page 50115 "Salesperson/Purchaser test lis"
// {
//     CardPageID = "Salesperson/Purchaser test lis";
//     PageType = List;
//     SourceTable = "Salesperson/Purchaser Test";

//     layout
//     {
//         area(content)
//         {
//             repeater(content1)
//             {
//                 ShowCaption = false;
//                 field(Code; Rec.Code)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Name; Rec.Name)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Commission %"; Rec."Commission %")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Global Dimension 1 Code"; Rec."Global Dimension 1 Code")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Global Dimension 2 Code"; Rec."Global Dimension 2 Code")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("E-Mail"; Rec."E-Mail")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Phone No."; Rec."Phone No.")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Next To-do Date"; Rec."Next To-do Date")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("No. of Opportunities"; Rec."No. of Opportunities")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Estimated Value (LCY)"; Rec."Estimated Value (LCY)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Calcd. Current Value (LCY)"; Rec."Calcd. Current Value (LCY)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Date Filter"; Rec."Date Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("No. of Interactions"; Rec."No. of Interactions")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Cost (LCY)"; Rec."Cost (LCY)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Duration (Min.)"; Rec."Duration (Min.)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Job Title"; Rec."Job Title")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Action Taken Filter"; Rec."Action Taken Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Sales Cycle Filter"; Rec."Sales Cycle Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Sales Cycle Stage Filter"; Rec."Sales Cycle Stage Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Probability % Filter"; Rec."Probability % Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Completed % Filter"; Rec."Completed % Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Avg. Estimated Value (LCY)"; Rec."Avg. Estimated Value (LCY)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Avg.Calcd. Current Value (LCY)"; Rec."Avg.Calcd. Current Value (LCY)")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Contact Filter"; Rec."Contact Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Contact Company Filter"; Rec."Contact Company Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Campaign Filter"; Rec."Campaign Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Estimated Value Filter"; Rec."Estimated Value Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Calcd. Current Value Filter"; Rec."Calcd. Current Value Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Chances of Success % Filter"; Rec."Chances of Success % Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("To-do Status Filter"; Rec."To-do Status Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Closed To-do Filter"; Rec."Closed To-do Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Priority Filter"; Rec."Priority Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Team Filter"; Rec."Team Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Opportunity Entry Exists"; Rec."Opportunity Entry Exists")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("To-do Entry Exists"; Rec."To-do Entry Exists")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Close Opportunity Filter"; Rec."Close Opportunity Filter")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Search E-Mail"; Rec."Search E-Mail")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("E-Mail 2"; Rec."E-Mail 2")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Address; Rec.Address)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Address 2"; Rec."Address 2")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(City; Rec.City)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Fax No."; Rec."Fax No.")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Post Code"; Rec."Post Code")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Rate ADV"; Rec."Rate ADV")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(test_vincent_numero; test_vincent_numero)
//                 {
//                     ApplicationArea = All;
//                 }
//             }
//         }
//     }

//     actions
//     {
//     }
// }

