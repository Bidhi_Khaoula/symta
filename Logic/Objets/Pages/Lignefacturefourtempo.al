page 50044 "Ligne facture four. tempo"
{

    CardPageID = "Ligne facture four. tempo";
    Editable = true;
    InsertAllowed = false;
    PageType = List;
    SourceTable = "Import facture tempo. fourn.";
    SourceTableView = SORTING("No séquence", "Type ligne")
                      ORDER(Ascending)
                      WHERE("Type Ligne" = CONST(Ligne),
                            "date intégration" = FILTER(''));

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = true;
                field("erreur integration"; Rec."erreur integration")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the erreur integration field.';
                }
                field("Type erreur"; Rec."Type erreur")
                {
                    Editable = false;
                    Image = Message;
                    // OptionCaption = Bitmap53,Bitmap45,Bitmap32;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type erreur field.';

                    trigger OnAssistEdit()
                    begin
                        ActionCorrection();
                    end;
                }
                field("Code fournisseur"; Rec."Code fournisseur")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code fournisseur field.';
                }
                field("No Facture"; Rec."No Facture")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Facture field.';
                }
                field("Date Facture"; Rec."Date Facture")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Facture field.';
                }
                field("Code article SYMTA"; Rec."Code article SYMTA")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article SYMTA field.';
                }
                field("No. 2"; Item."No. 2")
                {
                    Caption = 'Référence active';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active field.';
                }
                field("Code article Fournisseur"; Rec."Code article Fournisseur")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article Fournisseur field.';
                }
                field(Désignation; Rec.Désignation)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field(Quantité; Rec.Quantité)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field("Pu net"; Rec."Pu net")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pu net field.';
                }
                field("Total Net"; Rec."Total Net")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net field.';
                }
                field("No commande"; Rec."No commande")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande field.';
                }
                field("ligne commande"; Rec."ligne commande")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ligne commande field.';
                }
                field("No commande affecté"; Rec."No commande affecté")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande affecté field.';
                }
                field("No ligne de commande"; Rec."No ligne de commande")
                {
                    Caption = 'No ligne de commande affectée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No ligne de commande affectée field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Rafraichir)
            {
                Caption = 'Rafraichir';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Rafraichir action.';

                trigger OnAction()
                begin
                    Refresh();
                end;
            }
            action("Multi-consultation")
            {
                Caption = 'Multi-consultation';
                Promoted = true;
                PromotedCategory = Process;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-consultation action.';

                trigger OnAction()
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    FrmQuid.InitArticle(rec."Code article SYMTA");

                    FrmQuid.RUNMODAL();
                end;
            }
            action("Résoudre")
            {
                Caption = 'Résoudre';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Résoudre action.';

                trigger OnAction()
                begin
                    ActionCorrection();
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        IF NOT Item.GET(rec."Code article SYMTA") THEN Item.INIT();
    end;

    trigger OnOpenPage()
    var
        cpt: Integer;
    begin
        Refresh();
    end;

    var
        TMPfactureFourn: Record "Import facture tempo. fourn.";
        Item: Record Item;

        Gestion: Codeunit "Réception Fournisseur";

        Error001: Label 'Vous ne pouvez pas attacher plus que possible.';

        Confirm001: Label 'Attention, vous êtes sur le point de valider cet écart. Etes vous sur ?';
        "Integration en cours": Boolean;

    procedure ActionCorrection()
    var
        LigneAchat: Record "Purchase Line";
        New_FacTmp: Record "Import facture tempo. fourn.";
        TempLigneAchatToChoose: Record "Purchase Line" temporary;
        "TMP_Importfacturetempo.fourn.": Record "Import facture tempo. fourn.";
        formCorcetion: Page "Détail Ligne Facture";
        FormSelectionLigneAchat: Page "Purchase Lines to attach";
        IsMaj: Boolean;
    begin
        // Permet de gérer les partielles
        IsMaj := FALSE;
        // FIN permet de gérer les partielles
        CASE rec."Type erreur" OF
            1:
                BEGIN
                    CLEAR(TMPfactureFourn);
                    TMPfactureFourn := Rec;
                    TMPfactureFourn.SETRECFILTER();
                    CLEAR(formCorcetion);
                    formCorcetion.LOOKUPMODE := TRUE;
                    formCorcetion.SETTABLEVIEW(TMPfactureFourn);
                    IF formCorcetion.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        IF CONFIRM(Confirm001) THEN BEGIN
                            formCorcetion.GETRECORD(TMPfactureFourn);
                            Gestion."Correction Erreur ligne"(TMPfactureFourn);
                            IF TMPfactureFourn."No commande" = '' THEN
                                Gestion."erreur ligne non affecté"(TMPfactureFourn)
                            ELSE
                                Gestion."erreur ligne affecté"(TMPfactureFourn);
                            CurrPage.UPDATE(FALSE);
                        END;
                    END;
                END;
            2:
                BEGIN
                    LigneAchat.RESET();
                    LigneAchat.SETRANGE("Document Type", LigneAchat."Document Type"::Order);
                    LigneAchat.SETRANGE("Buy-from Vendor No.", Rec."Code fournisseur");
                    LigneAchat.SETRANGE(Type, LigneAchat.Type::Item);

                    IF Rec."Code article Fournisseur" <> '' THEN
                        LigneAchat.SETRANGE("Item Reference No.", Rec."Code article Fournisseur")
                    ELSE
                        LigneAchat.SETRANGE("No.", Rec."Code article SYMTA");


                    LigneAchat.SETFILTER("Outstanding Quantity", '<>%1', 0);
                    IF LigneAchat.ISEMPTY AND (LigneAchat.GETFILTER("Item Reference No.") <> '') THEN BEGIN
                        LigneAchat.SETRANGE("Item Reference No.");
                        LigneAchat.SETRANGE("No.", Rec."Code article SYMTA");
                    END;

                    // MC Le 14-09-2011 => Gestion des attaches partielles
                    // Remise de la quantité à attacher à zéro sur toutes les lignes d'achats.
                    //LigneAchat.MODIFYALL(LigneAchat."Qty to attach", 0);
                    //LigneAchat.MODIFYALL(LigneAchat."Qté sur facture", Quantité);
                    //COMMIT;
                    // On ne veut pas afficher les lignes qui sont déjà séléctionnées.
                    LigneAchat.CLEARMARKS();
                    IF LigneAchat.FINDFIRST() THEN
                        REPEAT
                            LigneAchat.CALCFIELDS("Qty On Fac Receipt", "Qty On Receipt Order Manual");
                            IF LigneAchat."Qty On Fac Receipt" + LigneAchat."Qty On Receipt Order Manual" < LigneAchat."Outstanding Quantity" THEN
                                LigneAchat.MARK(TRUE);
                        UNTIL LigneAchat.NEXT() = 0;
                    LigneAchat.MARKEDONLY(TRUE);
                    TempLigneAchatToChoose.RESET();
                    TempLigneAchatToChoose.DELETEALL();
                    IF LigneAchat.FINDSET() THEN
                        REPEAT
                            TempLigneAchatToChoose := LigneAchat;
                            TempLigneAchatToChoose."Qty to attach" := 0;
                            TempLigneAchatToChoose."Qté sur facture" := rec.Quantité;
                            TempLigneAchatToChoose."N° Fac" := rec."No Facture";
                            TempLigneAchatToChoose.Insert();
                        UNTIL LigneAchat.NEXT() = 0;
                    // FIN MC Le 14-09-2011.

                    CLEAR(FormSelectionLigneAchat);
                    FormSelectionLigneAchat.LOOKUPMODE := TRUE;
                    FormSelectionLigneAchat.SETRECORD(TempLigneAchatToChoose);
                    //     FormSelectionLigneAchat.SETRECORD(TempLigneAchatToChoose);
                    ///     FormSelectionLigneAchat.SETTABLEVIEW(TempLigneAchatToChoose);
                    FormSelectionLigneAchat.InitTmpfacFou(Rec, TempLigneAchatToChoose);
                    IF FormSelectionLigneAchat.RUNMODAL() = ACTION::LookupOK THEN
                     //IF FORM.RUNMODAL(50029,TempLigneAchatToChoose)=ACTION::LookupOK THEN
                     BEGIN
                        FormSelectionLigneAchat.GETRECORD(TempLigneAchatToChoose);
                        IF CONFIRM(Confirm001) THEN BEGIN
                            LigneAchat := TempLigneAchatToChoose;
                            // MC Le 14-09-2011 => Gestion des attaches partielles
                            IF (LigneAchat."Qty to attach" <> 0) THEN BEGIN
                                IF (LigneAchat."Qty to attach" > rec.Quantité) THEN
                                    ERROR(Error001)
                                ELSE BEGIN
                                    // On va voir si il existe déjà une ligne liée à la ligne de commande.
                                    New_FacTmp.SETRANGE("No commande affecté", LigneAchat."Document No.");
                                    New_FacTmp.SETRANGE("No ligne de commande", LigneAchat."Line No.");
                                    New_FacTmp.SETRANGE("No Facture", rec."No Facture");
                                    IF New_FacTmp.FINDFIRST() THEN BEGIN
                                        New_FacTmp.Quantité := New_FacTmp.Quantité + LigneAchat."Qty to attach";
                                        New_FacTmp."Code article SYMTA" := LigneAchat."No.";
                                        New_FacTmp.MODIFY();
                                        // Mise à jour de la quantité de base
                                        rec.Quantité -= LigneAchat."Qty to attach";
                                        rec.MODIFY();
                                        // Si quantité de base est devenu 0 alors suppression de la ligne
                                        IF rec.Quantité = 0 THEN
                                            rec.DELETE();
                                    END
                                    ELSE
                                          // Si la ligne de commande n'est pas encore liée.
                                          BEGIN
                                        // On regarde si c'est du partielle
                                        IF (LigneAchat."Qty to attach" < rec.Quantité) THEN BEGIN
                                            New_FacTmp.INIT();
                                            New_FacTmp := Rec;
                                            New_FacTmp."Code article SYMTA" := LigneAchat."No.";
                                            New_FacTmp.Quantité := LigneAchat."Qty to attach";
                                            New_FacTmp."No séquence" := New_FacTmp.GetNextEntryNo();
                                            New_FacTmp."No commande affecté" := LigneAchat."Document No.";
                                            New_FacTmp."No ligne de commande" := LigneAchat."Line No.";
                                            New_FacTmp."No commande" := LigneAchat."Document No.";
                                            New_FacTmp."ligne commande" := LigneAchat."Line No.";

                                            New_FacTmp.Insert();
                                            // Mise à jour de la quantité de base
                                            rec.Quantité -= LigneAchat."Qty to attach";
                                            rec.MODIFY();
                                        END
                                        // Si ce n'est pas du partielle on modifie simplement la ligne de base
                                        ELSE BEGIN
                                            rec."No commande affecté" := LigneAchat."Document No.";
                                            rec."No ligne de commande" := LigneAchat."Line No.";
                                            rec."No commande" := LigneAchat."Document No.";
                                            rec."ligne commande" := LigneAchat."Line No.";
                                            rec."Code article SYMTA" := LigneAchat."No.";

                                            rec.MODIFY();
                                        END;
                                    END;
                                    CurrPage.UPDATE(FALSE);
                                    "TMP_Importfacturetempo.fourn.".COPY(Rec);
                                    IF "TMP_Importfacturetempo.fourn.".FINDFIRST() THEN
                                        REPEAT
                                            IF "TMP_Importfacturetempo.fourn."."Type erreur" <> 0 THEN
                                                Gestion."erreur ligne affecté"("TMP_Importfacturetempo.fourn.");
                                        UNTIL "TMP_Importfacturetempo.fourn.".NEXT() = 0;
                                    CurrPage.UPDATE(FALSE);
                                END;
                            END;
                        END;
                    END;
                END;
        END;
    end;

    procedure Refresh()
    begin
        IF NOT "Integration en cours" THEN BEGIN
            TMPfactureFourn.COPY(Rec);
            IF TMPfactureFourn.FINDFIRST() THEN
                REPEAT
                    IF TMPfactureFourn."No commande" = '' THEN BEGIN
                        Gestion."erreur ligne non affecté"(TMPfactureFourn);
                        IF (TMPfactureFourn."Type erreur" = 0) AND (TMPfactureFourn."No commande" = '') THEN
                            ERROR('erreur control');
                    END
                    ELSE
                        Gestion."erreur ligne affecté"(TMPfactureFourn);
                UNTIL TMPfactureFourn.NEXT() = 0;
        END;
    end;

    local procedure TypeerreurOnPush()
    begin
        ActionCorrection();
    end;
}

