page 50099 "Paramètres Cadre Contrat"
{
    CardPageID = "parametre operation/Bordereau";
    PageType = Card;
    SourceTable = "Parametre Edition Contrat";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(Name; rec.Name)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Name field.';
            }
            field(Image9; rec."Image 9")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 9 field.';
            }
            field(Image8; rec."Image 8")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 8 field.';
            }
            field(Image7; rec."Image 7")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 7 field.';
            }
            field(Image6; rec."Image 6")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 6 field.';
            }
            field(Image5; rec."Image 5")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 5 field.';
            }
            field(Image4; rec."Image 4")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 4 field.';
            }
            field(Image3; rec."Image 3")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 3 field.';
            }
            field(Image2; rec."Image 2")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 2 field.';
            }
            field(Image1; rec."Image 1")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Image 1 field.';
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Image 1")
            {
                Caption = 'Image 1';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 1 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 1".HASVALUE;
                    IF rec."Image 1".Import('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001Qst, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export2)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 1".HASVALUE THEN
                        rec."Image 1".EXPORT('*.BMP');
                end;
            }
            action("Image 2")
            {
                Caption = 'Image 2';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 2 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 2".HASVALUE;
                    IF rec."Image 2".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001Qst, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export3)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 2".HASVALUE THEN
                        rec."Image 2".EXPORT('*.BMP');
                end;
            }
            action("Image 3")
            {
                Caption = 'Image 3';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 3 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 3".HASVALUE;
                    IF rec."Image 3".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export4)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 3".HASVALUE THEN
                        rec."Image 3".EXPORT('*.BMP');
                end;
            }
            action("Image 4")
            {
                Caption = 'Image 4';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 4 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 4".HASVALUE;
                    IF rec."Image 4".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export5)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 4".HASVALUE THEN
                        rec."Image 4".EXPORT('*.BMP');
                end;
            }
            action("Image 5")
            {
                Caption = 'Image 5';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 5 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 5".HASVALUE;
                    IF rec."Image 5".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001Qst, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export6)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 5".HASVALUE THEN
                        rec."Image 5".EXPORT('*.BMP');
                end;
            }
            action("Image 6")
            {
                Caption = 'Image 6';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 6 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 6".HASVALUE;
                    IF rec."Image 6".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export7)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 6".HASVALUE THEN
                        rec."Image 6".EXPORT('*.BMP');
                end;
            }
            action("Image 7")
            {
                Caption = 'Image 7';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 7 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 7".HASVALUE;
                    IF rec."Image 7".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export8)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 7".HASVALUE THEN
                        rec."Image 7".EXPORT('*.BMP');
                end;
            }
            action("Image 8")
            {
                Caption = 'Image 8';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 8 action.';

                trigger OnAction()
                begin
                    PictureExists := Rec."Image 8".HASVALUE;
                    IF Rec."Image 8".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export9)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF Rec."Image 8".HASVALUE THEN
                        Rec."Image 8".EXPORT('*.BMP');
                end;
            }
            action("Image_9")
            {
                Caption = 'Image 9';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Image 9 action.';

                trigger OnAction()
                begin
                    PictureExists := rec."Image 9".HASVALUE;
                    IF rec."Image 9".IMPORT('*.BMP') = '' THEN
                        EXIT;
                    IF PictureExists THEN
                        IF NOT CONFIRM(Text001QSt, FALSE) THEN
                            EXIT;
                    CurrPage.SAVERECORD();
                end;
            }
            action(Export)
            {
                Caption = 'Export';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Export action.';

                trigger OnAction()
                begin
                    IF rec."Image 9".HASVALUE THEN
                        rec."Image 9".EXPORT('*.BMP');
                end;
            }
        }
    }

    var
        PictureExists: Boolean;
        Text001Qst: Label 'Do you want to replace the existing picture?';
}

