page 50322 "Packing List Subform1"
{
    ApplicationArea = all;
    Caption = 'Détail articles';
    InsertAllowed = false;
    PageType = ListPart;
    SourceTable = "Packing List Line";
    SourceTableView = SORTING("Whse. Shipment No.", "WSL Sorting Sequence No.", "Line No.")
                      ORDER(Ascending);

    layout
    {
        area(content)
        {
            repeater("Général")
            {
                Caption = 'Général';
                field("Line No."; Rec."Line No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° ligne field.';
                }
                field("Line Status"; Rec."Line Status")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Whse. Shipment No."; Rec."Whse. Shipment No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° expédition entrepôt field.';
                }
                field("Posted Source No."; Rec."Posted Source No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° origine enreg. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° article field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation 2 field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Package No."; Rec."Package No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° colis field.';

                    trigger OnValidate()
                    begin
                        // indispensable pour mettre à jour les champs calculés de toutes les pages.
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                field("Quantity to Ship"; Rec."Quantity to Ship")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité à expédier field.';
                }
                field("Line Quantity"; Rec."Line Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité de la ligne field.';

                    trigger OnValidate()
                    begin
                        // indispensable pour mettre à jour les champs calculés de toutes les pages.
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                field("Unit Weight"; Rec."Unit Weight")
                {
                    Enabled = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids unitaire field.';
                }
                field("Line Weight"; Rec."Line Weight")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids de la ligne field.';
                }
                field("WIIO Préparation"; Rec."WIIO Préparation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the WIIO Préparation field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Ligne)
            {
                Caption = 'Ligne';
                action("Dupliquer ligne")
                {
                    Caption = 'Dupliquer ligne';
                    Image = Copy;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Dupliquer ligne action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.LineDuplicate(Rec);
                    end;
                }
            }
        }
    }

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";
}

