page 50201 "Customer Page"
{
    DelayedInsert = false;
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = Document;
    SourceTable = customer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater("Général")
            {
                Editable = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(prenom; Rec."Search Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Search Name field.';
                }
            }
            group("Liste des commandes")
            {
                Editable = true;
                Visible = true;
                part(Sales; "Sales Header")
                {
                    Editable = false;
                    SubPageLink = "Sell-to Customer No." = FIELD("No.");
                    Visible = true;
                    ApplicationArea = All;
                }
            }
            group("Liste des commandes archivees")
            {
                part(SalesHistory; "Sales History")
                {
                    SubPageLink = "Sell-to Customer No." = FIELD("No.");
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
    }
}

