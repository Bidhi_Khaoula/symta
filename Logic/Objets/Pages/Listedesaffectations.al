page 50214 "Liste des affectations"
{
    Editable = false;
    PageType = List;
    SourceTable = "Affectation Réception";
    SourceTableView = SORTING("Nom Fournisseur", "Date commande")
                    ORDER(Ascending)
                    WHERE(Closed = CONST(false));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Receipt No."; Rec."Receipt No.")
                {
                    StyleExpr = Style;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Réception field.';
                }
                field("Receipt Line No."; Rec."Receipt Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Ligne réception field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° fournisseur field.';
                }
                field("Nom Fournisseur"; Rec."Nom Fournisseur")
                {
                    Caption = 'Nom';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field("No Cde Client"; Rec."No Cde Client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Cde Client field.';
                }
                field("N° Article 2"; Rec."N° Article 2")
                {
                    Caption = 'N° Article 2';
                    StyleExpr = Style;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article 2 field.';
                }
                field(Designation; Rec.Designation)
                {
                    Caption = 'Désignation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Designation 2"; Rec."Designation 2")
                {
                    Caption = 'Désignation 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation 2 field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field("Quantity To Receive"; Rec."Quantity To Receive")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité à recevoir field.';
                }
                field("Sales Order No."; Rec."Sales Order No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Commande vente field.';
                }
                field("Sales Order Line No."; Rec."Sales Order Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Ligne vente field.';
                }
                field("N° Donneur d'ordre"; Rec."N° Donneur d'ordre")
                {
                    Caption = 'N° Donneur d''ordre';
                    ApplicationArea = All;
                    StyleExpr = gStyleExp_Export;
                    ToolTip = 'Specifies the value of the N° Donneur d''ordre field.';
                }
                field("Nom du destinataire"; Rec."Nom du destinataire")
                {
                    Caption = 'Nom destinataire';
                    ApplicationArea = All;
                    StyleExpr = gStyleExp_Export;
                    ToolTip = 'Specifies the value of the Nom destinataire field.';
                }
                field("Date commande"; Rec."Date commande")
                {
                    Caption = 'Date commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date commande field.';
                }
                field("Quantité restante"; Rec."Quantité restante")
                {
                    Caption = 'Quantité restante';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité restante field.';
                }
                field("Date livraison demandée"; Rec."Date livraison demandée")
                {
                    Caption = 'Date livraison demandée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison demandée field.';
                }
                field("Code utilisateur création"; Rec."Code utilisateur création")
                {
                    Caption = 'Code utilisateur création cde';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur création cde field.';
                }
                field("<Control1100284020>"; Rec."Type de commande")
                {
                    Caption = 'Type de commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("N° Doc externe"; Rec."N° Doc externe")
                {
                    Caption = 'N° Document externes';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Document externes field.';
                }
                field("% remise ligne"; Rec."% remise ligne")
                {
                    Caption = '% Remise Ligne';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise Ligne field.';
                }
                field(Closed; Rec.Closed)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traité field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(CocheLigne)
            {
                Caption = 'Traiter';
                Image = Evaluate;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Traiter action.';

                trigger OnAction()
                begin
                    CurrPage.SETSELECTIONFILTER(Rec);
                    Rec.MODIFYALL(Closed, TRUE);
                    Rec.MARKEDONLY := FALSE;
                    Rec.RESET();
                    Rec.SETRANGE(Closed, FALSE);
                    CurrPage.UPDATE(FALSE);
                end;
            }
            action(ShowSalesOrder)
            {
                Caption = 'Afficher commande vente';
                Image = Sales;
                Promoted = true;
                PromotedIsBig = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Afficher commande vente action.';

                trigger OnAction()
                var
                    rec_salesheader: Record "Sales Header";
                    pge_SalesHeader: Page "Sales Order";
                begin
                    rec_salesheader.GET(1, Rec."Sales Order No.");
                    rec_salesheader.SETRECFILTER();
                    pge_SalesHeader.SETTABLEVIEW(rec_salesheader);
                    pge_SalesHeader.RUN();
                end;
            }
            action(SupprimerAffectation)
            {
                Caption = 'Epurer affectations';
                ApplicationArea = All;
                Image = DeleteAllBreakpoints;
                ToolTip = 'Executes the Epurer affectations action.';
                trigger OnAction()
                begin
                    // CFR le 04/10/2023 - R‚gie : Fonction de suppression des affectations inutiles
                    DeleteAffectationReception();
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        /*
        SalesHeader.INIT();
        Item.INIT();
        Vendor.INIT();
        
        IF SalesHeader.GET(1, "Sales Order No.") THEN;
        IF SalesLine.GET(1, "Sales Order No.", "Sales Order Line No.")THEN;
        IF Item.GET("Item No.") THEN;
        IF Vendor.GET("Vendor No.") THEN;
        */
        CalcStyle();

    end;

    trigger OnOpenPage()
    begin
        // CFR le 04/10/2023 - R‚gie : modification du filtre pour gain de performance (… confirmer) >> pass‚ dans les propri‚t‚s de la page
        //SETRANGE(Closed, FALSE);
    end;

    var
        Style: Text;
        gStyleExp_Export: Text;

    local procedure CalcStyle()
    VAR
        lCustomer: Record Customer;
    begin
        Style := 'Normal';
        IF Rec.Urgence THEN Style := 'Unfavorable';

        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        gStyleExp_Export := '';
        IF (lCustomer.GET(Rec."N° Donneur d'ordre")) AND (lCustomer."Salesperson Code" = '19') THEN
            gStyleExp_Export := 'Unfavorable';
        //FIN CFR le 05/04/2023
    end;

    LOCAL PROCEDURE DeleteAffectationReception();
    VAR
        lAffectationReception: Record "Affectation Réception";
        lEndDate: Date;
        lTotal: Integer;
    BEGIN
        // CFR le 04/10/2023 - R‚gie : Fonction de suppression des affectations inutiles
        lEndDate := CALCDATE('<-3M>', TODAY());
        lTotal := lAffectationReception.COUNT();
        lAffectationReception.SETRANGE(Closed, TRUE);
        lAffectationReception.SETRANGE("Date commande", 0D, lEndDate);
        IF lAffectationReception.FINDSET() THEN BEGIN
            IF CONFIRM(STRSUBSTNO('Voulez vous supprimer les affectation terminées concernant des date de commandes antérieures à %1 ?\Nombre de lignes concern‚es : %2/%3', lEndDate, lAffectationReception.COUNT(), lTotal), FALSE) THEN BEGIN
                lAffectationReception.DELETEALL();
                MESSAGE('Traitement terminé');
            END;
        END
        ELSE
            MESSAGE('Pas d''affectation à supprimer');
    END;

    // CFR le 01/01/2022 - R‚gie : Tri par d‚faut de la page [SourceTableView]
    // CFR le 04/10/2023 - R‚gie : Fonction de suppression des affectations inutiles
    // CFR le 04/10/2023 - R‚gie : [Couleur] sur client 19 [EXPORT]
    // CFR le 04/10/2023 - R‚gie : modification du filtre pour gain de performance (… confirmer)
}

