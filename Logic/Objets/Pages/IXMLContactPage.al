page 50270 IXMLContactPage
{
    ApplicationArea = all;
    UsageCategory = Lists;
    DelayedInsert = false;
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = Document;
    SourceTable = Contact;
    SourceTableView = WHERE(Type = CONST(Company),
                            Supprimé = CONST(false));

    layout
    {
        area(content)
        {
            group("Général")
            {
                Editable = false;
                field(IXML; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(PICTURE_24; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(NAME_Contacts; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(SNAME_Contact; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(UID_x33; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(LINES_10; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(VISIBLE_NO; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(DURATION_NO; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(SECTIONS_Informations; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                group(Informations)
                {
                    field(SInc1_Nom; Rec.Name)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Name field.';
                    }
                    field(KFSc2_No; Rec."No.")
                    {
                        Caption = 'No.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the No. field.';
                    }
                    field(TUIn_Telephone; Rec."Phone No.")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Phone No. field.';
                    }
                    field(EUIn_eMail; Rec."E-Mail")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Email field.';
                    }
                    field("_Code centrale"; recCentraleActive.Name)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Name field.';
                    }
                    field(Ofp56_Client; _mvarStr_CustomerNo)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the _mvarStr_CustomerNo field.';
                    }
                }
                field("SECTIONS_Coordonnées"; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                group("Coordonnées")
                {
                    field(s1UIn_Adresse; Rec.Address)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Address field.';
                    }
                    field("s1UIn_Code Postal"; Rec."Post Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Post Code field.';
                    }
                    field(s1c3UIn_Ville; Rec.City)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the City field.';
                    }
                    field(s1U_Code_Pays; Rec."Country/Region Code")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Country/Region Code field.';
                    }
                }
                field(SECTIONS_Liens; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                group(Liens)
                {
                    field(s2Okp22_Interactions; '')
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the '''' field.';
                    }
                    field(s2Okp37_Personnes; '')
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the '''' field.';
                    }
                    field(s2VOkp34_Actions; '')
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the '''' field.';
                    }
                    field(s2M_Map; NoMap)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the NoMap field.';
                    }
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord();
    var
        rec_ContactBusinessRelations: Record "Contact Business Relation";
        rec_Customer: Record Customer;
    begin
        rec_ContactBusinessRelations.RESET();
        rec_ContactBusinessRelations.SETRANGE("Contact No.", Rec."No.");
        IF rec_ContactBusinessRelations.FINDFIRST() THEN
            _mvarStr_CustomerNo := rec_ContactBusinessRelations."No.";
        // CFR le 10/03/2021 - SFD20210201 historique centrale active
        IF rec_Customer.GET(_mvarStr_CustomerNo) THEN BEGIN
            rec_Customer.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
            rec_Customer.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
            rec_Customer.CALCFIELDS("Centrale Active");
        END;

        recCentraleActive.RESET();
        IF rec_Customer."Centrale Active" <> '' THEN
            IF recCentraleActive.GET(rec_Customer."Centrale Active") THEN;
        /*{
recCentraleActive.RESET();
IF Rec."Centrale Active" <> '' THEN
IF recCentraleActive.GET(Rec."Centrale Active") THEN;
}*/
        NoMap := Rec."No.";
    end;

    trigger OnOpenPage();
    var
        rec_SalesPerson: Record "Salesperson/Purchaser";
    begin
        rec_SalesPerson.RESET();
        //todo   rec_SalesPerson.SETFILTER(rec_SalesPerson."Windows Login", '%1', NaviwayFunctions.getLoginFilter());
        IF rec_SalesPerson.FINDFIRST() THEN BEGIN
            IF NOT rec_SalesPerson.AllCustomersAccess THEN
                Rec.SETRANGE("Salesperson Code", rec_SalesPerson.Code)
            ELSE
                //SETFILTER("Salesperson Code", '<>%1', '19');
                Rec.SETFILTER("Salesperson Code", '%1|%2|%3|%4|%5', '01', '02', '04', '05', '06');
        END
        ELSE
            //Vendeur par défaut
            Rec.SETRANGE("Salesperson Code", 'x_eskape_x');
    end;

    var

        _mvarStr_CustomerNo: Code[20];
        recCentraleActive: Record Customer;
        NoMap: Code[20];
}

