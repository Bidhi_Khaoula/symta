page 50050 "Fusion Réception"
{
    CardPageID = "Fusion Réception";
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Generals Parameters";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(NoReceptionPrincipale; NoReceptionPrincipale)
            {
                Caption = 'Réception Principale';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Réception Principale field.';

                trigger OnLookup(var Text: Text): Boolean
                var
                    Reception: Record "Warehouse Receipt Header";
                    FrmReception: Page "Warehouse Receipts";
                begin
                    Reception.RESET();

                    CLEAR(FrmReception);
                    FrmReception.SETTABLEVIEW(Reception);
                    FrmReception.LOOKUPMODE(TRUE);
                    IF FrmReception.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                        FrmReception.GETRECORD(Reception);
                        NoReceptionPrincipale := Reception."No.";
                    END
                end;
            }
            field(CtrlFocus; gFocus)
            {
                ApplicationArea = All;
                //TODOControlAddIn=[Ahead_EXFocus;PublicKeyToken=41567e7a458383d3];
                ShowCaption = false;
            }
            repeater(content1)
            {
                ShowCaption = false;
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        Reception: Record "Warehouse Receipt Header";
                        FrmReception: Page "Warehouse Receipts";
                    begin
                        Reception.RESET();
                        CLEAR(FrmReception);
                        //Pouvoir fusionner une réception avec elle meme.
                        //Reception.SETFILTER("No.", '<>%1', NoReceptionPrincipale);
                        Reception.SETRANGE("N° Fusion Réception", '');
                        FrmReception.SETTABLEVIEW(Reception);
                        FrmReception.LOOKUPMODE(TRUE);
                        IF FrmReception.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmReception.GETRECORD(Reception);
                            Rec.Code := Reception."No.";
                        END
                    end;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Fusionner)
            {
                Caption = 'Fusionner';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Fusionner action.';

                trigger OnAction()
                var
                    Reception: Record "Warehouse Receipt Header";
                begin
                    IF NoReceptionPrincipale = '' THEN
                        ERROR(ESK001Err);

                    Reception.GET(NoReceptionPrincipale);
                    Reception.VALIDATE("N° Fusion Réception", NoReceptionPrincipale);
                    Reception.MODIFY(TRUE);

                    Rec.RESET();
                    IF Rec.FINDFIRST() THEN
                        REPEAT
                            Reception.GET(Rec.Code);
                            Reception.VALIDATE("N° Fusion Réception", NoReceptionPrincipale);
                            Reception.MODIFY(TRUE);
                        UNTIL Rec.NEXT() = 0
                    ELSE
                        ERROR(ESK002Err);
                    // CFR le 17/01/2024 - R‚gie : demande de Nathalie >> Ne pas activer la cr‚ation de la demande de transport jusqu'… nouvel ordre
                    /*  {
                      // GRI le 31/03/2023 : demande de transport

                      // on v‚rifie si pas d‚j… pr‚sent sur une demande transporteur
                      DemandeReceipt.SETRANGE(Type, DemandeReceipt.Type::Receipt);
         DemandeReceipt.SETRANGE("No.", NoReceptionPrincipale);
         IF DemandeReceipt.FINDFIRST () THEN lDemandeNo := DemandeReceipt."Demande No.";

         Rec.RESET();
         IF Rec.FINDFIRST () THEN BEGIN
             REPEAT
                 DemandeReceipt.SETRANGE(Type, DemandeReceipt.Type::Receipt);
                 DemandeReceipt.SETRANGE("No.", Rec.Code);
                 IF DemandeReceipt.FINDFIRST () THEN lDemandeNo := DemandeReceipt."Demande No.";
             UNTIL Rec.NEXT() = 0;
         END;

         IF lDemandeNo <> '' THEN
             MESSAGE('Remarque : une réception déjà sur la demande transporteur %1', lDemandeNo)


         ELSE IF CONFIRM('Voulez-vous créer une demande de transport ?') THEN BEGIN
             // GRI le 31/03/2023
             IF CONFIRM('Voulez-vous créer une demande de transport ?') THEN BEGIN
                 Reception.GET(NoReceptionPrincipale);

                 DemandeTransporteur.INIT();
                 DemandeTransporteur.VALIDATE(Type, DemandeTransporteur.Type::Réception);


                 WhseRcptLine.SETRANGE("No.", Rec.Code);
                 IF WhseRcptLine.FINDFIRST () THEN
                     // on r‚cupŠre les infos de la commande achat
                     IF PurchaseHeader.GET(WhseRcptLine."Source Subtype", WhseRcptLine."Source No.") THEN BEGIN
                         DemandeTransporteur.VALIDATE("Vendor No.", PurchaseHeader."Pay-to Vendor No.");
                         DemandeTransporteur."Ship-to Name" := PurchaseHeader."Pay-to Name";
                         DemandeTransporteur."Ship-to Name 2" := PurchaseHeader."Pay-to Name 2";
                         DemandeTransporteur."Ship-to Address" := PurchaseHeader."Pay-to Address";
                         DemandeTransporteur."Ship-to Address 2" := PurchaseHeader."Pay-to Address 2";
                         DemandeTransporteur."Ship-to City" := PurchaseHeader."Pay-to City";
                         DemandeTransporteur."Ship-to Post Code" := PurchaseHeader."Pay-to Post Code";
                         DemandeTransporteur."Ship-to Country/Region Code" := PurchaseHeader."Pay-to Country/Region Code";
                     END;

                 DemandeTransporteur.INSERT(TRUE);

                 // insertion ligne receptions de la demande
                 CLEAR(DemandeReceipt);
                 DemandeReceipt.VALIDATE(Type, DemandeReceipt.Type::Receipt);
                 DemandeReceipt.VALIDATE("Demande No.", DemandeTransporteur."No.");
                 DemandeReceipt.VALIDATE("No.", NoReceptionPrincipale);
                 IF DemandeReceipt.INSERT(TRUE) THEN;

                 Rec.RESET();
                 IF Rec.FINDFIRST () THEN BEGIN
                     REPEAT
                         DemandeReceipt.VALIDATE(Type, DemandeReceipt.Type::Receipt);
                         DemandeReceipt.VALIDATE("Demande No.", DemandeTransporteur."No.");
                         DemandeReceipt.VALIDATE("No.", Rec.Code);
                         IF DemandeReceipt.INSERT(TRUE) THEN;
                     UNTIL Rec.NEXT() = 0;
                 END;
             END;
// FIN GRI le 31/03/2023 : demande de transport
                      }*/
                    MESSAGE('Fusion terminée');
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        // CFR le 23/03/2023 - R‚gie : focalisation et initialisation … l'ouverture de la page
        gFocus := 'ID1000000000;MD' + FORMAT(TIME, 0, 1);
    end;

    PROCEDURE InitNoReceptionPrincipale(pNoReceptionPrincipale: Code[20]);
    BEGIN
        // CFR le 23/03/2023 - R‚gie : focalisation et initialisation … l'ouverture de la page
        NoReceptionPrincipale := pNoReceptionPrincipale;
    END;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    var
        Reception: Record "Warehouse Receipt Header";
    begin
        IF NoReceptionPrincipale = '' THEN
            ERROR(ESK001Err);

        Reception.GET(Rec.Code);
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Rec.Type := 'FUS_RECEPT';
    end;

    var
        NoReceptionPrincipale: Code[20];
        // LstReception: Record "Warehouse Receipt Header" temporary;
        ESK001Err: Label 'No Réception principale ne doit pas être vide !';
        ESK002Err: Label 'Il faut selectionner au moins une sous fusion.';
        gFocus: Text[50];

    // CFR le 23/03/2023 - R‚gie : focalisation et initialisation … l'ouverture de la page
}

