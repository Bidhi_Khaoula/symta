page 50088 "Item Reclass. Journal MiniLoad"
{

    AutoSplitKey = true;
    Caption = 'Item Reclass. Journal';
    CardPageID = "Item Reclass. Journal MiniLoad";
    DataCaptionFields = "Journal Batch Name";
    DelayedInsert = true;
    PageType = Document;
    SaveValues = false;
    SourceTable = "Item Journal Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(CurrentJnlBatchName; CurrentJnlBatchName)
            {
                Caption = 'Batch Name';
                Lookup = true;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Batch Name field.';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    CurrPage.SAVERECORD();
                    CduGFunctions.LookupNameMiniLoad(CurrentJnlBatchName, Rec);
                    CurrPage.UPDATE(FALSE);
                end;

                trigger OnValidate()
                begin
                    ItemJnlMgt.CheckName(CurrentJnlBatchName, Rec);
                    CurrentJnlBatchNameOnAfterVali();
                end;
            }
            field(GetPickingPoste_; GetPickingPoste())
            {
                Caption = 'Poste de picking :';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Poste de picking : field.';
            }
            repeater(content1)
            {
                ShowCaption = false;
                field("A Valider"; Rec."A Valider")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the A Valider field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';

                    trigger OnValidate()
                    begin
                        ItemJnlMgt.GetItem(rec."Item No.", ItemDescription);
                        rec.ShowShortcutDimCode(ShortcutDimCode);
                        rec.ShowNewShortcutDimCode(NewShortcutDimCode);
                    end;
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field(Description; rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Commentaire; rec.Commentaire)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
                field("Shortcut Dimension 1 Code"; Rec."Shortcut Dimension 1 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 1 Code field.';
                }
                field("New Shortcut Dimension 1 Code"; Rec."New Shortcut Dimension 1 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Shortcut Dimension 1 Code field.';
                }
                field("Shortcut Dimension 2 Code"; Rec."Shortcut Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 2 Code field.';
                }
                field("New Shortcut Dimension 2 Code"; Rec."New Shortcut Dimension 2 Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Shortcut Dimension 2 Code field.';
                }
                field(ShortcutDimCode3; ShortcutDimCode[3])
                {
                    CaptionClass = '1,2,3';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[3] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(3, ShortcutDimCode[3]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(3, ShortcutDimCode[3]);
                    end;
                }
                field(NewShortcutDimCode_3; NewShortcutDimCode[3])
                {
                    CaptionClass = Text000Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[3] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(3, NewShortcutDimCode[3]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(3, NewShortcutDimCode[3]);
                    end;
                }
                field(ShortcutDimCode_4; ShortcutDimCode[4])
                {
                    CaptionClass = '1,2,4';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[4] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(4, ShortcutDimCode[4]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(4, ShortcutDimCode[4]);
                    end;
                }
                field(NewShortcutDimCode_4; NewShortcutDimCode[4])
                {
                    CaptionClass = Text001Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[4] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(4, NewShortcutDimCode[4]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(4, NewShortcutDimCode[4]);
                    end;
                }
                field(ShortcutDimCode_5; ShortcutDimCode[5])
                {
                    CaptionClass = '1,2,5';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[5] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(5, ShortcutDimCode[5]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(5, ShortcutDimCode[5]);
                    end;
                }
                field(NewShortcutDimCode_5; NewShortcutDimCode[5])
                {
                    CaptionClass = Text002Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[5] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(5, NewShortcutDimCode[5]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(5, NewShortcutDimCode[5]);
                    end;
                }
                field(ShortcutDimCode_6; ShortcutDimCode[6])
                {
                    CaptionClass = '1,2,6';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[6] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(6, ShortcutDimCode[6]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(6, ShortcutDimCode[6]);
                    end;
                }
                field(NewShortcutDimCode_6; NewShortcutDimCode[6])
                {
                    CaptionClass = Text003Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[6] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(6, NewShortcutDimCode[6]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(6, NewShortcutDimCode[6]);
                    end;
                }
                field(ShortcutDimCode_7; ShortcutDimCode[7])
                {
                    CaptionClass = '1,2,7';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[7] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(7, ShortcutDimCode[7]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(7, ShortcutDimCode[7]);
                    end;
                }
                field(NewShortcutDimCode_7; NewShortcutDimCode[7])
                {
                    CaptionClass = Text004Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[7] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(7, NewShortcutDimCode[7]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(7, NewShortcutDimCode[7]);
                    end;
                }
                field(ShortcutDimCode_8; ShortcutDimCode[8])
                {
                    CaptionClass = '1,2,8';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ShortcutDimCode[8] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupShortcutDimCode(8, ShortcutDimCode[8]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.ValidateShortcutDimCode(8, ShortcutDimCode[8]);
                    end;
                }
                field(NewShortcutDimCode_8; NewShortcutDimCode[8])
                {
                    CaptionClass = Text005Lbl;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NewShortcutDimCode[8] field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        rec.LookupNewShortcutDimCode(8, NewShortcutDimCode[8]);
                    end;

                    trigger OnValidate()
                    begin
                        rec.TESTFIELD("Entry Type", rec."Entry Type"::Transfer);
                        rec.ValidateNewShortcutDimCode(8, NewShortcutDimCode[8]);
                    end;
                }
                field("Location Code"; rec."Location Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';

                    trigger OnValidate()
                    var
                        WMSManagement: Codeunit "WMS Management";
                    begin
                        WMSManagement.CheckItemJnlLineLocation(Rec, xRec);
                    end;
                }
                field("Bin Code"; rec."Bin Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("New Location Code"; rec."New Location Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Location Code field.';

                    trigger OnValidate()
                    var
                        WMSManagement: Codeunit "WMS Management";
                    begin
                        WMSManagement.CheckItemJnlLineLocation(Rec, xRec);
                    end;
                }
                field("New Bin Code"; rec."New Bin Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the New Bin Code field.';
                }
                field("Salespers./Purch. Code"; rec."Salespers./Purch. Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salespers./Purch. Code field.';
                }
                field("Gen. Bus. Posting Group"; rec."Gen. Bus. Posting Group")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gen. Bus. Posting Group field.';
                }
                field("Gen. Prod. Posting Group"; rec."Gen. Prod. Posting Group")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gen. Prod. Posting Group field.';
                }
                field(Quantity; rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Unit of Measure Code"; rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Unit Amount"; rec."Unit Amount")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Amount field.';
                }
                field(Amount; rec.Amount)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field("Indirect Cost %"; rec."Indirect Cost %")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Indirect Cost % field.';
                }
                field("Unit Cost"; rec."Unit Cost")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost field.';
                }
                field("Applies-to Entry"; rec."Applies-to Entry")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Applies-to Entry field.';
                }
                field("Quantité traitée par WMS"; rec."Quantité traitée par WMS")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité traitée par WMS field.';
                }
                field("Envoyé MINILOAD"; rec."Envoyé MINILOAD")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Envoyé MINILOAD field.';
                }
                field("Date Envoi MINILOAD"; rec."Date Envoi MINILOAD")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Envoi MINILOAD field.';
                }
                field("Heure Envoi MINILOAD"; rec."Heure Envoi MINILOAD")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Envoi MINILOAD field.';
                }
                field(GetBoxTypee_; GetBoxTypee())
                {
                    Caption = 'Type de bac';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de bac field.';
                }
                field(GetPoidsTotal_; GetPoidsTotal())
                {
                    Caption = 'Poids total';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids total field.';
                }
                field("Reason Code"; rec."Reason Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reason Code field.';
                }
            }
            group(group)
            {
                ShowCaption = false;
                field(" "; '')
                {
                    CaptionClass = Text19009191Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(ItemDescription; ItemDescription)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ItemDescription field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Line")
            {
                Caption = '&Line';
                Image = Line;
                action(Dimensions)
                {
                    AccessByPermission = TableData 348 = R;
                    Caption = 'Dimensions';
                    Image = Dimensions;
                    ShortCutKey = 'Shift+Ctrl+D';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Dimensions action.';

                    trigger OnAction()
                    begin
                        rec.ShowReclasDimensions();
                        CurrPage.SAVERECORD();
                    end;
                }
                action("Item &Tracking Lines")
                {
                    Caption = 'Item &Tracking Lines';
                    Image = ItemTrackingLines;
                    ShortCutKey = 'Shift+Ctrl+I';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Item &Tracking Lines action.';

                    trigger OnAction()
                    begin
                        rec.OpenItemTrackingLines(TRUE);
                    end;
                }
                action("Bin Contents")
                {
                    Caption = 'Bin Contents';
                    Image = BinContent;
                    RunObject = Page "Bin Contents List";
                    RunPageLink = "Location Code" = FIELD("Location Code"),
                                  "Item No." = FIELD("Item No."),
                                  "Variant Code" = FIELD("Variant Code");
                    RunPageView = SORTING("Location Code", "Item No.", "Variant Code");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Bin Contents action.';
                }
                action("Ouvrir multiconsultation")
                {
                    Image = CalculateConsumption;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'Ctrl+M';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ouvrir multiconsultation action.';

                    trigger OnAction()
                    begin
                        rec.OuvrirMultiConsultation();
                    end;
                }
            }
            group("&Item")
            {
                Caption = '&Item';
                Image = Item;
                action(Card)
                {
                    Caption = 'Card';
                    Image = EditLines;
                    RunObject = Page "Item card";
                    RunPageLink = "No." = FIELD("Item No.");
                    ShortCutKey = 'Shift+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Card action.';
                }
                action("Ledger E&ntries")
                {
                    Caption = 'Ledger E&ntries';
                    Image = ItemLedger;
                    Promoted = false;
                    //The property 'PromotedCategory' can only be set if the property 'Promoted' is set to 'true'
                    //PromotedCategory = Process;
                    RunObject = Page "Item Ledger Entries";
                    RunPageLink = "Item No." = FIELD("Item No.");
                    RunPageView = SORTING("Item No.");
                    ShortCutKey = 'Ctrl+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ledger E&ntries action.';
                }
                group("Item Availability by")
                {
                    Caption = 'Item Availability by';
                    Image = ItemAvailability;
                    action("Event")
                    {
                        Caption = 'Event';
                        Image = "Event";
                        ApplicationArea = All;
                        ToolTip = 'Executes the Event action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec, ItemAvailFormsMgt.ByEvent())
                        end;
                    }
                    action(Period)
                    {
                        Caption = 'Period';
                        Image = Period;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Period action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec, ItemAvailFormsMgt.ByPeriod())
                        end;
                    }
                    action(Variant)
                    {
                        Caption = 'Variant';
                        Image = ItemVariant;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Variant action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec, ItemAvailFormsMgt.ByVariant())
                        end;
                    }
                    action(Location)
                    {
                        AccessByPermission = TableData 14 = R;
                        Caption = 'Location';
                        Image = Warehouse;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Location action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec, ItemAvailFormsMgt.ByLocation())
                        end;
                    }
                    action("BOM Level")
                    {
                        Caption = 'BOM Level';
                        Image = BOMLevel;
                        ApplicationArea = All;
                        ToolTip = 'Executes the BOM Level action.';

                        trigger OnAction()
                        begin
                            ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec, ItemAvailFormsMgt.ByBOM())
                        end;
                    }
                }
            }
        }
        area(processing)
        {
            group("F&unctions")
            {
                Caption = 'F&unctions';
                Image = "Action";
                action("E&xplode BOM")
                {
                    Caption = 'E&xplode BOM';
                    Image = ExplodeBOM;
                    RunObject = Codeunit "Item Jnl.-Explode BOM";
                    ApplicationArea = All;
                    ToolTip = 'Executes the E&xplode BOM action.';
                }
                separator("  ")
                {
                }
                action("Get Bin Content")
                {
                    AccessByPermission = TableData 7302 = R;
                    Caption = 'Get Bin Content';
                    Ellipsis = true;
                    Image = GetBinContent;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Get Bin Content action.';

                    trigger OnAction()
                    var
                        BinContent: Record "Bin Content";
                        GetBinContent: Report "Whse. Get Bin Content";
                    begin
                        BinContent.SETRANGE("Location Code", rec."Location Code");
                        GetBinContent.SETTABLEVIEW(BinContent);
                        GetBinContent.InitializeItemJournalLine(Rec);
                        GetBinContent.RUNMODAL();
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                separator("   ")
                {
                }
                action("Import Fusion")
                {
                    Caption = 'Import Fusion';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Import Fusion action.';

                    trigger OnAction()
                    var
                        CUFusion: Codeunit "Generation des pointages";
                    begin
                        CUFusion.CreationFeuilleReclassement(CurrentJnlBatchName);
                    end;
                }
                separator("    ")
                {
                }
                action("Envoyer vers MiniLoad")
                {
                    Caption = 'Envoyer vers MiniLoad';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Envoyer vers MiniLoad action.';

                    trigger OnAction()
                    var
                        rec_InventorySetup: Record "Inventory Setup";
                        LItemJnlLine: Record "Item Journal Line";
                        ItemJnlBatch: Record "Item Journal Batch";
                        cu_MiniLoad: Codeunit "Gestion MINILOAD";


                    begin
                        // Si déjà envoyé alors erreur
                        LItemJnlLine.SETRANGE("Journal Template Name", rec."Journal Template Name");
                        LItemJnlLine.SETRANGE("Journal Batch Name", rec."Journal Batch Name");
                        LItemJnlLine.SETRANGE("Envoyé MINILOAD", TRUE);
                        IF NOT LItemJnlLine.ISEMPTY THEN
                            ERROR(Error001Err);

                        // Mise à jour champ [Line No. For MiniLoad]
                        ItemJnlBatch.GET(rec."Journal Template Name", rec."Journal Batch Name");
                        ItemJnlBatch.SetMiniLoadLineNo();

                        // Récupération du code emplacement Miniload
                        rec_InventorySetup.GET();

                        // Envoi des messages d'entrées dans le miniload
                        LItemJnlLine.RESET();
                        LItemJnlLine.SETRANGE("Journal Template Name", rec."Journal Template Name");
                        LItemJnlLine.SETRANGE("Journal Batch Name", rec."Journal Batch Name");
                        LItemJnlLine.SETRANGE("New Bin Code", rec_InventorySetup."Code emplacement MiniLoad");
                        IF NOT LItemJnlLine.ISEMPTY THEN
                            cu_MiniLoad.EntréesSortie_MsgREP(LItemJnlLine);


                        // Envoi des messages de sortie du MiniLoad
                        LItemJnlLine.RESET();
                        LItemJnlLine.SETRANGE("Journal Template Name", rec."Journal Template Name");
                        LItemJnlLine.SETRANGE("Journal Batch Name", rec."Journal Batch Name");
                        LItemJnlLine.SETRANGE("Bin Code", rec_InventorySetup."Code emplacement MiniLoad");
                        IF NOT LItemJnlLine.ISEMPTY THEN
                            cu_MiniLoad.EntréesSortie_MsgSOR(LItemJnlLine);
                    end;
                }
                action("Recevoir depuis MiniLoad")
                {
                    Caption = 'Recevoir depuis MiniLoad';
                    ApplicationArea = All;
                    Image = Receivables;
                    ToolTip = 'Executes the Recevoir depuis MiniLoad action.';

                    trigger OnAction()
                    var
                        miniload: Codeunit "Gestion MINILOAD";
                    begin
                        miniload.GetMsgCEP();
                        miniload.GetMsgCSO();
                        miniload.GetMsgEIM();
                    end;
                }
            }
            group("P&osting")
            {
                Caption = 'P&osting';
                action("Test Report")
                {
                    Caption = 'Test Report';
                    Ellipsis = true;
                    Image = TestReport;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Test Report action.';

                    trigger OnAction()
                    begin
                        ReportPrint.PrintItemJnlLine(Rec);
                    end;
                }
                action("P&ost")
                {
                    Caption = 'P&ost';
                    Image = Post;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the P&ost action.';

                    trigger OnAction()
                    begin
                        // CFR 01/09/2020 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                        rec.SETRANGE("A Valider", TRUE);
                        // FIN CFR 01/09/2020

                        CODEUNIT.RUN(CODEUNIT::"Item Jnl.-Post", Rec);
                        CurrentJnlBatchName := rec.GETRANGEMAX("Journal Batch Name");
                        CurrPage.UPDATE(FALSE);

                        // CFR 01/09/2020 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                        rec.SETRANGE("A Valider");
                        // FIN CFR 01/09/2020
                    end;
                }
                action("Post and &Print")
                {
                    Caption = 'Post and &Print';
                    Image = PostPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'Shift+F11';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Post and &Print action.';

                    trigger OnAction()
                    begin
                        // CFR 01/09/2020 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                        rec.SETRANGE("A Valider", TRUE);
                        // FIN CFR 01/09/2020

                        CODEUNIT.RUN(CODEUNIT::"Item Jnl.-Post+Print", Rec);
                        CurrentJnlBatchName := rec.GETRANGEMAX("Journal Batch Name");
                        CurrPage.UPDATE(FALSE);

                        // CFR 01/09/2020 => Lors de la validation de la feuille, ne filtre que sur les champs … [A Valider] … vrai
                        rec.SETRANGE("A Valider");
                        // FIN CFR 01/09/2020
                    end;
                }
            }
            action("&Print")
            {
                Caption = '&Print';
                Ellipsis = true;
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Print action.';

                trigger OnAction()
                var
                    ItemJnlLine: Record "Item Journal Line";
                begin
                    ItemJnlLine.COPY(Rec);
                    ItemJnlLine.SETRANGE("Journal Template Name", rec."Journal Template Name");
                    ItemJnlLine.SETRANGE("Journal Batch Name", rec."Journal Batch Name");
                    REPORT.RUNMODAL(REPORT::"Inventory Movement", TRUE, TRUE, ItemJnlLine);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        rec.ShowShortcutDimCode(ShortcutDimCode);
        rec.ShowNewShortcutDimCode(NewShortcutDimCode);
        OnAfter_GetCurrRecord();
        ItemNoOnFormat(FORMAT(rec."Item No."));
    end;

    trigger OnDeleteRecord(): Boolean
    var
        ReserveItemJnlLine: Codeunit "Item Jnl. Line-Reserve";
    begin
        COMMIT();
        IF NOT ReserveItemJnlLine.DeleteLineConfirm(Rec) THEN
            EXIT(FALSE);
        ReserveItemJnlLine.DeleteLine(Rec);
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        rec.SetUpNewLine(xRec);
        CLEAR(ShortcutDimCode);
        CLEAR(NewShortcutDimCode);
        rec."Entry Type" := rec."Entry Type"::Transfer;
        rec."Document No." := CurrentJnlBatchName;
        OnAfter_GetCurrRecord();
    end;

    trigger OnOpenPage()
    var
        JnlSelected: Boolean;
    begin
        OpenedFromBatch := (rec."Journal Batch Name" <> '') AND (rec."Journal Template Name" = '');
        IF OpenedFromBatch THEN BEGIN
            CurrentJnlBatchName := rec."Journal Batch Name";
            ItemJnlMgt.OpenJnl(CurrentJnlBatchName, Rec);

            EXIT;
        END;
        ItemJnlMgt.TemplateSelection(PAGE::"Item Reclass. Journal", 1, FALSE, Rec, JnlSelected);
        IF NOT JnlSelected THEN
            ERROR('');
        CduGFunctions.OpenJnlMiniLoad(CurrentJnlBatchName, Rec);
    end;

    var
        rec_Item: Record Item;
        ItemJnlMgt: Codeunit ItemJnlManagement;
        ReportPrint: Codeunit "Test Report-Print";
        ItemAvailFormsMgt: Codeunit "Item Availability Forms Mgt";
        CduGFunctions: Codeunit "Codeunits Functions";
        CUMultiRef: Codeunit "Gestion Multi-référence";

        Text000Lbl: Label '1,2,3,New ';
        Text001Lbl: Label '1,2,4,New ';
        Text002Lbl: Label '1,2,5,New ';
        Text003Lbl: Label '1,2,6,New ';
        Text004Lbl: Label '1,2,7,New ';
        Text005Lbl: Label '1,2,8,New ';
        Error001Err: Label 'Cette feuille à déjà servie à l''envoi d''information au miniload. Veuillez utiliser une nouvelle feuille.';

        //_mvarRecJnlBatch: Record "Item Journal Batch";
        Text19009191Lbl: Label 'Item Description';
        CurrentJnlBatchName: Code[10];
        ItemDescription: Text[50];
        ShortcutDimCode: array[8] of Code[20];
        NewShortcutDimCode: array[8] of Code[20];
        OpenedFromBatch: Boolean;





    procedure GetPoidsTotal(): Decimal
    begin
        IF rec_Item.GET(rec."Item No.") THEN
            EXIT(rec.Quantity * rec_Item."Net Weight");
    end;

    procedure GetBoxTypee(): Code[20]
    begin
        IF rec_Item.GET(rec."Item No.") THEN
            EXIT(FORMAT(rec_Item."Box Type"));
    end;

    procedure GetPickingPoste(): Code[20]
    var
        rec_JnlBatch: Record "Item Journal Batch";
    begin
        IF rec_JnlBatch.GET(rec."Journal Template Name", CurrentJnlBatchName) THEN
            EXIT(FORMAT(rec_JnlBatch."Station MiniLoad"));
        EXIT('');
    end;

    local procedure CurrentJnlBatchNameOnAfterVali()
    begin
        CurrPage.SAVERECORD();
        ItemJnlMgt.SetName(CurrentJnlBatchName, Rec);
        CurrPage.UPDATE(FALSE);
    end;

    local procedure OnAfter_GetCurrRecord()
    begin
        xRec := Rec;
        ItemJnlMgt.GetItem(rec."Item No.", ItemDescription);
    end;

    local procedure Control69OnDeactivate()
    var
        CUPointage: Codeunit "Generation des pointages";
    begin
        CUPointage.CreationFeuilleReclassement(CurrentJnlBatchName);
    end;

    local procedure ItemNoOnAfterInput(var Text: Text[1024])
    var
        c: Code[20];
    begin
        // AD Le 21-04-2010 => SYMTA -> Gestion Multireference
        EVALUATE(c, Text);
        //----------------------------------------------------------------------------------------------------------------------------------
        //WF le 29/03/2011 => Ajouter des articles en multi référence
        //Text := CUMultiRef.RechercheArticleByActive(c);
        Text := CUMultiRef.RechercheMultiReference(c);
        //Fin WF le 29/03/2011
        //----------------------------------------------------------------------------------------------------------------------------------


        CduGFunctions.OnAfterInputItemNo(Text);
    end;

    local procedure ItemNoOnFormat(Text: Text[1024])
    begin
        // MC Le 10-11-2011 => SYMTA -> Gestion MultiReference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;
}

