page 50255 "MAJ prix vente kit"
{
    PageType = List;
    SourceTable = Item;
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Selection; Rec.Selection)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Selection field.';
                }
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("No. 2"; Rec."No. 2")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    StyleExpr = StyleTxt;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Inventory; Rec.Inventory)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Inventory field.';
                }
                field("Unit Cost"; Rec."Unit Cost")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost field.';
                }
                field("Prix vente actuel"; Rec."Prix vente actuel")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix vente actuel field.';
                }
                field("Prix calculé"; Rec."Prix calculé")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix calculé field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action("tous désélectionner ")
            {
                Image = SelectEntries;
                Promoted = true;
                ApplicationArea = All;
                ToolTip = 'Executes the tous désélectionner  action.';

                trigger OnAction()
                begin

                    Rec.MODIFYALL(Selection, FALSE);
                    CurrPage.UPDATE(FALSE);
                end;
            }
            action("tous sélectionner")
            {
                Image = SelectEntries;
                Promoted = true;
                ApplicationArea = All;
                ToolTip = 'Executes the tous sélectionner action.';

                trigger OnAction()
                begin
                    IF Rec.FINDSET() THEN
                        REPEAT
                            IF (Rec."Prix calculé" <> Rec."Prix vente actuel") AND (Rec."Prix calculé" <> 0) THEN BEGIN
                                Rec.Selection := TRUE;
                                Rec.MODIFY();
                            END;
                        UNTIL Rec.NEXT() = 0;

                    CurrPage.UPDATE(FALSE);
                end;
            }
            action("Multi-Consultation")
            {
                Image = CalculateConsumption;
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                begin
                    Rec.OuvrirMultiConsultation();
                end;
            }
            action("Mise a jours TARIF")
            {
                Image = "Action";
                Promoted = true;
                ApplicationArea = All;
                ToolTip = 'Executes the Mise a jours TARIF action.';

                trigger OnAction()
                begin
                    IF CONFIRM('Voulez vous mettre a jours les tarifs ventes pour les articles séléctionnés ?') THEN
                        MajTARIF();
                end;
            }
            group(Availability)
            {
                Caption = 'Availability';
                Image = Item;
                action("Items b&y Location")
                {
                    AccessByPermission = TableData 14 = R;
                    Caption = 'Items b&y Location';
                    Image = ItemAvailbyLoc;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Items b&y Location action.';

                    trigger OnAction()
                    var
                        ItemsByLocation: Page "Items by Location";
                    begin
                        ItemsByLocation.SETRECORD(Rec);
                        ItemsByLocation.RUN();
                    end;
                }
            }
            group("Master Data")
            {
                Caption = 'Master Data';
                Image = DataEntry;
                action("&Units of Measure")
                {
                    Caption = '&Units of Measure';
                    Image = UnitOfMeasure;
                    RunObject = Page "Item Units of Measure";
                    RunPageLink = "Item No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Units of Measure action.';
                }
                action("Va&riants")
                {
                    Caption = 'Va&riants';
                    Image = ItemVariant;
                    RunObject = Page "Item Variants";
                    RunPageLink = "Item No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Va&riants action.';
                }
                group(Dimensions)
                {
                    Caption = 'Dimensions';
                    Image = Dimensions;
                    action("Dimensions-Single")
                    {
                        Caption = 'Dimensions-Single';
                        Image = Dimensions;
                        RunObject = Page "Default Dimensions";
                        RunPageLink = "Table ID" = CONST(27),
                                      "No." = FIELD("No.");
                        ShortCutKey = 'Shift+Ctrl+D';
                        ApplicationArea = All;
                        ToolTip = 'Executes the Dimensions-Single action.';
                    }
                    action("Dimensions-&Multiple")
                    {
                        AccessByPermission = TableData Dimension = R;
                        Caption = 'Dimensions-&Multiple';
                        Image = DimensionSets;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Dimensions-&Multiple action.';

                        trigger OnAction()
                        var
                            Item: Record Item;
                            DefaultDimMultiple: Page "Default Dimensions-Multiple";
                        begin
                            CurrPage.SETSELECTIONFILTER(Item);
                            DefaultDimMultiple.SetMultiRecord(Item, Rec.FieldNo("No."));
                            DefaultDimMultiple.RUNMODAL();
                        end;
                    }
                }
                action("Substituti&ons")
                {
                    Caption = 'Substituti&ons';
                    Image = ItemSubstitution;
                    RunObject = Page "Item Substitution Entry";
                    RunPageLink = Type = CONST(Item),
                                  "No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Substituti&ons action.';
                }
                action("Cross Re&ferences")
                {
                    Caption = 'Cross Re&ferences';
                    Image = Change;
                    RunObject = Page "Item Reference Entries";
                    RunPageLink = "Item No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Cross Re&ferences action.';
                }
                action("E&xtended Texts")
                {
                    Caption = 'E&xtended Texts';
                    Image = Text;
                    RunObject = Page "Extended Text List";
                    RunPageLink = "Table Name" = CONST(Item),
                                  "No." = FIELD("No.");
                    RunPageView = SORTING("Table Name", "No.", "Language Code", "All Language Codes", "Starting Date", "Ending Date");
                    ApplicationArea = All;
                    ToolTip = 'Executes the E&xtended Texts action.';
                }
                action(Translations)
                {
                    Caption = 'Translations';
                    Image = Translations;
                    RunObject = Page "Item Translations";
                    RunPageLink = "Item No." = FIELD("No."),
                                  "Variant Code" = CONST();
                    ApplicationArea = All;
                    ToolTip = 'Executes the Translations action.';
                }
                action("&Picture")
                {
                    Caption = '&Picture';
                    Image = Picture;
                    RunObject = Page "Item Picture";
                    RunPageLink = "No." = FIELD("No."),
                                  "Date Filter" = FIELD("Date Filter"),
                                  "Global Dimension 1 Filter" = FIELD("Global Dimension 1 Filter"),
                                  "Global Dimension 2 Filter" = FIELD("Global Dimension 2 Filter"),
                                  "Location Filter" = FIELD("Location Filter"),
                                  "Drop Shipment Filter" = FIELD("Drop Shipment Filter"),
                                  "Variant Filter" = FIELD("Variant Filter");
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Picture action.';
                }
                action(Identifiers)
                {
                    Caption = 'Identifiers';
                    Image = BarCode;
                    RunObject = Page "Item Identifiers";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.", "Variant Code", "Unit of Measure Code");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Identifiers action.';
                }
            }
            group("Assembly/Production")
            {
                Caption = 'Assembly/Production';
                Image = Production;
                action(Structure)
                {
                    Caption = 'Structure';
                    Image = Hierarchy;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Structure action.';

                    trigger OnAction()
                    var
                        BOMStructure: Page "BOM Structure";
                    begin
                        BOMStructure.InitItem(Rec);
                        BOMStructure.RUN();
                    end;
                }
                action("Cost Shares")
                {
                    Caption = 'Cost Shares';
                    Image = CostBudget;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Cost Shares action.';

                    trigger OnAction()
                    var
                        BOMCostShares: Page "BOM Cost Shares";
                    begin
                        BOMCostShares.InitItem(Rec);
                        BOMCostShares.RUN();
                    end;
                }
                group("Assemb&ly")
                {
                    Caption = 'Assemb&ly';
                    Image = AssemblyBOM;
                    action("<Action32>")
                    {
                        Caption = 'Assembly BOM';
                        Image = BOM;
                        RunObject = Page "Assembly BOM";
                        RunPageLink = "Parent Item No." = FIELD("No.");
                        RunPageMode = View;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Assembly BOM action.';
                    }
                    action("Where-Used")
                    {
                        Caption = 'Where-Used';
                        Image = Track;
                        RunObject = Page "Where-Used List";
                        RunPageLink = Type = CONST(Item),
                                      "No." = FIELD("No.");
                        RunPageView = SORTING(Type, "No.");
                        ApplicationArea = All;
                        ToolTip = 'Executes the Where-Used action.';
                    }
                }
            }
            group(History)
            {
                Caption = 'History';
                Image = History;
                group("E&ntries")
                {
                    Caption = 'E&ntries';
                    Image = Entries;
                    action("Ledger E&ntries")
                    {
                        Caption = 'Ledger E&ntries';
                        Image = ItemLedger;
                        Promoted = false;
                        //The property 'PromotedCategory' can only be set if the property 'Promoted' is set to 'true'
                        //PromotedCategory = Process;
                        RunObject = Page "Item Ledger Entries";
                        RunPageLink = "Item No." = FIELD("No.");
                        RunPageView = SORTING("Item No.");
                        ShortCutKey = 'Ctrl+F7';
                        ApplicationArea = All;
                        ToolTip = 'Executes the Ledger E&ntries action.';
                    }
                    action("&Reservation Entries")
                    {
                        Caption = '&Reservation Entries';
                        Image = ReservationLedger;
                        RunObject = Page "Reservation Entries";
                        RunPageLink = "Reservation Status" = CONST(Reservation),
                                      "Item No." = FIELD("No.");
                        RunPageView = SORTING("Item No.", "Variant Code", "Location Code", "Reservation Status");
                        ApplicationArea = All;
                        ToolTip = 'Executes the &Reservation Entries action.';
                    }
                    action("&Phys. Inventory Ledger Entries")
                    {
                        Caption = '&Phys. Inventory Ledger Entries';
                        Image = PhysicalInventoryLedger;
                        RunObject = Page "Phys. Inventory Ledger Entries";
                        RunPageLink = "Item No." = FIELD("No.");
                        RunPageView = SORTING("Item No.");
                        ApplicationArea = All;
                        ToolTip = 'Executes the &Phys. Inventory Ledger Entries action.';
                    }
                    action("&Value Entries")
                    {
                        Caption = '&Value Entries';
                        Image = ValueLedger;
                        RunObject = Page "Value Entries";
                        RunPageLink = "Item No." = FIELD("No.");
                        RunPageView = SORTING("Item No.");
                        ApplicationArea = All;
                        ToolTip = 'Executes the &Value Entries action.';
                    }
                    action("&Warehouse Entries")
                    {
                        Caption = '&Warehouse Entries';
                        Image = BinLedger;
                        RunObject = Page "Warehouse Entries";
                        RunPageLink = "Item No." = FIELD("No.");
                        RunPageView = SORTING("Item No.", "Bin Code", "Location Code", "Variant Code", "Unit of Measure Code", "Lot No.", "Serial No.", "Entry Type", Dedicated);
                        ApplicationArea = All;
                        ToolTip = 'Executes the &Warehouse Entries action.';
                    }
                }
                group(Statistics)
                {
                    Caption = 'Statistics';
                    Image = Statistics;
                    action(Statistics2)
                    {
                        Caption = 'Statistics';
                        Image = Statistics;
                        Promoted = true;
                        PromotedCategory = Process;
                        ShortCutKey = 'F7';
                        ApplicationArea = All;
                        ToolTip = 'Executes the Statistics action.';

                        trigger OnAction()
                        var
                            ItemStatistics: Page "Item Statistics";
                        begin
                            ItemStatistics.SetItem(Rec);
                            ItemStatistics.RUNMODAL();
                        end;
                    }
                    action("Entry Statistics")
                    {
                        Caption = 'Entry Statistics';
                        Image = EntryStatistics;
                        RunObject = Page "Item Entry Statistics";
                        RunPageLink = "No." = FIELD("No."),
                                      "Date Filter" = FIELD("Date Filter"),
                                      "Global Dimension 1 Filter" = FIELD("Global Dimension 1 Filter"),
                                      "Global Dimension 2 Filter" = FIELD("Global Dimension 2 Filter"),
                                      "Location Filter" = FIELD("Location Filter"),
                                      "Drop Shipment Filter" = FIELD("Drop Shipment Filter"),
                                      "Variant Filter" = FIELD("Variant Filter");
                        ApplicationArea = All;
                        ToolTip = 'Executes the Entry Statistics action.';
                    }
                    action("T&urnover")
                    {
                        Caption = 'T&urnover';
                        Image = Turnover;
                        RunObject = Page "Item Turnover";
                        RunPageLink = "No." = FIELD("No."),
                                      "Global Dimension 1 Filter" = FIELD("Global Dimension 1 Filter"),
                                      "Global Dimension 2 Filter" = FIELD("Global Dimension 2 Filter"),
                                      "Location Filter" = FIELD("Location Filter"),
                                      "Drop Shipment Filter" = FIELD("Drop Shipment Filter"),
                                      "Variant Filter" = FIELD("Variant Filter");
                        ApplicationArea = All;
                        ToolTip = 'Executes the T&urnover action.';
                    }
                }
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Comment Sheet";
                    RunPageLink = "Table Name" = CONST(Item),
                                  "No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';
                }
            }
            group("S&ales")
            {
                Caption = 'S&ales';
                Image = Sales;
                action(Prices)
                {
                    Caption = 'Prices';
                    Image = Price;
                    RunObject = Page "Sales Prices";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Prices action.';
                }
                action("Line Discounts")
                {
                    Caption = 'Line Discounts';
                    Image = LineDiscount;
                    RunObject = Page "Sales Line Discounts";
                    RunPageLink = Type = CONST(Item),
                                  Code = FIELD("No.");
                    RunPageView = SORTING(Type, Code);
                    ApplicationArea = All;
                    ToolTip = 'Executes the Line Discounts action.';
                }
                action("Prepa&yment Percentages")
                {
                    Caption = 'Prepa&yment Percentages';
                    Image = PrepaymentPercentages;
                    RunObject = Page "Sales Prepayment Percentages";
                    RunPageLink = "Item No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Prepa&yment Percentages action.';
                }
                action(Orders2)
                {
                    Caption = 'Orders';
                    Image = Document;
                    RunObject = Page "Sales Orders";
                    RunPageLink = Type = CONST(Item),
                                  "No." = FIELD("No.");
                    RunPageView = SORTING("Document Type", Type, "No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Orders action.';
                }
                action("Returns Orders")
                {
                    Caption = 'Returns Orders';
                    Image = ReturnOrder;
                    RunObject = Page "Sales Return Orders";
                    RunPageLink = Type = CONST(Item),
                                  "No." = FIELD("No.");
                    RunPageView = SORTING("Document Type", Type, "No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Returns Orders action.';
                }
            }
            group("&Purchases")
            {
                Caption = '&Purchases';
                Image = Purchasing;
                action("Ven&dors")
                {
                    Caption = 'Ven&dors';
                    Image = Vendor;
                    RunObject = Page "Item Vendor Catalog";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ven&dors action.';
                }
                action(Prices2)
                {
                    Caption = 'Prices';
                    Image = Price;
                    RunObject = Page "Purchase Prices";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Prices action.';
                }
                action("Line Discounts2")
                {
                    Caption = 'Line Discounts';
                    Image = LineDiscount;
                    RunObject = Page "Purchase Line Discounts";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Line Discounts action.';
                }
                action("Prepa&yment Percentages2")
                {
                    Caption = 'Prepa&yment Percentages';
                    Image = PrepaymentPercentages;
                    RunObject = Page "Purchase Prepmt. Percentages";
                    RunPageLink = "Item No." = FIELD("No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Prepa&yment Percentages action.';
                }
                action(Orders)
                {
                    Caption = 'Orders';
                    Image = Document;
                    RunObject = Page "Purchase Orders";
                    RunPageLink = Type = CONST(Item),
                                  "No." = FIELD("No.");
                    RunPageView = SORTING("Document Type", Type, "No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Orders action.';
                }
                action("Return Orders")
                {
                    Caption = 'Return Orders';
                    Image = ReturnOrder;
                    RunObject = Page "Purchase Return Orders";
                    RunPageLink = Type = CONST(Item),
                                  "No." = FIELD("No.");
                    RunPageView = SORTING("Document Type", Type, "No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Return Orders action.';
                }
                action("Nonstoc&k Items")
                {
                    Caption = 'Nonstoc&k Items';
                    Image = NonStockItem;
                    RunObject = Page "Catalog Item List";
                    ApplicationArea = All;
                    ToolTip = 'Executes the Nonstoc&k Items action.';
                }
            }
            group(Warehouse)
            {
                Caption = 'Warehouse';
                Image = Warehouse;
                action("&Bin Contents")
                {
                    Caption = '&Bin Contents';
                    Image = BinContent;
                    RunObject = Page "Bin Contents";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Bin Contents action.';
                }
                action("Stockkeepin&g Units")
                {
                    Caption = 'Stockkeepin&g Units';
                    Image = SKU;
                    RunObject = Page "Stockkeeping Unit List";
                    RunPageLink = "Item No." = FIELD("No.");
                    RunPageView = SORTING("Item No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the Stockkeepin&g Units action.';
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        StyleTxt := '';
        IF rec."Prix vente actuel" <> rec."Prix calculé" THEN
            StyleTxt := 'Attention';
    end;

    var
        MfgSetup: Record "Manufacturing Setup";
        GLSetup: Record "General Ledger Setup";
        TempItem: Record Item temporary;
        // TempWorkCenter: Record "Work Center" temporary;
        // TempMachineCenter: Record "Machine Center" temporary;
        TempProdBOMVersionErrBuf: Record "Production BOM Version" temporary;
        TempRtngVersionErrBuf: Record "Routing Version" temporary;
        CostCalcMgt: Codeunit "Cost Calculation Management";
        VersionMgt: Codeunit VersionManagement;
        UOMMgt: Codeunit "Unit of Measure Management";
        Text000: Label 'Too many levels. Must be below %1.';
        /*   Text001: Label '&Top level,&All levels';
          Text002: Label '@1@@@@@@@@@@@@@'; */
        //  CalcMfgPrompt: Label 'One or more subassemblies on the assembly list for item %1 use replenishment system Prod. Order. Do you want to calculate standard cost for those subassemblies?';
        TargetText: Label 'Standard Cost,Unit Price';
        RecursionInstruction: Label 'Calculate the %3 of item %1 %2 by rolling up the assembly list components. Select All levels to include and update the %3 of any subassemblies.', Comment = '%1 = Item No., %2 = Description';
        //  NonAssemblyItemError: Label 'Item %1 %2 does not use replenishment system Assembly. The %3 will not be calculated.', Comment = '%1 = Item No., %2 = Description';
        NoAssemblyListError: Label 'Item %1 %2 has no assembly list. The %3 will not be calculated.', Comment = '%1 = Item No., %2 = Description';
        NonAssemblyComponentWithList: Label 'One or more subassemblies on the assembly list for this item does not use replenishment system Assembly. The %1 for these subassemblies will not be calculated. Are you sure that you want to continue?';


        // Window: Dialog;
        MaxLevel: Integer;
        CalculationDate: Date;
        CalcMultiLevel: Boolean;
        UseAssemblyList: Boolean;
        LogErrors: Boolean;
        ShowDialog: Boolean;
        StdCostWkshName: Text[50];
        // ColIdx: Option ,StdCost,ExpCost,ActCost,Dev,"Var";
        //  RowIdx: Option ,MatCost,ResCost,ResOvhd,AsmOvhd,Total;
        PrixventeCalculer: Decimal;
        StyleTxt: Text;

    procedure Init(var _Item: Record Item)
    var

    begin
        IF _Item.FINDSET() THEN
            REPEAT
                Rec := _Item;
                Rec."Prix vente actuel" := GetPrixTarif(Rec);
                Rec.Selection := FALSE;
                PrixventeCalculer := 0;
                CalcAssemblyItemPrice(Rec."No.");
                Rec."Prix calculé" := PrixventeCalculer;

                IF (Rec."Prix calculé" <> Rec."Prix vente actuel") AND (Rec."Prix calculé" <> 0) THEN
                    Rec.Selection := TRUE;

                Rec.Insert();
            UNTIL _Item.NEXT() = 0;
    end;

    procedure GetPrixTarif(_item: Record Item): Decimal
    var
        PrixClient: Record "Sales Price";
    begin
        PrixClient.RESET();
        PrixClient.SETRANGE("Item No.", _item."No.");
        PrixClient.SETRANGE("Sales Type", PrixClient."Sales Type"::"All Customers");
        PrixClient.SETRANGE("Starting Date", 0D, WORKDATE());
        PrixClient.SETRANGE("Ending Date", WORKDATE(), 99991231D);

        IF PrixClient.ISEMPTY THEN
            PrixClient.SETRANGE("Ending Date", 0D);

        IF PrixClient.ISEMPTY THEN BEGIN
            PrixClient.SETRANGE("Starting Date", 0D);
            PrixClient.SETRANGE("Ending Date", WORKDATE(), 99991231D);
        END;


        IF PrixClient.FINDFIRST() THEN
            EXIT(PrixClient."Unit Price")
        ELSE
            EXIT(_item."Unit Price");
    end;

    procedure CalcAssemblyItemPrice(ItemNo: Code[20])
    var
        Item: Record Item;
        Instruction: Text[1024];
        Depth: Integer;
        NewCalcMultiLevel: Boolean;
        AssemblyContainsProdBOM: Boolean;
    begin
        Item.GET(ItemNo);
        Instruction := PrepareAssemblyCalculation(Item, Depth, 2, AssemblyContainsProdBOM); // 2=UnitPrice
                                                                                            /*
                                                                                         IF Depth > 1 THEN
                                                                                           CASE STRMENU(Text001,1,Instruction) OF
                                                                                             0:
                                                                                               EXIT;
                                                                                             1:
                                                                                               NewCalcMultiLevel := FALSE;
                                                                                             2:
                                                                                               NewCalcMultiLevel := TRUE;
                                                                                           END;
                                                                                              */

        NewCalcMultiLevel := TRUE;

        SetProperties(WORKDATE(), NewCalcMultiLevel, TRUE, FALSE, '', FALSE);

        Item.GET(ItemNo);
        DoCalcAssemblyItemPrice(Item, 0);

    end;

    procedure PrepareAssemblyCalculation(var Item: Record Item; var Depth: Integer; Target: Option "Standard Cost","Unit Price"; var ContainsProdBOM: Boolean) Instruction: Text[1024]
    var
        CalculationTarget: Text[80];
        SubNonAssemblyItemWithList: Boolean;
    begin
        CalculationTarget := SELECTSTR(Target, TargetText);

        //IF NOT Item.IsAssemblyItem THEN
        //  ERROR(NonAssemblyItemError,Item."No.",Item.Description,CalculationTarget);
        AnalyzeAssemblyList(Item, Depth, SubNonAssemblyItemWithList, ContainsProdBOM);
        IF Depth = 0 THEN
            ERROR(NoAssemblyListError, Item."No.", Item.Description, CalculationTarget);
        Instruction := STRSUBSTNO(RecursionInstruction, Item."No.", Item.Description, CalculationTarget);
        IF SubNonAssemblyItemWithList THEN
            Instruction += STRSUBSTNO(NonAssemblyComponentWithList, CalculationTarget)
    end;

    procedure SetProperties(NewCalculationDate: Date; NewCalcMultiLevel: Boolean; NewUseAssemblyList: Boolean; NewLogErrors: Boolean; NewStdCostWkshName: Text[50]; NewShowDialog: Boolean)
    begin
        TempItem.DELETEALL();
        TempProdBOMVersionErrBuf.DELETEALL();
        TempRtngVersionErrBuf.DELETEALL();
        CLEARALL();

        CalculationDate := NewCalculationDate;
        CalcMultiLevel := NewCalcMultiLevel;
        UseAssemblyList := NewUseAssemblyList;
        LogErrors := NewLogErrors;
        StdCostWkshName := NewStdCostWkshName;
        ShowDialog := NewShowDialog;

        MaxLevel := 50;
        MfgSetup.GET();
        GLSetup.GET();
    end;

    local procedure DoCalcAssemblyItemPrice(var Item: Record Item; Level: Integer)
    var
        BOMComp: Record "BOM Component";
        CompItem: Record Item;
        CompResource: Record Resource;
        UnitPrice: Decimal;
    begin
        IF Level > MaxLevel THEN
            ERROR(Text000, MaxLevel);

        IF NOT CalcMultiLevel AND (Level <> 0) THEN
            EXIT;

        IF NOT Item.IsAssemblyItem() THEN
            EXIT;

        BOMComp.SETRANGE("Parent Item No.", Item."No.");
        IF BOMComp.FIND('-') THEN BEGIN
            REPEAT
                CASE BOMComp.Type OF
                    BOMComp.Type::Item:
                        IF CompItem.GET(BOMComp."No.") THEN BEGIN
                            DoCalcAssemblyItemPrice(CompItem, Level + 1);
                            UnitPrice +=
                              BOMComp."Quantity per" *
                              UOMMgt.GetQtyPerUnitOfMeasure(CompItem, BOMComp."Unit of Measure Code") *
                              //CompItem."Unit Price"
                              GetPrixTarif(CompItem);
                        END;
                    BOMComp.Type::Resource:
                        IF CompResource.GET(BOMComp."No.") THEN
                            UnitPrice +=
                              BOMComp."Quantity per" *
                              UOMMgt.GetResQtyPerUnitOfMeasure(CompResource, BOMComp."Unit of Measure Code") *
                              CompResource."Unit Price";
                END
            UNTIL BOMComp.NEXT() = 0;
            UnitPrice := ROUND(UnitPrice, GLSetup."Unit-Amount Rounding Precision");
            PrixventeCalculer := UnitPrice;
        END;
    end;

    procedure AnalyzeProdBOM(ProductionBOMNo: Code[20]; var Depth: Integer; var NonAssemblyItemWithList: Boolean; var ContainsProdBOM: Boolean)
    var
        ProdBOMLine: Record "Production BOM Line";
        SubItem: Record Item;
        PBOMVersionCode: Code[10];
        BaseDepth: Integer;
        MaxDepth: Integer;
    begin
        SetProdBOMFilters(ProdBOMLine, PBOMVersionCode, ProductionBOMNo);
        IF ProdBOMLine.FINDSET() THEN BEGIN
            Depth += 1;
            BaseDepth := Depth;
            REPEAT
                CASE ProdBOMLine.Type OF
                    ProdBOMLine.Type::Item:
                        BEGIN
                            SubItem.GET(ProdBOMLine."No.");
                            MaxDepth := BaseDepth;
                            AnalyzeAssemblyList(SubItem, MaxDepth, NonAssemblyItemWithList, ContainsProdBOM);
                            IF MaxDepth > Depth THEN
                                Depth := MaxDepth
                        END;
                    ProdBOMLine.Type::"Production BOM":
                        BEGIN
                            MaxDepth := BaseDepth;
                            AnalyzeProdBOM(ProdBOMLine."No.", MaxDepth, NonAssemblyItemWithList, ContainsProdBOM);
                            MaxDepth -= 1;
                            IF MaxDepth > Depth THEN
                                Depth := MaxDepth
                        END;
                END;
            UNTIL ProdBOMLine.NEXT() = 0
        END
    end;

    procedure AnalyzeAssemblyList(var Item: Record Item; var Depth: Integer; var NonAssemblyItemWithList: Boolean; var ContainsProdBOM: Boolean)
    var
        BOMComponent: Record "BOM Component";
        SubItem: Record Item;
        BaseDepth: Integer;
        MaxDepth: Integer;
    begin
        IF Item.IsMfgItem() AND ((Item."Production BOM No." <> '') OR (Item."Routing No." <> '')) THEN BEGIN
            ContainsProdBOM := TRUE;
            IF Item."Production BOM No." <> '' THEN
                AnalyzeProdBOM(Item."Production BOM No.", Depth, NonAssemblyItemWithList, ContainsProdBOM)
            ELSE
                Depth += 1;
            EXIT
        END;
        BOMComponent.SETRANGE("Parent Item No.", Item."No.");
        IF BOMComponent.FINDSET() THEN BEGIN
            IF NOT Item.IsAssemblyItem() THEN BEGIN
                NonAssemblyItemWithList := TRUE;
                EXIT
            END;
            Depth += 1;
            BaseDepth := Depth;
            REPEAT
                IF (BOMComponent.Type = BOMComponent.Type::Item) AND (BOMComponent."No." <> '') THEN BEGIN
                    SubItem.GET(BOMComponent."No.");
                    MaxDepth := BaseDepth;
                    AnalyzeAssemblyList(SubItem, MaxDepth, NonAssemblyItemWithList, ContainsProdBOM);
                    IF MaxDepth > Depth THEN
                        Depth := MaxDepth
                END
            UNTIL BOMComponent.NEXT() = 0
        END;
    end;

    local procedure SetProdBOMFilters(var ProdBOMLine: Record "Production BOM Line"; var PBOMVersionCode: Code[10]; ProdBOMNo: Code[20])
    var
        ProdBOMHeader: Record "Production BOM Header";
    begin
        PBOMVersionCode :=
          VersionMgt.GetBOMVersion(ProdBOMNo, CalculationDate, TRUE);
        IF PBOMVersionCode = '' THEN BEGIN
            ProdBOMHeader.GET(ProdBOMNo);
            TestBOMVersionIsCertified(PBOMVersionCode, ProdBOMHeader);
        END;

        WITH ProdBOMLine DO BEGIN
            SETRANGE("Production BOM No.", ProdBOMNo);
            SETRANGE("Version Code", PBOMVersionCode);
            SETFILTER("Starting Date", '%1|..%2', 0D, CalculationDate);
            SETFILTER("Ending Date", '%1|%2..', 0D, CalculationDate);
            SETFILTER("No.", '<>%1', '')
        END
    end;

    local procedure TestBOMVersionIsCertified(BOMVersionCode: Code[20]; ProdBOMHeader: Record "Production BOM Header"): Boolean
    begin
        IF BOMVersionCode = '' THEN BEGIN
            IF ProdBOMHeader.Status <> ProdBOMHeader.Status::Certified THEN
                IF LogErrors THEN
                    InsertInErrBuf(ProdBOMHeader."No.", '', FALSE)
                ELSE
                    ProdBOMHeader.TESTFIELD(Status, ProdBOMHeader.Status::Certified);
        END;
    end;

    local procedure InsertInErrBuf(No: Code[20]; Version: Code[10]; IsRtng: Boolean)
    begin
        IF NOT LogErrors THEN
            EXIT;

        IF IsRtng THEN BEGIN
            TempRtngVersionErrBuf."Routing No." := No;
            TempRtngVersionErrBuf."Version Code" := Version;
            IF TempRtngVersionErrBuf.INSERT() THEN;
        END ELSE BEGIN
            TempProdBOMVersionErrBuf."Production BOM No." := No;
            TempProdBOMVersionErrBuf."Version Code" := Version;
            IF TempProdBOMVersionErrBuf.INSERT() THEN;
        END;
    end;

    local procedure MajTARIF()
    var
        PrixClient: Record "Sales Price";
    begin
        Rec.SETRANGE(Selection, TRUE);
        IF Rec.FINDSET() THEN
            REPEAT
                //Recherche de la ligne a clôturer
                PrixClient.RESET();
                PrixClient.SETRANGE("Item No.", Rec."No.");
                PrixClient.SETRANGE("Sales Type", PrixClient."Sales Type"::"All Customers");
                PrixClient.SETRANGE("Starting Date", 0D, WORKDATE());
                PrixClient.SETRANGE("Ending Date", WORKDATE(), 99991231D);

                IF PrixClient.ISEMPTY THEN
                    PrixClient.SETRANGE("Ending Date", 0D);

                IF PrixClient.ISEMPTY THEN BEGIN
                    PrixClient.SETRANGE("Starting Date", 0D);
                    PrixClient.SETRANGE("Ending Date", WORKDATE(), 99991231D);
                END;


                IF PrixClient.FINDFIRST() THEN BEGIN
                    PrixClient."Ending Date" := CALCDATE('<-1D>', WORKDATE());
                    PrixClient.MODIFY();

                    PrixClient."Starting Date" := WORKDATE();
                    PrixClient."Ending Date" := 0D;
                    PrixClient."Unit Price" := Rec."Prix calculé";
                    PrixClient.Coefficient := 1;
                    PrixClient."Prix catalogue" := Rec."Prix calculé";
                    IF NOT PrixClient.INSERT() THEN PrixClient.MODIFY();
                END
                ELSE BEGIN
                    PrixClient.RESET();
                    PrixClient.INIT();
                    PrixClient."Item No." := Rec."No.";
                    PrixClient."Sales Type" := PrixClient."Sales Type"::"All Customers";
                    PrixClient."Starting Date" := WORKDATE();
                    PrixClient."Unit Price" := Rec."Prix calculé";
                    PrixClient."Allow Invoice Disc." := TRUE;
                    PrixClient."Allow Line Disc." := TRUE;
                    PrixClient."Unit of Measure Code" := Rec."Sales Unit of Measure";
                    PrixClient.INSERT(TRUE);
                    PrixClient.Coefficient := 1;
                    PrixClient."Prix catalogue" := Rec."Prix calculé";
                    PrixClient.MODIFY();
                END;


            UNTIL Rec.NEXT() = 0;

        COMMIT();
        MESSAGE('Traitement effectué');
        CurrPage.CLOSE();
    end;
}

