page 50316 "WIIO - CommentaireArticle"
{
    ApplicationArea = all;
    SourceTable = "Comment Line";
    SourceTableView = SORTING("Table Name", "No.", "Line No.")
                      ORDER(Ascending)
                      WHERE("Table Name" = CONST(Item));

    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
            }
        }
    }

    actions
    {
    }
}

