page 50329 "Demande Receipt ListPart"
{
    PageType = ListPart;
    ShowFilter = false;
    SourceTable = "Demande Trans. Link";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Demande No."; Rec."Demande No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Demande field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the "N° " field.';

                    trigger OnValidate();
                    var
                        DemandeLink: Record "Demande Trans. Link";
                    begin

                        SetExternalValues();

                        IF (Rec."No." <> '') AND (xRec."No." <> Rec."No.") THEN BEGIN
                            DemandeLink.SETFILTER("Demande No.", '<>%1', Rec."Demande No.");
                            DemandeLink.SETRANGE(Type, Rec.Type);
                            DemandeLink.SETRANGE("No.", Rec."No.");
                            IF DemandeLink.FINDFIRST() THEN ERROR('Le document est déjà sur la demande %1', DemandeLink."Demande No.");
                            CurrPage.UPDATE(TRUE);
                        END;
                    end;
                }
                field(gExternalNo; gExternalNo)
                {
                    Caption = 'N° Doc externe';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Doc externe field.';
                }
                field(gAmount; gAmount)
                {
                    Caption = 'Montant HT';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant HT field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord();
    begin
        SetExternalValues();
    end;

    trigger OnNewRecord(BelowxRec: Boolean);
    begin
        gAmount := '';
        gExternalNo := '';
    end;

    var
        gExternalNo: Text;
        gAmount: Text;

    local procedure SetExternalValues();
    var
        PurchInvoice: Record "Purch. Inv. Header";
        PurchHeader: Record "Purchase Header";
        WhseReceiptLine: Record "Warehouse Receipt Line";
        WhseReceiptHeader: Record "Warehouse Receipt Header";
        PurchOrder: Record "Purchase Header";
        value: Decimal;
        lCurrencyCode: Code[10];
    begin
        gExternalNo := '';
        gAmount := '';
        IF Rec."No." = '' THEN EXIT;
        PurchInvoice.SETAUTOCALCFIELDS(Amount);
        IF Rec.Type = Rec.Type::"Purch. Invoice Archive" THEN
            IF PurchInvoice.GET(Rec."No.") THEN BEGIN
                gExternalNo := PurchInvoice."Vendor Invoice No.";
                gAmount := FORMAT(PurchInvoice.Amount);
                IF (PurchInvoice."Currency Code" <> '') AND (PurchInvoice."Currency Code" <> 'EUR') THEN gAmount += ' (' + PurchInvoice."Currency Code" + ')';
            END;

        PurchHeader.SETAUTOCALCFIELDS(Amount);
        IF Rec.Type = Rec.Type::"Purch. Invoice" THEN
            IF PurchHeader.GET(PurchHeader."Document Type"::Invoice, Rec."No.") THEN BEGIN
                gExternalNo := PurchHeader."Vendor Invoice No.";
                gAmount := FORMAT(PurchHeader.Amount);
                IF (PurchHeader."Currency Code" <> '') AND (PurchHeader."Currency Code" <> 'EUR') THEN gAmount += ' (' + PurchHeader."Currency Code" + ')';
            END;

        IF Rec.Type = Rec.Type::Receipt THEN BEGIN
            IF WhseReceiptHeader.GET(Rec."No.") THEN BEGIN
                gExternalNo := WhseReceiptHeader."Vendor Shipment No.";
                WhseReceiptLine.SETRANGE("No.", Rec."No.");
                IF WhseReceiptLine.FINDSET() THEN
                    REPEAT
                        value += WhseReceiptLine.GetInfoLigne('TOTALNET2'); //"Qty. to Receive" * WhseReceiptLine."Prix Brut Th‚orique";
                        IF lCurrencyCode = '' THEN
                            IF PurchOrder.GET(PurchOrder."Document Type"::Order, WhseReceiptLine."Source No.") THEN
                                lCurrencyCode := PurchOrder."Currency Code";
                    UNTIL WhseReceiptLine.NEXT() = 0;
                gAmount := FORMAT(value);
                IF (lCurrencyCode <> '') AND (lCurrencyCode <> 'EUR') THEN gAmount += ' (' + lCurrencyCode + ')';
            END;
        END;
    end;
}

