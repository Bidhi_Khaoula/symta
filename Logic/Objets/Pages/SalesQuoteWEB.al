page 50061 "Sales Quote WEB"
{
    ApplicationArea = all;
    Caption = 'Sales Quote';
    CardPageID = "Sales Quote WEB";
    InsertAllowed = false;
    PageType = Document;
    RefreshOnActivate = true;
    SourceTable = "Sales Header";
    SourceTableView = WHERE("Document Type" = FILTER(Quote),
                            "Devis Web" = CONST(True));

    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General';
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    Enabled = "Sell-to Customer No.Enable";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';

                    trigger OnValidate()
                    begin
                        SelltoCustomerNoOnAfterValidat();
                    end;
                }
                field("Sell-to Contact No."; Rec."Sell-to Contact No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Contact No. field.';

                    trigger OnValidate()
                    begin
                        SelltoContactNoOnAfterValidate();
                    end;
                }
                field("Sell-to Customer Template Code"; Rec."Sell-to Customer Templ. Code")
                {
                    Enabled = SelltoCustomerTemplateCodeEnab;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer Template Code field.';

                    trigger OnValidate()
                    begin
                        SelltoCustomerTemplateCodeOnAf();
                    end;
                }
                field("Sell-to Customer Name"; Rec."Sell-to Customer Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer Name field.';
                }
                field("Sell-to Address"; Rec."Sell-to Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Address field.';
                }
                field("Sell-to Address 2"; Rec."Sell-to Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Address 2 field.';
                }
                field("Sell-to Post Code"; Rec."Sell-to Post Code")
                {
                    Caption = 'Sell-to Post Code/City';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Post Code/City field.';
                }
                field("Sell-to City"; Rec."Sell-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to City field.';
                }
                field("Sell-to Contact"; Rec."Sell-to Contact")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Contact field.';
                }
                field("Source Document Type"; Rec."Source Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Origine Document field.';
                }
                field("External Document No."; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field(Status; Rec.Status)
                {
                    Caption = 'Statut devis';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut devis field.';
                }
                field("No. of Archived Versions"; Rec."No. of Archived Versions")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. of Archived Versions field.';

                    trigger OnDrillDown()
                    begin
                        CurrPage.SAVERECORD();
                        COMMIT();
                        SalesHeaderArchive.SETRANGE("Document Type", Rec."Document Type"::Quote);
                        SalesHeaderArchive.SETRANGE("No.", Rec."No.");
                        SalesHeaderArchive.SETRANGE("Doc. No. Occurrence", Rec."Doc. No. Occurrence");
                        IF SalesHeaderArchive.GET(Rec."Document Type"::Quote, Rec."No.", Rec."Doc. No. Occurrence", Rec."No. of Archived Versions") THEN;
                        PAGE.RUNMODAL(PAGE::"Sales List Archive", SalesHeaderArchive);
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                field("Order Date"; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Requested Delivery Date"; Rec."Requested Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Delivery Date field.';
                }
                field("Promised Delivery Date"; Rec."Promised Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Promised Delivery Date field.';
                }
                field("Quote Type"; Rec."Quote Type")
                {
                    Caption = 'Type Edition';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Edition field.';
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Salesperson Code field.';

                    trigger OnValidate()
                    begin
                        SalespersonCodeOnAfterValidate();
                    end;
                }
                field("Campaign No."; Rec."Campaign No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Campaign No. field.';
                }
                field("Order Create User"; Rec."Order Create User")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur Création Cde field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Mode d'expédition"; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field("Transporteur imperatif"; Rec."Transporteur imperatif")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Transporteur imperatif field.';
                }
                field("Saturday Delivery"; Rec."Saturday Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Livraison le samedi field.';
                }
                field(CalcWeightToShip_FALSE; rec.CalcWeightToShip(FALSE))
                {
                    Caption = 'Poids brut à expédier';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids brut à expédier field.';
                }
            }
            part(SalesLines; "Sales Quote Subform WEB")
            {
                SubPageLink = "Document No." = FIELD("No.");
                ApplicationArea = All;
            }
            group(Invoicing)
            {
                Caption = 'Invoicing';
                field("Bill-to Customer No."; Rec."Bill-to Customer No.")
                {
                    Enabled = "Bill-to Customer No.Enable";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Customer No. field.';

                    trigger OnValidate()
                    begin
                        BilltoCustomerNoOnAfterValidat();
                    end;
                }
                field("Bill-to Contact No."; Rec."Bill-to Contact No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Contact No. field.';
                }
                field("Bill-to Customer Template Code"; Rec."Bill-to Customer Templ. Code")
                {
                    Enabled = BilltoCustomerTemplateCodeEnab;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Customer Template Code field.';

                    trigger OnValidate()
                    begin
                        BilltoCustomerTemplateCodeOnAf();
                    end;
                }
                field("Bill-to Name"; Rec."Bill-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Name field.';
                }
                field("Bill-to Address"; Rec."Bill-to Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Address field.';
                }
                field("Bill-to Address 2"; Rec."Bill-to Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Address 2 field.';
                }
                field("Bill-to Post Code"; Rec."Bill-to Post Code")
                {
                    Caption = 'Bill-to Post Code/City';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Post Code/City field.';
                }
                field("Bill-to City"; Rec."Bill-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to City field.';
                }
                field("Bill-to Contact"; Rec."Bill-to Contact")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bill-to Contact field.';
                }
                field("Payment Terms Code"; Rec."Payment Terms Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Payment Terms Code field.';
                }
                field("Echéances fractionnées"; Rec."Echéances fractionnées")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Echéances fractionnées field.';

                    trigger OnValidate()
                    begin
                        Ech233ancesfractionn233esOnAft();
                    end;
                }
                field("Payment Terms Code 2"; Rec."Payment Terms Code 2")
                {
                    Editable = "Payment Terms Code 2Editable";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Payment Terms Code 2 field.';
                }
                field("Due Date 2"; Rec."Due Date 2")
                {
                    Editable = "Due Date 2Editable";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the date d''échéance 2 field.';
                }
                field("Taux Premiere Fraction"; Rec."Taux Premiere Fraction")
                {
                    Editable = "Taux Premiere FractionEditable";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Taux Premiere Fraction field.';
                }
                field("Shortcut Dimension 1 Code"; Rec."Shortcut Dimension 1 Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 1 Code field.';

                    trigger OnValidate()
                    begin
                        ShortcutDimension1CodeOnAfterV();
                    end;
                }
                field("Shortcut Dimension 2 Code"; Rec."Shortcut Dimension 2 Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shortcut Dimension 2 Code field.';

                    trigger OnValidate()
                    begin
                        ShortcutDimension2CodeOnAfterV();
                    end;
                }
                field("Due Date"; Rec."Due Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Due Date field.';
                }
                field("Payment Discount %"; Rec."Payment Discount %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Payment Discount % field.';
                }
                field("Pmt. Discount Date"; Rec."Pmt. Discount Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pmt. Discount Date field.';
                }
                field("Payment Method Code"; Rec."Payment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Payment Method Code field.';
                }
                field("Prices Including VAT"; Rec."Prices Including VAT")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prices Including VAT field.';

                    trigger OnValidate()
                    begin
                        PricesIncludingVATOnAfterValid();
                    end;
                }
                field("VAT Bus. Posting Group"; Rec."VAT Bus. Posting Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the VAT Bus. Posting Group field.';
                }
                field("Invoice Type"; Rec."Invoice Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Facturation field.';
                }
                field("Exclure RFA"; Rec."Exclure RFA")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Exclure RFA field.';
                }
                field("Multi echéance"; Rec."Multi echéance")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multi echéance field.';
                }
                field("Customer Price Group"; Rec."Customer Price Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Customer Price Group field.';
                }
                field("Customer Disc. Group"; Rec."Customer Disc. Group")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Customer Disc. Group field.';
                }
                field("Centrale Active"; Rec."Centrale Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Centrale Active field.';
                }
            }
            group(Shipping)
            {
                Caption = 'Shipping';
                field("Ship-to Code"; Rec."Ship-to Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Code field.';
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field("Ship-to Address"; Rec."Ship-to Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address field.';
                }
                field("Ship-to Address 2"; Rec."Ship-to Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address 2 field.';
                }
                field("Ship-to Post Code"; Rec."Ship-to Post Code")
                {
                    Caption = 'Ship-to Post Code/City';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Post Code/City field.';
                }
                field("Ship-to City"; Rec."Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to City field.';
                }
                field("Ship-to Contact"; Rec."Ship-to Contact")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Contact field.';
                }
                field("Abandon remainder"; Rec."Abandon remainder")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Abandon reliquat field.';
                }
                field("Chiffrage BL"; Rec."Chiffrage BL")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Chiffrage BL field.';
                }
                field("Saturday Delivery2"; Rec."Saturday Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Livraison le samedi field.';
                }
                field("Instructions of Delivery"; Rec."Instructions of Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Instructions de livraison field.';
                }
                field("Insurance of Delivery"; Rec."Insurance of Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assurance de livraison field.';
                }
                field("Cash on Delivery"; Rec."Cash on Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Contre remboursement field.';
                }
                field("Combine Shipments"; Rec."Combine Shipments")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Combine Shipments field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Outbound Whse. Handling Time"; Rec."Outbound Whse. Handling Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outbound Whse. Handling Time field.';
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Method Code field.';
                }
                field("Shipping Agent Code2"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Transporteur imperatif2"; Rec."Transporteur imperatif")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Transporteur imperatif field.';
                }
                field("Shipping Agent Service Code"; Rec."Shipping Agent Service Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Service Code field.';
                }
                field("Shipping Time"; Rec."Shipping Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Time field.';
                }
                field("Late Order Shipping"; Rec."Late Order Shipping")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Late Order Shipping field.';
                }
                field("Package Tracking No."; Rec."Package Tracking No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Package Tracking No. field.';
                }
                field("Shipment Date"; Rec."Shipment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Date field.';
                }
                field("Shipping Advice"; Rec."Shipping Advice")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Advice field.';
                }
                field("Mode d'expédition2"; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field("Completely Shipped"; Rec."Completely Shipped")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Completely Shipped field.';
                }
                field("N° affrètement"; Rec."N° affrètement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° affrètement field.';
                }
            }
            group("Foreign Trade")
            {
                Caption = 'Foreign Trade';
                field("Currency Code"; Rec."Currency Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Currency Code field.';

                    trigger OnAssistEdit()
                    begin
                        CurrPage.UPDATE();
                        SalesCalcDiscByType.ApplyDefaultInvoiceDiscount(0, Rec);
                    end;

                    trigger OnValidate()
                    begin
                        CurrencyCodeOnAfterValidate();
                    end;
                }
                field("EU 3-Party Trade"; Rec."EU 3-Party Trade")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the EU 3-Party Trade field.';
                }
                field("Transaction Type"; Rec."Transaction Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Transaction Type field.';
                }
                field("Transaction Specification"; Rec."Transaction Specification")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Transaction Specification field.';
                }
                field("Transport Method"; Rec."Transport Method")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Transport Method field.';
                }
                field("Exit Point"; Rec."Exit Point")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Exit Point field.';
                }
                field("Area"; Rec."Area")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Area field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("&Quote")
            {
                Caption = '&Quote';
                Image = Quote;
                action(Statistics)
                {
                    Caption = 'Statistics';
                    Image = Statistics;
                    Promoted = true;
                    PromotedCategory = Process;
                    ShortCutKey = 'F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Statistics action.';

                    trigger OnAction()
                    begin
                        rec.CalcInvDiscForHeader();
                        COMMIT();
                        PAGE.RUNMODAL(PAGE::"Sales Statistics", Rec);
                        SalesCalcDiscByType.ResetRecalculateInvoiceDisc(Rec);
                    end;
                }
                action("Customer Card")
                {
                    Caption = 'Customer Card';
                    Image = Customer;
                    RunObject = Page "Customer Card";
                    RunPageLink = "No." = FIELD("Sell-to Customer No.");
                    ShortCutKey = 'Shift+F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Customer Card action.';
                }
                action("C&ontact Card")
                {
                    Caption = 'C&ontact Card';
                    Image = Card;
                    RunObject = Page "Contact Card";
                    RunPageLink = "No." = FIELD("Sell-to Contact No.");
                    ApplicationArea = All;
                    ToolTip = 'Executes the C&ontact Card action.';
                }
                action("Co&mments")
                {
                    Caption = 'Co&mments';
                    Image = ViewComments;
                    RunObject = Page "Sales Comment Sheet";
                    RunPageLink = "Document Type" = FIELD("Document Type"),
                                  "No." = FIELD("No."),
                                  "Document Line No." = CONST(0);
                    ApplicationArea = All;
                    ToolTip = 'Executes the Co&mments action.';
                }
                action(Dimensions)
                {
                    AccessByPermission = TableData 348 = R;
                    Caption = 'Dimensions';
                    Image = Dimensions;
                    ShortCutKey = 'Shift+Ctrl+D';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Dimensions action.';

                    trigger OnAction()
                    begin
                        rec.ShowDocDim();
                        CurrPage.SAVERECORD();
                    end;
                }
                action(Approvals)
                {
                    Caption = 'Approvals';
                    Image = Approvals;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Approvals action.';

                    trigger OnAction()
                    var
                        ApprovalEntries: Page "Approval Entries";
                    begin
                        ApprovalEntries.SetRecordFilters(DATABASE::"Sales Header", rec."Document Type", rec."No.");
                        ApprovalEntries.RUN();
                    end;
                }
            }
            group("Créer commande grp")
            {
                Caption = 'Créer commande';
                Visible = true;
                action("Créer commande")
                {
                    Caption = 'Créer commande';
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Créer commande action.';

                    trigger OnAction()
                    var
                        PurchaseHeader: Record "Purchase Header";
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin

                        //LM le 01-08-2012 =>test si ligne en cours d'insertion
                        IF NOT lm_fonction.encours_insert(rec."No.") THEN BEGIN
                            IF ApprovalsMgmt.PrePostApprovalCheckSales(Rec) THEN BEGIN
                                //WF Le 29-04-2010 => FarGroup Analyse complémentaire Avril 2010 ã2.7
                                //AskArchivage();
                                //FIN WF
                                CODEUNIT.RUN(CODEUNIT::"Sales-Quote to Order (Yes/No)", Rec);
                            END;
                        END;
                    end;
                }
            }
        }
        area(processing)
        {
            group("F&unctions")
            {
                Caption = 'F&unctions';
                Image = "Action";
                action("Calculate &Invoice Discount")
                {
                    Caption = 'Calculate &Invoice Discount';
                    Image = CalculateInvoiceDiscount;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Calculate &Invoice Discount action.';

                    trigger OnAction()
                    begin
                        ApproveCalcInvDisc();
                    end;
                }
                separator(" ")
                {
                }
                action("Get St&d. Cust. Sales Codes")
                {
                    Caption = 'Get St&d. Cust. Sales Codes';
                    Ellipsis = true;
                    Image = CustomerCode;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Get St&d. Cust. Sales Codes action.';

                    trigger OnAction()
                    var
                        StdCustSalesCode: Record "Standard Customer Sales Code";
                    begin
                        StdCustSalesCode.InsertSalesLines(Rec);
                    end;
                }
                separator("  ")
                {
                }
                action(CopyDocument)
                {
                    Caption = 'Copy Document';
                    Ellipsis = true;
                    Image = CopyDocument;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Copy Document action.';

                    trigger OnAction()
                    begin
                        CopySalesDoc.SetSalesHeader(Rec);
                        CopySalesDoc.RUNMODAL();
                        CLEAR(CopySalesDoc);
                    end;
                }
                action("Archive Document")
                {
                    Caption = 'Archi&ve Document';
                    Image = Archive;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Archi&ve Document action.';

                    trigger OnAction()
                    begin
                        ArchiveManagement.ArchiveSalesDocument(Rec);
                        CurrPage.UPDATE(FALSE);
                    end;
                }
                separator("   ")
                {
                }
            }
            group(Create)
            {
                Caption = 'Create';
                Image = NewCustomer;
                action("Make Order")
                {
                    Caption = 'Make &Order';
                    Image = MakeOrder;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Make &Order action.';

                    trigger OnAction()
                    var
                        PurchaseHeader: Record "Purchase Header";
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin
                        IF ApprovalsMgmt.PrePostApprovalCheckSales(Rec) THEN
                            CODEUNIT.RUN(CODEUNIT::"Sales-Quote to Order (Yes/No)", Rec);
                    end;
                }
                action("C&reate Customer")
                {
                    Caption = 'C&reate Customer';
                    Image = NewCustomer;
                    ApplicationArea = All;
                    ToolTip = 'Executes the C&reate Customer action.';

                    trigger OnAction()
                    begin
                        IF rec.CheckCustomerCreated(FALSE) THEN
                            CurrPage.UPDATE(TRUE);
                    end;
                }
                action("Create &To-do")
                {
                    Caption = 'Create &To-do';
                    Image = NewToDo;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Create &To-do action.';

                    trigger OnAction()
                    begin
                        rec.CreateTask();

                    end;
                }
                separator("    ")
                {
                }
            }
            action("&Print")
            {
                Caption = '&Print';
                Ellipsis = true;
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the &Print action.';

                trigger OnAction()
                begin
                    DocPrint.PrintSalesHeader(Rec);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        ActivateFields();

        FctOnAfterGetCurrRecord();
    end;

    trigger OnDeleteRecord(): Boolean
    begin
        CurrPage.SAVERECORD();

        EXIT(rec.ConfirmDeletion());
    end;

    trigger OnInit()
    begin
        "Bill-to Customer No.Enable" := TRUE;
        "Sell-to Customer No.Enable" := TRUE;
        SelltoCustomerTemplateCodeEnab := TRUE;
        BilltoCustomerTemplateCodeEnab := TRUE;
        "Taux Premiere FractionEditable" := TRUE;
        "Due Date 2Editable" := TRUE;
        "Payment Terms Code 2Editable" := TRUE;
        SalesHistoryStnVisible := TRUE;
        BillToCommentBtnVisible := TRUE;
        BillToCommentPictVisible := TRUE;
        SalesHistoryBtnVisible := TRUE;
    end;

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        rec.CheckCreditMaxBeforeInsert();
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        rec."Responsibility Center" := UserMgt.GetSalesFilter();
        FctOnAfterGetCurrRecord();
    end;

    trigger OnOpenPage()
    begin
        IF UserMgt.GetSalesFilter() <> '' THEN BEGIN
            rec.FILTERGROUP(2);
            rec.SETRANGE("Responsibility Center", UserMgt.GetSalesFilter());
            rec.FILTERGROUP(0);
        END;

        ActivateFields();
    end;

    var

        //  SalesSetup: Record "Sales & Receivables Setup";

        SalesHeaderArchive: Record "Sales Header Archive";
        CopySalesDoc: Report "Copy Sales Document";
        DocPrint: Codeunit "Document-Print";
        UserMgt: Codeunit "User Setup Management";
        ArchiveManagement: Codeunit ArchiveManagement;
        SalesCalcDiscByType: Codeunit "Sales - Calc Discount By Type";
        // SalesInfoPaneMgt: Codeunit "Sales Info-Pane Management";

        // "--- GVarESKAPE ---": Integer;
        GestionSYMTA: Codeunit "Gestion SYMTA";
        lm_fonction: Codeunit lm_fonction;
        SalesHistoryBtnVisible: Boolean;
        BillToCommentPictVisible: Boolean;
        BillToCommentBtnVisible: Boolean;
        SalesHistoryStnVisible: Boolean;
        "Payment Terms Code 2Editable": Boolean;
        "Due Date 2Editable": Boolean;
        "Taux Premiere FractionEditable": Boolean;
        BilltoCustomerTemplateCodeEnab: Boolean;
        SelltoCustomerTemplateCodeEnab: Boolean;
        "Sell-to Customer No.Enable": Boolean;
        "Bill-to Customer No.Enable": Boolean;
        /*   Text19070588Lbl: Label 'Sell-to Customer';
          Text19069283Lbl: Label 'Bill-to Customer'; */
        Text000Err: Label 'Unable to execute this function while in view only mode.';

    procedure UpdateAllowed(): Boolean
    begin
        IF CurrPage.EDITABLE = FALSE THEN BEGIN
            MESSAGE(Text000Err);
            EXIT(FALSE);
        END ELSE
            EXIT(TRUE);
    end;

    procedure ActivateFields()
    begin
        BilltoCustomerTemplateCodeEnab := rec."Bill-to Customer No." = '';
        SelltoCustomerTemplateCodeEnab := rec."Sell-to Customer No." = '';
        "Sell-to Customer No.Enable" := rec."Sell-to Customer Templ. Code" = '';
        "Bill-to Customer No.Enable" := rec."Bill-to Customer Templ. Code" = '';
    end;

    local procedure UpdateInfoPanel()
    var

        CduLFunctions: Codeunit "Codeunits Functions";
        DifferSellToBillTo: Boolean;
    begin
        DifferSellToBillTo := Rec."Sell-to Customer No." <> Rec."Bill-to Customer No.";
        SalesHistoryBtnVisible := DifferSellToBillTo;
        BillToCommentPictVisible := DifferSellToBillTo;
        BillToCommentBtnVisible := DifferSellToBillTo;
        SalesHistoryStnVisible := CduLFunctions.DocExist(Rec, Rec."Sell-to Customer No.");
        IF DifferSellToBillTo THEN
            SalesHistoryBtnVisible := CduLFunctions.DocExist(Rec, Rec."Bill-to Customer No.");

        SalesHistoryBtnVisible := FALSE;
    end;

    local procedure ApproveCalcInvDisc()
    begin
        CurrPage.SalesLines.PAGE.ApproveCalcInvDisc();
    end;

    procedure VisibleEchFraction()
    begin
        "Payment Terms Code 2Editable" := Rec."Echéances fractionnées";
        "Due Date 2Editable" := Rec."Echéances fractionnées";
        "Taux Premiere FractionEditable" := Rec."Echéances fractionnées";
    end;

    procedure SendFax()
    var
        LDoc: Record "Sales Header";
        LSendFax: Codeunit "Eskape Communication";
        LDocRef: RecordRef;
    begin
        LDoc := Rec;
        LDoc.SETRECFILTER();
        LDocRef.GETTABLE(LDoc);
        LSendFax.ReportToFax(LDocRef, 0, '');
    end;

    local procedure SelltoCustomerNoOnAfterValidat()
    begin
        ActivateFields();
        CurrPage.UPDATE();
    end;

    local procedure SalespersonCodeOnAfterValidate()
    begin
        CurrPage.SalesLines.PAGE.UpdateForm(TRUE);
    end;

    local procedure SelltoContactNoOnAfterValidate()
    begin
        ActivateFields();
        CurrPage.UPDATE();
    end;

    local procedure SelltoCustomerTemplateCodeOnAf()
    begin
        ActivateFields();
        CurrPage.UPDATE();
    end;

    local procedure BilltoCustomerNoOnAfterValidat()
    begin
        ActivateFields();
        CurrPage.UPDATE();
    end;

    local procedure ShortcutDimension1CodeOnAfterV()
    begin
        CurrPage.SalesLines.PAGE.UpdateForm(TRUE);
    end;

    local procedure ShortcutDimension2CodeOnAfterV()
    begin
        CurrPage.SalesLines.PAGE.UpdateForm(TRUE);
    end;

    local procedure PricesIncludingVATOnAfterValid()
    begin
        CurrPage.UPDATE();
    end;

    local procedure BilltoCustomerTemplateCodeOnAf()
    begin
        ActivateFields();
        CurrPage.UPDATE();
    end;

    local procedure Ech233ancesfractionn233esOnAft()
    begin
        VisibleEchFraction();
    end;

    local procedure CurrencyCodeOnAfterValidate()
    begin
        CurrPage.SalesLines.PAGE.UpdateForm(TRUE);
    end;

    local procedure FctOnAfterGetCurrRecord()
    begin
        xRec := Rec;
        ActivateFields();
    end;

    local procedure SelltoCustomerNoOnAfterInput(var Text: Text[1024])
    begin
        // AD Le 24-05-2011 => SYMTA
        GestionSYMTA.CompleteNoClient(Text);
    end;
}

