page 50246 "Multi Consultation PrixVente"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = ListPart;
    SourceTable = "Sales Price";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Référence Active"; Rec."Référence Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Sales Type"; Rec."Sales Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Type field.';
                }
                field("Sales Code"; Rec."Sales Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Code field.';
                }
                field("Minimum Quantity"; Rec."Minimum Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Minimum Quantity field.';
                }
                field("Prix catalogue"; Rec."Prix catalogue")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix catalogue field.';
                }
                field(Coefficient; Rec.Coefficient)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coefficient field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
                field("Allow Line Disc."; Rec."Allow Line Disc.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Allow Line Disc. field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure InitForm(_pCodeArticle: Code[20]; _pTypeCommande: Integer)
    var
        LItem: Record Item;
        LPrix: Record "Sales Price";
    begin
        Rec.DELETEALL();
        IF NOT LItem.GET(_pCodeArticle) THEN EXIT;


        CLEAR(LPrix);
        LPrix.SETRANGE("Item No.", LItem."No.");
        LPrix.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
        LPrix.SETRANGE("Starting Date", 0D, WORKDATE());
        IF LPrix.FINDFIRST() THEN
            REPEAT
                Rec := LPrix;
                Rec.Insert();
            UNTIL LPrix.NEXT() = 0;
    end;
}

