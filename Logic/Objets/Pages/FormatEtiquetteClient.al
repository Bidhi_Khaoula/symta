page 50004 "Format Etiquette Client"
{
    CardPageID = "Format Etiquette Client";
    PageType = List;
    SourceTable = "Format Etiquette Client";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Format Code"; Rec."Format Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Format field.';
                }
                field(Descrption; Rec.Descrption)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Designation field.';
                }
                field("Imprimer N° Article"; Rec."Imprimer N° Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer N° Article field.';
                }
                field("Imprimer Gencode"; Rec."Imprimer Gencode")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Gencode field.';
                }
                field("Imprimer Référence Externe"; Rec."Imprimer Référence Externe")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Référence Externe field.';
                }
                field("Imprimer Designatio,"; Rec."Imprimer Designatio,")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Designatio, field.';
                }
                field("Imprimer Colisage"; Rec."Imprimer Colisage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Colisage field.';
                }
                field("Imprimer Zone Libre 1"; Rec."Imprimer Zone Libre 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Zone Libre 1 field.';
                }
                field("Imprimer Zone Libre 2"; Rec."Imprimer Zone Libre 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Zone Libre 2 field.';
                }
                field("Imprimer Zone Libre 3"; Rec."Imprimer Zone Libre 3")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Zone Libre 3 field.';
                }
                field("Imprimer Zone Libre 4"; Rec."Imprimer Zone Libre 4")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Zone Libre 4 field.';
                }
                field("Imprimer Nom Client"; Rec."Imprimer Nom Client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Nom Client field.';
                }
            }
        }
    }
}

