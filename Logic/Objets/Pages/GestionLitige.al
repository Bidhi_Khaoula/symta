page 50095 "Gestion Litige"
{
    CardPageID = "Gestion Litige";
    PageType = List;
    SourceTable = "Gestion Litige";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Whse Receipt No."; Rec."Whse Receipt No.")
                {
                    Caption = 'No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Date de création"; Rec."Date de création")
                {
                    Caption = 'Date de création';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date de création field.';
                }
                field("N° BL fournisseur"; Rec."N° BL fournisseur")
                {
                    Caption = 'N° BL fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° BL fournisseur field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    Caption = 'Line No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    Caption = 'N° Fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Fournisseur field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    Caption = 'Item No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';

                    trigger OnValidate()
                    begin
                        IF rec_item.GET(Rec."Item No.") THEN;
                    end;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Ref. Active 2"; Rec."Ref. Active 2")
                {
                    Caption = 'Ref active';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref active field.';
                }
                field("Ref. Fournisseur"; Rec."Ref. Fournisseur")
                {
                    Caption = 'Ref. Fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Fournisseur field.';
                }
                field("Quantité Théorique"; Rec."Quantité Théorique")
                {
                    Caption = 'Quantity';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Receipt Quantity"; Rec."Receipt Quantity")
                {
                    Caption = 'Qté Reçue';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté Reçue field.';
                }
                field("Prix Brut Théorique"; Rec."Prix Brut Théorique")
                {
                    Caption = 'Px Brut fac';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Px Brut fac field.';
                }
                field("Remise 1 Théorique"; Rec."Remise 1 Théorique")
                {
                    Caption = 'Remise 1/fac';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1/fac field.';
                }
                field("Remise 2 Théorique"; Rec."Remise 2 Théorique")
                {
                    Caption = 'Remise 2/fac';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2/fac field.';
                }
                field("Prix Net Théorique"; Rec."Prix Net Théorique")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Net Théorique field.';
                }
                field("Total Net Théorique"; Rec."Total Net Théorique")
                {
                    Caption = 'Total Net fac';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net fac field.';
                }
                field("Prix Brut Réceptionné"; Rec."Prix Brut Réceptionné")
                {
                    Caption = 'Prix Brut Réceptionné';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut Réceptionné field.';
                }
                field("Remise 1 Réceptionnée"; Rec."Remise 1 Réceptionnée")
                {
                    Caption = 'Remise 1 Réceptionnée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 Réceptionnée field.';
                }
                field("Remise 2 Réceptionnée"; Rec."Remise 2 Réceptionnée")
                {
                    Caption = 'Remise 2 Réceptionnée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 Réceptionnée field.';
                }
                field("Prix Net Réceptionné"; Rec."Prix Net Réceptionné")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Net Réceptionné field.';
                }
                field("Total Net  Réceptionné"; Rec."Total Net  Réceptionné")
                {
                    Caption = 'Total Net  Réceptionné';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net  Réceptionné field.';
                }
                field(Ecart; Rec.Ecart)
                {
                    Caption = 'Ecart';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ecart field.';
                }
                field("Date Litige"; Rec."Date Litige")
                {
                    Caption = 'Date Litige';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Litige field.';
                }
                field("Litige Prix"; Rec."Litige Prix")
                {
                    Caption = 'Litige Prix';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Litige Prix field.';
                }
                field("Liitige Quantité"; Rec."Liitige Quantité")
                {
                    Caption = 'Liitige Quantité';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Liitige Quantité field.';
                }
                field(Action; Rec.Action)
                {
                    Caption = 'Action';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Action field.';
                }
                field(Statut; Rec.Statut)
                {
                    Caption = 'Statut';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Closed Date"; Rec."Closed Date")
                {
                    Caption = 'Date Cloture';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Cloture field.';
                }
                field("document régularisation"; Rec."document régularisation")
                {
                    Caption = 'document régularisation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the document régularisation field.';
                }
                field("référence régularisation"; Rec."référence régularisation")
                {
                    Caption = 'référence régularisation';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the référence régularisation field.';
                }
                field(vide; vide)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the vide field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Fiche)
            {
                Caption = 'Fiche';
                Promoted = true;
                PromotedCategory = Process;
                RunObject = Page "Fiche litige";
                RunPageLink = "Whse Receipt No." = FIELD("Whse Receipt No."),
                              "Line No." = FIELD("Line No.");
                RunPageView = SORTING("Whse Receipt No.", "Line No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Fiche action.';
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        IF rec_item.GET(Rec."Item No.") THEN;
    end;

    var
        rec_item: Record Item;
        vide: Text[30];

    procedure Diff(): Decimal
    begin
        EXIT(Rec."Total Net  Réceptionné" - Rec."Total Net Théorique");
    end;
}

