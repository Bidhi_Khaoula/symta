page 50075 "Interdiction Vente"
{
    CardPageID = "Interdiction Vente";
    PageType = List;
    SourceTable = "Interdiction Vente";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Type Article"; Rec."Type Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Article field.';
                }
                field("Code Article"; Rec."Code Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Article field.';
                }
                field("Type Vente"; Rec."Type Vente")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Vente field.';
                }
                field("Code Vente"; Rec."Code Vente")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Vente field.';
                }
                field(Statut; Rec.Statut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Date Debut"; Rec."Date Debut")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Debut field.';
                }
                field("Date Fin"; Rec."Date Fin")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Fin field.';
                }
            }
        }
    }

    actions
    {
    }
}

