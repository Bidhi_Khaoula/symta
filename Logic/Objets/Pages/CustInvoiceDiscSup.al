page 50012 "Cust. Invoice Disc. Sup."
{
    Caption = 'Remise vente supplémentaire';
    CardPageID = "Cust. Invoice Disc. Sup.";
    PageType = List;
    SourceTable = "Cust. Invoice Disc. Sup.";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Ligne Type"; Rec."Ligne Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type ligne field.';
                }
                field("Gl Account No."; Rec."Gl Account No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° de compte comptable field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Pourcent Disc."; Rec."Pourcent Disc.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pourcentage remise field.';
                }
            }
        }
    }
}

