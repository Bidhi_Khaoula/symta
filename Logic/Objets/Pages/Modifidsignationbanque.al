page 50016 "Modifié désignation banque"
{
    CardPageID = "Modifié désignation banque";
    PageType = Card;
    Permissions = TableData "Bank Account Ledger Entry" = rm;
    SourceTable = "Bank Account Ledger Entry";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(General)
            {
                field("Bank Account No."; Rec."Bank Account No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bank Account No. field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
            }
        }
    }
}

