page 50233 "ECommerce Sales Line"
{
    PageType = List;
    SourceTable = "Sales Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Type_Document; Rec."Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Type field.';
                }
                field(No_Cde; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field(Type_Ligne; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(No; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Reference_Active; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field(Qte_Commandee; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field(Prix_Brut; Rec."Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';
                }
                field(Remise_1; Rec."Discount1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise1 field.';
                }
                field(Remise_2; Rec."Discount2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise2 field.';
                }
                field(Prix_Net; Rec."Net Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix unitaire net field.';
                }
                field(Montant_HT; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
            }
        }
    }

    actions
    {
    }
}

