page 50323 "Packing List Subform2"
{
    ApplicationArea = all;
    Caption = 'Détail colis';
    PageType = ListPart;
    SourceTable = "Packing List Package";

    layout
    {
        area(content)
        {
            repeater("Général")
            {
                Caption = 'Général';
                field("Package No."; Rec."Package No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° colis field.';
                }
                field("Package Type"; Rec."Package Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de colis field.';
                }
                field("Package Quantity"; Rec."Package Quantity")
                {
                    StyleExpr = gStylePackageQuantity;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité dans colis field.';
                }
                field("Package Weight"; Rec."Package Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids brut field.';

                    trigger OnValidate()
                    begin
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                field("Tare Weight"; Rec."Tare Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Tare Weight field.';
                }
                field("Net Weight"; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
                field(Length; Rec.Length)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Longueur (cm) field.';
                }
                field(Width; Rec.Width)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Largeur (cm) field.';
                }
                field(Height; Rec.Height)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hauteur (cm) field.';
                }
                field(Volume; Rec.Volume)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Volume (m3) field.';
                }
                field("Outside Of EC"; Rec."Outside Of EC")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hors CE field.';
                }
                field("Comment 1"; Rec."Comment 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire 1 field.';
                }
                field("Comment 2"; Rec."Comment 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire 2 field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Ligne)
            {
                Caption = 'Ligne';
                action("Imprimer étiquette")
                {
                    Caption = 'Imprimer étiquette';
                    Image = Print;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer étiquette action.';

                    trigger OnAction()
                    begin
                        gGestionBP.ImprimerEtiquetteColis(Rec."Packing List No.", Rec."Package No.", '');
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        // CFR 15/12/2020 > WIIO - Packing List - format des qté en PL
        IF (Rec."Package Quantity" = 0) THEN
            gStylePackageQuantity := 'Unfavorable'
        ELSE
            gStylePackageQuantity := '';
        // FIN CFR 02/09/2020
    end;

    var
        gGestionBP: Codeunit "Gestion Bon Préparation";
        gStylePackageQuantity: Text;
}

