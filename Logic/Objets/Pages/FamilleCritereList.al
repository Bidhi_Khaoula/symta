page 50243 "Famille Critere List"
{
    Caption = 'Affectation Critères';
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Famille Critere";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Super Famille"; Rec."Super Famille")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Super Famille field.';
                }
                field(Famille; Rec.Famille)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Famille field.';
                }
                field("Sous Famille"; Rec."Sous Famille")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille field.';
                }
                field("Code Critere"; Rec."Code Critere")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Critere field.';
                }
                field(Priorité; Rec.Priorité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Priorité field.';
                }
            }
        }
    }

    actions
    {
    }
}

