page 50024 "Import Tarif Tab_Tar"
{
    CardPageID = "Import Tarif Tab_Tar";
    PageType = Card;
    RefreshOnActivate = true;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(General)
            {
                Caption = 'General';
                field(Etape; Etape)
                {
                    Caption = 'Etape';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Etape field.';

                    trigger OnValidate()
                    begin
                        EnableFields()
                    end;
                }
                field(CodeFournisseurTarif; CodeFournisseurTarif)
                {
                    Caption = 'Fournisseur / Groupe TARIF';
                    Enabled = CodeFournisseurTarifVisible;
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fournisseur / Groupe TARIF field.';
                }
                field(CodeFournisseurArticle; CodeFournisseurArticle)
                {
                    Caption = 'Fournisseur / Groupe ARTICLE';
                    Enabled = CodeFournisseurArticleVisible;
                    TableRelation = Vendor;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fournisseur / Groupe ARTICLE field.';
                }
                field(ParRefActive; _ParRefActive)
                {
                    Caption = 'Par Référence Active';
                    Enabled = ParRefActiveVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Par Référence Active field.';
                }
                field(AvecAncienneRemise; _AvecAncienneRemise)
                {
                    Caption = 'Avec Ancienne Remise';
                    Enabled = AvecAncienneRemiseVisible;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Avec Ancienne Remise field.';
                }
                field(ClotureToutTarif; ClotureToutTarif)
                {
                    Caption = 'Clôturer tout les tarifs du fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Clôturer tout les tarifs du fournisseur field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Valider)
            {
                Caption = 'Valider';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Valider action.';

                trigger OnAction()
                begin
                    CLEAR(GestionTabTar);
                    CASE Etape OF
                        Etape::"Import Fichier":
                            ImportFichier();
                        Etape::"Insertion Fichier":
                            InsertionFichier();
                        Etape::Statistiques:
                            Statistiques();
                        Etape::"Implémenter Tarif":
                            ImplémenterTarif();
                    END;
                end;
            }
            action("MAJ NH- PCC MPL")
            {
                ApplicationArea = All;
                RunObject = Report "Reset PCC MPL absent TAB_TAR";
                Promoted = true;
                PromotedIsBig = true;
                Image = UpdateDescription;
                PromotedCategory = Report;
                ToolTip = 'Executes the MAJ NH- PCC MPL action.';
            }
        }
    }

    trigger OnInit()
    begin
        AvecAncienneRemiseVisible := TRUE;
        ParRefActiveVisible := TRUE;
        CodeFournisseurArticleVisible := TRUE;
        CodeFournisseurTarifVisible := TRUE;
    end;

    trigger OnOpenPage()
    begin
        CodeFournisseurTarif := 'XXXXXXX-FAC';

        Etape := Etape::"Import Fichier";
        EnableFields();
    end;

    var
        GestionTabTar: Codeunit "Gestion des tarifs Fourn. Temp";
        CodeFournisseurTarif: Code[20];
        CodeFournisseurArticle: Code[20];
        _ParRefActive: Boolean;
        _AvecAncienneRemise: Boolean;

        ESK001Lbl: Label 'NH,DIV,QTE';
        Etape: Option "Import Fichier","Insertion Fichier",Statistiques,"Implémenter Tarif";
        CodeFournisseurTarifVisible: Boolean;
        CodeFournisseurArticleVisible: Boolean;
        ParRefActiveVisible: Boolean;
        AvecAncienneRemiseVisible: Boolean;
        ClotureToutTarif: Boolean;

    procedure EnableFields()
    begin

        CASE Etape OF
            Etape::"Import Fichier":
                BEGIN
                    CodeFournisseurTarifVisible := FALSE;
                    CodeFournisseurArticleVisible := FALSE;
                    ParRefActiveVisible := FALSE;
                    AvecAncienneRemiseVisible := FALSE;
                END;
            Etape::"Insertion Fichier":
                BEGIN
                    CodeFournisseurTarifVisible := TRUE;
                    CodeFournisseurArticleVisible := FALSE;
                    ParRefActiveVisible := FALSE;
                    AvecAncienneRemiseVisible := FALSE;
                END;
            Etape::Statistiques:
                BEGIN
                    CodeFournisseurTarifVisible := TRUE;
                    CodeFournisseurArticleVisible := TRUE;
                    ParRefActiveVisible := TRUE;
                    AvecAncienneRemiseVisible := TRUE;
                END;
            Etape::"Implémenter Tarif":
                BEGIN
                    CodeFournisseurTarifVisible := TRUE;
                    CodeFournisseurArticleVisible := FALSE;
                    ParRefActiveVisible := FALSE;
                    AvecAncienneRemiseVisible := FALSE;
                END;

        END;
        CurrPage.UPDATE(TRUE);
        //MESSAGE('ad %1', CodeFournisseurTarifVisible);
    end;

    procedure ImportFichier()
    var

        RecText250: Record "Temp Texte 250";
        CuFile: Codeunit "File Management";
        //dtp: XMLport "IMPORT Tarifs Fournisseurs";
        varXmlFile: File;
        varInputStream: InStream;
        NomFichier: Text;
    begin
        //IF NOT CONFIRM('Importation dans table temporaire ?') THEN EXIT;

        RecText250.DELETEALL();

        //NomFichier := CuFile.OpenFileDialog('Fichier', '', 'Tous Fichiers (*.*)|*.*');

        NomFichier := CuFile.UploadFile('Fichier', '');

        varXmlFile.OPEN(NomFichier);
        varXmlFile.CREATEINSTREAM(varInputStream);
        XMLPORT.IMPORT(XMLPORT::"IMPORT Tarifs Fournisseurs", varInputStream);
        varXmlFile.CLOSE();


        MESSAGE('Terminé !');
    end;

    procedure InsertionFichier()
    var
        raz: Integer;
        formatFichier: Code[10];
        Selection: Integer;
    begin
        IF CodeFournisseurTarif = '' THEN ERROR('Code Fournisseur/Groupe obligatoire !');

        IF NOT CONFIRM('Insertion des tarifs dans tab_tar ?') THEN EXIT;

        //raz := CONFIRM('Remise à zéro des articles ?');
        Selection := STRMENU('MIS A JOUR / AJOUT, ----, EFFACER TOUT', 1);
        CASE Selection OF
            1:
                raz := 0;
            2:
                raz := 1;
            3:
                raz := 2;
        END;

        Selection := STRMENU(ESK001Lbl, 2);
        CASE Selection OF
            1:
                formatFichier := 'NH';
            2:
                formatFichier := 'DIV';
            3:
                formatFichier := 'QTE';
        END;

        GestionTabTar.FctInsertTempIntoTabTar(CodeFournisseurTarif, raz, formatFichier);
        MESSAGE('Terminé !');
    end;

    procedure Statistiques()
    begin
        IF NOT CONFIRM('Génération des statistiques ?') THEN EXIT;
        GestionTabTar.FctStatistiquesTarifs(CodeFournisseurArticle, CodeFournisseurTarif, _ParRefActive, _AvecAncienneRemise);
        MESSAGE('Terminé !');
    end;

    procedure "ImplémenterTarif"()
    begin
        IF NOT CONFIRM('Implémenter Tarifs ?') THEN EXIT;
        GestionTabTar.ImplémenterTarif('', CodeFournisseurTarif, ClotureToutTarif);
        MESSAGE('Terminé !');
    end;

    local procedure EtapeOnAfterValidate()
    begin
        EnableFields();
    end;

    // CFR le 19/10/2022 => R‚gie : mise … jour des fiches articles aprŠs import tab_tar (ajout action vers report 50153)
}

