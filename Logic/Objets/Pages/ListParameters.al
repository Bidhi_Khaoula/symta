page 50000 "List Parameters"
{

    Caption = 'Liste paramètres';
    CardPageID = "List Parameters";
    Editable = true;
    PageType = List;
    SourceTable = "Generals Parameters";
    ApplicationArea = All;
    UsageCategory = Lists;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(Code; rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(LongDescription; rec.LongDescription)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Libellé Long field.';
                }
                field(integer1; rec.integer1)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entier1 field.';
                }
                field(ShortDescription; rec.ShortDescription)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Libellé Court field.';
                }
                field(LongDescription2; rec.LongDescription2)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Libellé Long2 field.';
                }
                field(Type; rec.Type)
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(LongCode; rec.LongCode)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Long field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action("Remises Groupes")
                {
                    Caption = 'Remises Groupes';
                    RunObject = Page "Remises Fournisseurs";
                    RunPageLink = "Vendor Disc. Group" = FIELD(Code);
                    ApplicationArea = All;
                    Image = Discount;
                    ToolTip = 'Executes the Remises Groupes action.';
                }
            }
        }
    }
}

