page 50013 "Modifé désignation écriture GN"
{
    CardPageID = "Modifé désignation écriture GN";
    PageType = Card;
    Permissions = TableData 17 = rm;
    SourceTable = "G/L Entry";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("G/L Account No."; Rec."G/L Account No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the G/L Account No. field.';
            }
            field(Description; Rec.Description)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Description field.';
            }
        }
    }
}

