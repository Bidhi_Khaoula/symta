page 50010 "Sales Line Détail"
{
    Caption = 'Liste spéciale vente';
    CardPageID = "Sales Line Détail";
    PageType = Card;
    SaveValues = true;
    SourceTable = "Sales Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Price field.';

                    trigger OnValidate()
                    begin
                        UnitPriceOnAfterValidate();
                    end;
                }
                field("Unit Cost (LCY)"; Rec."Unit Cost (LCY)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost (LCY) field.';

                    trigger OnValidate()
                    begin
                        UnitCostLCYOnAfterValidate();
                    end;
                }
                field("Profit %"; "Profit %")
                {
                    Caption = '% Marge';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Marge field.';
                }
                field("Line Discount %"; Rec."Line Discount %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount % field.';

                    trigger OnValidate()
                    begin
                        LineDiscount37OnAfterValidate();
                    end;
                }
                field("Line Discount Amount"; Rec."Line Discount Amount")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount Amount field.';
                }
                field("VAT Base Amount"; Rec."VAT Base Amount")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the VAT Base Amount field.';
                }
                field("VAT %"; Rec."VAT %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the VAT % field.';
                }
                field("Amount Including VAT"; Rec."Amount Including VAT")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount Including VAT field.';
                }
                field("Code Vendeur"; "Code Vendeur")
                {
                    Caption = 'Code Vendeur';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Vendeur field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Quantity Shipped"; Rec."Quantity Shipped")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity Shipped field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field("Quantity Invoiced"; Rec."Quantity Invoiced")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity Invoiced field.';
                }
                field("Requested Delivery Date"; Rec."Requested Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Delivery Date field.';
                }
                field("Promised Delivery Date"; Rec."Promised Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Promised Delivery Date field.';
                }
                field("Shipping Time"; Rec."Shipping Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Time field.';
                }
                field("Gross Weight"; Rec."Gross Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gross Weight field.';
                }
                field("Net Weight"; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        IF SalesHeader.GET(Rec."Document Type", Rec."Document No.") THEN
            "Code Vendeur" := SalesHeader."Salesperson Code";

        "Calcule Marge"();
        NoOnFormat(FORMAT(Rec."No."));
    end;

    var

        SalesHeader: Record "Sales Header";
        CUMultiRef: Codeunit "Gestion Multi-référence";
        "Profit %": Decimal;

        "Code Vendeur": Code[10];

    procedure "Calcule Marge"()
    begin
        IF Rec."Net Unit Price" <> 0 THEN
            "Profit %" :=
              ROUND(
                100 * (1 - Rec."Unit Cost" / Rec."Net Unit Price"), 0.00001)
        ELSE
            "Profit %" := 0;
    end;

    local procedure LineDiscount37OnAfterValidate()
    begin
        "Calcule Marge"();
    end;

    local procedure UnitPriceOnAfterValidate()
    begin
        "Calcule Marge"();
    end;

    local procedure UnitCostLCYOnAfterValidate()
    begin
        "Calcule Marge"();
    end;

    local procedure NoOnFormat(Text: Text[1024])
    begin
        // MC Le 10-11-2011 => SYMTA -> Gestion MultiReference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;
}

