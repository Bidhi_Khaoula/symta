page 50094 "Multi Consultation Pri Factbox"
{
    PageType = CardPart;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Unit Cost"; Rec."Unit Cost")
            {
                Caption = 'Prix de revient(coût unitaire)';
                DecimalPlaces = 4 : 4;
                DrillDown = false;
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix de revient(coût unitaire) field.';
            }
            field("Cost is Adjusted"; Rec."Cost is Adjusted")
            {
                Caption = 'Cout ajusté';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Cout ajusté field.';
            }
            field("Profit %"; Rec."Profit %")
            {
                Caption = 'Marge';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Marge field.';
            }
            field(PrixCatalogue; PrixCatalogue)
            {
                Caption = 'Prix catalogue';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix catalogue field.';
            }
            field(Coefficient; Coefficient)
            {
                Caption = 'Coefficient';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Coefficient field.';
            }
            field(prix; prix)
            {
                Caption = 'Prix tarif';
                Editable = false;
                Style = Strong;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix tarif field.';
            }
            field(prix_net; prix_net)
            {
                Caption = 'Prix net';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Prix net field.';
            }
            field(remise1; remise1)
            {
                Caption = 'Remise 1';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 1 field.';
            }
            field(remise2; remise2)
            {
                Caption = 'Remise 2';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Remise 2 field.';
            }
            field(total; total)
            {
                Caption = 'Total';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Total field.';

                trigger OnValidate()
                begin

                    affiche_tarif();
                end;
            }
            field(Debug; Rec."No." + '*' + FORMAT(quantité_demande) + '*' + client_demande)
            {
                Caption = 'Debug';
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Debug field.';
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        affiche_tarif();
    end;

    var
        RecClient: Record Customer;
        RecFournisseur: Record Vendor;

        Rec_Devise: Record Currency;
        CurrExchRate: Record "Currency Exchange Rate";
        "codN°Fournisseur": Code[60];
        PrixCatalogue: Decimal;
        Coefficient: Decimal;
        prix_net: Decimal;
        prix: Decimal;
        remise1: Decimal;
        remise2: Decimal;
        remise: Decimal;
        total: Decimal;

        prix_four: Decimal;
        prix_net_four: Decimal;
        remise1_four: Decimal;
        remise2_four: Decimal;
        total_four: Decimal;
        DEEEAmount: Decimal;
        "quantité_demande": Decimal;
        TypeCdeVente: Integer;
        client_demande: Code[20];

        prix_four_devise: Decimal;
        factor: Decimal;
        fournisseur_demande: Code[20];

    procedure affiche_tarif()
    var
        TempPurchPrice: Record "Purchase Price" temporary;
        //  TempPurchLineDisc: Record "Purchase Line Discount" temporary;
        TempSalesPrice: Record "Sales Price" temporary;
        TempSalesLineDisc: Record "Sales Line Discount" temporary;
        "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
        LCodeunitsFunctions: Codeunit "Codeunits Functions";

    //  PurchPrice: Record "Purchase Price" temporary;
    begin
        //IF NOT ArticleChoisi THEN
        //  BEGIN
        //    prix     := 0;
        //    prix_net := 0;
        //    remise1  := 0;
        //    remise2  := 0;
        //    DEEEAmount := 0;
        //    // MC Le 25-10-2011 => Gestion des prix catalogues
        //    PrixCatalogue := 0;
        //    Coefficient := 0;
        //    // FIN MC Le 25-10-2011
        //    EXIT;
        //  END;

        IF Rec."Item Charge DEEE" <> '' THEN
            DEEEAmount := Rec."Item Charge DEEE Amount" * quantité_demande
        ELSE
            DEEEAmount := 0;

        // Remplissage des prix de base
        prix := Rec."Unit Price";
        prix_four := Rec."Last Direct Cost";
        remise1 := 0;
        remise2 := 0;
        //ERROR('affiche tarif' + client_demande + '*' + FORMAT(prix));
        remise1 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 1,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        remise2 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 2,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        IF NOT LCodeunitsFunctions.GetUnitPrice(
                  TempSalesPrice, '', '', '',
                  '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, remise, prix,
                  '', '', TypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
        THEN
            prix := Rec."Unit Price"

        // MC Le 25-10-2011 => Gestion des prix catalogues
        ELSE BEGIN
            PrixCatalogue := TempSalesPrice."Prix catalogue";
            Coefficient := TempSalesPrice.Coefficient;
        END;
        // FIN MC Le 25-10-2011.

        // Calcul du prix pour ce client.
        IF client_demande <> '' THEN BEGIN
            // Recherche des remises
            remise1 := 0;
            remise2 := 0;
            remise := 0;

            // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
            RecClient.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
            RecClient.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
            RecClient.CALCFIELDS("Centrale Active");
            // FIN CFR le 10/03/2021 - SFD20210201 historique centrale active

            remise1 := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 1,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.


            remise2 := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 2,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.

            remise := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 0,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.

            //MESSAGE('%1  %2   %3', remise1, remise2, remise);



            // Recherche si des prix tarif existe => Sinon on prend le prix de la fiche.
            IF NOT LCodeunitsFunctions.GetUnitPrice(
                      TempSalesPrice, RecClient."No.", '', RecClient."Customer Price Group",
                      '', Rec."No.", '', Rec."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, remise, prix,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                       TypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
            THEN
                prix := Rec."Unit Price"
            // MC Le 25-10-2011 => Gestion des prix catalogues
            ELSE BEGIN
                IF TempSalesPrice."Prix catalogue" <> 0 THEN BEGIN
                    PrixCatalogue := TempSalesPrice."Prix catalogue";
                    Coefficient := TempSalesPrice.Coefficient;
                END;
            END;
            // FIN MC Le 25-10-2011
        END;
        //FIN  Calcul du prix pour ce client.

        // Calcul du net et total
        IF remise <> 0 THEN
            prix_net := ROUND(prix - (prix * remise) / 100, 0.01)
        ELSE BEGIN
            prix_net := ROUND(prix, 0.01);
            remise1 := 0;
            remise2 := 0;
        END;

        IF prix_net <> 0 THEN
            Rec."Profit %" :=
              ROUND(
                100 * (1 - Rec."Unit Cost" / prix_net), 0.00001)
        ELSE
            Rec."Profit %" := 0;


        total := quantité_demande * prix_net;
        // FIN Calcul du net et total


        //remise1_four := "Purch Price Calc. Mgt.".GetPurchDisc(
        //          TempPurchLineDisc, '',
        //           "No.", '', "Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande) ;


        IF fournisseur_demande <> '' THEN BEGIN
            // Recherche des remises
            //    remise1_four := "Purch Price Calc. Mgt.".GetPurchDisc(
            //              TempPurchLineDisc, RecFournisseur."No.",
            //               "No.", '', "Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande) ;


            // Recherche si des prix tarif existe => Sinon on prend le prix de la fiche.
            //    IF NOT "Purch Price Calc. Mgt.".GetUnitCost(
            //              TempPurchPrice, RecFournisseur."No.",
            //              "No.", '', "Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande,
            //              code_mag, prix_four)
            //    THEN
            BEGIN
                prix_four := Rec."Last Direct Cost";
            END;

        END;

        // Calcul du net et total
        remise2_four := 0;
        IF remise1_four <> 0 THEN
            prix_net_four := ROUND(prix_four - (prix_four * remise1_four) / 100, 0.01)
        ELSE
            prix_net_four := ROUND(prix_four, 0.01);
        // FIN Calcul du net et total


        total_four := quantité_demande * prix_net_four;

        // MC Le 11-05-2010 FAR.AC.01 Analyse complémentaire => On veut le prix d'achat dans la devise du fournisseur
        prix_four_devise := prix_four;
        IF RecFournisseur.GET("codN°Fournisseur") THEN BEGIN
            "Purch Price Calc. Mgt.".FindPurchPrice(TempPurchPrice, RecFournisseur."No.", Rec."No."
            , '', Rec."Base Unit of Measure", RecFournisseur."Currency Code", WORKDATE(), FALSE);
            IF TempPurchPrice.FINDFIRST() THEN
                prix_four_devise := TempPurchPrice."Direct Unit Cost";
        END;
        //FIN MC Le 11-05-2010 FAR.AC.01

        //MC Le 19-05-2010 FAR.AC.01 Analyse complémentaire => calcul prix d'achat DS en fonction de celui
        // en devise fournisseur
        IF Rec_Devise.GET(RecFournisseur."Currency Code") THEN BEGIN
            factor := CurrExchRate.ExchangeRate(TODAY, Rec_Devise.Code);
            prix_four :=
            CurrExchRate.ExchangeAmtFCYToLCY(
              WORKDATE(), Rec_Devise.Code,
              prix_four_devise, factor);
        END;
    end;

    procedure Init(_Quantite: Decimal; _clientNo: Code[20]; _fournisseurNo: Code[20]; _TypeCdeVente: Option)
    begin
        quantité_demande := _Quantite;
        client_demande := _clientNo;
        fournisseur_demande := _fournisseurNo;
        TypeCdeVente := _TypeCdeVente;

        IF client_demande <> '' THEN RecClient.GET(client_demande);
        IF fournisseur_demande <> '' THEN RecFournisseur.GET(fournisseur_demande);
        affiche_tarif();
        CurrPage.UPDATE(FALSE);
    end;

    procedure Vider()
    begin
        prix := 123456;
        prix_net := 0;
        remise1 := 0;
        remise2 := 0;
        DEEEAmount := 0;
        PrixCatalogue := 0;
        Coefficient := 0;
        CLEAR(RecClient);
        CLEAR(RecFournisseur);
        total := 123456;
        affiche_tarif();
        CurrPage.UPDATE(FALSE);
    end;
}

