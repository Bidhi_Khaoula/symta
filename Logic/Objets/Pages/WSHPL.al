page 50227 "WSH / PL"
{
    Caption = 'Expéditions entrepôt et listes de colisage';
    CardPageID = "Warehouse Shipment";
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = true;
    PageType = List;
    SourceTable = "Warehouse Shipment Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(GetDestinationName; Rec.GetDestinationName())
                {
                    Caption = 'Donneur d''ordre';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Donneur d''ordre field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Assigned User ID"; Rec."Assigned User ID")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field(Status; Rec.Status)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field("Packing List No."; Rec."Packing List No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° liste de colisage field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        lPackingListHeader: Record "Packing List Header";
                    begin
                        IF Rec."Packing List No." = '' THEN
                            AffecterPL()
                        ELSE BEGIN
                            IF lPackingListHeader.GET(Rec."Packing List No.") THEN
                                AffichePL(lPackingListHeader);
                        END;
                    end;
                }
                field(gPackingListHeaderComment; gPackingListHeader.Comment)
                {
                    Caption = 'Commentaire liste de colisage';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire liste de colisage field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetCurrRecord()
    begin
        //CurrPage.UPDATE(FALSE);
    end;

    trigger OnAfterGetRecord()
    begin
        CLEAR(gPackingListHeader);
        gPackingListHeader.RESET();
        IF gPackingListHeader.GET(Rec."Packing List No.") THEN;
        //CurrPage.UPDATE(FALSE);
    end;

    var
        gPackingListHeader: Record "Packing List Header";

    local procedure AffecterPL()
    var
        lPackingListHeader: Record "Packing List Header";
        lPackingListMgmt: Codeunit "Packing List Mgmt";
    begin
        IF lPackingListMgmt.getPackingListForWhseShipment(lPackingListHeader, Rec."No.") THEN
            AffichePL(lPackingListHeader);
        //CurrPage.UPDATE(FALSE);
    end;

    local procedure AffichePL(pPackingListHeader: Record "Packing List Header")
    var
        lPackingListHeaderPage: Page "Packing List Header";
    begin
        CLEAR(lPackingListHeaderPage);
        pPackingListHeader.FILTERGROUP(2);
        pPackingListHeader.SETRANGE("No.", pPackingListHeader."No.");
        pPackingListHeader.SETRECFILTER();
        pPackingListHeader.FILTERGROUP(0);
        lPackingListHeaderPage.SETRECORD(pPackingListHeader);
        lPackingListHeaderPage.RUN();
        CurrPage.UPDATE(FALSE);
    end;
}

