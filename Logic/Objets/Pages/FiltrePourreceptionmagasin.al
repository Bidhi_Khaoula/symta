page 50020 "Filtre Pour reception magasin"
{
    CardPageID = "Filtre Pour reception magasin";
    PageType = Card;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Multi-Référence"; MultiRef)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the MultiRef field.';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    // MIG 2015 - GR - FORM
                    //// Frm_Art.LOOKUPMODE:=TRUE;

                    ////IF Frm_Art.RUNMODAL=ACTION::LookupOK THEN
                    //// BEGIN
                    ////  Frm_Art.GETRECORD(rec_item);
                    ////  "Réf active":=rec_item."No. 2";
                    //// END;
                    ////CLEAR(Frm_Art);
                end;
            }
            field("Code Fournisseur"; "code fournisseur")
            {
                LookupPageID = "Vendor List";
                TableRelation = Vendor;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the code fournisseur field.';
            }
            field("N° Commande"; "N° commande")
            {
                LookupPageID = "Purchase List";
                TableRelation = "Purchase Header"."No.";
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the N° commande field.';
            }
            field("Ref. Active"; "Réf active")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Réf active field.';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    // MIG2015 - GR - FORM
                    //// Frm_Art.LOOKUPMODE:=TRUE;

                    ////IF Frm_Art.RUNMODAL=ACTION::LookupOK THEN
                    //// BEGIN
                    ////  Frm_Art.GETRECORD(rec_item);
                    ////  "Réf active":=rec_item."No. 2";
                    //// END;
                    ////CLEAR(Frm_Art);
                end;
            }
            field("Code Article"; "Code Article")
            {
                LookupPageID = "Item List";
                TableRelation = Item;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Code Article field.';
            }
            field("Ref. Fournisseur"; "Ref fournisseur")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Ref fournisseur field.';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    // MIG2015 - GR - 2015
                    ////CLEAR(Frm_refextern);
                    ////IF "Ref fournisseur"<>'' THEN
                    //// rec_refexterne.SETRANGE("Vendor No.","Ref fournisseur");

                    ////Frm_refextern.LOOKUPMODE:=TRUE;
                    ////IF Frm_refextern.RUNMODAL=ACTION::LookupOK THEN
                    //// BEGIN
                    ////  Frm_refextern.GETRECORD(rec_refexterne);
                    ////  "Ref fournisseur":=rec_refexterne."Vendor Item No.";
                    //// END;
                end;
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("Group Executer")
            {
                Caption = 'Executer';
                action("Action Executer")
                {
                    Caption = 'Executer';
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F7';
                    ApplicationArea = All;
                    Image = Apply;
                    ToolTip = 'Executes the Executer action.';

                    trigger OnAction()
                    begin
                        Executer();
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    var
        EnteteCmd: Record "Purchase Header";
    begin
        IF "code fournisseur" = '' THEN BEGIN

            WhseReceiptLG.SETRANGE("No.", WhseReceiptHeader."No.");
            WhseReceiptLG.SETFILTER("Source No.", '<>%1', '');
            IF WhseReceiptLG.FINDFIRST() THEN BEGIN
                EnteteCmd.GET(EnteteCmd."Document Type"::Order, WhseReceiptLG."Source No.");
                "code fournisseur" := EnteteCmd."Buy-from Vendor No.";

            END;
        END;
    end;

    var
        RecDemmandeMagasin: Record "Warehouse Request";
        WhseReceiptHeader: Record "Warehouse Receipt Header";
        WhseReceiptLG: Record "Warehouse Receipt Line";
        GetSourceBatch: Report "Get Source Documents sYM";
        GestionMultiref: Codeunit "Gestion Multi-référence";
        "code fournisseur": Code[250];
        "N° commande": Code[20];
        "Réf active": Code[20];
        "Code Article": Code[20];
        "Ref fournisseur": Code[20];
        //  rec_item: Record Item;
        // rec_refexterne: Record "Item Vendor";


        MultiRef: Code[20];

        Text000Err: Label 'Au moins un filtre doit être renseigné.';

    procedure Executer()
    var
        RecLgAch: Record "Purchase Line";
        LWhseLine: Record "Warehouse Receipt Line";
    begin
        // MCO Le 27-09-2019 => Régie
        IF ("code fournisseur" = '') AND ("N° commande" = '') AND ("Réf active" = '') AND (MultiRef = '') AND ("Code Article" = '') AND ("Ref fournisseur" = '') THEN
            ERROR(Text000Err);
        // MCO Le 27-09-2019 => Régie

        RecDemmandeMagasin.SETRANGE("Source Document", RecDemmandeMagasin."Source Document"::"Purchase Order");
        IF "code fournisseur" <> '' THEN
            // C'est un filtre fournisseur donc on utilise un SETFILTER et non un SETRANGE
            RecLgAch.SETFILTER("Buy-from Vendor No.", "code fournisseur");

        IF "N° commande" <> '' THEN
            RecLgAch.SETRANGE("Document No.", "N° commande");

        IF "Réf active" <> '' THEN
            RecLgAch.SETRANGE("Recherche référence", "Réf active");

        // MC Le 03-05-2013 => Vérif si la référence n'est pas déjà dans la commande.
        IF "Réf active" <> '' THEN
            "Code Article" := GestionMultiref.RechercheArticleByActive("Réf active");
        // FIN MC Le 03-05-2013

        IF MultiRef <> '' THEN
            "Code Article" := GestionMultiref.RechercheMultiReference(MultiRef);


        IF "Code Article" <> '' THEN
            RecLgAch.SETRANGE("No.", "Code Article");

        IF "Ref fournisseur" <> '' THEN
            RecLgAch.SETRANGE("Item Reference No.", "Ref fournisseur");

        // AD Le 16-04-2012 => Vérif si la référence n'est pas déjà dans la commande

        LWhseLine.SETRANGE("No.", WhseReceiptHeader."No.");
        LWhseLine.SETRANGE("Item No.", "Code Article");
        IF LWhseLine.FINDFIRST() THEN
            IF NOT CONFIRM('Référence déjà présente sur la réception !, Voulez-vous extraire les éventuelles autres lignes ?') THEN
                ERROR('Référence déjà présente sur la réception !');
        // FIN AD Le 16-04-2012


        GetSourceBatch.SETTABLEVIEW(RecDemmandeMagasin);
        GetSourceBatch.SETTABLEVIEW(RecLgAch);
        GetSourceBatch.SetOneCreatedReceiptHeader(WhseReceiptHeader);

        // MC Le 30-01-2013 => Possibilité de mettre une ligne de commande sur deux réceptions différentes.
        GetSourceBatch.HideCheckIfLineInWhseDoc(TRUE);
        // FIN MC Le 30-01-2013
        GetSourceBatch.USEREQUESTPAGE(FALSE);
        GetSourceBatch.RUNMODAL();

        // AD Le 16-04-2012 => Changement de méthode
        /*
        IF RecLgAch.COUNT > 1 THEN
          IF CONFIRM('ATTENTION %1 Lignes', TRUE, RecLgAch.COUNT) THEN;
        */
        IF GetSourceBatch.GetNbLineCreate() <> 1 THEN
            //  IF CONFIRM('ATTENTION %1 Lignes', TRUE, GetSourceBatch.GetNbLineCreate) THEN;
            MESSAGE('ATTENTION %1 Lignes', GetSourceBatch.GetNbLineCreate());
        // FIN AD Le 16-04-2012

        CurrPage.CLOSE();

    end;

    procedure SetOneCreatedReceiptHeader(WhseReceiptHeader2: Record "Warehouse Receipt Header")
    begin
        WhseReceiptHeader := WhseReceiptHeader2;
    end;

    procedure FctFiltreFrn(_pFiltreFrn: Text[250])
    begin
        "code fournisseur" := _pFiltreFrn;
    end;
}

