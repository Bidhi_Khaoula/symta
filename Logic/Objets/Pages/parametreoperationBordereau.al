page 50093 "parametre operation/Bordereau"
{
    CardPageID = "parametre operation/Bordereau";
    PageType = List;
    SourceTable = "PARAM IMPORT";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Compte gene"; Rec."Compte gene")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compte gene field.';
                }
                field("code journal"; Rec."code journal")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the code journal field.';
                }
                field(operation; Rec.operation)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the operation field.';
                }
                field("mode de reglement"; Rec."mode de reglement")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the mode de reglement field.';
                }
                field(bordereau; Rec.bordereau)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the bordereau field.';
                }
            }
        }
    }

    actions
    {
    }
}

