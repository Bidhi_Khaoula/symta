page 50249 "Purchases Lines"
{
    ApplicationArea = all;
    Caption = 'Purchases Lines';
    PageType = List;
    SourceTable = "Purchase Line";
    SourceTableView = WHERE("Document Type" = CONST(Order));

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Document No."; Rec."Document No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Buy-from Vendor No."; Rec."Buy-from Vendor No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Buy-from Vendor No. field.';
                }
                field(Type; Rec.Type)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Unit of Measure"; Rec."Unit of Measure")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Qty. to Receive"; Rec."Qty. to Receive")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Receive field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    HideValue = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field("Qty. to Invoice"; Rec."Qty. to Invoice")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Invoice field.';
                }
                field("Direct Unit Cost"; Rec."Direct Unit Cost")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Direct Unit Cost field.';
                }
                field("Amt. Rcd. Not Invoiced"; Rec."Amt. Rcd. Not Invoiced")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amt. Rcd. Not Invoiced field.';
                }
                field("Quantity Received"; Rec."Quantity Received")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity Received field.';
                }
                field("Quantity Invoiced"; Rec."Quantity Invoiced")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity Invoiced field.';
                }
                field("Receipt Line No."; Rec."Receipt Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Receipt Line No. field.';
                }
                field("Requested Receipt Date"; Rec."Requested Receipt Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Receipt Date field.';
                }
                field("Promised Receipt Date"; Rec."Promised Receipt Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Promised Receipt Date field.';
                }
                field("Planned Receipt Date"; Rec."Planned Receipt Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Planned Receipt Date field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Order Date"; Rec."Order Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("Vendor Item No."; Rec."Vendor Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise1 field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise2 field.';
                }
                field("Net Unit Cost ctrl fac"; Rec."Net Unit Cost ctrl fac")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Cost field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Type field.';
                }
                field("Vendor Order No."; Rec."Vendor Order No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Order No. field.';
                }
                field(VendorName; Vendor.Name)
                {
                    Caption = 'Nom fournisseur';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom fournisseur field.';
                }
                field("Vendor Code Appro"; Rec."Vendor Code Appro")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code appro fournisseur field.';
                }
                field("Vendor Item Desciption"; ItemVendor."Vendor Item Desciption")
                {
                    Caption = 'Designation fournisseur';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Designation fournisseur field.';
                }
                field("Order Create User"; PurchHeader."Order Create User")
                {
                    Caption = 'Code utilisateur création commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur création commande field.';
                }
                field(PurchHeaderStatus; PurchHeader.Status)
                {
                    Caption = 'Statut Commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Commande field.';
                }
                field(Commentaires; Rec.Commentaires)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Release)
            {
                Caption = 'Re&lease';
                Image = ReleaseDoc;
                Promoted = true;
                PromotedCategory = Process;
                ShortCutKey = 'Ctrl+F9';
                ApplicationArea = All;
                ToolTip = 'Executes the Re&lease action.';

                trigger OnAction()
                var
                    ReleasePurchDoc: Codeunit "Release Purchase Document";
                begin
                    PurchHeader.GET(Rec."Document Type", Rec."Document No.");
                    ReleasePurchDoc.PerformManualRelease(PurchHeader);
                end;
            }
            action("Re&open")
            {
                Caption = 'Re&open';
                Image = ReOpen;
                ApplicationArea = All;
                ToolTip = 'Executes the Re&open action.';

                trigger OnAction()
                var
                    ReleasePurchDoc: Codeunit "Release Purchase Document";
                begin
                    PurchHeader.GET(rec."Document Type", rec."Document No.");
                    ReleasePurchDoc.PerformManualReopen(PurchHeader);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        IF Vendor.GET(Rec."Buy-from Vendor No.") THEN;
        IF ItemVendor.GET(Rec."Buy-from Vendor No.", Rec."No.", Rec."Variant Code") THEN;
        IF PurchHeader.GET(Rec."Document Type", Rec."Document No.") THEN;
    end;

    trigger OnOpenPage()
    begin
        Rec.SETFILTER("Outstanding Quantity", '<>0');
    end;

    var
        PurchHeader: Record "Purchase Header";
        Vendor: Record Vendor;
        ItemVendor: Record "Item Vendor";
}

