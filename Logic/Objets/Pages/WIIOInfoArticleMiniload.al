page 50307 "WIIO - Info Article Miniload"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Ref Active"; Rec."No. 2")
                {
                    Caption = 'No. 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Emplacement Defaut"; EmplacementParDefaut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the EmplacementParDefaut field.';
                }
                field("Emplacement Miniload"; InventorySetup."Code emplacement MiniLoad")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code emplacement MiniLoad field.';
                }
                field("Net Weight"; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
                field("Box Type"; Rec."Box Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de bac field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    var
        LWMSMngt: Codeunit "WMS Management";
    begin
        CompanyInfo.GET();
        CompanyInfo.TESTFIELD("Location Code");
        EmplacementParDefaut := '';
        LWMSMngt.GetDefaultBin(Rec."No.", '', CompanyInfo."Location Code", EmplacementParDefaut);

        InventorySetup.GET();
        InventorySetup.TESTFIELD("Code emplacement MiniLoad");
    end;

    var
        CompanyInfo: Record "Company Information";
        InventorySetup: Record "Inventory Setup";
        EmplacementParDefaut: Code[10];
}

