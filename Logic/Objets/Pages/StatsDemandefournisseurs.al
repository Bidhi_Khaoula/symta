page 50027 "Stats Demande fournisseurs"
{
    CardPageID = "Stats Demande fournisseurs";
    Editable = false;
    PageType = List;
    SourceTable = "Vendor Amount";
    SourceTableTemporary = true;
    SourceTableView = SORTING("Amount (LCY)", "Amount 2 (LCY)", "Vendor No.")
                      ORDER(Descending);

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Vendor No."; Rec."Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor No. field.';
                }
                field(GetVendorName; Rec.GetVendorName())
                {
                    Caption = 'Nom';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field("Amount (LCY)"; Rec."Amount (LCY)")
                {
                    Caption = 'Amount (LCY)';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount (LCY) field.';
                }
                field("Amount 2 (LCY)"; Rec."Amount 2 (LCY)")
                {
                    Caption = 'Amount 2 (LCY)';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount 2 (LCY) field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure InsertRecords(p_WrkSheetName: Code[10]; p_BatchName: Code[10]; p_LineNo: Decimal)
    var
        Rec_ReqLine: Record "Requisition Line";
        lm_fonction: Codeunit lm_fonction;
        dec_MtTot: Decimal;
        dec_MtVend: Decimal;

        type_cde: Integer;
    begin
        // ** Cette fonction permet d'instancier les enregistrements du formulaire ** \\

        // Initialisation du montant total.
        dec_MtTot := 0;

        //LM le 24-09-2012=>type de commande
        type_cde := STRMENU('&Niveau 0, &Niveau 1,&Niveau 2,&Niveau 3', 1) - 1;

        // Parcours des enregistrements.
        Rec_ReqLine.RESET();
        Rec_ReqLine.SETRANGE("Worksheet Template Name", p_WrkSheetName);
        Rec_ReqLine.SETRANGE("Journal Batch Name", p_BatchName);
        Rec_ReqLine.SETFILTER(Quantity, '<>%1', 0);
        Rec_ReqLine.SETRANGE("Accept Action Message", TRUE);
        // Possibilité de filtrer sur juste un ensemble de lignes.
        IF p_LineNo <> 0 THEN
            Rec_ReqLine.SETRANGE("Indice Jauge", 0, p_LineNo);

        IF Rec_ReqLine.FINDSET() THEN
            REPEAT
                // Calcul le montant
                // LM le 240912 calcul du net achat fournisseur
                dec_MtVend := lm_fonction.prix_net_achat(Rec_ReqLine."No.", Rec_ReqLine."Vendor No.", type_cde) * Rec_ReqLine.Quantity;
                /*
                    IF Rec_ReqLine."Line Discount %" <> 0 THEN
                      dec_MtVend :=  Rec_ReqLine."Direct Unit Cost" * Rec_ReqLine.Quantity
                      //dec_MtVend :=  (Rec_ReqLine."Direct Unit Cost" -
                      //    (Rec_ReqLine."Direct Unit Cost" *  Rec_ReqLine."Line Discount %" / 100)) * Rec_ReqLine.Quantity
                    ELSE
                      dec_MtVend := Rec_ReqLine."Direct Unit Cost" * Rec_ReqLine.Quantity;
                    // Recherche si le fournisseur existe déjà.
                */


                Rec.RESET();
                Rec.SETRANGE("Vendor No.", Rec_ReqLine."Vendor No.");
                IF Rec.FINDFIRST() THEN BEGIN
                    dec_MtTot += dec_MtVend;
                    //"Amount (LCY)" += dec_MtVend;
                    Rec.RENAME(Rec."Amount (LCY)" + dec_MtVend, 0, Rec_ReqLine."Vendor No.");
                END
                ELSE BEGIN
                    // Initialisation et insertion de l'enregistrement.
                    Rec.INIT();
                    Rec.VALIDATE("Vendor No.", Rec_ReqLine."Vendor No.");
                    Rec.VALIDATE("Amount (LCY)", dec_MtVend);
                    Rec.Insert();
                    dec_MtTot += dec_MtVend;
                END;
            UNTIL Rec_ReqLine.NEXT() = 0;

        // Calcul la part de chaque fournisseur
        Rec.RESET();
        IF Rec.FINDFIRST() THEN
            REPEAT
                //VALIDATE("Amount 2 (LCY)", "Amount (LCY)" * 100 / dec_MtTot);
                Rec.RENAME(Rec."Amount (LCY)", Rec."Amount (LCY)" * 100 / dec_MtTot, Rec."Vendor No.");
            UNTIL Rec.NEXT() = 0;

    end;

    procedure DeleteRecords()
    begin
        Rec.DELETEALL();
    end;
}

