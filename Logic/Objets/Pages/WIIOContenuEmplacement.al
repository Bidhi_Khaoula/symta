page 50311 "WIIO - ContenuEmplacement"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Bin Content";
    SourceTableView = SORTING("Location Code", "Bin Code", "Item No.", "Variant Code", "Unit of Measure Code")
                      WHERE(Quantity = FILTER(<> 0));

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field(Ref_Active; Rec."Référence Active")
                {
                    Caption = 'Ref_Active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref_Active field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field(capacité; Rec.capacité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the capacité field.';
                }
                field("% Seuil Capacité Réap. Pick."; Rec."% Seuil Capacité Réap. Pick.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Seuil Capacité Réap. Pick. field.';
                }
                field("Last Movement Date"; Rec."Last Movement Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity (Base) field.';
                }
                field("Last Entry Date"; Rec."Last Entry Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dernière entrée field.';
                }
                field(Default; Rec.Default)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Default field.';
                }
                field(Fixed; Rec.Fixed)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fixed field.';
                }
                field(Dedicated; Rec.Dedicated)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dedicated field.';
                }
                field("Last Movement Comment"; Rec.GetCommentaireInfoEntrée('COMMENT'))
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetCommentaireInfoEntrée(''COMMENT'') field.';
                }
            }
        }
    }

    actions
    {
    }
}

