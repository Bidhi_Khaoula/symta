Page 50298 "Shipping Agent Comment List"
{
    Caption = 'Commentaires transporteur';
    SourceTable = "Shipping Agent Comment Line";
    PageType = List;
    ApplicationArea = all;
    UsageCategory = Lists;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                ShowCaption = false;
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Beginning Date"; Rec."Beginning Date")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date de début field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Date de fin field.';
                }


                field(Department; Rec.Department)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Département field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
            }

        }
    }
}

