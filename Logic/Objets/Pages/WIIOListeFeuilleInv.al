page 50317 "WIIO - Liste Feuille Inv."
{
    // version WIIO,SFD20210929

    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Journal Batch";
    SourceTableView = SORTING("Journal Template Name", Name)
                      ORDER(Ascending)
                      WHERE("Journal Template Name" = CONST('INV. PHYS.'));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Nbre Ligne"; Rec."Nbre Ligne")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nbre Ligne field.';
                }
                field(Seuil; gSeuil)
                {
                    Caption = 'Seuil';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Seuil field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord();
    begin
        gInventorySetup.GET();
        gSeuil := gInventorySetup."Inventory Amount Level";
    end;

    var
        gInventorySetup: Record "Inventory Setup";

        gSeuil: Decimal;

}
