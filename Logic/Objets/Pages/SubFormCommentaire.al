page 50046 "Sub Form Commentaire"
{
    AutoSplitKey = true;
    Caption = 'Commentaires';
    CardPageID = "Sub Form Commentaire";
    DelayedInsert = true;
    PageType = ListPart;
    SourceTable = "Sales Comment Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater("Détail")
            {
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
            }
        }
    }

    actions
    {
    }
}

