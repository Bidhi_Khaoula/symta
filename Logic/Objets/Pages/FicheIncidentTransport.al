page 50048 "Fiche Incident Transport"
{

    CardPageID = "Fiche Incident Transport";
    PageType = Document;
    SourceTable = "Incident transport";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Document Date"; Rec."Document Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Date field.';
                }
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(GetCustNomCustomerNo; Rec.GetCustNom(rec."Customer No."))
                {
                    Caption = 'Nom';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field(GetCustCPCustomerNo; Rec.GetCustCP(rec."Customer No."))
                {
                    Caption = 'Code Postal';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Postal field.';
                }
                field(GetCustVilleCustomerNo; Rec.GetCustVille(rec."Customer No."))
                {
                    Caption = 'Ville';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ville field.';
                }
                field("Order No."; Rec."Order No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order No. field.';
                }
                field("Sales Shipment No."; Rec."Sales Shipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° expédition vente field.';
                }
                field("External Document No."; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field("Contact No."; Rec."Contact No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Contact No. field.';
                }
                field(Contact; Rec.Contact)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Contact field.';
                }
                field("Phone No."; Rec."Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Téléphone field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field(GetDateBLSalesShipmentNo; Rec.GetDateBL(rec."Sales Shipment No."))
                {
                    Caption = 'Date';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Status; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Create User ID"; Rec."Create User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur Création field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Create Time"; Rec."Create Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Création field.';
                }
                field("Traité par"; Rec."Traité par")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traité par field.';
                }
                field("Traité le"; Rec."Traité le")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traité le field.';
                }
                field("Traité à"; Rec."Traité à")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traité à field.';
                }
            }
            part("Détail"; "Sub Form Commentaire")
            {
                SubPageLink = "No." = FIELD("No.");
                SubPageView = SORTING("Document Type", "No.", "Document Line No.", "Line No.")
                              ORDER(Ascending)
                              WHERE("Document Type" = CONST("Incident transport"));
                ApplicationArea = All;
            }
        }
        area(factboxes)
        {
            part("Sales Hist. Sell-to FactBox"; "Sales Hist. Sell-to FactBox")
            {
                SubPageLink = "No." = FIELD("Customer No.");
                Visible = true;
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Fonction)
            {
                Caption = 'Fonction';
                action("Créer")
                {
                    Caption = 'Créer';
                    Image = NewDocument;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Créer action.';

                    trigger OnAction()
                    begin
                        Rec.Créer();
                    end;
                }
                action("Traité")
                {
                    Caption = 'Traité';
                    Image = ReleaseDoc;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Traité action.';

                    trigger OnAction()
                    begin
                        Rec.Traiter();
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                action(Cloturer)
                {
                    Caption = 'Cloturer';
                    Image = Closed;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Cloturer action.';

                    trigger OnAction()
                    begin
                        Rec.Cloturer();
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                action(Rouvrir)
                {
                    Caption = 'Rouvrir';
                    Image = ReOpen;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Rouvrir action.';

                    trigger OnAction()
                    begin
                        Rec.Rouvrir()
                    end;
                }
            }
            group(Therefore)
            {
                Caption = 'Therefore';
                // action(View)
                // {
                //     Caption = 'View';
                //     Image = View;
                //     ApplicationArea = All;

                //     trigger OnAction()
                //     var
                //         cThereforeFunctions: Codeunit "52101146";
                //     begin
                //         cThereforeFunctions.OpenDocumentList(cThereforeFunctions.GetTableID(Rec.TABLENAME), Rec."No.", 0D, 0, 0);
                //     end;
                // }
                // action(Add)
                // {
                //     Caption = 'Add';
                //     Image = PostDocument;
                //     ApplicationArea = All;

                //     trigger OnAction()
                //     var
                //         cThereforeFunctions: Codeunit "52101146";
                //     begin
                //         cThereforeFunctions.OpenAddDocument(cThereforeFunctions.GetTableID(Rec.TABLENAME), Rec."No.", 0D, 0, 0, '');
                //     end;
                // }
                // action(Scan)
                // {
                //     Caption = 'Scan';
                //     Image = PostPrint;
                //     ApplicationArea = All;

                //     trigger OnAction()
                //     var
                //         cThereforeFunctions: Codeunit "52101146";
                //     begin
                //         cThereforeFunctions.OpenScanDocument(cThereforeFunctions.GetTableID(Rec.TABLENAME), Rec."No.", 0D, 0, 0, '');
                //     end;
                // }
                // action("Show Navigator")
                // {
                //     Caption = 'Show Navigator';
                //     Image = View;
                //     ApplicationArea = All;

                //     trigger OnAction()
                //     var
                //         cThereforeFunctions: Codeunit "52101146";
                //     begin
                //         cThereforeFunctions.StartNavigator(cThereforeFunctions.GetTableID(Rec.TABLENAME), 0);
                //     end;
                // }
            }
        }
    }
}

