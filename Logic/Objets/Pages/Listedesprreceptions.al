page 50076 "Liste des pré-receptions"
{
    CardPageID = "Liste des pré-receptions";
    Editable = false;
    PageType = List;
    SourceTable = "Saisis Réception Magasin";

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Utilisateur Modif"; Rec."Utilisateur Modif")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur Modif field.';
                }
                field("Date Modif"; Rec."Date Modif")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modif field.';
                }
                field("Heure Modif"; Rec."Heure Modif")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Modif field.';
                }
            }
        }
    }

    actions
    {
    }
}

