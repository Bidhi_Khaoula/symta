page 50200 "Purchase Price Deletion"
{
    ApplicationArea = all;
    Caption = 'Purchase Price Deletion';
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = List;
    SourceTable = "Purchase Price";

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Item No."; Rec."Item No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor No. field.';
                }
                field("Currency Code"; Rec."Currency Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Currency Code field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Direct Unit Cost"; Rec."Direct Unit Cost")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Direct Unit Cost field.';
                }
                field("Minimum Quantity"; Rec."Minimum Quantity")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Minimum Quantity field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Référence Active"; Rec."Référence Active")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field("Prix net achat"; Rec."Prix net achat")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix net achat field.';
                }
                field("Vendor Item No."; Rec."Vendor Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Marked For Deletion"; Rec."Marked For Deletion")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Marqué pour suppression field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Computing)
            {
                Caption = 'Computing';
                action("report")
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Computing';
                    Image = ServiceSetup;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Computing';
                    trigger OnAction();
                    var
                        lDeletion: Report "Suppression prix achat";
                    begin
                        lDeletion.SETTABLEVIEW(Rec);
                        lDeletion.RUN();
                    end;
                }
                action(delete)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Computing';
                    Image = DeleteExpiredComponents;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Computing';

                    trigger OnAction();
                    var
                        lDeletion: Page "Item Vendor Deletion";
                    begin
                        lDeletion.RUN();
                    end;
                }
            }
        }
    }
}

