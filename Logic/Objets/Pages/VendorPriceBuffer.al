page 50026 "Vendor Price Buffer"
{

    Caption = 'Prix d''achat';
    CardPageID = "Vendor Price Buffer";
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = ListPart;
    SourceTable = "Vendor Price Buffer";
    SourceTableTemporary = true;
    SourceTableView = SORTING("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price")
                      ORDER(Ascending);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Général)
            {
                ShowCaption = false;
                repeater(Lines)
                {
                    field(VendorNoText; VendorNoText)
                    {
                        CaptionClass = Rec.FIELDCAPTION("Vendor No.");
                        Editable = false;
                        Style = None;
                        StyleExpr = "Vendor No.Emphasize";
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the VendorNoText field.';
                    }
                    field(Name; Rec.Name)
                    {
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Nom field.';
                    }
                    field("Ref.Fournisseur"; ref_fou)
                    {
                        Caption = 'Réf. fournisseur';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Réf. fournisseur field.';
                    }
                    field("Unit Price"; Rec."Unit Price")
                    {
                        Editable = false;
                        Enabled = true;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Prix Brut field.';
                    }
                    field("Code remise"; cod_rem)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the cod_rem field.';
                    }
                    field("Date début"; Rec."Date début")
                    {
                        Caption = 'Date Tarif';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Date Tarif field.';
                    }
                    field(Discount; Rec.Discount)
                    {
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the % Remise field.';
                    }
                    field("Statut Qualité"; Rec."Statut Qualité")
                    {
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Statut Qualité field.';
                    }
                    field("Statut Approvisionnement"; Rec."Statut Approvisionnement")
                    {
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Statut Approvisionnement field.';
                    }
                    field("Net Unit Price"; Rec."Net Unit Price")
                    {
                        Editable = false;
                        Style = Strong;
                        StyleExpr = TRUE;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Prix Net field.';
                    }
                    field("Profit %"; Rec."Profit %")
                    {
                        ToolTip = '(Prix_Vente - (Prix_ achat + Cout_indirect)) / Prix_Vente x 100';
                        Editable = false;
                        Style = Strong;
                        StyleExpr = TRUE;
                        ApplicationArea = All;
                    }
                    field("Marge Net"; marge_net)
                    {
                        Caption = '% de Marge Net';
                        ToolTip = '(Meilleur_Prix_Vente - (Prix_ achat + Cout_indirect)) / Meilleur_Prix_Vente x 100';
                        Style = Attention;
                        StyleExpr = "Marge NetEmphasize";
                        Editable = false;
                        ApplicationArea = All;
                    }
                    field(marge_four_pre; marge_four_pre)
                    {
                        Caption = 'Rapport four precedent';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Rapport four precedent field.';
                    }
                    field("Unité d'achat"; Rec."Unité d'achat")
                    {
                        Style = Attention;
                        StyleExpr = "Unité d'achatEmphasize";
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Unité d''achat field.';
                    }
                    field(MinimumorderText; MinimumorderText)
                    {
                        ToolTip = 'Minimum de commande de la fiche fournisseur/article';
                        // BlankNumbers = BlankZero;
                        // BlankZero = true;
                        CaptionClass = Rec.FIELDCAPTION("Minimum order");
                        Editable = false;
                        Style = Unfavorable;
                        StyleExpr = "Minimum orderEmphasize";
                        ApplicationArea = All;
                    }
                    field(ByHowManyText; ByHowManyText)
                    {
                        // BlankNumbers = BlankZero;
                        // BlankZero = true;
                        CaptionClass = Rec.FIELDCAPTION("By How Many");
                        Editable = false;
                        Style = Unfavorable;
                        StyleExpr = "By How ManyEmphasize";
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the ByHowManyText field.';
                    }
                    field("Quantité minimum TA"; Rec."Minimum Qty")
                    {
                        ToolTip = 'Quantité minimum du tarif achat';
                        Style = Unfavorable;
                        StyleExpr = MinimumQtyEmphasize;
                        ApplicationArea = All;
                    }
                    field(Quantity; Rec.Quantity)
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Quantité field.';
                    }
                    field(bln_Selected; Rec.Selected)
                    {
                        Visible = bln_SelectedVisible;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Séléctionner field.';

                        trigger OnValidate()
                        begin
                            //IF NOT Selected THEN ERROR('Séléctionnez un autre prix pour enlever celui ci'); // MCO Le 04-10-2018 => Régie
                            SelectedOnAfterValidate();
                        end;
                    }
                    field("Référence Fournisseur"; Rec."Référence Fournisseur")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Référence Fournisseur field.';
                    }
                    field("Vendor Item Desciption"; Rec."Vendor Item Desciption")
                    {
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Vendor Item No. field.';
                    }
                    field(GetNbPrice_; GetNbPrice())
                    {
                        Caption = 'Nb prix achat';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Nb prix achat field.';
                    }
                    field("Lead Time Calculation"; rec_itemvendor."Lead Time Calculation")
                    {
                        Caption = 'Délai de réappro.';
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Délai de réappro. field.';
                    }
                    field("Indirect Cost %"; Rec."Indirect Cost")
                    {
                        ApplicationArea = All;
                        StyleExpr = gStyleExp_IndirectCost;
                        ToolTip = 'Specifies the value of the Indirect Cost % field.';
                    }
                    field("Date Fin"; Rec."Date Fin")
                    {
                        ApplicationArea = All;
                        Visible = false;
                        ToolTip = 'Specifies the value of the Date Fin field.';
                    }
                    field(rem_cod; rem_cod)
                    {
                        Caption = '[Détail] meilleur code remise';
                        ApplicationArea = All;
                        Visible = false;
                        Editable = false;
                        ToolTip = 'Specifies the value of the [Détail] meilleur code remise field.';
                    }
                    field(rem_cli_max; rem_cli_max)
                    {
                        Caption = '[Détail] meilleur remise (si autorisée)';
                        ApplicationArea = All;
                        Visible = false;
                        Editable = false;
                        ToolTip = 'Specifies the value of the [Détail] meilleur remise (si autorisée) field.';
                    }
                    field(prix_vente; prix_vente)
                    {
                        ApplicationArea = All;
                        Caption = '[Détail] meilleur prix vente';
                        Visible = false;
                        Editable = false;
                        ToolTip = 'Specifies the value of the [Détail] meilleur prix vente field.';
                    }
                    field(px_vente_net; px_vente_net)
                    {
                        Caption = '[Détail] meilleur prix vente net';
                        ApplicationArea = All;
                        Visible = false;
                        Editable = false;
                        ToolTip = 'Specifies the value of the [Détail] meilleur prix vente net field.';
                    }
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Purcha&se Prices")
            {
                Caption = 'Purcha&se Prices';
                // Promoted = true;
                // PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Purcha&se Prices action.';

                trigger OnAction()
                var
                    rec_PurchPrice: Record "Purchase Price";
                    frm_PurchPrice: Page "Purchase Prices";
                begin
                    rec_PurchPrice.RESET();
                    rec_PurchPrice.SETRANGE("Item No.", Rec."Item No.");
                    rec_PurchPrice.SETRANGE("Vendor No.", Rec."Vendor No.");
                    rec_PurchPrice.SETRANGE("Starting Date", 0D, WORKDATE());
                    rec_PurchPrice.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
                    frm_PurchPrice.SETTABLEVIEW(rec_PurchPrice);
                    frm_PurchPrice.RUN();
                end;
            }
            action("Prix f&ournisseur")
            {
                Caption = 'Prix f&ournisseur';
                ApplicationArea = All;
                ToolTip = 'Executes the Prix f&ournisseur action.';

                trigger OnAction()
                var
                    recL_ItemVendor: Record "Item Vendor";
                    pgeL_ItemVendorCard: Page "Item Vendor Card";
                begin
                    recL_ItemVendor.RESET();
                    recL_ItemVendor.SETRANGE("Vendor No.", Rec."Vendor No.");
                    recL_ItemVendor.SETRANGE("Item No.", Rec."Item No.");
                    pgeL_ItemVendorCard.SETTABLEVIEW(recL_ItemVendor);
                    pgeL_ItemVendorCard.RUN();
                end;
            }
            action("Catalogue fournisseur")
            {
                Caption = 'Catalogue fournisseur';
                ApplicationArea = All;
                ToolTip = 'Executes the Catalogue fournisseur action.';

                trigger OnAction()
                var
                    recL_ItemVendor: Record "Item Vendor";
                    pgeL_ItemVendorCard: Page "Item Vendor Catalog";
                begin
                    recL_ItemVendor.RESET();
                    IF GItemNo = '' THEN
                        recL_ItemVendor.SETRANGE("Item No.", Rec."Item No.")
                    ELSE
                        recL_ItemVendor.SETRANGE("Item No.", GItemNo);
                    pgeL_ItemVendorCard.SETTABLEVIEW(recL_ItemVendor);
                    pgeL_ItemVendorCard.RUN();
                end;
            }
            action("Proposer sélection")
            {
                Caption = 'Proposer sélection';
                ApplicationArea = All;
                Visible = FALSE;
                Image = ApprovalSetup;
                ToolTip = 'Executes the Proposer sélection action.';
                trigger OnAction()
                begin
                    // CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e
                    SetSelectedByMinimumOrder();
                    // FIN CFR Le 13/04/2022
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    var
        LPxNet: Boolean;
    begin
        //LM le 27-07-2012 affichage ref fourni  (car err version de base en multi consult)
        rec_itemvendor.INIT();
        rec_itemvendor.SETRANGE(rec_itemvendor."Item No.", Rec."Item No.");
        rec_itemvendor.SETRANGE(rec_itemvendor."Vendor No.", Rec."Vendor No.");
        IF rec_itemvendor.FINDFIRST() THEN
            ref_fou := rec_itemvendor."Vendor Item No.";
        cod_rem := rec_itemvendor."Code Remise";

        // LM de 24-07-2012=>calcul meilleur prix net tous client et meilleure marge de vente net
        IF (px_vente_net = 0) THEN BEGIN
            lm_fonction.remise_cli_max(rec."Item No.", rem_cod, rem_cli_max);
            lm_fonction.prix_vente(rec."Item No.", prix_vente, LPxNet); // AD Le 06-02-2015 => Ajout du pxnet
            IF LPxNet THEN rem_cli_max := 0;
            px_vente_net := prix_vente * (100 - rem_cli_max) / 100;
        END;

        IF (px_vente_net <> 0) THEN BEGIN
            // CFR Le 27/09/2023 => R‚gie : Gestion [% de marge net]
            //marge_net := (px_vente_net - "Net Unit Price") / px_vente_net * 100
            Rec.CALCFIELDS("Indirect Cost");
            marge_net := Rec.CalcMarge(Rec."Net Unit Price", px_vente_net, Rec."Indirect Cost");
            // FIN CFR Le 27/09/2023
        END
        ELSE
            marge_net := 0;

        //LM le 26-11-2012 affiche marge par rapport au fournisseur suivant
        IF anc_ref = Rec."Item No." THEN
            IF prix_net_achat_ref <> 0 THEN
                marge_four_pre := (Rec."Net Unit Price" - prix_net_achat_ref) / prix_net_achat_ref * 100
            ELSE
                marge_four_pre := 0;

        IF marge_four_pre < 0 THEN marge_four_pre := 0;

        prix_net_achat_ref := Rec."Net Unit Price";
        anc_ref := Rec."Item No.";
        VendorNoText := FORMAT(Rec."Vendor No.");
        VendorNoTextOnFormat(VendorNoText);
        margenetOnFormat();
        Unit233d39achatOnFormat(FORMAT(Rec."Unité d'achat"));
        MinimumorderText := FORMAT(Rec."Minimum order");
        MinimumorderTextOnFormat(MinimumorderText);
        ByHowManyText := FORMAT(Rec."By How Many");
        ByHowManyTextOnFormat(ByHowManyText);
        // MCO Le 20-11-2018 => Copie colle du principe au dessus mais bien compliqué
        MinimumQtyText := FORMAT(Rec."Minimum Qty");
        MinimumQtyTextOnFormat(MinimumQtyText);
        // FIN MCO Le 20-11-2018

        // CFR le 25/10/2023 - R‚gie : [Couleur] sur [Co–t indirect]
        gStyleExp_IndirectCost := '';
        IF Rec."Indirect Cost" <> 0 THEN
            gStyleExp_IndirectCost := 'Unfavorable';
        // FIN CFR le 25/10/2023
    end;

    trigger OnInit()
    begin
        bln_SelectedVisible := TRUE;
        anc_ref := '';
        prix_net_achat_ref := 0;
    end;

    var
        tempBuffer: Record "Vendor Price Buffer" temporary;
        Error001: Label 'Pour enlever un fournisseur de la selection, il faut en selectionner un autre.';
        ref_fou: Text[30];
        rec_itemvendor: Record "Item Vendor";
        lm_fonction: Codeunit lm_fonction;
        rem_cod: Text[30];
        rem_cli_max: Decimal;
        prix_vente: Decimal;
        px_vente_net: Decimal;
        marge_net: Decimal;
        marge_four_pre: Decimal;
        prix_net_achat_ref: Decimal;
        anc_ref: Text[30];
        cod_rem: Text[30];
        "Vendor No.Emphasize": Text;
        VendorNoText: Text[1024];
        "Marge NetEmphasize": Boolean;
        "Unité d'achatEmphasize": Boolean;
        "Minimum orderEmphasize": Boolean;
        MinimumorderText: Text[1024];
        "By How ManyEmphasize": Boolean;
        ByHowManyText: Text[1024];
        bln_SelectedVisible: Boolean;
        GItemNo: Code[20];
        MinimumQtyEmphasize: Boolean;
        MinimumQtyText: Text[1024];
        gStyleExp_IndirectCost: Text;


    procedure InsertBufferRecord(p_ReqLine: Record "Requisition Line")
    begin
        px_vente_net := 0;

        Rec.DELETEALL();
        tempBuffer.DELETEALL();
        Rec.SetRecords(p_ReqLine, tempBuffer);

        IF tempBuffer.FINDFIRST() THEN
            REPEAT
                Rec := tempBuffer;
                IF NOT Rec.INSERT() THEN Rec.MODIFY();
            UNTIL tempBuffer.NEXT() = 0;

        IF Rec.FINDFIRST() THEN;

        // CFR Le 28/09/2023 => R‚gie : Bug de filtrage de l'action vers la page [Catalogue fournisseur]
        GItemNo := p_ReqLine."No.";
        // FIN CFR Le 28/09/2023

        CurrPage.UPDATE(FALSE);
    end;

    procedure InsertBufferRecordByMulti(ItemNo: Code[20]; QteDde: Decimal; VendorNo: Code[20]; TypeCde: Integer)
    begin
        px_vente_net := 0;

        // MC Le 19-07-2012 => On veut pouvoir gérer par type de commande donc paramètre en plus
        Rec.DELETEALL();
        tempBuffer.DELETEALL();

        GItemNo := ItemNo;
        IF ItemNo <> '' THEN // AD le10-12-2015
            Rec.SetRecordsByMulti(ItemNo, QteDde, VendorNo, tempBuffer, TypeCde, FALSE);

        IF tempBuffer.FINDFIRST() THEN
            REPEAT
                Rec := tempBuffer;
                IF NOT Rec.INSERT() THEN Rec.MODIFY();
            UNTIL tempBuffer.NEXT() = 0;

        IF Rec.FINDFIRST() THEN;

        CurrPage.UPDATE(FALSE);
    end;

    procedure HideSelection(bln_Visible: Boolean)
    begin
        bln_SelectedVisible := bln_Visible;
    end;



    procedure ShowLineDisc()
    begin
    end;

    procedure GetNbPrice() rtn_int: Integer
    var
        //PurchPriceCalcMgt: Codeunit "Purch. Price Calc. Mgt.";
        CduLFunctions: Codeunit "Codeunits Functions";
    begin
        EXIT(CduLFunctions.NoOfPurchPriceByItemVendor(Rec."Vendor No.", Rec."Item No.", '', '', '', WORKDATE(), TRUE));
    end;

    local procedure SelectedOnAfterValidate()
    var
        rec_ReqLine: Record "Requisition Line";
    begin
        // Mise à jour la ligne de demande d'achat et de la quantité de la ligne.
        IF Rec.Selected THEN BEGIN
            rec_ReqLine.RESET();
            rec_ReqLine.GET(Rec."Worksheet Template Name", Rec."Journal Batch Name", Rec."Line No.");
            rec_ReqLine.VALIDATE("Vendor No.", Rec."Vendor No.");

            //LM le 28-08-2012 =>corrige la stat appro
            rec_ReqLine.VALIDATE(rec_ReqLine."Direct Unit Cost", Rec."Net Unit Price");
            // MCO Le 04-10-2018 => Régie
            rec_ReqLine."Buffer Price Record ID" := Rec.RECORDID;
            rec_ReqLine."Price Qty" := Rec."Minimum Qty";
            rec_ReqLine."Price Selectionned" := TRUE;
            rec_ReqLine.MODIFY(TRUE);
            // MCO Le 04-10-2018 => Régie
            InsertBufferRecord(rec_ReqLine);
        END
        ELSE
            // MCO Le 24-03-2016 => Le message déconne quand on selectionne une ligne
            //MESSAGE(Error001);
            // FIN MCO Le 24-03-2016
            Rec.Selected := TRUE;

    end;

    local procedure VendorNoTextOnFormat(var Text: Text[1024])
    var
        rec_item: Record Item;
        rec_vendor: Record Vendor;
    begin
        "Vendor No.Emphasize" := '';
        // LM le 18-01-2013 =>affiche en rouge le fournisseur conseillé par l'optimisation appro  et le four privilégié en appro
        IF rec_item.GET(Rec."Item No.") THEN;
        IF Text = rec_item."Vendor No." THEN
            "Vendor No.Emphasize" := 'Attention';


        IF rec_vendor.GET(Rec."Vendor No.") THEN;
        IF rec_vendor.Privilège_appro THEN BEGIN
            "Vendor No.Emphasize" := 'Favorable';
        END;
    end;

    local procedure margenetOnFormat()
    begin
        "Marge NetEmphasize" := FALSE;
        IF marge_net < 0 THEN
            "Marge NetEmphasize" := TRUE;

    end;

    local procedure Unit233d39achatOnFormat(Text: Text[1024])
    var
    //  Litem: Record Item;
    begin
        /*
        IF Litem.GET("Item No.") THEN
          IF Litem."Base Unit of Measure" <> "Unité d'achat" THEN
            BEGIN
              CurrForm."Unité d'achat".UPDATEFORECOLOR(255);
              CurrForm."Unité d'achat".UPDATEFONTBOLD(TRUE);
            END;
        */
        "Unité d'achatEmphasize" := FALSE;
        IF (Text <> '1') AND (Text <> '0') THEN
            "Unité d'achatEmphasize" := TRUE;


    end;

    local procedure MinimumorderTextOnFormat(var Text: Text[1024])
    begin
        "Minimum orderEmphasize" := FALSE;
        IF (Text <> '0.00') AND (Text <> '1.00') AND (Text <> '0,00') AND (Text <> '1,00') AND (Text <> '0') AND (Text <> '1') THEN
            "Minimum orderEmphasize" := TRUE;

    end;

    local procedure ByHowManyTextOnFormat(var Text: Text[1024])
    begin
        "By How ManyEmphasize" := FALSE;
        IF (Text <> '0.00') AND (Text <> '1.00') AND (Text <> '0,00') AND (Text <> '1,00') AND (Text <> '0') AND (Text <> '1') THEN
            "By How ManyEmphasize" := TRUE;

    end;

    local procedure MinimumQtyTextOnFormat(var Text: Text[1024])
    begin
        MinimumQtyEmphasize := FALSE;
        IF (Text <> '0.00') AND (Text <> '1.00') AND (Text <> '0,00') AND (Text <> '1,00') AND (Text <> '0') AND (Text <> '1') THEN
            MinimumQtyEmphasize := TRUE;

    end;

    procedure GetFirstVendorNo() VendorNo: Code[20]
    begin
        // ANI Le 23-05-2016 FE20160617 : Meilleur fournisseur
        Rec.SETCURRENTKEY("Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price");
        Rec.ASCENDING := TRUE;
        VendorNo := '';
        IF Rec.FINDFIRST() THEN
            VendorNo := Rec."Vendor No.";
    end;

    procedure ESKUpdate()
    begin
        CurrPage.UPDATE(FALSE);
    end;

    PROCEDURE SetSelectedByMinimumOrder();
    VAR
        lRequisitionLine: Record "Requisition Line";
        lEventsAppro: Codeunit "Events Appro";
    BEGIN
        // CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e
        lRequisitionLine.RESET();
        // CFR Le 10/05/2022 => R‚gie : Manque le IF !!
        IF lRequisitionLine.GET(Rec."Worksheet Template Name", Rec."Journal Batch Name", Rec."Line No.") THEN
            lEventsAppro.SetSelectedByMinimumOrder(lRequisitionLine, Rec);
        // FIN CFR Le 13/04/2022
    END;

    // CFR le 16/03/2022 => FA20220203 : ajout champ 50 "Date d‚but" & 51 "Date fin"
    // CFR Le 13/04/2022 => R‚gie : S‚lectionner par d‚faut le tarif en fonction de la quantit‚ demand‚e
    // CFR Le 10/05/2022 => R‚gie : Manque le IF !!
    // CFR Le 27/09/2023 => R‚gie : Gestion [% de marge net]
    // CFR Le 28/09/2023 => R‚gie : Bug de filtrage de l'action vers la page [Catalogue fournisseur]
    // CFR le 25/10/2023 - R‚gie : [Couleur] sur [Co–t indirect]
}

