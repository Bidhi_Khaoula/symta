page 50202 "Sales Header"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Sales Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(CustomerNo; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(No; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Date; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field(Reference; Rec."External Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field(SalesStatus; Rec.Status)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Status field.';
                }
                field(ShipmentStatus; Rec."Shipment Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Livraison field.';
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field(AmountVTA; Rec."Amount Including VAT")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount Including VAT field.';
                }
                field(RespAssist; Rec.Responsable)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Responsable field.';
                }
                field(ContactName; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(ContactMail; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(ContactPhone; '')
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                part(Lines; "Sales Lines")
                {
                    SubPageLink = "Document No." = FIELD("No."),
                                  "Document Type" = FIELD("Document Type");
                    ApplicationArea = All;
                }
                part(Status; "Sales Status")
                {
                    SubPageLink = "Shipment Status" = FIELD("Shipment Status"),
                                  Status = FIELD(Status);
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
    }

    var
    /* strContactName: Text[200];
    strContactMail: Text[128];
    strContactTel: Text[20];
    test: Text[30]; */
}

