page 50063 "Ventes Manuelles"
{
    AutoSplitKey = true;
    CardPageID = "Ventes Manuelles";
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Ventes Manuelles";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Item No."; rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';

                    trigger OnValidate()
                    begin
                        IF Rec_Item.GET(rec."Item No.") THEN;
                    end;
                }
                field(Reference_Active; Rec_Item."No. 2")
                {
                    Caption = 'Référence active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active field.';
                }
                field("Posting Date"; rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Item Ledger Entry Type"; rec."Item Ledger Entry Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Ledger Entry Type field.';
                }
                field("Source No."; rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field(Description; rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Valued Quantity"; Rec."Valued Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valued Quantity field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        ItemNoOnFormat(FORMAT(rec."Item No."));
        IF Rec_Item.GET(rec."Item No.") THEN;
    end;

    var

        Rec_Item: Record Item;
        CUMultiRef: Codeunit "Gestion Multi-référence";

    local procedure ItemNoOnAfterInput(var Text: Text[1024])
    var
        C: Code[20];
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        EVALUATE(C, Text);
        Text := CUMultiRef.RechercheArticleByActive(C);
    end;

    local procedure ItemNoOnFormat(Text: Text[1024])
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;
}

