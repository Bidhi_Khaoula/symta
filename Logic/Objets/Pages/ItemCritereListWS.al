page 50254 "Item Critere List WS"
{
    Caption = 'Critères par article';
    PageType = List;
    SourceTable = "Article Critere";
    SourceTableView = SORTING("Code Article", Priorité);
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code Article"; Rec."Code Article")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Article field.';
                }
                field("Code Critere"; Rec."Code Critere")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Critere field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field(Priorité; Rec.Priorité)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Priorité field.';
                }
                field(Text; Rec.Text)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Text field.';
                }
                field(Booléen; Rec.Booléen)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Booléen field.';
                }
                field(Entier; Rec.Entier)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Entier field.';
                }
                field(Décimal; Rec.Décimal)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Décimal field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Initialiser)
            {
                ApplicationArea = All;
                ToolTip = 'Executes the Initialiser action.';

                trigger OnAction()
                var
                    LItem: Record Item;
                begin

                    LItem.GET(Rec."Code Article");
                    LItem.ReInitCriteria('');
                    CurrPage.UPDATE(FALSE);
                end;
            }
        }
    }

    trigger OnInit()
    begin

        BtnFiltreVariante := TRUE;
        FiltreVariante := Rec.GETFILTER("Variant Code");
    end;

    var
        FiltreVariante: Text[30];
        BtnFiltreVariante: Boolean;
}

