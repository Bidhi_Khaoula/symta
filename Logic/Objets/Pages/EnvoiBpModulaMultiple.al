page 50057 "Envoi Bp Modula Multiple"
{
    // // 25/05/2020 : WIIO => Ajout du 0 en dernier paramètre

    CardPageID = "Envoi Bp Modula";
    DelayedInsert = true;
    DeleteAllowed = false;
    PageType = List;
    SourceTable = "Warehouse Shipment Header";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';

                    trigger OnValidate()
                    var
                        WhseShipHeader: Record "Warehouse Shipment Header";
                    begin
                        WhseShipHeader.GET(Rec."No.");
                        Rec."Location Code" := WhseShipHeader."Location Code";
                        Rec."Bin Code" := WhseShipHeader."Bin Code";
                        Rec."Zone Code" := WhseShipHeader."Zone Code";
                        Rec."Destination Type" := WhseShipHeader."Destination Type";
                        Rec."Destination No." := WhseShipHeader."Destination No.";
                    end;
                }
                field("Location Code"; Rec."Location Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Destination Type"; Rec."Destination Type")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination Type field.';
                }
                field("Destination No."; Rec."Destination No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Destination No. field.';
                }
                field(GetName; Get_Name())
                {
                    Caption = 'Nom';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fonctions)
            {
                Caption = 'Fonctions';
                action("Génération")
                {
                    Caption = 'Génération';
                    Image = ConfirmAndPrint;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Génération action.';

                    trigger OnAction()
                    begin
                        ExportBp();
                    end;
                }
            }
        }
    }

    procedure Get_Name(): Text[100]
    var
        cust: Record Customer;
    begin
        CASE Rec."Destination Type" OF
            Rec."Destination Type"::Customer:
                IF cust.GET(Rec."Destination No.") THEN
                    EXIT(cust.Name);
        END;
    end;

    procedure ExportBp()
    var
        GestionModula: Codeunit "Gestion Bon Préparation";
    begin
        Rec.RESET();
        //GestionModula.ExportBlsModula(Rec); => AD Le 27-09-2016 => REGIE -> VD Plus utilisé
        GestionModula.ImprimeEtiquettePrepa(Rec, 0); // WIIO => Ajout du 0 en dernier paramètre
        Rec.DELETEALL();
        CurrPage.UPDATE(FALSE);
    end;
}

