page 50005 "Exlusions Comissions"
{
    CardPageID = "Exlusions Comissions";
    PageType = List;
    SourceTable = "Exclusions Comissions";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Salesperson Type"; Rec."Salesperson Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Vendeur field.';
                }
                field("Salesperson Code"; Rec."Salesperson Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Vendeur field.';
                }
                field("Sales Type"; Rec."Sales Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Vente field.';
                }
                field("Sales Code"; Rec."Sales Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Vente field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field("Comission %"; Rec."Comission %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Comission field.';
                }
            }
        }
    }
}

