page 50320 "Packing List List"
{
    Caption = 'Listes de colisage';
    CardPageID = "Packing List Header";
    DataCaptionFields = "No.";
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    RefreshOnActivate = true;
    SourceTable = "Packing List Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                Caption = 'Général';
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° field.';
                }
                field("Header Status"; Rec."Header Status")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field("Creation User"; Rec."Creation User")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur création field.';
                }
                field("Creation Date"; Rec."Creation Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date création field.';
                }
                field("Creation Time"; Rec."Creation Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure création field.';
                }
                field("Calculate Header Weight"; Rec."Calculate Header Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids brut total saisi field.';
                }
                field("Theoretical Weight"; Rec."Theoretical Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Theoretical Weight field.';
                }
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(GetDestinationName; Rec.GetDestinationName())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetDestinationName() field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Process)
            {
                Caption = 'Process';
                action("Imprimer liste par article")
                {
                    Caption = 'Imprimer liste par article';
                    Image = PrintCheck;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer liste par article action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."No.", TRUE, 'ARTICLE');
                    end;
                }
                action("Imprimer liste par colis")
                {
                    Caption = 'Imprimer liste par colis';
                    Image = PrintCover;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Imprimer liste par colis action.';

                    trigger OnAction()
                    begin
                        gPackingListMgmt.PrintPackingListItem(Rec."No.", TRUE, 'COLIS');
                    end;
                }
            }
        }
    }

    var
        gPackingListMgmt: Codeunit "Packing List Mgmt";
}

