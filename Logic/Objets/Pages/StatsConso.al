page 50083 "Stats Conso"
{
    CardPageID = "Stats Conso";
    Editable = false;
    PageType = List;
    SourceTable = "Stats Conso";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Item Ledger Entry Type"; Rec."Item Ledger Entry Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Ledger Entry Type field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Valued Quantity"; Rec."Valued Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valued Quantity field.';
                }
            }
        }
    }

    actions
    {
    }
}

