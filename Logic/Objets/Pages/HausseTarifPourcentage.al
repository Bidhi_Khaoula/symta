page 50023 "Hausse Tarif Pourcentage"
{
    CardPageID = "Hausse Tarif Pourcentage";
    PageType = Card;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(CodeFournisseurTarif; CodeFournisseur)
            {
                Caption = 'Fournisseur';
                TableRelation = Vendor;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Fournisseur field.';
            }
            field(Hausse; Hausse)
            {
                Caption = 'Hausse';
                ApplicationArea = All;
                AutoFormatType = 2;
                ToolTip = 'Specifies the value of the Hausse field.';
            }
            field(DateTarif; DateTarif)
            {
                Caption = 'Date Application';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Date Application field.';
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Valider)
            {
                Caption = 'Valider';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Valider action.';

                trigger OnAction()
                begin
                    CLEAR(GestionTabTar);
                    MAJTarif();
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        CodeFournisseur := '627510-FAC';

        DateTarif := WORKDATE();
    end;

    var
        GestionTabTar: Codeunit "Gestion des tarifs Fourn. Temp";
        CodeFournisseur: Code[20];
        Hausse: Decimal;
        DateTarif: Date;

    /*    Text19068807: Label 'HAUSSE EN POURCENTAGE TARIF';
       Text19015031: Label '%';
*/
    procedure MAJTarif()
    begin
        IF NOT CONFIRM('MAJ des Tarifs de %1 % pour le fournisseur %2 ?', FALSE, Hausse, CodeFournisseur) THEN EXIT;
        GestionTabTar.HausseEnPourcentage(CodeFournisseur, Hausse, DateTarif);
    end;

    // CFR le 11/05/2022 => R‚gie : Hausse [AutoFormatType = 2] >> when you want to format the data as a unit amount. (5 d‚cimales)
}

