page 50309 "WIIO - Liste Feuille Reappro"
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Journal Batch";
    SourceTableView = SORTING("Journal Template Name", Name)
                      ORDER(Ascending)
                      WHERE("Journal Template Name" = CONST('RECLASS'));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Nbre Ligne"; Rec."Nbre Ligne")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nbre Ligne field.';
                }
                field("Type Feuille Reclassement WIIO"; Rec."Type Feuille Reclassement WIIO")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Feuille Reclassement WIIO field.';
                }
            }
        }
    }

    actions
    {
    }
}

