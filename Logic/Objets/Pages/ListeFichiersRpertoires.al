page 50006 "Liste Fichiers / Répertoires"
{
    CardPageID = "Liste Fichiers / Répertoires";
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Liste Fichiers / Répertoires";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field(Path; Rec.Path)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Path field.';
                }
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Time; Rec.Time)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Time field.';
                }
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Ouvrir)
            {
                Caption = 'Ouvrir';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Ouvrir action.';

                trigger OnAction()
                begin
                    HYPERLINK(Rec.Path + '\' + Rec.Name);
                end;
            }
        }
    }

    trigger OnOpenPage()
    var
        Files: Record File;
        i: Integer;
    begin
        // Vide la table temporaire utilisée pour afficher les fichiers
        Rec.RESET();
        Rec.DELETEALL();
        i := 1;

        // Lecture des fichiers
        Files.RESET();


        Files.SETFILTER(Path, '%1', Lien);

        IF Filtre <> '' THEN
            Files.SETFILTER(Name, '%1', Filtre);

        Files.SETRANGE("Is a file", TRUE);

        IF Files.FINDFIRST() THEN
            REPEAT
                // Enregistrement de chaque fichier dans la table temporaire.
                Rec."No." := i;
                Rec.Path := Files.Path;
                Rec."Is a File" := Files."Is a file";
                Rec.Name := Files.Name;
                Rec.Size := Files.Size;
                Rec.Date := Files.Date;
                Rec.Time := Files.Time;
                Rec.Insert();
                i += 1;
            UNTIL Files.NEXT() = 0;
    end;

    var
        Lien: Text[1024];
        Filtre: Text[1024];

    procedure Initialisation()
    begin
    end;

    procedure InitialisationPhoto(Item: Record Item)
    begin
        Lien := '\\MACGRAPH\photos_emails\Photos\BD\';
        Filtre := Item."No." + '*-dt.jpg';
    end;

    procedure InitialisationFicheProduit(Item: Record Item)
    var
        Marque: Record Manufacturer;
    begin
        Lien := '\\srvtse\datas\graphisme\Fiches_produits\';

        Marque.GET(Item."Manufacturer Code");
        Lien += Marque.Code + '-' + Marque.Name + '\';

        Filtre := Item."No." + '*.pdf';
    end;

    procedure "InitialisationQuanlité"(Item: Record Item)
    begin
        Lien := '\\srvtse\datas\public\QUALITE\DOSSIERS QUALITE\Dossiers qualité\' + Item."No.";
        Filtre := '';
    end;

    procedure "InitialisationVuesEclatées"(Item: Record Item)
    begin
        Lien := '\\MACGRAPH\exploded_views\';
        Filtre := Item."No." + '*.pdf';
    end;

    procedure InitialisationArtworks(Item: Record Item)
    var
        num: Integer;
        Fourchette: Code[20];
    begin
        Lien := '\\MACGRAPH\partages\Documents Acrobat\service_graphisme\';

        EVALUATE(num, COPYSTR(Item."No.", 1, 3));
        IF (num >= 101) AND (num <= 111) THEN
            Fourchette := '101000-111000';

        IF (num >= 111) AND (num <= 114) THEN
            Fourchette := '111000-114000';

        IF (num >= 115) AND (num <= 116) THEN
            Fourchette := '115000-116000';

        IF (num >= 116) AND (num <= 200) THEN
            Fourchette := '116000-200000';

        IF (num >= 200) AND (num <= 300) THEN
            Fourchette := '200000-300000';

        IF (num >= 300) AND (num <= 400) THEN
            Fourchette := '300000-400000';

        IF (num >= 400) AND (num <= 600) THEN
            Fourchette := '400000-600000';

        IF (num >= 600) AND (num <= 800) THEN
            Fourchette := '600000-800000';

        IF (num >= 900) AND (num <= 1000) THEN
            Fourchette := '900000-1000000';

        Lien += Fourchette + '\' + Item."No.";
    end;

    procedure InitialisationPalette(Item: Record Item)
    begin
        Lien := '\\MACGRAPH\photos_emails\Logistique\Photos palettisation\';
        Filtre := '@' + Item."No." + '*.jpg';
    end;
}

