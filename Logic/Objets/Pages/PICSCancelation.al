page 50264 "PICS Cancelation"
{
    Caption = 'Annulation PICS';
    InsertAllowed = false;
    PageType = List;
    Permissions = TableData "Sales Shipment Header" = rimd;
    DeleteAllowed = false;
    ModifyAllowed = false;
    SourceTable = "PICS Cancelation";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; rec."WSH No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("WSH Source No."; rec."WSH Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("WSH Source Document"; rec."WSH Source Document")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Document field.';
                }
                field("WSH Assigned User ID"; rec."WSH Assigned User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field("WSH Assignment Date"; rec."WSH Assignment Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assignment Date field.';
                }
                field("WSH Assignment Time"; rec."WSH Assignment Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assignment Time field.';
                }
                field("SH Ship-to Name"; rec."SH Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer Name 2 field.';
                }
                field("SH Ship-to City"; rec."SH Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to City field.';
                }
                field("SH Ship-to Post Code"; rec."SH Ship-to Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Post Code field.';
                }
                field("Cancel User ID"; rec."Cancel User ID")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Assigned User ID field.';
                }
                field("Cancel Date"; rec."Cancel Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date annulation field.';
                }
                field("Cancel Time"; rec."Cancel Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure annulation field.';
                }
                field("Cancel DateTime"; rec."Cancel DateTime")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date et Heure d''annulation field.';
                }
            }
        }
    }

    actions
    {
    }
}

