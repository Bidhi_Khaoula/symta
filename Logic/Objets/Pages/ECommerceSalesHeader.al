page 50232 "ECommerce Sales Header"
{
    PageType = List;
    SourceTable = "Sales Header";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(SearchDocNo; Rec."No.")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. field.';
            }
            field(SearchDocType; Rec."Document Type")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Document Type field.';
            }
            repeater(Group)
            {
                field(Type_Document; Rec."Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document Type field.';
                }
                field(Code_Client; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field(No_Cde; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Ref_Cde_Client; Rec."External Document No.")
                {
                    Caption = 'External Document No.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the External Document No. field.';
                }
                field(Transport_Information; Rec."Transport Information")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Info. transport field.';
                }
                field(Type_Cde; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field(Date_Livraison_Demandee; Rec."Requested Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Delivery Date field.';
                }
                field(Date_Livraison_Confirmee; Rec."Promised Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Promised Delivery Date field.';
                }
                field(Mode_Expedition; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field(Livraison_Samedi; Rec."Saturday Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Livraison le samedi field.';
                }
                field(Web; Rec."Devis Web")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Devis Web field.';
                }
                field(Date_Integration; Rec."EDI Integration Date")
                {
                    Editable = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date d''intégration EDI field.';
                }
                field(Heure_Integration; Rec."EDI Integration Time")
                {
                    Editable = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure d''intégration EDI field.';
                }
                field(Abandon_reliquat; Rec."Abandon remainder")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Abandon reliquat field.';
                }
                field("Shipment Status"; Rec."Shipment Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut Livraison field.';
                }
                field("Source Document Type"; Rec."Source Document Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Origine Document field.';
                }
                field("Order Date"; Rec."Order Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Order Date field.';
                }
                field("Create Time"; Rec."Create Time")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Création field.';
                }
                field("Create Date"; Rec."Create Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field(WorkDescription; WorkDescription)
                {
                    ShowCaption = false;
                    Importance = Additional;
                    ApplicationArea = All;
                    MultiLine = true;
                    ToolTip = 'Specifies the products or service being offered;', comment = 'FRA=Spécifie les produits ou services offerts;';
                    trigger OnValidate()
                    begin
                        Rec.SetWorkDescription(WorkDescription);
                    end;
                }
                field("Port Charge Client"; Rec."Port Charge Client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Port Charge Client field.';
                }

                field("Enlevement Charge Client"; Rec."Enlevement Charge Client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Enlevement Charge Client field.';
                }
                field(Amount; Rec.Amount)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field(Weight; Rec.Weight)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Ship-to Address"; Rec."Ship-to Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address field.';
                }
                field("Ship-to Address 2"; Rec."Ship-to Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address 2 field.';
                }
                field("Ship-to City"; Rec."Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to City field.';
                }
                field("Saturday Delivery"; Rec."Saturday Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Livraison le samedi field.';
                }
                field("Do Not Print Active Ref"; Rec."Do Not Print Active Ref")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Do Not Print Active Ref field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnInsertRecord(BelowxRec: Boolean): Boolean
    begin
        // DZA Le 01/06/2012 - Type Source WEB par défaut lors de l'appel de cette page
        Rec.VALIDATE("Source Document Type", 'WEB');
        // Fin DZA Le 01/06/2012
    end;

    trigger OnAfterGetRecord()
    begin
        WorkDescription := Rec.GetWorkDescription();
    end;

    var
        WorkDescription: Text;
}

