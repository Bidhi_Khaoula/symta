page 50001 "Invoicing Date"
{
    ApplicationArea = all;
    PageType = StandardDialog;
    Caption = 'Saisir Date facture (JJMMAA) :';
    layout
    {
        area(Content)
        {
            field(date_fac; date_fac)
            {
                ApplicationArea = All;
                Caption = 'Date Facture : ';
                ToolTip = 'Specifies the value of the Date Facture :  field.';
            }
        }
    }

    procedure FctGetInvDate(): date
    var
        LErrMgtLbl: Label 'Invoicing Date must be filled.';
    begin
        if date_fac = 0D then
            Error(LErrMgtLbl);
        exit(date_fac);
    end;

    var
        date_fac: Date;
}