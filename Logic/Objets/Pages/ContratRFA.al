page 50014 "Contrat RFA"
{
    AutoSplitKey = true;
    CardPageID = "Contrat RFA";
    DelayedInsert = true;
    PageType = List;
    SourceTable = "Contrat RFA";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(FiltreCodeContrat; FiltreCodeContrat)
            {
                Caption = 'Filtre Contrat';
                TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_CONTRAT_RFA'));
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Filtre Contrat field.';

                trigger OnValidate()
                begin
                    // MIG 2015 - GR - FORM
                    //FiltreCodeContratOnAfterValidate;
                end;
            }
            repeater(Lines)
            {
                field("Code Contrat RFA"; Rec."Code Contrat RFA")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Contrat RFA field.';
                }
                field("Type Ligne"; Rec."Type Ligne")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Ligne field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
            }
        }
    }
    var
        FiltreCodeContrat: Code[20];

    procedure SetRecFilters()
    begin
        Rec.SETRANGE("Code Contrat RFA", FiltreCodeContrat);

        CurrPage.UPDATE(FALSE);
    end;

    local procedure FiltreCodeContratOnAfterValida()
    begin
        CurrPage.SAVERECORD();
        SetRecFilters();
    end;
}

