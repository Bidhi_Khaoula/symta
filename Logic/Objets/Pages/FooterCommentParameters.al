page 50068 "Footer Comment Parameters"
{
    AutoSplitKey = true;
    Caption = 'Footer Comment Parameters';
    CardPageID = "Footer Comment Parameters";
    PageType = List;
    SourceTable = "Sales Comment Line";
    SourceTableView = WHERE("Document Type" = CONST("Sales Documents"));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(Comment; Rec.Comment)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment field.';
                }
                field("Print Wharehouse Shipment"; Rec."Print Wharehouse Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Impression exp. magasin field.';
                }
                field("Print Shipment"; Rec."Print Shipment")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Impression expédition field.';
                }
                field("Print Invoice"; Rec."Print Invoice")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Impression facture field.';
                }
                field("Print Order"; Rec."Print Order")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Impression commande/Retour field.';
                }
                field("Print Quote"; Rec."Print Quote")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Impression devis field.';
                }
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field("End Date"; Rec."End Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Fin field.';
                }
            }
        }
    }

    actions
    {
    }
}

