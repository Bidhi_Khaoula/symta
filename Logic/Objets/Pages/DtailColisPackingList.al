page 50031 "Détail Colis Packing List"
{
    CardPageID = "Détail Colis Packing List";
    PageType = Card;
    SourceTable = "Détail Packing List";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("N° expedition magasin"; Rec."N° expedition magasin")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° expedition magasin field.';
                }
                field(Length; Rec.Length)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Length field.';
                }
                field(Width; Rec.Width)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Width field.';
                }
                field(Height; Rec.Height)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Height field.';
                }
                field(Cubage; Rec.Cubage)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cubage field.';
                }
                field("Comment 1"; Rec."Comment 1")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire 1 field.';
                }
                field("Comment 2"; Rec."Comment 2")
                {
                    Caption = 'Commentaire 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire 2 field.';
                }
                field("Numero de Colis"; Rec."Numero de Colis")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero de Colis field.';
                }
                field(Weight; Rec.Weight)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Weight field.';
                }
                field("Pack Nb"; Rec."Pack Nb")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nb Colis field.';
                }
                field("Hors CE"; Rec."Hors CE")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hors CE field.';
                }
            }
        }
    }
}

