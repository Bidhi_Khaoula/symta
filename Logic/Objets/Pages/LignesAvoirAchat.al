page 50256 "Lignes Avoir Achat"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Purch. Cr. Memo Line";
    SourceTableView = SORTING("Buy-from Vendor No.")
                      ORDER(Ascending)
                      WHERE(Type = CONST(Item));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Buy-from Vendor No."; Rec."Buy-from Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Buy-from Vendor No. field.';
                }
                field(GetVendorName_; GetVendorName())
                {
                    Caption = 'Nom fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom fournisseur field.';
                }
                field(GetItemVendorCodeRemise_; GetItemVendorCodeRemise())
                {
                    Caption = 'Code remise fournisseur article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code remise fournisseur article field.';
                }
                field("Pay-to Vendor No."; Rec."Pay-to Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pay-to Vendor No. field.';
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(GetItemFamilyCode_; GetItemFamilyCode())
                {
                    ApplicationArea = All;
                    Caption = 'Famille article';
                    ToolTip = 'Specifies the value of the Famille article field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("Reference No."; Rec."Item Reference No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Reference No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Unit of Measure"; Rec."Unit of Measure")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure field.';
                }
                field("Direct Unit Cost"; Rec."Direct Unit Cost")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Direct Unit Cost field.';
                }
                field("Discount1 %"; Rec."Discount1 %")
                {
                    Caption = 'Rem 1 / Cde';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Rem 1 / Cde field.';
                }
                field("Discount2 %"; Rec."Discount2 %")
                {
                    Caption = 'Rem 2 / Cde';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Rem 2 / Cde field.';
                }
                field("Line Discount %"; Rec."Line Discount %")
                {
                    Caption = 'Line Discount %';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Discount % field.';
                }
                field(Amount; Rec.Amount)
                {
                    Caption = 'Amount';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Amount field.';
                }
                field("Item Category Code"; Rec."Item Category Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Category Code field.';
                }
                field(Stocké; Rec.Stocké)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field("Manufacturer Code"; Rec."Manufacturer Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Manufacturer Code field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field("Vendor Order No."; Rec."Vendor Order No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Order No. field.';
                }
                field("Document date"; recHeader."Document Date")
                {
                    Caption = 'Date document';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date document field.';
                }
                field("Code devise"; recHeader."Currency Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Currency Code field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        recHeader.GET(Rec."Document No.");
    end;

    var
        recHeader: Record "Purch. Cr. Memo Hdr.";

    LOCAL PROCEDURE GetVendorName(): Text[100];
    VAR
        lVendor: Record Vendor;
    BEGIN
        // CFR le 11/05/2022 => R‚gie : Ajout info fournisseur & article
        IF lVendor.GET(Rec."Buy-from Vendor No.") THEN
            EXIT(lVendor.Name)
        ELSE
            EXIT('');
    END;

    LOCAL PROCEDURE GetItemFamilyCode(): Code[20];
    VAR
        lItem: Record Item;
    BEGIN
        // CFR le 11/05/2022 => R‚gie : Ajout info fournisseur & article
        IF (Rec.Type = Rec.Type::Item) AND lItem.GET(Rec."No.") THEN
            EXIT(lItem."Item Category Code")
        ELSE
            EXIT('');
    END;

    LOCAL PROCEDURE GetItemVendorCodeRemise(): Code[20];
    VAR
        lItemVendor: Record "Item Vendor";
    BEGIN
        // CFR le 11/05/2022 => R‚gie : Ajout info fournisseur & article
        lItemVendor.SETRANGE("Vendor No.", Rec."Buy-from Vendor No.");
        lItemVendor.SETRANGE("Item No.", Rec."No.");
        IF lItemVendor.FINDFIRST() THEN
            EXIT(lItemVendor."Code Remise")
        ELSE
            EXIT('');
    END;

    // CFR le 11/05/2022 => R‚gie : Ajout info fournisseur & article
}

