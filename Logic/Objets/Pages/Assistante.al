page 50205 Assistante
{
    PageType = List;
    SourceTable = "User Setup";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(eMail; Rec."E-Mail")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the E-Mail field.';
                }
                field(Telephone; Rec."Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Phone No. field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        //strName := '';
        //IF rec_User.GET(Rec."User ID") THEN strName := rec_User.Name;
    end;

    var
        strName: Text[100];
}

