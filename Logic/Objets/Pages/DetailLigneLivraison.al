page 50034 "Detail Ligne Livraison"
{
    CardPageID = "Detail Ligne Livraison";
    Editable = false;
    PageType = List;
    SourceTable = "Sales Line";
    SourceTableView = SORTING("Document Type", "Document No.", "Line No.")
                      WHERE(Type = CONST(Item));

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field("A Traiter en préparation"; Rec."A Traiter en préparation")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the A Traiter en préparation field.';
                }
                field("Net Weight"; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
                field("Unit Volume"; Rec."Unit Volume")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit Volume field.';
                }
                field("Line Amount"; Rec."Line Amount")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line Amount field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    var
        init_ligne: Record "Sales Line";
    begin
        init_ligne.RESET();
        init_ligne.SETRANGE("Document Type", init_ligne."Document Type"::Order);
        init_ligne.SETRANGE("Document No.", Rec.GETFILTER("Document No."));
        IF init_ligne.FINDFIRST() THEN
            REPEAT
                init_ligne.MajDelivry(init_ligne);
            UNTIL init_ligne.NEXT() = 0;
    end;
}

