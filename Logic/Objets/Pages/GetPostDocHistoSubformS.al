page 50096 "Get Post.Doc - Histo Subform S"
{
    CardPageID = "Get Post.Doc - Histo Subform S";
    Editable = false;
    PageType = ListPart;
    SourceTable = "Value Entry";
    SourceTableView = SORTING("Source Type", "Source No.", "Item No.", "Posting Date", "Entry Type", Adjustment)
                      ORDER(Ascending)
                      WHERE("Récupération Historiques" = CONST(True),
                            "Document Type" = CONST("Sales Invoice"));

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Posting Date"; Rec."Posting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Posting Date field.';
                }
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("No Commande Histo."; Rec."No Commande Histo.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Commande Histo. field.';
                }
                field("No Livraion Histo."; Rec."No Livraion Histo.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Livraion Histo. field.';
                }
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Valued Quantity"; Rec."Valued Quantity" * -1)
                {
                    Caption = 'Quantité valorisée';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité valorisée field.';
                }
                field("Qte Retour Histo."; Rec."Qte Retour Histo.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qte Retour Histo. field.';
                }
                field("Reste à Retourné"; Rec."Reste à Retourné")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reste à Retourné field.';
                }
                field("Cost per Unit"; Rec."Cost per Unit")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cost per Unit field.';
                }
                field("Sales Amount (Actual)"; Rec."Sales Amount (Actual)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Amount (Actual) field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        ItemNoOnFormat(FORMAT(Rec."Item No."));
    end;

    var
        ToSalesHeader: Record "Sales Header";
        /*    QtyNotReturned: Decimal;
           RevUnitCostLCY: Decimal; */
        RevQtyFilter: Boolean;
        FillExactCostReverse: Boolean;
        Visible: Boolean;
        // "--- GVarESKAPE ---": Integer;
        CUMultiRef: Codeunit "Gestion Multi-référence";

    local procedure CalcQtyReturned(): Decimal
    begin
        //IF (Type = Type::Item) AND (Quantity - QtyNotReturned > 0) THEN
        //  EXIT(Quantity - QtyNotReturned);
        //EXIT(0);
    end;

    procedure Initialize(NewToSalesHeader: Record "Sales Header"; NewRevQtyFilter: Boolean; NewFillExactCostReverse: Boolean; NewVisible: Boolean)
    begin
        ToSalesHeader := NewToSalesHeader;
        RevQtyFilter := NewRevQtyFilter;
        FillExactCostReverse := NewFillExactCostReverse;
        Visible := NewVisible;
    end;

    procedure GetSelectedLine(var FromValueEntry: Record "Value Entry")
    begin
        FromValueEntry.COPY(Rec);
        CurrPage.SETSELECTIONFILTER(FromValueEntry);
    end;

    local procedure ItemNoOnAfterInput(var Text: Text[1024])
    var
        C: Code[20];
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        EVALUATE(C, Text);
        Text := CUMultiRef.RechercheArticleByActive(C);
    end;

    local procedure ItemNoOnFormat(Text: Text[1024])
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;
}

