page 50229 "Teliae UM Detail"
{
    AutoSplitKey = true;
    Caption = 'Teliae UM Detail';
    DelayedInsert = true;
    DeleteAllowed = false;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = ListPart;
    Permissions = TableData "Sales Shipment Header" = rimd;
    SourceTable = "Teliae UM Detail";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Reference UM"; Rec."Reference UM")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence colis field.';
                }
                field("No. EEE"; Rec."No. EEE")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° EEE field.';
                }
                field("No. BP"; Rec."No. BP")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° BP field.';
                }
                field("Weight UM (Kg)"; Rec."Weight UM (Kg)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids colis (kg) field.';

                    trigger OnValidate();
                    var
                        lWarehouseShipmentHeader: Record "Warehouse Shipment Header";
                        lTeliaeUMDetail: Record "Teliae UM Detail";
                        lSalesShipmentHeader: Record "Sales Shipment Header";
                        lNewWeight: Decimal;

                    begin
                        // CFR le 14/01/2021 - Le poids du BP est désormais calculé à partir de la somme des poids des colis
                        IF lWarehouseShipmentHeader.GET(Rec."No. BP") THEN BEGIN
                            lTeliaeUMDetail.SETRANGE("No. BP", Rec."No. BP");
                            lTeliaeUMDetail.CALCSUMS("Weight UM (Kg)");
                            lNewWeight := lTeliaeUMDetail."Weight UM (Kg)" + Rec."Weight UM (Kg)" - xRec."Weight UM (Kg)";
                            lWarehouseShipmentHeader.VALIDATE(Poids, lNewWeight);
                            lWarehouseShipmentHeader.MODIFY();
                        END;

                        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
                        lSalesShipmentHeader.RESET();
                        lSalesShipmentHeader.SETRANGE("Preparation No.", Rec."No. BP");
                        IF lSalesShipmentHeader.COUNT() > 1 THEN
                            lSalesShipmentHeader.SETRANGE("Regroupement Centrale", FALSE);
                        IF lSalesShipmentHeader.FINDFIRST() THEN BEGIN
                            lTeliaeUMDetail.SETRANGE("No. BP", Rec."No. BP");
                            lTeliaeUMDetail.CALCSUMS("Weight UM (Kg)");
                            lNewWeight := lTeliaeUMDetail."Weight UM (Kg)" + Rec."Weight UM (Kg)" - xRec."Weight UM (Kg)";
                            lSalesShipmentHeader.VALIDATE(Weight, lNewWeight);
                            lSalesShipmentHeader.VALIDATE("Nb Of Box", lTeliaeUMDetail.COUNT());
                            lSalesShipmentHeader.MODIFY();
                            //CurrPage.UPDATE(TRUE);
                        END;
                    end;
                }
                field("Length UM (cm)"; Rec."Length UM (cm)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Length UM (cm) field.';
                }
                field("Width UM (cm)"; Rec."Width UM (cm)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Width UM (cm) field.';
                }
                field("Height UM (cm)"; Rec."Height UM (cm)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Height UM (cm) field.';
                }
                field("Volume UM (m3)"; Rec."Volume UM (m3)")
                {
                    Editable = false;
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Volume colis (m3) field.';
                }
                field("Type UM"; Rec."Type UM")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Colis field.';
                }
                field("Information UM"; Rec."Information UM")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Information colis field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure CalcSumWeight(pNoBP: Code[20]): Decimal;
    var
        lTeliaeUMDetail: Record "Teliae UM Detail";
    begin
        // CFR 28/09/2022 - FE20190930 V4 : Détail des colis
        lTeliaeUMDetail.SETRANGE("No. BP", Rec."No. BP");
        lTeliaeUMDetail.CALCSUMS("Weight UM (Kg)");
        EXIT(lTeliaeUMDetail."Weight UM (Kg)");
    end;
}

