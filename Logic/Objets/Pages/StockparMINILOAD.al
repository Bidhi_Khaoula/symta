page 50087 "Stock par MINILOAD"
{
    CardPageID = "Stock par MINILOAD";
    Editable = false;
    PageType = Card;
    SourceTable = "Message receive from  MINILOAD";
    SourceTableView = SORTING("Entry No.")
                      ORDER(Ascending)
                      WHERE("Message No." = FILTER('STO*'));

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(content1)
            {
                ShowCaption = false;
                field("Location Code"; rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Magasin field.';
                }
                field(Code; rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Quantity; rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
                field(Date; rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Heure; rec.Heure)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Hour field.';
                }
            }
        }
    }

    actions
    {
    }
}

