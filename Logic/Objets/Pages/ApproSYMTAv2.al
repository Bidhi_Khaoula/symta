page 50025 "Appro. SYMTA v2"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = true;
    PageType = Card;
    RefreshOnActivate = true;
    SaveValues = true;
    ShowFilter = false;
    SourceTable = "Requisition Line";
    SourceTableView = SORTING("Fournisseur Calculé", "Ref. Active")
                      ORDER(Ascending);

    layout
    {
        area(content)
        {
            group(Feuille)
            {
                field(CurrentJnlBatchName; CurrentJnlBatchName)
                {
                    Caption = 'Name';
                    Lookup = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        CurrPage.SAVERECORD();
                        ReqJnlManagement.LookupName(CurrentJnlBatchName, Rec);
                        CurrPage.UPDATE(FALSE);
                    end;

                    trigger OnValidate()
                    begin
                        ReqJnlManagement.CheckName(CurrentJnlBatchName, Rec);
                        CurrentJnlBatchNameOnAfterVali();
                    end;
                }
            }
            group("Général")
            {
                Caption = 'Général';
                field("No."; Rec."No.")
                {
                    TableRelation = Item;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(CUMultiRefRechercheRefActiveNo; CUMultiRef.RechercheRefActive(Rec."No."))
                {
                    Caption = 'Ref. Active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field("Type de commande"; Rec."Type de commande")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';

                    trigger OnValidate()
                    begin
                        TypedecommandeOnAfterValidate();
                    end;
                }
                field("Accept Action Message"; Rec."Accept Action Message")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Accept Action Message field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    Caption = 'Quantity';
                    DecimalPlaces = 2 : 2;
                    Style = Attention;
                    StyleExpr = StyleQuantity;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';

                    trigger OnValidate()
                    begin

                        // AD Le 30-01-2020 => REGIE -> Il y a avit un FALSE mais du coup pose pb sur les modifications des autres champs
                        // voir dev dans events du qte. Du coup je mets un TRUE. Mais je ne sais pas si d'autres impacts
                        //CurrPage.UPDATE(FALSE);
                        CurrPage.UPDATE(TRUE);
                    end;
                }
                field(StockATerme; Item.Inventory - _Reservé + _Attendu)
                {
                    Caption = 'Stock A Terme';
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock A Terme field.';
                }
                field(Stock; Item.Inventory)
                {
                    Caption = 'Stock';
                    DecimalPlaces = 2 : 2;
                    Editable = false;
                    Style = StrongAccent;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock field.';
                }
                field("<_Attendu>"; _Attendu)
                {
                    Caption = 'Attendu Global';
                    Editable = false;
                    Style = StrongAccent;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Attendu Global field.';

                    trigger OnDrillDown()
                    var
                        ligne_achat: Record "Purchase Line";
                        frm_achat: Page "Purchase Lines";
                    begin
                        ligne_achat.RESET();
                        ligne_achat.SETRANGE(ligne_achat."No.", Rec."No.");
                        ligne_achat.SETRANGE("Document Type", ligne_achat."Document Type"::Order);
                        ligne_achat.SETFILTER(ligne_achat."Outstanding Quantity", '>%1', 0);

                        frm_achat.EDITABLE(FALSE);
                        frm_achat.SETTABLEVIEW(ligne_achat);
                        frm_achat.RUNMODAL();
                    end;
                }
                field(_Reservé; _Reservé)
                {
                    Caption = 'Réservé Global';
                    Editable = false;
                    Style = StrongAccent;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Réservé Global field.';

                    trigger OnDrillDown()
                    var
                        ligne_vente: Record "Sales Line";
                        frm_vente: Page "Sales Lines";
                    begin
                        ligne_vente.RESET();
                        ligne_vente.SETRANGE(ligne_vente."No.", Rec."No.");
                        ligne_vente.SETRANGE("Document Type", ligne_vente."Document Type"::Order);
                        ligne_vente.SETFILTER(ligne_vente."Outstanding Quantity", '>%1', 0);

                        frm_vente.EDITABLE(FALSE);
                        frm_vente.SETTABLEVIEW(ligne_vente);
                        frm_vente.RUNMODAL();
                    end;
                }
                field("Retours Achats"; Item."Qty. on Purchase Return")
                {
                    BlankZero = true;
                    Caption = 'Retours Achats';
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Retours Achats field.';
                }
                field("Retours Ventes"; Item."Qty. on Sales Return")
                {
                    BlankZero = true;
                    Caption = 'Retours Ventes';
                    Editable = false;
                    Style = Favorable;
                    StyleExpr = StyleQteRetourVente;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Retours Ventes field.';
                }
                field("Marque"; Item."Manufacturer Code")
                {
                    Caption = 'Marque';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Marque field.';
                }
                field("Fusion en attente sur"; recitem."No. 2")
                {
                    Caption = 'Fusion en attente sur';
                    Editable = false;
                    Style = Unfavorable;
                    StyleExpr = FusionEnAttenteFormat;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fusion en attente sur field.';
                }
                field(px_vente_net; px_vente_net)
                {
                    Caption = 'Px vente net (rem. maxi)';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Px vente net (rem. maxi) field.';
                }
                field(CoefVente; coef_vente)
                {
                    Caption = 'Coef. Vente';
                    Editable = false;
                    Enabled = true;
                    Style = Unfavorable;
                    StyleExpr = StyleCoefVente;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coef. Vente field.';
                }
                field("Conso Mois En Cours"; Rec."Conso Mois En Cours")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Conso Mois En Cours field.';
                }
                field("Mini Encours"; Rec."Mini Encours")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mini Encours field.';
                }
                field(Stocké; Item.Stocké)
                {
                    Caption = 'Stocké';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field("Create Date"; Item."Create Date")
                {
                    Caption = 'Date création fiche';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date création fiche field.';
                }
                field("Date Dernière Sortie"; Item."Date Dernière Sortie")
                {
                    Caption = 'Date dernière Sortie';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dernière Sortie field.';
                }
                field("Date dernière entrée"; Item."Date dernière entrée")
                {
                    Caption = 'Date dernière Entrée';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dernière Entrée field.';
                }
                field(Pagination; Pagination)
                {
                    Caption = 'Pagination';
                    // DecimalPlaces = 0 : 2;
                    Editable = false;
                    ShowCaption = false;
                    ApplicationArea = All;
                }
                field("Code Appro"; Rec."Code Appro")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
                group(grp)
                {
                    ShowCaption = false;
                    fixed(fix)
                    {
                        ShowCaption = false;
                        group(N)
                        {
                            Caption = 'N';
                            field(Conso_1_1; TabConso[1] [1])
                            {
                                BlankZero = true;
                                Caption = 'Janvier';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_1;
                                ApplicationArea = All;
                            }
                            field(Conso_1_2; TabConso[1] [2])
                            {
                                BlankZero = true;
                                Caption = 'Février';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_2;
                                ApplicationArea = All;
                            }
                            field(Conso_1_3; TabConso[1] [3])
                            {
                                BlankZero = true;
                                Caption = 'Mars';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_3;
                                ApplicationArea = All;
                            }
                            field(Conso_1_4; TabConso[1] [4])
                            {
                                BlankZero = true;
                                Caption = 'Avril';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_4;
                                ApplicationArea = All;
                            }
                            field(Conso_1_5; TabConso[1] [5])
                            {
                                BlankZero = true;
                                Caption = 'Mai';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_5;
                                ApplicationArea = All;
                            }
                            field(Conso_1_6; TabConso[1] [6])
                            {
                                BlankZero = true;
                                Caption = 'Juin';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_6;
                                ApplicationArea = All;
                            }
                            field(Conso_1_7; TabConso[1] [7])
                            {
                                BlankZero = true;
                                Caption = 'Juillet';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_7;
                                ApplicationArea = All;
                            }
                            field(Conso_1_8; TabConso[1] [8])
                            {
                                BlankZero = true;
                                Caption = 'Août';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_8;
                                ApplicationArea = All;
                            }
                            field(Conso_1_9; TabConso[1] [9])
                            {
                                BlankZero = true;
                                Caption = 'Septembre';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_9;
                                ApplicationArea = All;
                            }
                            field(Conso_1_10; TabConso[1] [10])
                            {
                                BlankZero = true;
                                Caption = 'Octobre';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_10;
                                ApplicationArea = All;
                            }
                            field(Conso_1_11; TabConso[1] [11])
                            {
                                BlankZero = true;
                                Caption = 'Novembre';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_11;
                                ApplicationArea = All;
                            }
                            field(Conso_1_12; TabConso[1] [12])
                            {
                                BlankZero = true;
                                Caption = 'Décembre';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                ShowCaption = false;
                                Style = Attention;
                                StyleExpr = StyleConso_1_12;
                                ApplicationArea = All;
                            }
                            field(TabTotalConso_1; TabTotalConso[1])
                            {
                                Caption = 'Total';
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Standard;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Total field.';
                            }
                        }
                        group("N-1")
                        {
                            Caption = 'N-1';
                            field(Conso_2_1; TabConso[2] [1])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_1;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [1] field.';
                            }
                            field(Conso_2_2; TabConso[2] [2])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_2;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [2] field.';
                            }
                            field(Conso_2_3; TabConso[2] [3])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_3;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [3] field.';
                            }
                            field(Conso_2_4; TabConso[2] [4])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_4;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [4] field.';
                            }
                            field(Conso_2_5; TabConso[2] [5])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_5;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [5] field.';
                            }
                            field(Conso_2_6; TabConso[2] [6])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_6;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [6] field.';
                            }
                            field(Conso_2_7; TabConso[2] [7])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_7;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [7] field.';
                            }
                            field(Conso_2_8; TabConso[2] [8])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_8;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [8] field.';
                            }
                            field(Conso_2_9; TabConso[2] [9])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_9;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [9] field.';
                            }
                            field(Conso_2_10; TabConso[2] [10])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_10;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [10] field.';
                            }
                            field(Conso_2_11; TabConso[2] [11])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_11;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [11] field.';
                            }
                            field(Conso_2_12; TabConso[2] [12])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_2_12;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[2] [12] field.';
                            }
                            field(TabTotalConso_2; TabTotalConso[2])
                            {
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Standard;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabTotalConso[2] field.';
                            }
                        }
                        group("N-2")
                        {
                            Caption = 'N-2';
                            field(Conso_3_1; TabConso[3] [1])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_1;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [1] field.';
                            }
                            field(Conso_3_2; TabConso[3] [2])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_2;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [2] field.';
                            }
                            field(Conso_3_3; TabConso[3] [3])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_3;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [3] field.';
                            }
                            field(Conso_3_4; TabConso[3] [4])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_4;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [4] field.';
                            }
                            field(Conso_3_5; TabConso[3] [5])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_5;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [5] field.';
                            }
                            field(Conso_3_6; TabConso[3] [6])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_6;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [6] field.';
                            }
                            field(Conso_3_7; TabConso[3] [7])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_7;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [7] field.';
                            }
                            field(Conso_3_8; TabConso[3] [8])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_8;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [8] field.';
                            }
                            field(Conso_3_9; TabConso[3] [9])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_9;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [9] field.';
                            }
                            field(Conso_3_10; TabConso[3] [10])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_10;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [10] field.';
                            }
                            field(Conso_3_11; TabConso[3] [11])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_11;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [11] field.';
                            }
                            field(Conso_3_12; TabConso[3] [12])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_3_12;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[3] [12] field.';
                            }
                            field(TabTotalConso_3; TabTotalConso[3])
                            {
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Standard;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabTotalConso[3] field.';
                            }
                        }
                        group("N-3")
                        {
                            Caption = 'N-3';
                            field(Conso_4_1; TabConso[4] [1])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_1;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [1] field.';
                            }
                            field(Conso_4_2; TabConso[4] [2])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_2;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [2] field.';
                            }
                            field(Conso_4_3; TabConso[4] [3])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_3;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [3] field.';
                            }
                            field(Conso_4_4; TabConso[4] [4])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_4;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [4] field.';
                            }
                            field(Conso_4_5; TabConso[4] [5])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_5;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [5] field.';
                            }
                            field(Conso_4_6; TabConso[4] [6])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_6;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [6] field.';
                            }
                            field(Conso_4_7; TabConso[4] [7])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_7;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [7] field.';
                            }
                            field(Conso_4_8; TabConso[4] [8])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_8;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [8] field.';
                            }
                            field(Conso_4_9; TabConso[4] [9])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_9;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [9] field.';
                            }
                            field(Conso_4_10; TabConso[4] [10])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_10;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [10] field.';
                            }
                            field(Conso_4_11; TabConso[4] [11])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_11;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [11] field.';
                            }
                            field(Conso_4_12; TabConso[4] [12])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_4_12;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[4] [12] field.';
                            }
                            field(TabTotalConso_4; TabTotalConso[4])
                            {
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Standard;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabTotalConso[4] field.';
                            }
                        }
                        group("N-4")
                        {
                            Caption = 'N-4';
                            field(Conso_5_1; TabConso[5] [1])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_1;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [1] field.';
                            }
                            field(Conso_5_2; TabConso[5] [2])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_2;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [2] field.';
                            }
                            field(Conso_5_3; TabConso[5] [3])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_3;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [3] field.';
                            }
                            field(Conso_5_4; TabConso[5] [4])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_4;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [4] field.';
                            }
                            field(Conso_5_5; TabConso[5] [5])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_5;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [5] field.';
                            }
                            field(Conso_5_6; TabConso[5] [6])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_6;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [6] field.';
                            }
                            field(Conso_5_7; TabConso[5] [7])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_7;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [7] field.';
                            }
                            field(Conso_5_8; TabConso[5] [8])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_8;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [8] field.';
                            }
                            field(Conso_5_9; TabConso[5] [9])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_9;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [9] field.';
                            }
                            field(Conso_5_10; TabConso[5] [10])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_10;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [10] field.';
                            }
                            field(Conso_5_11; TabConso[5] [11])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_11;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [11] field.';
                            }
                            field(Conso_5_12; TabConso[5] [12])
                            {
                                BlankZero = true;
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Attention;
                                StyleExpr = StyleConso_5_12;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabConso[5] [12] field.';
                            }
                            field(TabTotalConso_5; TabTotalConso[5])
                            {
                                DecimalPlaces = 0 : 0;
                                Editable = false;
                                Style = Standard;
                                StyleExpr = TRUE;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the TabTotalConso[5] field.';
                            }
                        }
                    }
                }
            }
            part(frm_BufferEquivalence; "Equivalence Appro")
            {
                SubPageLink = "No." = FIELD("No.");
                ApplicationArea = All;
            }
            part(frm_BufferPrice; "Vendor Price Buffer")
            {
                UpdatePropagation = Both;
                ApplicationArea = All;
            }
        }
        area(factboxes)
        {
            part("Appro. SYMTA Stk Factbox"; 50039)
            {
                Caption = 'Stock';
                SubPageLink = "No." = FIELD("No.");
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("Réappro")
            {
                Caption = 'Réappro';
                action(Statistiques)
                {
                    Caption = 'Statistiques';
                    ShortCutKey = 'F7';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Statistiques action.';

                    trigger OnAction()
                    var
                        frm_Stat: Page "Stats Demande fournisseurs";
                        Selection: Integer;
                        blnLineFilter: Boolean;
                        dec_LineFilter: Decimal;
                    begin
                        Selection := STRMENU(text000Qst, 1);
                        IF Selection = 0 THEN
                            EXIT;
                        blnLineFilter := Selection IN [2];


                        // Si on filtre sur les lignes alors on se positionne sur l'enregistrement précédent.
                        IF blnLineFilter THEN
                            dec_LineFilter := rec."Indice Jauge"

                        ELSE
                            dec_LineFilter := 0;
                        // Instancie et ouvre le formulaire des statistiques.
                        CLEAR(frm_Stat);
                        frm_Stat.DeleteRecords();
                        frm_Stat.InsertRecords(rec."Worksheet Template Name", rec."Journal Batch Name", dec_LineFilter);
                        frm_Stat.RUN();
                    end;
                }
            }
        }
        area(processing)
        {
            action("Multi-Consultation")
            {
                Caption = 'Multi-Consultation';
                Promoted = true;
                PromotedCategory = Process;
                ShortCutKey = 'Ctrl+M';
                ApplicationArea = All;
                ToolTip = 'Executes the Multi-Consultation action.';

                trigger OnAction()
                var
                    LFrmQuid: Page "Multi -consultation";
                begin
                    //FrmQuid.InitClient(SalesLine."Sell-to Customer No.");
                    LFrmQuid.InitArticle(rec."No.");
                    LFrmQuid.RUNMODAL();
                end;
            }
            action(Ventes)
            {
                Caption = 'Ventes';
                Promoted = true;
                PromotedCategory = Process;
                ShortCutKey = 'Ctrl+V';
                ApplicationArea = All;
                ToolTip = 'Executes the Ventes action.';

                trigger OnAction()
                var
                    lItem: Record Item;
                    RecEcrituresArticles: Record "Item Ledger Entry";
                    FrmEcrituresArticle: Page "Item Ledger Entries_lm";
                begin

                    // CFR le 16/12/2020 - Régie : Accès direct aux écritures ventes
                    IF NOT lItem.GET(Rec."No.") THEN
                        ERROR('Vous devez spécifier au préalable un article.');

                    RecEcrituresArticles.RESET();
                    IF Rec."Location Code" <> '' THEN
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", Rec."Location Code");

                    RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", Rec."No.");
                    RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Entry Type", RecEcrituresArticles."Entry Type"::Sale);

                    CLEAR(FrmEcrituresArticle);
                    FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                    FrmEcrituresArticle.RUNMODAL();
                end;
            }
            action(Suivant)
            {
                ShortCutKey = 'F4';
                ApplicationArea = All;
                ToolTip = 'Executes the Suivant action.';

                trigger OnAction()
                begin
                    Rec.NEXT(1);
                end;
            }
        }
    }

    trigger OnAfterGetCurrRecord()
    var
        LItemvendor: Record "Item Vendor";
        // LUser: Record User;
        commentline: Record "Comment Line";
        lm_fonction: Codeunit lm_fonction;
        FrmCommentLine: Page "Comment Sheet";
        // px_vente: Decimal;
        rem_cod: Text[30];
        rem_cli_max: Decimal;
        prix_vente: Decimal;
        LpxNet: Boolean;
    begin
        CalculConso.CalculerConsoSur4ans(rec."No.", TabConso, TabTotalConso, WORKDATE());

        GetColor2();

        CurrPage.frm_BufferPrice.PAGE.InsertBufferRecord(Rec);
        CurrPage.frm_BufferPrice.PAGE.ESKUpdate();
        //CurrPage.frm_BufferPrice.PAGE.UPDATE;

        Item.GET(rec."No.");
        Item.CALCFIELDS("Date Dernière Sortie");

        Item.CalcAttenduReserve(_Reservé, _Attendu);

        // AD Le 01-04-2015
        Item.CALCFIELDS("Qty. on Purchase Return");
        // FIN AD Le 01-04-2015

        // AD Le 28-09-2016 => REGIE
        Item.CALCFIELDS("Qty. on Sales Return");
        StyleQteRetourVente := Item."Qty. on Sales Return" <> 0;

        LItemvendor.RESET();
        LItemvendor.SETRANGE("Item No.", rec."No.");
        LItemvendor.SETRANGE("Statut Qualité", LItemvendor."Statut Qualité"::"Contrôle en cours");
        IF LItemvendor.FINDFIRST() THEN
            MESSAGE(ESK001Msg, LItemvendor."Vendor No.");

        IF NOT Item.Stocké THEN
            MESSAGE(ESK002Msg);

        IF (Item."Reorder Point" <> 0) OR (Item."Maximum Inventory" <> 0) THEN
            MESSAGE(ESK003Msg, Item."Reorder Point", Item."Maximum Inventory");

        // LM le 23-07-2012 =>ref ctive de fusion SP
        FusionEnAttenteFormat := FALSE;
        recitem.INIT();
        IF recitem.GET(Item."Fusion en attente sur") THEN FusionEnAttenteFormat := TRUE;

        // LM de 24-07-2012=>retourne la meilleure remise tous clients confondus, le prix de vente TOUS CLIENT et calcul le prix net
        lm_fonction.remise_cli_max(rec."No.", rem_cod, rem_cli_max);
        lm_fonction.prix_vente(rec."No.", prix_vente, LpxNet); // AD Le 06-02-2015 => Ajout du px net
        IF LpxNet THEN rem_cli_max := 0; // AD Le 06-02-2015
        lm_fonction.coef_vente(rec."No.", coef_vente);
        px_vente_net := prix_vente * (100 - rem_cli_max) / 100;

        // LM Le 09-08-2012 => Affichage des commentaires appro
        commentline.RESET();
        //CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
        commentline.SETRANGE("No.", rec."No.");
        commentline.SETRANGE("Afficher Appro", TRUE);
        IF commentline.COUNT <> 0 THEN BEGIN
            CLEAR(FrmCommentLine);
            FrmCommentLine.SETTABLEVIEW(commentline);
            FrmCommentLine.EDITABLE(FALSE);
            FrmCommentLine.RUNMODAL();
        END;
        // FIN LM le 09-08-2012

        // LM Le 18-04-2013
        Item.CALCFIELDS("Assembly BOM");
        IF Item."Assembly BOM" THEN
            MESSAGE(ESK004Msg, Item."No. 2");


        QuantityOnFormat();
        coefventeOnFormat();

        Pagination := FORMAT(rec."Indice Jauge" / 10000) + '/' + FORMAT(rec.COUNT);


        //MESSAGE('test');
    end;

    trigger OnAfterGetRecord()
    var
    // LItemvendor: Record "Item Vendor";
    // px_vente: Decimal;
    // lm_fonction: Codeunit lm_fonction;
    // rem_cod: Text[30];
    // rem_cli_max: Decimal;
    // prix_vente: Decimal;
    // commentline: Record "Comment Line";
    // LpxNet: Boolean;
    // FrmCommentLine: Page "Comment Sheet";
    // LUser: Record User;
    begin
        // CalculConso.CalculerConsoSur4ans("No.",TabConso,TabTotalConso,WORKDATE);
        //
        // GetColor2;
        //
        // CurrPage.frm_BufferPrice.PAGE.InsertBufferRecord(Rec);
        // CurrPage.frm_BufferPrice.PAGE.ESKUpdate;
        // //CurrPage.frm_BufferPrice.PAGE.UPDATE;
        //
        // Item.GET("No.");
        // Item.CALCFIELDS("Date Dernière Sortie");
        //
        // Item.CalcAttenduReserve(_Reservé, _Attendu);
        //
        // // AD Le 01-04-2015
        // Item.CALCFIELDS("Qty. on Purchase Return");
        // // FIN AD Le 01-04-2015
        //
        // // AD Le 28-09-2016 => REGIE
        // Item.CALCFIELDS("Qty. on Sales Return");
        // StyleQteRetourVente := Item."Qty. on Sales Return" <> 0;
        //
        // LItemvendor.RESET();
        // LItemvendor.SETRANGE("Item No.", "No.");
        // LItemvendor.SETRANGE("Statut Qualité", LItemvendor."Statut Qualité"::"Contrôle en cours");
        // IF LItemvendor.FINDFIRST () THEN
        //  MESSAGE(ESK001, LItemvendor."Vendor No.");
        //
        // IF NOT Item.Stocké THEN
        //  MESSAGE(ESK002);
        //
        // IF (Item."Reorder Point" <> 0) OR (Item."Maximum Inventory" <> 0) THEN
        //  MESSAGE(ESK003, Item."Reorder Point", Item."Maximum Inventory");
        //
        // // LM le 23-07-2012 =>ref ctive de fusion SP
        // FusionEnAttenteFormat := FALSE;
        // recitem.INIT();
        // IF recitem.GET(Item."Fusion en attente sur") THEN FusionEnAttenteFormat := TRUE;
        //
        // // LM de 24-07-2012=>retourne la meilleure remise tous clients confondus, le prix de vente TOUS CLIENT et calcul le prix net
        // lm_fonction.remise_cli_max("No.",rem_cod,rem_cli_max);
        // lm_fonction.prix_vente("No.",prix_vente, LpxNet); // AD Le 06-02-2015 => Ajout du px net
        // IF LpxNet THEN rem_cli_max := 0; // AD Le 06-02-2015
        // lm_fonction.coef_vente("No.",coef_vente);
        // px_vente_net:=prix_vente*(100-rem_cli_max)/100;
        //
        // // LM Le 09-08-2012 => Affichage des commentaires appro
        // commentline.RESET();
        // //CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
        // commentline.SETRANGE("No.","No.");
        // commentline.SETRANGE("Afficher Appro", TRUE);
        // IF commentline.COUNT <> 0 THEN
        //  BEGIN
        //    CLEAR(FrmCommentLine);
        //    FrmCommentLine.SETTABLEVIEW(commentline);
        //    FrmCommentLine.EDITABLE(FALSE);
        //    FrmCommentLine.RUNMODAL();
        //  END;
        // // FIN LM le 09-08-2012
        //
        // // LM Le 18-04-2013
        // Item.CALCFIELDS("Assembly BOM");
        // IF Item."Assembly BOM" THEN
        //  MESSAGE(ESK004, Item."No. 2");
        //
        // QuantityOnFormat;
        // coefventeOnFormat;
        //
        // Pagination := FORMAT("Indice Jauge"/10000) + '/' + FORMAT(COUNT);


        //MESSAGE('test');
    end;

    trigger OnOpenPage()
    var
        JnlSelected: Boolean;
    begin
        OpenedFromBatch := (rec."Journal Batch Name" <> '') AND (rec."Worksheet Template Name" = '');
        IF OpenedFromBatch THEN BEGIN
            CurrentJnlBatchName := rec."Journal Batch Name";
            ReqJnlManagement.OpenJnl(CurrentJnlBatchName, Rec);
            EXIT;
        END;
        ReqJnlManagement.WkshTemplateSelection(PAGE::"Req. Worksheet", FALSE, "Req. Worksheet Template Type".FromInteger(0), Rec, JnlSelected);
        IF NOT JnlSelected THEN
            ERROR('');
        ReqJnlManagement.OpenJnl(CurrentJnlBatchName, Rec);

        SetDatz();
    end;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    var
        LUser: Record User;
    begin
        // MCO Le 18-01-2016 => Gestion du dernier numéro traité
        CLEAR(LUser);
        LUser.SETRANGE("User Name", USERID);
        //todo
        // IF LUser.FINDFIRST () THEN BEGIN
        // LUser."Dernière Ref. Appro" := rec."No.";
        // LUser.MODIFY();
        // END;

        // FIN MCO Le 18-01-2016 => Gestion du dernier numéro traité
    end;

    var
        recitem: Record Item;
        Item: Record Item;
        CalculConso: Codeunit CalculConso;
        CUMultiRef: Codeunit "Gestion Multi-référence";
        ReqJnlManagement: Codeunit ReqJnlManagement;
        TabConso: array[5, 12] of Decimal;
        TabTotalConso: array[5] of Decimal;
        text000Qst: Label '&Toutes les lignes, &Jusque ligne en cours.';
        "_Reservé": Decimal;
        _Attendu: Decimal;
        ESK001Msg: Label 'Il existe au moins un fournisseur [%1] avec statut qualité à contrôle en cours !', Comment = '%1 = fournisseur';
        ESK002Msg: Label '-- ATTENTION -- Article Non Stocké';
        ESK003Msg: Label 'Point de commande %1 \ Stock Maximum %2', Comment = '%1 = Point de commande ;%2 = Stock maximum';
        px_vente_net: Decimal;
        ESK004Msg: Label 'La référence %1 est un kit!', Comment = '%1 = N° Article';
        coef_vente: Decimal;
        // Text19015056: Label 'N';
        // Text19034574: Label 'N - 1';
        // Text19034575: Label 'N - 2';
        // Text19034576: Label 'N - 3';
        // Text19034569: Label 'N - 4';
        StyleConso_0_1: Boolean;
        StyleConso_0_2: Boolean;
        StyleConso_0_3: Boolean;
        StyleConso_0_4: Boolean;
        StyleConso_0_5: Boolean;
        StyleConso_0_6: Boolean;
        StyleConso_0_7: Boolean;
        StyleConso_0_8: Boolean;
        StyleConso_0_9: Boolean;
        StyleConso_0_10: Boolean;
        StyleConso_0_11: Boolean;
        StyleConso_0_12: Boolean;
        StyleConso_1_1: Boolean;
        StyleConso_1_2: Boolean;
        StyleConso_1_3: Boolean;
        StyleConso_1_4: Boolean;
        StyleConso_1_5: Boolean;
        StyleConso_1_6: Boolean;
        StyleConso_1_7: Boolean;
        StyleConso_1_8: Boolean;
        StyleConso_1_9: Boolean;
        StyleConso_1_10: Boolean;
        StyleConso_1_11: Boolean;
        StyleConso_1_12: Boolean;
        StyleConso_2_1: Boolean;
        StyleConso_2_2: Boolean;
        StyleConso_2_3: Boolean;
        StyleConso_2_4: Boolean;
        StyleConso_2_5: Boolean;
        StyleConso_2_6: Boolean;
        StyleConso_2_7: Boolean;
        StyleConso_2_8: Boolean;
        StyleConso_2_9: Boolean;
        StyleConso_2_10: Boolean;
        StyleConso_2_11: Boolean;
        StyleConso_2_12: Boolean;
        StyleConso_3_1: Boolean;
        StyleConso_3_2: Boolean;
        StyleConso_3_3: Boolean;
        StyleConso_3_4: Boolean;
        StyleConso_3_5: Boolean;
        StyleConso_3_6: Boolean;
        StyleConso_3_7: Boolean;
        StyleConso_3_8: Boolean;
        StyleConso_3_9: Boolean;
        StyleConso_3_10: Boolean;
        StyleConso_3_11: Boolean;
        StyleConso_3_12: Boolean;
        StyleConso_4_1: Boolean;
        StyleConso_4_2: Boolean;
        StyleConso_4_3: Boolean;
        StyleConso_4_4: Boolean;
        StyleConso_4_5: Boolean;
        StyleConso_4_6: Boolean;
        StyleConso_4_7: Boolean;
        StyleConso_4_8: Boolean;
        StyleConso_4_9: Boolean;
        StyleConso_4_10: Boolean;
        StyleConso_4_11: Boolean;
        StyleConso_4_12: Boolean;
        StyleConso_5_1: Boolean;
        StyleConso_5_2: Boolean;
        StyleConso_5_3: Boolean;
        StyleConso_5_4: Boolean;
        StyleConso_5_5: Boolean;
        StyleConso_5_6: Boolean;
        StyleConso_5_7: Boolean;
        StyleConso_5_8: Boolean;
        StyleConso_5_9: Boolean;
        StyleConso_5_10: Boolean;
        StyleConso_5_11: Boolean;
        StyleConso_5_12: Boolean;
        StyleQuantity: Boolean;
        StyleCoefVente: Boolean;
        Pagination: Text[250];
        FusionEnAttenteFormat: Boolean;
        StyleQteRetourVente: Boolean;
        CurrentJnlBatchName: Code[10];
        OpenedFromBatch: Boolean;

    procedure GetColor(Annee: Integer; Mois: Integer): Integer
    begin
        IF COPYSTR(rec."Chaine Mois", Mois, 1) = 'O' THEN
            EXIT(255)
        ELSE
            EXIT(1);
    end;

    local procedure GetColor2()
    begin
        StyleConso_0_1 := FALSE;
        StyleConso_0_2 := FALSE;
        StyleConso_0_3 := FALSE;
        StyleConso_0_4 := FALSE;
        StyleConso_0_5 := FALSE;
        StyleConso_0_6 := FALSE;
        StyleConso_0_7 := FALSE;
        StyleConso_0_8 := FALSE;
        StyleConso_0_9 := FALSE;
        StyleConso_0_10 := FALSE;
        StyleConso_0_11 := FALSE;
        StyleConso_0_12 := FALSE;

        StyleConso_1_1 := FALSE;
        StyleConso_1_2 := FALSE;
        StyleConso_1_3 := FALSE;
        StyleConso_1_4 := FALSE;
        StyleConso_1_5 := FALSE;
        StyleConso_1_6 := FALSE;
        StyleConso_1_7 := FALSE;
        StyleConso_1_8 := FALSE;
        StyleConso_1_9 := FALSE;
        StyleConso_1_10 := FALSE;
        StyleConso_1_11 := FALSE;
        StyleConso_1_12 := FALSE;

        StyleConso_2_1 := FALSE;
        StyleConso_2_2 := FALSE;
        StyleConso_2_3 := FALSE;
        StyleConso_2_4 := FALSE;
        StyleConso_2_5 := FALSE;
        StyleConso_2_6 := FALSE;
        StyleConso_2_7 := FALSE;
        StyleConso_2_8 := FALSE;
        StyleConso_2_9 := FALSE;
        StyleConso_2_10 := FALSE;
        StyleConso_2_11 := FALSE;
        StyleConso_2_12 := FALSE;

        StyleConso_3_1 := FALSE;
        StyleConso_3_2 := FALSE;
        StyleConso_3_3 := FALSE;
        StyleConso_3_4 := FALSE;
        StyleConso_3_5 := FALSE;
        StyleConso_3_6 := FALSE;
        StyleConso_3_7 := FALSE;
        StyleConso_3_8 := FALSE;
        StyleConso_3_9 := FALSE;
        StyleConso_3_10 := FALSE;
        StyleConso_3_11 := FALSE;
        StyleConso_3_12 := FALSE;

        StyleConso_3_1 := FALSE;
        StyleConso_3_2 := FALSE;
        StyleConso_3_3 := FALSE;
        StyleConso_3_4 := FALSE;
        StyleConso_3_5 := FALSE;
        StyleConso_3_6 := FALSE;
        StyleConso_3_7 := FALSE;
        StyleConso_3_8 := FALSE;
        StyleConso_3_9 := FALSE;
        StyleConso_3_10 := FALSE;
        StyleConso_3_11 := FALSE;
        StyleConso_3_12 := FALSE;

        StyleConso_4_1 := FALSE;
        StyleConso_4_2 := FALSE;
        StyleConso_4_3 := FALSE;
        StyleConso_4_4 := FALSE;
        StyleConso_4_5 := FALSE;
        StyleConso_4_6 := FALSE;
        StyleConso_4_7 := FALSE;
        StyleConso_4_8 := FALSE;
        StyleConso_4_9 := FALSE;
        StyleConso_4_10 := FALSE;
        StyleConso_4_11 := FALSE;
        StyleConso_4_12 := FALSE;

        StyleConso_5_1 := FALSE;
        StyleConso_5_2 := FALSE;
        StyleConso_5_3 := FALSE;
        StyleConso_5_4 := FALSE;
        StyleConso_5_5 := FALSE;
        StyleConso_5_6 := FALSE;
        StyleConso_5_7 := FALSE;
        StyleConso_5_8 := FALSE;
        StyleConso_5_9 := FALSE;
        StyleConso_5_10 := FALSE;
        StyleConso_5_11 := FALSE;
        StyleConso_5_12 := FALSE;

        IF COPYSTR(Rec."Chaine Mois", 1, 1) = 'O' THEN BEGIN
            StyleConso_0_1 := TRUE;
            StyleConso_1_1 := TRUE;
            StyleConso_2_1 := TRUE;
            StyleConso_3_1 := TRUE;
            StyleConso_4_1 := TRUE;
            StyleConso_5_1 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 2, 1) = 'O' THEN BEGIN
            StyleConso_0_2 := TRUE;
            StyleConso_1_2 := TRUE;
            StyleConso_2_2 := TRUE;
            StyleConso_3_2 := TRUE;
            StyleConso_4_2 := TRUE;
            StyleConso_5_2 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 3, 1) = 'O' THEN BEGIN
            StyleConso_0_3 := TRUE;
            StyleConso_1_3 := TRUE;
            StyleConso_2_3 := TRUE;
            StyleConso_3_3 := TRUE;
            StyleConso_4_3 := TRUE;
            StyleConso_5_3 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 4, 1) = 'O' THEN BEGIN
            StyleConso_0_4 := TRUE;
            StyleConso_1_4 := TRUE;
            StyleConso_2_4 := TRUE;
            StyleConso_3_4 := TRUE;
            StyleConso_4_4 := TRUE;
            StyleConso_5_4 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 5, 1) = 'O' THEN BEGIN
            StyleConso_0_5 := TRUE;
            StyleConso_1_5 := TRUE;
            StyleConso_2_5 := TRUE;
            StyleConso_3_5 := TRUE;
            StyleConso_4_5 := TRUE;
            StyleConso_5_5 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 6, 1) = 'O' THEN BEGIN
            StyleConso_0_6 := TRUE;
            StyleConso_1_6 := TRUE;
            StyleConso_2_6 := TRUE;
            StyleConso_3_6 := TRUE;
            StyleConso_4_6 := TRUE;
            StyleConso_5_6 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 7, 1) = 'O' THEN BEGIN
            StyleConso_0_7 := TRUE;
            StyleConso_1_7 := TRUE;
            StyleConso_2_7 := TRUE;
            StyleConso_3_7 := TRUE;
            StyleConso_4_7 := TRUE;
            StyleConso_5_7 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 8, 1) = 'O' THEN BEGIN
            StyleConso_0_8 := TRUE;
            StyleConso_1_8 := TRUE;
            StyleConso_2_8 := TRUE;
            StyleConso_3_8 := TRUE;
            StyleConso_4_8 := TRUE;
            StyleConso_5_8 := TRUE;
        END;
        IF COPYSTR(Rec."Chaine Mois", 9, 1) = 'O' THEN BEGIN
            StyleConso_0_9 := TRUE;
            StyleConso_1_9 := TRUE;
            StyleConso_2_9 := TRUE;
            StyleConso_3_9 := TRUE;
            StyleConso_4_9 := TRUE;
            StyleConso_5_9 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 10, 1) = 'O' THEN BEGIN
            StyleConso_0_10 := TRUE;
            StyleConso_1_10 := TRUE;
            StyleConso_2_10 := TRUE;
            StyleConso_3_10 := TRUE;
            StyleConso_4_10 := TRUE;
            StyleConso_5_10 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 11, 1) = 'O' THEN BEGIN
            StyleConso_0_11 := TRUE;
            StyleConso_1_11 := TRUE;
            StyleConso_2_11 := TRUE;
            StyleConso_3_11 := TRUE;
            StyleConso_4_11 := TRUE;
            StyleConso_5_11 := TRUE;
        END;

        IF COPYSTR(Rec."Chaine Mois", 12, 1) = 'O' THEN BEGIN
            StyleConso_0_12 := TRUE;
            StyleConso_1_12 := TRUE;
            StyleConso_2_12 := TRUE;
            StyleConso_3_12 := TRUE;
            StyleConso_4_12 := TRUE;
            StyleConso_5_12 := TRUE;
        END;
    end;

    local procedure TypedecommandeOnAfterValidate()
    begin
        CurrPage.frm_BufferPrice.PAGE.InsertBufferRecord(Rec);
        CurrPage.UPDATE();
    end;

    local procedure QuantityOnFormat()
    begin
        IF NOT Item.Stocké THEN
            StyleQuantity := TRUE
        ELSE
            StyleQuantity := FALSE
    end;

    local procedure coefventeOnFormat()
    begin
        IF coef_vente <> 1 THEN
            StyleCoefVente := TRUE
        ELSE
            StyleCoefVente := FALSE;
    end;

    local procedure CurrentJnlBatchNameOnAfterVali()
    var
    begin
        CurrPage.SAVERECORD();
        ReqJnlManagement.SetName(CurrentJnlBatchName, Rec);
        CurrPage.UPDATE(FALSE);

        SetDatz();
    end;

    local procedure SetDatz()
    var
        LUser: Record User;
        LAppro: Record "Requisition Line";
    begin
        Rec.FILTERGROUP(2);
        Rec.SETRANGE("Journal Batch Name", CurrentJnlBatchName);
        Rec.FILTERGROUP(0);


        // AD Le 16-04-2012 => Demande NG -> On affiche les fusions en attente
        Rec.SETRANGE("Fusion en attente sur");
        // FIN AD Le 16-04-2012

        // AD Le 29-12-2015
        CLEAR(LUser);
        LUser.SETRANGE("User Name", USERID);
        IF LUser.FINDFIRST() THEN BEGIN
            CLEAR(LAppro);
            //LAppro.SETRANGE("Worksheet Template Name"
            LAppro.SETRANGE("Journal Batch Name", CurrentJnlBatchName);
            LAppro.SETRANGE(Type, LAppro.Type::Item);
            //todo
            // LAppro.SETRANGE("No.", LUser."Dernière Ref. Appro");
            IF LAppro.FINDFIRST() THEN
                IF CONFIRM('Se placer sur la référence %1 ?', TRUE, LAppro."No.") THEN
                    Rec.SETPOSITION(LAppro.GETPOSITION(TRUE));
        END;
    end;
}
