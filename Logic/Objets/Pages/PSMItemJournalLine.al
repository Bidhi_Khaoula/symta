page 50253 "PSM Item Journal Line"
{
    PageType = Card;
    SourceTable = "Item Journal Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(General)
            {
                field("Journal Template Name"; Rec."Journal Template Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Journal Template Name field.';
                }
                field("Journal Batch Name"; Rec."Journal Batch Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Journal Batch Name field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Qty. (Calculated)"; Rec."Qty. (Calculated)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Calculated) field.';
                }
                field("Quantité PSM"; Rec."Quantité PSM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité PSM field.';
                }
                field("Date PSM"; Rec."Date PSM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date PSM field.';
                }
                field("Heure PSM"; Rec."Heure PSM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure PSM field.';
                }
                field("Utilisateur PSM"; Rec."Utilisateur PSM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur PSM field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the référence active field.';
                }
            }
        }
    }

    actions
    {
    }
}

