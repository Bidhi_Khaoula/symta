page 50210 "Equipment List"
{
    Caption = 'Equipments List';
    DelayedInsert = true;
    PageType = List;
    SourceTable = Equipment;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Equipment No."; Rec."Equipment No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment No. field.';
                }
                field(Manufacturer; Rec.Manufacturer)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Brand field.';
                }
                field(Model; Rec.Model)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Model field.';
                }
                field("Equipment Type"; Rec."Equipment Type")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Equipment Type field.';
                }
                field(Options; Rec.Options)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Options field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action("Articles / Equipements")
            {
                Image = Components;
                RunObject = Page "Item Equipment";
                RunPageLink = "Equipment No." = FIELD("Equipment No.");
                ApplicationArea = All;
                ToolTip = 'Executes the Articles / Equipements action.';
            }
        }
    }

    local procedure OpenEquipmentPage()
    var
        recL_EquipItem: Record "Modele / Article";
        pgeL_EquipItem: Page "Item Equipment";
    begin
        CLEAR(recL_EquipItem);
        CLEAR(pgeL_EquipItem);

        recL_EquipItem.SETRANGE("Equipment No.", Rec."Equipment No.");
        pgeL_EquipItem.SETTABLEVIEW(recL_EquipItem);
        pgeL_EquipItem.RUN();
    end;
    //CFR le 09/12/2021 - FA20210912 : ajout champ options
}

