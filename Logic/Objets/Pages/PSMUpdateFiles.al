page 50220 PSMUpdateFiles
{
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = File;
    SourceTableView = WHERE(Path = CONST('\\SRV-SQL-NAV2015\PSM\SMARTNAV'));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Path; Rec.Path)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Path field.';
                }
                field("Is a file"; Rec."Is a file")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Is a file field.';
                }
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field(Size; Rec.Size)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Size field.';
                }
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Time; Time)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Time field.';
                }
                field(Data; _Data)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the _Data field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin

        CLEAR(_Data);
        Rec.CALCFIELDS(Data);

        IF Rec.Data.HASVALUE THEN BEGIN
            Rec.Data.CREATEINSTREAM(IStream);
            MemoryStream := MemoryStream.MemoryStream();
            COPYSTREAM(MemoryStream, IStream);
            Bytes := MemoryStream.GetBuffer();
            _Data.ADDTEXT(Convert.ToBase64String(Bytes));
        END;
    end;

    var
        _Data: BigText;
        Bytes: DotNet Array;
        Convert: DotNet Convert;
        MemoryStream: DotNet MemoryStream;
        // OStream: OutStream;
        IStream: InStream;
}

