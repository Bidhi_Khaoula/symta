page 50327 "Demande Transporteur Card"
{

    Caption = 'Demande Transporteur';
    PageType = Card;
    SourceTable = "Demande transporteur";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Demande field.';
                }
                field(Statut; Rec.Statut)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut field.';
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';

                    trigger OnValidate();
                    begin
                        gIsExpedition := (Rec.Type = Rec.Type::Expédition);
                    end;
                }
                field("Customer No."; Rec."Customer No.")
                {
                    Editable = gIsExpedition;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Client field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    Editable = NOT gIsExpedition;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Fournisseur field.';

                    trigger OnValidate();
                    var
                        Address: Record "Order Address";
                        AddressChoice: Page "Demande Address Choice";
                    begin
                        Address.SETRANGE("Vendor No.", Rec."Vendor No.");
                        IF NOT Address.ISEMPTY THEN BEGIN
                            // s'il y a des adresses d'enlevement
                            CLEAR(Address);
                            AddressChoice.LOOKUPMODE := TRUE;
                            AddressChoice.InitVendor(Rec."Vendor No.");
                            AddressChoice.SETRECORD(Address);
                            // on propose la liste
                            IF AddressChoice.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                AddressChoice.GETRECORD(Address);
                                IF Address.Code <> '' THEN BEGIN // si different adresse principale
                                                                 // on modifie les valeurs de livraison
                                    Rec."Ship-to Name" := Address.Name;
                                    Rec."Ship-to Name 2" := Address."Name 2";
                                    Rec."Ship-to Address" := Address.Address;
                                    Rec."Ship-to Address 2" := Address."Address 2";
                                    Rec."Ship-to City" := Address.City;
                                    Rec."Ship-to Post Code" := Address."Post Code";
                                    Rec."Ship-to Country/Region Code" := Address."Country/Region Code";
                                END;
                            END;
                        END;
                    end;
                }
                field("Ship-to Name"; Rec."Ship-to Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name field.';
                }
                field("Packing List No."; Rec."Packing List No.")
                {
                    Editable = gIsExpedition;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Packing List field.';
                }
                field("Nombre Unite"; Rec."Nombre Unite")
                {
                    ToolTip = 'Saisie libre pour préciser le nombre total de colis/palettes';
                    ApplicationArea = all;
                }
                field("Created At"; Rec."Created At")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Création field.';
                }
                field("Created By"; Rec."Created By")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Crée par field.';
                }
                field("Date Départ"; Rec."Date Départ")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date départ prévue field.';
                }
                field("Expected Delivery Date"; Rec."Expected Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison prévue field.';
                }
                field("Delivery Date"; Rec."Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date livraison field.';
                }
                field("Shipment Method Code"; Rec."Shipment Method Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipment Method Code field.';
                }
                field("Incoterm City"; Rec."Incoterm City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ville incoterm field.';
                }
                field("Shipping Vendor No."; Rec."Shipping Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shipping Agent Code field.';
                }
                field("Shipping Vendor Name"; Rec."Shipping Vendor Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom transporteur field.';
                }
                field("Shipping Email"; Rec."Shipping Email")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Email transporteur field.';
                }
                field("Package Tracking No."; Rec."Package Tracking No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Package Tracking No. field.';
                }
                field("Shipping Invoice No."; Rec."Shipping Invoice No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Facture transporteur field.';
                }
                field("Code Appro"; Rec."Code Appro")
                {
                    Editable = NOT gIsExpedition;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
                field(Facturé; Rec.Facturé)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Facturé field.';
                }
                field(gCommentaire; gCommentaire)
                {
                    Caption = 'Commentaires';
                    MultiLine = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires field.';

                    trigger OnValidate();
                    begin
                        Rec.SetCommentaire(gCommentaire);
                    end;
                }
            }
            group(Adresse)
            {
                field("Ship-to Name 2"; Rec."Ship-to Name 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Name 2 field.';
                }
                field("Ship-to Address"; Rec."Ship-to Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address field.';
                }
                field("Ship-to Address 2"; Rec."Ship-to Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Address 2 field.';
                }
                field("Ship-to City"; Rec."Ship-to City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to City field.';
                }
                field("Ship-to Post Code"; Rec."Ship-to Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Post Code field.';
                }
                field("Ship-to Country/Region Code"; Rec."Ship-to Country/Region Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ship-to Country/Region Code field.';
                }
                field("Ship-to Phone"; Rec."Ship-to Phone")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Téléphone field.';
                }
                field("Ship-to E-mail"; Rec."Ship-to E-mail")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Email field.';
                }
            }
            group("Montants (Euros HT)")
            {
                field("Cout Transport"; Rec."Cout Transport")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coût Transport HT field.';

                    trigger OnValidate();
                    begin
                        gCoutGlobal := Rec.GetCoutGlobal();
                    end;
                }
                field("Frais douane"; Rec."Frais douane")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Frais Douane HT field.';

                    trigger OnValidate();
                    begin
                        gCoutGlobal := Rec.GetCoutGlobal();
                    end;
                }
                field("Frais annexe"; Rec."Frais annexe")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Frais Annexe HT field.';

                    trigger OnValidate();
                    begin
                        gCoutGlobal := Rec.GetCoutGlobal();
                    end;
                }
                field("Frais emballage"; Rec."Frais emballage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Frais Emballage HT field.';

                    trigger OnValidate();
                    begin
                        gCoutGlobal := Rec.GetCoutGlobal();
                    end;
                }
                field("Frais Taxe Gasoil"; Rec."Frais Taxe Gasoil")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Frais Taxe Gasoil HT field.';

                    trigger OnValidate();
                    begin
                        gCoutGlobal := Rec.GetCoutGlobal();
                    end;
                }
                field(gCoutGlobal; gCoutGlobal)
                {
                    Caption = 'Coût global HT';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Coût global HT field.';
                }
                field("Valeur transport vendu"; Rec."Valeur transport vendu")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valeur transport vendu HT field.';
                }
                field(GetValueVente;
                Rec.GetValueVente())
                {
                    Caption = 'Valeur Vente HT';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valeur Vente HT field.';
                }
                field(GetValueAchat;
                Rec.GetValueAchat())
                {
                    Caption = 'Valeur Achat HT';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valeur Achat HT field.';
                }
            }
            part("Infos Colisage"; "Packing List Header Part")
            {
                Caption = 'Infos Colisage';
                Editable = false;
                SubPageLink = "No." = FIELD("Packing List No.");
                Visible = gIsExpedition;
                ApplicationArea = All;
            }
            part("Détail Colisage"; "Packing List Subform2")
            {
                Caption = 'Détail Colisage';
                Editable = false;
                SubPageLink = "Packing List No." = FIELD("Packing List No.");
                Visible = gIsExpedition;
                ApplicationArea = All;
            }
            part("Colisage Réception"; "Receipt Packing ListPart")
            {
                Caption = 'Colisage Réception';
                Editable = NOT gIsExpedition;
                SubPageLink = "Demande No." = FIELD("No.");
                Visible = NOT gIsExpedition;
                ApplicationArea = All;
            }
            part(Liens; "Demande Receipt ListPart")
            {
                Caption = 'Liens';
                Editable = NOT gIsExpedition;
                SubPageLink = "Demande No." = FIELD("No."),
                              "Vendor No. Filter" = FIELD("Vendor No.");
                Visible = NOT gIsExpedition;
                ApplicationArea = All;
            }
        }
        area(factboxes)
        {
            systempart(Links; Links)
            {
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(creation)
        {
            action("Envoyer mail")
            {
                Image = Email;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                ApplicationArea = All;
                Caption = 'Mail enlèvement';
                ToolTip = 'Mail pour enlèvement';
                trigger OnAction();
                var
                    ReceiptLine: Record "Receipt Packing List";
                    PackingListPackage: Record "Packing List Package";
                    Country: Record "Country/Region";
                    PackingList: Record "Packing List Header";
                    EskapeComm: Codeunit "Eskape Communication : Email";
                    Body: Text;
                begin
                    Body := 'Bonjour,<br><br>';
                    Body += 'Pourrais-tu stp organiser l’enlèvement de : <br><br>';
                    // packing list R‚ception
                    IF Rec.Type = Rec.Type::Réception THEN BEGIN
                        ReceiptLine.SETRANGE("Demande No.", Rec."No.");
                        IF ReceiptLine.FINDSET() THEN
                            REPEAT
                                IF (ReceiptLine."Type colisage" = ReceiptLine."Type colisage"::Metres) THEN
                                    Body += STRSUBSTNO('&nbsp;- %1m plancher (Poids : %2kgs)<br>', ReceiptLine.Quantité, ReceiptLine.Poids)
                                ELSE IF (ReceiptLine."Type colisage" = ReceiptLine."Type colisage"::Complet) THEN
                                    Body += STRSUBSTNO('&nbsp;- %1 camion complet<br>', ReceiptLine.Quantité)
                                ELSE
                                    Body += STRSUBSTNO('&nbsp;- %1 %2 &nbsp; %3m x %4m x %5m<br>',
                                      ReceiptLine.Quantité, ReceiptLine."Type colisage",
                                      ReceiptLine.Longeur, ReceiptLine.Largeur, ReceiptLine.Hauteur);
                            UNTIL ReceiptLine.NEXT() = 0;

                        Body += '<br>';
                        IF Rec.GetReceiptVolume() > 0 THEN
                            Body += STRSUBSTNO('Poids total : %1kgs<br>Volume total : %2m3', Rec.GetReceiptPoids(), Rec.GetReceiptVolume()) + '<br><br>';
                        Body += 'Chez notre fournisseur : <br>';
                    END;
                    // packing list Exp‚dition
                    IF Rec.Type = Rec.Type::Expédition THEN BEGIN
                        PackingList.SETAUTOCALCFIELDS("Calculate Header Weight", "Total Volume");
                        PackingList.GET(Rec."Packing List No.");
                        PackingListPackage.SETRANGE("Packing List No.", Rec."Packing List No.");
                        IF PackingListPackage.FINDSET() THEN
                            REPEAT
                                Body += STRSUBSTNO('&nbsp;- %1 &nbsp; %2m x %3m x %4m<br>',
                                  PackingListPackage."Package Type",
                                  PackingListPackage.Length / 100, PackingListPackage.Height / 100, PackingListPackage.Width / 100);
                            UNTIL PackingListPackage.NEXT() = 0;
                        Body += '<br>' + STRSUBSTNO('Poids total : %1kgs<br>Volume total : %2m3', PackingList."Calculate Header Weight", PackingList."Total Volume") + '<br><br>';
                        Body += 'Pour notre client : <br>';
                    END;
                    // coordonn‚es
                    IF Country.GET(Rec."Ship-to Country/Region Code") THEN;
                    Body += '  &nbsp; Nom : ' + Rec."Ship-to Name" + '<br>';
                    Body += '  &nbsp; Adresse : ' + Rec."Ship-to Address" + '<br>';
                    Body += '  &nbsp; Code postal : ' + Rec."Ship-to Post Code" + '<br>';
                    Body += '  &nbsp; Ville : ' + Rec."Ship-to City" + '<br>';
                    Body += '  &nbsp; Pays : ' + Country.Name + '<br>';
                    Body += '  &nbsp; Téléphone : ' + Rec."Ship-to Phone" + '<br>';
                    Body += '  &nbsp; Email : ' + Rec."Ship-to E-mail" + '<br>';
                    Body += '<br>';


                    Body += 'Merci beaucoup de me confirmer qui passera et quand.<br><br>';
                    Body += 'Bien cordialement.<br><br>';

                    EskapeComm.OutlookMail('', 'Demande transporteur ' + Rec."No.", Rec."Shipping Email", '', Body);
                end;
            }
            action("Envoyer mail 2")
            {
                ApplicationArea = All;
                Caption = 'Mail prix';
                ToolTip = 'Mail pour demande de prix';
                Promoted = true;
                PromotedIsBig = true;
                Image = Email;
                PromotedCategory = Process;
                PromotedOnly = true;
                Trigger OnAction()
                VAR
                    ReceiptLine: Record "Receipt Packing List";
                    PackingListPackage: Record "Packing List Package";
                    Country: Record "Country/Region";
                    PackingList: Record "Packing List Header";
                    EskapeComm: Codeunit "Eskape Communication : Email";
                    Body: Text;
                BEGIN
                    Body := 'Bonjour,<br><br>';

                    // packing list R‚ception
                    IF Rec.Type = Rec.Type::Réception THEN BEGIN
                        Body += 'Pourrais-tu stp nous faire une cotation pour lÉenlèvement de : <br><br>';

                        ReceiptLine.SETRANGE("Demande No.", Rec."No.");
                        IF ReceiptLine.FINDSET() THEN
                            REPEAT
                                IF (ReceiptLine."Type colisage" = ReceiptLine."Type colisage"::Metres) THEN
                                    Body += STRSUBSTNO('&nbsp;- %1m plancher (Poids : %2kgs)<br>', ReceiptLine.Quantité, ReceiptLine.Poids)
                                ELSE IF (ReceiptLine."Type colisage" = ReceiptLine."Type colisage"::Complet) THEN
                                    Body += STRSUBSTNO('&nbsp;- %1 camion complet<br>', ReceiptLine.Quantité)
                                ELSE
                                    Body += STRSUBSTNO('&nbsp;- %1 %2 &nbsp; %3m x %4m x %5m<br>',
                                      ReceiptLine.Quantité, ReceiptLine."Type colisage",
                                      ReceiptLine.Longeur, ReceiptLine.Largeur, ReceiptLine.Hauteur);
                            UNTIL ReceiptLine.NEXT() = 0;

                        Body += '<br>';
                        IF Rec.GetReceiptVolume() > 0 THEN Body += STRSUBSTNO('Poids total : %1kgs<br>Volume total : %2m3', Rec.GetReceiptPoids(), Rec.GetReceiptVolume()) + '<br><br>';
                        Body += 'Chez notre fournisseur : <br>';
                    END;

                    // packing list Exp‚dition
                    IF Rec.Type = Rec.Type::Expédition THEN BEGIN
                        Body += 'Pourrais-tu stp chiffrer lÉexp‚dition : <br><br>';

                        PackingList.SETAUTOCALCFIELDS("Calculate Header Weight", "Total Volume");
                        PackingList.GET(Rec."Packing List No.");
                        PackingListPackage.SETRANGE("Packing List No.", Rec."Packing List No.");
                        IF PackingListPackage.FINDSET() THEN
                            REPEAT
                                Body += STRSUBSTNO('&nbsp;- %1 &nbsp; %2m x %3m x %4m<br>',
                                  PackingListPackage."Package Type",
                                  PackingListPackage.Length / 100, PackingListPackage.Height / 100, PackingListPackage.Width / 100);
                            UNTIL PackingListPackage.NEXT() = 0;
                        Body += '<br>' + STRSUBSTNO('Poids total : %1kgs<br>Volume total : %2m3', PackingList."Calculate Header Weight", PackingList."Total Volume") + '<br><br>';
                        Body += 'Pour notre client : <br>';
                    END;

                    // coordonn‚es
                    IF Country.GET(Rec."Ship-to Country/Region Code") THEN;
                    Body += '  &nbsp; Nom : ' + Rec."Ship-to Name" + '<br>';
                    Body += '  &nbsp; Adresse : ' + Rec."Ship-to Address" + '<br>';
                    Body += '  &nbsp; Code postal : ' + Rec."Ship-to Post Code" + '<br>';
                    Body += '  &nbsp; Ville : ' + Rec."Ship-to City" + '<br>';
                    Body += '  &nbsp; Pays : ' + Country.Name + '<br>';
                    Body += '  &nbsp; Téléphone : ' + Rec."Ship-to Phone" + '<br>';
                    Body += '  &nbsp; Email : ' + Rec."Ship-to E-mail" + '<br>';
                    Body += '<br>';


                    Body += 'Merci beaucoup.<br><br>';
                    Body += 'Bien cordialement.<br><br>';

                    EskapeComm.OutlookMail('', 'Demande prix ' + Rec."No.", Rec."Shipping Email", '', Body);
                END;
            }
        }
    }

    trigger OnAfterGetCurrRecord();
    begin
        gIsExpedition := (Rec.Type = Rec.Type::Expédition);
        gCommentaire := Rec.GetCommentaire();
    end;

    trigger OnAfterGetRecord();
    begin
        gCoutGlobal := Rec.GetCoutGlobal();
    end;

    var
        gIsExpedition: Boolean;
        gCommentaire: Text;
        gCoutGlobal: Decimal;
}

