page 50008 "Lignes à livrer"
{
    CardPageID = "Lignes à livrer";
    Editable = false;
    PageType = List;
    SourceTable = "Sales Line";
    SourceTableView = SORTING("Document Type", Type, "No.", "Outstanding Quantity")
                      WHERE("Document Type" = CONST(Order),
                            Type = CONST(Item),
                            "Outstanding Quantity" = FILTER(<> 0),
                            "Recherche référence" = FILTER(<> ''));

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("Document No."; Rec."Document No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Document No. field.';
                }
                field("Sell-to Customer No."; Rec."Sell-to Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sell-to Customer No. field.';
                }
                field("Sell-to Customer Name"; SalesHeader."Sell-to Customer Name")
                {
                    Caption = 'Nom client';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom client field.';
                }
                field("Order Create User"; SalesHeader."Order Create User")
                {
                    Caption = 'Code utilisateur création';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code utilisateur création field.';
                }
                field("Recherche référence"; Rec."Recherche référence")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Recherche référence field.';
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Outstanding Quantity"; Rec."Outstanding Quantity")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Outstanding Quantity field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Requested Delivery Date"; Rec."Requested Delivery Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Requested Delivery Date field.';
                }
                field("Type de commande"; SalesHeader."Type de commande")
                {
                    Caption = 'Type de commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field(ItemInventory; Item.Inventory)
                {
                    Caption = 'Stock physique';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock physique field.';
                }
                field(dec_attendu; dec_attendu)
                {
                    Caption = 'Qty. to Assign';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Assign field.';
                }
                field(dec_reserve; dec_reserve)
                {
                    Caption = 'Return Qty. to Receive';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Return Qty. to Receive field.';
                }
                field("Item Manufacturer Code"; Rec."Item Manufacturer Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Marque field.';
                }
                field("Item Category Code"; Rec."Item Category Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Category Code field.';
                }
                field("Item Code Appro"; Rec."Item Code Appro")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        SalesHeader.GET(Rec."Document Type", Rec."Document No.");
        Item.GET(Rec."No.");
        Item.CalcAttenduReserve(dec_reserve, dec_attendu);
    end;

    var
        SalesHeader: Record "Sales Header";
        Item: Record Item;
        dec_attendu: Decimal;
        dec_reserve: Decimal;

    //CFR : Accessibilit‚ Info Reliquats clients ajout [Marque] [Famille] [Sous Famille] [Code appro]
}

