page 50231 "ECommerce Article"
{
    PageType = List;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code_Article; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Designation; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Designation_2; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field(Reference_Active; Rec."No. 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Stock; Rec.Inventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Inventory field.';
                }
                field(Poids_Brut; Rec."Gross Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Gross Weight field.';
                }
                field(Poids_Net; Rec."Net Weight")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net Weight field.';
                }
                field(Dispo_Web; GetStockPrepaSymta())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetStockPrepaSymta() field.';
                }
                field(stock_maxi_publiable; Rec."Stock Maxi Publiable")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Maxi Publiable field.';
                }
                field(stock_mort; Rec."Stock Mort")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock Mort field.';
                }
                field(Dispo_A_Date; GetDispoADate())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetDispoADate() field.';
                }
                field(No_Nomenclature; Nomenclature)
                {
                    Caption = 'No_Nomenclature';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No_Nomenclature field.';
                }
                field(Publiable; Rec.Publiable)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Publiable field.';
                }
                field("Super Famille Marketing By Def"; Rec."Super Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Super Famille Marketing By Def field.';
                }
                field("Famille Marketing By Def"; Rec."Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Famille Marketing By Def field.';
                }
                field("Sous Famille Marketing By Def"; Rec."Sous Famille Marketing By Def")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sous Famille Marketing By Def field.';
                }
                field("Net Price Only"; Rec."Net Price Only")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Net price only field.';
                }
                field(Multiple_Vente; decG_SalesMultiple)
                {
                    Caption = 'Multiple_Vente';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple_Vente field.';
                }
                field("Manufacturer Code"; rec."Manufacturer Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Manufacturer Code field.';
                }
                field("Item Category Code"; rec."Item Category Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item Category Code field.';
                }
                field("Kit Web Status"; Rec."Kit Web Status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Etat du kit (Web) field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    var
        LBOM: Record "BOM Component";
    begin
        CLEAR(LBOM);
        Nomenclature := '';
        LBOM.SETRANGE("Parent Item No.", Rec."No.");
        IF NOT LBOM.ISEMPTY THEN
            Nomenclature := Rec."No.";

        // MCO Le 07-06-2017 => Régie
        IF Rec."Respect Sales Multiple" THEN
            decG_SalesMultiple := Rec."Sales multiple"
        ELSE
            decG_SalesMultiple := 0;
        // MCO Le 07-06-2017 => Régie
    end;

    var
        Nomenclature: Code[20];
        decG_SalesMultiple: Decimal;

    procedure GetStockPrepaSymta() dec_rtn: Decimal
    var
        rec_WhseShipment: Record "Warehouse Shipment Line";
        qteprepa: Decimal;
    begin
        // Fonction renvoyant le stock - la quantité sur bon de préparation.
        Rec.CALCFIELDS(Inventory);
        qteprepa := 0;
        // Calcul la qté sur bon de prépa.
        rec_WhseShipment.RESET();
        rec_WhseShipment.SETRANGE("Item No.", Rec."No.");
        IF rec_WhseShipment.FINDSET() THEN
            REPEAT
                qteprepa += rec_WhseShipment."Qty. to Ship (Base)";
            UNTIL rec_WhseShipment.NEXT() = 0;

        dec_rtn := Rec.Inventory - qteprepa;
    end;

    procedure GetDispoADate(): Decimal
    var
        Item: Record Item;
        AvailableToPromise: Codeunit "Available to Promise";
        LookaheadDateformula: DateFormula;
        GrossRequirement: Decimal;
        ScheduledReceipt: Decimal;
        PeriodType: Option Day,Week,Month,Quarter,Year;
        AvailabilityDate: Date;
    begin
        IF Item.GET(Rec."No.") THEN BEGIN

            AvailabilityDate := TODAY;

            Item.RESET();
            Item.SETRANGE("Date Filter", 0D, AvailabilityDate);
            Item.SETRANGE("Variant Filter", '');
            Item.SETRANGE("Location Filter", GetDefaultLocation());
            Item.SETRANGE("Drop Shipment Filter", FALSE);

            EXIT(
              AvailableToPromise.CalcQtyAvailabletoPromise(
                Item,
                GrossRequirement,
                ScheduledReceipt,
                AvailabilityDate,
                "Analysis Period Type".FromInteger(PeriodType),
                LookaheadDateformula));
        END;
    end;

    procedure GetDefaultLocation() txt_rtn: Code[20]
    var
        Company: Record "Company Information";
    begin
        Company.RESET();
        Company.GET();
        txt_rtn := Company."Location Code";
    end;
}

