page 50007 "Historisation Taux SAV"
{
    CardPageID = "Historisation Taux SAV";
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Historisation Taux SAV / Porte";
    SourceTableView = SORTING("Type Ligne", "No.", Date, Heure)
                      ORDER(Descending);

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field(Date; Rec.Date)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
                field(Heure; Rec.Heure)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure field.';
                }
                field("Taux Sav"; Rec."Taux Sav")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Taux Sav field.';
                }
            }
        }
    }
}

