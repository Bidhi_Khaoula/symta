page 50245 "Multi Consultation RemiseVente"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = ListPart;
    SourceTable = "Sales Line Discount";
    SourceTableTemporary = true;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Type de commande"; Rec."Type de commande")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';
                }
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field("Sales Code"; Rec."Sales Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Sales Code field.';
                }
                field("Référence Active"; Rec."Référence Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence Active field.';
                }
                field("Code Marque"; Rec."Code Marque")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Marque field.';
                }
                field("Code Famille"; Rec."Code Famille")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Famille field.';
                }
                field("Code Sous Famille"; Rec."Code Sous Famille")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Sous Famille field.';
                }
                field("Line Discount 1 %"; Rec."Line Discount 1 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise ligne 1 field.';
                }
                field("Line Discount 2 %"; Rec."Line Discount 2 %")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the % Remise ligne 2 field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
            }
        }
    }

    actions
    {
    }

    procedure InitForm(_pCodeClient: Code[20]; _pTypeCommande: Integer)
    var
        LCust: Record Customer;
        LRemise: Record "Sales Line Discount";
    begin
        Rec.DELETEALL();
        IF NOT LCust.GET(_pCodeClient) THEN EXIT;


        CLEAR(LRemise);
        LRemise.SETCURRENTKEY("Sales Type", "Sales Code", Type, Code, "Starting Date",
        "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity");
        LRemise.SETRANGE("Sales Type", LRemise."Sales Type"::Customer);
        LRemise.SETRANGE("Sales Code", LCust."No.");
        LRemise.SETFILTER("Ending Date", '%1|>=%2', 0D, WORKDATE());
        LRemise.SETRANGE("Starting Date", 0D, WORKDATE());
        IF LRemise.FINDFIRST() THEN
            REPEAT
                Rec := LRemise;
                Rec.Insert();
            UNTIL LRemise.NEXT() = 0;

        LRemise.SETRANGE("Sales Type", LRemise."Sales Type"::"Customer Disc. Group");
        LRemise.SETRANGE("Sales Code", LCust."Customer Disc. Group");
        IF LRemise.FINDFIRST() THEN
            REPEAT
                Rec := LRemise;
                Rec.Insert();
            UNTIL LRemise.NEXT() = 0;
    end;
}

