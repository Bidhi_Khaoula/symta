page 50078 "Liste import"
{
    CardPageID = "Liste import";
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Entete import";
    ApplicationArea = All;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(Nom; Rec.Nom)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field(journ; Rec.journ)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the journ field.';
                }
                field(oper; Rec.oper)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the oper field.';
                }
                field(decr; Rec.decr)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the decr field.';
                }
                field(ref; Rec.ref)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref field.';
                }
                field(dech; Rec.dech)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the dech field.';
                }
                field("Montant debit"; Rec."Montant debit")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant debit field.';
                }
                field("Montant credit"; Rec."Montant credit")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant credit field.';
                }
            }
        }
    }

    actions
    {
    }
}

