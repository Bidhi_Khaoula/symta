page 50313 "WIIO - Liste Imprimante"
{
    DeleteAllowed = false;
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Generals Parameters";
    SourceTableView = SORTING(Type, Code)
                      ORDER(Ascending)
                      WHERE(Type = FILTER('NICELBL_IMP' | 'IMP_ETQ_RECEPT'));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Code; Rec.Code)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code field.';
                }
                field(Name; Rec.Code)
                {
                    Caption = 'Nom';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom field.';
                }
                field(LongCode; Rec.LongCode)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Long field.';
                }
            }
        }
    }

    actions
    {
    }
}

