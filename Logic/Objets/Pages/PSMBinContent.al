page 50221 PSMBinContent
{
    SourceTable = "Bin Content";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Bin Code"; Rec."Bin Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field(Fixed; Rec.Fixed)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fixed field.';
                }
                field(Default; Rec.Default)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Default field.';
                }
            }
        }
    }

    actions
    {
    }
}

