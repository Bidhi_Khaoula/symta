// page 50114 "Salesperson/Purchaser Card tes"
// {
//     Caption = 'Salesperson/Purchaser Card';
//     CardPageID = "Salesperson/Purchaser Card tes";
//     PageType = Card;
//     SourceTable = "Salesperson/Purchaser Test";

//     layout
//     {
//         area(content)
//         {
//             group(General)
//             {
//                 Caption = 'General';
//                 field(Code; Rec.Code)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Name; Rec.Name)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Job Title"; Rec."Job Title")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Commission %"; Rec."Commission %")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Phone No."; Rec."Phone No.")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("E-Mail"; Rec."E-Mail")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Next To-do Date"; Rec."Next To-do Date")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Rate ADV"; Rec."Rate ADV")
//                 {
//                     ApplicationArea = All;
//                 }
//             }
//             group(Invoicing)
//             {
//                 Caption = 'Invoicing';
//                 field("Global Dimension 1 Code"; Rec."Global Dimension 1 Code")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Global Dimension 2 Code"; Rec."Global Dimension 2 Code")
//                 {
//                     ApplicationArea = All;
//                 }
//             }
//             group(Address)
//             {
//                 Caption = 'Address';
//                 field(Name; Rec.Name)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field(Address; Rec.Address)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Address 2"; Rec."Address 2")
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Post Code"; Rec."Post Code")
//                 {
//                     Caption = 'Post Code/City';
//                     ApplicationArea = All;
//                 }
//                 field(City; Rec.City)
//                 {
//                     ApplicationArea = All;
//                 }
//                 field("Fax No."; Rec."Fax No.")
//                 {
//                     ApplicationArea = All;
//                 }
//             }
//             group(Sale)
//             {
//                 Caption = 'Sale';
//                 field(test_vincent_numero; Rec.test_vincent_numero)
//                 {
//                     ApplicationArea = All;
//                 }
//             }
//         }
//     }

//     actions
//     {
//         area(navigation)
//         {
//             group("&Salesperson")
//             {
//                 Caption = '&Salesperson';
//                 action("Tea&ms")
//                 {
//                     Caption = 'Tea&ms';
//                     RunObject = Page "Salesperson Teams";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 action("Con&tacts")
//                 {
//                     Caption = 'Con&tacts';
//                     RunObject = Page "Contact List";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 action(Dimensions)
//                 {
//                     Caption = 'Dimensions';
//                     Image = Dimensions;
//                     RunObject = Page 540;
//                     RunPageLink = "Table ID" = CONST(13), "No." = FIELD(Code);
//                     ShortCutKey = 'Shift+Ctrl+D';
//                     ApplicationArea = All;
//                 }
//                 action(Statistics)
//                 {
//                     Caption = 'Statistics';
//                     Image = Statistics;
//                     Promoted = true;
//                     PromotedCategory = Process;
//                     RunObject = Page 5117;
//                     RunPageLink = Code = FIELD(Code);
//                     ShortCutKey = 'F7';
//                     ApplicationArea = All;
//                 }
//                 action("C&ampaigns")
//                 {
//                     Caption = 'C&ampaigns';
//                     RunObject = Page "Campaign Card";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 action("S&egments")
//                 {
//                     Caption = 'S&egments';
//                     Image = Segment;
//                     RunObject = Page Segment;
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 separator()
//                 {
//                     Caption = '';
//                 }
//                 action("Interaction Log E&ntries")
//                 {
//                     Caption = 'Interaction Log E&ntries';
//                     RunObject = Page "Interaction Log Entries";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ShortCutKey = 'Ctrl+F7';
//                     ApplicationArea = All;
//                 }
//                 action("Postponed &Interactions")
//                 {
//                     Caption = 'Postponed &Interactions';
//                     RunObject = Page "Postponed Interactions";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 action("T&o-dos")
//                 {
//                     Caption = 'T&o-dos';
//                     RunObject = Page "Task List";
//                     RunPageLink = "Salesperson Code" = FIELD(Code), "System To-do Type" = FILTER(Organizer | "Salesperson Attendee");
//                     RunPageView = SORTING("Salesperson Code");
//                     ApplicationArea = All;
//                 }
//                 group("Oppo&rtunities")
//                 {
//                     Caption = 'Oppo&rtunities';
//                     action(List)
//                     {
//                         Caption = 'List';
//                         RunObject = Page "Opportunity List";
//                         RunPageLink = "Salesperson Code" = FIELD(Code);
//                         RunPageView = SORTING("Salesperson Code");
//                         ApplicationArea = All;
//                     }
//                     action("Bar Chart")
//                     {
//                         Caption = 'Bar Chart';
//                         ApplicationArea = All;

//                         trigger OnAction()
//                         begin
//                             OpportunityBarChart.SetSalespersonFilter(rec.Code);
//                             OpportunityBarChart.RUN();
//                         end;
//                     }
//                 }
//                 separator()
//                 {
//                 }
//                 action(Objectifs)
//                 {
//                     Caption = 'Objectifs';
//                     RunObject = Page "Objectifs Vendeur";
//                     RunPageLink = "Salesperson Code" = FIELD(Code);
//                     RunPageView = SORTING("Salesperson Code", "Cust. Country/Region Code", "Cust. Family Code 1", "Item Manufacturer Code") ORDER(Ascending);
//                     ApplicationArea = All;
//                 }
//             }
//         }
//         area(processing)
//         {
//             action("Create &Interact")
//             {
//                 Caption = 'Create &Interact';
//                 Image = CreateInteraction;
//                 Promoted = true;
//                 PromotedCategory = Process;
//                 ApplicationArea = All;

//                 trigger OnAction()
//                 begin
//                     rec.CreateInteraction;
//                 end;
//             }
//         }
//     }

//     trigger OnInsertRecord(BelowxRec: Boolean): Boolean
//     begin
//         IF xRec.Code = '' THEN
//             rec.RESET();
//     end;

//     var
//         Mail: Codeunit Mail;
// }

