page 50055 "Nb Devis Web Factbox"
{
    ApplicationArea = all;
    PageType = CardPart;

    layout
    {
        area(content)
        {
            field("Nb Devis Web non export"; cu_WebInfo.GetNbOfWebQuote(FALSE))
            {
                Caption = 'Nb Devis Web non Export';
                Style = Attention;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Nb Devis Web non Export field.';

                trigger OnDrillDown()
                begin
                    // FBO le 05-04-2017 => FE20170324
                    OpenFormWebQuote(FALSE);
                    // FIN FBO le 05-04-2017
                end;
            }
            field("Nb Devis Web Export"; cu_WebInfo.GetNbOfWebQuote(TRUE))
            {
                Style = Attention;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetNbOfWebQuote(TRUE) field.';

                trigger OnDrillDown()
                begin
                    // FBO le 05-04-2017 => FE20170324
                    OpenFormWebQuote(TRUE);
                    // FIN FBO le 05-04-2017
                end;
            }
            field("Nb Lignes affectation"; rec_Affectation.GetNbOfNonCheckedLine())
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the GetNbOfNonCheckedLine() field.';

                trigger OnDrillDown()
                begin
                    rec_Affectation.OpenPage()
                end;
            }
        }
    }

    actions
    {
    }

    var
        rec_Affectation: Record "Affectation Réception";
        cu_WebInfo: Codeunit "ECommerce Functions";


    local procedure OpenFormWebQuote(varExport: Boolean)
    var
        LWebQuote: Record "Sales Header";
        LFrmWebQuote: Page "Sales Quotes Web";
    begin
        // FBO le 05-04-2017 => FE20170324
        CLEAR(LWebQuote);
        LWebQuote.SETRANGE("Document Type", LWebQuote."Document Type"::Quote);
        LWebQuote.SETRANGE("Devis Web", TRUE);
        LWebQuote.SETRANGE("Export Salesperson", varExport);

        LFrmWebQuote.SETTABLEVIEW(LWebQuote);

        LFrmWebQuote.RUNMODAL();
        // FIN FBO le 05-04-2017
    end;
}

