page 50056 "Detail Saisie Reception"
{
    DelayedInsert = false;
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = List;
    SourceTable = "Saisis Réception Magasin";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Line No."; Rec."Line No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Line No. field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Ref. Active"; Rec.GetRefActive())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetRefActive() field.';
                }
                field("Ref Fournisseur"; Rec.GetRefFournisseur())
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the GetRefFournisseur() field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Utilisateur Modif"; Rec."Utilisateur Modif")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Utilisateur Modif field.';
                }
                field("Date Modif"; Rec."Date Modif")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Modif field.';
                }
                field("Heure Modif"; Rec."Heure Modif")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure Modif field.';
                }
                field(Commentaire; Rec.Commentaire)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
            }
        }
    }

    actions
    {
    }
}

