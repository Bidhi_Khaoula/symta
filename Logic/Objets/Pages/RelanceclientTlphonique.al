page 50038 "Relance client Télèphonique"
{
    CardPageID = "Relance client Télèphonique";
    InsertAllowed = false;
    PageType = Document;
    Permissions = TableData "Cust. Ledger Entry" = rm;
    SourceTable = "Relance Telephonique";
    SourceTableView = SORTING("N° client");
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("No."; Client."No.")
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the No. field.';
            }
            field("Phone No."; Client."Phone No.")
            {
                Caption = 'Télèphone';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Télèphone field.';
            }
            field(Balance; Client.Balance)
            {
                Caption = 'Solde Client';
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Solde Client field.';

                trigger OnDrillDown()
                begin
                    DtldCustLedgEntry.SETRANGE("Customer No.", Client."No.");
                    CustLedgEntry.DrillDownOnEntries(DtldCustLedgEntry);
                end;
            }
            field(Name; Client.Name)
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Name field.';
            }
            field("Montant en relance_"; "Montant en relance"())
            {
                Editable = false;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant en relance() field.';
            }
            repeater(Lines)
            {
                field("N° client"; Rec."N° client")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° client field.';
                }
                field("Ville client"; Rec."Ville client")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ville client field.';
                }
                field("Type document"; Rec."Type document")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type document field.';
                }
                field("N° de document"; Rec."N° de document")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° de document field.';
                }
                field("Date compta"; Rec."Date compta")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date compta field.';
                }
                field("Date d'echéance"; Rec."Date d'echéance")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date d''echéance field.';
                }
                field("Groupe compta client"; Rec."Groupe compta client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Groupe compta client field.';
                }
                field(Désignation; Rec.Désignation)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Montant initial"; Rec."Montant initial")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant initial field.';
                }
                field("Montant restant"; Rec."Montant restant")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant restant field.';
                }
                field("Dernier niveau"; Rec."Dernier niveau")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Dernier niveau field.';
                }
                field("Prochain niveau"; Rec."Prochain niveau")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prochain niveau field.';
                }
            }
            field(""; '')
            {
                CaptionClass = Text19039943Lbl;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the '''' field.';
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Fax)
            {
                Caption = 'Fax';
                Visible = true;
                action(Test)
                {
                    Caption = 'Test';
                    RunObject = Report "Sales - Return Receipt SYM";
                    ApplicationArea = All;
                    ToolTip = 'Executes the Test action.';
                }
                action("Envoi Par Fax")
                {
                    Caption = 'Envoi Par Fax';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Envoi Par Fax action.';

                    trigger OnAction()
                    var
                        rec_relance: Record "Relance Telephonique";
                        rec_relance2: Record "Relance Telephonique";
                        //TODOrapport: Report 50036;
                        Modele: Record "Reminder Terms";
                        code_client: Code[20];
                        i: Integer;
                        Options: Text[250];
                        Choice: Integer;

                    begin

                        IF NOT CONFIRM('Envoi par fax?') THEN
                            EXIT;

                        IF Modele.FINDFIRST() THEN
                            REPEAT
                                i += 1;
                                IF Options = '' THEN
                                    Options := Modele.Code
                                ELSE
                                    Options := Options + ',' + Modele.Code;
                            UNTIL Modele.NEXT() = 0;

                        IF i > 0 THEN
                            Choice := STRMENU(Options, 1);

                        i := 1;
                        IF Choice > 0 THEN BEGIN
                            Modele.FINDFIRST();
                            WHILE Choice > i DO BEGIN
                                i += 1;
                                Modele.NEXT();
                            END;
                        END;
                        // Annulation
                        IF Choice = 0 THEN BEGIN
                            EXIT;
                        END;
                        code_client := '';
                        rec_relance.SETCURRENTKEY("N° client");
                        rec_relance.COPYFILTERS(Rec);
                        IF rec_relance.FINDFIRST() THEN
                            REPEAT
                                IF code_client <> rec_relance."N° client" THEN BEGIN
                                    /*//TODOCLEAR(rapport);
                                    rapport."Charger modele"(Modele);
                                    rapport."Charger num fax";
                                    rapport.USEREQUESTFORM := FALSE;*/
                                    rec_relance2.RESET();
                                    rec_relance2.SETRANGE("N° client", rec_relance."N° client");
                                    /*//TODOrapport.SETTABLEVIEW(rec_relance2);
                                    rapport.RUN();*/
                                    SLEEP(1000);
                                END;
                                code_client := rec_relance."N° client";
                            UNTIL rec_relance.NEXT() = 0;
                    end;
                }
            }
        }
        area(processing)
        {
            action("Fiche Client")
            {
                Caption = 'Fiche Client';
                Promoted = true;
                PromotedCategory = Process;
                RunObject = Page "Customer Card";
                RunPageLink = "No." = FIELD("N° client");
                ApplicationArea = All;
                ToolTip = 'Executes the Fiche Client action.';
            }
            action(Extraire)
            {
                Caption = 'Extraire';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Extraire action.';

                trigger OnAction()
                begin
                    REPORT.RUNMODAL(REPORT::"Extraire Relance Télèphonique");
                end;
            }
            action(Editer)
            {
                Caption = 'Editer';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Editer action.';

                trigger OnAction()
                begin
                    //TODOREPORT.RUNMODAL(REPORT::"Relance Télèphonique", TRUE, FALSE, Rec);
                end;
            }
            action(Valider)
            {
                Caption = 'Valider';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Valider action.';

                trigger OnAction()
                var
                    Ecriture_client: Record "Cust. Ledger Entry";
                    MAJ_relance: Record "Relance Telephonique";
                begin
                    IF CONFIRM('Voulez-vous validez ?') THEN BEGIN
                        MAJ_relance.COPY(Rec);
                        IF MAJ_relance.FINDFIRST() THEN
                            REPEAT
                                Ecriture_client.GET(MAJ_relance."N° sequence client");
                                Ecriture_client."Last Issued Reminder Level" := MAJ_relance."Prochain niveau";
                                Ecriture_client.MODIFY();
                                MAJ_relance.DELETE();
                            UNTIL MAJ_relance.NEXT() = 0;
                    END;
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        IF NOT Client.GET(Rec."N° client") THEN
            Client.INIT();

        Client.CALCFIELDS(Client.Balance);
    end;

    var
        Client: Record Customer;
        DtldCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
        CustLedgEntry: Record "Cust. Ledger Entry";
        Text19039943Lbl: Label 'Montant en Relance';

    procedure "Montant en relance"(): Decimal
    var
        rec_relance: Record "Relance Telephonique";
    begin
        rec_relance.SETCURRENTKEY("N° client");
        rec_relance.SETRANGE("N° client", Rec."N° client");
        rec_relance.CALCSUMS("Montant restant");
        EXIT(rec_relance."Montant restant");
    end;
}

