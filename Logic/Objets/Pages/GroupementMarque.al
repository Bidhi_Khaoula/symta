page 50059 "Groupement/Marque"
{
    CardPageID = "Groupement/Marque";
    PageType = List;
    SourceTable = "Groupement/Marque";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Code client"; Rec."Code client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code client field.';
                }
                field("Code Marque"; Rec."Code Marque")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Marque field.';
                }
            }
        }
    }

    actions
    {
    }
}

