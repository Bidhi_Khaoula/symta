page 50073 "Diff Stocks Physique Empl"
{
    CardPageID = "Diff Stocks Physique Empl";
    PageType = List;
    SourceTable = difference_stocks_physique_emp;

    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field(No_; Rec.No_)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No_ field.';
                }
                field(QE; Rec.QE)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the QE field.';
                }
                field(QP; Rec.QP)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the QP field.';
                }
            }
        }
    }

    actions
    {
    }
}

