page 50340 "Shipping Setup Setup"
{
    ApplicationArea = all;

    Caption = 'Paramètres transporteur';
    PageType = Card;
    SourceTable = "Shipping Setup";

    layout
    {
        area(content)
        {
            group("Général")
            {
                field("Compte Frais douane"; Rec."Compte Frais douane")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compte Frais douane field.';
                }
                field("Compte Frais annexe"; Rec."Compte Frais annexe")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compte Frais annexe field.';
                }
                field("Compte Frais d'emballage"; Rec."Compte Frais d'emballage")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compte Frais d''emballage field.';
                }
                field("Compte Cout de transport"; Rec."Compte Cout de transport")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Compte Cout de transport field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage();
    begin
        IF NOT Rec.FINDFIRST() THEN Rec.Insert();
    end;
}

