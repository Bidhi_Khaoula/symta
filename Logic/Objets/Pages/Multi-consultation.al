page 50033 "Multi -consultation"
{
    ApplicationArea = all;
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = Document;
    UsageCategory = Documents;
    SourceTable = "Multi consultation";
    SourceTableTemporary = true;

    layout
    {
        area(content)
        {
            group(Client)
            {
                Caption = 'Client';
                group("Group 1")
                {
                    ShowCaption = false;
                    group("Article")
                    {
                        ShowCaption = false;
                        field("N°Article"; "codN°Article")
                        {
                            Caption = 'N° Article';
                            // ShowCaption = false;
                            TableRelation = Item."No.";
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° Article field.';

                            trigger OnValidate()
                            var
                                ItemCross: Record "Item Reference";
                                FrmItemCross: Page "Item References";
                                CRLF: Text[2];
                            begin

                                // VD 18-03-2015 Suppression des espaces copier/coller Excel
                                CRLF[1] := 13;
                                CRLF[2] := 10;
                                "codN°Article" := DELCHR("codN°Article", '=', CRLF);



                                IF STRPOS("codN°Article", '*') <> 0 THEN BEGIN
                                    ItemCross.RESET();
                                    ItemCross.SETFILTER("Reference No.", '%1', "codN°Article");
                                    FrmItemCross.SETTABLEVIEW(ItemCross);
                                    FrmItemCross.LOOKUPMODE := TRUE;
                                    IF FrmItemCross.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                        FrmItemCross.GETRECORD(ItemCross);
                                        "codN°Article" := ItemCross."Reference No.";
                                    END

                                END;

                                IF "codN°Article" <> '' THEN BEGIN
                                    MAJRecArticles();


                                    "codN°Article" := RecArticle."No.";
                                    IF "codN°Article" <> '' THEN BEGIN
                                        // Formulaire des prix fournisseurs
                                        CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                                        //               MESSAGE('_%1',"codN°Article");
                                        CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti("codN°Article", quantité_demande, "codN°Fournisseur",
                                                                                                TypeCdeAchat);
                                    END;

                                    // AD Le 14-12-2009 => GDI -> Gestion multiréférence
                                    "codN°Article" := RecArticle."No. 2";
                                    // FIN AD Le 14-12-2009
                                END ELSE
                                    EffacerArticle();

                                IF "codN°Article" <> '' THEN
                                    affiche_tarif();

                                InitFactBox();

                                //CurrPage.UPDATE(FALSE);
                            end;
                        }
                        field(Description; RecArticle.Description)
                        {
                            Caption = 'Désignation';
                            Editable = false;
                            Enabled = false;
                            ShowCaption = false;
                            ApplicationArea = All;
                        }
                    }

                    field(InfoDivers; InfoDivers)
                    {
                        Caption = '          Info. diverses';
                        Editable = false;
                        Enabled = false;
                        HideValue = false;
                        Style = Unfavorable;
                        StyleExpr = TRUE;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the           Info. diverses field.';
                    }
                    field("Description 2"; RecArticle."Description 2")
                    {
                        Caption = '          Info. achat';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the           Info. achat field.';
                    }
                    field("Description 3"; RecArticle."Description 3")
                    {
                        Caption = '          Info vente';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the           Info vente field.';
                    }

                    group("Groupe N°Client")
                    {
                        ShowCaption = false;
                        field("N°Client"; "codN°Client")
                        {
                            Caption = 'N° Client';
                            // ShowCaption = false;
                            TableRelation = Customer."No.";
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the N° Client field.';

                            trigger OnValidate()
                            begin
                                Rec.VALIDATE("Customer No", "codN°Client");

                                IF "codN°Client" <> '' THEN BEGIN
                                    MAJRecClients();
                                    "codN°Client" := RecClient."No.";
                                END ELSE
                                    EffacerClient();

                                affiche_tarif();
                                InitFactBox();
                            end;
                        }
                        field(Name; RecClient.Name)
                        {
                            Caption = 'Nom';
                            Editable = false;
                            ShowCaption = false;
                            ApplicationArea = All;
                        }
                    }
                }
                field(Comment; RecArticle.Comment)
                {
                    Caption = 'Commentaires';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires field.';

                    trigger OnDrillDown()
                    begin
                        // >>>>> ajout cyril 15/05/01
                        RecLigneComment.SETFILTER("Table Name", 'Article');
                        RecLigneComment.SETFILTER("No.", RecArticle."No.");
                        CLEAR(FrmLigneComment);
                        FrmLigneComment.SETTABLEVIEW(RecLigneComment);
                        FrmLigneComment.SETRECORD(RecLigneComment);
                        IF RecArticle."No." <> '' THEN
                            FrmLigneComment.RUN();
                        // <<<<<
                    end;
                }
                field(Rec_HASLINKS; Rec.HASLINKS())
                {
                    Caption = 'Liens article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Liens article field.';
                }
                // field(magasin; code_mag)
                // {
                //     Caption = 'Magasin';
                //     Editable = false;
                //     TableRelation = Location;
                //     ApplicationArea = All;

                //     trigger OnValidate()
                //     begin
                //         IF ArticleChoisi THEN
                //             MAJRecArticles;
                //     end;
                // }
                field("No."; RecArticle."No.")
                {
                    Caption = 'Ref interne';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref interne field.';
                }
                field(TypeCdeVente; TypeCdeVente)
                {
                    Caption = 'Type de commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';

                    trigger OnValidate()
                    begin
                        IF ArticleChoisi THEN BEGIN
                            MAJRecArticles();
                            affiche_tarif();
                            InitFactBox();
                        END;
                    end;
                }
                field("Qté"; quantité_demande)
                {
                    Caption = 'Qté';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté field.';

                    trigger OnValidate()
                    begin
                        IF ArticleChoisi THEN
                            affiche_tarif();
                        InitFactBox();
                    end;
                }
                field("Zone / Emplacement"; CodeZone + ' / ' + CodeEmplacement)
                {
                    Caption = 'Zone / Emplacement';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone / Emplacement field.';
                }
                field("Nombre de devis web non export"; cu_webinfo.GetNbOfWebQuote(FALSE))
                {
                    Caption = 'Nombre de devis web non export';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre de devis web non export field.';
                }
                field("Nombre de devis web export"; cu_webinfo.GetNbOfWebQuote(TRUE))
                {
                    Caption = 'Nombre de devis web export';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nombre de devis web export field.';
                }
                field(AvailabilityDate; AvailabilityDate)
                {
                    Caption = 'Date de contrôle';
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date de contrôle field.';

                    trigger OnValidate()
                    begin
                        ERROR('TODO 2');
                        //AvailabilityDateOnAfterValidate;
                    end;
                }
                group("Groupe 2")
                {
                    ShowCaption = false;
                    field(ctrlChampMarque; ChampMarque)
                    {
                        Caption = 'Marque';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Marque field.';
                        Trigger OnValidate()
                        VAR
                            ItemCross: Record "Item Reference";

                            FrmItemCross: Page "Item References";
                            CRLF: Text[2];
                        BEGIN
                            // VD 18-03-2015 Suppression des espaces copier/coller Excel
                            CRLF[1] := 13;
                            CRLF[2] := 10;
                            "codN°Article" := DELCHR("codN°Article", '=', CRLF);



                            IF STRPOS("codN°Article", '*') <> 0 THEN BEGIN
                                ItemCross.RESET();
                                ItemCross.SETFILTER("Reference No.", '%1', "codN°Article");
                                FrmItemCross.SETTABLEVIEW(ItemCross);
                                FrmItemCross.LOOKUPMODE := TRUE;
                                IF FrmItemCross.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                    FrmItemCross.GETRECORD(ItemCross);
                                    "codN°Article" := ItemCross."Reference No.";
                                END

                            END;

                            IF "codN°Article" <> '' THEN BEGIN
                                MAJRecArticles();


                                "codN°Article" := RecArticle."No.";
                                IF "codN°Article" <> '' THEN BEGIN
                                    // Formulaire des prix fournisseurs
                                    CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                                    //               MESSAGE('_%1',"codNøArticle");
                                    CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti("codN°Article", quantité_demande, "codN°Fournisseur",
                                                                                            TypeCdeAchat);
                                END;

                                // AD Le 14-12-2009 => GDI -> Gestion multir‚f‚rence
                                "codN°Article" := RecArticle."No. 2";
                                // FIN AD Le 14-12-2009
                            END ELSE
                                EffacerArticle();

                            IF "codN°Article" <> '' THEN
                                affiche_tarif();

                            InitFactBox();

                            //CurrPage.UPDATE(FALSE);
                        END;
                    }


                    field(ChampFamille; ChampFamille)
                    {
                        Caption = 'Famille';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Famille field.';
                    }


                    field(ChampSousFamille; ChampSousFamille)
                    {
                        Caption = 'Sous Famille';
                        Editable = false;
                        Enabled = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Sous Famille field.';
                    }


                }
                group("Groupe 3")
                {
                    ShowCaption = false;
                    group("Groupe 4")
                    {
                        ShowCaption = false;
                        field(gDetailStockMort; gDetailStockMort)
                        {
                            Caption = 'Stock mort';
                            Editable = false;
                            ApplicationArea = all;
                            ToolTip = 'Specifies the value of the Stock mort field.';
                        }

                        field("date maj stock mort"; RecArticle."date maj stock mort")
                        {
                            Caption = '          Date';
                            Editable = false;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the           Date field.';
                        }
                        field("user maj stock mort"; RecArticle."user maj stock mort")
                        {
                            Caption = '          Utilisateur';
                            Editable = false;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the           Utilisateur field.';
                        }
                    }
                    group("Groupe 5")
                    {
                        ShowCaption = false;
                        field(Stocké; gDetailStocke)
                        {
                            Caption = 'Stocké';
                            Editable = false;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the Stocké field.';
                        }
                        field("date maj stocké"; RecArticle."date maj stocké")
                        {
                            Caption = '          Date';
                            Editable = false;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the           Date field.';
                        }
                        field("user maj stocké"; RecArticle."user maj stocké")
                        {
                            Caption = '          Utilisateur';
                            Editable = false;
                            ApplicationArea = All;
                            ToolTip = 'Specifies the value of the           Utilisateur field.';
                        }
                    }
                }
                field("Sales multiple"; RecArticle."Sales multiple")
                {
                    Caption = 'Multiple de vente';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple de vente field.';
                }
                field(Marque; RecArticle."Manufacturer Code")
                {
                    Caption = 'Marque';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Marque field.';
                }
                field(NomAppro; NomAppro)
                {
                    Caption = 'Code Appro';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Appro field.';
                }
                field(MarqueBis; RecArticle."Net Weight")
                {
                    Caption = 'Poids';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
                field(fusion; recarticle3."No. 2")
                {
                    Caption = 'Fusion en attente sur';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fusion en attente sur field.';
                }
                field("Dernière entrée"; RecArticle."Date dernière entrée")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date dernière entrée field.';
                }
                field("Dernière sortie"; RecArticle."Date Dernière Sortie")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Dernière Sortie field.';
                }
            }
            group(Fournisseur)
            {
                Caption = 'Fournisseur';
                group("Groupe 6")
                {
                    ShowCaption = false;
                    field("N°Fournisseur"; "codN°Fournisseur")
                    {
                        Caption = 'N° Fournisseur';
                        // ShowCaption = false;
                        TableRelation = Vendor."No.";
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the N° Fournisseur field.';

                        trigger OnLookup(var Text: Text): Boolean
                        begin
                            RecFournisseur.RESET();
                            CLEAR(FrmListeFournisseurs);
                            FrmListeFournisseurs.SETTABLEVIEW(RecFournisseur);
                            FrmListeFournisseurs.LOOKUPMODE(TRUE);
                            IF FrmListeFournisseurs.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                FrmListeFournisseurs.GETRECORD(RecFournisseur);
                                FournisseurChoisi := TRUE;
                                // >>>>> ajout cyril 15/05/01
                                //ChercherCommentaireFournisseur.CommenteFournisseur (RecFournisseur);
                                //CommentaireFournisseur := ChercherCommentaireFournisseur.ExisteCommentaire;
                                // <<<<<
                                "codN°Fournisseur" := RecFournisseur."No.";
                            END;

                            IF "codN°Fournisseur" <> '' THEN BEGIN
                                MAJRecFournisseurs();
                                "codN°Fournisseur" := RecFournisseur."No.";
                            END ELSE
                                Effacerfournisseur();

                            affiche_tarif();
                        end;

                        trigger OnValidate()
                        begin

                            IF "codN°Fournisseur" <> '' THEN
                                MAJRecFournisseurs()

                            ELSE
                                Effacerfournisseur();

                            affiche_tarif();
                            InitFactBox();
                        end;
                    }
                    field("Vendor Name"; RecFournisseur.Name)
                    {
                        Caption = 'Nom';
                        Editable = false;
                        ShowCaption = false;
                        ApplicationArea = All;
                    }

                    field("N°ArticleBis"; "codN°Article")
                    {
                        Caption = 'N° Article';
                        // ShowCaption = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the N° Article field.';

                        trigger OnLookup(var Text: Text): Boolean
                        begin
                            RecArticle.RESET();
                            CLEAR(FrmListeArticles);

                            FrmListeArticles.SETTABLEVIEW(RecArticle);
                            FrmListeArticles.LOOKUPMODE(TRUE);
                            IF FrmListeArticles.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                                FrmListeArticles.GETRECORD(RecArticle);
                                ArticleChoisi := TRUE;
                                // >>>>> ajout cyril 15/05/01
                                //ChercherCommentaireArticle.RUN (RecArticle);
                                //CommentaireArticle := ChercherCommentaireArticle.ExisteCommentaire;
                                "codN°Article" := RecArticle."No.";
                                // <<<<<
                            END;

                            IF "codN°Article" <> '' THEN BEGIN
                                MAJRecArticles();
                                "codN°Article" := RecArticle."No.";
                                IF RecArticle."Vendor No." <> '' THEN BEGIN
                                    "codN°Fournisseur" := RecArticle."Vendor No.";
                                    IF "codN°Fournisseur" <> '' THEN BEGIN
                                        MAJRecFournisseurs();
                                        "codN°Fournisseur" := RecFournisseur."No.";
                                    END ELSE
                                        Effacerfournisseur();
                                END;
                            END ELSE
                                EffacerArticle();

                            affiche_tarif();
                        end;

                        trigger OnValidate()
                        begin
                            IF "codN°Article" <> '' THEN BEGIN
                                MAJRecArticles();
                                "codN°Article" := RecArticle."No.";
                                // Formulaire des prix fournisseurs
                                CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                                CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti("codN°Article", quantité_demande, "codN°Fournisseur"
                                                                                        , TypeCdeAchat);

                            END ELSE
                                EffacerArticle();

                            //MC Analyse complémentaire 11-05-2010
                            //Pour récupérer le fournisseur et les infos associées
                            IF RecArticle."Vendor No." <> '' THEN BEGIN
                                "codN°Fournisseur" := RecArticle."Vendor No.";
                                IF "codN°Fournisseur" <> '' THEN BEGIN
                                    MAJRecFournisseurs();
                                    "codN°Fournisseur" := RecFournisseur."No.";
                                END ELSE
                                    Effacerfournisseur();
                            END;




                            affiche_tarif();

                            InitFactBox();
                            // AD Le 14-12-2009 => GDI -> Gestion multiréférence
                            "codN°Article" := RecArticle."No. 2";
                            // FIN AD Le 14-12-2009
                        end;
                    }
                    field("Description Article"; RecArticle.Description)
                    {
                        Caption = 'Désignation';
                        Editable = false;
                        ShowCaption = false;
                        ApplicationArea = All;
                    }

                    group("Groupe 7")
                    {
                        ShowCaption = false;
                        field(""; '')
                        {
                            ApplicationArea = all;
                            Editable = false;
                            HideValue = true;
                            ShowCaption = false;
                            Visible = false;
                        }
                        field("Désignation"; RecArticle."Description 2")
                        {
                            Caption = 'Désignation';
                            Editable = false;
                            ShowCaption = false;
                            ApplicationArea = All;
                        }
                    }

                }
                field(TypeCdeAchat; TypeCdeAchat)
                {
                    Caption = 'Type de commande';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type de commande field.';

                    trigger OnValidate()
                    begin
                        IF ArticleChoisi THEN BEGIN
                            MAJRecArticles();
                            affiche_tarif();
                            "codN°Article" := RecArticle."No.";
                            IF "codN°Article" <> '' THEN BEGIN
                                // Formulaire des prix fournisseurs
                                CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                                CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti("codN°Article", quantité_demande, "codN°Fournisseur",
                                                                                        TypeCdeAchat);
                            END;
                            // AD Le 14-12-2009 => GDI -> Gestion multiréférence
                            "codN°Article" := RecArticle."No. 2";
                            // FIN AD Le 14-12-2009
                        END;
                    end;
                }
                field(magasinBis; code_mag)
                {
                    Caption = 'Magasin';
                    TableRelation = Location;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Magasin field.';

                    trigger OnValidate()
                    begin
                        IF ArticleChoisi THEN
                            MAJRecArticles();
                    end;
                }
                field("<Control1000000064Bis>"; quantité_demande)
                {
                    Caption = 'Qté';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté field.';

                    trigger OnValidate()
                    begin
                        IF ArticleChoisi THEN
                            affiche_tarif();
                    end;
                }
                field(AverageCostLCY; AverageCostLCY)
                {
                    AutoFormatType = 2;
                    Caption = 'Prix de revient(coût unitaire)';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix de revient(coût unitaire) field.';

                    trigger OnDrillDown()
                    begin
                        CODEUNIT.RUN(CODEUNIT::"Show Avg. Calc. - Item", RecArticle);
                    end;
                }
                group(Stock)
                {
                    Caption = 'Stock';
                    field("Stock physique Bis"; RecArticle.Inventory)
                    {
                        Caption = 'Stock physique(qté)';
                        Editable = false;
                        Enabled = true;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Stock physique(qté) field.';

                        trigger OnDrillDown()
                        begin
                            IF NOT ArticleChoisi THEN
                                ERROR('Vous devez spécifier au préalable un article.');

                            RecEcrituresArticles.RESET();
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                            IF code_mag <> '' THEN
                                RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                            CLEAR(FrmEcrituresArticle);
                            FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                            FrmEcrituresArticle.RUNMODAL();
                        end;
                    }
                    field("Qté sur commande vente Bis"; RecArticle."Qty. on Sales Order")
                    {
                        Caption = 'Qté sur cde vente';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Qté sur cde vente field.';

                        trigger OnDrillDown()
                        var
                            ligne_vente: Record "Sales Line";
                            frm_ligne_vente: Page "Sales Lines";
                        begin
                            IF ArticleChoisi THEN BEGIN
                                ligne_vente.INIT();
                                ligne_vente.RESET();
                                ligne_vente.SETRANGE(ligne_vente."Document Type", ligne_vente."Document Type"::Order);
                                ligne_vente.SETRANGE(ligne_vente.Type, ligne_vente.Type::Item);
                                ligne_vente.SETRANGE(ligne_vente."No.", RecArticle."No.");
                                ligne_vente.SETCURRENTKEY(ligne_vente."Document Type", ligne_vente.Type, ligne_vente."No.");
                                IF code_mag <> '' THEN
                                    ligne_vente.SETRANGE(ligne_vente."Location Code", code_mag);

                                frm_ligne_vente.EDITABLE(FALSE);
                                frm_ligne_vente.SETTABLEVIEW(ligne_vente);
                                frm_ligne_vente.RUNMODAL();
                            END
                        end;
                    }
                    field("Qté sur commande achat Bis"; RecArticle."Qty. on Purch. Order")
                    {
                        Caption = 'Qté sur cde achat';
                        DrillDownPageID = "Purchase Lines";
                        Editable = false;
                        Enabled = true;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Qté sur cde achat field.';

                        trigger OnDrillDown()
                        var
                            ligne_achat: Record "Purchase Line";
                            frm_achat: Page "Purchase Lines";
                        begin
                            IF ArticleChoisi THEN BEGIN
                                ligne_achat.INIT();
                                ligne_achat.RESET();
                                ligne_achat.SETRANGE(ligne_achat."Document Type", ligne_achat."Document Type"::Order);
                                ligne_achat.SETRANGE(ligne_achat.Type, ligne_achat.Type::Item);
                                ligne_achat.SETRANGE(ligne_achat."No.", RecArticle."No.");
                                ligne_achat.SETCURRENTKEY(ligne_achat."Document Type", ligne_achat.Type, ligne_achat."No.");
                                IF code_mag <> '' THEN
                                    ligne_achat.SETRANGE(ligne_achat."Location Code", code_mag);

                                frm_achat.EDITABLE(FALSE);
                                frm_achat.SETTABLEVIEW(ligne_achat);
                                frm_achat.RUNMODAL();
                            END
                        end;
                    }
                    field("Qté sur commande fabrication"; RecArticle."Qty. on Prod. Order")
                    {
                        Caption = 'Qté sur cde fabrication';
                        Editable = false;
                        Enabled = true;
                        Visible = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Qté sur cde fabrication field.';

                        trigger OnDrillDown()
                        var
                            ordre_fabrication: Record "Prod. Order Line";
                            frm_ordre_fabrication: Page "Prod. Order Line List";
                        begin
                            IF ArticleChoisi THEN BEGIN
                                ordre_fabrication.INIT();
                                ordre_fabrication.RESET();
                                ordre_fabrication.SETRANGE(ordre_fabrication."Item No.", RecArticle."No.");
                                ordre_fabrication.SETCURRENTKEY(ordre_fabrication."Item No.");
                                IF code_mag <> '' THEN
                                    ordre_fabrication.SETRANGE(ordre_fabrication."Location Code", code_mag);

                                frm_ordre_fabrication.EDITABLE(FALSE);
                                frm_ordre_fabrication.SETTABLEVIEW(ordre_fabrication);
                                frm_ordre_fabrication.RUNMODAL();
                            END
                        end;
                    }
                }
                group(Prix)
                {
                    Caption = 'Prix';
                    field(prix_four; prix_four)
                    {
                        Caption = 'Prix d''achat (DS)';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Prix d''achat (DS) field.';
                    }
                    field(prix_four_devise; prix_four_devise)
                    {
                        Caption = 'Prix d''achat (DF)';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Prix d''achat (DF) field.';
                    }
                    field(remise1_four; remise1_four)
                    {
                        Caption = 'Remise 1';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Remise 1 field.';
                    }
                    field(total_four; total_four)
                    {
                        Caption = 'Total';
                        Editable = false;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Total field.';

                        trigger OnValidate()
                        begin
                            IF ArticleChoisi THEN
                                affiche_tarif();
                        end;
                    }
                }
            }
            part(frm_BufferPrice; "Vendor Price Buffer")
            {
                ApplicationArea = All;
            }
            part(Frm_RemiseVente; "Multi Consultation RemiseVente")
            {
                Caption = 'Remises Ventes Actives';
                ApplicationArea = All;
            }
            part(Frm_PrixVente; "Multi Consultation PrixVente")
            {
                Caption = 'Prix Ventes Actifs';
                ApplicationArea = All;
            }
        }
        area(factboxes)
        {
            part("Nb Devis Web"; "Nb Devis Web Factbox")
            {
                Caption = 'Devis Web';
                ApplicationArea = All;
            }
            part("Multi Consultation Qty Factbox"; "Multi Consultation Qty Factbox")
            {
                Caption = 'Quantités';
                SubPageLink = "No." = FIELD("Item No"),
                              "Location Filter" = FIELD("Location Code");
                Visible = true;
                ApplicationArea = All;
            }
            part("Pri Factbox"; "Multi Consultation Pri Factbox")
            {
                Caption = 'Prix';
                SubPageLink = "No." = FIELD("Item No");
                ApplicationArea = All;
            }
            part("Hist Factbox"; "Sales Hist. Sell-to FactBox")
            {
                SubPageLink = "No." = FIELD("Customer No");
                Visible = true;
                ApplicationArea = All;
            }
            part("Item Web Info"; "Item Web Info")
            {
                SubPageLink = "No." = FIELD("Item No");
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group("Historique client")
            {
                Caption = 'Historique client';
                action(Documents)
                {
                    Caption = 'Documents';
                    Image = Document;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Documents action.';

                    trigger OnAction()
                    var
                        PageSalesHist: Page "Sales Hist. Sell-to FactBox";
                    begin
                        //SalesInfoPaneMgt.LookupCustSalesHistory(RecEntêteVente,RecClient."No.",FALSE);
                        CLEAR(PageSalesHist);
                        PageSalesHist.SETTABLEVIEW(RecClient);
                        PageSalesHist.SETRECORD(RecClient);
                        PageSalesHist.RUNMODAL();
                    end;
                }
                action("Ecritures valeurs Ventes")
                {
                    Caption = 'Ecritures valeurs Ventes';
                    Image = ValueLedger;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ecritures valeurs Ventes action.';

                    trigger OnAction()
                    begin

                        IF ("codN°Client" = '') AND ("codN°Article" = '') THEN
                            EXIT;

                        RecEcrituresValeurs.RESET();
                        CLEAR(FrmEcrituresValeurs);
                        RecEcrituresValeurs.SETCURRENTKEY("Source Type", "Source No.", "Item No.");

                        IF ("codN°Client" <> '') THEN
                            RecEcrituresValeurs.SETRANGE("Source No.", "codN°Client");

                        IF ("codN°Article" <> '') THEN
                            RecEcrituresValeurs.SETRANGE("Item No.", RecArticle."No.");
                        RecEcrituresValeurs.SETRANGE("Expected Cost", FALSE);
                        RecEcrituresValeurs.SETRANGE("Item Charge No.", '');
                        RecEcrituresValeurs.SETRANGE("Item Ledger Entry Type", RecEcrituresValeurs."Item Ledger Entry Type"::Sale);
                        FrmEcrituresValeurs.SETTABLEVIEW(RecEcrituresValeurs);

                        FrmEcrituresValeurs.RUNMODAL();
                    end;
                }
                action("Ecritures valeurs Achats")
                {
                    Caption = 'Ecritures valeurs Achats';
                    Image = ValueLedger;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ecritures valeurs Achats action.';

                    trigger OnAction()
                    begin
                        IF ("codN°Client" = '') AND ("codN°Article" = '') THEN
                            EXIT;

                        RecEcrituresValeurs.RESET();
                        CLEAR(FrmEcrituresValeurs);
                        RecEcrituresValeurs.SETCURRENTKEY("Source Type", "Source No.", "Item No.");
                        IF ("codN°Client" <> '') THEN
                            RecEcrituresValeurs.SETRANGE("Source No.", "codN°Client");

                        IF ("codN°Article" <> '') THEN
                            RecEcrituresValeurs.SETRANGE("Item No.", RecArticle."No.");
                        RecEcrituresValeurs.SETRANGE("Expected Cost", FALSE);
                        RecEcrituresValeurs.SETRANGE("Item Charge No.", '');
                        RecEcrituresValeurs.SETRANGE("Item Ledger Entry Type", RecEcrituresValeurs."Item Ledger Entry Type"::Purchase);
                        FrmEcrituresValeurs.SETTABLEVIEW(RecEcrituresValeurs);
                        FrmEcrituresValeurs.RUNMODAL();
                    end;
                }
            }
            group("&Stock client")
            {
                Caption = '&Stock client';
                action(Rotation)
                {
                    Caption = 'Rotation';
                    Image = Turnover;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Rotation action.';

                    trigger OnAction()
                    var
                        ItemsByPeriod: Page "Item Availability by Periods";
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        ItemsByPeriod.SETRECORD(RecArticle);
                        ItemsByPeriod.RUN();
                    end;
                }
                action("&Emplacement")
                {
                    Caption = '&Emplacement';
                    Image = BinContent;
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Emplacement action.';

                    trigger OnAction()
                    var
                        BinContent: Record "Bin Content";
                    begin
                        BinContent.ShowBinContents2(code_mag, RecArticle."No.", '', '');
                    end;
                }
            }
            group("&Commandes client")
            {
                Caption = '&Commandes client';
                action(Commandes)
                {
                    Caption = 'Commandes';
                    Image = Document;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Commandes action.';

                    trigger OnAction()
                    begin
                        RecEntêteVente.RESET();
                        RecEntêteVente.CLEARMARKS();

                        IF (NOT ClientChoisi) AND ("codN°BL" = '') THEN
                            ERROR('Vous devez spécifier au préalable un client');

                        IF (ClientChoisi) AND (ArticleChoisi) AND ("codN°BL" <> '') THEN BEGIN
                            IF NOT RecExpéditionVente.GET("codN°BL") THEN
                                ERROR('Ce numéro de B.L. n''existe pas');
                            IF RecExpéditionVente."Sell-to Customer No." <> RecClient."No." THEN
                                ERROR('Ce client n''a pas eu ce numéro de B.L.');
                            IF NOT RecEntêteVente.GET(RecEntêteVente."Document Type"::Order, RecExpéditionVente."Order No.") THEN
                                ERROR('La commande associée à ce numéro de B.L. est soldée.');
                            RecLignesVente.RESET();
                            RecLignesVente.SETRANGE(RecLignesVente."Document Type", RecEntêteVente."Document Type");
                            RecLignesVente.SETRANGE(RecLignesVente."Document No.", RecEntêteVente."No.");
                            RecLignesVente.SETRANGE(RecLignesVente.Type, RecLignesVente.Type::Item);
                            RecLignesVente.SETRANGE(RecLignesVente."No.", RecArticle."No.");
                            IF RecLignesVente.FIND('-') THEN
                                RecEntêteVente.MARK(TRUE)
                            ELSE
                                ERROR('Ce numéro de B.L. ne contient pas cet article dans la commande associée.');
                        END;

                        IF (ClientChoisi) AND (ArticleChoisi) AND ("codN°BL" = '') THEN BEGIN
                            RecEntêteVente.SETRANGE(RecEntêteVente."Document Type", RecEntêteVente."Document Type"::Order);
                            RecEntêteVente.SETRANGE(RecEntêteVente."Sell-to Customer No.", RecClient."No.");
                            IF NOT RecEntêteVente.FIND('-') THEN
                                ERROR('Aucune commande ouverte pour ce client.');
                            TrouvéCommande := FALSE;
                            REPEAT
                                RecLignesVente.RESET();
                                RecLignesVente.SETRANGE(RecLignesVente."Document Type", RecEntêteVente."Document Type");
                                RecLignesVente.SETRANGE(RecLignesVente."Document No.", RecEntêteVente."No.");
                                RecLignesVente.SETRANGE(RecLignesVente.Type, RecLignesVente.Type::Item);
                                RecLignesVente.SETRANGE(RecLignesVente."No.", RecArticle."No.");
                                IF RecLignesVente.FIND('-') THEN BEGIN
                                    RecEntêteVente.MARK(TRUE);
                                    TrouvéCommande := TRUE;
                                END;
                            UNTIL RecEntêteVente.NEXT() = 0;
                            IF NOT TrouvéCommande THEN
                                ERROR('Aucune commande ouverte pour ce client avec cet article.');
                        END;

                        IF (ClientChoisi) AND (NOT ArticleChoisi) AND ("codN°BL" <> '') THEN BEGIN
                            IF NOT RecExpéditionVente.GET("codN°BL") THEN
                                ERROR('Ce numéro de B.L. n''existe pas');
                            IF RecExpéditionVente."Sell-to Customer No." <> RecClient."No." THEN
                                ERROR('Ce client n''a pas eu ce numéro de B.L.');
                            IF NOT RecEntêteVente.GET(RecEntêteVente."Document Type"::Order, RecExpéditionVente."Order No.") THEN
                                ERROR('La commande associée à ce numéro de B.L. est soldée.');
                            RecEntêteVente.MARK(TRUE);
                        END;

                        IF (ClientChoisi) AND (NOT ArticleChoisi) AND ("codN°BL" = '') THEN BEGIN
                            RecEntêteVente.SETRANGE(RecEntêteVente."Document Type", RecEntêteVente."Document Type"::Order);
                            RecEntêteVente.SETRANGE(RecEntêteVente."Sell-to Customer No.", RecClient."No.");
                            IF NOT RecEntêteVente.FIND('-') THEN
                                ERROR('Aucune commande ouverte pour ce client.');
                            REPEAT
                                RecEntêteVente.MARK(TRUE);
                            UNTIL RecEntêteVente.NEXT() = 0;
                        END;

                        IF (NOT ClientChoisi) AND (ArticleChoisi) AND ("codN°BL" <> '') THEN BEGIN
                            IF NOT RecExpéditionVente.GET("codN°BL") THEN
                                ERROR('Ce numéro de B.L. n''existe pas');
                            IF NOT RecEntêteVente.GET(RecEntêteVente."Document Type"::Order, RecExpéditionVente."Order No.") THEN
                                ERROR('La commande associée à ce numéro de B.L. est soldée.');
                            RecLignesVente.RESET();
                            RecLignesVente.SETRANGE(RecLignesVente."Document Type", RecEntêteVente."Document Type");
                            RecLignesVente.SETRANGE(RecLignesVente."Document No.", RecEntêteVente."No.");
                            RecLignesVente.SETRANGE(RecLignesVente.Type, RecLignesVente.Type::Item);
                            RecLignesVente.SETRANGE(RecLignesVente."No.", RecArticle."No.");
                            IF RecLignesVente.FIND('-') THEN
                                RecEntêteVente.MARK(TRUE)
                            ELSE
                                ERROR('Ce numéro de B.L. ne contient pas cet article dans la commande associée.');
                        END;

                        IF (NOT ClientChoisi) AND (NOT ArticleChoisi) AND ("codN°BL" <> '') THEN BEGIN
                            IF NOT RecExpéditionVente.GET("codN°BL") THEN
                                ERROR('Ce numéro de B.L. n''existe pas');
                            IF NOT RecEntêteVente.GET(RecEntêteVente."Document Type"::Order, RecExpéditionVente."Order No.") THEN
                                ERROR('La commande associée à ce numéro de B.L. est soldée.');
                            RecEntêteVente.MARK(TRUE);
                        END;

                        RecEntêteVente.MARKEDONLY(TRUE);

                        CLEAR(FrmCommandesClients);
                        FrmCommandesClients.SETTABLEVIEW(RecEntêteVente);
                        FrmCommandesClients.EDITABLE := FALSE;
                        //FrmCommandesClients.RendreInactifBoutons;
                        FrmCommandesClients.RUNMODAL();
                    end;
                }
                action("Lignes commandes")
                {
                    Caption = 'Lignes commandes';
                    Image = Line;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Lignes commandes action.';

                    trigger OnAction()
                    var
                        ligne_vente: Record "Sales Line";
                        frm_ligne_vente: Page "Sales Lines";
                    begin
                        ligne_vente.INIT();
                        ligne_vente.RESET();

                        IF (NOT ClientChoisi) THEN
                            ERROR('Vous devez spécifier au préalable un client');
                        ligne_vente.SETRANGE(ligne_vente."Sell-to Customer No.", RecClient."No.");
                        ligne_vente.SETRANGE(ligne_vente."Document Type", ligne_vente."Document Type"::Order);
                        ligne_vente.SETRANGE(ligne_vente.Type, ligne_vente.Type::Item);
                        /*
                            IF code_mag<>'' THEN
                             ligne_vente.SETRANGE(ligne_vente."Location Code",code_mag);
                        */

                        IF ArticleChoisi THEN
                            ligne_vente.SETRANGE(ligne_vente."No.", RecArticle."No.");
                        //ligne_vente.SETCURRENTKEY(ligne_vente."Document Type",ligne_vente.Type,ligne_vente."Sell-to Customer No.",ligne_vente."No.");

                        frm_ligne_vente.EDITABLE(FALSE);
                        frm_ligne_vente.SETTABLEVIEW(ligne_vente);
                        frm_ligne_vente.RUNMODAL();

                    end;
                }
                action("Lignes devis")
                {
                    Caption = 'Lignes devis';
                    Image = Quote;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Lignes devis action.';

                    trigger OnAction()
                    var
                        ligne_vente: Record "Sales Line";
                        frm_ligne_vente: Page "Sales Lines";
                    begin
                        ligne_vente.INIT();
                        ligne_vente.RESET();

                        IF (NOT ClientChoisi) THEN
                            ERROR('Vous devez spécifier au préalable un client');

                        ligne_vente.SETRANGE(ligne_vente."Sell-to Customer No.", RecClient."No.");
                        ligne_vente.SETRANGE(ligne_vente."Document Type", ligne_vente."Document Type"::Quote);
                        ligne_vente.SETRANGE(ligne_vente.Type, ligne_vente.Type::Item);
                        /*
                            IF code_mag<>'' THEN
                             ligne_vente.SETRANGE(ligne_vente."Location Code",code_mag);
                        */

                        IF ArticleChoisi THEN
                            ligne_vente.SETRANGE(ligne_vente."No.", RecArticle."No.");
                        //ligne_vente.SETCURRENTKEY(ligne_vente."Document Type",ligne_vente.Type,ligne_vente."Sell-to Customer No.",ligne_vente."No.");

                        frm_ligne_vente.EDITABLE(FALSE);
                        frm_ligne_vente.SETTABLEVIEW(ligne_vente);
                        frm_ligne_vente.RUNMODAL();

                    end;
                }
            }
            group("&Mouvements client")
            {
                Caption = '&Mouvements client';
                action(Ventes)
                {
                    Caption = 'Ventes';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ventes action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Entry Type", RecEcrituresArticles."Entry Type"::Sale);

                        // AD Le 13-11-2015 => MIG2015
                        IF ClientChoisi THEN
                            RecEcrituresArticles.SETRANGE("Source No.", RecClient."No.");

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Achats)
                {
                    Caption = 'Achats';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Achats action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Entry Type", RecEcrituresArticles."Entry Type"::Purchase);

                        //IF FournisseurChoisi THEN
                        //  RecEcrituresArticles.SETRANGE (RecEcrituresArticles."N° client / fournisseur", RecFournisseur."N°");

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Tous)
                {
                    Caption = 'Tous';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Tous action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Rotation1)
                {
                    Caption = 'Rotation';
                    Image = Turnover;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Rotation action.';

                    trigger OnAction()
                    var
                        FrmRotationArticle: Page "Item Turnover";
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        CLEAR(FrmRotationArticle);
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE("Location Code", code_mag);
                        RecArticle.SETRANGE("No.", RecArticle."No.");
                        FrmRotationArticle.SETTABLEVIEW(RecArticle);
                        FrmRotationArticle.RUNMODAL();
                        RecArticle.SETRANGE("No.");
                    end;
                }
                separator("  ")
                {
                }
                action("Stock MiniLoad")
                {
                    Caption = 'Stock MiniLoad';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Stock MiniLoad action.';

                    trigger OnAction()
                    var

                        rec_Message: Record "Message receive from  MINILOAD";
                        cu_MiniLoad: Codeunit "Gestion MINILOAD";
                        frm_stock: Page "Stock par MINILOAD";

                    begin
                        IF "codN°Article" = '' THEN EXIT;

                        RecArticle.SETRECFILTER();
                        cu_MiniLoad.Article_MsgPST(RecArticle, code_mag);
                        CLEAR(cu_MiniLoad);
                        cu_MiniLoad.GetMsgSTO(RecArticle."No.");

                        rec_Message.RESET();
                        rec_Message.SETRANGE(Code, RecArticle."No.");
                        rec_Message.SETRANGE("Location Code", code_mag);
                        rec_Message.FINDLAST();
                        rec_Message.SETRECFILTER();
                        CLEAR(frm_stock);
                        frm_stock.SETTABLEVIEW(rec_Message);
                        frm_stock.RUN();
                    end;
                }
            }
            group("&Commandes fournisseur")
            {
                Caption = '&Commandes fournisseur';
                action("Commandes &ouvertes")
                {
                    Caption = 'Commandes &ouvertes';
                    Image = Document;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Commandes &ouvertes action.';

                    trigger OnAction()
                    begin
                        RecEntêteVente.RESET();
                        RecEntêteVente.CLEARMARKS();

                        IF NOT FournisseurChoisi THEN
                            ERROR('Vous devez spécifier au préalable un client');

                        RecEntêteAchat.SETRANGE("Document Type", RecEntêteAchat."Document Type"::Order);
                        RecEntêteAchat.SETRANGE(RecEntêteAchat."Buy-from Vendor No.", RecFournisseur."No.");
                        IF NOT RecEntêteAchat.FIND('-') THEN
                            ERROR('Aucune commande ouverte pour ce client.');
                        REPEAT
                            RecEntêteAchat.MARK(TRUE);
                        UNTIL RecEntêteAchat.NEXT() = 0;



                        RecEntêteAchat.MARKEDONLY(TRUE);

                        CLEAR(FrmCommandesFournisseurs);
                        FrmCommandesFournisseurs.SETTABLEVIEW(RecEntêteAchat);
                        FrmCommandesFournisseurs.EDITABLE := FALSE;
                        FrmCommandesFournisseurs.RUNMODAL();
                    end;
                }
                action("Lignes ouvertes")
                {
                    Caption = 'Lignes ouvertes';
                    Image = Line;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Lignes ouvertes action.';

                    trigger OnAction()
                    var
                        ligne_achat: Record "Purchase Line";
                        frm_ligne_achat: Page "Purchase Lines";
                    begin
                        ligne_achat.INIT();
                        ligne_achat.RESET();

                        IF (NOT FournisseurChoisi) THEN
                            ERROR('Vous devez spécifier au préalable un client');
                        ligne_achat.SETRANGE(ligne_achat."Buy-from Vendor No.", RecFournisseur."No.");
                        ligne_achat.SETRANGE("Document Type", ligne_achat."Document Type"::Order);
                        ligne_achat.SETRANGE(Type, ligne_achat.Type::Item);
                        /*
                            IF code_mag<>'' THEN
                             ligne_achat.SETRANGE(ligne_achat."Location Code",code_mag);
                        */

                        IF ArticleChoisi THEN
                            ligne_achat.SETRANGE("No.", RecArticle."No.");
                        // ligne_achat.SETCURRENTKEY("Document Type",Type,ligne_achat."Buy-from Vendor No.","No.");

                        frm_ligne_achat.EDITABLE(FALSE);
                        frm_ligne_achat.SETTABLEVIEW(ligne_achat);
                        frm_ligne_achat.RUNMODAL();

                    end;
                }
            }
            group("&Mouvements fournisseur")
            {
                Caption = '&Mouvements fournisseur';
                action(Ventes2)
                {
                    Caption = 'Ventes';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Ventes action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Entry Type", RecEcrituresArticles."Entry Type"::Sale);

                        //IF ClientChoisi THEN
                        //  RecEcrituresArticles.SETRANGE (RecEcrituresArticles."N° client / fournisseur", RecClient."No.");

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Achats2)
                {
                    Caption = 'Achats';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Achats action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Entry Type", RecEcrituresArticles."Entry Type"::Purchase);

                        //IF FournisseurChoisi THEN
                        //  RecEcrituresArticles.SETRANGE (RecEcrituresArticles."N° client / fournisseur", RecFournisseur."N°");

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Tous2)
                {
                    Caption = 'Tous';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Tous action.';

                    trigger OnAction()
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        RecEcrituresArticles.RESET();
                        RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Item No.", RecArticle."No.");
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE(RecEcrituresArticles."Location Code", code_mag);

                        CLEAR(FrmEcrituresArticle);
                        FrmEcrituresArticle.SETTABLEVIEW(RecEcrituresArticles);
                        FrmEcrituresArticle.RUNMODAL();
                    end;
                }
                action(Rotation2)
                {
                    Caption = 'Rotation';
                    Image = Turnover;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Rotation action.';

                    trigger OnAction()
                    var
                        FrmRotationArticle: Page "Item Turnover";
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');

                        CLEAR(FrmRotationArticle);
                        IF code_mag <> '' THEN
                            RecEcrituresArticles.SETRANGE("Location Code", code_mag);

                        FrmRotationArticle.SETTABLEVIEW(RecArticle);
                        FrmRotationArticle.RUNMODAL();
                    end;
                }
            }
        }
        area(processing)
        {
            action(Vider)
            {
                Image = DeleteAllBreakpoints;
                Promoted = true;
                PromotedIsBig = true;
                Visible = false;
                ApplicationArea = All;
                ToolTip = 'Executes the Vider action.';

                trigger OnAction()
                begin
                    VideEcran()
                end;
            }
            group(ArticleAct)
            {
                Caption = 'Article';
                action("Fiche Article")
                {
                    Caption = 'Fiche Article';
                    Image = Item;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Fiche';
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        LRecArticle: Record Item;
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');
                        CLEAR(FrmFicheArticle);
                        LRecArticle := RecArticle;
                        LRecArticle.SETRECFILTER();
                        LRecArticle.SETRANGE("Location Filter");
                        FrmFicheArticle.SETTABLEVIEW(LRecArticle);
                        FrmFicheArticle.SETRECORD(LRecArticle);
                        //FrmFicheArticle.EDITABLE := FALSE;
                        //FrmFicheArticle.ParamètreMultiConsultation;
                        FrmFicheArticle.RUNMODAL();
                    end;
                }
                action(BtnSubstitute)
                {
                    Caption = 'Etiquette';
                    Image = BarCode;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Etiquette action.';

                    trigger OnAction()
                    var
                        FrmEtq: Page "Etiquette NICELABEL";
                    begin
                        IF RecArticle."No." <> '' THEN BEGIN
                            FrmEtq.InitValeurs(RecArticle."No.", '', '', '', '');
                            FrmEtq.RUNMODAL();
                        END;
                    end;
                }
                action("Additional Item temp")
                {
                    Caption = 'Articles complémentaires';
                    ApplicationArea = All;
                    Promoted = true;
                    Image = CoupledItem;
                    PromotedCategory = Process;
                    ToolTip = 'Executes the Articles complémentaires action.';
                    trigger OnAction()
                    var
                        lAdditionalItem: Record "Additional Item";
                        lAdditionalItemList: Page "Additional Item List";

                    BEGIN

                        // CFR le 24/09/2021 - SFD20210201 articles supplémentaires : lien
                        CLEAR(lAdditionalItem);
                        lAdditionalItem.SETRANGE("Item No.", Rec."Item No");
                        lAdditionalItemList.SETTABLEVIEW(lAdditionalItem);
                        lAdditionalItemList.EDITABLE(FALSE);
                        lAdditionalItemList.RUNMODAL();

                    end;
                }
                group("Assemb&ly")
                {
                    Caption = 'Assemb&ly';
                    Image = AssemblyBOM;
                    action("<Action1100284054>")
                    {
                        Caption = 'Assembly BOM';
                        Image = BOM;
                        Promoted = true;
                        PromotedCategory = Process;
                        PromotedIsBig = false;
                        ApplicationArea = All;
                        ToolTip = 'Executes the Assembly BOM action.';

                        trigger OnAction()
                        var
                            LAssemblyBOM: Record "BOM Component";
                            FrmAssemblyBOM: Page "Assembly BOM";

                        begin

                            // AD Le 21-10-2016
                            CLEAR(LAssemblyBOM);
                            LAssemblyBOM.SETRANGE("Parent Item No.", Rec."Item No");
                            FrmAssemblyBOM.SETTABLEVIEW(LAssemblyBOM);
                            FrmAssemblyBOM.EDITABLE(FALSE);
                            FrmAssemblyBOM.RUNMODAL();
                        end;
                    }
                    action("Where-Used")
                    {
                        Caption = 'Where-Used';
                        Image = Track;
                        Promoted = true;
                        PromotedCategory = Process;
                        PromotedIsBig = false;
                        RunObject = Page "Where-Used List";
                        RunPageLink = Type = CONST(Item),
                                      "No." = FIELD("Item No");
                        RunPageView = SORTING(Type, "No.");
                        ApplicationArea = All;
                        ToolTip = 'Executes the Where-Used action.';
                    }
                }
                action(BtnSubstituteBis)
                {
                    Caption = 'Susbst.';
                    Image = ItemSubstitution;
                    Promoted = true;
                    PromotedCategory = Process;
                    Visible = BtnSubstituteVisible;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Susbst. action.';

                    trigger OnAction()
                    var
                        salesLine: Record "Sales Line";
                        ItemSubst: Codeunit "Item Subst.";
                        save_codeart: Code[20];
                    begin
                        salesLine.INIT();
                        salesLine."Document Type" := salesLine."Document Type"::Order;
                        salesLine."Document No." := 'MULTICONSSULT';
                        salesLine.Type := salesLine.Type::Item;
                        salesLine."No." := RecArticle."No.";
                        salesLine."Location Code" := code_mag;
                        salesLine.Quantity := quantité_demande;
                        // MCO Le 31-03-2015 => Régie
                        save_codeart := "codN°Article";
                        "codN°Article" := Format(ItemSubst.ItemSubstGet(salesLine));
                        IF "codN°Article" = '' THEN
                            "codN°Article" := save_codeart;
                        // MCO Le 31-03-2015 => Régie

                        IF "codN°Article" <> '' THEN BEGIN
                            MAJRecArticles();


                            "codN°Article" := RecArticle."No.";
                            IF "codN°Article" <> '' THEN BEGIN
                                // Formulaire des prix fournisseurs
                                CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                                CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti("codN°Article", quantité_demande, "codN°Fournisseur",
                                                                                        TypeCdeAchat);
                            END;

                            // AD Le 14-12-2009 => GDI -> Gestion multiréférence
                            "codN°Article" := RecArticle."No. 2";
                            // FIN AD Le 14-12-2009

                        END ELSE
                            EffacerArticle();

                        IF "codN°Article" <> '' THEN
                            affiche_tarif();
                    end;
                }
                action("Conso. Achat")
                {
                    Caption = 'Conso. Achat';
                    Image = Purchase;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Conso. Achat action.';

                    trigger OnAction()
                    var
                        FrmConsoArticle: Page "Sorties Mensuelles";
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');
                        CLEAR(FrmConsoArticle);
                        FrmConsoArticle.SETTABLEVIEW(RecArticle);
                        FrmConsoArticle.SETRECORD(RecArticle);
                        FrmConsoArticle.InitDispo(Dispo);
                        FrmConsoArticle.RUNMODAL();
                    end;
                }
            }
            group(Customer)
            {
                Caption = 'Client';
                action("Fiche Client")
                {
                    Caption = 'Fiche Client';
                    Image = Customer;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Fiche';
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        IF NOT ClientChoisi THEN
                            ERROR('Vous devez spécifier au préalable un client.');
                        CLEAR(FrmFicheClient);
                        FrmFicheClient.SETTABLEVIEW(RecClient);
                        FrmFicheClient.SETRECORD(RecClient);
                        // MC Le 05-12-2013 => Enlever cette ligne car cela s'annule à l'ouverture
                        //FrmFicheClient.EDITABLE := FALSE;
                        // FIN MC Le 05-12-2013 => Enlever cette ligne car cela s'annule à l'ouverture
                        //FrmFicheClient.ParamètreMultiConsultation;
                        FrmFicheClient.RUNMODAL();
                    end;
                }
                action(Interactions)
                {
                    Caption = 'Interactions';
                    Image = Interaction;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Interactions action.';

                    trigger OnAction()
                    var
                        Interaction: Record "Interaction Log Entry";
                        ContBusRel: Record "Contact Business Relation";
                        cont: Record Contact;
                    begin
                        WITH ContBusRel DO BEGIN
                            SETCURRENTKEY("Link to Table", "No.");
                            SETRANGE("Link to Table", "Link to Table"::Customer);
                            SETRANGE("No.", "codN°Client");
                            IF NOT FindFirst() THEN
                                EXIT;
                            cont.GET("Contact No.");
                            Interaction.RESET();
                            Interaction.SETRANGE("Contact Company No.", cont."No.");
                            PAGE.RUNMODAL(PAGE::"Interaction Log Entries", Interaction);
                        END;
                    end;
                }
                action(Contacts)
                {
                    Caption = 'Contacts';
                    Image = CustomerContact;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the Contacts action.';

                    trigger OnAction()
                    var
                        //  Interaction: Record "Interaction Log Entry";
                        ContBusRel: Record "Contact Business Relation";
                        cont: Record Contact;
                    begin
                        ContBusRel.SETCURRENTKEY("Link to Table", "No.");
                        ContBusRel.SETRANGE("Link to Table", ContBusRel."Link to Table"::Customer);
                        ContBusRel.SETRANGE("No.", "codN°Client");
                        IF ContBusRel.FIND('-') THEN
                            cont.SETRANGE("Company No.", ContBusRel."Contact No.")
                        // MCO Le 08-12
                        ELSE
                            cont.SETFILTER("Company No.", '*');

                        IF PAGE.RUNMODAL(0, cont) = ACTION::LookupOK THEN;
                    end;
                }
            }
            group(Fournisseur2)
            {
                Caption = 'Fournisseur';
                action("Fiche Fournisseur")
                {
                    Caption = 'Fiche Fournisseur';
                    Image = Vendor;
                    Promoted = true;
                    PromotedCategory = Process;
                    ToolTip = 'Fiche';
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        IF NOT FournisseurChoisi THEN
                            ERROR('Vous devez spécifier au préalable un Fournisseur.');
                        CLEAR(FrmFicheFournisseur);
                        FrmFicheFournisseur.SETTABLEVIEW(RecFournisseur);
                        FrmFicheFournisseur.SETRECORD(RecFournisseur);
                        FrmFicheFournisseur.EDITABLE := FALSE;
                        //FrmFicheFournisseur.ParamètreMultiConsultation;
                        FrmFicheFournisseur.RUNMODAL();
                    end;
                }
                action("&Stock")
                {
                    Caption = '&Stock';
                    Image = Inventory;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Stock action.';

                    trigger OnAction()
                    var
                        ItemsByLocation: Page "Item Availability by Location";
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR('Vous devez spécifier au préalable un article.');
                        /*CLEAR(FrmStockArticle);
                        FrmStockArticle.PassageParametres (FALSE, 'Aucun', RecArticle."No.", '', 0);
                        FrmStockArticle.RUNMODAL();
                        */
                        ItemsByLocation.SETRECORD(RecArticle);
                        ItemsByLocation.RUN();

                    end;
                }
                action("&Contenu emplacement")
                {
                    Caption = '&Contenu emplacement';
                    Image = BinContent;
                    Promoted = true;
                    PromotedCategory = Process;
                    ApplicationArea = All;
                    ToolTip = 'Executes the &Contenu emplacement action.';

                    trigger OnAction()
                    var
                        BinContent: Record "Bin Content";
                        ItemBinContent: Page "Bin Contents";
                        text00001Lbl: Label 'Vous devez spécifier au préalable un article.';
                    begin
                        IF NOT ArticleChoisi THEN
                            ERROR(text00001Lbl);
                        BinContent.RESET();
                        BinContent.SETRANGE("Location Code", code_mag);
                        BinContent.SETRANGE(BinContent."Item No.", RecArticle."No.");
                        ItemBinContent.SETTABLEVIEW(BinContent);
                        ItemBinContent.RUN();
                    end;
                }
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        InitFactBox();

        //CurrPage.UPDATE(FALSE);
    end;

    trigger OnClosePage()
    begin
        // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
        SetLinks('');
        // FIN CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
    end;

    trigger OnOpenPage()
    var
        "paramètres ventes": Record "Sales & Receivables Setup";
        "Warehouse Employee": Record "Warehouse Employee";
        CompanyInfo: Record "Company Information";
    begin
        // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
        IF NOT Rec.GET(USERID) THEN BEGIN
            Rec.Code := USERID;
            Rec.Insert();
        END;
        SetLinks('');
        // FIN CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article

        RecArticle.Stocké := FALSE;
        // Initialisation du type commande achat
        TypeCdeAchat := TypeCdeAchat::"Niveau 3";
        //EffacerArticle;
        quantité_demande := 1;

        // AD Le 16-05-2007 => Recherche le code magasin à utiliser -> Soit celui de lu salarié magasin, soit celui de la société.
        "Warehouse Employee".RESET();
        "Warehouse Employee".SETRANGE("User ID", USERID);
        "Warehouse Employee".SETRANGE(Default, TRUE);
        IF "Warehouse Employee".FINDFIRST() THEN
            code_mag := "Warehouse Employee"."Location Code"
        ELSE BEGIN
            CompanyInfo.GET();
            IF CompanyInfo."Location Code" <> '' THEN
                //  ERROR('Aucun code magasin paramétré.')
                //ELSE
                code_mag := CompanyInfo."Location Code";
        END;

        IF initialisation THEN BEGIN
            IF "codN°Client" <> '' THEN
                MAJRecClients()
            ELSE
                EffacerClient();
            IF "codN°Fournisseur" <> '' THEN
                MAJRecFournisseurs()
            ELSE
                Effacerfournisseur();
            affiche_tarif();
        END
        ELSE BEGIN
            EffacerClient();
            Effacerfournisseur();
        END;


        IF initialisation THEN BEGIN
            IF "codN°Article" <> '' THEN BEGIN
                MAJRecArticles();
                "codN°Article" := RecArticle."No. 2";
                // Formulaire des prix fournisseurs
                CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
                CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti(RecArticle."No.", quantité_demande, "codN°Fournisseur"
                                                                             , TypeCdeAchat);

            END ELSE
                EffacerArticle();
        END;

        InitFactBox();
        affiche_tarif();


        AvailabilityDate := WORKDATE();
    end;

    var
        RecClient: Record Customer;
        "RecEntêteVente": Record "Sales Header";
        RecLignesVente: Record "Sales Line";
        "RecExpéditionVente": Record "Sales Shipment Header";
        RecArticle: Record Item;
        RecEcrituresArticles: Record "Item Ledger Entry";
        RecEcrituresValeurs: Record "Value Entry";
        RecLigneComment: Record "Comment Line";
        rec_utilisateur: Record "User Setup";
        RecFournisseur: Record Vendor;
        "RecEntêteAchat": Record "Purchase Header";
        RecLignesAchat: Record "Purchase Line";
        "RecRéceptionAchat": Record "Purch. Rcpt. Header";
        recmagasin: Record Location;
        RecParam: Record "Generals Parameters";
        Rec_Devise: Record Currency;
        CurrExchRate: Record "Currency Exchange Rate";
        RecArticle2: Record Item;
        recarticle3: Record Item;
        ItemCostMgt: Codeunit ItemCostManagement;
        SalesInfoPaneMgt: Codeunit "Sales Info-Pane Management";
        GestionMultireference: Codeunit "Gestion Multi-référence";
        cu_webinfo: Codeunit "ECommerce Functions";
        GestionSYMTA: Codeunit "Gestion SYMTA";
        FrmEcrituresValeurs: Page "Value Entries";
        FrmCommandesClients: Page "Sales Order";
        FrmFicheClient: Page "Customer Card";
        FrmListeClients: Page "Customer List";
        FrmFicheArticle: Page "Item Card";
        FrmListeArticles: Page "Item List";
        FrmStockArticle: Page "Item Avail. by Location Lines";
        FrmEcrituresArticle: Page "Item Ledger Entries_lm";
        FrmFicheFournisseur: Page "Vendor Card";
        FrmListeFournisseurs: Page "Vendor List";
        FrmCommandesFournisseurs: Page "Purchase Order";
        FrmLigneComment: Page "Comment Sheet";
        frm_magasin: Page "Location List";

        ClientChoisi: Boolean;
        "codN°Article": Code[60];

        ArticleChoisi: Boolean;

        "codN°Fournisseur": Code[60];
        "codN°BR": Code[60];
        FournisseurChoisi: Boolean;
        "TrouvéCommande": Boolean;
        CommentaireClient: Boolean;
        CommentaireArticle: Boolean;
        CommentaireFournisseur: Boolean;
        "codN°BL": Code[20];

        "quantité_demande": Decimal;
        prix_net: Decimal;
        prix: Decimal;
        NomRechercheClient: Code[30];
        "codN°Client": Code[60];

        remise1: Decimal;
        remise2: Decimal;
        remise: Decimal;
        gDetailStockMort: Text[100];
        gDetailStocke: Text[100];
        gDetailPubliable: Text[100];


        code_mag: Code[10];


        AverageCostLCY: Decimal;
        AverageCostACY: Decimal;
        total: Decimal;
        initialisation: Boolean;
        prix_four: Decimal;
        prix_net_four: Decimal;
        remise1_four: Decimal;
        remise2_four: Decimal;
        total_four: Decimal;
        DEEEAmount: Decimal;


        AvailabilityDate: Date;
        DispoADate: Decimal;
        Dispo: Decimal;

        "-------------": Integer;
        CurrentSteps: Integer;
        "---": Integer;
        "Profit %": Decimal;
        "---------FAR.AC.01------------": Integer;
        prix_four_devise: Decimal;

        factor: Decimal;
        "--- SYMTA ---": Integer;


        TypeCdeVente: Option "Dépannage","Réappro",Stock;
        TypeCdeAchat: Option "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        CodeZone: Code[10];
        CodeEmplacement: Code[10];
        PrixCatalogue: Decimal;
        Coefficient: Decimal;
        Text50002Lbl: Label 'Il existe des articles de substitution.';
        ESK001Lbl: Label 'Pas de fiche article mais existe dans Tab Tar';
        NomAppro: Text[30];

        ESK002Err: Label 'La référence %1 est un kit !', Comment = '%1 = Ref No';

        BtnSubstituteVisible: Boolean;
        KVisible: Boolean;
        KitVisible: Boolean;
        InfoDivers: Text;
        ChampMarque: Text[50];
        ChampFamille: Text[50];
        ChampSousFamille: Text[50];

    procedure MAJRecClients()
    var
        CommentLine: Record "Comment Line";
        //  wNomRechercheClient: Code[60];
        "wcodN°Client": Code[60];
    /*  wtxtNomClient: Text[60];
     wtxtAdresseClient: Text[60];
     "wtxtAdresse (2ème lig) Client": Text[60];
     "wcodCode postal Client": Code[60];
     wtxtVilleClient: Text[60];
     wtxtSIRETClient: Text[20]; */

    begin
        "wcodN°Client" := '@' + "codN°Client" + '*';
        RecClient.RESET();
        RecClient.INIT();
        RecClient.SETFILTER(RecClient."No.", "wcodN°Client");

        IF RecClient.FIND('-') THEN
            ClientChoisi := TRUE
        ELSE
            EffacerClient();


        //MIGV5
        IF RecClient."No." <> '' THEN BEGIN
            CommentLine.RESET();
            CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Customer);
            CommentLine.SETRANGE("No.", RecClient."No.");
            CommentLine.SETRANGE("Display Sales Order", TRUE);
            IF CommentLine.COUNT > 0 THEN
                PAGE.RUN(PAGE::"Comment Sheet", CommentLine);
        END;

        // AD Le 13-03-2016
        CurrPage.Frm_RemiseVente.PAGE.InitForm(RecClient."No.", TypeCdeVente);
        CurrPage.UPDATE(FALSE);
    end;

    procedure MAJRecArticles()
    var
        wNomRechercheArticle: Code[60];
        "wcodN°Article": Code[60];
        "wtxtDésignationArticle": Text[60];
        "wtxtDésignation2Article": Text[60];
        px: Decimal;
        TempSalesLine: Record "Sales Line" temporary;
        ProdBOMHeader: Record "Production BOM Header";
        ProdBOMLine: Record "Production BOM Line";
        CommentLine: Record "Comment Line";
        WMSMngt: Codeunit "WMS Management";
        TabTar: Record Tab_tar_tmp;
        mess_txt: Char;
        "kitting header": Record "Production BOM Header";
        WhereUsedMgt: Codeunit "Where-Used Management";
        WhereUsedLine: Record "Where-Used Line";
        FrmCommentLine: Page "Comment Sheet";
        BomComponent: Record "BOM Component";
        LMarque: Record Manufacturer;
        LFamille: Record "Item Category";
        CduLFunctions: Codeunit "Codeunits Functions";
    //TODO LSousFamille: Record "Product Group";
    begin
        // Charge le record de l'article

        // AD Le 14-12-2009 => GDI -> Gestion multiréférence
        //"wcodN°Article" := '@*' + "codN°Article" + '*';
        //"wcodN°Article" := '@' + "codN°Article" + '*';
        //RecArticle.RESET();
        //RecArticle.INIT();
        //RecArticle.SETFILTER(RecArticle."No.","wcodN°Article");

        InfoDivers := '';
        RecArticle.RESET();
        RecArticle.INIT();
        RecArticle.SETFILTER(RecArticle."No.", GestionMultireference.RechercheMultiReference("codN°Article"));
        // FIN AD Le 14-12-2009

        // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
        SetLinks('');
        // FIN CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article

        IF code_mag <> '' THEN
            RecArticle.SETRANGE("Location Filter", code_mag);

        IF RecArticle.FIND('-') THEN BEGIN
            ArticleChoisi := TRUE;
            RecArticle.CALCFIELDS(Inventory);
            RecArticle.CALCFIELDS("Qty. on Purch. Order");
            RecArticle.CALCFIELDS("Qty. on Sales Order");
            RecArticle.CALCFIELDS("Qty. on Blanket Sales Order");
            RecArticle.CALCFIELDS("Qty. on Service Order");
            RecArticle.CALCFIELDS("Qty. on Purchase Return");
            //CPH ticket 9530 - la date de sortie ne s'affiche pas
            RecArticle.CALCFIELDS("Date Dernière Sortie");

            RecArticle.CALCFIELDS(Comment);

            //Production
            RecArticle2.GET(RecArticle."No.");
            RecArticle2.CALCFIELDS("Qty. on Prod. Order");
            RecArticle2.CALCFIELDS("Qty. on Component Lines");


            NomAppro := '';
            IF RecArticle."Code Appro" <> '' THEN BEGIN
                RecParam.GET('ART_CODE_APPRO', RecArticle."Code Appro");
                NomAppro := RecParam.LongDescription;
            END;

            //   AD Le 27-12-2011 => Demande de LM
            CLEAR(BomComponent);
            BomComponent.SETRANGE("Parent Item No.", RecArticle."No.");
            IF NOT BomComponent.ISEMPTY THEN BEGIN
                //   IF RecArticle."Kit BOM No." <> '' THEN
                // MESSAGE(ESK002, RecArticle."No. 2"); AD Le 26-11-2015 => Demande de Marie Laure plus de message
                InfoDivers += 'KIT';
            END;
            //   // FIN AD Le 27-12-2011
            CLEAR(BomComponent);
            BomComponent.SETRANGE("No.", RecArticle."No.");
            BomComponent.SETRANGE(Type, BomComponent.Type::Item);
            IF NOT BomComponent.ISEMPTY THEN BEGIN
                //   IF RecArticle."Kit BOM No." <> '' THEN
                InfoDivers += 'COMPOSANT KIT';
            END;

            //    RecArticle.CALCFIELDS("Qty. on Kit Sales Lines");
            RecArticle.SETRANGE("No.");
            // AD Le 29-09-2007 => V5
            //ItemCostMgt.CalculateAverageCost(RecArticle,AverageCostLCY,AverageCostACY);
            AverageCostLCY := ROUND(RecArticle."Unit Cost", 0.01);
            // FIN AD Le 29-09-2007
            // AD Le 27-08-2007 => Recherche des articles de substitutions
            RecArticle.CALCFIELDS("No. of Substitutes");
            BtnSubstituteVisible := RecArticle."No. of Substitutes" > 0;
            DispoADate := CalcAvailability();
            Dispo := RecArticle.Inventory - RecArticle."Qty. on Sales Order"
                 //- RecArticle."Qty. on Kit Sales Lines"
                 - RecArticle2."Qty. on Component Lines";
            IF RecArticle."No. of Substitutes" > 0 THEN BEGIN
                IF STRLEN(InfoDivers) <> 0 THEN
                    InfoDivers += '/';
                InfoDivers += 'SUBSTITU.';
            END;


            //lm le 13-02-2013 =>affichage kit
            KVisible := FALSE;
            "kitting header".RESET();
            "kitting header".SETRANGE("kitting header"."No.", RecArticle."No.");
            IF "kitting header".COUNT = 1 THEN KVisible := TRUE;

            // MCO Le 01-04-2015 => Régie
            WhereUsedMgt.WhereUsedFromItem(RecArticle, WORKDATE(), TRUE);
            KitVisible := WhereUsedMgt.NextRecord(1, WhereUsedLine) <> 0;

            // FIN MCO Le 01-04-2015

            // AD Le 29-10-2009 => FARGROUP => Prise en compte des commande ouverte dans le dispo
            RecArticle.CALCFIELDS("Qty. on Blanket Sales Order");
            Dispo -= RecArticle."Qty. on Blanket Sales Order";
            // FIN AD Le 29-10-2009

            // AD Le 24-1-2010 => FARGROUP
            // GR 06-10-15 RecArticle.CALCFIELDS("Qte sur Compo. feuille assem");
            Dispo -= RecArticle."Qte sur Compo. feuille assem";

            RecArticle.GestionPlusFourni('', '');

            // FIN AD Le 19-02-2009

            //    IF (RecArticle."Kit BOM No." <> '') AND (RecArticle."Automatic Build Kit BOM") THEN
            //      BEGIN
            //        //FrmDispo.SalesLineShowWarning(TempSalesLine,TempKitSalesLines);
            //        ///****
            //        Dispo:=SalesLineShowWarning(RecArticle."No.");
            //      END;

            // AD Le 07-09-2009 => FARGROUP -> Affichage des commentaires QUID
            CommentLine.RESET();
            CommentLine.SETRANGE("Table Name", CommentLine."Table Name"::Item);
            CommentLine.SETRANGE("No.", RecArticle."No.");
            CommentLine.SETRANGE("Afficher Multiconsultation", TRUE);
            IF CommentLine.COUNT <> 0 THEN BEGIN
                CLEAR(FrmCommentLine);
                FrmCommentLine.SETTABLEVIEW(CommentLine);
                FrmCommentLine.EDITABLE(FALSE);
                FrmCommentLine.RUNMODAL();
            END;
            // FIN AD Le 07-09-2009

            // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
            // CFR le 11/03/2021 : position du code importante, doit être après l'affichage du commentaire, sinon plantage avec RunModal
            SetLinks(RecArticle."No.");
            // FIN CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article

            // AD Le 17-10-2011 => Recherche Emplacment et Zone
            CodeEmplacement := '';
            CodeZone := '';
            CduLFunctions.ESK_GetDefaultBin(RecArticle."No.", '', code_mag, CodeEmplacement, CodeZone);
            // FIN AD Le 17-10-2011

            // LM le 23-07-2012 => recherche ref active de fusion en attente
            recarticle3.INIT();
            IF recarticle3.GET(RecArticle."Fusion en attente sur") THEN;
            // FIN LM le 23-07-2012

            // AD Le 23-03-2016

            IF LMarque.GET(RecArticle."Manufacturer Code") THEN
                ChampMarque := LMarque.Code + ' - ' + LMarque.Name
            ELSE
                ChampMarque := LMarque.Code;
            IF LFamille.GET(RecArticle."Item Category Code") THEN
                ChampFamille := LFamille.Code + ' - ' + LFamille.Description
            ELSE
                ChampFamille := RecArticle."Item Category Code";
            //todo
            // IF LSousFamille.GET(RecArticle."Item Category Code", RecArticle."Product Group Code") THEN
            //     ChampSousFamille := LSousFamille.Code + ' - ' + LSousFamille.Description
            // ELSE
            //     ChampSousFamille := RecArticle."Product Group Code";

            ChampSousFamille := RecArticle.GetCodeFamille();

            gDetailStockMort := FORMAT(RecArticle."Stock Mort");
            IF (RecArticle."date maj stock mort" <> 0D) THEN BEGIN
                gDetailStockMort += '  - modifié le ' + FORMAT(RecArticle."date maj stock mort");
                IF (RecArticle."user maj stock mort" <> '') THEN
                    gDetailStockMort += ' par ' + FORMAT(RecArticle."user maj stock mort");
            END;

            gDetailStocke := FORMAT(RecArticle.Stocké);
            IF (RecArticle."date maj stocké" <> 0D) THEN BEGIN
                gDetailStocke += '  - modifié le ' + FORMAT(RecArticle."date maj stocké");
                IF (RecArticle."user maj stocké" <> '') THEN
                    gDetailStocke += ' par ' + FORMAT(RecArticle."user maj stocké");
            END;

            gDetailPubliable := FORMAT(RecArticle.Publiable);
            IF (RecArticle."Date Modif. Publiable Web" <> 0D) THEN BEGIN
                gDetailPubliable += '  - modifié le ' + FORMAT(RecArticle."Date Modif. Publiable Web");
            END;
            // AD Le 13-03-2016
            CurrPage.Frm_PrixVente.PAGE.InitForm(RecArticle."No.", TypeCdeVente);
            CurrPage.UPDATE(FALSE);


        END
        ELSE BEGIN

            TabTar.RESET();
            TabTar.SETRANGE(c_fournisseur, 'EURFRA-FAC');
            TabTar.SETRANGE(ref_active, "codN°Article");
            IF TabTar.FINDFIRST() THEN BEGIN
                AverageCostLCY := 0;
                AverageCostACY := 0;
                EffacerArticle();
                DispoADate := 0;

                MESSAGE('Tarif EURFRA-FAC %1 %2 %3', TabTar.design_fourni, TabTar.px_brut, TabTar.code_remise);
                //RecArticle.Description := TabTar.design_fourni;
                //PrixCatalogue := TabTar.px_brut;
                //RecArticle."No. 2" := TabTar.ref_active;
            END;


        END;
    end;

    procedure MAJRecFournisseurs()
    var
        wNomRechercheFournisseur: Code[60];
        "wcodN°Fournisseur": Code[60];
        wtxtNomFournisseur: Text[60];
        wtxtAdresseFournisseur: Text[60];
        "wtxtAdresse (2ème lig) Fourni": Text[60];
        "wcodCode postal Fournisseur": Code[60];
        wtxtVilleFournisseur: Text[60];
        wtxtSIRETFournisseur: Text[20];
    begin
        "wcodN°Fournisseur" := '@*' + "codN°Fournisseur" + '*';
        RecFournisseur.INIT();
        RecFournisseur.RESET();
        RecFournisseur.SETFILTER(RecFournisseur."No.", "wcodN°Fournisseur");

        IF RecFournisseur.FIND('-') THEN BEGIN
            FournisseurChoisi := TRUE;
            "codN°Fournisseur" := RecFournisseur."No."
        END
        ELSE
            Effacerfournisseur();

        // >>>>> ajout cyril 15/05/01
        //ChercherCommentaireFournisseur.CommenteFournisseur (RecFournisseur);
        //CommentaireFournisseur := ChercherCommentaireFournisseur.ExisteCommentaire;
        // <<<<<
    end;

    procedure EffacerClient()
    begin
        // Efface les données client

        ClientChoisi := FALSE;

        CLEAR(RecClient);
        RecClient.INIT();
        RecClient.RESET();

        "codN°Client" := '';

        // >>>>> ajout cyril 15/05/01
        //ChercherCommentaireClient.CommenteClient (RecClient);
        //CommentaireClient := ChercherCommentaireClient.ExisteCommentaire;
        // <<<<<
    end;

    procedure EffacerArticle()
    begin
        // Efface les données article

        // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article
        SetLinks('');
        // FIN CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article

        InfoDivers := '';

        ArticleChoisi := FALSE;

        CLEAR(RecArticle);
        RecArticle.INIT();
        RecArticle.RESET();

        AverageCostLCY := 0;
        AverageCostACY := 0;
        "Profit %" := 0;

        "codN°Article" := '';
        RecArticle.Stocké := FALSE;

        NomAppro := '';

        // >>>>> ajout cyril 15/05/01
        //ChercherCommentaireArticle.RUN (RecArticle);
        //CommentaireArticle := ChercherCommentaireArticle.ExisteCommentaire;
        // <<<<<
        // AD Le 23-03-2016
        ChampMarque := '';
        ChampFamille := '';
        ChampSousFamille := '';

        gDetailStockMort := '';
        gDetailStocke := '';
        gDetailPubliable := '';
    end;

    procedure affiche_tarif()
    var
        LCodeunitsFunctions: Codeunit "Codeunits Functions";
        TempSalesPrice: Record "Sales Price" temporary;
        TempSalesLineDisc: Record "Sales Line Discount" temporary;
        TempPurchPrice: Record "Purchase Price" temporary;
        TempPurchLineDisc: Record "Purchase Line Discount" temporary;
        "Purch Price Calc. Mgt.": Codeunit "Purch. Price Calc. Mgt.";
    // PurchPrice: Record "Purchase Price" temporary;
    begin

        IF NOT ArticleChoisi THEN BEGIN
            prix := 0;
            prix_net := 0;
            remise1 := 0;
            remise2 := 0;
            DEEEAmount := 0;
            // MC Le 25-10-2011 => Gestion des prix catalogues
            PrixCatalogue := 0;
            Coefficient := 0;
            // FIN MC Le 25-10-2011
            EXIT;
        END;

        IF RecArticle."Item Charge DEEE" <> '' THEN
            DEEEAmount := RecArticle."Item Charge DEEE Amount" * quantité_demande
        ELSE
            DEEEAmount := 0;


        // Remplissage des prix de base
        prix := RecArticle."Unit Price";
        prix_four := RecArticle."Last Direct Cost";
        remise1 := 0;
        remise2 := 0;

        remise1 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 1,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        remise2 := LCodeunitsFunctions.GetSalesDisc(
                   TempSalesLineDisc, '', '', '',
                   '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 2,
                   '', '', TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.);

        IF NOT LCodeunitsFunctions.GetUnitPrice(
                  TempSalesPrice, '', '', '',
                  '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, remise, prix,
                  '', '', TypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
        THEN BEGIN
            prix := RecArticle."Unit Price";
        END
        // MC Le 25-10-2011 => Gestion des prix catalogues
        ELSE BEGIN
            PrixCatalogue := TempSalesPrice."Prix catalogue";
            Coefficient := TempSalesPrice.Coefficient;
        END;
        // FIN MC Le 25-10-2011.

        // Calcul du prix pour ce client.
        IF ClientChoisi THEN BEGIN
            // Recherche des remises
            remise1 := 0;
            remise2 := 0;
            remise := 0;
            // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
            RecClient.SETFILTER("Centrale Active Starting DF", '..%1', TODAY());
            RecClient.SETFILTER("Centrale Active Ending DF", '%1..', TODAY());
            RecClient.CALCFIELDS("Centrale Active");
            // FIN CFR le 10/03/2021 - SFD20210201 historique centrale active

            remise1 := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 1,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.


            remise2 := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 2,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.

            remise := LCodeunitsFunctions.GetSalesDisc(
                      TempSalesLineDisc, RecClient."No.", '', RecClient."Customer Disc. Group",
                      '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, 0,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                      TypeCdeVente); // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.

            //MESSAGE('%1  %2   %3', remise1, remise2, remise);



            // Recherche si des prix tarif existe => Sinon on prend le prix de la fiche.
            IF NOT LCodeunitsFunctions.GetUnitPrice(
                      TempSalesPrice, RecClient."No.", '', RecClient."Customer Price Group",
                      '', RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE(), FALSE, quantité_demande, remise, prix,
                      RecClient."Sous Groupe Tarifs", RecClient."Centrale Active",
                       TypeCdeVente) // MC Le 19-04-2011 => SYMTA -> Tarifs ventes.
            THEN BEGIN
                prix := RecArticle."Unit Price";
            END
            // MC Le 25-10-2011 => Gestion des prix catalogues
            ELSE BEGIN
                IF TempSalesPrice."Prix catalogue" <> 0 THEN BEGIN
                    PrixCatalogue := TempSalesPrice."Prix catalogue";
                    Coefficient := TempSalesPrice.Coefficient;
                END;
            END;
            // FIN MC Le 25-10-2011
        END;
        //FIN  Calcul du prix pour ce client.

        // Calcul du net et total
        IF remise <> 0 THEN
            prix_net := ROUND(prix - (prix * remise) / 100, 0.01)
        ELSE BEGIN
            prix_net := ROUND(prix, 0.01);
            remise1 := 0;
            remise2 := 0;
        END;

        IF prix_net <> 0 THEN
            "Profit %" :=
              ROUND(
                100 * (1 - RecArticle."Unit Cost" / prix_net), 0.00001)
        ELSE
            "Profit %" := 0;


        total := quantité_demande * prix_net;
        // FIN Calcul du net et total


        //remise1_four := "Purch Price Calc. Mgt.".GetPurchDisc(
        //          TempPurchLineDisc, '',
        //           RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande) ;


        IF FournisseurChoisi THEN BEGIN
            // Recherche des remises
            //    remise1_four := "Purch Price Calc. Mgt.".GetPurchDisc(
            //              TempPurchLineDisc, RecFournisseur."No.",
            //               RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande) ;


            // Recherche si des prix tarif existe => Sinon on prend le prix de la fiche.
            //    IF NOT "Purch Price Calc. Mgt.".GetUnitCost(
            //              TempPurchPrice, RecFournisseur."No.",
            //              RecArticle."No.", '', RecArticle."Base Unit of Measure", '', WORKDATE, FALSE, quantité_demande,
            //              code_mag, prix_four)
            //    THEN
            BEGIN
                prix_four := RecArticle."Last Direct Cost";
            END;

        END;

        // Calcul du net et total
        remise2_four := 0;
        IF remise1_four <> 0 THEN
            prix_net_four := ROUND(prix_four - (prix_four * remise1_four) / 100, 0.01)
        ELSE
            prix_net_four := ROUND(prix_four, 0.01);
        // FIN Calcul du net et total


        total_four := quantité_demande * prix_net_four;

        // MC Le 11-05-2010 FAR.AC.01 Analyse complémentaire => On veut le prix d'achat dans la devise du fournisseur
        prix_four_devise := prix_four;
        IF RecFournisseur.GET("codN°Fournisseur") THEN BEGIN
            "Purch Price Calc. Mgt.".FindPurchPrice(TempPurchPrice, RecFournisseur."No.", RecArticle."No."
            , '', RecArticle."Base Unit of Measure", RecFournisseur."Currency Code", WORKDATE(), FALSE);
            IF TempPurchPrice.FINDFIRST() THEN
                prix_four_devise := TempPurchPrice."Direct Unit Cost";
        END;
        //FIN MC Le 11-05-2010 FAR.AC.01

        //MC Le 19-05-2010 FAR.AC.01 Analyse complémentaire => calcul prix d'achat DS en fonction de celui
        // en devise fournisseur
        IF Rec_Devise.GET(RecFournisseur."Currency Code") THEN BEGIN
            factor := CurrExchRate.ExchangeRate(TODAY, Rec_Devise.Code);
            prix_four :=
            CurrExchRate.ExchangeAmtFCYToLCY(
              WORKDATE(), Rec_Devise.Code,
              prix_four_devise, factor);
        END;
    end;

    procedure init("code client": Code[20])
    begin
        "codN°Client" := "code client";
        initialisation := TRUE;
    end;

    procedure Effacerfournisseur()
    begin
        // Efface les données fournisseur

        FournisseurChoisi := FALSE;

        CLEAR(RecFournisseur);
        RecFournisseur.INIT();
        RecFournisseur.RESET();

        "codN°Fournisseur" := '';

        // >>>>> ajout cyril 15/05/01
        //ChercherCommentaireFournisseur.CommenteFournisseur (RecFournisseur);
        //CommentaireFournisseur := ChercherCommentaireFournisseur.ExisteCommentaire;
        // <<<<<
    end;

    procedure CalcAvailability(): Decimal
    var
        AvailableToPromise: Codeunit "Available to Promise";
        LookaheadDateformula: DateFormula;
        GrossRequirement: Decimal;
        ScheduledReceipt: Decimal;
        PeriodType: Option Day,Week,Month,Quarter,Year;

        Item: Record Item;
    begin
        IF Item.GET(RecArticle."No.") THEN BEGIN

            IF AvailabilityDate = 0D THEN
                AvailabilityDate := WORKDATE();

            Item.RESET();
            Item.SETRANGE("Date Filter", 0D, AvailabilityDate);
            Item.SETRANGE("Variant Filter", '');
            Item.SETRANGE("Location Filter", code_mag);
            Item.SETRANGE("Drop Shipment Filter", FALSE);

            EXIT(
              AvailableToPromise.CalcQtyAvailableToPromise(
                Item,
                GrossRequirement,
                ScheduledReceipt,
                AvailabilityDate,
                "Analysis Period Type".FromInteger(PeriodType),
                LookaheadDateformula));
        END;
    end;

    procedure SalesLineShowWarning(_pCArticle: Code[20]) QtyTot: Decimal
    var
        NStkItem: Record Item;
        NotPreparate: Boolean;
        ProdBOMHeader: Record "Shipping Agent Services";
        ProdBOMLine: Record "Production BOM Line";
        "Current Quantity": Decimal;
        Item: Record Item;
    begin
        Item.GET(_pCArticle);
        Item.CALCFIELDS("Substitutes Exist");


        QtyTot := 10000000;
        NotPreparate := FALSE;
        //ProdBOMHeader.TESTFIELD(Status,ProdBOMHeader.Status::Certified);
        //ProdBOMHeader.TESTFIELD(Type,ProdBOMHeader.Type::Kitting);
        //TempKitSalesLines.RESET();
        //ProdBOMLine.SETRANGE("Production BOM No.",ProdBOMHeader."No.");
        //IF ProdBOMLine.FINDSET() THEN
        //REPEAT
        //      WITH KitComp2 DO BEGIN
        //        INIT();

        //        "Line No." := ProdBOMLine."Line No.";
        //        "Item No." := ProdBOMLine."No.";
        //        Description := ProdBOMLine.Description;
        //        "Quantity per" := ProdBOMLine."Quantity per";
        //        Item.GET("Item No.");
        //       Item.CALCFIELDS(
        //       Inventory,
        //      "Qty. on Purch. Order",
        //      "Qty. on Sales Order",
        //      "Qty. on Kit Sales Lines",
        //      "Scheduled Need (Qty.)",
        //      "Scheduled Receipt (Qty.)",
        //      "Qty. in Transit",
        //      "Trans. Ord. Receipt (Qty.)",
        //      "Trans. Ord. Shipment (Qty.)",
        //      "Qty. on Service Order");
        //       Inventory:=Item.Inventory;
        //       "Gross Requirement":=Item."Qty. on Sales Order"+Item."Qty. on Kit Sales Lines";
        //       {CheckItemAvail.GetParam(
        //          Item.Inventory,"Gross Requirement","Scheduled Receipt","Current Quantity",
        //          "Total Quantity","Earliest Available Date");
        //        "Total Quantity" := "Total Quantity" + "Current Quantity";
        //        "Current Quantity" := -"Current Quantity";                 }

        //       /// IF (("Total Quantity" + "Current Quantity") > 0) AND ("Quantity per" <> 0) THEN
        //       ///   "Able to Make Qty." := ROUND(("Total Quantity" + "Current Quantity") / "Quantity per",1,'<')


        //         IF (Item.Inventory-"Gross Requirement">0) THEN
        //           AbleToMakeQty:= ROUND((Item.Inventory-"Gross Requirement")/ "Quantity per",1,'<')
        //           ELSE
        //           BEGIN
        //          AbleToMakeQty := 0;
        //           END;


        //      END;
        //     IF   QtyTot>AbleToMakeQty THEN
        //     QtyTot:=AbleToMakeQty;


        ///UNTIL ProdBOMLine.NEXT=0;
        //EXIT(QtyTot);
    end;

    procedure InitClient("code client": Code[20])
    begin

        "codN°Client" := "code client";
        Rec.VALIDATE("Customer No", "code client");

        IF "code client" <> '' THEN BEGIN
            MAJRecClients();
            "codN°Client" := RecClient."No.";
        END ELSE
            EffacerClient();

        initialisation := TRUE;
        InitFactBox();

        //CurrPage.UPDATE(FALSE);
    end;

    procedure InitFournisseur(_pCodeFrn: Code[20])
    begin

        "codN°Fournisseur" := _pCodeFrn;
        Rec.VALIDATE("Vendor No", _pCodeFrn);

        IF _pCodeFrn <> '' THEN BEGIN
            MAJRecFournisseurs();
            "codN°Fournisseur" := RecFournisseur."No.";
        END ELSE
            Effacerfournisseur();

        initialisation := TRUE;
        InitFactBox();
    end;

    procedure InitArticle("code article": Code[20])
    var
        ItemCross: Record "Item Reference";
    begin
        "codN°Article" := "code article";
        initialisation := TRUE;
    end;

    local procedure AvailabilityDateOnAfterValidat()
    begin
        DispoADate := CalcAvailability();
    end;

    local procedure AverageCostLCYOnActivate()
    begin
        ItemCostMgt.CalculateAverageCost(RecArticle, AverageCostLCY, AverageCostACY);
    end;

    local procedure codN176ClientOnAfterInput(var Text: Text[1024])
    begin
        GestionSYMTA.CompleteNoClient(Text);
    end;

    procedure InitFactBox()
    begin
        Rec.VALIDATE("Item No", RecArticle."No.");
        Rec.VALIDATE("Location Code", code_mag);
        CurrPage."Pri Factbox".PAGE.Init(quantité_demande, "codN°Client", "codN°Fournisseur", TypeCdeVente);
        CurrPage."Hist Factbox".PAGE.UPDATE(FALSE);
        //Message('IFB');
        //CurrPage.UPDATE(FALSE);
    end;

    local procedure VideEcran()
    begin
        CLEAR(RecArticle);
        CLEAR(Rec);

        EffacerClient();
        Effacerfournisseur();
        EffacerArticle();
        InitFactBox();

        CurrPage."Pri Factbox".PAGE.Vider();
        CurrPage."Pri Factbox".PAGE.UPDATE(FALSE);

        CurrPage."Hist Factbox".PAGE.Vide();
        CurrPage."Hist Factbox".PAGE.UPDATE(FALSE);

        CurrPage.frm_BufferPrice.PAGE.HideSelection(TRUE);
        CurrPage.frm_BufferPrice.PAGE.InsertBufferRecordByMulti(RecArticle."No.", quantité_demande, "codN°Fournisseur"
                                                                , TypeCdeAchat);

        CurrPage.UPDATE(FALSE);
    end;

    local procedure SetLinks(pItemNo: Code[20])
    var
        lItem: Record Item;
        lRecordLinkManagement: Codeunit "Record Link Management";
    begin
        // CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article

        // Efface les liens
        IF Rec.HASLINKS THEN BEGIN
            Rec.DELETELINKS();
            // CFR le 12/03/2021 : COMMIT nécessaire pour éviter les blocages liés au RunMODAL de certaines form de commentaires
            COMMIT();
        END;

        // Charge les liens articles
        IF lItem.GET(pItemNo) THEN BEGIN
            lRecordLinkManagement.CopyLinks(lItem, Rec);
            // CFR le 12/03/2021 : COMMIT nécessaire pour éviter les blocages liés au RunMODAL de certaines form de commentaires
            COMMIT();
        END;
    end;
}

