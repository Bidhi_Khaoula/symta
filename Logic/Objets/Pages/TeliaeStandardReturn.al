page 50228 "Teliae Standard Return"
{
    ApplicationArea = all;
    Caption = 'Teliae Standard Return';
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Teliae Standard Return";

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° séquence field.';
                }
                field("Import No."; Rec."Import No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° import field.';
                }
                field("Import Date"; Rec."Import Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date d''import field.';
                }
                field("Reference UM"; Rec."Reference UM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reference UM field.';
                }
                field("Info Colis UM"; Rec."Info Colis UM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Info Colis UM field.';
                }
                field("Numero UM"; Rec."Numero UM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero UM field.';
                }
                field("Poids UM"; Rec."Poids UM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids UM field.';
                }
                field("URL Tracking UM"; Rec."URL Tracking UM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the URL Tracking UM field.';
                }
                field("Code Barre de tri (UM)"; Rec."Code Barre de tri (UM)")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Barre de tri (UM) field.';
                }
                field("Code Barre Client"; Rec."Code Barre Client")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Barre Client field.';
                }
                field("Code Barre Transporteur"; Rec."Code Barre Transporteur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Barre Transporteur field.';
                }
                field("Numero Recepisse"; Rec."Numero Recepisse")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero Recepisse field.';
                }
                field("Numero BL"; Rec."Numero BL")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero BL field.';
                }
                field("URL Tracking Expedition"; Rec."URL Tracking Expedition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the URL Tracking Expedition field.';
                }
                field("Nom Expédition"; Rec."Nom Expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Expédition field.';
                }
                field("Code Transporteur"; Rec."Code Transporteur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Transporteur field.';
                }
                field("Code Produit"; Rec."Code Produit")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Produit field.';
                }
                field("Date Expedition"; Rec."Date Expedition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Expedition field.';
                }
                field("Numero Bordereau"; Rec."Numero Bordereau")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Numero Bordereau field.';
                }
                field("Date creation Bordereau"; Rec."Date creation Bordereau")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date creation Bordereau field.';
                }
                field("Nom Compte Chargeur"; Rec."Nom Compte Chargeur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom Compte Chargeur field.';
                }
                field("Reference Destinataire"; Rec."Reference Destinataire")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reference Destinataire field.';
                }
                field("Code Tournee"; Rec."Code Tournee")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Tournee field.';
                }
                field("Code Preparateur"; Rec."Code Preparateur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Preparateur field.';
                }
            }
        }
    }

    actions
    {
    }
}

