page 50103 "Whse. Receipt Subform_lm270712"
{
    AutoSplitKey = true;
    Caption = 'Whse. Receipt Subform';
    DelayedInsert = true;
    InsertAllowed = false;
    LinksAllowed = false;
    MultipleNewLines = true;
    PageType = Card;
    SourceTable = "Warehouse Receipt Line";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Source Document"; Rec."Source Document")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source Document field.';
                }
                field("Source No."; Rec."Source No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Source No. field.';
                }
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Reference fournisseur"; ref_fou)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ref_fou field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Location Code"; Rec."Location Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Location Code field.';
                }
                field("Zone Code"; Rec."Zone Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Zone Code field.';
                }
                field("Bin Code"; Rec."Bin Code")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Bin Code field.';

                    trigger OnValidate()
                    begin
                        BinCodeOnAfterValidate();
                    end;
                }
                field("Cross-Dock Zone Code"; Rec."Cross-Dock Zone Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cross-Dock Zone Code field.';
                }
                field("Cross-Dock Bin Code"; Rec."Cross-Dock Bin Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Cross-Dock Bin Code field.';
                }
                field("Shelf No."; Rec."Shelf No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Shelf No. field.';
                }
                field("Close Line"; Rec."Close Line")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Solder ligne field.';
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantity field.';
                }
                field("Qty. (Base)"; Rec."Qty. (Base)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. (Base) field.';
                }
                field("Qty. to Receive"; Rec."Qty. to Receive")
                {
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Receive field.';

                    trigger OnValidate()
                    var
                        rec_WhseRcptLine: Record "Warehouse Receipt Line";
                    begin
                        //MC Le 15-09-2011 => Ne pas pouvoir modifier une quantité provenant d'un import de fac
                        IF Rec."N° sequence facture fourn." <> 0 THEN
                            ERROR(ESK50000Err);
                        // FIN MC Le 15-09-2011

                        // MC Le 24-04-2012 => Ne pas pouvoir modifier une quantité si la ligne de commande est sur plusieurs receptions
                        rec_WhseRcptLine.RESET();
                        rec_WhseRcptLine.SETRANGE("Source No.", Rec."Source No.");
                        rec_WhseRcptLine.SETRANGE("Source Line No.", Rec."Source Line No.");

                        IF rec_WhseRcptLine.COUNT > 1 THEN
                            ERROR(ESK50001Err);
                        // FIN MC Le 24-04-2012
                        QtytoReceiveOnAfterValidate();
                    end;
                }
                field(GetInfoLignePXBRUT; rec.GetInfoLigne('PXBRUT'))
                {
                    Caption = 'Prix Brut';
                    Editable = false;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Brut field.';
                }
                field(GetInfoLigneREMISE1; rec.GetInfoLigne('REMISE1'))
                {
                    Caption = 'Remise 1';
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 1 field.';
                }
                field(GetInfoLigneNET1; rec.GetInfoLigne('NET1'))
                {
                    Caption = 'NET 1';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NET 1 field.';
                }
                field(GetInfoLigneTOTALNET1; rec.GetInfoLigne('TOTALNET1'))
                {
                    Caption = 'Total Net 1';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net 1 field.';
                }
                field(GetInfoLigneREMISE2; rec.GetInfoLigne('REMISE2'))
                {
                    Caption = 'Remise 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Remise 2 field.';
                }
                field(GetInfoLigneNET2; rec.GetInfoLigne('NET2'))
                {
                    Caption = 'Prix Net';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Prix Net field.';
                }
                field(GetInfoLigneTOTALNET2; rec.GetInfoLigne('TOTALNET2'))
                {
                    Caption = 'Total Net';
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Total Net field.';
                }
                field("Qty. to Cross-Dock"; Rec."Qty. to Cross-Dock")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Cross-Dock field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        ShowCrossDockOpp(CrossDockOpp2);
                        CurrPage.UPDATE();
                    end;
                }
                field("Qty. Received"; Rec."Qty. Received")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Received field.';
                }
                field("Qty. to Receive (Base)"; Rec."Qty. to Receive (Base)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Receive (Base) field.';
                }
                field("Qty. to Cross-Dock (Base)"; Rec."Qty. to Cross-Dock (Base)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. to Cross-Dock (Base) field.';

                    trigger OnLookup(var Text: Text): Boolean
                    begin
                        ShowCrossDockOpp(CrossDockOpp2);
                        CurrPage.UPDATE();
                    end;
                }
                field("Qty. Received (Base)"; Rec."Qty. Received (Base)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Received (Base) field.';
                }
                field("Qty. Outstanding"; Rec."Qty. Outstanding")
                {
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Outstanding field.';
                }
                field("Qty. Outstanding (Base)"; Rec."Qty. Outstanding (Base)")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. Outstanding (Base) field.';
                }
                field("Due Date"; Rec."Due Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Due Date field.';
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Unit of Measure Code field.';
                }
                field("Qty. per Unit of Measure"; Rec."Qty. per Unit of Measure")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qty. per Unit of Measure field.';
                }
                field(GetInfoLigneCodeREFFOURN; rec.GetInfoLigneCode('REFFOURN'))
                {
                    Caption = 'Ref. Fournisseur';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Fournisseur field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        ref_fou := '';
        rec_purchline.RESET();
        rec_purchline.SETRANGE(rec_purchline."Document No.", rec."Source No.");
        rec_purchline.SETRANGE(rec_purchline."Line No.", rec."Source Line No.");

        IF rec_purchline.FINDFIRST() THEN
            ref_fou := rec_purchline."Item Reference No.";
        ItemNoOnFormat(FORMAT(rec."Item No."));
    end;

    var
        CrossDockOpp2: Record "Whse. Cross-Dock Opportunity";

        rec_purchline: Record "Purchase Line";
        CUMultiRef: Codeunit "Gestion Multi-référence";
        Text001Lbl: Label 'Cross-Docking has been disabled for %1 %3 and/or %2 %4.', Comment = '%1 = Article ; %2 = Emplacement ; %3 = Num Client ; %4 = Code Emplacement';
        ESK50000Err: Label 'Vous ne pouvez pas modifier une quantité provenant d''une ligne d''import de factures.';
        // "--- GVarESKAPE ---": Integer;
        ESK50001Err: Label 'Vous ne pouvez pas modifier la quantité car la ligne de commande est sur plusieurs réceptions.';
        ref_fou: Text[30];


    procedure ShowSourceLine()
    var
        WMSMgt: Codeunit "WMS Management";
    begin
        WMSMgt.ShowSourceDocLine(
          rec."Source Type", rec."Source Subtype", rec."Source No.", rec."Source Line No.", 0);
    end;

    procedure ShowBinContents()
    var
        BinContent: Record "Bin Content";
    begin
        BinContent.ShowBinContents(rec."Location Code", rec."Item No.", rec."Variant Code", rec."Bin Code");
    end;

    procedure ItemAvailability(AvailabilityType: Option Date,Variant,Location)
    begin
        ItemAvailability(AvailabilityType);
    end;

    procedure WhsePostRcptYesNo()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
        WhsePostReceiptYesNo: Codeunit "Whse.-Post Receipt (Yes/No)";
    begin
        WhseRcptLine.COPY(Rec);
        WhsePostReceiptYesNo.RUN(WhseRcptLine);
        rec.RESET();
        rec.SETCURRENTKEY("No.", "Sorting Sequence No.");
        CurrPage.UPDATE(FALSE);
    end;

    procedure WhsePostRcptPrint()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
        WhsePostReceiptPrint: Codeunit "Whse.-Post Receipt + Print";
    begin
        WhseRcptLine.COPY(Rec);
        WhsePostReceiptPrint.RUN(WhseRcptLine);
        rec.RESET();
        rec.SETCURRENTKEY("No.", "Sorting Sequence No.");
        CurrPage.UPDATE(FALSE);
    end;

    procedure WhsePostRcptPrintPostedRcpt()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
        WhsePostReceiptPrintPostedRcpt: Codeunit "Whse.-Post Receipt + Pr. Pos.";
    begin
        WhseRcptLine.COPY(Rec);
        WhsePostReceiptPrintPostedRcpt.RUN(WhseRcptLine);
        rec.RESET();
        CurrPage.UPDATE(FALSE);
    end;

    procedure AutofillQtyToReceive()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        WhseRcptLine.COPY(Rec);
        rec.AutofillQtyToReceive(WhseRcptLine);
    end;

    procedure DeleteQtyToReceive()
    var
        WhseRcptLine: Record "Warehouse Receipt Line";
    begin
        WhseRcptLine.COPY(Rec);
        rec.DeleteQtyToReceive(WhseRcptLine);
    end;

    procedure OpenItemTracking_Lines()
    begin
        Rec.OpenItemTrackingLines();
    end;

    procedure ShowCrossDockOpp(var CrossDockOpp: Record "Whse. Cross-Dock Opportunity" temporary)
    var
        Item: Record Item;
        Location: Record Location;
        CrossDockMgt: Codeunit "Whse. Cross-Dock Management";
        UseCrossDock: Boolean;
    begin
        CrossDockMgt.GetUseCrossDock(UseCrossDock, rec."Location Code", rec."Item No.");
        IF NOT UseCrossDock THEN
            ERROR(Text001Lbl, Item.TABLECAPTION, Location.TABLECAPTION, rec."Item No.", rec."Location Code");
        CrossDockMgt.ShowCrossDock(CrossDockOpp, '', rec."No.", rec."Line No.", rec."Location Code", rec."Item No.", rec."Variant Code");
    end;

    procedure GetPrixNet(): Decimal
    var
        ligneAchat: Record "Purchase Line";
    begin
        IF NOT ligneAchat.GET(ligneAchat."Document Type"::Order, rec."Source No.", rec."Source Line No.") THEN
            EXIT(0)
        ELSE
            EXIT(ligneAchat."Net Unit Cost");
    end;

    procedure GetPrixBrut(): Decimal
    var
        ligneAchat: Record "Purchase Line";
    begin
        IF NOT ligneAchat.GET(ligneAchat."Document Type"::Order, rec."Source No.", rec."Source Line No.") THEN
            EXIT(0)
        ELSE
            EXIT(ligneAchat."Direct Unit Cost");
    end;

    procedure PositionEnBas()
    begin
        rec.FINDLAST();
    end;

    local procedure BinCodeOnAfterValidate()
    begin
        CurrPage.UPDATE();
    end;

    local procedure QtytoReceiveOnAfterValidate()
    begin
        CurrPage.SAVERECORD();
    end;

    local procedure ItemNoOnAfterInput(var Text: Text[1024])
    var
        c: Code[20];
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        EVALUATE(c, Text);
        Text := CUMultiRef.RechercheArticleByActive(c);
    end;

    local procedure ItemNoOnFormat(Text: Text[1024])
    begin
        // AD Le 21-04-2010 => GDI -> Gestion Multireference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;
}

