page 50037 Transport
{
    CardPageID = Transport;
    PageType = Card;
    SourceTable = "Shipping Label";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("Entry No."; Rec."Entry No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° de Séquence field.';
                }
                field("Shipping Agent Code"; Rec."Shipping Agent Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Transporteur field.';
                }
                field("Product Code"; Rec."Product Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Produit field.';
                }
                field("Prepare Code"; Rec."Prepare Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Préparateur field.';
                }
                field("BL No."; Rec."BL No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° BL field.';
                }
                field("Instruction of Delivery"; Rec."Instruction of Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Instruction de Livraison field.';
                }
                field("Nb Of Box"; Rec."Nb Of Box")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nbre de  Colis Total field.';

                    trigger OnValidate()
                    var
                        i: Integer;
                        NoSeriesMgt: Codeunit NoSeriesManagement;
                        SalesSetup: Record "Sales & Receivables Setup";
                    begin
                        SalesSetup.GET();
                        IF Rec."Nb Of Box" = 0 THEN
                            ERROR(txt50003Err);

                        IF (Rec."Nb Of Box" <> 0) AND (Rec."No. start label" <> '') THEN
                            ERROR(txt50001Lbl)
                        ELSE
                            IF CONFIRM(txt50002Lbl) THEN BEGIN
                                Rec."No. start label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
                                i := Rec."Nb Of Box";
                                IF i = 1 THEN Rec."No. end label" := Rec."No. start label";

                                WHILE i > 1 DO BEGIN
                                    Rec."No. end label" := NoSeriesMgt.GetNextNo(SalesSetup."Shipment label Nos.", WORKDATE(), TRUE);
                                    i -= 1;
                                END;
                            END
                    end;
                }
                field(Weight; Rec.Weight)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids field.';
                }
                field("Cash on delivery"; Rec."Cash on delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the C/Rembours field.';
                }
                field("Mode d'expédition"; Rec."Mode d'expédition")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mode d''expédition field.';
                }
                field("Date of  bl"; Rec."Date of  bl")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date du bl field.';
                }
                field("Imperative date of Delivery"; Rec."Imperative date of Delivery")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date de Livraison Impérative field.';
                }
                field("No. start label"; Rec."No. start label")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Etiquette Départ field.';
                }
                field("No. end label"; Rec."No. end label")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Etiquette Fin field.';
                }
            }
            group(Livraison)
            {
                Caption = 'Livraison';
                field("Recipient Code"; Rec."Recipient Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code du Destinataire field.';

                    trigger OnValidate()
                    var
                        RecCustomer: Record Customer;
                    begin
                        IF CONFIRM('Changement de l''adresse ?') THEN
                            IF RecCustomer.GET(Rec."Recipient Code") THEN BEGIN
                                Rec."Recipient Name" := RecCustomer.Name;
                                Rec."Recipient Address" := RecCustomer.Address;
                                Rec."Recipient Address 2" := RecCustomer."Address 2";
                                Rec."Recipient Post Code" := RecCustomer."Post Code";
                                Rec."Recipient City" := RecCustomer.City;
                                Rec."Recipient Country Code" := RecCustomer."Country/Region Code";
                                Rec.MODIFY();

                            END
                    end;
                }
                field("Recipient Name"; Rec."Recipient Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom du Destinataire field.';
                }
                field("Recipient Address"; Rec."Recipient Address")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Adresse 1 du Destinataire field.';
                }
                field("Recipient Address 2"; Rec."Recipient Address 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Adresse 2 du Destinataire field.';
                }
                field("Recipient Post Code"; Rec."Recipient Post Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Postal du Destinataire field.';
                }
                field("Recipient City"; Rec."Recipient City")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ville du Destinataire field.';
                }
                field("Recipient Country Code"; Rec."Recipient Country Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code Pays du Destinataire field.';
                }
                field("Recipient Person"; Rec."Recipient Person")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Personne Destinataire field.';
                }
                field("Recipient Phone No."; Rec."Recipient Phone No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Téléphone du Destinataire field.';
                }
            }
            group(Tramsmission)
            {
                Caption = 'Tramsmission';
                field("Transmission date"; Rec."Transmission date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date transmission field.';
                }
                field("Transmission heure"; Rec."Transmission heure")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Heure transmission field.';
                }
                field("Transmission status"; Rec."Transmission status")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Statut de transmission field.';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            group(Report)
            {
                Caption = 'Report';
                action(Etiquette)
                {
                    Caption = 'Etiquette';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Etiquette action.';

                    trigger OnAction()
                    var
                        Logiflux: Record "Shipping Label";
                        EtqLogiflux: Codeunit "Edition Etiquette LOGIFLUX";
                    begin
                        IF Rec."No. start label" <> '' THEN BEGIN
                            Logiflux.SETFILTER("Entry No.", Rec."Entry No.");
                            IF transporteur.GET(Rec."Shipping Agent Code") THEN
                                IF transporteur."Label Report ID" <> 0 THEN
                                    REPORT.RUNMODAL(transporteur."Label Report ID", FALSE, FALSE, Logiflux)
                                ELSE
                                    EtqLogiflux.EditionEtiquetteLogiflux(Rec, 'M');
                        END;
                    end;
                }
                action(Transporteur)
                {
                    Caption = 'Transporteur';
                    RunObject = Page "Shipping Agents";
                    ApplicationArea = All;
                    ToolTip = 'Executes the Transporteur action.';
                }
            }
        }
    }

    var
        transporteur: Record "Shipping Agent";
        txt50001Lbl: Label 'Impossble numéros dejà générés';
        txt50002Lbl: Label 'Voulez vouz créer les étiquettes ?';
        txt50003Err: Label 'Nre de colis doit être different de 0';
}

