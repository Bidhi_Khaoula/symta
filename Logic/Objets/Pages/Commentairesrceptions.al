page 50053 "Commentaires réceptions"
{
    CardPageID = "Commentaires réceptions";
    PageType = Card;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(content1)
            {
                ShowCaption = false;
                field(NoFusion; NoFusion)
                {
                    Caption = 'N° Réception';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Réception field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        Reception: Record "Warehouse Receipt Header";
                        FrmReception: Page "Warehouse Receipts";
                    begin
                        Reception.RESET();
                        // On ne veut pas les réceptions fusionnées.
                        Reception.SETRANGE(Reception.Fusion, FALSE);
                        CLEAR(FrmReception);
                        FrmReception.SETTABLEVIEW(Reception);
                        FrmReception.LOOKUPMODE(TRUE);
                        IF FrmReception.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmReception.GETRECORD(Reception);
                            NoFusion := Reception."No.";
                            ValidateNoFusion();
                        END
                    end;

                    trigger OnValidate()
                    begin
                        ValidateNoFusion();
                    end;
                }
                field(NoArticle; NoArticle)
                {
                    Caption = 'N° Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        Item: Record Item;
                        FrmItem: Page "Item List";
                    begin
                        Item.RESET();
                        CLEAR(FrmItem);
                        FrmItem.SETTABLEVIEW(Item);
                        FrmItem.LOOKUPMODE(TRUE);
                        IF FrmItem.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmItem.GETRECORD(Item);
                            NoArticle := Item."No. 2";
                            ValidateNoArticle();
                        END
                    end;

                    trigger OnValidate()
                    begin
                        ValidateNoArticle();
                    end;
                }
                field(NoArticleInterne; NoArticleInterne)
                {
                    Caption = 'N° Article Interne';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article Interne field.';
                }
                field(txtComment; txtComment)
                {
                    Caption = 'Commentaire';
                    Enabled = txtCommentEnable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaire field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(btn_Valider)
            {
                Caption = 'Valider';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Valider action.';

                trigger OnAction()
                begin
                    ValiderCommentaire();
                end;
            }
        }
    }

    trigger OnInit()
    begin
        txtCommentEnable := TRUE;
    end;

    trigger OnOpenPage()
    begin
        ViderInfoLigne();
    end;

    var
        Item: Record Item;
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        ESK001Err: Label 'No Réception incorrect !';
        ESK002Err: Label 'Veuillez d''abord choisir une réception.';
        ESK003Lbl: Label 'Article %1 n''existe pas.';

        //ESK004: Label 'Veuillez d''abord choisir une réception et un article.';
        ESK005Lbl: Label 'L''article %1 n''est pas présent sur la réception %2.';
        NoFusion: Code[20];
        NoArticle: Code[20];
        NoArticleInterne: Code[20];
        txtComment: Text[80];
        txtCommentEnable: Boolean;

    procedure ValidateNoArticle()
    var
        //  Reception: Record "Warehouse Receipt Header";
        rec_Comment: Record "Warehouse Comment Line";
        LineReception: Record "Warehouse Receipt Line";
    begin
        IF NoFusion = '' THEN BEGIN
            ViderInfoLigne();
            ERROR(ESK002Err);
        END;

        IF NoArticle = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            NoArticleInterne := GestionMultiRef.RechercheMultiReference(NoArticle);
            IF NoArticleInterne <> 'RIENTROUVE' THEN
                Item.GET(NoArticleInterne)
            ELSE BEGIN
                ViderInfoLigne();
                ERROR(STRSUBSTNO(ESK003Lbl, NoArticle));
            END;
            // Vérification de l'existence de l'article sur la réception
            LineReception.SETRANGE("N° Fusion Réception", NoFusion);
            LineReception.SETRANGE("Item No.", Item."No.");
            IF LineReception.ISEMPTY THEN BEGIN
                ViderInfoLigne();
                ERROR(STRSUBSTNO(ESK005Lbl, NoArticle, NoFusion));
            END;
            // On va chercher s'il existe déjà en commentaire
            rec_Comment.SETRANGE("Table Name", rec_Comment."Table Name"::"Whse. Receipt");
            rec_Comment.SETRANGE(Type, rec_Comment.Type::"Pre-Receipt");
            rec_Comment.SETRANGE("No.", NoFusion);
            rec_Comment.SETRANGE("Item No.", NoArticleInterne);
            IF rec_Comment.FINDFIRST() THEN
                txtComment := rec_Comment.Comment;
            EnabledComment(TRUE);
        END;
    end;

    procedure ValidateNoFusion()
    var
        Reception: Record "Warehouse Receipt Header";
    begin
        IF NoFusion = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            Reception.GET(NoFusion);
            IF (Reception."N° Fusion Réception" <> '') THEN
                IF Reception."N° Fusion Réception" <> Reception."No." THEN BEGIN
                    NoFusion := '';
                    ERROR(ESK001Err);
                END;
        END;
    end;

    procedure ViderInfoLigne()
    begin
        NoFusion := '';
        NoArticle := '';
        NoArticleInterne := '';
        txtComment := '';
        EnabledComment(FALSE);
    end;

    procedure EnabledComment(bln_IsEnabled: Boolean)
    begin
        txtCommentEnable := bln_IsEnabled;
    end;

    procedure ValiderCommentaire()
    var
        NewComment: Record "Warehouse Comment Line";
        LNoLigne: Integer;
    begin
        // Insertion du commentaire
        WITH NewComment DO BEGIN
            NewComment.RESET();
            NewComment.SETRANGE("Table Name", "Table Name"::"Whse. Receipt");
            NewComment.SETRANGE(Type, Type::"Pre-Receipt");
            NewComment.SETRANGE("Item No.", NoArticleInterne);
            NewComment.SETRANGE("No.", NoFusion);
            IF NewComment.FINDFIRST() THEN
                NewComment.DELETE();

            NewComment.RESET();
            NewComment.SETRANGE("Table Name", "Table Name"::"Whse. Receipt");
            NewComment.SETRANGE(Type, Type::"Pre-Receipt");
            NewComment.SETRANGE("No.", NoFusion);
            IF NewComment.FINDLAST() THEN
                LNoLigne := NewComment."Line No." + 10000
            ELSE
                LNoLigne := 10000;



            INIT();
            "Table Name" := "Table Name"::"Whse. Receipt";
            Type := Type::"Pre-Receipt";
            Date := WORKDATE();
            "Item No." := NoArticleInterne;
            "No." := NoFusion;
            Comment := txtComment;
            "Line No." := LNoLigne;
            IF NOT INSERT(TRUE) THEN MODIFY(TRUE);
        END;
        ViderInfoLigne();
    end;
}

