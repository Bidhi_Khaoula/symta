page 50352 "WIIO - DetailReceptionEntrepot"
{

    Editable = false;
    PageType = List;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field(NoReception; NoReception)
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the NoReception field.';
            }
            repeater(Content2)
            {
                ShowCaption = false;
                field("No."; Rec."No.")
                {
                    Caption = 'N° Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article field.';

                    trigger OnLookup(var Text: Text): Boolean;
                    var

                    begin
                    end;
                }
                field(NoArticleInterne; NoArticleInterne)
                {
                    Caption = 'N° Article Interne';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Article Interne field.';
                }
                field(Description; Rec.Description)
                {
                    Caption = 'Désignation';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    Caption = 'Désignation 2';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation 2 field.';
                }
                field(CtrlPoidsArticle; PoidsArticle)
                {
                    Caption = 'Poids Net';
                    DecimalPlaces = 2 :;
                    Editable = CtrlPoidsArticleEditable;
                    Enabled = CtrlPoidsArticleEditable;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Poids Net field.';
                }
                field(Inventory; Rec.Inventory)
                {
                    Caption = 'Physique';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Physique field.';
                }
                field("Qty. on Sales Order"; Rec."Qty. on Sales Order")
                {
                    Caption = 'Qté sur Cde Vente';
                    Editable = false;
                    Enabled = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Qté sur Cde Vente field.';

                    trigger OnDrillDown();
                    var

                    begin
                    end;
                }
                field(Stocké; Rec.Stocké)
                {
                    Caption = 'Stocké';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field(Comment; Rec.Comment)
                {
                    Caption = 'Commentaires';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires field.';
                }
                field(ImprimerEtiquetteParArticle; ImprimerEtiquetteParArticle)
                {
                    Caption = 'Imprimer Etiquette Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Imprimer Etiquette Article field.';
                }
                field(NbARecevoir; NbARecevoir)
                {
                    Caption = 'A Réceptionner';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the A Réceptionner field.';
                }
                field(NbRecus; NbRecus)
                {
                    Caption = 'Déjà Réceptionné';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Déjà Réceptionné field.';
                }
                field(QteReçue; QteReçue)
                {
                    Caption = 'Quantité reçue';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité reçue field.';
                }
                field(YourComment; YourComment)
                {
                    Caption = 'Votre commentaire';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Votre commentaire field.';
                }
                field(EmplacementRangement; EmplacementRangement)
                {
                    Caption = 'Emplacement';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Emplacement field.';
                }
                field(HorsReception; HorsReception)
                {
                    Editable = false;
                    Enabled = false;
                    Style = Strong;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the HorsReception field.';
                }
                field(capacité; capacité)
                {
                    Caption = 'Capacité casier';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Capacité casier field.';
                }
                field("Sales multiple"; Rec."Sales multiple")
                {
                    Caption = 'Multiple de vente';
                    Editable = false;
                    Enabled = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Multiple de vente field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
        }
    }

    trigger OnAfterGetRecord();
    begin
        NoFusion := NoReception;
        ValidateNoFusion();
        ValidateNoArticle();
    end;

    var
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        NoFusion: Code[20];
        NoArticleInterne: Code[20];
        "QteReçue": Decimal;
        EmplacementRangement: Code[10];
        PoidsArticle: Decimal;
        GenImprimerEtiquetteParArticle: Boolean;
        ImprimerEtiquetteParArticle: Boolean;
        /*   "ImprimerRéférenceSP": Boolean;
          ImprimanteEtiquette: Code[20];
          CodeUtilisateur: Code[20];
          "---": Integer; */
        NbArtARecevoir: Decimal;
        NbArtRecus: Decimal;
        NbARecevoir: Decimal;
        NbRecus: Decimal;
        HorsReception: Code[40];

        gComment: Text[80];
        NomFrn: Text[50];
        "capacité": Decimal;
        [InDataSet]
        CtrlPoidsArticleEditable: Boolean;
        // Focus: Text[50];
        YourComment: Text[80];
        ESK001Err: Label 'No Réception incorrect !';
        //ESK002: Label 'HORS RECEPTION';
        //ESK003: Label 'Enregistrer la ligne ?';
        //ESK004: Label 'Veuillez d''abord choisir une réception.';
        ESK005Lbl: Label 'Article %1 n''existe pas.', Comment = '%1 = No Article';
        /*  ESK006: Label 'Référence à marquer !!';
         ESK007: Label 'Référence à controler !! Statut : %1.';
         ESK008: Label 'Le multiple d''achat pour l''article est %1.';
         ESK009: Label 'Veuillez d''abord choisir un article.';
         ESK010: Label 'Voulez vous modifier le poids de l''article à %1 ?';
         ESK011: Label 'Vous devez choisir une réception !';
         CONFIRM001: Label 'Voulez-vous commencer le pointage de cette reception pour le fournisseur (%1) %2 ?';
  */
        NoReception: Code[20];

    procedure ViderInfoLigne();
    begin
        NoArticleInterne := '';

        Jauge(NoFusion, NbArtARecevoir, NbArtRecus);


        NbARecevoir := 0;
        NbRecus := 0;
        QteReçue := 0;
        EmplacementRangement := '';
        HorsReception := '';
        gComment := '';
        PoidsArticle := 0;
        YourComment := '';
    end;

    procedure ValidateNoFusion();
    var
        Reception: Record "Warehouse Receipt Header";

    begin
        IF NoFusion = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            Reception.GET(NoFusion);
            IF (Reception."N° Fusion Réception" <> '') THEN
                IF Reception."N° Fusion Réception" <> Reception."No." THEN BEGIN
                    NoFusion := '';
                    ERROR(ESK001Err);
                END;

            Jauge(NoFusion, NbArtARecevoir, NbArtRecus);
            NomFrn := Reception.GetVendorName();
        END;
    end;

    procedure ValidateNoArticle();
    var
        BinContent: Record "Bin Content";
        Reception: Record "Warehouse Receipt Header";

        cduL_GestionSYMTA: Codeunit "Gestion SYMTA";
        TempCommentLine: Record "Comment Line" temporary;
    begin
        ImprimerEtiquetteParArticle := GenImprimerEtiquetteParArticle;

        NoArticleInterne := GestionMultiRef.RechercheMultiReference(Rec."No.");
        IF NoArticleInterne <> 'RIENTROUVE' THEN
            Rec.GET(NoArticleInterne)
        ELSE BEGIN
            ViderInfoLigne();
            ERROR(STRSUBSTNO(ESK005Lbl, Rec."No."));
        END;


        PoidsArticle := Rec."Net Weight";
        CtrlPoidsArticleEditable := PoidsArticle = 0;

        Rec.CALCFIELDS(Inventory, "Qty. on Sales Order");

        QteArticle(NoFusion, NoArticleInterne);

        BinContent.RESET();

        IF NoFusion <> '' THEN BEGIN
            Reception.GET(NoFusion);
            BinContent.SETRANGE("Location Code", Reception."Location Code");
        END;

        BinContent.SETRANGE("Item No.", Rec."No.");
        BinContent.SETRANGE(Default, TRUE);
        IF BinContent.FINDFIRST() THEN
            EmplacementRangement := BinContent."Bin Code";
        capacité := BinContent.capacité;

        // Récupération du commentaire
        IF NoFusion <> '' THEN
            gComment := RécupèreCommentaire(NoFusion, Rec."No.");

        // On charge dans une temporaire les 3 sortes de commentaires en excluant les doublons
        TempCommentLine.RESET();
        cduL_GestionSYMTA.GetCommentaireReception(TempCommentLine, NoArticleInterne, NoFusion, FALSE);
    end;

    procedure Jauge(_pNoFusion: Code[20]; var _pNbARecevoir: Decimal; var _pNbRecus: Decimal);
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
        TempMontantArticle: Record "Item Amount" temporary;
    begin
        // Cherche combien d'article a recevoir et conbien de recus
        TempMontantArticle.RESET();
        TempMontantArticle.DELETEALL();
        // Cherche le nb à recevoir
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                TempMontantArticle.RESET();
                TempMontantArticle.SETRANGE("Item No.", WhseReceiptLine."Item No.");
                IF TempMontantArticle.FINDFIRST() THEN
                    TempMontantArticle.RENAME(TempMontantArticle.Amount + WhseReceiptLine.Quantity, TempMontantArticle."Amount 2", TempMontantArticle."Item No."
              )
                ELSE BEGIN
                    TempMontantArticle.INIT();
                    TempMontantArticle.Amount := WhseReceiptLine."Qty. Outstanding (Base)";
                    TempMontantArticle."Amount 2" := 0;
                    TempMontantArticle."Item No." := WhseReceiptLine."Item No.";
                    TempMontantArticle.INSERT(TRUE);
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Cherche le nb à de recus complets
        TempMontantArticle.RESET();
        _pNbARecevoir := TempMontantArticle.COUNT;
        _pNbRecus := 0;
        IF TempMontantArticle.FINDFIRST() THEN
            REPEAT
                "Pré-reception".RESET();
                "Pré-reception".SETCURRENTKEY("No.", "Item No.");
                "Pré-reception".SETRANGE("No.", _pNoFusion);
                "Pré-reception".SETRANGE("Item No.", TempMontantArticle."Item No.");
                "Pré-reception".CALCSUMS(Quantity);
                IF "Pré-reception".Quantity >= TempMontantArticle.Amount THEN
                    _pNbRecus += 1;
            UNTIL TempMontantArticle.NEXT() = 0;
    end;

    procedure QteArticle(_pNoFusion: Code[20]; _pItemNo: Code[20]);
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        "Pré-reception": Record "Saisis Réception Magasin";
    begin
        HorsReception := '';
        NbARecevoir := 0;
        NbRecus := 0;

        WhseReceiptLine.RESET();
        WhseReceiptLine.SETCURRENTKEY("Item No.", "N° Fusion Réception");
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pNoFusion);
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);

        WhseReceiptLine.CALCSUMS("Qty. Outstanding (Base)", "Qty. to Receive (Base)");

        "Pré-reception".RESET();
        "Pré-reception".SETCURRENTKEY("No.", "Item No.");
        "Pré-reception".SETRANGE("No.", _pNoFusion);
        "Pré-reception".SETRANGE("Item No.", _pItemNo);
        "Pré-reception".CALCSUMS(Quantity);
        NbARecevoir := WhseReceiptLine."Qty. to Receive (Base)";
        NbRecus := "Pré-reception".Quantity;
    end;

    procedure "RécupèreCommentaire"(_pNoFusion: Code[20]; _pNoArticle: Code[20]): Text[60];
    var
        rec_Comment: Record "Warehouse Comment Line";
    begin
        // Récupère le commentaire s'il existe.
        rec_Comment.SETRANGE("Table Name", rec_Comment."Table Name"::"Whse. Receipt");
        rec_Comment.SETRANGE(Type, rec_Comment.Type::"Pre-Receipt");
        rec_Comment.SETRANGE("No.", _pNoFusion);
        rec_Comment.SETRANGE("Item No.", _pNoArticle);
        IF rec_Comment.FINDFIRST() THEN
            EXIT(rec_Comment.Comment)
        ELSE
            EXIT('');
    end;

    procedure VerifSiRefAControler(_pItemNo: Code[20]; _pFusionNo: Code[20]; var _pTxtMsg: Text[80]): Boolean;
    var
        WhseReceiptLine: Record "Warehouse Receipt Line";
        PurchaseLine: Record "Purchase Line";
        ItemVendor: Record "Item Vendor";
    begin
        _pTxtMsg := '';

        // Verif pour la reception principale
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("No.", _pFusionNo);
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;

        // Verif pour les autres receptions
        WhseReceiptLine.RESET();
        WhseReceiptLine.SETRANGE("N° Fusion Réception", _pFusionNo);  // La modif est ici
        WhseReceiptLine.SETRANGE("Item No.", _pItemNo);
        IF WhseReceiptLine.FINDFIRST() THEN
            REPEAT
                IF WhseReceiptLine."Source Document" = WhseReceiptLine."Source Document"::"Purchase Order" THEN BEGIN
                    PurchaseLine.GET(PurchaseLine."Document Type"::Order, WhseReceiptLine."Source No.", WhseReceiptLine."Source Line No.");
                    IF ItemVendor.GET(PurchaseLine."Buy-from Vendor No.", WhseReceiptLine."Item No.", WhseReceiptLine."Variant Code") THEN
                        IF ItemVendor."Statut Qualité" <> ItemVendor."Statut Qualité"::Conforme THEN BEGIN
                            _pTxtMsg := FORMAT(ItemVendor."Statut Qualité");
                            EXIT(TRUE);
                        END;
                END;
            UNTIL WhseReceiptLine.NEXT() = 0;
    end;
}

