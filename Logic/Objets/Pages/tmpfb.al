Page 50265 "tmp fb"
{
    Permissions = TableData "Sales Shipment Header" = rimd;
    SourceTable = "Sales Shipment Header";
    PageType = List;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                ShowCaption = false;
                Field("No."; Rec."No.")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                Field("Entierement facturé"; Rec."Entierement facturé")
                {
                    ApplicationArea = all;
                    ToolTip = 'Specifies the value of the Entierement facturé field.';
                }
            }

        }

    }
}

