page 50097 "Etiquette NICELABEL"
{
    CardPageID = "Etiquette NICELABEL";
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = Card;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field(txt_User; _User)
                {
                    Caption = 'User';
                    Editable = true;
                    TableRelation = "User Setup";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the User field.';
                }
                field(_Date; _Date)
                {
                    Caption = 'Date';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date field.';
                }
            }
            group("Article et quantité")
            {
                Caption = 'Article et quantité';
                field(_NoArticle; _NoArticle)
                {
                    Caption = 'Active Reference';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Active Reference field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        LItem: Record Item;
                        FrmItem: Page "Item List";
                    begin
                        CLEAR(LItem);
                        CLEAR(FrmItem);
                        FrmItem.SETTABLEVIEW(LItem);
                        FrmItem.LOOKUPMODE(TRUE);
                        IF FrmItem.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                            FrmItem.GETRECORD(LItem);
                            _NoArticle := LItem."No. 2";
                            ValideNoArticle();
                        END
                    end;

                    trigger OnValidate()
                    var
                        "User Setup": Record "User Setup";
                    begin
                        ValideNoArticle();
                    end;
                }
                field(ItemDescription; Item.Description)
                {
                    Caption = 'Désignation';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("Ref. SP"; Item."No.")
                {
                    Caption = 'Ref. SP';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. SP field.';
                }
                field(_QtyCondit; _QtyCondit)
                {
                    Caption = 'Quantité';
                    // DecimalPlaces = 0 : 0;
                    MinValue = 1;
                    ApplicationArea = All;
                    StyleExpr = gStyleExp_QtyCondit;
                    ToolTip = 'Specifies the value of the Quantité field.';
                    trigger OnValidate()
                    begin
                        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
                        SetStyleExp_QtyCondit();
                    end;
                }
            }
            group("Lot d'étiquettes depuis :")
            {
                Caption = 'Lot d''étiquettes depuis :';
                field(_NoCommande; _NoCommande)
                {
                    Caption = 'N° Commande';
                    TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Order));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Commande field.';
                }
                field(_NoBl; _NoBl)
                {
                    Caption = 'N° B.L.';
                    TableRelation = "Sales Shipment Header"."No.";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° B.L. field.';
                }
                field(_NoRM; _NoRM)
                {
                    Caption = 'N° Réception';
                    TableRelation = "Warehouse Receipt Header"."No.";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Réception field.';
                }
                field(_NoRME; _NoRME)
                {
                    Caption = 'N° Réception Enregistrée';
                    TableRelation = "Posted Whse. Receipt Header"."No.";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Réception Enregistrée field.';
                }
                field(_FeuilleInv; _FeuilleInv)
                {
                    Caption = 'Feuille Inv.';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Feuille Inv. field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        LEnrg: Record "Item Journal Line";
                    begin
                        LEnrg.SETRANGE("Journal Template Name", 'INV. PHYS.');
                        LEnrg.SETRANGE("Journal Batch Name", '');
                        ItemJnlMgt.LookupName(_FeuilleInv, LEnrg);
                    end;
                }
                field(_FeuilleArt; _FeuilleArt)
                {
                    Caption = 'Feuille Article';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Feuille Article field.';

                    trigger OnLookup(var Text: Text): Boolean
                    var
                        LEnrg: Record "Item Journal Line";
                    begin
                        LEnrg.SETRANGE("Journal Template Name", 'ARTICLE');
                        LEnrg.SETRANGE("Journal Batch Name", '');
                        ItemJnlMgt.LookupName(_FeuilleArt, LEnrg);
                    end;
                }
            }
            group("Impression et commentaires")
            {
                Caption = 'Impression et commentaires';
                field(_QtyEtq; _QtyEtq)
                {
                    Caption = 'Number Of Label';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Number Of Label field.';
                }
                field(_Format; _Format)
                {
                    Caption = 'Number Of Label';
                    TableRelation = "Generals Parameters".Code WHERE(Type = CONST('NICELBL_FRM'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Number Of Label field.';

                    trigger OnValidate()
                    var
                        LParam: Record "Generals Parameters";
                        LParam2: Record "Generals Parameters";
                    begin
                        IF LParam.GET('NICELBL_FRM', _Format) THEN
                            IF LParam2.GET('NICELBL_IMP', LParam.LongCode) THEN
                                _Imprimante := LParam2.Code;
                    end;
                }
                field(_Imprimante; _Imprimante)
                {
                    Caption = 'Printer';
                    TableRelation = "Generals Parameters".Code WHERE(Type = FILTER('NICELBL_IMP' | 'IMP_ETQ_RECEPT'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Printer field.';
                }
                field(""; '')
                {
                    Editable = false;
                    Enabled = false;
                    Visible = true;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(_TabComment_1; _TabComment[1])
                {
                    Caption = 'Commentaires 1 / Palette';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires 1 / Palette field.';
                }
                field(_TabComment_2; _TabComment[2])
                {
                    Caption = 'Commentaires 2';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires 2 field.';
                }
                field(_TabComment_3; _TabComment[3])
                {
                    Caption = 'Commentaires 3';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Commentaires 3 field.';
                }
                field(_Texte; _Texte)
                {
                    Caption = 'Texte';
                    TableRelation = "Generals Parameters".Code WHERE(Type = CONST('NICELBL_TXT'));
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Texte field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Imprimer)
            {
                Caption = 'Imprimer';
                action(Etiquette)
                {
                    Caption = 'Etiquette';
                    Image = Print;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ShortCutKey = 'F9';
                    ApplicationArea = All;
                    ToolTip = 'Executes the Etiquette action.';

                    trigger OnAction()
                    begin

                        ImprimerEtiquette();
                    end;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin

        _Date := WORKDATE();
        _User := USERID;
        // MCO Le 24-03-2016 => Initialisation du nombre d'étiquette
        _QtyEtq := 1;
        // MCO Le 24-03-2016 => Initialisation du nombre d'étiquette
    end;

    trigger OnAfterGetRecord()
    begin
        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        SetStyleExp_QtyCondit();
    end;

    var
        Item: Record Item;
        GestionMultiRef: Codeunit "Gestion Multi-référence";
        GestionMagasin: Codeunit "Gestion Magasin";

        ItemJnlMgt: Codeunit ItemJnlManagement;
        ESK005Lbl: Label 'Article %1 Inexistant !', Comment = '%1 = Numero Article';
        ESK001Err: Label 'Quantité Incorrecte';
        ESK002Err: Label 'Format Obligatoire';
        ESK003Err: Label 'Imprimante Obligatoire';
        _User: Code[30];
        _Date: Date;
        _NoArticle: Code[20];
        _QtyCondit: Integer;
        _QtyEtq: Integer;
        _Imprimante: Code[20];
        _Format: Code[10];
        _Texte: Code[20];
        NoArticleInterne: Code[20];


        _NoCommande: Code[20];
        _NoRM: Code[20];
        _NoRME: Code[20];
        _NoBl: Code[20];

        _TabComment: array[3] of Text[30];
        _FeuilleInv: Code[20];
        _FeuilleArt: Code[20];
        gStyleExp_QtyCondit: Text;

    procedure ValideNoArticle()
    begin
        IF _NoArticle = '' THEN
            ViderInfoLigne()
        ELSE BEGIN
            NoArticleInterne := GestionMultiRef.RechercheMultiReference(_NoArticle);
            IF NoArticleInterne <> 'RIENTROUVE' THEN BEGIN
                Item.GET(NoArticleInterne);
                _QtyCondit := Item."Sales multiple";
                IF _QtyCondit < 1 THEN _QtyCondit := 1;
            END
            ELSE BEGIN
                ViderInfoLigne();
                ERROR(STRSUBSTNO(ESK005Lbl, _NoArticle));
            END;
        END;
        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        SetStyleExp_QtyCondit();
    end;

    procedure ViderInfoLigne()
    begin
        CLEAR(_NoArticle);
        CLEAR(_QtyEtq);
        CLEAR(NoArticleInterne);
        CLEAR(Item);
        CLEAR(_Format);
        CLEAR(_Texte);
        CLEAR(_NoCommande);
        CLEAR(_NoRM);
        CLEAR(_NoRME);
        CLEAR(_NoBl);
        _QtyCondit := 1;
        CLEAR(_TabComment);

        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        SetStyleExp_QtyCondit();
    end;

    procedure ImprimerEtiquette()
    var
        LCuEtq: Codeunit "Etiquettes Articles";
    begin
        // TEST DES VARIABLES
        IF _QtyEtq < 1 THEN
            ERROR(ESK001Err);
        IF _Format = '' THEN
            ERROR(ESK002Err);
        IF _Imprimante = '' THEN
            ERROR(ESK003Err);

        // Impression pour un article
        IF Item."No." <> '' THEN
            LCuEtq.EditionEtiquetteUniquNiceLabel(Item, FormatCondit(_QtyCondit), _Texte, '', GestionMagasin.GetCasierDefaut(Item."No."),
                _TabComment, '', _QtyEtq, _Format, _Imprimante);

        // Impression pour une commande
        IF _NoCommande <> '' THEN
            ImprimerCde(_NoCommande);


        // Impression pour un BL
        IF _NoBl <> '' THEN
            ImprimerBL(_NoBl);

        // Impression pour une réception
        IF _NoRM <> '' THEN
            ImprimerRM(_NoRM);

        // Impression pour une réception enregistrée
        IF _NoRME <> '' THEN
            ImprimerRME(_NoRME);

        // Immpression pour feuillle inventaire
        IF _FeuilleInv <> '' THEN
            ImprimerFeuilleInv('INV. PHYS.', _FeuilleInv);

        // Immpression pour feuillle article
        IF _FeuilleArt <> '' THEN
            ImprimerFeuilleInv('ARTICLE', _FeuilleArt);


        ViderInfoLigne();
        MESSAGE('Impression Terminée');
    end;

    procedure ImprimerCde(_pNoDoc: Code[20])
    var
        LLigDoc: Record "Sales Line";
        LCuEtq: Codeunit "Etiquettes Articles";
        LFichierEtq: File;
    begin
        //_pNoDoc := DELCHR(_pNoDoc, '=', '-');
        LCuEtq.OuvrirEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);

        CLEAR(LLigDoc);
        LLigDoc.SETRANGE("Document Type", LLigDoc."Document Type"::Order);
        LLigDoc.SETRANGE("Document No.", _pNoDoc);
        LLigDoc.SETRANGE(Type, LLigDoc.Type::Item);
        LLigDoc.FINDFIRST();
        REPEAT
            Item.GET(LLigDoc."No.");
            LCuEtq.EdittEtiqetteNiceLabel(LFichierEtq, Item, FormatCondit(_QtyCondit), _Texte, _pNoDoc,
                GestionMagasin.GetCasierDefaut(Item."No."),
                _TabComment, '', _QtyEtq, _Format, _Imprimante);
        UNTIL LLigDoc.NEXT() = 0;

        LCuEtq.FermeEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);
    end;

    procedure ImprimerBL(_pNoDoc: Code[20])
    var
        LLigDoc: Record "Sales Shipment Line";
        LCuEtq: Codeunit "Etiquettes Articles";
        LFichierEtq: File;
    begin
        LCuEtq.OuvrirEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);

        CLEAR(LLigDoc);
        LLigDoc.SETRANGE("Document No.", _pNoDoc);
        LLigDoc.SETRANGE(Type, LLigDoc.Type::Item);
        LLigDoc.FINDFIRST();
        REPEAT
            Item.GET(LLigDoc."No.");
            LCuEtq.EdittEtiqetteNiceLabel(LFichierEtq, Item, FormatCondit(_QtyCondit), _Texte, _pNoDoc,
            GestionMagasin.GetCasierDefaut(Item."No."),
                _TabComment, '', _QtyEtq, _Format, _Imprimante);
        UNTIL LLigDoc.NEXT() = 0;
        LCuEtq.FermeEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);
    end;

    procedure ImprimerRM(_pNoDoc: Code[20])
    var
        LLigDoc: Record "Warehouse Receipt Line";
        LCuEtq: Codeunit "Etiquettes Articles";
        LFichierEtq: File;
    begin
        LCuEtq.OuvrirEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);

        CLEAR(LLigDoc);
        LLigDoc.SETRANGE("No.", _pNoDoc);
        LLigDoc.FINDFIRST();
        REPEAT
            Item.GET(LLigDoc."Item No.");
            LCuEtq.EdittEtiqetteNiceLabel(LFichierEtq, Item, FormatCondit(_QtyCondit), _Texte, _pNoDoc,
            GestionMagasin.GetCasierDefaut(Item."No."),
                _TabComment, '', _QtyEtq, _Format, _Imprimante);
        UNTIL LLigDoc.NEXT() = 0;
        LCuEtq.FermeEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);
    end;

    procedure ImprimerRME(_pNoDoc: Code[20])
    var
        LLigDoc: Record "Posted Whse. Receipt Line";
        LCuEtq: Codeunit "Etiquettes Articles";
        LFichierEtq: File;
    begin
        LCuEtq.OuvrirEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);

        CLEAR(LLigDoc);

        LLigDoc.SETRANGE("No.", _pNoDoc);
        LLigDoc.FINDFIRST();
        REPEAT
            Item.GET(LLigDoc."Item No.");
            LCuEtq.EdittEtiqetteNiceLabel(LFichierEtq, Item, FormatCondit(_QtyCondit), _Texte, _pNoDoc,
                GestionMagasin.GetCasierDefaut(Item."No."),
                _TabComment, '', _QtyEtq, _Format, _Imprimante);
        UNTIL LLigDoc.NEXT() = 0;
        LCuEtq.FermeEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pNoDoc, LFichierEtq);
    end;

    procedure ImprimerFeuilleInv(_pTypeDoc: Code[20]; _pNoDoc: Code[20])
    var
        LLigDoc: Record "Item Journal Line";
        LCuEtq: Codeunit "Etiquettes Articles";
        LFichierEtq: File;
    begin
        LCuEtq.OuvrirEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pTypeDoc + '_' + _pNoDoc, LFichierEtq);

        CLEAR(LLigDoc);
        //CFR le 30/11/2022 => R‚gie : Tri par d‚faut sur [Code emplacement]
        LLigDoc.SETCURRENTKEY("Bin Code", "Item No.", "Variant Code", "Posting Date");
        LLigDoc.SETASCENDING("Bin Code", TRUE);
        //FIN CFR

        LLigDoc.SETRANGE("Journal Template Name", _pTypeDoc);
        LLigDoc.SETRANGE("Journal Batch Name", _pNoDoc);
        LLigDoc.FINDFIRST();
        REPEAT
            Item.GET(LLigDoc."Item No.");
            LCuEtq.EdittEtiqetteNiceLabel(LFichierEtq, Item, FormatCondit(Item."Sales multiple"),
                _Texte, _pNoDoc, LLigDoc."Bin Code",
                _TabComment, '', _QtyEtq, _Format, _Imprimante);
        UNTIL LLigDoc.NEXT() = 0;
        LCuEtq.FermeEtiquetteNiceLabel(DELCHR(USERID, '=', '\') + '_' + _pTypeDoc + '_' + _pNoDoc, LFichierEtq);
    end;

    procedure InitValeurs(_pNoArticle: Code[20]; _pNoCommande: Code[20]; _pNoBl: Code[20]; _pNoRm: Code[20]; _pNoRme: Code[20])
    begin
        IF _pNoArticle <> '' THEN BEGIN
            _NoArticle := _pNoArticle;
            ValideNoArticle();
        END;

        _NoCommande := _pNoCommande;
        _NoBl := _pNoBl;
        _NoRM := _pNoRm;
        _NoRME := _pNoRme;
    end;

    procedure FormatCondit(_pValeur: Integer): Integer
    begin
        IF _pValeur < 1 THEN _pValeur := 1;
        EXIT(_pValeur);
    end;

    LOCAL PROCEDURE SetStyleExp_QtyCondit();
    BEGIN
        //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
        gStyleExp_QtyCondit := '';
        IF (_QtyCondit > 1) THEN
            gStyleExp_QtyCondit := 'Unfavorable';
        //FIN CFR le 05/04/2023
    END;

    //CFR le 30/11/2022 => R‚gie : Tri par d‚faut sur [Code emplacement]
    //CFR le 05/04/2023 => R‚gie : [Couleur] sur champ _QtyCondit
}

