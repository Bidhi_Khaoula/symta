page 50021 "Tableau de synthèse V.2"
{

    CardPageID = "Tableau de synthèse V.2";
    PageType = Card;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field(WORKDATE; WORKDATE())
                {
                    Caption = 'Valeurs au :';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Valeurs au : field.';
                }
                field(Text19020869; '')
                {
                    CaptionClass = Text19020869Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19005396; '')
                {
                    CaptionClass = Text19005396Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19022981; '')
                {
                    CaptionClass = Text19022981Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19032198; '')
                {
                    CaptionClass = Text19032198Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(TotalAmount; TotalAmount)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmount field.';
                }
                field(TotalAmount_Number; TotalAmount_Number)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmount_Number field.';
                }
                field(Number; Number)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Number field.';
                }
                field(Text19080001; '')
                {
                    CaptionClass = Text19080001Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19065853; '')
                {
                    CaptionClass = Text19065853Lbl;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19080002; '')
                {
                    CaptionClass = Text19080002Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19080003; '')
                {
                    CaptionClass = Text19080003Lbl;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19014021; '')
                {
                    CaptionClass = Text19014021Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19017002; '')
                {
                    CaptionClass = Text19017002Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19016725; '')
                {
                    CaptionClass = Text19016725Lbl;
                    Style = Standard;
                    StyleExpr = TRUE;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19080004; '')
                {
                    CaptionClass = Text19080004Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19080005; '')
                {
                    CaptionClass = Text19080005Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(FacturesVentesJour_1; FacturesVentesJour[1])
                {
                    Caption = 'DU JOUR';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the DU JOUR field.';
                }
                field(FacturesVentesMois_1; FacturesVentesMois[1])
                {
                    Caption = 'MOIS EN COURS';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MOIS EN COURS field.';
                }
                field("FacturesVentesMoisN-1MJ_1"; "FacturesVentesMoisN-1MJ"[1])
                {
                    Caption = 'MOIS N-1 MEME JOUR';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MOIS N-1 MEME JOUR field.';
                }
                field("FacturesVentesMoisN-1_1"; "FacturesVentesMoisN-1"[1])
                {
                    Caption = 'MOIS N-1 COMPLET';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the MOIS N-1 COMPLET field.';
                }
                field(FacturesVentesAnnee_1; FacturesVentesAnnee[1])
                {
                    Caption = 'ANNEE EN COURS';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ANNEE EN COURS field.';
                }
                field("FacturesVentesAnneeN-1MJ_1"; "FacturesVentesAnneeN-1MJ"[1])
                {
                    Caption = 'ANNEE N-1 MEME JOUR';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ANNEE N-1 MEME JOUR field.';
                }
                field("FacturesVentesAnneeN-1_1"; "FacturesVentesAnneeN-1"[1])
                {
                    Caption = 'ANNEE N-1 COMPLETE';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ANNEE N-1 COMPLETE field.';
                }
                field(Text19057128; '')
                {
                    CaptionClass = Text19057128Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(TotalAmountPurchase; TotalAmountPurchase)
                {
                    Caption = 'CA HT';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the CA HT field.';
                }
                field(TotalAmountP; TotalAmountP)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmountP field.';
                }
                field(PORTEFEUILLE; HistoPrtf.Date)
                {
                    Caption = 'PORTEFEUILLE';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the PORTEFEUILLE field.';
                }
                field(Montant; HistoPrtf.Montant)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Montant field.';
                }
                field(TotalAmountP_NumberP; TotalAmountP_NumberP)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmountP_NumberP field.';
                }
                field(NumberP; NumberP)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the NumberP field.';
                }
                field(TotalAmountReliquat; TotalAmountReliquat)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmountReliquat field.';
                }
                field(TotalAmountShipment; TotalAmountShipment)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmountShipment field.';
                }
                field(Text19023848; '')
                {
                    CaptionClass = Text19023848Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(Text19026508; '')
                {
                    CaptionClass = Text19026508Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(FacturesVentesJour_2; FacturesVentesJour[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesJour[2] field.';
                }
                field(FacturesVentesJour_3; FacturesVentesJour[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesJour[3] field.';
                }
                field(FacturesVentesMois_2; FacturesVentesMois[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMois[2] field.';
                }
                field(FacturesVentesMois_3; FacturesVentesMois[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMois[3] field.';
                }
                field("FacturesVentesMoisN-1MJ_2"; "FacturesVentesMoisN-1MJ"[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMoisN-1MJ[2] field.';
                }
                field("FacturesVentesMoisN-1MJ_3"; "FacturesVentesMoisN-1MJ"[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMoisN-1MJ[3] field.';
                }
                field("FacturesVentesMoisN-1_2"; "FacturesVentesMoisN-1"[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMoisN-1[2] field.';
                }
                field("FacturesVentesMoisN-1_3"; "FacturesVentesMoisN-1"[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesMoisN-1[3] field.';
                }
                field(FacturesVentesAnnee_2; FacturesVentesAnnee[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnnee[2] field.';
                }
                field(FacturesVentesAnnee_3; FacturesVentesAnnee[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnnee[3] field.';
                }
                field("FacturesVentesAnneeN-1MJ_2"; "FacturesVentesAnneeN-1MJ"[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnneeN-1MJ[2] field.';
                }
                field("FacturesVentesAnneeN-1MJ_3"; "FacturesVentesAnneeN-1MJ"[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnneeN-1MJ[3] field.';
                }
                field("FacturesVentesAnneeN-1_2"; "FacturesVentesAnneeN-1"[2])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnneeN-1[2] field.';
                }
                field("FacturesVentesAnneeN-1_5"; "FacturesVentesAnneeN-1"[3])
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the FacturesVentesAnneeN-1[3] field.';
                }
                field(Text19034327; '')
                {
                    CaptionClass = Text19034327Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(TotalAmountPurchaseP; TotalAmountPurchaseP)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TotalAmountPurchaseP field.';
                }
                field(TauxServiceJour; TauxServiceJour)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TauxServiceJour field.';
                }
                field(TauxServiceMois; TauxServiceMois)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the TauxServiceMois field.';
                }
                field(ValeurStock; ValeurStock)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ValeurStock field.';
                }
            }
            group("Cadrage Stock")
            {
                Caption = 'Cadrage Stock';
                field("Stock"; 'Stock ' + FORMAT(MvtDateDeb))
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ''Stock '' + FORMAT(MvtDateDeb) field.';
                }
                field(" "; '')
                {
                    CaptionClass = Text19010045Lbl;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the '''' field.';
                }
                field(StockDerDate; StockDerDate)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the StockDerDate field.';
                }
                field(MvtVentes; MvtVentes)
                {
                    Caption = 'Mvt Ventes';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mvt Ventes field.';
                }
                field(MvtAchats; MvtAchats)
                {
                    Caption = 'Mvt Achats';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mvt Achats field.';
                }
                field(MvtAutres; MvtAutres)
                {
                    Caption = 'Mvt Autres';
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Mvt Autres field.';
                }
                field("StockDerDate +MvtVentes + MvtAchats + MvtAutres"; StockDerDate + MvtVentes + MvtAchats + MvtAutres)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the StockDerDate + MvtVentes + MvtAchats + MvtAutres field.';
                }
                field(ValeurStock1; ValeurStock)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the ValeurStock field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    var
        TraitelentNuit: Codeunit "Procédure de Nuit";
    begin
        // AD Le 19-02-2009 => QUestion avant le lancement du traitement
        IF NOT CONFIRM('Lancement du tableau de synthèse ?', FALSE) THEN
            EXIT;
        // FIN AD Le 19-02-2009

        calcul_stock := CONFIRM('Calcul du stock ?', FALSE);

        // AD Le 12-11-2010
        TraitelentNuit.CalculPortefeuilleVente();
        TraitelentNuit.CalculValeurDuStock();
        // FIN AD Le 12-11-2010


        // modif Sidamo CS le 24/3/04
        // Tableau de synthèse
        // Calcul des commandes du jour + commandes enregistrées
        Number := 0;
        TotalAmount := 0;
        TotalAmount_Number := 0;
        //initialisation variable tx de service
        NbArticle := 0;//E

        SalesHeader.RESET();
        //Fj  activer clé correspondand au filtre et préferer setrange au Setfilter
        //Filtre 1 date,Type
        SalesHeader.SETCURRENTKEY("Order Date", "Document Type");
        SalesHeader.SETFILTER(SalesHeader."Document Type", '%1|%2', SalesHeader."Document Type"::Order,
             SalesHeader."Document Type"::"Blanket Order");
        SalesHeader.SETRANGE(SalesHeader."Order Date", WORKDATE());
        SalesHeader.SETRANGE(Loans, FALSE);

        IF SalesHeader.FIND('-') THEN BEGIN
            "SartNo." := SalesHeader."No.";
            REPEAT
                //    SalesHeader.CALCFIELDS(Amount);
                //    TotalAmount+=SalesHeader.Amount;
                Number += 1;

                SalesLine.RESET();
                SalesLine.SETCURRENTKEY(SalesLine."Document Type", SalesLine."Document No.", SalesLine."Line No.");
                SalesLine.SETRANGE(SalesLine."Document Type", SalesHeader."Document Type");
                SalesLine.SETRANGE(SalesLine."Document No.", SalesHeader."No.");
                IF SalesLine.FIND('-') THEN
                    REPEAT
                        TotalAmount += SalesLine."Line Amount";
                    // Window.UPDATE(1,SalesLine."Document No.");
                    UNTIL SalesLine.NEXT() = 0;

            UNTIL SalesHeader.NEXT() = 0;
            "EndNo." := SalesHeader."No.";
        END;

        // cdes enregistrées
        //Filtre 1 arch date,type
        OrderHeaderArchive.RESET();
        OrderHeaderArchive.SETCURRENTKEY("Order Date", "Document Type");
        OrderHeaderArchive.SETRANGE("Document Type", SalesHeader."Document Type"::Order);
        OrderHeaderArchive.SETRANGE(OrderHeaderArchive."Order Date", WORKDATE());
        OrderHeaderArchive.SETRANGE(Loans, FALSE);


        IF OrderHeaderArchive.FIND('-') THEN
            REPEAT
                //   OrderHeaderArchive.CALCFIELDS(Amount);
                //   TotalAmount+=OrderHeaderArchive.Amount;

                Number += 1;

                OrderLineArchive.RESET();
                OrderLineArchive.SETCURRENTKEY(OrderLineArchive."Document Type", OrderLineArchive."Document No.",
                  OrderLineArchive."Line No.");
                OrderLineArchive.SETRANGE(OrderLineArchive."Document Type", OrderHeaderArchive."Document Type");
                OrderLineArchive.SETRANGE(OrderLineArchive."Document No.", OrderHeaderArchive."No.");
                IF OrderLineArchive.FIND('-') THEN
                    REPEAT

                        TotalAmount += OrderLineArchive."Line Amount";
                    // Window.UPDATE(1,OrderLineArchive."Document No.");

                    UNTIL OrderLineArchive.NEXT() = 0;

            UNTIL OrderHeaderArchive.NEXT() = 0;

        IF Number <> 0 THEN
            TotalAmount_Number := TotalAmount / Number;

        // Calcul du portefeuille des commandes de vente
        // ---------------------------------------------
        NumberP := 0;
        TotalAmountP := 0;
        TotalAmountReliquat := 0;
        TotalAmountP_NumberP := 0;

        SalesHeader.RESET();
        SalesHeader.SETCURRENTKEY("Order Date", SalesHeader."Document Type");
        SalesHeader.SETFILTER(SalesHeader."Document Type", '%1|%2', SalesHeader."Document Type"::Order,
             SalesHeader."Document Type"::"Blanket Order");
        SalesHeader.SETRANGE(Loans, FALSE);

        IF SalesHeader.FIND('-') THEN
            REPEAT
                NumberP += 1;

                SalesLine.RESET();
                SalesLine.SETCURRENTKEY(SalesLine."Document Type", SalesLine."Document No.", SalesLine."Line No.");
                SalesLine.SETRANGE(SalesLine."Document Type", SalesHeader."Document Type");
                SalesLine.SETRANGE("Document No.", SalesHeader."No.");

                IF SalesLine.FIND('-') THEN
                    REPEAT
                        IF SalesLine.Quantity <> 0 THEN BEGIN

                            IF SalesLine."Document Type" = SalesLine."Document Type"::Order THEN
                                TotalAmountP += SalesLine."Outstanding Quantity" * SalesLine."Net Unit Price"
                            ELSE
                                TotalAmountP += SalesLine."Blanket Order Quantity Outstan" * SalesLine."Net Unit Price";


                            IF SalesHeader."Shipment Status" = SalesHeader."Shipment Status"::Reliquat THEN
                                TotalAmountReliquat += SalesLine."Outstanding Quantity" * SalesLine."Net Unit Price";
                        END


                    UNTIL SalesLine.NEXT() = 0;

            UNTIL SalesHeader.NEXT() = 0;

        IF Number <> 0 THEN
            TotalAmount_Number := TotalAmount / Number;

        IF NumberP <> 0 THEN
            TotalAmountP_NumberP := TotalAmountP / NumberP;

        // Portefeuille de commande N-1
        HistoPrtf.RESET();
        HistoPrtf.SETRANGE("Type Ligne", HistoPrtf."Type Ligne"::"Portefeuille Commande");
        // AD Le 17-11-2010
        // HistoPrtf.SETRANGE(Date, CALCDATE('-1A', WORKDATE)); => CODE D'ORIGINE
        HistoPrtf.SETFILTER(Date, '..%1', CALCDATE('-1A', WORKDATE()));
        IF NOT HistoPrtf.FINDLAST() THEN
            CLEAR(HistoPrtf);

        // Livraisons à facturer

        TotalAmountShipment := 0;
        //pas filtre -> expédition vente enregistre livré non fac
        SalesShipmentLine.RESET();
        SalesShipmentLine.SETRANGE(Loans, FALSE);
        IF SalesShipmentLine.FIND('-') THEN
            REPEAT
                TotalAmountShipment += (SalesShipmentLine."Qty. Shipped Not Invoiced" * SalesShipmentLine."Net Unit Price");
            UNTIL SalesShipmentLine.NEXT() = 0;


        // *****************************************************************************************************
        // Calcul des factures du jour ATTENTION -> BASE SUR LES ECRITURES VALEURS DONC PAS LES COMPTES GENERAUX
        // [Ré-écrit par AD Le 06-11-2009]
        // *****************************************************************************************************
        // Facture du jour
        // ---------------
        "CalculCA-MargePeriode"(WORKDATE(), WORKDATE(), FacturesVentesJour[1], FacturesVentesJour[2]);
        IF FacturesVentesJour[1] <> 0 THEN
            FacturesVentesJour[3] := FacturesVentesJour[2] / FacturesVentesJour[1] * 100;



        // Facture du mois
        // ---------------
        DateDebut := CALCDATE('<-1M+FM+1J>', WORKDATE());
        DateFin := CALCDATE('<+FM>', DateDebut);
        "CalculCA-MargePeriode"(DateDebut, DateFin, FacturesVentesMois[1], FacturesVentesMois[2]);
        IF FacturesVentesMois[1] <> 0 THEN
            FacturesVentesMois[3] := FacturesVentesMois[2] / FacturesVentesMois[1] * 100;


        // Facture de l'année
        // ------------------
        DateDebut := DMY2DATE(1, 1, DATE2DMY(WORKDATE(), 3));
        DateFin := DMY2DATE(31, 12, DATE2DMY(WORKDATE(), 3));
        "CalculCA-MargePeriode"(DateDebut, DateFin, FacturesVentesAnnee[1], FacturesVentesAnnee[2]);
        IF FacturesVentesAnnee[1] <> 0 THEN
            FacturesVentesAnnee[3] := FacturesVentesAnnee[2] / FacturesVentesAnnee[1] * 100;

        // Facture du mois N-1 Complet
        // ---------------------------
        DateDebut := CALCDATE('<-1M+FM+1J-1A>', WORKDATE());
        DateFin := CALCDATE('<+FM>', DateDebut);
        "CalculCA-MargePeriode"(DateDebut, DateFin, "FacturesVentesMoisN-1"[1], "FacturesVentesMoisN-1"[2]);
        IF "FacturesVentesMoisN-1"[1] <> 0 THEN
            "FacturesVentesMoisN-1"[3] := "FacturesVentesMoisN-1"[2] / "FacturesVentesMoisN-1"[1] * 100;

        // Facture du mois N-1 Même jour
        // -----------------------------
        DateDebut := CALCDATE('<-1M+FM+1J-1A>', WORKDATE());
        DateFin := CALCDATE('<-1A>', WORKDATE());
        "CalculCA-MargePeriode"(DateDebut, DateFin, "FacturesVentesMoisN-1MJ"[1], "FacturesVentesMoisN-1MJ"[2]);
        IF "FacturesVentesMoisN-1MJ"[1] <> 0 THEN
            "FacturesVentesMoisN-1MJ"[3] := "FacturesVentesMoisN-1MJ"[2] / "FacturesVentesMoisN-1MJ"[1] * 100;

        // Facture de l'annee N-1 Complete
        // -------------------------------
        DateDebut := DMY2DATE(1, 1, DATE2DMY(WORKDATE(), 3) - 1);
        DateFin := DMY2DATE(31, 12, DATE2DMY(WORKDATE(), 3) - 1);
        "CalculCA-MargePeriode"(DateDebut, DateFin, "FacturesVentesAnneeN-1"[1], "FacturesVentesAnneeN-1"[2]);
        IF "FacturesVentesAnneeN-1"[1] <> 0 THEN
            "FacturesVentesAnneeN-1"[3] := "FacturesVentesAnneeN-1"[2] / "FacturesVentesAnneeN-1"[1] * 100;

        // Facture du mois N-1 Même jour
        // -----------------------------
        DateDebut := DMY2DATE(1, 1, DATE2DMY(WORKDATE(), 3) - 1);
        DateFin := CALCDATE('<-1A>', WORKDATE());
        "CalculCA-MargePeriode"(DateDebut, DateFin, "FacturesVentesAnneeN-1MJ"[1], "FacturesVentesAnneeN-1MJ"[2]);
        IF "FacturesVentesAnneeN-1MJ"[1] <> 0 THEN
            "FacturesVentesAnneeN-1MJ"[3] := "FacturesVentesAnneeN-1MJ"[2] / "FacturesVentesAnneeN-1MJ"[1] * 100;



        // *****************************************************************************************************
        // FIN du calcul des factures
        // *****************************************************************************************************

        //*
        // Calcul des commandes fournisseur du jour
        // ----------------------------------------
        TotalAmountPurchase := 0;

        //filtre 1 acha date type
        GLSetup.GET();

        PurchaseHeader.RESET();
        PurchaseHeader.SETCURRENTKEY("Document Type", "Order Date");
        PurchaseHeader.SETRANGE("Order Date", WORKDATE());
        PurchaseHeader.SETRANGE("Document Type", PurchaseHeader."Document Type"::Order);

        IF PurchaseHeader."Currency Factor" = 0 THEN
            PurchaseHeader."Currency Factor" := 1;

        IF PurchaseHeader.FIND('-') THEN
            REPEAT

                PurchaseLine.RESET();
                PurchaseLine.SETCURRENTKEY(PurchaseLine."Document Type", PurchaseLine."Document No.", PurchaseLine."Line No.");
                PurchaseLine.SETRANGE(PurchaseLine."Document Type", PurchaseHeader."Document Type");
                PurchaseLine.SETRANGE(PurchaseLine."Document No.", PurchaseHeader."No.");

                IF PurchaseLine.FIND('-') THEN
                    REPEAT
                        IF PurchaseLine.Quantity <> 0 THEN
                            // AD Le 01-02-2010 => Pour exclusion des % de frais
                            //TotalAmountPurchase += (PurchaseLine."Outstanding Quantity"*PurchaseLine."Unit Cost (LCY)");
                            IF PurchaseHeader."Currency Code" <> '' THEN BEGIN
                                UnitCostCurrency :=
                                  CurrExchRate.ExchangeAmtFCYToLCY(
                                    PurchaseHeader."Posting Date", PurchaseLine."Currency Code",
                                    PurchaseLine."Direct Unit Cost", PurchaseHeader."Currency Factor");
                            END ELSE
                                UnitCostCurrency := PurchaseLine."Direct Unit Cost";


                        TotalAmountPurchase += (PurchaseLine."Outstanding Quantity" * UnitCostCurrency);
                    // FIN AD Le 01-02-2010


                    UNTIL PurchaseLine.NEXT() = 0;
            UNTIL PurchaseHeader.NEXT() = 0;
        //*

        //*
        // Calcul du portefeuille commande fournisseur
        // -------------------------------------------
        TotalAmountPurchaseP := 0;
        //filtre  2 achat type
        PurchaseHeader.RESET();
        PurchaseHeader.SETCURRENTKEY("Document Type", "Order Date");
        PurchaseHeader.SETRANGE(PurchaseHeader."Document Type", PurchaseHeader."Document Type"::Order);

        IF PurchaseHeader.FIND('-') THEN
            REPEAT

                PurchaseLine.RESET();
                PurchaseLine.SETCURRENTKEY(PurchaseLine."Document Type", PurchaseLine."Document No.", PurchaseLine."Line No.");
                PurchaseLine.SETRANGE(PurchaseLine."Document Type", PurchaseHeader."Document Type");
                PurchaseLine.SETRANGE(PurchaseLine."Document No.", PurchaseHeader."No.");

                IF PurchaseLine.FIND('-') THEN
                    REPEAT
                        IF PurchaseLine.Quantity <> 0 THEN
                            // AD Le 01-02-2010 => Pour exclusion des % de frais
                            //TotalAmountPurchaseP += (PurchaseLine."Outstanding Quantity" * PurchaseLine."Unit Cost (LCY)");
                            IF PurchaseHeader."Currency Code" <> '' THEN BEGIN
                                UnitCostCurrency :=
                                  CurrExchRate.ExchangeAmtFCYToLCY(
                                    PurchaseHeader."Posting Date", PurchaseLine."Currency Code",
                                    PurchaseLine."Direct Unit Cost", PurchaseHeader."Currency Factor");
                            END ELSE
                                UnitCostCurrency := PurchaseLine."Direct Unit Cost";


                        TotalAmountPurchaseP += (PurchaseLine."Outstanding Quantity" * UnitCostCurrency);
                    // FIN AD Le 01-02-2010


                    // Window.UPDATE(1,PurchaseLine."Document No.");

                    UNTIL PurchaseLine.NEXT() = 0;

            UNTIL PurchaseHeader.NEXT() = 0;

        // Valeur du stock

        IF calcul_stock THEN BEGIN
            ValeurStock := 0;
            Item.RESET();

            Item.SETRANGE(Item."Sans Mouvements de Stock", FALSE);
            Item.SETFILTER("Date Filter", '..%1', WORKDATE());
            IF Item.FIND('-') THEN
                REPEAT
                    // Item.SETRANGE(Item."Factory Filter"TRUE);
                    // Item.SETRANGE(Item."Batch Fixed",FALSE);

                    // AD Le 12-11-2009 => Gestion du stock a date
                    // Item.CALCFIELDS(Inventory);
                    // IF Item.Inventory<>0 THEN
                    // BEGIN
                    //    pamp:=0;
                    //    IF Item."Costing Method"=Item."Costing Method"::Average THEN
                    //       pamp:=Item."Unit Cost";
                    //    IF pamp=0 THEN
                    //        pamp:=Item."Standard Cost";
                    //    IF pamp=0 THEN
                    //        pamp:=Item."Unit Cost";
                    //    ValeurStock+=pamp*Item.Inventory;
                    // END;
                    Item.CALCFIELDS("Inventory At Date");
                    IF Item."Inventory At Date" <> 0 THEN BEGIN
                        pamp := 0;
                        IF Item."Costing Method" = Item."Costing Method"::Average THEN
                            pamp := Item."Unit Cost";
                        IF pamp = 0 THEN
                            pamp := Item."Standard Cost";
                        IF pamp = 0 THEN
                            pamp := Item."Unit Cost";
                        ValeurStock += pamp * Item."Inventory At Date";
                    END;



                /*  est ce fini ?
                // on va chercher la quantité dans les points de stock : d'après moi, on peut prendre celle de l'article
                Quantité:=0;
                StockkeepingUnit.RESET();
                StockkeepingUnit.SETCURRENTKEY("Item No.");
                StockkeepingUnit.SETFILTER(StockkeepingUnit."Item No.", Item."No.");
                IF StockkeepingUnit.FIND('-') THEN
                REPEAT
                   StockkeepingUnit.CALCFIELDS(Inventory);
                   Quantité+=StockkeepingUnit.Inventory;

                UNTIL StockkeepingUnit.NEXT=0;
                ValeurStock+=(pamp*Quantité);
                */

                UNTIL Item.NEXT() = 0;
            CadrageStock();
        END;

        // *****************************************************************************************************
        // Calcul des des taux de service
        // [Ré-écrit par AD Le 06-11-2009]
        // *****************************************************************************************************
        TauxServiceJour := CalculTxService.CalculTauxService(WORKDATE(), WORKDATE(), '', '');
        TauxServiceMois := CalculTxService.CalculTauxService(CALCDATE('<-1M+FM+1J>', WORKDATE()), CALCDATE('<+FM>', WORKDATE()), '', '');
        // *****************************************************************************************************
        // Fin du Calcul des des taux de service
        // *****************************************************************************************************

    end;

    var

        SalesLine: Record "Sales Line";
        SalesHeader: Record "Sales Header";
        // SalesInvoiceHeader: Record "Sales Invoice Header";
        PurchaseHeader: Record "Purchase Header";
        OrderHeaderArchive: Record "Sales Header Archive";
        SalesShipmentLine: Record "Sales Shipment Line";
        PurchaseLine: Record "Purchase Line";
        Item: Record Item;
        OrderLineArchive: Record "Sales Line Archive";
        //    SalesInvoiceLine: Record "Sales Invoice Line";
        GLSetup: Record "General Ledger Setup";
        CurrExchRate: Record "Currency Exchange Rate";
        HistoPrtf: Record "Historisation Taux SAV / Porte";
        CalculTxService: Codeunit "Calcul Taux Service";

        TotalAmount: Decimal;
        Number: Integer;
        TotalAmount_Number: Decimal;
        // "CAHT Commande": Decimal;
        TotalAmountP: Decimal;
        NumberP: Integer;
        TotalAmountP_NumberP: Decimal;
        TotalAmountReliquat: Decimal;
        TotalAmountShipment: Decimal;


        DateDebut: Date;
        DateFin: Date;
        TotalAmountPurchase: Decimal;

        TotalAmountPurchaseP: Decimal;
        ValeurStock: Decimal;
        //  ItemCostMgt: Codeunit ItemCostManagement;
        pamp: Decimal;
        /*   AverageCostLCY: Decimal;
          AverageCostACY: Decimal;
          StockkeepingUnit: Record "Stockkeeping Unit";
          "Quantité": Integer;
          "qté commandée": Integer; */
        NbArticle: Integer;
        /*   dispo: Decimal;
          Location: Record Location;
          Month: Integer;
          LastDay: Integer;
                ExpectedInventory: Decimal;
          QtyAvailable: Decimal;
          AvailabilityMgt: Codeunit "Available Management";
          Available: Decimal;
          InitialQtyAvailable: Decimal;
          Window: Dialog; */
        "SartNo.": Code[20];
        "EndNo.": Code[20];
        /*  Cout: Decimal;
         CoutMonth: Decimal;
         coutYear: Decimal;
         salescreditheader: Record "Sales Cr.Memo Header";
         salescreditline: Record "Sales Cr.Memo Line"; */
        calcul_stock: Boolean;

        //    "--- Mouvelles variables [AD]": Integer;
        FacturesVentesJour: array[3] of Decimal;
        FacturesVentesMois: array[3] of Decimal;
        FacturesVentesAnnee: array[3] of Decimal;
        "FacturesVentesMoisN-1MJ": array[3] of Decimal;
        "FacturesVentesMoisN-1": array[3] of Decimal;
        "FacturesVentesAnneeN-1MJ": array[3] of Decimal;
        "FacturesVentesAnneeN-1": array[3] of Decimal;
        TauxServiceJour: Decimal;
        TauxServiceMois: Decimal;

        UnitCostCurrency: Decimal;
        // "---": Integer;
        MvtVentes: Decimal;
        MvtAchats: Decimal;
        MvtAutres: Decimal;
        MvtDateDeb: Date;
        StockDerDate: Decimal;
        "ValeurStockCalculé": Decimal;
        Text19020869Lbl: Label 'COMMANDES';
        Text19005396Lbl: Label 'COMMANDES EN RELIQUAT';
        Text19022981Lbl: Label 'CA HT';
        Text19010045Lbl: Label 'VALEUR';
        Text19032198Lbl: Label 'LIVRAISONS A FACTURER';
        Text19080001Lbl: Label 'CA HT';
        Text19065853Lbl: Label 'FOURNISSEUR';
        Text19080002Lbl: Label 'COMMANDES';
        Text19080003Lbl: Label 'FOURNISSEUR';
        Text19014021Lbl: Label 'TAUX DE SERVICE DU JOUR';
        Text19017002Lbl: Label 'TAUX DE SERVICE DU MOIS';
        Text19016725Lbl: Label 'STOCK';
        Text19080004Lbl: Label 'VALEUR';
        Text19080005Lbl: Label 'CA HT';
        Text19057128Lbl: Label 'DU JOUR';
        Text19023848Lbl: Label 'MARGE';
        Text19026508Lbl: Label '% MARGE';
        Text19034327Lbl: Label 'PORTEFEUILLE';

    procedure "CalculCA-MargePeriode"(DateDeb: Date; DateFin: Date; var Montant: Decimal; var Marge: Decimal)
    var
        ValueEntry: Record "Value Entry";
    begin
        ValueEntry.RESET();
        ValueEntry.SETRANGE("Posting Date", DateDeb, DateFin);
        ValueEntry.SETCURRENTKEY("Item No.", "Posting Date", "Item Ledger Entry Type",
                                                             "Entry Type", "Variance Type", "Item Charge No.", "Location Code", "Variant Code"
        );
        ValueEntry.SETRANGE("Item Ledger Entry Type", ValueEntry."Item Ledger Entry Type"::Sale);
        ValueEntry.SETRANGE("Item Charge No.", '');
        ValueEntry.CALCSUMS("Sales Amount (Actual)", "Cost Amount (Actual)");

        Montant := ValueEntry."Sales Amount (Actual)";
        Marge := Montant - (ValueEntry."Cost Amount (Actual)" * -1);
    end;

    procedure CadrageStock()
    var
        Histo: Record "Historisation Taux SAV / Porte";

        ValueEntry: Record "Value Entry";
        DerDate: Date;
    begin
        // Recherche le stock de la veille
        Histo.RESET();
        Histo.SETRANGE("Type Ligne", Histo."Type Ligne"::"Valeur du stock");
        Histo.SETRANGE(Date, CALCDATE('<-1J>', WORKDATE()));
        IF NOT Histo.FINDLAST() THEN BEGIN
            Histo.SETRANGE(Date);
            Histo.FINDLAST();
        END;


        DerDate := Histo.Date;
        StockDerDate := Histo.Montant;

        MvtDateDeb := DerDate;

        ValueEntry.RESET();
        ValueEntry.SETCURRENTKEY(
        "Item No.", "Posting Date", "Item Ledger Entry Type",
        "Entry Type", "Variance Type", "Item Charge No.", "Location Code", "Variant Code");
        ValueEntry.SETRANGE("Posting Date", CALCDATE('<+1J>', DerDate), WORKDATE());


        ValueEntry.SETRANGE("Item Ledger Entry Type", ValueEntry."Item Ledger Entry Type"::Sale);
        ValueEntry.CALCSUMS("Cost Amount (Actual)", "Cost Amount (Expected)");
        MvtVentes := ValueEntry."Cost Amount (Actual)" + ValueEntry."Cost Amount (Expected)";

        ValueEntry.SETRANGE("Item Ledger Entry Type", ValueEntry."Item Ledger Entry Type"::Purchase);
        ValueEntry.CALCSUMS("Cost Amount (Actual)");
        MvtAchats := ValueEntry."Cost Amount (Actual)";

        ValueEntry.SETFILTER("Item Ledger Entry Type", '<>%1&<>%2', ValueEntry."Item Ledger Entry Type"::Sale,
                                                                    ValueEntry."Item Ledger Entry Type"::Purchase);
        ValueEntry.CALCSUMS("Cost Amount (Actual)");
        MvtAutres := ValueEntry."Cost Amount (Actual)";


        ValeurStockCalculé := StockDerDate + MvtVentes + MvtAchats + MvtAutres;
    end;
}
