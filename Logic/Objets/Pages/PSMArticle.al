page 50250 "PSM Article"
{
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Group)
            {
                field(No; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(No2; Rec."No. 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field(Designation; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Designation 2"; Rec."Description 2")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field(Inventory; Rec.Inventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Inventory field.';
                }
                field("Date Inventaire PSM"; Rec."Date Inventaire PSM")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Inventaire PSM field.';
                }
            }
        }
    }

    actions
    {
    }
}

