page 50060 "Eskape Communication"
{
    ApplicationArea = all;
    CardPageID = "Eskape Communication";
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = List;
    SourceTable = Contact;

    layout
    {
        area(content)
        {
            field(Txt_Destinataire; strRecipient)
            {
                Caption = 'Destinataire : ';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Destinataire :  field.';
            }
            repeater(Lst_Contact)
            {
                Editable = false;
                field(Type; rec.Type)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type field.';
                }
                field("Company Name"; Rec."Company Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Company Name field.';
                }
                field(Name; Rec.Name)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field("First Name"; Rec."First Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the First Name field.';
                }
                field("Info Contact"; Rec."Info Contact")
                {
                    Caption = 'Info contact';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Info contact field.';
                }
                field("Job Title"; Rec."Job Title")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Job Title field.';
                }
                field("Fax No."; Rec."Fax No.")
                {
                    Visible = "Fax No.Visible";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Fax No. field.';
                }
                field("E-Mail"; Rec."E-Mail")
                {
                    Visible = "E-MailVisible";
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Email field.';
                }
            }
            field(Lbl_Title; '')
            {
                CaptionClass = Text19031036Lbl;
                Style = Standard;
                StyleExpr = TRUE;
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the '''' field.';
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Btn_Reset)
            {
                Caption = 'Annuler la sélection';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Annuler la sélection action.';

                trigger OnAction()
                begin
                    strRecipient := strDefaultRecipient;
                end;
            }
        }
    }

    trigger OnAfterGetCurrRecord()
    begin
        SelectRecipient();
        ;
    end;

    trigger OnAfterGetRecord()
    begin
        TypeOnFormat();
        NameOnFormat();
        FirstNameOnFormat();
        JobTitleOnFormat();
        FaxNoOnFormat();
        EMailOnFormat();
    end;

    trigger OnInit()
    begin
        "E-MailVisible" := TRUE;
        "Fax No.Visible" := TRUE;
    end;

    trigger OnOpenPage()
    begin
        // MC Le 20-04-2011 => Pour se positionner sur le contact du document.
        Rec.SETPOSITION(STRSUBSTNO(PositionTxt, ContactNo));
    end;

    var
        strRecipient: Text[255];
        strDefaultRecipient: Text[255];
        Multiple: Boolean;
        ContactNo: Code[20];
        PositionTxt: Label 'N°=CONST(%1)', Comment = '%1 = Postion';
        TypeEmphasize: Boolean;
        NameEmphasize: Boolean;
        "First NameEmphasize": Boolean;
        "Job TitleEmphasize": Boolean;
        "Fax No.Emphasize": Boolean;
        "E-MailEmphasize": Boolean;
        "Fax No.Visible": Boolean;
        "E-MailVisible": Boolean;
        Text19031036Lbl: Label 'Choix du destinataire';

    procedure InitForm(pPrinter: Option ptrEmail,ptrFax; pRecordRef: RecordRef)
    var
        recSalesHeader: Record "Sales Header";
        recPurchaseHeader: Record "Purchase Header";
        recSalesInvoiceHeader: Record "Sales Invoice Header";
        recSalesShipmentHeader: Record "Sales Shipment Header";
        recSalesCreditHeader: Record "Sales Cr.Memo Header";
        recCustomer: Record Customer;
        recReturnReceiptHeader: Record "Return Receipt Header";
        recVendor: Record Vendor;
    begin
        //Selon le type d'impression on affiche la colonne Télécopie ou la colonne Email
        CASE pPrinter OF
            pPrinter::ptrEmail:
                BEGIN
                    "Fax No.Visible" := FALSE;
                    "E-MailVisible" := TRUE;
                END;
            pPrinter::ptrFax:
                BEGIN
                    "Fax No.Visible" := TRUE;
                    "E-MailVisible" := FALSE;
                END;
        END;

        //Identifie le destinataire par défaut
        CASE pRecordRef.NUMBER OF

            //Vente
            DATABASE::"Sales Header":
                BEGIN
                    //Récupère l'enregistrement
                    recSalesHeader.RESET();
                    recSalesHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recSalesHeader.FINDFIRST() THEN BEGIN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recSalesHeader."Sell-to Contact No."
                                                                                            , recSalesHeader."Sell-to Customer No.", 0
    );

                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recSalesHeader."Sell-to Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recSalesHeader."Sell-to Customer No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recSalesHeader."Sell-to Contact No."
                                                                                            , recSalesHeader."Sell-to Customer No.", 0
    );
                                    //Affiche la liste des contacts
                                    ListContact(recSalesHeader."Sell-to Customer No.", 0);
                                END;
                        END;
                    END;
                END;

            //return receipt header
            DATABASE::"Return Receipt Header":
                BEGIN
                    //Récupère l'enregistrement
                    recReturnReceiptHeader.RESET();
                    recReturnReceiptHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recReturnReceiptHeader.FINDFIRST() THEN BEGIN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recReturnReceiptHeader."Sell-to Contact No."
                                          , recReturnReceiptHeader."Sell-to Customer No.", 0);

                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recReturnReceiptHeader."Sell-to Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recReturnReceiptHeader."Sell-to Customer No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recReturnReceiptHeader."Sell-to Contact No."
                                            , recReturnReceiptHeader."Sell-to Customer No.", 0);
                                    //Affiche la liste des contacts
                                    ListContact(recReturnReceiptHeader."Sell-to Customer No.", 0);
                                END;
                        END;
                    END;
                END;

            //Achat
            DATABASE::"Purchase Header":
                BEGIN
                    //Récupère l'enregistrement
                    recPurchaseHeader.RESET();
                    recPurchaseHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recPurchaseHeader.FINDFIRST() THEN BEGIN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recPurchaseHeader."Buy-from Contact No."
                                                                                          , recPurchaseHeader."Buy-from Vendor No.", 1
    );
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recPurchaseHeader."Buy-from Contact No.");


                                    //Affiche la liste des contacts
                                    ListContact(recPurchaseHeader."Buy-from Vendor No.", 1);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recPurchaseHeader."Buy-from Contact No."
                                                                                          , recPurchaseHeader."Buy-from Vendor No.", 1
    );
                                    //Affiche la liste des contacts
                                    ListContact(recPurchaseHeader."Buy-from Vendor No.", 1);
                                END;
                        END;
                    END;
                END;

            //Facture vente enregistrée
            DATABASE::"Sales Invoice Header":
                BEGIN
                    //Récupère l'enregistrement
                    recSalesInvoiceHeader.RESET();
                    recSalesInvoiceHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recSalesInvoiceHeader.FINDFIRST() THEN BEGIN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recSalesInvoiceHeader."Sell-to Contact No."
                                                                                     , recSalesInvoiceHeader."Sell-to Customer No.", 0
    );
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recSalesInvoiceHeader."Sell-to Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recSalesInvoiceHeader."Sell-to Customer No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recSalesInvoiceHeader."Sell-to Contact No."
                                                                                     , recSalesInvoiceHeader."Sell-to Customer No.", 0
    );
                                    //Affiche la liste des contacts
                                    ListContact(recSalesInvoiceHeader."Sell-to Customer No.", 0);
                                END;
                        END;
                    END;

                END;

            //Avoir vente enregistrée
            DATABASE::"Sales Cr.Memo Header":
                BEGIN
                    //Récupère l'enregistrement
                    recSalesCreditHeader.RESET();
                    recSalesCreditHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recSalesCreditHeader.FINDFIRST() THEN BEGIN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recSalesCreditHeader."Sell-to Contact No."
                                                                                     , recSalesCreditHeader."Sell-to Customer No.", 0
    );
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recSalesCreditHeader."Sell-to Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recSalesCreditHeader."Sell-to Customer No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recSalesCreditHeader."Sell-to Contact No."
                                                                                     , recSalesCreditHeader."Sell-to Customer No.", 0
    );
                                    //Affiche la liste des contacts
                                    ListContact(recSalesCreditHeader."Sell-to Customer No.", 0);
                                END;
                        END;
                    END;

                END;

            //Expedition vente enregistrée
            DATABASE::"Sales Shipment Header":
                BEGIN
                    //Récupère l'enregistrement
                    recSalesShipmentHeader.RESET();
                    recSalesShipmentHeader.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recSalesShipmentHeader.FINDFIRST() THEN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recSalesShipmentHeader."Sell-to Contact No."
                                                                                     , recSalesShipmentHeader."Sell-to Customer No.",
    0
    );
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recSalesShipmentHeader."Sell-to Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recSalesShipmentHeader."Sell-to Customer No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recSalesShipmentHeader."Sell-to Contact No."
                                                                                     , recSalesShipmentHeader."Sell-to Customer No.",
    0
    );
                                    //Affiche la liste des contacts
                                    ListContact(recSalesShipmentHeader."Sell-to Customer No.", 0);
                                END;
                        END;


                END;

            //Client
            DATABASE::Customer:
                BEGIN
                    //Récupère l'enregistrement
                    recCustomer.RESET();
                    recCustomer.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recCustomer.FINDFIRST() THEN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recCustomer."Primary Contact No.", recCustomer."No.", 0);
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recCustomer."Primary Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recCustomer."No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recCustomer."Primary Contact No.", recCustomer."No.", 0);
                                    //Affiche la liste des contacts
                                    ListContact(recCustomer."No.", 0);
                                END;
                        END;

                END;


            //Client
            DATABASE::Vendor:
                BEGIN
                    //Récupère l'enregistrement
                    recVendor.RESET();
                    recVendor.SETVIEW(pRecordRef.GETVIEW());

                    //Si l'enregistrement est trouvé
                    IF recVendor.FINDFIRST() THEN
                        CASE pPrinter OF
                            pPrinter::ptrEmail:
                                BEGIN
                                    //Récupère l'adresse Email
                                    strDefaultRecipient := GetEmail(recVendor."Primary Contact No.", recVendor."No.", 0);
                                    // Passe en globale le numéro du contact à afficher.
                                    SetCurrLine(recVendor."Primary Contact No.");

                                    //Affiche la liste des contacts
                                    ListContact(recVendor."No.", 0);
                                END;
                            pPrinter::ptrFax:
                                BEGIN
                                    //Récupère le numéro de Fax
                                    strDefaultRecipient := GetFaxNumber(recVendor."Primary Contact No.", recVendor."No.", 0);
                                    //Affiche la liste des contacts
                                    ListContact(recVendor."No.", 0);
                                END;
                        END;

                END;
        END;

        //Au démarrage, le destinataire est le destinataire par défaut
        strRecipient := strDefaultRecipient;
    end;

    procedure GetEmail(pContactNo: Text[30]; pNo: Text[30]; pType: Option Customer,Vendor) strReturn_Email: Text[255]
    var
        recCustomer: Record Customer;
        recVendor: Record Vendor;
    begin
        //Initialise le retour de la fonction
        strReturn_Email := '';

        //Si un contact est passé, on va chercher ses informations, sinon on interroge le client
        IF (pContactNo <> '') AND (Rec.GET(pContactNo)) THEN
            strReturn_Email := Rec."E-Mail"
        ELSE
            //Selon le type de fiche à ouvrir
            CASE pType OF
                pType::Customer:
                    BEGIN
                        //Recherche la fiche client
                        recCustomer.RESET();
                        recCustomer.SETRANGE(recCustomer."No.", pNo);

                        //Retourne l'adresse email
                        IF recCustomer.FINDFIRST() THEN strReturn_Email := recCustomer."E-Mail";
                    END;
                pType::Vendor:
                    BEGIN
                        //Recherche la fiche fournisseur
                        recVendor.RESET();
                        recVendor.SETRANGE(recVendor."No.", pNo);

                        //Retourne l'adresse email
                        IF recVendor.FINDFIRST() THEN strReturn_Email := recVendor."E-Mail";
                    END;

            END;
    end;

    procedure GetFaxNumber(pContactNo: Text[30]; pNo: Text[30]; pType: Option Customer,Vendor) strReturn_FaxNumber: Text[255]
    var
        recCustomer: Record Customer;
        recVendor: Record Vendor;
    begin
        //Initialise le retour de la fonction
        strReturn_FaxNumber := '';

        //Si un contact est passé, on va chercher ses informations, sinon on interroge le client
        IF (pContactNo <> '') AND (Rec.GET(pContactNo)) THEN
            strReturn_FaxNumber := Rec."Fax No."
        ELSE
            //Selon le type de fiche à ouvrir
            CASE pType OF
                pType::Customer:
                    BEGIN
                        //Recherche la fiche client
                        recCustomer.RESET();
                        recCustomer.SETRANGE(recCustomer."No.", pNo);

                        //Retourne le numéro de Fax
                        IF recCustomer.FINDFIRST() THEN strReturn_FaxNumber := recCustomer."Fax No.";
                    END;
                pType::Vendor:
                    BEGIN
                        //Recherche la fiche fournisseur
                        recVendor.RESET();
                        recVendor.SETRANGE(recVendor."No.", pNo);

                        //Retourne le numéro de Fax
                        IF recVendor.FINDFIRST() THEN strReturn_FaxNumber := recVendor."Fax No.";
                    END;
            END;
    end;

    procedure GetReturnValue() strReturn: Text[255]
    begin
        strReturn := strRecipient;
    end;

    procedure SelectRecipient()
    begin
        IF "E-MailVisible" THEN
            IF NOT Multiple THEN
                strRecipient := Rec."E-Mail"
            ELSE BEGIN
                IF strRecipient <> '' THEN strRecipient += ';';
                strRecipient += Rec."E-Mail";
            END;
        IF "Fax No.Visible" THEN strRecipient := Rec."Fax No.";
    end;

    procedure ListContact(pNo: Text[30]; pType: Option Customer,Vendor)
    var
        recContBusRelation: Record "Contact Business Relation";
    begin
        //Réinitialise tous les filtres sur la table contact
        Rec.RESET();

        //Filtre la table Contact en fonction du numéro de client passé
        IF Rec.GET(pNo) THEN
            Rec.SETRANGE("Company No.", Rec."Company No.")
        ELSE BEGIN
            recContBusRelation.RESET();
            recContBusRelation.SETCURRENTKEY("Link to Table", "No.");
            CASE pType OF
                pType::Customer:

                    recContBusRelation.SETRANGE("Link to Table", recContBusRelation."Link to Table"::Customer);


                pType::Vendor:

                    recContBusRelation.SETRANGE("Link to Table", recContBusRelation."Link to Table"::Vendor);

            END;
            recContBusRelation.SETRANGE("No.", pNo);
            IF recContBusRelation.FINDFIRST() THEN
                Rec.SETRANGE("Company No.", recContBusRelation."Contact No.")
            ELSE
                Rec.SETRANGE("No.", '');
        END;
    end;

    procedure SetCurrLine(pContactNo: Code[20])
    begin
        ContactNo := pContactNo;
    end;

    local procedure TypeOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure NameOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure FirstNameOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure JobTitleOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure FaxNoOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure EMailOnActivate()
    begin
        SelectRecipient();
    end;

    local procedure TypeOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            TypeEmphasize := TRUE;
    end;

    local procedure NameOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            NameEmphasize := TRUE;
    end;

    local procedure FirstNameOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            "First NameEmphasize" := TRUE;
    end;

    local procedure JobTitleOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            "Job TitleEmphasize" := TRUE;
    end;

    local procedure FaxNoOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            "Fax No.Emphasize" := TRUE;
    end;

    local procedure EMailOnFormat()
    begin
        IF Rec.Type = Rec.Type::Company THEN
            "E-MailEmphasize" := TRUE;
    end;
}

