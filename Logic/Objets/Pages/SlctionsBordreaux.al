page 50022 "Séléctions Bordreaux"
{
    PageType = Worksheet;
    SourceTable = Integer;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            field("Montant Min. de la selection"; "select min")
            {
                Caption = 'Montant Min. de la selection';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant Min. de la selection field.';
            }
            field("Montant Max. de la selection"; "select max")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the select max field.';
            }
            field("Montant Min. de l'effet"; "effet min")
            {
                Caption = 'Montant Min. de l''effet';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant Min. de l''effet field.';
            }
            field("Montant Max. de l'effet"; "effet max")
            {
                Caption = 'Montant Max. de l''effet';
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the Montant Max. de l''effet field.';
            }
            field("Echeance Maximun"; "date ech")
            {
                ApplicationArea = All;
                ToolTip = 'Specifies the value of the date ech field.';
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    begin
        "date ech" := WORKDATE();
        "select max" := 999999;
        "effet max" := 999999;
    end;

    var
        "select min": Decimal;
        "select max": Decimal;
        "effet min": Decimal;
        "effet max": Decimal;
        "date ech": Date;

    procedure Recup_variable(var "_select min": Decimal; var "_select max": Decimal; var "_effet min": Decimal; var "_effet max": Decimal; var "_date ech": Date)
    begin
        "_select min" := "select min";
        "_select max" := "select max";
        "_effet min" := "effet min";
        "_effet max" := "effet max";
        "_date ech" := "date ech";
    end;
}

