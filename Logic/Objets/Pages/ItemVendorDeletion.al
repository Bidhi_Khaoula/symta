page 50213 "Item Vendor Deletion"
{

    Caption = 'Item Vendor Without Purchase Price';
    DeleteAllowed = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Item Vendor";
    SourceTableView = SORTING("Vendor No.", "Item No.", "Variant Code")
                      ORDER(Ascending)
                      WHERE("Vendor No." = FILTER('TARCAS*|TARCNH*'),
                            "Pourchase Price Number" = CONST(0));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Item No."; Rec."Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Item No. field.';
                }
                field("Vendor No."; Rec."Vendor No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor No. field.';
                }
                field("Vendor Name"; Rec."Vendor Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Name field.';
                }
                field("Vendor Item No."; Rec."Vendor Item No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Date Mise a Jour"; Rec."Date Mise a Jour")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Mise a Jour field.';
                }
                field("Vendor Item Desciption"; Rec."Vendor Item Desciption")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Vendor Item No. field.';
                }
                field("Ref. Active"; Rec."Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active field.';
                }
                field("Pourchase Price Number"; Rec."Pourchase Price Number")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Pourchase Price Number field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Computing)
            {
                Caption = 'Computing';
                action("count")
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Count';
                    Image = AutofillQtyToHandle;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Executes the Count action.';

                    trigger OnAction();
                    var
                        lCompteur: Integer;
                    begin

                        lCompteur := DeleteLigne(TRUE);
                        IF lCompteur > 0 THEN
                            MESSAGE('Nombre de lignes à supprimer : %1', lCompteur)
                        ELSE
                            MESSAGE('Rien à supprimer')
                    end;
                }
                action(delete)
                {
                    ApplicationArea = Basic, Suite;
                    Caption = 'Computing';
                    Image = DeleteQtyToHandle;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ToolTip = 'Computing';

                    trigger OnAction();
                    var
                        TXT0001: Label '%1 lignes [Article-fournisseur] sans tarif achat associé vont être supprimées. Souhaitez vous continuer ?';
                        TXT002: Label 'Confirmer vous la suppression ?';
                        lCompteur: Integer;
                    begin

                        lCompteur := DeleteLigne(TRUE);
                        IF CONFIRM(STRSUBSTNO(TXT0001, lCompteur)) THEN
                            IF CONFIRM(TXT002) THEN
                                //Rec.DELETEALL(TRUE);
                                DeleteLigne(FALSE);

                    end;
                }
            }
        }
    }

    local procedure DeleteLigne(pTest: Boolean): Integer;
    var
        lItemVendor: Record "Item Vendor";
        lVendorNo: Code[10];
        lCompteur: Integer;
        lClef: Text[100];
    begin
        lCompteur := 0;
        lClef := '';

        Rec.SETCURRENTKEY("Vendor No.", "Item No.", "Variant Code");
        Rec.SETASCENDING("Vendor No.", TRUE);
        IF Rec.FINDSET() THEN
            REPEAT
                lVendorNo := COPYSTR(Rec."Vendor No.", 1, 6);

                // s'il existe d'autre [Article-fournisseur] avec la même référence fournisseur, c'est elles que je supprime... donc je garde la première
                lItemVendor.SETCURRENTKEY("Vendor No.", "Item No.", "Variant Code");
                lItemVendor.SETASCENDING("Vendor No.", TRUE);
                lItemVendor.SETFILTER("Vendor No.", '%1&>%2', lVendorNo + '-*', Rec."Vendor No.");
                lItemVendor.SETRANGE("Item No.", Rec."Item No.");
                lItemVendor.SETRANGE("Variant Code", Rec."Variant Code");
                lItemVendor.SETRANGE("Vendor Item No.", Rec."Vendor Item No.");
                lItemVendor.SETRANGE("Pourchase Price Number", 0);
                //MESSAGE(lItemVendor.GETFILTERS());

                IF lItemVendor.FINDSET() THEN BEGIN
                    IF (pTest) THEN BEGIN
                        IF (lClef <> lVendorNo + '-' + Rec."Item No." + '-' + Rec."Vendor Item No." + '-' + Rec."Variant Code") THEN
                            lCompteur += lItemVendor.COUNT();
                        lClef := lVendorNo + '-' + Rec."Item No." + '-' + Rec."Vendor Item No." + '-' + Rec."Variant Code";
                    END
                    ELSE
                        lItemVendor.DELETEALL(TRUE);

                END;

            UNTIL Rec.NEXT() = 0;

        EXIT(lCompteur);
    end;
}

