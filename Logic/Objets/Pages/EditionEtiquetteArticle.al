page 50018 "Edition Etiquette Article"
{
    CardPageID = "Edition Etiquette Article";
    DeleteAllowed = false;
    InsertAllowed = false;
    PageType = Card;
    SourceTable = Item;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group("Général")
            {
                Caption = 'Général';
                field("No."; Rec."No.")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Description; Rec.Description)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Description 2"; Rec."Description 2")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description 2 field.';
                }
                field(TypeEtiquette; TypeEtiquette)
                {
                    Caption = 'Type Etiquette';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Type Etiquette field.';
                }
                field(Qte; Qte)
                {
                    Caption = 'Quantité';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Quantité field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action(Edition)
            {
                Caption = 'Edition';
                Promoted = true;
                PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Edition action.';

                trigger OnAction()
                var
                    CUEtiquette: Codeunit "Etiquettes Articles";
                begin
                    CUEtiquette.EditItemLabel(Rec."No.", FORMAT(TypeEtiquette), Qte);
                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        Qte := 1;
    end;

    var
        TypeEtiquette: Option Petit,Grand;
        Qte: Decimal;
}

