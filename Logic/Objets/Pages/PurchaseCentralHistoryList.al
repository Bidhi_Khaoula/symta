page 50300 "Purchase Central History List"
{
    Caption = 'Historique des centrales d''achat';
    PageType = List;
    SourceTable = "Purchase Central History";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Customer No."; Rec."Customer No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Customer Name"; Rec."Customer Name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Name field.';
                }
                field("Active Central"; Rec."Active Central")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Centrale active field.';
                }
                field("Active Central name"; Rec."Active Central name")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Nom centrale active field.';
                }
                field("Groupement payeur"; Rec."Groupement payeur")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Groupement payeur field.';
                }
                field("Starting Date"; Rec."Starting Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Starting Date field.';
                }
                field("Ending Date"; Rec."Ending Date")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ending Date field.';
                }
                field("Libre 3"; Rec."Libre 3")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code adhérent field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage();
    begin
        // CFR le 10/03/2021 - SFD20210201 historique Centrale Active
        Rec.FILTERGROUP(4);
        Rec.SETRANGE("Starting Date");
        Rec.SETRANGE("Ending Date");
        Rec.FILTERGROUP(0);
        Rec.SETRANGE("Starting Date");
        Rec.SETRANGE("Ending Date");
    end;
}

