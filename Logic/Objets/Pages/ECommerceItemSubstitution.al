page 50234 "ECommerce Item Substitution"
{
    PageType = List;
    SourceTable = "Item Substitution";
    SourceTableView = SORTING(Type, "No.", "Variant Code", "Substitute Type", "Substitute No.", "Substitute Variant Code")
                      WHERE("Affichage WEB" = CONST(True));
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(Article; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field(Article_Substitut; Rec."Substitute No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Substitute No. field.';
                }
                field(Designation; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field(Stock; Rec.Inventory)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Inventory field.';
                }
                field(Reference_Active; Rec_Item."No. 2")
                {
                    Caption = 'Référence active';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Référence active field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        // Récupère l'article.
        Rec_Item.GET(Rec."No.");
    end;

    var
        Rec_Item: Record Item;
}

