page 50043 "Détail Ligne Facture"
{
    CardPageID = "Détail Ligne Facture";
    InsertAllowed = false;
    PageType = Card;
    SourceTable = "Import facture tempo. fourn.";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            group(Générale)
            {
                field("Code fournisseur"; Rec."Code fournisseur")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code fournisseur field.';
                }
                field("No Facture"; Rec."No Facture")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No Facture field.';
                }
                field("Date Facture"; Rec."Date Facture")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Date Facture field.';
                }
                field("Code article SYMTA"; Rec."Code article SYMTA")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article SYMTA field.';
                }
                field("No. 2"; Item."No. 2")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. 2 field.';
                }
                field("Code article Fournisseur"; Rec."Code article Fournisseur")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Code article Fournisseur field.';
                }
                field(Désignation; Rec.Désignation)
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Désignation field.';
                }
                field("erreur integration"; Rec."erreur integration")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the erreur integration field.';
                }
                field("No commande affecté"; Rec."No commande affecté")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande affecté field.';
                }
                field("No commande"; Rec."No commande")
                {
                    Editable = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No commande field.';
                }
            }
            group("Groupe 2")
            {
                ShowCaption = false;
                group("Groupe 3")
                {
                    ShowCaption = false;
                    fixed(Fixed)
                    {
                        group("Valeur Fournisseur")
                        {
                            Caption = 'Valeur Fournisseur';
                            field(Quantité; Rec.Quantité)
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Quantité field.';
                            }
                            field("Pu brut"; Rec."Pu brut")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Pu brut field.';
                            }
                            field("Remise 1"; Rec."Remise 1")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Remise 1 field.';
                            }
                            field("Remise 2"; Rec."Remise 2")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Remise 2 field.';
                            }
                            field("Pu net"; Rec."Pu net")
                            {
                                DecimalPlaces = 0 : 5;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Pu net field.';
                            }
                            field("Total Net"; Rec."Total Net")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Total Net field.';
                            }
                        }
                        group("Valeur commande")
                        {
                            Caption = 'Valeur commande';
                            field("Quantité restante commande"; Rec."Quantité restante commande")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Quantité restante commande field.';
                            }
                            field("Pu brut commande"; Rec."Pu brut commande")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Pu brut commande field.';
                            }
                            field("Remise 1 commande"; Rec."Remise 1 commande")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Remise 1 commande field.';
                            }
                            field("Remise 2 commande"; Rec."Remise 2 commande")
                            {
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Remise 2 commande field.';
                            }
                            field("Pu net commande"; Rec."Pu net commande")
                            {
                                DecimalPlaces = 0 : 5;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Pu net commande field.';
                            }
                        }
                        group("Nouvelle valeur")
                        {
                            Caption = 'Nouvelle valeur';
                            field("Nouvelle quantité"; Rec."Nouvelle quantité")
                            {
                                Editable = "Nouvelle quantitéEditable";
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Nouvelle quantité field.';
                            }
                            field("Nouveau Pu Brut"; Rec."Nouveau Pu Brut")
                            {
                                Editable = "Nouveau Pu BrutEditable";
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Nouveau Pu Brut field.';
                            }
                            field("Nouvelle Remise 1"; Rec."Nouvelle Remise 1")
                            {
                                Editable = "Nouvelle Remise 1Editable";
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Nouvelle Remise 1 field.';
                            }
                            field("Nouvelle Remise 2"; Rec."Nouvelle Remise 2")
                            {
                                Editable = "Nouvelle Remise 2Editable";
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Nouvelle Remise 2 field.';
                            }
                            field("Nouveau cout net"; Rec."Nouveau cout net")
                            {
                                DecimalPlaces = 0 : 5;
                                Editable = false;
                                ApplicationArea = All;
                                ToolTip = 'Specifies the value of the Nouveau cout net field.';
                            }
                        }
                    }
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetRecord()
    begin
        "Nouvelle quantitéEditable" := Rec."Erreur Quantité";
        "Nouveau Pu BrutEditable" := Rec."Erreur côut";
        "Nouvelle Remise 1Editable" := Rec."Erreur côut";
        "Nouvelle Remise 2Editable" := Rec."Erreur côut";
    end;

    trigger OnInit()
    begin
        "Nouvelle Remise 2Editable" := TRUE;
        "Nouvelle Remise 1Editable" := TRUE;
        "Nouveau Pu BrutEditable" := TRUE;
        "Nouvelle quantitéEditable" := TRUE;
    end;

    trigger OnOpenPage()
    begin
        IF Item.GET(Rec."Code article SYMTA") THEN;
    end;

    var
        Confirm001: Label 'Etes vous sur de valider l''écart ? Ensuite vous ne pourrez plus modifier cet écart !';
        Item: Record Item;
        "Nouvelle quantitéEditable": Boolean;
        "Nouveau Pu BrutEditable": Boolean;
        "Nouvelle Remise 1Editable": Boolean;
        "Nouvelle Remise 2Editable": Boolean;
        Text19011028: Label 'Valeur Fournisseur';
        Text19010360: Label 'Valeur commande';
        Text19047090: Label 'Nouvelle valeur';
}

