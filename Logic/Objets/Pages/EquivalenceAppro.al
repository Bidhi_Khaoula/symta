page 50028 "Equivalence Appro"
{
    CardPageID = "Equivalence Appro";
    Editable = false;
    PageType = ListPart;
    SourceTable = "Item Substitution";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Lines)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the No. field.';
                }
                field("Variant Code"; Rec."Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Variant Code field.';
                }
                field("Substitute No."; Rec."Substitute No.")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Substitute No. field.';
                }
                field("Substitute Ref. Active"; Rec."Substitute Ref. Active")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Ref. Active substitut field.';
                }
                field("Substitute Variant Code"; Rec."Substitute Variant Code")
                {
                    Visible = false;
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Substitute Variant Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    Style = Attention;
                    StyleExpr = TRUE;
                    ToolTip = 'Specifies the value of the Description field.';
                }
                field("Substitute Type"; Rec."Substitute Type")
                {
                    ApplicationArea = All;
                    Visible = FALSE;
                    ToolTip = 'Specifies the value of the Substitute Type field.';
                }
                field("Sub. Item No."; Rec."Sub. Item No.")
                {
                    ApplicationArea = All;
                    Visible = FALSE;
                    ToolTip = 'Specifies the value of the Sub. Item No. field.';
                }
                field(GetReserve_; GetReserve())
                {
                    Caption = 'Reserve';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Reserve field.';
                }
                field(GetAttendu_; GetAttendu())
                {
                    Caption = 'Attendu';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Attendu field.';
                }
                field(GetStock_; GetStock())
                {
                    Caption = 'Stock';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stock field.';
                }
                field(GetIsStocked_; GetIsStocked())
                {
                    Caption = 'Stocké';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Stocké field.';
                }
                field(GetBestPrice_; GetBestPrice())
                {
                    Caption = 'Meilleur Prix (Niveau 3)';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Meilleur Prix (Niveau 3) field.';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Conso Achat")
            {
                Caption = 'Conso Achat';
                // Promoted = true;
                // PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Conso Achat action.';

                trigger OnAction()
                var
                    FrmConsoArticle: Page "Sorties Mensuelles";
                begin
                    IF Item.GET(Rec."Substitute No.") THEN;

                    CLEAR(FrmConsoArticle);
                    FrmConsoArticle.SETTABLEVIEW(Item);
                    FrmConsoArticle.SETRECORD(Item);
                    FrmConsoArticle.InitDispo(GetStock() - GetReserve() + GetAttendu());
                    FrmConsoArticle.RUNMODAL();
                end;
            }
            action("Multi Consultation")
            {
                Caption = 'Multi Consultation';
                // Promoted = true;
                // PromotedCategory = Process;
                ApplicationArea = All;
                ToolTip = 'Executes the Multi Consultation action.';

                trigger OnAction()
                var
                    FrmQuid: Page "Multi -consultation";
                begin
                    IF Item.GET(Rec."Substitute No.") THEN;

                    FrmQuid.InitArticle(Item."No. 2");
                    FrmQuid.RUNMODAL();
                end;
            }
        }

    }

    trigger OnAfterGetRecord()
    begin
        NoOnFormat(FORMAT(Rec."No."));
        SubstituteNoOnFormat(FORMAT(Rec."Substitute No."));
    end;

    var
        Item: Record Item;
        CUMultiRef: Codeunit "Gestion Multi-référence";

    procedure GetReserve() dec_rtn: Decimal
    var
        attendu: Decimal;
        reserve: Decimal;
    begin
        dec_rtn := 0;

        IF Item.GET(Rec."Substitute No.") THEN BEGIN
            Item.CalcAttenduReserve(reserve, attendu);
            dec_rtn := reserve;
        END;
    end;

    procedure GetAttendu() dec_rtn: Decimal
    var
        Reserve: Decimal;
        Attendu: Decimal;
    begin
        dec_rtn := 0;

        IF Item.GET(Rec."Substitute No.") THEN BEGIN
            Item.CalcAttenduReserve(Reserve, Attendu);
            dec_rtn := Attendu;
        END;
    end;

    procedure GetStock() dec_rtn: Decimal
    begin
        dec_rtn := 0;
        IF Item.GET(Rec."Substitute No.") THEN BEGIN
            Item.CALCFIELDS(Inventory);
            dec_rtn := Item.Inventory;
        END;
    end;

    procedure GetIsStocked() bln_rtn: Boolean
    begin
        bln_rtn := FALSE;

        IF Item.GET(Rec."Substitute No.") THEN
            bln_rtn := Item.Stocké;
    end;

    local procedure NoOnFormat(Text: Text[1024])
    begin
        // MC Le 10-11-2011 => SYMTA -> Gestion MultiReference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;

    local procedure SubstituteNoOnFormat(Text: Text[1024])
    begin
        // MC Le 10-11-2011 => SYMTA -> Gestion MultiReference
        Text := CUMultiRef.RechercheRefActive(Text);
    end;

    local procedure GetBestPrice() Dec_return: Decimal
    var
        SingleInstance: Codeunit SingleInstance;
    begin
        SingleInstance.GetBestPurchPrice(Rec."Substitute No.", 99999, 3, Dec_return, '');
        EXIT(Dec_return);
    end;

    // CFR le 12/04/2022 - R‚gie : Mise en forme de la page des appros (masque des champs par d‚faut) + Description en rouge
}

