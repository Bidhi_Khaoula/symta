page 50260 "Translation List"
{
    Caption = 'Traductions List';
    PageType = List;
    ShowFilter = false;
    SourceTable = "Translation SYMTA";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Table No"; Rec."Table No")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Table field.';
                }
                field("Champ table"; Rec."Champ table")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the N° Field field.';
                }
                field(Nom; Rec.Nom)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Field Name field.';
                }
                field(Caption; Caption)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Field Label field.';
                }
                field("Language Code"; Rec."Language Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Language Code field.';
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Traduction field.';
                }
                field(Record_ID; FORMAT(Rec.RECORDID))
                {
                    Caption = 'Record_ID';
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Record_ID field.';
                }
            }
        }
    }

    actions
    {
    }

    trigger OnOpenPage()
    begin
        Rec.SETRANGE("Language Code");
        //Enregistrement := FORMAT(RECORDID);
        //MESSAGE(Enregistrement);
    end;

    var
    /*   Item: Char;
      Enregistrement: Text; */
}

