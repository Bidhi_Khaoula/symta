page 50222 "Comment Setup List"
{
    Caption = 'Commentaire mouvement PSM';
    PageType = List;
    SourceTable = "Comment Setup";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Comment Code"; Rec."Comment Code")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment Code field.';
                }
                field("Comment Text"; Rec."Comment Text")
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Comment Text field.';
                }
            }
        }
    }

    actions
    {
    }
}

