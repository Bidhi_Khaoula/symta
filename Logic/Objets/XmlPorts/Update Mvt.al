xmlport 50062 "Update Mvt"
{
    Caption = 'Update Mvt';
    Permissions = TableData "Return Shipment Line" = rm;
    Direction = Import;
    TextEncoding = UTF8;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Integer; Integer)
            {
                //TODOSourceTableView = SORTING(Field1);
                AutoSave = false;
                textelement(date_compta)
                {

                }
                textelement(type_ecr)
                {

                }
                textelement(type_doc)
                {

                }
                textelement(no_doc)
                {

                }
                textelement(no_art)
                {

                }
                textelement(ref_act)
                {

                }
                textelement(qte)
                {

                }
                textelement(qte_fac)
                {

                }
                textelement(reg_rec_mag)
                {

                }
                textelement(dem_rec_mag)
                {

                }
                textelement(reg_ret_ach)
                {

                }
                textelement(dem_ret_ach)
                {

                }
                trigger OnAfterInsertRecord()
                begin
                    // donn‚es du fichier
                    _date_compta := date_compta;
                    _type_ecr := type_ecr;
                    _type_doc := type_doc;
                    _no_doc := no_doc;
                    _no_art := no_art;
                    _ref_act := ref_act;
                    _qte := qte;
                    _qte_fac := qte_fac;
                    _reg_rec_mag := reg_rec_mag;
                    _dem_rec_mag := dem_rec_mag;
                    _reg_ret_ach := reg_ret_ach;
                    _dem_ret_ach := dem_ret_ach;

                    CASE _type_ecr OF
                        'Achat':

                            //ERROR('=%1 %2',_type_doc, 'R‚ception achat' = _type_doc);
                            CASE _type_doc OF
                                // ACHAT/BR/CA
                                'Réception achat':
                                    BEGIN
                                        CLEAR(PostedWhseReceiptLine);
                                        WITH PostedWhseReceiptLine DO BEGIN
                                            SETRANGE("Item No.", _no_art);
                                            //SETRANGE("Source Line No.", "Document Line No.");
                                            SETRANGE("Posted Source Document", "Posted Source Document"::"Posted Receipt");
                                            SETRANGE("Posted Source No.", _no_doc);
                                            IF FINDFIRST() THEN
                                                //"Receipt Adjustment" := "Receipt Adjustment"::" ";
                                                CASE _dem_rec_mag OF
                                                    'Soldé':

                                                        IF "Receipt Request" = "Receipt Request"::" " THEN BEGIN
                                                            "Receipt Request" := "Receipt Request"::Ended;
                                                            MODIFY();
                                                        END;
                                                //ELSE
                                                //  "Receipt Request" := "Receipt Request"::" ";
                                                END;
                                        END;
                                    END;
                                // ACHAT/RAE/RA
                                'Expédition retour achat':
                                    BEGIN
                                        CLEAR(ReturnShipmentLine); // RAE
                                        WITH ReturnShipmentLine DO BEGIN
                                            SETRANGE("No.", _no_art);
                                            //SETRANGE("Line No.", "Document Line No.");
                                            SETRANGE("Document No.", _no_doc);
                                            IF FINDFIRST() THEN BEGIN
                                                CLEAR(PurchaseLine);// RA
                                                PurchaseLine.SETRANGE("No.", "No.");
                                                PurchaseLine.SETRANGE("Line No.", "Return Order Line No.");
                                                PurchaseLine.SETRANGE("Document No.", "Return Order No.");
                                                PurchaseLine.SETRANGE("Document Type", PurchaseLine."Document Type"::"Return Order");
                                                IF PurchaseLine.FINDFIRST() THEN
                                                    //PurchaseLine."Regularisation retour" := PurchaseLine."Regularisation retour"::" ";
                                                    CASE _dem_ret_ach OF
                                                        'Soldé':
                                                            IF PurchaseLine."Demande retour" = PurchaseLine."Demande retour"::" " THEN BEGIN
                                                                PurchaseLine."Demande retour" := PurchaseLine."Demande retour"::Soldé;
                                                                PurchaseLine.MODIFY();
                                                            END;
                                                    //ELSE
                                                    //  PurchaseLine."Demande retour" := PurchaseLine."Demande retour"::" ";
                                                    END;
                                            END;
                                        END;
                                    END;
                            END;
                    END;


                    /*
                      // ACHAT/BR/CA
                      RESET();
SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Purchase);
SETFILTER("Document Type", '=%1', "Document Type"::"Purchase Receipt");
lPostedWhseReceiptLine.RESET();
lPostedWhseReceiptLine.SETRANGE("Item No.", "Item No.");
lPostedWhseReceiptLine.SETRANGE("Source Line No.", "Document Line No.");
lPostedWhseReceiptLine.SETRANGE("Posted Source Document", lPostedWhseReceiptLine."Posted Source Document"::"Posted Receipt");
lPostedWhseReceiptLine.SETRANGE("Posted Source No.", "Document No.");
"Warehouse Document No." := lPostedWhseReceiptLine."Whse. Receipt No.";
"Source Document No." := lPostedWhseReceiptLine."Source No.";

// ACHAT/RAE/RA
RESET;
SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Purchase);
SETFILTER("Document Type", '=%1', "Document Type"::"Purchase Return Shipment");
lReturnShipmentLine.RESET();
lReturnShipmentLine.SETRANGE("Document No.", "Document No.");
lReturnShipmentLine.SETRANGE("No.", "Item No.");
lReturnShipmentLine.SETRANGE("Line No.", "Document Line No.");
"Warehouse Document No." := '';
"Source Document No." := lReturnShipmentLine."Return Order No.";

// VENTE/BL/CV
RESET;
SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Sale);
SETFILTER("Document Type", '=%1', "Document Type"::"Sales Shipment");
lPostedWhseShipmentLine.RESET();
lPostedWhseShipmentLine.SETRANGE("Item No.", "Item No.");
lPostedWhseShipmentLine.SETRANGE("Source Line No.", "Document Line No.");
lPostedWhseShipmentLine.SETRANGE("Posted Source Document", lPostedWhseShipmentLine."Posted Source Document"::"Posted Shipment");
lPostedWhseShipmentLine.SETRANGE("Posted Source No.", "Document No.");
"Warehouse Document No." := lPostedWhseShipmentLine."Whse. Shipment No.";
"Source Document No." := lPostedWhseShipmentLine."Source No.";
// VENTE/RVE/RV
RESET;
SETRANGE("Entry Type", lItemLedgerEntry."Entry Type"::Sale);
SETFILTER("Document Type", '=%1', "Document Type"::"Sales Return Receipt");
lReturnReceiptLine.RESET();
lReturnReceiptLine.SETRANGE("Document No.", "Document No.");
lReturnReceiptLine.SETRANGE("No.", "Item No.");
lReturnReceiptLine.SETRANGE("Line No.", "Document Line No.");
"Warehouse Document No." := '';
"Source Document No." := lReturnReceiptLine."Return Order No.";
                    */
                end;
            }
        }
    }


    trigger OnPostXmlPort()
    begin
        MESSAGE('Terminé');
    end;

    var
        // ItemLedgerEntry: Record "Item Ledger Entry";
        PostedWhseReceiptLine: Record "Posted Whse. Receipt Line";
        // PostedWhseShipmentLine: Record "Posted Whse. Shipment Line";
        ReturnShipmentLine: Record "Return Shipment Line";
        // ReturnReceiptLine: Record "Return Receipt Line";
        PurchaseLine: Record "Purchase Line";
        _date_compta: Text;
        _type_ecr: Text;
        _type_doc: Text;
        _no_doc: Text;
        _no_art: Text;
        _ref_act: Text;
        _qte: Text;
        _qte_fac: Text;
        _reg_rec_mag: Text;
        _dem_rec_mag: Text;
        _reg_ret_ach: Text;
        _dem_ret_ach: Text;
    // qty: Decimal;
}