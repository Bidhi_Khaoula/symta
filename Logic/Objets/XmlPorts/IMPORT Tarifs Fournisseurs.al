xmlport 50015 "IMPORT Tarifs Fournisseurs"
{
    Caption = 'IMPORT Tarifs Fournisseurs';
    Direction = Import;
    PreserveWhiteSpace = true;
    TextEncoding = WINDOWS;
    Format = VariableText;
    FieldSeparator = '~';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;

                textelement(tmp)
                {

                }
                trigger OnAfterInsertRecord()
                begin
                    _tmp := tmp;
                    "Temp Texte 250".Tmp := _tmp;
                    // MCO Le 08-02-2017 => R‚gie
                    "Temp Texte 250".Ref_Fourn := COPYSTR("Temp Texte 250".Tmp, 21, 20);
                    "Temp Texte 250".Ref_Fourn := CONVERTSTR("Temp Texte 250".Ref_Fourn, '.', ' ');
                    // FIN MCO Le 08-02-2017 => R‚gie
                    IF "Temp Texte 250".INSERT() THEN;
                end;
            }
        }
    }


    var
        "Temp Texte 250": Record "Temp Texte 250";
        _tmp: Text[250];

    // MCO Le 08-02-2017 => R‚gie

}