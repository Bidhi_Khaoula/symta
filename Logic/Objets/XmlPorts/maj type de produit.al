xmlport 50041 "maj type de produit"
{
    Caption = 'maj type de produit';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(ProductType; Item."Product Type")
                {

                }
            }
        }
    }

}