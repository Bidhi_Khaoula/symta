xmlport 50069 "maj art. Substitution"
{
    Caption = 'maj art. Substitution';
    TransactionType = UpdateNoLocks;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemSubstitution; "Item Substitution")
            {
                AutoUpdate = true;
                AutoReplace = false;
                fieldelement(No; ItemSubstitution."No.")
                {

                }
                fieldelement(CodeVariante; ItemSubstitution."Variant Code")
                {

                }
                fieldelement(TypeSubstitution; ItemSubstitution."Substitute Type")
                {

                }
                fieldelement(NoSustitut; ItemSubstitution."Substitute No.")
                {

                }
                fieldelement(RefActiveSubstitut; ItemSubstitution."Ref. Active")
                {

                }
                fieldelement(Description; ItemSubstitution.Description)
                {

                }
                fieldelement(Interchangeable; ItemSubstitution.Interchangeable)
                {

                }
                fieldelement(NodeNConditioname3; ItemSubstitution.Condition)
                {

                }
                fieldelement(AffichageWEB; ItemSubstitution."Affichage WEB")
                {

                }
                fieldelement(CodeUtilisateurCreation; ItemSubstitution."Create User ID")
                {

                }
                fieldelement(DateCreation; ItemSubstitution."Create Date")
                {

                }
            }
        }
    }
}