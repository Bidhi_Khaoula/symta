xmlport 50064 "Import article corrige"
{
    Caption = 'Import article corrige';
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(root)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                textelement(_BuilderName)
                {

                }
                textelement(_BuilderItemNo)
                {

                }
                textelement(_VendorItemNo)
                {

                }
                textelement(_Designation)
                {

                }
                trigger OnBeforeInsertRecord()
                begin
                    LItemTot += 1;

                    // Recherche de l'existence du fournisseur
                    RecVendor.SETRANGE("No.", _BuilderName); // MCO Le 24-10-2018 => ON prend le Nø et plus le nom.

                    IF RecVendor.FINDFIRST() THEN BEGIN
                        // ON recherche l'article
                        RecItem.SETRANGE("No. 2", _BuilderItemNo);
                        IF RecItem.FINDFIRST() THEN BEGIN
                            RecItem2 := RecItem;
                            RecItem2.TRANSFERFIELDS(RecItemModel, FALSE);

                            RecItem2."No." := RecItem."No.";
                            RecItem2."No. 2" := _BuilderItemNo;
                            // MCO Le 24-10-2018 => FE20181016 -> SYMTA
                            IF _Designation <> '' THEN
                                RecItem2.VALIDATE(Description, _Designation);
                            RecItem2.MODIFY();
                            // FIN MCO Le 24-10-2018 => FE20181016

                            // Cr‚ation de l'article fournisseur
                            IF RecItemVendorModel.GET(RecVendor."No.", RecItemModel."No.", '') THEN BEGIN
                                RecitemVendor.Init();
                                RecitemVendor.TRANSFERFIELDS(RecItemVendorModel);
                                RecitemVendor."Item No." := RecItem2."No.";
                                RecitemVendor.VALIDATE("Ref. Active", _BuilderItemNo);
                                RecitemVendor.VALIDATE("Vendor Item No.", _VendorItemNo);
                                IF NOT RecitemVendor.INSERT(TRUE) THEN
                                    IF RecitemVendor.MODIFY(TRUE) THEN;
                                LItemOK += 1;
                            END;
                        END;
                    END;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(content)
            {
                group(Model)
                {
                    Caption = 'Model';
                    field("Item model"; ItemModel)
                    {
                        Caption = 'Item model';
                        TableRelation = Item;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Item model field.';
                    }
                }
            }
        }

        trigger OnQueryClosePage(CloseAction: Action): Boolean
        begin
            IF CloseAction = ACTION::OK THEN
                IF NOT RecItemModel.GET(ItemModel) THEN
                    ERROR(Err001Err);
        end;
    }
    trigger OnInitXmlPort()
    begin
        LItemOK := 0;
        LItemTot := 0;
    end;

    trigger OnPostXmlPort()
    begin
        MESSAGE(Txt001Lbl, LItemOK, LItemTot - LItemOK);
    end;

    var
        RecItemModel: Record Item;
        RecItemVendorModel: Record "Item Vendor";
        RecItem: Record Item;
        RecitemVendor: Record "Item Vendor";
        RecVendor: Record Vendor;
        RecItem2: Record Item;
        ItemModel: Code[20];
        LItemOK: Integer;
        LItemTot: Integer;
        Txt001Lbl: Label 'Import article terminé, %1 articles créés, %2 articles en erreur', Comment = '%1 = Nombre des Articles créés ; %2=Nombre des articles en erreur';
        Err001Err: Label 'Le code article modèle est obligatoire';

    // FBO le 18-04-2017 => FE20170310

}