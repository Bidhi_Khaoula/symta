xmlport 50079 "MAJ articles - info_achat"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                XmlName = 'Item';
                fieldelement("No."; Item."No.")
                {
                }
                fieldelement(Info_achat; Item."Description 2")
                {
                }
            }
        }
    }

}

