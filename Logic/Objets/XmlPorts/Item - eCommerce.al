// xmlport 50000 "Item - eCommerce"
// {
//     Caption = 'Item - eCommerce';
//     Encoding = UTF8;

//     schema
//     {
//         textelement(Articles)
//         {
//             tableelement(Article; Item)
//             {
//                 SourceTableView = SORTING(Field60200) WHERE(Field60200 = FILTER(2 | 3));
//                 fieldelement(Code; Article."No.")
//                 {

//                 }
//                 fieldelement(Designation; Article.Description)
//                 {

//                 }
//                 fieldelement(Designation2; Article."Description 2")
//                 {

//                 }
//                 fieldelement(PoidsUnitaire; Article."Net Weight")
//                 {

//                 }
//                 fieldelement(VolumeUnitaire; Article."Unit Volume")
//                 {

//                 }
//                 textelement(PoidsMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         PoidsMaster := FORMAT(UniteBaseMAS.Weight);
//                     end;
//                 }
//                 textelement(LargeurMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         LargeurMaster := FORMAT(UniteBaseMAS.Width);
//                     end;
//                 }
//                 textelement(LongueurMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         LongueurMaster := FORMAT(UniteBaseMAS.Length);
//                     end;
//                 }
//                 textelement(HauteurMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         HauteurMaster := FORMAT(UniteBaseMAS.Height);
//                     end;
//                 }
//                 textelement(QuantiteMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         QuantiteMaster := FORMAT(UniteBaseMAS."Qty. per Unit of Measure");
//                     end;
//                 }
//                 textelement(VolumeUnitaireMaster)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         VolumeUnitaireMaster := FORMAT(UniteBaseMAS.Cubage);
//                     end;
//                 }
//                 textelement(NbPiecesSurPalette)
//                 {
//                     trigger OnBeforePassVariable()
//                     begin
//                         NbPiecesSurPalette := FORMAT(UniteBasePAL."Qty. per Unit of Measure");
//                     end;
//                 }
//                 fieldelement(CodeFamille; Article."Item Category Code")
//                 {

//                 }
//                 //TODOfieldelement(CodeSousFamille; Article."Product Group Code") { }
//                 fieldelement(CodeMarketing; Article."Code Marketing")
//                 {

//                 }
//                 fieldelement(LibelleMarketingFrancais; Article."Libelle Marketing")
//                 {

//                 }
//                 fieldelement(CodeMarque; Article."Manufacturer Code")
//                 {

//                 }
//                 textelement(Criteres)
//                 {
//                     tableelement(Critere; "Vendor Price Buffer")
//                     {
//                         LinkFields = Field1 = FIELD(Field1);
//                         LinkTable = article;
//                         fieldelement(CodeCritere; Critere."Journal Batch Name")
//                         {

//                         }
//                         textelement(LibelleCritere)
//                         {
//                             trigger OnBeforePassVariable()
//                             begin
//                                 LibelleCritere := Critere.Nom;
//                             end;
//                         }
//                         fieldelement(Priorite; Critere."Line No.")
//                         {

//                         }
//                         textelement(Valeur)
//                         {
//                             trigger OnBeforePassVariable()
//                             begin
//                                 Valeur := Table50020.GetTypeValue;
//                             end;
//                         }
//                         textelement(Unite)
//                         {
//                             trigger OnBeforePassVariable()
//                             begin
//                                 Unite := Critere.Unité
//                             end;
//                         }
//                         trigger OnAfterGetRecord()
//                         begin
//                             IF Critere.GET(Table50020."Code Critere") THEN;
//                         end;
//                     }
//                 }
//                 trigger OnAfterGetRecord()
//                 begin
//                     IF UniteBaseMAS.GET(Article."No.", 'MAS') THEN;
//                     IF UniteBasePAL.GET(Article."No.", 'PAL') THEN;

//                 end;

//             }

//         }


//     }
//     var
//         UniteBaseMAS: Record "Item Unit of Measure";
//         UniteBasePAL: Record "Item Unit of Measure";
//         Critere: Record "Trafic Transporteur";
// }