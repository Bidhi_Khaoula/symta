xmlport 50033 "maj stock maxi publiable"
{
    Caption = 'maj stock maxi publiable';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Stock_Maxi_Publiable; Item."Stock Maxi Publiable")
                {

                }
            }
        }
    }
}