xmlport 50066 "maj stock mort"
{
    Caption = 'maj stock mort';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                RequestFilterFields = "No.";
                SourceTableView = SORTING("No.");
                UseTemporary = false;
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Stock_Mort; Item."Stock Mort")
                {

                }
                fieldelement(date_maj_stock_mort; Item."date maj stock mort")
                {

                }
                fieldelement(user_maj_stock_mort; Item."user maj stock mort")
                {

                }
            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}