xmlport 50034 "maj code appro"
{
    Caption = 'maj code appro';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(CodeAppro; Item."Code Appro")
                {

                }
            }
        }
    }

    trigger OnInitXmlPort()
    begin
        MESSAGE('Structure fichier (Pas de Titre) : Ref SP;Code appro');
    end;
}