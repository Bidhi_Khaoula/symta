xmlport 50074 "MAJ articles - Transporteu DPD"
{
    Caption = 'MAJ articles - Transporteu DPD';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Publiable_Web; Item."Recommended Shipping Agent")
                {

                }
            }
        }
    }
}