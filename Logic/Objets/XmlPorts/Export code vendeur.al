xmlport 50053 "Export code vendeur"
{
    Caption = 'Export code vendeur';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Customer; Customer)
            {
                AutoUpdate = true;
                AutoReplace = false;
                fieldelement(numero_client; Customer."No.")
                {

                }
                fieldelement(code_vendeur; Customer."Salesperson Code")
                {

                }
            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;

}