xmlport 50073 "MAJ - fournisseur Preferentiel"
{
    Caption = 'MAJ - fournisseur Preferentiel';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Fournisseur_preferentiel; Item."Vendor No.")
                {

                }
            }
        }
    }
}