xmlport 50054 "Import article"
{
    Caption = 'Import article';
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(root)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                textelement(_BuilderName)
                {

                }
                textelement(_BuilderItemNo)
                {

                }
                textelement(_VendorItemNo)
                {

                }
                textelement(_Designation)
                {

                }
                trigger OnBeforeInsertRecord()
                begin
                    LItemTot += 1;

                    // Recherche de l'existence du fournisseur
                    RecVendor.SETRANGE("No.", _BuilderName); // MCO Le 24-10-2018 => ON prend le Nø et plus le nom.

                    IF RecVendor.FINDFIRST() THEN BEGIN
                        // Cr‚ation de l'article
                        RecItem.init();
                        RecItem.TRANSFERFIELDS(RecItemModel);
                        RecItem."No." := '';
                        RecItem."No. 2" := _BuilderItemNo;

                        // MCO Le 24-10-2018 => FE20181016 -> SYMTA
                        IF _Designation <> '' THEN
                            RecItem.VALIDATE(Description, _Designation);
                        // FIN MCO Le 24-10-2018 => FE20181016

                        RecItem.INSERT(TRUE);
                        RecItem.VALIDATE("Base Unit of Measure");
                        // MCO Le 24-10-2018 => FE20181016 -> SYMTA
                        IF _Designation <> '' THEN
                            RecItem.VALIDATE(Description, _Designation);
                        // FIN MCO Le 24-10-2018 => FE20181016

                        // Cr‚ation de l'article fournisseur
                        IF RecItemVendorModel.GET(RecVendor."No.", RecItemModel."No.", '') THEN BEGIN
                            RecitemVendor.init();
                            RecitemVendor.TRANSFERFIELDS(RecItemVendorModel);
                            RecitemVendor."Item No." := RecItem."No.";
                            RecitemVendor.VALIDATE("Ref. Active", _BuilderItemNo);
                            RecitemVendor.VALIDATE("Vendor Item No.", _VendorItemNo);
                            RecitemVendor.INSERT(TRUE);
                            LItemOK += 1;
                        END;

                        // CFR le 25/10/2023 - R‚gie : Cr‚ation du contenu emplacement par d‚faut ZD/1PT
                        CLEAR(gBinCreationWorksheetLine);
                        gBinCreationWorksheetLine.VALIDATE("Worksheet Template Name", 'CONTENU EM');
                        gBinCreationWorksheetLine.VALIDATE(Name, 'DEFAUT');
                        gBinCreationWorksheetLine.VALIDATE("Location Code", 'SP');
                        gBinCreationWorksheetLine.VALIDATE("Line No.", gLineNo);
                        gBinCreationWorksheetLine.VALIDATE(Type, gBinCreationWorksheetLine.Type::"Bin Content");
                        gBinCreationWorksheetLine.VALIDATE("Bin Code", 'ZD');
                        gBinCreationWorksheetLine.VALIDATE(Default, TRUE);
                        gBinCreationWorksheetLine.VALIDATE("Item No.", RecItem."No.");
                        gBinCreationWorksheetLine.INSERT();// THEN MESSAGE('Insert : %1', RecItem."No.");
                        gLineNo += 10000;
                        // FIN CFR le 25/10/2023

                    END;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(content)
            {
                group(Model)
                {
                    Caption = 'Model';
                    field("Item model"; ItemModel)
                    {
                        Caption = 'Item model';
                        TableRelation = Item;
                        ApplicationArea = All;
                        ToolTip = 'Specifies the value of the Item model field.';
                    }
                }
            }
        }
        trigger OnQueryClosePage(CloseAction: ACTION): Boolean
        begin
            IF CloseAction = ACTION::OK THEN
                IF NOT RecItemModel.GET(ItemModel) THEN
                    ERROR(Err001Err);
        end;
    }
    trigger OnInitXmlPort()
    begin
        LItemOK := 0;
        LItemTot := 0;
    end;

    trigger OnPreXmlPort()
    begin
        // CFR le 25/10/2023 - R‚gie : Cr‚ation du contenu emplacement par d‚faut ZD/1PT
        gBinCreationWorksheetLine.SETRANGE("Worksheet Template Name", 'CONTENU EM');
        gBinCreationWorksheetLine.SETRANGE(Name, 'DEFAUT');
        gBinCreationWorksheetLine.DELETEALL();
        gLineNo := 10000;
        // FIN CFR le 25/10/2023 
    end;

    trigger OnPostXmlPort()
    var
        lBinCreateLine: Record "Bin Creation Worksheet Line";
    begin
        // CFR le 25/10/2023 - R‚gie : Cr‚ation du contenu emplacement par d‚faut ZD/1PT
        IF NOT gBinCreationWorksheetLine.ISEMPTY() THEN BEGIN
            lBinCreateLine.COPY(gBinCreationWorksheetLine);
            CODEUNIT.RUN(CODEUNIT::"Bin Content Create", lBinCreateLine);
            lBinCreateLine.RESET();
        END;
        // FIN CFR le 25/10/2023

        MESSAGE(Txt001Msg, LItemOK, LItemTot - LItemOK);
    end;

    var
        RecItemModel: Record Item;
        RecItemVendorModel: Record "Item Vendor";
        RecItem: Record Item;
        RecitemVendor: Record "Item Vendor";
        RecVendor: Record Vendor;
        gBinCreationWorksheetLine: Record "Bin Creation Worksheet Line";
        ItemModel: Code[20];
        LItemOK: Integer;
        LItemTot: Integer;
        Txt001Msg: Label 'Import article terminé, %1 articles créés, %2 articles en erreur', Comment = '%1 = Nombre des Articles créés ; %2=Nombre des articles en erreur';
        Err001Err: Label 'Le code article modèle est obligatoire';
        gLineNo: Integer;
    // FBO le 18-04-2017 => FE20170310
    // CFR le 25/10/2023 - R‚gie : Cr‚ation du contenu emplacement par d‚faut ZD/1PT
}