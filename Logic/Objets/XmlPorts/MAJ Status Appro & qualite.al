xmlport 50026 "MAJ Status Appro & qualite"
{
    Caption = 'MAJ Status Appro & qualite';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemVendor; "Item Vendor")
            {
                RequestFilterFields = "Item No.";
                UseTemporary = false;
                AutoUpdate = true;
                fieldelement("No."; ItemVendor."Item No.")
                {

                }
                fieldelement(Fournisseur; ItemVendor."Vendor No.")
                {

                }
                fieldelement(statu_appro; ItemVendor."Statut Approvisionnement")
                {

                }
                fieldelement(date_maj_statu_appro; ItemVendor."date maj statut appro")
                {

                }
                fieldelement(usr_maj_statu_appro; ItemVendor."user maj statut appro")
                {

                }
                fieldelement(statu_qualite; ItemVendor."Statut Qualité")
                {

                }
                fieldelement(date_maj_statu_qualite; ItemVendor."date maj statut qualité")
                {

                }
                fieldelement(user_majŠstatu_qualite; ItemVendor."user maj statut qualité")
                {

                }
            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}