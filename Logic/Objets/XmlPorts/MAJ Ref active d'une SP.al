xmlport 50027 "MAJ Ref active d'une SP"
{
    Caption = 'MAJ Ref active d''une SP';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(No_2; Item."No. 2")
                {

                }
            }
        }
    }
}