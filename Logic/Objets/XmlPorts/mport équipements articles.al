xmlport 50006 "Import équipements articles"
{
    Caption = 'Import équipements articles';
    Direction = Both;
    Format = VariableText;
    FieldDelimiter = '<None>';
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                AutoUpdate = false;
                AutoReplace = false;
                textelement(Ref)
                {
                }
                textelement(Type)
                {
                }
                textelement(Marque)
                {
                }
                textelement(Modele)
                {
                }
                textelement(Repere)
                {
                }
                trigger OnAfterInsertRecord()
                begin
                    Item.SETRANGE("No. 2", "Ref");
                    Item.FINDFIRST();

                    Equipment.SETRANGE(Manufacturer, Marque);
                    Equipment.SETRANGE("Equipment Type", Type);
                    Equipment.SETRANGE(Model, Modele);
                    Equipment.FINDFIRST();

                    ItemEquipment.INIT();
                    ItemEquipment.VALIDATE("Item No.", Item."No.");
                    ItemEquipment.VALIDATE("Equipment No.", Equipment."Equipment No.");
                    ItemEquipment.VALIDATE("Trace No.", Repere);
                    ItemEquipment.INSERT(TRUE);
                end;
            }
        }
    }

    var
        Item: Record Item;
        Equipment: Record Equipment;
        ItemEquipment: Record "Modele / Article";
}