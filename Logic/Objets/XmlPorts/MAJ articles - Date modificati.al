xmlport 50051 "MAJ articles - Date modificati"
{
    Caption = 'MAJ articles - Date modificati';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Modify_Date; Item."Modify Date")
                {

                }
            }
        }
    }
}