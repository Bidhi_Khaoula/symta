xmlport 50080 "MAJ-Code remise_livraison"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;
    TransactionType = Update;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Customer; Customer)
            {
                AutoReplace = false;
                AutoUpdate = true;
                XmlName = 'Customer';
                fieldelement(numero_client; Customer."No.")
                {
                }
                fieldelement(C_Remise_Livraison; Customer."Shipment Method Code")
                {
                }
            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}

