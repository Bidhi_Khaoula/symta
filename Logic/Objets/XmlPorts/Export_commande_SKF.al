xmlport 50059 "Export_commande_SKF"
{
    Caption = 'Export_commande_SKF';
    Direction = Export;
    Format = VariableText;
    FieldSeparator = ';';
    FileName = 'skf_commande.csv';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Commande_achat; "Purchase Header")
            {
                RequestFilterFields = "Buy-from Vendor No.", "No.";
                AutoUpdate = true;
                MinOccurs = Once;
                fieldelement(Numero_commande; Commande_achat."No.")
                {

                }
                fieldelement(Fournisseur; Commande_achat."Buy-from Vendor No.")
                {

                }
                fieldelement(Type_commande; Commande_achat."Vendor Order No.")
                {

                }
                fieldelement(Livraisson_nom; Commande_achat."Ship-to Name")
                {

                }
                fieldelement(Livraisson_nom2; Commande_achat."Ship-to Name 2")
                {

                }
                fieldelement(Livraison_adresse; Commande_achat."Ship-to Address")
                {

                }
                fieldelement(Livraison_adresse2; Commande_achat."Ship-to Address 2")
                {

                }
                fieldelement(Livraison_cp; Commande_achat."Ship-to Post Code")
                {

                }
                fieldelement(Livraison_ville; Commande_achat."Ship-to City")
                {

                }
            }
            tableelement(lignes_commande; "Purchase Line")
            {
                //TODOLinkFields = Field3 = FIELD(Field3);
                LinkTable = Commande_achat;
                textelement(num_ligne)
                {
                    trigger OnBeforePassVariable()
                    begin
                        num_ligne := 'LINE' //+ (FORMAT("Purchase Line"."Line No.")); //

                    end;
                }
                fieldelement(Numero_de_ligne; lignes_commande."Line No.")
                {

                }
                fieldelement(ref_externe; lignes_commande."Item Reference No.")
                {

                }
                fieldelement(Quantite; lignes_commande.Quantity)
                {

                }
            }
        }
    }
}