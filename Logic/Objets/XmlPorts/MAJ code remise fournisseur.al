xmlport 50045 "MAJ code remise fournisseur"
{
    Caption = 'MAJ code remise fournisseur';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemVendor; "Item Vendor")
            {
                fieldelement("ItemNo."; ItemVendor."Item No.")
                {

                }
                fieldelement("VendorNo."; ItemVendor."Vendor No.")
                {

                }
                fieldelement(CodeRemise; ItemVendor."Code Remise")
                {

                }
            }
        }
    }
}