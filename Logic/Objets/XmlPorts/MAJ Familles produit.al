xmlport 50049 "MAJ Familles produit"
{
    Caption = 'MAJ Familles produit';
    Direction = Both;
    Format = VariableText;
    FieldDelimiter = 'None';
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement(Ref_SP; Item."No.")
                {

                }
                fieldelement(Famille; Item."Item Category Code")
                {

                }
            }
        }
    }
}