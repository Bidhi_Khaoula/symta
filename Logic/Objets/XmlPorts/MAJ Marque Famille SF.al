xmlport 50068 "MAJ Marque Famille SF"
{
    Caption = 'MAJ Marque Famille SF';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                RequestFilterFields = "No.";
                SourceTableView = SORTING("No.");
                UseTemporary = false;
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Marque; Item."Manufacturer Code")
                {

                }
                fieldelement(Famille; Item."Item Category Code")
                {

                }
                //fieldelement(Sous_Famille; Item."Product Group Code"){ }
            }
        }
    }

}