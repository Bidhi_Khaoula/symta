xmlport 50044 "maj grille remise client"
{
    Caption = 'maj grille remise client';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(SalesLineDiscount; "Sales Line Discount")
            {
                fieldelement(SalesCode; SalesLineDiscount."Sales Code")
                {

                }
                fieldelement(Type; SalesLineDiscount.Type)
                {

                }
                fieldelement(LineDiscount1; SalesLineDiscount."Line Discount 1 %")
                {

                }
                fieldelement(LineDiscount2; SalesLineDiscount."Line Discount 2 %")
                {

                }
                fieldelement(LineDiscount; SalesLineDiscount."Line Discount %")
                {

                }
                fieldelement(SalesType; SalesLineDiscount."Sales Type")
                {

                }
                fieldelement(MinimumQuantity; SalesLineDiscount."Minimum Quantity")
                {

                }
                fieldelement(Type_de_commande; SalesLineDiscount."Type de commande")
                {

                }
                fieldelement(CodeMarque; SalesLineDiscount."Code Marque")
                {

                }
                fieldelement(CodeFamille; SalesLineDiscount."Code Famille")
                {

                }
                fieldelement(CodeSousFamille; SalesLineDiscount."Code Sous Famille")
                {

                }
                fieldelement(CodeArticle; SalesLineDiscount."Code Article")
                {

                }
                fieldelement(StartingDate; SalesLineDiscount."Starting Date")
                {

                }
                fieldelement(EndingDate; SalesLineDiscount."Ending Date")
                {

                }
            }
        }
    }
}