xmlport 50065 "MAJ articles - DPD Couleur"
{
    Caption = 'MAJ articles - DPD Couleur';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Transporteur_preconise; Item."Recommended Shipping Agent")
                {

                }
            }
        }
    }
}