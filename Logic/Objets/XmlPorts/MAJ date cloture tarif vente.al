xmlport 50008 "MAJ date cloture tarif vente"
{
    caption = 'MAJ date cloture tarif vente';
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(SalesPrice; "Sales Price")
            {
                RequestFilterFields = "Item No.", "Sales Code", "Currency Code";
                AutoUpdate = true;
                fieldelement(Ref_sp; SalesPrice."Item No.")
                {

                }
                fieldelement(Date_cloture; SalesPrice."Ending Date")
                {

                }
                fieldelement(TypeTarif; SalesPrice."Sales Type")
                {

                }
                fieldelement(type_COMMANDE; SalesPrice."Type de commande")
                {

                }
                fieldelement(Code_vente; SalesPrice."Sales Code")
                {
                    FieldValidate = Yes;

                }
            }
        }
    }



}