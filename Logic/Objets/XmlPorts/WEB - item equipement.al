xmlport 50055 "WEB - item equipement"
{
    Caption = 'WEB - item equipement';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemModeleArticle; "Modele / Article")
            {
                AutoUpdate = true;
                MinOccurs = Once;
                fieldelement(Ref_SP; ItemModeleArticle."Item No.")
                {

                }
                fieldelement(Num_equipement; ItemModeleArticle."Equipment No.")
                {

                }
                fieldelement(Marque; ItemModeleArticle."Equipment Manufacturer")
                {

                }
                fieldelement(Modele; ItemModeleArticle."Equipment Model")
                {

                }
                fieldelement(Type; ItemModeleArticle."Equipment Type")
                {

                }
                fieldelement(Ref_active; ItemModeleArticle."Item No.2")
                {

                }
            }
        }
    }
}