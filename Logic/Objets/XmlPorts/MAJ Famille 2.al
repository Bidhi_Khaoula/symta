// xmlport 50012 "MAJ Famille 2"
// {
//     Caption = 'MAJ Famille 2';
//     Direction = Import;
//     Format = VariableText;
//     FieldSeparator = ';';
//     schema
//     {
//         textelement(ImportElementVar)
//         {
//             tableelement(Integer; Integer)
//             {
//                 AutoSave = false;
//                 textelement(CArticle)
//                 {

//                 }
//                 textelement(CFamille)
//                 {

//                 }
//                 trigger OnAfterInsertRecord()
//                 var
//                     Famille: Record "Item Category";
//                 //"S-Famille": Record "Product Group";
//                 //"OldS-Famille": Record "Product Group";
//                 begin
//                     _CArticle := CArticle;
//                     _CFamille := CFamille;

//                     i += 1;

//                     Item.GET(CUMulti.RechercheArticleByActive(_CArticle));


//                     IF NOT Famille.GET(_CFamille) THEN BEGIN
//                         CLEAR(Famille);
//                         Famille.Code := _CFamille;
//                         Famille.Description := FORMAT(WORKDATE);
//                         Famille.VALIDATE("Def. Inventory Posting Group", 'NEGOCE');
//                         Famille.VALIDATE("Def. Costing Method", Famille."Def. Costing Method"::"3");
//                         Famille.VALIDATE("Def. VAT Prod. Posting Group", 'NORMAL');
//                         Famille.INSERT(TRUE);
//                     END;

//                     IF NOT "S-Famille".GET(_CFamille, Item."Product Group Code") THEN BEGIN
//                         "OldS-Famille".GET(Item."Item Category Code", Item."Product Group Code");
//                         "S-Famille" := "OldS-Famille";
//                         "S-Famille"."Item Category Code" := _CFamille;
//                         "S-Famille".INSERT(TRUE);
//                     END;


//                     Item."Item Category Code" := _CFamille;
//                     Item.MODIFY();
//                 end;
//             }
//         }
//     }


//     trigger OnPreXmlPort()
//     begin
//         i := 0;
//     end;

//     var
//         _CArticle: Code[20];
//         _CFamille: Code[10];
//         "---": Integer;
//         Item: Record Item;
//         CUMulti: Codeunit "Gestion Multi-référence";
//         i: Integer;
// }