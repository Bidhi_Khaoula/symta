xmlport 50077 "MAJ articles - Catégorie produ"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                XmlName = 'Item';
                fieldelement("No."; Item."No.")
                {
                }
                fieldelement(Categorie_produit; Item."Catégorie produit")
                {
                }
            }
        }
    }

}

