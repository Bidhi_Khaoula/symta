xmlport 50063 "Renommage casier"
{
    Caption = 'Renommage casier';
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportCasier)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                textelement(AncienCasier)
                {

                }
                textelement(NouveauCasier)
                {

                }
                trigger OnBeforeInsertRecord()
                begin
                    it += 1;
                    tab_casier1[it] := AncienCasier;
                    tab_casier2[it] := NouveauCasier;
                end;
            }
        }
    }


    trigger OnInitXmlPort()
    begin
        it := 0;

        CLEAR(rec_Location);
        IF rec_Location.FINDFIRST() THEN
            str_DefaultLocation := rec_Location.Code
    end;

    trigger OnPreXmlPort()
    begin
        FenetreTitre := LblFenetreTitreLbl;
        IF NOT CONFIRM(Text001Qst) THEN
            ERROR(Text003Msg);

        Fenetre.OPEN('#1##########' + '\' +
                     'Ligne  :  #2##########' + '\' +
                     'Ancien casier :  #3##########' + '\' +
                     'Nouveau casier :  #4########## \' +
                     '  @5@@@@@@@@@@', FenetreTitre);
        Fenetre.UPDATE(2, '');
        Fenetre.UPDATE(3, '');
        Fenetre.UPDATE(4, '');
        Fenetre.UPDATE(5, 0);
        //SLEEP(3000);
    end;

    trigger OnPostXmlPort()
    begin
        imax := it;

        FOR it := 1 TO imax DO BEGIN
            Fenetre.UPDATE(2, it);
            Fenetre.UPDATE(3, tab_casier1[it]);
            Fenetre.UPDATE(4, tab_casier2[it]);
            Fenetre.UPDATE(5, ROUND(it / imax * 10000, 1));
            SLEEP(3000);
            IF rec_Bin.GET(str_DefaultLocation, tab_casier1[it]) THEN
                rec_Bin.RENAME(str_DefaultLocation, tab_casier2[it])
            //MESSAGE('Mise … jour en cours : %1',it);
        END;

        MESSAGE(Text002Msg, imax);
        Fenetre.CLOSE();
    end;

    var
        rec_Bin: Record Bin;
        rec_Location: Record Location;
        Text001Qst: Label 'Lancement de la mise à jour ?';
        Text002Msg: Label 'Mise à jour terminée : %1 casier(s) renommé(s)', Comment = '%1 = Nombre casiers';
        Text003Msg: Label 'Traitement abandonné';
        str_DefaultLocation: Code[20];
        Fenetre: Dialog;
        LblFenetreTitreLbl: Label 'Renommage des casiers';
        FenetreTitre: Text;

        tab_casier1: ARRAY[8000] OF Code[10];
        tab_casier2: ARRAY[8000] OF Code[10];
        it: Integer;
        imax: Integer;

    //- PMA le 28-03-18 (FE20180321 - Renommage casier)


}