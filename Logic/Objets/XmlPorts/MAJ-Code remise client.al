xmlport 50061 "MAJ-Code remise client"
{
    Caption = 'MAJ-Code remise client';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Customer; Customer)
            {
                AutoUpdate = true;
                AutoReplace = false;
                fieldelement(numero_client; Customer."No.")
                {

                }
                fieldelement(code_remise; Customer."Customer Disc. Group")
                {

                }
            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}