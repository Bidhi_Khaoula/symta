xmlport 50067 "MAJ Sou-Famille"
{
    Caption = 'MAJ Sou-Famille';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                RequestFilterFields = "No.";
                SourceTableView = SORTING("No.");
                UseTemporary = false;
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                //TODOfieldelement(Sous_Famille; Item."Product Group Code"){ }
            }
        }
    }
    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}