xmlport 50047 "Maj Nature"
{
    Caption = 'Maj Nature';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Nature; Item.Nature)
                {

                }
            }
        }
    }

}