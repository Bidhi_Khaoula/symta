xmlport 50048 "Categorie Produit MAJ"
{
    Caption = 'Categorie Produit MAJ';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Catégorieproduit; Item."Catégorie produit")
                {

                }
            }
        }
    }
}