xmlport 50078 "MAJ fournisseur unite achats"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement("Item Vendor"; "Item Vendor")
            {
                AutoUpdate = true;
                XmlName = 'ItemVendor';
                fieldelement("ItemNo."; "Item Vendor"."Item No.")
                {
                }
                fieldelement("VendorNo."; "Item Vendor"."Vendor No.")
                {
                }
                fieldelement(Unite_achat; "Item Vendor"."Purch. Unit of Measure")
                {
                }
            }
        }
    }

}

