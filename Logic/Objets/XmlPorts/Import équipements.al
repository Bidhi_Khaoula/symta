xmlport 50005 "Import équipements"
{
    Caption = 'Import équipements';
    Direction = Both;
    Format = VariableText;
    FieldDelimiter = '<None>';
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Equipment; Equipment)
            {
                AutoUpdate = true;
                fieldelement(Manufacturer; Equipment.Manufacturer)
                {
                    FieldValidate = Yes;
                }
                fieldelement(Model; Equipment.Model)
                {
                    FieldValidate = Yes;
                }
                fieldelement(EquipmentType; Equipment."Equipment Type")
                {
                    FieldValidate = Yes;
                }
            }
        }
    }
    trigger OnInitXmlPort()
    begin
        MESSAGE('Rappel colonnes du fichier CSV : Marque;Modele;Type');
    end;

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');

    end;
}