xmlport 50042 "maj code concur"
{
    Caption = 'maj code concur';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(CodeConcurrence; Item."Code Concurrence")
                {

                }
            }
        }
    }

}