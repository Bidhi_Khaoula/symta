xmlport 50052 "MAJ articles - Publiable Web"
{
    Caption = 'MAJ articles - Publiable Web';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Publiable_Web; Item."Publiable")
                {

                }
            }
        }
    }
}