xmlport 50076 "GPS - SUPP -CONTACT"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;
    TransactionType = Update;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Contact; Contact)
            {
                AutoReplace = false;
                AutoUpdate = true;
                XmlName = 'Customer';
                fieldelement(numero_contact; Contact."No.")
                {
                }
                fieldelement(Latitude; Contact.Latitude)
                {
                }
                fieldelement(Longitude; Contact.Longitude)
                {
                }
                fieldelement(Calcul_coordonnees; Contact."Change Coordinate")
                {
                }
            }
        }
    }


    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}

