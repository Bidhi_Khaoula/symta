xmlport 50081 "MAJ Articles - multi critères"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;
    TransactionType = Update;

    schema
    {
        textelement("<importelementvar>")
        {
            XmlName = 'ImportElementVar';
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                RequestFilterFields = "No.";
                XmlName = 'Item';
                SourceTableView = SORTING("No.");
                UseTemporary = false;
                fieldelement("No."; Item."No.")
                {
                }
                fieldelement(Designation; Item.Description)
                {
                }
                fieldelement(Publiable; Item.Publiable)
                {
                }
                fieldelement(Stocke; Item."Stocké")
                {
                }
                fieldelement(date_stocke; Item."date maj stocké")
                {
                }
                fieldelement(user_stocke; Item."user maj stocké")
                {
                }
            }
        }
    }


    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}

