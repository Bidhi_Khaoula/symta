xmlport 50003 "Customer - eCommerce"
{
    Caption = 'Customer - eCommerce';
    Encoding = UTF8;
    schema
    {
        textelement(Clients)
        {
            tableelement(Customer; Customer)
            {
                fieldelement(Code; Customer."No.")
                {

                }
                fieldelement(Nom; Customer.Name)
                {

                }
                fieldelement(Addresse; Customer.Address)
                {

                }
                fieldelement(Addresse2; Customer."Address 2")
                {

                }
                fieldelement(CodePostal; Customer."Post Code")
                {

                }
                fieldelement(Departement; Customer.County)
                {

                }
                fieldelement(Ville; Customer.City)
                {

                }
                fieldelement(CodePays; Customer."Country/Region Code")
                {

                }
                fieldelement(Telephone; Customer."Phone No.")
                {

                }
                fieldelement(Fax; Customer."Telex No.")
                {

                }
                textelement(Distributeur)
                {

                }
                fieldelement(SalesLCY; Customer."Sell to Sales (LCY)")
                {

                }
                fieldelement(PresentWEB; Customer."Présent Web")
                {

                }
                trigger OnAfterGetRecord()
                begin
                    IF ParamVente.GET() THEN;
                    DateFrom := CALCDATE(ParamVente."Sales LCY Period", TODAY);

                    Customer.SETRANGE(Customer."Date Filter", DateFrom, TODAY);
                    // AD Le 17-06-2010 => Chagement du champ
                    //Customer.CALCFIELDS(Customer."Sales (LCY)");
                    //IF ((Customer."Pr‚sent Web") AND (ParamVente."Sales LCY" <= Customer."Sales (LCY)"))
                    Customer.CALCFIELDS("Sell to Sales (LCY)");
                    //----------------------------------------------------------------------------------------------------------------------------------
                    //WF le 16/09/2010 => Ajout d'une option permettant de forc‚ la pr‚sence Web
                    //IF ((Customer."Pr‚sent Web") AND (ParamVente."Sales LCY" <= Customer."Sell to Sales (LCY)"))
                    IF (((Customer."Présent Web") AND (ParamVente."Sales LCY" <= Customer."Sell to Sales (LCY)"))) OR (Customer."Forced Web")
                        //Fin WF le 16/09/2010
                        //----------------------------------------------------------------------------------------------------------------------------------
                        // FIN AD Le 17-06-2010
                        THEN
                        Distributeur := 'OUI'
                    ELSE
                        Distributeur := 'NON';
                end;
            }
        }
    }

    var
        ParamVente: Record "Sales & Receivables Setup";
        DateFrom: Date;

    //WF le 16/09/2010 => Ajout d'une option permettant de forc‚ la pr‚sence Web

}