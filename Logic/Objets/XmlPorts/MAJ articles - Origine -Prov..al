xmlport 50071 "MAJ articles - Origine -Prov."
{
    Caption = 'MAJ articles - Origine -Prov.';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Origine; Item."Country/Region of Origin Code")
                {

                }
                fieldelement(Provenance; Item."Code pays/région provenance")
                {

                }
            }
        }
    }

}