xmlport 50058 "MAJ articles - web prix net"
{
    Caption = 'MAJ articles - web prix net';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(prix_net_web; Item."Net Price Only")
                {

                }
            }
        }
    }
}