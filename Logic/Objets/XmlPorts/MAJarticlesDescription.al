xmlport 50070 "MAJ articles - Description"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                XmlName = 'Item';
                fieldelement("No."; Item."No.")
                {
                }
                fieldelement(Description; Item.Description)
                {
                }
            }
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }
}

