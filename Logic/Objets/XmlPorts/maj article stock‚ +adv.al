xmlport 50025 "maj article stocké +adv"
{
    Caption = 'maj article stocké +adv';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                RequestFilterFields = "No.";
                SourceTableView = SORTING("No.");
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {
                    FieldValidate = Yes;
                }
                fieldelement(Stocké; Item.Stocké)
                {
                }
                fieldelement(date_maj_stocké; Item."date maj stocké")
                {
                }
                fieldelement(user_maj_stocké; Item."user maj stocké")
                {
                }


            }
        }
    }

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}