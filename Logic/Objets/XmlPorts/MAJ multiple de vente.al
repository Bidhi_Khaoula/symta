xmlport 50030 "MAJ multiple de vente"
{
    caption = 'MAJ multiple de vente';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Sales_multiple; Item."Sales multiple")
                {

                }
                fieldelement(Date_multiple; Item."date maj multiple")
                {

                }
                fieldelement(User_multiple; Item."user maj multiple")
                {

                }
            }
        }
    }

}