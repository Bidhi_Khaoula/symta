xmlport 50060 "Cloture tarif fournisseur"
{
    Caption = 'Cloture tarif fournisseur';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Prix_fournisseur; "Purchase Price")
            {
                AutoUpdate = true;
                fieldelement(Numero_article; Prix_fournisseur."Item No.")
                {

                }
                fieldelement(Numero_fournisseur; Prix_fournisseur."Vendor No.")
                {

                }
                fieldelement(date_debut; Prix_fournisseur."Starting Date")
                {

                }
                fieldelement(date_fin; Prix_fournisseur."Ending Date")
                {

                }
                fieldelement(Code_Unite; Prix_fournisseur."Unit of Measure Code")
                {

                }
            }
        }
    }
    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}