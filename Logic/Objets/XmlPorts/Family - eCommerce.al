xmlport 50001 "Family - eCommerce"
{
    Caption = 'Family - eCommerce';
    Encoding = UTF8;
    schema
    {
        textelement(Familles)
        {
            tableelement(Famille; "Item Category")
            {
                fieldelement(CodeFamille; Famille.Code)
                {

                }
                fieldelement(LibelleFamille; Famille.Description)
                {

                }
                textelement(SousFamilles)
                {  /*TODO
                    tableelement(SousFamille; "Product Group")
                    {
                        LinkFields = Field1 = FIELD(Field1);
                        LinkTable = famille;
                        fieldelement(CodeSousFamille; SousFamille.Code)
                        {

                        }
                        fieldelement(LibelleSousFamille; SousFamille.Description)
                        {

                        }
                        trigger OnAfterGetRecord()
                        begin
                            //MC => On ne remonte que les familles qui possŠdent au moins 1 produit Publiable Cata+Web
                            Rec_Item.RESET();
                            Rec_Item.SETRANGE("Item Category Code", SousFamille."Item Category Code");
                            Rec_Item.SETRANGE("Product Group Code", SousFamille.Code);
                            Rec_Item.SETRANGE(Publiable, Rec_Item.Publiable::"Catalogue+Web");

                            IF Rec_Item.ISEMPTY THEN
                                currXMLport.SKIP;
                            //FIN MC
                        end;
                    }*/
                }
                trigger OnAfterGetRecord()
                begin
                    //MC => On ne remonte que les familles qui possŠdent au moins 1 produit Publiable Cata+Web
                    Rec_Item.RESET();
                    Rec_Item.SETRANGE("Item Category Code", Famille.Code);
                    Rec_Item.SETRANGE(Publiable, Rec_Item.Publiable::"Catalogue+Web");

                    IF Rec_Item.ISEMPTY THEN
                        currXMLport.SKIP();
                    //FIN MC
                end;
            }
        }
    }
    var
        Rec_Item: Record item;
}