xmlport 50043 "maj table item vendor remise"
{
    Caption = 'maj table item vendor remise';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemVendor; "Item Vendor")
            {
                AutoUpdate = true;
                fieldelement("ItemNo."; ItemVendor."Item No.")
                {

                }
                fieldelement("VendorNo."; ItemVendor."Vendor No.")
                {

                }
                fieldelement(Code_Remise; ItemVendor."Code Remise")
                {
                    FieldValidate = No;
                    MinOccurs = Zero;
                    MaxOccurs = Once;
                }
            }
        }
    }
}