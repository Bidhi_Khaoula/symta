xmlport 50036 "MAJ vendor_no dans item"
{
    Caption = 'MAJ vendor_no dans item';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement("VendorNo."; Item."Vendor No.")
                {

                }
            }
        }
    }

    trigger OnPreXmlPort()
    Var
        rec_item: Record Item;
    BEGIN

        IF CONFIRM('RAZ de vendor_no dans tout le fichier ITEM') THEN BEGIN
            rec_item.RESET();
            rec_item.MODIFYALL(rec_item."Vendor No.", '');
        END;
    END;
}