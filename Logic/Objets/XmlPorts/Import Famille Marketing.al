xmlport 50004 "Import Famille Marketing"
{
    Caption = 'Import Famille Marketing';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                textelement(CodeArticle) { }
                textelement(SuperFamilleMarketing) { }

                textelement(FamilleMarketing) { }

                textelement(SousFamilleMarketing) { }

                trigger OnAfterInsertRecord()
                var
                    Famille: Record "Famille Marketing";
                begin
                    _CArticle := CodeArticle;
                    _CSuperFamille := SuperFamilleMarketing;
                    _CFamille := FamilleMarketing;
                    _CSousFamille := SousFamilleMarketing;

                    // recherche article par rapport … sa ref
                    IF NOT Item.GET(CUMulti.RechercheArticleByActive(_CArticle)) THEN ERROR(Format('Article ' + CodeArticle + ' introuvable'));


                    // cr‚ation famille si inexistante
                    IF NOT Famille.GET(_CSuperFamille) THEN BEGIN
                        CLEAR(Famille);
                        Famille.Code := _CSuperFamille;
                        Famille.Description := _CSuperFamille;
                        Famille.Type := Famille.Type::"Super family";
                        Famille.INSERT(TRUE);
                    END;

                    IF NOT Famille.GET(_CFamille) THEN BEGIN
                        CLEAR(Famille);
                        Famille.Code := _CFamille;
                        Famille.Description := _CFamille;
                        Famille."Code Niveau supérieur" := _CSuperFamille;
                        Famille.Type := Famille.Type::Family;
                        Famille.INSERT(TRUE);
                    END;

                    IF NOT Famille.GET(_CSousFamille) THEN BEGIN
                        CLEAR(Famille);
                        Famille.Code := _CSousFamille;
                        Famille.Description := _CSousFamille;
                        Famille."Code Niveau supérieur" := _CFamille;
                        Famille.Type := Famille.Type::"Sous family";
                        Famille.INSERT(TRUE);
                    END;

                    // attribution des valeurs … l'article
                    Item."Super Famille Marketing" := _CSuperFamille;
                    Item."Famille Marketing" := _CFamille;
                    Item."Sous Famille Marketing" := _CSousFamille;

                    Item.MODIFY();
                end;

            }
        }

    }
    trigger OnInitXmlPort()
    begin
        ERROR('plus util');
    end;

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import termimé');
    end;

    var
        Item: Record Item;
        CUMulti: Codeunit "Gestion Multi-référence";
        _CArticle: Code[20];
        _CSuperFamille: Code[10];
        _CFamille: Code[10];
        _CSousFamille: Code[10];
}