xmlport 50056 "Nomenclature - Article"
{
    Caption = 'Nomenclature - Article';
    Direction = Both;
    Format = VariableText;
    FieldDelimiter = '<None>';
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(num_nomenclature; Item."Tariff No.")
                {

                }
            }
        }
    }
}