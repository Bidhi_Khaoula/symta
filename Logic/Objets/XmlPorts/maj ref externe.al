xmlport 50038 "maj ref externe"
{
    Caption = 'maj ref externe';
    TransactionType = UpdateNoLocks;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemReference; "Item Reference")
            {
                AutoUpdate = true;
                MinOccurs = Once;

                fieldelement(ItemNo; ItemReference."Item No.")
                {

                }
                fieldelement(ReferenceNo; ItemReference."Reference No.")
                {

                }
                fieldelement(Type; ItemReference."Reference Type")
                {

                }
                fieldelement(Description; ItemReference.Description)
                {
                    MinOccurs = Zero;
                }
                fieldelement(UnitofMeasure; ItemReference."Unit of Measure")
                {
                    MinOccurs = Zero;

                }
                fieldelement(CodeConstructeur; ItemReference."Code Constructeur")
                {

                }
                fieldelement(publiableweb; ItemReference."publiable web")
                {

                }
                fieldelement(User_modify; ItemReference."Modify User ID")
                {

                }
                fieldelement(Modify_Date; ItemReference."Modify Date")
                {

                }
            }
        }
    }

}