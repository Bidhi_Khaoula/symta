xmlport 50057 "MAJ articles - qté vente WEB"
{
    Caption = 'MAJ articles - qté vente WEB';
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(Maxi_publiable; Item."Stock Maxi Publiable")
                {

                }
            }
        }
    }
}