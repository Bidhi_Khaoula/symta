xmlport 50029 "MAJ ref fournisseur"
{
    Caption = 'MAJ ref fournisseur';
    Direction = Both;
    Format = VariableText;
    FieldDelimiter = 'None';
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(ItemVendor; "Item Vendor")
            {
                AutoUpdate = true;
                fieldelement("ItemNo."; ItemVendor."Item No.")
                {

                }
                fieldelement("VendorNo."; ItemVendor."Vendor No.")
                {

                }
                fieldelement("VendorItemNo."; ItemVendor."Vendor Item No.")
                {

                }
            }
        }
    }
    trigger OnInitXmlPort()
    begin
        MESSAGE('Rappel colonnes du fichier CSV : Nø Article;Nø Fournisseur;Ref fournisseur');
    end;

    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}