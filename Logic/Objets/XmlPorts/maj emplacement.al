xmlport 50040 "maj emplacement"
{
    Caption = 'maj emplacement';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Integer; Integer)
            {
                AutoSave = false;

                textelement(ref_interne)
                {

                }
                textelement(anc_emplacement)
                {

                }
                textelement(nou_emplacement)
                {

                }
                textelement(nou_zone)
                {

                }
                trigger OnAfterInitRecord()
                var
                    rec_bin_cont: Record "Bin Content";
                    rec_item: Record Item;
                begin
                    _ref_interne := ref_interne;
                    _anc_emplacement := anc_emplacement;
                    _nou_emplacement := nou_emplacement;
                    _nou_zone := nou_zone;

                    rec_item.INIT();
                    IF NOT rec_item.GET(_ref_interne) THEN currXMLport.SKIP();
                    IF NOT rec_bin_cont.GET('SP', _anc_emplacement, _ref_interne, '', rec_item."Base Unit of Measure") THEN currXMLport.SKIP();

                    MESSAGE(_ref_interne);
                    MESSAGE(_nou_emplacement);
                    MESSAGE(_nou_zone);
                end;
            }
        }
    }

    var
        _ref_interne: Code[20];
        _anc_emplacement: Code[20];
        _nou_emplacement: Code[20];
        _nou_zone: Code[10];
}