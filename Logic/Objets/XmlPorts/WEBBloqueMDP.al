xmlport 50075 "WEB - Bloque - MDP"
{
    Direction = Both;
    FieldSeparator = ';';
    Format = VariableText;
    TransactionType = Update;

    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Customer; Customer)
            {
                AutoReplace = false;
                AutoUpdate = true;
                XmlName = 'Customer';
                fieldelement(numero_client; Customer."No.")
                {
                }
                fieldelement("Bloqué"; Customer.Blocked)
                {
                }
                fieldelement(login_web; Customer."Login WEB")
                {
                }
                fieldelement(Mot_de_passe; Customer."Mot de passe WEB")
                {
                }
                fieldelement(Desactive_web; Customer."Disable Web")
                {
                }
                fieldelement("Supprimé"; Customer."Supprimé")
                {
                }
            }
        }
    }


    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}

