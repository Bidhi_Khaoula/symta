xmlport 50007 "Marketing - Familles  articles"
{
    Caption = 'Marketing - Familles  articles';
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                RequestFilterFields = "Super Famille Marketing", "Famille Marketing", "Sous Famille Marketing";
                SourceTableView = SORTING("No.") ORDER(Ascending);
                AutoUpdate = true;
                fieldattribute("No."; Item."No.")
                {

                }
                fieldattribute(SuperFamilleMarketing; Item."Super Famille Marketing")
                {

                }
                fieldattribute(FamilleMarketing; Item."Famille Marketing")
                {
                }
                fieldattribute(SousFamilleMarketing; Item."Sous Famille Marketing")
                {

                }
            }
        }
    }



    //-MCO Le 17-01-2018 => VU avec vincent plus util refonte de sheirarchies

}