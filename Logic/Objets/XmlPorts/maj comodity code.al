xmlport 50037 "maj comodity code"
{
    Caption = 'maj comodity code';
    Direction = Import;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; Item)
            {
                AutoUpdate = true;
                fieldelement("No."; Item."No.")
                {

                }
                fieldelement(ComodityCode; Item."Comodity Code")
                {

                }
            }
        }
    }

}