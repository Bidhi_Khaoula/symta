xmlport 50050 "MAJ Status Appro"
{
    Caption = 'MAJ Status Appro';
    TransactionType = Update;
    Direction = Both;
    Format = VariableText;
    FieldSeparator = ';';
    schema
    {
        textelement(ImportElementVar)
        {
            tableelement(Item; "Item Vendor")
            {
                RequestFilterFields = "Item No.";
                UseTemporary = false;
                AutoUpdate = true;
                fieldelement("No."; Item."Item No.")
                {

                }
                fieldelement(Fournisseur; Item."Vendor No.")
                {

                }
                fieldelement(statu_appro; Item."Statut Approvisionnement")
                {

                }
                fieldelement(date_maj_statu_appro; Item."date maj statut appro")
                {

                }
                fieldelement(usr_maj_statu_appro; Item."user maj statut appro")
                {

                }
            }
        }
    }
    trigger OnPostXmlPort()
    begin
        MESSAGE('Import terminé');
    end;
}