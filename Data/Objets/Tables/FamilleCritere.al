table 50056 "Famille Critere"
{

    fields
    {
        field(1; Famille; Code[20])
        {
            TableRelation = "Famille Marketing".Code WHERE(Type = CONST("Family"),
                                                            "Code Niveau supérieur" = FIELD("Super Famille"));
        }
        field(2; "Sous Famille"; Code[20])
        {
            TableRelation = "Famille Marketing".Code WHERE(Type = CONST("Sous family"),
                                                            "Code Niveau supérieur" = FIELD(Famille));
        }
        field(3; "Super Famille"; Code[20])
        {
            TableRelation = "Famille Marketing".Code WHERE(Type = CONST("Super family"));
        }
        field(5; "Code Critere"; Code[20])
        {
            TableRelation = Critere.Code;
        }
        field(10; "Priorité"; Integer)
        {
        }
    }

    keys
    {
        key(Key1; "Super Famille", Famille, "Sous Famille", "Code Critere")
        {
            Clustered = true;
        }
        key(Key2; "Priorité")
        {
        }
    }

    fieldgroups
    {
    }
}

