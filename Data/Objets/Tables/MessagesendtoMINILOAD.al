table 50045 "Message send to MINILOAD"
{
    Caption = 'Message envoyé à MINILOAD';

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'N° Séquence';
        }
        field(2; "User ID"; Code[20])
        {
            Caption = 'Utilisateur';
        }
        field(3; "Message No."; Code[50])
        {
            Caption = 'N° Message';
        }
        field(4; Date; Date)
        {
        }
        field(5; Heure; Time)
        {
            Caption = 'Hour';
        }
    }

    keys
    {
        key(Key1; "Entry No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

