table 50062 "Comment Setup"
{

    fields
    {
        field(1; "Comment Code"; Code[10])
        {
            Description = 'Code du commentaire';
        }
        field(2; "Comment Text"; Text[40])
        {
            Description = 'Texte du commentaire';
        }
    }

    keys
    {
        key(PKey1; "Comment Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

