table 50015 "Relance Telephonique"
{

    fields
    {
        field(1; "N° de document"; Code[20])
        {
        }
        field(2; "N° client"; Code[20])
        {
        }
        field(3; "Date compta"; Date)
        {
        }
        field(4; "Date d'echéance"; Date)
        {
        }
        field(5; "Désignation"; Text[50])
        {
        }
        field(6; "Type document"; Code[20])
        {
        }
        field(7; "Montant initial"; Decimal)
        {
        }
        field(8; "Montant restant"; Decimal)
        {
        }
        field(9; "N° sequence client"; Integer)
        {
        }
        field(10; "Dernier niveau"; Integer)
        {
        }
        field(11; "Prochain niveau"; Integer)
        {
            TableRelation = "Reminder Level"."No." WHERE("Reminder Terms Code" = FIELD("Code relance"));
        }
        field(12; "Code relance"; Code[10])
        {
        }
        field(13; "Groupe compta client"; Code[10])
        {
            Editable = false;
        }
        field(14; "Mode de reglement client"; Code[10])
        {
        }
        field(15; "Ville client"; Text[30])
        {
        }
    }

    keys
    {
        key(PKey1; "N° sequence client")
        {
            Clustered = true;
        }
        key(FKey2; "N° client")
        {
            SumIndexFields = "Montant restant";
        }
    }

    fieldgroups
    {
    }
}

