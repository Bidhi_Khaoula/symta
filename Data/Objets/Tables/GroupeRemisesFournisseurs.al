table 50040 "Groupe Remises Fournisseurs"
{

    fields
    {
        field(1; "Vendor Disc. Group"; Code[10])
        {
            Caption = 'Customer Disc. Group';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FOU_GRPT_REMISE'));
        }
        field(2; "Discount Code"; Code[10])
        {
            Caption = 'Code Remise';
        }
        field(4; "Starting Date"; Date)
        {
            Caption = 'Starting Date';
        }
        field(5; "Line Discount %"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Line Discount %';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE -> Non editable';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(14; "Minimum Quantity"; Decimal)
        {
            Caption = 'Minimum Quantity';
            MinValue = 0;
        }
        field(15; "Ending Date"; Date)
        {
            Caption = 'Ending Date';
        }
        field(50001; "Line Discount 1 %"; Decimal)
        {
            Caption = '% Remise ligne 1';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
        }
        field(50002; "Line Discount 2 %"; Decimal)
        {
            Caption = '% Remise ligne 2';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
        }
        field(50402; "Type de commande"; Option)
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
    }

    keys
    {
        key(PKey1; "Vendor Disc. Group", "Discount Code", "Starting Date", "Minimum Quantity", "Type de commande")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

