table 50077 "Demande Trans. Link"
{
    fields
    {
        field(1; "Demande No."; Code[20])
        {
            Caption = 'N° Demande';
            TableRelation = "Demande transporteur";
        }
        field(2; "No."; Code[20])
        {
            Caption = '"N° "';
            TableRelation = IF (Type = CONST(Receipt)) "Warehouse Receipt Header"."No."
            ELSE
            IF (Type = CONST("Purch. Invoice")) "Purchase Header"."No." WHERE("Document Type" = CONST(Invoice),
                                                                                              "Buy-from Vendor No." = FIELD("Vendor No. Filter"))
            ELSE
            IF (Type = CONST("Purch. Invoice Archive")) "Purch. Inv. Header"."No.";
        }
        field(3; Type; Option)
        {
            OptionCaption = 'Reception,Facture achat,Facture achat enregistrée';
            OptionMembers = Receipt,"Purch. Invoice","Purch. Invoice Archive";
        }
        field(10; "Vendor No. Filter"; Code[10])
        {
            FieldClass = FlowFilter;
        }
    }

    keys
    {
        key(Key1; "Demande No.", Type, "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

