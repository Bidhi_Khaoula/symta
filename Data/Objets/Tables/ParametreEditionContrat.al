table 50039 "Parametre Edition Contrat"
{

    fields
    {
        field(1; Name; Text[30])
        {
        }
        field(2; "Image 1"; BLOB)
        {
        }
        field(3; "Image 2"; BLOB)
        {
        }
        field(4; "Image 3"; BLOB)
        {
        }
        field(5; "Image 4"; BLOB)
        {
        }
        field(6; "Image 5"; BLOB)
        {
        }
        field(7; "Image 6"; BLOB)
        {
        }
        field(8; "Image 7"; BLOB)
        {
        }
        field(9; "Image 8"; BLOB)
        {
        }
        field(10; "Image 9"; BLOB)
        {
        }
    }

    keys
    {
        key(PKey1; Name)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

