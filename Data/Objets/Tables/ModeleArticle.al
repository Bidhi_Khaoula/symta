table 50061 "Modele / Article"
{

    fields
    {
        field(1; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            NotBlank = true;
            TableRelation = Item WHERE(Type = CONST(Inventory));
        }
        field(2; "Equipment No."; Integer)
        {
            Caption = 'Equipment No.';
            TableRelation = Equipment."Equipment No.";
        }
        field(3; "Trace No."; Code[20])
        {
            Caption = 'Trace No.';
        }
        field(10; "Equipment Manufacturer"; Code[20])
        {
            CalcFormula = Lookup(Equipment.Manufacturer WHERE("Equipment No." = FIELD("Equipment No.")));
            Caption = 'Equipment Manufacturer';
            Editable = false;
            FieldClass = FlowField;
        }
        field(11; "Equipment Model"; Code[20])
        {
            CalcFormula = Lookup(Equipment.Model WHERE("Equipment No." = FIELD("Equipment No.")));
            Caption = 'Equipment Model';
            Editable = false;
            FieldClass = FlowField;
        }
        field(12; "Equipment Type"; Code[20])
        {
            CalcFormula = Lookup(Equipment."Equipment Type" WHERE("Equipment No." = FIELD("Equipment No.")));
            Caption = 'Equipment Type';
            Editable = false;
            FieldClass = FlowField;
        }
        field(13; "Equipment Options"; Text[250])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Equipment.Options WHERE("Equipment No." = FIELD("Equipment No.")));
            Caption = 'Options équipement';
            Description = 'CFR le 09/12/2021 - FA20210912';
            Editable = false;
        }

        field(30; "Item No.2"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Item No.2';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(FKey1; "Item No.", "Equipment No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

