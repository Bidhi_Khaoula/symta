table 50017 "Trafic Transporteur"
{

    fields
    {
        field(1; "Responsibility Center"; Code[10])
        {
            Caption = 'Responsibility Center';
            TableRelation = "Responsibility Center";
        }
        field(2; "Mode d'expédition"; Option)
        {
            Description = 'AD Le 05-03-2010 => GDI -> Défini le mode d''expédition (Normal, Express ...)';
            Editable = true;
            OptionMembers = Messagerie,Express,"Contre Rembourssement Normal","Contre Rembourssement Express","Express Samedi",Affretement,"Contre Rembourssement Samedi";

        }
        field(3; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";
        }
        field(4; "Country/Region Code"; Code[10])
        {
            Caption = 'Country/Region Code';
            TableRelation = "Country/Region";
        }
        field(100; "Code Transporteur Logiflux"; Code[10])
        {
        }
        field(101; "Code Prestation Logiflux"; Code[10])
        {
        }
    }

    keys
    {
        key(PKey1; "Responsibility Center", "Mode d'expédition", "Shipping Agent Code", "Country/Region Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

