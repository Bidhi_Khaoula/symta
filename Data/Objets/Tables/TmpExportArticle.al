table 50050 "Tmp Export Article"
{
    fields
    {
        field(1; "No."; Code[40])
        {
            Caption = 'No.';
            NotBlank = true;
        }
        field(2; "No. 2"; Code[40])
        {
            Caption = 'No. 2';
        }
        field(3; Description; Text[50])
        {
            Caption = 'Description';
        }
        field(5; "Description 2"; Text[50])
        {
            Caption = 'Description 2';
        }
        field(12; "Shelf No."; Code[10])
        {
            Caption = 'Shelf No.';
            Editable = false;
        }
        field(60206; "Libelle Marketing"; Text[50])
        {
        }
    }

    keys
    {
        key(PKey1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

