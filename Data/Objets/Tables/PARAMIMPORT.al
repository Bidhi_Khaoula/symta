table 50042 "PARAM IMPORT"
{

    fields
    {
        field(1; "Compte gene"; Code[20])
        {
            TableRelation = "G/L Account";
        }
        field(2; "code journal"; Code[10])
        {
            TableRelation = "Source Code";
        }
        field(3; operation; Code[10])
        {
        }
        field(4; "mode de reglement"; Code[10])
        {
        }
        field(5; bordereau; Code[20])
        {
            TableRelation = "Payment Class";
        }
    }

    keys
    {
        key(Key1; "Compte gene", "code journal", operation, "mode de reglement")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

