table 50070 "Teliae Standard Return"
{
    Caption = 'Teliae Standard Return';

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'N° séquence';
        }
        field(2; "Import No."; Integer)
        {
            Caption = 'N° import';
        }
        field(3; "Import Date"; Date)
        {
            Caption = 'Date d''import';
        }
        field(10; "Reference UM"; Text[35])
        {
        }
        field(20; "Info Colis UM"; Text[8])
        {
        }
        field(30; "Numero UM"; Text[3])
        {
        }
        field(40; "Poids UM"; Text[15])
        {
        }
        field(50; "URL Tracking UM"; Text[200])
        {
        }
        field(60; "Code Barre de tri (UM)"; Text[70])
        {
        }
        field(70; "Code Barre Client"; Text[70])
        {
        }
        field(80; "Code Barre Transporteur"; Text[64])
        {
        }
        field(90; "Numero Recepisse"; Text[8])
        {
        }
        field(100; "Numero BL"; Text[30])
        {
        }
        field(110; "URL Tracking Expedition"; Text[200])
        {
        }
        field(120; "Nom Expédition"; Text[35])
        {
        }
        field(130; "Code Transporteur"; Text[5])
        {
        }
        field(140; "Code Produit"; Text[5])
        {
        }
        field(150; "Date Expedition"; Date)
        {
        }
        field(160; "Numero Bordereau"; Text[10])
        {
        }
        field(170; "Date creation Bordereau"; DateTime)
        {
        }
        field(180; "Nom Compte Chargeur"; Text[35])
        {
        }
        field(190; "Reference Destinataire"; Text[35])
        {
        }
        field(200; "Code Tournee"; Text[35])
        {
        }
        field(210; "Code Preparateur"; Text[10])
        {
        }
    }

    keys
    {
        key(PKey1; "Entry No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

