table 50022 "Description Status commandes"
{

    fields
    {
        field(1; "Shipment Status"; Enum "Shipment Status")
        {
        }
        field(2; Status; Enum "Description Status")
        {

        }
        field(3; Description; Text[50])
        {
        }
    }

    keys
    {
        key(PKey1; Status, "Shipment Status")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

