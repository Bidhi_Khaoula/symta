table 50030 "Packing List"
{

    fields
    {
        field(1; "N° expedition magasin"; Code[20])
        {
        }
        field(2; "N° ligne"; Integer)
        {
        }
        field(3; "N° expedition enregistré"; Code[20])
        {
        }
        field(4; "N° Ligne origine"; Integer)
        {
        }
        field(5; "N° article"; Code[20])
        {
        }
        field(6; "Désignation"; Text[50])
        {
        }
        field(7; "Quantité à expédié"; Decimal)
        {
        }
        field(8; "Quantité Dans le colis"; Decimal)
        {
        }
        field(9; "Numero de Colis"; Code[13])
        {
        }
        field(10; "N° BL"; Code[20])
        {
        }
        field(11; "Shelf No."; Code[10])
        {
            Caption = 'N° emplacement';
        }
    }

    keys
    {
        key(PKey1; "N° expedition magasin", "N° ligne")
        {
            Clustered = true;
        }
        key(FKey2; "N° expedition magasin", "N° Ligne origine")
        {
            SumIndexFields = "Quantité Dans le colis";
        }
        key(FKey3; "N° BL", "Numero de Colis")
        {
        }
        key(FKey4; "N° expedition enregistré", "Numero de Colis")
        {
        }
        key(FKey5; "N° expedition magasin", "Numero de Colis")
        {
        }
        key(FKey6; "N° expedition magasin", "Shelf No.")
        {
        }
    }

    fieldgroups
    {
    }
}

