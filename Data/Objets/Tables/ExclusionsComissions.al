table 50005 "Exclusions Comissions"
{

    fields
    {
        field(1; "Salesperson Type"; Option)
        {
            Caption = 'Type Vendeur';
            OptionMembers = Tous,Vendeur;
        }
        field(2; "Salesperson Code"; Code[10])
        {
            Caption = 'Code Vendeur';
            TableRelation = IF ("Salesperson Type" = CONST(Vendeur)) "Salesperson/Purchaser".Code;
        }
        field(5; "Sales Type"; Option)
        {
            Caption = 'Type Vente';
            OptionMembers = Tous,Centrale,"Famille 1","Famille 2","Famille 3",Client;
        }
        field(6; "Sales Code"; Code[10])
        {
            Caption = 'Code Vente';
            TableRelation = IF ("Sales Type" = CONST(Centrale)) "Generals Parameters".Code
            ELSE
            IF ("Sales Type" = CONST("Famille 1")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'))
            ELSE
            IF ("Sales Type" = CONST("Famille 2")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'))
            ELSE
            IF ("Sales Type" = CONST("Famille 3")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'))
            ELSE
            IF ("Sales Type" = CONST(Client)) Customer."No.";
        }
        field(9; Type; Option)
        {
            Caption = 'Type';
            OptionMembers = Tous,Marque,Famille,"Sous Famille",Article;
        }
        field(10; "Code"; Code[10])
        {
            Caption = 'Code';
            TableRelation = IF (Type = CONST(Marque)) Manufacturer.Code
            ELSE
            IF (Type = CONST(Famille)) "Item Category".Code
            ELSE
            IF (Type = CONST("Sous Famille")) "Item Category".Code
            ELSE
            IF (Type = CONST(Article)) Item."No.";
        }
        field(100; "Comission %"; Decimal)
        {
            Caption = '% Comission';
        }
    }

    keys
    {
        key(PKey1; "Salesperson Type", "Salesperson Code", "Sales Type", "Sales Code", Type, "Code")
        {
            Clustered = true;
        }
        key(FKey2; "Comission %")
        {
        }
    }

    fieldgroups
    {
    }
}

