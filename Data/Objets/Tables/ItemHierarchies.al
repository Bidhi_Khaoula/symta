table 50064 "Item Hierarchies"
{

    fields
    {
        field(1; "Code Article"; Code[20])
        {
            TableRelation = Item."No.";
        }
        field(2; "Super Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER("Super family"));
        }
        field(3; "Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER(Family),
                                                            "Code Niveau supérieur" = FIELD("Super Famille Marketing"));

        }
        field(4; "Sous Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER("Sous family"),
                                                            "Code Niveau supérieur" = FIELD("Famille Marketing"));
        }
        field(5; "By Default"; Boolean)
        {
            Caption = 'Hiérarchie pirncipale';
        }
    }

    keys
    {
        key(PKey1; "Code Article", "Super Famille Marketing", "Famille Marketing", "Sous Famille Marketing")
        {
            Clustered = true;
        }
        key(FKey2; "By Default")
        {
        }
    }

    fieldgroups
    {
    }
}

