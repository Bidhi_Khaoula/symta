table 50059 "Famille Marketing"
{

    fields
    {
        field(1; "Code"; Code[10])
        {
        }
        field(2; Description; Text[80])
        {
            Description = 'ANI Le 18-05-2016 FE20160427 passage à 80 caract.';
        }
        field(3; "Code Niveau supérieur"; Code[10])
        {
            TableRelation = IF (Type = CONST(Family)) "Famille Marketing".Code WHERE(Type = FILTER("Super family"))
            ELSE
            IF (Type = CONST("Sous family")) "Famille Marketing".Code WHERE(Type = FILTER(Family));
        }
        field(4; Type; Option)
        {
            OptionMembers = "Super family",Family,"Sous family";
        }
        field(5; Priority; Integer)
        {
            Caption = 'Priority';
            Description = 'ANI Le 18-05-2016 FE20160427 nouveau champ';
        }
    }

    keys
    {
        key(PKey1; "Code Niveau supérieur", Type, "Code")
        {
            Clustered = true;
        }
        key(FKey2; Priority, "Code Niveau supérieur", Type, "Code")
        {
        }
    }

    fieldgroups
    {
    }
}

