table 60006 "Packing List Header"
{

    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'N°';
        }
        field(5; "Header Status"; Option)
        {
            Caption = 'Statut';
            OptionMembers = Ouvert,"Lancé","Enregistré","Bloqué";
        }
        field(10; "Creation User"; Text[250])
        {
            Caption = 'Utilisateur création';
        }
        field(11; "Creation Date"; Date)
        {
            Caption = 'Date création';
        }
        field(12; "Creation Time"; Time)
        {
            Caption = 'Heure création';
        }
        field(23; "Calculate Header Weight"; Decimal)
        {
            CalcFormula = Sum("Packing List Package"."Package Weight" WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Poids brut total saisi';
            DecimalPlaces = 0 : 5;
            Description = 'SFD20201005';
            Editable = false;
            FieldClass = FlowField;
            MinValue = 0;
        }
        field(24; "Net Header Weight"; Decimal)
        {
            CalcFormula = Sum("Packing List Package"."Net Weight" WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Poids net total saisi';
            Editable = false;
            FieldClass = FlowField;
            MinValue = 0;
        }
        field(25; "Total Volume"; Decimal)
        {
            CalcFormula = Sum("Packing List Package".Volume WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Volume total';
            Editable = false;
            FieldClass = FlowField;
            MinValue = 0;
        }
        field(26; "Package Number"; Integer)
        {
            CalcFormula = Count("Packing List Package" WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Nombre colis';
            Editable = false;
            FieldClass = FlowField;
            MinValue = 0;
        }
        field(40; "Whse Shipment Header Quantity"; Integer)
        {
            CalcFormula = Count("Warehouse Shipment Header" WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Nombre d''expédition entrepôt';
            Editable = false;
            FieldClass = FlowField;
        }
        field(100; "First Posted Shipment No."; Code[20])
        {
            CalcFormula = Lookup("Packing List Line"."Posted Source No." WHERE("Packing List No." = FIELD("No."),
                                                                                "Posted Source Document" = CONST("Posted Shipment"),
                                                                                "Posted Source No." = FILTER(<> '')));
            Caption = 'Premier N° origine enregistré';
            Editable = false;
            FieldClass = FlowField;
        }
        field(110; "Sell-to Customer No."; Code[20])
        {
            Caption = 'Sell-to Customer No.';
            Editable = false;
            TableRelation = Customer;
        }
        field(50000; Comment; Text[250])
        {
            Caption = 'Comment';
            Description = 'SFD20201005';
        }
        field(50001; "Theoretical Weight"; Decimal)
        {
            CalcFormula = Sum("Packing List Line"."Line Weight" WHERE("Packing List No." = FIELD("No.")));
            Caption = 'Theoretical Weight';
            DecimalPlaces = 0 : 5;
            Description = 'SFD20201005';
            Editable = false;
            FieldClass = FlowField;
            MinValue = 0;
        }
    }

    keys
    {
        key(PKey1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; "No.", "Sell-to Customer No.", "Header Status")
        {
        }
    }
}

