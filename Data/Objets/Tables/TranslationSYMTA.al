table 50069 "Translation SYMTA"
{

    fields
    {
        field(1; Record_ID; RecordID)
        {
            Caption = 'Identifiant de l''enregistrement';
            Description = 'Permet d''identifier la table + clé de l''enregistrement';
            Editable = false;
        }
        field(2; "Language Code"; Code[10])
        {
            Caption = 'Language Code';
            NotBlank = true;
            TableRelation = Language;
        }
        field(3; Description; Text[250])
        {
            Caption = 'Traduction';
            Description = 'ANI Le 13-02-2015 => 250 caractères';
        }
        field(4; "Description 2"; Text[250])
        {
            Caption = 'Description 2';
            Description = 'ANI Le 13-02-2015 => 250 caractères';
        }
        field(5; "Table No"; Integer)
        {
            Caption = 'N° Table';
            Editable = false;
        }
        field(6; "Champ table"; Integer)
        {
            Caption = 'N° Field';
            trigger OnLookup()
            var
                "Field": Record Field;
                F_Field: Page "Champs table";
            begin
                CLEAR(F_Field);
                Field.RESET();
                Field.SETRANGE(TableNo, "Table No");
                Field.SETRANGE(Class, Field.Class::Normal);
                F_Field.SETTABLEVIEW(Field);
                F_Field.LOOKUPMODE := TRUE;
                IF F_Field.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                    F_Field.Recup_Filtre(Field);
                    Field.MARKEDONLY(TRUE);
                    IF Field.FindSet() THEN
                        REPEAT
                            IF "Champ table" <> 0 THEN
                                IF NOT MODIFY() THEN INSERT();
                            "Champ table" := Field."No.";
                            Nom := Field.FieldName;
                            Caption := Field."Field Caption";
                        UNTIL Field.NEXT() = 0
                    ELSE BEGIN
                        F_Field.GETRECORD(Field);
                        "Champ table" := Field."No.";
                        Nom := Field.FieldName;
                        Caption := Field."Field Caption";
                    END;
                END
            end;
        }
        field(7; Nom; Text[60])
        {
            Caption = 'Field Name';
        }
        field(8; Caption; Text[60])
        {
            Caption = 'Field Label';
        }
    }

    keys
    {
        key(PKey1; Record_ID, "Table No", "Language Code", "Champ table")
        {
            Clustered = true;
        }
    }
}

