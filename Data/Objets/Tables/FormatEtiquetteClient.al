table 50004 "Format Etiquette Client"
{
    fields
    {
        field(1; "Format Code"; Code[10])
        {
            Caption = 'Code Format';
            Description = '// Code format étiqette';
        }
        field(10; Descrption; Text[30])
        {
            Caption = 'Designation';
            Description = '// Nom du format';
        }
        field(100; "Imprimer N° Article"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(101; "Imprimer Gencode"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(102; "Imprimer Référence Externe"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(103; "Imprimer Designatio,"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(104; "Imprimer Colisage"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(105; "Imprimer Zone Libre 1"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(106; "Imprimer Zone Libre 2"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(107; "Imprimer Zone Libre 3"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(108; "Imprimer Zone Libre 4"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(109; "Imprimer Nom Client"; Option)
        {
            OptionMembers = Non,Oui,"Inversé";
        }
        field(500; "Repertoire Etiquette"; Text[250])
        {
        }
    }

    keys
    {
        key(PKey1; "Format Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

