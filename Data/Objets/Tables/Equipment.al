table 50060 Equipment
{
    fields
    {
        field(1; "Equipment No."; Integer)
        {
            AutoIncrement = true;
            Caption = 'Equipment No.';
            Editable = false;
        }
        field(10; Manufacturer; Code[20])
        {
            Caption = 'Brand';
            TableRelation = Manufacturer;
        }
        field(11; Model; Code[20])
        {
            Caption = 'Model';
        }
        field(12; "Equipment Type"; Code[20])
        {
            Caption = 'Equipment Type';
        }
        field(13; Options; Text[250])
        {
            Caption = 'Options';
            Description = 'CFR le 09/12/2021 - FA20210912';
        }
    }

    keys
    {
        key(PKey1; "Equipment No.")
        {
            Clustered = true;
        }
        key(FKey2; Manufacturer)
        {
        }
        key(FKey3; Model)
        {
        }
        key(FKey4; "Equipment Type")
        {
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; "Equipment No.", Manufacturer, Model, "Equipment Type")
        {
        }
    }
}

