table 50055 "Traduction Critere"
{
    fields
    {
        field(1; "Code Critere"; Code[20])
        {
            TableRelation = Critere.Code;
        }
        field(2; "Code Langue"; Code[20])
        {
            TableRelation = Language.Code;
        }
        field(3; Nom; Text[30])
        {
        }
    }

    keys
    {
        key(PKey1; "Code Critere", "Code Langue")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

