table 50032 "Groupement/Marque"
{
    fields
    {
        field(1; Type; Option)
        {
            OptionMembers = Marque,Autre;
        }
        field(2; "Code client"; Code[20])
        {
            TableRelation = Customer;
        }
        field(3; "Code Marque"; Code[10])
        {
            TableRelation = Manufacturer.Code;
        }
    }

    keys
    {
        key(PKey1; Type, "Code client", "Code Marque")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

