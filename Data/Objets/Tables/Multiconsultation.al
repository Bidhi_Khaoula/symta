table 50054 "Multi consultation"
{
    fields
    {
        field(1; "Code"; Code[50])
        {
            Description = '// CFR le 10/03/2021 - Régie : Links vers ceux de la fiche article';
        }
        field(2; "Customer No"; Code[20])
        {
            TableRelation = Customer;
        }
        field(3; "Item No"; Code[20])
        {
            TableRelation = Item;
        }
        field(4; "Location Code"; Code[20])
        {
            TableRelation = Location;
        }
        field(5; "Vendor No"; Code[20])
        {
            TableRelation = Vendor;
        }
    }

    keys
    {
        key(PKey1; "Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

