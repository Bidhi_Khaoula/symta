table 50018 "Ventes Manuelles"
{

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'Entry No.';
        }
        field(2; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            TableRelation = Item;
        }
        field(3; "Posting Date"; Date)
        {
            Caption = 'Posting Date';
        }
        field(4; "Item Ledger Entry Type"; Option)
        {
            Caption = 'Item Ledger Entry Type';
            OptionCaption = 'Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ';
            OptionMembers = Purchase,Sale,"Positive Adjmt.","Negative Adjmt.",Transfer,Consumption,Output," ";
        }
        field(5; "Source No."; Code[20])
        {
            Caption = 'Source No.';
            TableRelation = Customer;
        }
        field(6; "Document No."; Code[20])
        {
            Caption = 'Document No.';
        }
        field(7; Description; Text[50])
        {
            Caption = 'Description';
        }
        field(8; "Location Code"; Code[10])
        {
            Caption = 'Location Code';
            TableRelation = Location;
        }
        field(12; "Valued Quantity"; Decimal)
        {
            Caption = 'Valued Quantity';
            DecimalPlaces = 0 : 5;
        }
        field(1000; "Utilisateur Création"; Code[20])
        {
            Editable = false;
        }
        field(1001; "Date Création"; Date)
        {
            Editable = false;
        }
        field(1002; "Heure Création"; Time)
        {
        }
    }

    keys
    {
        key(PKey1; "Entry No.")
        {
            Clustered = true;
        }
        key(FKey2; "Item No.", "Posting Date", "Item Ledger Entry Type", "Location Code")
        {
            SumIndexFields = "Valued Quantity";
        }
    }

    fieldgroups
    {
    }

}

