table 50019 "Incident transport"
{

    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'No.';
        }
        field(10; "Document Date"; Date)
        {
            Caption = 'Document Date';
        }
        field(20; "Order No."; Code[20])
        {
            Caption = 'Order No.';
            TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Order));
        }
        field(21; "Sales Shipment No."; Code[20])
        {
            Caption = 'N° expédition vente';
            TableRelation = IF ("Order No." = FILTER(= ''),
                                "Customer No." = FILTER(= '')) "Sales Shipment Header"."No."
            ELSE
            IF ("Order No." = FILTER(<> ''),
                                         "Customer No." = FILTER(= '')) "Sales Shipment Header"."No." WHERE("Order No." = FIELD("Order No."))
            ELSE
            IF ("Order No." = FILTER(= ''),
                                                  "Customer No." = FILTER(<> '')) "Sales Shipment Header"."No." WHERE("Sell-to Customer No." = FIELD("Customer No."))
            ELSE
            IF ("Order No." = FILTER(<> ''),
                                                           "Customer No." = FILTER(<> '')) "Sales Shipment Header"."No." WHERE("Sell-to Customer No." = FIELD("Customer No."),
                                                                                                                         "Order No." = FIELD("Order No."));

        }
        field(25; "External Document No."; Code[35])
        {
            Caption = 'External Document No.';
        }
        field(29; "Customer No."; Code[20])
        {
            Caption = 'Sell-to Customer No.';
            NotBlank = true;
            TableRelation = Customer;
        }
        field(30; "Contact No."; Code[20])
        {
            Caption = 'Sell-to Contact No.';
            TableRelation = Contact;
            trigger OnLookup()
            var
                LCont: Record Contact;
                LContBusinessRelation: Record "Contact Business Relation";
            begin
                IF "Customer No." <> '' THEN
                    IF LCont.GET("Contact No.") THEN
                        LCont.SETRANGE("Company No.", LCont."Company No.")
                    ELSE BEGIN
                        LContBusinessRelation.RESET();
                        LContBusinessRelation.SETCURRENTKEY("Link to Table", "No.");
                        LContBusinessRelation.SETRANGE("Link to Table", LContBusinessRelation."Link to Table"::Customer);
                        LContBusinessRelation.SETRANGE("No.", "Customer No.");
                        IF LContBusinessRelation.FINDFIRST() THEN
                            LCont.SETRANGE("Company No.", LContBusinessRelation."Contact No.")
                        ELSE
                            LCont.SETRANGE("No.", '');
                    END;

                IF "Contact No." <> '' THEN
                    IF LCont.GET("Contact No.") THEN;
                IF PAGE.RUNMODAL(0, LCont) = ACTION::LookupOK THEN BEGIN
                    xRec := Rec;
                    VALIDATE("Contact No.", LCont."No.");
                END;
            end;
        }
        field(31; Contact; Text[50])
        {
            Caption = 'Contact';
        }
        field(33; "Phone No."; Text[30])
        {
            Caption = 'Téléphone';
        }
        field(50; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";
        }
        field(100; Status; Option)
        {
            Caption = 'Statut';
            Editable = false;
            OptionMembers = Ouvert,"Créé","Traité","Cloturé";
        }
        field(109; "No. Series"; Code[10])
        {
            Caption = 'No. Series';
            Editable = false;
            TableRelation = "No. Series";
        }
        field(200; "Commentaire Incident"; Text[80])
        {
        }
        field(210; "Commentaire Traitement"; Text[80])
        {
        }
        field(50800; "Create User ID"; Code[20])
        {
            Caption = 'Code utilisateur Création';
            Editable = false;
        }
        field(50801; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Editable = false;
        }
        field(50802; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Editable = false;
        }
        field(50810; "Traité par"; Code[20])
        {
            Editable = false;
        }
        field(50811; "Traité le"; Date)
        {
            Editable = false;
        }
        field(50812; "Traité à"; Time)
        {
            Editable = false;
        }
        field(50815; "Cloturé par"; Code[20])
        {
            Editable = false;
        }
        field(50816; "Cloturé le"; Date)
        {
            Editable = false;
        }
        field(50817; "Cloturé à"; Time)
        {
            Editable = false;
        }
        field(50900; "Customer Name"; Text[100])
        {
            CalcFormula = Lookup(Customer.Name WHERE("No." = FIELD("Customer No.")));
            Caption = 'Customer Name';
            Description = 'MCO Le 31-03-2015 => Régie';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(Key1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

