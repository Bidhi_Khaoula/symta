table 50002 "Ligne import"
{

    fields
    {
        field(1; num_ecriture; Integer)
        {
        }
        field(2; n_ordre; Integer)
        {
        }
        field(3; cpt; Code[20])
        {
        }
        field(4; type_cpt; Code[1])
        {
        }
        field(5; lib_cpt; Text[70])
        {
        }
        field(6; lib_ecr; Text[70])
        {
        }
        field(7; sens; Code[10])
        {
        }
        field(8; mont; Decimal)
        {
        }
        field(9; euro; Text[30])
        {
        }
        field(10; mt_euro; Decimal)
        {
        }
        field(11; journ; Code[10])
        {
        }
    }

    keys
    {
        key(PKey1; journ, num_ecriture, n_ordre)
        {
            Clustered = true;
        }
        key(Key2; sens)
        {
            SumIndexFields = mont;
        }
        key(Key3; journ, lib_ecr, num_ecriture)
        {
        }
    }

    fieldgroups
    {
    }
}

