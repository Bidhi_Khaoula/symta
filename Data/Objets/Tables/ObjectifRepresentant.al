table 50003 "Objectif Representant"
{

    fields
    {
        field(1; "Salesperson Code"; Code[10])
        {
            Caption = 'Salesperson Code';
            TableRelation = "Salesperson/Purchaser";
        }
        field(2; "Cust. Country/Region Code"; Code[10])
        {
            Caption = 'Country/Region Code';
            TableRelation = "Country/Region";
        }
        field(3; "Cust. Family Code 1"; Code[10])
        {
            Caption = 'Code Famille 1 Client';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'));
        }
        field(4; "Item Manufacturer Code"; Code[10])
        {
            Caption = 'Manufacturer Code';
            TableRelation = Manufacturer;
        }
        field(5; "Item Category Code"; Code[10])
        {
            Caption = 'Item Category Code';
            TableRelation = "Item Category";
        }
        field(6; "Direction Code"; Code[10])
        {
            Caption = 'Code Direction';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_DIRECTION'));
        }
        field(10; "Date référence"; Date)
        {
        }
        field(100; "Objectif CA"; Decimal)
        {
            Caption = 'Objectif CA';
        }
        field(101; "Objectif CA Promo"; Decimal)
        {
            Caption = 'Objectif CA Promo';
        }
        field(110; "Objectif Marge"; Decimal)
        {
        }
        field(111; "Objectif Marge Promo"; Decimal)
        {
        }
    }

    keys
    {
        key(PKey1; "Date référence", "Salesperson Code", "Cust. Country/Region Code", "Cust. Family Code 1", "Item Manufacturer Code", "Direction Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

