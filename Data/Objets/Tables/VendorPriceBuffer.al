table 50020 "Vendor Price Buffer"
{
    fields
    {
        field(1; "Worksheet Template Name"; Code[10])
        {
            Caption = 'Worksheet Template Name';
        }
        field(2; "Journal Batch Name"; Code[10])
        {
            Caption = 'Journal Batch Name';
        }
        field(3; "Line No."; Integer)
        {
            Caption = 'Line No.';
        }
        field(4; "Item No."; Code[20])
        {
            Caption = 'No.';
            TableRelation = Item;
        }
        field(5; Description; Text[50])
        {
            Caption = 'Description';
        }
        field(6; "Description 2"; Text[50])
        {
            Caption = 'Description 2';
        }
        field(7; "Vendor No."; Code[20])
        {
            Caption = 'Vendor No.';
            TableRelation = Vendor;
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(8; Name; Text[50])
        {
            Caption = 'Nom';
        }
        field(9; "Unit Price"; Decimal)
        {
            Caption = 'Prix Brut';
        }
        field(10; Discount; Decimal)
        {
            Caption = '% Remise';
        }
        field(11; "Net Unit Price"; Decimal)
        {
            Caption = 'Prix Net';
        }
        field(12; "Profit %"; Decimal)
        {
            Caption = '% de Marge';
        }
        field(13; "Minimum order"; Decimal)
        {
            Caption = 'Minimum de commande';
        }
        field(14; "By How Many"; Decimal)
        {
            Caption = 'PCB';
        }
        field(15; Quantity; Decimal)
        {
            Caption = 'Quantité';
        }
        field(16; Selected; Boolean)
        {
            Caption = 'Séléctionner';
        }
        field(30; "Meilleur Prix Vente"; Decimal)
        {
            Description = 'AD Le 27-01-2012 => SYMTA ->';
            Editable = false;
        }
        field(50; "Date début"; Date)
        {
            Description = 'AD Le 16-04-2012 => SYMTA ->';
        }
        field(51; "Date Fin"; Date)
        {
            Description = 'AD Le 01-04-2015 => SYMTA ->';
        }
        field(50028; "Indirect Cost"; Decimal)
        {
            CalcFormula = Max("Item Vendor"."Indirect Cost" WHERE("Item No." = FIELD("Item No."),
                                                                     "Vendor No." = FIELD("Vendor No.")));
            Caption = 'Indirect Cost %';
            DecimalPlaces = 0 : 5;
            Description = 'CFR le 14/09/2021 => Régie ajout champ';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50060; "Statut Qualité"; Option)
        {
            Description = 'AD Le 31-05-2011 => SYMTA ->';
            Editable = false;
            OptionMembers = Conforme,"Non Conforme","A contrôler","Contrôle en cours";
        }
        field(50061; "Statut Approvisionnement"; Option)
        {
            Description = 'AD Le 31-05-2011 => SYMTA ->';
            Editable = false;
            OptionMembers = "Non Bloqué","Bloqué","Plus Fourni";
        }
        field(50062; "Référence Fournisseur"; Code[30])
        {
            Description = 'AD Le 27-01-2012 => SYMTA ->';
            Editable = false;
        }
        field(50063; "Code Remise"; Code[10])
        {
            Description = 'AD Le 27-01-2012 => SYMTA ->';
            Editable = false;
        }
        field(50064; "Unité d'achat"; Code[10])
        {
            Description = 'AD Le 27-01-2012 => SYMTA ->';
            Editable = false;
        }
        field(50065; "Minimum Qty"; Decimal)
        {
            Description = 'MCO Le 03-10-2018';
        }
        field(50066; "Vendor Item Desciption"; Text[50])
        {
            Caption = 'Vendor Item No.';
            Description = 'AD Le 30-01-2020 => REGIE ->';
            Editable = false;
        }
    }

    keys
    {
        key(PKey1; "Worksheet Template Name", "Journal Batch Name", "Line No.", "Vendor No.", "Date début", "Minimum Qty")
        {
            Clustered = true;
        }
        key(FKey2; "Worksheet Template Name", "Journal Batch Name", "Line No.", "Net Unit Price")
        {
        }
        key(FKey3; "Minimum Qty")
        {

        }
    }

    fieldgroups
    {
    }
}

