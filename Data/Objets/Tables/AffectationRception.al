table 50063 "Affectation Réception"
{

    fields
    {
        field(1; "Receipt No."; Code[20])
        {
            Caption = 'N° Réception';
            Editable = false;
            TableRelation = "Warehouse Receipt Line"."No.";
        }
        field(2; "Receipt Line No."; Integer)
        {
            Caption = 'N° Ligne réception';
            Editable = false;
        }
        field(3; "Sales Order No."; Code[20])
        {
            Caption = 'N° Commande vente';
            Editable = false;
            TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Order));
        }
        field(4; "Sales Order Line No."; Integer)
        {
            Caption = 'N° Ligne vente';
            Editable = false;
        }
        field(20; "Item No."; Code[20])
        {
            Caption = 'N° Article';
            Editable = false;
            TableRelation = Item;
        }
        field(40; "Vendor No."; Code[20])
        {
            Caption = 'N° fournisseur';
            Editable = false;
            TableRelation = Vendor;
        }
        field(50; Urgence; Boolean)
        {
        }
        field(51; "No Cde Client"; Text[250])
        {
        }
        field(80; Quantity; Decimal)
        {
            Caption = 'Quantité';
        }
        field(81; "Quantity To Receive"; Decimal)
        {
            Caption = 'Quantité à recevoir';
        }
        field(100; Closed; Boolean)
        {
            Caption = 'Traité';
        }
        field(1000; "N° Article 2"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            FieldClass = FlowField;
        }
        field(1001; "Nom Fournisseur"; Text[100])
        {
            CalcFormula = Lookup(Vendor.Name WHERE("No." = FIELD("Vendor No.")));
            FieldClass = FlowField;
        }
        field(1002; "N° Donneur d'ordre"; Code[20])
        {
            CalcFormula = Lookup("Sales Header"."Sell-to Customer No." WHERE("Document Type" = CONST(Order),
                                                                              "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
        }
        field(1003; "Nom du destinataire"; Text[100])
        {
            CalcFormula = Lookup("Sales Header"."Ship-to Name" WHERE("Document Type" = CONST(Order),
                                                                      "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
        }
        field(1004; "Date commande"; Date)
        {
            CalcFormula = Lookup("Sales Header"."Order Date" WHERE("Document Type" = CONST(Order),
                                                                    "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
        }
        field(1005; "Quantité restante"; Decimal)
        {
            CalcFormula = Lookup("Sales Line"."Outstanding Quantity" WHERE("Document Type" = CONST(Order),
                                                                            "Document No." = FIELD("Sales Order No."),
                                                                            "Line No." = FIELD("Sales Order Line No.")));
            FieldClass = FlowField;
        }
        field(1006; "Date livraison demandée"; Date)
        {
            CalcFormula = Lookup("Sales Line"."Requested Delivery Date" WHERE("Document Type" = CONST(Order),
                                                                               "Document No." = FIELD("Sales Order No."),
                                                                               "Line No." = FIELD("Sales Order Line No.")));
            FieldClass = FlowField;
        }
        field(1007; "Code utilisateur création"; Code[50])
        {
            CalcFormula = Lookup("Sales Header"."Create User ID" WHERE("Document Type" = CONST(Order),
                                                                        "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
        }
        field(1008; "Type de commande"; Option)
        {
            CalcFormula = Lookup("Sales Header"."Type de commande" WHERE("Document Type" = CONST(Order),
                                                                          "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
            OptionMembers = "Dépannage","Réappro",Stock;
        }
        field(1009; "N° Doc externe"; Text[50])
        {
            CalcFormula = Lookup("Sales Header"."External Document No." WHERE("Document Type" = CONST(Order),
                                                                               "No." = FIELD("Sales Order No.")));
            FieldClass = FlowField;
        }
        field(1010; "% remise ligne"; Decimal)
        {
            CalcFormula = Lookup("Sales Line"."Line Discount %" WHERE("Document Type" = CONST(Order),
                                                                       "Document No." = FIELD("Sales Order No."),
                                                                       "Line No." = FIELD("Sales Order Line No.")));
            FieldClass = FlowField;
        }
        field(1011; Designation; Text[100])
        {
            CalcFormula = Lookup(Item.Description WHERE("No." = FIELD("Item No.")));
            FieldClass = FlowField;
        }
        field(1012; "Designation 2"; Text[50])
        {
            CalcFormula = Lookup(Item."Description 2" WHERE("No." = FIELD("Item No.")));
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(PKey1; "Receipt No.", "Receipt Line No.", "Sales Order No.", "Sales Order Line No.")
        {
            Clustered = true;
        }
        key(FKey2; Closed)
        {
        }
    }

    fieldgroups
    {
    }
}

