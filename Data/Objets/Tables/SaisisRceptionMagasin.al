table 50021 "Saisis Réception Magasin"
{
    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'No.';
            Editable = false;
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No.';
            Editable = false;
        }
        field(12; "Bin Code"; Code[20])
        {
            Caption = 'Bin Code';
        }
        field(14; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            Editable = false;
            TableRelation = Item;
        }
        field(15; Quantity; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Editable = false;
        }
        field(16; "Utilisateur Modif"; Code[20])
        {
        }
        field(17; "Date Modif"; Date)
        {
        }
        field(18; "Heure Modif"; Time)
        {
        }
        field(20; Commentaire; Text[80])
        {
            Description = 'MCO Le 08-06-2017 => Régie';
        }
    }

    keys
    {
        key(PKey1; "No.", "Line No.")
        {
            Clustered = true;
        }
        key(FKey2; "No.", "Item No.")
        {
            SumIndexFields = Quantity;
        }
    }

    fieldgroups
    {
    }
}

