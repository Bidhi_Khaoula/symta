table 50078 "Shipping Setup"
{
    fields
    {
        field(1; "Code"; Code[20])
        {
        }
        field(2; "Series Nos."; Code[20])
        {
        }
        field(3; "Compte Frais douane"; Code[10])
        {
            TableRelation = "G/L Account";
        }
        field(4; "Compte Frais annexe"; Code[10])
        {
            TableRelation = "G/L Account";
        }
        field(5; "Compte Frais d'emballage"; Code[10])
        {
            TableRelation = "G/L Account";
        }
        field(6; "Compte Cout de transport"; Code[10])
        {
            TableRelation = "G/L Account";
        }
    }

    keys
    {
        key(PKey1; "Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

