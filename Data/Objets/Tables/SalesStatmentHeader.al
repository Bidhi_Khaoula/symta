table 50024 "Sales Statment Header"
{
    Caption = 'Relevé client';
    Permissions = TableData "Cust. Ledger Entry" = rm;

    fields
    {
        field(1; "Statement No."; Code[20])
        {
            Caption = 'N° Relevé';
        }
        field(2; "Customer No."; Code[10])
        {
            Caption = 'N° Client';
            TableRelation = Customer;
        }
        field(10; "Statement Date"; Date)
        {
            Caption = 'Date relevé';
        }
        field(11; "Due Date"; Date)
        {
            Caption = 'Date d''échéance';
        }
        field(14; "Payment Method Code"; Code[10])
        {
            Caption = 'Payment Method Code';
            Editable = false;
            TableRelation = "Payment Method";
        }
        field(20; "No. Printed"; Integer)
        {
        }
        field(30; "Imprimer Relevé"; Boolean)
        {
            Editable = false;
        }
        field(100; "Statement Amount"; Decimal)
        {
            Caption = 'Montant relevé';
        }
        field(120; "Nb Ecritures"; Integer)
        {
            CalcFormula = Count("Cust. Ledger Entry" WHERE("Statement No." = FIELD("Statement No."),
                                                            "Customer No." = FIELD("Customer No.")));
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(PKey1; "Statement No.", "Customer No.")
        {
            Clustered = true;
        }
        key(FKey2; "Customer No.", "Statement No.")
        {
        }
    }

    fieldgroups
    {
    }
}

