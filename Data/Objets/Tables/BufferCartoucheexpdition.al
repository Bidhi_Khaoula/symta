table 50012 "Buffer Cartouche expédition"
{
    fields
    {
        field(1; "Shipping No."; Code[20])
        {
            Caption = 'N° expédition';
        }
        field(2; "No."; Code[20])
        {
            Caption = 'N°';
        }
        field(100; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";
        }
        field(101; "Shipping Agent Service Code"; Code[10])
        {
            Caption = 'Shipping Agent Service Code';
            TableRelation = "Shipping Agent Services".Code WHERE("Shipping Agent Code" = FIELD("Shipping Agent Code"));
        }
        field(200; "User Code"; Code[20])
        {
            Caption = 'Code Utilisateur';
            Description = 'AD Le 11-04-2007 => Pour le suivi des commandes';
            Editable = false;
            TableRelation = User;
        }
        field(290; "Shipment Code"; Code[20])
        {
            Caption = 'Code Port';
            TableRelation = "G/L Account"."No.";
        }
        field(300; Weight; Decimal)
        {
            Caption = 'Poids';
        }
        field(310; "Shipment Cost"; Decimal)
        {
            Caption = 'Cout Port';
        }
        field(311; "Shipment Amount"; Decimal)
        {
            Caption = 'Montant Port';
        }
        field(312; "Shipment in order"; Boolean)
        {
            Caption = 'Port en commande';
        }
        field(410; "Packing Code"; Code[20])
        {
            Caption = 'Code Emballage';
        }
        field(411; "Packing Cost"; Decimal)
        {
            Caption = 'Cout Emballage';
        }
        field(412; "Packing Amount"; Decimal)
        {
            Caption = 'Montant Emballage';
        }
        field(413; "Packing in order"; Boolean)
        {
            Caption = 'Emballage en commande';
        }
    }

    keys
    {
        key(PKey1; "Shipping No.", "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

