table 50027 "EDI Orders Adress"
{

    fields
    {
        field(1; "EDI Document Type"; Code[20])
        {
            Caption = 'Type Document EDI';
        }
        field(2; "EDI Document No"; Code[25])
        {
            Caption = 'N° Document EDI';
        }
        field(3; "EDI Adress Type"; Code[10])
        {
            Caption = 'Type Adresse EDI';
        }
        field(10; Name; Text[30])
        {
            Caption = 'Name';
        }
        field(11; "Name 2"; Text[30])
        {
            Caption = 'Name 2';
        }
        field(13; Contact; Text[30])
        {
            Caption = 'Contact';
        }
        field(15; Address; Text[30])
        {
            Caption = 'Address';
        }
        field(16; "Address 2"; Text[30])
        {
            Caption = 'Address 2';
        }
        field(17; City; Text[30])
        {
            Caption = 'City';
        }
        field(18; "Post Code"; Code[20])
        {
            Caption = 'Post Code';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(20; County; Text[30])
        {
            Caption = 'County';
        }
        field(30; "Phone No."; Text[30])
        {
            Caption = 'Phone No.';
        }
        field(31; "Fax No."; Text[30])
        {
            Caption = 'Fax No.';
        }
        field(32; "E-Mail"; Text[80])
        {
            Caption = 'E-Mail';
        }
        field(35; "Country Code"; Code[10])
        {
            Caption = 'Country Code';
            TableRelation = "Country/Region";
        }
    }

    keys
    {
        key(PKey1; "EDI Document Type", "EDI Document No", "EDI Adress Type")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

