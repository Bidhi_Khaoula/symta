table 50029 "TEMP Import tarifs Ventes"
{

    fields
    {
        field(1; Excel_CArticle; Text[20])
        {
        }
        field(2; Excel_TypeCommande; Text[30])
        {
        }
        field(3; Excel_TypeTarif; Text[30])
        {
        }
        field(4; Excel_CTarif; Text[10])
        {
        }
        field(5; Excel_QteMini; Text[10])
        {
        }
        field(6; Excel_Prix; Text[20])
        {
        }
        field(7; Excel_CoefCatalogue; Text[30])
        {
        }
        field(8; Excel_PrixCatalogue; Text[30])
        {
        }
        field(9; Excel_DateDebut; Text[10])
        {
        }
        field(10; Excel_PrixNet; Text[1])
        {
        }
    }

    keys
    {
        key(PKey1; Excel_CArticle, Excel_TypeCommande, Excel_TypeTarif, Excel_CTarif, Excel_QteMini)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

