table 50023 "Interdiction Vente"
{

    fields
    {
        field(1; "Type Article"; Option)
        {
            OptionCaption = 'Tous,Marque,Famille,Sous Famille,Article';
            OptionMembers = Tous,Marque,Famille,"Sous Famille",Article;
        }
        field(2; "Code Article"; Code[20])
        {
            TableRelation = IF ("Type Article" = CONST(Marque)) Manufacturer.Code
            ELSE
            IF ("Type Article" = CONST(Famille)) "Item Category".Code
            ELSE
            IF ("Type Article" = CONST("Sous Famille")) "Item Category".Code
            ELSE
            IF ("Type Article" = CONST(Article)) Item."No.";
        }
        field(3; "Type Vente"; Option)
        {
            OptionCaption = 'Tous,Famille 1,Famille 2,Famille 3,Client';
            OptionMembers = Tous,"Famille 1","Famille 2","Famille 3",Client;
        }
        field(4; "Code Vente"; Code[20])
        {
            TableRelation = IF ("Type Vente" = CONST("Famille 1")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'))
            ELSE
            IF ("Type Vente" = CONST("Famille 2")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'))
            ELSE
            IF ("Type Vente" = CONST("Famille 3")) "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'))
            ELSE
            IF ("Type Vente" = CONST(Client)) Customer."No.";
        }
        field(5; Statut; Option)
        {
            OptionCaption = 'Autorisation,Interdiction';
            OptionMembers = Autorisation,Interdiction;
        }
        field(6; "Date Debut"; Date)
        {
        }
        field(7; "Date Fin"; Date)
        {
        }
    }

    keys
    {
        key(PKey1; "Type Article", "Code Article", "Type Vente", "Code Vente", Statut, "Date Debut", "Date Fin")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

