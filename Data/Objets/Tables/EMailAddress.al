table 50074 "E-Mail Address"
{
    fields
    {
        field(1; "Entry No."; Integer)
        {
            AutoIncrement = true;
        }
        field(5; "Entry Type"; Option)
        {
            Caption = 'Type d''adresse';
            Editable = false;
            OptionCaption = '" ,Client facturation"';
            OptionMembers = " ","Client facturation";
        }
        field(10; "Customer No."; Code[20])
        {
            Caption = 'No.';
            Editable = false;
            TableRelation = Customer;
        }
        field(20; "E-Mail"; Text[100])
        {
        }
    }

    keys
    {
        key(PKey1; "Entry No.")
        {
            Clustered = true;
        }
        key(FKey2; "Entry Type", "Customer No.")
        {
        }
    }

    fieldgroups
    {
    }
}

