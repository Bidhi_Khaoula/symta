table 50047 "Affectation Ligne Achat"
{

    fields
    {
        field(1; "Purch. Document Type"; Enum "Purchase Document Type")
        {
            Caption = 'Document Type';

        }
        field(3; "Purch. Document No."; Code[20])
        {
            Caption = 'Document No.';
            TableRelation = "Purchase Header"."No." WHERE("Document Type" = FIELD("Purch. Document Type"));
        }
        field(4; "Purch. Line No."; Integer)
        {
            Caption = 'Line No.';
        }
        field(7; "Sales Document No."; Code[20])
        {
            Caption = 'Document No.';
            TableRelation = "Sales Header"."No." WHERE("Document Type" = CONST(Order));
            trigger OnLookup()
            var
                LSalesLine: Record "Sales Line";
                LPurchLine: Record "Purchase Line";
                LFrmSalesLine: Page "Sales Lines";
            begin
                LPurchLine.GET("Purch. Document Type", "Purch. Document No.", "Purch. Line No.");

                CLEAR(LSalesLine);
                LSalesLine.SETCURRENTKEY("Document Type", Type, "No.", "Outstanding Quantity");
                LSalesLine.SETRANGE("Document Type", LPurchLine."Document Type");
                LSalesLine.SETRANGE(Type, LPurchLine.Type);
                LSalesLine.SETRANGE("No.", LPurchLine."No.");
                LSalesLine.SETFILTER("Outstanding Quantity", '<>%1', 0);
                LFrmSalesLine.SETTABLEVIEW(LSalesLine);
                LFrmSalesLine.EDITABLE := FALSE;
                LFrmSalesLine.LOOKUPMODE := TRUE;
                IF LFrmSalesLine.RUNMODAL() = ACTION::LookupOK THEN BEGIN
                    LFrmSalesLine.GETRECORD(LSalesLine);
                    VALIDATE("Sales Document No.", LSalesLine."Document No.");
                END;
            end;
        }
    }

    keys
    {
        key(PKey1; "Purch. Document Type", "Purch. Document No.", "Purch. Line No.", "Sales Document No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

