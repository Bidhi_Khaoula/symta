table 50025 "EDI Error"
{

    fields
    {
        field(1; "Sequence No."; Integer)
        {
            Caption = 'N° Séquence';
        }
        field(10; "Document Type"; Code[20])
        {
            Caption = 'Type Document';
        }
        field(11; "EDI Document No"; Code[25])
        {
            Caption = 'No Document EDI';
        }
        field(20; Description; Text[250])
        {
            Caption = 'Desciption';
        }
        field(50; "Error Date"; Date)
        {
            Caption = 'Date Erreur';
        }
        field(51; "Error Time"; Time)
        {
            Caption = 'Heure Erreur';
        }
        field(60; "Edit Error"; Boolean)
        {
            Caption = 'Erreur Editée';
        }
    }

    keys
    {
        key(PKey1; "Sequence No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

