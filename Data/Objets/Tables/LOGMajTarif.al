Table 60004 "LOG Maj Tarif"
{
    FIELDS
    {
        field(1; ID; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(2; Date; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(3; Heure; Time)
        {
            DataClassification = ToBeClassified;
        }
        field(4; User; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(10; Libellé; Code[100])
        {
            DataClassification = ToBeClassified;
        }
        field(11; Commentaire; Code[100])
        {
            DataClassification = ToBeClassified;
        }
        field(12; Marque; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(20; "Ref Active TARIF"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(21; "Ref Fournisseur TARIF"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(22; "Date MAJ TARIF"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(23; "Prix TARIF"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
        field(30; "Ref Active ARTICLE"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(31; "Ref Fournisseur  ARTICLE"; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(32; "Date MAJ ARTICLE"; Date)
        {
            DataClassification = ToBeClassified;
        }
        field(33; "Prix ARTICLE"; Decimal)
        {
            DataClassification = ToBeClassified;
        }
    }
    KEYS
    {
        key(Pk; ID)
        {
            Clustered = true;
        }
    }
}