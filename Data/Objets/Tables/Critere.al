table 50058 Critere
{
    fields
    {
        field(1; "Code"; Code[20])
        {
        }
        field(2; Nom; Text[100])
        {
        }
        field(10; Type; Option)
        {
            OptionMembers = "Booléen",Texte,"Décimal",Entier;
        }
        field(11; "Unité"; Code[20])
        {
        }
        field(99999; Modif; Date)
        {
            Description = 'AD LE 10-06-2014 => TMP ->  POUR SIGNALER QUE LE CRITERE A CHANGE DE TYPE';
        }
    }

    keys
    {
        key(PKey1; "Code")
        {
            Clustered = true;
        }
        key(FKey2; Nom)
        {
        }
    }

    fieldgroups
    {
    }
}

