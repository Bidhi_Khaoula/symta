table 50013 "Shipping Label"
{
    fields
    {
        field(1; "Entry No."; Code[20])
        {
            Caption = 'N° de Séquence';
            NotBlank = false;
        }
        field(2; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Code Transporteur';
            TableRelation = "Shipping Agent".Code;
        }
        field(3; "Product Code"; Code[10])
        {
            Caption = 'Code Produit';
            TableRelation = "Shipping Agent Services"."Product Code" WHERE("Shipping Agent Code" = FIELD("Shipping Agent Code"));
        }
        field(4; "Network Code"; Code[10])
        {
            Caption = 'Code Réseau';
        }
        field(5; "Prepare Code"; Code[30])
        {
            Caption = 'Code Préparateur';
            TableRelation = "Warehouse Employee"."User ID";
        }
        field(6; "BL No."; Code[20])
        {
            Caption = 'N° BL';
        }
        field(7; "Receipt No."; Integer)
        {
            Caption = 'N° de Récépissé';
        }
        field(8; "Day of BL"; Integer)
        {
            Caption = 'Jour du BL';
        }
        field(9; "Month Of BL"; Integer)
        {
            Caption = 'Mois du BL';
        }
        field(10; "Year Of BL"; Integer)
        {
            Caption = 'Année du BL';
        }
        field(11; "Recipient Code"; Code[20])
        {
            Caption = 'Code du Destinataire';
            TableRelation = Customer."No.";
        }
        field(12; "Recipient Name"; Text[50])
        {
            Caption = 'Nom du Destinataire';
        }
        field(13; "Recipient Address"; Text[50])
        {
            Caption = 'Adresse 1 du Destinataire';
        }
        field(14; "Recipient Address 2"; Text[50])
        {
            Caption = 'Adresse 2 du Destinataire';
        }
        field(15; "Recipient Post Code"; Code[10])
        {
            Caption = 'Code Postal du Destinataire';
        }
        field(16; "Recipient City"; Code[30])
        {
            Caption = 'ville du Destinataire';
        }
        field(17; "Recipient Country Code"; Code[10])
        {
            Caption = 'Code Pays du Destinataire';
        }
        field(18; "Recipient Erea"; Code[10])
        {
            Caption = 'Région du Destinataire';
        }
        field(19; "Instruction of Delivery"; Text[70])
        {
            Caption = 'Instruction de Livraison';
        }
        field(20; "Notice Delivery"; Text[70])
        {
            Caption = 'Remarque de Livraison';
        }
        field(21; "Nature of Goods"; Text[26])
        {
            Caption = 'Nature de Marchandise';
        }
        field(22; "Recipient Person"; Text[30])
        {
            Caption = 'Personne Destinataire';
        }
        field(23; "Recipient Phone No."; Text[25])
        {
            Caption = 'Téléphone du Destinataire';
        }
        field(24; Representative; Text[25])
        {
            Caption = 'Representant';
        }
        field(25; "Nb Of Box Vrac"; Integer)
        {
            Caption = 'Nbre de  Colis Vrac';
        }
        field(26; "Nb Of Box"; Integer)
        {
            Caption = 'Nbre de  Colis Total';
        }
        field(27; "Nb Of Pallets"; Integer)
        {
            Caption = 'Nbre de  Palette';
        }
        field(28; "Nb Of Pallets Deposit On 1"; Integer)
        {
            Caption = 'Nbre de  Palette Consignées 1';
        }
        field(29; "Type Of Pallets Deposit On 1"; Code[6])
        {
            Caption = 'Type de  Palette Consignées 1';
        }
        field(30; "Nb Of Pallets Deposit On 2"; Integer)
        {
            Caption = 'Nbre de  Palette Consignées 2';
        }
        field(31; "Type Of Pallets Deposit On 2"; Integer)
        {
            Caption = 'Type de  Palette Consignées 2';
        }
        field(32; "Nb Of Pallets Deposit On 3"; Integer)
        {
            Caption = 'Nbre de  Palette Consignées 3';
        }
        field(33; "Type Of Pallets Deposit On 3"; Integer)
        {
            Caption = 'Type de  Palette Consignées 3';
        }
        field(34; "Nb Of Pallets Deposit On 4"; Integer)
        {
            Caption = 'Nbre de  Palette Consignées 4';
        }
        field(35; "Type Of Pallets Deposit On 4"; Integer)
        {
            Caption = 'Type de  Palette Consignées 4';
        }
        field(36; "Nb Of Pallets Deposit On 5"; Integer)
        {
            Caption = 'Nbre de  Palette Consignées 5';
        }
        field(37; "Type Of Pallets Deposit On 5"; Integer)
        {
            Caption = 'Type de  Palette Consignées 5';
        }
        field(38; Weight; Decimal)
        {
            Caption = 'Poids';
            DecimalPlaces = 2 : 2;
        }
        field(39; "Assured Box"; Boolean)
        {
            Caption = 'Colis Assurés';
        }
        field(40; "Matters Dangerous"; Code[8])
        {
            Caption = 'Matières Dangereuse';
        }
        field(41; Manuel; Option)
        {
            Caption = 'Mode d''envoi';
            OptionMembers = P," ",D,S;
        }
        field(42; "Tax or Exonerated VAT"; Option)
        {
            Caption = 'Taxable ou Exonéré de TVA';
            OptionMembers = T," ",E;
        }
        field(43; "Cash on delivery"; Decimal)
        {
            Caption = 'C/Rembours';
            DecimalPlaces = 2 : 2;
        }
        field(44; "Devise C/rembours Maximum"; Code[3])
        {
            Caption = 'Devise C/rembours Maximum';
        }
        field(45; "Imperative day of Delivery"; Integer)
        {
            Caption = 'Jour de Livraison Impératif';
        }
        field(46; "Imperative month of Delivery"; Integer)
        {
            Caption = 'Mois de Livraison Impératif';
        }
        field(47; "Imperative Year of Delivery"; Integer)
        {
            Caption = 'Année de Livraison Impératif';
        }
        field(48; "Day of Delivery Period begin"; Integer)
        {
            Caption = 'Jour de Livraison début Période';
        }
        field(49; "month of Delivery Period begin"; Integer)
        {
            Caption = 'Mois de Livraison début Période';
        }
        field(50; "year of Delivery Period begin"; Integer)
        {
            Caption = 'Année de Livraison début Période';
        }
        field(51; "Time of Delivery Period begin"; Integer)
        {
            Caption = 'Heure de Livraison début Période';
        }
        field(52; "Day of Delivery Period End"; Integer)
        {
            Caption = 'Jour de Livraison Fin Période';
        }
        field(53; "month of Delivery Period End"; Integer)
        {
            Caption = 'Mois de Livraison Fin Période';
        }
        field(54; "year of Delivery Period End"; Integer)
        {
            Caption = 'Année de Livraison Fin Période';
        }
        field(55; "Time of Delivery Period End"; Integer)
        {
            Caption = 'Heure de Livraison Fin Période';
        }
        field(56; Longor; Integer)
        {
            Caption = 'Longueur';
        }
        field(57; Volume; Integer)
        {
            Caption = 'Volume';
        }
        field(58; "Recipient Name cheque"; Text[50])
        {
            Caption = 'Nom du dest. du Cheque';
        }
        field(59; "Recipient Street 1 cheque"; Text[50])
        {
            Caption = 'Rue 1 du dest. du Cheque';
        }
        field(60; "Recipient Street 2 cheque"; Text[50])
        {
            Caption = 'Rue 2 du dest. du Cheque';
        }
        field(61; "Recipient Post Code cheque"; Code[10])
        {
            Caption = 'Code Postal du Dest. Cheque';
        }
        field(62; "Recipient City Cheque"; Text[30])
        {
            Caption = 'Ville du dest. Cheque';
        }
        field(63; "Recipient Country Cheque"; Code[10])
        {
            Caption = 'Code Pays du Dest. Cheque';
        }
        field(64; "Goods or Documents"; Option)
        {
            Caption = 'Marchandises ou Documents';
            OptionMembers = M,D;
        }
        field(65; "Facture Pro Format"; Code[20])
        {
            Caption = 'Facture Pro Format';
        }
        field(66; Incoterm; Text[3])
        {
            Caption = 'Incoterm';
        }
        field(67; "Nature OF product (NDP)"; Text[13])
        {
            Caption = 'Nature du produit (NDP)';
        }
        field(68; Height; Text[3])
        {
            Caption = 'Hauteur';
            Description = '  sous forme 015 pour 15 cm';
        }
        field(69; Width; Text[3])
        {
            Caption = 'Largeur';
            Description = '  sous forme 015 pour 15 cm';
        }
        field(70; Depth; Text[3])
        {
            Caption = 'Profondeur';
            Description = '  sous forme 015 pour 15 cm';
        }
        field(71; "Nber Object"; Text[3])
        {
            Caption = 'Nombre d''objet';
            Description = '  sous forme 015 pour 15 objets';
        }
        field(72; "Name of the importer"; Text[50])
        {
            Caption = 'Nom de L''importateur';
        }
        field(73; "Street 1 of the importer"; Text[50])
        {
            Caption = 'Rue de L''importateur 1';
        }
        field(74; "Street 2 of the importer"; Text[50])
        {
            Caption = 'Rue de L''importateur 2';
        }
        field(75; "Post Code of the importer"; Text[10])
        {
            Caption = 'Code postal de L''importateur';
        }
        field(76; "City of of the importer"; Text[30])
        {
            Caption = 'Ville de L''importateur';
        }
        field(77; "Phone of the importer"; Text[15])
        {
            Caption = 'Telephone de L''importateur';
        }
        field(78; "No. start label"; Code[10])
        {
            Caption = 'N° Etiquette Départ';
            Editable = false;
            Enabled = true;
        }
        field(79; "No. end label"; Code[10])
        {
            Caption = 'N° Etiquette Fin';
            Editable = false;
        }
        field(80; "No. ligne"; Integer)
        {
            Caption = 'N° ligne';
        }
        field(81; "Shipping Agent Services"; Code[20])
        {
            Caption = 'Prestation transporteur';
            TableRelation = "Shipping Agent Services".Code WHERE("Shipping Agent Code" = FIELD("Shipping Agent Code"));
        }
        field(82; "Date of  bl"; Date)
        {
            Caption = 'Date du bl';
        }
        field(83; "Imperative date of Delivery"; Date)
        {
            Caption = 'Date de Livraison Impérative';
        }
        field(84; "Transmission date"; Date)
        {
            Caption = 'Date transmission';
            Editable = false;
        }
        field(85; "Transmission heure"; Time)
        {
            Caption = 'Heure transmission';
            Editable = false;
        }
        field(86; "Transmission status"; Text[2])
        {
            Caption = 'Statut de transmission';
            InitValue = 'AT';
        }
        field(87; Account; Code[10])
        {
            Caption = 'Account';
        }
        field(90; "Amount Assured"; Decimal)
        {
            Caption = 'Montant assuré';
            Description = '// AD Le 04-03-2008 => Demande de Virginie';
        }
        field(100; "External Document No."; Code[35])
        {
            Caption = 'External Document No.';
            Description = 'AD Le 24-01-2011 - D Le 07-12-2015 => MIG2015 -> à 35 au lieu de 20 dans la nouvelle version';
        }
        field(101; "Mode d'expédition"; enum "Mode Expédition")
        {
            Description = 'MC Le 26-04-2011 => Définition du mode d''expédition. (Normal, Express ...)';
        }
        field(102; "Nombre d'étiquette"; Integer)
        {
        }
        field(200; "Teliae Deleted"; Boolean)
        {
            Caption = 'Deleted';
            Description = 'CFR Le 27/04/2020 FE20190930';
        }
        field(201; "Teliae Deleting User"; Code[20])
        {
            Caption = 'User suppression';
            Description = 'CFR Le 27/04/2020 FE20190930';
        }
        field(202; "Teliae Deleting Date"; Date)
        {
            Caption = 'Deleting Date';
            Description = 'CFR Le 27/04/2020 FE20190930';
        }
        field(203; "Teliae Deleting Time"; Time)
        {
            Caption = 'Deleting Time';
            Description = 'CFR Le 27/04/2020 FE20190930';
        }
        field(204; "Teliae End Of Day"; Boolean)
        {
            Caption = 'End Of Day';
            Description = 'CFR Le 27/04/2020 FE20190930';
        }
        field(210; "UM Quantity"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = Count("Teliae UM Detail" WHERE("No. BP" = FIELD("Preparation No.")));
            Caption = 'Nombre d''UM';
        }
        field(50610; "Preparation No."; Code[20])
        {
            Caption = 'N° BP';
            Description = ' MC Le 27-04-11 => SYMTA -> Garde en mémoire le numéro du BP.';
        }
    }

    keys
    {
        key(PKey1; "Entry No.")
        {
            Clustered = true;
        }
        key(FKey2; "Shipping Agent Code", "Shipping Agent Services")
        {
        }
        key(FKey3; "Shipping Agent Code", Account)
        {
        }
        key(FKey4; "BL No.")
        {
        }
        key(FKey5; "Preparation No.")
        {
        }
        key(FKey6; "Date of  bl")
        {
        }
    }

    fieldgroups
    {
    }
}

