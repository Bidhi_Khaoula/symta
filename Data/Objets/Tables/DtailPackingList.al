table 50031 "Détail Packing List"
{

    fields
    {
        field(1; "N° expedition magasin"; Code[20])
        {
        }
        field(3; "N° expedition enregistré"; Code[20])
        {
        }
        field(4; "N° BL"; Code[20])
        {
        }
        field(9; "Numero de Colis"; Code[13])
        {
        }
        field(50; Length; Decimal)
        {
            Caption = 'Length';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(51; Width; Decimal)
        {
            Caption = 'Width';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(52; Height; Decimal)
        {
            Caption = 'Height';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(53; Cubage; Decimal)
        {
            Caption = 'Cubage';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(56; Weight; Decimal)
        {
            Caption = 'Weight';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(60; "Pack Nb"; Integer)
        {
            Caption = 'Nb Colis';
        }
        field(65; "Hors CE"; Boolean)
        {
            Caption = 'Hors CE';
        }
        field(100; "Comment 1"; Text[50])
        {
            Caption = 'Commentaire 1';
        }
        field(101; "Comment 2"; Text[50])
        {
        }
    }

    keys
    {
        key(PKey1; "N° expedition magasin", "Numero de Colis")
        {
            Clustered = true;
        }
        key(FKey2; "Numero de Colis")
        {
        }
    }

    fieldgroups
    {
    }
}

