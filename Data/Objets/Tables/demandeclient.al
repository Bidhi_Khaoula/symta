table 50049 "demande_client"
{

    fields
    {
        field(1; code_client; Code[20])
        {
        }
        field(2; "référence"; Code[20])
        {
        }
        field(3; "qte_demandé"; Decimal)
        {
        }
        field(4; qte_stock; Decimal)
        {
        }
        field(5; date_demande; DateTime)
        {
        }
        field(6; user; Code[20])
        {
        }
        field(10; "No Document"; Code[20])
        {
            Description = 'AD Le 27-09-2016 => REGIE -> Pb De Clé si on a 2 fois la ref dans NAV';
        }
        field(11; "No Ligne Document"; Integer)
        {
            Description = 'AD Le 27-09-2016 => REGIE -> Pb De Clé si on a 2 fois la ref dans NAV';
        }
    }

    keys
    {
        key(PKey1; date_demande, code_client, "référence", "No Document", "No Ligne Document")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

