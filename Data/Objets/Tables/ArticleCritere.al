table 50057 "Article Critere"
{
    // LookupPageID = "Item Critere List";

    fields
    {
        field(1; "Code Article"; Code[20])
        {
            TableRelation = Item."No.";
        }
        field(2; "Code Critere"; Code[20])
        {
            TableRelation = Critere.Code;
        }
        field(4; "Variant Code"; Code[10])
        {
            Caption = 'Variant Code';
            TableRelation = "Item Variant".Code WHERE("Item No." = FIELD("Code Article"));
        }
        field(7; "Priorité"; Integer)
        {
        }
        field(10; Text; Text[250])
        {
        }
        field(12; "Booléen"; Option)
        {
            OptionMembers = " ",Non,Oui;
        }
        field(14; Entier; Integer)
        {
        }
        field(16; "Décimal"; Decimal)
        {
        }
        field(50; "Super famille"; Text[250])
        {
            CalcFormula = Lookup(Item."Super Famille Marketing" WHERE("No." = FIELD("Code Article")));
            Editable = false;
            Enabled = false;
            FieldClass = FlowField;
        }
        field(51; Famille; Text[250])
        {
            CalcFormula = Lookup(Item."Famille Marketing" WHERE("No." = FIELD("Code Article")));
            Editable = false;
            Enabled = false;
            FieldClass = FlowField;
        }
        field(52; "Sous famille"; Text[250])
        {
            CalcFormula = Lookup(Item."Sous Famille Marketing" WHERE("No." = FIELD("Code Article")));
            Editable = false;
            Enabled = false;
            FieldClass = FlowField;
        }
        field(60; "Super Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Item Hierarchies"."Super Famille Marketing" WHERE("Code Article" = FIELD("Code Article"));
        }
        field(61; "Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Item Hierarchies"."Famille Marketing" WHERE("Code Article" = FIELD("Code Article"),
                                                                          "Super Famille Marketing" = FIELD("Super Famille Marketing"));
        }
        field(62; "Sous Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Item Hierarchies"."Sous Famille Marketing" WHERE("Code Article" = FIELD("Code Article"),
                                                                               "Super Famille Marketing" = FIELD("Super Famille Marketing"),
                                                                               "Famille Marketing" = FIELD("Famille Marketing"));
        }
        field(50800; "Create User ID"; Code[20])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
        field(50801; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
        field(50802; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
        field(50805; "Modify User ID"; Code[20])
        {
            Caption = 'Code utilisateur Modification';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
        field(50806; "Modify Date"; Date)
        {
            Caption = 'Date Modification';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
        field(50807; "Modify Time"; Time)
        {
            Caption = 'Heure Modification';
            Description = 'TRACABILITE ENREGISTREMENT FE20190425';
        }
    }

    keys
    {
        key(PKey1; "Code Article", "Code Critere", "Variant Code")
        {
            Clustered = true;
        }
        key(FKey2; "Code Article", "Priorité")
        {
        }
        key(FKey3; "Code Article", "Super Famille Marketing", "Famille Marketing", "Sous Famille Marketing", "Priorité")
        {
        }
    }

    fieldgroups
    {
    }
}

