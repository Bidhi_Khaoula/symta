table 50034 "Tarifs Fournisseurs Glb"
{

    fields
    {
        field(1; "Vendor No."; Code[20])
        {
            Caption = 'Vendor No.';
            TableRelation = Vendor;
            //This property is currently not supported
            //TestTableRelation = true;
            ValidateTableRelation = true;
        }
        field(2; "No. 2"; Code[20])
        {
            Caption = 'No. 2';
            Description = 'AD Le 27-04-2011 => SYMTA -> Référence Active';
        }
        field(10; "Vendor Item No."; Text[20])
        {
            Caption = 'Vendor Item No.';
        }
        field(11; "Vendor Description"; Text[30])
        {
            Caption = 'Désignation fournisseur';
        }
        field(15; Description; Text[30])
        {
            Caption = 'Description';
        }
        field(16; "Description 2"; Text[30])
        {
            Caption = 'Description 2';
        }
        field(20; "Date Tarif"; Date)
        {
        }
        field(21; "Unit Price"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Unit Price';
            MinValue = 0;
        }
        field(22; "Net Unit Price"; Decimal)
        {
            Caption = 'Prix unitaire Net';
        }
        field(40; "Gross Weight"; Decimal)
        {
            Caption = 'Gross Weight';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 29-10-2009 => FARGROUP -> Non editable car mis a jour par la fenêtre des unités';
            Editable = false;
            MinValue = 0;
        }
        field(50; "Purch. Unit of Measure"; Code[10])
        {
            Caption = 'Purch. Unit of Measure';
            TableRelation = "Unit of Measure".Code;
        }
        field(51; "Purch. Multiple"; Decimal)
        {
            Caption = 'Multiple d''achat';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
        }
        field(100; "Variance prix"; Code[10])
        {
        }
        field(101; "Comodity Code"; Code[5])
        {
        }
        field(102; "Code remise"; Code[10])
        {
        }
        field(103; "Ligne produit"; Code[10])
        {
        }
        field(104; "Code gestion"; Code[10])
        {
        }
        field(105; "Code lexique"; Code[10])
        {
        }
        field(106; "Code Fonction"; Code[10])
        {
        }
        field(107; "Code origine"; Code[10])
        {
        }
        field(108; "Code Retour"; Code[10])
        {
        }
        field(150; "Divers 1"; Text[30])
        {
        }
        field(151; "Divers 2"; Text[30])
        {
        }
        field(152; "Divers 3"; Text[30])
        {
        }
        field(153; "Divers 4"; Text[30])
        {
        }
        field(154; "Divers 5"; Text[30])
        {
        }
        field(200; "Qte Colonne"; Decimal)
        {
        }
        field(210; "Qte Colonne 1"; Decimal)
        {
        }
        field(211; "Prix Colonne 1"; Decimal)
        {
        }
        field(215; "Qte Colonne 2"; Decimal)
        {
        }
        field(216; "Prix Colonne 2"; Decimal)
        {
        }
        field(220; "Qte Colonne 3"; Decimal)
        {
        }
        field(221; "Prix Colonne 3"; Decimal)
        {
        }
    }

    keys
    {
        key(PKey1; "Vendor No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

