table 50000 "Generals Parameters"
{
    Caption = 'Paramètres Généraux';
    fields
    {
        field(1; Type; Code[20])
        {
            Caption = 'Type';
            NotBlank = true;
        }
        field(2; "Code"; Code[15])
        {
            Caption = 'Code';
            NotBlank = true;
        }
        field(3; ShortCode; Code[20])
        {
            Caption = 'Code Court';
        }
        field(4; LongCode; Code[50])
        {
            Caption = 'Code Long';
        }
        field(5; ShortDescription; Text[20])
        {
            Caption = 'Libellé Court';
        }
        field(6; LongDescription; Text[50])
        {
            Caption = 'Libellé Long';
        }
        field(7; Decimal1; Decimal)
        {
            Caption = 'Decimal1';
        }
        field(8; Decimal2; Decimal)
        {
            Caption = 'Decimal2';
        }
        field(9; integer1; Integer)
        {
            Caption = 'Entier1';
        }
        field(10; integer2; Integer)
        {
            Caption = 'Entier2';
        }
        field(11; Date1; Date)
        {
            Caption = 'Date1';
        }
        field(12; Date2; Date)
        {
            Caption = 'Date2';
        }
        field(15; LongDescription2; Text[250])
        {
            Caption = 'Libellé Long2';
        }
        field(25; Default; Boolean)
        {
            Caption = 'Défaut';
        }
    }

    keys
    {
        key(PKey1; Type, "Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; "Code", LongDescription)
        {
        }
    }
}

