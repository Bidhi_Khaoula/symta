table 60008 "Packing List Package"
{
    fields
    {
        field(1; "Packing List No."; Code[20])
        {
            Caption = 'N° liste de colisage';
        }
        field(2; "Package No."; Code[20])
        {
            Caption = 'N° colis';
        }
        field(10; "Package Type"; Option)
        {
            Caption = 'Type de colis';
            OptionMembers = Carton,Palette;
        }
        field(11; "Package Quantity"; Decimal)
        {
            CalcFormula = Sum("Packing List Line"."Line Quantity" WHERE("Packing List No." = FIELD("Packing List No."),
                                                                         "Package No." = FIELD("Package No.")));
            Caption = 'Quantité dans colis';
            Editable = false;
            FieldClass = FlowField;
        }
        field(12; "Package Weight Auto"; Decimal)
        {
            CalcFormula = Sum("Packing List Line"."Line Weight" WHERE("Packing List No." = FIELD("Packing List No."),
                                                                       "Package No." = FIELD("Package No.")));
            Caption = 'Poids du colis Calculé';
            FieldClass = FlowField;
        }
        field(13; "Package Weight"; Decimal)
        {
            Caption = 'Poids brut';
            DecimalPlaces = 2 : 2;
            MinValue = 0;
        }
        field(20; Length; Decimal)
        {
            Caption = 'Longueur (cm)';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(21; Width; Decimal)
        {
            Caption = 'Largeur (cm)';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(22; Height; Decimal)
        {
            Caption = 'Hauteur (cm)';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(23; Volume; Decimal)
        {
            Caption = 'Volume (m3)';
            DecimalPlaces = 0 : 5;
            MinValue = 0;
        }
        field(30; "Outside Of EC"; Boolean)
        {
            Caption = 'Hors CE';
        }
        field(31; "Comment 1"; Text[50])
        {
            Caption = 'Commentaire 1';
        }
        field(32; "Comment 2"; Text[50])
        {
            Caption = 'Commentaire 2';
        }
        field(40; "Indice Colis"; Integer)
        {
            Caption = 'Indice';
            Description = 'GRI le 13/08/2020 (WIIO)';
        }
        field(50000; "Tare Weight"; Decimal)
        {
            Caption = 'Tare Weight';
            DecimalPlaces = 0 : 5;
            Description = 'SFD20201005';
            MinValue = 0;
        }
        field(50001; "Net Weight"; Decimal)
        {
            Caption = 'Net Weight';
            DecimalPlaces = 2 : 2;
            Description = 'SFD20201005';
            Editable = false;
            MinValue = 0;
        }
    }

    keys
    {
        key(PKey1; "Packing List No.", "Package No.")
        {
            Clustered = true;
        }
        key(FKey2; "Package No.", "Package Type")
        {
        }
        key(FKey3; "Packing List No.", "Indice Colis")
        {
        }
    }

    fieldgroups
    {
    }
}

