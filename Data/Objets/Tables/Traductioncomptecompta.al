table 50016 "Traduction compte compta"
{

    fields
    {
        field(1; "Compte DBX"; Code[20])
        {
        }
        field(2; "Compte Navision"; Code[20])
        {
            NotBlank = true;
            TableRelation = "G/L Account"."No.";
        }
        field(3; "Désignation"; Text[50])
        {
            Editable = false;
        }
    }

    keys
    {
        key(PKey1; "Compte DBX", "Compte Navision")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

