table 60007 "Packing List Line"
{
    fields
    {
        field(1; "Packing List No."; Code[20])
        {
            Caption = 'N° liste de colisage';
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'N° ligne';
        }
        field(3; "Line Status"; Option)
        {
            Caption = 'Statut';
            OptionCaption = 'Ouvert,Enregistré';
            OptionMembers = Open,Close;
        }
        field(11; "Package No."; Code[20])
        {
            Caption = 'N° colis';
            TableRelation = "Packing List Package"."Package No." WHERE("Packing List No." = FIELD("Packing List No."));
        }
        field(20; "Item No."; Code[20])
        {
            Caption = 'N° article';
        }
        field(21; Description; Text[50])
        {
            Caption = 'Désignation';
        }
        field(22; "Description 2"; Text[50])
        {
            Caption = 'Désignation 2';
        }
        field(23; "Quantity to Ship"; Decimal)
        {
            Caption = 'Quantité à expédier';
        }
        field(24; "Line Quantity"; Decimal)
        {
            Caption = 'Quantité de la ligne';
        }
        field(25; "Unit Weight"; Decimal)
        {
            Caption = 'Poids unitaire';
        }
        field(26; "Line Weight"; Decimal)
        {
            Caption = 'Poids de la ligne';
            Editable = false;
        }
        field(100; "Whse. Shipment No."; Code[20])
        {
            Caption = 'N° expédition entrepôt';
        }
        field(101; "Whse. Shipment Line No."; Integer)
        {
            Caption = 'N° ligne expédition entrepôt';
        }
        field(102; "WSL Sorting Sequence No."; Integer)
        {
            Caption = 'N° séquence tri ligne expédition entrepôt';
        }
        field(105; "Posted Whse. Shipment No."; Code[20])
        {
            Caption = 'N° expéd. entrep. enreg.';
        }
        field(106; "Posted Whse. Shipment Line No."; Integer)
        {
            Caption = 'N° ligne expéd. entrep. enreg.';
        }
        field(110; "Source Type"; Integer)
        {
            Caption = 'Type origine';
        }
        field(111; "Source Subtype"; Option)
        {
            Caption = 'Sous-type origine';
            OptionMembers = "0","1","2","3","4","5","6","7","8","9","10";
        }
        field(112; "Source No."; Code[20])
        {
            Caption = 'N° origine';
        }
        field(113; "Source Line No."; Integer)
        {
            Caption = 'N° ligne origine';
        }
        field(114; "Source Document"; Option)
        {
            Caption = 'Document origine';
            OptionCaption = ',Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,,Outbound Transfer,,,,,,,,Service Order';
            OptionMembers = ,"Sales Order",,,"Sales Return Order","Purchase Order",,,"Purchase Return Order",,"Outbound Transfer",,,,,,,,"Service Order";
        }
        field(120; "Posted Source No."; Code[20])
        {
            Caption = 'N° origine enreg.';
        }
        field(122; "Posted Source Document"; Option)
        {
            Caption = 'Document origine enreg.';
            OptionCaption = ' ,Réception enreg.,,Réception retour enreg.,,Expédition enreg.,,Expédition retour enreg.,,,Réception transfert enreg.';
            OptionMembers = " ","Posted Receipt",,"Posted Return Receipt",,"Posted Shipment",,"Posted Return Shipment",,,"Posted Transfer Shipment";
        }
        field(50000; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Référence Active';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50001; "Zone Code"; Code[10])
        {
            Caption = 'Zone Code';
            Description = 'SFD20201005';
            Editable = false;
            TableRelation = Zone.Code;
        }
        field(50002; "Bin Code"; Code[20])
        {
            Caption = 'Bin Code';
            Description = 'SFD20201005';
            Editable = false;
        }
        field(50082; "WIIO Préparation"; Boolean)
        {
            CalcFormula = Max("Warehouse Shipment Line"."WIIO Préparation" WHERE("No." = FIELD("Whse. Shipment No."),
                                                                                  "Line No." = FIELD("Whse. Shipment Line No.")));
            Description = 'WIIO2 -> Ligne préparée';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(PKey1; "Packing List No.", "Line No.")
        {
            Clustered = true;
        }
        key(FKey2; "Packing List No.", "Package No.")
        {
            SumIndexFields = "Line Quantity";
        }
        key(FKey3; "Whse. Shipment No.", "WSL Sorting Sequence No.", "Line No.")
        {
        }
        key(FKey4; "Packing List No.", "Whse. Shipment No.", "Whse. Shipment Line No.")
        {
            SumIndexFields = "Line Quantity";
        }
    }

    fieldgroups
    {
    }

}

