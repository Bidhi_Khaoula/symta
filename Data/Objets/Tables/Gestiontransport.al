table 50011 "Gestion transport"
{

    fields
    {
        field(1; "Secteur Port"; Code[10])
        {
            NotBlank = false;
            TableRelation = Territory;
        }
        field(2; "Poids début"; Decimal)
        {
            MinValue = 0;
        }
        field(4; "Code article Port"; Code[20])
        {
            TableRelation = "G/L Account"."No.";
        }
        field(5; "Code transporteur"; Code[20])
        {
            NotBlank = true;
            TableRelation = "Shipping Agent".Code;
        }
        field(6; "Code prestation transport"; Code[20])
        {
            NotBlank = true;
            TableRelation = "Shipping Agent Services".Code WHERE("Shipping Agent Code" = FIELD("Code transporteur"));
        }
        field(7; "Cout Port"; Decimal)
        {
        }
        field(8; "Prix Port"; Decimal)
        {
        }
        field(50; "Code article Emballage"; Code[20])
        {
            TableRelation = "G/L Account"."No.";
        }
        field(51; "Cout Emballage"; Decimal)
        {
        }
        field(52; "Prix Emballage"; Decimal)
        {
        }
    }

    keys
    {
        key(PKey1; "Code transporteur", "Code prestation transport", "Secteur Port", "Poids début")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

