table 50046 "Message receive from  MINILOAD"
{

    fields
    {
        field(1; "Entry No."; Integer)
        {
            Caption = 'N° Séquence';
        }
        field(3; "Message No."; Code[50])
        {
            Caption = 'Message No.';
        }
        field(4; Date; Date)
        {
        }
        field(5; Heure; Time)
        {
            Caption = 'Hour';
        }
        field(6; "Code"; Code[50])
        {
        }
        field(7; Description; Text[250])
        {
        }
        field(8; "Location Code"; Code[20])
        {
            Caption = 'Magasin';
        }
        field(9; Quantity; Decimal)
        {
            Caption = 'Quantité';
        }
        field(10; "Order No"; Code[50])
        {
            Caption = 'N° Commande';
        }
        field(11; "Line No"; Integer)
        {
            Caption = 'N° Ligne';
        }
    }

    keys
    {
        key(PKey1; "Entry No.")
        {
            Clustered = true;
        }
        key(FKey2; "Message No.", "Code", Date)
        {
        }
    }

    fieldgroups
    {
    }
}

