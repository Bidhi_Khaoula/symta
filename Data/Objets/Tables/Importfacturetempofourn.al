table 50033 "Import facture tempo. fourn."
{

    fields
    {
        field(1; "No séquence"; Integer)
        {
        }
        field(2; "Document Type"; Option)
        {
            Caption = 'Document Type';
            OptionCaption = 'Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order';
            OptionMembers = Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        }
        field(3; "Type ligne"; Option)
        {
            OptionCaption = 'Entete,Ligne';
            OptionMembers = Entete,Ligne;
        }
        field(5; "Code fournisseur"; Code[20])
        {
            TableRelation = Vendor;
        }
        field(100; "No Facture"; Code[20])
        {
        }
        field(101; "Date Facture"; Date)
        {
        }
        field(120; "No commande"; Code[20])
        {
            TableRelation = "Purchase Header"."No." WHERE("Document Type" = CONST(Order));
        }
        field(121; "ligne commande"; Integer)
        {
            TableRelation = "Purchase Line"."Line No." WHERE("Document Type" = CONST(Order),
                                                              "Document No." = FIELD("No commande"));
        }
        field(200; "Code article SYMTA"; Code[20])
        {
        }
        field(201; "Code article Fournisseur"; Code[20])
        {
        }
        field(210; "Désignation"; Text[50])
        {
        }
        field(220; "Quantité"; Decimal)
        {
        }
        field(250; "Pu brut"; Decimal)
        {
        }
        field(251; "Remise 1"; Decimal)
        {
        }
        field(252; "Remise 2"; Decimal)
        {
        }
        field(253; "Pu net"; Decimal)
        {
            DecimalPlaces = 0 : 3;
        }
        field(254; "Total Net"; Decimal)
        {
        }
        field(1001; "Date import"; Date)
        {
        }
        field(1002; "heure import"; Time)
        {
        }
        field(1003; "utilisateur import"; Code[20])
        {
        }
        field(1004; "libéllé import erreur"; Text[150])
        {
        }
        field(1021; "date intégration"; Date)
        {
        }
        field(1022; "heure intégration"; Time)
        {
        }
        field(1023; "utilisateur integration"; Code[20])
        {
        }
        field(1024; "erreur integration"; Text[150])
        {
        }
        field(1030; "No de reception"; Code[20])
        {
        }
        field(1031; "No ligne reception"; Integer)
        {
        }
        field(1035; "No commande affecté"; Code[20])
        {
            TableRelation = "Purchase Header"."No." WHERE("Document Type" = CONST(Order));
        }
        field(1036; "No ligne de commande"; Integer)
        {
            TableRelation = "Purchase Line"."Line No." WHERE("Document Type" = CONST(Order),
                                                              "Document No." = FIELD("No commande affecté"));
        }
        field(1040; "No reception enregistré"; Code[20])
        {
        }
        field(1041; "No ligne reception enregistré"; Integer)
        {
        }
        field(1996; "Aucune ligne trouvé"; Boolean)
        {
        }
        field(1997; "Erreur côut"; Boolean)
        {
        }
        field(1998; "Erreur Quantité"; Boolean)
        {
        }
        field(1999; "Plusieur ligne possible"; Boolean)
        {
        }
        field(2000; "Erreur traité"; Boolean)
        {
        }
        field(2001; "Code article commande"; Code[20])
        {
        }
        field(2002; "Quantité restante commande"; Decimal)
        {
        }
        field(2003; "Pu net commande"; Decimal)
        {
        }
        field(2004; "Code article a utilisé"; Code[20])
        {
        }
        field(2005; "Nouvelle quantité"; Decimal)
        {
        }
        field(2006; "Nouveau cout net"; Decimal)
        {
        }
        field(2007; "Pu brut commande"; Decimal)
        {
        }
        field(2008; "Remise 1 commande"; Decimal)
        {
        }
        field(2009; "Remise 2 commande"; Decimal)
        {
        }
        field(2010; "Total net commande"; Decimal)
        {
        }
        field(3000; "Type erreur"; Integer)
        {
        }
        field(3001; "Nouveau Pu Brut"; Decimal)
        {
        }
        field(3002; "Nouvelle Remise 1"; Decimal)
        {
        }
        field(3003; "Nouvelle Remise 2"; Decimal)
        {
        }
        field(3004; "Code Article Origine"; Code[20])
        {
        }
    }

    keys
    {
        key(Key1; "No séquence", "Type ligne")
        {
            Clustered = true;
        }
        key(Key2; "date intégration", "No Facture", "Code article SYMTA")
        {
        }
        key(Key3; "No Facture")
        {
        }
        key(Key4; "No commande affecté", "No ligne de commande", "No de reception")
        {
            SumIndexFields = "Quantité";
        }
    }

    fieldgroups
    {
    }
}

