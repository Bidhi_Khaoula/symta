table 50038 "Gestion Litige"
{

    fields
    {
        field(1; "Whse Receipt No."; Code[20])
        {
            Caption = 'No.';
            Editable = true;
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'Line No.';
            Editable = true;
        }
        field(10; "Vendor No."; Code[20])
        {
            Caption = 'N° Fournisseur';
            TableRelation = Vendor;
        }
        field(12; "Ref. Active"; Code[20])
        {
            Caption = 'Ref. Active';
            Editable = false;
        }
        field(13; "Ref. Fournisseur"; Code[20])
        {
            Caption = 'Ref. Fournisseur';
        }
        field(14; "Item No."; Code[20])
        {
            Caption = 'Item No.';
            Editable = true;
            TableRelation = Item;
        }
        field(15; "Quantité Théorique"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Editable = true;
        }
        field(16; "Receipt Quantity"; Decimal)
        {
            Caption = 'Quantité Réceptionnée';
            DecimalPlaces = 0 : 5;
        }
        field(20; "Prix Brut Théorique"; Decimal)
        {
            Caption = 'Prix Brut Théorique';
        }
        field(21; "Remise 1 Théorique"; Decimal)
        {
            Caption = 'Remise 1 Théorique';
        }
        field(22; "Remise 2 Théorique"; Decimal)
        {
            Caption = 'Remise 2 Théorique';
        }
        field(23; "Prix Net Théorique"; Decimal)
        {
            Caption = 'Prix Net Théorique';
            Editable = false;
        }
        field(25; "Total Net Théorique"; Decimal)
        {
            Caption = 'Total Net Théorique';
            DecimalPlaces = 2 : 2;
            Editable = false;
        }
        field(30; "Prix Brut Réceptionné"; Decimal)
        {
            Caption = 'Prix Brut Réceptionné';
        }
        field(31; "Remise 1 Réceptionnée"; Decimal)
        {
            Caption = 'Remise 1 Réceptionnée';
        }
        field(32; "Remise 2 Réceptionnée"; Decimal)
        {
            Caption = 'Remise 2 Réceptionnée';
        }
        field(33; "Prix Net Réceptionné"; Decimal)
        {
            Caption = 'Prix Net Réceptionné';
            Editable = false;
        }
        field(35; "Total Net  Réceptionné"; Decimal)
        {
            Caption = 'Total Net  Réceptionné';
            DecimalPlaces = 2 : 2;
            Editable = false;
        }
        field(40; "Receipt No."; Code[20])
        {
            Caption = 'N° Reception';
        }
        field(41; "Receipt Line No."; Integer)
        {
            Caption = 'N° Reception ligne';
        }
        field(50; Description; Text[50])
        {
            Caption = 'Description';
        }
        field(100; "Date Litige"; Date)
        {
            Caption = 'Date Litige';
        }
        field(110; "Litige Prix"; Boolean)
        {
            Caption = 'Litige Prix';
        }
        field(111; "Liitige Quantité"; Boolean)
        {
            Caption = 'Liitige Quantité';
        }
        field(120; "Action"; Code[20])
        {
            Caption = 'Action';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ACTION_LITIGE'));
        }
        field(125; Statut; Code[20])
        {
            Caption = 'Statut';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('STATUT_LITIGE'));
        }
        field(130; "Closed Date"; Date)
        {
            Caption = 'Date Cloture';
        }
        field(131; "document régularisation"; Code[20])
        {
            Caption = 'document régularisation';
        }
        field(132; "référence régularisation"; Code[20])
        {
            Caption = 'référence régularisation';
        }
        field(133; "Date de création"; Date)
        {
            Caption = 'Date de création';
        }
        field(134; "N° BL fournisseur"; Code[35])
        {
            Caption = 'N° BL fournisseur';
        }
        field(50000; Ecart; Decimal)
        {
            Caption = 'Ecart';
            Description = 'MIG2015';
            Editable = false;
        }
        field(50801; "Ref. Active 2"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Ref. Active';
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(PKey1; "Whse Receipt No.", "Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

