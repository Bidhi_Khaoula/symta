table 50010 "Shipement Packaging"
{

    fields
    {
        field(1; "Sales Shipement no"; Code[20])
        {
            Caption = 'No bon livraison';
        }
        field(2; "Packaging Code"; Code[20])
        {
            Caption = 'Code embalage';
            NotBlank = true;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('EMBALLAGE'));
        }
        field(3; Qte; Decimal)
        {
        }
        field(4; weight; Decimal)
        {
            Caption = 'Poids';
        }
    }

    keys
    {
        key(PKey1; "Sales Shipement no", "Packaging Code")
        {
            Clustered = true;
            SumIndexFields = Qte;
        }
    }

    fieldgroups
    {
    }
}

