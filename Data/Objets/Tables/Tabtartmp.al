table 50036 "Tab_tar_tmp"
{

    fields
    {
        field(1; Societe; Code[10])
        {
        }
        field(2; Status; Code[10])
        {
        }
        field(3; c_fournisseur; Code[20])
        {
        }
        field(4; c_etab; Code[10])
        {
        }
        field(5; ref_active; Code[20])
        {
        }
        field(6; ref_fourni; Code[20])
        {
        }
        field(7; design_fourni; Code[50])
        {
            Description = '// CFR le 13/04/2022 => R‚gie : [design_fourni] passe de 30 … 50';

        }
        field(8; designation; Code[30])
        {
        }
        field(9; poids; Decimal)
        {
        }
        field(10; unite_achat; Code[10])
        {
        }
        field(11; condit_achat; Code[10])
        {
        }
        field(12; px_brut; Decimal)
        {
            AutoFormatType = 2;
        }
        field(13; px_net; Decimal)
        {
            AutoFormatType = 2;
        }
        field(14; date_tarif; Date)
        {
        }
        field(15; variante_prix; Code[10])
        {
        }
        field(16; comodity_code; Code[10])
        {
        }
        field(17; code_remise; Code[10])
        {
        }
        field(18; ligne_produit; Code[10])
        {
        }
        field(19; code_gestion; Code[10])
        {
        }
        field(20; code_lexique; Code[10])
        {
        }
        field(21; code_fonction; Code[10])
        {
        }
        field(22; code_origine; Code[10])
        {
        }
        field(23; code_retour; Code[10])
        {
        }
        field(24; divers1; Code[20])
        {
        }
        field(25; divers2; Code[20])
        {
        }
        field(26; divers3; Code[20])
        {
        }
        field(27; divers4; Code[20])
        {
        }
        field(28; divers5; Code[20])
        {
        }
        field(29; qte_col; Integer)
        {
        }
        field(30; qte_col1; Integer)
        {
        }
        field(31; prix_brut1; Decimal)
        {
            AutoFormatType = 2;
        }
        field(32; qte_col2; Integer)
        {
        }
        field(33; prix_brut2; Decimal)
        {
            AutoFormatType = 2;
        }
        field(34; qte_col3; Integer)
        {
        }
        field(35; prix_brut3; Decimal)
        {
            AutoFormatType = 2;
        }
        field(150; date_tarif_2; Code[20])
        {
        }
        field(151; Date_Traitement; Date)
        {
        }
    }

    keys
    {
        key(PKey1; Societe, Status, c_fournisseur, ref_active)
        {
            Clustered = true;
        }
        key(FKey2; ref_active)
        {
        }
        key(FKey3; ref_active, c_fournisseur)
        {
        }
        key(FKey4; ref_fourni)
        {
        }
        key(FKey5; c_fournisseur)
        {
        }
        key(FKey6; Societe, Status, c_fournisseur, ref_fourni)
        {
        }
    }

    fieldgroups
    {
    }
}

