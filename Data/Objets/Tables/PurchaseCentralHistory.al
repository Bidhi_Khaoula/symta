table 50071 "Purchase Central History"
{
    Caption = 'Purchase Central History';
    DataCaptionFields = "Customer No.", "Customer Name";
    Permissions = TableData "Purchase Central History" = rimd;

    fields
    {
        field(10; "Customer No."; Code[20])
        {
            Caption = 'No.';
            TableRelation = Customer;
        }
        field(15; "Customer Name"; Text[100])
        {
            CalcFormula = Lookup(Customer.Name WHERE("No." = FIELD("Customer No.")));
            Caption = 'Name';
            Editable = false;
            FieldClass = FlowField;
        }
        field(20; "Starting Date"; Date)
        {
            Caption = 'Starting Date';
        }
        field(25; "Ending Date"; Date)
        {
            Caption = 'Ending Date';
        }
        field(50040; "Active Central"; Code[10])
        {
            Caption = 'Centrale active';
            Description = 'MC Le 18-04-2011 => SYMTA -> TARIFS VENTES';
            TableRelation = Customer;
        }
        field(50041; "Groupement payeur"; Boolean)
        {
            Description = 'FB Le 26-04-2011 => SYMTA';
        }
        field(50045; "Active Central name"; Text[100])
        {
            CalcFormula = Lookup(Customer.Name WHERE("No." = FIELD("Active Central")));
            Caption = 'Nom centrale active';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50087; "Libre 3"; Code[20])
        {
            Caption = 'Code adhérent';
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
        }
    }

    keys
    {
        key(Key1; "Customer No.", "Starting Date")
        {
            Clustered = true;
        }
        key(Key2; "Ending Date")
        {
        }
    }

    fieldgroups
    {
    }
}

