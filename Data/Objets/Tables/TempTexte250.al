table 50035 "Temp Texte 250"
{

    fields
    {
        field(1; Tmp; Text[250])
        {
            Description = 'Temporaire pour les tarifs fournisseurs';
        }
        field(50000; Ref_Fourn; Code[50])
        {
            Description = 'MCO Le 08-02-2017 => Régie : Pour détecter les doublons avant maj tarif';
        }
    }

    keys
    {
        key(PKey1; Tmp)
        {
            Clustered = true;
        }
        key(FKey2; Ref_Fourn)
        {
        }
    }

    fieldgroups
    {
    }
}

