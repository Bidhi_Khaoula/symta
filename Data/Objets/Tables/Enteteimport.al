table 50001 "Entete import"
{

    fields
    {
        field(1; num_ecriture; Integer)
        {
        }
        field(2; num_rgl; Text[10])
        {
        }
        field(3; soc; Text[10])
        {
        }
        field(4; exer; Text[10])
        {
        }
        field(5; broui; Text[10])
        {
        }
        field(6; journ; Text[10])
        {
        }
        field(7; oper; Text[10])
        {
        }
        field(8; decr; Date)
        {
        }
        field(9; ref; Text[30])
        {
        }
        field(10; dech; Date)
        {
        }
        field(11; c_user; Text[30])
        {
        }
        field(12; nbre_ecri; Integer)
        {
        }
        field(13; c_type_rgl; Text[30])
        {
        }
        field(14; date_tire; Date)
        {
        }
        field(15; ref_tire; Text[30])
        {
        }
        field(16; "Montant debit"; Decimal)
        {
            CalcFormula = Sum("Ligne import".mont WHERE(num_ecriture = FIELD(num_ecriture),
                                                         sens = CONST('D')));
            Editable = false;
            FieldClass = FlowField;
        }
        field(17; "Montant credit"; Decimal)
        {
            CalcFormula = Sum("Ligne import".mont WHERE(num_ecriture = FIELD(num_ecriture),
                                                         sens = CONST('C')));
            Editable = false;
            FieldClass = FlowField;
        }
        field(18; dfac; Date)
        {
        }
        field(19; Nom; Text[60])
        {
            Editable = false;
            FieldClass = Normal;
        }
    }

    keys
    {
        key(PKey1; journ, num_ecriture)
        {
            Clustered = true;
        }
        key(FKey2; Nom)
        {
        }
    }

    fieldgroups
    {
    }

}

