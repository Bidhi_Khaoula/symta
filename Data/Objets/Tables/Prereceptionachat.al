table 50044 "Pre-reception achat"
{

    fields
    {
        field(1; "N° fournisseur"; Code[20])
        {
            NotBlank = true;
        }
        field(2; "N° document"; Code[20])
        {
            NotBlank = true;
        }
        field(3; "N° Ligne"; Integer)
        {
        }
        field(4; "Réference fournisseur"; Code[20])
        {
        }
        field(5; "Code Article"; Code[20])
        {
        }
        field(6; "Ref. Active"; Code[20])
        {
        }
        field(7; Designation; Text[50])
        {
        }
        field(8; Quantite; Decimal)
        {
        }
        field(9; "Cout Net"; Decimal)
        {
        }
        field(10; "Montant Ligne"; Decimal)
        {
            Editable = false;
        }
        field(11; "Recherche Référence"; Code[20])
        {
            TableRelation = Item;
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(12; "Type de commande"; Option)
        {
            Caption = 'Order Type';
            Description = 'MC Le 11-05-2011 => SYMTA -> Gestion des couts.';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(13; "Cout brut"; Decimal)
        {
        }
        field(14; Remise; Decimal)
        {
        }
        field(15; "Remise 1"; Decimal)
        {
        }
        field(16; "Remise 2"; Decimal)
        {
        }
        field(20; Commentaire; Text[80])
        {
            Description = 'MCO Le 08-06-2017 => Régie';
        }
    }

    keys
    {
        key(Key1; "N° fournisseur", "N° document", "N° Ligne")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

