table 50052 "difference_stocks_physique_emp"
{
    DataPerCompany = false;
    LinkedObject = true;

    fields
    {
        field(1; No_; Code[10])
        {
            Editable = false;
        }
        field(2; QE; Decimal)
        {
            Editable = false;
        }
        field(3; QP; Decimal)
        {
            Editable = false;
        }
    }

    keys
    {
        key(PKey1; No_)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

