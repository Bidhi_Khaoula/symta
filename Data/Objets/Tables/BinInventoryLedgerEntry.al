table 50072 "Bin Inventory Ledger Entry"
{
    Caption = 'Ecritures inventaire emplacement';

    fields
    {
        field(10; "Posting Date"; Date)
        {
            Caption = 'Posting Date';
        }
        field(20; "Document No."; Code[20])
        {
            Caption = 'Document No.';
        }
        field(30; "Location Code"; Code[10])
        {
            Caption = 'Location Code';
            TableRelation = Location;
        }
        field(40; "Bin Code"; Code[20])
        {
            Caption = 'Bin Code';
        }
        field(50; "Salespers./Purch. Code"; Code[10])
        {
            Caption = 'Salespers./Purch. Code';
            TableRelation = "Salesperson/Purchaser";
        }
        field(60; "Number Of Phys. Inv. Entry"; Integer)
        {
            CalcFormula = Count("Phys. Inventory Ledger Entry" WHERE("Posting Date" = FIELD("Posting Date"),
                                                                      "Document No." = FIELD("Document No."),
                                                                      "Location Code" = FIELD("Location Code"),
                                                                      "Bin Code" = FIELD("Bin Code")));
            Caption = 'Nombre écriture article';
            FieldClass = FlowField;
        }
        field(70; "Journal Template Name"; Code[10])
        {
            Caption = 'Journal Template Name';
            TableRelation = "Item Journal Template";
        }
        field(80; "Journal Batch Name"; Code[10])
        {
            Caption = 'Journal Batch Name';
            TableRelation = "Item Journal Batch".Name WHERE("Journal Template Name" = FIELD("Journal Template Name"));
        }
    }

    keys
    {
        key(PKey1; "Posting Date", "Document No.", "Location Code", "Bin Code")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

