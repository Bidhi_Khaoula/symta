table 50076 "Demande transporteur"
{
    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'N° Demande';
        }
        field(2; Statut; Option)
        {
            OptionMembers = Ouvert,"En cours","Enlevé","Livré","Cloturé";
        }
        field(3; Type; Option)
        {
            OptionMembers = "Expédition","Réception";
        }
        field(4; "Packing List No."; Code[20])
        {
            Caption = 'N° Packing List';
            TableRelation = "Packing List Header" WHERE("Sell-to Customer No." = FIELD("Customer No."));
        }
        field(5; "Created By"; Code[50])
        {
            Caption = 'Crée par';
            Editable = false;
        }
        field(6; "No. Series"; Code[20])
        {
        }
        field(10; "Created At"; Date)
        {
            Caption = 'Date Création';
            Editable = false;
        }
        field(11; "Date Départ"; Date)
        {
            Caption = 'Date départ prévue';
        }
        field(12; "Expected Delivery Date"; Date)
        {
            Caption = 'Date livraison prévue';
        }
        field(13; "Delivery Date"; Boolean)
        {
            Caption = 'Date livraison';
        }
        field(20; "Customer No."; Code[20])
        {
            Caption = 'N° Client';
            TableRelation = Customer;
        }
        field(21; "Vendor No."; Code[20])
        {
            Caption = 'N° Fournisseur';
            TableRelation = Vendor;
        }
        field(31; "Ship-to Name"; Text[50])
        {
            Caption = 'Ship-to Name';
        }
        field(32; "Ship-to Name 2"; Text[50])
        {
            Caption = 'Ship-to Name 2';
        }
        field(33; "Ship-to Address"; Text[50])
        {
            Caption = 'Ship-to Address';
        }
        field(34; "Ship-to Address 2"; Text[50])
        {
            Caption = 'Ship-to Address 2';
        }
        field(35; "Ship-to City"; Text[30])
        {
            Caption = 'Ship-to City';
            TableRelation = "Post Code".City;
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(36; "Ship-to Post Code"; Code[20])
        {
            Caption = 'Ship-to Post Code';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(37; "Ship-to Country/Region Code"; Code[10])
        {
            Caption = 'Ship-to Country/Region Code';
            TableRelation = "Country/Region";
        }
        field(38; "Ship-to Phone"; Text[30])
        {
            Caption = 'Téléphone';
        }
        field(39; "Ship-to E-mail"; Text[50])
        {
            Caption = 'Email';
        }
        field(40; "Shipment Method Code"; Code[10])
        {
            Caption = 'Shipment Method Code';
            TableRelation = "Shipment Method";
        }
        field(41; "Shipping Vendor No."; Code[10])
        {
            AccessByPermission = TableData 5790 = R;
            Caption = 'Shipping Agent Code';
            TableRelation = Vendor;
        }
        field(42; "Shipping Vendor Name"; Text[30])
        {
            Caption = 'Nom transporteur';
        }
        field(43; "Package Tracking No."; Text[30])
        {
            Caption = 'Package Tracking No.';
        }
        field(44; "Shipping Invoice No."; Text[30])
        {
            Caption = 'N° Facture transporteur';
        }
        field(45; "Incoterm City"; Text[50])
        {
            Caption = 'Ville incoterm';
        }
        field(46; "Shipping Email"; Text[50])
        {
            Caption = 'Email transporteur';
        }
        field(50; "Cout Transport"; Decimal)
        {
            Caption = 'Coût Transport HT';
        }
        field(51; "Frais douane"; Decimal)
        {
            Caption = 'Frais Douane HT';
        }
        field(52; "Frais annexe"; Decimal)
        {
            Caption = 'Frais Annexe HT';
        }
        field(53; "Frais emballage"; Decimal)
        {
            Caption = 'Frais Emballage HT';
        }
        field(54; "Cout Global"; Decimal)
        {
            Caption = 'Cout Global HT';
        }
        field(55; "Valeur transport vendu"; Decimal)
        {
            Caption = 'Valeur transport vendu HT';
        }
        field(56; "Frais Taxe Gasoil"; Decimal)
        {
            Caption = 'Frais Taxe Gasoil HT';
        }
        field(57; "Currency Code"; Code[10])
        {
            Caption = 'Devise';
            InitValue = 'EUR';
            TableRelation = Currency;
        }
        field(60; Commentaire; BLOB)
        {
            SubType = Memo;
        }
        field(70; "Facturé"; Boolean)
        {
        }
        field(71; "Code Appro"; Code[10])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CODE_APPRO'));
        }
        field(80; "Nombre Unite"; Integer)
        {
            Caption = 'Nombre d''unité';
            Description = 'CFR le 18/04/2024 - Régie';
        }
    }

    keys
    {
        key(Key1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

