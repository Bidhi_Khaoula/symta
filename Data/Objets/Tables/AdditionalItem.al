Table 50065 "Additional Item"
{
    Caption = 'Additional Item';

    FIELDS
    {
        field(1; "Item No."; Code[20])
        {
            TableRelation = Item;
            Caption = 'No.';
            NotBlank = True;
        }
        field(10; "Additional Item No."; Code[20])
        {
            TableRelation = Item;
            Caption = 'Item No.';
            NotBlank = True;
        }
        field(11; Commentaire; Text[50])
        {
            Caption = 'Description';
        }
        field(20; Quantity; Decimal)
        {
            Caption = 'Quantité';
            MinValue = 0;
        }
        field(100; "Item Description"; Text[100])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item.Description WHERE("No." = FIELD("Item No.")));
            Caption = 'Description article';
            Editable = False;
        }
        field(101; "Item Description 2"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."Description 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Description 2 article';
            Editable = False;
        }
        field(102; "Item No. 2"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Référence active article';
            Editable = False;
        }
        field(105; "Additional Item Description"; Text[100])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item.Description WHERE("No." = FIELD("Additional Item No.")));
            Caption = 'Description article complémentaire';
            Editable = False;
        }
        field(106; "Additional Item Description 2"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."Description 2" WHERE("No." = FIELD("Additional Item No.")));
            Caption = 'Description 2 article complémentaire';
            Editable = False;
        }
        field(107; "Additional Item No. 2"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Additional Item No.")));
            Caption = 'Référence active article complémentaire';
            Editable = False;
        }
        field(200; "Create User ID"; Code[40])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRAC';
            Editable = False;
        }
        field(201; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Editable = False;
        }
        field(202; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Editable = False;
        }
        field(210; "Modify User ID"; Code[40])
        {
            Caption = 'Code utilisateur modif. Cde';
            Editable = False;
        }
        field(211; "Modify Date"; Date)
        {
            Caption = 'Date modif. Cde';
            Editable = False;
        }
        field(212; "Modify Time"; Time)
        {
            Caption = 'Heure modif. Cde';
            Editable = False;
        }
        field(50050; "Additional Search Reference"; Code[40])
        {
            TableRelation = Item;
            ValidateTableRelation = false;
            Caption = 'Recherche référence complémentaire';
            Description = 'Copie du champ de la table 37';
        }
    }
    KEYS
    {
        key(Pkey1; "Item No.", "Additional Item No.")
        {
            Clustered = True;
        }

    }
}