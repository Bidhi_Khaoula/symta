table 50026 "EDI Ordes Header"
{

    fields
    {
        field(1; "EDI Document Type"; Code[20])
        {
            Caption = 'Type Document EDI';
        }
        field(2; "EDI Document No"; Code[25])
        {
            Caption = 'No Document EDI';
        }
        field(10; "EDI Document Date"; Date)
        {
            Caption = 'Date Document EDI';
        }
        field(11; "Delivery Date"; Date)
        {
            Caption = 'Date de livraison';
        }
        field(20; "Sell-To Customer EDI Code"; Code[20])
        {
            Caption = 'Code EDI Client Donneur D''ordre';
        }
        field(25; "Ship-To Customer EDI Code"; Code[15])
        {
            Caption = 'Code EDI Client Liraison';
        }
        field(50; "Currency Code"; Code[10])
        {
            Caption = 'Cde Devise';
        }
        field(55; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Code Transporteur';
        }
        field(56; "Shipping Agent Name"; Text[50])
        {
            Caption = 'Nom Transporteur';
        }
        field(80; "Discount Amount"; Decimal)
        {
            Caption = 'Montant Remise';
        }
        field(81; "Discount %"; Decimal)
        {
            Caption = '% Remise';
        }
        field(82; "Discount % 2"; Decimal)
        {
            Caption = '% Remise 2';
        }
        field(85; Amount; Decimal)
        {
            Caption = 'Montant Total';
        }
        field(100; "Comment 1"; Text[250])
        {
            Caption = 'Commentaire 1';
        }
        field(101; "Comment 2"; Text[250])
        {
            Caption = 'Commentaire 2';
        }
        field(102; "Comment 3"; Text[250])
        {
            Caption = 'Commentaire 3';
        }
        field(103; "Comment 4"; Text[250])
        {
            Caption = 'Commentaire 4';
        }
        field(105; "Comment 5"; Text[250])
        {
            Caption = 'Commentaire 5';
        }
        field(120; "Delivery instructions 1"; Text[250])
        {
            Caption = 'Instructions de livraison 1';
        }
        field(121; "Delivery instructions 2"; Text[250])
        {
            Caption = 'Instructions de livraison 2';
        }
        field(122; "Delivery instructions 3"; Text[250])
        {
            Caption = 'Instructions de livraison 3';
        }
        field(123; "Delivery instructions 4"; Text[250])
        {
            Caption = 'Instructions de livraison 4';
        }
        field(124; "Delivery instructions 5"; Text[250])
        {
            Caption = 'Instructions de livraison 5';
        }
        field(125; "Delivery instructions 6"; Text[250])
        {
            Caption = 'Instructions de livraison 6';
        }
        field(140; "Delivery Term 1"; Text[250])
        {
            Caption = 'Condition de livraison 1';
        }
        field(141; "Delivery Term 2"; Text[250])
        {
            Caption = 'Condition de livraison 3';
        }
        field(1000; "Integration status"; Boolean)
        {
            Caption = 'Statut Intégration';
        }
        field(1001; "Integration Date"; Date)
        {
            Caption = 'Date Intégration';
        }
        field(1002; "Integration Time"; Time)
        {
            Caption = 'Heure Intégration';
        }
        field(1003; "Integration User"; Code[20])
        {
            Caption = 'Utilisateur Intégration';
        }
        field(1008; "Integration Document No."; Code[20])
        {
            Caption = 'N° Document Intégré';
        }
        field(1020; "Edition status"; Boolean)
        {
            Caption = 'Statut Edition';
        }
        field(1021; "Edition Date"; Date)
        {
            Caption = 'Date Edition';
        }
        field(1022; "Edition Time"; Time)
        {
            Caption = 'Heure Edition';
        }
        field(1023; "Edition User"; Code[20])
        {
            Caption = 'Utilisateur Edition';
        }
    }

    keys
    {
        key(PKey1; "EDI Document Type", "EDI Document No")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

