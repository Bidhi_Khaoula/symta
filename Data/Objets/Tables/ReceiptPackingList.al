table 50079 "Receipt Packing List"
{
    fields
    {
        field(1; "Demande No."; Code[10])
        {
        }
        field(2; "Line No."; Integer)
        {
            AutoIncrement = true;
        }
        field(3; "Type colisage"; Option)
        {
            OptionCaption = 'Palette, Carton, "Mètres planchers", Complet';
            OptionMembers = Palette,Carton,Metres,Complet;
        }
        field(4; Longeur; Decimal)
        {
            Caption = 'Longeur (m)';
        }
        field(5; Largeur; Decimal)
        {
            Caption = 'Largeur (m)';
        }
        field(6; Hauteur; Decimal)
        {
            Caption = 'Hauteur (m)';
        }
        field(7; Poids; Decimal)
        {
        }
        field(8; "Quantité"; Decimal)
        {
        }
    }

    keys
    {
        key(PKey1; "Demande No.", "Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

