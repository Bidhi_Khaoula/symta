table 50041 "Teliae UM Detail"
{
    fields
    {
        field(1; "Reference UM"; Code[20])
        {
            Caption = 'Référence colis';
        }
        field(2; "Type UM"; Option)
        {
            Caption = 'Type Colis';
            OptionCaption = 'Cardboard,Pallet';
            OptionMembers = Cardboard,Pallet;
        }
        field(10; "Weight UM (Kg)"; Decimal)
        {
            Caption = 'Poids colis (kg)';
        }
        field(20; "Volume UM (m3)"; Decimal)
        {
            Caption = 'Volume colis (m3)';
        }
        field(30; "Length UM (cm)"; Decimal)
        {
            Caption = 'Length UM (cm)';
        }
        field(40; "Width UM (cm)"; Decimal)
        {
            Caption = 'Width UM (cm)';
        }
        field(50; "Height UM (cm)"; Decimal)
        {
            Caption = 'Height UM (cm)';
        }
        field(60; "Information UM"; Text[70])
        {
            Caption = 'Information colis';
        }
        field(70; "No. EEE"; Code[20])
        {
            Caption = 'N° EEE';
        }
        field(80; "No. BP"; Code[20])
        {
            Caption = 'N° BP';
        }
    }

    keys
    {
        key(PKey1; "Reference UM")
        {
            Clustered = true;
        }
        key(FKey2; "No. BP")
        {
            SumIndexFields = "Weight UM (Kg)";
        }
    }

    fieldgroups
    {
    }
}

