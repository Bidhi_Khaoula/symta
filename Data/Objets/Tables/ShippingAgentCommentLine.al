table 50075 "Shipping Agent Comment Line"
{
    Caption = 'Ligne commentaire transporteur';

    fields
    {
        field(10; "Shipping Agent Code"; Code[10])
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";
        }
        field(20; "Beginning Date"; Date)
        {
            Caption = 'Date de début';
        }
        field(30; "Ending Date"; Date)
        {
            Caption = 'Date de fin';
        }
        field(35; Department; Code[2])
        {
            Caption = 'Département';
        }
        field(40; Comment; Text[80])
        {
            Caption = 'Comment';
        }
    }

    keys
    {
        key(PKey1; "Shipping Agent Code", "Beginning Date", "Ending Date", Department)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

