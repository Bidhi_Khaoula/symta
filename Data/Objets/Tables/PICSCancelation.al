table 50073 "PICS Cancelation"
{
    Caption = 'Annulation PICS';

    fields
    {
        field(1; "WSH No."; Code[20])
        {
            Caption = 'No.';
        }
        field(2; "WSH Source No."; Code[20])
        {
            Caption = 'Source No.';
            Editable = false;
        }
        field(3; "WSH Source Document"; Option)
        {
            Caption = 'Source Document';
            Editable = false;
            OptionCaption = ',Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output';
            OptionMembers = ,"Sales Order",,,"Sales Return Order","Purchase Order",,,"Purchase Return Order","Inbound Transfer","Outbound Transfer","Prod. Consumption","Prod. Output";
        }
        field(4; "WSH Assigned User ID"; Code[50])
        {
            Caption = 'Assigned User ID';
        }
        field(5; "WSH Assignment Date"; Date)
        {
            Caption = 'Assignment Date';
            Editable = false;
        }
        field(6; "WSH Assignment Time"; Time)
        {
            Caption = 'Assignment Time';
            Editable = false;
        }
        field(100; "SH Ship-to Name"; Text[50])
        {
            Caption = 'Sell-to Customer Name 2';
            Editable = false;
        }
        field(101; "SH Ship-to City"; Text[30])
        {
            Caption = 'Sell-to City';
            Editable = false;
        }
        field(102; "SH Ship-to Post Code"; Code[20])
        {
            Caption = 'Sell-to Post Code';
            Editable = false;
        }
        field(200; "Cancel User ID"; Code[50])
        {
            Caption = 'Assigned User ID';
        }
        field(201; "Cancel Date"; Date)
        {
            Caption = 'Date annulation';
        }
        field(202; "Cancel Time"; Time)
        {
            Caption = 'Heure annulation';
        }
        field(203; "Cancel DateTime"; DateTime)
        {
            Caption = 'Date et Heure d''annulation';
        }
    }

    keys
    {
        key(Key1; "WSH No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

