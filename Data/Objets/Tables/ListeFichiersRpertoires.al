table 50006 "Liste Fichiers / Répertoires"
{

    fields
    {
        field(1; "No."; Integer)
        {
        }
        field(2; Path; Text[250])
        {
        }
        field(3; "Is a File"; Boolean)
        {
        }
        field(4; Name; Text[250])
        {
        }
        field(5; Size; Integer)
        {
        }
        field(6; Date; Date)
        {
        }
        field(7; Time; Time)
        {
        }
    }

    keys
    {
        key(PKey1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

