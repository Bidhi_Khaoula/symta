table 50008 "Contrat RFA"
{

    fields
    {
        field(1; "Code Contrat RFA"; Code[20])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_CONTRAT_RFA'));
        }
        field(2; "Line No."; Integer)
        {
            Caption = 'N° Ligne';
        }
        field(10; "Type Ligne"; Option)
        {
            OptionMembers = Inclusion,Exclusion;
        }
        field(15; Type; Option)
        {
            Caption = 'Type';
            OptionMembers = Tous,Marque,Famille,"Sous Famille",Article;
        }
        field(16; "Code"; Code[10])
        {
            Caption = 'Code';
            TableRelation = IF (Type = CONST(Marque)) Manufacturer.Code
            ELSE
            IF (Type = CONST(Famille)) "Item Category".Code
            ELSE
            IF (Type = CONST("Sous Famille")) "Item Category".Code
            ELSE
            IF (Type = CONST(Article)) Item."No.";
        }
        field(20; "Starting Date"; Date)
        {
            Caption = 'Starting Date';
            Description = '==> Analyse complémentaire';
        }
        field(21; "Ending Date"; Date)
        {
            Caption = 'Ending Date';
            Description = '==> Analyse complémentaire';
        }
    }

    keys
    {
        key(PKey1; "Code Contrat RFA", "Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }

}

