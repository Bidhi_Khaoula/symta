table 50028 "EDI Orders Line"
{

    fields
    {
        field(1; "EDI Document Type"; Code[20])
        {
            Caption = 'Type Document EDI';
        }
        field(2; "EDI Document No"; Code[25])
        {
            Caption = 'N° Document EDI';
        }
        field(3; "EDI Document Line No."; Integer)
        {
            Caption = 'N° Ligne Docuement EDI';
        }
        field(10; "Item ean"; Code[15])
        {
            Caption = 'Ean Article';
        }
        field(11; "Item No"; Text[20])
        {
            Caption = 'N° Article';
        }
        field(15; "Description 1"; Text[50])
        {
            Caption = 'Désignation 1';
        }
        field(16; "Description 2"; Text[50])
        {
            Caption = 'Désignation 2';
        }
        field(20; Vat; Code[10])
        {
            Caption = 'TVA';
        }
        field(30; Quantity; Decimal)
        {
            Caption = 'Quantité';
        }
        field(35; Package; Decimal)
        {
            Caption = 'Colis';
        }
        field(40; "Net price"; Decimal)
        {
            Caption = 'Prix net';
        }
        field(41; "Gross Price"; Decimal)
        {
            Caption = 'Prix brut';
        }
        field(45; "Discount 1"; Decimal)
        {
            Caption = 'Remise 1';
        }
        field(46; "Discount 2"; Decimal)
        {
            Caption = 'Remise 2';
        }
        field(100; "Comment 1"; Text[250])
        {
            Caption = 'Commentaire 1';
        }
        field(101; "Comment 2"; Text[250])
        {
            Caption = 'Commentaire 2';
        }
        field(102; "Comment 3"; Text[250])
        {
            Caption = 'Commentaire 3';
        }
        field(103; "Comment 4"; Text[250])
        {
            Caption = 'Commentaire 4';
        }
        field(104; "Comment 5"; Text[250])
        {
            Caption = 'Commentaire 5';
        }
        field(1008; "Integration Document No."; Code[20])
        {
            Caption = 'N° Document Intégré';
        }
        field(1009; "Integration Document Line No."; Integer)
        {
            Caption = 'N° Ligne Document Intégré';
        }
    }

    keys
    {
        key(PKey1; "EDI Document Type", "EDI Document No", "EDI Document Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

