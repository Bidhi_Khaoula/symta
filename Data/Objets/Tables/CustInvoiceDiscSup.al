table 50014 "Cust. Invoice Disc. Sup."
{
    Caption = 'Remise Client supplémentaire';

    fields
    {
        field(1; "Customer No."; Code[20])
        {
            Caption = 'Code client';
            TableRelation = Customer;
        }
        field(2; "Ligne Type"; Option)
        {
            Caption = 'Type ligne';
            OptionCaption = 'Header,Ligne';
            OptionMembers = Entete,Ligne;
        }
        field(3; "Gl Account No."; Code[10])
        {
            Caption = 'N° de compte comptable';
            TableRelation = "G/L Account" WHERE("Account Type" = CONST(Posting),
                                                 "Direct Posting" = CONST(True));
        }
        field(4; Description; Text[50])
        {
            Caption = 'Désignation';
        }
        field(5; "Pourcent Disc."; Decimal)
        {
            Caption = 'Pourcentage remise';
        }
    }

    keys
    {
        key(PKey1; "Customer No.", "Ligne Type", "Gl Account No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

