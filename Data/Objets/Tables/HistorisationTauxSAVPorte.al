table 50007 "Historisation Taux SAV / Porte"
{
    PasteIsValid = false;

    fields
    {
        field(1; "Type Ligne"; Option)
        {
            Editable = false;
            OptionMembers = "SAV Client","SAV Article","Portefeuille Commande","SAV Fournisseur","Valeur du stock";
        }
        field(2; "No."; Code[20])
        {
            Caption = 'N°';
            Editable = false;
            TableRelation = IF ("Type Ligne" = CONST("SAV Client")) Customer."No."
            ELSE
            IF ("Type Ligne" = CONST("SAV Article")) Item."No."
            ELSE
            IF ("Type Ligne" = CONST("SAV Fournisseur")) Vendor."No.";
        }
        field(5; Date; Date)
        {
            Caption = 'Date';
            Editable = true;
        }
        field(6; Heure; Time)
        {
            Caption = 'Heure';
            Editable = false;
        }
        field(10; "Taux Sav"; Decimal)
        {
            Caption = 'Taux Sav';
            Editable = false;
        }
        field(15; Montant; Decimal)
        {
        }
        field(16; Marge; Decimal)
        {
        }
        field(17; Qte; Decimal)
        {
        }
        field(30; "Montant Base"; Decimal)
        {
        }
        field(31; "Marge Base"; Decimal)
        {
        }
        field(32; "Qte Base"; Decimal)
        {
        }
    }

    keys
    {
        key(PKey1; "Type Ligne", "No.", Date, Heure)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

