enum 50000 "Contact Address Format"
{
    Extensible = true;

    value(0; First)
    {
        Caption = 'First';
    }
    value(1; "After Company Name")
    {
        Caption = 'After Company Name';
    }
    value(2; Last)
    {
        Caption = 'Last';
    }
    value(3; "Ne pas afficher")
    {
        Caption = 'Ne pas afficher';
    }
}