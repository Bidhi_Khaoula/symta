enum 50008 "Line type"
{
    value(0; Normal)
    {
        caption = 'Normal';
    }
    value(1; "Réparation")
    {
        caption = 'Réparation';
    }
    value(2; Echange)
    {
        caption = 'Echange';
    }
    value(3; SAV)
    {
        caption = 'SAV';
    }
    value(4; Gratuit)
    {
        caption = 'Gratuit';
    }
}