enum 50005 "Quote Type"
{
    Extensible = true;

    value(0; Devis)
    {
        Caption = 'Devis';
    }
    value(1; Proforma)
    {
        Caption = 'Proforma';
    }
}