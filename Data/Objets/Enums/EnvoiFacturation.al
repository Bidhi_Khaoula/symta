enum 50003 "Envoi Facturation"
{
    value(0; Papier)
    {
        Caption = 'Papier';
    }
    value(1; "E-Mail")
    {
        Caption = 'E-Mail';
    }
    value(2; "Papier + E-Mail")
    {
        Caption = 'Papier + E-Mail';
    }
    value(3; "1 Fac par E-Mail")
    {
        Caption = '1 Fac par E-Mail';
    }
    value(4; "Non Envoye")
    {
        Caption = 'Non Envoye';
    }
}