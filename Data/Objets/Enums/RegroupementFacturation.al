enum 50001 "Regroupement Facturation"
{

    value(0; Aucun)
    {
        Caption = 'Aucun';
    }
    value(1; "1 Facture pas donneur d'ordre")
    {
        Caption = '1 Facture pas donneur d''ordre';
    }
    value(2; "1 Facture par client payeur")
    {
        Caption = '1 Facture par client payeur';
    }
}