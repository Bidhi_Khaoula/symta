enum 50004 "Mode Expédition"
{

    value(0; " ")
    {
        caption = ' ';
    }
    value(1; Messagerie)
    {
        caption = 'Messagerie';
    }
    value(2; Express)
    {
        caption = 'Express';
    }
    value(3; "Contre Rembourssement Normal")
    {
        caption = 'Contre Rembourssement Normal';
    }
    value(4; "Contre Rembourssement Express")
    {
        caption = 'Contre Rembourssement Express';
    }
    value(5; "Express Samedi")
    {
        caption = 'Express Samedi';
    }
    value(6; Affretement)
    {
        caption = 'Affretement';
    }
    value(7; "Contre Rembourssement Samedi")
    {
        caption = 'Contre Rembourssement Samedi';

    }
}