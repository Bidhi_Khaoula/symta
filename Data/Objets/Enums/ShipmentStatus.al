enum 50002 "Shipment Status"
{
    value(0; "A Livrer")
    {
        caption = 'A Livrer';
    }
    value(1; Reliquat)
    {
        caption = 'Reliquat';
    }
    value(2; "Livrée")
    {
        caption = 'Livrée';
    }
}