enum 50009 "Description Status"
{
    value(0; Open)
    {
        Caption = 'Ouvert';
    }
    value(1; Released)
    {
        Caption = 'Lancé';
    }
    value(2; "Pending Approval")
    {
        Caption = 'Approbation suspendue';
    }
    value(3; "Pending Prepayment")
    {
        Caption = 'Acompte suspendu';
    }
    value(4; Preparate)
    {
        Caption = 'A préparer';
    }
    value(5; Bloqued)
    {
        Caption = 'Bloquée';
    }
}