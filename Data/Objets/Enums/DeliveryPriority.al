enum 50006 "Delivery Priority"
{
    Extensible = true;
    value(0; "1 : urgent")
    {
        Caption = '1 : urgent';
    }
    value(1; "2 : Normal")
    {
        Caption = '2 : Normal';
    }
}