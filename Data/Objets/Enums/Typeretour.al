enum 50007 "Type retour"
{
    Extensible = true;

    value(0; Retour)
    {
        Caption = 'Retour';
    }
    value(1; Garantie)
    {
        Caption = 'Garantie';
    }
}