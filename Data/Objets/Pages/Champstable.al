page 50261 "Champs table"
{
    Caption = 'Table Fields';
    PageType = List;
    SourceTable = Field;
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; Rec."No.")
                {
                    ApplicationArea = All;
                }
                field("Field Caption"; Rec."Field Caption")
                {
                    Caption = 'Field caption';
                    ApplicationArea = All;
                }
                field(FieldName; Rec.FieldName)
                {
                    Caption = 'Field name';
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
    }

    procedure Recup_Filtre(var _pFieldFilter: Record Field)
    var
        LFiltreTmp: Record Field;
    begin
        LFiltreTmp.MARKEDONLY := TRUE;
        IF LFiltreTmp.FINDFIRST() THEN
            _pFieldFilter.COPY(LFiltreTmp)
        ELSE
            _pFieldFilter.COPY(Rec)
    end;
}

