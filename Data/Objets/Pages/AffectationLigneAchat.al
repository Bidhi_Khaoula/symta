page 50071 "Affectation Ligne Achat"
{
    CardPageID = "Affectation Ligne Achat";
    PageType = List;
    SourceTable = "Affectation Ligne Achat";
    ApplicationArea = all;
    layout
    {
        area(content)
        {
            repeater(content1)
            {
                ShowCaption = false;
                field("Sales Document No."; Rec."Sales Document No.")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
    }
}

