tableextension 50051 "Sales & Receivables Setup" extends "Sales & Receivables Setup" //311
{
    fields
    {
        field(50001; "Shipment label Nos."; Code[10])
        {
            Caption = 'N° Etiquette Expédition';
            Description = 'ESKAPE MODULE ETIQUETTE TRANSPORT';
            TableRelation = "No. Series";
        }
        field(50002; "Shipment label Entry Nos."; Code[10])
        {
            Caption = 'N° Ecriture Etiquette Expédition';
            Description = 'ESKAPE MODULE ETIQUETTE TRANSPORT';
            TableRelation = "No. Series";
        }
        field(50003; "Alerte sur Relance"; Boolean)
        {
        }
        field(50004; "Niveau de relance pour alerte"; Integer)
        {
        }
        field(50005; "Delai de securité (Paiement)"; DateFormula)
        {
        }
        field(50006; "Teliae Export Folder"; Text[250])
        {
            Caption = 'Teliae Export Folder';
            Description = 'CFR 27/04/2020 - FE20190930';
        }
        field(50007; "Teliae Error Mail"; Text[100])
        {
            Caption = 'Teliae Error Mail';
            Description = 'CFR 27/04/2020 - FE20190930';
        }
        field(50008; "Teliae Return Folder"; Text[250])
        {
            Caption = 'Teliae Return Folder';
            Description = 'CFR 27/04/2020 - FE20190930';
        }
        field(50009; "Package Nos."; Code[20])
        {
            Caption = 'Package Nos.';
            Description = 'CFR 27/04/2020 - FE20190930';
            TableRelation = "No. Series";
        }
        field(50010; "Shipment Damage Nos."; Code[10])
        {
            Caption = 'Order Nos.';
            Description = 'AD Le 30-09-2011 => Gestion des incidents transport';
            TableRelation = "No. Series";
        }
        field(50015; "Longueur Alerte Dimension"; Decimal)
        {
            Caption = 'Longueur Alerte Dimension (en m)';
            Description = 'AD Le 24-03-2016 =>';
        }
        field(50020; "Code Client Reprise Express"; Code[20])
        {
            Caption = 'Code Client Remise Express';
            Description = 'CFR le 18/11/2020 : Régie > remises express\';
            TableRelation = Customer;
        }
        field(50030; "Quote Default Validity Period"; DateFormula)
        {
            Caption = 'Délai de validité par défaut des devis';
            Description = '// CFR le 04/10/2023 - Régie : Date de validité des devis';
        }
        field(50040; "Quote Max Validity Period"; DateFormula)
        {
            Caption = 'Délai de validité maximum des devis';
            Description = '// CFR le 04/10/2023 - Régie : Date de validité des devis';
        }

        field(50050; "Coef. Saisonnalité Janvier"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50051; "Coef. Saisonnalité Février"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50052; "Coef. Saisonnalité Mars"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50053; "Coef. Saisonnalité Avril"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50054; "Coef. Saisonnalité Mai"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50055; "Coef. Saisonnalité Juin"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50056; "Coef. Saisonnalité Juillet"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50057; "Coef. Saisonnalité Aout"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50058; "Coef. Saisonnalité Septembre"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50059; "Coef. Saisonnalité Octobre"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50060; "Coef. Saisonnalité Novembre"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50061; "Coef. Saisonnalité Decembre"; Decimal)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion de la saisonnalité pour les objectifs';
        }
        field(50080; "Do not search description"; Boolean)
        {
            Caption = 'Do not search description';
            Description = 'MCO Le 14-02-2018 => Régie';
        }
        field(50500; "Loans Nos"; Code[10])
        {
            Caption = 'N° prêt';
            Description = '//Souce de N° de prêt//';
            TableRelation = "No. Series";
        }
        field(50501; "Loans Loacation"; Code[20])
        {
            Caption = 'Magasin de prêt';
            Description = '//MAgsin de prêt//';
            TableRelation = Location;
        }
        field(50750; "Due Date 1"; Date)
        {
            Caption = 'Due Date';
            Description = 'FB -> SYMTA';
        }
        field(50751; "Due Date 2"; Date)
        {
            Caption = 'Due Date';
            Description = 'FB -> SYMTA';
        }
        field(50752; "Due Date 3"; Date)
        {
            Caption = 'Due Date';
            Description = 'FB -> SYMTA';
        }
        field(50753; "Pourcentage échéance 1"; Decimal)
        {
            Description = 'FB -> SYMTA';
        }
        field(50754; "Pourcentage échéance 2"; Decimal)
        {
            Description = 'FB -> SYMTA';
        }
        field(60000; "Sales LCY"; Decimal)
        {
            Caption = 'Sales LCY';
            Description = 'WF le 28-04-2010 => FARGROUP -> eCommerce';
        }
        field(60001; "Sales LCY Period"; DateFormula)
        {
            Caption = 'Sales LCY Period';
            Description = 'WF le 28-04-2010 => FARGROUP -> eCommerce';
        }
        field(60002; "Shipment Method"; Code[10])
        {
            Caption = 'Méthode de livraison';
            TableRelation = "Shipment Method";
        }
    }

}

