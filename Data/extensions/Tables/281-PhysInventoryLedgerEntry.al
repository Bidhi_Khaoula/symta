tableextension 50021 "Phys. Inventory Ledger Entry" extends "Phys. Inventory Ledger Entry" //281
{
    fields
    {
        Field(50002; "No. 2"; Code[20])
        {
            Caption = 'No. 2';
            Description = '// CFR le 24/06/2022 - SFD20210929 : Inventaires, suivi de la référence active';
        }
        Field(50005; "Product Type"; Code[10])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_TYPE'));
            Caption = 'Type Produit';
            Description = 'GRI le 21/09/2022 SFD20210929';
        }
        Field(50403; "Bin Code"; Code[20])
        {
            TableRelation = Bin;
            Caption = 'Bin Code';
            Description = '// CFR le 16/03/2022 - SFD20210929 : Inventaires, suivi du Code emplacement';
        }
        Field(50500; "Warehouse User ID"; Code[50])
        {
            Caption = 'Assigned User ID';
            Description = '// CFR le 05/04/2022 - SFD20210929 : Inventaires, suivi du magasinier';
        }
        Field(50701; "Manufacturer Code"; Code[10])
        {
            TableRelation = Manufacturer;
            Caption = 'Manufacturer Code';
            Description = 'GRI le 21/09/2022 SFD20210929';
        }
        Field(50702; "Item Category Code"; Code[20])
        {
            TableRelation = "Item Category";
            Caption = 'Item Category Code';
            Description = 'GRI le 21/09/2022 SFD20210929';
        }

    }

}