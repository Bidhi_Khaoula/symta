tableextension 50107 "Return Shipment Line" extends "Return Shipment Line" //6651
{
    // // CFR le 14/09/2021 => Régie : Ajout champ 50150 Idem AD Le 04-12-2012 sur table 121
    fields
    {
        field(50150; "Déjà Extraite sur Avoir"; Boolean)
        {
            CalcFormula = Exist("Purchase Line" WHERE("Document Type" = CONST("Credit Memo"),
                                                       "Return Shipment No." = FIELD("Document No."),
                                                       "Return Shipment Line No." = FIELD("Line No.")));
            Description = 'CFR le 14/09/2021 > Idem AD Le 04-12-2012 sur table 121';
            FieldClass = FlowField;
        }
        field(50210; "Whse Receipt No."; Code[20])
        {
            Caption = 'N° Réception entrepôt';
            Description = 'CFR le 25/10/2023 - Régie';
            Editable = false;
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
    }
}

