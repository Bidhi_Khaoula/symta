tableextension 50091 "Item Substitution" extends "Item Substitution" //5715
{
    fields
    {
        field(50000; "Affichage WEB"; Boolean)
        {
            Description = 'MC Le 05-09-2012 => Permet d''inclure certains substituts dans l''export WEB';
        }
        field(50801; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("No.")));
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50802; "Substitute Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Substitute No.")));
            Caption = 'Ref. Active substitut';
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50900; "Create User ID"; Code[20])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
        field(50901; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
        field(50902; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
        field(50905; "Modify User ID"; Code[20])
        {
            Caption = 'Code utilisateur Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
        field(50906; "Modify Date"; Date)
        {
            Caption = 'Date Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
        field(50907; "Modify Time"; Time)
        {
            Caption = 'Heure Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
        }
    }
}

