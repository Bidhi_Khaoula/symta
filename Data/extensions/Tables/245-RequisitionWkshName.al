tableextension 50020 "Requisition Wksh. Name" extends "Requisition Wksh. Name" //245
{
    fields
    {
        field(50000; "Last Item No."; Code[20])
        {
            Caption = 'Dernière référence appro';
            Description = 'CFR le 18/10/2022 => Pour se replacer sur la ref. de la dernière Appro';
        }

    }
}
