tableextension 50128 "Posted Whse. Shipment Line" extends "Posted Whse. Shipment Line" //7323
{
    fields
    {
        field(50000; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 02-11-2015 =>';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50060; "Qté Ouverte avant validation"; Decimal)
        {
            Description = '// AD Le 04-11-2009 => Pour stocker la quantité qui devait être livrée (pour le taux de service notament)';
            Editable = true;
        }
        field(50151; "Gerer par groupement"; Boolean)
        {
            Description = 'FB Le 26-04-2011 => Symta';
        }
        field(50152; Weight; Decimal)
        {
            Caption = 'Weight';
            DecimalPlaces = 0 : 5;
            Description = '// MC Le 04-05-2011 => On fauit suivre le champ depuis la table Expédition Magasin.';
        }
    }
    keys
    {
    }
}

