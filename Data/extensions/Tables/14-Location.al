tableextension 50028 Location extends Location //14
{
    fields
    {
        field(50000; "Emplacement Tampon WIIO"; Code[10])
        {
            Description = 'WIIO ->';
            TableRelation = Bin.Code WHERE("Location Code" = FIELD(Code));
        }
        field(50500; "Loan Location"; Boolean)
        {
            Caption = 'Magasin de prêt';
            Description = '//Notion de magasin de prêt//';
        }
        field(50501; "Autoriser emplacement"; Boolean)
        {
            Description = '// MC Le 28-04-2011 => SYMTA -> Gestion des zones.';
        }
    }
}

