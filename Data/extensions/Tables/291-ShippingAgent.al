tableextension 50048 "Shipping Agent" extends "Shipping Agent" //291
{
    fields
    {
        field(50001; "Label Report ID"; Integer)
        {
            Caption = 'Report ID';
            Description = 'ET';
        }
        field(50003; "Label Folder"; Text[180])
        {
            Caption = 'Répertoire étiquette';
            Description = 'ET';
        }
        field(50004; "Form of Transport"; Integer)
        {
            Caption = 'Bordereau de tranport';
            Description = 'ET';
            TableRelation = Object.ID WHERE(Type = CONST("Report"));
        }
        field(50010; "Date imperative Saturday only"; Boolean)
        {
            Caption = 'Date impérative uniquement samedi';
        }
        field(50020; "Poids colis maximum"; Decimal)
        {
            Description = 'MCO Le 04-10-2018 => Régie';
        }
        field(50021; "Maximum Length Item"; Decimal)
        {
            Caption = 'Maximum Length Item';
            Description = 'CFR 08/07/2019 : FE20190624 > En (cm) pour être compatible avec Item Unit Of Mesure';
        }
        field(50022; "Alert Package Weight"; Decimal)
        {
            Caption = 'Poids alerte colis';
            Description = 'CFR le 11/05/2022 => Régie : Poids d''alerte';
        }
        field(50030; "Recomended Color"; Option)
        {
            Caption = 'Couleur transporteur préconisé';
            OptionMembers = " ",Bleu,Rouge,Vert,Orange,Gris;
            Description = 'CFR le 06/04/2022 => Régie : Couleur transporteur préconisé pour coloration ligne vente';
        }
        field(50031; "No Delivery On Saturday"; Boolean)
        {
            Caption = 'Pas de livraison le samedi';
            Description = 'CFR le 30/11/2022 => Régie : blocage de certains mode d''expédition';
        }
        field(50032; "Mandatory Non Blocking"; Boolean)
        {
            Caption = 'Transporteur non bloquant';
            Description = 'CFR le 30/11/2022 => Régie : transporteur obligatoire non bloquant';
        }

    }
}

