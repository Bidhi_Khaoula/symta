tableextension 50003 "Payment Status" extends "Payment Status" //10861
{
    fields
    {
        field(50000; Montant; Decimal)
        {
            CalcFormula = Sum("Payment Line".Amount WHERE("Payment Class" = FIELD("Payment Class"),
                                                           "Status No." = FIELD(Line),
                                                           "Copied To No." = CONST()));
            FieldClass = FlowField;
        }
    }
}

