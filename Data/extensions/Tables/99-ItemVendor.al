tableextension 50147 "Item Vendor" extends "Item Vendor" //99
{
    fields
    {
        field(50000; "Purch. Unit of Measure"; Code[10])
        {
            Caption = 'Purch. Unit of Measure';
            Description = 'AD Le 11-05-2011 => SYMTA ->';
            TableRelation = "Item Unit of Measure".Code WHERE("Item No." = FIELD("Item No."));
        }
        field(50001; "Minimum Order Quantity"; Decimal)
        {
            Caption = 'Minimum Order Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 11-05-2011 => SYMTA ->';
            MinValue = 0;
        }
        field(50002; "Order Multiple"; Decimal)
        {
            Caption = 'Order Multiple';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 11-05-2011 => SYMTA ->';
            MinValue = 0;
        }
        field(50003; "Purch. Check Qty Max"; Decimal)
        {
            Caption = 'Controle Quantite Maxi Achat';
            Description = 'AD Le 11-05-2011 => SYMTA ->';
        }
        field(50004; "Purch. Multiple"; Decimal)
        {
            Caption = 'Multiple d''achat';
            Description = 'AD Le 11-05-2011 => SYMTA ->';
        }
        field(50025; "Last Direct Cost"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Last Direct Cost';
            Description = 'AD Le 11-05-2011 => SYMTA ->';
            MinValue = 0;
        }
        field(50028; "Indirect Cost"; Decimal)
        {
            Caption = 'Indirect Cost %';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 11-05-2011 => SYMTA ->';
            MinValue = 0;
        }
        field(50050; "Code Remise"; Code[10])
        {
            Description = 'AD Le 11-05-2011 => SYMTA ->';
        }
        field(50060; "Statut Qualité"; Option)
        {
            Description = 'AD Le 31-05-2011 => SYMTA ->';
            OptionMembers = Conforme,"Non Conforme","A contrôler","Contrôle en cours";
        }
        field(50061; "Statut Approvisionnement"; Option)
        {
            Description = 'AD Le 31-05-2011 => SYMTA ->';
            OptionMembers = "Non Bloqué","Bloqué","Plus Fourni";
        }
        field(50080; "Date Mise a Jour"; Date)
        {
            Description = 'AD Le 11-05-2011 => SYMTA ->';
        }
        field(50100; "Vendor Item Desciption"; Text[50])
        {
            Caption = 'Vendor Item No.';
            Description = 'AD Le 11-05-2011 => SYMTA ->';

        }
        field(50101; "date maj statut appro"; Date)
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
        }
        field(50102; "date maj statut qualité"; Date)
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
        }
        field(50103; "user maj statut appro"; Code[20])
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
        }
        field(50104; "user maj statut qualité"; Code[20])
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
        }
        field(50105; "Ref. Active"; Code[20])
        {
            Editable = false;
        }
        field(50106; "Vendor Name"; Text[100])
        {
            CalcFormula = Lookup(Vendor.Name WHERE("No." = FIELD("Vendor No.")));
            Description = 'MCO Le 18-01-2016 => SYMTA -> Demande de Nathalie';
            Editable = false;
            FieldClass = FlowField;
            Caption = 'Vendor Name';
        }
        field(50107; "Pourchase Price Number"; Integer)
        {
            FieldClass = FlowField;
            CalcFormula = Count("Purchase Price" WHERE("Item No." = FIELD("Item No."),
                                                    "Vendor No." = FIELD("Vendor No.")));
            Caption = 'Pourchase Price Number';
            Description = 'CFR Le 07/10/2019 => FE20180726 : catalogue fournisseur sans lien avec tarif Achat';
        }
        field(50200; Comment; Text[250])
        {
            Caption = 'Commentaire';
            Description = 'CFR le 23/03/2023 => Régie : Ajout Champ [Comment]';
        }

    }
    keys
    {
        key(FKey1; "Vendor Item No.")
        {
        }
    }
}

