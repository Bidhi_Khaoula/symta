tableextension 50131 "Phys. Invt. Item Selection" extends "Phys. Invt. Item Selection" //7380
{
    keys
    {
        key(FKey1; "Location Code", "Phys Invt Counting Period Code", "Item No.", "Variant Code")
        {
        }
        key(FKey2; "Location Code", "Phys Invt Counting Period Code", "Last Counting Date", "Item No.", "Variant Code")
        {
        }
    }
}

