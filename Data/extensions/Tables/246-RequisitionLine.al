tableextension 50037 "Requisition Line" extends "Requisition Line" //246
{
    fields
    {
        field(50000; "Reservé Calculé"; Decimal)
        {
            Description = '// AD Le 23-09-2009 => Reservé au moment du lancement du calcul';
            Editable = false;
        }
        field(50003; "Attendu Calculé"; Decimal)
        {
            Description = '// AD Le 23-09-2009 => Attendu au moment du lancement du calcul';
            Editable = false;
        }
        field(50007; "Délai Calculé"; Decimal)
        {
            Description = '// AD Le 23-09-2009 => Délai au moment du lancement du calcul';
            Editable = false;
        }
        field(50010; "Stock Calculé"; Decimal)
        {
            Description = '// AD Le 23-09-2009 => Stock au moment du lancement du calcul';
            Editable = false;
        }
        field(50012; "Projection Mini"; Decimal)
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50013; "Projection Maxi"; Decimal)
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50014; "Conso Mois En Cours"; Decimal)
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50015; "Mini Encours"; Decimal)
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50016; Formule; Decimal)
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50017; "Fusion en attente sur"; Code[20])
        {
            Description = 'AD Le 31-05-2011 => SYMTA';
            Editable = false;
        }
        field(50020; "Chaine Mois"; Text[30])
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50021; "Type Comparaison"; Text[30])
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50022; "Type Projection"; Text[30])
        {
            Description = 'AD Le 23-05-2011 => SYMTA';
            Editable = false;
        }
        field(50050; "Type de commande"; Option)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Gestion des couts.';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50051; "Date création fiche"; Date)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Date de création de l''article';
        }
        field(50052; "Date dernère sortie"; Date)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Date de dernière sortie de l''article';
        }
        field(50053; Stocke; Boolean)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Stocké de l''article';
        }
        field(50054; "Ref. Active"; Code[20])
        {
            Editable = false;
        }
        field(50055; "Fournisseur Calculé"; Code[20])
        {
            Editable = false;
        }
        field(50060; "Indice Jauge"; Integer)
        {
            Editable = false;
        }
        field(50062; "Code Appro"; Code[10])
        {
            CalcFormula = Lookup(Item."Code Appro" WHERE("No." = FIELD("No.")));
            Description = 'MCO Le 07-06-2017 => Régie';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CODE_APPRO'));
        }
        field(50080; "Buffer Price Record ID"; RecordID)
        {
            Description = 'MCO Le 04-10-2018 => Régie';
        }
        field(50081; "Price Qty"; Decimal)
        {
            Description = 'MCO Le 04-10-2018 => Régie';
        }
        field(50082; "Price Selectionned"; Boolean)
        {
            Description = 'MCO Le 04-10-2018 => Régie';
        }
        field(50100; FiltreFournisseurAExclure; Text[250])
        {
            Caption = 'Filtre Fournisseur A Exclure';
            Description = 'CFR le 18/10/2022 => Régie';
        }
        field(50101; TriEcartMarge; Decimal)
        {
            Caption = 'Tri Suivant Ecart Marge';
            Description = 'CFR le 18/10/2022 => Régie';
        }
        field(50705; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }

    }
    keys
    {
        key(FKey1; "Worksheet Template Name", "Journal Batch Name", "Vendor No.", "Line No.")
        {
            MaintainSQLIndex = true;
        }
        key(FKey2; "Vendor No.")
        {
            MaintainSQLIndex = false;
            SumIndexFields = "Cost Amount";
        }
        key(Key3; "Fournisseur Calculé", "Ref. Active")
        {
        }
    }
}

