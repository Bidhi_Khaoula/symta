tableextension 50023 "Return Shipment Header" extends "Return Shipment Header" //6650
{
    fields
    {
        field(50002; "Order Dispatch Date"; Date)
        {
            Caption = 'Date d''envoi commande';
            Description = 'CFR le 10/05/2022 => Régie : date purement informative';
        }
        field(50060; "Incoterm City"; Text[50])
        {
            Caption = 'Ville Incoterm ICC 2020';
            Description = 'CFR le 03/09/2021 - SFD20210201 Incoterm 2020';
        }

    }

}