tableextension 50099 "Warehouse Setup" extends "Warehouse Setup" //5769
{
    fields
    {
        field(50000; "Différence poids autorisée"; Decimal)
        {
            Description = 'AD Le 04-05-2007 => Pour le cartouche BL';
        }
        field(50001; "Limite Poids Pièces Encombrant"; Decimal)
        {
            Description = 'AD Le 26-05-2011 => SYMTA';
        }
        field(50002; "Empl. Etiquettes Prépara"; Text[250])
        {
            Description = 'AD Le 26-05-2011 => SYMTA -> Répertoire de dépose des étiquettes de préparation';
        }
        field(50003; "Code Emballage par def"; Code[20])
        {
            Description = 'AD Le 01-12-2011 => SYMTA';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('EMBALLAGE'));
        }
        field(50010; "Chemin Frm Etiquette NiceLabel"; Text[250])
        {
            Description = 'AD Le 29-01-2015';
        }
        field(50011; "Chemin Etiquette NiceLabel"; Text[250])
        {
            Description = 'AD Le 29-01-2015';
        }
        field(60010; "Packing List Nos."; Code[20])
        {
            AccessByPermission = TableData 60006 = R;
            Caption = 'N° liste de colisage';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
            TableRelation = "No. Series";
        }
        field(60011; "Item PL Report No."; Integer)
        {
            Caption = 'N° de report liste de colisage par article';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
            TableRelation = AllObjWithCaption."Object ID" WHERE("Object Type" = CONST(Report),
                                                                 "Object ID" = FILTER(49999 .. 999999));
        }
        field(60012; "Package PL Report No."; Integer)
        {
            Caption = 'N° de report liste de colisage par colis';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
            TableRelation = AllObjWithCaption."Object ID" WHERE("Object Type" = CONST(Report),
                                                                 "Object ID" = FILTER(49999 .. 999999));
        }
    }
}

