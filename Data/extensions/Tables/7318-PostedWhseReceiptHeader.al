tableextension 50123 "Posted Whse. Receipt Header" extends "Posted Whse. Receipt Header" //7318
{
    fields
    {
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50014; "Date Arrivage Marchandise"; Date)
        {
            Description = 'FBRUN le 12/08/20 => FE20200728';
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50025; "Frais LCY"; Decimal)
        {
            Caption = 'Frais DS';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en Eur';
            Editable = false;
        }
        field(50027; "Frais FCY"; Decimal)
        {
            Caption = 'Frais Devise Commande';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise';
            Editable = false;
        }
        field(50030; "Total origine"; Decimal)
        {
            CalcFormula = Sum("Posted Whse. Receipt Line"."Total Origine" WHERE("No." = FIELD("No.")));
            Description = 'CFR le 19/11/2020 : Régie > Récep. Entr. Enreg. (total origine)';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50050; "N° Fusion Réception"; Code[20])
        {
            Description = 'AD Le 16-05-2011 => SYMTA -> Récéption Entrepot';
            Editable = false;
            TableRelation = "Warehouse Receipt Header";

        }
    }
}

