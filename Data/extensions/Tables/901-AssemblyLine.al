tableextension 50139 "Assembly Line" extends "Assembly Line" //901
{
    fields
    {
        field(50801; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("No.")));
            Caption = 'Ref. Active';
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}

