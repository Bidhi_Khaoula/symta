tableextension 50101 "Shipping Agent Services" extends "Shipping Agent Services" //5790
{
    fields
    {
        field(50000; "Product Code"; Code[3])
        {
            Caption = 'Code Produit';
            Description = 'ET';
        }
        field(50001; Account; Code[10])
        {
            Caption = 'compte';
            Description = 'ET';
        }
        field(50002; Default; Boolean)
        {
            Caption = 'Defaut';
            Description = 'ET';
        }
        field(50003; Name; Text[50])
        {
            Caption = 'Nom';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50004; Address; Text[50])
        {
            Caption = 'Adresse';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50005; "Address 2"; Text[50])
        {
            Caption = 'Adresse (2ème ligne)';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50006; City; Text[30])
        {
            Caption = 'City';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50007; "Country/Region Code"; Code[10])
        {
            Caption = 'Country/Region Code';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
            TableRelation = "Country/Region";
        }
        field(50008; "Post Code"; Code[20])
        {
            Caption = 'Post Code';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50009; "Phone No."; Text[30])
        {
            Caption = 'Phone No.';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
            ExtendedDatatype = PhoneNo;
        }
        field(50010; "Fax No."; Text[30])
        {
            Caption = 'Fax No.';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50011; County; Text[30])
        {
            Caption = 'County';
            Description = 'GR Le 29-07-2015 => MIG2015';
        }
    }
    keys
    {//todo
        // key(FKey1; "Shipping Agent Code", Account, "Code")
        // {
        // }
    }

}

