tableextension 50145 "Comment Line" extends "Comment Line" //97
{
    fields
    {
        field(50000; "Afficher Multiconsultation"; Boolean)
        {
            Caption = 'Afficher Multiconsultation';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Pour Afficher dans le quid';
        }
        field(50001; "Afficher Appro"; Boolean)
        {
            Description = 'LM le 09-08-2012=>commentaire en appro';
        }
        field(50002; "No. 2"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("No.")));
            Caption = 'No. 2';
            Description = 'CFR Le 16/12/2020 => SYMTA -> Référence Active';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50110; "Print Wharehouse Shipment"; Boolean)
        {
            Caption = 'Impression exp. magasin';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50111; "Print Shipment"; Boolean)
        {
            Caption = 'Impression expédition';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50112; "Print Invoice"; Boolean)
        {
            Caption = 'Impression facture';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50113; "Display Sales Order"; Boolean)
        {
            Caption = 'Affichage commande Vente/Devis/Retour';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50114; "Print Order"; Boolean)
        {
            Caption = 'Impression commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50115; "Print Quote"; Boolean)
        {
            Caption = 'Impression devis';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50116; "Display Purch. Order"; Boolean)
        {
            Caption = 'Affichage commande Achat';
            Description = 'AD Le 08-07-2010 => ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50117; "Print Purch. Order"; Boolean)
        {
            Caption = 'Impression Commande achat';
            Description = 'MC Le 27-10-2011 =>ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50118; "Display Purch. Receipt"; Boolean)
        {
            Caption = 'Affichage Réception Magasin';
            Description = 'AD Le 02-11-2011 =>ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50119; "Display Web"; Boolean)
        {
            Caption = 'Affichage site web';
            Description = 'CFR Le 17/05/2021 => SFD20210423 : Commentaire article web';
        }
        field(50120; "Language Web"; Option)
        {
            Caption = 'Code langue web';
            Description = 'CFR Le 17/05/2021 => SFD20210423 : Commentaire article web';
            OptionMembers = " ",FR,EN;
        }
    }

}

