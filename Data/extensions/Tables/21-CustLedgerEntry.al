tableextension 50034 "Cust. Ledger Entry" extends "Cust. Ledger Entry" //21
{
    fields
    {
        field(50000; "Statement No."; Code[10])
        {
            Caption = 'N° Relevé';
            Description = 'REL Gestion des relevés';
        }
        field(50001; "Statement Date"; Date)
        {
            Caption = 'Date Relevé';
            Description = 'REL Gestion des relevés';
        }
        field(50002; "No. Printed Statement"; Integer)
        {
            Caption = 'Nbre impressions relevé';
            Description = 'REL Gestion des relevés';
        }
    }
    keys
    {
        key(Fkey1; "Customer No.", "Reason Code")
        {
        }
        key(Fkey2; "Customer No.", Open, "Posting Date")
        {
        }
        key(Fkey3; "Sell-to Customer No.", "Global Dimension 1 Code", "Global Dimension 2 Code", "Posting Date", "Currency Code")
        {
            SumIndexFields = "Sales (LCY)", "Profit (LCY)", "Inv. Discount (LCY)";
        }
        //todo
        // key(Fkey4; "Customer No.", "Statement No.")
        // {
        // }
        key(Fkey5; "Customer No.", "Applies-to ID", Open, "Due Date")
        {
        }
        key(Fkey6; Open, "Document Type")
        {
        }
    }
}

