tableextension 50035 Vendor extends Vendor //23
{
    fields
    {
        field(50000; "Délai ETD -> ETA"; DateFormula)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
        }
        field(50001; "Vendor Type"; Code[20])
        {
            Caption = 'Type Fournisseur';
            Description = 'AD Le 25-01-2010 => Pour différencier le type de fournisseur';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FOU_TYPE'));
        }
        field(50005; "Seuil Franco"; Decimal)
        {
            Description = 'AD Le 16-04-2012 =>';
        }
        field(50007; "Info Contact"; Text[50])
        {
            Description = 'AD Le 09-11-2009 => FARGROUP -> Champ réservé pour l''info de la fiche contact';
            Editable = false;
            Enabled = false;
        }
        field(50008; "Responsabilité"; Code[10])
        {
            Description = 'AD Le 09-11-2009 => FARGROUP -> Champ réservé pour l''info de la fiche contact';
            Editable = false;
            Enabled = false;
        }
        field(50010; "Vendor Disc. Group"; Code[10])
        {
            Caption = 'Customer Disc. Group';
            Description = 'AD Le 12-05-2011 => SYMTA';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FOU_GRPT_REMISE'));
        }
        field(50020; "Privilège_appro"; Boolean)
        {
            Description = 'LM Le 18-01-2013 => si oui, privilégie l''appro chez ce fournisseur (meme s''il est moins competitif)';
        }
        field(50036; "Masquer Ref. Active"; Boolean)
        {
            Description = 'AD Le 20-01-2014';
        }
        field(50037; "Masquer Logo Société"; Boolean)
        {
            Description = 'AD Le 20-01-2014';
        }
        field(50060; "Incoterm City"; Text[50])
        {
            Caption = 'Ville Incoterm';
            Description = 'CFR le 03/09/2021 - SFD20210201 Incoterm 2020';
        }
        field(50062; "Code Appro"; Code[10])
        {
            Description = 'ANI Le 16-06-2016 => FE20150424 acheteur fiche article';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CODE_APPRO'));

        }
        field(50070; "Taux S.A.V."; Decimal)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50071; "Date Taux S.A.V."; Date)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50100; Entete; Code[20])
        {
        }
        field(50101; CodeComptaDbx; Code[20])
        {
        }
        field(50150; "EORI Code"; Text[30])
        {
            Caption = 'Code EORI';
            Description = 'CFR le 19/11/2020 : Régie > Code EORI\';
        }
        field(50200; "Groupe Tarif Fournisseur"; Code[20])
        {
            Description = 'AD Le 08-09-2011 => SYMTA -> GESTION TAB_TAR -> Pour regrouper des fournisseurs';
            TableRelation = Vendor;
        }
        field(50210; "implement tarif par ref_active"; Boolean)
        {
            Description = 'LM le 14-09-2012 =>option implementation tarif achat';
        }
        field(50700; "Vendor EDI Code"; Code[20])
        {
            Caption = 'Code EDI Fournisseur';
            Description = 'EDI => Code EDI DU CLIENT (Gencode - Siret - Interne ,,,)';
        }
        field(50705; "Code Cde EDI"; Code[10])
        {
            Description = 'AD Le 29-09-2008 => ORDERS EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50706; "Date Début Export Cde EDI"; Date)
        {
            Description = 'AD Le 29-09-2008 => ORDERS EDI';
        }
        field(50710; "Code Reception EDI"; Code[10])
        {
            Description = 'AD Le 07-01-2008 => DESADV EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50711; "Date Début Export Recept. EDI"; Code[10])
        {
            Description = 'AD Le 07-01-2008 => DESADV EDI';
        }
        field(50720; "Code Facturation EDI"; Code[10])
        {
            Description = 'AD Le 29-09-2008 => Facturation EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50721; "Date Début Export Facture EDI"; Date)
        {
            Description = 'AD Le 29-09-2008 => Facturation EDI';
        }
        field(50730; "Erreur intégration"; Boolean)
        {
        }
        field(50801; "Solde DS a date"; Decimal)
        {
            CalcFormula = Sum("Detailed Vendor Ledg. Entry".Amount WHERE("Vendor No." = FIELD("No."),
                                                                          "Initial Entry Global Dim. 1" = FIELD("Global Dimension 1 Filter"),
                                                                          "Initial Entry Global Dim. 2" = FIELD("Global Dimension 2 Filter"),
                                                                          "Currency Code" = FIELD("Currency Filter"),
                                                                          "Posting Date" = FIELD(UPPERLIMIT("Date Filter"))));
            FieldClass = FlowField;
        }
        field(50802; "Purchases (LCY) Date Doc."; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = - Sum("Vendor Ledger Entry"."Purchase (LCY)" WHERE("Vendor No." = FIELD("No."),
                                                                             "Global Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                                                             "Global Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                                                             "Document Date" = FIELD("Date Filter"),
                                                                             "Currency Code" = FIELD("Currency Filter")));
            Caption = 'Purchases (LCY)';
            Description = 'AD Le 06-02-2015 => Achat sur la date de document';
            Editable = false;
            FieldClass = FlowField;
        }
        field(60051; "Supprimé"; Boolean)
        {
            Description = 'LM Le 13-06-2013 =>identifie les comptes supprimés pour adv et compta';
        }
        field(60052; SIRET; Text[14])
        {
            Description = 'GRI le 24/01/2024';
        }
    }
    keys
    {
        key(FKey1; "Vendor Type")
        {
        }
    }
}

