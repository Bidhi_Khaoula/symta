tableextension 50000 "Bank Account Ledger Entry" extends "Bank Account Ledger Entry" //271
{
    fields
    {
        field(60000; "Date de relevé"; Date)
        {
            FieldClass = FlowField;
            CalcFormula = Lookup("Bank Account Statement"."Statement Date" WHERE("Bank Account No." = FIELD("Bank Account No."),
                                                        "Statement No." = FIELD("Statement No.")));
        }
        field(60001; Select; Boolean)
        {
            caption = 'Selection';
        }
        field(90000; "Lock gn"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Exist("G/L Entry" WHERE("G/L Account No." = FILTER('512*'),
                         "Posting Date" = FIELD("Posting Date"),
                         "Document Type" = FIELD("Document Type"),
                         "Document No." = FIELD("Document No."),
                         "Source Type" = CONST("Bank Account"),
                         "Source No." = FIELD("Bank Account No.")));

        }
    }
}