tableextension 50093 Manufacturer extends Manufacturer //5720
{
    fields
    {
        field(50000; "Colonne Stats"; Boolean)
        {
            Description = 'AD Le 05-11-2011 => Est ce que la marque a une colonne dans la stat de Bernard';
        }
        field(50005; "Tri Stats"; Integer)
        {
            Description = 'CFR le 18/11/2020 : Régie > Tri';
        }
        field(50010; "Gen. Prod. Posting Group"; Code[10])
        {
            Caption = 'Gen. Prod. Posting Group';
            Description = 'ANI Le 14-04-2015 => FE20150402';
            TableRelation = "Gen. Product Posting Group";
        }
        field(50011; "Tariff No."; Code[10])
        {
            Caption = 'Tariff No.';
            Description = 'ANI Le 14-04-2015 => FE20150402';
            TableRelation = "Tariff Number";
        }
        field(50300; "Envoyé MINILOAD"; Boolean)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50301; "Date Envoi MINILOAD"; Date)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50302; "Heure Envoi MINILOAD"; Time)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50500; "Filtre Client"; Code[20])
        {
            FieldClass = FlowFilter;
            TableRelation = Customer;
        }
        field(50501; "Date Filter"; Date)
        {
            Caption = 'Date Filter';
            FieldClass = FlowFilter;
        }
        field(50502; "Ca Ecritures Valeurs"; Decimal)
        {
            CalcFormula = Sum("Value Entry"."Sales Amount (Actual)" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                           "Posting Date" = FIELD("Date Filter"),
                                                                           "Manufacturer Code" = FIELD(Code),
                                                                           "Source No." = FIELD("Filtre Client")));
            Description = 'AD Le 04-01-2012 => SYMTA -> Optimisé pour CA par le marque';
            FieldClass = FlowField;
        }
    }
    keys
    {
        key(Key1; "Tri Stats")
        {
        }
    }
}

