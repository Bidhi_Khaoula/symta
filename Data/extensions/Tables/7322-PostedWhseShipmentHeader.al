tableextension 50127 "Posted Whse. Shipment Header" extends "Posted Whse. Shipment Header" //7322
{
    fields
    {
        modify("Assigned User ID")
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('MAG_EMPLOYES'));
            Description = 'AD Le 30-10-2015 => Je change le lien car des utilisateurs ne sont pas dans le domaine';
        }
        field(50000; "Source No."; Code[20])
        {
            Caption = 'Source No.';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
        }
        field(50001; "Source Document"; Option)
        {
            Caption = 'Source Document';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
            OptionCaption = ',Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output';
            OptionMembers = ,"Sales Order",,,"Sales Return Order","Purchase Order",,,"Purchase Return Order","Inbound Transfer","Outbound Transfer","Prod. Consumption","Prod. Output";
        }
        field(50002; "Destination Type"; Option)
        {
            Caption = 'Destination Type';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
            OptionCaption = ' ,Customer,Vendor,Location,Item,Family,Sales Order';
            OptionMembers = " ",Customer,Vendor,Location,Item,Family,"Sales Order";
        }
        field(50003; "Destination No."; Code[20])
        {
            Caption = 'Destination No.';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
        }
        field(50004; Poids; Decimal)
        {
            Description = 'MC Le 02-05-2011 => Poids du BP.';
        }
        field(50005; "Mode d'expédition"; enum "Mode Expédition")
        {
            Description = 'MC Le 26-04-2011 => Définition du mode d''expédition. (Normal, Express ...)';
        }
        field(50008; Assurance; Boolean)
        {
            Description = 'AD Le 26-01-2012';
        }
        field(60200; "Packing List No."; Code[20])
        {
            Caption = 'N° liste de colisage';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
            TableRelation = "Packing List Header";
        }
    }
}

