tableextension 50045 "Bank Acc. Reconciliation Line" extends "Bank Acc. Reconciliation Line" //274
{
    fields
    {
        field(50000; Pointage; Boolean)
        {
            Description = 'Add on JLD - pointage';
        }
        field(60001; Select; Boolean)
        {
            Caption = 'Selection';
        }
    }
    keys
    {//TODO
        // key(Fkey1; "Bank Account No.", "Statement No.", Pointage)
        // {
        //     SumIndexFields = "Applied Amount", "Statement Amount";
        // }
    }
}

