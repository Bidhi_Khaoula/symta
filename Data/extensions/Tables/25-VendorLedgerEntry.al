tableextension 50038 "Vendor Ledger Entry" extends "Vendor Ledger Entry" //25
{
    keys
    {
        key(FKey1; "Vendor No.", "Reason Code")
        {
        }
        key(FKey2; "Vendor No.", "Posting Date", "Closed by Entry No.")
        {
        }
        key(FKey3; "Vendor No.", "Document Date")
        {
            SumIndexFields = "Purchase (LCY)", "Inv. Discount (LCY)";
        }
    }
}

