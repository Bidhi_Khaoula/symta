tableextension 50077 "Purchase Line Archive" extends "Purchase Line Archive" //5110
{
    fields
    {
        field(50000; "Initial Quantity"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'Editable=No';
        }
        field(50005; "Quantité Etiquette"; Integer)
        {
            Description = 'AD Le 31-10-2009 => FARGROUP -> Pre-reception -> Qte d''étiquette';
        }
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50022; "% Cout Indirect Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % de base standard Nav';
        }
        field(50023; "Frais Généraux Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Frais de base standard Nav';
        }
        field(50025; "Frais LCY"; Decimal)
        {
            Caption = 'Frais DS';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en Eur';
        }
        field(50027; "Frais FCY"; Decimal)
        {
            Caption = 'Frais Devise Commande';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise';
        }
        field(50028; "Frais FCY -> LCY"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise -> Conversion en eros';
            Editable = false;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50050; "Recherche référence"; Code[40])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            Editable = false;
            TableRelation = IF (Type = CONST("G/L Account")) "G/L Account"
            ELSE
            IF (Type = CONST(Item)) Item
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge";
            ValidateTableRelation = false;
        }
        field(50063; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
            Editable = false;
        }
        field(50071; Commentaires; Text[50])
        {
            Description = 'GRI le 22/06/23 - FA20230525';
        }
        field(50180; "Stocké"; Boolean)
        {
            CalcFormula = Lookup(Item.Stocké WHERE("No." = FIELD("No.")));
            Description = 'AD Le 06-02-2015 => SYMTA (Rien a voir avec la notion non stocké habituel) C''est pour les réappros';
            Editable = false;
            FieldClass = FlowField;
            InitValue = true;
        }
        field(50181; "Manufacturer Code"; Code[10])
        {
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("No.")));
            Caption = 'Manufacturer Code';
            Description = 'AD Le 06-02-2015 => SYMTA';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = Manufacturer;
        }
        field(50210; "Whse Receipt No."; Code[20])
        {
            Caption = 'N° Réception entrepôt';
            Description = 'CFR le 25/10/2023 - Régie';
            Editable = false;
        }
        field(50500; "Vendor Order No."; Code[35])
        {
            Caption = 'Vendor Order No.';
            Description = 'MC Le 27-09-2016 => Régie';
        }
        field(50501; "Type de commande"; Option)
        {
            Caption = 'Order Type';
            Description = 'MC Le 27-09-2016 => Régie';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
    }
}

