tableextension 50136 "Country/Region" extends "Country/Region" //9
{
    fields
    {
        field(50000; "Code pays DBX"; Code[10])
        {
        }
        field(50001; "Contact Address Format Spec"; Enum "Contact Address Format")
        {
            Caption = 'Format adresse contact Spec';
            InitValue = "After Company Name";
        }
    }
}

