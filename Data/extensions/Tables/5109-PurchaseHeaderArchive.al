tableextension 50076 "Purchase Header Archive" extends "Purchase Header Archive" //5109
{
    fields
    {
        field(50000; "Date de dépat estimée (ETD)"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
            Editable = false;
        }
        field(50001; "Date d'arrivée estimée (ETA)"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
            Editable = false;
        }
        field(50002; "Order Dispatch Date"; Date)
        {
            Caption = 'Date d''envoi commande';
            Description = 'CFR le 10/05/2022 => Régie : date purement informative';
        }
        field(50004; "Etat Marchandise"; Option)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
            Editable = false;
            OptionMembers = " ",Flottant,"A quai";
        }
        field(50005; "Heure réception"; Time)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
            Editable = false;
        }
        field(50010; "Qté Container 20"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50011; "Qté Container 40"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50012; "Qté Container HQ"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50013; "Qté Container MIX"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50020; "Nom du bateau"; Text[30])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50021; "Port livraison"; Text[30])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50025; "No Container"; Code[11])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50026; "No Plomb"; Code[7])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50030; "LC Number"; Code[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50031; "LC Date"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50032; "LC Banque"; Code[10])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50040; Document; Option)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            InitValue = Non;
            OptionMembers = Oui,Non,"Irregularité",Rien;
        }
        field(50041; "Priorité"; Enum "Delivery Priority")
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            InitValue = "2 : Normal";
        }
        field(50044; "Code Transitaire"; Code[10])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ACH-TRANSITAIRE'));
        }
        field(50050; "Type de commande"; Option)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Gestion des couts.';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50060; "Incoterm City"; Text[50])
        {
            Caption = 'Ville Incoterm';
            Description = 'CFR le 03/09/2021 - SFD20210201 Incoterm 2020';
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50200; "Date Envoi Fax/pdf"; Date)
        {
            Description = 'FAX';
            Editable = false;
        }
        field(50201; "Heure Envoi Fax/Pdf"; Time)
        {
            Description = 'FAX';
            Editable = false;
        }
        field(50202; "Utilisateur Envoi Fax/Pdf"; Code[20])
        {
            Description = 'FAX';
            Editable = false;
            TableRelation = User;
        }
        field(50203; "N° Fac"; Code[20])
        {
            Description = 'MC Le 15-11-2011 => Possibilité de voir le numéro de facture lors de l''affectation d''une ligne';
            Enabled = false;
        }
        field(50300; "Create User ID"; Code[20])
        {
            Description = 'MC Le 24-10-2011 => Information de création du record.';
        }
        field(50301; "Réceptionneur"; Code[20])
        {
            Description = 'MC Le 26-10-2011 => Suivi des informations de réception';
        }
    }
}

