tableextension 50026 "Sales Cue" extends "Sales Cue" //9053
{
    fields
    {
        field(50000; "Sales Orders - Open Symta"; Integer)
        {
            AccessByPermission = TableData "Sales Shipment Header" = R;
            CalcFormula = count("Sales Header" where("Document Type" = const(Order),
                                                      Status = const(Open),
                                                      "Responsibility Center" = field("Responsibility Center Filter"),
                                                      "Document Soldé" = const(false)));
            Caption = 'Sales Orders - Open';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}
