tableextension 50010 "Sales Invoice Line" extends "Sales Invoice Line" //113
{
    fields
    {
        field(50000; "Initial Quantity"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 28-08-2009 => Enregistre la quantité initialement prévu sur la commande en cas d''abandon de reliquat';
            Editable = false;
            Enabled = false;

        }
        field(50002; Kit; Boolean)
        {
            CalcFormula = Exist("BOM Component" WHERE("Parent Item No." = FIELD("No.")));
            Description = 'AD Le 02-11-2015 => Flowfield si c''est un Kit';
            Enabled = false;
            FieldClass = FlowField;
        }
        field(50003; "Blanket Order Quantity Outstan"; Decimal)
        {
            Caption = 'Qté Cde Ouverte Restante';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 21-10-2009 => Qte resstante a transformer en commande sur la commande ouverte  (reservation)';
            Editable = false;
            Enabled = false;
        }
        field(50006; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'MC Le 17-05-2010 =>Analyse complémentaire FARGROUP';
        }
        field(50007; "Dimension Hors Norme"; Boolean)
        {
            Caption = 'Dimension Hors Norme';
            Description = 'AD Le 24-03-2016';
            Enabled = false;
        }
        field(50010; "Line type"; Enum "Line type")
        {
            Caption = 'Type ligne';
            Description = 'AD Le 08-09-2009 => FARGROUP -> Type de ligne de commande';

        }
        field(50011; "Campaign No."; Code[20])
        {
            Caption = 'Campaign No.';
            Description = 'AD Le 16-11-2009 => Pour faire suivre une campagne à la ligne (Tarif + RFA)';
            Editable = true;
            TableRelation = Campaign;
        }
        field(50016; "Sans mouvement de stock"; Boolean)
        {
            Caption = 'Not stored';
            Description = 'AD Le 17-09-2009 => Non Stocké';
            Editable = false;
        }
        field(50020; Urgent; Boolean)
        {
            Description = 'CFR le 18/11/2020 : Régie > remises express';
            Enabled = false;
        }
        field(50040; "% Participation port"; Code[20])
        {
            Description = ' MC Le 27-04-2011 => SYMTA -> Transport';
            TableRelation = Resource;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50050; "Recherche référence"; Code[40])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            Editable = false;
            TableRelation = IF (Type = CONST(" ")) "Standard Text"
            ELSE
            IF (Type = CONST("G/L Account")) "G/L Account"
            ELSE
            IF (Type = CONST(Item)) Item
            ELSE
            IF (Type = CONST(Resource)) Resource
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge";
            ValidateTableRelation = false;
        }
        field(50051; "Référence saisie"; Code[40])
        {
            Description = 'MC Le 06-06-2011 => SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            Editable = false;
        }
        field(50052; "Originally Ordered No. Pour BL"; Code[20])
        {
            Caption = 'Originally Ordered No.';
            Description = 'AD Le 17-04-2012 => SYMTA -> Pour faire suivre dans les BLs';
            Editable = false;
        }
        field(50060; "Qté Ouverte avant validation"; Decimal)
        {
            Description = '// AD Le 04-11-2009 => Pour stocker la quantité qui devait être livrée (pour le taux de service notament)';
            Editable = true;
            Enabled = false;
        }
        field(50065; "Qte Extraire sur Retour"; Decimal)
        {
            Description = 'AD Le 27-09-2016 => REGIE -> Qte déja extraite sur des retours';
            Editable = false;
            Enabled = false;
        }
        field(50066; "RV Defective Location"; Code[10])
        {
            Caption = 'Localisation';
            Description = 'CFR le 30/11/2022 => Régie : Localisation des piŠces déffectueuses';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('RV_LOCATION'));
        }
        field(50070; "Additional Available"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Exist("Additional Item" WHERE("Item No." = FIELD("No.")));
            Caption = 'Substitution Available';
            Description = 'CFR le 24/09/2021 => SFD20210201 articles complémentaires';
            Editable = false;
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50107; "Net Unit Price"; Decimal)
        {
            BlankZero = true;
            Caption = 'Prix unitaire net';
            DecimalPlaces = 2 : 2;
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = false;
        }
        field(50110; "Print Wharehouse Shipment"; Boolean)
        {
            Caption = 'Impression exp. magasin';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50111; "Print Shipment"; Boolean)
        {
            Caption = 'Impression expédition';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50112; "Print Invoice"; Boolean)
        {
            Caption = 'Impression facture';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50113; "Display Order"; Boolean)
        {
            Caption = 'Affichage commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50114; "Print Order"; Boolean)
        {
            Caption = 'Impression commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50115; "Print Quote"; Boolean)
        {
            Caption = 'Impression devis';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50140; "Internal Line Type"; Option)
        {
            Caption = 'Type ligne interne';
            Description = 'AD Le 31-08-2009 => Gestion des type de ligne';
            OptionMembers = " ",Shipment,Packing,"Discount Header","Discount Line",RuptureBl,"Participation transport","RuptureClientLivré";
        }
        field(50141; "Transport Shipment No"; Code[20])
        {
            Caption = 'N° de livraison du port';
            Description = 'AD Le 31-08-2009 => Gestion des frais de port';
            Enabled = false;
        }
        field(50150; "A Traiter en préparation"; Decimal)
        {
            Caption = 'A Traiter en préparation';
            DecimalPlaces = 0 : 5;
            Enabled = false;
        }
        field(50151; "Gerer par groupement"; Boolean)
        {
            Description = 'FB Le 26-04-2011 => Symta';
        }
        field(50170; "Opération ADV"; Boolean)
        {
            Caption = 'Opération ADV';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            Editable = false;
        }
        field(50171; "Commission %"; Decimal)
        {
            Caption = '% Commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50172; "Commission % Calculate"; Decimal)
        {
            Caption = '% Commission Calculé';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50300; "N° client Livré"; Code[20])
        {
            Description = 'FB Le 10-10-2011 -> Suivi dans la facture pour imprimer par client livré';
            TableRelation = Customer;
        }
        field(50500; Loans; Boolean)
        {
            Caption = 'Prêt';
            Description = '//PRET//';
        }
        field(50501; "Qty To Receive Loans"; Decimal)
        {
            Caption = 'Qté prêt à recevoir';
            Description = '//Prêt Partie retour//';
            Enabled = false;
        }
        field(50502; "Qty To Receive Loans(Base)"; Decimal)
        {
            Caption = 'Qté prêt à recevoir(base)';
            Description = '//Prêt Partie retour//';
            Editable = false;
            Enabled = false;
        }
        field(50503; "Return Qty Loans"; Decimal)
        {
            Caption = 'Qté prêt reçu';
            Description = '//Prêt Partie retour//';
            Editable = false;
            Enabled = false;
        }
        field(50504; "Return Qty Loans(Base)"; Decimal)
        {
            Caption = 'Qté prêt reçu(base)';
            Description = '//Prêt Partie retour//';
            Editable = false;
            Enabled = false;
        }
        field(50700; "EDI Document Line"; Boolean)
        {
            Caption = 'Ligne de commande EDI';
            Description = 'EDI => Indique si c''est une ligne de commande EDI';
            Editable = false;
        }
        field(50701; "EDI Line No."; Integer)
        {
            Caption = 'N° de ligne EDI';
            Description = 'EDI => Numéro de la ligne dans l''EDI';
            Editable = false;
        }
        field(50702; "EDI Document No."; Code[20])
        {
            Caption = 'N° Document EDI';
            Description = 'EDI => N° du document EDI';
            Editable = false;
        }
        field(50710; "EDI Quantity"; Decimal)
        {
            Caption = 'Quantité EDI';
            Description = 'EDI => Quantité EDI';
            Editable = false;
        }
        field(50711; "EDI Net Price"; Decimal)
        {
            Caption = 'Prix Net EDI';
            Description = 'EDI => Prix EDI';
            Editable = false;
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
        field(50900; "Qty To Ship. Save"; Decimal)
        {
            Description = 'MCO Le 08-09-2017 => Régie Problème au niveau des lignes négatives sur expeditions ...';
            Enabled = false;
        }
        field(50920; "Marge %"; Decimal)
        {
            Caption = 'Marge / coût unitaire';
            Description = 'CFR le 04/10/2024 - Régie ajout du calcul de marge';
            Editable = false;
        }
        field(70000; "Ne pas recalculer abattement"; Boolean)
        {
            Description = 'MC Le 24-09-2012 => Ne pas recalculer abattement lors de copy document mgt';
            Enabled = false;
        }
    }
    keys
    {
        key(FKey1; "Posting Date", "No.")
        {
        }
        key(FKey2; "Document No.", Type, "No.")
        {
        }
        // todo
        // key(FKey3; "Document No.", "N° client Livré", "Shipment No.")
        // {
        //     SumIndexFields = Amount, "Amount Including VAT";
        // }
        // key(FKey4; "N° client Livré")
        // {
        //     SumIndexFields = Amount, "Amount Including VAT";
        // }
    }

}

