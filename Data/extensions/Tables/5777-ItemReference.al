tableextension 50092 "Item Reference" extends "Item Reference" //5777
{
    fields
    {

        modify("Reference Type No.")
        {
            TableRelation = IF ("Reference Type" = CONST(Customer)) Customer."No."
            ELSE
            IF ("Reference Type" = CONST(Vendor)) Vendor."No."
            ELSE
            IF ("Reference Type" = CONST(Achat)) "Generals Parameters".Code WHERE(Type = CONST('MULTI_ACHAT'))
            ELSE
            IF ("Reference Type" = CONST(Concurent)) "Generals Parameters".Code WHERE(Type = CONST('MULTI_CONCURENT'))
            ELSE
            IF ("Reference Type" = CONST("Ancienne Fournisseur")) Vendor."No."
            ELSE
            IF ("Reference Type" = CONST("Ste Fusion")) Item."No.";
        }
        modify("Reference No.")
        {

            //Unsupported feature: Property Modification (Data type) on ""Reference No."(Field 6)".

            Description = 'MC Le 06-06-2011 => Passage de la référence externe à 40 car';
        }
        modify(Description)
        {
            Description = 'AD Le 17-09-2009 => FARGROUP -> Changement de taille de 30 à 60';
        }

        field(50000; "Zone Libre 1"; Text[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            Enabled = false;
        }
        field(50001; "Zone Libre 2"; Text[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            Enabled = false;
        }
        field(50002; "Zone Libre 3"; Text[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            Enabled = false;
        }
        field(50003; "Zone Libre 4"; Text[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            Enabled = false;
        }
        field(50010; "Code Format Etiquette"; Code[10])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            TableRelation = "Format Etiquette Client"."Format Code";
        }
        field(50011; "Shipment Label Print"; Option)
        {
            Caption = 'Impression des étiquettes livraison';
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des format d''étiquette client';
            InitValue = Aucune;
            OptionMembers = Aucune,"1 par article","1 par ligne","Par multiple d'achat","Par multiple de vente";
        }
        field(50020; "Code Constructeur"; Code[15])
        {
            Caption = 'Code Constructeur';
            Description = 'AD Le 12-02-2016 => Demande de Vincent';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CONSTRUCTEUR'));
        }
        field(50800; "Create User ID"; Code[20])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50801; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50802; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50805; "Modify User ID"; Code[20])
        {
            Caption = 'Code utilisateur Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50806; "Modify Date"; Date)
        {
            Caption = 'Date Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50807; "Modify Time"; Time)
        {
            Caption = 'Heure Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50850; "publiable web"; Boolean)
        {
            Caption = 'publiable web';
            Description = 'FB Le 20/02/2017';
        }
    }
}

