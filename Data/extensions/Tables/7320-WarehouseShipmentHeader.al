tableextension 50125 "Warehouse Shipment Header" extends "Warehouse Shipment Header" //7320
{
    fields
    {
        modify("Assigned User ID")
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('MAG_EMPLOYES'));
            Description = 'AD Le 30-10-2015 => Je change le lien car des utilisateurs ne sont pas dans le domaine';
        }
        modify(Status)
        {
            OptionCaption = 'Open,Released';

            //Unsupported feature: Property Modification (OptionString) on "Status(Field 47)".

            Description = 'AD Le 28-08-2009 => FARGROUP -> Possibilité de bloquer un BP avant la validation';
        }
        field(50000; "Source No."; Code[20])
        {
            Caption = 'Source No.';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
        }
        field(50001; "Source Document"; Option)
        {
            Caption = 'Source Document';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
            OptionCaption = ',Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output';
            OptionMembers = ,"Sales Order",,,"Sales Return Order","Purchase Order",,,"Purchase Return Order","Inbound Transfer","Outbound Transfer","Prod. Consumption","Prod. Output";
        }
        field(50002; "Destination Type"; Option)
        {
            Caption = 'Destination Type';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
            OptionCaption = ' ,Customer,Vendor,Location,Item,Family,Sales Order';
            OptionMembers = " ",Customer,Vendor,Location,Item,Family,"Sales Order";
        }
        field(50003; "Destination No."; Code[20])
        {
            Caption = 'Destination No.';
            Description = 'AD Le 19-11-2009 => Information du document d''origine';
            Editable = false;
        }
        field(50004; Poids; Decimal)
        {
            Description = 'MC Le 02-05-2011 => Poids du BP.';
        }
        field(50005; "Mode d'expédition"; enum "Mode Expédition")
        {
            Description = 'MC Le 26-04-2011 => Définition du mode d''expédition. (Normal, Express ...)';
        }
        field(50006; "Nb Of Box"; Integer)
        {
            Caption = 'Nb de colis';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
            InitValue = 1;
        }
        field(50007; "Code Emballage"; Code[20])
        {
            Description = 'MC Le 27-04-11 => SYMTA -> Emballage';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('EMBALLAGE'));
        }
        field(50008; Assurance; Boolean)
        {
            Description = 'AD Le 26-01-2012';
        }
        field(50010; "Type de commande"; Option)
        {
            Description = 'AD Le 01-04-2015';
            Editable = false;
            OptionMembers = "Dépannage","Réappro",Stock;
        }
        field(50020; "Line Count"; Integer)
        {
            CalcFormula = Count("Warehouse Shipment Line" WHERE("No." = FIELD("No.")));
            Caption = 'Nombre de ligne';
            Description = 'WIIO => Nbre de ligne dans le BP';
            FieldClass = FlowField;
        }
        field(50050; "Date Création"; Date)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50051; "Heure Création"; Time)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50052; "Utilisateur Création"; Code[50])
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50055; "Date Impression"; Date)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50056; "Heure Impression"; Time)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50057; "Utilisateur Impression"; Code[20])
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50060; "Date Flashage"; Date)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50061; "Heure Flashage"; Time)
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(50062; "Utilisateur Flashage"; Code[20])
        {
            Description = 'AD Le 31-03-2015 =>';
            Editable = false;
        }
        field(60200; "Packing List No."; Code[20])
        {
            Caption = 'N° liste de colisage';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
            TableRelation = "Packing List Header";
        }
        field(50035; Blocked; Boolean)
        {
            Caption = 'Bloqué';
            Editable = false;
            Description = 'AD Le 28-08-2009 => FARGROUP -> Possibilité de bloquer un BP avant la validation';
        }
    }
}

