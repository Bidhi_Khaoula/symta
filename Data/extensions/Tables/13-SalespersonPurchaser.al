tableextension 50019 "Salesperson/Purchaser" extends "Salesperson/Purchaser" //13
{
    fields
    {
        field(50000; Address; Text[50])
        {
            Caption = 'Address';
        }
        field(50001; "Address 2"; Text[50])
        {
            Caption = 'Address 2';
        }
        field(50002; City; Text[30])
        {
            Caption = 'City';

        }
        field(50003; "Fax No."; Text[30])
        {
            Caption = 'Fax No.';
        }
        field(50004; "Post Code"; Code[20])
        {
            Caption = 'Post Code';
            TableRelation = "Post Code";
            //This property is currently not supported
            // TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50005; County; Text[30])
        {
            Caption = 'County';
        }
        field(50006; "Country/Region Code"; Code[10])
        {
            Caption = 'Country/Region Code';
            TableRelation = "Country/Region";
        }
        field(50025; "Export Salesperson"; Boolean)
        {
            Caption = 'Export Salesperson';
            Description = 'FBO le 05-04-2017 => FE20170324';
        }
        field(50170; "Rate ADV"; Decimal)
        {
            Caption = 'Taux ADV';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions -> Taux à déffalquer';
            MaxValue = 100;
            MinValue = 0;
        }
        field(65001; "Windows Login"; Text[132])
        {
            Description = 'Naviway : Gestion des droits';
        }
        field(65002; AllCustomersAccess; Boolean)
        {
            Caption = 'Accès tous clients';
            Description = 'Naviway : Gestion des droits';
        }
    }

}

