tableextension 50144 "Customer Posting Group" extends "Customer Posting Group" //92
{
    fields
    {
        field(50000; "Shipment Account"; Code[20])
        {
            Caption = 'Compte Port';
            Description = 'AD Le 31-08-2009 => Gestion des frais de port';
            TableRelation = "G/L Account";
        }
        field(50001; "Discount Account"; Code[20])
        {
            Caption = 'Compte Remise';
            Description = 'AD Le 03-11-2011 => Remises supplémentaires';
            TableRelation = "G/L Account";
        }
    }
}

