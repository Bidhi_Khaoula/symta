tableextension 50090 "VAT Rate Change Log Entry" extends "VAT Rate Change Log Entry" //552
{
    fields
    {
        modify("Converted Date")
        {
            caption = '';
        }
        modify("Entry No.")
        {
            caption = '';
        }
        modify("Table ID")
        {
            caption = '';
        }
        modify("Table Caption")
        {
            caption = '';
        }
        modify("Old Gen. Prod. Posting Group")
        {
            caption = '';
        }
        modify("New Gen. Prod. Posting Group")
        {
            caption = '';
        }
        modify("Old VAT Prod. Posting Group")
        {
            caption = '';
        }
        modify("New VAT Prod. Posting Group")
        {
            caption = '';
        }
        modify(Converted)
        {
            caption = '';
        }
    }

}

