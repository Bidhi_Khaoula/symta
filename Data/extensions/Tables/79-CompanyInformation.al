tableextension 50133 "Company Information" extends "Company Information"  //79
{
    fields
    {
        field(50000; "CNUF Code"; Code[6])
        {
            Caption = 'Code CNUF';
            Description = 'AD Le 17-07-2009 => Code CNUF pour calcul Gencode';
        }
        field(50001; "Fond gris"; BLOB)
        {
        }
        field(50002; "Blue Background"; BLOB)
        {
            Caption = 'Fond bleu';
        }
        field(50010; "Header Picture"; BLOB)
        {
            Caption = 'Logo en-tête';
        }
        field(50011; "Bottom Picture"; BLOB)
        {
            Caption = 'Logo pied';
        }
        field(50015; PDFPrinter; Code[10])
        {
        }
        field(50020; "Email Admin File Projet"; Text[100])
        {
            Description = 'AD Le 27-01-2012 => Mail pour l''administrateur du nas en cas d''erreur';
        }
        field(50025; "MailToFax Domain"; Text[50])
        {
            Caption = 'MailToFax Domain';
            Description = 'ANI Le 13-03-2015 => domaine de messagerie pour le mailToFax de la fiche client';
        }
        field(50030; "Picture Address"; BLOB)
        {
            Caption = 'Image avec adresse';
            Description = 'GR Le 05-11-2015 Image contant l''adresse de la société';
            SubType = Bitmap;
        }
        field(50040; EORI; Text[50])
        {
            Description = '// CFR le 06/04/2023 - Régie : Ajout du Code EORI';
        }
        field(50700; "Company EDI Code"; Code[20])
        {
            Caption = 'Code EDI Société';
            Description = '// EDI => Code de la société pour l''EDI';
        }
        field(50710; "Import EDI Folder"; Text[250])
        {
            Caption = 'Répertoire Import EDI';
            Description = '// EDI => Répertoire ou sont stocké les fichier EDI à importer';
        }
        field(50720; "Export EDI Folder"; Text[250])
        {
            Caption = 'Répertoire Export EDI';
            Description = '// EDI => Répertoire ou sont stocké les fichier EDI à exporter';
        }
        field(50799; "Nb Jour Verif Commande Article"; Text[30])
        {
            Description = 'AD Le 25-11-2010 => GDI -> Verifier à la ligne de commande si un article n''est pas deja commandé';
        }
        field(50800; "Nb Jour Verif Commande"; Text[30])
        {
            Description = 'MC Le 04-03-2010 => GDI ->Verifier à la ligne de commande si un article n''est pas deja commandé ou une cde client';
        }
    }
}

