tableextension 50053 "Inventory Setup" extends "Inventory Setup" //313
{
    fields
    {
        field(50000; "Message send to MiniLoad"; Text[250])
        {
            Caption = 'Message envoyé au miniload';
        }
        field(50001; "Message receive from MiniLoad"; Text[250])
        {
            Caption = 'Message reçu depuis miniload';
        }
        field(50002; "Code emplacement MiniLoad"; Code[20])
        {
            TableRelation = Bin.Code;
        }
        field(50003; "Numero debut feuille MiniLoad"; Code[10])
        {
        }
        field(50010; "% Seuil Capacité Réap. Pick."; Decimal)
        {
            Description = 'AD Le 23-03-2015 => Seuil pour le réapprovisionnement casier Picking';
        }
        field(50013; "Nom Feuille Réappro. Picking"; Code[10])
        {
            Description = 'AD Le 31-03-2015';
        }
        field(50014; "Nom Feuille Réap. Pick. Minil."; Code[10])
        {
            Description = 'AD Le 31-03-2015';
        }
        field(50015; "Mail Réappro. Picking"; Text[50])
        {
            Description = 'AD Le 23-03-2015 => Mail pour le réappro casier Picking';
        }
        field(50016; "Envoyer Reappro. Miniload"; Boolean)
        {
            Description = 'AD Le 24-03-2015 => Permet d''envoyer la feuille en ato au miniload après le réappro';
        }
        field(50017; "Nom Feuille Réappro. PU"; Code[10])
        {
            Description = 'AD Le 31-03-2015';
        }
        field(50020; "Inventory Amount Level"; Decimal)
        {
            Caption = 'Seuil montant inventaire coloration';
            Description = 'CFR le 06/04/2022 => SFD20210929 : inventaires';
        }
    }
}

