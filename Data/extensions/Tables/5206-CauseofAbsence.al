tableextension 50080 "Cause of Absence" extends "Cause of Absence" //5206
{
    fields
    {
        modify(Description)
        {
            Caption = 'Description';
        }
        modify("Unit of Measure Code")
        {
            TableRelation = "Unit of Measure";
        }
    }
}

