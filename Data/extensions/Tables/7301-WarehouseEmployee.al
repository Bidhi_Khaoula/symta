tableextension 50116 "Warehouse Employee" extends "Warehouse Employee" //7301
{
    fields
    {
        field(50000; "Code Remettant Logiflux"; Integer)
        {
        }
        field(50001; "Validation Logistique Direct"; Boolean)
        {
            Description = 'MC Le 06-06-2011 => Pied du formulaire des livraisons.';
        }
        field(50002; "Pas de Contrôle Poids"; Boolean)
        {
            Caption = 'Pas de Contrôle Poids';
            Description = 'CFR Le 24/06/2019 => FE20190529';
        }
    }
}

