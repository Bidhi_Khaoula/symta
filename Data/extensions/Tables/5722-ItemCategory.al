tableextension 50022 "Item Category" extends "Item Category" //5722
{
    fields
    {
        field(50000; "Exclude Express Discount"; Boolean)
        {
            Caption = 'Exclure remise express';
            Description = '// CFR le 06/04/2023 - Régie : Ajout du champ [Exclude Express Discount]';
        }
    }
}