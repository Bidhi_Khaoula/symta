tableextension 50032 Customer extends Customer //18
{
    fields
    {

        modify("Bill-to Customer No.")
        {
            Caption = 'Bill-to Customer No.';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Client Facturé -> Changement de caption';
        }
        modify("Sales (LCY)")
        {
            Caption = 'Sales (LCY)';
            Description = 'AD Le 17-06-2010 => Ajout du [Facturé] dans le caption';
        }

        field(50000; "Family Code 1"; Code[10])
        {
            Caption = 'Code Famille 1';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'));
        }
        field(50001; "Family Code 2"; Code[10])
        {
            Caption = 'Code Famille 2';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'));
        }
        field(50002; "Family Code 3"; Code[10])
        {
            Caption = 'Code Famille 3';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'));
        }
        field(50005; "Activity Code"; Code[10])
        {
            Caption = 'Code Activité';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ACTIVITE'));
        }
        field(50006; "Direction Code"; Code[10])
        {
            Caption = 'Code Direction';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_DIRECTION'));
        }
        field(50007; "Info Contact"; Text[50])
        {
            Description = 'AD Le 09-11-2009 => FARGROUP -> Champ réservé pour l''info de la fiche contact';
            Editable = false;
            Enabled = false;
        }
        field(50008; "Responsabilité"; Code[10])
        {
            Description = 'AD Le 09-11-2009 => FARGROUP -> Champ réservé pour l''info de la fiche contact';
            Editable = false;
            Enabled = false;
        }
        field(50009; "Code Contrat RFA"; Code[20])
        {
            Description = 'AD Le 01-02-2009 => FARGROUP RFA ->';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_CONTRAT_RFA'));
        }
        field(50010; Responsable; Code[15])
        {
            Caption = 'Responsable';
            Description = 'AD Le 28-08-2009 => FARGROUP';
            TableRelation = User;
        }
        field(50011; "Regroupement Facturation"; Enum "Regroupement Facturation")
        {
            Description = 'AD Le 28-04-2011 => SYMPTA -> Type de regroupement des factures';
            InitValue = "1 Facture par client payeur";
        }
        field(50012; "Invoice Type"; Code[10])
        {
            Caption = 'Type Facturation';
            Description = 'AD Le 12-04-2007 => CTA';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_T_FAC'));
        }
        field(50013; "Nombre d'écriture en relance"; Integer)
        {
            CalcFormula = Count("Cust. Ledger Entry" WHERE("Customer No." = FIELD("No."),
                                                            Open = CONST(True),
                                                            "Last Issued Reminder Level" = FILTER(> 0)));
            Description = 'FB LE 03-11-2009 =>FARGROUP -> alerte vente';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50014; "FAX Comptabilité"; Text[20])
        {
            Description = 'FB LE 03-11-2009 =>FARGROUP -> compta';
        }
        field(50015; "Mailing Information"; Boolean)
        {
            Caption = 'Info mailing';
            Description = 'SID';
            InitValue = true;
        }
        field(50016; "APE Code"; Code[10])
        {
            Description = 'AD Le 29-04-2011 => GDI';
        }
        field(50017; "Lien Etiquette Livraison"; Code[20])
        {
            Description = 'AD Le 08-07-2010 => FARGROUP -> Modèle pour les etiquettes livraisons';
            TableRelation = Customer;
        }
        field(50020; "Code Assurance Credit"; Code[10])
        {
            Caption = 'Code Assurance Credit';
            Description = 'AD Le 28-08-2009 => FARGROUP';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ASS_CRED'));
        }
        field(50021; "Identif. Assurance Credit"; Code[20])
        {
            Caption = 'Identif. Assurance Credit';
            Description = 'AD Le 28-08-2009 => FARGROUP';
        }
        field(50026; "Inverser Adresses Factures"; Boolean)
        {
            Description = 'AD Le 26-05-2014 => SYMTA';
        }
        field(50030; "Présent Web"; Boolean)
        {
            Caption = 'Présent Web';
            Description = 'AD Le 28-08-2009 => FARGROUP';
        }
        field(50032; "Surface de vente"; Decimal)
        {
            BlankZero = true;
            Caption = 'Surface de vente';
            Description = 'AD Le 28-08-2009 => FARGROUP';
            MinValue = 0;
        }
        field(50033; "Code Groupement RFA"; Code[10])
        {
            Description = 'AD Le 03-11-2011 => SYMTA';
            TableRelation = Customer;
        }
        field(50035; "Masquer remise sur édition"; Boolean)
        {
            Description = 'AD Le 28-08-2009 => FARGROUP';
        }
        field(50036; "Masquer Ref. Active"; Boolean)
        {
            Description = 'AD Le 20-01-2014';
        }
        field(50037; "Masquer Logo Société"; Boolean)
        {
            Description = 'AD Le 20-01-2014';
        }
        field(50039; Entete; Code[10])
        {
            Description = 'AD Le 03-03-2010 => GDI';
        }
        field(50040; "Centrale Active"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup("Purchase Central History"."Active Central" WHERE("Customer No." = FIELD("No."),
                                                                              "Starting Date" = FIELD("Centrale Active Starting DF"),
                                                                              "Ending Date" = FIELD("Centrale Active Ending DF")));
            Description = 'MC Le 18-04-2011 => SYMTA -> TARIFS VENTES - CFR le 24/09/2021 - SFD20210201 historique centrale active - Modification du champ [Code centrale >> Flowfield]';
            Editable = false;
        }
        field(50041; "Groupement payeur"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Lookup("Purchase Central History"."Groupement payeur" WHERE("Customer No." = FIELD("No."),
              "Starting Date" = FIELD("Centrale Active Starting DF"),
              "Ending Date" = FIELD("Centrale Active Ending DF")));
            Description = 'FB Le 26-04-2011 => SYMTA - CFR le 24/09/2021 - SFD20210201 historique centrale active - Modification du champ [Groupement payeur >> Flowfield]';
            Editable = false;
        }
        field(50042; Holding; Code[20])
        {
            Caption = 'Holding';
            Description = 'AD Le 29-01-2020 => REGIE';
            TableRelation = Customer;
        }
        field(50043; "Confirmation Centrale"; Boolean)
        {
            Description = 'AD Le 28-09-2016 => REGIE -> Message au lancement de la commande';
        }
        field(50044; "Centrale Active Starting DF"; Date)
        {
            FieldClass = FlowFilter;
            Caption = 'Date Filter';
            Description = 'SFD20210201 historique centrale active - Modification du champ [Code centrale >> Flowfield]';
        }
        field(50045; "Centrale Active Ending DF"; Date)
        {
            FieldClass = FlowFilter;
            Caption = 'Date Filter';
            Description = 'SFD20210201 historique centrale active - Modification du champ [Code centrale >> Flowfield] }';
        }
        field(50050; "Sell to Sales (LCY)"; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = Sum("Cust. Ledger Entry"."Sales (LCY)" WHERE("Sell-to Customer No." = FIELD("No."),
                                                                        "Global Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                                                        "Global Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                                                        "Posting Date" = FIELD("Date Filter"),
                                                                        "Currency Code" = FIELD("Currency Filter")));
            Caption = 'Sales (LCY)';
            Description = 'AD Le 17-06-2010 => Vente en tant que donneur d''ordre';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50055; "No. of Orders For Date"; Integer)
        {
            AccessByPermission = TableData 110 = R;
            CalcFormula = Count("Sales Header" WHERE("Document Type" = CONST(Order),
                                                      "Sell-to Customer No." = FIELD("No."),
                                                      "Document Date" = FIELD("Date Filter")));
            Caption = 'No. of Orders For Date';
            Description = 'AD Le 29-01-2020 => REGIE';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50056; "No. of Pstd. Ship.  For Date"; Integer)
        {
            CalcFormula = Count("Sales Shipment Header" WHERE("Sell-to Customer No." = FIELD("No."),
                                                               "Posting Date" = FIELD("Date Filter")));
            Caption = 'No. of Pstd. Shipments  For Date';
            Description = 'AD Le 29-01-2020 => REGIE';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50060; "Envoi Facturation"; enum "Envoi Facturation")
        {
            Description = 'AD Le 29-01-2020 => REGIE';
        }
        field(50061; "E-Mail Facturation"; Text[100])
        {
            Description = 'AD Le 29-01-2020 => REGIE';
        }
        field(50062; "E-Mail secondaire"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Exist("E-Mail Address" WHERE("Entry Type" = CONST("Client facturation"),
                                                                    "Customer No." = FIELD("No.")));
            TableRelation = "E-Mail Address" WHERE("Entry Type" = CONST("Client facturation"),
                                                                   "Customer No." = FIELD("No."));

            Description = 'CFR le 18/10/2022 => Régie';
            Editable = false;
            Trigger OnLookup()
            VAR
            // lEMailList: Page 50267;
            // lEMailAddress: Record 50074;
            BEGIN
                /*{lEMailAddress.SETRANGE("Entry Type", lEMailAddress."Entry Type"::"Client facturation");
                lEMailAddress.SETRANGE("Customer No.", Rec."No.");

                lEMailList.SETRECORD(lEMailAddress);
                lEMailList.SETTABLEVIEW(lEMailAddress);
                lEMailList.EDITABLE(TRUE);
                lEMailList.RUNMODAL();}*/
            END;
        }
        field(50070; "Taux S.A.V."; Decimal)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50071; "Date Taux S.A.V."; Date)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50080; "Code Enseigne 1"; Code[20])
        {
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ENSEIGNE'));
        }
        field(50081; "Code Enseigne 2"; Code[20])
        {
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ENSEIGNE'));
        }
        field(50085; "Libre 1"; Code[20])
        {
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
        }
        field(50086; "Libre 2"; Code[20])
        {
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
        }
        field(50087; "Libre 3"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup("Purchase Central History"."Libre 3" WHERE("Customer No." = FIELD("No."),
                                                                            "Starting Date" = FIELD("Centrale Active Starting DF"),
                                                                            "Ending Date" = FIELD("Centrale Active Ending DF")));
            Caption = 'Code adhérent';
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif - SFD20210201 historique centrale active - Modification du champ [Code adhérent >> Flowfield]';
            Editable = false;
        }
        field(50088; "Libre 4"; Code[20])
        {
            Description = 'AD Le 29-04-2011 => SYMTA -> Spécif';
        }
        //todo
        // field(50090; "Mobile Phone No."; Text[30])
        // {
        //     Caption = 'Mobile Phone No.';
        //     Description = 'MCO Le 23-03-2016 => Gestion du mobile sur client';
        //     ExtendedDatatype = PhoneNo;
        // }
        field(50095; "Bloquer en commande"; Boolean)
        {
            Description = 'MCO Le 21-11-2018 => Régie';
        }
        field(50100; "Create Date"; Date)
        {
            Caption = 'Date de création';
            Description = 'CFR le 22/09/2021 => Régie : date de création';
            Editable = false;
        }
        field(50102; "Invoice Customer No."; Code[20])
        {
            Caption = 'N° Client facturé';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Client Facturé';
            TableRelation = Customer;
        }
        field(50103; "Abandon remainder"; Boolean)
        {
            Caption = 'Abandon reliquat';
            Description = 'ESK AD Le 28-10-2009 => OUI Par dedaut';
            InitValue = true;
        }
        field(50104; "Figuring Sales Shipment"; Boolean)
        {
            Caption = 'Bon de livraison chiffré';
            Description = 'AD Le 12-04-2007 => CTA';
            InitValue = false;
        }
        field(50105; "Client Comptoir"; Boolean)
        {
            Description = 'AD Le 23-11-2011 => Détermine si un client est comptoir (pour gestion dans les éditions)';
        }
        field(50109; "Mode d'expédition"; enum "Mode Expédition")
        {
            Description = 'MC Le 26-04-2011 => Définition du mode d''expédition. (Normal, Express ...)';
            InitValue = Express;
        }
        field(50110; "Transporteur imperatif"; Boolean)
        {
            Description = 'MC Le 26-04-2011 => Rendre le transporteur impératif.';
        }
        field(50115; "Adresse Expédition"; Text[50])
        {
            Description = 'AD Le 29-04-2011 => Adresse à utiliser dans LOGIFLUX';
        }
        field(50116; "Adresse 2 Expédition"; Text[50])
        {
            Description = 'AD Le 29-04-2011 => Adresse à utiliser dans LOGIFLUX';
        }
        field(50117; "Ville Expédition"; Text[30])
        {
            Caption = 'City';
            Description = 'AD Le 29-04-2011 => Adresse à utiliser dans LOGIFLUX';
        }
        field(50118; "Code postal Expédition"; Code[20])
        {
            Caption = 'Post Code';
            Description = 'AD Le 29-04-2011 => Adresse à utiliser dans LOGIFLUX';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50119; "Country/Region Code Expédition"; Code[10])
        {
            Caption = 'Country/Region Code';
            Description = 'GR Le 29-07-2015 => Migration 2015';
            TableRelation = "Country/Region";
        }
        field(50120; "County Expédition"; Text[30])
        {
            Caption = 'County';
            Description = 'GR Le 29-07-2015 => Migration 2015';
        }
        field(50125; "Code Expéditeur"; Code[10])
        {
            Description = 'AD Le 29-04-2011 => Expéditeur à utiliser dans LOGIFLUX';
        }
        field(50130; "Incoterm City"; Text[50])
        {
            Caption = 'Ville Incoterm ICC 2020';
            Description = 'CFR le 06/04/2023 - Régie : Incoterm ICC 2020 Ventes';
        }
        field(50131; "Incoterm Code"; Code[10])
        {
            TableRelation = "Shipment Method";
            Caption = 'Shipment Method Code';
            Description = 'CFR le 06/04/2023 - Régie : Incoterm ICC 2020 Ventes';
        }

        field(50144; "CA HT donneur d'ordre"; Decimal)
        {
            CalcFormula = Sum("Value Entry"."Sales Amount (Actual)" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                           "Posting Date" = FIELD("Date Filter"),
                                                                           "Source No." = FIELD("No.")));
            Description = 'ANI Le 27-03-2015 => GDI -> Calculé par les écritures de valeurs';
            FieldClass = FlowField;
        }
        field(50150; "EORI Code"; Text[30])
        {
            Caption = 'Code EORI';
            Description = 'CFR le 19/11/2020 : Régie > Code EORI\';
        }
        field(50152; "DEEE Tax"; Boolean)
        {
            Caption = 'Soumis à la taxe DEEE';
            Description = 'DEE';
            InitValue = true;
        }
        field(50170; "Rate Commission"; Decimal)
        {
            Caption = 'Coefficient de commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            InitValue = 0.89;
            MinValue = 0;
        }
        field(50700; "Customer EDI Code"; Code[20])
        {
            Caption = 'Code EDI Client';
            Description = 'EDI => Code EDI DU CLIENT (Gencode - Siret - Interne ,,,)';
        }
        field(50705; "Code AR EDI"; Code[10])
        {
            Description = 'AD Le 29-09-2008 => ORDRSP EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50706; "Date Début Export AR EDI"; Date)
        {
            Description = 'AD Le 29-09-2008 => ORDRSP EDI';
        }
        field(50710; "Code Livraison EDI"; Code[10])
        {
            Description = 'AD Le 07-01-2008 => DESADV EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50711; "Date Début Export Livr. EDI"; Code[10])
        {
            Description = 'AD Le 07-01-2008 => DESADV EDI';
        }
        field(50720; "Code Facturation EDI"; Code[10])
        {
            Description = 'AD Le 29-09-2008 => Facturation EDI';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('FIC_EDI'));
        }
        field(50721; "Date Début Export Facture EDI"; Date)
        {
            Description = 'AD Le 29-09-2008 => Facturation EDI';
        }
        field(50730; "Erreur intégration"; Boolean)
        {
        }
        field(50800; "Sous Groupe Tarifs"; Code[20])
        {
            Description = 'MC Le 18-04-2011 => SYMTA -> TARIFS VENTES';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_SOUS_GRP_TAR'));
        }
        field(50801; "Solde DS a date"; Decimal)
        {
            CalcFormula = Sum("Detailed Cust. Ledg. Entry".Amount WHERE("Customer No." = FIELD("No."),
                                                                         "Initial Entry Global Dim. 1" = FIELD("Global Dimension 1 Filter"),
                                                                         "Initial Entry Global Dim. 2" = FIELD("Global Dimension 2 Filter"),
                                                                         "Currency Code" = FIELD("Currency Filter"),
                                                                         "Posting Date" = FIELD(UPPERLIMIT("Date Filter"))));
            FieldClass = FlowField;
        }
        field(50802; "Ca Ecritures Valeurs"; Decimal)
        {
            CalcFormula = Sum("Value Entry"."Sales Amount (Actual)" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                           "Posting Date" = FIELD("Date Filter"),
                                                                           "Manufacturer Code" = FIELD("Filtre Marque"),
                                                                           "Source No." = FIELD("No.")));
            Description = 'AD Le 04-01-2012 => SYMTA -> Optimisé pour CA par le marque';
            FieldClass = FlowField;
        }
        field(50803; "Filtre Marque"; Code[10])
        {
            Description = 'AD Le 04-01-2012 => SYMTA -> Pour CA par le marque';
            FieldClass = FlowFilter;
        }
        field(50804; "Ca Client Facturé"; Decimal)
        {
            CalcFormula = Sum("Value Entry"."Sales Amount (Actual)" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                           "Posting Date" = FIELD("Date Filter"),
                                                                           "Manufacturer Code" = FIELD("Filtre Marque"),
                                                                           "Invoice Customer No." = FIELD("No.")));
            Description = 'ANI Le 21-04-2015';
            FieldClass = FlowField;
        }
        field(50805; "Shipped Not Inv. SellCus (LCY)"; Decimal)
        {
            AutoFormatType = 1;
            CalcFormula = Sum("Sales Line"."Shipped Not Invoiced (LCY)" WHERE("Document Type" = CONST(Order),
                                                                               "Sell-to Customer No." = FIELD("No."),
                                                                               "Shortcut Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                                                               "Shortcut Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                                                               "Currency Code" = FIELD("Currency Filter")));
            Caption = 'Shipped Not Invoiced (LCY)';
            Description = 'AD Le 22-04-2015 => SYMTA -> Le livré non facturé mais du donneur d''ordre';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50810; "Commentaire client bloqué"; Text[30])
        {
            Description = 'FB le 06-11-2017';
        }
        field(50820; "AR Commande Therefore"; Boolean)
        {
            Description = 'CFR le 06/04/2022 => Régie';
        }
        field(50821; "E-Mail AR Cde Auto"; Text[80])
        {
            Caption = 'Email AR Cde Auto';
            Description = 'CFR le 12/04/2022 => Régie VD->Modification à 80 le 21/04/2022';
        }
        field(60000; CodeComptaDbx; Code[10])
        {
            Description = 'AD le 15-09-2009 => Recup des données DBX';
        }
        field(60001; "Forced Web"; Boolean)
        {
            Caption = 'Web Forçé';
            Description = 'WF le 16-09-2010 => Utilisation pour le site internet';
        }
        field(60002; "Droits Téléchargement WEB"; Code[10])
        {
        }
        field(60003; "Login WEB"; Text[30])
        {
            Description = 'MC Le 14-06-2011 => Login/MDP E-Commerce';
        }
        field(60004; "Mot de passe WEB"; Text[30])
        {
            Description = 'MC Le 14-06-2011 => Login/MDP E-Commerce';
        }
        field(60005; "Disable Web"; Boolean)
        {
            Caption = 'Disable Web';
            Description = 'MCO Le 20-11-2018 => Régie';
        }
        field(60010; "Acces Right"; Option)
        {
            Caption = 'Access Right';
            Description = 'CFR 08/07/2019 : FE20190624';
            OptionCaption = 'All,No Consultation,No Sales';
            OptionMembers = All,"No Consultation","No Sales";
        }
        field(60011; "Consult Price Right"; Boolean)
        {
            Caption = 'Consult Price Right';
            Description = 'CFR 08/07/2019 : FE20190624';
        }
        field(60050; "No. Of Invoice Recup Histo"; Integer)
        {
            CalcFormula = Count("Value Entry" WHERE("Source No." = FIELD("No."),
                                                     "Récupération Historiques" = CONST(True),
                                                     "Document Type" = CONST("Sales Invoice")));
            Description = 'AD Le 20-04-2010 => GDI -> Nb d''écritures historiques dans les écritures des valeurs [RETOUR]';
            FieldClass = FlowField;
        }
        field(60051; "Supprimé"; Boolean)
        {
            Description = 'LM Le 13-06-2013 =>identifie les comptes supprimés pour adv et compta';
        }
        field(60052; SIRET; Text[14])
        {
            Description = 'LM Le 13-06-2013 =>identifie les comptes supprimés pour adv et compta';
        }
    }
    keys
    {
        key(FKey1; "Salesperson Code", "No.")
        {
        }
        key(FKey2; "Code Facturation EDI")
        {
        }
        key(FKey3; "Invoice Customer No.")
        {
        }
    }
    fieldgroups
    {
        addlast(DropDown; "Bloquer en commande", Blocked) { }
    }

}

