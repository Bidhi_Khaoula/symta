tableextension 50042 Item extends Item //27
{
    DataCaptionFields = "No. 2", Description;
    fields
    {
        modify("No. 2")
        {
            Caption = 'Référence Active';
            Description = 'AD Le 27-04-2011 => SYMTA -> Référence Active';
        }
        modify("Description 2")
        {
            Caption = 'Description 2';
            Description = 'CFR le 16/12/2020 - Régie : Informations achat';
        }

        modify("Manufacturer Code")
        {
            Caption = 'Marque';
            Description = 'AD Le 17-07-2009 => FARGROUP -> Code Fabriquant Renomé en Marque';
        }
        modify("Item Category Code")
        {
            Caption = 'Famille';
            Description = 'AD Le 17-07-2009 => FARGROUP -> Code catégorie article Renomé en Fammille';
        }
        //todo
        // modify("Product Group Code")
        // {
        //     Caption = 'Product Group Code';
        //     Description = 'AD Le 17-07-2009 => FARGROUP -> Code groupe produits Renomé en Sous Fammille';
        // }

        field(50000; "Qty. on Blanket Sales Order"; Decimal)
        {
            CalcFormula = Sum("Sales Line"."Blanket Order Quantity Outstan" WHERE("Document Type" = CONST("Blanket Order"),
                                                                                   Type = CONST(Item),
                                                                                   "No." = FIELD("No."),
                                                                                   "Shortcut Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                                                                   "Shortcut Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                                                                   "Location code" = FIELD("Location Filter"),
                                                                                   "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                                   "Variant Code" = FIELD("Variant Filter"),
                                                                                   "Shipment Date" = FIELD("Date Filter")));
            Caption = 'Qty. on Sales Order';
            DecimalPlaces = 0 : 5;
            Editable = false;
            FieldClass = FlowField;
        }
        field(50001; "Inventory At Date"; Decimal)
        {
            CalcFormula = Sum("Item Ledger Entry".Quantity WHERE("Item No." = FIELD("No."),
                                                                  "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                  "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                  "Location code" = FIELD("Location Filter"),
                                                                  "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                  "Variant Code" = FIELD("Variant Filter"),
                                                                  "Lot No." = FIELD("Lot No. Filter"),
                                                                  "Serial No." = FIELD("Serial No. Filter"),
                                                                  "Posting Date" = FIELD("Date Filter")));
            Caption = 'Inventory';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 12-11-2009 => Stock à date ("Date Filter")';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50002; "Purchases (Qty.) 2"; Decimal)
        {
            CalcFormula = Sum("Value Entry"."Invoiced Quantity" WHERE("Item Ledger Entry Type" = CONST(Purchase),
                                                                       "Item No." = FIELD("No."),
                                                                       "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                       "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                       "Location code" = FIELD("Location Filter"),
                                                                       "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                       "Variant Code" = FIELD("Variant Filter"),
                                                                       "Posting Date" = FIELD("Date Filter")));
            Caption = 'Purchases (Qty.)';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 25-11-2009 => FARGROUP -> Contrairement au champ 71, celui ci travail sur les ecritures valeurs (donc on a les histos DBX) Utilisé dans le F9 de la fiche article.';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50004; "Catégorie produit"; Code[10])
        {
        }
        field(50005; "Product Type"; Code[10])
        {
            Caption = 'Type Produit';
            Description = 'AD Le 17-07-2009 => FARGROUP -> Type de produit';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_TYPE'));
        }
        field(50006; "Code SECIMPAC"; Code[10])
        {
            Caption = 'Code SECIMPAC';
        }
        field(50008; "Number Of Elements"; Decimal)
        {
            BlankZero = true;
            Caption = 'Nombre d''élements';
            Description = 'AD Le 17-07-2009 => FARGROUP -> Pour la taxe batterie';
        }
        field(50010; "Check Qty Hors Norme"; Decimal)
        {
            Caption = 'Controle Quantite Hors Norme';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
            Editable = false;
            Enabled = true;
        }
        field(50011; "Check Min Profit %"; Decimal)
        {
            Caption = 'Controle % Marge Mini';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
        }
        field(50012; "Sales multiple"; Decimal)
        {
            Caption = 'Multiple de vente';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
        }
        field(50014; "Purch. Check Qty Max"; Decimal)
        {
            Caption = 'Controle Quantite Maxi Achat';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
        }
        field(50016; "Purch. Multiple"; Decimal)
        {
            Caption = 'Multiple d''achat';
            Description = 'AD Le 13-04-2007 => Fonction DBX';
        }
        field(50018; "Qte sur feuille assemblage"; Decimal)
        {
            Caption = 'Qte sur feuille assemblage';
            Description = 'Editable=No GR Le 29-07-15 MIG2015 A voir car la table n''existe plus ?';
            Enabled = false;
            // FieldClass = FlowField;
        }
        field(50019; "Qte sur Compo. feuille assem"; Decimal)
        {
            Description = 'Editable=No GR Le 29-07-15 MIG2015 A voir car la table n''existe plus ?';
        }
        field(50020; "Nb master par couche"; Integer)
        {
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50021; "Nb couche par palette"; Integer)
        {
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50022; "Nb master sur champ par couche"; Integer)
        {
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50023; "Nb master sur champ par palett"; Integer)
        {
            Caption = 'Nb master sur champ par palette';
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50024; "Couche croisée"; Boolean)
        {
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50025; "Quantité par container"; Integer)
        {
            Description = 'AD Le 07-09-2009 => FARGROUP';
        }
        field(50026; "Classe Poids"; Integer)
        {
            Description = 'AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement';
        }
        field(50027; "Allée"; Code[1])
        {
            Description = 'AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement';
        }
        field(50028; "N° Emplacement"; Integer)
        {
            Description = 'AD Le 08-10-2009 => FARGROUP -> Pour générer le code emplacement';
        }
        field(50030; "Variante active"; Code[10])
        {
            Caption = 'Variante active';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion des versions';
            TableRelation = "Item Variant".Code WHERE("Item No." = FIELD("No."));
        }
        field(50031; "Variante Showroom"; Code[10])
        {
            Caption = 'Variante Showroom';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion des versions';
            TableRelation = "Item Variant".Code WHERE("Item No." = FIELD("No."));
        }
        field(50040; "% Participation port"; Code[20])
        {
            Description = ' MC Le 27-04-2011 => SYMTA -> Transport';
            TableRelation = Resource;
        }
        field(50050; "Formule Stock Normatif"; Option)
        {
            Caption = 'Formule Stock Normatif';
            Description = 'AD Le 02-09-2009 => FARGROUP -> Calcul Mini Maxi';
            OptionMembers = " ","S-(12DernierMois + 3ProchainMois + 3ProchainMois) / 3","1-Non géré","2-Non géré","3-Non géré","4-(12DernierMois + 3DernierMois) / 2","5-3DernierMois","6-DernierMois";
        }
        field(50051; "Nbre Jour de Stock"; Integer)
        {
            Caption = 'Nbre Jour de Stock';
            Description = 'AD Le 02-09-2009 => FARGROUP -> Calcul Mini Maxi';
        }
        field(50052; "Coef. multiplicateur"; Decimal)
        {
            Caption = 'Coef. multiplicateur';
            Description = 'AD Le 02-09-2009 => FARGROUP -> Calcul Mini Maxi (Pour Minorer / Majorer le stock)';
        }
        field(50058; "Emplacement par défaut"; Code[20])
        {
            CalcFormula = Lookup("Bin Content"."Bin Code" WHERE("Item No." = FIELD("No."),
                                                                 Default = CONST(True)));
            Description = 'AD Le 18-12-2015 => MIG2015 -> Avant une fonction';
            FieldClass = FlowField;
            TableRelation = "Bin Content"."Bin Code" WHERE("Item No." = FIELD("No."),
                                                            Default = CONST(True));
        }
        field(50059; "Date Dernière Sortie"; Date)
        {
            CalcFormula = Max("Value Entry"."Posting Date" WHERE("Item Ledger Entry Type" = FILTER("Sale"),
                                                                  "Valued Quantity" = FILTER(< 0),
                                                                  "Item No." = FIELD("No."),
                                                                  "Document Type" = FILTER(<> "Purchase Credit Memo"),
                                                                  "Item Ledger Entry Quantity" = FILTER(<> 0)));
            Caption = 'Date Dernière Sortie';
            Description = 'AD Le 15-11-2011 => SYMTA -> Date dernière mvt sortie';
            FieldClass = FlowField;
        }
        field(50060; "Conso. Normative"; Decimal)
        {
            Caption = 'Conso. Normative';
            Description = 'AD Le 02-09-2009 => FARGROUP -> Calcul Mini Maxi';
            Editable = false;
        }
        field(50061; "Date Conso. Normative"; Date)
        {
            Caption = 'Date Conso. Normative';
            Description = 'AD Le 02-09-2009 => FARGROUP -> Calcul Mini Maxi';
            Editable = false;
        }
        field(50062; "Code Appro"; Code[10])
        {
            Description = 'AD Le 06-05-2011 => SYMTA -> Code Appro';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CODE_APPRO'));
        }
        field(50063; "Hors Norme Sales (Qty.)"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Invoiced Quantity" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                        "Item No." = FIELD("No."),
                                                                        "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                        "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                        "Location code" = FIELD("Location Filter"),
                                                                        "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                        "Variant Code" = FIELD("Variant Filter"),
                                                                        "Posting Date" = FIELD("Date Filter"),
                                                                        "Hors Normes" = CONST(True)));
            Caption = 'Sales (Qty.)';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 08-09-2009 => Ecritures de valeurs hors normes/Campagnes';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50064; "Campaign Sales (Qty.)"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Invoiced Quantity" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                        "Item No." = FIELD("No."),
                                                                        "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                        "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                        "Location code" = FIELD("Location Filter"),
                                                                        "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                        "Variant Code" = FIELD("Variant Filter"),
                                                                        "Posting Date" = FIELD("Date Filter"),
                                                                        "Campaign No." = FILTER(<> '')));
            Caption = 'Sales (Qty.)';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 08-09-2009 => Ecritures de valeurs hors normes/Campagnes';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50065; "Ancien Conso. Normative"; Decimal)
        {
            Description = 'MC Le 26-05-2010 =>Analyse complémentaire Etats d''alertes';
            Editable = false;
        }
        field(50066; "Variation Conso. Normative (%)"; Decimal)
        {
            Description = 'MC Le 26-05-2010 =>Analyse complémentaire Etats d''alertes';
            Editable = false;
        }
        field(50070; "Taux S.A.V."; Decimal)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50071; "Date Taux S.A.V."; Date)
        {
            Description = 'AD Le 30-09-2009 => FARGROUP -> SAV';
            Editable = false;
        }
        field(50080; "Consommation Navision"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Item Ledger Entry Quantity" WHERE("Item Ledger Entry Type" = CONST(Sale),
                                                                                 "Item No." = FIELD("No."),
                                                                                 "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                                 "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                                 "Location code" = FIELD("Location Filter"),
                                                                                 "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                                 "Variant Code" = FIELD("Variant Filter"),
                                                                                 "Posting Date" = FIELD("Date Filter"),
                                                                                 "Récupération Historiques" = CONST(false)));
            Description = 'MC Le 14-06-2011 => SYMTA -> Calcul des consommations';
            FieldClass = FlowField;
        }
        field(50081; "Consommation Dbx"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Valued Quantity" WHERE("Item Ledger Entry Type" = FILTER(Sale | Consumption),
                                                                      "Item No." = FIELD("No."),
                                                                      "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                      "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                      "Location code" = FIELD("Location Filter"),
                                                                      "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                      "Variant Code" = FIELD("Variant Filter"),
                                                                      "Posting Date" = FIELD("Date Filter"),
                                                                      "Récupération Historiques" = CONST(True)));
            Description = 'MC Le 14-06-2011 => SYMTA -> Calcul des consommations';
            FieldClass = FlowField;
        }
        field(50082; "Qte Ecritures Conso. Kit"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Invoiced Quantity" WHERE("Item Ledger Entry Type" = FILTER(Consumption | "Assembly Consumption"),
                                                                        "Document Type" = FILTER(' ' | "Sales Shipment" | "Posted Assembly"),
                                                                        "Item No." = FIELD("No."),
                                                                        "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                        "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                        "Location code" = FIELD("Location Filter"),
                                                                        "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                        "Variant Code" = FIELD("Variant Filter"),
                                                                        "Posting Date" = FIELD("Date Filter"),
                                                                        "Récupération Historiques" = CONST(false)));
            Description = 'MC Le 14-06-2011 => SYMTA -> Pour les rotations article avoir la quantité sur les fabrications normales en + des kits';
            FieldClass = FlowField;
        }
        field(50083; "Qty. on Purchase Return"; Decimal)
        {
            CalcFormula = Sum("Purchase Line"."Outstanding Qty. (Base)" WHERE("Document Type" = CONST("Return Order"),
                                                                               Type = CONST(Item),
                                                                               "No." = FIELD("No."),
                                                                               "Shortcut Dimension 1 Code" = FIELD("Global Dimension 1 Filter"),
                                                                               "Shortcut Dimension 2 Code" = FIELD("Global Dimension 2 Filter"),
                                                                               "Location code" = FIELD("Location Filter"),
                                                                               "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                               "Variant Code" = FIELD("Variant Filter"),
                                                                               "Expected Receipt Date" = FIELD("Date Filter")));
            Caption = 'Qty. on Purch. Order';
            DecimalPlaces = 0 : 5;
            Editable = false;
            FieldClass = FlowField;
        }
        field(50084; "Consommation Achat Navision"; Decimal)
        {
            CalcFormula = - Sum("Value Entry"."Item Ledger Entry Quantity" WHERE("Item Ledger Entry Type" = CONST(Purchase),
                                                                                 "Item Ledger Entry Quantity" = FILTER(< 0),
                                                                                 "Item No." = FIELD("No."),
                                                                                 "Global Dimension 1 code" = FIELD("Global Dimension 1 Filter"),
                                                                                 "Global Dimension 2 code" = FIELD("Global Dimension 2 Filter"),
                                                                                 "Location code" = FIELD("Location Filter"),
                                                                                 "Drop Shipment" = FIELD("Drop Shipment Filter"),
                                                                                 "Variant Code" = FIELD("Variant Filter"),
                                                                                 "Posting Date" = FIELD("Date Filter"),
                                                                                 "Récupération Historiques" = CONST(false)));
            Description = 'AD Le 04-12-2012 => SYMTA -> Il s''agit des achat négatifs (retour achat)';
            FieldClass = FlowField;
        }
        field(50090; "Net Price Only"; Boolean)
        {
            Caption = 'Net price only';
            Description = 'FBO le 05-04-2017 => FE20170323';
            InitValue = false;
        }
        field(50100; "Gencod EAN13"; Text[13])
        {
            Caption = 'Gencod EAN13';
            Description = '=> Gestion des codes EAN13';
        }
        field(50102; "Item Charge DEEE"; Code[20])
        {
            Caption = 'Frais annexe DEEE';
            Description = 'DEEE';
            TableRelation = "Item Charge";
        }
        field(50103; "Item Charge DEEE Amount"; Decimal)
        {
            Caption = 'Montant frais annexe DEE';
            Description = 'DEEE';
        }
        field(50104; "Official DEEE Code"; Code[10])
        {
            Caption = 'Code Officiel DEEE';
            Description = 'DEEE';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART-DEEE'));
        }
        field(50110; "Process Blocked"; Option)
        {
            Caption = 'Processus bloqué';
            Description = 'AD Le 26-08-2009 => Gestion avancé du bloque';
            OptionMembers = " ",Ventes,Achats,"Ventes & Achats";
        }
        field(50120; "Description 3"; Text[50])
        {
            Caption = 'Informations vente';
            Description = 'CFR le 16/12/2020 - Régie : Informations vente';
        }
        field(50130; "Technical Manual"; Boolean)
        {
            Caption = 'Notice technique';
            Description = 'CFR le 18/10/2022 => Régie';
        }
        field(50131; "Technical Manual Name"; text[250])
        {
            Caption = 'Nom du fichier notice';
            Description = 'CFR le 18/10/2022 => Régie';
        }
        field(50132; "Kit Web Status"; Option)
        {
            Caption = 'Etat du kit (Web)';
            OptionMembers = ,Modifiable,"Non modifiable","Non visible";
            Description = 'CFR le 19/10/2022 => Régie';
        }
        field(50140; "Filtre Code Remise Réappro"; code[10])
        {
            FieldClass = FlowFilter;
            Description = 'CFR le 28/09/2023 => Régie - Réappro : filtre Item Vendor sur Code Remise';
        }
        field(50150; "Sans Mouvements de Stock"; Boolean)
        {
            Caption = 'Sans Mouvements de Stock';
            Description = 'AD Le 17-09-2009 => Sans Mouvements de Stock';
        }
        field(50152; "Assurance transport"; Boolean)
        {
            Description = 'AD Le 11-01-2012 => transport';
        }
        field(50170; "Rate Commission"; Decimal)
        {
            Caption = 'Coefficient de commission';
            DecimalPlaces = 2 : 5;
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            InitValue = 1;
            MinValue = 0;
        }
        field(50178; "Comodity Code"; Code[10])
        {
            Caption = 'PCC/CNH';
            Description = 'CFR le 22/03/2022 => Régie : PCC/CNH';
        }
        field(50179; "Stock Mort"; Option)
        {
            Description = 'AD Le 18-11-2011 => SYMTA + CFR Le 14/09/2021 => Régie, modification des options';
            OptionMembers = Non,"Plus fourni","Définitif";
        }
        field(50180; "Stocké"; Boolean)
        {
            Description = 'AD Le 31-05-2011 => SYMTA (Rien a voir avec la notion non stocké habituel) C''est pour les réappros';
            InitValue = true;
        }
        field(50181; "Fusion en attente sur"; Code[20])
        {
            Description = 'AD Le 31-05-2011 => SYMTA';
            TableRelation = Item."No.";
        }
        field(50182; "Date dernière entrée"; Date)
        {
            Caption = 'Date dernière entrée';
            Description = 'AD Le 30-11-2011 => SYMTA';
        }
        field(50183; "Marquage réception"; Boolean)
        {
            Caption = 'Marquage réception';
            Description = 'AD Le 09-06-2011 => SYMTA';
        }
        field(50184; "Controle ADV pour stockage"; Boolean)
        {
            Description = 'LM le 29-03-2013=>adv autorise le stockage de la pieces par les appros';
        }
        field(50186; "Une Etq Reception Par art"; Boolean)
        {
            Description = 'AD Le 30-10-2020 => REGIE';
        }
        field(50200; "Code Concurrence"; Code[10])
        {
            Caption = 'MPL - MPC/CNH';
            Description = 'DZ le 12/03/2012 - CFR le 22/03/2022 => R‚gie : MPL - MPC/CNH';
        }
        field(50201; Nature; Code[10])
        {
            Description = 'DZ le 12/03/2012';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_NATURE'));
        }

        field(50202; "Code pays/région provenance"; Code[10])
        {
            TableRelation = "Country/Region";
            Description = 'FBR le 29/12/21';
        }
        field(50203; "Recommended Shipping Agent"; Code[10])
        {
            TableRelation = "Shipping Agent";
            Caption = 'Shipping Agent Code';
            Description = 'CFR le 06/04/2022 => Régie : transporteur préconisé pour coloration ligne vente';
        }
        field(50204; "Mandatory Shipping Agent"; Boolean)
        {
            Caption = 'Shipping Agent Code';
            Description = 'CFR le 30/11/2022 => Régie : transporteur préconisé peut être obligatoire';
        }
        field(50299; "Nb Contenu Emplacement"; Integer)
        {
            CalcFormula = Count("Bin Content" WHERE("Item No." = FIELD("No.")));
            Description = 'AD Le 03-07-2014';
            FieldClass = FlowField;
        }
        field(50300; "Envoyé MINILOAD"; Boolean)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50301; "Date Envoi MINILOAD"; Date)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50302; "Heure Envoi MINILOAD"; Time)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50303; "Box Type"; Option)
        {
            Caption = 'Type de bac';
            Description = 'MC Le 04-06-2012 => MINILOAD';
            OptionMembers = SmallBox,BigBox;
        }
        field(50304; "date maj stock mort"; Date)
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
        }
        field(50305; "user maj stock mort"; Code[20])
        {
            Description = 'LM Le 06-08-2012 =>SYMTA ->';
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50306; "date maj stocké"; Date)
        {
            Description = 'LM Le 25-01-2012=>SYMTA';
        }
        field(50307; "user maj stocké"; Code[20])
        {
            Description = 'LM Le 25-01-2012=>SYMTA';
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50308; "date maj multiple"; Date)
        {
            Caption = 'date maj multiple';
            Description = 'MCO Le 08-06-2017 => Régie';
        }
        field(50309; "user maj multiple"; Code[20])
        {
            Caption = 'user maj multiple';
            Description = 'MCO Le 08-06-2017 => Régie';
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50704; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
        field(50800; "Create User ID"; Code[20])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50801; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50802; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50805; "Modify User ID"; Code[20])
        {
            Caption = 'Code utilisateur Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50806; "Modify Date"; Date)
        {
            Caption = 'Date Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(50807; "Modify Time"; Time)
        {
            Caption = 'Heure Modification';
            Description = 'TRACABILITE ENREGISTREMENT';
        }
        field(60010; Longueur; Decimal)
        {
            Caption = 'Longueur (Cm)';
            DecimalPlaces = 3 : 3;
            MinValue = 0;
            Description = 'CFR le 11/05/2022 => Régie -> Non editable car mis a jour par la fenêtre des unités';
            Editable = false;
        }
        field(60011; Largeur; Decimal)
        {
            Caption = 'Largeur  (Cm)';
            DecimalPlaces = 3 : 3;
            MinValue = 0;
            Description = 'CFR le 11/05/2022 => Régie -> Non editable car mis a jour par la fenêtre des unités';
            Editable = false;
        }
        field(60012; Epaisseur; Decimal)
        {
            Caption = 'Epaisseur (Cm)';
            DecimalPlaces = 3 : 3;
        }
        field(60050; "Groupe Véhicule"; Code[10])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_GRP_VEHICULE'));
        }
        field(60051; "Groupe Marque"; Code[10])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_GRP_MARQUE'));
        }
        field(60052; "Groupe Famille"; Code[10])
        {
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_GRP_FAMILLE'));
        }
        field(60100; "Date Inventaire PSM"; Date)
        {
            Description = 'DZ le 19-09-2012 => PSM Inventaire';
        }
        field(60200; Publiable; Option)
        {
            Description = 'WF le 28-04-2010 => FARGROUP -> Site eCommerce';
            OptionCaption = 'Non,ZZ-1-Non utilisé,Web,ZZ-2-Non utilisé';
            OptionMembers = Non,Catalogue,Web,"Catalogue+Web";
        }
        field(60201; "Stock Maxi Publiable"; Decimal)
        {
            Description = 'AD Le 07-12-2011';
        }
        field(60202; "Date Modif. Publiable Web"; Date)
        {
            Description = 'AD Le 28-04-2019 => FE20190425';
            Editable = false;
        }
        field(60203; "Date Modif. Super Fam. Mark"; Date)
        {
            Description = 'AD Le 31-05-2019 => FE20190425';
            Editable = false;
        }
        field(60204; "Date Modif. Fam. Mark"; Date)
        {
            Description = 'AD Le 31-05-2019 => FE20190425';
            Editable = false;
        }
        field(60205; "Code Marketing"; Code[20])
        {
            Description = 'WF le 28-04-2010 => FARGROUP -> Site eCommerce';
        }
        field(60206; "Libelle Marketing"; Text[50])
        {
            Description = 'WF le 28-04-2010 => FARGROUP -> Site eCommerce';
        }
        field(60207; "Super Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER("Super family"));
        }
        field(60208; "Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER(Family),
                                                            "Code Niveau supérieur" = FIELD("Super Famille Marketing"));
        }
        field(60209; "Sous Famille Marketing"; Code[10])
        {
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            TableRelation = "Famille Marketing".Code WHERE(Type = FILTER("Sous family"),
                                                            "Code Niveau supérieur" = FIELD("Famille Marketing"));
        }
        field(60210; "Respect Sales Multiple"; Boolean)
        {
            Caption = 'Respect Sales Multiple';
            Description = 'MCO Le 07-06-2017 => Régie';
        }
        field(60300; Selection; Boolean)
        {
            Description = 'FE20171009';
        }
        field(60301; "Prix vente actuel"; Decimal)
        {
            Description = 'FE20171009';
        }
        field(60302; "Prix calculé"; Decimal)
        {
            Description = 'FE20171009';
        }
        field(60307; "Super Famille Marketing By Def"; Code[10])
        {
            CalcFormula = Lookup("Item Hierarchies"."Super Famille Marketing" WHERE("Code Article" = FIELD("No."),
                                                                                     "By Default" = CONST(True)));
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            Editable = false;
            FieldClass = FlowField;
        }
        field(60308; "Famille Marketing By Def"; Code[10])
        {
            CalcFormula = Lookup("Item Hierarchies"."Famille Marketing" WHERE("Code Article" = FIELD("No."),
                                                                               "By Default" = CONST(True)));
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            Editable = false;
            FieldClass = FlowField;
        }
        field(60309; "Sous Famille Marketing By Def"; Code[10])
        {
            CalcFormula = Lookup("Item Hierarchies"."Sous Famille Marketing" WHERE("Code Article" = FIELD("No."),
                                                                                    "By Default" = CONST(True)));
            Description = 'GR le 05-10-2015 => Hierarchie marketing';
            Editable = false;
            FieldClass = FlowField;
        }
    }
    keys
    {//todo
        // key(FKey1; "Item Category Code", "Product Group Code")
        // {
        // }
        key(FKey1; "Item Category Code")
        {
        }
        key(FKey2; Publiable)
        {
        }
        key(FKey3; "Variation Conso. Normative (%)")
        {
        }
        key(FKey4; "Product Type")
        {
        }
        key(FKey5; "No. 2")
        {
        }
        key(Key6; "Comodity Code", "Process Blocked")
        {
        }
        key(FKey7; "Code Appro", "Process Blocked")
        {
        }
    }
    fieldgroups
    {
        addlast(DropDown; "Substitutes Exist", "Assembly BOM", "Sales multiple", "Gross Weight", Inventory) { }
    }

}

