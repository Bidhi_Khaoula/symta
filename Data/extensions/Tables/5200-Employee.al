tableextension 50079 Employee extends Employee //5200
{
    fields
    {
        field(50100; "Chef de secteur"; Boolean)
        {
            Description = 'Dessosage';
        }
        field(50101; "Chef d'équipe"; Boolean)
        {
            Description = 'Dessosage';
        }
        field(50102; Administratif; Boolean)
        {
            Description = 'Dessosage';
        }
        field(50103; "Equipe par defaut"; Integer)
        {
            Description = 'Dessosage';
        }
        field(51022; "Heure d'embauche"; Time)
        {
            Description = 'PM le 02/07/14 ==> pour la DPAE';
        }
        field(51036; "Client de rattachement"; Code[10])
        {
            Caption = 'Client Connecting';
            Description = 'Le 18/09/15 (PM) ==> déplacé en 8009720';
            Enabled = false;
        }
    }
    keys
    {
        key(FKey1; "Employment Date", "Last Name")
        {
        }
    }
}

