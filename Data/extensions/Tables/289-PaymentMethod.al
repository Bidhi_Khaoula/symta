tableextension 50047 "Payment Method" extends "Payment Method" //289
{
    fields
    {
        field(50000; "Editer Traite"; Boolean)
        {
            Description = 'AD Le 06-12-2011 => Gestion Traites';
        }
        field(50001; "Commentaires traites"; Text[250])
        {
            Description = 'AD Le 06-12-2011 => Gestion Traites';
        }
        field(50102; "Scénario paiement"; Code[20])
        {
            TableRelation = "Payment Class";
        }
        field(50103; "Regrouper ligne du scénario"; Boolean)
        {
        }
        field(50700; "EDI Code"; Code[10])
        {
            Caption = 'Code EDI';
            Description = 'AD Le 13-02-2009 => Code Utilisé en EDI';
        }
    }
}

