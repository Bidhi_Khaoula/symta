tableextension 50062 "Purchase Line" extends "Purchase Line" //39
{
    fields
    {
        field(50000; "Initial Quantity"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'Editable=No';
        }
        field(50005; "Quantité Etiquette"; Integer)
        {
            Description = 'AD Le 31-10-2009 => FARGROUP -> Pre-reception -> Qte d''étiquette';
        }
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50022; "% Cout Indirect Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % de base standard Nav';
        }
        field(50023; "Frais Généraux Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Frais de base standard Nav';
        }
        field(50025; "Frais LCY"; Decimal)
        {
            Caption = 'Frais DS';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en Eur';
        }
        field(50027; "Frais FCY"; Decimal)
        {
            Caption = 'Frais Devise Commande';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise';
        }
        field(50028; "Frais FCY -> LCY"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise -> Conversion en eros';
            Editable = false;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50045; "Lien Commande client"; Integer)
        {
            BlankNumbers = BlankZero;
            BlankZero = true;
            CalcFormula = Count("Affectation Ligne Achat" WHERE("Purch. Document Type" = FIELD("Document Type"),
                                                                 "Purch. Document No." = FIELD("Document No."),
                                                                 "Purch. Line No." = FIELD("Line No.")));
            Description = 'AD Le 21-03-2013 => SYMTA -> Pour lier achat a vente';
            Editable = false;
            FieldClass = FlowField;
            trigger OnLookup()
            var
                LLien: Record "Affectation Ligne Achat";
                LFrmLien: Page "Affectation Ligne Achat";
            begin
                LLien.SETRANGE("Purch. Document Type", "Document Type");
                LLien.SETRANGE("Purch. Document No.", "Document No.");
                LLien.SETRANGE("Purch. Line No.", "Line No.");
                LFrmLien.SETTABLEVIEW(LLien);
                LFrmLien.RUNMODAL();
            end;

        }
        field(50050; "Recherche référence"; Code[40])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            TableRelation = IF (Type = CONST(" ")) "Standard Text"
            ELSE
            IF (Type = CONST("G/L Account")) "G/L Account"
            ELSE
            IF (Type = CONST(Item)) Item
            ELSE
            IF (Type = CONST(Resource)) Resource
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge";
            ValidateTableRelation = false;
        }
        field(50060; "Vendor Shipment No."; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 20-01-2014 - AD Le 24-03-2015 Champ Activé';
            Editable = false;
        }
        field(50061; "Posted Whse. Receipt No."; Code[20])
        {
            Caption = 'No.';
            Description = 'AD Le 20-01-2014';
            Editable = false;
            Enabled = false;
        }
        field(50062; "Whse. Receipt No."; Code[20])
        {
            Caption = 'Whse. Receipt No.';
            Description = 'AD Le 20-01-2014';
            Editable = false;
            Enabled = false;
        }
        field(50063; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
            Editable = false;
        }
        field(50070; "Vendor Code Appro"; Code[10])
        {
            CalcFormula = Lookup(Vendor."Code Appro" WHERE("No." = FIELD("Buy-from Vendor No.")));
            Caption = 'Code appro fournisseur';
            Description = 'CFR le 19/11/2020 : Régie > Code Appro du fournisseur\';
            FieldClass = FlowField;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ART_CODE_APPRO'));
        }
        field(50071; Commentaires; Text[50])
        {
            Description = 'GRI le 22/06/23 - FA20230525';
        }
        field(50101; "Direct Unit Cost Ctrl Fac"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Direct Unit Cost';
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
        }
        field(50102; "Line Discount % Ctrl Fac"; Decimal)
        {
            Caption = 'Line Discount %';
            DecimalPlaces = 0 : 5;
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = -100;
        }
        field(50105; "Net Unit Cost ctrl fac"; Decimal)
        {
            AutoFormatExpression = 'Currency Code';
            AutoFormatType = 2;
            Caption = 'Unit Cost';
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
        }
        field(50106; "Initial Quantity ctrl fac"; Decimal)
        {
            Caption = 'Quantité initiale origine';
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
        }
        field(50107; "Net Unit Cost"; Decimal)
        {
            BlankZero = true;
            Caption = 'Cout unitaire net';
            DecimalPlaces = 2 : 2;
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = false;
        }
        field(50150; "Déjà Extraite sur Facture"; Boolean)
        {
            Description = 'AD Le 04-12-2012 => A oui si la réception est sur une facture en cours non validée';
            Enabled = false;
        }
        field(50180; "Stocké"; Boolean)
        {
            Description = 'AD Le 06-02-2015 => SYMTA (Rien a voir avec la notion non stocké habituel) C''est pour les réappros';
            Editable = false;
            Enabled = false;
            InitValue = true;
        }
        field(50181; "Manufacturer Code"; Code[10])
        {
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("No.")));
            Caption = 'Manufacturer Code';
            Description = 'AD Le 06-02-2015 => SYMTA';
            Editable = false;
            Enabled = false;
            FieldClass = FlowField;
            TableRelation = Manufacturer;
        }
        field(50200; "Qty On Fac Receipt"; Decimal)
        {
            CalcFormula = Sum("Import facture tempo. fourn.".Quantité WHERE("No commande affecté" = FIELD("Document No."),
                                                                             "No ligne de commande" = FIELD("Line No."),
                                                                             "No de reception" = CONST()));
            Caption = 'Quantité sur réception';
            Description = 'MC Le 14-09-2011 => FLOWFIELD permettant de calculer la quantité sur l''import de facture';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50201; "Qty to attach"; Decimal)
        {
            Caption = 'Quantité à pointer';
            Description = 'MC Le 14-09-2011 => Possibilité d''attacher partiellement une ligne de pré-reception. Module : Import factures achats';
        }
        field(50202; "Qté sur facture"; Decimal)
        {
            DecimalPlaces = 0 : 2;
        }
        field(50203; "N° Fac"; Code[20])
        {
            Description = 'MC Le 15-11-2011 => Possibilité de voir le numéro de facture lors de l''affectation d''une ligne';
        }
        field(50204; "Qty On Receipt Order Manual"; Decimal)
        {
            CalcFormula = Sum("Warehouse Receipt Line"."Qty. to Receive" WHERE("Source No." = FIELD("Document No."),
                                                                                "Source Line No." = FIELD("Line No."),
                                                                                "Source Type" = CONST(39)));
            Caption = 'Qty On Receipt Order';
            Description = 'MC Le 14-09-2011 => FLOWFIELD permettant de calculer la quantité sur réception';
            FieldClass = FlowField;
        }
        field(50205; "Regularisation retour"; Option)
        {
            Description = 'LM le 29-10-2012 => gestion des retours | FE20171019';
            OptionMembers = " ",Avoir,Echange,"Refusé","Non réclamé","Réparé",Echantillon,Ferraille,"Annule réception",Montage,"Reçu par erreur","Erreur entrée/NAV";
        }
        field(50206; "Demande retour"; Option)
        {
            Description = 'LM le 29-10-2012 =>gestion des retours';
            OptionMembers = " ","Demandé","Accepté","Expédié","Soldé";
            AccessByPermission = TableData "Warehouse Entry" = rm;
        }
        field(50209; "Qte Sur Facture Frn"; Decimal)
        {
            BlankZero = true;
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 21-03-2013 => Pour voir combien le frn a facturé en cas de litige';
            Editable = true;
        }
        field(50210; "Whse Receipt No."; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Min("Posted Whse. Receipt Line"."Whse. Receipt No." WHERE("Posted Source Document" = CONST("Posted Receipt"),
                                                                              "Posted Source No." = FIELD("Receipt No.")));
            Caption = 'N° Réception entrepôt';
            Description = 'CFR le 25/10/2023 - Régie';
            Editable = false;
        }
        field(50300; "Recept Exced"; Boolean)
        {
            Description = 'MCO Le 03-10-2018';
        }
        field(50400; "Commentaire Réception"; Boolean)
        {
            CalcFormula = Exist("Purch. Comment Line" WHERE("Document Type" = FIELD(UPPERLIMIT("Document Type")),
                                                             "No." = FIELD("Document No."),
                                                             "Document Line No." = FIELD("Line No."),
                                                             "Display Receipt" = CONST(True)));
            Description = 'AD Le 01-04-2015';
            FieldClass = FlowField;
        }
        field(50500; "Vendor Order No."; Code[35])
        {
            Caption = 'Vendor Order No.';
            Description = 'MC Le 27-09-2016 => Régie';
        }
        field(50501; "Type de commande"; Option)
        {
            Caption = 'Order Type';
            Description = 'MC Le 27-09-2016 => Régie';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50502; "Currency Code 2"; Code[20])
        {
            CalcFormula = Lookup("Purch. Inv. Header"."Currency Code" WHERE("No." = FIELD("Document No.")));
            Caption = 'Currency Code';
            Description = 'MC Le 27-09-2016 => Régie';
            Enabled = false;
            FieldClass = FlowField;
        }
        field(50503; "Location Code Temp"; Code[10])
        {
            Caption = 'Return Location Code';
            Description = 'GB Le 19-09-2017 => Reprise dev perdu FE20150608';
            TableRelation = Location WHERE("Use As In-Transit" = CONST(false));
        }
        field(50504; "Bin Code Temp"; Code[20])
        {
            Caption = 'Return Bin Code';
            Description = 'GB Le 19-09-2017 => Reprise dev perdu FE20150608';
            TableRelation = IF ("Document Type" = FILTER("Return Order"),
                                Quantity = FILTER(>= 0)) "Bin Content"."Bin Code" WHERE("Location Code" = FIELD("Location Code Temp"),
                                                                                      "Item No." = FIELD("No."),
                                                                                      "Variant Code" = FIELD("Variant Code"))
            ELSE
            Bin.Code WHERE("Location Code" = FIELD("Location Code Temp"));
        }
        field(50505; "Location Code Orig"; Code[10])
        {
            Description = 'GB Le 19-09-2017 => Reprise dev perdu FE20150608';
        }
        field(50506; "Bin Code Orig"; Code[20])
        {
            Description = 'GB Le 19-09-2017 => Reprise dev perdu FE20150608';
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
    }
    keys
    {
        key(FKey1; "Document Type", Type, "No.", "Variant Code", "Drop Shipment", "Location Code", "Requested Receipt Date")
        {
            SumIndexFields = "Outstanding Qty. (Base)";
        }
        key(FKey2; "Promised Receipt Date")
        {
            MaintainSQLIndex = false;
        }
        key(FKey3; "Requested Receipt Date")
        {
            MaintainSQLIndex = false;
        }
        key(FKey4; "Document No.", "Planned Receipt Date")
        {
        }
        key(FKey5; "Planned Receipt Date")
        {
        }
        key(FKey6; Type, "No.", "Bin Code")
        {
        }
    }
}

