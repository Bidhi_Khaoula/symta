tableextension 50124 "Posted Whse. Receipt Line" extends "Posted Whse. Receipt Line" //7319
{
    fields
    {
        field(50001; "Ref. Active"; Code[20])
        {
            Description = 'AD Le 21-03-2013 => Plus facile pour les recherches';
            Editable = false;
        }
        field(50050; "N° Fusion Réception"; Code[20])
        {
            Description = 'AD Le 16-05-2011 => SYMTA -> Récéption Entrepot';
            Editable = false;
            TableRelation = "Warehouse Receipt Header";
        }
        field(50060; "Source Partner No."; Code[20])
        {
            TableRelation = IF ("Source Type" = CONST(37)) Customer
            ELSE
            IF ("Source Type" = CONST(39)) Vendor;
            Caption = 'N° partenaire source';
            Description = 'CFR le 27/09/2023 - Régie';
            Editable = false;
        }
        field(50061; "Source Partner Name"; Text[50])
        {
            Caption = 'Nom partenaire source';
            Description = 'CFR le 27/09/2023 - Régie';
        }

        field(50100; "N° sequence facture fourn."; Integer)
        {
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50110; "Prix Brut Origine"; Decimal)
        {
            Editable = false;
        }
        field(50111; "Remise 1 Origine"; Decimal)
        {
            Editable = false;
        }
        field(50112; "Net 1 Origine"; Decimal)
        {
            Editable = false;
        }
        field(50113; "Total Net 1 Origine"; Decimal)
        {
            Editable = false;
        }
        field(50114; "Remise 2 Origine"; Decimal)
        {
            Editable = false;
        }
        field(50115; "Net 2 Origine"; Decimal)
        {
            Editable = false;
        }
        field(50116; "Total Origine"; Decimal)
        {
            Editable = false;
        }
        field(50200; "Qte Reçue théorique"; Decimal)
        {
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50201; "Prix Brut Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50202; "Remise 1 Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50203; "Remise 2 Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50210; "Litige prix"; Boolean)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50300; "Référence fournisseur"; Code[40])
        {
            Description = 'MCO Le 01-04-2015 => Régie';
            Editable = false;
        }
        field(50310; "Receipt Adjustment"; Option)
        {
            Caption = 'Régularisation Réception';
            Description = 'ANI Le 27-11-2017 FE20171019';
            OptionMembers = " ","Echantillon fournisseur","Facturé","Reçu à la place de","Reçu en plus non signalé","Reçu en plus signalé","Réparé","Retour échantillon","Ste Retour NC-Echange","Manque pas signalé","Erreur entrée/NAV";
        }
        field(50311; "Receipt Request"; Option)
        {
            Caption = 'Demande Réception';
            Description = 'ANI Le 27-11-2017 FE20171019';
            OptionCaption = ' ,Demandé,Accepté,Expédié,Soldé';
            OptionMembers = " ",Requested,Accepted,Sent,Ended;
        }
        field(50312; "Date Arrivage Marchandise"; Date)
        {
            Description = 'Fbrun le 10-08-20 FE20200728';
        }
    }
    keys
    {
        key(FKey1; "Posted Source Document", "Posted Source No.")
        {
        }
        key(FKey2; "Whse. Receipt No.", "Source Line No.", "Source No.", "Posted Source No.")
        {
        }
    }
}

