tableextension 50126 "Warehouse Shipment Line" extends "Warehouse Shipment Line" //7321
{
    fields
    {
        field(50000; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 02-11-2015 =>';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50060; "Date Flashage"; Date)
        {
            CalcFormula = Lookup("Warehouse Shipment Header"."Date Flashage" WHERE("No." = FIELD("No.")));
            Description = 'ANI le 14-02-2018';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50061; "Heure Flashage"; Time)
        {
            CalcFormula = Lookup("Warehouse Shipment Header"."Heure Flashage" WHERE("No." = FIELD("No.")));
            Description = 'ANI le 14-02-2018';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50062; "Utilisateur Flashage"; Code[20])
        {
            CalcFormula = Lookup("Warehouse Shipment Header"."Utilisateur Flashage" WHERE("No." = FIELD("No.")));
            Description = 'ANI le 14-02-2018';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50082; "WIIO Préparation"; Boolean)
        {
            Description = 'WIIO -> Ligne préparée';
        }
        field(50083; "Qty. Packing List"; Decimal)
        {
            CalcFormula = Sum("Packing List Line"."Line Quantity" WHERE("Whse. Shipment No." = FIELD("No."),
                                                                         "Whse. Shipment Line No." = FIELD("Line No."),
                                                                         "Package No." = FILTER(<> '')));
            Caption = 'Qté dans Liste de colisage';
            FieldClass = FlowField;
        }
        field(50151; "Gerer par groupement"; Boolean)
        {
            Description = 'FB Le 26-04-2011 => Symta';
        }
        field(60200; "Packing List No."; Code[20])
        {
            CalcFormula = Lookup("Warehouse Shipment Header"."Packing List No." WHERE("No." = FIELD("No.")));
            Caption = 'Packing List No.';
            Description = 'SFD20201005';
            Editable = false;
            FieldClass = FlowField;
        }
    }
    keys
    {
        //todo
        //     key(FKey1; "No.", "Source Type", "Source Subtype", "Source No.", "Gerer par groupement")
        //     {
        //     }
        key(FKey2; "Item No.", "Bin Code")
        {
        }
    }
}

