tableextension 50113 "Purchase Price" extends "Purchase Price" //7012
{
    fields
    {
        field(50050; "Code Remise"; Code[10])
        {
            CalcFormula = Lookup("Item Vendor"."Code Remise" WHERE("Item No." = FIELD("Item No."),
                                                                  "Vendor No." = FIELD("Vendor No."),
                                                                    "Variant Code" = FIELD("Variant Code")));
            Description = 'CFR le 15/09/2021 => Régie';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50100; "Vendor Item Description"; Text[50])
        {
            CalcFormula = Lookup("Item Vendor"."Vendor Item Desciption" WHERE("Item No." = FIELD("Item No."),
                                                                             "Vendor No." = FIELD("Vendor No.")));
            Caption = 'Vendor Item No.';
            Description = 'CFR le 15/09/2021 => Régie';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50106; "Vendor Name"; Text[100])
        {
            CalcFormula = Lookup(Vendor.Name WHERE("No." = FIELD("Vendor No.")));
            Caption = 'Vendor Name';
            Description = 'CFR le 15/09/2021 => Régie';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50200; "Marked For Deletion"; Boolean)
        {
            Caption = 'Marqué pour suppression';
            Description = 'CFR Le 17-06-2016 FE20180726';
        }
        field(70000; "Référence Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 10-05-2010 => Permet de stocker la ref active pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
        field(70001; "Prix net achat"; Decimal)
        {
            Description = 'LM Le 16-01-2013 => Permet de tracer l''historiques des prix nets achats';
        }
        field(70005; "Vendor Item No."; Text[50])
        {
            CalcFormula = Lookup("Item Vendor"."Vendor Item No." WHERE("Item No." = FIELD("Item No."),
                                                                      "Vendor No." = FIELD("Vendor No.")));
            Caption = 'Vendor Item No.';
            Description = 'AD Le 24-03-2016 => Permet de stocker la ref pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
    }
    keys
    {
        key(FKey1; "Vendor No.", "Ending Date")
        {
        }
    }
}

