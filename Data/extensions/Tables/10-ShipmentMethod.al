tableextension 50001 "Shipment Method" extends "Shipment Method" //10
{
    fields
    {
        field(50100; "Show Risk Transfer Address"; Boolean)
        {
            Caption = 'Afficher adresse transfert de risque';
            Description = 'CFR le 03/09/2021 - SFD20210201 Incoterm 2020';
        }
        field(50103; "Montant Franco Port"; Decimal)
        {
            Description = 'AD Le 27-10-2008 => Montant du franco de port';
        }
    }
}

