tableextension 50088 "VAT Rate Change Setup" extends "VAT Rate Change Setup" //550
{
    Caption = '';
    fields
    {
        modify("Primary Key")
        {
            Caption = '';
        }
        modify("Update Gen. Prod. Post. Groups")
        {
            caption = '';
        }
        modify("Update G/L Accounts")
        {
            caption = '';
        }
        modify("Update Items")
        {
            caption = '';
        }
        modify("Update Item Templates")
        {
            caption = '';
        }
        modify("Update Item Charges")
        {
            caption = '';
        }
        modify("Update Resources")
        {
            caption = '';
        }
        modify("Update Gen. Journal Lines")
        {
            caption = '';
        }
        modify("Update Gen. Journal Allocation")
        {
            caption = '';
        }
        modify("Update Std. Gen. Jnl. Lines")
        {
            caption = '';
        }
        modify("Update Res. Journal Lines")
        {
            caption = '';
        }
        modify("Update Job Journal Lines")
        {
            caption = '';
        }
        modify("Update Requisition Lines")
        {
            caption = '';
        }
        modify("Update Std. Item Jnl. Lines")
        {
            caption = '';
        }
        modify("Update Service Docs.")
        {
            caption = '';
        }
        modify("Update Serv. Price Adj. Detail")
        {
            caption = '';
        }
        modify("Update Sales Documents")
        {
            caption = '';
        }
        modify("Update Purchase Documents")
        {
            caption = '';
        }
        modify("Update Production Orders")
        {
            caption = '';
        }
        modify("Update Work Centers")
        {
            caption = '';
        }
        modify("Update Machine Centers")
        {
            caption = '';
        }
        modify("Update Reminders")
        {
            caption = '';
        }
        modify("Update Finance Charge Memos")
        {
            caption = '';
        }
        modify("VAT Rate Change Tool Completed")
        {
            caption = '';
        }
        modify("Ignore Status on Sales Docs.")
        {
            caption = '';
        }
        modify("Ignore Status on Purch. Docs.")
        {
            caption = '';
        }
        modify("Perform Conversion")
        {
            caption = '';
        }
        modify("Item Filter")
        {
            caption = '';
        }
        modify("Account Filter")
        {
            caption = '';
        }
        modify("Resource Filter")
        {
            caption = '';
        }
        modify("Ignore Status on Service Docs.")
        {
            caption = '';
        }
        modify("Update Unit Price For G/L Acc.")
        {
            caption = '';
        }
        modify("Upd. Unit Price For Item Chrg.")
        {
            caption = '';
        }
        modify("Upd. Unit Price For FA")
        {
            caption = '';
        }


    }
}

