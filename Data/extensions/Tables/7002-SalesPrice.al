tableextension 50111 "Sales Price" extends "Sales Price" //7002
{
    fields
    {
        modify("Sales Code")
        {
            TableRelation = IF ("Sales Type" = CONST("Customer Price Group")) "Customer Price Group"
            ELSE
            IF ("Sales Type" = CONST(Customer)) Customer
            ELSE
            IF ("Sales Type" = CONST(Campaign)) Campaign
            ELSE
            IF ("Sales Type" = CONST("Sous Groupe tarif")) "Generals Parameters".Code WHERE(Type = CONST('CLI_SOUS_GRP_TAR'))
            ELSE
            IF ("Sales Type" = CONST(Centrale)) Customer."No.";
        }
        field(50000; "Prix catalogue"; Decimal)
        {
            Description = '// MC Le 19-04-2011 => SYMTA -> Gestion des tarifs.';

        }
        field(50001; Coefficient; Decimal)
        {
            Description = '// MC Le 19-04-2011 => SYMTA -> Gestion des tarifs.';
            InitValue = 1;
        }
        field(50002; "user maj prix"; Code[20])
        {
            Description = 'LM le 04-12-2013';
        }
        field(50030; "Manufacturer Code"; Code[10])
        {
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("Item No.")));
            Caption = 'Manufacturer Code';
            Description = 'MCO Le 08-06-2017 => Régie';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = Manufacturer;

        }
        field(50031; "Item Category Code"; Code[20])
        {
            CalcFormula = Lookup(Item."Item Category Code" WHERE("No." = FIELD("Item No.")));
            Caption = 'Item Category Code';
            Description = 'MCO Le 08-06-2017 => Régie';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = "Item Category";
        }
        field(50032; "Product Group Code"; Code[20])
        {
            CalcFormula = Lookup(Item."Item Category Code" WHERE("No." = FIELD("Item No.")));
            Caption = 'Product Group Code';
            Description = 'MCO Le 08-06-2017 => Régie';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = "Item Category".Code;
        }
        field(50402; "Type de commande"; Option)
        {
            Description = 'AD Le 04-11-2011 => SYMTA -> REMISES VENTES';
            OptionMembers = Tous,"Dépannage","Réappro",Stock;
        }
        field(70000; "Référence Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 10-05-2010 => Permet de stocker la ref active pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
        field(99999; "TMP AD"; Boolean)
        {
        }
    }
    keys
    {
        //todo
        // key(FKey1; "Item No.", "Sales Type", "Sales Code", "Type de commande", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity")
        // {
        // }
    }

}

