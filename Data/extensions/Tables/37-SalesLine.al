tableextension 50058 "Sales Line" extends "Sales Line" //37
{
    fields
    {
        modify("Item Category Code")
        {
            Caption = 'Item Category Code';
        }
        field(50000; "Initial Quantity"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 28-08-2009 => Enregistre la quantité initialement prévu sur la commande en cas d''abandon de reliquat';
            Editable = false;
        }
        field(50002; Kit; Boolean)
        {
            CalcFormula = Exist("BOM Component" WHERE("Parent Item No." = FIELD("No.")));
            Description = 'AD Le 02-11-2015 => Flowfield si c''est un Kit';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50003; "Blanket Order Quantity Outstan"; Decimal)
        {
            Caption = 'Qté Cde Ouverte Restante';
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 21-10-2009 => Qte resstante a transformer en commande sur la commande ouverte  (reservation)';
            Editable = false;
        }
        field(50006; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'MC Le 17-05-2010 =>Analyse complémentaire FARGROUP';
        }
        field(50007; "Dimension Hors Norme"; Boolean)
        {
            Caption = 'Dimension Hors Norme';
            Description = 'AD Le 24-03-2016';
        }
        field(50010; "Line type"; Enum "Line type")
        {
            Caption = 'Type ligne';
            Description = 'AD Le 08-09-2009 => FARGROUP -> Type de ligne de commande';

        }
        field(50011; "Campaign No."; Code[20])
        {
            Caption = 'Campaign No.';
            Description = 'AD Le 16-11-2009 => Pour faire suivre une campagne à la ligne (Tarif + RFA)';
            Editable = false;
            TableRelation = Campaign;

        }
        field(50016; "Sans mouvement de stock"; Boolean)
        {
            Caption = 'Not stored';
            Description = 'AD Le 17-09-2009 => Non Stocké';

        }
        field(50020; Urgent; Boolean)
        {
            Description = 'CFR le 18/11/2020 : Régie > remises express';
        }
        field(50040; "% Participation port"; Code[20])
        {
            Description = ' MC Le 27-04-2011 => SYMTA -> Transport';
            TableRelation = Resource;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50050; "Recherche référence"; Code[40])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence.MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            TableRelation = IF (Type = CONST(" ")) "Standard Text"
            ELSE
            IF (Type = CONST("G/L Account")) "G/L Account"
            ELSE
            IF (Type = CONST(Item)) Item
            ELSE
            IF (Type = CONST(Resource)) Resource
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge";
            ValidateTableRelation = false;
        }
        field(50051; "Référence saisie"; Code[40])
        {
            Description = 'MC Le 06-06-2011 => SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            Editable = false;
        }
        field(50052; "Originally Ordered No. Pour BL"; Code[20])
        {
            Caption = 'Originally Ordered No.';
            Description = 'AD Le 17-04-2012 => SYMTA -> Pour faire suivre dans les BLs';
            Editable = false;
            Enabled = false;
        }
        field(50060; "Qté Ouverte avant validation"; Decimal)
        {
            Description = '// AD Le 04-11-2009 => Pour stocker la quantité qui devait être livrée (pour le taux de service notament)';
            Editable = true;
            Enabled = false;
        }
        field(50065; "Qte Extraire sur Retour"; Decimal)
        {
            Description = 'AD Le 27-09-2016 => REGIE -> Qte déja extraite sur des retours';
            Editable = false;
            Enabled = false;
        }
        field(50066; "RV Defective Location"; Code[10])
        {
            Caption = 'Localisation';
            Description = 'CFR le 30/11/2022 => Régie : Localisation des piŠces déffectueuses';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('RV_LOCATION'));
        }
        field(50070; "Additional Available"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Exist("Additional Item" WHERE("Item No." = FIELD("No.")));
            Caption = 'Substitution Available';
            Description = 'CFR le 24/09/2021 => SFD20210201 articles complémentaires';
            Editable = false;
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = -100;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50107; "Net Unit Price"; Decimal)
        {
            BlankZero = true;
            Caption = 'Prix unitaire net';
            DecimalPlaces = 2 : 2;
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
            Editable = false;
        }
        field(50110; "Print Wharehouse Shipment"; Boolean)
        {
            Caption = 'Impression exp. magasin';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50111; "Print Shipment"; Boolean)
        {
            Caption = 'Impression expédition';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50112; "Print Invoice"; Boolean)
        {
            Caption = 'Impression facture';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50113; "Display Order"; Boolean)
        {
            Caption = 'Affichage commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50114; "Print Order"; Boolean)
        {
            Caption = 'Impression commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50115; "Print Quote"; Boolean)
        {
            Caption = 'Impression devis';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50120; "Description 3"; Text[50])
        {
            Caption = 'Informations vente';
            Description = 'CFR le 16/12/2020 - Régie : Informations vente';
        }
        field(50140; "Internal Line Type"; Option)
        {
            Caption = 'Type ligne interne';
            Description = 'AD Le 31-08-2009 => Gestion des type de ligne';
            OptionMembers = " ",Shipment,Packing,"Discount Header","Discount Line",RuptureBl,"Participation transport","RuptureClientLivré";
        }
        field(50141; "Transport Shipment No"; Code[20])
        {
            Caption = 'N° de livraison du port';
            Description = 'AD Le 31-08-2009 => Gestion des frais de port';
        }
        field(50150; "A Traiter en préparation"; Decimal)
        {
            Caption = 'A Traiter en préparation';
            DecimalPlaces = 0 : 5;
        }
        field(50151; "Gerer par groupement"; Boolean)
        {
            Description = 'FB Le 26-04-2011 => Symta';
        }
        field(50170; "Opération ADV"; Boolean)
        {
            Caption = 'Opération ADV';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
        }
        field(50171; "Commission %"; Decimal)
        {
            Caption = '% Commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50172; "Commission % Calculate"; Decimal)
        {
            Caption = '% Commission Calculé';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50173; "Header Salesperson Code"; Code[20])
        {
            CalcFormula = Lookup("Sales Header"."Salesperson Code" WHERE("Document Type" = FIELD("Document Type"),
                                                                          "No." = FIELD("Document No.")));
            Caption = 'Salesperson Code';
            Description = 'AD Le 18-01-2016 => MIG2015';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = "Salesperson/Purchaser";
        }
        field(50300; "N° client Livré"; Code[20])
        {
            Description = 'FB Le 10-10-2011 -> Suivi dans la facture pour imprimer par client livré';
            TableRelation = Customer;
        }
        field(50500; Loans; Boolean)
        {
            Caption = 'Prêt';
            Description = '//PRET//';
        }
        field(50501; "Qty To Receive Loans"; Decimal)
        {
            Caption = 'Qté prêt à recevoir';
            Description = '//Prêt Partie retour//';
        }
        field(50502; "Qty To Receive Loans(Base)"; Decimal)
        {
            Caption = 'Qté prêt à recevoir(base)';
            Description = '//Prêt Partie retour//';
            Editable = false;
        }
        field(50503; "Return Qty Loans"; Decimal)
        {
            Caption = 'Qté prêt reçu';
            Description = '//Prêt Partie retour//';
            Editable = false;
        }
        field(50504; "Return Qty Loans(Base)"; Decimal)
        {
            Caption = 'Qté prêt reçu(base)';
            Description = '//Prêt Partie retour//';
            Editable = false;
        }
        field(50700; "EDI Document Line"; Boolean)
        {
            Caption = 'Ligne de commande EDI';
            Description = 'EDI => Indique si c''est une ligne de commande EDI';
            Editable = false;
        }
        field(50701; "EDI Line No."; Integer)
        {
            Caption = 'N° de ligne EDI';
            Description = 'EDI => Numéro de la ligne dans l''EDI';
            Editable = false;
        }
        field(50702; "EDI Document No."; Code[20])
        {
            Caption = 'N° Document EDI';
            Description = 'EDI => N° du document EDI';
            Editable = false;
        }
        field(50710; "EDI Quantity"; Decimal)
        {
            Caption = 'Quantité EDI';
            Description = 'EDI => Quantité EDI';
            Editable = false;
        }
        field(50711; "EDI Net Price"; Decimal)
        {
            Caption = 'Prix Net EDI';
            Description = 'EDI => Prix EDI';
            Editable = false;
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
        field(50900; "Qty To Ship. Save"; Decimal)
        {
            Description = 'MCO Le 08-09-2017 => Régie Problème au niveau des lignes négatives sur expeditions ...';
        }
        field(50910; "Item Manufacturer Code"; Code[10])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("No.")));
            Caption = 'Marque';
            Description = 'CFR : Accessibilité Info Reliquats clients';
            Editable = false;
        }
        field(50911; "Item Code Appro"; Code[10])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Item."Code Appro" WHERE("No." = FIELD("No.")));
            Caption = 'Code Appro';
            Description = 'CFR : Accessibilité Info Reliquats clients';
            Editable = false;
        }
        field(50920; "Marge %"; Decimal)
        {
            Caption = 'Marge / coût unitaire';
            Description = 'CFR le 04/10/2024 - Régie ajout du calcul de marge';
            Editable = false;
        }
        field(50930; "Has Comment"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = Exist("Sales Comment Line" WHERE("Document Type" = FIELD("Document Type"),
                                                      "No." = FIELD("Document No."),
                                                       "Document Line No." = FIELD("Line No.")));
            Caption = 'Commentaire';
            Description = 'CFR le 05/10/2024 - Régie';
            Editable = false;
        }
        field(60050; "No Ecriture Histo Retournée"; Integer)
        {
            Description = 'AD Le 24-04-2010 => GDI -> Pour retourner des historiques';
        }
        field(60051; "Prix Unitaire Doc. Origine"; Decimal)
        {
            Description = 'AD Le 10-10-2011 => Gestion des retours';
        }
        field(70000; "Ne pas recalculer abattement"; Boolean)
        {
            Description = 'MC Le 24-09-2012 => Ne pas recalculer abattement lors de copy document mgt';
        }
    }
    keys
    {
        key(FKey1; "Zone Code")
        {
            MaintainSQLIndex = false;
        }
        key(FKey2; "Document Type", "Document No.", Type)
        {
            MaintainSQLIndex = true;
        }
        key(FKey3; "Document Type", Type, "No.", "Outstanding Quantity")
        {
        }
        key(FKey4; "Document Type", "Sell-to Customer No.", "Currency Code")
        {
            SumIndexFields = "Shipped Not Invoiced (LCY)";
        }
        key(FKey5; "Completely Shipped")
        {
            MaintainSQLIndex = false;
        }
        key(FKey6; "Document Type", Type, "Outstanding Quantity")
        {
        }
        key(FKey7; "Document Type", "Appl.-from Item Entry", "Outstanding Quantity", "Sell-to Customer No.")
        {
            SumIndexFields = "Outstanding Quantity";
        }
        //todo
        // key(Key8; Type, "No.", "Bin Code")
        // {
        // }
    }

}

