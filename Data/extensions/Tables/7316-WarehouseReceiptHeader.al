tableextension 50121 "Warehouse Receipt Header" extends "Warehouse Receipt Header" //7316
{
    fields
    {
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50012; Information; Text[250])
        {
            Caption = 'Information';
            Description = '// CFR le 27/09/2023 - R‚gie : Champ Information';
        }
        field(50013; "Vendor Type No."; Option)
        {
            Caption = 'Type n° doc. fournisseur';
            OptionMembers = ,"Bon de livraison",Facture,Proforma;
            Description = 'CFR le 27/09/2023 - Régie : type de n° de document fournisseur';
        }

        field(50014; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50025; "Frais Port France (DS)"; Decimal)
        {
            Caption = 'Frais Port France (DS)';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en Eur';
            Editable = true;
        }
        field(50027; "Frais Port Maritime (DC)"; Decimal)
        {
            Caption = 'Frais Port Maritime (DC)';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise';
            Editable = true;
        }
        field(50030; "First Line Origin No."; Code[20])
        {
            CalcFormula = Lookup("Warehouse Receipt Line"."Origin n No." WHERE("No." = FIELD("No.")));
            Caption = 'N° Origine Première Ligne';
            Description = 'AD Le 31-01-2020 => REGIE -> Flowflled sur le fournisseur';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50050; "N° Fusion Réception"; Code[20])
        {
            Description = 'AD Le 16-05-2011 => SYMTA -> Récéption Entrepot';
            TableRelation = "Warehouse Receipt Header";
        }
        field(50051; Fusion; Boolean)
        {
            Description = 'MC Le 07-06-2011 => SYMTA -> Récéption Entrepot';
        }
        field(50060; Position; Option)
        {
            Description = 'MC Le 24-10-2011 => SYMTA -> Traçabilité';
            OptionMembers = " ","En magasin";
        }
        field(50061; "En réception"; Boolean)
        {
            Description = 'MC Le 24-10-2011 => SYMTA -> Traçabilité';
        }
        field(50062; "Filtre Fournisseur"; Text[250])
        {
            Description = 'AD Le 27-01-2012';
        }
    }
    keys
    {
        key(Fkey; "Vendor Shipment No.")
        {
        }
    }
}

