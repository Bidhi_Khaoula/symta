tableextension 50112 "Sales Line Discount" extends "Sales Line Discount" //7004
{
    fields
    {
        //todo
        // modify("Sales Code")
        // {
        //     TableRelation = IF ("Sales Type" = CONST("Customer Disc.Group")) "Customer Discount Group"
        //     ELSE
        //     IF ("Sales Type" = CONST(Customer)) Customer
        //     ELSE
        //     IF ("Sales Type" = CONST(Campaign)) Campaign
        //     ELSE
        //     IF ("Sales Type" = CONST("Sous Groupe tarif")) "Generals Parameters".Code WHERE(Type = CONST('CLI_SOUS_GRP_TAR'))
        //     ELSE
        //     IF ("Sales Type" = CONST(Centrale)) "Generals Parameters".Code WHERE(Type = CONST('CLI_CENTRALE'));
        // }
        modify("Line Discount %")
        {
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE -> Non editable';

        }
        modify("Sales Type")
        {
            OptionCaption = 'Customer,Sous Groupe tarif,Centrale,Customer Disc. Group,All Customers,Campaign';

        }

        field(50001; "Line Discount 1 %"; Decimal)
        {
            Caption = '% Remise ligne 1';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';
        }
        field(50002; "Line Discount 2 %"; Decimal)
        {
            Caption = '% Remise ligne 2';
            Description = 'AD Le 15-09-2009 => Gestion des prix/Remise ESKAPE';

        }
        field(50402; "Type de commande"; Option)
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            OptionMembers = "Dépannage","Réappro",Stock;
        }
        field(60012; "Code Marque"; Code[10])
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            TableRelation = Manufacturer.Code;
        }
        field(60013; "Code Famille"; Code[10])
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            TableRelation = "Item Category";
        }
        field(60014; "Code Sous Famille"; Code[10])
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            TableRelation = "Item Category".Code;
        }
        field(60015; "Code Article"; Code[20])
        {
            Description = 'MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            TableRelation = Item;
        }
        field(70000; "Référence Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Code Article")));
            Description = 'AD Le 10-05-2010 => Permet de stocker la ref active pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
    }
    keys
    {//todo
        // key(FKey1; Type, "Code", "Sales Type", "Sales Code", "Starting Date", "Currency Code", "Variant Code", "Unit of Measure Code", "Minimum Quantity", "Type de commande", "Code Marque", "Code Famille", "Code Sous Famille", "Code Article")
        // {
        // }
        key(FKey2; "Code Marque", "Code Famille", "Code Sous Famille")
        {
        }
    }
}

