tableextension 50140 "Warehouse Basic Cue" extends "Warehouse Basic Cue" //9050
{
    fields
    {
        field(50000; "Whse Shipment"; Integer)
        {
            CalcFormula = Count("Warehouse Shipment Header");
            Caption = 'Expéditiion';
            Description = 'AD Le 19-11-2015 => MIG 2015';
            FieldClass = FlowField;
        }
        field(50001; "Whse Receip"; Integer)
        {
            CalcFormula = Count("Warehouse Receipt Header");
            Caption = 'Réception';
            Description = 'AD Le 19-11-2015 => MIG 2015';
            FieldClass = FlowField;
        }
    }
}

