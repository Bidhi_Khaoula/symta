tableextension 50135 "Item Journal Line" extends "Item Journal Line" //83
{
    fields
    {
        field(50000; "A Valider"; Boolean)
        {
            Caption = 'A Valider';
            Description = 'AD Le 21-04-2015 => Lors de la validation de la feuille, ne filtre que sur les champs à vrai';
        }
        field(50010; "Source Responsable"; Code[15])
        {
            Caption = 'Responsable Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = User;
        }
        field(50012; "Invoice Customer No."; Code[20])
        {
            Caption = 'N° Client facturé';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Client Facturé';
            Editable = false;
            TableRelation = Customer;
        }
        field(50013; "Document Code"; Code[20])
        {
            Caption = 'Code Document';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion du type de document';
            Editable = false;
        }
        field(50014; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
            Editable = false;
        }
        field(50016; "Sans mouvement de stock"; Boolean)
        {
            Caption = 'Not stored';
            Description = 'AD Le 17-09-2009 => Non Stocké';
        }
        field(50020; "Manufacturer Code"; Code[10])
        {
            Caption = 'Manufacturer Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Manufacturer;
        }
        field(50023; "External Document No. 2"; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 24-03-2015 => Avoir le no de BL Fournisseur à la ligne';
            Editable = false;
        }
        field(50025; "Sales Line type"; Enum "Line type")
        {
            Caption = 'Type ligne vente';
            Description = 'AD Le 08-09-2009 => FARGROUP -> Type de ligne de commande';
        }
        field(50031; "Campaign No."; Code[20])
        {
            Caption = 'N° campagne';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Campaign;
        }
        field(50032; "Opération ADV"; Boolean)
        {
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
        }
        field(50033; "Commission %"; Decimal)
        {
            Caption = '% Commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50034; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'AD Le 12-11-2009 => FARGROUP';
            Editable = false;
        }
        field(50036; "Requested Delivery Date"; Date)
        {
            Caption = 'Requested Delivery Date';
            Editable = false;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50042; "New Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50050; "Currency Code"; Code[10])
        {
            Caption = 'Currency Code';
            Description = 'AD Le 18-11-2009 => FARGROUP -> Devise achat';
            Editable = false;
            TableRelation = Currency;
        }
        field(50051; "Currency Factor"; Decimal)
        {
            Caption = 'Currency Factor';
            DecimalPlaces = 0 : 15;
            Description = 'AD Le 18-11-2009 => FARGROUP -> Devise achat';
            Editable = false;
            MinValue = 0;
        }
        field(50052; "Recherche référence"; Code[40])
        {
            Caption = 'référence active';
            TableRelation = Item;
            ValidateTableRelation = false;
        }
        field(50060; Commentaire; Text[40])
        {
            Description = 'AD Le 13-01-2012';
        }
        field(50090; "Warehouse Document No."; Code[20])
        {
            Caption = 'N° document magasin';
            Description = 'ANI Le 27-11-2017 FE20171019';
        }
        field(50091; "Source Document No."; Code[20])
        {
            Caption = 'N° document origine';
            Description = 'ANI Le 27-11-2017 FE20171019';
        }
        field(50092; "Référence saisie"; Code[40])
        {
            Description = 'FBR LE 10-08-20 FE20200728 - Symta - ajout de champs';
            Editable = false;
        }
        field(50250; "Return Appl.-from Item Entry"; Integer)
        {
            Caption = 'Appl.-from Item Entry';
            Description = 'MC Le 31-01-2013 => Stats conso retour à la date de l''écriture origine';
            MinValue = 0;
        }
        field(50300; "Envoyé MINILOAD"; Boolean)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50301; "Date Envoi MINILOAD"; Date)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50302; "Heure Envoi MINILOAD"; Time)
        {
            Description = 'AD Le 13-04-2012 => MINILOAD';
        }
        field(50303; "Quantité traitée par WMS"; Decimal)
        {
            DecimalPlaces = 0 : 0;
            Description = 'MC Le 19-04-2012 => MINILOAD';
            Editable = true;
        }
        field(50304; MiniLoad; Boolean)
        {
            Description = 'MC Le 04-06-2012 => Permet de cibler les lignes à destination de MiniLoad';
        }
        field(50305; "Line No for MiniLoad"; Integer)
        {
            Description = 'MC Le 19-06-2012 => MINILOAD ne gère que 5 caractères. Donc lors de l''envoi on alimente le champ.';
        }
        field(50400; "Quantité PSM"; Decimal)
        {
            Description = 'MC Le 19-09-2012 => Inventaire';
        }
        field(50401; "Date PSM"; Date)
        {
            Caption = 'Date PSM';
            Description = 'MC Le 19-09-2012 => Inventaire';
        }
        field(50402; "Heure PSM"; Time)
        {
            Description = 'MC Le 19-09-2012 => Inventaire';
        }
        field(50403; "Utilisateur PSM"; Code[20])
        {
            Description = 'MC Le 19-09-2012 => Inventaire';
        }
        field(50500; "Warehouse User ID"; Code[50])
        {
            Caption = 'Assigned User ID';
            Description = 'CFR le 05/04/2022 => SFD20210929 : Inventaire, suivi du magasinier';
        }
        field(50501; WIIO_LastPickingInventory; Boolean)
        {
            Description = 'CFR le 05/04/2022 => SFD20210929 : Inventaire, conservation paramétrage tablette';
        }
        field(50502; WIIO_LastStaticInventory; Boolean)
        {
            Description = 'CFR le 05/04/2022 => SFD20210929 : Inventaire, conservation paramétrage tablette';
        }
        field(50707; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }

        field(50801; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Caption = 'Ref. Active';
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
        field(90000; Ref; Code[20])
        {
        }
        field(90001; Produit; Code[20])
        {
        }
        field(90002; marque; Code[20])
        {
        }
    }
    keys
    {
        key(FKey1; "Entry Type", "Location Code", "Bin Code", "Item No.", "Variant Code", "Posting Date")
        {
        }
    }

}

