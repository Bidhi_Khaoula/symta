tableextension 50014 "Purch. Rcpt. Line" extends "Purch. Rcpt. Line" //121
{
    fields
    {
        field(50000; "Initial Quantity"; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            Description = 'Editable=No';
            Enabled = false;
        }
        field(50005; "Quantité Etiquette"; Integer)
        {
            Description = 'AD Le 31-10-2009 => FARGROUP -> Pre-reception -> Qte d''étiquette';
            Enabled = false;
        }
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50022; "% Cout Indirect Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % de base standard Nav';
            Editable = false;
        }
        field(50023; "Frais Généraux Article"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Frais de base standard Nav';
            Editable = false;
        }
        field(50025; "Frais LCY"; Decimal)
        {
            Caption = 'Frais DS';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en Eur';
            Editable = false;
        }
        field(50027; "Frais FCY"; Decimal)
        {
            Caption = 'Frais Devise Commande';
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise';
            Editable = false;
        }
        field(50028; "Frais FCY -> LCY"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> Montant des frais en devise -> Conversion en eros';
            Editable = false;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = ' MC Le 27-04-2011 => SYMTA -> SYM-ZONE';
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50045; "Lien Commande client"; Code[20])
        {
            Description = 'AD Le 21-03-2013 => SYMTA -> Pour lier achat a vente';
            Editable = false;
        }
        field(50050; "Recherche référence"; Code[40])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence. MC Le 06-06-2011 => Passage de la référence externe à 40 car';
            Editable = false;
            TableRelation = IF (Type = CONST("G/L Account")) "G/L Account"
            ELSE
            IF (Type = CONST(Item)) Item
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge";
            ValidateTableRelation = false;
        }
        field(50060; "Vendor Shipment No."; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 20-01-2014';
            Editable = false;
        }
        field(50061; "Posted Whse. Receipt No."; Code[20])
        {
            Caption = 'No.';
            Description = 'AD Le 20-01-2014';
            Editable = false;
        }
        field(50062; "Whse. Receipt No."; Code[20])
        {
            Caption = 'Whse. Receipt No.';
            Description = 'AD Le 20-01-2014';
            Editable = false;
        }
        field(50063; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
            Enabled = false;
        }
        field(50101; "Direct Unit Cost Ctrl Fac"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Direct Unit Cost';
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
        }
        field(50102; "Line Discount % Ctrl Fac"; Decimal)
        {
            Caption = 'Line Discount %';
            DecimalPlaces = 0 : 5;
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50105; "Unit Cost ctrl fac"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 2;
            Caption = 'Unit Cost';
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
            Editable = false;
        }
        field(50106; "Initial Quantity ctrl fac"; Decimal)
        {
            Description = 'MC Le 15-09-2011 => SStock le prix d''origine';
        }
        field(50150; "Déjà Extraite sur Facture"; Boolean)
        {
            CalcFormula = Exist("Purchase Line" WHERE("Document Type" = CONST(Invoice),
                                                       "Receipt No." = FIELD("Document No."),
                                                       "Receipt Line No." = FIELD("Line No.")));
            Description = 'AD Le 04-12-2012 => A oui si la réception est sur une facture en cours non validée';
            FieldClass = FlowField;
        }
        field(50180; "Stocké"; Boolean)
        {
            CalcFormula = Lookup(Item.Stocké WHERE("No." = FIELD("No.")));
            Description = 'AD Le 06-02-2015 => SYMTA (Rien a voir avec la notion non stocké habituel) C''est pour les réappros';
            Editable = false;
            FieldClass = FlowField;
            InitValue = true;
        }
        field(50181; "Manufacturer Code"; Code[10])
        {
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("No.")));
            Caption = 'Manufacturer Code';
            Description = 'AD Le 06-02-2015 => SYMTA';
            Editable = false;
            FieldClass = FlowField;
            TableRelation = Manufacturer;
        }
        field(50200; "Qty On Receipt Order"; Decimal)
        {
            CalcFormula = Sum("Import facture tempo. fourn.".Quantité WHERE("No commande affecté" = FIELD("Document No."),
                                                                             "No ligne de commande" = FIELD("Line No.")));
            Caption = 'Quantité sur réception';
            Description = 'MC Le 14-09-2011 => FLOWFIELD permettant de calculer la quantité sur réception du aux factures temporaires';
            Editable = false;
            Enabled = false;
            FieldClass = FlowField;
        }
        field(50201; "Qty to attach"; Decimal)
        {
            Caption = 'Quantité à pointer';
            Description = 'MC Le 14-09-2011 => Possibilité d''attacher partiellement une ligne de pré-reception. Module : Import factures achats';
            Enabled = false;
        }
        field(50202; "Qté sur facture"; Decimal)
        {
            DecimalPlaces = 0 : 2;
            Enabled = false;
        }
        field(50203; "N° Fac"; Code[20])
        {
            Description = 'MC Le 15-11-2011 => Possibilité de voir le numéro de facture lors de l''affectation d''une ligne';
            Enabled = false;
        }
        field(50209; "Qte Sur Facture Frn"; Decimal)
        {
            Description = 'AD Le 21-03-2013 => Pour voir combien le frn a facturé en cas de litige';
            Editable = false;
        }
        field(50210; "Whse Receipt No."; Code[20])
        {
            Caption = 'N° Réception entrepôt';
            Description = 'CFR le 25/10/2023 - Régie';
            Editable = false;
        }
        field(50400; Commentaire; Boolean)
        {
            Enabled = false;
        }
        field(50500; "Vendor Order No."; Code[35])
        {
            Caption = 'Vendor Order No.';
            Description = 'MC Le 27-09-2016 => Régie';
        }
        field(50501; "Type de commande"; Option)
        {
            Caption = 'Order Type';
            Description = 'MC Le 27-09-2016 => Régie';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50502; "Currency Code 2"; Code[20])
        {
            CalcFormula = Lookup("Purch. Inv. Header"."Currency Code" WHERE("No." = FIELD("Document No.")));
            Caption = 'Currency Code';
            Description = 'MC Le 27-09-2016 => Régie';
            Enabled = false;
            FieldClass = FlowField;
        }
        field(50712; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
    }
    keys
    {
        key(FKey1; Type, "No.")
        {
        }
        key(FKey2; "Pay-to Vendor No.", "Document No.", "Qty. Rcd. Not Invoiced")
        {
            MaintainSQLIndex = false;
        }
        key(FKey3; "Pay-to Vendor No.", "Qty. Rcd. Not Invoiced")
        {
        }
        key(FKey5; "Pay-to Vendor No.")
        {
            SQLIndex = "Pay-to Vendor No.", "Qty. Rcd. Not Invoiced";
        }
    }


}

