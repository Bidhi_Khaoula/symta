tableextension 50054 "Item Ledger Entry" extends "Item Ledger Entry" //32
{
    fields
    {
        field(50000; "Source Family Code 1"; Code[10])
        {
            Caption = 'Code Famille 1 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'));
        }
        field(50001; "Source Family Code 2"; Code[10])
        {
            Caption = 'Code Famille 2 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'));
        }
        field(50002; "Source Family Code 3"; Code[10])
        {
            Caption = 'Code Famille 3 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'));
        }
        field(50005; "Source Activity Code"; Code[10])
        {
            Caption = 'Code Activité Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ACTIVITE'));
        }
        field(50006; "Source Direction Code"; Code[10])
        {
            Caption = 'Code Direction Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_DIRECTION'));
        }
        field(50010; "Return Quantity"; Decimal)
        {
            FieldClass = FlowField;
            CalcFormula = Sum("Return Receipt Line".Quantity WHERE("Appl.-from Item Entry" = FIELD("Entry No.")));
            Caption = 'Quantité retournée';
            Description = 'CFR le 17/12/2021 FA20211414';
        }
        field(50013; "Document Code"; Code[20])
        {
            Caption = 'Code Document';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion du type de document';
            Editable = false;
        }
        field(50014; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
        }
        field(50020; "Manufacturer Code"; Code[10])
        {
            Caption = 'Manufacturer Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Enabled = false;
            TableRelation = Manufacturer;
        }
        field(50023; "External Document No. 2"; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 24-03-2015 => Avoir le no de BL Fournisseur à la ligne';
            Editable = false;
        }
        field(50025; "Sales Line type"; Enum "Line type")
        {
            Caption = 'Type ligne vente';
            Description = 'AD Le 08-09-2009 => FARGROUP -> Type de ligne de commande';
        }
        field(50027; "Hors Normes"; Boolean)
        {
            Caption = 'Hors Normes';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Détermine la qte est > a une qté paramétrée dans le fiche article';
        }
        field(50031; "Campaign No."; Code[20])
        {
            Caption = 'N° campagne';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Campaign;
        }
        field(50033; "Stock Physique"; Decimal)
        {
            Description = 'AD Le 20-01-2014';
            Editable = false;
        }
        field(50041; "Journal Batch Name"; Code[10])
        {
            Caption = 'Journal Batch Name';
            Description = 'CFR le 24/06/2022 - SFD20210929 : Inventaires';
        }
        field(50042; Holding; Code[20])
        {
            Caption = 'Holding';
            Description = 'AD Le 29-01-2020 => REGIE';
            Enabled = false;
            TableRelation = Customer;
        }
        field(50060; Commentaire; Text[40])
        {
            Description = 'AD Le 13-01-2012';
            Editable = false;
        }
        field(50063; "Date Réelle Ecriture"; Date)
        {
            Description = 'AD Le 23-09-2019 => FE201909??';
            Editable = false;
        }
        field(50064; "User Entry"; Code[50])
        {
            Caption = 'User ID';
            Description = 'AD Le 30-01-2020 => REGIE -> Utilisateur qui a validé l''écriture';
            Editable = false;
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;

        }
        field(50080; "Ref Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'MCO Le 09-12-2015 => Référence active de l''article.';
            FieldClass = FlowField;
        }
        field(50081; "Item Card Manufacturer"; Code[10])
        {
            CalcFormula = Lookup(Item."Manufacturer Code" WHERE("No." = FIELD("Item No.")));
            Caption = 'Marque Fiche Article';
            Description = 'AD Le 18-12-2015 => MIG2015 -> Flowfield';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50082; "Item Card Family"; Code[20])
        {
            CalcFormula = Lookup(Item."Item Category Code" WHERE("No." = FIELD("Item No.")));
            Caption = 'Sous Famille Fiche Article';
            Description = 'AD Le 18-12-2015 => MIG2015 -> Flowfield';
            Editable = false;
            FieldClass = FlowField;
        }
        // field(50083; "Item Card Sub Family"; Code[20])
        // {
        //     CalcFormula = Lookup(Item."Item Category Code" WHERE("No." = FIELD("Item No.")));
        //     Caption = 'Sous Famille Fiche Article';
        //     Description = 'AD Le 18-12-2015 => MIG2015 -> Flowfield';
        //     Editable = false;
        //     FieldClass = FlowField;
        // }
        field(50084; "Référence saisie"; Code[40])
        {
            Description = 'FBR LE 10-08-20 FE20200728 - Symta - ajout de champs';
            Editable = false;
        }
        field(50090; "Warehouse Document No."; Code[20])
        {
            Caption = 'N° document magasin';
            Description = 'ANI Le 27-11-2017 FE20171019';
        }
        field(50091; "Source Document No."; Code[20])
        {
            Caption = 'N° document origine';
            Description = 'ANI Le 27-11-2017 FE20171019';
        }
        field(50092; "Return Purchase Adjustment"; Option)
        {
            CalcFormula = Lookup("Purchase Line"."Regularisation retour" WHERE("Document Type" = CONST("Return Order"),
                                                                                "Document No." = FIELD("Source Document No."),
                                                                                "Line No." = FIELD("Document Line No.")));
            Caption = 'Régularisation Retour Achat';
            Description = 'ANI Le 27-11-2017 FE20171019';
            FieldClass = FlowField;
            OptionMembers = " ",Avoir,Echange,"Refusé","Non réclamé","Réparé",Echantillon,Ferraille,Poubelle,"Annule réception",Montage,"Reçu par erreur","Erreur entrée/NAV";
        }
        field(50093; "Return Purchase Request"; Option)
        {
            CalcFormula = Lookup("Purchase Line"."Demande retour" WHERE("Document Type" = CONST("Return Order"),
                                                                         "Document No." = FIELD("Source Document No."),
                                                                         "Line No." = FIELD("Document Line No.")));
            Caption = 'Demande Retour Achat';
            Description = 'ANI Le 27-11-2017 FE20171019';
            FieldClass = FlowField;
            OptionMembers = " ","Demandé","Accepté","Expédié","Soldé";
        }
        field(50094; "Warehouse Receipt Adjustment"; Option)
        {
            CalcFormula = Lookup("Posted Whse. Receipt Line"."Receipt Adjustment" WHERE("Whse. Receipt No." = FIELD("Warehouse Document No."),
                                                                                         "Source Line No." = FIELD("Document Line No."),
                                                                                         "Source No." = FIELD("Source Document No."),
                                                                                         "Posted Source No." = FIELD("Document No.")));
            Caption = 'Régularisation Réception Magasin';
            Description = 'ANI Le 27-11-2017 FE20171019';
            FieldClass = FlowField;
            OptionMembers = " ","Echantillon fournisseur","Facturé","Reçu à la place de","Reçu en plus non signalé","Reçu en plus signalé","Réparé","Retour échantillon","Ste Retour NC-Echange gnalé","Manque pas signalé","Erreur entrée/NAV";
        }
        field(50095; "Warehouse Receipt Request"; Option)
        {
            CalcFormula = Lookup("Posted Whse. Receipt Line"."Receipt Request" WHERE("Whse. Receipt No." = FIELD("Warehouse Document No."),
                                                                                      "Source Line No." = FIELD("Document Line No."),
                                                                                      "Source No." = FIELD("Source Document No."),
                                                                                      "Posted Source No." = FIELD("Document No.")));
            Caption = 'Demande Réception Magasin';
            Description = 'ANI Le 27-11-2017 FE20171019';
            FieldClass = FlowField;
            OptionCaption = ' ,Demandé,Accepté,Expédié,Soldé';
            OptionMembers = " ",Requested,Accepted,Sent,Ended;
        }
        field(50250; "Return Appl.-from Item Entry"; Integer)
        {
            Caption = 'Appl.-from Item Entry';
            Description = 'MC Le 31-01-2013 => Stats conso retour à la date de l''écriture origine';
            MinValue = 0;
        }
        field(50707; "Product Group Code Symta"; Code[10])
        {
            Caption = 'Product Group Code';
        }
    }
    keys
    {
        key(FKey1; "Entry Type", "Order Type", "Order No.")
        {
        }
    }
}

