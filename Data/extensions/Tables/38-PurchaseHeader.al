tableextension 50060 "Purchase Header" extends "Purchase Header" //38
{
    fields
    {
        modify("Buy-from Vendor No.")
        {
            TableRelation = IF ("Document Type" = FILTER("Invoice" | "Credit Memo")) Vendor WHERE("No." = FILTER('*FAC'))
            ELSE
            IF ("Document Type" = FILTER("Quote" | "Order" | "Blanket Order" | "Return Order")) Vendor;
            Description = '// MCO Le 24-03-2016 => Changement du table relation pour facture et avoir';
        }

        field(11000; "Registration No."; Text[20])
        {
            Caption = 'Registration No.';
        }
        field(50000; "Date de dépat estimée (ETD)"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';

        }
        field(50001; "Date d'arrivée estimée (ETA)"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
        }
        field(50002; "Order Dispatch Date"; Date)
        {
            Caption = 'Date d''envoi commande';
            Description = 'CFR le 10/05/2022 => Régie : date purement informative';
        }
        field(50004; "Etat Marchandise"; Option)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
            OptionMembers = " ",Flottant,"A quai";
        }
        field(50005; "Heure réception"; Time)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
        }
        field(50010; "Qté Container 20"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50011; "Qté Container 40"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50012; "Qté Container HQ"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50013; "Qté Container MIX"; Integer)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50020; "Nom du bateau"; Text[30])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50021; "Port livraison"; Text[30])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50025; "No Container"; Code[11])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50026; "No Plomb"; Code[7])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50030; "LC Number"; Code[20])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50031; "LC Date"; Date)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50032; "LC Banque"; Code[10])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
        }
        field(50040; Document; Option)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            InitValue = Non;
            OptionMembers = Oui,Non,"Irregularité",Rien;
        }
        field(50041; "Priorité"; Enum "Delivery Priority")
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            InitValue = "2 : Normal";
        }
        field(50044; "Code Transitaire"; Code[10])
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Pour les achats ASIE';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('ACH-TRANSITAIRE'));
        }
        field(50050; "Type de commande"; Option)
        {
            Caption = 'Order Type';
            Description = 'MC Le 11-05-2011 => SYMTA -> Gestion des couts.';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50060; "Incoterm City"; Text[50])
        {
            Caption = 'Ville Incoterm ICC 2020';
            Description = 'CFR le 03/09/2021 - SFD20210201 Incoterm 2020';
        }
        field(50100; "Work Description"; BLOB)
        {
            Caption = 'Work Description';
            Description = 'CFR le 16/12/2020 - Régie : Description du travail';
        }
        field(50200; "Date Envoi Fax/pdf"; Date)
        {
            Description = 'FAX';
            Editable = false;
        }
        field(50201; "Heure Envoi Fax/Pdf"; Time)
        {
            Description = 'FAX';
            Editable = false;
        }
        field(50202; "Utilisateur Envoi Fax/Pdf"; Code[20])
        {
            Description = 'FAX';
            Editable = false;
            TableRelation = User;
        }
        field(50300; "Create User ID"; Code[20])
        {
            Description = 'MC Le 24-10-2011 => Information de création du record.';
        }
        field(50301; "Réceptionneur"; Code[20])
        {
            Description = 'MC Le 26-10-2011 => Suivi des informations de réception';
        }
        field(50400; "Nb Lignes restantes"; Integer)
        {
            CalcFormula = Count("Purchase Line" WHERE("Document Type" = FIELD("Document Type"),
                                                       "Document No." = FIELD("No."),
                                                       "Outstanding Quantity" = FILTER(> 0)));
            Description = 'MCO Le 14-02-2018 => Régie';
            FieldClass = FlowField;
        }
        field(50799; "Order Create User"; Code[50])
        {
            Caption = 'Code utilisateur Création Cde';
            Description = 'TRAC';
            Editable = true;
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;
        }
        field(50800; "Montant HT facture"; Decimal)
        {
            Caption = 'Montant TTC';
            Description = 'MC Le 30-01-2013 => Le champ devient un TTC';
        }
        field(50801; Periode; Integer)
        {
        }
    }

    keys
    {
        key(FKey1; "Document Type", "Order Date")
        {
        }
        key(FKey2; "Vendor Invoice No.")
        {
        }
    }

    fieldgroups
    {
        addlast(DropDown; "No.", "Vendor Invoice No.") { }
    }
}

