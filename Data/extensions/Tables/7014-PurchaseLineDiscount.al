tableextension 50114 "Purchase Line Discount" extends "Purchase Line Discount" //7014
{
    fields
    {
        field(50001; "Line Discount 1 %"; Decimal)
        {
            Caption = '% Remise ligne 1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
        }
        field(50002; "Line Discount 2 %"; Decimal)
        {
            Caption = '% Remise ligne 2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
        }
        field(50050; "Type de commande"; Option)
        {
            Description = 'MC Le 11-05-2011 => SYMTA -> Gestion des couts.';
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(70000; "Référence Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 10-05-2010 => Permet de stocker la ref active pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}

