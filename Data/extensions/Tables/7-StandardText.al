tableextension 50110 "Standard Text" extends "Standard Text" //7
{
    fields
    {
        field(50110; "Print Wharehouse Shipment"; Boolean)
        {
            Caption = 'Impression exp. magasin';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50111; "Print Shipment"; Boolean)
        {
            Caption = 'Impression expédition';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50112; "Print Invoice"; Boolean)
        {
            Caption = 'Impression facture';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50113; "Display Order"; Boolean)
        {
            Caption = 'Affichage commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50114; "Print Order"; Boolean)
        {
            Caption = 'Impression commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50115; "Print Quote"; Boolean)
        {
            Caption = 'Impression devis';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
    }
}

