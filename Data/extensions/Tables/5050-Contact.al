tableextension 50069 Contact extends Contact //5050
{
    fields
    {
        modify("Organizational Level Code")
        {
            Caption = 'Organizational Level Code';
        }

        field(50007; "Info Contact"; Text[50])
        {
            Description = 'AD Le 09-11-2009 => FARGROUP -> Champ réservé pour l''info de la fiche contact';
        }
        field(50008; "First  Job Responsibilitie"; Code[10])
        {
            CalcFormula = Lookup("Contact Job Responsibility"."Job Responsibility Code" WHERE("Contact No." = FIELD("No.")));
            Caption = 'Première responsabilité';
            Description = 'AD Lookup sur la première responsabilité';
            FieldClass = FlowField;
        }
        field(50040; "Centrale Active"; Code[10])
        {
            Description = 'ANI Le 27-03-2015 => Code centrale (synchro fiche client)';
            TableRelation = Customer;
        }
        field(50090; "Mobile Phone No. bis"; Text[30])
        {
            Caption = 'Mobile Phone No.';
            Description = 'MCO Le 23-03-2016 => Gestion du mobile sur client';
            Enabled = false;
            ExtendedDatatype = PhoneNo;
        }
        field(60051; "Supprimé"; Boolean)
        {
            Description = 'AD Le 21-04-2015 =>identifie les comptes supprimés pour adv et compta';
        }
        field(60100; "Old Address"; Text[50])
        {
            Caption = 'Address';
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60101; "Old Address 2"; Text[50])
        {
            Caption = 'Address 2';
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60102; "Old City"; Text[30])
        {
            TableRelation = IF ("Country/Region Code" = CONST()) "Post Code".City
            ELSE
            IF ("Country/Region Code" = FILTER(<> '')) "Post Code".City WHERE("Country/Region Code" = FIELD("Country/Region Code"));
            ValidateTableRelation = False;
            // TestTableRelation = False;
            Caption = 'City';
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60103; "Old Post Code"; Code[20])
        {
            TableRelation = IF ("Country/Region Code" = CONST()) "Post Code"
            ELSE
            IF ("Country/Region Code" = FILTER(<> '')) "Post Code" WHERE("Country/Region Code" = FIELD("Country/Region Code"));
            ValidateTableRelation = False;
            // TestTableRelation = False;
            Caption = 'Post Code';
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60104; "Old Country/Region Code"; Code[10])
        {
            TableRelation = "Country/Region";
            Caption = 'Country/Region Code';
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60105; "Temporary Latitude"; Decimal)
        {
            Caption = 'Latitude temporaire';
            DecimalPlaces = 2 : 10;
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }
        field(60106; "Temporary Longitude"; Decimal)
        {
            Caption = 'Longitude  temporaire';
            DecimalPlaces = 2 : 10;
            Description = '// CFR le 12/05/2022 => Régie';
            Editable = False;
        }

        field(65000; Latitude; Decimal)
        {
            DecimalPlaces = 2 : 10;
            Description = 'Naviway - GeoLocalisation => Ajout de la latitude';
        }
        field(65001; Longitude; Decimal)
        {
            DecimalPlaces = 2 : 10;
            Description = 'Naviway - GeoLocalisation => Ajout de la longitude';
        }
        field(65003; "Result Code"; Text[30])
        {
            Description = 'Naviway - GeoLocalisation => Ajout du résultat de la requete Google';
            Editable = false;
        }
        field(65004; "Change Coordinate"; Boolean)
        {
            Caption = 'Recalculer les coordonnées';
            Description = 'Naviway - GeoLocalisation => Ajout du résultat de la requete Google';
        }
        field(65005; Categorie; Code[10])
        {
            Description = 'Naviway - Catégorie définition la punaise sur la carte';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('NAVIWAY_CAT'));
        }
    }
}

