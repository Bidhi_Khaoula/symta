tableextension 50006 "Payment Line" extends "Payment Line" //10866
{
    // CFR le 22/09/2021 => Régie : bloquer débit/crédit
    fields
    {

        field(50000; "Désignation"; Text[50])
        {
        }
        field(50001; Selection; Boolean)
        {
        }
        field(50002; "Date de valeur"; Date)
        {
        }
        field(50300; "Pourcentage montant initial"; Decimal)
        {
        }
    }
    keys
    {
        key(FKey1; "Account Type", "Account No.", "Due Date", "Payment Class", "Payment in Progress")
        {
            SumIndexFields = "Amount (LCY)", Amount;
        }
        //todo
        // key(FKey2; "Account Type", "Account No.", "Date de valeur", "Payment in Progress")
        // {
        //     SumIndexFields = "Amount (LCY)", Amount;
        // }
        key(FKey3; "No.", "Due Date")
        {
            SumIndexFields = "Amount (LCY)", Amount;
        }
        key(FKey4; "Due Date", "Credit Amount")
        {
        }
        key(FKey5; "Due Date")
        {
        }
        key(Key6; "Due Date", "Debit Amount")
        {
        }
        key(Key7; "No.", "Due Date", "Debit Amount")
        {
        }
        key(Key8; "No.", "Due Date", "Credit Amount")
        {
        }
        key(Key9; "Due Date", "Copied To Line", "Payment in Progress", "Acceptation Code")
        {
            SumIndexFields = "Amount (LCY)", Amount;
        }
        key(Key10; "Copied To No.", "Status No.", "Payment Class")
        {
            SumIndexFields = "Amount (LCY)", Amount;
        }
    }
}

