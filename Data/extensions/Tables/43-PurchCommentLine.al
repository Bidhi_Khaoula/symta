tableextension 50063 "Purch. Comment Line" extends "Purch. Comment Line"  //43
{
    fields
    {
        field(50113; "Display Order"; Boolean)
        {
            Caption = 'Affichage commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50114; "Print Order"; Boolean)
        {
            Caption = 'Impression commande';
            Description = 'ESK GESTION IMPRESSION COMMENTAIRES';
        }
        field(50115; "Display Receipt"; Boolean)
        {
            Description = 'LM le 230713=> Affichage en saisie de reception';
        }
    }
}

