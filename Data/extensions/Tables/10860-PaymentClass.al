tableextension 50002 "Payment Class" extends "Payment Class" //10860
{
    fields
    {
        field(50000; Sign; Option)
        {
            Caption = 'Sens';
            Description = 'CFR le 22/09/2021 => Régie : bloquer débit/crédit';
            OptionMembers = " ","Crédit","Débit";
        }
    }
}

