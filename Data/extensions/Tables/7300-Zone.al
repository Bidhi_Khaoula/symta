tableextension 50115 Zone extends Zone //7300
{
    fields
    {
        field(50000; "BP Printer Name"; Text[250])
        {
            Caption = 'Printer Name';
            Description = 'AD Le 26-05-2011 => Imprimante pour les BP';
            TableRelation = Printer;
        }
        field(50001; "Label BP Printer Name"; Text[100])
        {
            Caption = 'Nom de l''imprimante Etiquette BP';
            Description = 'AD Le 27-09-2016 => REGIE ->';
        }
    }
}

