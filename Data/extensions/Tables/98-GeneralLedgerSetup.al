tableextension 50146 "General Ledger Setup" extends "General Ledger Setup" //98
{
    fields
    {
        field(50100; "Fichier client"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier client";
                UploadFile();
                "Fichier client" := FileName;
            end;
        }
        field(50101; "Fichier Compta Vente"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier Compta Vente";
                UploadFile();
                "Fichier Compta Vente" := FileName;
            end;
        }
        field(50102; "Fichier reglement"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier reglement";
                UploadFile();
                "Fichier reglement" := FileName;
            end;
        }
        field(50103; "Modéle feuille compta Vente"; Code[10])
        {
            TableRelation = "Gen. Journal Template";
        }
        field(50104; "Feuille compta Vente"; Code[10])
        {
            TableRelation = "Gen. Journal Batch".Name WHERE("Journal Template Name" = FIELD("Modéle feuille compta Vente"));
        }
        field(50105; "Racine fichier client"; Text[30])
        {
        }
        field(50106; "Racine fichier compta vente"; Text[30])
        {
        }
        field(50107; "Fichier fournisseur"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier fournisseur";
                UploadFile();
                "Fichier fournisseur" := FileName;
            end;
        }
        field(50108; "Racine fichier fournisseur"; Text[30])
        {
        }
        field(50109; "Fichier Compta achat"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier Compta achat";
                UploadFile();
                "Fichier Compta achat" := FileName;
            end;

        }
        field(50110; "Racine fichier compta achat"; Text[30])
        {
        }
        field(50111; "Fichier RIB Client"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier RIB Client";
                UploadFile();
                "Fichier RIB Client" := FileName;
            end;

        }
        field(50112; "Racine fichier RIB Client"; Text[30])
        {
        }
        field(50113; "Fichier RIB Fournisseur"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier RIB Fournisseur";
                UploadFile();
                "Fichier RIB Fournisseur" := FileName;
            end;
        }
        field(50114; "Racine fichier RIB Fournisseur"; Text[30])
        {
        }
        field(50115; "Fichier commentaire client"; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier commentaire client";
                UploadFile();
                "Fichier commentaire client" := FileName;
            end;

        }
        field(50116; "Racine fichier comm. client"; Text[30])
        {
        }
        field(50117; "Fichier commentaire four."; Text[250])
        {
            trigger OnLookup()
            begin
                FileName := "Fichier commentaire four.";
                UploadFile();
                "Fichier commentaire four." := FileName;
            end;
        }
        field(50118; "Racine fichier comm. four."; Text[30])
        {
        }
        field(50119; "Modéle feuille compta achat"; Code[10])
        {
            TableRelation = "Gen. Journal Template";
        }
        field(50120; "Feuille compta achat"; Code[10])
        {
            TableRelation = "Gen. Journal Batch".Name WHERE("Journal Template Name" = FIELD("Modéle feuille compta achat"));
        }
        field(50121; "Racine fichier reglement"; Text[30])
        {
        }
    }
    procedure UploadFile()
    var
    // CommonDialogMgt: Codeunit "412";
    // UploadedFileName: Text[250];
    begin
        ERROR('Fonction a revoir avec la 2015');
        //UploadedFileName := CommonDialogMgt.OpenFile('Fichier',FileName,1,'',0);
        //FileName := UploadedFileName;
    end;

    var
        FileName: Text[250];
}

