tableextension 50102 "Value Entry" extends "Value Entry" //5802
{
    fields
    {
        field(50000; "Source Family Code 1"; Code[10])
        {
            Caption = 'Code Famille 1 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'));
        }
        field(50001; "Source Family Code 2"; Code[10])
        {
            Caption = 'Code Famille 2 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'));
        }
        field(50002; "Source Family Code 3"; Code[10])
        {
            Caption = 'Code Famille 3 Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'));
        }
        field(50005; "Source Activity Code"; Code[10])
        {
            Caption = 'Code Activité Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ACTIVITE'));
        }
        field(50006; "Source Direction Code"; Code[10])
        {
            Caption = 'Code Direction Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_DIRECTION'));
        }
        field(50010; "Source Responsable"; Code[15])
        {
            Caption = 'Responsable Origine';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = User;
        }
        field(50012; "Invoice Customer No."; Code[20])
        {
            Caption = 'N° Client facturé';
            Description = 'Editable=No';
            TableRelation = Customer;
        }
        field(50013; "Document Code"; Code[20])
        {
            Caption = 'Code Document';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion du type de document';
            Editable = false;
        }
        field(50014; "Date Arrivage Marchandise"; Date)
        {
            Description = 'AD Le 31-01-2020 => REGIE -> Date d''arrivée de la marchandise chez SYMTA';
        }
        field(50015; "Territory Code"; Code[10])
        {
            Caption = 'Territory Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Territory;
        }
        field(50016; "Sans mouvement de stock"; Boolean)
        {
            Caption = 'Not stored';
            Description = 'AD Le 17-09-2009 => Non Stocké';
        }
        field(50020; "Manufacturer Code"; Code[10])
        {
            Caption = 'Manufacturer Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Manufacturer;
        }
        field(50021; "Item Category Code"; Code[10])
        {
            Caption = 'Item Category Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Item Category";
        }
        field(50022; "Product Group Code"; Code[10])
        {
            Caption = '"Item Category Code';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = "Item Category".Code;
        }
        field(50023; "External Document No. 2"; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 24-03-2015 => Avoir le no de BL Fournisseur à la ligne';
            Editable = false;
        }
        field(50025; "Sales Line type"; Enum "Line type")
        {
            Caption = 'Type ligne vente';
            Description = 'AD Le 08-09-2009 => FARGROUP -> Type de ligne de commande';
        }
        field(50027; "Hors Normes"; Boolean)
        {
            Caption = 'Hors Normes';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Détermine la qte est > a une qté paramétrée dans le fiche article';
        }
        field(50029; "Exclure Consommations"; Boolean)
        {
            Description = 'AD Le 15-11-2011 => SYMTA -> Ne pas prendre dans le calcul des consommantios';
        }
        field(50030; Promo; Boolean)
        {
            Caption = 'Promo';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
        }
        field(50031; "Campaign No."; Code[20])
        {
            Caption = 'N° campagne';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            TableRelation = Campaign;
        }
        field(50032; "Opération ADV"; Boolean)
        {
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Editable = false;
        }
        field(50033; "Commission %"; Decimal)
        {
            Caption = '% Commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50034; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'AD Le 12-11-2009 => FARGROUP';
            Editable = false;
        }
        field(50036; "Requested Delivery Date"; Date)
        {
            Caption = 'Requested Delivery Date';
            Editable = false;
        }
        field(50042; Holding; Code[20])
        {
            Caption = 'Holding';
            Description = 'AD Le 29-01-2020 => REGIE';
            Editable = false;
            TableRelation = Customer;
        }
        field(50050; "Currency Code"; Code[10])
        {
            Caption = 'Currency Code';
            Description = 'AD Le 18-11-2009 => FARGROUP -> Devise achat';
            Editable = false;
            TableRelation = Currency;
        }
        field(50051; "Currency Factor"; Decimal)
        {
            Caption = 'Currency Factor';
            DecimalPlaces = 0 : 15;
            Description = 'AD Le 18-11-2009 => FARGROUP -> Devise achat';
            Editable = false;
            MinValue = 0;
        }
        field(50060; Commenatire; Text[40])
        {
            Description = 'AD Le 13-01-2012';
            Editable = false;
            Enabled = false;
        }
        field(50063; "Date Réelle Ecriture"; Date)
        {
            Description = 'AD Le 23-09-2019 => FE201909??';
            Editable = false;
        }
        field(50064; "User Entry"; Code[50])
        {
            Caption = 'User ID';
            Description = 'AD Le 30-01-2020 => REGIE -> Utilisateur qui a validé l''écriture';
            Editable = false;
            TableRelation = User."User Name";
            //This property is currently not supported
            //TestTableRelation = false;
        }
        field(60000; "N° article Pour Stats"; Code[20])
        {
            Description = 'AD Le 22-07-2011 => SYMTA -> Pour les fusions articles';
            TableRelation = Item;
        }
        field(60050; "Recherche référence"; Code[20])
        {
            Description = 'AD Le 11-12-2009 => GDI/SYMTA -> Multiréférence';
            Editable = false;
            //The property 'ValidateTableRelation' can only be set if the property 'TableRelation' is set
            //ValidateTableRelation = false;
        }
        field(70000; "Récupération Historiques"; Boolean)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70001; "No Commande Histo."; Code[10])
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70002; "No Livraion Histo."; Code[10])
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70003; "Qte Histo."; Decimal)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70004; "Cout Histo."; Decimal)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70005; "Montant Histo."; Decimal)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70010; "Qte Retour Histo."; Decimal)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
        field(70011; "Reste à Retourné"; Decimal)
        {
            Description = 'AD le 20-04-2010 => GDI -> Pour isoler les lignes d''historiques';
        }
    }
    keys
    {  // todo 
        // key(FKey1; "Item No.", "Valuation Date", "Location Code", "Variant Code", "Sans mouvement de stock")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Cost Amount (Expected)", "Cost Amount (Actual)", "Cost Amount (Expected) (ACY)", "Cost Amount (Actual) (ACY)", "Item Ledger Entry Quantity";
        // }
        // key(FKey2; "Item No.", "Posting Date", "Item Ledger Entry Type", "Entry Type", "Variance Type", "Item Charge No.", "Location Code", "Variant Code", "Hors Normes", "Campaign No.")
        // {
        //     SumIndexFields = "Invoiced Quantity", "Sales Amount (Expected)", "Sales Amount (Actual)", "Cost Amount (Expected)", "Cost Amount (Actual)", "Cost Amount (Non-Invtbl.)", "Purchase Amount (Actual)", "Item Ledger Entry Quantity";
        // }
        // key(FKey3; "Item No.", "Posting Date", "Item Ledger Entry Type", "Entry Type", "Variance Type", "Source No.", "External Document No.", "Document Code", "Sales Line type")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Invoiced Quantity", "Sales Amount (Expected)", "Sales Amount (Actual)";
        // }
        // key(FKey4; "Salespers./Purch. Code", "Territory Code", "Source No.")
        // {
        //     MaintainSQLIndex = false;
        // }
        // key(FKey5; "Salespers./Purch. Code", "Source No.", "Posting Date", "Document No.", "Commission %")
        // {
        //     MaintainSQLIndex = false;
        // }
        // key(FKey6; "Source Responsable", "Source No.", "Posting Date", "Document No.", "Commission %")
        // {
        //     MaintainSQLIndex = false;
        // }
        //    key(FKey7; "Item No.", "Posting Date", "Item Ledger Entry Type", "Entry Type", "Variance Type", "Source No.", "Récupération Historiques", "Location Code", "Document No.")
        //    {
        //        MaintainSQLIndex = false;
        //        SumIndexFields = "Invoiced Quantity", "Valued Quantity", "Item Ledger Entry Quantity";
        //    }
        // key(FKey8; "Posting Date", "Item Ledger Entry Type", "Item No.", "Location Code", "Variant Code", "Drop Shipment", "Document Type", "Récupération Historiques")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Invoiced Quantity";
        // }
        // key(Key9; "Source No.", "Document Type", "Récupération Historiques")
        // {
        //     MaintainSQLIndex = false;
        // }
        // key(Key10; "Posting Date", "Source No.", "Item No.", "Manufacturer Code", "Item Ledger Entry Type")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Sales Amount (Actual)";
        // }
        // key(Key11; "Posting Date", "Source No.", "Manufacturer Code", "Item Ledger Entry Type")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Sales Amount (Actual)";
        // }
        // key(Key12; "Posting Date", "Manufacturer Code", "Item Ledger Entry Type", "Source No.")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Sales Amount (Actual)";
        // }
        key(FKey13; "Document No.", "Document Line No.", "User ID")
        {
            MaintainSQLIndex = false;
        }
        // key(Key14; "Invoice Customer No.", "Item Ledger Entry Type", "Posting Date")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Sales Amount (Actual)";
        // }
        key(FKey15; "Posting Date", "Document No.")
        {
        }
    }


}

