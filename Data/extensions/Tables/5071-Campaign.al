tableextension 50072 Campaign extends Campaign  //5071
{
    fields
    {
        field(50000; Promo; Boolean)
        {
            Description = 'AD Le 08-10-2009 => FARGROUP -> Promotions';
        }
        field(50006; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'AD Le 12-11-2009 => FARGROUP';
            Editable = true;
        }
        field(50170; "Rate Commission"; Decimal)
        {
            Caption = 'Coefficient de commission';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Commissions';
            InitValue = 1;
            MinValue = 0;
        }
    }
}

