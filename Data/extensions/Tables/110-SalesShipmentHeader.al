tableextension 50007 "Sales Shipment Header" extends "Sales Shipment Header" //110
{

    fields
    {
        modify("Bill-to Customer No.")
        {
            Caption = 'Bill-to Customer No.';
        }
        field(50000; "Source Document Type"; Code[20])
        {
            Caption = 'Type Origine Document';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Gestion du type de document';
            Editable = false;
        }
        field(50001; "Quote Type"; Enum "Quote Type")
        {
            Caption = 'Type de devis';
            Enabled = false;
            InitValue = Proforma;
        }
        field(50003; "Invoice Customer No."; Code[20])
        {
            Caption = 'N° Client facturé';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Client Facturé';
            Editable = false;
            TableRelation = Customer;
        }
        field(50004; "Client final commande ouverte"; Code[20])
        {
            Caption = 'Client final commande ouverte';
            Description = 'AD Le 07-09-2009 => FARGROUP -> Client final lors de la transfo. d''une commande ouverte en commande normale.';
            Enabled = false;
        }
        field(50005; Responsable; Code[15])
        {
            Caption = 'Responsable';
            Description = 'AD Le 28-08-2009 => FARGROUP';
            Editable = false;
            TableRelation = User;
        }
        field(50006; "Exclure RFA"; Boolean)
        {
            Caption = 'Exclure RFA';
            Description = 'AD Le 12-11-2009 => FARGROUP';
            Editable = false;
        }
        field(50007; "Direction Code"; Code[10])
        {
            Caption = 'Code Direction';
            Description = 'AD Le 25-08-2009 => FARGROUP -> Statistiques';
            Editable = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_DIRECTION'));
        }
        field(50010; "Invoice Type"; Code[10])
        {
            Caption = 'Type Facturation';
            Description = 'AD Le 13-04-2007 => Pour la facturation';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_T_FAC'));
        }
        field(50011; "No Storage Mvt"; Boolean)
        {
            Caption = 'Sans Mouvement de stock';
            Description = 'AD Le 24-04-2007 => Pas de mouvement de stock.';
        }
        field(50012; "Shipment Status"; Enum "Shipment Status")
        {
            Caption = 'Statut Livraison';
            Description = 'AD Le 25-04-2007 => Status de livraison de la commande';
            Editable = false;
        }
        field(50013; "Delivery Priority"; Enum "Delivery Priority")
        {
            Caption = 'Priorité de livraison';
            Enabled = false;
            InitValue = "2 : Normal";
        }
        field(50014; "Select Order"; Boolean)
        {
            Caption = 'Commandé selectionné';
            Description = 'AD Le 19-09-2009 => Selection de préparation de commande';
            Editable = true;
            Enabled = false;
        }
        field(50015; "Can be Prepared"; Option)
        {
            Caption = 'Préparable';
            Description = 'AD Le 19-09-2009 => Selection de préparation de commande';
            Editable = false;
            Enabled = false;
            OptionMembers = Total,Partiel,"Pas de stock";
        }
        field(50016; "Not stored"; Boolean)
        {
            Caption = 'Not stored';
            Description = 'AD Le 17-09-2009 => Non Stocké';
            Editable = false;
        }
        field(50017; "Abandon remainder"; Boolean)
        {
            Caption = 'Abandon reliquat';
            Description = 'cph 14/03/08 => abandon de reliquat';
        }
        field(50019; "Return Status"; Option)
        {
            Caption = 'Statut Retour';
            Description = 'AD Le 27-09-2016 => REGIE -> Status de réception du retour';
            Editable = false;
            Enabled = false;
            OptionMembers = "A Livrer",Reliquat,"Livrée";
        }
        field(50021; "Chiffrage BL"; Boolean)
        {
            Description = 'CPH 08/07/08 - BL Chiffré';
        }
        field(50025; "Export Salesperson"; Boolean)
        {
            Caption = 'Export Salesperson';
            Description = 'FBO le 05-04-2017 => FE20170324';
            Enabled = false;
        }
        field(50041; "Zone Code"; Code[10])
        {
            Caption = 'Code zone';
            Description = 'AD Le 14-09-2011 => Utilisé dans les BL pour connaitre la zone concernée par les lignes du BL';
            Editable = false;
            TableRelation = Zone.Code WHERE("Location Code" = FIELD("Location Code"));
        }
        field(50046; "Avoid Active Central"; Boolean)
        {
            Caption = 'Avoid Active Central';
            Description = 'SFD20210201 historique centrale active';
        }

        field(50060; "Document Soldé"; Boolean)
        {
            Description = 'AD Le 19-10-2015 => SYMTA N''archive pas les commandes => Utilisé pour définir qu''un document est terminé (livré facturé)';
            Editable = false;
            Enabled = false;
        }
        field(50070; "RFA Contract"; Option)
        {
            Caption = 'Contrat RFA';
            Description = '==> FAR.AC.01 Inclusion/Exclusion des contrats RFA';
            Enabled = false;
            OptionCaption = 'Include,Exclude';
            OptionMembers = Include,Exclude;
        }
        field(50080; "Code Enseigne 1"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Customer."Code Enseigne 1" WHERE("No." = FIELD("Sell-to Customer No.")));
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ENSEIGNE'));
            Description = 'CFR le 04/10/2023 - Régie (suivi depuis fiche client)';
            Editable = false;
        }
        field(50081; "Code Enseigne 2"; Code[20])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Customer."Code Enseigne 1" WHERE("No." = FIELD("Sell-to Customer No.")));
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ENSEIGNE'));
            Description = 'CFR le 04/10/2023 - Régie (suivi depuis fiche client)';
            Editable = false;
        }
        field(50090; "Quote Validity Date"; Date)
        {
            Caption = 'Date de validité du devis';
            Description = '[ CFR le 04/10/2023 - Régie : Date de validité des devis]';
        }

        field(50100; "Saturday Delivery"; Boolean)
        {
            Caption = 'Livraison le samedi';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50101; "Instructions of Delivery"; Text[70])
        {
            Caption = 'Instructions de livraison';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50102; "Insurance of Delivery"; Boolean)
        {
            Caption = 'Assurance de livraison';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50103; "Cash on Delivery"; Decimal)
        {
            Caption = 'Contre remboursement';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50104; "Amount Assured"; Decimal)
        {
            Caption = 'Montant Assuré';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50105; "Shipment Sector"; Code[10])
        {
            Caption = 'Secteur Port';
            Description = 'AD Le 27-03-1008 => Gestion du secteur de port';
        }
        field(50106; Preparateur; Code[20])
        {
            Caption = 'Preparateur';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50107; Weight; Decimal)
        {
            Caption = 'Poids';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50108; "Nb Of Box"; Integer)
        {
            Caption = 'Nb de colis';
            Description = 'AD Le 07-04-2007 => Gestion des étiquettes transport';
        }
        field(50109; "N° affrètement"; Code[20])
        {
            Caption = 'N° affrètement';
            Description = 'MC LE 06-01-2009 => FARGROUP -> Affrètements';
        }
        field(50110; "Nb Of Label"; Integer)
        {
            Caption = 'Nb d''étiquettes';
            Description = 'MC Le 06-06-2011 => SYMTA -> Gestion des étiquettes transport';
            Editable = false;
        }
        field(50130; "Incoterm City"; Text[50])
        {
            ; Caption = 'Ville Incoterm ICC 2020';
            Description = 'CFR le 06/04/2023 - Régie : Incoterm ICC 2020 Ventes';
        }
        field(50131; "Incoterm Code"; Code[10])
        {
            TableRelation = "Shipment Method";
            Caption = 'Shipment Method Code';
            Description = 'CFR le 06/04/2023 - Régie : Incoterm ICC 2020 Ventes';
        }

        field(50141; "Mode d'expédition"; enum "Mode Expédition")
        {
            Description = 'MC Le 26-04-2011 => Définition du mode d''expédition. (Normal, Express ...)';
        }
        field(50150; "Material Information"; Text[35])
        {
            Caption = 'Info. matériel';
            Description = 'CFR le 18/04/2024 - Réie : info matériel (tous documents)';
        }
        field(50151; "Transport Information"; Text[35])
        {
            Caption = 'Info. transport';
            Description = 'CFR le 18/04/2024 - Régie : info transport (retours)';
        }
        field(50160; "Suivi Devis"; Option)
        {
            OptionMembers = ,"En cours";
            Description = 'CFR le 18/04/2024 - Régie : suivi devis (devis)';
        }

        field(50200; "Date Envoi Fax/pdf"; Date)
        {
            Description = '// AD Le 04-03-2008 => Gestion des fax';
            Editable = false;
        }
        field(50201; "Heure Envoi Fax/Pdf"; Time)
        {
            Description = '// AD Le 04-03-2008 => Gestion des fax';
            Editable = false;
        }
        field(50202; "Utilisateur Envoi Fax/Pdf"; Code[20])
        {
            Description = '// AD Le 04-03-2008 => Gestion des fax';
            Editable = false;
            TableRelation = User;
        }
        field(50300; "Family Code 1"; Code[10])
        {
            Caption = 'Code Famille 1';
            Description = 'MC Le 22-10-2010 => FARGROUP -> Statistiques';
            Editable = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_1'));
        }
        field(50301; "Family Code 2"; Code[10])
        {
            Caption = 'Code Famille 2';
            Description = 'MC Le 22-10-2010 => FARGROUP -> Statistiques';
            Editable = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_2'));
        }
        field(50302; "Family Code 3"; Code[10])
        {
            Caption = 'Code Famille 3';
            Description = 'MC Le 22-10-2010 => FARGROUP -> Statistiques';
            Editable = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_FAM_3'));
        }
        field(50303; "Activity Code"; Code[10])
        {
            Caption = 'Code Activité';
            Description = 'MC Le 22-10-2010 => FARGROUP -> Statistiques';
            Editable = false;
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_ACTIVITE'));
        }
        field(50400; "Centrale Active"; Code[10])
        {
            Description = 'MC Le 18-04-2011 => SYMTA -> TARIFS VENTES';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_CENTRALE'));
        }
        field(50401; "Sous Groupe Tarifs"; Code[20])
        {
            Description = 'MC Le 18-04-2011 => SYMTA -> TARIFS VENTES';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('CLI_SOUS_GRP_TAR'));
        }
        field(50402; "Type de commande"; Option)
        {
            Description = ' MC Le 20-04-2011 => SYMTA -> REMISES VENTES';
            OptionMembers = "Dépannage","Réappro",Stock;
        }
        field(50403; "Groupement payeur"; Boolean)
        {
            Description = 'FB->SYMTA';
        }
        field(50405; "Force Mandatory Shipping Agent"; Boolean)
        {
            Caption = 'Forcer transporteur obligatoire';
            Description = 'CFR le 30/11/2022 => Régie : transporteur obligatoire (50405)';
        }

        field(50410; "Transporteur imperatif"; Boolean)
        {
            Description = 'MC Le 27-04-11 => SYMTA -> Transport';
        }
        field(50411; "Code Emballage"; Code[20])
        {
            Description = 'MC Le 27-04-11 => SYMTA -> Emballage';
            TableRelation = "Generals Parameters".Code WHERE(Type = CONST('EMBALLAGE'));
        }
        field(50412; "Regroupement Centrale"; Boolean)
        {
            Description = 'MC Le 04-05-11 => SYMTA -> Regroupement';
        }
        field(50500; Loans; Boolean)
        {
            Caption = 'Prêt';
            Description = '//PRET//';
        }
        field(50501; "Expiration Loans Date"; Date)
        {
            Caption = 'Date d''expiration prêt';
            Description = '//PRET//';
        }
        field(50502; "En attente de facturation"; Boolean)
        {
            Description = 'Pour ne pas facturer';
        }
        field(50503; "User En Attente"; Code[40])
        {
            Caption = 'Utilisateur mise en attente';
            Description = 'CFR le 22/09/2021 => Régie : BL suivi de l''utilisateur de mise en attente';
            Editable = false;
        }
        field(50600; "Scénario paiement"; Code[20])
        {
            Enabled = false;
            TableRelation = "Payment Class";
        }
        field(50610; "Preparation No."; Code[20])
        {
            Caption = 'N° BP';
            Description = ' MC Le 27-04-11 => SYMTA -> Garde en mémoire le numéro du BP.';
        }
        field(50611; "Preparation Valid Date"; Time)
        {
            Caption = 'Heure validation BP';
            Description = ' MC Le 27-04-11 => SYMTA -> Garde en mémoire la date de validation du bon de prépa.';
        }
        field(50700; "EDI Document"; Boolean)
        {
            Caption = 'Document EDI';
            Description = 'EDI => Indique si c''est de l''EDI';
            Editable = false;
        }
        field(50701; "EDI Document No."; Code[20])
        {
            Caption = 'N° Document EDI';
            Description = 'EDI => N° du document EDI';
            Editable = false;
        }
        field(50710; "EDI Integration Date"; Date)
        {
            Caption = 'Date d''intégration EDI';
            Description = 'EDI => Date d''intégration du document';
            Editable = false;
        }
        field(50711; "EDI Integration Time"; Time)
        {
            Caption = 'Heure d''intégration EDI';
            Description = 'EDI => Heure d''intégration du document';
            Editable = false;
        }
        field(50712; "EDI Integration User"; Code[20])
        {
            Caption = 'Utilisateur d''intégration EDI';
            Description = 'EDI => Utilisateur ayant lancé l''intégration du document';
            Editable = false;
        }
        field(50720; "EDI AR Generate"; Boolean)
        {
            Caption = 'AR EDI Généré';
            Description = 'EDI => ORDRSP Généré ?';
            Enabled = false;
        }
        field(50721; "EDI AR Date"; Date)
        {
            Caption = 'Date AR EDI';
            Description = 'EDI => Date ORDRSP';
            Enabled = false;
        }
        field(50722; "EDI AR Time"; Time)
        {
            Caption = 'Heure AR EDI';
            Description = 'EDI => Heure ORDRSP';
            Enabled = false;
        }
        field(50723; "EDI AR User"; Code[20])
        {
            Caption = 'Utilisateur AR EDI';
            Description = 'EDI => Utilisateur ayant généré l''ORDRSP';
            Enabled = false;
        }
        field(50730; "EDI Exportation"; Boolean)
        {
            Caption = 'Exportation EDI';
            Description = 'EDI => Document EDI exporté';
        }
        field(50731; "EDI Exportation Date"; Date)
        {
            Caption = 'Date d''exportation EDI';
            Description = 'EDI => Date d''exportation du document';
        }
        field(50732; "EDI Exportation Time"; Time)
        {
            Caption = 'Heure d''exportation EDI';
            Description = 'EDI => Heure d''exportation du document';
        }
        field(50733; "EDI Exportation User"; Code[20])
        {
            Caption = 'Utilisateur d''exportation EDI';
            Description = 'EDI => Utilisateur ayant lancé l''exportation du document';
        }
        field(50734; "EDI Exportation Code"; Code[20])
        {
            Caption = 'Code d''exportation EDI';
            Description = 'EDI => Code fichier de l''exportation';
        }
        field(50780; "Multi echéance"; Boolean)
        {
            Description = 'FB ->SYMTA';
        }
        field(50781; "Echéances fractionnées"; Boolean)
        {
        }
        field(50782; "Payment Terms Code 2"; Code[10])
        {
            Caption = 'Payment Terms Code';
            TableRelation = "Payment Terms";
        }
        field(50783; "Due Date 2"; Date)
        {
            Caption = 'date d''échéance 2';
        }
        field(50784; "Taux Premiere Fraction"; Decimal)
        {
        }
        field(50785; "Regroupement Facturation"; Enum "Regroupement Facturation")
        {
            Description = 'AD Le 28-04-2011 => SYMTA -> Type de regroupement des factures';
        }
        field(50790; "Do Not Print Active Ref"; Boolean)
        {
            Caption = 'Do Not Print Active Ref';
            Description = 'MCO Le 03-10-2018 => Régie';
            Enabled = false;
        }
        field(50799; "Order Create User"; Code[40])
        {
            Caption = 'Code utilisateur Création Cde';
            Description = 'TRAC';
            Editable = false;
        }
        field(50800; "Create User ID"; Code[40])
        {
            Caption = 'Code utilisateur Création';
            Description = 'TRAC';
            Editable = false;
            Enabled = false;
        }
        field(50801; "Create Date"; Date)
        {
            Caption = 'Date Création';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
            Enabled = false;
        }
        field(50802; "Create Time"; Time)
        {
            Caption = 'Heure Création';
            Description = 'TRACABILITE ENREGISTREMENT';
            Editable = false;
            Enabled = false;
        }
        field(50803; "Modify User ID"; Code[40])
        {
            Caption = 'Code utilisateur modif. Cde';
            Description = 'CFR le 18/11/2020';
            Editable = false;
            Enabled = false;
        }
        field(50804; "Modify Date"; Date)
        {
            Caption = 'Date modif. Cde';
            Description = 'CFR le 18/11/2020';
            Editable = false;
            Enabled = false;
        }
        field(50805; "Modify Time"; Time)
        {
            Caption = 'Heure modif. Cde';
            Description = 'CFR le 18/11/2020';
            Editable = false;
            Enabled = false;
        }
        field(50810; "Devis Web"; Boolean)
        {
            Description = 'MC Le 22-10-11 => SYMTA WEB';
        }
        field(50811; "Devis Web Intégré"; Boolean)
        {
            Description = 'AD Le 01-05-2016 => Pour spécifier qu''un devis est totalement intégré';
            Enabled = false;
        }
        field(50900; "Somme Qté livrée non facturée"; Decimal)
        {
            CalcFormula = Sum("Sales Shipment Line"."Qty. Shipped Not Invoiced" WHERE("Document No." = FIELD("No.")));
            Description = 'FB Le 10-05-2011 -> SYMTA Facturation';
            FieldClass = FlowField;
        }
        field(50901; "Entierement facturé"; Boolean)
        {
            Description = 'FB Le 10-05-2011 -> SYMTA Facturation';
        }
        field(50902; "Service Zone Code"; Code[10])
        {
            Caption = 'Service Zone Code';
            Description = 'GR Le 05-10-2015 => Affichage champ dans commande/devis';
            TableRelation = "Service Zone";
        }
        field(60200; "Packing List No."; Code[20])
        {
            Caption = 'N° liste de colisage';
            Description = 'WIIO -> ESKVN2.0 : PACKING V2 (Récup Navinégoce BC)';
        }
        field(70000; "Montant Cde Dbx"; Decimal)
        {
            Description = 'AD Le 21-09-2009 => FARGROUP -> Pour récupération du carnet de commande';
            Editable = false;
            Enabled = false;
        }
        field(70001; "Montant Port Dbx"; Decimal)
        {
            Description = 'AD Le 21-09-2009 => FARGROUP -> Pour récupération du carnet de commande';
            Editable = false;
            Enabled = false;
        }
        field(70002; "Remise Generale Dbx"; Decimal)
        {
            Description = 'AD Le 21-09-2009 => FARGROUP -> Pour récupération du carnet de commande';
            Enabled = false;
        }
    }
    keys
    {//todo
        // key(FKey1; "Entierement facturé", "Payment Method Code", "Payment Terms Code", "Bill-to Customer No.", "Regroupement Centrale", "Multi echéance", "Echéances fractionnées", "Sell-to Customer No.", "Regroupement Facturation", "Posting Date")
        // {
        // }
        // key(FKey2; "Entierement facturé", "Payment Method Code", "Payment Terms Code", "Bill-to Customer No.", "Regroupement Centrale", "Multi echéance", "Echéances fractionnées", "Invoice Customer No.", "Sell-to Customer No.", "Regroupement Facturation")
        // {
        // }
        // key(FKey3; Preparateur, "Posting Date")
        // {
        // }
        key(FKey4; "Preparation No.")
        {
        }
        key(FKey5; "Location Code", "Posting Date")
        {
        }
    }
}

