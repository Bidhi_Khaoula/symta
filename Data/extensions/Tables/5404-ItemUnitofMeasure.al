tableextension 50087 "Item Unit of Measure" extends "Item Unit of Measure" //5404
{
    fields
    {
        modify(Length)
        {
            Caption = 'Length';
            Description = '// AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3 (Modif Libelle)';
        }
        modify(Width)
        {
            Caption = 'Width';
            Description = '// AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3 (Modif Libelle)';
        }
        modify(Height)
        {
            Caption = 'Height';
            Description = '// AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3 (Modif Libelle)';
        }
        modify(Cubage)
        {
            Caption = 'Cubage';
            Description = '// AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3 (Modif Libelle)';
        }
        modify(Weight)
        {
            Caption = 'Weight';
            Description = '// AD Le 27-10-2009 => FARGROUP -> Les dimension sont en cm mais le cubage en m3 (Modif Libelle)';
        }


    }
}

