tableextension 50117 "Bin Content" extends "Bin Content" //7302
{
    fields
    {
        field(50000; "capacité"; Decimal)
        {
        }
        field(50001; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 02-11-2015 =>';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50002; Description; Text[100])
        {
            CalcFormula = Lookup(Item.Description WHERE("No." = FIELD("Item No.")));
            Caption = 'Description';
            Description = 'AD Le 02-11-2015 =>';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50005; _flag; Boolean)
        {
            Description = 'ANI Le 20-03-2015 => à TRUE si créé manuellement par le report 50062';
        }
        field(50010; "% Seuil Capacité Réap. Pick."; Decimal)
        {
            Description = 'AD Le 22-04-2015 => Seuil pour le réapprovisionnement casier Picking';
        }
        field(50020; "Return Vendor Name"; Text[50])
        {
            FieldClass = FlowField;
            CalcFormula = Lookup(Bin."Return Vendor Name" WHERE("Location Code" = FIELD("Location Code"),
                                                           Code = FIELD("Bin Code")));
            Caption = 'Name';
            Editable = false;
        }
        field(70000; "Référence Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'AD Le 10-05-2010 => Permet de stocker la ref active pour faire des recherches / Filtres';
            Editable = false;
            FieldClass = FlowField;
        }
        field(70010; "Last Movement Date"; Date)
        {
            CalcFormula = Max("Warehouse Entry"."Registering Date" WHERE("Location Code" = FIELD("Location Code"),
                                                                          "Bin Code" = FIELD("Bin Code"),
                                                                          "Item No." = FIELD("Item No."),
                                                                          "Variant Code" = FIELD("Variant Code"),
                                                                          "Unit of Measure Code" = FIELD("Unit of Measure Code"),
                                                                          "Lot No." = FIELD("Lot No. Filter"),
                                                                          "Serial No." = FIELD("Serial No. Filter")));
            Caption = 'Quantity (Base)';
            Description = 'PMA le 20-02-2019 ==> FE20190117';
            Editable = false;
            FieldClass = FlowField;
        }
        field(70011; "Last Entry Date"; Date)
        {
            CalcFormula = Max("Warehouse Entry"."Registering Date" WHERE("Location Code" = FIELD("Location Code"),
                                                                          "Bin Code" = FIELD("Bin Code"),
                                                                          "Item No." = FIELD("Item No."),
                                                                          "Variant Code" = FIELD("Variant Code"),
                                                                          "Unit of Measure Code" = FIELD("Unit of Measure Code"),
                                                                          "Lot No." = FIELD("Lot No. Filter"),
                                                                          "Serial No." = FIELD("Serial No. Filter"),
                                                                          "Qty. (Base)" = FILTER(> 0)));
            Caption = 'Date dernière entrée';
            Description = 'PMA le 20-02-2019 ==> FE20190117';
            Editable = false;
            FieldClass = FlowField;
        }
        field(70012; "Bin Content Creation Date"; Date)
        {
            Caption = 'Date création contenu emplacement';
            Description = 'PMA le 20-02-2019 ==> FE20190117';
        }
    }
    keys
    {
        key(FKey1; "Location Code", "Item No.", "Bin Code")
        {
        }
        key(FKey2; Default)
        {

        }
    }
}

