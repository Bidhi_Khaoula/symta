tableextension 50137 "BOM Component" extends "BOM Component" //90
{
    fields
    {
        field(50000; "Emplacement par Défaut"; Code[20])
        {
            CalcFormula = Lookup("Bin Content"."Bin Code" WHERE("Item No." = FIELD("No."),
                                                                 Fixed = CONST(True),
                                                                 Default = CONST(True)));
            Description = 'AD Le 17-12-2015';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50800; "Parent Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Parent Item No.")));
            Caption = 'Ref. Active nomenclature';
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
        field(50801; "Ref. Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("No.")));
            Description = 'MIG2015';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}

