tableextension 50122 "Warehouse Receipt Line" extends "Warehouse Receipt Line" //7317
{
    fields
    {
        field(50000; "Close Line"; Boolean)
        {
            Caption = 'Solder ligne';
            Description = 'AD Le 28-08-2009 => Possibilité de solder la ligne d''origine';
        }
        field(50001; "Ref. Active"; Code[20])
        {
            Description = 'AD Le 21-03-2013 => Plus facile pour les recherches';
            Editable = false;
        }
        field(50010; "Vendor Shipment No."; Code[35])
        {
            Caption = 'Vendor Shipment No.';
            Description = 'AD Le 24-03-2015 => Avoir le no de BL Fournisseur à la ligne';
        }
        field(50039; "Origine Type"; Option)
        {
            Caption = 'Destination Type';
            Description = 'AD Le 30-01-2020 => REGIE';
            Editable = false;
            OptionCaption = ' ,Customer,Vendor,Location';
            OptionMembers = " ",Customer,Vendor,Location;
        }
        field(50040; "Origin n No."; Code[20])
        {
            Caption = 'Destination No.';
            Description = 'AD Le 30-01-2020 => REGIE';
            Editable = false;
            TableRelation = IF ("Origine Type" = CONST(Customer)) Customer."No."
            ELSE
            IF ("Origine Type" = CONST(Vendor)) Vendor."No."
            ELSE
            IF ("Origine Type" = CONST(Location)) Location.Code;
        }
        field(50050; "N° Fusion Réception"; Code[20])
        {
            Description = 'AD Le 16-05-2011 => SYMTA -> Récéption Entrepot';
            TableRelation = "Warehouse Receipt Header";
        }
        field(50100; "N° sequence facture fourn."; Integer)
        {
        }
        field(50101; "Direct Unit Cost Fac"; Decimal)
        {
            AutoFormatType = 2;
            Caption = 'Direct Unit Cost';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
        }
        field(50102; "Line Discount % Fac"; Decimal)
        {
            Caption = 'Line Discount %';
            DecimalPlaces = 0 : 5;
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE -> Non editable';
            Editable = false;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50103; "Discount1 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50104; "Discount2 %"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50105; "Discount1 % Fac"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise1';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            Editable = true;
            MaxValue = 100;
            MinValue = 0;
        }
        field(50106; "Discount2 % Fac"; Decimal)
        {
            BlankZero = true;
            Caption = '% Remise2';
            Description = 'MC Le 09-05-2011 => Gestion des prix/Remise ESKAPE';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50200; "Qte Reçue théorique"; Decimal)
        {
            DecimalPlaces = 0 : 5;
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50201; "Prix Brut Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50202; "Remise 1 Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50203; "Remise 2 Théorique"; Decimal)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50210; "Litige prix"; Boolean)
        {
            Description = 'AD Le 20-03-3012 => Gestion des litiges';
        }
        field(50300; "Référence fournisseur"; Code[40])
        {
            Description = 'MCO Le 01-04-2015 => Régie';
            Editable = false;
        }
        field(50301; "Vendor Order No."; Code[35])
        {
            CalcFormula = Lookup("Purchase Header"."Vendor Order No." WHERE("Document Type" = CONST(Order),
                                                                             "No." = FIELD("Source No.")));
            Caption = 'Vendor Order No.';
            Description = 'PMA le 21-03-2018 (FE20180301)';
            FieldClass = FlowField;
        }
        field(50302; "Vendor Order Type"; Option)
        {
            CalcFormula = Lookup("Purchase Header"."Type de commande" WHERE("Document Type" = CONST(Order),
                                                                             "No." = FIELD("Source No.")));
            Caption = 'Vendor Order Type';
            Description = 'PMA le 28-03-2018 (FE20180321)';
            FieldClass = FlowField;
            OptionMembers = "Niveau 0","Niveau 1","Niveau 2","Niveau 3";
        }
        field(50310; "Receipt Adjustment"; Option)
        {
            Caption = 'Régularisation Réception';
            Description = 'ANI Le 27-11-2017 FE20171019';
            OptionMembers = " ","Echantillon fournisseur","Facturé","Reçu à la place de","Reçu en plus non signalé","Reçu en plus signalé","Réparé","Retour échantillon","Ste Retour NC-Echange","Manque pas signalé","Erreur entrée/NAV";
        }
        field(50311; "Receipt Request"; Option)
        {
            Caption = 'Demande Réception';
            Description = 'ANI Le 27-11-2017 FE20171019';
            OptionCaption = ' ,Demandé,Accepté,Expédié,Soldé';
            OptionMembers = " ",Requested,Accepted,Sent,Ended;
        }
    }
    keys
    {    //todo
        // key(FKey1; "Item No.", "N° Fusion Réception")
        // {
        //     SumIndexFields = "Qty. Outstanding (Base)";
        // }
        key(FKey2; "Source Type", "Source No.", "Source Line No.")
        {
            SumIndexFields = "Qty. to Receive (Base)";
        }
        //todo
        // key(FKey3; "Source Type", "Source No.", "Source Line No.", "N° sequence facture fourn.")
        // {
        //     MaintainSQLIndex = false;
        //     SumIndexFields = "Qty. to Receive";
        // }
        key(FKey4; "Item No.", "Bin Code")
        {
        }
    }

}

