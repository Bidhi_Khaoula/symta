tableextension 50036 "Item Journal Batch" extends "Item Journal Batch" //233
{
    fields
    {
        field(50000; MiniLoad; Boolean)
        {
            Description = '// Permet de spécifier si la feuille est utilisée pour MiniLoad ou non';
        }
        field(50001; "Code MiniLoad"; Code[20])
        {
            Description = '// Permet de gérer un numéro unique par feuille miniload';
        }
        field(50002; "Station MiniLoad"; Option)
        {
            Description = '// Permet de gérer le poste de picking';
            OptionMembers = PK_01,PK_02;
        }
        field(50003; PSM; Boolean)
        {
            Description = 'ANI Le 07-06-2017';
        }
        field(50010; "Type Feuille Reclassement WIIO"; Option)
        {
            Description = 'WIIO ->';
            Editable = false;
            OptionMembers = " ",Surstock,Picking,"Entrée Miniload","Sortie Miniload";
        }
        field(50011; "Nbre Ligne"; Integer)
        {
            CalcFormula = Count("Item Journal Line" WHERE("Journal Template Name" = FIELD("Journal Template Name"),
                                                           "Journal Batch Name" = FIELD(Name)));
            Description = 'WIIO ->';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}

