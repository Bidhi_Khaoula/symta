tableextension 50052 "Purchases & Payables Setup" extends "Purchases & Payables Setup" //312
{
    fields
    {
        field(50000; "Délai ETD -> ETA"; DateFormula)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
        }
        field(50001; "Délai ETA -> Réception"; DateFormula)
        {
            Description = 'AD Le 31-08-2009 => FARGROUP -> Gestion des dates pour les achats ASIE';
        }
        field(50010; "% Facture HK"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais hong kong';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50015; "% Assurance"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Frais assurance';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50020; "% Coef Réception"; Decimal)
        {
            Description = 'AD Le 22-09-2009 => FARGROUP -> Cout -> % Coef réception';
            MaxValue = 100;
            MinValue = 0;
        }
        field(50030; "Quadient Export Path"; Text[250])
        {
            Caption = 'Chemin export Quadient';
            Description = 'GRI le 11/01/2024';
        }
        field(50050; "Une seule réappro"; Boolean)
        {
            Description = 'MCO Le 01-04-2015 => Régie';
        }
        field(50051; "Magasin Retour"; Code[10])
        {
            Caption = 'Return location';
            Description = 'GB Le 19-09-2017 => Reprise dev perdu de la FE20150608';
            TableRelation = Location.Code;
        }
    }
}

