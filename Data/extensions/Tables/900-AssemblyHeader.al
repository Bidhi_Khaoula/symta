tableextension 50138 "Assembly Header" extends "Assembly Header" //900
{
    fields
    {
        field(50000; Checked; Boolean)
        {
            Caption = 'Vérifié';
            Description = 'CFR le 11/05/2022 => Régie : Champ pour identifier graphiquement les KITS vérifiés';
        }
        field(50080; "Ref Active"; Code[20])
        {
            CalcFormula = Lookup(Item."No. 2" WHERE("No." = FIELD("Item No.")));
            Description = 'MCO Le 09-12-2015 => Référence active de l''article.';
            Editable = false;
            FieldClass = FlowField;
        }
    }
}

