enumextension 50001 "Item Reference Type" extends "Item Reference Type" //5726
{

    value(4; Origine)
    {
        caption = 'Origine';
    }
    value(5; "Interne Origine")
    {
        caption = 'Interne Origine';
    }
    value(6; "Interne Verin")
    {
        caption = 'Interne Verin';
    }
    value(7; Achat)
    {
        caption = 'Achat';
    }
    value(8; Concurent)
    {
        caption = 'Concurent';
    }
    value(9; Identification)
    {
        caption = 'Identification';
    }
    value(10; "Ancienne Fournisseur")
    {
        caption = 'Ancienne Fournisseur';
    }
    value(11; "Ancienne Origine")
    {
        caption = 'Ancienne Origine';
    }
    value(12; "Ancienne Active")
    {
        caption = 'Ancienne Active';
    }
    value(13; Dbx)
    {
        caption = 'Dbx';
    }
    value(14; "Ste Fusion")
    {
        caption = 'Ste Fusion';
    }
    value(15; "Non Qualifié")
    {
        caption = 'Non Qualifié';
    }
}