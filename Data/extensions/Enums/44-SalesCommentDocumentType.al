enumextension 50006 "Sales Comment Document Type" extends "Sales Comment Document Type" //44
{
    value(10; "Retour garantie")
    {
        caption = 'Retour garantie';
    }
    value(11; "Incident transport")
    {
        caption = 'Incident transport';
    }
    value(12; "Sales Documents")
    {
        caption = 'Sales Documents';
    }
}