enumextension 50000 "Employee Marital Status" extends "Employee Marital Status" //5226
{
    value(5; "Marital Life")
    {
        caption = 'Marital Life';
    }
    value(6; PACS)
    {
        caption = 'PACS';
    }
    value(7; Separated)
    {
        caption = 'Separated';
    }
}